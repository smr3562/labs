BS=$1
ELF=$2
PSINIT=$3
while true; do
    read -p "Is "$BS" your bitstream file? [y/n]" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
while true; do
    read -p "Is "$ELF" your ELF file? [y/n]" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
while true; do
    read -p "Is "$PSINIT" your ps7_init.tcl file? [y/n]" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

source /opt/Xilinx/Vivado/2019.1/settings64.sh
echo "connect
connect -url TCP:127.0.0.1:3121

fpga $BS
targets 1 
source $PSINIT 
ps7_init
ps7_post_config
targets 2
rst -processor
dow $ELF
con
exit
" >>./tmp.tcl

#less tmp.tcl

/opt/Xilinx/SDK/2019.1/bin/xsct tmp.tcl
rm tmp.tcl
