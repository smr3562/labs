This scripts were written to interact with the remote computers of the lab in SMR3562. 


Be sure to set them as executable before running them. 

```
chmod +x load_zdbrd.sh
```

## program_fpga.sh

This script programs ONLY the FPGA loading the bitstream. 

### Syntax
```
source program_fpga.sh <bitstream.bit>
```

## load_ps.sh
This script loads into the ARM the ELF file and ps7_init.tcl running inmmediatly the program in the ARM

### Syntax
```
source load_ps.sh <elffile.elf> <ps7_init.tcl>
```

## load_zdbrd.sh
This script programs the FPGA Bitstream (.bit), and loads into the ARM processor the ELF file and ps7_init.tcl running inmmediatly the program in the ARM

### Syntax
```
source load_zdbrd.sh <bitstream.bit> <elffile.elf> <ps7_init.tcl>
```


