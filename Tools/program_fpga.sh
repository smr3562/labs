source /opt/Xilinx/Vivado/2019.1/settings64.sh

bitstr_path=$1
echo "# Connect to the Digilent cable
open_hw
connect_hw_server -url localhost:3121
current_hw_target [get_hw_targets]
open_hw_target

# Program and Refresh the Zedboard Device
set Device [lindex [get_hw_devices] 1]
current_hw_device \$Device
refresh_hw_device -update_hw_probes false \$Device
set_property PROGRAM.FILE \"$bitstr_path\" \$Device
program_hw_devices \$Device
refresh_hw_device \$Device
quit
" >>./tmp.tcl

vivado -mode tcl -source tmp.tcl
#less tmp.tcl
rm tmp.tcl
