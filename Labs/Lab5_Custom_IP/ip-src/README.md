# Custom components and Vivado projects (solutions)

Instructions:
* Create a project (`lab_custom_ip_c1/c2/c3`) for the `Zedboard`
* Add `<labs_repo>/Labs/Lab5_Custom_IP/ip-src/ip-repo` to the project (Settings -> IP -> Repository)
* In cases 2 and 3, add the VHDL source:
  * **Case 2:** `<labs_repo>/Labs/Lab5_Custom_IP/vhdl_src/case_2/pwm_simple.vhd`
  * **Case 3:** `<labs_repo>/Labs/Lab5_Custom_IP/vhdl_src/case_3/pwm_complete.vhd`
* Run in the *Tcl console* the command `source <labs_repo>/Labs/Lab5_Custom_IP/ip-src/case<X>.tcl` (`<X>` = 1, 2 or 3)
* Create a wrapper for the design (it must be the top level, if not, right click -> `Set as Top`)
* Generate the Bitstream
* Export Hardware (include the Bitstream)

* Launch SDK (2019.1 and before)
* Create an Empty Baremetal/Standalone Application
* Imports under `src` the file `<labs_repo>/Labs/Lab5_Custom_IP/c_src/sol/lab_custom_ip_c<X>.c`  (`<X>` = 1, 2 or 3)
* Program the FPGA, configure a serial terminal and run the Application
