
#include "xil_printf.h"

#include "xparameters.h"
#include "my_pwm_ip_c3.h"
#include "xil_io.h"
#include "xscugic.h"
#include "xil_exception.h"

XScuGic GicInstance;

volatile int countInterrupt=0;

void InterruptHandler(void *CallbackRef)
{
	countInterrupt++;
	MY_PWM_IP_C3_mWriteReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG0_OFFSET, 16+8+2+1 );
	MY_PWM_IP_C3_mWriteReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG0_OFFSET, 0+8+2+1 );
}

int SetupInterruptSystem(XScuGic *GicPtr)
{
	int Status;
	XScuGic_Config *GicConfig;

	Xil_ExceptionInit();

	GicConfig = XScuGic_LookupConfig(XPAR_PS7_SCUGIC_0_DEVICE_ID);
	if (NULL == GicConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(GicPtr, GicConfig,
				       GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,
			     (Xil_ExceptionHandler)XScuGic_InterruptHandler,
			     GicPtr);

	Status = XScuGic_Connect (GicPtr, XPS_FPGA0_INT_ID, (Xil_ExceptionHandler) InterruptHandler, NULL);
	if (Status != XST_SUCCESS) {xil_printf("\nError connecting IRQ from PL");return XST_FAILURE;}

	XScuGic_Enable(GicPtr, XPS_FPGA0_INT_ID);

	Xil_ExceptionEnable();
	return XST_SUCCESS;
}

//-----------------------------------------------------------
int main()
{

    int Status ;
    int i;
    int k;
    int valReg1,valReg3,valReg4;

    xil_printf("-- Start --\r\n");

    // reset
	MY_PWM_IP_C3_mWriteReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG0_OFFSET,0);
	MY_PWM_IP_C3_mWriteReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG0_OFFSET,1);

	MY_PWM_IP_C3_mWriteReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG2_OFFSET,0x7FFF);

	MY_PWM_IP_C3_mWriteReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG0_OFFSET,2+1);

	Status = SetupInterruptSystem(&GicInstance);
	if (Status != XST_SUCCESS) {
		xil_printf ("Setup Interrupt System failed!\n");
	}
	else{
		xil_printf ("Interrupt: OK\n");
	}

	MY_PWM_IP_C3_mWriteReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG0_OFFSET,8+2+1);

	while (1){
		for (k=0x3FFF; k>0x0; k--){
			MY_PWM_IP_C3_mWriteReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG2_OFFSET,k);
			for (i=0; i<9999; i++); // delay loop

			if( 0==k%5000)
			{
			  valReg1 = MY_PWM_IP_C3_mReadReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG1_OFFSET);
			  valReg3 = MY_PWM_IP_C3_mReadReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG3_OFFSET);
			  valReg4 = MY_PWM_IP_C3_mReadReg(XPAR_MY_PWM_IP_C3_0_S_AXI_BASEADDR, MY_PWM_IP_C3_S_AXI_SLV_REG4_OFFSET);
			  xil_printf("---------------------------------------------\n\r");
			  xil_printf("Register 1 (status): %d\n\r",valReg1);
			  xil_printf("Register 3 (current version): %d\n\r",valReg3);
			  xil_printf("Register 4 (pwm value): %d\n\r\n\r",valReg4);
			  xil_printf("Interrupt Counter: %d\n\r",countInterrupt);
			}
		}
	}

    return 0;
}
//-----------------------------------------------------------
//-- EOF
//-----------------------------------------------------------
