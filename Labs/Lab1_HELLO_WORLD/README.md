![LOGO](./../.img/LOGO.png)

# Lab 1: Vivado Design Flow for a Simple PS Design

Prepared by:
C. Sisterna & L. Crespo
ICTP-MLAB

# **Click [here](https://gitlab.com/smr3562/labs/-/wikis/Lab-1:-Vivado-Design-Flow-for-a-Simple-PS-Design) to go to the lab guide.**

## Introduction
This lab guides you through the process of using Vivado Development Suite to create a simple SoPC design targeting just the PS part of the Zynq-FPGA in the ZedBoard.  You will create the board design in the Vivado IP integrator, to export it to the SDK tool and generate the board support package and use an already done template to display the “Hello world” string in a console.

## Objectives
After completing this lab, you will be able to:

Create a Vivado project based in the IP Integrator
Add and configure the PS7
Generate HDL wrapper
Export the design to SDK
Create an application project in SDK, creating a board support package and ‘C’ code
Configure the UART to communicate the PC with the ZedBoard
Program the PS7 using the .elf file
Execute the ‘C’ code in the processor

#Description

The design consists of creating a simple project in which the PS7 will be configured to communicate with the PC to display the ‘Hello World’ string.
