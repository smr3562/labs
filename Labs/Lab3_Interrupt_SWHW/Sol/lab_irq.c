/*
 * lab_irq.c: interrupt test application
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>

#include "platform.h"
#include "xil_printf.h"
#include "xscugic.h"
#include "xparameters.h"
#include "xil_exception.h"
#include "xtmrctr.h"
#include "xil_io.h"

#include "math.h"

// xparameters definition of scugic device
#define INTERRUPT_DEVICE_ID 			XPAR_PS7_SCUGIC_0_DEVICE_ID
// xparameters definition of the interrupt fabric value for the Timer Interrupt
#define TIMER_FABRIC_INTERRUPT_ID		XPAR_FABRIC_AXI_TIMER_0_INTERRUPT_INTR

#define		pi 	3.141516
XScuGic InterruptController; /* Instance of the Interrupt Controller */
static XScuGic_Config *GicConfig;/* The configuration parameters of the
controller */

void Timer_InterruptHandler(void *InstancePtr, u8 TmrCtrNumber)
{
	int i;
	for(i = 0; i < 100; i++ )
		print("0,\n");
	// Stop timer, reset timer and start it
	XTmrCtr_Stop(InstancePtr,TmrCtrNumber);
	XTmrCtr_Reset(InstancePtr,TmrCtrNumber);
	XTmrCtr_Start(InstancePtr,TmrCtrNumber);
}

int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr)
{
	/*
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the ARM processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
			XScuGicInstancePtr);
	/*
	 * Enable interrupts in the ARM
	 */
	Xil_ExceptionEnable();
	return XST_SUCCESS;
}

int ScuGicInterrupt_Init(u16 DeviceId,XTmrCtr *TimerInstancePtr)
{
	int Status;
	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 * */
	GicConfig = XScuGic_LookupConfig(DeviceId);
	if (NULL == GicConfig) return XST_FAILURE;
	Status = XScuGic_CfgInitialize(&InterruptController, GicConfig,
		GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) return XST_FAILURE;
	// Setup the Interrupt System
	Status = SetUpInterruptSystem(&InterruptController);
	if (Status != XST_SUCCESS) return XST_FAILURE;
	/** Connect a device driver handler that will be called when an
	* interrupt for the device occurs, the device driver handler performs
	* the specific interrupt processing for the device
	*/
	Status = XScuGic_Connect(&InterruptController,
		TIMER_FABRIC_INTERRUPT_ID,
	(Xil_ExceptionHandler)XTmrCtr_InterruptHandler,
	(void *)TimerInstancePtr);
	if (Status != XST_SUCCESS) return XST_FAILURE;
	XScuGic_Enable(&InterruptController, TIMER_FABRIC_INTERRUPT_ID);
	return XST_SUCCESS;
}

int main()
{
	XTmrCtr TimerInstancePtr;
	int xStatus;

	print("##### Application Starts #####\n\r");
	print("\r\n");
	xStatus = XTmrCtr_Initialize(&TimerInstancePtr,XPAR_AXI_TIMER_0_DEVICE_ID);
	if(XST_SUCCESS != xStatus) print("TIMER INIT FAILED \n\r");

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//Set Timer Handler
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	XTmrCtr_SetHandler(&TimerInstancePtr, Timer_InterruptHandler, &TimerInstancePtr);
	XTmrCtr_SetResetValue(&TimerInstancePtr,0, 0xFCFFFFFF);

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//Setting timer Option (Interrupt Mode And Auto Reload )
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	XTmrCtr_SetOptions(&TimerInstancePtr,
		XPAR_AXI_TIMER_0_DEVICE_ID,
	(XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION ));

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//SCUGIC interrupt controller Initialization
	//Registration of the Timer ISR
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	xStatus = ScuGicInterrupt_Init(INTERRUPT_DEVICE_ID, &TimerInstancePtr);
	if(XST_SUCCESS != xStatus) print(" :( SCUGIC INIT FAILED \n\r");

	//Start Timer
	XTmrCtr_Start(&TimerInstancePtr,0);
	print("timer start \n\r");

	//Wait For interrupt;

	print("Wait for the Timer interrupt to trigger \r\n");
	print("########################################\r\n");
	print(" \r\n");

	float t = 0, value = 0;
	while (1){
		value = 1500 * sin((float)t/66);
		printf("%f,\n", value);
	    t++;
	    t = 0 ? t >= pi * 5000 :t;
	}
	cleanup_platform();
	return 0;
}