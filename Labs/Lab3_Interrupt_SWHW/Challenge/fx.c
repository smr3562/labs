/*
 * fx.c
 *
 *  Created on: Dec 7, 2018
 *      Author: kbo
 */

float fx(float x){
		double value;
    	if ((x>-7)&&(x<-3)){
    		value=3*sqrt(1-((x*x)/49)*sqrt(abs(abs(x)-3)/abs((abs(x)-3))));
    	}
    	else if ((x>=-3)&&(x<-1))
    		value=6*sqrt(10)/7+1.5+0.5*x-3*sqrt(10)/7*sqrt(-(x*x)-2*x+3);
    	else if ((x>=-1)&&(x<-0.75))
    		value=8*x+9;
    	else if ((x>=-0.75)&&(x<-0.5))
    		value=-3*x+0.75;
    	else if ((x>=-0.5)&&(x<0.5))
			value=2.25;
    	else if ((x>=0.5)&&(x<0.75))
    		value=3*x+0.75;
		else if ((x>=0.75)&&(x<1))
			value=-8*x+9;
		else if ((x>=1)&&(x<3))
			value=6*sqrt(10)/7+1.5-0.5*x-3*sqrt(10)/7*sqrt(-(x*x)+2*x+3);
		else if ((x>=3)&&(x<7))
			value=3*sqrt(1-((x*x)/49)*sqrt(abs(abs(x)-3)/abs((abs(x)-3))));
    	else{
    		value=0;
    	}
    	return value;
}
