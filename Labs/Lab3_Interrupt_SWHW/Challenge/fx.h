/*
 * fx.h
 *
 *  Created on: Dec 7, 2018
 *      Author: kbo
 */

#ifndef SRC_FX_H_
#define SRC_FX_H_

float fx(float x);

#endif /* SRC_FX_H_ */
