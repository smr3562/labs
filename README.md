![LOGO](./.img/LOGO.png)

# Joint ICTP-IAEA School on FPGA-based SoC and its Applications for Nuclear and Related Instrumentation | (smr 3562)

## Link to [event.](http://indico.ictp.it/event/9443/)


Lab Activities:
1. Lab 1:   [Hello World](./Lab1_HELLO_WORLD/README.md)
1. Lab 2:	GPIO (PMODS/CB)
1. Lab 3:	External Interrupt
1. Lab 4:	DMA
1. Lab 5:	Custom IP (with vhdl exercise)
1. Lab 6:	ComBlock
