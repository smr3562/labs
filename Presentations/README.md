![image](https://gitlab.com/smr3562/labs/-/raw/master/.img/LOGO.png)

## There are 2 ways to get the workshop files:

### 1. Using git in your PC

- [Install Git and Clone the project.](https://gitlab.com/smr3562/labs/-/wikis/How-to-install-Git-and-clone-the-project)

### 2. Downloading the files from GitLab

1. <a href="https://gitlab.com/smr3562/labs/-/tree/master" target="_blank">Click here</a>
to open the repository link.

2. Click the download button.

![image](https://gitlab.com/smr3562/labs/-/wikis/uploads/f70ba9a3544170854f3819ccdb4fa559/image.png)

3. Select the format to download the compressed.

4. Un-compress the repo and you are done.
