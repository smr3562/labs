# VHDL examples

Here you are some VHDL examples ready to run using GHDL (`bash run.py`).
You can also take the `.vhdl` files to use the Vivado simulator.

These were companion examples of the following presentations:
* [VHDL for Synthesis](http://indico.ictp.it/event/9443/session/237/contribution/536/material/slides/)
* [VHDL for Simulation](http://indico.ictp.it/event/9443/session/250/contribution/563/material/slides/)

More about GHDL (a free and open-source VHDL simulator):
* [Repository](https://github.com/ghdl/ghdl)
* [Quick Start Guide](https://ghdl.github.io/ghdl/quick_start/index.html)
* [Docker Containers](https://github.com/ghdl/docker)
* [Windows package](https://hdl.github.io/containers/#_usage) (and other FOSS tools).
