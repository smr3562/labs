----------------------------------------------------------------------------------
-- Company: 
-- Engineer: M. Bogovac
-- 
-- Create Date: 04/07/2020 07:37:31 AM
-- Design Name: 
-- Module Name: test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: calculates the following recursive relation
--     y(n) = x(n) - u(n)
--     u(n) = u(n-1) + (1/2^m)*y(n)
--   it is the same as 
--     y(n) = x(n) - (1/2^m)*u(n)    subtract scaled
--     u(n) = u(n-1) + y(n)          accumulate

-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dc_stabilizer is
   Port 
      (  
      x : in std_logic_vector(15 downto 0);
	  feedback : in std_logic_vector(15 downto 0);   
      y : out std_logic_vector(15 downto 0);           
      threshold_high : in std_logic_vector(15 downto 0);   
	  threshold_low : in std_logic_vector(15 downto 0);   
      resetn : in std_logic;
      clk : in std_logic;
      dc_stab_enable : in std_logic;
	  m: in std_logic_vector (4 downto 0);
	  dbg_acc : out std_logic_vector (15 downto 0)
      );
end dc_stabilizer;

architecture Behavioral of dc_stabilizer is
   
   signal en : std_logic;
--    signal m : integer := 7;
   signal y16_reg     : signed(15 downto 0) := (others => '0');       
   signal y16         : signed(15 downto 0);        
   signal y32         : signed(31 downto 0);        
   signal x16         : signed(15 downto 0);        
   signal x32         : signed(31 downto 0);        
   signal u32         : signed(31 downto 0);        
   signal acc32       : signed(31 downto 0) := (others => '0');        
   signal threshold_high16 : signed(15 downto 0);        
   signal threshold_low16 : signed(15 downto 0);        
   
   signal feedback16  : signed(15 downto 0);
   signal feedback32  : signed(31 downto 0);
   
begin

   -- inputs conversion to sign
   x16 <= signed(x);
   feedback16 <= signed(feedback);
   x32 <= resize(x16,32);
   feedback32 <= resize(feedback16,32);
   threshold_high16 <= signed(threshold_high);
   threshold_low16 <= signed(threshold_low);
   dbg_acc <= std_logic_vector(resize(acc32,16));
   
   
   
   
   
   --y32 <= x32 - u32;
            
   	subtractor1 : process (clk)
    variable result: signed(31 downto 0);
	begin
		if resetn = '0' or dc_stab_enable = '0' then
			result := x32;
		elsif clk='1' and clk'event then
			result := x32 - u32;
			--check for + overflow in 16 bit sign arithmetic
			if(result(31)='0' and result(15)='1') then 
				--note: the maximal number is valid only for BLR_DATA_WIDTH=16
				--generalization requires to write function (TODO in the next version)
				result := x"00007fff";
			--check for - overflow in 16 bit sign arithmetic
			elsif(result(31)='1' and result(15)='0') then 		
				--note: the minimal number is valid only for BLR_DATA_WIDTH=16
				--generalization requires to write function (TODO in the next version)
				result := x"FFFF8000";
			end if;
		end if;
		y32 <= result;
	end process;
   
   
   
   
   
   
   
   y16 <= resize(y32,16); 
   y16_register :process (clk)   
   begin
      if clk'event and clk ='1' then
         if resetn = '0' then
            y16_reg <= (others => '0');
         else
            y16_reg <= y16;
         end if;
      end if;
   end process;
   y <= std_logic_vector(y16_reg);-- when dc_stab_enable = '1' else x;

 
   -- implementation of the recursive relation 
   -- enable accumulator
   -- compare threshold with registered output
     
   --en <= '1' when feedback16 <  threshold_high16 and feedback16 > threshold_low16 and dc_stab_enable = '1' else '0';
   en <= '1' when feedback16 <  threshold_high16 and feedback16 > threshold_low16 else '0';
    
   
    
   -- calculate (1/2^m)*u(n)
   
   u32 <= shift_right(acc32,to_integer(unsigned(m)));
   -- calculate u(n) = u(n-1) + y(n)
   
   accumulator : process (clk)
   begin
      if clk='1' and clk'event then
	     if resetn = '0' or dc_stab_enable= '0' then
            acc32 <= (others => '0');
		 elsif en = '1' then
		    acc32 <= acc32 + feedback32;
		 end if;
      end if;
   end process;

    --
    -- end of implementation of the recursive relation
    --

end Behavioral;
