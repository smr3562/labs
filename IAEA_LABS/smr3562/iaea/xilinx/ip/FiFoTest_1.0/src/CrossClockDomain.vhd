library ieee;
use ieee.std_logic_1164.all;
 
entity CrossClockDomain is

  port (
    clk : in std_logic;

    rst : in std_logic;
 
    -- Write port
    data_in : in std_logic;

    -- Read port
    
    data_out : out std_logic
 
    );
end CrossClockDomain;
 
architecture rtl of CrossClockDomain is
 
   signal sinhroFF : std_logic;
   signal outFF: std_logic;
begin
 
 
  -- prvi FF
  process(data_in, outFF,rst)
  begin
  
    if outFF = '1' or rst = '1' then
	   sinhroFF <= '0';
	elsif rising_edge(data_in) then
	   sinhroFF <= '1';
	end if;
	
  end process;
 
 
  -- drugi FF
  process(clk, rst)
  begin
  
    if rst = '1' then
	   outFF <= '0';
	elsif rising_edge(clk) then
	   outFF  <= sinhroFF;
	end if;
		
  end process;
  data_out <= outFF;
 
 
end architecture;