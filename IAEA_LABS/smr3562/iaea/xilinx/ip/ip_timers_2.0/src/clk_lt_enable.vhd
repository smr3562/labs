----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/24/2019 05:17:18 PM
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


--
-- Unsigned Up Counter with Asynchronous Reset and Clock Enable
--
entity clk_lt_enable is
	port(  clk, clr, ce: in std_logic;
           dir : in std_logic_vector(1 downto 0);
           lt_clk_en : out std_logic);
end clk_lt_enable;
	    
architecture Behavioral of clk_lt_enable is
    signal lt_clk_en_i : std_logic; 
	signal tmp : std_logic_vector(16 downto 0):="00000000000000000";
	constant DIVISOR : std_logic_vector(16 downto 0):= "11000011010011111";--"1869F"; --99999, for clk=50MHz

begin
    
    lt_clk_en <= lt_clk_en_i;

	process (clk, clr, ce)
	begin
		if (clr='1') then--or tmp >= DIVISOR) then
			tmp <= (others => '0');
            lt_clk_en_i <= '0';
		elsif (clk'event and clk='1') then
           if (tmp >= DIVISOR) then
			   tmp <= (others => '0');
			   lt_clk_en_i <= '1';
		   elsif (ce='1') then
		      lt_clk_en_i <= '0';
              if( dir = "00") then
                 --tmp <= tmp + X"0001"; -- count forward (in -1,0,1 pattern)
                 tmp <= tmp + "00000000000000010";   -- (0,1,2) 
              elsif (dir = "01") then
                 --tmp <= tmp - "0001";  -- count backward (in -1,0,1 pattern) 
                 tmp <= tmp;             -- (0,1,2) 
              else
                 --tmp <= tmp ;          -- don't count (in -1,0,1 pattern)
                 tmp <= tmp + "00000000000000001";   -- (0,1,2) 
              end if;
		      --end if;
			end if;
		end if;
	end process;

end Behavioral;