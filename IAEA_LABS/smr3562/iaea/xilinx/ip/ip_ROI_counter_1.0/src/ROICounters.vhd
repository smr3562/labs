library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--use work.fifo_pkg.all;

entity ROICounters is
   port
      (      
      srst				: in std_logic; --from ROI FIFO
	  reset: in std_logic;
	  clk				: in std_logic;
	  sense          : in std_logic;                        -- Low to high or high to low clears countrs and writes to FIFO        
      amp_value	   : in  std_logic_vector(15 downto 0);
	  amp_ready	   : in  std_logic;
	  wr_en				 : out std_logic;
      data_out			 : out std_logic_vector (79 downto 0);	  	  
	  
	  ROI_limH_1         : in  std_logic_vector(15 downto 0);
	  ROI_limL_1         : in  std_logic_vector(15 downto 0);
	  ROI_limH_2         : in  std_logic_vector(15 downto 0);
	  ROI_limL_2         : in  std_logic_vector(15 downto 0);
	  ROI_limH_3         : in  std_logic_vector(15 downto 0);
	  ROI_limL_3         : in  std_logic_vector(15 downto 0);
	  ROI_limH_4         : in  std_logic_vector(15 downto 0);
	  ROI_limL_4         : in  std_logic_vector(15 downto 0);
	  ROI_limH_5         : in  std_logic_vector(15 downto 0);
	  ROI_limL_5         : in  std_logic_vector(15 downto 0)

      );
end ROICounters;



architecture rtl of ROICounters is
	                                                                                                                   
-- Define the states of state machine
--    type    state is (IDLE, SEND_STREAM, CLR_ROI);                                                           
--    signal  sm_state : state := IDLE;                                                   
    
    -- internal signals
signal strobe: std_logic;
signal hit1, hit2, hit3, hit4, hit5: std_logic;
signal counter1, counter2, counter3, counter4, counter5: std_logic_vector(15 downto 0);
signal enable1_i, enable2_i, enable3_i, enable4_i, enable5_i :std_logic;

component comparator is
   generic
      (
	  DATA_WIDTH : integer :=16
	  );
   port
      (
	  data, ThL, ThH : in std_logic_vector(DATA_WIDTH-1 downto 0);
	  hit : out std_logic
	  );
end component;


component counter is
   generic
      (
	  DATA_WIDTH : integer :=16
	  );
   port
      (
	  aresetn: in std_logic;
	  clk, clear, enable : in std_logic;
	  count : out std_logic_vector(DATA_WIDTH-1 downto 0)
	  );
end component;


component rf_edge_trigger is
   port
      (
	  aresetn: in std_logic;
	  clk : in std_logic;
      x : in std_logic;
      trigger : out std_logic
	  );
end component;



    
begin
    --
    -- detect when 'start' signal changes its state (toggled)
    -- 
	
   U0: rf_edge_trigger
   port map(aresetn=> reset, clk=>clk, x=>sense, trigger=>strobe);



   enable1_i <= hit1 and amp_ready;
   enable2_i <= hit2 and amp_ready;
   enable3_i <= hit3 and amp_ready;   
   enable4_i <= hit4 and amp_ready;
   enable5_i <= hit5 and amp_ready;
   

   
   CMP1: comparator
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  data => amp_value,
	  ThL =>ROI_limL_1,
	  ThH => ROI_limH_1,
	  hit =>hit1
	  );

   CMP2: comparator
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  data => amp_value,
	  ThL =>ROI_limL_2,
	  ThH => ROI_limH_2,
	  hit =>hit2
	  );

   CMP3: comparator
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  data => amp_value,
	  ThL =>ROI_limL_3,
	  ThH => ROI_limH_3,
	  hit =>hit3
	  );

   CMP4: comparator
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  data => amp_value,
	  ThL =>ROI_limL_4,
	  ThH => ROI_limH_4,
	  hit =>hit4
	  );
	
   CMP5: comparator
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  data => amp_value,
	  ThL =>ROI_limL_5,
	  ThH => ROI_limH_5,
	  hit =>hit5
	  );	
	
   CNT1: counter
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  aresetn=> reset,
	  clk=> clk,
	  clear=> strobe or srst,
	  enable=> enable1_i,
	  count => counter1
	  );

   CNT2: counter
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  aresetn=> reset,
	  clk=> clk,
	  clear=> strobe or srst,
	  enable=> enable2_i,
	  count => counter2
	  );

   CNT3: counter
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  aresetn=> reset,
	  clk=> clk,
	  clear=> strobe or srst,
	  enable=> enable3_i,
	  count => counter3
	  );

   CNT4: counter
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  aresetn=> reset,
	  clk=> clk,
	  clear=> strobe or srst,
	  enable=> enable4_i,
	  count => counter4
	  );

   CNT5: counter
   generic map
      (
	  DATA_WIDTH =>16
	  )
   port map
      (
	  aresetn=> reset,
	  clk=> clk,
	  clear=> strobe or srst,
	  enable=> enable5_i,
	  count => counter5
	  );
   wr_en <= strobe;
   data_out <= counter1&counter2&counter3&counter4&counter5;
	
	
	                                                                             
end rtl;
