----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/24/2019 04:55:28 PM
-- Design Name: 
-- Module Name: comparator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


--
-- Unsigned N-Bit Window Comparator
--
entity comparator is
   generic
      (
	  DATA_WIDTH : integer :=16
	  );
   port
      (
	  data, ThL, ThH : in std_logic_vector(DATA_WIDTH-1 downto 0);
	  hit : out std_logic
	  );
end comparator;

architecture Behavioral of comparator is
   signal cmp1 : std_logic;
   signal cmp2 : std_logic;
   begin
	  cmp1 <= '1' when data >= ThL else '0';
	  cmp2 <= '1' when data <= ThH else '0';
	  hit <= cmp1 and cmp2;
end Behavioral;

