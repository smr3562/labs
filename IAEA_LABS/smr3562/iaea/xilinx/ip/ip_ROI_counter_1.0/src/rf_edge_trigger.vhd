----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/17/2020 08:54:28 AM
-- Design Name: 
-- Module Name: rf_edge_trigger - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rf_edge_trigger is
   port
      (
	  aresetn: in std_logic;
	  clk : in std_logic;
      x : in std_logic;
      trigger : out std_logic)
	  ;
end rf_edge_trigger;

architecture Behavioral of rf_edge_trigger is

   signal x_meta : std_logic;
   signal x_last : std_logic;
   signal x_current : std_logic;

begin

--step 1. sync x with clock (if already sync then process cn be replaced with 'x_current <= x')
process (clk,aresetn) is
   begin
   if (aresetn = '0') then
      x_meta <='0';
	  x_current <= '0';
   elsif rising_edge(clk) then
      x_meta <= x;
      x_current <= x_meta;
   end if;
end process;
    
    --step 2. delay the current value 'x_current' by one clock cycle.
x_last <= x_current when rising_edge(clk);
    
--step 3. detect change using xor
process(x_current, x_last)
   begin
   if    (x_current xor x_last) = '1' then
      trigger <= '1';
   else
      trigger <= '0';
   end if;    
end process;

end Behavioral;
