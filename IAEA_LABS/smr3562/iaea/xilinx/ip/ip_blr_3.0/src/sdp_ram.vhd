----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/24/2020 12:15:37 AM
-- Design Name: 
-- Module Name: sdp_ram - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sdp_ram is
   generic(
			RAM_DATA_WIDTH : integer :=32;
			RAM_ADDRESS_WIDTH : integer :=10
		);
    port(
        clk : in std_logic;
		en : in std_logic;
        addra : in std_logic_vector(RAM_ADDRESS_WIDTH-1 downto 0);
        addrb : in std_logic_vector(RAM_ADDRESS_WIDTH-1 downto 0);
        dia : in std_logic_vector(RAM_DATA_WIDTH-1 downto 0);
        dob : out std_logic_vector(RAM_DATA_WIDTH-1 downto 0)
    );
end sdp_ram;

architecture Behavioral of sdp_ram is
--
-- we want to infer Simple Dual Port (SDP) RAM in READ_FIRST mode 
-- 
type ram_type is array (1023 downto 0) of std_logic_vector(RAM_DATA_WIDTH-1 downto 0);
shared variable RAM : ram_type;
begin
--
-- If you want ro use below code, add we, ena and enb ports
--
--process(clk)
--begin
--		if clk'event and clk = '1' then
--			if enb = '1' then
--          	dob <= RAM(conv_integer(addrb));
--			end if;
--
--			if ena = '1' then
--				if wea = '1' then
--					RAM(conv_integer(addra)) := dia;
--				end if;
--			end if;
--		end if;            
--end process;
--https://forums.xilinx.com/t5/Synthesis/Is-there-a-way-to-infer-simple-dual-port-block-RAMs-in-READ/td-p/689270	
    process(clk) 
    begin
        if clk'event and clk = '1' then
			dob <= RAM(conv_integer(addrb));
			if(en='1') then
				RAM(conv_integer(addra)) := dia;
			end if;
        end if;            
    end process;
   
end Behavioral;
