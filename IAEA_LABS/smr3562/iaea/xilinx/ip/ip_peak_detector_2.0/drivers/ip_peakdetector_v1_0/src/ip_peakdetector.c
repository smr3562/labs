#include "ip_peakdetector.h"

int ip_peakdetector_CfgInitialize(ip_peakdetector *InstancePtr, ip_peakdetector_Config *ConfigPtr)
{
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

	/* Clear instance memory and make copy of configuration*/
	memset(InstancePtr, 0, sizeof(ip_peakdetector));
	memcpy(&InstancePtr->Config, ConfigPtr, sizeof(ip_peakdetector_Config));

    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;
 	
    return XST_SUCCESS;
}


static u32 ip_peakdetector_offset[IP_PEAKDETECTOR_PRM_CNT] =
{
	0							, //Tclk - not used in FPGA logic;
	(IP_PEAKDETECTOR_R4_X_DELAY), //x_delay
	(IP_PEAKDETECTOR_R5_X_NOISE), //x_noise
	(IP_PEAKDETECTOR_R6_X_MIN)	, //x_min
	(IP_PEAKDETECTOR_R7_X_MAX) 	, //x_max
	(IP_PEAKDETECTOR_R8_EN_PKD)	  //en_pkd
};

// register read/write access
// bit 0: 1=Readable 0=not readable
// bit 1: 1=writable 0=not writable
static int ip_peakdetector_rw[IP_PEAKDETECTOR_PRM_CNT] =
{
	0,		//Tclk;
	
	3,		//x_delay;
	3,		//x_noise;
	3,		//x_min;
	3,		//x_max;
	3		//en_pkd;
};

//
// read from registers
//
void ip_peakdetector_ReadLogic(ip_peakdetector *pkd)
{
	u32 baseaddr;
	u32 *prm;
	int i, r;

	baseaddr = pkd->Config.BaseAddress;
	prm = pkd->Config.prm;
	
	for(i=0;i<IP_PEAKDETECTOR_PRM_CNT;i++)
	{
		//read parameter from FPGA Logic if its register is readable
		r = (ip_peakdetector_rw[i] & 1);
		if (r != 1) continue;
		//u32 *addr = baseaddr + ip_peakdetector_offset[i];
		//prm[i] = *( (volatile u32*)(addr) );
		prm[i] = ip_peakdetector_ReadReg(baseaddr, ip_peakdetector_offset[i]);
	}
}

//
// write to registers
//
void ip_peakdetector_WriteLogic(ip_peakdetector *pkd)
{
	u32 baseaddr;
	u32 *prm;
	int i, r;

	baseaddr = pkd->Config.BaseAddress;
	prm = pkd->Config.prm;
	
	for(int i=0;i<IP_PEAKDETECTOR_PRM_CNT;i++)
	{
		//write parameter to the FPGA Logic if its register is writable
		r = (ip_peakdetector_rw[i] & 2);
		if (r != 2) continue;
		//u32 *addr = baseaddr + ip_peakdetector_offset[i];
		//*( (volatile u32*)(addr) ) = prm[i];
		ip_peakdetector_WriteReg(baseaddr, ip_peakdetector_offset[i], prm[i]);
	}
}

