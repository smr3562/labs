
#include "xparameters.h"
#include "ip_peakdetector.h"

/*
* The configuration table for devices
*/

ip_peakdetector_Config ip_peakdetector_ConfigTable[XPAR_IP_PEAKDETECTOR_NUM_INSTANCES] =
{
	{
		XPAR_IP_PEAKDETECTOR_0_DEVICE_ID,
		XPAR_IP_PEAKDETECTOR_0_AXIBUSDOMAIN_S_AXI_BASEADDR,
	
		20,			//prm[ 0] = Tclk: 	clock period time in nsec
		
		15,			//prm[ 1] = x_delay = (int) ((0.1*IpShaper.ConfigPtr->Taupk+IpShaper.ConfigPtr->Taupk_top)/(double)IpShaper.ConfigPtr->Tclk);=					
		81,			//prm[ 2] = x_noise = (0.005,16,14);;
		163,		//prm[ 3] = x_min 	= (0.01,16,14);
		32604,		//prm[ 4] = x_max 	= (1.99,16,14);
		1 			//prm[ 5] = en_pkd 	= 1;
	}
};


