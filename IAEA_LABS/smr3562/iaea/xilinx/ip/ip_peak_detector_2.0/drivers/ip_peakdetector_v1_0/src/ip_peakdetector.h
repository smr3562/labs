#ifndef IP_PEAKDETECTOR__H
#define IP_PEAKDETECTOR__H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/

#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#include "ip_peakdetector_hw.h"

/**************************** Type Definitions ******************************/
#define IP_PEAKDETECTOR_PRM_CNT 			6
#define IP_PEAKDETECTOR_PRM_INDEX_TCLK		0
#define IP_PEAKDETECTOR_PRM_INDEX_X_DELAY	1
#define IP_PEAKDETECTOR_PRM_INDEX_X_NOISE	2
#define IP_PEAKDETECTOR_PRM_INDEX_X_MIN		3
#define IP_PEAKDETECTOR_PRM_INDEX_X_MAX		4
#define IP_PEAKDETECTOR_PRM_INDEX_EN_PKD	5

typedef struct {
    u16 DeviceId;
    u32 BaseAddress;
//***************************************************
// user defined parameters
//***************************************************
//	prm[0] = Tclk; 			Tclk
//	prm[1] = x_delay; 		delay
//	prm[2] = x_noise;		noise amplitude;
//	prm[3] = x_min;			LLD
//	prm[4] = x_max;			ULD
//	prm[5] = en_pkd;		1=enable;0=disable
	u32 prm[IP_PEAKDETECTOR_PRM_CNT];
	
} ip_peakdetector_Config;

/**
* The ip_peakdetector driver instance data. The user is required to
* allocate a variable of this type for every ip_peakdetector device in the system.
* A pointer to a variable of this type is then passed to the driver
* API functions.
*/
typedef struct {

	ip_peakdetector_Config Config;
    u32 IsReady;
	
} ip_peakdetector;
/***************** Macros (Inline Functions) Definitions *********************/

#define ip_peakdetector_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define ip_peakdetector_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes *****************************/

int ip_peakdetector_Initialize(ip_peakdetector *InstancePtr, u16 DeviceId);
ip_peakdetector_Config* ip_peakdetector_LookupConfig(u16 DeviceId);
int ip_peakdetector_CfgInitialize(ip_peakdetector *InstancePtr, ip_peakdetector_Config *ConfigPtr);

void ip_peakdetector_ReadLogic(ip_peakdetector *pkd);
void ip_peakdetector_WriteLogic(ip_peakdetector *pkd);

#ifdef __cplusplus
}
#endif

#endif
