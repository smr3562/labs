-------------------------------------------------------------------
-- System Generator version 2018.3 VHDL source file.
--
-- Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity ip_peakdetector_xlAsynRegister is

   generic (d_width          : integer := 5;          -- Width of d input
            init_value       : bit_vector := b"00");  -- Binary init value string

   port (d     : in std_logic_vector (d_width-1 downto 0);
         rst   : in std_logic_vector(0 downto 0) := "0";
         en    : in std_logic_vector(0 downto 0) := "1";
         d_ce  : in std_logic;
         d_clk : in std_logic;
         q_ce  : in std_logic;
         q_clk : in std_logic;
         q     : out std_logic_vector (d_width-1 downto 0));

end ip_peakdetector_xlAsynRegister;

architecture behavior of ip_peakdetector_xlAsynRegister is

   component synth_reg_w_init
      generic (width      : integer;
               init_index : integer;
               init_value : bit_vector;
               latency    : integer);
      port (i   : in std_logic_vector(width-1 downto 0);
            ce  : in std_logic;
            clr : in std_logic;
            clk : in std_logic;
            o   : out std_logic_vector(width-1 downto 0));
   end component; -- end synth_reg_w_init

   signal internal_d_clr      : std_logic;
   signal internal_d_ce       : std_logic;
   signal internal_q_clr      : std_logic;
   signal internal_q_ce       : std_logic;

   signal d1_net              : std_logic_vector (d_width-1 downto 0);
   signal d2_net              : std_logic_vector (d_width-1 downto 0);
   signal d3_net              : std_logic_vector (d_width-1 downto 0);

begin

   internal_d_clr <= rst(0) and d_ce;
   internal_d_ce  <= en(0) and d_ce;
   -- drive default values on enable and clear ports
   internal_q_clr <= '0' and q_ce;
   internal_q_ce  <= '1' and q_ce;

   -- Synthesizable behavioral model
   synth_reg_inst_0 : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d,
                ce  => internal_d_ce,
                clr => internal_d_clr,
                clk => d_clk,
                o   => d1_net);

   synth_reg_inst_1 : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d1_net,
                ce  => internal_q_ce,
                clr => internal_q_clr,
                clk => q_clk,
                o   => d2_net);

   synth_reg_inst_2 : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d2_net,
                ce  => internal_q_ce,
                clr => internal_q_clr,
                clk => q_clk,
                o   => d3_net);

   synth_reg_inst_3 : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d3_net,
                ce  => internal_q_ce,
                clr => internal_q_clr,
                clk => q_clk,
                o   => q);

end architecture behavior;


library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_254866499c is
  port (
    input_port : in std_logic_vector((16 - 1) downto 0);
    output_port : out std_logic_vector((16 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_254866499c;
architecture behavior of sysgen_reinterpret_254866499c
is
  signal input_port_1_40: signed((16 - 1) downto 0);
  signal output_port_5_5_force: unsigned((16 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_signed(input_port);
  output_port_5_5_force <= signed_to_unsigned(input_port_1_40);
  output_port <= unsigned_to_std_logic_vector(output_port_5_5_force);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_2ef5d3fc33 is
  port (
    op : out std_logic_vector((14 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_2ef5d3fc33;
architecture behavior of sysgen_constant_2ef5d3fc33
is
begin
  op <= "10000000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

--$Header: /devl/xcs/repo/env/Jobs/sysgen/src/xbs/blocks/xlconvert/hdl/xlconvert.vhd,v 1.1 2004/11/22 00:17:30 rosty Exp $
---------------------------------------------------------------------
--
--  Filename      : xlconvert.vhd
--
--  Description   : VHDL description of a fixed point converter block that
--                  converts the input to a new output type.

--
---------------------------------------------------------------------


---------------------------------------------------------------------
--
--  Entity        : xlconvert
--
--  Architecture  : behavior
--
--  Description   : Top level VHDL description of fixed point conver block.
--
---------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity convert_func_call_ip_peakdetector_xlconvert is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        result : out std_logic_vector (dout_width-1 downto 0));
end convert_func_call_ip_peakdetector_xlconvert ;

architecture behavior of convert_func_call_ip_peakdetector_xlconvert is
begin
    -- Convert to output type and do saturation arith.
    result <= convert_type(din, din_width, din_bin_pt, din_arith,
                           dout_width, dout_bin_pt, dout_arith,
                           quantization, overflow);
end behavior;


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity ip_peakdetector_xlconvert  is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        en_width     : integer := 1;
        en_bin_pt    : integer := 0;
        en_arith     : integer := xlUnsigned;
        bool_conversion : integer :=0;           -- if one, convert ufix_1_0 to
                                                 -- bool
        latency      : integer := 0;             -- Ouput delay clk cycles
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        en  : in std_logic_vector (en_width-1 downto 0);
        ce  : in std_logic;
        clr : in std_logic;
        clk : in std_logic;
        dout : out std_logic_vector (dout_width-1 downto 0));

end ip_peakdetector_xlconvert ;

architecture behavior of ip_peakdetector_xlconvert  is

    component synth_reg
        generic (width       : integer;
                 latency     : integer);
        port (i       : in std_logic_vector(width-1 downto 0);
              ce      : in std_logic;
              clr     : in std_logic;
              clk     : in std_logic;
              o       : out std_logic_vector(width-1 downto 0));
    end component;

    component convert_func_call_ip_peakdetector_xlconvert 
        generic (
            din_width    : integer := 16;            -- Width of input
            din_bin_pt   : integer := 4;             -- Binary point of input
            din_arith    : integer := xlUnsigned;    -- Type of arith of input
            dout_width   : integer := 8;             -- Width of output
            dout_bin_pt  : integer := 2;             -- Binary point of output
            dout_arith   : integer := xlUnsigned;    -- Type of arith of output
            quantization : integer := xlTruncate;    -- xlRound or xlTruncate
            overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
        port (
            din : in std_logic_vector (din_width-1 downto 0);
            result : out std_logic_vector (dout_width-1 downto 0));
    end component;


    -- synthesis translate_off
--    signal real_din, real_dout : real;    -- For debugging info ports
    -- synthesis translate_on
    signal result : std_logic_vector(dout_width-1 downto 0);
    signal internal_ce : std_logic;

begin

    -- Debugging info for internal full precision variables
    -- synthesis translate_off
--     real_din <= to_real(din, din_bin_pt, din_arith);
--     real_dout <= to_real(dout, dout_bin_pt, dout_arith);
    -- synthesis translate_on

    internal_ce <= ce and en(0);

    bool_conversion_generate : if (bool_conversion = 1)
    generate
      result <= din;
    end generate; --bool_conversion_generate

    std_conversion_generate : if (bool_conversion = 0)
    generate
      -- Workaround for XST bug
      convert : convert_func_call_ip_peakdetector_xlconvert 
        generic map (
          din_width   => din_width,
          din_bin_pt  => din_bin_pt,
          din_arith   => din_arith,
          dout_width  => dout_width,
          dout_bin_pt => dout_bin_pt,
          dout_arith  => dout_arith,
          quantization => quantization,
          overflow     => overflow)
        port map (
          din => din,
          result => result);
    end generate; --std_conversion_generate

    latency_test : if (latency > 0) generate
        reg : synth_reg
            generic map (
              width => dout_width,
              latency => latency
            )
            port map (
              i => result,
              ce => internal_ce,
              clr => clr,
              clk => clk,
              o => dout
            );
    end generate;

    latency0 : if (latency = 0)
    generate
        dout <= result;
    end generate latency0;

end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_logical_80d79f42b8 is
  port (
    d0 : in std_logic_vector((14 - 1) downto 0);
    d1 : in std_logic_vector((14 - 1) downto 0);
    y : out std_logic_vector((14 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_logical_80d79f42b8;
architecture behavior of sysgen_logical_80d79f42b8
is
  signal d0_1_24: std_logic_vector((14 - 1) downto 0);
  signal d1_1_27: std_logic_vector((14 - 1) downto 0);
  type array_type_latency_pipe_5_26 is array (0 to (1 - 1)) of std_logic_vector((14 - 1) downto 0);
  signal latency_pipe_5_26: array_type_latency_pipe_5_26 := (
    0 => "00000000000000");
  signal latency_pipe_5_26_front_din: std_logic_vector((14 - 1) downto 0);
  signal latency_pipe_5_26_back: std_logic_vector((14 - 1) downto 0);
  signal latency_pipe_5_26_push_front_pop_back_en: std_logic;
  signal fully_2_1_bit: std_logic_vector((14 - 1) downto 0);
begin
  d0_1_24 <= d0;
  d1_1_27 <= d1;
  latency_pipe_5_26_back <= latency_pipe_5_26(0);
  proc_latency_pipe_5_26: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (latency_pipe_5_26_push_front_pop_back_en = '1')) then
        latency_pipe_5_26(0) <= latency_pipe_5_26_front_din;
      end if;
    end if;
  end process proc_latency_pipe_5_26;
  fully_2_1_bit <= d0_1_24 xor d1_1_27;
  latency_pipe_5_26_front_din <= fully_2_1_bit;
  latency_pipe_5_26_push_front_pop_back_en <= '1';
  y <= latency_pipe_5_26_back;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_2a25fbe518 is
  port (
    input_port : in std_logic_vector((14 - 1) downto 0);
    output_port : out std_logic_vector((14 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_2a25fbe518;
architecture behavior of sysgen_reinterpret_2a25fbe518
is
  signal input_port_1_40: signed((14 - 1) downto 0);
  signal output_port_5_5_force: unsigned((14 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_signed(input_port);
  output_port_5_5_force <= signed_to_unsigned(input_port_1_40);
  output_port <= unsigned_to_std_logic_vector(output_port_5_5_force);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_mcode_block_a80b0cfdd5 is
  port (
    x : in std_logic_vector((16 - 1) downto 0);
    xdelay : in std_logic_vector((16 - 1) downto 0);
    xnoise : in std_logic_vector((16 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    x_m_xnoise : in std_logic_vector((16 - 1) downto 0);
    xdelay_m_xnoise : in std_logic_vector((16 - 1) downto 0);
    xmin : in std_logic_vector((16 - 1) downto 0);
    xmax : in std_logic_vector((16 - 1) downto 0);
    pkd : out std_logic_vector((1 - 1) downto 0);
    cmp_en : out std_logic_vector((1 - 1) downto 0);
    cmp_rst : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_mcode_block_a80b0cfdd5;
architecture behavior of sysgen_mcode_block_a80b0cfdd5
is
  signal x_12_43: signed((16 - 1) downto 0);
  signal xdelay_12_46: signed((16 - 1) downto 0);
  signal xnoise_12_54: signed((16 - 1) downto 0);
  signal en_12_62: boolean;
  signal x_m_xnoise_12_66: signed((16 - 1) downto 0);
  signal xdelay_m_xnoise_12_78: signed((16 - 1) downto 0);
  signal xmin_12_95: signed((16 - 1) downto 0);
  signal xmax_12_101: signed((16 - 1) downto 0);
  signal state_15_23_next: unsigned((3 - 1) downto 0);
  signal state_15_23: unsigned((3 - 1) downto 0) := "000";
  signal pkd_i_16_23_next: boolean;
  signal pkd_i_16_23: boolean := false;
  signal cmp_rst_i_17_27_next: boolean;
  signal cmp_rst_i_17_27: boolean := false;
  signal cmp_en_i_18_26_next: boolean;
  signal cmp_en_i_18_26: boolean := false;
  signal rel_24_8: boolean;
  signal state_join_24_5: unsigned((3 - 1) downto 0);
  signal rel_35_16: boolean;
  signal state_join_35_13: unsigned((3 - 1) downto 0);
  signal rel_47_16: boolean;
  signal state_join_47_13: unsigned((3 - 1) downto 0);
  signal rel_55_16: boolean;
  signal rel_55_30: boolean;
  signal bool_55_16: boolean;
  signal pkd_i_join_55_13: boolean;
  signal rel_61_16: boolean;
  signal rel_61_30: boolean;
  signal bool_61_16: boolean;
  signal rel_63_21: boolean;
  signal state_join_61_13: unsigned((3 - 1) downto 0);
  signal rel_66_16: boolean;
  signal rel_66_30: boolean;
  signal bool_66_16: boolean;
  signal pkd_i_join_66_13: boolean;
  signal state_join_28_5: unsigned((3 - 1) downto 0);
  signal cmp_en_i_join_28_5: boolean;
  signal pkd_i_join_28_5: boolean;
  signal cmp_rst_i_join_28_5: boolean;
begin
  x_12_43 <= std_logic_vector_to_signed(x);
  xdelay_12_46 <= std_logic_vector_to_signed(xdelay);
  xnoise_12_54 <= std_logic_vector_to_signed(xnoise);
  en_12_62 <= ((en) = "1");
  x_m_xnoise_12_66 <= std_logic_vector_to_signed(x_m_xnoise);
  xdelay_m_xnoise_12_78 <= std_logic_vector_to_signed(xdelay_m_xnoise);
  xmin_12_95 <= std_logic_vector_to_signed(xmin);
  xmax_12_101 <= std_logic_vector_to_signed(xmax);
  proc_state_15_23: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        state_15_23 <= state_15_23_next;
      end if;
    end if;
  end process proc_state_15_23;
  proc_pkd_i_16_23: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        pkd_i_16_23 <= pkd_i_16_23_next;
      end if;
    end if;
  end process proc_pkd_i_16_23;
  proc_cmp_rst_i_17_27: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        cmp_rst_i_17_27 <= cmp_rst_i_17_27_next;
      end if;
    end if;
  end process proc_cmp_rst_i_17_27;
  proc_cmp_en_i_18_26: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        cmp_en_i_18_26 <= cmp_en_i_18_26_next;
      end if;
    end if;
  end process proc_cmp_en_i_18_26;
  rel_24_8 <= en_12_62 = false;
  proc_if_24_5: process (rel_24_8, state_15_23)
  is
  begin
    if rel_24_8 then
      state_join_24_5 <= std_logic_vector_to_unsigned("000");
    else 
      state_join_24_5 <= state_15_23;
    end if;
  end process proc_if_24_5;
  rel_35_16 <= x_12_43 > xnoise_12_54;
  proc_if_35_13: process (rel_35_16, state_join_24_5)
  is
  begin
    if rel_35_16 then
      state_join_35_13 <= std_logic_vector_to_unsigned("010");
    else 
      state_join_35_13 <= state_join_24_5;
    end if;
  end process proc_if_35_13;
  rel_47_16 <= x_12_43 < xdelay_m_xnoise_12_78;
  proc_if_47_13: process (rel_47_16, state_join_24_5)
  is
  begin
    if rel_47_16 then
      state_join_47_13 <= std_logic_vector_to_unsigned("100");
    else 
      state_join_47_13 <= state_join_24_5;
    end if;
  end process proc_if_47_13;
  rel_55_16 <= x_12_43 >= xmin_12_95;
  rel_55_30 <= x_12_43 <= xmax_12_101;
  bool_55_16 <= rel_55_16 and rel_55_30;
  proc_if_55_13: process (bool_55_16, pkd_i_16_23)
  is
  begin
    if bool_55_16 then
      pkd_i_join_55_13 <= true;
    else 
      pkd_i_join_55_13 <= pkd_i_16_23;
    end if;
  end process proc_if_55_13;
  rel_61_16 <= x_12_43 < xnoise_12_54;
  rel_61_30 <= xdelay_12_46 < xnoise_12_54;
  bool_61_16 <= rel_61_16 and rel_61_30;
  rel_63_21 <= x_m_xnoise_12_66 > xdelay_12_46;
  proc_if_61_13: process (bool_61_16, rel_63_21, state_join_24_5)
  is
  begin
    if bool_61_16 then
      state_join_61_13 <= std_logic_vector_to_unsigned("001");
    elsif rel_63_21 then
      state_join_61_13 <= std_logic_vector_to_unsigned("010");
    else 
      state_join_61_13 <= state_join_24_5;
    end if;
  end process proc_if_61_13;
  rel_66_16 <= x_12_43 >= xmin_12_95;
  rel_66_30 <= x_12_43 <= xmax_12_101;
  bool_66_16 <= rel_66_16 and rel_66_30;
  proc_if_66_13: process (bool_66_16, pkd_i_16_23)
  is
  begin
    if bool_66_16 then
      pkd_i_join_66_13 <= true;
    else 
      pkd_i_join_66_13 <= pkd_i_16_23;
    end if;
  end process proc_if_66_13;
  proc_switch_28_5: process (cmp_en_i_18_26, cmp_rst_i_17_27, pkd_i_16_23, pkd_i_join_55_13, pkd_i_join_66_13, state_join_24_5, state_join_35_13, state_join_47_13, state_join_61_13)
  is
  begin
    case state_join_24_5 is 
      when "000" =>
        state_join_28_5 <= std_logic_vector_to_unsigned("001");
        cmp_en_i_join_28_5 <= false;
        pkd_i_join_28_5 <= false;
        cmp_rst_i_join_28_5 <= true;
      when "001" =>
        state_join_28_5 <= state_join_35_13;
        cmp_en_i_join_28_5 <= false;
        pkd_i_join_28_5 <= false;
        cmp_rst_i_join_28_5 <= true;
      when "010" =>
        state_join_28_5 <= std_logic_vector_to_unsigned("011");
        cmp_en_i_join_28_5 <= false;
        pkd_i_join_28_5 <= false;
        cmp_rst_i_join_28_5 <= true;
      when "011" =>
        state_join_28_5 <= state_join_47_13;
        cmp_en_i_join_28_5 <= true;
        pkd_i_join_28_5 <= false;
        cmp_rst_i_join_28_5 <= false;
      when "100" =>
        state_join_28_5 <= std_logic_vector_to_unsigned("101");
        cmp_en_i_join_28_5 <= false;
        pkd_i_join_28_5 <= pkd_i_join_55_13;
        cmp_rst_i_join_28_5 <= false;
      when "101" =>
        state_join_28_5 <= state_join_61_13;
        cmp_en_i_join_28_5 <= false;
        pkd_i_join_28_5 <= pkd_i_join_66_13;
        cmp_rst_i_join_28_5 <= false;
      when others =>
        state_join_28_5 <= std_logic_vector_to_unsigned("000");
        cmp_en_i_join_28_5 <= cmp_en_i_18_26;
        pkd_i_join_28_5 <= pkd_i_16_23;
        cmp_rst_i_join_28_5 <= cmp_rst_i_17_27;
    end case;
  end process proc_switch_28_5;
  state_15_23_next <= state_join_28_5;
  pkd_i_16_23_next <= pkd_i_join_28_5;
  cmp_rst_i_17_27_next <= cmp_rst_i_join_28_5;
  cmp_en_i_18_26_next <= cmp_en_i_join_28_5;
  pkd <= boolean_to_vector(pkd_i_16_23);
  cmp_en <= boolean_to_vector(cmp_en_i_18_26);
  cmp_rst <= boolean_to_vector(cmp_rst_i_17_27);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_mcode_block_0c3d4c1851 is
  port (
    x : in std_logic_vector((16 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    xmax : out std_logic_vector((16 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_mcode_block_0c3d4c1851;
architecture behavior of sysgen_mcode_block_0c3d4c1851
is
  signal x_11_34: signed((16 - 1) downto 0);
  signal rst_11_37: boolean;
  signal en_11_42: boolean;
  signal xmax_i_12_24_next: signed((16 - 1) downto 0);
  signal xmax_i_12_24: signed((16 - 1) downto 0) := "0000000000000000";
  signal xmax_i_12_24_rst: std_logic;
  signal xmax_i_12_24_en: std_logic;
  signal rel_17_8: boolean;
  signal rel_19_13: boolean;
  signal rel_19_28: boolean;
  signal bool_19_13: boolean;
  signal xmax_i_join_17_5: signed((16 - 1) downto 0);
  signal xmax_i_join_17_5_en: std_logic;
  signal xmax_i_join_17_5_rst: std_logic;
begin
  x_11_34 <= std_logic_vector_to_signed(x);
  rst_11_37 <= ((rst) = "1");
  en_11_42 <= ((en) = "1");
  proc_xmax_i_12_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (xmax_i_12_24_rst = '1')) then
        xmax_i_12_24 <= "0000000000000000";
      elsif ((ce = '1') and (xmax_i_12_24_en = '1')) then 
        xmax_i_12_24 <= xmax_i_12_24_next;
      end if;
    end if;
  end process proc_xmax_i_12_24;
  rel_17_8 <= rst_11_37 = true;
  rel_19_13 <= en_11_42 = true;
  rel_19_28 <= xmax_i_12_24 < x_11_34;
  bool_19_13 <= rel_19_13 and rel_19_28;
  proc_if_17_5: process (bool_19_13, rel_17_8, x_11_34)
  is
  begin
    if rel_17_8 then
      xmax_i_join_17_5_rst <= '1';
    elsif bool_19_13 then
      xmax_i_join_17_5_rst <= '0';
    else 
      xmax_i_join_17_5_rst <= '0';
    end if;
    if bool_19_13 then
      xmax_i_join_17_5_en <= '1';
    else 
      xmax_i_join_17_5_en <= '0';
    end if;
    xmax_i_join_17_5 <= x_11_34;
  end process proc_if_17_5;
  xmax_i_12_24_next <= x_11_34;
  xmax_i_12_24_rst <= xmax_i_join_17_5_rst;
  xmax_i_12_24_en <= xmax_i_join_17_5_en;
  xmax <= signed_to_std_logic_vector(xmax_i_12_24);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_3018b0b739 is
  port (
    op : out std_logic_vector((10 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_3018b0b739;
architecture behavior of sysgen_constant_3018b0b739
is
begin
  op <= "0000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_389a487358 is
  port (
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_389a487358;
architecture behavior of sysgen_constant_389a487358
is
begin
  op <= "1";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_8089087059 is
  port (
    input_port : in std_logic_vector((10 - 1) downto 0);
    output_port : out std_logic_vector((10 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_8089087059;
architecture behavior of sysgen_reinterpret_8089087059
is
  signal input_port_1_40: unsigned((10 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port <= unsigned_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_relational_3828661dd3 is
  port (
    a : in std_logic_vector((10 - 1) downto 0);
    b : in std_logic_vector((10 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_relational_3828661dd3;
architecture behavior of sysgen_relational_3828661dd3
is
  signal a_1_31: unsigned((10 - 1) downto 0);
  signal b_1_34: unsigned((10 - 1) downto 0);
  type array_type_op_mem_37_22 is array (0 to (1 - 1)) of boolean;
  signal op_mem_37_22: array_type_op_mem_37_22 := (
    0 => false);
  signal op_mem_37_22_front_din: boolean;
  signal op_mem_37_22_back: boolean;
  signal op_mem_37_22_push_front_pop_back_en: std_logic;
  signal result_12_3_rel: boolean;
begin
  a_1_31 <= std_logic_vector_to_unsigned(a);
  b_1_34 <= std_logic_vector_to_unsigned(b);
  op_mem_37_22_back <= op_mem_37_22(0);
  proc_op_mem_37_22: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_37_22_push_front_pop_back_en = '1')) then
        op_mem_37_22(0) <= op_mem_37_22_front_din;
      end if;
    end if;
  end process proc_op_mem_37_22;
  result_12_3_rel <= a_1_31 = b_1_34;
  op_mem_37_22_front_din <= result_12_3_rel;
  op_mem_37_22_push_front_pop_back_en <= '1';
  op <= boolean_to_vector(op_mem_37_22_back);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
--
--  Filename      : xlslice.vhd
--
--  Description   : VHDL description of a block that sets the output to a
--                  specified range of the input bits. The output is always
--                  set to an unsigned type with it's binary point at zero.
--
---------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity ip_peakdetector_xlslice is
    generic (
        new_msb      : integer := 9;           -- position of new msb
        new_lsb      : integer := 1;           -- position of new lsb
        x_width      : integer := 16;          -- Width of x input
        y_width      : integer := 8);          -- Width of y output
    port (
        x : in std_logic_vector (x_width-1 downto 0);
        y : out std_logic_vector (y_width-1 downto 0));
end ip_peakdetector_xlslice;

architecture behavior of ip_peakdetector_xlslice is
begin
    y <= x(new_msb downto new_lsb);
end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_mcode_block_0c4ff59dc3 is
  port (
    x : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((1 - 1) downto 0);
    re : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_mcode_block_0c4ff59dc3;
architecture behavior of sysgen_mcode_block_0c4ff59dc3
is
  signal x_1_38: unsigned((1 - 1) downto 0);
  signal ff_2_17_next: unsigned((1 - 1) downto 0);
  signal ff_2_17: unsigned((1 - 1) downto 0) := "0";
  signal rel_5_5: boolean;
  signal rel_5_15: boolean;
  signal bool_5_5: boolean;
  signal re_join_5_2: boolean;
begin
  x_1_38 <= std_logic_vector_to_unsigned(x);
  proc_ff_2_17: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        ff_2_17 <= ff_2_17_next;
      end if;
    end if;
  end process proc_ff_2_17;
  rel_5_5 <= ff_2_17 = std_logic_vector_to_unsigned("0");
  rel_5_15 <= x_1_38 = std_logic_vector_to_unsigned("1");
  bool_5_5 <= rel_5_5 and rel_5_15;
  proc_if_5_2: process (bool_5_5)
  is
  begin
    if bool_5_5 then
      re_join_5_2 <= true;
    else 
      re_join_5_2 <= false;
    end if;
  end process proc_if_5_2;
  ff_2_17_next <= x_1_38;
  y <= unsigned_to_std_logic_vector(ff_2_17);
  re <= boolean_to_vector(re_join_5_2);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_df705fa772 is
  port (
    input_port : in std_logic_vector((1 - 1) downto 0);
    output_port : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_df705fa772;
architecture behavior of sysgen_reinterpret_df705fa772
is
  signal input_port_1_40: unsigned((1 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port <= unsigned_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_770a5d44ff is
  port (
    input_port : in std_logic_vector((16 - 1) downto 0);
    output_port : out std_logic_vector((16 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_770a5d44ff;
architecture behavior of sysgen_reinterpret_770a5d44ff
is
  signal input_port_1_40: unsigned((16 - 1) downto 0);
  signal output_port_5_5_force: signed((16 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port_5_5_force <= unsigned_to_signed(input_port_1_40);
  output_port <= signed_to_std_logic_vector(output_port_5_5_force);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity axibusdomain_axi_lite_interface is 
    port(
        r8_en_pkd : out std_logic_vector(31 downto 0);
        r7_x_max : out std_logic_vector(31 downto 0);
        r6_x_min : out std_logic_vector(31 downto 0);
        r5_x_noise : out std_logic_vector(31 downto 0);
        r4_x_delay : out std_logic_vector(31 downto 0);
        axibusdomain_clk : out std_logic;
        axibusdomain_aclk : in std_logic;
        axibusdomain_aresetn : in std_logic;
        axibusdomain_s_axi_awaddr : in std_logic_vector(5-1 downto 0);
        axibusdomain_s_axi_awvalid : in std_logic;
        axibusdomain_s_axi_awready : out std_logic;
        axibusdomain_s_axi_wdata : in std_logic_vector(32-1 downto 0);
        axibusdomain_s_axi_wstrb : in std_logic_vector(32/8-1 downto 0);
        axibusdomain_s_axi_wvalid : in std_logic;
        axibusdomain_s_axi_wready : out std_logic;
        axibusdomain_s_axi_bresp : out std_logic_vector(1 downto 0);
        axibusdomain_s_axi_bvalid : out std_logic;
        axibusdomain_s_axi_bready : in std_logic;
        axibusdomain_s_axi_araddr : in std_logic_vector(5-1 downto 0);
        axibusdomain_s_axi_arvalid : in std_logic;
        axibusdomain_s_axi_arready : out std_logic;
        axibusdomain_s_axi_rdata : out std_logic_vector(32-1 downto 0);
        axibusdomain_s_axi_rresp : out std_logic_vector(1 downto 0);
        axibusdomain_s_axi_rvalid : out std_logic;
        axibusdomain_s_axi_rready : in std_logic
    );
end axibusdomain_axi_lite_interface;
architecture structural of axibusdomain_axi_lite_interface is 
component axibusdomain_axi_lite_interface_verilog is
    port(
        r8_en_pkd : out std_logic_vector(31 downto 0);
        r7_x_max : out std_logic_vector(31 downto 0);
        r6_x_min : out std_logic_vector(31 downto 0);
        r5_x_noise : out std_logic_vector(31 downto 0);
        r4_x_delay : out std_logic_vector(31 downto 0);
        axibusdomain_clk : out std_logic;
        axibusdomain_aclk : in std_logic;
        axibusdomain_aresetn : in std_logic;
        axibusdomain_s_axi_awaddr : in std_logic_vector(5-1 downto 0);
        axibusdomain_s_axi_awvalid : in std_logic;
        axibusdomain_s_axi_awready : out std_logic;
        axibusdomain_s_axi_wdata : in std_logic_vector(32-1 downto 0);
        axibusdomain_s_axi_wstrb : in std_logic_vector(32/8-1 downto 0);
        axibusdomain_s_axi_wvalid : in std_logic;
        axibusdomain_s_axi_wready : out std_logic;
        axibusdomain_s_axi_bresp : out std_logic_vector(1 downto 0);
        axibusdomain_s_axi_bvalid : out std_logic;
        axibusdomain_s_axi_bready : in std_logic;
        axibusdomain_s_axi_araddr : in std_logic_vector(5-1 downto 0);
        axibusdomain_s_axi_arvalid : in std_logic;
        axibusdomain_s_axi_arready : out std_logic;
        axibusdomain_s_axi_rdata : out std_logic_vector(32-1 downto 0);
        axibusdomain_s_axi_rresp : out std_logic_vector(1 downto 0);
        axibusdomain_s_axi_rvalid : out std_logic;
        axibusdomain_s_axi_rready : in std_logic
    );
end component;
begin
inst : axibusdomain_axi_lite_interface_verilog
    port map(
    r8_en_pkd => r8_en_pkd,
    r7_x_max => r7_x_max,
    r6_x_min => r6_x_min,
    r5_x_noise => r5_x_noise,
    r4_x_delay => r4_x_delay,
    axibusdomain_clk => axibusdomain_clk,
    axibusdomain_aclk => axibusdomain_aclk,
    axibusdomain_aresetn => axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr => axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wdata => axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb => axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp => axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr => axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata => axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp => axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
    axibusdomain_s_axi_rready => axibusdomain_s_axi_rready
);
end structural;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

-------------------------------------------------------------------
 -- System Generator VHDL source file.
 --
 -- Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
 -- text/file contains proprietary, confidential information of Xilinx,
 -- Inc., is distributed under license from Xilinx, Inc., and may be used,
 -- copied and/or disclosed only pursuant to the terms of a valid license
 -- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
 -- this text/file solely for design, simulation, implementation and
 -- creation of design files limited to Xilinx devices or technologies.
 -- Use with non-Xilinx devices or technologies is expressly prohibited
 -- and immediately terminates your license unless covered by a separate
 -- agreement.
 --
 -- Xilinx is providing this design, code, or information "as is" solely
 -- for use in developing programs and solutions for Xilinx devices.  By
 -- providing this design, code, or information as one possible
 -- implementation of this feature, application or standard, Xilinx is
 -- making no representation that this implementation is free from any
 -- claims of infringement.  You are responsible for obtaining any rights
 -- you may require for your implementation.  Xilinx expressly disclaims
 -- any warranty whatsoever with respect to the adequacy of the
 -- implementation, including but not limited to warranties of
 -- merchantability or fitness for a particular purpose.
 --
 -- Xilinx products are not intended for use in life support appliances,
 -- devices, or systems.  Use in such applications is expressly prohibited.
 --
 -- Any modifications that are made to the source code are done at the user's
 -- sole risk and will be unsupported.
 --
 -- This copyright and support notice must be retained as part of this
 -- text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
 -- reserved.
 -------------------------------------------------------------------
 library IEEE;
 use IEEE.std_logic_1164.all;
 use IEEE.std_logic_arith.all;

entity ip_peakdetector_xladdsub is 
   generic (
     core_name0: string := "";
     a_width: integer := 16;
     a_bin_pt: integer := 4;
     a_arith: integer := xlUnsigned;
     c_in_width: integer := 16;
     c_in_bin_pt: integer := 4;
     c_in_arith: integer := xlUnsigned;
     c_out_width: integer := 16;
     c_out_bin_pt: integer := 4;
     c_out_arith: integer := xlUnsigned;
     b_width: integer := 8;
     b_bin_pt: integer := 2;
     b_arith: integer := xlUnsigned;
     s_width: integer := 17;
     s_bin_pt: integer := 4;
     s_arith: integer := xlUnsigned;
     rst_width: integer := 1;
     rst_bin_pt: integer := 0;
     rst_arith: integer := xlUnsigned;
     en_width: integer := 1;
     en_bin_pt: integer := 0;
     en_arith: integer := xlUnsigned;
     full_s_width: integer := 17;
     full_s_arith: integer := xlUnsigned;
     mode: integer := xlAddMode;
     extra_registers: integer := 0;
     latency: integer := 0;
     quantization: integer := xlTruncate;
     overflow: integer := xlWrap;
     c_latency: integer := 0;
     c_output_width: integer := 17;
     c_has_c_in : integer := 0;
     c_has_c_out : integer := 0
   );
   port (
     a: in std_logic_vector(a_width - 1 downto 0);
     b: in std_logic_vector(b_width - 1 downto 0);
     c_in : in std_logic_vector (0 downto 0) := "0";
     ce: in std_logic;
     clr: in std_logic := '0';
     clk: in std_logic;
     rst: in std_logic_vector(rst_width - 1 downto 0) := "0";
     en: in std_logic_vector(en_width - 1 downto 0) := "1";
     c_out : out std_logic_vector (0 downto 0);
     s: out std_logic_vector(s_width - 1 downto 0)
   );
 end ip_peakdetector_xladdsub;
 
 architecture behavior of ip_peakdetector_xladdsub is 
 component synth_reg
 generic (
 width: integer := 16;
 latency: integer := 5
 );
 port (
 i: in std_logic_vector(width - 1 downto 0);
 ce: in std_logic;
 clr: in std_logic;
 clk: in std_logic;
 o: out std_logic_vector(width - 1 downto 0)
 );
 end component;
 
 function format_input(inp: std_logic_vector; old_width, delta, new_arith,
 new_width: integer)
 return std_logic_vector
 is
 variable vec: std_logic_vector(old_width-1 downto 0);
 variable padded_inp: std_logic_vector((old_width + delta)-1 downto 0);
 variable result: std_logic_vector(new_width-1 downto 0);
 begin
 vec := inp;
 if (delta > 0) then
 padded_inp := pad_LSB(vec, old_width+delta);
 result := extend_MSB(padded_inp, new_width, new_arith);
 else
 result := extend_MSB(vec, new_width, new_arith);
 end if;
 return result;
 end;
 
 constant full_s_bin_pt: integer := fractional_bits(a_bin_pt, b_bin_pt);
 constant full_a_width: integer := full_s_width;
 constant full_b_width: integer := full_s_width;
 
 signal full_a: std_logic_vector(full_a_width - 1 downto 0);
 signal full_b: std_logic_vector(full_b_width - 1 downto 0);
 signal core_s: std_logic_vector(full_s_width - 1 downto 0);
 signal conv_s: std_logic_vector(s_width - 1 downto 0);
 signal temp_cout : std_logic;
 signal internal_clr: std_logic;
 signal internal_ce: std_logic;
 signal extra_reg_ce: std_logic;
 signal override: std_logic;
 signal logic1: std_logic_vector(0 downto 0);


 component ip_peakdetector_c_addsub_v12_0_i0
    port ( 
    a: in std_logic_vector(17 - 1 downto 0);
    clk: in std_logic:= '0';
    ce: in std_logic:= '0';
    s: out std_logic_vector(c_output_width - 1 downto 0);
    b: in std_logic_vector(17 - 1 downto 0) 
 		  ); 
 end component;

begin
 internal_clr <= (clr or (rst(0))) and ce;
 internal_ce <= ce and en(0);
 logic1(0) <= '1';
 addsub_process: process (a, b, core_s)
 begin
 full_a <= format_input (a, a_width, b_bin_pt - a_bin_pt, a_arith,
 full_a_width);
 full_b <= format_input (b, b_width, a_bin_pt - b_bin_pt, b_arith,
 full_b_width);
 conv_s <= convert_type (core_s, full_s_width, full_s_bin_pt, full_s_arith,
 s_width, s_bin_pt, s_arith, quantization, overflow);
 end process addsub_process;


 comp0: if ((core_name0 = "ip_peakdetector_c_addsub_v12_0_i0")) generate 
  core_instance0:ip_peakdetector_c_addsub_v12_0_i0
   port map ( 
         a => full_a,
         clk => clk,
         ce => internal_ce,
         s => core_s,
         b => full_b
  ); 
   end generate;

latency_test: if (extra_registers > 0) generate
 override_test: if (c_latency > 1) generate
 override_pipe: synth_reg
 generic map (
 width => 1,
 latency => c_latency
 )
 port map (
 i => logic1,
 ce => internal_ce,
 clr => internal_clr,
 clk => clk,
 o(0) => override);
 extra_reg_ce <= ce and en(0) and override;
 end generate override_test;
 no_override: if ((c_latency = 0) or (c_latency = 1)) generate
 extra_reg_ce <= ce and en(0);
 end generate no_override;
 extra_reg: synth_reg
 generic map (
 width => s_width,
 latency => extra_registers
 )
 port map (
 i => conv_s,
 ce => extra_reg_ce,
 clr => internal_clr,
 clk => clk,
 o => s
 );
 cout_test: if (c_has_c_out = 1) generate
 c_out_extra_reg: synth_reg
 generic map (
 width => 1,
 latency => extra_registers
 )
 port map (
 i(0) => temp_cout,
 ce => extra_reg_ce,
 clr => internal_clr,
 clk => clk,
 o => c_out
 );
 end generate cout_test;
 end generate;
 
 latency_s: if ((latency = 0) or (extra_registers = 0)) generate
 s <= conv_s;
 end generate latency_s;
 latency0: if (((latency = 0) or (extra_registers = 0)) and
 (c_has_c_out = 1)) generate
 c_out(0) <= temp_cout;
 end generate latency0;
 tie_dangling_cout: if (c_has_c_out = 0) generate
 c_out <= "0";
 end generate tie_dangling_cout;
 end architecture behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
 --
 --  Filename      : xlcounter_rst.vhd
 --
 --  Created       : 1/31/01
 --  Modified      :
 --
 --  Description   : VHDL wrapper for a counter. This wrapper
 --                  uses the Binary Counter CoreGenerator core.
 --
 ---------------------------------------------------------------------
 
 
 ---------------------------------------------------------------------
 --
 --  Entity        : xlcounter
 --
 --  Architecture  : behavior
 --
 --  Description   : Top level VHDL description of a counter.
 --
 ---------------------------------------------------------------------
 
 library IEEE;
 use IEEE.std_logic_1164.all;

entity ip_peakdetector_xlcounter_free is 
   generic (
     core_name0: string := "";
     op_width: integer := 5;
     op_arith: integer := xlSigned
   );
   port (
     ce: in std_logic;
     clr: in std_logic;
     clk: in std_logic;
     op: out std_logic_vector(op_width - 1 downto 0);
     up: in std_logic_vector(0 downto 0) := (others => '0');
     load: in std_logic_vector(0 downto 0) := (others => '0');
     din: in std_logic_vector(op_width - 1 downto 0) := (others => '0');
     en: in std_logic_vector(0 downto 0);
     rst: in std_logic_vector(0 downto 0)
   );
 end ip_peakdetector_xlcounter_free;
 
 architecture behavior of ip_peakdetector_xlcounter_free is


 component ip_peakdetector_c_counter_binary_v12_0_i0
    port ( 
      clk: in std_logic;
      ce: in std_logic;
      SINIT: in std_logic;
      load: in std_logic;
      l: in std_logic_vector(op_width - 1 downto 0);
      q: out std_logic_vector(op_width - 1 downto 0) 
 		  ); 
 end component;

-- synthesis translate_off
   constant zeroVec: std_logic_vector(op_width - 1 downto 0) := (others => '0');
   constant oneVec: std_logic_vector(op_width - 1 downto 0) := (others => '1');
   constant zeroStr: string(1 to op_width) :=
     std_logic_vector_to_bin_string(zeroVec);
   constant oneStr: string(1 to op_width) :=
     std_logic_vector_to_bin_string(oneVec);
 -- synthesis translate_on
 
   signal core_sinit: std_logic;
   signal core_ce: std_logic;
   signal op_net: std_logic_vector(op_width - 1 downto 0);
 begin
   core_ce <= ce and en(0);
   core_sinit <= (clr or rst(0)) and ce;
   op <= op_net;


 comp0: if ((core_name0 = "ip_peakdetector_c_counter_binary_v12_0_i0")) generate 
  core_instance0:ip_peakdetector_c_counter_binary_v12_0_i0
   port map ( 
        clk => clk,
        ce => core_ce,
        SINIT => core_sinit,
        load => load(0),
        l => din,
        q => op_net
  ); 
   end generate;

end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
 use IEEE.std_logic_1164.all;

entity ip_peakdetector_xlspram is 
   generic (
     core_name0: string := "";
     c_width: integer := 12;
     c_address_width: integer := 4;
     latency: integer := 1
     );
   port (
     data_in: in std_logic_vector(c_width - 1 downto 0);
     addr: in std_logic_vector(c_address_width - 1 downto 0);
     we: in std_logic_vector(0 downto 0);
     en: in std_logic_vector(0 downto 0);
     rst: in std_logic_vector(0 downto 0);
     ce: in std_logic;
     clk: in std_logic;
     data_out: out std_logic_vector(c_width - 1 downto 0)
   );
 end ip_peakdetector_xlspram;
 
 architecture behavior of ip_peakdetector_xlspram is
 component synth_reg
 generic (
 width: integer;
 latency: integer
 );
 port (
 i: in std_logic_vector(width - 1 downto 0);
 ce: in std_logic;
 clr: in std_logic;
 clk: in std_logic;
 o: out std_logic_vector(width - 1 downto 0)
 );
 end component;
 signal core_data_out, dly_data_out: std_logic_vector(c_width - 1 downto 0);
 signal core_we, core_ce, sinit: std_logic;


 component ip_peakdetector_blk_mem_gen_i0
    port ( 
      addra: in std_logic_vector(c_address_width - 1 downto 0);
      clka: in std_logic;
      dina: in std_logic_vector(c_width - 1 downto 0);
      wea: in std_logic_vector(0 downto 0);
      ena: in std_logic;
      douta: out std_logic_vector(c_width - 1 downto 0) 
 		  ); 
 end component;

begin
 data_out <= dly_data_out;
 core_we <= we(0);
 core_ce <= ce and en(0);
 sinit <= rst(0) and ce;


 comp0: if ((core_name0 = "ip_peakdetector_blk_mem_gen_i0")) generate 
  core_instance0:ip_peakdetector_blk_mem_gen_i0
   port map ( 
        addra => addr,
        clka => clk,
        dina => data_in,
        wea(0) => core_we,
        ena => core_ce,
        douta => core_data_out
  ); 
   end generate;

latency_test: if (latency > 1) generate
 reg: synth_reg
 generic map (
 width => c_width,
 latency => latency - 1
 )
 port map (
 i => core_data_out,
 ce => core_ce,
 clr => '0',
 clk => clk,
 o => dly_data_out
 );
 end generate;
 latency_1: if (latency <= 1) generate
 dly_data_out <= core_data_out;
 end generate;
 end behavior;

