#include "xparameters.h"
#include "ip_scope.h"

/*
* The configuration table for devices
*/

ip_scope_Config ip_scope_ConfigTable[XPAR_IP_SCOPE_NUM_INSTANCES] =
{
	{
		XPAR_IP_SCOPE_0_DEVICE_ID,
		XPAR_IP_SCOPE_0_AXIBUSDOMAIN_S_AXI_BASEADDR
	}
};


