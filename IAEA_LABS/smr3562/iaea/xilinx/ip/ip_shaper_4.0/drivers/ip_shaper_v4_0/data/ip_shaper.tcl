proc generate {drv_handle} {
    xdefine_include_file $drv_handle "xparameters.h" "ip_shaper" "NUM_INSTANCES" "DEVICE_ID" "C_AXI_CLK_DOMAIN_S_AXI_BASEADDR" "C_AXI_CLK_DOMAIN_S_AXI_HIGHADDR" 
    xdefine_config_file $drv_handle "ip_shaper_g.c" "ip_shaper" "DEVICE_ID" "C_AXI_CLK_DOMAIN_S_AXI_BASEADDR" 
    xdefine_canonical_xpars $drv_handle "xparameters.h" "ip_shaper" "DEVICE_ID" "C_AXI_CLK_DOMAIN_S_AXI_BASEADDR" "C_AXI_CLK_DOMAIN_S_AXI_HIGHADDR" 

}