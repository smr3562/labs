/******************************************************************************
*
* Copyright (C) 2020 - 2021 IAEA.  All rights reserved.
*
******************************************************************************/


//xilinx declarations
#include "xil_types.h"
#include "xparameters.h"		//Contains the address and configurations of need for the system
#include "xstatus.h"			//Contains the status definitions used
#include "platform.h"			//Declarations for the MicroBlaze
#include <string.h>

//custom declarations
#include "define.h"

#include "uart.h"
#include "ip_scope.h"
#include "invert_and_offset.h"
#include "ip_mux16_2_if.h"
#include "ip_shaper.h"
#include "ip_blr.h"
#include "ip_dc_stabilizer.h"
#include "ip_peakdetector.h"
#include "ip_timers.h"
#include "xbram.h"
#include "intc.h"
#include "registers.h"
#include "util.h"

#include "main.h"

int main()
   {
	InitSoPC();

	while(1)
	{
		DoHostTasks();
	}
	cleanup_platform();
	return 0;
   }

void DoHostTasks()
   {
   int cmdID;
   unsigned int uiReceivedCount;

   if(UartLiteReceiveCommand(XPAR_UARTLITE_0_DEVICE_ID, u8Command, &uiReceivedCount, MEMORY_RX_BUFFER_SIZE) == XST_SUCCESS)
      {
      cmdID = GetCommandId(u8Command, uiReceivedCount) + 1;
      switch (cmdID)
         {
         //"$SP": Set Filter parameters
         case 3:
            DoCommand003(u8Command, uiReceivedCount);
			break;
         //"$LS" Load Scope ( PS Scope -> Host; PS Scope is flushed automatically in interrupt mode)
         case 4:
            DoCommand004(u8Command, uiReceivedCount);
			break;
		 //"$RM": Read spectrum ( PS DRAM - host)
		 case 6:
			DoCommand006(u8Command, uiReceivedCount);
			break;
		 //"$AQ": AcQuisition start/stop
		 case 7:
			DoCommand007(u8Command, uiReceivedCount);
			break;
		 //"$WT": Write Timer register
		 case 8:
			DoCommand008(u8Command, uiReceivedCount);
			break;
		 //"$RT": Read Timer register
		 case 9:
			DoCommand009(u8Command, uiReceivedCount);
			break;
		 //"$CS": Clear Spectrum
		 case 10:
			DoCommand010(u8Command, uiReceivedCount);
			break;
		 //"$GP": Get parameters
		 case 11:
			DoCommand011(u8Command, uiReceivedCount);
			break;
		 //"$GR": Get ROI
		 case 12:
			DoCommand012(u8Command, uiReceivedCount);
			break;
		 //"$SR": Set ROI
		 case 13:
			DoCommand013(u8Command, uiReceivedCount);
			break;
         //"$VR": Read ROI values
         case 14:
            DoCommand014(u8Command, uiReceivedCount);
            break;
         case 15://$RL: get RealTimer/LiveTimer
        	DoCommand015(u8Command, uiReceivedCount);
        	break;
         default:
			SendError(ERROR_INVALID_COMMAND);
			break;
		 }
      }
   }

void InitSoPC()
   {
   ip_shaper_Config *ip_shaper_ConfigPtrSlow;
   XBram_Config *XBram_ConfigPtr;

   init_platform();

   //**************************************************
   //	initialize UART controller
   //**************************************************
   UartLiteInit(XPAR_UARTLITE_0_DEVICE_ID);

   //**************************************************
   //	initialize interrupt controller
   //**************************************************
   IntcPSInit((u16)XPAR_INTC_0_DEVICE_ID);
   IntcEnableInterrupt(XPAR_INTC_0_DEVICE_ID);

   //**************************************************
   //	initialize IP Scope
   //**************************************************
   ip_scope_Initialize(&IpScope, XPAR_IP_SCOPE_0_DEVICE_ID);
   scope_SetDefaultUserParameters(IpScope.Config.prm);
   ip_scope_WriteLogic(&IpScope);

   //**************************************************
   //	initialize IP Shaper
   //**************************************************
   ip_shaper_ConfigPtrSlow = ip_shaper_LookupConfig(XPAR_DPP_0_PULSE_CONDITIONING_SLOW_IP_SHAPER_0_DEVICE_ID);
   ip_shaper_CfgInitialize(&IpShaperSlow, ip_shaper_ConfigPtrSlow);
   shaper_SetDefaultUserParametersSlow(IpShaperSlow.Config.prm);
   ip_shaper_WriteLogic(&IpShaperSlow);

   //**************************************************
   //	initialize IP Peak Detector Slow
   //**************************************************
   ip_peakdetector_Initialize(&IpPeakDetectorSlow, XPAR_DPP_0_PHA_PKD_IP_PEAKDETECTOR_0_DEVICE_ID);
   peakdetector_SetDefaultUserParametersSlow(IpPeakDetectorSlow.Config.prm);
   ip_peakdetector_WriteLogic(&IpPeakDetectorSlow);

   //**************************************************
   //	initialize IP BRAM Controller
   //**************************************************
   XBram_ConfigPtr = XBram_LookupConfig(XPAR_BRAM_0_DEVICE_ID);
   XBram_CfgInitialize(&Bram, XBram_ConfigPtr, XBram_ConfigPtr->CtrlBaseAddress);


   //**************************************************
   //	initialize timers
   //**************************************************
   //preset
   Xil_Out32(TIMER_REGISTER4, (u32)1000000); //preset = 1000000 msec
   //C=enabled,RT;B=enabled,LT;A=disabled
   Xil_Out32(TIMER_REGISTER5, TIMERC_ENABLED | TIMERB_ENABLED | TIMERB_COUNTS_LIVE);




}

int GetCommandId(u8 *u8Command, unsigned int uiCommandSize)
   {
   int i;
   char szCmdId[4];

   for(i=0;i<3;i++) szCmdId[i] = u8Command[i];
   szCmdId[3]='\0';

   for(i=0;i<CMD_CNT;i++)
	  {
	  if(strcmp(szCmdId, m_szCmdId[i]) == 0)return (i);
	  }
   return -1;
   }


//
// CMD003: $SP = Set parameters
// command parameters: param_group_id, p1, p2, ... pn
//
void DoCommand003(u8 *u8Command, unsigned int uiCommandSize)
   {
   int iParam;
   char szParameter[32];
   u32 u32Data;
   if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;
   iParam = atoi32(szParameter);
   switch(iParam)
      {

	  case 1://IP_Shaper parameter group
	  {
		 u32 *prm = IpShaperSlow.Config.prm;
		 int j=2;
		 for(int i=0;i<IP_SHAPER_PRM_CNT;i++,j++)
			{
			if( GetCommandParameter(u8Command,uiCommandSize,j,szParameter) == 0) goto failed;
			prm[i] = atou32(szParameter);
			}
		 ip_shaper_WriteLogic(&IpShaperSlow);
		 //dependent parameters: IP PeakDetector: x_delay
		 break;
		 }
	  //IP Peak Detector parameter group
	  case 2:
	     {
		 u32 *prm = IpPeakDetectorSlow.Config.prm;
		 int j=2;
		 for(int i=0;i<IP_PEAKDETECTOR_PRM_CNT;i++,j++)
			{
			if( GetCommandParameter(u8Command,uiCommandSize,j,szParameter) == 0) goto failed;
			prm[i] = atou32(szParameter);
			}
		 ip_peakdetector_WriteLogic(&IpPeakDetectorSlow);
		 break;
         }
	  case 3://IP Scope parameter group
		 {
		 u32 *prm = IpScope.Config.prm;
		 int j=2;
		 for(int i=0;i<IP_SCOPE_PRM_CNT;i++,j++)
			{
			if( GetCommandParameter(u8Command,uiCommandSize,j,szParameter) == 0) goto failed;
			prm[i] = atou32(szParameter);
			}
		 ip_scope_WriteLogic(&IpScope);
		 break;
         }
		  //IP Timers parameter group
      case 4:
	     //1st parameter goes to REG4, it is preset
		 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(TIMER_REGISTER4, u32Data);
		 //2nd parameter goes to REG5, settings
		 if( GetCommandParameter(u8Command,uiCommandSize,3,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter) + (Xil_In32(TIMER_REGISTER5) &0x00000003) ;
		 Xil_Out32(TIMER_REGISTER5, u32Data);
		 break;

      case 5://BLR SLOW parameters
		 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(SLOWBLR_REG0, u32Data);
		 if( GetCommandParameter(u8Command,uiCommandSize,3,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(SLOWBLR_REG1, u32Data);
		 if( GetCommandParameter(u8Command,uiCommandSize,4,szParameter) == 0) goto failed;
		 Xil_Out32(SLOWBLR_REG2, 0);
		 u32Data = atou32(szParameter);
		 Xil_Out32(SLOWBLR_REG2, u32Data);
		 break;
	  case 6://scopemux
		 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(SCOPEMUX_REG0, u32Data);
		 break;
	  case 7://DCS SLOW parameters
		 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(SLOWDCS_REG0, u32Data);
		 if( GetCommandParameter(u8Command,uiCommandSize,3,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(SLOWDCS_REG1, u32Data);
		 if( GetCommandParameter(u8Command,uiCommandSize,4,szParameter) == 0) goto failed;
		 Xil_Out32(SLOWDCS_REG2, 0);
		 u32Data = atou32(szParameter);
		 Xil_Out32(SLOWDCS_REG2, u32Data);
		 break;
	  case 8://invert & offset
		 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(INVERT_OFFSET_REG0, u32Data);
		 break;
      default:
		  goto failed;
		  break;
      }
   u8Replay[0] = '!';
   u8Replay[1] = 'S';
   u8Replay[2] = 'P';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';
   UartLiteSendReply(XPAR_MB_PERIPH_0_AXI_UARTLITE_0_DEVICE_ID, u8Replay,5);

   return;
failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   }

//
// CMD004: $LS = Load data from scope (same as Read but using interrupt)
// command parameter: channel ID (integer value 1 or 2)
//
void DoCommand004(u8 *u8Command, unsigned int uiCommandSize)
   {
   if(IntcIsWaveformReady()==0)
	 {
	 //SendError(ERROR_SCOPE_DATA_NOT_READY);
	 //return;
	 }

   //header
   u8Replay[0] = '!';
   u8Replay[1] = 'L';
   u8Replay[2] = (u8) '\n';
   u8Replay[3] = (u8) '\r';

   //data
   u32 *data = (u32 *) (u8Replay + 4);
   ip_scope_ReadWaveform(&IpScope,data);

   UartLiteSendReply(XPAR_MB_PERIPH_0_AXI_UARTLITE_0_DEVICE_ID, u8Replay, MEMORY_TX_BUFFER_SIZE);

   //clear scope and enable interrupt
   ip_scope_WaveformAccepted(&IpScope);
   IntcEnableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);
   }

//
// CMD006: $RM = Get data from spectrum
//
void DoCommand006(u8 *u8Command, unsigned int uiCommandSize)
   {
   char szParameter[32];
   int iParam;
   u32 addr;
   int i;
   //data
   u32 *data = (u32 *) (u8Replay + 4);
   //header
   u8Replay[0] = '!';
   u8Replay[1] = 'R';
   u8Replay[2] = '\n';
   u8Replay[3] = '\r';

   if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;

   iParam = atoi32(szParameter);
   switch(iParam)
      {
   	  case 0://read low portion
   		 for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
   			 {
   			 addr = XPAR_BRAM_0_BASEADDR+4*i;
   			 data[i]= Xil_In32(addr);
   			 }
   		 break;
   	  case 1: //read high portion
   		 for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
   		   	{
   		   	addr = XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE+4*i;
   		   	data[i]= Xil_In32(addr);
   		   	}
   	     break;
   	  case 2: //read active portion
   		 if (  Xil_In32(TIMER_REGISTER3) & BRAM_SEGMENT )
   		    {
   			for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
   			   {
   			   addr = XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE+4*i;
   			   data[i]= Xil_In32(addr);
   			   }
   		  	}
   		 else
   		  	{
   			for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
   			   {
   			   addr = XPAR_BRAM_0_BASEADDR+4*i;
   			   data[i]= Xil_In32(addr);
   			   }
   		  	}
   	     break;
   	  case 3: //read non-active portion
   		 if (Xil_In32 (TIMER_REGISTER3) & BRAM_SEGMENT )
   		    {
   			for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
   			   {
   			   addr = XPAR_BRAM_0_BASEADDR+4*i;
   			   data[i]= Xil_In32(addr);
   			   }
   		  	}
   		 else
   		  	{
   			for(i=0; i< BRAM_SPECTRUM_SIZE;i++)
   			   {
   			   addr = XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE+4*i;
   			   data[i]= Xil_In32(addr);
   			   }
   		  	}
         break;
   	  default:
   		 goto failed;
   		 break;
         }
   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, MEMORY_TX_BUFFER_SIZE);
   return;
failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   return;
   }

//
// CMD007: $AQ = Acquisition start/stop
//
void DoCommand007(u8 *u8Command, unsigned int uiCommandSize)
   {
	   char szParameter[32];
	   int iParam;
	   u32 u32RegData;

	   if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0)
	      {
		  SendError(ERROR_INVALID_COMMAND_PARAMETER);
		  return;
		  }

	   iParam = atoi32(szParameter);
	   switch(iParam)
	      {
		  //stop acquisition
		  case 2:
			 //stop scope
			 ip_scope_Acq(&IpScope,0);
			 IntcDisableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);

	         //stop MCA - its is stopped by setting bit 0 and bit 1 of timers register #5
		     // step 1: read register #5
		     u32RegData = Xil_In32(TIMER_REGISTER5);
			 // step 2: set b1b0 = 00, only first 11 bits are used
			 u32RegData &= ~(TIMER_MANUAL_START | TIMER_AUTO_START);
			 Xil_Out32(TIMER_REGISTER5, u32RegData);

			 break;

		  //start acquisition
		  case 1:
		     //start scope
			 ip_scope_Acq(&IpScope,1);
			 ip_scope_WaveformAccepted(&IpScope);
			 IntcEnableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);

			 //clean BRAM
			 ClearPlSpectrum(XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE);
			 ClearPlSpectrum(XPAR_BRAM_0_BASEADDR);

			 //start MCA - its is started by setting bit 1 and bit 2 of timers register #5
			 // step 1: read register #5
			 u32RegData = Xil_In32(TIMER_REGISTER5);
			 // step 2: set b2b1 = 01, only first 11 bits are used
			 u32RegData &= ~(TIMER_MANUAL_START | TIMER_AUTO_START);
			 u32RegData |= TIMER_MANUAL_START;
			 Xil_Out32(TIMER_REGISTER5, u32RegData);
			 break;

		  //clear all timers
		  case 4:
			 //set bits 9,10,11 of the register 5 to 1 and then to 0
			 //step 1: set bits 9,10,11 to 1
			 u32RegData = Xil_In32( TIMER_REGISTER5);
			 u32RegData|= (TIMERC_CLEARED |TIMERB_CLEARED | TIMERA_CLEARED);
			 Xil_Out32(TIMER_REGISTER5, u32RegData);
			 //step 2: set bits 9,10,11 to 0
			 u32RegData&= ~(TIMERC_CLEARED |TIMERB_CLEARED | TIMERA_CLEARED);
			 Xil_Out32(TIMER_REGISTER5, u32RegData);
			 break;

		  default:
			 goto failed;
			 break;
	      }

   u8Replay[0] = '!';
   u8Replay[1] = 'A';
   u8Replay[2] = 'Q';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   return;

failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   }


//
// CMD008: Write to Timers register
// command parameter1: register ID (zero based index)
// command parameter2: register value
//
void DoCommand008(u8 *u8Command, unsigned int uiCommandSize)
   {
   char szParameter1[32];
   char szParameter2[32];
   u32 u32RegData, u32RegId;

   if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter1) == 0) goto failed;
   if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter2) == 0) goto failed;

   u32RegId = atou32(szParameter1);
   u32RegData = atou32(szParameter2);

   Xil_Out32(XPAR_DPP_0_PHA_IP_TIMERS_0_S00_AXI_BASEADDR + (u32RegId<<2), u32RegData);

   //send response
   u8Replay[0] = '!';
   u8Replay[1] = 'W';
   u8Replay[2] = 'T';
   u8Replay[3] = (u8) '\n';
   u8Replay[4] = (u8) '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   return;
failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   }
//
// CMD009: $RT = read from a Timers' register
// command parameter1: register ID (zero based index or -1)
//
void DoCommand009(u8 *u8Command, unsigned int uiCommandSize)
   {
   char szParameter[32];
   int iLen, i32RegId;
   u32 u32RegData, u32RegDataArray[6];
   u32 addr;
   if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;
   i32RegId = atoi32(szParameter);
   if(i32RegId == -1)    //read all
      {
	  addr = TIMER_REGISTER0;
	  for(int i=0;i<6;i++)
		 {
		 u32RegDataArray[i] = Xil_In32(addr);
		 addr += 4;
		 }
	  iLen = u32atoa(u32RegDataArray, 6, (char*)u8Replay+4);
	  }
   else    //read one
	  {
	  addr = TIMER_REGISTER0 + (u32)(i32RegId*4);
	  u32RegData = Xil_In32(addr);
	  iLen = u32toa(u32RegData, (char*)u8Replay+4);
	  }
   //send response: prefix
   u8Replay[0] = '!';
   u8Replay[1] = 'R';
   u8Replay[2] = 'T';
   u8Replay[3] = ' ';
   //send response: sufix
   u8Replay[iLen+4] = '\n';
   u8Replay[iLen+5] = '\r';
   u8Replay[iLen+6] = '\0';
   iLen = strlen((char*)u8Replay);
   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, iLen);
   return;
failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   return;
   }

//
// CMD010: $CS = Clear Spectrum, alternative $AQ 3
// command parameter1: segment ID
//
void DoCommand010(u8 *u8Command, unsigned int uiCommandSize)
   {
   char szParameter[32];
   int iParam;
   if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;
   iParam = atoi32(szParameter);
   switch(iParam)
      {
	  case 0://erase low segment
	     ClearPlSpectrum(XPAR_BRAM_0_BASEADDR);
		 break;
	  case 1: //erase high segment
		 ClearPlSpectrum(XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE);
	     break;
	  case 2: //erase active segment
		 if (Xil_In32(TIMER_REGISTER3) & BRAM_SEGMENT)
			ClearPlSpectrum(XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE);
		 else
			ClearPlSpectrum(XPAR_BRAM_0_BASEADDR);
	     break;
	  case 3: //erase non-active segment
		 if (Xil_In32(TIMER_REGISTER3) & BRAM_SEGMENT)
			ClearPlSpectrum(XPAR_BRAM_0_BASEADDR);
		 else
			ClearPlSpectrum(XPAR_BRAM_0_BASEADDR+4*BRAM_SPECTRUM_SIZE);
         break;
	  default:
		  goto failed;
		 break;
      }
   u8Replay[0] = '!';
   u8Replay[1] = 'C';
   u8Replay[2] = 'S';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';
   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   return;
failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   return;
   }


//
// CMD011: $GP = Get parameters
// command parameters: param_group_id, p1, p2, ... pn
//
void DoCommand011(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'G';
   u8Replay[2] = 'P';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }


//
// CMD012: $GR = Get ROIs
// no parameters, send 5 x 32-bit numbers
//
void DoCommand012(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'G';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD013: $SR = Set ROIs
//  no parameters, get 5 x 32-bit numbers
//
void DoCommand013(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'S';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD014: $VR = Get Value from ROI FIFO
// no parameters, get 5 x 32-bit numbers
//
void DoCommand014(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'V';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }
//
// CMD015: $RL. get real/live timer values
// no parameters
//
void DoCommand015(u8 *u8Command, unsigned int uiCommandSize)
   {
   int iLen;
   u32 u32RegDataArray[2];
   u32 addr;

   addr = TIMER_REGISTER6;
   for(int i=0;i<2;i++)
      {
	  u32RegDataArray[i] = Xil_In32(addr);
	  addr += 4;
	  }
   iLen = u32atoa(u32RegDataArray, 2, (char*)u8Replay+4);

   //send response
   //prefix
   u8Replay[0] = '!';
   u8Replay[1] = 'R';
   u8Replay[2] = 'L';
   u8Replay[3] = ' ';
   //sufix
   u8Replay[iLen+4] = '\n';
   u8Replay[iLen+5] = '\r';
   //u8Replay[iLen+6] = '\0';

   //iLen = strlen((char*)u8Replay);
   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, iLen+6);
   }



//
// Parses for command parameter
// if parameter is first then iParametrId = 1
// if parameter is second then iParameter = 2 etc
// on return the szParameter contains the command parameter as string (null terminated)
//
int GetCommandParameter(u8 *u8Command, unsigned int uiCommandSize, int iParameterId, char *szParameter)
   {
   char *c1,*c2,*cc;
   int i=0;
   char szCommand[MEMORY_RX_BUFFER_SIZE];

   memcpy(szCommand,u8Command,uiCommandSize);
   szCommand[uiCommandSize]='\0';

   cc = szCommand;

   //look for i-th occurrences of space character '' i=iParametrId
   while(1)
      {
	  i++;
	  c1 = strchr(cc,' ');
	  if(c1 == NULL) return 0;
	  cc = c1 + 1;
      if(i == iParameterId) break;
	  }

   //look for the next ' '
   c2 = strchr(cc,' ');
   if(c2)
	  {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }

   //look for the '\n'
   c2 = strchr(cc,'\n');
   if(c2)
	  {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }
   //look for the '\r'
   c2 = strchr(cc,'\r');
   if(c2)
      {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }
   return 0;
   }

//
// assuming iError in the interval [0, 99]
//
void SendError(int iErrorId)
   {
   char c1,c2;

   c1 = (char) (0x30 + iErrorId/10);
   c2 = (char) (0x30 + iErrorId%10);

   u8Replay[0] = '!';
   u8Replay[1] = 'E';
   u8Replay[2] = 'R';
   u8Replay[3] = 'R';
   u8Replay[4] = 'O';
   u8Replay[5] = 'R';
   u8Replay[6] = ':';
   u8Replay[7] = c1;
   u8Replay[8] = c2;
   u8Replay[9] = '\n';
   u8Replay[10] = '\r';
   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, 11);
   }


//
// hard-coded default values for the scope IP
//
void scope_SetDefaultUserParameters(u32 *prm)
   {
   prm[ 0]	= 20;		//Tclk: 			clock period time in nsec
   prm[ 1]	= 2048;		//bram_size:		scope size
   prm[ 2]	= 1638;		//(0.1,16,14);		threshold
   prm[ 3]	= 100;		//delay				delay in clocks
   prm[ 4]	= 1;		//enable			enable
   prm[ 5]  = 1;			//clear
   prm[ 6]  = 1;			//full
   }
//
// hard-coded default values
//
void shaper_SetDefaultUserParametersSlow (u32 *prm)
   {
   prm[ 0]	= 20;		//Tclk: 			clock period time in nsec
   prm[ 1]	= 100;		//Taur:				rise time of exponential input signal in nsec
   prm[ 2]	= 420;		//Taud:				decay time of exponential input signal in nsec
   prm[ 3]	= 1000;		//Taupk:			trapezoid filter: peaking time in nsec
   prm[ 4]	= 100;		//Taupk_top:		trapezoid flat top in nsec
   prm[ 5]	= 33554432;	//gain;				gain at output of the filter
   prm[ 6]	= 16119371; //b10: 				(exp(-Tclk/Taud,24,24);
   prm[ 7]  = 671088;	//na_inv			(Tclk/Taupk,26,25)
   prm[ 8]  = 47;		//na				Taupk/Tclk (10.0)
   prm[ 9]  = 52;		//nb				(Taupk+taupk_top)/Tclk (10.0)
   prm[10]  = 60572317;	//U(1,32,24);		b00
   prm[11]  = 13736022;	//U(0,24,24);		b20
   }
//
// hard-coded default values
//
void peakdetector_SetDefaultUserParametersSlow(u32 *prm)
   {
   prm[ 0]	= 20;		//Tclk: 	clock period time in nsec
   prm[ 1] = 15;		//x_delay 	= (int) ((0.1*IpShaper.ConfigPtr->Taupk+IpShaper.ConfigPtr->Taupk_top)/(double)IpShaper.ConfigPtr->Tclk);		//trapezoid filter: peaking time in sec
   prm[ 2] = 81;		//x_noise 	= (0.005,16,14);;
   prm[ 3] = 163;		//x_min 	= (0.01,16,14);
   prm[ 4] = 32602;		//x_max 	= (1.99,16,14);
   prm[ 5] = 1;			//en_pkd 	= 1;
   }

void ClearPlSpectrum(u32  base_addr)
   {
   u32 addr;
   int i;
   for(i=0; i< BRAM_SPECTRUM_SIZE;i++) //lower half
	  {
	  addr = base_addr+4*i;
	  Xil_Out32(addr,0);
	  }
   }
