/*
 * uart.c
 *
 *  Created on: Nov 13, 2019
 *      Author: boss
 */


#include "xparameters.h"
#include "xuartlite.h"
#include "xuartlite_l.h"
#include "xil_printf.h"
#include "define.h"
#include "uart.h"

static XUartLite UartLite;		/* Instance of the UartLite Device */

int UartLiteInit(u16 DeviceId)
   {
   int Status;

   Status = XUartLite_Initialize(&UartLite, DeviceId);
   if (Status != XST_SUCCESS)
      {
	  return XST_FAILURE;
	  }

   /*
	* Perform a self-test to ensure that the hardware was built correctly.
	*/
   Status = XUartLite_SelfTest(&UartLite);
   if (Status != XST_SUCCESS)
      {
	  return XST_FAILURE;
	  }

   return XST_SUCCESS;
   }

int UartLiteSendReply(u16 DeviceId, u8 *szReplay, unsigned int uiReplayCount)
   {
   unsigned int uiSentCountTotal = 0;
   unsigned int uiSentCount, uiRestCount;

   /*
    * Block sending the buffer.
    */
again:
   //wait until sending finished
   while(XUartLite_IsSending(&UartLite)==TRUE) {;}

   uiRestCount = uiReplayCount - uiSentCountTotal;
   uiSentCount = XUartLite_Send(&UartLite, szReplay+uiSentCountTotal, uiRestCount);
   uiSentCountTotal += uiSentCount;

   if (uiSentCountTotal == uiReplayCount)
      {
	  return XST_SUCCESS;
	  }
   goto again;

   }

//
// Receive Command
// function is blocked until '\r' character is received
//
int UartLiteReceiveCommand(u16 DeviceId, u8 *szCommand, unsigned int *uiReceivedCount, unsigned int uiMaxCount)
   {
   //u32 CsrRegister;
   u8  data;
   unsigned int ReceivedCount = 0;

   *uiReceivedCount = ReceivedCount;

   *uiReceivedCount = ReceivedCount;
   /*
    * Receive the number of bytes which is transfered.
	* Data may be received in fifo with some delay hence we continuously
	* check the receive fifo for valid data and update the receive buffer
	* accordingly.
	*/
   while(1)
	  {

	  //wait until there is data
	  while (XUartLite_IsReceiveEmpty(UartLite.RegBaseAddress)) {;}
	  //read data
	  data = XUartLite_ReadReg(UartLite.RegBaseAddress, XUL_RX_FIFO_OFFSET);

	  //return if buffer overflow
	  if(ReceivedCount == uiMaxCount)
	     {
		 return XST_FAILURE;
		 }

	  //take data
	  szCommand[ReceivedCount++] = data;
	  *uiReceivedCount = ReceivedCount;

	  //return if event character
	  if(data == EV_RXFLAG)
		 {
		 return XST_SUCCESS;
		 }
	  }
   }
