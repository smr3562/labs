/*
 * define.h
 *
 *  Created on: Oct 21, 2019
 *      Author: boss
 */

#ifndef SRC_DEFINE_H_
#define SRC_DEFINE_H_


#define MEMORY_RX_BUFFER_SIZE 256  //CMD_MAXCNT 256
#define MEMORY_TX_BUFFER_SIZE 256
//for ROI: 1023x12 + preffix (2) + size (2) + suffix (2)
#define UART_TX_BUFFER_SIZE 256

//BRAM is actually twice the size
#define CMD_CNT 15

#define ERROR_INVALID_COMMAND 0
#define ERROR_INVALID_COMMAND_PARAMETER 1
#define ERROR_SCOPE_DATA_NOT_READY 2


#define EV_RXFLAG 			((u8) 13)


#endif /* SRC_DEFINE_H_ */

