-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Feb 14 16:48:31 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-4/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_scope_0_0/design_1_ip_scope_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_scope_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog is
  port (
    axi_awready_reg_0 : out STD_LOGIC;
    axi_wready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    i : out STD_LOGIC_VECTOR ( 10 downto 0 );
    \slv_reg_array_reg[0][0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[3][15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r3_dina : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg_array_reg[5][0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[6][10]_0\ : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    r9_douta : in STD_LOGIC_VECTOR ( 31 downto 0 );
    q : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog : entity is "axibusdomain_axi_lite_interface_verilog";
end design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog;

architecture STRUCTURE of design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog is
  signal axi_araddr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^d\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dec_r__15\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^i\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal r1_addra : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal r2_wea : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \^r3_dina\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r4_threshold : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r5_enable : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal r6_delay : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal r7_clear : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg_array[0][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][12]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][13]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][16]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][17]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][18]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][20]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][21]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][22]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][24]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][25]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][26]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][27]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][28]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][29]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][30]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_6_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][8]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][10]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][12]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][13]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][16]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][17]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][18]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][20]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][21]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][22]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][24]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][25]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][26]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][27]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][28]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][29]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][30]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][8]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][7]_i_2_n_0\ : STD_LOGIC;
  signal \^slv_reg_array_reg[0][0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg_array_reg[3][15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^slv_reg_array_reg[5][0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg_array_reg[6][10]_0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal slv_wire_array : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \slv_reg_array[0][10]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \slv_reg_array[0][11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \slv_reg_array[0][12]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \slv_reg_array[0][13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \slv_reg_array[0][14]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \slv_reg_array[0][15]_i_3\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \slv_reg_array[0][16]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg_array[0][17]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[0][18]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \slv_reg_array[0][19]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \slv_reg_array[0][1]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \slv_reg_array[0][20]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \slv_reg_array[0][21]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \slv_reg_array[0][22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \slv_reg_array[0][23]_i_3\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \slv_reg_array[0][24]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \slv_reg_array[0][25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \slv_reg_array[0][26]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \slv_reg_array[0][27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \slv_reg_array[0][28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \slv_reg_array[0][29]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \slv_reg_array[0][2]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \slv_reg_array[0][30]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \slv_reg_array[0][31]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[0][3]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \slv_reg_array[0][4]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \slv_reg_array[0][5]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \slv_reg_array[0][6]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \slv_reg_array[0][7]_i_3\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \slv_reg_array[0][8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \slv_reg_array[0][9]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \slv_reg_array[1][0]_i_2\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \slv_reg_array[1][11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \slv_reg_array[1][12]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \slv_reg_array[1][13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \slv_reg_array[1][14]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \slv_reg_array[1][15]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \slv_reg_array[1][16]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg_array[1][17]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[1][18]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \slv_reg_array[1][19]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \slv_reg_array[1][1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg_array[1][20]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \slv_reg_array[1][21]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \slv_reg_array[1][22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \slv_reg_array[1][23]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \slv_reg_array[1][24]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \slv_reg_array[1][25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \slv_reg_array[1][26]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \slv_reg_array[1][27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \slv_reg_array[1][28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \slv_reg_array[1][29]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \slv_reg_array[1][30]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \slv_reg_array[1][31]_i_2\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[1][3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \slv_reg_array[1][4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \slv_reg_array[1][7]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \slv_reg_array[1][8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \slv_reg_array[2][6]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[3][1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg_array[3][3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \slv_reg_array[3][4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \slv_reg_array[3][6]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[3][7]_i_2\ : label is "soft_lutpair6";
begin
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  d(0) <= \^d\(0);
  i(10 downto 0) <= \^i\(10 downto 0);
  r3_dina(31 downto 0) <= \^r3_dina\(31 downto 0);
  \slv_reg_array_reg[0][0]_0\(0) <= \^slv_reg_array_reg[0][0]_0\(0);
  \slv_reg_array_reg[3][15]_0\(15 downto 0) <= \^slv_reg_array_reg[3][15]_0\(15 downto 0);
  \slv_reg_array_reg[5][0]_0\(0) <= \^slv_reg_array_reg[5][0]_0\(0);
  \slv_reg_array_reg[6][10]_0\(10 downto 0) <= \^slv_reg_array_reg[6][10]_0\(10 downto 0);
\axi_araddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(0),
      Q => axi_araddr(0),
      R => p_0_in
    );
\axi_araddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(1),
      Q => axi_araddr(1),
      R => p_0_in
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(2),
      Q => axi_araddr(2),
      R => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(3),
      Q => axi_araddr(3),
      R => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(4),
      Q => axi_araddr(4),
      R => p_0_in
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(5),
      Q => axi_araddr(5),
      R => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => p_0_in
    );
\axi_awaddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(0),
      Q => axi_awaddr(0),
      R => p_0_in
    );
\axi_awaddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(1),
      Q => axi_awaddr(1),
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(2),
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(3),
      Q => axi_awaddr(3),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(4),
      Q => axi_awaddr(4),
      R => p_0_in
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(5),
      Q => axi_awaddr(5),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axibusdomain_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => p_0_in
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => axibusdomain_s_axi_bvalid,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(0),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[0]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[0]_i_3_n_0\,
      O => slv_wire_array(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => q(0),
      I1 => \^slv_reg_array_reg[6][10]_0\(0),
      I2 => \dec_r__15\(1),
      I3 => \^slv_reg_array_reg[5][0]_0\(0),
      I4 => \dec_r__15\(0),
      I5 => \^r3_dina\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(0),
      I1 => \^d\(0),
      I2 => \dec_r__15\(1),
      I3 => \^i\(0),
      I4 => \dec_r__15\(0),
      I5 => \^slv_reg_array_reg[0][0]_0\(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(10),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[10]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[10]_i_3_n_0\,
      O => slv_wire_array(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(10),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(10),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(10),
      I1 => r5_enable(10),
      I2 => \dec_r__15\(1),
      I3 => \^i\(10),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(11),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[11]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[11]_i_3_n_0\,
      O => slv_wire_array(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(11),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(11),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(11),
      I1 => r5_enable(11),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(11),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(12),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[12]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[12]_i_3_n_0\,
      O => slv_wire_array(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(12),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(12),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(12),
      I1 => r5_enable(12),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(12),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(13),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[13]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[13]_i_3_n_0\,
      O => slv_wire_array(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(13),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(13),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(13),
      I1 => r5_enable(13),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(13),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(14),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[14]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[14]_i_3_n_0\,
      O => slv_wire_array(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(14),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(14),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(14),
      I1 => r5_enable(14),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(14),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(15),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[15]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[15]_i_3_n_0\,
      O => slv_wire_array(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(15),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(15),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(15),
      I1 => r5_enable(15),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(15),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(16),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[16]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[16]_i_3_n_0\,
      O => slv_wire_array(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(16),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(16),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(16),
      I1 => r5_enable(16),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(16),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(17),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[17]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[17]_i_3_n_0\,
      O => slv_wire_array(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(17),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(17),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(17),
      I1 => r5_enable(17),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(17),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(18),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[18]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[18]_i_3_n_0\,
      O => slv_wire_array(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(18),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(18),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(18),
      I1 => r5_enable(18),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(18),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(19),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[19]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[19]_i_3_n_0\,
      O => slv_wire_array(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(19),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(19),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(19),
      I1 => r5_enable(19),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(19),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(1),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[1]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[1]_i_3_n_0\,
      O => slv_wire_array(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(1),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(1),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(1),
      I1 => r5_enable(1),
      I2 => \dec_r__15\(1),
      I3 => \^i\(1),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(20),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[20]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[20]_i_3_n_0\,
      O => slv_wire_array(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(20),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(20),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(20),
      I1 => r5_enable(20),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(20),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(21),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[21]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[21]_i_3_n_0\,
      O => slv_wire_array(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(21),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(21),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(21),
      I1 => r5_enable(21),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(21),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(22),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[22]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[22]_i_3_n_0\,
      O => slv_wire_array(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(22),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(22),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(22),
      I1 => r5_enable(22),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(22),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(23),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[23]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[23]_i_3_n_0\,
      O => slv_wire_array(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(23),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(23),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(23),
      I1 => r5_enable(23),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(23),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(24),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[24]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[24]_i_3_n_0\,
      O => slv_wire_array(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(24),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(24),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(24),
      I1 => r5_enable(24),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(24),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(25),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[25]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[25]_i_3_n_0\,
      O => slv_wire_array(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(25),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(25),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(25),
      I1 => r5_enable(25),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(25),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(26),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[26]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[26]_i_3_n_0\,
      O => slv_wire_array(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(26),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(26),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(26),
      I1 => r5_enable(26),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(26),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(27),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[27]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[27]_i_3_n_0\,
      O => slv_wire_array(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(27),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(27),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(27),
      I1 => r5_enable(27),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(27),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(28),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[28]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[28]_i_3_n_0\,
      O => slv_wire_array(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(28),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(28),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(28),
      I1 => r5_enable(28),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(28),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(29),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[29]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[29]_i_3_n_0\,
      O => slv_wire_array(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(29),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(29),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(29),
      I1 => r5_enable(29),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(29),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(2),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[2]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[2]_i_3_n_0\,
      O => slv_wire_array(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(2),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(2),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(2),
      I1 => r5_enable(2),
      I2 => \dec_r__15\(1),
      I3 => \^i\(2),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(30),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[30]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[30]_i_3_n_0\,
      O => slv_wire_array(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(30),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(30),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(30),
      I1 => r5_enable(30),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(30),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(31),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[31]_i_3_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[31]_i_5_n_0\,
      O => slv_wire_array(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => axi_araddr(0),
      I1 => axi_araddr(4),
      I2 => axi_araddr(1),
      I3 => axi_araddr(5),
      I4 => axi_araddr(3),
      I5 => axi_araddr(2),
      O => \dec_r__15\(3)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(31),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(31),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(4),
      I3 => axi_araddr(1),
      O => \dec_r__15\(2)
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(31),
      I1 => r5_enable(31),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(31),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(31),
      O => \axi_rdata[31]_i_5_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(3),
      I3 => axi_araddr(1),
      O => \dec_r__15\(1)
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(2),
      I3 => axi_araddr(1),
      O => \dec_r__15\(0)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(3),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[3]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[3]_i_3_n_0\,
      O => slv_wire_array(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(3),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(3),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(3),
      I1 => r5_enable(3),
      I2 => \dec_r__15\(1),
      I3 => \^i\(3),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(4),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[4]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[4]_i_3_n_0\,
      O => slv_wire_array(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(4),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(4),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(4),
      I1 => r5_enable(4),
      I2 => \dec_r__15\(1),
      I3 => \^i\(4),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(5),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[5]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[5]_i_3_n_0\,
      O => slv_wire_array(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(5),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(5),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(5),
      I1 => r5_enable(5),
      I2 => \dec_r__15\(1),
      I3 => \^i\(5),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(6),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[6]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[6]_i_3_n_0\,
      O => slv_wire_array(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(6),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(6),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(6),
      I1 => r5_enable(6),
      I2 => \dec_r__15\(1),
      I3 => \^i\(6),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(7),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[7]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[7]_i_3_n_0\,
      O => slv_wire_array(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(7),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(7),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(7),
      I1 => r5_enable(7),
      I2 => \dec_r__15\(1),
      I3 => \^i\(7),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(8),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[8]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[8]_i_3_n_0\,
      O => slv_wire_array(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(8),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(8),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(8),
      I1 => r5_enable(8),
      I2 => \dec_r__15\(1),
      I3 => \^i\(8),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(9),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[9]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[9]_i_3_n_0\,
      O => slv_wire_array(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(9),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(9),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(9),
      I1 => r5_enable(9),
      I2 => \dec_r__15\(1),
      I3 => \^i\(9),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(0),
      Q => axibusdomain_s_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(10),
      Q => axibusdomain_s_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(11),
      Q => axibusdomain_s_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(12),
      Q => axibusdomain_s_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(13),
      Q => axibusdomain_s_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(14),
      Q => axibusdomain_s_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(15),
      Q => axibusdomain_s_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(16),
      Q => axibusdomain_s_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(17),
      Q => axibusdomain_s_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(18),
      Q => axibusdomain_s_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(19),
      Q => axibusdomain_s_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(1),
      Q => axibusdomain_s_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(20),
      Q => axibusdomain_s_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(21),
      Q => axibusdomain_s_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(22),
      Q => axibusdomain_s_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(23),
      Q => axibusdomain_s_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(24),
      Q => axibusdomain_s_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(25),
      Q => axibusdomain_s_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(26),
      Q => axibusdomain_s_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(27),
      Q => axibusdomain_s_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(28),
      Q => axibusdomain_s_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(29),
      Q => axibusdomain_s_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(2),
      Q => axibusdomain_s_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(30),
      Q => axibusdomain_s_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(31),
      Q => axibusdomain_s_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(3),
      Q => axibusdomain_s_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(4),
      Q => axibusdomain_s_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(5),
      Q => axibusdomain_s_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(6),
      Q => axibusdomain_s_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(7),
      Q => axibusdomain_s_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(8),
      Q => axibusdomain_s_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(9),
      Q => axibusdomain_s_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^axibusdomain_s_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => p_0_in
    );
\slv_reg_array[0][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8FFF000080000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      I3 => \slv_reg_array[0][31]_i_4_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[0][0]_0\(0),
      O => \slv_reg_array[0][0]_i_1_n_0\
    );
\slv_reg_array[0][10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(10),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][10]_i_1_n_0\
    );
\slv_reg_array[0][11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(11),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][11]_i_1_n_0\
    );
\slv_reg_array[0][12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(12),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][12]_i_1_n_0\
    );
\slv_reg_array[0][13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(13),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][13]_i_1_n_0\
    );
\slv_reg_array[0][14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(14),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][14]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      O => \slv_reg_array[0][15]_i_2_n_0\
    );
\slv_reg_array[0][15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(15),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][15]_i_3_n_0\
    );
\slv_reg_array[0][15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => axibusdomain_aresetn,
      I5 => axibusdomain_s_axi_wstrb(1),
      O => \slv_reg_array[0][15]_i_4_n_0\
    );
\slv_reg_array[0][15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][15]_i_5_n_0\
    );
\slv_reg_array[0][16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(16),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][16]_i_1_n_0\
    );
\slv_reg_array[0][17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(17),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][17]_i_1_n_0\
    );
\slv_reg_array[0][18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(18),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][18]_i_1_n_0\
    );
\slv_reg_array[0][19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(19),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][19]_i_1_n_0\
    );
\slv_reg_array[0][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(1),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][1]_i_1_n_0\
    );
\slv_reg_array[0][20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(20),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][20]_i_1_n_0\
    );
\slv_reg_array[0][21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(21),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][21]_i_1_n_0\
    );
\slv_reg_array[0][22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(22),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][22]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][23]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][23]_i_4_n_0\,
      O => \slv_reg_array[0][23]_i_2_n_0\
    );
\slv_reg_array[0][23]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(23),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][23]_i_3_n_0\
    );
\slv_reg_array[0][23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_4_n_0\
    );
\slv_reg_array[0][23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(2),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_5_n_0\
    );
\slv_reg_array[0][24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(24),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][24]_i_1_n_0\
    );
\slv_reg_array[0][25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(25),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][25]_i_1_n_0\
    );
\slv_reg_array[0][26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(26),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][26]_i_1_n_0\
    );
\slv_reg_array[0][27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(27),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][27]_i_1_n_0\
    );
\slv_reg_array[0][28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(28),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][28]_i_1_n_0\
    );
\slv_reg_array[0][29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(29),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][29]_i_1_n_0\
    );
\slv_reg_array[0][2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(2),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][2]_i_1_n_0\
    );
\slv_reg_array[0][30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(30),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][30]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][31]_i_2_n_0\
    );
\slv_reg_array[0][31]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(31),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][31]_i_3_n_0\
    );
\slv_reg_array[0][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFCFFFCFFFCFEFD"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[0][31]_i_4_n_0\
    );
\slv_reg_array[0][31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_5_n_0\
    );
\slv_reg_array[0][31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(3),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_6_n_0\
    );
\slv_reg_array[0][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(3),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][3]_i_1_n_0\
    );
\slv_reg_array[0][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(4),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][4]_i_1_n_0\
    );
\slv_reg_array[0][5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(5),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][5]_i_1_n_0\
    );
\slv_reg_array[0][6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(6),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][6]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      O => \slv_reg_array[0][7]_i_2_n_0\
    );
\slv_reg_array[0][7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(7),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][7]_i_3_n_0\
    );
\slv_reg_array[0][7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_4_n_0\
    );
\slv_reg_array[0][7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(0),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_5_n_0\
    );
\slv_reg_array[0][8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(8),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][8]_i_1_n_0\
    );
\slv_reg_array[0][9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(9),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][9]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4FFF404040004040"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => \slv_reg_array[1][0]_i_2_n_0\,
      I2 => \slv_reg_array[1][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^i\(0),
      O => \slv_reg_array[1][0]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(0),
      O => \slv_reg_array[1][0]_i_2_n_0\
    );
\slv_reg_array[1][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDD0200"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(10),
      I4 => \^i\(10),
      O => \slv_reg_array[1][10]_i_1_n_0\
    );
\slv_reg_array[1][10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][10]_i_2_n_0\
    );
\slv_reg_array[1][11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(11),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][11]_i_1_n_0\
    );
\slv_reg_array[1][12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(12),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][12]_i_1_n_0\
    );
\slv_reg_array[1][13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(13),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][13]_i_1_n_0\
    );
\slv_reg_array[1][14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(14),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array[1][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      O => \slv_reg_array[1][15]_i_1_n_0\
    );
\slv_reg_array[1][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(15),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][15]_i_2_n_0\
    );
\slv_reg_array[1][16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(16),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][16]_i_1_n_0\
    );
\slv_reg_array[1][17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(17),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][17]_i_1_n_0\
    );
\slv_reg_array[1][18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(18),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][18]_i_1_n_0\
    );
\slv_reg_array[1][19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(19),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][19]_i_1_n_0\
    );
\slv_reg_array[1][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(1),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][1]_i_1_n_0\
    );
\slv_reg_array[1][20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(20),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][20]_i_1_n_0\
    );
\slv_reg_array[1][21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(21),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][21]_i_1_n_0\
    );
\slv_reg_array[1][22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(22),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][22]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      O => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(23),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][23]_i_2_n_0\
    );
\slv_reg_array[1][24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(24),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][24]_i_1_n_0\
    );
\slv_reg_array[1][25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(25),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][25]_i_1_n_0\
    );
\slv_reg_array[1][26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(26),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][26]_i_1_n_0\
    );
\slv_reg_array[1][27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(27),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][27]_i_1_n_0\
    );
\slv_reg_array[1][28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(28),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][28]_i_1_n_0\
    );
\slv_reg_array[1][29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(29),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][29]_i_1_n_0\
    );
\slv_reg_array[1][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(2),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(2),
      O => \slv_reg_array[1][2]_i_1_n_0\
    );
\slv_reg_array[1][30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(30),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][30]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      O => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(31),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][31]_i_2_n_0\
    );
\slv_reg_array[1][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][31]_i_3_n_0\
    );
\slv_reg_array[1][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \slv_reg_array[1][10]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[1][31]_i_4_n_0\
    );
\slv_reg_array[1][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(3),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][3]_i_1_n_0\
    );
\slv_reg_array[1][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(4),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][4]_i_1_n_0\
    );
\slv_reg_array[1][5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(5),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(5),
      O => \slv_reg_array[1][5]_i_1_n_0\
    );
\slv_reg_array[1][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(6),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(6),
      O => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array[1][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      O => \slv_reg_array[1][7]_i_1_n_0\
    );
\slv_reg_array[1][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(7),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][7]_i_2_n_0\
    );
\slv_reg_array[1][8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(8),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][8]_i_1_n_0\
    );
\slv_reg_array[1][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDD0200"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(9),
      I4 => \^i\(9),
      O => \slv_reg_array[1][9]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFEF00"
    )
        port map (
      I0 => \slv_reg_array[2][6]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \slv_reg_array[2][0]_i_2_n_0\,
      I4 => \^d\(0),
      O => \slv_reg_array[2][0]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \slv_reg_array[2][31]_i_2_n_0\,
      I1 => \slv_reg_array[0][7]_i_5_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[2][0]_i_2_n_0\
    );
\slv_reg_array[2][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE0100"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(10),
      I4 => r5_enable(10),
      O => \slv_reg_array[2][10]_i_1_n_0\
    );
\slv_reg_array[2][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array[2][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array[2][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(2),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(2),
      O => \slv_reg_array[2][2]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(5),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[2][31]_i_2_n_0\
    );
\slv_reg_array[2][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(5),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(5),
      O => \slv_reg_array[2][5]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(6),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(6),
      O => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      O => \slv_reg_array[2][6]_i_2_n_0\
    );
\slv_reg_array[2][6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000F0001000A"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[2][6]_i_3_n_0\
    );
\slv_reg_array[2][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][7]_i_1_n_0\
    );
\slv_reg_array[2][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE0100"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(9),
      I4 => r5_enable(9),
      O => \slv_reg_array[2][9]_i_1_n_0\
    );
\slv_reg_array[3][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4FF040404000404"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => \slv_reg_array[1][0]_i_2_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[3][15]_0\(0),
      O => \slv_reg_array[3][0]_i_1_n_0\
    );
\slv_reg_array[3][10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(10),
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(10),
      O => \slv_reg_array[3][10]_i_1_n_0\
    );
\slv_reg_array[3][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array[3][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(1),
      O => \slv_reg_array[3][1]_i_1_n_0\
    );
\slv_reg_array[3][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array[3][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(2),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][2]_i_1_n_0\
    );
\slv_reg_array[3][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array[3][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(3),
      O => \slv_reg_array[3][3]_i_1_n_0\
    );
\slv_reg_array[3][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(4),
      O => \slv_reg_array[3][4]_i_1_n_0\
    );
\slv_reg_array[3][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(5),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][5]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(6),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][6]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(5),
      O => \slv_reg_array[3][6]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array[3][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(7),
      O => \slv_reg_array[3][7]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFDFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(5),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(1),
      I4 => axi_awaddr(4),
      I5 => axi_awaddr(3),
      O => \slv_reg_array[3][7]_i_3_n_0\
    );
\slv_reg_array[3][9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(9),
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(9),
      O => \slv_reg_array[3][9]_i_1_n_0\
    );
\slv_reg_array[4][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[4][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^r3_dina\(0),
      O => \slv_reg_array[4][0]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][15]_i_2_n_0\
    );
\slv_reg_array[4][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array[4][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][23]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array[4][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][31]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[4][31]_i_3_n_0\
    );
\slv_reg_array[4][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array[4][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][7]_i_2_n_0\
    );
\slv_reg_array[5][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[5][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[5][0]_0\(0),
      O => \slv_reg_array[5][0]_i_1_n_0\
    );
\slv_reg_array[5][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array[5][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][15]_i_2_n_0\
    );
\slv_reg_array[5][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array[5][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][23]_i_2_n_0\
    );
\slv_reg_array[5][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array[5][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][31]_i_2_n_0\
    );
\slv_reg_array[5][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFFFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(5),
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[5][31]_i_3_n_0\
    );
\slv_reg_array[5][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array[5][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][7]_i_2_n_0\
    );
\slv_reg_array[6][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[6][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[6][10]_0\(0),
      O => \slv_reg_array[6][0]_i_1_n_0\
    );
\slv_reg_array[6][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array[6][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][15]_i_2_n_0\
    );
\slv_reg_array[6][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array[6][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][23]_i_2_n_0\
    );
\slv_reg_array[6][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array[6][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][31]_i_2_n_0\
    );
\slv_reg_array[6][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[6][31]_i_3_n_0\
    );
\slv_reg_array[6][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array[6][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][7]_i_2_n_0\
    );
\slv_reg_array_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[0][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[0][0]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[0][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => r7_clear(10),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r7_clear(11),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r7_clear(12),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r7_clear(13),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r7_clear(14),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r7_clear(15),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r7_clear(16),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r7_clear(17),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r7_clear(18),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r7_clear(19),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => r7_clear(1),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r7_clear(20),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r7_clear(21),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r7_clear(22),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r7_clear(23),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r7_clear(24),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r7_clear(25),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r7_clear(26),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r7_clear(27),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r7_clear(28),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r7_clear(29),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => r7_clear(2),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r7_clear(30),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r7_clear(31),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => r7_clear(3),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => r7_clear(4),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => r7_clear(5),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => r7_clear(6),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => r7_clear(7),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => r7_clear(8),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => r7_clear(9),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[1][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][0]_i_1_n_0\,
      Q => \^i\(0),
      R => '0'
    );
\slv_reg_array_reg[1][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][10]_i_1_n_0\,
      Q => \^i\(10),
      R => '0'
    );
\slv_reg_array_reg[1][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => r6_delay(11),
      R => '0'
    );
\slv_reg_array_reg[1][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => r6_delay(12),
      R => '0'
    );
\slv_reg_array_reg[1][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => r6_delay(13),
      R => '0'
    );
\slv_reg_array_reg[1][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => r6_delay(14),
      R => '0'
    );
\slv_reg_array_reg[1][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => r6_delay(15),
      R => '0'
    );
\slv_reg_array_reg[1][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r6_delay(16),
      R => '0'
    );
\slv_reg_array_reg[1][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r6_delay(17),
      R => '0'
    );
\slv_reg_array_reg[1][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r6_delay(18),
      R => '0'
    );
\slv_reg_array_reg[1][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r6_delay(19),
      R => '0'
    );
\slv_reg_array_reg[1][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => \^i\(1),
      R => '0'
    );
\slv_reg_array_reg[1][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r6_delay(20),
      R => '0'
    );
\slv_reg_array_reg[1][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r6_delay(21),
      R => '0'
    );
\slv_reg_array_reg[1][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r6_delay(22),
      R => '0'
    );
\slv_reg_array_reg[1][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r6_delay(23),
      R => '0'
    );
\slv_reg_array_reg[1][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r6_delay(24),
      R => '0'
    );
\slv_reg_array_reg[1][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r6_delay(25),
      R => '0'
    );
\slv_reg_array_reg[1][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r6_delay(26),
      R => '0'
    );
\slv_reg_array_reg[1][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r6_delay(27),
      R => '0'
    );
\slv_reg_array_reg[1][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r6_delay(28),
      R => '0'
    );
\slv_reg_array_reg[1][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r6_delay(29),
      R => '0'
    );
\slv_reg_array_reg[1][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][2]_i_1_n_0\,
      Q => \^i\(2),
      R => '0'
    );
\slv_reg_array_reg[1][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r6_delay(30),
      R => '0'
    );
\slv_reg_array_reg[1][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r6_delay(31),
      R => '0'
    );
\slv_reg_array_reg[1][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][3]_i_1_n_0\,
      Q => \^i\(3),
      R => '0'
    );
\slv_reg_array_reg[1][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][4]_i_1_n_0\,
      Q => \^i\(4),
      R => '0'
    );
\slv_reg_array_reg[1][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][5]_i_1_n_0\,
      Q => \^i\(5),
      R => '0'
    );
\slv_reg_array_reg[1][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][6]_i_1_n_0\,
      Q => \^i\(6),
      R => '0'
    );
\slv_reg_array_reg[1][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][7]_i_2_n_0\,
      Q => \^i\(7),
      R => '0'
    );
\slv_reg_array_reg[1][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => \^i\(8),
      R => '0'
    );
\slv_reg_array_reg[1][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][9]_i_1_n_0\,
      Q => \^i\(9),
      R => '0'
    );
\slv_reg_array_reg[2][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][0]_i_1_n_0\,
      Q => \^d\(0),
      R => '0'
    );
\slv_reg_array_reg[2][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][10]_i_1_n_0\,
      Q => r5_enable(10),
      R => '0'
    );
\slv_reg_array_reg[2][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => r5_enable(11),
      R => '0'
    );
\slv_reg_array_reg[2][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => r5_enable(12),
      R => '0'
    );
\slv_reg_array_reg[2][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => r5_enable(13),
      R => '0'
    );
\slv_reg_array_reg[2][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => r5_enable(14),
      R => '0'
    );
\slv_reg_array_reg[2][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => r5_enable(15),
      R => '0'
    );
\slv_reg_array_reg[2][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r5_enable(16),
      R => '0'
    );
\slv_reg_array_reg[2][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r5_enable(17),
      R => '0'
    );
\slv_reg_array_reg[2][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r5_enable(18),
      R => '0'
    );
\slv_reg_array_reg[2][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r5_enable(19),
      R => '0'
    );
\slv_reg_array_reg[2][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => r5_enable(1),
      R => '0'
    );
\slv_reg_array_reg[2][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r5_enable(20),
      R => '0'
    );
\slv_reg_array_reg[2][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r5_enable(21),
      R => '0'
    );
\slv_reg_array_reg[2][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r5_enable(22),
      R => '0'
    );
\slv_reg_array_reg[2][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r5_enable(23),
      R => '0'
    );
\slv_reg_array_reg[2][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r5_enable(24),
      R => '0'
    );
\slv_reg_array_reg[2][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r5_enable(25),
      R => '0'
    );
\slv_reg_array_reg[2][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r5_enable(26),
      R => '0'
    );
\slv_reg_array_reg[2][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r5_enable(27),
      R => '0'
    );
\slv_reg_array_reg[2][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r5_enable(28),
      R => '0'
    );
\slv_reg_array_reg[2][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r5_enable(29),
      R => '0'
    );
\slv_reg_array_reg[2][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][2]_i_1_n_0\,
      Q => r5_enable(2),
      R => '0'
    );
\slv_reg_array_reg[2][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r5_enable(30),
      R => '0'
    );
\slv_reg_array_reg[2][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r5_enable(31),
      R => '0'
    );
\slv_reg_array_reg[2][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][3]_i_1_n_0\,
      Q => r5_enable(3),
      R => '0'
    );
\slv_reg_array_reg[2][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][4]_i_1_n_0\,
      Q => r5_enable(4),
      R => '0'
    );
\slv_reg_array_reg[2][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][5]_i_1_n_0\,
      Q => r5_enable(5),
      R => '0'
    );
\slv_reg_array_reg[2][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][6]_i_1_n_0\,
      Q => r5_enable(6),
      R => '0'
    );
\slv_reg_array_reg[2][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][7]_i_2_n_0\,
      Q => r5_enable(7),
      R => '0'
    );
\slv_reg_array_reg[2][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => r5_enable(8),
      R => '0'
    );
\slv_reg_array_reg[2][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][9]_i_1_n_0\,
      Q => r5_enable(9),
      R => '0'
    );
\slv_reg_array_reg[3][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[3][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][10]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(10),
      R => '0'
    );
\slv_reg_array_reg[3][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(11),
      R => '0'
    );
\slv_reg_array_reg[3][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(12),
      R => '0'
    );
\slv_reg_array_reg[3][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(13),
      R => '0'
    );
\slv_reg_array_reg[3][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(14),
      R => '0'
    );
\slv_reg_array_reg[3][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(15),
      R => '0'
    );
\slv_reg_array_reg[3][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r4_threshold(16),
      R => '0'
    );
\slv_reg_array_reg[3][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r4_threshold(17),
      R => '0'
    );
\slv_reg_array_reg[3][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r4_threshold(18),
      R => '0'
    );
\slv_reg_array_reg[3][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r4_threshold(19),
      R => '0'
    );
\slv_reg_array_reg[3][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(1),
      R => '0'
    );
\slv_reg_array_reg[3][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r4_threshold(20),
      R => '0'
    );
\slv_reg_array_reg[3][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r4_threshold(21),
      R => '0'
    );
\slv_reg_array_reg[3][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r4_threshold(22),
      R => '0'
    );
\slv_reg_array_reg[3][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r4_threshold(23),
      R => '0'
    );
\slv_reg_array_reg[3][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r4_threshold(24),
      R => '0'
    );
\slv_reg_array_reg[3][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r4_threshold(25),
      R => '0'
    );
\slv_reg_array_reg[3][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r4_threshold(26),
      R => '0'
    );
\slv_reg_array_reg[3][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r4_threshold(27),
      R => '0'
    );
\slv_reg_array_reg[3][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r4_threshold(28),
      R => '0'
    );
\slv_reg_array_reg[3][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r4_threshold(29),
      R => '0'
    );
\slv_reg_array_reg[3][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(2),
      R => '0'
    );
\slv_reg_array_reg[3][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r4_threshold(30),
      R => '0'
    );
\slv_reg_array_reg[3][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r4_threshold(31),
      R => '0'
    );
\slv_reg_array_reg[3][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(3),
      R => '0'
    );
\slv_reg_array_reg[3][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(4),
      R => '0'
    );
\slv_reg_array_reg[3][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(5),
      R => '0'
    );
\slv_reg_array_reg[3][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(6),
      R => '0'
    );
\slv_reg_array_reg[3][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][7]_i_2_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(7),
      R => '0'
    );
\slv_reg_array_reg[3][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(8),
      R => '0'
    );
\slv_reg_array_reg[3][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][9]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(9),
      R => '0'
    );
\slv_reg_array_reg[4][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][0]_i_1_n_0\,
      Q => \^r3_dina\(0),
      R => '0'
    );
\slv_reg_array_reg[4][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => \^r3_dina\(10),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => \^r3_dina\(11),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => \^r3_dina\(12),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => \^r3_dina\(13),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => \^r3_dina\(14),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => \^r3_dina\(15),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => \^r3_dina\(16),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => \^r3_dina\(17),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => \^r3_dina\(18),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => \^r3_dina\(19),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => \^r3_dina\(1),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => \^r3_dina\(20),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => \^r3_dina\(21),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => \^r3_dina\(22),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => \^r3_dina\(23),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => \^r3_dina\(24),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => \^r3_dina\(25),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => \^r3_dina\(26),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => \^r3_dina\(27),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => \^r3_dina\(28),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => \^r3_dina\(29),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => \^r3_dina\(2),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => \^r3_dina\(30),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => \^r3_dina\(31),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => \^r3_dina\(3),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => \^r3_dina\(4),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => \^r3_dina\(5),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => \^r3_dina\(6),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => \^r3_dina\(7),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => \^r3_dina\(8),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => \^r3_dina\(9),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[5][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[5][0]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[5][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => r2_wea(10),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r2_wea(11),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r2_wea(12),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r2_wea(13),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r2_wea(14),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r2_wea(15),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r2_wea(16),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r2_wea(17),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r2_wea(18),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r2_wea(19),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => r2_wea(1),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r2_wea(20),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r2_wea(21),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r2_wea(22),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r2_wea(23),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r2_wea(24),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r2_wea(25),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r2_wea(26),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r2_wea(27),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r2_wea(28),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r2_wea(29),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => r2_wea(2),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r2_wea(30),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r2_wea(31),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => r2_wea(3),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => r2_wea(4),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => r2_wea(5),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => r2_wea(6),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => r2_wea(7),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => r2_wea(8),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => r2_wea(9),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[6][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[6][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(10),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r1_addra(11),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r1_addra(12),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r1_addra(13),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r1_addra(14),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r1_addra(15),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r1_addra(16),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r1_addra(17),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r1_addra(18),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r1_addra(19),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(1),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r1_addra(20),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r1_addra(21),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r1_addra(22),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r1_addra(23),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r1_addra(24),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r1_addra(25),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r1_addra(26),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r1_addra(27),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r1_addra(28),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r1_addra(29),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(2),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r1_addra(30),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r1_addra(31),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(3),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(4),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(5),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(6),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(7),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(8),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(9),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axibusdomain_s_axi_rvalid\,
      I2 => \^axi_arready_reg_0\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a is
  port (
    \ff_2_17_reg[0]_0\ : out STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a : entity is "sysgen_mcode_block_2be3aaba4a";
end design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a;

architecture STRUCTURE of design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a is
begin
\ff_2_17_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => register4_q_net(0),
      Q => \ff_2_17_reg[0]_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d is
  port (
    \full_i_5_24_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    register_q_net : in STD_LOGIC;
    \state_4_23_reg[1]_2\ : in STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d : entity is "sysgen_mcode_block_46de72b52d";
end design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d;

architecture STRUCTURE of design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d is
  signal \addr_i_6_24[10]_i_1_n_0\ : STD_LOGIC;
  signal \addr_i_6_24[10]_i_2_n_0\ : STD_LOGIC;
  signal \addr_i_6_24[7]_i_2_n_0\ : STD_LOGIC;
  signal \^addrb\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \full_i_5_24[0]_i_1_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal rel_39_16 : STD_LOGIC;
  signal \rel_39_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_39_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_39_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_39_16_carry_n_0 : STD_LOGIC;
  signal rel_39_16_carry_n_1 : STD_LOGIC;
  signal rel_39_16_carry_n_2 : STD_LOGIC;
  signal rel_39_16_carry_n_3 : STD_LOGIC;
  signal state_4_23 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \state_4_23[0]_i_1_n_0\ : STD_LOGIC;
  signal \state_4_23[0]_i_2_n_0\ : STD_LOGIC;
  signal \state_4_23[1]_i_1_n_0\ : STD_LOGIC;
  signal wm_8_20_inv_i_1_n_0 : STD_LOGIC;
  signal wm_8_20_reg_inv_n_0 : STD_LOGIC;
  signal NLW_rel_39_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_39_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \addr_i_6_24[0]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addr_i_6_24[1]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addr_i_6_24[2]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \addr_i_6_24[3]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \addr_i_6_24[4]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \addr_i_6_24[7]_i_2\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \addr_i_6_24[9]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \full_i_5_24[0]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \state_4_23[0]_i_2\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of wm_8_20_inv_i_1 : label is "soft_lutpair35";
begin
  addrb(10 downto 0) <= \^addrb\(10 downto 0);
\addr_i_6_24[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^addrb\(0),
      O => p_0_in(0)
    );
\addr_i_6_24[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^addrb\(10),
      I1 => \addr_i_6_24[10]_i_2_n_0\,
      I2 => \^addrb\(7),
      I3 => \^addrb\(6),
      I4 => \^addrb\(8),
      I5 => \^addrb\(9),
      O => \addr_i_6_24[10]_i_1_n_0\
    );
\addr_i_6_24[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^addrb\(2),
      I1 => \^addrb\(0),
      I2 => \^addrb\(1),
      I3 => \^addrb\(3),
      I4 => \^addrb\(4),
      I5 => \^addrb\(5),
      O => \addr_i_6_24[10]_i_2_n_0\
    );
\addr_i_6_24[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^addrb\(0),
      I1 => \^addrb\(1),
      O => p_0_in(1)
    );
\addr_i_6_24[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^addrb\(2),
      I1 => \^addrb\(0),
      I2 => \^addrb\(1),
      O => p_0_in(2)
    );
\addr_i_6_24[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^addrb\(1),
      I1 => \^addrb\(0),
      I2 => \^addrb\(2),
      I3 => \^addrb\(3),
      O => p_0_in(3)
    );
\addr_i_6_24[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^addrb\(4),
      I1 => \^addrb\(1),
      I2 => \^addrb\(0),
      I3 => \^addrb\(2),
      I4 => \^addrb\(3),
      O => p_0_in(4)
    );
\addr_i_6_24[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^addrb\(5),
      I1 => \^addrb\(3),
      I2 => \^addrb\(2),
      I3 => \^addrb\(0),
      I4 => \^addrb\(1),
      I5 => \^addrb\(4),
      O => p_0_in(5)
    );
\addr_i_6_24[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA6AAA"
    )
        port map (
      I0 => \^addrb\(6),
      I1 => \^addrb\(5),
      I2 => \^addrb\(4),
      I3 => \^addrb\(3),
      I4 => \addr_i_6_24[7]_i_2_n_0\,
      O => p_0_in(6)
    );
\addr_i_6_24[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^addrb\(7),
      I1 => \addr_i_6_24[7]_i_2_n_0\,
      I2 => \^addrb\(3),
      I3 => \^addrb\(4),
      I4 => \^addrb\(5),
      I5 => \^addrb\(6),
      O => p_0_in(7)
    );
\addr_i_6_24[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \^addrb\(1),
      I1 => \^addrb\(0),
      I2 => \^addrb\(2),
      O => \addr_i_6_24[7]_i_2_n_0\
    );
\addr_i_6_24[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \^addrb\(8),
      I1 => \^addrb\(6),
      I2 => \^addrb\(7),
      I3 => \addr_i_6_24[10]_i_2_n_0\,
      O => p_0_in(8)
    );
\addr_i_6_24[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^addrb\(9),
      I1 => \addr_i_6_24[10]_i_2_n_0\,
      I2 => \^addrb\(7),
      I3 => \^addrb\(6),
      I4 => \^addrb\(8),
      O => p_0_in(9)
    );
\addr_i_6_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(0),
      Q => \^addrb\(0),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \addr_i_6_24[10]_i_1_n_0\,
      Q => \^addrb\(10),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(1),
      Q => \^addrb\(1),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(2),
      Q => \^addrb\(2),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(3),
      Q => \^addrb\(3),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(4),
      Q => \^addrb\(4),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(5),
      Q => \^addrb\(5),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(6),
      Q => \^addrb\(6),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(7),
      Q => \^addrb\(7),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(8),
      Q => \^addrb\(8),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(9),
      Q => \^addrb\(9),
      R => wm_8_20_reg_inv_n_0
    );
\full_i_5_24[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => state_4_23(1),
      I1 => register_q_net,
      I2 => state_4_23(0),
      O => \full_i_5_24[0]_i_1_n_0\
    );
\full_i_5_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \full_i_5_24[0]_i_1_n_0\,
      Q => \full_i_5_24_reg[0]_0\(0),
      R => '0'
    );
rel_39_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_39_16_carry_n_0,
      CO(2) => rel_39_16_carry_n_1,
      CO(1) => rel_39_16_carry_n_2,
      CO(0) => rel_39_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => NLW_rel_39_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\rel_39_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_39_16_carry_n_0,
      CO(3) => rel_39_16,
      CO(2) => \rel_39_16_carry__0_n_1\,
      CO(1) => \rel_39_16_carry__0_n_2\,
      CO(0) => \rel_39_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_4_23_reg[1]_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_39_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_4_23_reg[1]_1\(3 downto 0)
    );
\state_4_23[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B7F7B7F7BFFFB7F7"
    )
        port map (
      I0 => state_4_23(1),
      I1 => register_q_net,
      I2 => state_4_23(0),
      I3 => rel_39_16,
      I4 => \^addrb\(10),
      I5 => \state_4_23[0]_i_2_n_0\,
      O => \state_4_23[0]_i_1_n_0\
    );
\state_4_23[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \^addrb\(9),
      I1 => \^addrb\(8),
      I2 => \^addrb\(6),
      I3 => \^addrb\(7),
      I4 => \addr_i_6_24[10]_i_2_n_0\,
      O => \state_4_23[0]_i_2_n_0\
    );
\state_4_23[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBFF0000F0000000"
    )
        port map (
      I0 => \state_4_23_reg[1]_2\,
      I1 => register4_q_net(0),
      I2 => rel_39_16,
      I3 => state_4_23(0),
      I4 => register_q_net,
      I5 => state_4_23(1),
      O => \state_4_23[1]_i_1_n_0\
    );
\state_4_23_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \state_4_23[0]_i_1_n_0\,
      Q => state_4_23(0),
      R => '0'
    );
\state_4_23_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \state_4_23[1]_i_1_n_0\,
      Q => state_4_23(1),
      R => '0'
    );
we_i_7_22_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => '1',
      Q => web(0),
      R => wm_8_20_reg_inv_n_0
    );
wm_8_20_inv_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => state_4_23(0),
      I1 => state_4_23(1),
      I2 => register_q_net,
      O => wm_8_20_inv_i_1_n_0
    );
wm_8_20_reg_inv: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => wm_8_20_inv_i_1_n_0,
      Q => wm_8_20_reg_inv_n_0,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_sysgen_relational_f845914c88 is
  port (
    LOAD : out STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    o : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_sysgen_relational_f845914c88 : entity is "sysgen_relational_f845914c88";
end design_1_ip_scope_0_0_sysgen_relational_f845914c88;

architecture STRUCTURE of design_1_ip_scope_0_0_sysgen_relational_f845914c88 is
  signal \op_mem_37_22[0]_i_2_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_3_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_4_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_5_n_0\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal result_12_3_rel : STD_LOGIC;
  signal \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\op_mem_37_22[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => o(9),
      I1 => Q(9),
      I2 => o(10),
      I3 => Q(10),
      O => \op_mem_37_22[0]_i_2_n_0\
    );
\op_mem_37_22[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(6),
      I1 => Q(6),
      I2 => Q(8),
      I3 => o(8),
      I4 => Q(7),
      I5 => o(7),
      O => \op_mem_37_22[0]_i_3_n_0\
    );
\op_mem_37_22[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(3),
      I1 => Q(3),
      I2 => Q(5),
      I3 => o(5),
      I4 => Q(4),
      I5 => o(4),
      O => \op_mem_37_22[0]_i_4_n_0\
    );
\op_mem_37_22[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(0),
      I1 => Q(0),
      I2 => Q(2),
      I3 => o(2),
      I4 => Q(1),
      I5 => o(1),
      O => \op_mem_37_22[0]_i_5_n_0\
    );
\op_mem_37_22_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => result_12_3_rel,
      Q => LOAD,
      R => '0'
    );
\op_mem_37_22_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result_12_3_rel,
      CO(2) => \op_mem_37_22_reg[0]_i_1_n_1\,
      CO(1) => \op_mem_37_22_reg[0]_i_1_n_2\,
      CO(0) => \op_mem_37_22_reg[0]_i_1_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\(3 downto 0),
      S(3) => \op_mem_37_22[0]_i_2_n_0\,
      S(2) => \op_mem_37_22[0]_i_3_n_0\,
      S(1) => \op_mem_37_22[0]_i_4_n_0\,
      S(0) => \op_mem_37_22[0]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => enable(0),
      Q => register_q_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net,
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  port (
    d3_net : out STD_LOGIC;
    d2_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net,
      Q => d3_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\ is
  port (
    d2_net : out STD_LOGIC;
    d1_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net,
      Q => d2_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\ is
  port (
    d1_net : out STD_LOGIC;
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_enable(0),
      Q => d1_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(15 downto 0) <= \^o\(15 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => \^o\(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => \^o\(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_39_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(15),
      I1 => \rel_39_16_carry__0\(15),
      I2 => \rel_39_16_carry__0\(14),
      I3 => \^o\(14),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0)
    );
\rel_39_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(13),
      I1 => \rel_39_16_carry__0\(13),
      I2 => \^o\(12),
      I3 => \rel_39_16_carry__0\(12),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(2)
    );
\rel_39_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(11),
      I1 => \rel_39_16_carry__0\(11),
      I2 => \^o\(10),
      I3 => \rel_39_16_carry__0\(10),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(1)
    );
\rel_39_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(9),
      I1 => \rel_39_16_carry__0\(9),
      I2 => \^o\(8),
      I3 => \rel_39_16_carry__0\(8),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(0)
    );
rel_39_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(7),
      I1 => \rel_39_16_carry__0\(7),
      I2 => \^o\(6),
      I3 => \rel_39_16_carry__0\(6),
      O => S(3)
    );
rel_39_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(5),
      I1 => \rel_39_16_carry__0\(5),
      I2 => \^o\(4),
      I3 => \rel_39_16_carry__0\(4),
      O => S(2)
    );
rel_39_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(3),
      I1 => \rel_39_16_carry__0\(3),
      I2 => \^o\(2),
      I3 => \rel_39_16_carry__0\(2),
      O => S(1)
    );
rel_39_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(1),
      I1 => \rel_39_16_carry__0\(1),
      I2 => \^o\(0),
      I3 => \rel_39_16_carry__0\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(15 downto 0) <= \^o\(15 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(14),
      Q => \^o\(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(15),
      Q => \^o\(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_39_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(13),
      I1 => \rel_39_16_carry__0\(13),
      I2 => \^o\(12),
      I3 => \rel_39_16_carry__0\(12),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(2)
    );
\rel_39_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(11),
      I1 => \rel_39_16_carry__0\(11),
      I2 => \^o\(10),
      I3 => \rel_39_16_carry__0\(10),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(1)
    );
\rel_39_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(9),
      I1 => \rel_39_16_carry__0\(9),
      I2 => \^o\(8),
      I3 => \rel_39_16_carry__0\(8),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(0)
    );
\rel_39_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(15),
      I1 => \rel_39_16_carry__0\(15),
      I2 => \rel_39_16_carry__0\(14),
      I3 => \^o\(14),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0)
    );
rel_39_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(7),
      I1 => \rel_39_16_carry__0\(7),
      I2 => \^o\(6),
      I3 => \rel_39_16_carry__0\(6),
      O => DI(3)
    );
rel_39_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(5),
      I1 => \rel_39_16_carry__0\(5),
      I2 => \^o\(4),
      I3 => \rel_39_16_carry__0\(4),
      O => DI(2)
    );
rel_39_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(3),
      I1 => \rel_39_16_carry__0\(3),
      I2 => \^o\(2),
      I3 => \rel_39_16_carry__0\(2),
      O => DI(1)
    );
rel_39_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(1),
      I1 => \rel_39_16_carry__0\(1),
      I2 => \^o\(0),
      I3 => \rel_39_16_carry__0\(0),
      O => DI(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => clear(0),
      Q => register4_q_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => r8_full(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => full(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_clear(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_xil_defaultlib_srlc33e is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_xil_defaultlib_srlc33e : entity is "xil_defaultlib_srlc33e";
end design_1_ip_scope_0_0_xil_defaultlib_srlc33e;

architecture STRUCTURE of design_1_ip_scope_0_0_xil_defaultlib_srlc33e is
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \reg_array[0].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \reg_array[0].fde_used.u2\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \reg_array[0].fde_used.u2\ : label is "TRUE";
begin
\reg_array[0].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \reg_array[0].fde_used.u2_0\(0),
      Q => full(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init : entity is "blk_mem_gen_prim_wrapper_init";
end design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 8) => dina(16 downto 9),
      DIADI(7 downto 0) => dina(7 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1) => dina(17),
      DIPADIP(0) => dina(8),
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15 downto 8) => douta(16 downto 9),
      DOADO(7 downto 0) => douta(7 downto 0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => douta(17),
      DOPADOP(0) => douta(8),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\ : entity is "blk_mem_gen_prim_wrapper_init";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 15) => B"00000000000000000",
      DIADI(14 downto 8) => dina(13 downto 7),
      DIADI(7) => '0',
      DIADI(6 downto 0) => dina(6 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20\,
      DOADO(14 downto 8) => douta(13 downto 7),
      DOADO(7) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28\,
      DOADO(6 downto 0) => douta(6 downto 0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71\,
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\ : entity is "blk_mem_gen_prim_wrapper_init";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 4) => addrb(10 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 8) => dina(16 downto 9),
      DIADI(7 downto 0) => dina(7 downto 0),
      DIBDI(31 downto 16) => B"0000000000000000",
      DIBDI(15 downto 8) => dinb(16 downto 9),
      DIBDI(7 downto 0) => dinb(7 downto 0),
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1) => dina(17),
      DIPADIP(0) => dina(8),
      DIPBDIP(3 downto 2) => B"00",
      DIPBDIP(1) => dinb(17),
      DIPBDIP(0) => dinb(8),
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15 downto 8) => douta(16 downto 9),
      DOADO(7 downto 0) => douta(7 downto 0),
      DOBDO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 16),
      DOBDO(15 downto 8) => doutb(16 downto 9),
      DOBDO(7 downto 0) => doutb(7 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => douta(17),
      DOPADOP(0) => douta(8),
      DOPBDOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 2),
      DOPBDOP(1) => doutb(17),
      DOPBDOP(0) => doutb(8),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => enb,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => web(0),
      WEBWE(2) => web(0),
      WEBWE(1) => web(0),
      WEBWE(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\ : entity is "blk_mem_gen_prim_wrapper_init";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 4) => addrb(10 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 15) => B"00000000000000000",
      DIADI(14 downto 8) => dina(13 downto 7),
      DIADI(7) => '0',
      DIADI(6 downto 0) => dina(6 downto 0),
      DIBDI(31 downto 15) => B"00000000000000000",
      DIBDI(14 downto 8) => dinb(13 downto 7),
      DIBDI(7) => '0',
      DIBDI(6 downto 0) => dinb(6 downto 0),
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20\,
      DOADO(14 downto 8) => douta(13 downto 7),
      DOADO(7) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28\,
      DOADO(6 downto 0) => douta(6 downto 0),
      DOBDO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 16),
      DOBDO(15) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52\,
      DOBDO(14 downto 8) => doutb(13 downto 7),
      DOBDO(7) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60\,
      DOBDO(6 downto 0) => doutb(6 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71\,
      DOPBDOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 2),
      DOPBDOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74\,
      DOPBDOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75\,
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => enb,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => web(0),
      WEBWE(2) => web(0),
      WEBWE(1) => web(0),
      WEBWE(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
klQ5h9hgPT4p1YBkLUYG8lguTKRhX0z2xODTfvC0iZ4ANS2ufXQ4XxirL31R7wyoNNR1hd7rMjwq
bhFP3bRYRg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
m0Wn8/fGtoK5/3nNxNBBvNhfylkjw8jAoTERkIzypaD+FUTSQmf52TpAbVY1LPDo/g6d8tzIrR8n
vZ2G5ckAbcwDRGFF5wpZ/kzyANCTceWo7S+WbzLbzhWkG5fkJbJk/tafbSvqruxodmgtrUI2IZDv
n7Mrf4GRIdC1iTgv+oM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mC2bSkADVOO0t7I1BTHPnOuMcPIdeypycvBkfsUtXFD/WSov2Pt6qF5xMTcs6woq+465yEDvey00
mpX9JC1ypEuHaZI+dtH7s+A4vYLXy34kFLryTKJtqycAy6XqNtTegvy4giaDFwDn7LgsamHSf/4D
cVGL06Ul6gm+ZOMvzok6Y9TBltlEkZOZ4GncKspHPcn3EJar4GdDxKcPll1RqSFbA2m7bWz6jPV3
uOD6IWnIflyobdLIsuB8hse3kjXBG5sl6X8AP4armpo9TNNnRIRG2FoI6g16Sj6zldS9XF54ge/w
jnsP0vce7kfzvP1LjtFrWdW0raW61dIddZVz5A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
LC8srWyP8+H+elFwWxNw8UhEF94Tx3vV4OlwBcwFszd9xuvwYRipLEwY4ar7HEoCwJI7beW6k6I1
ntJSXGEnPMgIDe7PadSQIz6gi9ErRHWRhWD6bEDNd/ZjFQuOakXk78jzxOTKuOpQ+ExjdHImyPzK
mDR0BnkVc/QiSO7zpOf3yjx9Vzb0Fba6OgXg0iLPz1MUr0ahYIc0MAkw4pRBo6IH5ilqVbkgNQcT
G4z/0OSzsBxipE9o1elAez7CnehtEQ66g6i3JpIIwqwnpZNQ9otSnS8LN/spRcQLOUuBq9ynGkPL
sF7EUF1gLkTFFMGgnZgawWi0V0jhiHQkJuLKPQ==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ijV0yStg7uRIl3uzK8/hlbIaWGHa9aPC5Eu/o1vErrwtArYsGFt3RCyG/S90FB6jkuLgqwPR8ZlQ
P9t/F2FWmEkwwjGbdrRKFfpbkjh5HVn0vvLKCP3SiVHXCOWxxb5z8BV+yCNdpgdnsHFecK1M8ydQ
C530kRu3UD1LcnZcWJi41LcJAc5rvlw/SP1gbl+I1qsRNEHsb+MK5vyjgwBZAqKyqi7/UK1VEPdq
myeWeCRrU0GqEq5y/PHBMknv1SqNe0d5qzG1rmAtC4df+iivMCc9xuHsCA7iqoe+ZKnMmnA/8F6+
nY+gx8AQNplCeFxWppKH952fIYplHtE2rKQyVQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
saHlQAxOuJkLItpv5N3fJoJP3EdR34QNYHtUBFx7fcQy8HAUaZ/9Tlt2kfpn9r/pk1MmJ7uf4z73
LyaG/PWw7v3yH/KIVSLeIxiaSHoVfdWH1/RDH6k3DTDNbWSJDOjGSvMTThUKe899F8IXTD5jKxCh
frGB3Io0W4klQV7ADEw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RFiy0rrSiVrZMC725S3XkUSDuMS7Z+xog1sOwADnIBmdCChtYLUQVO9oPjXIujfg3bBFTH3qzII3
Y63fFMSZnnzk+ups3RPMBqEPPdMQSwlpDzsvhCOwYHe/rsOPZvqd7lL6QOKoA3mS9TZIP+mOaTKd
vzQiVyfS0rs+QHdJcw49jD5y7Dc3clQHD9xZMAMUHa5v8hzX4IOcnkSUOLpsZptR/WM7rcMnTzCL
x6m2UC+xSrwrb5vEz9cePMHx9NJyO2DBG0HTEDzGQQkrOCJJjvBxtB3r/E7/3eWGHvMwphSJnfCd
PfvYXz88X+ZVCW9dTNfc75jI7ogVMSunmtUuMw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
lfjxLlExJbLibO3ZJmufIxRbQBS8mrIsevWoNvhxn1t2crAQGU1yEPkj/++GcYN97vrYLsOJ/HXK
6bwuqQLVphq6ToSeVVL4LbmEiWNr+tdcxnNLHuktGUuH8PKoWWJ2XNlxErtmz4QW5Dw62n4KL8B2
d4OeGl0IEL1ghs6j/XscQ71K6O0+zJ04rgDYoCjZpmj1s5/u0kF4LGuk2r79GlYAU6IhapTzMtAl
dgJkWT3hKLQMJVUH/sxa4ysIBkVM0kuzO++ue4SAoTusrsgqQEM1rc1GEMNq/lMKNpGUHkXb2FM9
KLIvaSTO/SYooCgLmqzOh9jqEx1P4aGII2CdXg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
X0EPoeI4SFxUq3YabXIBjxeb37JrVbOJCzSLOghPhDiyxTk/TZvROJupeVQ7KVOWcZDtl8zQwgaV
pQzK+y0hOREwSkmW+P5q9+oTf/OfWwCGzYskaWkiChzjAtz2u5/e4YyjL7aPr+ZwNOxoVzO1jnlU
4W6zyti2Kj5unOj27FIHoBEZUVw8+yRovwYKzQmSRNkGuptxqPK44FkIWfsBTczbVV8exP8prW4s
eSvimxvLsjyXBSXqyp2Lvruaqr+Vid4SUdW6exAhRo6z3VqXTA5U26Pye84/w1Wt5SXP1Z5yAGHy
XsM3hPfdS/cpjlI8CuLJO6g/xvmkrhmiGrYbVA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2960)
`protect data_block
k9CyxsRi6XOkh6HjYiYS8Qcg2N8YD7gHvfywRlywH35tPBZdaYZHyXrh6cS6URLtsh3mZ6cTo3qB
OfoLDwMqYY/ooGlLamgihiL9GRJ3axvD8NraB5WXgy/h8fPT8q3QImtxyw1eTS4vibAeHpjp2xUr
63aIsVKKW9Nn6DeYvXYLQF4P3iJdb3zBYwNOulT9PTH/JFjv/vebaXHApXuM2xIQ3ysc9eN+0SGK
0UiSYelEeI853ct4eu0DaW9TpNR0mEWAIcb2tMEswUj1ZNally2gLzMrGrnA++S8hOjTRl1I8uOw
n4AKAz0Jac9fC1s59K9y0KwZlsM8iwYWAYXvBbwy59co51GCiJKaI1xQdtN6OHspKNbmY294cbC+
qwElNAXgL1jvFWKZgtgJVFp/EQLoETy4mEeP09zpwuVwYzmy47FbQfwHQqc7Lv+55zJd9uBZRebE
ikyKN8kDvzMdSyFV8iNhXRUBnpWl/btyZi4mordS7xHDQwmTiXbUyTRgFx/n2YumiWwsi0USa+vE
8Ax0tTPoe6E5eFdkIk6DthuQgiqVK05ltV379sPEmjOklchFixKv8lLYKlbxmcgnBR5dC3mSNzJ+
udrKHkukRqezf1aEEIdyYdG7H9lbwqImaLcwe5vkmiUzogG3eOKhEsH0z8nD9kQQ7fu+TU0iLaB6
3IKmnki4NY008YtpEturE/qGvZJ5Jb+PL1xFVAkGbhymcW7xb6n7GeoqWZN4qoo2YlpfiMUtnEEC
Mi6jViq/6kbXsYmio0lU5LBi7hbk/siEsMOaQg6kVjF8sbdaeDk6EZAsgNSn+s4jG0jTLPMEj/AG
b9RnhFBrch59UxPcmoT5BUERmT0OwksXja3wkkSt23gF8tUsvGuDvdGIyYyWDR3j/ilJDoOiATMD
Kk2bQqIyf1RaVkzIM+JYf1IT8dXUJPyQNkowLqoCxeGpXNs/wbhpOJBQAAEyKjQyMt3AKz5ePlc6
6M0ZoG+zV3pvoBkDI74jkBytALU+NKE7eyWrIyFxf+AKWCjMdVarvYnFVw4Z0HpFm8tSZ+eCVdiH
dNHZ0cYJ905QIoXjp/XEr2d0V68gzXQ/yNLjpL+66DtHs9JW+4S1TRlSmbApMC7BHRRsBwTC7Qin
A9wgI0mXu0rc7v1VnzGIo29vulRXU7onhWxhUJqfC2uZvqCdJPMNYalwcPVcWEiLuvcxsl4Qwq+g
X7ZMbiXaZZm0oHw3MGr5d8r6nMV//p3THyMXCdNDdyywtMQcOcLrncwxAtzc7DKOWdYbt4XYMU3p
g/vrF9wLgn9ai8Lw3lZvrNyHY503e8maWYjy3TUUm5ox+erOGxWHLfafUftnthqJnT0j8Ushaa3t
b2Dk7rc3afJ2MNAE0zq+Rpqmkj8GHmUBXS6ANRCQpPZ45qRpDfjvH7jCjJIieyT5a9kCe7heM4Om
VPlCUONmNETi5lTF+byawqD1sqhhm6hbpIcJK3eLVhfjquyItOd+aFteJYBKMwm76xKxBJCvso1S
MCBE2+coWi46kfjGGyAeonDan4+5u+/2v1gdbmNDa3CAXSVe+JkdSRisAp2q4j33cMHCu0+F5/NL
YRjRfaAZHkhmTg7O16A6UyAuenbp2okFTi2zR/Zv76KHMeQwcqk7xonQ503AHz9D87MsZv4ljW97
7SrdWzAgYD8+bSiKh0GDer1I21OSa0zK3DYUasVAvkya5HEGuhUQEzmjZLBnDmmHws8eZiPKNSap
zSgaCxLQtM420YhZjsb6SYFu2BRAJtNPbqjMPSLmyFjlrVDPYerlLpk/DPOT6tivK+Jy2aX3FZEy
l+2Z+9Gv6xnwZGei2Yy14JApqcNLbNrliK5iwpja9ZnRnl5YYf9ddtHqmJWUCI9MYdMaeAxrDaXO
QrRY62SEgV235p+8u3QYXMswPevRQrgwmkDemB798YCZW4QSfctW2p5+ocinSb6Z1oHVZyFystHi
dZd4Hh30QvSwfnv1yIt6Bwq1NBUdsW8duZmXFJfxR+BaoD4hcKnLeEEu1+5BQkNKR3nBTo6t1eql
oekKnYxEdGfZmKJrs98fMPAfCfHsTEtIYnB8HUu4wd+kgAPBoa0IiajSbHd7cevvnF46NSdbdy05
oRNFaV6beKJ3qnHsSLlipP/PbA3vyMk931WXPXte82mF/QIIOPAGzk2DFSn0DpQkQml/jAUnheeT
sTUlBGb7ytzAj+OQaUDFpgrBOofwTJSUAGhST+AEFrFzrq34CoCPvM/xSRQgtyZooAs8UjqyU4vu
dW8C4HMHO+ab5gVbAWvt+WnGe3QTmIrr/i5wq6k/NVt5OgkaJ8QKv7tG974PGvU2ttFeJvfOi2hN
JA/ceJ8NGhdPtzfpqWhHxKbHo26EiLolt3ANU8sNirUnXmbv/PPz6d2/O3l17vBzw4jroRPHKPv4
03rcFh8H886AAS33s0Eog2X/jM70qwLiiinkeZvQlO2fF+7PblGtp95NKLM7RIf1nwgs+HejaDCJ
RnCeCVqzzLguw/7kOBETVb4CsfnkPkJK1faqSSj81k51XFVoefYrJChSo/5FRyryYcsa7RfocB9X
Nm2CNouGsgp2ZXAI/mjhHBZnDBVXL1k0tYom5kc4DBiK2epF5o6O+CUf9Jv6unoagDU2FoNz/Y+Y
fQwljL7iZmGfnv3STk9dnxw5wHO9OPxgeDVAcL4EscftM7pGMFkUtUF3wgIIovbGEhjqgd0v0vbm
xcxsXmjwaKXQHnEhKLBwCriAo9y1GLWGiN6RV2FE8GeH75BRa0jdrHEDQm6v5P0ST/xALj2kGXCc
BQpBhsdtOkSupIEszLrrt+GKgHLDfvvZLOARiy/77VpdJaHQIHvhJl8Lw9Teqzx9DrnrQmvZlHtO
O4kdPRIsKe7Qul507kRs1LsLCNS3DMToCsbEoZC9JqGi0L3VaYcWozEpRNRdy0EPvH+HaH5CGWLe
SC0i4+NpQs162byp6C5CrVB4oQn4BNyKhEZfpYy2wDjgyJSBYwlSVb36BcXKbokt8O8n463EMADK
YCBlywnfqaVvIps28RBg+Xm+u/tDTOE/Jk3Yl2NSNunJeBhWvcmZ/6gEE6+NH9i45DpCImW94Kq9
e4JvdvWzhjyEXCjXdEeDlC7NlXC9qEXTKK3fXkIisuZGrdLA+GGGQhLXm4L210jLwDigGf0x6tjH
NcyW+1wFAUsZFVCH8hhPGkOnxT6BAirlZ7fKO6M2v8RAFCnY98/3DqqoiDhNHUUvuqqzpvNs6qso
swsZLNgherbAEO2b1saqDDtbUZ5l1ydNbxwnozVmg6zR/r1WB7JHNjoFlOkA4/TZW9pyvhi8QFEP
D0AXPKW0wYMkhHR4zCLEKNj1G1eniAGT+xQjzvu/0Kt8Z0fBdBM/gp449A7tC3ht55dXZwKVbO1L
pd/Iz+pMkzLCAfW58+iHTbEfxUB3DsZ8XjYyzLxQHOCYmI+y2NDfBoM9xheZ4k+Jkyi6+uMPeMWv
v4OUZmzRaSfeM8lCdrCXooIq9/dWFbrAfhwNAOSbsFol4RbCnKe+x/VMEIki1EMJTjEottZgIVFp
c6mYEdp4UFN6LI9eJZnZGBzdDWH5y3zrmocz4qpH/1rzRvpH8XOt6j5wu3t/Ua9v1kfrvYmmJ+GD
LaesooBEBHGk5wTKHquBF6o0U4T61OstuUCpO6+EI36k3yjz/2z8tUjhU7BuCxWNABeSDvYTcNiZ
uShj5UZ8ES0QLl7zg0DvebOUIs+Eh8TMgoRZa22jmEze1L7DRX8/lwowExIYxvCpb1uXVhnXTsZY
sYYrlbywkiLyg0dEHlp7ymd3sQHIh8l2szUMEQn9NouMVQht8tCndEVlPABYqobnF7zYOPxCTbBB
gaPs/fpCGUDDITTkDQ8fSxwzHuJKjfyWnPQMw6/6Rkl1vKwkL+a6TSKUoSJYuoJ40QisNc0=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_axibusdomain_axi_lite_interface is
  port (
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    i : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    \slv_reg_array_reg[0][0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[3][15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r3_dina : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg_array_reg[5][0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[6][10]\ : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    r9_douta : in STD_LOGIC_VECTOR ( 31 downto 0 );
    q : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_axibusdomain_axi_lite_interface : entity is "axibusdomain_axi_lite_interface";
end design_1_ip_scope_0_0_axibusdomain_axi_lite_interface;

architecture STRUCTURE of design_1_ip_scope_0_0_axibusdomain_axi_lite_interface is
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^axibusdomain_s_axi_arready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_awready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_bvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_wready\ : STD_LOGIC;
begin
  axibusdomain_s_axi_arready <= \^axibusdomain_s_axi_arready\;
  axibusdomain_s_axi_awready <= \^axibusdomain_s_axi_awready\;
  axibusdomain_s_axi_bvalid <= \^axibusdomain_s_axi_bvalid\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  axibusdomain_s_axi_wready <= \^axibusdomain_s_axi_wready\;
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7444444444444444"
    )
        port map (
      I0 => axibusdomain_s_axi_bready,
      I1 => \^axibusdomain_s_axi_bvalid\,
      I2 => axibusdomain_s_axi_wvalid,
      I3 => axibusdomain_s_axi_awvalid,
      I4 => \^axibusdomain_s_axi_wready\,
      I5 => \^axibusdomain_s_axi_awready\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0F88"
    )
        port map (
      I0 => \^axibusdomain_s_axi_arready\,
      I1 => axibusdomain_s_axi_arvalid,
      I2 => axibusdomain_s_axi_rready,
      I3 => \^axibusdomain_s_axi_rvalid\,
      O => axi_rvalid_i_1_n_0
    );
inst: entity work.design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog
     port map (
      axi_arready_reg_0 => \^axibusdomain_s_axi_arready\,
      axi_awready_reg_0 => \^axibusdomain_s_axi_awready\,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      axi_wready_reg_0 => \^axibusdomain_s_axi_wready\,
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bvalid => \^axibusdomain_s_axi_bvalid\,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rvalid => \^axibusdomain_s_axi_rvalid\,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => d(0),
      i(10 downto 0) => i(10 downto 0),
      q(0) => q(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      \slv_reg_array_reg[0][0]_0\(0) => \slv_reg_array_reg[0][0]\(0),
      \slv_reg_array_reg[3][15]_0\(15 downto 0) => \slv_reg_array_reg[3][15]\(15 downto 0),
      \slv_reg_array_reg[5][0]_0\(0) => \slv_reg_array_reg[5][0]\(0),
      \slv_reg_array_reg[6][10]_0\(10 downto 0) => \slv_reg_array_reg[6][10]\(10 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller is
  port (
    \full_i_5_24_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    register_q_net : in STD_LOGIC;
    \state_4_23_reg[1]_1\ : in STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller : entity is "ip_scope_leveltriggerfifocontroller";
end design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller is
begin
mcode: entity work.design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      \full_i_5_24_reg[0]_0\(0) => \full_i_5_24_reg[0]\(0),
      register4_q_net(0) => register4_q_net(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk,
      \state_4_23_reg[1]_0\(3 downto 0) => \state_4_23_reg[1]\(3 downto 0),
      \state_4_23_reg[1]_1\(3 downto 0) => \state_4_23_reg[1]_0\(3 downto 0),
      \state_4_23_reg[1]_2\ => \state_4_23_reg[1]_1\,
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_risingedgetrigger is
  port (
    \ff_2_17_reg[0]\ : out STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_risingedgetrigger : entity is "ip_scope_risingedgetrigger";
end design_1_ip_scope_0_0_ip_scope_risingedgetrigger;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_risingedgetrigger is
begin
mcode: entity work.design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a
     port map (
      \ff_2_17_reg[0]_0\ => \ff_2_17_reg[0]\,
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_xil_defaultlib_synth_reg is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_xil_defaultlib_synth_reg : entity is "xil_defaultlib_synth_reg";
end design_1_ip_scope_0_0_xil_defaultlib_synth_reg;

architecture STRUCTURE of design_1_ip_scope_0_0_xil_defaultlib_synth_reg is
begin
\partial_one.last_srlc33e\: entity work.design_1_ip_scope_0_0_xil_defaultlib_srlc33e
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2_0\(0) => \reg_array[0].fde_used.u2\(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
  port (
    d1_net : out STD_LOGIC;
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net => d1_net,
      r5_enable(0) => r5_enable(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
  port (
    d2_net : out STD_LOGIC;
    d1_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\
     port map (
      d1_net => d1_net,
      d2_net => d2_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\ is
  port (
    d3_net : out STD_LOGIC;
    d2_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\
     port map (
      d2_net => d2_net,
      d3_net => d3_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\
     port map (
      d3_net => d3_net,
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\
     port map (
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp_0\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(0) => \fd_prim_array[15].bit_is_0.fdre_comp\(0),
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp_0\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(0) => \fd_prim_array[15].bit_is_0.fdre_comp\(0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\
     port map (
      d1_net(0) => d1_net(0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d3_net(0) => d3_net(0),
      r8_full(0) => r8_full(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r7_clear(0) => r7_clear(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => o(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_prim_width is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_prim_width : entity is "blk_mem_gen_prim_width";
end design_1_ip_scope_0_0_blk_mem_gen_prim_width;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_prim_width is
begin
\prim_init.ram\: entity work.design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(17 downto 0) => dina(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\ : entity is "blk_mem_gen_prim_width";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\ is
begin
\prim_init.ram\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(13 downto 0) => dina(13 downto 0),
      douta(13 downto 0) => douta(13 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\ : entity is "blk_mem_gen_prim_width";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\ is
begin
\prim_init.ram\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(17 downto 0) => dina(17 downto 0),
      dinb(17 downto 0) => dinb(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      doutb(17 downto 0) => doutb(17 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\ : entity is "blk_mem_gen_prim_width";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\ is
begin
\prim_init.ram\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(13 downto 0) => dina(13 downto 0),
      dinb(13 downto 0) => dinb(13 downto 0),
      douta(13 downto 0) => douta(13 downto 0),
      doutb(13 downto 0) => doutb(13 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Z2O3JxaV1V8zF9ehcfCuGOsd5T1KuAOPUPIrWap14Tq2G3SHruCflKb8o872V587hSAAisEdGw/7
p8asJKC1diXbMdmJdMz3tcSvI17eN81WLQoTgRM+BVp+nV2SgRbpDYKhbMdYAuYLaSK/eMkWEVl5
ZVT9594Mwn/Cmmlp/tfnpawPSUogdWS8kUuwn2ndZhvHZo8hz56QnGh9ZBdYlD/rwDmopXC2TTEV
y9aTOhVK9qdyby5Z8De0dPl2npyzExpwWKy3o0ciqmi6W3U3UkJhqfQBasNA51PjIMnnNLv8Ax5o
8ez/ltPrkNRBbH0cNjRcrjCErZKdshuebt67gA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3L4j3EKlbobjRD7zTLKK1goA3TOhvXWfb2pwC2yW8mKfbwP7NEAJR7Qzfd8HGEhAADqDOqzrvPcy
nvvuNg9gLzGVQDfAq5G0j++W3cGqJD7qKmOZD3OGe5nlJLBSqmkWvidHzPgfZ9aqa10UHlahb6Js
jOlmcJ0hiqH9isV9PU/Gee4qShQhnS1H2KfbuHf2rVViSeJVF0uh0tT4KEVUng8+MAk7KtUE5UNh
ORmihEU7ctbupzOlDa2W9kvu/OqfuxCjjX5+RyRYxSJMC9Pc0f1migjZrHl7x7EF179s0pimptjC
2LE7B/HSALdohuIT31VfB8yHlbb2V/1JBHsK1A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10752)
`protect data_block
/5JqeQL8tovfcOv8LePiEOPxJc6VwM/ZNJOGejoQtH9JuVEge7PC/f8YpnaKSgEX/K7XP6c8VfKO
3eQ2Famg4oKiYkN4h6fNqMH2blCTLUE4Q8SBFXTtL/TQhhOivs73b0RFqboOu6/ZL53lDEH+edYH
hnsG7zHuwsXuXZcXe6/3r2I9Ruf9us+smMB3txz41JLabEWY3sKFaRu0CqzRRaZMMOFBr9vzhaIp
tENdWJCUSXJnU0fVp0QWgdAXQDJhVZZJJE443SSPMAxKNpNEX8mLxtykNoF2ZKL+go3uNBz0EA8C
K3eA8kd6kdj2iXxkAxAyqPBzCTrAQkoqnkItjiRUemxpgbkXJ5r/aEgXyEsX5btEwCMOxmi0OufZ
3+QUJ3y8jeh5CMu9YAPC/UF+r47vtNuCxWI09wcksnirrJS8qTqA1nLw/Y9NjPVK7jMb1OHjDpXw
JaQkIGeOaoToERZvi+S5Vb+4Mxi+iCc3a5vqUngjXI8xYuOAWN2fYGOWrOVeeAj9YbN9uGzIqcMq
kI9SD/Cn2f/lVbf72B96cfVN5fZ/F+fHB5BqUg629mzmH8B9K99UflmFR6++4azeB9F+f4CcCuty
Ry5nPybFK0uEByOpQWX4Hke2KLCDQuLW2bZVgUmbFjZCN9JJmgCeG5P54a14X3boh3ELnAnyuPGJ
yy7Y8YlwRABoVks4N/xW8jY26l4A7mp/j9C8JTRO+COuJVAyrhiV5w2jFvTCRcRDwG87JfD3wGvx
jl7bkcjOTm5peNjZO/XHyRCv88p8kA64QrnLzdGa7Zl0ucrzPQ6j7Wuq1L9QzRibiib7Si7hZyNZ
eJwo7h8vP12xcktwwr3p5gMmzW0/eBKFTRRQsWH8K52/NMHRGSTJ9Q+Rti3oSOQDJKfZ3XJT5ca/
Ix70CSd0wDiO6Nz8ZtqZQnGFcSsyorJB0xLanV9UwB3gfvqlaiun36q4cI38cOhIdwj08KqhKP+z
rcGVU04t17GjAQcoPN0qH8+ZE6ig4V2hNrCURFJc/hZWDYvhtzlCQf7smZDkLs2Hm0ziifOCi/cw
Ka9so3Slu83g6Z3soIvnk9BPVseMF6iF6kfW93FycrMddWr0ZefkukTX7tXdIwyc4+INQmv88OOF
iytkV9Hanu76hdYu5RUy8y3jE4s2IZhObR/+E49y0ZJUCpNqOQgKmb7iSZva3oiFLrXnTULTiMf8
ogcHOv3263f8BhNNPldh3GHGh7d5cZZjyZSz19ceTbiIzolOhGkIw/T/STnhEZmqKtYHe4iHhr8x
Fq4Xpgr3AhgXM7JMwFus+Cz/a90vBgRE8XkkBcdSA3wXkPNKBlSQRPkP7GvP05UvXiD1WsJlIOpQ
L+ksTcuQNFTvnY2erKhkjNW20h20Aib8tvipZed0ljYbXRIqWtcBRKrMf9RaTCm9hlKLiSDmGs1X
0zmkGysklVtkaafVa+FqkNOQOUWzqk4ECsS58ABcxun+gLCeyW6k5zn6IvtJaMIAH3Xpgqx+DhxF
pm89WCmoonWvruXRKmK5Owek33J3VcSvu2ODLt0ij4+docHl5ezuEwYO8j56AA6+49ZzwV4YHQyB
qeb8ePNaxOlcE1LoKH5wqCP8N348DBGT8j95GcgyHVSqYOs9sCXn1+luyWytQ3HYTywbwZ/oD/Fm
CkC1Tdxffttosd3Ks5tIOnq43ffBuCDI2fmnSO9jDWDcn6abYxLy9dsXANiXbWaBxqCzY5ycwPdi
xvGL3vv7jwfKcF7iqwvcAK5MK9Rx5LciR5FwdrhDycsKqTwu0HBT0plIYPpt1jeOmj2lQyNfyOmG
eW5OTXOjyQGvo1TtA1ZCwbJIzSv2SV8xnG3rNRTw9aro4ExNiKx4Aoa+82wUVSw91wVgE5ERLx6V
XBTkmnGK2nA6xbaC9Yvv0PluWi+qh/BhfcrTGRtYJinepGnoIn6dICTXijiOkN58ViK4aUwIcqCe
rDFg06O2tvyGlpg+K9zWx5DsBCqaRtZOjuq8lgT5fk14RiJx1vFyW8Gqo9lUv+gzj/wXEdMSHOR8
KsLkeZTPQeirXysjNsGeKf/9Y1c/VQdGP2upYAumSrbOtHwaEqLY99PnAg7kDbSHpE2FFXUCJ7US
QuEQHgFBeQvuresKQAhfuLvW+6VDwByYFTjMGnQ4eCk8ntfhPxc0x1wZ4FBCGibp+4xsvyUeWRVl
Nyg32FR0iRJeQ1S0oOfy4t4kdc9+df0GEWZ3sw7crkcF/ZAJ2WTj2Vz84+JL8a1X2Vn0vmf8+qvq
cUzKmJYKg2EMaLeJq1pfytyz14RoPolTMULS4auc8pCug7BFUiYnqcbhQNXXlQXJEjaUWKj/rA7w
GwDZxmw7FxEgXTilLmJsTBVWjuSBT6zbXaFxa5SVpAYwK3PCak9/+xR2HTPnyEG5mSi3URAwMdcT
ZwrNshA6luzIOmz2Z9e23KpEcn16iyQevHqpet27ajevJoMum1cjlVxGpggLdGBtwMegOzdcatuf
T9fY3J9iI2fMKAeSyuBx3EhGTl1dMckhIJmPwU8gzv4wHTrSrSlTlDuTU0ZMSJD//gY3dHMUR7QA
9NrO46OYNwrmA+Y1NsZZOx8oS9fHWqLjtsllYp0JasHqKA/g8R0IlriZgcjTj5vlSC9hgVqLLgGF
tbhnDj1w8D1rULvLh2LxtoyUmT+bqueNR/G/FM1v3BFlcHbobbEbFNyrrqPhFBv6/m/Xr+FGQrQt
/NRG7IlosjR9w0Z8/rSHyKMXfPUnj6RFtSU8+0Ls2oF8r1t8BQtkxT/Gna5kXBDG1CFOpEPfyl4g
KPqa6AcZwCjVBYkw/keRoBpMQ+jekMeK2SJ/0pJdaO3azaht9abKAPnvXH18laBhxSWbqKjPk7wq
E1kBceKamRLZm18tuA3wy2j6VddasDlDgI9uve41cLj70vxpCqI9Cc8QNgUTJtIKRZfhSdAfg68M
O0n6/bVJopuY/eCz6Y0OB8UHTC/0ksX/6Mq00xgJIsYvnXTHULBehsp4MZFOsDNt9iwkFCfEJjlh
g7RH78XTcyY9FuUce99cWZRWqGVYgqrNaT03QT+/cg2whWKWheZHTeR9fiCztM7yoL1VxtMSUnxW
MHaDO2S4YIbehpW5Omwsp89C1pcS3P6OSqVdEU8Tk/TUTF+WE3ODeU8p/PC2Qn4g3YH036AS4ZbA
2UrocU6EHBnho8Q95ScncsfXxSKYucewoxMIoBGqJZHE2HRWdH4VNVJfF2wB0tPlBQ8HVyf6Z9jJ
koQkVNDyZZoLM61P/8GWOmWRaWj7iusddxXqm/cAljL8D3dV4RMxRU6tlBM4rEdP2ojpWiJXwXqH
9MC4IPpCKLmnJL4LzHxZ7x4OE8N2tvKmEvfT/0VUvnMz3mfPkNwFlOG2qAMPK/cdC8W6mGSOXrKb
gybJL9nLqB8K1xjhisINFTkrMn8DfP+bmRf5UDCK3C+nO3c6u1LsP/J0mkKOWIt18GMFjNrWBHdF
I3YWFR4ecDTODgBKUSwGQUyM625Snsmy3tD6wpAjEbrhWjNwWRkkhjWlzwgRIpFOf2wneB2yxsBd
rzj4dYuWpeN1fhXZrpRdir97jdxNJdN0FRCtH7w3SjTxI0OJU6PTVXPckDkPVtYZ7eqWFzDYbmh3
k1L6kKy97smA60HTZ7WRnjFiStSDZZa8eqlVBP09kioth9xKFiTilV80HhYD2wcHGczBmrlhTO7R
I54XWBNIhPVArjzXGFnIJ7T8w769QlDrTL3vCmD+vvexcEkS3iyfrRlQPH2BR3OsL835p4GB+mmK
nac3OF+Hof7u8SWjdHZp05X3A099+0tzMyOtCU/zVkwrINDfjwsUWiZ/sGNBQEz2KN+2b3cgRX0e
hmU+YSScafNELcsehxDeQtQnBzgZv2/hPHFD+/4t0fpc4D/7MMblJn7tOkS9pnxNGSb6dIzvCYmh
iF0NcudIQgZkxT8gJiw+bWfC5g9NG7k8hdXLV9Y8rTnXijYm71eLZWFNOy2QKRy0kd+vatIuVcfE
cYYn+7Ie2F1pOwynXSvpE39WhZUD/GL2wsSYvC993SpehcNxm/gb1nZSkEUBsvvYRUVKOqAFmiVU
16SzPq8B/s7fXUtP6QTunbimD8g4ExOQTYnfX0xVwFh3zjMBJLYJjqdKexN3Z9/XgyfgQsopyEHQ
xrSDXRJ0thzCC1zzknzvLO3Mh+H7Uj11behGaWVphDZYMXMlnX1dPc3skR0sRMCx6VskU+Fzcq8u
aFBCkK63+U3QVcDiDoV1FSdarF0PX4x1EiDH6HdCuYU/QQfQPV2xCu8klNVRwbCe7EnGPYLwvqyE
QcGsuPldpY4lXy9t9TvZOcoSOgkmbfTWqZWoPJAvrJV8m+h/Y3uogiDQ5kNsQxXs15OjZPPLqrLz
5mIapdvwH17Hnq+Mz5EJHBFxl9OnqL4laqDaTk+o4UrwUkvPavqgfSJ0jR9eJb0neLfuDET2R1Yv
2RdTt5KZX51lq28kkYy5jOAWoO0NMo8BZ70j+qqhcnQny9G1MSj/YR69ds1R6duQgS/ht4FBhgbs
2CiBU+anGTQ5CY/rD5yL5oKYLmfBmdRCeRJMqDWoNSC+soPqM5ubsuSKdI3dielIRPNDc+HyAy1k
wyZTY9cQSaTf9w03pgsB+EbXn72ceHcaytdBpIPIPWCe0ovBmhv0tJIhoP6xMr5h6PU793Oo7EFt
wlF00auBqeiQm52ddHddIjfK66te+qYkgp+BdiEvJwi7Bt9a/dSFPUJzt7CEotTvscUPC13bIZDZ
U7EWjusz4Z6kXantYofWTzreuqNFoWAq3sf+t3QIzxywVxEoJCI7maXha8Ycy6oqh1gMAezsG7SD
gN/hh14Lf0jG/DndgBaIo7kvK0caFbBG9s9/PkwF47zHVs2My4FmSNdib2EfcSBFKnTEd1lAIaQI
wQecTrnDvRkm0Etq1kv4FJuPQMzm/4cM+Vl0Ft2V7BIo9UIJJWmFGuNzwNX7FZmyHcWhXO7C6dgp
H/RjLHs+7/fTYy4J1f6s5xlEP9rCxQFhsi8xgOWluHMDiVpXN0AdPz6uo9ZMxLxubkhvz1mwvxFc
mv+DvPamte6+TjMrdijmJfn2I4Pemom6WkYJH7JemIVk1+zAljZ9g9PXLbXdLfp3E/5BFjIMfR/Y
F4WIWQFE8bDcEe+P7DDA+heyCQego96uHZoEqxMUhW2JHM63XMnQCbflbnQM2jJZgMmf+nVd81T9
PtP/bRpgGaw52HihsKPydm9dCTtZ6o70vfpRjzXi6U+LKV08XDiOPkYC8WtIaYsNDE89s43gBpen
gY4ZIhWltknMCA7blML5ouAC3nlxAvwLx2nGqB6fJjp46y40TL6bR1rJx2uDfDaQqhX87KV1EsBV
zoOcxpAc1MMKQJUTXrtscROvXXrtGCJ/ES3DFJIoj9pLdkjR86QGtTFrw11aSW/kh42VwK7fzRq1
1N2gz4yoYuskclavsecd6N8mAmc9ydYpH7k7Bgi+E83d+ONnxHa3bE7nxiAp9UlRb3/rYTjZk3hL
62d2Y8jUU8m8j+gtzAakCGREY1abzT8ynheZYz65oGGurVfDUltI9oi01ex7tDbEi333ApA6hMJF
X0potykPMm9zKLKaWA7X/cnB7CjJodIRcPW1lKm9dclNI+rlzqU1P7pwKoWcloS56CAWlkbBFdvP
WY2aHQ3rBvJNyhGUor1nvzJtmP9Q/NswqUH73RmttwkGfLpGImALSttnjU0pOFi12X0GPW1Jxa6m
vAYztvwlxFx3koFz0MKtFgYszXhFoHPJ7IkL3Vz6sWSV/s1YRCim+IMpR1RTL7IF5CZ2p1VfHTj7
7j7Qt5rlkVRT7rSuOgjeHsSdhdmR96N2qxVT4/1vvWoXWghobBkNCQqoOg7TxZUhFvwS8ZDBw6Jq
dMsg86XZoyjWzxkKvbvHFq7dd65YrGlI5FHUSEB58eJelwlIIgnJnFWCPIgWX2G5uwJ9l1HMdRnf
/LCGth8A8y78EB2PAvliE3WnWTGbHuJJy/dsbgjOAhaq4MreOR2TiNhwmAKHd5i1nGYBx5+5sbPV
b2b396S8XMTJDiHYE3YAX+AmJsx6SMGZ3EELPdIXif8pp91LKYYeGzULJpJ+424D0vxo1Jj5eONU
zKcY2bF05CTAZUkWu6JtfvlJ6hQzzpYou3QpQ0mZ+7rc8mTMIH1U8TnA6fx38Djvo/9AIma/QjX2
6jJ6O3behVF40I0c5dr6gMUfxXHzpVoYGx46SX6r7gJckO5WBz2atrr5h425YTAJwza+OjCtmd0p
1lGy7OPLA3fmS0gibPwkhlbwJuQ8lE8Isto0fCQsYyy5Xy2aPqFz2fcmW4B9eX2Cj9QiDT3mJl/S
qIV6gpmeYRlOjod4asXWXVkfWUP8hXo7e8pzHKI7GWHJheaD+VlperT+LGqDxvywTjAoFLl0hDig
y4VYRnR6GRkFjigXyyyb3C27fT/6VnAjfQ+w9iNm0wBujRbtIMdTMJzK4EzuRaGYaRYZu5/bGV5l
qQZQCpv6nGK1dr2MMKrW+RLiyqAjPE7FVTg+vgl3liFGk95KqRtYHc++bXBaJQyNU9PrsufAruHt
rU1wXd+3Bt2Uixzmqje557U6Tjbfq+bqSF4CSWiptIiXUm1iuTOamW68b3ILrqrDmPbWi7E8Uvcn
O4OlRPBT1822TYP7xCn1bc1gPA2hJXxsxE7OqR+6m/JGzXuAQbS9I2e8rKB+JDSM3qyE+uC5VwCH
utjK3Mr5SleWmCpszk7Zo8G7KGf21O0+NDg0/FVr/Wf8uxQ+gJVjNk8+tHzsdID/nCaJkYUDEyR3
xVDRuYSGQoOtrDSNeuyTsmyTMtTDMHPFY2ZwlAwyVgVM6zX26+TozYopxG79tImUgpLapw5lRmCo
WDY6rhCa29FLoVGqVYP50EfjxUcF+JrJvHBj93VPXf6tBTx7wmM58tV2llnsuLrxWzgiXiQWs8fX
EBRgaoc8ntvxz1wkph+S8Fh72kekrdpNxW0XfGNEb5m+tNep53frxsi8WpcQJCsZhwsWTfvsnUUU
J1pg6g4F20RbwNbRHRUPZqythdHgnEzc4BAAaBTc/p56F1q0vmYsTvOS4FsQCzyPzB2RQOj0Li7k
E1G/Xexv0ckKLoYn6FCz89PFbJk2W0FVMICSG21PkTpNN3LDXFpiJegKrFrz2NOT5bdZ6VUBmdQr
EQw8Qf3SnGqSg4en+1QS0xxVnol/ZpUQ6KP5YKA+wk9lve+WmGa6O0hxM9ZTXG5uds/h5b6Y4qaV
9fh05MreqfHT0PvCb02mPXBSI0S/bNjtFe+6u1UQ/0PBzbCFB8DtfzpjT6hi6og0pAEO3OnOwzfs
orKMyHNmVxe0GcAsx9ZFxXKeAB1yBUq8diCII+QAH3HvCpNpEGcbhXeuwe0vffA7yKPU0qL/fdzY
ngs/5ty8HyEfMx30HqR5pye82dT+i7faAt0ty8BrrFTl3P5EMMeR+fHV7P3yE3SGkPxL1fjHx54H
VL2fV8lmtl1waRZ1YN8mOtGiAoBonZAy4ALAWubr18EulegtrIS9E2mF8F41rS8byGbp48P6JW1A
Try2chb3hAe2ZYbC6OGQoo/N8Cca+OZyy+I+qO8cfydTzpu6xjjs/0ExjP0+xxKhs3r5DpyHQGxU
BOBEgCw6B6KfS9PbWF40YtBRgSv+RBdI3mxq92NlXGDIpSyq/evnJidXkmXoVPaOAtcb9LgZ3/nl
f02iHyIiPwwocTsYthdgII9bh/ac0KXJRDYSRjj7TfQ67uoK9FRu/QSy6YRRxQ4d7nSSZwJUHLzC
ttqon+yyXAHUkYWtMRvn0xcDUTXJfJdzi/YqpaFMN68jTaqSuL8Pj3p45Q9Wr0QuLXudHsxtYHjn
ueAQ8PDnQ1vX72xlnmVTdzZqG2OLZgzjQHieiZ6rh55XTBUZa38pSGMK3ztpSAkJWlf4W94bV5Ks
GJRAWhoXxOuiLPBA6a9EcUJv/I78j0ITBdiiO6bEu+jub6Ih+3/fesChyO1uGRo8JvPwhKMx6OF9
uQwOkO6braSwfllp3en1lfICYGWqjmsD5EQcXdZKsLQqXfjdtmtq6JB3GLJdU0cqTYiXstOnV0gV
1yV2v3z+lMSMufOgQA8sv+J+hSALC2bF2mwR2Yy/RCYpRktg5EtKGOIHJ+wIxUIGY7EhKB6cnLVk
S2mgRlGFfCV0z7DRbQGvgw0EoNXE6mtBjgjkdxTNE4LtxiIPnPJ9gqlGu2YNEaHObENlrNXS9URO
DSKJE3fjWwqjwiypckoFCSrTv9wWmeEALoRpoQvSi+hjTlQMaiA/2XVmNJW443D6eF1C4yAuIxtI
ge+hP69QtlFNp4jviwbjcikPg1MGqA/E8s8Eki5wVuTNBWOdK97FhPlbzGvQ2lrzvy7nm5rCDAd1
41/ifW7/lWITLsj0lwmKcPt/cSNe5OZ4wcFvut2NRjIa+sg6UijxZ1OW+LaQo8WL5GBSrkbqTT2O
3mRueY3RPfvXGw2D40jLg2WdT//blQT/BsFYexUeMCz73W2voEeHIVOyUvzCt4zXmZdQE5WWCnVr
ciM7uHE4BrWi7MEvUwVJkvjDLBn21yx2qGXYRdBQPH3CR8Wd9Zdo83ONHDHYE9MqOrkI4hij0bah
41RbtA6phyQ4u3D/t+m7NXeWa8ED1sHvWg/8etKaoTQ2R5DTHPsJ4ZflYDwH+y/gH1kjK8A9e6Dh
nMNlym8sXOlPkWvSaspwC+Eqfnqh8gAyoWBsFu1YFJ9tPhnsHbis6e36IfFwXEDZA5h69uGANwug
8v5VobKxIXihMrN8Tum1TUIT59v56GEwhIc+uWrNTsrZGQQsODoseyjJGiUl5JAhQFm7RX+jVTEf
+ra2j2egXFBsrKRkb1klh5mDlt1ZslYdLKqPq1viSHu8fAqdYuo3ybxYetXytCIrRiM4kjIBC8EA
v37Idmr3vP+wV3t94u8x1Ir7sJX9ZM8PTiqlEJcB9wb0pL1S06yBO+uL2INYs1S3xYlEunvxhDxl
vnFMWRMq5PIDJ1C3/yJegb+xnO08Jvz8YdDdLcTtwcmLu6MKO4dzaP7szfbgEg8l8UyWr1JDeEpU
ocf+SVyoRRg1uK/+qIWf+AgTYSkBhhrAs/dGEHuggoPuZdMXopsl0wD+oMvD5WsA8ihoHowHUARE
FsiTAWR+Qqr9QBmPcEU91+Gv3C+h+bNKu2Kq/6npETMO/TNjIQcvzkrGY+j64T2wM+ChIRDt1oMR
LpQN/T/y2VQEDiXCrKvpnbnZuemxgUglnHMeUaFwvZy+xAxRLcywx29nxFKLGbLsWrcnQaJwFR7/
IE2mqJkOei5u5ItnPrZ9V3DNnV+uK7cHhTzty6F70KsYydjgG5KQ8qp7VW42cLrFfX9ekjiVHZHJ
afN2j/w/lsnu0ug8wXT+GzfpN9Io3/DPLYMfDTA14UuayE9zyMIVyxgagPvpwEtHCrELGsgt3Bd4
EyO5J/Rrb4azwecdofVuddkKYwE2o/qXMZqteDl/TvHat2Q6lsT3gyr2etE7cz7fkENjSRaCwLoY
S10XN4fDZPRYqmMy0fMDegoVW/aV+v2jEg4OmcDH4xZOYa6gaLlO6FLFlKTwzzKi3xpcVFLoJaW6
Cbu+ypclnXZ08+/5qzBbBw4SzGHfYnVG+R6ZW6JaOsNvh4yMDTYylBs08LdD6yX/HQlnrfGjIkbo
C1unIskK1qyVzi0wEAHzsXXzAM1YmCba2TsTuxDEiWG7fXXrvyYAddoEXX1oktBDAIP5jPD5nPjI
f4P6HLHgz1df38YU/8BbGfexUPAm4/Fa6nXOdd4kcJd7+kr/jVHpF2vgwrnNllRfViHRBJktBLMH
Ztpl/0e75BCAjGY0UI71wX4mhSHr8cmcjA8mxK4HeSBBWT6aymtoIUspulsavwfw4DreJ3WNZUEZ
HtPlk0dMCZRlT5SyU4WGk5VismTHvtFnknDGTYPdMkZB2XEuax/1vPiJP3bK0mFh6i5L8Fkcnrq1
A6ciqb6SO6pcpMW0Nkeei9lMq/YcvsiZTayitzyuB1xMyB542sRssMfoeGDLOAqMtS8bQuA5sIuY
MPkK6UDWlVhZkAKaZruy4oCoQP+I1HnrZ+8HYYxucQFZW2u/O1sgHu7Dambrl3OOm8E7kxBjuYSn
EnKLSIywEBaHQDwWNBgHaq0+MGY1hNJMFtkiMapIKUDhDEk75xoyiKXd7xvIpVS1ngzCM+1REHSx
2U3O3ZyFHSlW0OldMaDhomiNf9yomoW2hd5hCcUWbSpGls4nGLMgnXQaG1xgSjCccbT9ZaGkgCtf
KC55vILAVvMeetB+BqgZwyBGr8HCgw/7HkOS95iT7QWTVQYLPmik0up/PSTNfwhuvARunwKeSSDW
9ykykfPccYcf5F7d7H0Siy7LNZlWEjr6u68837zMQhSmWD7CIN38+1fkC4il93unRs76W0AKiUW8
Mf1hobTRrFT5Knox4ypt9wrouLLmUj4n3PgVzrnnaQQ7u2p+Oj9Amam4qUxFleK3bJL71TDCTHcc
NYgzsgjlU4hASveZJqZ9i9LvDDcPAcdAcFaZTXNItrmHZNxW/7rAh4A+z4sn3/rNboEHugkJYxZ/
Q9m/LjpLuNzfpd+kbrrcvPlLQfh2cVPJV9MjP1RATI4lv+45HCDwnoUKweuZWOiD1Wsp0oy2F0HV
sUKvxVJ89VXnBLyaxntvEBtpHB7wi8n4RByhcjNQ7IT8Ait6IgeU+SIIysHN/vk6k/6x0lrk0Zln
QnVOWAQVs6ClB0ZV5+1jXmImJnrzfCsCAeSoX2uUs9+8ltSEDH9nbSN1iJunDQRAXiFFFz08qRoh
H3kZncGhgMpRNlc0O5eUv6j3DUFdVszXYdn+BW102Wr1/1OjZus2S9/S/MR3PJF7L8VlrqZJjLqr
lnZ2ZuDqd1wyVrpcDI9roh/bhgDcIRjX/xDNpvNxspq6lV4BcMhpj5SQ9V+rpNOkpxmjzkEuWW0T
23TSeERd653kkguVUCg1UgLXSCtFcTn2qJldCKKssiy16LlPyPgitS9ESYOa/Ixa5v+nBC4gecft
h3mB6Up9i0Lesni08AjmliRC06YTYdpEf0cR7ug7S92xcJvmNzu6uJe4/yeeWVI+D35M1d7daOYp
t1nKJB1GSHJwrPMQfdLj8hkPwizPgnxHE8fNMLt9PUg4gu3MANDK/jI01qRlmxqcSq28t7JnYM7j
mlOtJYMuM3ocgbRGE/s5DI5T3bpXnipHzJccQ+otDTQArF7GGKdwMYc++EY+odEz6Fz4A8Gfu32P
sRow4wwgxf96VRwLv9Ceb4kvCK5HcYLejv4sPf2WucNglU9pGKslRDnrpa0NOQu4GqHuycgUq7cs
/p5qqEWfxn5iMbMl2lNP69O0o1ugpPJpF6xyRm8x/U1X6YckjqG/AELmW0edRQADHwtxvO0ASequ
IEvsJzuE50AiegVjZQqhkov6lxyLB7uWUbr4XXlI2GVzHw07KCYX3yH8WtG51es11+P3BrMZOB7w
SJmGx5DLIi9+GVJa3fo0bz4vARnWs1Lzq61ATYemW8ZDwzVRIG89LnpaZ5bsX7U/Djg/KnM4okRk
yMz3S0O2Y7Fvu0upL0XRoVKcwkAREy10z+87xogW7o4P08Ddn0ZLNmfC3BE0odn4BVZWMQVzO85d
DFSfHv56eGh4gyWQuwShhWRBE4x261QQJ9/9Jqo4jvBo0P37oVrdhshVuIOGmETSwPvhS9oV+Q1V
cl/7HLQnUOsShgYbrOdyp+olT1Z+zkJ+BSinmSLDWX8vhEblkRqLCCTtzCu3wVuaOi1htam8L9HI
FzQ4gV6/RGUznKJ0kRmKQJyOTuFVcBDVrCZH1XYWSE5No6YXb0T0JJocx4Q46DTmUSNPKrr9ZCDo
Kx6y4k7ty98Ykqa+S6v9b6U0Xi1AHfxhmKtwsyxevecem0E5XqVOr+s0RTdyjgjibKF+7BX6mH6X
hXNal4teiAs0Lka0r31CqtzO6S9qn+6j0j9rihQgFDmOER0wa6jIX9jBFRZfEA4FqD08qhPEh+/y
1cQD26YNbOL4WSj1pWq7klB3xbfoCgA+Uc3ntFbFwPXc/Jv9tHZ4kPg+vPmfwVCSIXPu0xKuy3Zl
iK22lJ+G6JeLhDExsJdAwaoGMbyamAvUWKdJnWCXvdZiJQ3+JKiNQEYGf0D5sZflm5knVtLBGNNY
Y9bQwinGy3wfo06rc33Hb/7m0j+M7NIYnj3Yj8rOgUYZX5RJeZgH+eyiazzle5Ja/0b5sBJv4OAq
L7o6hQccblx3re/xiLl0PDbn7SYTKkhFC3f78hDaNWKW1L26yb8xXyrAiAX3VWhXMtamk/tmeD6D
I9EyHEm/tASUwZSvafg7Pqt9jy3emAX80GZU2I1N6uQ4GAJwhkZOoS4uuTT4XP+rxMMx7IimkTng
7olJtaUkoXz6ZDaxH0DSPk9rsTf2VjWtakWxMxKLktG64H2qQ5zrncETLpSf0/qLh8DVQJqi+Njl
FAJlktHHNSNchCyhb/fvd3GjKaU03qG5sdq1M6OyIR1iyWNQx7jZUwGqDeZX8GtCVakeS0kMBdV4
AlSb5+S4XpyEgDVFvfmGTBtJR4+Y0moIMTc19/zGqzmjsuCNqmHm0VeDQtEtssLEHNfs0U9KRP3L
4cMGtxzXsjofbK+wjQmRhXeLJMkwKiYC4I1WqNDl6sRBWIwS1gGOq747+a8T58zQFKJLrCiXNbQs
OsnlAhIbiClf1TfufuET6owahOm2x7IN0bYBKN42K0fqqSD0vGAWQ/pV7f/kMVX/K9/ffXGAldNq
ZaHViH1prsOMaeOLEYtMQiW10hRIN15DhVStWSp99w9qKBF8PdpFScJOklBgRuId4UaDMqDy4Ffq
/R/90u4rdGjQ/14bWua/6jEQF/Mi1cW99+u3+zezDtAN82Cd5O1xfesC3KI9mTL2F/3SP1wyyNQA
cbClJNTAYznviZVmmhq7IhKZsDeaeP3J3F5qr/PBjq+EZDQpQaXFds+bdjG5pftkfDrmTcRNbZH3
IDhOVsX5FuLBulOyETz4xxsVC5B6azxUMb6R8Z4/epPHjmiyH+qxDQXaopnhMRf6+CgekzLkiQT2
76js6W64n9jK1ul4jnNdDxeR7LUd9K7jip7v4RHZr1yOOJBP5DFSYjqMN55BGc+GPj7i4eVjUGh0
OU+I5UrDr1dIEzCbRafBfrT+s3dsNS/tkzgwTFeOHGFcXZNBfYu06BAVDvWNPgca6mHTJoyNSIZY
T+ZVpXDUskkEYTnKVLzaIDZwVTC+A5jEitSnuXjmRMkB5zXh7idcbkIAAJTlokqkPyj5qMgVUFmB
LcdpF8xsPhsGWViAhsPCsyh50prR2rZYvfDpHrT6IP8+TOFh6zX4OzmHaTbmcnaAWC2XxJmVAgPf
Sye19Be2UMM9wL8fU34VNZJ3QVXJGuFFP+Z3GalrL3tmqtzx8fuoUXVBU7yLzpazldt33g6ELRqd
3PRQrH09EseN/9RbCQOH9Fu0AnFhm6HyP9xnZuD4iKdebzpXK4lONySXQoNSiSk68KfU8+t8EgtK
GEkuaJ6gLO6TLbtkL+UajcBysCDh/0QwQG9HjnXTxbYfUyJk1TSuL70waBN0S9X6F9IAGiJaXpQE
smgI61+1oBo9xC8rNwH0JMGRnkpiR8B2NzcBenGBo6+dRvbRB4iarO1/KOqzYY47x1CkWcbxzkfx
3fQBCWMa5xvLH6u3kZAbD+/r77W3piXEvppfMzG/wuWG0DjfsfqMRXzdp5OdZqteyijr27OXf+6/
U/Kq7NCmp7FFJj1uo3okB1NlCJ01kTVRofpZiUFNn7SPT47MxDOK8vuB9ik2q9sGWBV5rI6ms2M9
TDlr1R81FqkAWdXz/jn8OFUuOlY9jH2JWoYTQlqnvzuZhMCBb9BbO5KO/LgKh+jVABGpnuIQTHG+
EhMFqPTxX3mkqrhEfwAtirzCOO9VmbdWsl8FBPnlkwg1M9NmoXEvHoXGgGLAwIqn2V3Erxqm/LAW
DT1OqyxPhGymRw8tSHy3log/5NHwGFUHOv+Z/Wr2pVwANBuaLVydTwtSgj7v2KWi65pUJcx4Yuip
Xb5hveXJi3YxNKUOdRkC6eEuf0LskyKLwb2elHkmRhXP93c5IDn0esZqSZfb5E5hJ87+cML6N/HZ
mtKq8FXTFq7AssQZWk25UJ7mKCzFDSkO9HVmqt1VnzVnczF5Y5i/jYfVnyZKwixZUAGIrgsKWTGP
Wg+fo9lCqKR6hIH9bF67aSlmY4ndEqceIq/jjN54m/ev+2/a
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xlAsynRegister is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xlAsynRegister : entity is "ip_scope_xlAsynRegister";
end design_1_ip_scope_0_0_ip_scope_xlAsynRegister;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xlAsynRegister is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0)
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\
     port map (
      i(15 downto 0) => d3_net(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\ : entity is "ip_scope_xlAsynRegister";
end \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\ is
  signal d1_net : STD_LOGIC;
  signal d2_net : STD_LOGIC;
  signal d3_net : STD_LOGIC;
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net => d1_net,
      r5_enable(0) => r5_enable(0)
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\
     port map (
      d1_net => d1_net,
      d2_net => d2_net,
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\
     port map (
      d2_net => d2_net,
      d3_net => d3_net,
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\
     port map (
      d3_net => d3_net,
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\ : entity is "ip_scope_xlAsynRegister";
end \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\ is
  signal d1_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 10 downto 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => d1_net(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0)
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\
     port map (
      i(10 downto 0) => d1_net(10 downto 0),
      o(10 downto 0) => d2_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\
     port map (
      i(10 downto 0) => d2_net(10 downto 0),
      o(10 downto 0) => d3_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\
     port map (
      i(10 downto 0) => d3_net(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\ : entity is "ip_scope_xlAsynRegister";
end \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\ is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r7_clear(0) => r7_clear(0)
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\ : entity is "ip_scope_xlAsynRegister";
end \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\ is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\
     port map (
      d1_net(0) => d1_net(0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0)
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0)
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d3_net(0) => d3_net(0),
      r8_full(0) => r8_full(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\ is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\ : entity is "ip_scope_xlconvert";
end \design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\ is
begin
\latency_test.reg\: entity work.design_1_ip_scope_0_0_xil_defaultlib_synth_reg
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2\(0) => \reg_array[0].fde_used.u2\(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xlregister is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xlregister : entity is "ip_scope_xlregister";
end design_1_ip_scope_0_0_ip_scope_xlregister;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xlregister is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => \fd_prim_array[15].bit_is_0.fdre_comp\(0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\
     port map (
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => \fd_prim_array[15].bit_is_0.fdre_comp\(0),
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_generic_cstr is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_generic_cstr : entity is "blk_mem_gen_generic_cstr";
end design_1_ip_scope_0_0_blk_mem_gen_generic_cstr;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_generic_cstr is
begin
\ramloop[0].ram.r\: entity work.design_1_ip_scope_0_0_blk_mem_gen_prim_width
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(17 downto 0) => dina(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
\ramloop[1].ram.r\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(13 downto 0) => dina(31 downto 18),
      douta(13 downto 0) => douta(31 downto 18),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\ : entity is "blk_mem_gen_generic_cstr";
end \design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\ is
begin
\ramloop[0].ram.r\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(17 downto 0) => dina(17 downto 0),
      dinb(17 downto 0) => dinb(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      doutb(17 downto 0) => doutb(17 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
\ramloop[1].ram.r\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(13 downto 0) => dina(31 downto 18),
      dinb(13 downto 0) => dinb(31 downto 18),
      douta(13 downto 0) => douta(31 downto 18),
      doutb(13 downto 0) => doutb(31 downto 18),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
S2DZDz0G2HhIjZmlE6YOlAF/0l5U03vnvuYB53oGfS4xmLDMMQd5gyjXOh4WqcPWXoF0RnerE+qK
n6sjVW6l7qOxWqRktCpBkAz6CMd//UvAgDgtcVqArTKr77OqfZrMPRTQ3Z/zkJVlpkLfRCYe3paR
2ztUfkFqX/Npt0+ycYxCowojAVzqjLGuIxe1aNYQO7dV9pe6RqJjc957pvKk4GHHGND614ArqGr2
hn+9JiT0lBsl5n2gS2sAIyNsPfnPxBJ14+ZtCncInYWNHi69nzNhzNgtgMhiTBvX5fjpbBkBlbZd
k6j0KAJ9dEUxuIYpG9NdQsvohTef09ih0wsfOA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Qn199viPEXIxd2vw9HgEFaZ/6zMKOD+Z7gTKMrwEtrYKndr67xSpSqSm1NCVoqgUrGD47+2CuRYW
yUaxYJdMFGXVkkng2mxydEliQyIoyd5NdWwOoQ+P489lARQqBiE146y7iJIE9m521Ys7zXjHO+xA
bqSefPA9Km98uGxmTKTllEOKGeV2YzIeEzExt+JwofgtEv7SKV9McXxGOTHF0hfIz3/k2zUs953O
bj9GxUILeXY5I8PlxAwWaWRDj2rxDYwY3QWm5ytZaLVV5qLPoAC2QKX0Te5WAmPW8Ti4hFMpGRIj
yxaDoS1veh7SbyTDdDFZOqfp2vAO2l2b0+a5eA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 976)
`protect data_block
/5JqeQL8tovfcOv8LePiEOPxJc6VwM/ZNJOGejoQtH9JuVEge7PC/f8YpnaKSgEX/K7XP6c8VfKO
3eQ2Famg4oKiYkN4h6fNqMH2blCTLUE4Q8SBFXTtL/TQhhOivs73b0RFqboOu6/ZL53lDEH+edYH
hnsG7zHuwsXuXZcXe6/3r2I9Ruf9us+smMB3txz4m9fMh7odcNHfMQahxrwIq2VFYvuOvb2lxE9k
7XIDqQeEb/VQVScPUYm75WUV29PlV2vOvL79/4gdT2WvsCmL4fGyCYmNzjJqpgabBio5pFFsohKr
6SBR/8p3XoLYJWRoSBD2z0bhdjU2oAm65kSrZY0DDkD30q1m+lRVlgbTTs1WgDRGYr8mXR5G+eCs
RXAx0o1IhZuG3eeNTtb/tJ4A6EFEXGUch2ZGsSxVOn7HchfsHH44f6/YMFtJWcvOzFvd3+/jLOLl
QNaQWhhdQ5BTzQUkEfUmZljnxTxgAwSbr3/vQc8WSS2jkhAtQNPLU0nX+VYu7RDYFGJBJAIpqLli
uKk8fb6BlNt2iEGavHppoe78wqpWYOuNCuEJ3PjyBO5Oyuy3kYE9SqXH0mUNBYDONaMTrcH1ROwg
uFJcN6TzFXVO0enf1hQlHYfhWTN7qGpeEfxFdbhuYQphcz+y5BtNHrZgG2ovd7I1Tjrr3PVKy4ys
ivhmwdC6wWnOlSmHzK1s+QYW5hGYcSQulGFRYdNrHEEAhIPy8FUvOhda8NIDPVFVK2zZK5l/eU4X
PcgdUdhHZ5rjP4/6TSGWtXT4RoZvDFiSLJcAcbue1YoZTIq0Fb98UdLz7t2d3URfoN/QfvMfTm1S
OQzHF3V8D3fY+bFGVHsGu2h5DAPhXIhBcBYWD3afo4BIlEuefbtnaLwWgLrkK3L5zXmKHklpoiBg
4BYqbn7ZQWJe1b5KOg5O17tp0rI7Vjo+Yn3pFqX0ekSDgoOoRM4hvuRWBrazN9y7bVl+2Q6VBfxn
WbBNxgMZXNrz1Mo0Db+5m4D0q9YXSXGoTb6mYizVnQnuSy9cdjQ5Fl0X/oq60hpnhcKOyQiG/LqQ
fJQmnA9Memhos58OO6v5MVUaqremzQV4ozsauF1X8K3DAYpEIjD8vPiBNx+suVSJ6MD/wC+YFl4M
4S2OXVSy88PneWmkYAdSTKSo+2kzhg9OFi989+xnrcTCyN07f0IIUw8699LARexP61XKAtuT548p
XZ62T6xMPyW1T0WaolDaIi5Juseqw72XDl8TjXjxjdl4Zl0MM9p9AFL6J1TYQ3IUZASxxgWd/LlA
UutUaiEpxQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_top is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_top : entity is "blk_mem_gen_top";
end design_1_ip_scope_0_0_blk_mem_gen_top;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_top is
begin
\valid.cstr\: entity work.design_1_ip_scope_0_0_blk_mem_gen_generic_cstr
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\ : entity is "blk_mem_gen_top";
end \design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\ is
begin
\valid.cstr\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Fvhnc2Y7iq1Zks4KlhBTeRoxFm7J7Vo5uboleM3VG0CRCh6VQpO6LZnckGoGF5h5riZgOsSNBlLJ
elD/+1wj/YRbig8xdZf/J0tlnegTgTGJw4hDbhW7V3C5pQTL4lxDj6GnnukLprZOsu43/cge3jWe
9xPidoGojM8qiOL5TpEhT+jHd8S3t0okm6CvI1Xg0uAmp7/t9+B/OD1UH7a9a5IP5GXpdauistqV
Lb9qoZNzOwA3311nj3E3Ib4FlJI/pLW+H5B4qCJmmnkdNi+bbMD7P5BZoJ71a75aus+ZCnVSbwoT
EycnasQx/foE/NenSqOlepfTTXlrer0ZQJLvzw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YvfZp7cwE3VzYPNORbJm5f8dIOkVGp2/msNfcUWNhyfbfX39u8KXXZIWuxD7A0RwuzgAq98na3k3
a0NnRnxMnsNMEHYonHwFJh8Dw28z8MV09tcjerqaefG95QyAFYjWLUBQsYUoMqQ0XcIDWYqwLwsZ
R9n1Aepu073nQ6SURiQ/NFIGaHgaENPjaKn7YP5PROCQB6s6VTQL2hFInBjlqtpvWtVGXYmasTJv
wfR58238lmkEJWivUUtulTbfVodgdXWl5zFHGrevuaUakoc/YG7/yClyMJh0pD8NONr079tEn/8Y
kY8LiCgNkrRkYf+zPHb8vRQh7Cn0H1uIgx+S8g==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 944)
`protect data_block
/5JqeQL8tovfcOv8LePiEOPxJc6VwM/ZNJOGejoQtH9JuVEge7PC/f8YpnaKSgEX/K7XP6c8VfKO
3eQ2Famg4oKiYkN4h6fNqMH2blCTLUE4Q8SBFXTtL/TQhhOivs73b0RFqboOu6/ZL53lDEH+edYH
hnsG7zHuwsXuXZcXe6/3r2I9Ruf9us+smMB3txz43Vmk3l9Ir7hFesgKgScAFJC8e2D8H/ks/sGq
UJXZCp30eb60N7redVDkJr9YmwTReV3HrLVMt/VGKxwDOfAUHor+tQBBr3Upy6TosTHMbTkR1q8q
si0KnVWiZRAgSfZUicBGPW8lcdvD/Ke3BsqrMQueREil7MgTtLA735qZfBTE4bRSFpF8Lyy1yP9d
RjhHiwtO6A8L5FFTOidRLvhrUV+CASpl+WrmrWICSfgzxY+20XQqGSrn3CVkCTg7geSzZxhI2AvF
JHfAzClYC+DUVk8PCBJ8GRfEaCdd+Zpix5NNV8KOZJd8BDEZUMPQQYbkRD5H/5JVojdI+aH9kutX
BZ6wmmsl5G/G5K7y5ZG+vSs/r5g6guueIHKLQLCnkKCQzGs/EQPzvZ9hg26IzHi4Nkz63gP8zBsv
yc7ji0yiMUT0lON3RruvUYb0gLIRGAPpKuBhAciQTegb/x0ZcpwMiWEdsVTnUe84n4NQ52PRw//g
FhKdFx2StA2rh0b2NAXbqJWjhlP+bA5ydwQpsOApRkbb/LNIswz/4bNJiqAnfMzyE6Q+av8VVc/W
XEyrp71t8UrGz3uSyNM63ociosQmfzac87ID0N62jVNXfXWGRCKjCGpRGhy9CTon+/JoZEwniLSy
B/ah1Dst1ofPJKhswoibauQmH2wlWwlkuFBH7IblrrH/uX31TaktvaOOanLFUbmShXK7czPhhkA+
7VvXemCcjMwXd1cIBPIDtS7iGRyFjXRdiJAbLeT6w0DBnPPxYnZErEFalmMZxIfD06jhdV/WHOxK
yc39SRXLGvhUEEkgaDXRUZkuYGCABCklwdI8HaMO/HB+5N2EzZap03M52DlAtdbPkdURSSWh6EES
AK1Ez2tRJCEeVnGXfWylmL+iqTO/9jnhGISvMLOjLQWNNKL/ywSzVhUgXwyWI7a8/M+Q/vXHWULv
TzW1dTuo55jAyIlbmJoAM2kQkSjm+I+6cEFQlLtfdmSCfqImcRZSFGSLlF2nR29vuTDWz6jEXYHi
YE+SzdLNZkvMtcs5g5zy/FLd5efh9fpL93yOaK2mLtg=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth : entity is "blk_mem_gen_v8_4_3_synth";
end design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.design_1_ip_scope_0_0_blk_mem_gen_top
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth__parameterized0\ : entity is "blk_mem_gen_v8_4_3_synth";
end \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth__parameterized0\ is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
I27g6MYD4MW0QL8wlWDusLpmUQr++VIrJciW4kxAZvD1xu4xHc71nmmtQKn4RXprTItt3VZ4jlYb
W7sDjTzN9g==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZCd7ncP4pCL3bR7D848emv5B1F3GIhdyKdOrR8YDzkxL987Xx05dubKjo4b+YR7j9Ecj4dvvZe1H
6adMwXtSnoRoi30g04SXknicgV7eAYs7TVosxFfBt1CNgOyzN0bQbEUEFceUCCSfx8G96lJC7l6k
w+A3ZWFjWJVg1Vt6/vk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WdPinNhXowL0eItMQjILfN3ZtNRlAFPcgUKQgbQY6PjgOLOq8AYH8MaWMxwJ1Q3XLuR/CpJ3Msf8
9ck26xaGFODD49GyvXEm1m8jiFYiUuki4s8taTUnBUe8UmyAXLQtV80x3pecHd3LuDlAHzqlwHLb
RwNUJ8jmu/WUllodatqqpKOBuaenIRptuK0/OJ4m/EhxfYY9CrhNzvJ2OB+5wKW4GAF64RWui9+x
Bqw+bOAFz/60QdWNUEfYkftioYtQ1bPR23AMgCCwss6e7ZAS1ZDoUsWca/IzfDGAnuGA5fYYttxq
uoEZzpjupgu6Vfe6XH1ShFfcTJds9diYIBxF7w==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
J2PI9pkL6e7AvAtwdDAKsuYeJfRneKgQO+nHNIiAxTtlsTW9qYfsuK9HIkf0Sb62x0qE564ViGrS
3wuHHLwljmlXkNuh3H5s1WSIvBYog56SpodBhW9K9QsQbL95ZzCqaRj4TQCbxUYoSGwFsdPmmdM7
La05z8feuoK9AbXfQhrl8CwxQ/x9xQWu9KqCdtni2/rS3DBIIvXrfDnjMLkEKBjcfotyV3n5YDPS
xsl7G0OdCDCYK2w60G1P6raIszyzlL95Ntk3rgPPX40+b45hMQ7F7lMQBoxIOoJb8u1v/MHG31i3
lgLBBsa7tA8yQ8i1322megfZnBtOkWTPkmGATA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T2bPt/UsVlHeNg5Le1CxaL61udjoxzw96Kj3hgyH1tUYL6gN3XkvsqfcbtTEYOMh8h8ccS0iuxDR
u1AEFZYGDETWy4UIcIXJiMNJW9mKlMxeX0aYJmt2gk5SzH3rwCtKFV4BE7w8PwEho0CpeEjRbnf/
H/ZBsUMPSTkaly3dWhZ8iwOFxUFab/dTKOrq+/zYR0ymNFfbFDioA0t6O1QvpkQKgAmLKtz48cIZ
d8mpcUcwmUu2smVlnmcYowYxe9PRMHJNRJBKGeYM3CGauAWJ//wpfST4UNmOFt1mBZDKyRVEtdmb
fxus/Lw6a1mgHZoYg82xB4qQBrCBG6m9hlSvQA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
O9o1BY8Sbymv0CVlrYskQzxGr+xrqarG7EPRt/pk31lim//eAelugbq/q5/E+p/kQuJs9wJgGlt7
J4EL+RC/2F1gmfEvMCf58rYENifz93c15KzjI9WIzhK9r51ZA8fR9ZyTMeOrkrakuwslx8gz2Nd8
GtHVrcoDTocBpHaQfUc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
B1N78V5eMBUsTRlKv5Ei+E+GTZzSlJSEsn133UlYjp9l9fAT6QvueXKaAUXJstu/mKRIG6pF2HeT
10E1KqusBF+N1vx0mJm+BGQmw8VwnDo/1C0bMq/T7k3skIGTTlpmmE04UhvTFZ5eNWSXF3hpRbnX
7LVz1QBuLq3y5vrznsX3rcSujxWkx6qVpUyrOGyzLDbbTs8ND/8qOUNz+Tsj2+A/fjXiwPURl7Ci
pqZKDVAkBrWTm4hLakBaJZUjicezF1sJAkTUk1hm+9WjXbPsux0DtpZ2Gw6FtD0vv/mLn3UAKK8N
uj2yxGASMChllxrDzGBUQlWEDBX02i/b8OMnHA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GwfP4qXgcdqPY+Jn87SwhZpOW2Qk5JYBeuBYCWrGBjIjBhCodSumylkyypjZXMyM4KPLXixZtKOY
nmgJrUh9zP88PejxaDbZAmA9TTNA3jIwnNPCOPmSfMZiRcxQiK3oBA42uMSK1Ie66QLuz6QAyPsH
60vSEx/Z+xmCuP72N2UtqHWFJfKxNvxD41HBK1IwAbpKeFZpquA7pBeiHNyKmgQXxk/LrpaKZxTf
fFX0h0aHm9zvU/82vixYVdMjhUsXIwUQj0WjHj6TKPyS63UNCXf3woIa9Ol7oAqxmy+/NwgQK/J0
oDR9J3350sa293HgleUgy45+QoWr1IXQ66fofA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
izRBbHDDmlBUeeExsudqOD+gyEYFUKhLfn+dqtbOcfkw4qDjSHYx8dLHnnPkNy+QxiMp4BmZ2ECG
5KKqKyAHRCjuW+f8/S31ei02ynjq6tl0mONgVulrH8MbUMWLpTOqV5egf2G7mNY+5nQ0/DzIEx2O
qnmgTMmMkEtDiCYzEw+3EWGwSwKUACJOH0C9lGA84UT87ZPpseERE95myeerhfkysN6W91dMRh+3
FTHn6Q5W7zlA9hKsBIEsE7kmG/B7rGBoY4zw8UdWJ83/6CVOi3fl89SUniXSVjb7kCm5ZMN21FRP
c0slM9SZ3HfJq2a8b+FqClLO8q7+RmqZwsQAVw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 960)
`protect data_block
ZJHor40MSxGnxQefUdAN8IE/zImQ601ysuIIRCbBrrVqjhHuWPQwBI6oM2B4f16MfScJmpE91USJ
lAerj7Qag54LjHniHpDdBwXmZVTaLL+TVgQVYOh7VphTFn5kSA9zRVcT8MTDjzd7IpzbI4uzuK24
CY7lJX3QFbLOBYsF/BUgEBi5bzM2NGAQXRphxtxp451T11X3tLoomSIImTzfUoS5Mlcd8SE2oTXO
QtZDYuHIFvcwi8gqoKJ8LrOsNBkFxnxi3zvctS0dx5wo+96PMxHEVkfApliyRpPmAMKUZiTMrDwG
YgVMFNSPAAdpHul2oxUxuomsre0W5QEJJcvomutNwCcsPowLrTgUn/tvIY8Q9CkL8y55Go495XcX
BcUxRva8eVn44UJjVfj20wpaaiHUAsxzrFAlvKJ59BM7jPsTPBQllpbYTixubAJhSKiWhvFFv/jC
bBAp0e1Qf6QrmyiqpnkTJufxic3+kXzidGM+2b3yTWuIfbNC7fMAkLVEWi6WeRmGDiKe1zGoK1Yp
FTbjthm8THWIHZ5DUkmcVr/bcoCFPd40NMtdUkG3yOpKnaMnVuvPGqSA01dxRpQuLoYx2h2HdeaZ
yDwy4YnvSDVfDGSeeWIj7xdx3i1qdULa34Hon6k5tGBLMG9+9gO/MG5JzVcfJyIurTPTNAROohVf
7POjEheI+UHoyco07a8alv8iwA9l/tR8Tn17BFEkSGv1lNOz8ZkkX0/LkRu6m7LIk/w880zM/9Ha
DiW2do5tcsUietd2taFMgpRIYQUFsDJy2Yw8TdapPzG18RLmRys8r7dMQN1mZHuTbF/n7Qf8OCx2
qIHKNrrMBjPKe4jGrIFmeCSs5QncQTPLn61y/saS4VfKw6MPR25rNgmXwFnm2gyML689y+xzPX/K
wwkrLUVh4teo+S89WBEC3UW2K6ml0vHHVynldXwyIG7numZAGBYPBZDYVA8Y3tvvkEuF+YcEJMAj
PjA7UsZ38vL6FoEXryW/FhzDwGRfWhEYfa9D9xeACEUYV28MjObwfgtUpbN4GVcKKlI0mE+Gb4J8
ugvGYlktStRxK4xZr6SDDARw/uujjEmrUwZRslmjayas92VlU/BbJPeBW2eUQ1BdcfdS2tEgEfq5
/UXdaGPD+n5wlRkzQgEGzj7iA31gXJ/xdMIzBqvcXN3fMcWEMm4pwyxV7nFViiHvu1jFJNxiwNtn
nHFvIqMaUQli2027RH44xF6Vg2pUwsckVfZI5qVbRbhR/twQUTSHJaXD4Gjg1jWg
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "Estimated Power for IP     :     5.6824 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "ip_scope_blk_mem_gen_i1.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "ip_scope_blk_mem_gen_i1.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "blk_mem_gen_v8_4_3";
  attribute c_family : string;
  attribute c_family of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 : entity is "artix7";
end design_1_ip_scope_0_0_blk_mem_gen_v8_4_3;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_3 is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  doutb(31) <= \<const0>\;
  doutb(30) <= \<const0>\;
  doutb(29) <= \<const0>\;
  doutb(28) <= \<const0>\;
  doutb(27) <= \<const0>\;
  doutb(26) <= \<const0>\;
  doutb(25) <= \<const0>\;
  doutb(24) <= \<const0>\;
  doutb(23) <= \<const0>\;
  doutb(22) <= \<const0>\;
  doutb(21) <= \<const0>\;
  doutb(20) <= \<const0>\;
  doutb(19) <= \<const0>\;
  doutb(18) <= \<const0>\;
  doutb(17) <= \<const0>\;
  doutb(16) <= \<const0>\;
  doutb(15) <= \<const0>\;
  doutb(14) <= \<const0>\;
  doutb(13) <= \<const0>\;
  doutb(12) <= \<const0>\;
  doutb(11) <= \<const0>\;
  doutb(10) <= \<const0>\;
  doutb(9) <= \<const0>\;
  doutb(8) <= \<const0>\;
  doutb(7) <= \<const0>\;
  doutb(6) <= \<const0>\;
  doutb(5) <= \<const0>\;
  doutb(4) <= \<const0>\;
  doutb(3) <= \<const0>\;
  doutb(2) <= \<const0>\;
  doutb(1) <= \<const0>\;
  doutb(0) <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "Estimated Power for IP     :     10.698 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "ip_scope_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "ip_scope_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "blk_mem_gen_v8_4_3";
  attribute c_family : string;
  attribute c_family of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ : entity is "artix7";
end \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\ is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.\design_1_ip_scope_0_0_blk_mem_gen_v8_4_3_synth__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
I27g6MYD4MW0QL8wlWDusLpmUQr++VIrJciW4kxAZvD1xu4xHc71nmmtQKn4RXprTItt3VZ4jlYb
W7sDjTzN9g==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZCd7ncP4pCL3bR7D848emv5B1F3GIhdyKdOrR8YDzkxL987Xx05dubKjo4b+YR7j9Ecj4dvvZe1H
6adMwXtSnoRoi30g04SXknicgV7eAYs7TVosxFfBt1CNgOyzN0bQbEUEFceUCCSfx8G96lJC7l6k
w+A3ZWFjWJVg1Vt6/vk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WdPinNhXowL0eItMQjILfN3ZtNRlAFPcgUKQgbQY6PjgOLOq8AYH8MaWMxwJ1Q3XLuR/CpJ3Msf8
9ck26xaGFODD49GyvXEm1m8jiFYiUuki4s8taTUnBUe8UmyAXLQtV80x3pecHd3LuDlAHzqlwHLb
RwNUJ8jmu/WUllodatqqpKOBuaenIRptuK0/OJ4m/EhxfYY9CrhNzvJ2OB+5wKW4GAF64RWui9+x
Bqw+bOAFz/60QdWNUEfYkftioYtQ1bPR23AMgCCwss6e7ZAS1ZDoUsWca/IzfDGAnuGA5fYYttxq
uoEZzpjupgu6Vfe6XH1ShFfcTJds9diYIBxF7w==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
J2PI9pkL6e7AvAtwdDAKsuYeJfRneKgQO+nHNIiAxTtlsTW9qYfsuK9HIkf0Sb62x0qE564ViGrS
3wuHHLwljmlXkNuh3H5s1WSIvBYog56SpodBhW9K9QsQbL95ZzCqaRj4TQCbxUYoSGwFsdPmmdM7
La05z8feuoK9AbXfQhrl8CwxQ/x9xQWu9KqCdtni2/rS3DBIIvXrfDnjMLkEKBjcfotyV3n5YDPS
xsl7G0OdCDCYK2w60G1P6raIszyzlL95Ntk3rgPPX40+b45hMQ7F7lMQBoxIOoJb8u1v/MHG31i3
lgLBBsa7tA8yQ8i1322megfZnBtOkWTPkmGATA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T2bPt/UsVlHeNg5Le1CxaL61udjoxzw96Kj3hgyH1tUYL6gN3XkvsqfcbtTEYOMh8h8ccS0iuxDR
u1AEFZYGDETWy4UIcIXJiMNJW9mKlMxeX0aYJmt2gk5SzH3rwCtKFV4BE7w8PwEho0CpeEjRbnf/
H/ZBsUMPSTkaly3dWhZ8iwOFxUFab/dTKOrq+/zYR0ymNFfbFDioA0t6O1QvpkQKgAmLKtz48cIZ
d8mpcUcwmUu2smVlnmcYowYxe9PRMHJNRJBKGeYM3CGauAWJ//wpfST4UNmOFt1mBZDKyRVEtdmb
fxus/Lw6a1mgHZoYg82xB4qQBrCBG6m9hlSvQA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
O9o1BY8Sbymv0CVlrYskQzxGr+xrqarG7EPRt/pk31lim//eAelugbq/q5/E+p/kQuJs9wJgGlt7
J4EL+RC/2F1gmfEvMCf58rYENifz93c15KzjI9WIzhK9r51ZA8fR9ZyTMeOrkrakuwslx8gz2Nd8
GtHVrcoDTocBpHaQfUc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
B1N78V5eMBUsTRlKv5Ei+E+GTZzSlJSEsn133UlYjp9l9fAT6QvueXKaAUXJstu/mKRIG6pF2HeT
10E1KqusBF+N1vx0mJm+BGQmw8VwnDo/1C0bMq/T7k3skIGTTlpmmE04UhvTFZ5eNWSXF3hpRbnX
7LVz1QBuLq3y5vrznsX3rcSujxWkx6qVpUyrOGyzLDbbTs8ND/8qOUNz+Tsj2+A/fjXiwPURl7Ci
pqZKDVAkBrWTm4hLakBaJZUjicezF1sJAkTUk1hm+9WjXbPsux0DtpZ2Gw6FtD0vv/mLn3UAKK8N
uj2yxGASMChllxrDzGBUQlWEDBX02i/b8OMnHA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
AkCMNWYb/UIQtA8sTxq83f4nU06sXszseFURwDBDtFvchP4Z+EhwAt6DgcjADP5aKbK2P83fSJU9
iDpHyDqJECbW17igVYWx6SXcK4qmpaqhk3UTaYWXOpZ6YdjuEQA2D1kQb3tApLWEHIg9ymcsa47H
HLRDIMLU7YmL+ailk/yTL/kukwnH/i1tCWST2XizBTLDo1/hXWpD6EFdz3VxNCSb+qX6/zrxtY3l
dImwxsVCWXtFrZ5U91zqM9I+7hDkND5K3I7s1MPxvM3WGmR9U2z71NoVX5KeiQaBYsBAdw4G/cA/
5faM0VbJ9R46OUpAMZeWiyBsbm6z0OaYpfblqA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3WRB3mHlHr+Ad1M8KK6zkHbu4zo32V4ASTbmHe9aH57Fdq922SvipH0LiufPv+9wpQzgkIbtWGvN
31ON4XZr6bNMTcxEhBRf1fIrzEH4zlBfmObNvvC5Mnm53ww8GPqdJevdjBaszi6RxC7hfN4HK5VM
uSiZCTDs0QowMTdzaI1rD91emmsMUf0cqiZH+tWjv+fa1sfA+1fg62Q3ez8aWoaHnaH5jxPyfrk6
mSyn11fmHXgvi0rFl3v4CJnB4o9tpse0AfoXJsZvGSK6VsWK3+e8pXVE04pNvpsaw5WrVwS6uTvo
oXGDr8HENJkdGyC1XEXYl0FHOwFNS+cfUzQxsw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4560)
`protect data_block
ZJHor40MSxGnxQefUdAN8IE/zImQ601ysuIIRCbBrrVqjhHuWPQwBI6oM2B4f16MfScJmpE91USJ
lAerj7Qag54LjHniHpDdBwXmZVTaLL+TVgQVYOh7VphTFn5kSA9zRVcT8MTDjzd7IpzbI4uzuK24
CY7lJX3QFbLOBYsF/BUgEBi5bzM2NGAQXRphxtxp0nGU25Usr/iyMEueqSHhbzqUxv7oPP28xm2p
sS2El4XTN2M/820ZXDFrRH4xuTyFRiVPeZZycmizYp2SDeIZE3qdGb0mx8ZyOElV6w0tZwl8Sy6e
jL6PWEhRfEX4d7mknr6cwhAqNK+Qn++z227kCy/3p+hUcP/tBRI7LqTS/+hcSOMRnyS/q14vffWT
b3SFHmuSX/qwrw4GiVwG27phpQADKUq76bbmpJv4DKRHh62X6v39X0VnGQgFHDclWOFpwYMrwUcX
QNlki+Pua6kFVNxcvQwcrQO2TUp8p6zSTk5tn2iCQjD9x7PF+hef3N+pbULL+aWqNUr1D28sjnUe
rdm5pWYvyzz/qK4YRP1RyLxZKu4XomtaMC2LlTaabpQzmM9+oQNXqjmNy75WL+5W8+IwzWHb3Sho
t3UWus4PW9pOYXLdxTBq9bHbIxM1eBONczFRTwFXhnJUdaaOo1NmHt7pNAnEqyeM26VjFHqgxQ8K
QKFmV1Ogp234Pv6VzkwjF82h024xfrodV/SgA2dJ6nM41JHKXJt532zLiXKAuif164FO44Z+CuSD
uIbmwxSE4EPiW43/ySjTyt2A/vQdOe6qLZF1+s3zw9ja6vmHiPpTJtse3+M+cHhmySGamulSo1JI
+MRb/Behd9BYdYlHczUBvMW7ZSfAh5ZZLoR3u4wqj4vFkQPMjTx4fC9WHTLUN4zeFgoKj1qk4AsO
4W1lHcM9X1lONd3cUHwnBAdWtOVwPFbROCLK5DkvJdqgA9dn6QLNlbO+J7Rb/3yT/KEWFz2HYRfV
i/WlJQ9fqg5yq+C9kneq3eFTPEQgJrcFwfXt+b911o6XtFmsT0x+LPDMov+FFjaiEaTVxLCeLEPM
8TgKiHFrVKJ09hezeCVCclPBByCfadIdLBRUTecJszp7bJlIjlikccyAuoiFbeEvpv/70jGLKQ4T
MCrdU6cfW6O7uRAA5xHGAYkFciT34eMahwc/ZvzOTMPL6o+qF5CBoyqzZr1tE1nMYtoqasQq8HJP
P2JYuKxnsqE1INNsAXvzLYdb/Bc5MF1vec4/1sXT/+HLBQwlLONuj/LEP2hsX69BHGS/j1kNwT/4
ANcrqt9uJ8lfbPCXjorYAuSj24HWxh79Ud68v4C7LjeRmeRsvYOBr/AHrlBffMngklfbWLV5ZTvL
gD4WACrrfVpA5kCc9EdGcGRj8SoahyaRS+uF1+IKOFgsZ+lJL0HyvmlVyA1JCCokcm2BG366A0Rk
4joKFclJkq81up9t1wmZ9lOZRxR6EfU42UaaeF5mYuUa6S72vf2atldHDzQV7QTUG7WBjkjAtYdF
ThP6mkzYLlTWPgqfyZVUX5/NPaCuAhBYHGdX+GspSxCJ8aN8XztYT7EcZzWBFSL1kzIxg0bBp6Ib
/rAsCCK8pvwVcsBw4+MRtPBJ9nr2Z/NyBRP/DhJDKe5ZOPSx5lxOoPZjUInYxKYQl8+o914vjwJR
QBxb1r7FEkmbq59KpvwY6JvxQckLXlBmXTNyCrqp2V5fkzBZjE/kHO+j7aefQg9HB2a+OE81nxrZ
VJ217EKzwV/pNA1H/YhoIipLdATb9UhOPE+iGQTksQhs+PbcRFBEA8gBRea7GdxtSLClYd6uOMyO
OM4yi4J8yNTNi9QPhaWAxTpRaWi6YqV4IUeML+ZQx+4FcHOCzhF/f1srLwmBnyukI3DUUOHC0xLv
0mEoiB2lb+uPMVrkW6mkjMc3OgJU/qVszre+lrV1GFbDBf3eW4egGrhA1j76tx3AJziaAjH24psi
M0eI4WJEYk8hmkWR2v8Ju3SNMtGW4X5zKo9bwsjVaOQ1w4Xkv5XorYQQirolv/LSA9pPiJBiUjc5
ZljcOmCM8eXcp0WEzO2078qlSlrx3ryMbAMMFsCSSmI9cttgj6JA6DKXoIyx1pz0XnKn7R7k96AW
3UCM9iQEfwngUi/K+QDbCwOyg0/RpO4xxIq3NsLWZS7396Ckem9HudD2STAj0bC8MDMv5GkWTpUQ
by0qdiqt6cYmoCF+fhw1Jio9Po3cMo8zu7hv7MBXme057yeZTQemqcl/G0BVHBzR7fsQyiQgrhuZ
z06CHQfFGg+2zWXJ4QZcUxtuTgdq9j9OfB2k6Vn7ImBak7dNPYFcfevS40bzTun6WVmJNWZPyPgu
DBZRJAq9vGuF8q5fuyo15ScFyvQeLbnv4l0SDMGTDRjGvrPtjM/D/LfYn93H40zktfTkao570Ahq
bjQUEp/xmV5Cc+LXku32yjYqKyVF8KYIjWxFTTVD5hy6qR3ZV9UAWDt2bWYh+aaGhc91F/o2POCn
qkeoF+rwHPk2Lgb+w+bc7QrZgbtbHs0tFv+3lcecZbmyy/Q0qDEp11vixK6PiaCX/+c3VDbpRxLK
j1ECja/aFitvoh/Aa6BWCK7WYrtES9avtRTJwJowbvg7ejfR+5BDZJFQxONs537GbqYzpbb7XtSY
eHWb+rLJfQWgsFL1Fmb3JrtO55/LR9mTW+dZj1gA1Q6TWd06OGcj7DA89wvsQVW6hMnQJz2hGDqw
PfYggSoeUIsm/L4J3KdgLrQYLNOjgTyQe6sbSsflE0O95l9Osr82n5iNQreikJS3DlTyuENGT3sB
uFagYJS2GLtm5Dn5Nw8+w7cEhduY1uy7GpAiyC0Bu8Fi4CMnv5Uin8Huo2jRxwX7BjfiLuvIC5/6
iZaMP6bAOR9HWGHNGJzaS5eJ3nFSW09CRte4vkHT1cQR0uGZQRVYez5Fj23nxyCjVQBur07cZZGO
kZxzZhOJ339YrToFIOURIBEwiED+OAM3JNygk7U/a1+UfioBiYhV4xcIlOe9L9uGCOTorePXCGc7
7EN3i5JU0KunKhyzhqGDL27YCZIdKA21cSRZO5M9WMYTpIA4Hj4g3ikuyVf0TkptXLJUG+ztg3MO
AP7DhcJkTLaBgXsP4USX4E691su4ODIX4bk+Ehz/cDlHKlcGL0KD1KyHZ30ngJnlm2xmWOTp1c5b
+HJ1iv6jcy8a7vadrkwrR7DEtHMNVDXqE97o7s0aMEEMsDuFNMjHljcTUtSMvqMfAPTrQD22gxCn
x48mu5Q2b2BhV2SrEwQ6whDLQGSqhUEpwkPrk8iE55H67HQVOC0oF61JAoQh3F7HX86uJYfGI7pC
fZ7yehmkKBQkRohxAWlN8boW67Sl7t38v/OmhR5FBXyGHgRXiC0Wj/fZz583XGin0ctQ1L4z9ZJy
a3aPw26382lRnqJP7oWy7naztZ/ki6z5UZGQ9FVnhZA1Mqvz56lQBEgjGOR0Nj4ykNYVE//SfBd0
zSgqdBqWHb+b80pqC4neS/i8GxcK2S0WmlBUkg5Nsbg3KyxDbgg4guxLGdsXkTUHSjF5f1x2E31v
ldcptpqiBjEloxpvMVG0g93i+ba8Kb2O/AP77kdXtGLAOp3j+jO33PB+LuCqtHl7QsvGaLF9Hn1E
dDqdnSivJxi1X2oYhTKJj/1hckypPAT9PDFZWioE6r7r2HvRco4PaSxpNkEWtOxe6rJ5KmwdkaxV
zfEj0lOzOg3aPQ/7KxhVUViU/+3N/PejlWaCrieO0R+OztnWb/BiG0r89IUgicDvMhA5nQ/IU010
m72Q519sL3CgBIGcmPcWvJpjDsk7Ks+mPNbSwYY6/whkEOKjuOYBRWWOFZK7PGaZ+MYu6ZgdWW4L
mHAvtIFQEmpA4jeIbvOV+ITD1NRnFpBTOXjwsQQPAsa06kBhfQr+N5jVN3thvc0ahv0la0U33rBb
1SF74ykS6vQd87/5795X3amLEa8L+3yxY+UDZ4ATzETDQZ1B1vTCDSiF2rwUNKl0torVa8xwGSiD
czPCTapyaeN4Quf+Xieyfwky4XAa5ZyPi7l91V5gHZeQHCATeqouwmgGp4ON69vANhYZturCWbWx
R3LLcv6OQ4xsz7LSK6nMQDkpv6B3ITW9x2Tx/3vGLBY9LZzrXROsBWj/RB+PulqErwOypHiQsba7
6BHd96khtQdT2J9N/7w9xEdcmW1d7Qn5LedNbJVun8l+PegwvPjJuz8CqZS/75bdKkHYLao2gF/0
aQLl5VwqhUhEqgdG9t7/ECTAYPAdyjm+8qJcQpQH1cPz98HvUB6s/qVDwbrptRzeNheQIBnmZEpg
sYuIjVAVJNOqj6OkRHgDdJ94yrbkx0e5/Y0nHG80o87JxSfJn4rdSbv+KI2UukC+QAwNOeCQV8eJ
26yuLGoy3GablHOuN6tmEKqDXCik2PeyY/afVayEYwBTsGdAWDN0PVBo9gPUQDhKobSYCqXsSNF+
aUBGakyCkFxVExUqVIg5HSzQx/J+vUCCuFV8G+5oiP0M1Wes90MEw1+XwiZMDp7VpOGr+oGaFH7s
C6W+6wChsGr3Dcr+EtF+PaWKHky4Lc0xZUBP/cIVb5tJ57jV8cnLHRomDL+lWP7cFhZMk6yRwqKV
JWe5jJqs9Rtd1NJ4UYvlS7iCij2q2ctqUg0zCWtBWvvilLWXDWm8ZdUE/SeF78p0bS0/oRRuIUTq
kEashflb6V4G6fYglBTGWPqDvRqXqSNFqy9k1pbNmcDbwxwD7uhuqFPSboDwBNsih5gKIwzSoadZ
SbkNijHlz3YY1oj3JSuk2eR4FVVxk4sA2Vbnimal2v8pXNKQl3t7O3Y/atM+SKRThkeBshXxjhAR
4iMk0qmNw9jXBngI2nMySRdOXJy0cU47Uo7420yeBqefddaI9PiPvW+jJGIDj1OHzaMcZuBCxPmn
LAxBGeWfpup4/raD/UidE3TNmI0EsbRjrOUkK/TW0kxRSrPRMLKUibxWUM/GOg1AacgYRvRD302e
QPSpFUDcAUlWX6o5bKcBeoelp86dUuJe/NZ4VaaU6+mudlYFpXZ0Dn4SZG2t+pHPJNracZXqzDpK
u8IM4Tus8bgU3bW4QXKWI3h2F7MGUfTLrC6UBOx7lEyI4GhUheN7oa36syzK27uC+lGWkyN8dX58
KKgpPwPlui+SePPx0vM9kHJUQJE/nXY/D0v3W+gr0RA3IdO0YShLMt+4ML9Z0O+LbgO4cfLr2Ylj
gt2q7nVASPHLFOUYprAcTGDZm5gQ+jzZi2ESGmMShvIn8nQUMcSW2fak3fU+P9cdB/w5IHkjPkYY
3m4OEW73dYgVQ0LnI4tjY4K6TVMK8YM6YIbjmljRS4xCbJLwBCbwdKshFJ+mn8iPSgauPr3oCvmh
k2bzGeMcQEvnw5kV9yF9qC3NZgnBUQSyEI4zpBxTyiZToLEGZZBPKKaJgf1rmTCB08s93mj1VFdV
4R7CJoV42w9Wa+K5pRKcYNUEFU3DlW0vPL8O9T1+16JGOK5QH1KLiQFASsAHmlUs8iHy2MTzAIpn
RM9JjAcHv/YIu7we0Ed7QdWGvz49oQ2QZ0BeaHpNuHmO+cQCdgy2g351tewuqF9Vq1UP3k/aoXVL
wLXtDooEM2KgzlJifEjtd1Z4f7FUtn305s5wksrpxmZU00uyLYiU469qZvuJbYqBi0FXHnoc/P7x
qxsvhg1GSMG0C19snpHycFidmLxarW6gJDJvQZbb91QFGyy1L+fC6Abvf6rrIjsf06Tfe7WOPNBo
GD6AgX9yWvPfuNJXoM1OqpDsVxMFSDmZd1tdh+ZTY7n2jX/05DR0VRZXDDfcxEmMvLqD/rMxzyMm
nWk76vIzeP929PRmPpIz/FGJcPH041OV6bXrKhGwlB6m2bUjMWIC5Xgb4eCDplKt+bsMIQe8lGRD
jPt8k7e7kYGrjtTZCnb8ErvH/dYR4+nsMcWm/hTykHW9ptHPGkucGZnUt0DmAQGrBYxRjA2OBnAV
SXyjNXC5SBIIqOSKlXU0HJZ3H5uxL3/PS+8qsvuYXoiFKW8rqY2LtqypJqMAKXQjrhcs090x0Sa5
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 : entity is "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 : entity is "ip_scope_blk_mem_gen_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 : entity is "blk_mem_gen_v8_4_3,Vivado 2019.1";
end design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     10.698 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 1;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_scope_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_scope_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 2;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of clkb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK";
  attribute X_INTERFACE_PARAMETER of clkb : signal is "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of enb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of addrb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of dinb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of doutb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
  attribute X_INTERFACE_INFO of web : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB WE";
begin
U0: entity work.\design_1_ip_scope_0_0_blk_mem_gen_v8_4_3__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => enb,
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(10 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(10 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(10 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(10 downto 0),
      s_axi_rdata(31 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 : entity is "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_3,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 : entity is "ip_scope_blk_mem_gen_i1";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 : entity is "blk_mem_gen_v8_4_3,Vivado 2019.1";
end design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_doutb_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     5.6824 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_scope_blk_mem_gen_i1.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_scope_blk_mem_gen_i1.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
begin
U0: entity work.design_1_ip_scope_0_0_blk_mem_gen_v8_4_3
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => B"00000000000",
      clka => clka,
      clkb => '0',
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => B"00000000000000000000000000000000",
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => NLW_U0_doutb_UNCONNECTED(31 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(10 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(10 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(10 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(10 downto 0),
      s_axi_rdata(31 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_c_counter_binary_v12_0_13 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 10 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is 11;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 : entity is "c_counter_binary_v12_0_13";
end design_1_ip_scope_0_0_c_counter_binary_v12_0_13;

architecture STRUCTURE of design_1_ip_scope_0_0_c_counter_binary_v12_0_13 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_i_synth_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 1;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 11;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "1";
  attribute c_has_load of i_synth : label is 1;
  attribute c_has_thresh0 of i_synth : label is 0;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 0;
  attribute c_thresh0_value of i_synth : label is "1";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
  THRESH0 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_synth: entity work.design_1_ip_scope_0_0_c_counter_binary_v12_0_13_viv
     port map (
      CE => CE,
      CLK => CLK,
      L(10 downto 0) => L(10 downto 0),
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_i_synth_THRESH0_UNCONNECTED,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 : entity is "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 : entity is "ip_scope_c_counter_binary_v12_0_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 : entity is "c_counter_binary_v12_0_13,Vivado 2019.1";
end design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 is
  signal NLW_U0_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 11;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "1";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 1;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 0;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "1";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of LOAD : signal is "xilinx.com:signal:data:1.0 load_intf DATA";
  attribute X_INTERFACE_PARAMETER of LOAD : signal is "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of SINIT : signal is "xilinx.com:signal:data:1.0 sinit_intf DATA";
  attribute X_INTERFACE_PARAMETER of SINIT : signal is "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of L : signal is "xilinx.com:signal:data:1.0 l_intf DATA";
  attribute X_INTERFACE_PARAMETER of L : signal is "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute X_INTERFACE_PARAMETER of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.design_1_ip_scope_0_0_c_counter_binary_v12_0_13
     port map (
      CE => CE,
      CLK => CLK,
      L(10 downto 0) => L(10 downto 0),
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_U0_THRESH0_UNCONNECTED,
      UP => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xldpram is
  port (
    r9_douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r2_wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    r1_addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r3_dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xldpram : entity is "ip_scope_xldpram";
end design_1_ip_scope_0_0_ip_scope_xldpram;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xldpram is
  signal \comp0.core_instance0_n_32\ : STD_LOGIC;
  signal \comp0.core_instance0_n_33\ : STD_LOGIC;
  signal \comp0.core_instance0_n_34\ : STD_LOGIC;
  signal \comp0.core_instance0_n_35\ : STD_LOGIC;
  signal \comp0.core_instance0_n_36\ : STD_LOGIC;
  signal \comp0.core_instance0_n_37\ : STD_LOGIC;
  signal \comp0.core_instance0_n_38\ : STD_LOGIC;
  signal \comp0.core_instance0_n_39\ : STD_LOGIC;
  signal \comp0.core_instance0_n_40\ : STD_LOGIC;
  signal \comp0.core_instance0_n_41\ : STD_LOGIC;
  signal \comp0.core_instance0_n_42\ : STD_LOGIC;
  signal \comp0.core_instance0_n_43\ : STD_LOGIC;
  signal \comp0.core_instance0_n_44\ : STD_LOGIC;
  signal \comp0.core_instance0_n_45\ : STD_LOGIC;
  signal \comp0.core_instance0_n_46\ : STD_LOGIC;
  signal \comp0.core_instance0_n_47\ : STD_LOGIC;
  signal \comp0.core_instance0_n_48\ : STD_LOGIC;
  signal \comp0.core_instance0_n_49\ : STD_LOGIC;
  signal \comp0.core_instance0_n_50\ : STD_LOGIC;
  signal \comp0.core_instance0_n_51\ : STD_LOGIC;
  signal \comp0.core_instance0_n_52\ : STD_LOGIC;
  signal \comp0.core_instance0_n_53\ : STD_LOGIC;
  signal \comp0.core_instance0_n_54\ : STD_LOGIC;
  signal \comp0.core_instance0_n_55\ : STD_LOGIC;
  signal \comp0.core_instance0_n_56\ : STD_LOGIC;
  signal \comp0.core_instance0_n_57\ : STD_LOGIC;
  signal \comp0.core_instance0_n_58\ : STD_LOGIC;
  signal \comp0.core_instance0_n_59\ : STD_LOGIC;
  signal \comp0.core_instance0_n_60\ : STD_LOGIC;
  signal \comp0.core_instance0_n_61\ : STD_LOGIC;
  signal \comp0.core_instance0_n_62\ : STD_LOGIC;
  signal \comp0.core_instance0_n_63\ : STD_LOGIC;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_3,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0
     port map (
      addra(10 downto 0) => r1_addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => axibusdomain_clk,
      clkb => signaldomain_clk,
      dina(31 downto 0) => r3_dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => r9_douta(31 downto 0),
      doutb(31) => \comp0.core_instance0_n_32\,
      doutb(30) => \comp0.core_instance0_n_33\,
      doutb(29) => \comp0.core_instance0_n_34\,
      doutb(28) => \comp0.core_instance0_n_35\,
      doutb(27) => \comp0.core_instance0_n_36\,
      doutb(26) => \comp0.core_instance0_n_37\,
      doutb(25) => \comp0.core_instance0_n_38\,
      doutb(24) => \comp0.core_instance0_n_39\,
      doutb(23) => \comp0.core_instance0_n_40\,
      doutb(22) => \comp0.core_instance0_n_41\,
      doutb(21) => \comp0.core_instance0_n_42\,
      doutb(20) => \comp0.core_instance0_n_43\,
      doutb(19) => \comp0.core_instance0_n_44\,
      doutb(18) => \comp0.core_instance0_n_45\,
      doutb(17) => \comp0.core_instance0_n_46\,
      doutb(16) => \comp0.core_instance0_n_47\,
      doutb(15) => \comp0.core_instance0_n_48\,
      doutb(14) => \comp0.core_instance0_n_49\,
      doutb(13) => \comp0.core_instance0_n_50\,
      doutb(12) => \comp0.core_instance0_n_51\,
      doutb(11) => \comp0.core_instance0_n_52\,
      doutb(10) => \comp0.core_instance0_n_53\,
      doutb(9) => \comp0.core_instance0_n_54\,
      doutb(8) => \comp0.core_instance0_n_55\,
      doutb(7) => \comp0.core_instance0_n_56\,
      doutb(6) => \comp0.core_instance0_n_57\,
      doutb(5) => \comp0.core_instance0_n_58\,
      doutb(4) => \comp0.core_instance0_n_59\,
      doutb(3) => \comp0.core_instance0_n_60\,
      doutb(2) => \comp0.core_instance0_n_61\,
      doutb(1) => \comp0.core_instance0_n_62\,
      doutb(0) => \comp0.core_instance0_n_63\,
      ena => '1',
      enb => '1',
      wea(0) => r2_wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xlspram is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xlspram : entity is "ip_scope_xlspram";
end design_1_ip_scope_0_0_ip_scope_xlspram;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xlspram is
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_3,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_3,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1
     port map (
      addra(10 downto 0) => Q(10 downto 0),
      clka => signaldomain_clk,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => dinb(31 downto 0),
      ena => '1',
      wea(0) => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xlcounter_free is
  port (
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    LOAD : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xlcounter_free : entity is "ip_scope_xlcounter_free";
end design_1_ip_scope_0_0_ip_scope_xlcounter_free;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xlcounter_free is
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_counter_binary_v12_0_13,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0
     port map (
      CE => '1',
      CLK => signaldomain_clk,
      L(10 downto 0) => B"00000000000",
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SINIT => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_delayline is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    o : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_delayline : entity is "ip_scope_delayline";
end design_1_ip_scope_0_0_ip_scope_delayline;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_delayline is
  signal counter_op_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal relational_op_net : STD_LOGIC;
begin
counter: entity work.design_1_ip_scope_0_0_ip_scope_xlcounter_free
     port map (
      LOAD => relational_op_net,
      Q(10 downto 0) => counter_op_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
relational: entity work.design_1_ip_scope_0_0_sysgen_relational_f845914c88
     port map (
      LOAD => relational_op_net,
      Q(10 downto 0) => counter_op_net(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
single_port_ram: entity work.design_1_ip_scope_0_0_ip_scope_xlspram
     port map (
      Q(10 downto 0) => counter_op_net(10 downto 0),
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_signaldomain is
  port (
    addrb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \full_i_5_24_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_signaldomain : entity is "ip_scope_signaldomain";
end design_1_ip_scope_0_0_ip_scope_signaldomain;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_signaldomain is
  signal \^full_i_5_24_reg[0]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register1_n_0 : STD_LOGIC;
  signal register1_n_1 : STD_LOGIC;
  signal register1_n_2 : STD_LOGIC;
  signal register1_n_20 : STD_LOGIC;
  signal register1_n_21 : STD_LOGIC;
  signal register1_n_22 : STD_LOGIC;
  signal register1_n_23 : STD_LOGIC;
  signal register1_n_3 : STD_LOGIC;
  signal register1_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register2_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register3_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register4_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register5_n_0 : STD_LOGIC;
  signal register5_n_1 : STD_LOGIC;
  signal register5_n_2 : STD_LOGIC;
  signal register5_n_20 : STD_LOGIC;
  signal register5_n_21 : STD_LOGIC;
  signal register5_n_22 : STD_LOGIC;
  signal register5_n_23 : STD_LOGIC;
  signal register5_n_3 : STD_LOGIC;
  signal register5_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register_q_net : STD_LOGIC;
  signal risingedgetrigger_n_0 : STD_LOGIC;
begin
  \full_i_5_24_reg[0]\(0) <= \^full_i_5_24_reg[0]\(0);
convert: entity work.\design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2\(0) => \^full_i_5_24_reg[0]\(0),
      signaldomain_clk => signaldomain_clk
    );
delayline: entity work.design_1_ip_scope_0_0_ip_scope_delayline
     port map (
      dina(31 downto 16) => register2_q_net_x0(15 downto 0),
      dina(15 downto 0) => register3_q_net_x0(15 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
leveltriggerfifocontroller: entity work.design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller
     port map (
      DI(3) => register1_n_0,
      DI(2) => register1_n_1,
      DI(1) => register1_n_2,
      DI(0) => register1_n_3,
      S(3) => register5_n_0,
      S(2) => register5_n_1,
      S(1) => register5_n_2,
      S(0) => register5_n_3,
      addrb(10 downto 0) => addrb(10 downto 0),
      \full_i_5_24_reg[0]\(0) => \^full_i_5_24_reg[0]\(0),
      register4_q_net(0) => register4_q_net(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk,
      \state_4_23_reg[1]\(3) => register5_n_23,
      \state_4_23_reg[1]\(2) => register1_n_20,
      \state_4_23_reg[1]\(1) => register1_n_21,
      \state_4_23_reg[1]\(0) => register1_n_22,
      \state_4_23_reg[1]_0\(3) => register1_n_23,
      \state_4_23_reg[1]_0\(2) => register5_n_20,
      \state_4_23_reg[1]_0\(1) => register5_n_21,
      \state_4_23_reg[1]_0\(0) => register5_n_22,
      \state_4_23_reg[1]_1\ => risingedgetrigger_n_0,
      web(0) => web(0)
    );
register1: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\
     port map (
      DI(3) => register1_n_0,
      DI(2) => register1_n_1,
      DI(1) => register1_n_2,
      DI(0) => register1_n_3,
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2) => register1_n_20,
      \fd_prim_array[13].bit_is_0.fdre_comp\(1) => register1_n_21,
      \fd_prim_array[13].bit_is_0.fdre_comp\(0) => register1_n_22,
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => register1_n_23,
      o(15 downto 0) => register1_q_net_x0(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => register5_q_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register2: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => register2_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register3: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => register3_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register4: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
register5: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\
     port map (
      S(3) => register5_n_0,
      S(2) => register5_n_1,
      S(1) => register5_n_2,
      S(0) => register5_n_3,
      \fd_prim_array[13].bit_is_0.fdre_comp\(2) => register5_n_20,
      \fd_prim_array[13].bit_is_0.fdre_comp\(1) => register5_n_21,
      \fd_prim_array[13].bit_is_0.fdre_comp\(0) => register5_n_22,
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => register5_n_23,
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => register5_q_net(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => register1_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.design_1_ip_scope_0_0_ip_scope_xlregister
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
risingedgetrigger: entity work.design_1_ip_scope_0_0_ip_scope_risingedgetrigger
     port map (
      \ff_2_17_reg[0]\ => risingedgetrigger_n_0,
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_struct is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    r9_douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r2_wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    r1_addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r3_dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_struct : entity is "ip_scope_struct";
end design_1_ip_scope_0_0_ip_scope_struct;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_struct is
  signal convert_dout_net_x1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal mcode_we_net : STD_LOGIC;
  signal register1_q_net : STD_LOGIC;
  signal register2_q_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal register3_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal single_port_ram_data_out_net : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slice1_y_net : STD_LOGIC_VECTOR ( 10 downto 0 );
begin
dual_port_ram: entity work.design_1_ip_scope_0_0_ip_scope_xldpram
     port map (
      addrb(10 downto 0) => slice1_y_net(10 downto 0),
      axibusdomain_clk => axibusdomain_clk,
      dinb(31 downto 0) => single_port_ram_data_out_net(31 downto 0),
      r1_addra(10 downto 0) => r1_addra(10 downto 0),
      r2_wea(0) => r2_wea(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      signaldomain_clk => signaldomain_clk,
      web(0) => mcode_we_net
    );
register1: entity work.\design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register1_q_net,
      r5_enable(0) => r5_enable(0),
      signaldomain_clk => signaldomain_clk
    );
register2: entity work.\design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => register2_q_net(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register3: entity work.\design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register3_q_net(0),
      r7_clear(0) => r7_clear(0),
      signaldomain_clk => signaldomain_clk
    );
register4: entity work.\design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      full(0) => convert_dout_net_x1(0),
      r8_full(0) => r8_full(0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.design_1_ip_scope_0_0_ip_scope_xlAsynRegister
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => register_q_net(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
signaldomain: entity work.design_1_ip_scope_0_0_ip_scope_signaldomain
     port map (
      addrb(10 downto 0) => slice1_y_net(10 downto 0),
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      clear(0) => register3_q_net(0),
      dinb(31 downto 0) => single_port_ram_data_out_net(31 downto 0),
      enable(0) => register1_q_net,
      full(0) => convert_dout_net_x1(0),
      \full_i_5_24_reg[0]\(0) => full(0),
      i(15 downto 0) => register_q_net(15 downto 0),
      o(10 downto 0) => register2_q_net(10 downto 0),
      signaldomain_clk => signaldomain_clk,
      web(0) => mcode_we_net
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope is
  port (
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope : entity is "ip_scope";
end design_1_ip_scope_0_0_ip_scope;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope is
  signal \<const0>\ : STD_LOGIC;
  signal r1_addra : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal r2_wea : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r3_dina : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r4_threshold : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r5_enable : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r6_delay : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal r7_clear : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r8_full : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r9_douta : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  axibusdomain_s_axi_bresp(1) <= \<const0>\;
  axibusdomain_s_axi_bresp(0) <= \<const0>\;
  axibusdomain_s_axi_rresp(1) <= \<const0>\;
  axibusdomain_s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
axibusdomain_axi_lite_interface: entity work.design_1_ip_scope_0_0_axibusdomain_axi_lite_interface
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => r5_enable(0),
      i(10 downto 0) => r6_delay(10 downto 0),
      q(0) => r8_full(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      \slv_reg_array_reg[0][0]\(0) => r7_clear(0),
      \slv_reg_array_reg[3][15]\(15 downto 0) => r4_threshold(15 downto 0),
      \slv_reg_array_reg[5][0]\(0) => r2_wea(0),
      \slv_reg_array_reg[6][10]\(10 downto 0) => r1_addra(10 downto 0)
    );
ip_scope_struct: entity work.design_1_ip_scope_0_0_ip_scope_struct
     port map (
      axibusdomain_clk => axibusdomain_clk,
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      full(0) => full(0),
      r1_addra(10 downto 0) => r1_addra(10 downto 0),
      r2_wea(0) => r2_wea(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0),
      r5_enable(0) => r5_enable(0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0),
      r7_clear(0) => r7_clear(0),
      r8_full(0) => r8_full(0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0 is
  port (
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_scope_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_scope_0_0 : entity is "design_1_ip_scope_0_0,ip_scope,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_1_ip_scope_0_0 : entity is "sysgen";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_scope_0_0 : entity is "ip_scope,Vivado 2019.1";
end design_1_ip_scope_0_0;

architecture STRUCTURE of design_1_ip_scope_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of axibusdomain_aresetn : signal is "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of axibusdomain_aresetn : signal is "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_clk : signal is "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of axibusdomain_clk : signal is "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID";
  attribute X_INTERFACE_PARAMETER of axibusdomain_s_axi_rvalid : signal is "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID";
  attribute X_INTERFACE_INFO of signaldomain_clk : signal is "xilinx.com:signal:clock:1.0 signaldomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of signaldomain_clk : signal is "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB";
  attribute X_INTERFACE_INFO of ch1 : signal is "xilinx.com:signal:data:1.0 ch1 DATA";
  attribute X_INTERFACE_PARAMETER of ch1 : signal is "XIL_INTERFACENAME ch1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of ch2 : signal is "xilinx.com:signal:data:1.0 ch2 DATA";
  attribute X_INTERFACE_PARAMETER of ch2 : signal is "XIL_INTERFACENAME ch2, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of ch_trigger : signal is "xilinx.com:signal:data:1.0 ch_trigger DATA";
  attribute X_INTERFACE_PARAMETER of ch_trigger : signal is "XIL_INTERFACENAME ch_trigger, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of full : signal is "xilinx.com:signal:interrupt:1.0 full INTERRUPT";
  attribute X_INTERFACE_PARAMETER of full : signal is "XIL_INTERFACENAME full, SENSITIVITY LEVEL_HIGH, PortWidth 1";
begin
inst: entity work.design_1_ip_scope_0_0_ip_scope
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bresp(1 downto 0) => axibusdomain_s_axi_bresp(1 downto 0),
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rresp(1 downto 0) => axibusdomain_s_axi_rresp(1 downto 0),
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
