-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Feb 14 16:33:17 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               E:/smr3562/iaea/xilinx/projects/dpp-4/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dc_stabilizer_0_0/design_1_ip_dc_stabilizer_0_0_stub.vhdl
-- Design      : design_1_ip_dc_stabilizer_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_ip_dc_stabilizer_0_0 is
  Port ( 
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    feedback : in STD_LOGIC_VECTOR ( 15 downto 0 );
    resetn : in STD_LOGIC;
    clk : in STD_LOGIC;
    dbg_acc : out STD_LOGIC_VECTOR ( 15 downto 0 );
    axi_dc_stab_aclk : in STD_LOGIC;
    axi_dc_stab_aresetn : in STD_LOGIC;
    axi_dc_stab_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_dc_stab_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_dc_stab_awvalid : in STD_LOGIC;
    axi_dc_stab_awready : out STD_LOGIC;
    axi_dc_stab_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_dc_stab_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_dc_stab_wvalid : in STD_LOGIC;
    axi_dc_stab_wready : out STD_LOGIC;
    axi_dc_stab_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_dc_stab_bvalid : out STD_LOGIC;
    axi_dc_stab_bready : in STD_LOGIC;
    axi_dc_stab_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_dc_stab_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_dc_stab_arvalid : in STD_LOGIC;
    axi_dc_stab_arready : out STD_LOGIC;
    axi_dc_stab_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_dc_stab_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_dc_stab_rvalid : out STD_LOGIC;
    axi_dc_stab_rready : in STD_LOGIC
  );

end design_1_ip_dc_stabilizer_0_0;

architecture stub of design_1_ip_dc_stabilizer_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "x[15:0],y[15:0],feedback[15:0],resetn,clk,dbg_acc[15:0],axi_dc_stab_aclk,axi_dc_stab_aresetn,axi_dc_stab_awaddr[3:0],axi_dc_stab_awprot[2:0],axi_dc_stab_awvalid,axi_dc_stab_awready,axi_dc_stab_wdata[31:0],axi_dc_stab_wstrb[3:0],axi_dc_stab_wvalid,axi_dc_stab_wready,axi_dc_stab_bresp[1:0],axi_dc_stab_bvalid,axi_dc_stab_bready,axi_dc_stab_araddr[3:0],axi_dc_stab_arprot[2:0],axi_dc_stab_arvalid,axi_dc_stab_arready,axi_dc_stab_rdata[31:0],axi_dc_stab_rresp[1:0],axi_dc_stab_rvalid,axi_dc_stab_rready";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "ip_dc_stabilizer_v1_0,Vivado 2019.1";
begin
end;
