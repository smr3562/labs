----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/05/2020 08:40:44 AM
-- Design Name: 
-- Module Name: blr_pkg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


package blr_pkg is 

    function f_log2 (x : positive) return natural;
    component blr is 
	generic
		(
            BLR_DELAY_WIDTH	: integer	:= 10;
            BLR_ACCUMULATOR_WIDTH :	integer := 32;
            BLR_DATA_WIDTH	: integer	:= 16
		);
	port 
		(  
            x              : in std_logic_vector(BLR_DATA_WIDTH-1 downto 0);   --top modul does not support VHDL 2008   
            y              : out std_logic_vector(BLR_DATA_WIDTH-1 downto 0);           
            threshold_high : in std_logic_vector(BLR_DATA_WIDTH-1 downto 0);   
            threshold_low  : in std_logic_vector(BLR_DATA_WIDTH-1 downto 0);   
            resetn         : in std_logic;
            clk            : in std_logic;
            blr_enable     : in std_logic;
            m              : in std_logic_vector (f_log2(BLR_DELAY_WIDTH)-1 downto 0)
	  );
    end component;

end package blr_pkg;


package body blr_pkg is

    function f_log2 (x : positive) return natural is
      variable i : natural;
    begin
      i := 0;  
      while (2**i < x) and i < 31 loop
         i := i + 1;
      end loop;
      return i;
    end function;

end blr_pkg;