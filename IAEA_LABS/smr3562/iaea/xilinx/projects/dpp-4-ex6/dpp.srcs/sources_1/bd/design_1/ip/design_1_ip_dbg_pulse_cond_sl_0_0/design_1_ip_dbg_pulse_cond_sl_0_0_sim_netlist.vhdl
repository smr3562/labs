-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Feb 14 16:31:18 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-4/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_pulse_cond_sl_0_0/design_1_ip_dbg_pulse_cond_sl_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_dbg_pulse_cond_sl_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_dbg_pulse_cond_sl_0_0 is
  port (
    impulse : in STD_LOGIC_VECTOR ( 15 downto 0 );
    rect : in STD_LOGIC_VECTOR ( 15 downto 0 );
    shaper : in STD_LOGIC_VECTOR ( 15 downto 0 );
    blr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dc_stab : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dc_stab_acc : in STD_LOGIC_VECTOR ( 15 downto 0 );
    impulse_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rect_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    shaper_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    blr_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    dc_stab_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    dc_stab_acc_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_dbg_pulse_cond_sl_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_dbg_pulse_cond_sl_0_0 : entity is "design_1_ip_dbg_pulse_cond_sl_0_0,top,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_ip_dbg_pulse_cond_sl_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_ip_dbg_pulse_cond_sl_0_0 : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of design_1_ip_dbg_pulse_cond_sl_0_0 : entity is "top,Vivado 2019.1";
end design_1_ip_dbg_pulse_cond_sl_0_0;

architecture STRUCTURE of design_1_ip_dbg_pulse_cond_sl_0_0 is
  signal \^blr\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^dc_stab\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^dc_stab_acc\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^impulse\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^rect\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^shaper\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute x_interface_info : string;
  attribute x_interface_info of blr_out : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 blr";
  attribute x_interface_info of dc_stab_acc_out : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 dc_stab_acc";
  attribute x_interface_info of dc_stab_out : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 dc_stab";
  attribute x_interface_info of impulse_out : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 impulse";
  attribute x_interface_info of rect_out : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 rect";
  attribute x_interface_info of shaper_out : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 shaper";
begin
  \^blr\(15 downto 0) <= blr(15 downto 0);
  \^dc_stab\(15 downto 0) <= dc_stab(15 downto 0);
  \^dc_stab_acc\(15 downto 0) <= dc_stab_acc(15 downto 0);
  \^impulse\(15 downto 0) <= impulse(15 downto 0);
  \^rect\(15 downto 0) <= rect(15 downto 0);
  \^shaper\(15 downto 0) <= shaper(15 downto 0);
  blr_out(15 downto 0) <= \^blr\(15 downto 0);
  dc_stab_acc_out(15 downto 0) <= \^dc_stab_acc\(15 downto 0);
  dc_stab_out(15 downto 0) <= \^dc_stab\(15 downto 0);
  impulse_out(15 downto 0) <= \^impulse\(15 downto 0);
  rect_out(15 downto 0) <= \^rect\(15 downto 0);
  shaper_out(15 downto 0) <= \^shaper\(15 downto 0);
end STRUCTURE;
