// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sun Feb 14 16:31:03 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               E:/smr3562/iaea/xilinx/projects/dpp-4/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_term_pulse_co_0_3/design_1_ip_dbg_term_pulse_co_0_3_sim_netlist.v
// Design      : design_1_ip_dbg_term_pulse_co_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ip_dbg_term_pulse_co_0_3,top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "top,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_ip_dbg_term_pulse_co_0_3
   (impulse_out,
    rect_out,
    shaper_out,
    blr_out,
    dc_stab_out,
    dc_stab_acc_out);
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast impulse" *) output [15:0]impulse_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast rect" *) output [15:0]rect_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast shaper" *) output [15:0]shaper_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast blr" *) output [15:0]blr_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast dc_stab" *) output [15:0]dc_stab_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast dc_stab_acc" *) output [15:0]dc_stab_acc_out;

  wire \<const0> ;

  assign blr_out[15] = \<const0> ;
  assign blr_out[14] = \<const0> ;
  assign blr_out[13] = \<const0> ;
  assign blr_out[12] = \<const0> ;
  assign blr_out[11] = \<const0> ;
  assign blr_out[10] = \<const0> ;
  assign blr_out[9] = \<const0> ;
  assign blr_out[8] = \<const0> ;
  assign blr_out[7] = \<const0> ;
  assign blr_out[6] = \<const0> ;
  assign blr_out[5] = \<const0> ;
  assign blr_out[4] = \<const0> ;
  assign blr_out[3] = \<const0> ;
  assign blr_out[2] = \<const0> ;
  assign blr_out[1] = \<const0> ;
  assign blr_out[0] = \<const0> ;
  assign dc_stab_acc_out[15] = \<const0> ;
  assign dc_stab_acc_out[14] = \<const0> ;
  assign dc_stab_acc_out[13] = \<const0> ;
  assign dc_stab_acc_out[12] = \<const0> ;
  assign dc_stab_acc_out[11] = \<const0> ;
  assign dc_stab_acc_out[10] = \<const0> ;
  assign dc_stab_acc_out[9] = \<const0> ;
  assign dc_stab_acc_out[8] = \<const0> ;
  assign dc_stab_acc_out[7] = \<const0> ;
  assign dc_stab_acc_out[6] = \<const0> ;
  assign dc_stab_acc_out[5] = \<const0> ;
  assign dc_stab_acc_out[4] = \<const0> ;
  assign dc_stab_acc_out[3] = \<const0> ;
  assign dc_stab_acc_out[2] = \<const0> ;
  assign dc_stab_acc_out[1] = \<const0> ;
  assign dc_stab_acc_out[0] = \<const0> ;
  assign dc_stab_out[15] = \<const0> ;
  assign dc_stab_out[14] = \<const0> ;
  assign dc_stab_out[13] = \<const0> ;
  assign dc_stab_out[12] = \<const0> ;
  assign dc_stab_out[11] = \<const0> ;
  assign dc_stab_out[10] = \<const0> ;
  assign dc_stab_out[9] = \<const0> ;
  assign dc_stab_out[8] = \<const0> ;
  assign dc_stab_out[7] = \<const0> ;
  assign dc_stab_out[6] = \<const0> ;
  assign dc_stab_out[5] = \<const0> ;
  assign dc_stab_out[4] = \<const0> ;
  assign dc_stab_out[3] = \<const0> ;
  assign dc_stab_out[2] = \<const0> ;
  assign dc_stab_out[1] = \<const0> ;
  assign dc_stab_out[0] = \<const0> ;
  assign impulse_out[15] = \<const0> ;
  assign impulse_out[14] = \<const0> ;
  assign impulse_out[13] = \<const0> ;
  assign impulse_out[12] = \<const0> ;
  assign impulse_out[11] = \<const0> ;
  assign impulse_out[10] = \<const0> ;
  assign impulse_out[9] = \<const0> ;
  assign impulse_out[8] = \<const0> ;
  assign impulse_out[7] = \<const0> ;
  assign impulse_out[6] = \<const0> ;
  assign impulse_out[5] = \<const0> ;
  assign impulse_out[4] = \<const0> ;
  assign impulse_out[3] = \<const0> ;
  assign impulse_out[2] = \<const0> ;
  assign impulse_out[1] = \<const0> ;
  assign impulse_out[0] = \<const0> ;
  assign rect_out[15] = \<const0> ;
  assign rect_out[14] = \<const0> ;
  assign rect_out[13] = \<const0> ;
  assign rect_out[12] = \<const0> ;
  assign rect_out[11] = \<const0> ;
  assign rect_out[10] = \<const0> ;
  assign rect_out[9] = \<const0> ;
  assign rect_out[8] = \<const0> ;
  assign rect_out[7] = \<const0> ;
  assign rect_out[6] = \<const0> ;
  assign rect_out[5] = \<const0> ;
  assign rect_out[4] = \<const0> ;
  assign rect_out[3] = \<const0> ;
  assign rect_out[2] = \<const0> ;
  assign rect_out[1] = \<const0> ;
  assign rect_out[0] = \<const0> ;
  assign shaper_out[15] = \<const0> ;
  assign shaper_out[14] = \<const0> ;
  assign shaper_out[13] = \<const0> ;
  assign shaper_out[12] = \<const0> ;
  assign shaper_out[11] = \<const0> ;
  assign shaper_out[10] = \<const0> ;
  assign shaper_out[9] = \<const0> ;
  assign shaper_out[8] = \<const0> ;
  assign shaper_out[7] = \<const0> ;
  assign shaper_out[6] = \<const0> ;
  assign shaper_out[5] = \<const0> ;
  assign shaper_out[4] = \<const0> ;
  assign shaper_out[3] = \<const0> ;
  assign shaper_out[2] = \<const0> ;
  assign shaper_out[1] = \<const0> ;
  assign shaper_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
