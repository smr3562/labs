-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Feb 14 16:48:23 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_scope_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_scope_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog is
  port (
    axi_awready_reg_0 : out STD_LOGIC;
    axi_wready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    i : out STD_LOGIC_VECTOR ( 10 downto 0 );
    \slv_reg_array_reg[0][0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[3][15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r3_dina : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg_array_reg[5][0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[6][10]_0\ : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    r9_douta : in STD_LOGIC_VECTOR ( 31 downto 0 );
    q : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog is
  signal axi_araddr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^d\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dec_r__15\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^i\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal r1_addra : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal r2_wea : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \^r3_dina\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r4_threshold : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r5_enable : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal r6_delay : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal r7_clear : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg_array[0][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][12]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][13]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][16]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][17]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][18]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][20]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][21]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][22]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][24]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][25]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][26]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][27]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][28]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][29]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][30]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_6_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][8]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][10]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][12]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][13]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][16]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][17]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][18]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][20]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][21]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][22]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][24]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][25]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][26]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][27]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][28]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][29]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][30]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][8]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][7]_i_2_n_0\ : STD_LOGIC;
  signal \^slv_reg_array_reg[0][0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg_array_reg[3][15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^slv_reg_array_reg[5][0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg_array_reg[6][10]_0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal slv_wire_array : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \slv_reg_array[0][10]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \slv_reg_array[0][11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \slv_reg_array[0][12]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \slv_reg_array[0][13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \slv_reg_array[0][14]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \slv_reg_array[0][15]_i_3\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \slv_reg_array[0][16]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg_array[0][17]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[0][18]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \slv_reg_array[0][19]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \slv_reg_array[0][1]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \slv_reg_array[0][20]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \slv_reg_array[0][21]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \slv_reg_array[0][22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \slv_reg_array[0][23]_i_3\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \slv_reg_array[0][24]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \slv_reg_array[0][25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \slv_reg_array[0][26]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \slv_reg_array[0][27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \slv_reg_array[0][28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \slv_reg_array[0][29]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \slv_reg_array[0][2]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \slv_reg_array[0][30]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \slv_reg_array[0][31]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[0][3]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \slv_reg_array[0][4]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \slv_reg_array[0][5]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \slv_reg_array[0][6]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \slv_reg_array[0][7]_i_3\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \slv_reg_array[0][8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \slv_reg_array[0][9]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \slv_reg_array[1][0]_i_2\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \slv_reg_array[1][11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \slv_reg_array[1][12]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \slv_reg_array[1][13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \slv_reg_array[1][14]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \slv_reg_array[1][15]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \slv_reg_array[1][16]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg_array[1][17]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[1][18]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \slv_reg_array[1][19]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \slv_reg_array[1][1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg_array[1][20]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \slv_reg_array[1][21]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \slv_reg_array[1][22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \slv_reg_array[1][23]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \slv_reg_array[1][24]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \slv_reg_array[1][25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \slv_reg_array[1][26]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \slv_reg_array[1][27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \slv_reg_array[1][28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \slv_reg_array[1][29]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \slv_reg_array[1][30]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \slv_reg_array[1][31]_i_2\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[1][3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \slv_reg_array[1][4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \slv_reg_array[1][7]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \slv_reg_array[1][8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \slv_reg_array[2][6]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[3][1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg_array[3][3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \slv_reg_array[3][4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \slv_reg_array[3][6]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[3][7]_i_2\ : label is "soft_lutpair6";
begin
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  d(0) <= \^d\(0);
  i(10 downto 0) <= \^i\(10 downto 0);
  r3_dina(31 downto 0) <= \^r3_dina\(31 downto 0);
  \slv_reg_array_reg[0][0]_0\(0) <= \^slv_reg_array_reg[0][0]_0\(0);
  \slv_reg_array_reg[3][15]_0\(15 downto 0) <= \^slv_reg_array_reg[3][15]_0\(15 downto 0);
  \slv_reg_array_reg[5][0]_0\(0) <= \^slv_reg_array_reg[5][0]_0\(0);
  \slv_reg_array_reg[6][10]_0\(10 downto 0) <= \^slv_reg_array_reg[6][10]_0\(10 downto 0);
\axi_araddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(0),
      Q => axi_araddr(0),
      R => p_0_in
    );
\axi_araddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(1),
      Q => axi_araddr(1),
      R => p_0_in
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(2),
      Q => axi_araddr(2),
      R => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(3),
      Q => axi_araddr(3),
      R => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(4),
      Q => axi_araddr(4),
      R => p_0_in
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(5),
      Q => axi_araddr(5),
      R => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => p_0_in
    );
\axi_awaddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(0),
      Q => axi_awaddr(0),
      R => p_0_in
    );
\axi_awaddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(1),
      Q => axi_awaddr(1),
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(2),
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(3),
      Q => axi_awaddr(3),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(4),
      Q => axi_awaddr(4),
      R => p_0_in
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(5),
      Q => axi_awaddr(5),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axibusdomain_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => p_0_in
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => axibusdomain_s_axi_bvalid,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(0),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[0]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[0]_i_3_n_0\,
      O => slv_wire_array(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => q(0),
      I1 => \^slv_reg_array_reg[6][10]_0\(0),
      I2 => \dec_r__15\(1),
      I3 => \^slv_reg_array_reg[5][0]_0\(0),
      I4 => \dec_r__15\(0),
      I5 => \^r3_dina\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(0),
      I1 => \^d\(0),
      I2 => \dec_r__15\(1),
      I3 => \^i\(0),
      I4 => \dec_r__15\(0),
      I5 => \^slv_reg_array_reg[0][0]_0\(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(10),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[10]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[10]_i_3_n_0\,
      O => slv_wire_array(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(10),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(10),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(10),
      I1 => r5_enable(10),
      I2 => \dec_r__15\(1),
      I3 => \^i\(10),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(11),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[11]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[11]_i_3_n_0\,
      O => slv_wire_array(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(11),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(11),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(11),
      I1 => r5_enable(11),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(11),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(12),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[12]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[12]_i_3_n_0\,
      O => slv_wire_array(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(12),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(12),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(12),
      I1 => r5_enable(12),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(12),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(13),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[13]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[13]_i_3_n_0\,
      O => slv_wire_array(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(13),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(13),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(13),
      I1 => r5_enable(13),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(13),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(14),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[14]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[14]_i_3_n_0\,
      O => slv_wire_array(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(14),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(14),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(14),
      I1 => r5_enable(14),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(14),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(15),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[15]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[15]_i_3_n_0\,
      O => slv_wire_array(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(15),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(15),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(15),
      I1 => r5_enable(15),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(15),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(16),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[16]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[16]_i_3_n_0\,
      O => slv_wire_array(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(16),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(16),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(16),
      I1 => r5_enable(16),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(16),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(17),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[17]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[17]_i_3_n_0\,
      O => slv_wire_array(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(17),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(17),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(17),
      I1 => r5_enable(17),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(17),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(18),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[18]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[18]_i_3_n_0\,
      O => slv_wire_array(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(18),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(18),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(18),
      I1 => r5_enable(18),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(18),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(19),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[19]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[19]_i_3_n_0\,
      O => slv_wire_array(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(19),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(19),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(19),
      I1 => r5_enable(19),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(19),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(1),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[1]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[1]_i_3_n_0\,
      O => slv_wire_array(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(1),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(1),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(1),
      I1 => r5_enable(1),
      I2 => \dec_r__15\(1),
      I3 => \^i\(1),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(20),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[20]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[20]_i_3_n_0\,
      O => slv_wire_array(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(20),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(20),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(20),
      I1 => r5_enable(20),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(20),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(21),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[21]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[21]_i_3_n_0\,
      O => slv_wire_array(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(21),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(21),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(21),
      I1 => r5_enable(21),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(21),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(22),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[22]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[22]_i_3_n_0\,
      O => slv_wire_array(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(22),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(22),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(22),
      I1 => r5_enable(22),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(22),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(23),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[23]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[23]_i_3_n_0\,
      O => slv_wire_array(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(23),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(23),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(23),
      I1 => r5_enable(23),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(23),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(24),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[24]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[24]_i_3_n_0\,
      O => slv_wire_array(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(24),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(24),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(24),
      I1 => r5_enable(24),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(24),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(25),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[25]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[25]_i_3_n_0\,
      O => slv_wire_array(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(25),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(25),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(25),
      I1 => r5_enable(25),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(25),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(26),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[26]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[26]_i_3_n_0\,
      O => slv_wire_array(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(26),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(26),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(26),
      I1 => r5_enable(26),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(26),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(27),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[27]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[27]_i_3_n_0\,
      O => slv_wire_array(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(27),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(27),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(27),
      I1 => r5_enable(27),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(27),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(28),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[28]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[28]_i_3_n_0\,
      O => slv_wire_array(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(28),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(28),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(28),
      I1 => r5_enable(28),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(28),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(29),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[29]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[29]_i_3_n_0\,
      O => slv_wire_array(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(29),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(29),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(29),
      I1 => r5_enable(29),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(29),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(2),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[2]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[2]_i_3_n_0\,
      O => slv_wire_array(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(2),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(2),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(2),
      I1 => r5_enable(2),
      I2 => \dec_r__15\(1),
      I3 => \^i\(2),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(30),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[30]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[30]_i_3_n_0\,
      O => slv_wire_array(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(30),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(30),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(30),
      I1 => r5_enable(30),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(30),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(31),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[31]_i_3_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[31]_i_5_n_0\,
      O => slv_wire_array(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => axi_araddr(0),
      I1 => axi_araddr(4),
      I2 => axi_araddr(1),
      I3 => axi_araddr(5),
      I4 => axi_araddr(3),
      I5 => axi_araddr(2),
      O => \dec_r__15\(3)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(31),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(31),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(4),
      I3 => axi_araddr(1),
      O => \dec_r__15\(2)
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(31),
      I1 => r5_enable(31),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(31),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(31),
      O => \axi_rdata[31]_i_5_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(3),
      I3 => axi_araddr(1),
      O => \dec_r__15\(1)
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(2),
      I3 => axi_araddr(1),
      O => \dec_r__15\(0)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(3),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[3]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[3]_i_3_n_0\,
      O => slv_wire_array(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(3),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(3),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(3),
      I1 => r5_enable(3),
      I2 => \dec_r__15\(1),
      I3 => \^i\(3),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(4),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[4]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[4]_i_3_n_0\,
      O => slv_wire_array(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(4),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(4),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(4),
      I1 => r5_enable(4),
      I2 => \dec_r__15\(1),
      I3 => \^i\(4),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(5),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[5]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[5]_i_3_n_0\,
      O => slv_wire_array(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(5),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(5),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(5),
      I1 => r5_enable(5),
      I2 => \dec_r__15\(1),
      I3 => \^i\(5),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(6),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[6]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[6]_i_3_n_0\,
      O => slv_wire_array(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(6),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(6),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(6),
      I1 => r5_enable(6),
      I2 => \dec_r__15\(1),
      I3 => \^i\(6),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(7),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[7]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[7]_i_3_n_0\,
      O => slv_wire_array(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(7),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(7),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(7),
      I1 => r5_enable(7),
      I2 => \dec_r__15\(1),
      I3 => \^i\(7),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(8),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[8]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[8]_i_3_n_0\,
      O => slv_wire_array(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(8),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(8),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(8),
      I1 => r5_enable(8),
      I2 => \dec_r__15\(1),
      I3 => \^i\(8),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(9),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[9]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[9]_i_3_n_0\,
      O => slv_wire_array(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(9),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(9),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(9),
      I1 => r5_enable(9),
      I2 => \dec_r__15\(1),
      I3 => \^i\(9),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(0),
      Q => axibusdomain_s_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(10),
      Q => axibusdomain_s_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(11),
      Q => axibusdomain_s_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(12),
      Q => axibusdomain_s_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(13),
      Q => axibusdomain_s_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(14),
      Q => axibusdomain_s_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(15),
      Q => axibusdomain_s_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(16),
      Q => axibusdomain_s_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(17),
      Q => axibusdomain_s_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(18),
      Q => axibusdomain_s_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(19),
      Q => axibusdomain_s_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(1),
      Q => axibusdomain_s_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(20),
      Q => axibusdomain_s_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(21),
      Q => axibusdomain_s_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(22),
      Q => axibusdomain_s_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(23),
      Q => axibusdomain_s_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(24),
      Q => axibusdomain_s_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(25),
      Q => axibusdomain_s_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(26),
      Q => axibusdomain_s_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(27),
      Q => axibusdomain_s_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(28),
      Q => axibusdomain_s_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(29),
      Q => axibusdomain_s_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(2),
      Q => axibusdomain_s_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(30),
      Q => axibusdomain_s_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(31),
      Q => axibusdomain_s_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(3),
      Q => axibusdomain_s_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(4),
      Q => axibusdomain_s_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(5),
      Q => axibusdomain_s_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(6),
      Q => axibusdomain_s_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(7),
      Q => axibusdomain_s_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(8),
      Q => axibusdomain_s_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(9),
      Q => axibusdomain_s_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^axibusdomain_s_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => p_0_in
    );
\slv_reg_array[0][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8FFF000080000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      I3 => \slv_reg_array[0][31]_i_4_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[0][0]_0\(0),
      O => \slv_reg_array[0][0]_i_1_n_0\
    );
\slv_reg_array[0][10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(10),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][10]_i_1_n_0\
    );
\slv_reg_array[0][11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(11),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][11]_i_1_n_0\
    );
\slv_reg_array[0][12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(12),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][12]_i_1_n_0\
    );
\slv_reg_array[0][13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(13),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][13]_i_1_n_0\
    );
\slv_reg_array[0][14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(14),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][14]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      O => \slv_reg_array[0][15]_i_2_n_0\
    );
\slv_reg_array[0][15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(15),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][15]_i_3_n_0\
    );
\slv_reg_array[0][15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => axibusdomain_aresetn,
      I5 => axibusdomain_s_axi_wstrb(1),
      O => \slv_reg_array[0][15]_i_4_n_0\
    );
\slv_reg_array[0][15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][15]_i_5_n_0\
    );
\slv_reg_array[0][16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(16),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][16]_i_1_n_0\
    );
\slv_reg_array[0][17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(17),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][17]_i_1_n_0\
    );
\slv_reg_array[0][18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(18),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][18]_i_1_n_0\
    );
\slv_reg_array[0][19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(19),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][19]_i_1_n_0\
    );
\slv_reg_array[0][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(1),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][1]_i_1_n_0\
    );
\slv_reg_array[0][20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(20),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][20]_i_1_n_0\
    );
\slv_reg_array[0][21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(21),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][21]_i_1_n_0\
    );
\slv_reg_array[0][22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(22),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][22]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][23]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][23]_i_4_n_0\,
      O => \slv_reg_array[0][23]_i_2_n_0\
    );
\slv_reg_array[0][23]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(23),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][23]_i_3_n_0\
    );
\slv_reg_array[0][23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_4_n_0\
    );
\slv_reg_array[0][23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(2),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_5_n_0\
    );
\slv_reg_array[0][24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(24),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][24]_i_1_n_0\
    );
\slv_reg_array[0][25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(25),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][25]_i_1_n_0\
    );
\slv_reg_array[0][26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(26),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][26]_i_1_n_0\
    );
\slv_reg_array[0][27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(27),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][27]_i_1_n_0\
    );
\slv_reg_array[0][28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(28),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][28]_i_1_n_0\
    );
\slv_reg_array[0][29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(29),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][29]_i_1_n_0\
    );
\slv_reg_array[0][2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(2),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][2]_i_1_n_0\
    );
\slv_reg_array[0][30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(30),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][30]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][31]_i_2_n_0\
    );
\slv_reg_array[0][31]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(31),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][31]_i_3_n_0\
    );
\slv_reg_array[0][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFCFFFCFFFCFEFD"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[0][31]_i_4_n_0\
    );
\slv_reg_array[0][31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_5_n_0\
    );
\slv_reg_array[0][31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(3),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_6_n_0\
    );
\slv_reg_array[0][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(3),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][3]_i_1_n_0\
    );
\slv_reg_array[0][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(4),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][4]_i_1_n_0\
    );
\slv_reg_array[0][5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(5),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][5]_i_1_n_0\
    );
\slv_reg_array[0][6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(6),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][6]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      O => \slv_reg_array[0][7]_i_2_n_0\
    );
\slv_reg_array[0][7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(7),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][7]_i_3_n_0\
    );
\slv_reg_array[0][7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_4_n_0\
    );
\slv_reg_array[0][7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(0),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_5_n_0\
    );
\slv_reg_array[0][8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(8),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][8]_i_1_n_0\
    );
\slv_reg_array[0][9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(9),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][9]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4FFF404040004040"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => \slv_reg_array[1][0]_i_2_n_0\,
      I2 => \slv_reg_array[1][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^i\(0),
      O => \slv_reg_array[1][0]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(0),
      O => \slv_reg_array[1][0]_i_2_n_0\
    );
\slv_reg_array[1][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDD0200"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(10),
      I4 => \^i\(10),
      O => \slv_reg_array[1][10]_i_1_n_0\
    );
\slv_reg_array[1][10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][10]_i_2_n_0\
    );
\slv_reg_array[1][11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(11),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][11]_i_1_n_0\
    );
\slv_reg_array[1][12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(12),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][12]_i_1_n_0\
    );
\slv_reg_array[1][13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(13),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][13]_i_1_n_0\
    );
\slv_reg_array[1][14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(14),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array[1][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      O => \slv_reg_array[1][15]_i_1_n_0\
    );
\slv_reg_array[1][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(15),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][15]_i_2_n_0\
    );
\slv_reg_array[1][16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(16),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][16]_i_1_n_0\
    );
\slv_reg_array[1][17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(17),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][17]_i_1_n_0\
    );
\slv_reg_array[1][18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(18),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][18]_i_1_n_0\
    );
\slv_reg_array[1][19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(19),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][19]_i_1_n_0\
    );
\slv_reg_array[1][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(1),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][1]_i_1_n_0\
    );
\slv_reg_array[1][20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(20),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][20]_i_1_n_0\
    );
\slv_reg_array[1][21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(21),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][21]_i_1_n_0\
    );
\slv_reg_array[1][22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(22),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][22]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      O => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(23),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][23]_i_2_n_0\
    );
\slv_reg_array[1][24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(24),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][24]_i_1_n_0\
    );
\slv_reg_array[1][25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(25),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][25]_i_1_n_0\
    );
\slv_reg_array[1][26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(26),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][26]_i_1_n_0\
    );
\slv_reg_array[1][27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(27),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][27]_i_1_n_0\
    );
\slv_reg_array[1][28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(28),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][28]_i_1_n_0\
    );
\slv_reg_array[1][29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(29),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][29]_i_1_n_0\
    );
\slv_reg_array[1][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(2),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(2),
      O => \slv_reg_array[1][2]_i_1_n_0\
    );
\slv_reg_array[1][30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(30),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][30]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      O => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(31),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][31]_i_2_n_0\
    );
\slv_reg_array[1][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][31]_i_3_n_0\
    );
\slv_reg_array[1][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \slv_reg_array[1][10]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[1][31]_i_4_n_0\
    );
\slv_reg_array[1][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(3),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][3]_i_1_n_0\
    );
\slv_reg_array[1][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(4),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][4]_i_1_n_0\
    );
\slv_reg_array[1][5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(5),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(5),
      O => \slv_reg_array[1][5]_i_1_n_0\
    );
\slv_reg_array[1][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(6),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(6),
      O => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array[1][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      O => \slv_reg_array[1][7]_i_1_n_0\
    );
\slv_reg_array[1][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(7),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][7]_i_2_n_0\
    );
\slv_reg_array[1][8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(8),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][8]_i_1_n_0\
    );
\slv_reg_array[1][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDD0200"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(9),
      I4 => \^i\(9),
      O => \slv_reg_array[1][9]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFEF00"
    )
        port map (
      I0 => \slv_reg_array[2][6]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \slv_reg_array[2][0]_i_2_n_0\,
      I4 => \^d\(0),
      O => \slv_reg_array[2][0]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \slv_reg_array[2][31]_i_2_n_0\,
      I1 => \slv_reg_array[0][7]_i_5_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[2][0]_i_2_n_0\
    );
\slv_reg_array[2][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE0100"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(10),
      I4 => r5_enable(10),
      O => \slv_reg_array[2][10]_i_1_n_0\
    );
\slv_reg_array[2][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array[2][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array[2][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(2),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(2),
      O => \slv_reg_array[2][2]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(5),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[2][31]_i_2_n_0\
    );
\slv_reg_array[2][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(5),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(5),
      O => \slv_reg_array[2][5]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(6),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(6),
      O => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      O => \slv_reg_array[2][6]_i_2_n_0\
    );
\slv_reg_array[2][6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000F0001000A"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[2][6]_i_3_n_0\
    );
\slv_reg_array[2][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][7]_i_1_n_0\
    );
\slv_reg_array[2][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE0100"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(9),
      I4 => r5_enable(9),
      O => \slv_reg_array[2][9]_i_1_n_0\
    );
\slv_reg_array[3][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4FF040404000404"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => \slv_reg_array[1][0]_i_2_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[3][15]_0\(0),
      O => \slv_reg_array[3][0]_i_1_n_0\
    );
\slv_reg_array[3][10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(10),
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(10),
      O => \slv_reg_array[3][10]_i_1_n_0\
    );
\slv_reg_array[3][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array[3][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(1),
      O => \slv_reg_array[3][1]_i_1_n_0\
    );
\slv_reg_array[3][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array[3][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(2),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][2]_i_1_n_0\
    );
\slv_reg_array[3][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array[3][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(3),
      O => \slv_reg_array[3][3]_i_1_n_0\
    );
\slv_reg_array[3][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(4),
      O => \slv_reg_array[3][4]_i_1_n_0\
    );
\slv_reg_array[3][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(5),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][5]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(6),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][6]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(5),
      O => \slv_reg_array[3][6]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array[3][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(7),
      O => \slv_reg_array[3][7]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFDFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(5),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(1),
      I4 => axi_awaddr(4),
      I5 => axi_awaddr(3),
      O => \slv_reg_array[3][7]_i_3_n_0\
    );
\slv_reg_array[3][9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(9),
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(9),
      O => \slv_reg_array[3][9]_i_1_n_0\
    );
\slv_reg_array[4][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[4][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^r3_dina\(0),
      O => \slv_reg_array[4][0]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][15]_i_2_n_0\
    );
\slv_reg_array[4][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array[4][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][23]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array[4][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][31]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[4][31]_i_3_n_0\
    );
\slv_reg_array[4][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array[4][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][7]_i_2_n_0\
    );
\slv_reg_array[5][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[5][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[5][0]_0\(0),
      O => \slv_reg_array[5][0]_i_1_n_0\
    );
\slv_reg_array[5][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array[5][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][15]_i_2_n_0\
    );
\slv_reg_array[5][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array[5][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][23]_i_2_n_0\
    );
\slv_reg_array[5][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array[5][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][31]_i_2_n_0\
    );
\slv_reg_array[5][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFFFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(5),
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[5][31]_i_3_n_0\
    );
\slv_reg_array[5][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array[5][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][7]_i_2_n_0\
    );
\slv_reg_array[6][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[6][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[6][10]_0\(0),
      O => \slv_reg_array[6][0]_i_1_n_0\
    );
\slv_reg_array[6][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array[6][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][15]_i_2_n_0\
    );
\slv_reg_array[6][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array[6][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][23]_i_2_n_0\
    );
\slv_reg_array[6][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array[6][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][31]_i_2_n_0\
    );
\slv_reg_array[6][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[6][31]_i_3_n_0\
    );
\slv_reg_array[6][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array[6][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][7]_i_2_n_0\
    );
\slv_reg_array_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[0][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[0][0]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[0][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => r7_clear(10),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r7_clear(11),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r7_clear(12),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r7_clear(13),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r7_clear(14),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r7_clear(15),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r7_clear(16),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r7_clear(17),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r7_clear(18),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r7_clear(19),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => r7_clear(1),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r7_clear(20),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r7_clear(21),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r7_clear(22),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r7_clear(23),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r7_clear(24),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r7_clear(25),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r7_clear(26),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r7_clear(27),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r7_clear(28),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r7_clear(29),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => r7_clear(2),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r7_clear(30),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r7_clear(31),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => r7_clear(3),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => r7_clear(4),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => r7_clear(5),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => r7_clear(6),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => r7_clear(7),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => r7_clear(8),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => r7_clear(9),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[1][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][0]_i_1_n_0\,
      Q => \^i\(0),
      R => '0'
    );
\slv_reg_array_reg[1][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][10]_i_1_n_0\,
      Q => \^i\(10),
      R => '0'
    );
\slv_reg_array_reg[1][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => r6_delay(11),
      R => '0'
    );
\slv_reg_array_reg[1][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => r6_delay(12),
      R => '0'
    );
\slv_reg_array_reg[1][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => r6_delay(13),
      R => '0'
    );
\slv_reg_array_reg[1][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => r6_delay(14),
      R => '0'
    );
\slv_reg_array_reg[1][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => r6_delay(15),
      R => '0'
    );
\slv_reg_array_reg[1][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r6_delay(16),
      R => '0'
    );
\slv_reg_array_reg[1][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r6_delay(17),
      R => '0'
    );
\slv_reg_array_reg[1][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r6_delay(18),
      R => '0'
    );
\slv_reg_array_reg[1][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r6_delay(19),
      R => '0'
    );
\slv_reg_array_reg[1][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => \^i\(1),
      R => '0'
    );
\slv_reg_array_reg[1][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r6_delay(20),
      R => '0'
    );
\slv_reg_array_reg[1][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r6_delay(21),
      R => '0'
    );
\slv_reg_array_reg[1][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r6_delay(22),
      R => '0'
    );
\slv_reg_array_reg[1][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r6_delay(23),
      R => '0'
    );
\slv_reg_array_reg[1][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r6_delay(24),
      R => '0'
    );
\slv_reg_array_reg[1][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r6_delay(25),
      R => '0'
    );
\slv_reg_array_reg[1][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r6_delay(26),
      R => '0'
    );
\slv_reg_array_reg[1][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r6_delay(27),
      R => '0'
    );
\slv_reg_array_reg[1][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r6_delay(28),
      R => '0'
    );
\slv_reg_array_reg[1][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r6_delay(29),
      R => '0'
    );
\slv_reg_array_reg[1][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][2]_i_1_n_0\,
      Q => \^i\(2),
      R => '0'
    );
\slv_reg_array_reg[1][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r6_delay(30),
      R => '0'
    );
\slv_reg_array_reg[1][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r6_delay(31),
      R => '0'
    );
\slv_reg_array_reg[1][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][3]_i_1_n_0\,
      Q => \^i\(3),
      R => '0'
    );
\slv_reg_array_reg[1][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][4]_i_1_n_0\,
      Q => \^i\(4),
      R => '0'
    );
\slv_reg_array_reg[1][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][5]_i_1_n_0\,
      Q => \^i\(5),
      R => '0'
    );
\slv_reg_array_reg[1][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][6]_i_1_n_0\,
      Q => \^i\(6),
      R => '0'
    );
\slv_reg_array_reg[1][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][7]_i_2_n_0\,
      Q => \^i\(7),
      R => '0'
    );
\slv_reg_array_reg[1][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => \^i\(8),
      R => '0'
    );
\slv_reg_array_reg[1][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][9]_i_1_n_0\,
      Q => \^i\(9),
      R => '0'
    );
\slv_reg_array_reg[2][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][0]_i_1_n_0\,
      Q => \^d\(0),
      R => '0'
    );
\slv_reg_array_reg[2][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][10]_i_1_n_0\,
      Q => r5_enable(10),
      R => '0'
    );
\slv_reg_array_reg[2][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => r5_enable(11),
      R => '0'
    );
\slv_reg_array_reg[2][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => r5_enable(12),
      R => '0'
    );
\slv_reg_array_reg[2][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => r5_enable(13),
      R => '0'
    );
\slv_reg_array_reg[2][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => r5_enable(14),
      R => '0'
    );
\slv_reg_array_reg[2][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => r5_enable(15),
      R => '0'
    );
\slv_reg_array_reg[2][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r5_enable(16),
      R => '0'
    );
\slv_reg_array_reg[2][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r5_enable(17),
      R => '0'
    );
\slv_reg_array_reg[2][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r5_enable(18),
      R => '0'
    );
\slv_reg_array_reg[2][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r5_enable(19),
      R => '0'
    );
\slv_reg_array_reg[2][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => r5_enable(1),
      R => '0'
    );
\slv_reg_array_reg[2][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r5_enable(20),
      R => '0'
    );
\slv_reg_array_reg[2][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r5_enable(21),
      R => '0'
    );
\slv_reg_array_reg[2][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r5_enable(22),
      R => '0'
    );
\slv_reg_array_reg[2][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r5_enable(23),
      R => '0'
    );
\slv_reg_array_reg[2][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r5_enable(24),
      R => '0'
    );
\slv_reg_array_reg[2][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r5_enable(25),
      R => '0'
    );
\slv_reg_array_reg[2][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r5_enable(26),
      R => '0'
    );
\slv_reg_array_reg[2][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r5_enable(27),
      R => '0'
    );
\slv_reg_array_reg[2][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r5_enable(28),
      R => '0'
    );
\slv_reg_array_reg[2][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r5_enable(29),
      R => '0'
    );
\slv_reg_array_reg[2][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][2]_i_1_n_0\,
      Q => r5_enable(2),
      R => '0'
    );
\slv_reg_array_reg[2][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r5_enable(30),
      R => '0'
    );
\slv_reg_array_reg[2][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r5_enable(31),
      R => '0'
    );
\slv_reg_array_reg[2][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][3]_i_1_n_0\,
      Q => r5_enable(3),
      R => '0'
    );
\slv_reg_array_reg[2][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][4]_i_1_n_0\,
      Q => r5_enable(4),
      R => '0'
    );
\slv_reg_array_reg[2][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][5]_i_1_n_0\,
      Q => r5_enable(5),
      R => '0'
    );
\slv_reg_array_reg[2][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][6]_i_1_n_0\,
      Q => r5_enable(6),
      R => '0'
    );
\slv_reg_array_reg[2][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][7]_i_2_n_0\,
      Q => r5_enable(7),
      R => '0'
    );
\slv_reg_array_reg[2][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => r5_enable(8),
      R => '0'
    );
\slv_reg_array_reg[2][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][9]_i_1_n_0\,
      Q => r5_enable(9),
      R => '0'
    );
\slv_reg_array_reg[3][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[3][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][10]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(10),
      R => '0'
    );
\slv_reg_array_reg[3][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(11),
      R => '0'
    );
\slv_reg_array_reg[3][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(12),
      R => '0'
    );
\slv_reg_array_reg[3][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(13),
      R => '0'
    );
\slv_reg_array_reg[3][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(14),
      R => '0'
    );
\slv_reg_array_reg[3][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(15),
      R => '0'
    );
\slv_reg_array_reg[3][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r4_threshold(16),
      R => '0'
    );
\slv_reg_array_reg[3][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r4_threshold(17),
      R => '0'
    );
\slv_reg_array_reg[3][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r4_threshold(18),
      R => '0'
    );
\slv_reg_array_reg[3][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r4_threshold(19),
      R => '0'
    );
\slv_reg_array_reg[3][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(1),
      R => '0'
    );
\slv_reg_array_reg[3][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r4_threshold(20),
      R => '0'
    );
\slv_reg_array_reg[3][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r4_threshold(21),
      R => '0'
    );
\slv_reg_array_reg[3][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r4_threshold(22),
      R => '0'
    );
\slv_reg_array_reg[3][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r4_threshold(23),
      R => '0'
    );
\slv_reg_array_reg[3][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r4_threshold(24),
      R => '0'
    );
\slv_reg_array_reg[3][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r4_threshold(25),
      R => '0'
    );
\slv_reg_array_reg[3][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r4_threshold(26),
      R => '0'
    );
\slv_reg_array_reg[3][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r4_threshold(27),
      R => '0'
    );
\slv_reg_array_reg[3][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r4_threshold(28),
      R => '0'
    );
\slv_reg_array_reg[3][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r4_threshold(29),
      R => '0'
    );
\slv_reg_array_reg[3][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(2),
      R => '0'
    );
\slv_reg_array_reg[3][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r4_threshold(30),
      R => '0'
    );
\slv_reg_array_reg[3][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r4_threshold(31),
      R => '0'
    );
\slv_reg_array_reg[3][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(3),
      R => '0'
    );
\slv_reg_array_reg[3][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(4),
      R => '0'
    );
\slv_reg_array_reg[3][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(5),
      R => '0'
    );
\slv_reg_array_reg[3][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(6),
      R => '0'
    );
\slv_reg_array_reg[3][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][7]_i_2_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(7),
      R => '0'
    );
\slv_reg_array_reg[3][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(8),
      R => '0'
    );
\slv_reg_array_reg[3][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][9]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(9),
      R => '0'
    );
\slv_reg_array_reg[4][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][0]_i_1_n_0\,
      Q => \^r3_dina\(0),
      R => '0'
    );
\slv_reg_array_reg[4][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => \^r3_dina\(10),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => \^r3_dina\(11),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => \^r3_dina\(12),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => \^r3_dina\(13),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => \^r3_dina\(14),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => \^r3_dina\(15),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => \^r3_dina\(16),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => \^r3_dina\(17),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => \^r3_dina\(18),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => \^r3_dina\(19),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => \^r3_dina\(1),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => \^r3_dina\(20),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => \^r3_dina\(21),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => \^r3_dina\(22),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => \^r3_dina\(23),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => \^r3_dina\(24),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => \^r3_dina\(25),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => \^r3_dina\(26),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => \^r3_dina\(27),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => \^r3_dina\(28),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => \^r3_dina\(29),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => \^r3_dina\(2),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => \^r3_dina\(30),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => \^r3_dina\(31),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => \^r3_dina\(3),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => \^r3_dina\(4),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => \^r3_dina\(5),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => \^r3_dina\(6),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => \^r3_dina\(7),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => \^r3_dina\(8),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => \^r3_dina\(9),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[5][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[5][0]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[5][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => r2_wea(10),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r2_wea(11),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r2_wea(12),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r2_wea(13),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r2_wea(14),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r2_wea(15),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r2_wea(16),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r2_wea(17),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r2_wea(18),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r2_wea(19),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => r2_wea(1),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r2_wea(20),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r2_wea(21),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r2_wea(22),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r2_wea(23),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r2_wea(24),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r2_wea(25),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r2_wea(26),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r2_wea(27),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r2_wea(28),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r2_wea(29),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => r2_wea(2),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r2_wea(30),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r2_wea(31),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => r2_wea(3),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => r2_wea(4),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => r2_wea(5),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => r2_wea(6),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => r2_wea(7),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => r2_wea(8),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => r2_wea(9),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[6][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[6][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(10),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r1_addra(11),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r1_addra(12),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r1_addra(13),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r1_addra(14),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r1_addra(15),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r1_addra(16),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r1_addra(17),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r1_addra(18),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r1_addra(19),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(1),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r1_addra(20),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r1_addra(21),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r1_addra(22),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r1_addra(23),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r1_addra(24),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r1_addra(25),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r1_addra(26),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r1_addra(27),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r1_addra(28),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r1_addra(29),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(2),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r1_addra(30),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r1_addra(31),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(3),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(4),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(5),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(6),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(7),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(8),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(9),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axibusdomain_s_axi_rvalid\,
      I2 => \^axi_arready_reg_0\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a is
  port (
    \ff_2_17_reg[0]_0\ : out STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a is
begin
\ff_2_17_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => register4_q_net(0),
      Q => \ff_2_17_reg[0]_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d is
  port (
    \full_i_5_24_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    register_q_net : in STD_LOGIC;
    \state_4_23_reg[1]_2\ : in STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d is
  signal \addr_i_6_24[10]_i_1_n_0\ : STD_LOGIC;
  signal \addr_i_6_24[10]_i_2_n_0\ : STD_LOGIC;
  signal \addr_i_6_24[7]_i_2_n_0\ : STD_LOGIC;
  signal \^addrb\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \full_i_5_24[0]_i_1_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal rel_39_16 : STD_LOGIC;
  signal \rel_39_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_39_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_39_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_39_16_carry_n_0 : STD_LOGIC;
  signal rel_39_16_carry_n_1 : STD_LOGIC;
  signal rel_39_16_carry_n_2 : STD_LOGIC;
  signal rel_39_16_carry_n_3 : STD_LOGIC;
  signal state_4_23 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \state_4_23[0]_i_1_n_0\ : STD_LOGIC;
  signal \state_4_23[0]_i_2_n_0\ : STD_LOGIC;
  signal \state_4_23[1]_i_1_n_0\ : STD_LOGIC;
  signal wm_8_20_inv_i_1_n_0 : STD_LOGIC;
  signal wm_8_20_reg_inv_n_0 : STD_LOGIC;
  signal NLW_rel_39_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_39_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \addr_i_6_24[0]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addr_i_6_24[1]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addr_i_6_24[2]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \addr_i_6_24[3]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \addr_i_6_24[4]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \addr_i_6_24[7]_i_2\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \addr_i_6_24[9]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \full_i_5_24[0]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \state_4_23[0]_i_2\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of wm_8_20_inv_i_1 : label is "soft_lutpair35";
begin
  addrb(10 downto 0) <= \^addrb\(10 downto 0);
\addr_i_6_24[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^addrb\(0),
      O => p_0_in(0)
    );
\addr_i_6_24[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^addrb\(10),
      I1 => \addr_i_6_24[10]_i_2_n_0\,
      I2 => \^addrb\(7),
      I3 => \^addrb\(6),
      I4 => \^addrb\(8),
      I5 => \^addrb\(9),
      O => \addr_i_6_24[10]_i_1_n_0\
    );
\addr_i_6_24[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^addrb\(2),
      I1 => \^addrb\(0),
      I2 => \^addrb\(1),
      I3 => \^addrb\(3),
      I4 => \^addrb\(4),
      I5 => \^addrb\(5),
      O => \addr_i_6_24[10]_i_2_n_0\
    );
\addr_i_6_24[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^addrb\(0),
      I1 => \^addrb\(1),
      O => p_0_in(1)
    );
\addr_i_6_24[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^addrb\(2),
      I1 => \^addrb\(0),
      I2 => \^addrb\(1),
      O => p_0_in(2)
    );
\addr_i_6_24[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^addrb\(1),
      I1 => \^addrb\(0),
      I2 => \^addrb\(2),
      I3 => \^addrb\(3),
      O => p_0_in(3)
    );
\addr_i_6_24[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^addrb\(4),
      I1 => \^addrb\(1),
      I2 => \^addrb\(0),
      I3 => \^addrb\(2),
      I4 => \^addrb\(3),
      O => p_0_in(4)
    );
\addr_i_6_24[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^addrb\(5),
      I1 => \^addrb\(3),
      I2 => \^addrb\(2),
      I3 => \^addrb\(0),
      I4 => \^addrb\(1),
      I5 => \^addrb\(4),
      O => p_0_in(5)
    );
\addr_i_6_24[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA6AAA"
    )
        port map (
      I0 => \^addrb\(6),
      I1 => \^addrb\(5),
      I2 => \^addrb\(4),
      I3 => \^addrb\(3),
      I4 => \addr_i_6_24[7]_i_2_n_0\,
      O => p_0_in(6)
    );
\addr_i_6_24[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^addrb\(7),
      I1 => \addr_i_6_24[7]_i_2_n_0\,
      I2 => \^addrb\(3),
      I3 => \^addrb\(4),
      I4 => \^addrb\(5),
      I5 => \^addrb\(6),
      O => p_0_in(7)
    );
\addr_i_6_24[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \^addrb\(1),
      I1 => \^addrb\(0),
      I2 => \^addrb\(2),
      O => \addr_i_6_24[7]_i_2_n_0\
    );
\addr_i_6_24[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \^addrb\(8),
      I1 => \^addrb\(6),
      I2 => \^addrb\(7),
      I3 => \addr_i_6_24[10]_i_2_n_0\,
      O => p_0_in(8)
    );
\addr_i_6_24[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^addrb\(9),
      I1 => \addr_i_6_24[10]_i_2_n_0\,
      I2 => \^addrb\(7),
      I3 => \^addrb\(6),
      I4 => \^addrb\(8),
      O => p_0_in(9)
    );
\addr_i_6_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(0),
      Q => \^addrb\(0),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \addr_i_6_24[10]_i_1_n_0\,
      Q => \^addrb\(10),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(1),
      Q => \^addrb\(1),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(2),
      Q => \^addrb\(2),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(3),
      Q => \^addrb\(3),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(4),
      Q => \^addrb\(4),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(5),
      Q => \^addrb\(5),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(6),
      Q => \^addrb\(6),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(7),
      Q => \^addrb\(7),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(8),
      Q => \^addrb\(8),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(9),
      Q => \^addrb\(9),
      R => wm_8_20_reg_inv_n_0
    );
\full_i_5_24[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => state_4_23(1),
      I1 => register_q_net,
      I2 => state_4_23(0),
      O => \full_i_5_24[0]_i_1_n_0\
    );
\full_i_5_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \full_i_5_24[0]_i_1_n_0\,
      Q => \full_i_5_24_reg[0]_0\(0),
      R => '0'
    );
rel_39_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_39_16_carry_n_0,
      CO(2) => rel_39_16_carry_n_1,
      CO(1) => rel_39_16_carry_n_2,
      CO(0) => rel_39_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => NLW_rel_39_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\rel_39_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_39_16_carry_n_0,
      CO(3) => rel_39_16,
      CO(2) => \rel_39_16_carry__0_n_1\,
      CO(1) => \rel_39_16_carry__0_n_2\,
      CO(0) => \rel_39_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_4_23_reg[1]_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_39_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_4_23_reg[1]_1\(3 downto 0)
    );
\state_4_23[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B7F7B7F7BFFFB7F7"
    )
        port map (
      I0 => state_4_23(1),
      I1 => register_q_net,
      I2 => state_4_23(0),
      I3 => rel_39_16,
      I4 => \^addrb\(10),
      I5 => \state_4_23[0]_i_2_n_0\,
      O => \state_4_23[0]_i_1_n_0\
    );
\state_4_23[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \^addrb\(9),
      I1 => \^addrb\(8),
      I2 => \^addrb\(6),
      I3 => \^addrb\(7),
      I4 => \addr_i_6_24[10]_i_2_n_0\,
      O => \state_4_23[0]_i_2_n_0\
    );
\state_4_23[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBFF0000F0000000"
    )
        port map (
      I0 => \state_4_23_reg[1]_2\,
      I1 => register4_q_net(0),
      I2 => rel_39_16,
      I3 => state_4_23(0),
      I4 => register_q_net,
      I5 => state_4_23(1),
      O => \state_4_23[1]_i_1_n_0\
    );
\state_4_23_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \state_4_23[0]_i_1_n_0\,
      Q => state_4_23(0),
      R => '0'
    );
\state_4_23_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \state_4_23[1]_i_1_n_0\,
      Q => state_4_23(1),
      R => '0'
    );
we_i_7_22_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => '1',
      Q => web(0),
      R => wm_8_20_reg_inv_n_0
    );
wm_8_20_inv_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => state_4_23(0),
      I1 => state_4_23(1),
      I2 => register_q_net,
      O => wm_8_20_inv_i_1_n_0
    );
wm_8_20_reg_inv: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => wm_8_20_inv_i_1_n_0,
      Q => wm_8_20_reg_inv_n_0,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88 is
  port (
    LOAD : out STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    o : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88 is
  signal \op_mem_37_22[0]_i_2_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_3_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_4_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_5_n_0\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal result_12_3_rel : STD_LOGIC;
  signal \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\op_mem_37_22[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => o(9),
      I1 => Q(9),
      I2 => o(10),
      I3 => Q(10),
      O => \op_mem_37_22[0]_i_2_n_0\
    );
\op_mem_37_22[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(6),
      I1 => Q(6),
      I2 => Q(8),
      I3 => o(8),
      I4 => Q(7),
      I5 => o(7),
      O => \op_mem_37_22[0]_i_3_n_0\
    );
\op_mem_37_22[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(3),
      I1 => Q(3),
      I2 => Q(5),
      I3 => o(5),
      I4 => Q(4),
      I5 => o(4),
      O => \op_mem_37_22[0]_i_4_n_0\
    );
\op_mem_37_22[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(0),
      I1 => Q(0),
      I2 => Q(2),
      I3 => o(2),
      I4 => Q(1),
      I5 => o(1),
      O => \op_mem_37_22[0]_i_5_n_0\
    );
\op_mem_37_22_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => result_12_3_rel,
      Q => LOAD,
      R => '0'
    );
\op_mem_37_22_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result_12_3_rel,
      CO(2) => \op_mem_37_22_reg[0]_i_1_n_1\,
      CO(1) => \op_mem_37_22_reg[0]_i_1_n_2\,
      CO(0) => \op_mem_37_22_reg[0]_i_1_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\(3 downto 0),
      S(3) => \op_mem_37_22[0]_i_2_n_0\,
      S(2) => \op_mem_37_22[0]_i_3_n_0\,
      S(1) => \op_mem_37_22[0]_i_4_n_0\,
      S(0) => \op_mem_37_22[0]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => enable(0),
      Q => register_q_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net,
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  port (
    d3_net : out STD_LOGIC;
    d2_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net,
      Q => d3_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\ is
  port (
    d2_net : out STD_LOGIC;
    d1_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net,
      Q => d2_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\ is
  port (
    d1_net : out STD_LOGIC;
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_enable(0),
      Q => d1_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(15 downto 0) <= \^o\(15 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => \^o\(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => \^o\(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_39_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(15),
      I1 => \rel_39_16_carry__0\(15),
      I2 => \rel_39_16_carry__0\(14),
      I3 => \^o\(14),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0)
    );
\rel_39_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(13),
      I1 => \rel_39_16_carry__0\(13),
      I2 => \^o\(12),
      I3 => \rel_39_16_carry__0\(12),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(2)
    );
\rel_39_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(11),
      I1 => \rel_39_16_carry__0\(11),
      I2 => \^o\(10),
      I3 => \rel_39_16_carry__0\(10),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(1)
    );
\rel_39_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(9),
      I1 => \rel_39_16_carry__0\(9),
      I2 => \^o\(8),
      I3 => \rel_39_16_carry__0\(8),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(0)
    );
rel_39_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(7),
      I1 => \rel_39_16_carry__0\(7),
      I2 => \^o\(6),
      I3 => \rel_39_16_carry__0\(6),
      O => S(3)
    );
rel_39_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(5),
      I1 => \rel_39_16_carry__0\(5),
      I2 => \^o\(4),
      I3 => \rel_39_16_carry__0\(4),
      O => S(2)
    );
rel_39_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(3),
      I1 => \rel_39_16_carry__0\(3),
      I2 => \^o\(2),
      I3 => \rel_39_16_carry__0\(2),
      O => S(1)
    );
rel_39_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(1),
      I1 => \rel_39_16_carry__0\(1),
      I2 => \^o\(0),
      I3 => \rel_39_16_carry__0\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(15 downto 0) <= \^o\(15 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(14),
      Q => \^o\(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(15),
      Q => \^o\(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_39_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(13),
      I1 => \rel_39_16_carry__0\(13),
      I2 => \^o\(12),
      I3 => \rel_39_16_carry__0\(12),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(2)
    );
\rel_39_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(11),
      I1 => \rel_39_16_carry__0\(11),
      I2 => \^o\(10),
      I3 => \rel_39_16_carry__0\(10),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(1)
    );
\rel_39_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(9),
      I1 => \rel_39_16_carry__0\(9),
      I2 => \^o\(8),
      I3 => \rel_39_16_carry__0\(8),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(0)
    );
\rel_39_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(15),
      I1 => \rel_39_16_carry__0\(15),
      I2 => \rel_39_16_carry__0\(14),
      I3 => \^o\(14),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0)
    );
rel_39_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(7),
      I1 => \rel_39_16_carry__0\(7),
      I2 => \^o\(6),
      I3 => \rel_39_16_carry__0\(6),
      O => DI(3)
    );
rel_39_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(5),
      I1 => \rel_39_16_carry__0\(5),
      I2 => \^o\(4),
      I3 => \rel_39_16_carry__0\(4),
      O => DI(2)
    );
rel_39_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(3),
      I1 => \rel_39_16_carry__0\(3),
      I2 => \^o\(2),
      I3 => \rel_39_16_carry__0\(2),
      O => DI(1)
    );
rel_39_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(1),
      I1 => \rel_39_16_carry__0\(1),
      I2 => \^o\(0),
      I3 => \rel_39_16_carry__0\(0),
      O => DI(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => clear(0),
      Q => register4_q_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => r8_full(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => full(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_clear(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e is
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \reg_array[0].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \reg_array[0].fde_used.u2\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \reg_array[0].fde_used.u2\ : label is "TRUE";
begin
\reg_array[0].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \reg_array[0].fde_used.u2_0\(0),
      Q => full(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 8) => dina(16 downto 9),
      DIADI(7 downto 0) => dina(7 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1) => dina(17),
      DIPADIP(0) => dina(8),
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15 downto 8) => douta(16 downto 9),
      DOADO(7 downto 0) => douta(7 downto 0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => douta(17),
      DOPADOP(0) => douta(8),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 15) => B"00000000000000000",
      DIADI(14 downto 8) => dina(13 downto 7),
      DIADI(7) => '0',
      DIADI(6 downto 0) => dina(6 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20\,
      DOADO(14 downto 8) => douta(13 downto 7),
      DOADO(7) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28\,
      DOADO(6 downto 0) => douta(6 downto 0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71\,
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 4) => addrb(10 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 8) => dina(16 downto 9),
      DIADI(7 downto 0) => dina(7 downto 0),
      DIBDI(31 downto 16) => B"0000000000000000",
      DIBDI(15 downto 8) => dinb(16 downto 9),
      DIBDI(7 downto 0) => dinb(7 downto 0),
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1) => dina(17),
      DIPADIP(0) => dina(8),
      DIPBDIP(3 downto 2) => B"00",
      DIPBDIP(1) => dinb(17),
      DIPBDIP(0) => dinb(8),
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15 downto 8) => douta(16 downto 9),
      DOADO(7 downto 0) => douta(7 downto 0),
      DOBDO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 16),
      DOBDO(15 downto 8) => doutb(16 downto 9),
      DOBDO(7 downto 0) => doutb(7 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => douta(17),
      DOPADOP(0) => douta(8),
      DOPBDOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 2),
      DOPBDOP(1) => doutb(17),
      DOPBDOP(0) => doutb(8),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => enb,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => web(0),
      WEBWE(2) => web(0),
      WEBWE(1) => web(0),
      WEBWE(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 4) => addrb(10 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 15) => B"00000000000000000",
      DIADI(14 downto 8) => dina(13 downto 7),
      DIADI(7) => '0',
      DIADI(6 downto 0) => dina(6 downto 0),
      DIBDI(31 downto 15) => B"00000000000000000",
      DIBDI(14 downto 8) => dinb(13 downto 7),
      DIBDI(7) => '0',
      DIBDI(6 downto 0) => dinb(6 downto 0),
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20\,
      DOADO(14 downto 8) => douta(13 downto 7),
      DOADO(7) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28\,
      DOADO(6 downto 0) => douta(6 downto 0),
      DOBDO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 16),
      DOBDO(15) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52\,
      DOBDO(14 downto 8) => doutb(13 downto 7),
      DOBDO(7) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60\,
      DOBDO(6 downto 0) => doutb(6 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71\,
      DOPBDOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 2),
      DOPBDOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74\,
      DOPBDOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75\,
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => enb,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => web(0),
      WEBWE(2) => web(0),
      WEBWE(1) => web(0),
      WEBWE(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
klQ5h9hgPT4p1YBkLUYG8lguTKRhX0z2xODTfvC0iZ4ANS2ufXQ4XxirL31R7wyoNNR1hd7rMjwq
bhFP3bRYRg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
m0Wn8/fGtoK5/3nNxNBBvNhfylkjw8jAoTERkIzypaD+FUTSQmf52TpAbVY1LPDo/g6d8tzIrR8n
vZ2G5ckAbcwDRGFF5wpZ/kzyANCTceWo7S+WbzLbzhWkG5fkJbJk/tafbSvqruxodmgtrUI2IZDv
n7Mrf4GRIdC1iTgv+oM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mC2bSkADVOO0t7I1BTHPnOuMcPIdeypycvBkfsUtXFD/WSov2Pt6qF5xMTcs6woq+465yEDvey00
mpX9JC1ypEuHaZI+dtH7s+A4vYLXy34kFLryTKJtqycAy6XqNtTegvy4giaDFwDn7LgsamHSf/4D
cVGL06Ul6gm+ZOMvzok6Y9TBltlEkZOZ4GncKspHPcn3EJar4GdDxKcPll1RqSFbA2m7bWz6jPV3
uOD6IWnIflyobdLIsuB8hse3kjXBG5sl6X8AP4armpo9TNNnRIRG2FoI6g16Sj6zldS9XF54ge/w
jnsP0vce7kfzvP1LjtFrWdW0raW61dIddZVz5A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
LC8srWyP8+H+elFwWxNw8UhEF94Tx3vV4OlwBcwFszd9xuvwYRipLEwY4ar7HEoCwJI7beW6k6I1
ntJSXGEnPMgIDe7PadSQIz6gi9ErRHWRhWD6bEDNd/ZjFQuOakXk78jzxOTKuOpQ+ExjdHImyPzK
mDR0BnkVc/QiSO7zpOf3yjx9Vzb0Fba6OgXg0iLPz1MUr0ahYIc0MAkw4pRBo6IH5ilqVbkgNQcT
G4z/0OSzsBxipE9o1elAez7CnehtEQ66g6i3JpIIwqwnpZNQ9otSnS8LN/spRcQLOUuBq9ynGkPL
sF7EUF1gLkTFFMGgnZgawWi0V0jhiHQkJuLKPQ==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ijV0yStg7uRIl3uzK8/hlbIaWGHa9aPC5Eu/o1vErrwtArYsGFt3RCyG/S90FB6jkuLgqwPR8ZlQ
P9t/F2FWmEkwwjGbdrRKFfpbkjh5HVn0vvLKCP3SiVHXCOWxxb5z8BV+yCNdpgdnsHFecK1M8ydQ
C530kRu3UD1LcnZcWJi41LcJAc5rvlw/SP1gbl+I1qsRNEHsb+MK5vyjgwBZAqKyqi7/UK1VEPdq
myeWeCRrU0GqEq5y/PHBMknv1SqNe0d5qzG1rmAtC4df+iivMCc9xuHsCA7iqoe+ZKnMmnA/8F6+
nY+gx8AQNplCeFxWppKH952fIYplHtE2rKQyVQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
saHlQAxOuJkLItpv5N3fJoJP3EdR34QNYHtUBFx7fcQy8HAUaZ/9Tlt2kfpn9r/pk1MmJ7uf4z73
LyaG/PWw7v3yH/KIVSLeIxiaSHoVfdWH1/RDH6k3DTDNbWSJDOjGSvMTThUKe899F8IXTD5jKxCh
frGB3Io0W4klQV7ADEw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RFiy0rrSiVrZMC725S3XkUSDuMS7Z+xog1sOwADnIBmdCChtYLUQVO9oPjXIujfg3bBFTH3qzII3
Y63fFMSZnnzk+ups3RPMBqEPPdMQSwlpDzsvhCOwYHe/rsOPZvqd7lL6QOKoA3mS9TZIP+mOaTKd
vzQiVyfS0rs+QHdJcw49jD5y7Dc3clQHD9xZMAMUHa5v8hzX4IOcnkSUOLpsZptR/WM7rcMnTzCL
x6m2UC+xSrwrb5vEz9cePMHx9NJyO2DBG0HTEDzGQQkrOCJJjvBxtB3r/E7/3eWGHvMwphSJnfCd
PfvYXz88X+ZVCW9dTNfc75jI7ogVMSunmtUuMw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
AOsoh+bLDfN64roPg0RXJpqYqVJLsWhWGMtKBYvDlX5dMM7k2f6suED/4GzZPLcH/GElN7UgDeEo
AwHeBAIidxcHjZtXhPyT3Vj3HBeHzWZDAAVbZegQKmpMHimJM8qgpxRVqA1pYdf0+wmSl+hw/qHk
/ewZss7k7kFDqv+lmN9iTzsltiT+n+C2BYvulTR/gZUg6Pw5P1GuS7jTLvw4BO5a/v9nAfal0Vrq
5w0p1MpVBtr/Dav5PHs4od0q9c9pfB9lLY2NPhXZbDSpgvDhBzLvbemJH5mQA779mR6V0h1v+kII
uGe7w/5TH8DDaiejGeq7cSmk2LPBBw37oovChw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Cj+bpq7T8DiRlsxgWwAidbndVYWfFBvh7bvVsuFUarv4hm7BzTeGAMubq6yDeKWQj1/oADl9NKoO
Mpk0Pc7mE6VpuIBPflbQeVdmQ0G4U+EBtSvBD43mPIvXG03vapYScyjS+K8LUdUJf5KrSQt8Ckl7
NdFLDFXi/L63zhgbqQtztLLOzZYEBc3LaaQPqp+KLD5cxo9SgCF8wpHouGPCdl1B7iyS6Uta3cHy
jA4qPK+RHuuxySY+QPcIoyr8Tnsm0dHfenorCMkmrp6MkrpW5XnoQ8tRGO0mozopwBeNuHOxugCb
Obr2C0W0E2988DKSN6b4JlxXFkSlAWj1qaTlvQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2864)
`protect data_block
s1QejqdWPSy65eRikNo0aJPqcbaEErR72qS/xO9gayZs2Qvum4CapA5kM17ty9W3B3r9v8hC7eXw
0XQ4HvPRfC0qa+OwjCBrSpgzGZ6+79iOut6Mir4jARNphBH7Y9UwbPtzKkFbMS5G3qvOAAxwsNB4
Cvf1rVx/4vclYgKp6ey8ZPYMaoMqJdodx28iYxG83HUJ9ul5FqVppZysGEtPfHejG9zkYmRLtdq/
2Q+5y/ufZS3kmxLDvdT0aUCMVE7n6hNutJJ337VEEw3zVKbdsqaYrtXIYixgNBaD0WExmlhY9wsw
gbMjIcWVQC594/3AnnAp1O89oiWxu13jrRPYXupf+dvho0TxaA2A81Zms2a2q+rp6hc2n0VKIP4c
VwmMy7peNUGI2enpd8XLuVMkFsVfidVfQdEzX5zF8y8lumaoy7bUJHaDK1V1QLDiAAzA9WLZPgli
9wpBfnjaXY6qxVelnM8vTe874tDCKsCv75XhoZIeII/coMlSSJmaPqvy0hehXDq/Yj2ZA9eKHvxS
m82aQX2y+f+FuHUhtQOSGNE79yyZQsRoPGmMZ3huPdzfZvRjLm53ekt7rr6E/Th9FwCYhyUGSUYR
vK0KYyW/9DVvxL95LLG++nIS6251YK3b0yFZOTEhj0s8rwbU9+0LsTsOmcqs+u/8Up3Uiaa0Tx6D
nm0ENWf+xC0lTzppZF8NCqTmkqTBDiky/zkH8hrB+ZVolCB/ggrxfgPEGBBQvof3O1u+bwBAypXB
HPHPmJ6jBckR1vrxd4RMi5Bz1zQucarXQJ5k4VTSgFxhmeeA2L9ATT7RFOz5G+rYm2Y5MIN82TpI
lclywg9xAySxen8kVBTFqYpjiita+bh2lsuCuUoDRfMb4Gf4h687G6DoLpu4GULsrey56t62y8T2
ktILuMVo7rg6JBaf2MlaijvUN+TrLeu56y8GJu9O3GjU7aTPqY15NMGOZ+S6T4P91oohK5NKbnRC
1EC6bikYTLnzm9BQyrF7c3Ic2dHTAOMeS1Lt2jjGEhyaLWKaBdd4+LtTD0wQqogx0wk7na4H+P2V
ozapQh6n+8dJwhfRbMoyu7U9+Ts+OdKcbqJKHIBWSGBWPuzluCXv2YJI26VEvxM9JLjjuLePka4f
if2VnMLGclwk4MxUgGYh/rcbwAP4r5KDsSknU6c8pAiltqgfJljmpKgPLu4CT8bHOUYISgC8mXuN
ZR0dFezDZYClNjqTulWxFtM91nzIHKf9+bymIBEQ8uoem2Oo36U4rChDRDbES/wIIOwI6EbsVqYl
AhbEAboQkVeHlAaGpNPb+oF6WUKuXbqFwKvoqbyXFE4CrzAA6RktwRH71T2nnHU4fpelWcqXVWo3
zIG8qE1uhmNr35adeOVe8iAfi0ys0gkQ/hYRpmEnHsq0UgzG5JuryoNqaNiyZw9FWGs/AJNjPvTl
gyvhWgeCQbOEH+jpyaN4eYnyUWrrvSegEjnerD1lbkuQZleltlcnRq3xgrqDhQr/ibbZurC83JFf
bkRgtT0z1U8Iy+UzwaVPxZvakgdiRinNhw3yEJDy6u0OUj8akVD+h4irOkQUcoR0CbYlhgufiYcZ
Zq4aH8ZzQ/eAMmOz9YQ8fUVKRVvD7d1+kWv1/wbzBtPPlU9ZcI1OPkkgf2A4pJahpQPl9t9pGumC
HpYcrV3w62iHjHbKxxCEQ7Hh/K42oqRJ0IUipitVXTauI4kAtZhDLVzhmEkcXP+eP6hW6J6Rgb1d
Q49gu39R9BFG3E7LGq2Y0yUugoBxEAz2HoBBRJBwlIUuP9P1rlrO1uuIxUIULBjYiJRa+qu+r+/V
pydXL8YnM+htyHxEsfV76a73h/Rin+g/ICaHma6Wfq1oqkRVWkLBBHORzpfnP0FcK0Lb5qbFqCIF
Uqo9eFgJaNDD3g5dp0mmZXnwvX7/9OEJaeuRpck+h+4LiVZ13/saWzgwwUetCUSn9KRJ1pWi5yUf
GuoNDmEbRU8pJMRb6DvUx+kfq+F6L/RRX080ArNcebEXvybVM8l31MyEpGYOZRDUGgDetx//MZv5
O+33sae89uKBH7aAnOn7UiCCfYWXejKHCgC1Aad/iiy/LGsZR2auyOXSp50IyEcDoKy+yeMYm9C2
xcDKgGv5fsB2gBphIDaz5W729p0BcqpAeWMY0I5BEGwmkaluVT9dahUvbKUeIlsh6lXXOoxbYEOm
SKKKlqCW+E+8ZZW26vhZe2gt3QCS90CadTA9oFKZ15+NuEfFbKncJQa/7Nycq4Iqi+xjmm2F+MPq
4cgVrz1YLzXrZC3thvLW93Gnh4+m1mGcuYyGhznUwaFyH4wz3pD2/11m8FrXZKaxD35C8/AdB/y+
oItSLMdab24mGhksnZOINN2UPIF8yShBApui2CWJBmisraVhJZFNNGbyp1m8j3xqwbHZ6YazocJf
z6ksFtEdhHeP/1Ukbqnns0N5gzxtuXsBfd/jJ9qu0rNQP9oFR7mKb0Rmjl6ZNqZBZn0oG7DW4mWb
QzZiyRY69JmJ4log0mXNclEzrgTDoS3/vnbvUzBsbulwPAjXTTuvRgbsOHh1SCjD/0P3yPiRuhw4
Z+V3YqmTQnYQtpn7UFhopWzt/G8AnkM/STY2KMGOEzslLKaeo6LZFy3LX7RXh4sO0Z8QwgbmIsHM
kg4a2ixpSVyKsE26F38ZIDjamLU5tZUHl/yPxWumyA4lJ6k6tqFmDVYJYKY++EkGAaXeSLvyydNk
cfskCp2A0+6Pe7Dgul/f0oGFg9EVl5J7bqjvdTB6VzpymfDAnSZWL2Qp52Wf0UtSp1gERkXo89fv
PaZ5ZefbEbDsPRrIP4OWvPoETzHpWcbPIU27lYY/ekHtX4ZyxxcXAlK8Fku+DxLfrGQAp78Ea8j8
1zdCL+WTeXCjej98J5LQ0RZaYb0VQrlJP0zB44DyjPeshdUyIGMqq6AaW5QVXBiwI/9XGUZUyqZ+
O7tc6Ws5N42gBfE5QH7w96cdEhxpEcivuZRCB2lBHlHU/qveITNTkoXaZIpmBdratjTFsGdEzxTa
yP7QCVqR3dVdw7oGFtEl0KqEd32mGNuxS7FrnqnQDds1IoCtd/Jg6M6tr7y3GKgIH7rWJ7URhUzr
WZKoQNy2lDFSqfICq9o+9CTwg15yctpvUTheYcO9zp8OKMThIV+SIgkpojr8JUkIyxanfowRTmOY
kWcWSDwHNKru87WmLdaT251Z2p0fQwO7Dh4oLaq0g5RWrEQuu65ZE4PD54BoHkbAlT1DlDVtq90t
adK4d4htltW9y1JMn2LkACJo/MiitoNDKzX3qJ6fux805YE49nHvnJToF3Xf/07urMQVnPZ84726
ox/No2ggOClp6SIT/ddndv22/3OeIpO6Gga+ofRs2eSsyWtXWVNuxKeTLFoa37k1uNjWFrZYlSJE
b31ibAI2idX6YeQM09lVjix6pmKc6+hJADEnv/YICNJiAUtYBal1eQzZgu96ZkDzqZuVsFiDnBda
7DoLRJLGcYWHEtO/CEyqfNYYh2eMy78HC+Zgnad2fX515TRG38MOe83Gb2JbUc98pcT3EiiOADM9
zKXfeRCE+pyccnl1X58Id+PPYPCycDyS7DgqSJzKkI4QRdxTY3Tq7cqROEmGlKdsQoT4WnX2qP1+
x7G4W5KT25WR875k1EmYQ9HuGIBiwdHItrWVpsXR2pETeZbRJkVT86nItSAr8hHbGljCGH7exIF3
XpyL3KXkNd/8aeRr37bDbhSSEJkXgwTNCyv0rheosy82JF7alY2eZ4fKRbL+yDu9j4pAWTUXtG4t
9ttRBMU4drjqKSGc0Qg=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface is
  port (
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    i : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    \slv_reg_array_reg[0][0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[3][15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r3_dina : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg_array_reg[5][0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[6][10]\ : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    r9_douta : in STD_LOGIC_VECTOR ( 31 downto 0 );
    q : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface is
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^axibusdomain_s_axi_arready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_awready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_bvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_wready\ : STD_LOGIC;
begin
  axibusdomain_s_axi_arready <= \^axibusdomain_s_axi_arready\;
  axibusdomain_s_axi_awready <= \^axibusdomain_s_axi_awready\;
  axibusdomain_s_axi_bvalid <= \^axibusdomain_s_axi_bvalid\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  axibusdomain_s_axi_wready <= \^axibusdomain_s_axi_wready\;
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7444444444444444"
    )
        port map (
      I0 => axibusdomain_s_axi_bready,
      I1 => \^axibusdomain_s_axi_bvalid\,
      I2 => axibusdomain_s_axi_wvalid,
      I3 => axibusdomain_s_axi_awvalid,
      I4 => \^axibusdomain_s_axi_wready\,
      I5 => \^axibusdomain_s_axi_awready\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0F88"
    )
        port map (
      I0 => \^axibusdomain_s_axi_arready\,
      I1 => axibusdomain_s_axi_arvalid,
      I2 => axibusdomain_s_axi_rready,
      I3 => \^axibusdomain_s_axi_rvalid\,
      O => axi_rvalid_i_1_n_0
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog
     port map (
      axi_arready_reg_0 => \^axibusdomain_s_axi_arready\,
      axi_awready_reg_0 => \^axibusdomain_s_axi_awready\,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      axi_wready_reg_0 => \^axibusdomain_s_axi_wready\,
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bvalid => \^axibusdomain_s_axi_bvalid\,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rvalid => \^axibusdomain_s_axi_rvalid\,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => d(0),
      i(10 downto 0) => i(10 downto 0),
      q(0) => q(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      \slv_reg_array_reg[0][0]_0\(0) => \slv_reg_array_reg[0][0]\(0),
      \slv_reg_array_reg[3][15]_0\(15 downto 0) => \slv_reg_array_reg[3][15]\(15 downto 0),
      \slv_reg_array_reg[5][0]_0\(0) => \slv_reg_array_reg[5][0]\(0),
      \slv_reg_array_reg[6][10]_0\(10 downto 0) => \slv_reg_array_reg[6][10]\(10 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller is
  port (
    \full_i_5_24_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    register_q_net : in STD_LOGIC;
    \state_4_23_reg[1]_1\ : in STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller is
begin
mcode: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      \full_i_5_24_reg[0]_0\(0) => \full_i_5_24_reg[0]\(0),
      register4_q_net(0) => register4_q_net(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk,
      \state_4_23_reg[1]_0\(3 downto 0) => \state_4_23_reg[1]\(3 downto 0),
      \state_4_23_reg[1]_1\(3 downto 0) => \state_4_23_reg[1]_0\(3 downto 0),
      \state_4_23_reg[1]_2\ => \state_4_23_reg[1]_1\,
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger is
  port (
    \ff_2_17_reg[0]\ : out STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger is
begin
mcode: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a
     port map (
      \ff_2_17_reg[0]_0\ => \ff_2_17_reg[0]\,
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg is
begin
\partial_one.last_srlc33e\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2_0\(0) => \reg_array[0].fde_used.u2\(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
  port (
    d1_net : out STD_LOGIC;
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net => d1_net,
      r5_enable(0) => r5_enable(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
  port (
    d2_net : out STD_LOGIC;
    d1_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\
     port map (
      d1_net => d1_net,
      d2_net => d2_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\ is
  port (
    d3_net : out STD_LOGIC;
    d2_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\
     port map (
      d2_net => d2_net,
      d3_net => d3_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\
     port map (
      d3_net => d3_net,
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\
     port map (
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp_0\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(0) => \fd_prim_array[15].bit_is_0.fdre_comp\(0),
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp_0\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(0) => \fd_prim_array[15].bit_is_0.fdre_comp\(0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\
     port map (
      d1_net(0) => d1_net(0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d3_net(0) => d3_net(0),
      r8_full(0) => r8_full(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r7_clear(0) => r7_clear(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => o(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
begin
\prim_init.ram\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(17 downto 0) => dina(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(13 downto 0) => dina(13 downto 0),
      douta(13 downto 0) => douta(13 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(17 downto 0) => dina(17 downto 0),
      dinb(17 downto 0) => dinb(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      doutb(17 downto 0) => doutb(17 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(13 downto 0) => dina(13 downto 0),
      dinb(13 downto 0) => dinb(13 downto 0),
      douta(13 downto 0) => douta(13 downto 0),
      doutb(13 downto 0) => doutb(13 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bqxB/Jin9u5a3vWWM0sH7Y5GZQusr/D6N75LLPRPgy/PxiUaDoj1cMaqVDOFUaa8j6UW/0XuwyPw
zMgU0ufUOczQR0R6AiK6xta+nCgh3QhZKGcqC7Xu1l/M1fvYWChR+vv47GXG9Aj3Ffk3V3Cc1xDn
Seq0nexMCg4QecT5FvojIdnLv/QVW7W0uyxF7JdyZsfyR4aqWDqcLVyXYd9Fk5L0EebHg8hRas+B
Ly9orOPlUkP/yLeRA6CzDpS1fYIO8bsBTVWNxZW+/KZ0iQdKvHiyQqpckH8GvZH0e94v2vr6BFoH
zux+AthNVe5c/VvY2zBYTzYliHH6QkJb9clLSA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
wi0XyEKptMM8vB64PAu274jk6EbZKMd0Qwego9S3Rusdlft0gR3INEcgBaweJ98gfFBBn4Z7BYAn
Fpb1wKAO65YzxIrKCXEw9x3h4oJ8cMyimyJ2pKHQtTrJTcT5fcemu2dh5sLHcrxiablZpB5efXzi
8efHmAaVWmHP4uvjlMe5RNYJT79dGzWXf6coUsJPWpi4fot4jl5dXdg5ukR+iE51PUBG0Oi0Dbjc
yk8nzyRWi8gFE16vDdTikNEaqe2mutZv+yw11qEAyLemzUjyL0xxsAhkY6ZWqBbgcIpUmrZO6fB/
GSvup91TAwf1jPaV7a8aSgc+kjh5r/xE60e7sw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10672)
`protect data_block
PsXuhdCr9CLzj08bDFCZNMUJsZPrUm/2GcnvyfB0Tub1i4Q4hXO4Y6V7B9FN6KinVB/qlBbcQ26d
HRcYD49Uk8HAt+VW6p1WSJX37ghw2b+vJFVCPCdAb3VJXvIkH0PxP2GiAu1Lt+KKO+lBEjNW/cVK
3iI/vG3VL+Gjyu8J7LAQ5Yr2uOqAtjQbcG9kw72ytibCYdKlIRBX3RiN/KD0qlLMOKpDJLzgbFqq
eqV+wo/chGCBd/QTpJDImJjV53ZK6SxEYvqLke89sYjlYHGM2FFcY4dMad2+4fi2HlBK5U+zE3Kn
xJ0Mx/0yU56dIIFBPFKjKOlV2mwdTtToQ82OshvK6T9HBSkrZ+mNQOaL+Pexh673MirLWkaz4E0V
tqUlYi3B+nblsd8R95+yWmkugHpV3zI1Yg58CWvlblabwn5iZhXKKTl2zAcy4UV0sx4ysRx5Bgqe
yC71+2mBmdzjh3Uv1dkvjSRsJsB8Dms9FxeQ6yfHmJLQE0pLLhwjwksxf2wyujkpoJjHnzAZQzrU
ZL+uUs5E4y/2tNyBgmnbKB6ht1wW0ak89g99btJDLYrdlXmqwQKC2rnnWt4NToI8BY2/oCiu0+FP
YKdI9dxQfYcYCxP2q9eytQn0aIM+cV/4HmwVfwiUDGsBSb5m0BLhgsnqa3yzz9gKTca8fRTP32tr
IfEiawQjN7RDJHA4pKr61czuiNjkIvy4rWqO9xjyTdk1jFs3tAfjRKibZJRXJQEXzk0wbyCLEyez
scIysSdhDjr6TbhoKzX3zmNCIAFr/1qSVp0ISrRsbDpl6ctOxvbW0DpUkFHyBpU/h8jvh53pLq4P
MP6KtgYZFI17Wbwi1zAP+m9rEHuKBUB3Hu9rCgxnxeT+maeLBd/OkwVj+cjwG9NpqisD007qGL0b
qMP6yCVaE5PoBZjStSuKS/7mlZfW9/NaXT2Q32Hrdj2FoDz79KxWsGnfxcJraLJvk/XVL/thjsAU
V1FTit/MTH5dtn3K3XVsf7TbxTNUpH7DgaTfettHRak64mqM9fKmn5NnBxO8QdjnFvlcJQNCWiAm
KWXYzKD9GfuMXKFK2cWmyI4Q/w7xsW5wSoQcDsbOOHz38tc61UogqL41aYf21Ua0zm9INPfn1kuf
JqXdmxiGwt2UViF65i3esQzdOSrljHWjKWexuX3OF9Z9DhsXtq8iGVTjlQKCs/fdoIGvV9kdExpl
WJ71vexn9Hj+Ntga5VQ2GWvcsQ8tTAdh9iuYchYVWHPNckaoUlY6Zr5MTG6t7kdMwe7MidaDdVQR
4eGfO/JdGuezn93extMGAR5q0j+JmQiDoHc2W8K1u62JoN/lyu7KnlQPZyV3lMNqsZuMu+hNHKHG
ollHC/20sGhH+5+GXtHjWw04g6WR1t784YjC8smr58T+RCRCGJB6XKo23eB/C0HbKOrWAo6p9MHG
BEBIgHirgzcIVHImpkFFEfxImWn1ZUxdNaFJeLre/hO2HU73I4DUA9Z2vwnsK73KsoArC3huLJ3X
Mg2yaIbnGtMy8WVVle4xvmEryfQInOSZMnMqfy+rzaON7KmiPDlQvtBxZ6KptudnDFKw2fer4j8z
CpOhCQP0zokHOvnnjfmAHsPQZ2VBHw6aA6lb/9H+d0+vrHvHYT+f/NSuEj5z706CNyBIf9ER4/6V
PbQdF1s4ENVSiOSOZJNlMZO9+uQurbx+xXIbJmAdBcyHJ3f/GL9NB1zZ8mUWJZ7ER3Z6ioRMDQ5w
g3OkjdBrs1NkP97kMptgEV27/CCVXRX/nanNPsIeenNGAe8iGaoKDEwhMizMU9uq42BqknVsMFsU
/KEWxJlT4lLoBv0eIUbNv/nosk0/nlm3nIJ+Ykb28fiVI7o7FkB4B52DiGTCl28XjkDqzNKVm5q7
Il4HhT7boZLn/qS8tnUk7dIZXRWKs1j7lC6INnYFnO71YKF/47ncs9IGPb31wMGrmH+Bga/iV0Ut
iAJEW7dbS2XeXDvckMhQkOCUmQbKJdEBhUjmpTEthkAFuaWcbq3Oqah2iXEQ3L+1Em9WXRLuxg4R
e722D41W2su9RLDjln6gB1vs6SKY5hVF0F7fVipG3JjQ+MijZY12GAPOXPLYLyth/AbAaoroiAAZ
gnc4WJk3Y8oGH7FX28wutfwY6Syh9fFFDZOsy3B6WF7Hs9WI6Sc09utrjjNUQmIK3+ouP1t96Kgl
koVXvmP1H8in4krPqRBYO3l1Ob7Of+YfPawlUs2PLoyKMkh3eGFtQ748Jtk4QtQ+gN0wHkVrh7gh
ZD+UfvX2s62WE9LVBHhWqabC/RmymWwmMx+lptI00GMP/96Sbot3PRK4550I8aOnvugS5U2BSsyu
dE1RbcnlO02Gt81Eg6rQ1TQZXEqeMD/Pa6eArpRMLHoS6I5hYrUlvoIqSuHMUlfrYH10KGGMeWUf
7VpwQNrBsLsDWjBrRBLg1M/cnywj1bcYsTQJ7uqsXH+ucMT8CfxObkMyNevOpfkhL7THhFILcjFt
tiXVIEWCBau9HfprS/lolm5KlpAB+NUZ/zs0cfJKMy6mMX4dch3Es9IyAMdzsdBreaNakW0yDlIH
tjQAk3tmiv2Fi49kzpBM9cGHbtPzgqgubktZucOZQHMrP/VZloAbAJWZBYGUC24TBuDbjgPpd6Yq
VJDK2izGSreMDLE4Dy6qzpwMr3ZqPfDxIA9OcEfsysiv5Ypdp0jKg2wL3Puty8ZRdhU6hcaHUl32
XwniiTtgGe7I5TwA/kmcBl3Ia4lY4mwIz1y0cRfvSGadYI+IKQDPh+Z8RLJ2Nr3AdIyu5a0F//y6
GxKXbkPLJW3BWd2UJTI/QCSbfModWWsoxqcPqcDllMIA+BtJQHvs/quvgIWuwkFhm2fvm/QF5Ruo
srM4rNJGaxnlgFaawwtl0HYqvGDrMKc2b593Tw7mwSj24OP6RYsgK+Z9qsUmmk83Fmbv4V5Y45fz
zgSyMjJnW5iMESroYK4l5rDkxyPHwCiXyHDg0RUP6Sk2AmO4+SL8ST6Z/RSn77qPSna/LBVANa+o
HdlyNW8lokXkmzGr+HP1lVZCc7h60OWe5n38rLqG2Kvo553JDosdUN33+ov0qlfnmeSNwhbzyWH7
vs6mmvkKd6pnx0Z0ZCL0LkWGC5CT5TcP+Zv6RUBarDndfR/e+ZcZHC06305Po6anz/RaBOVFFhk2
yXDHSXltspRJQeibRWdE1Csw2zQeKPDXaTyguCFhJ6KyajeBYTW9CfLecWSdNpgvFLLJ/IlYbvqe
pIKWYLLPPVIesz0TGp1Sphy9boIXJaO7ClGyA/4m1xEKON6E57sp0ZGABd6H+1BaIM98hOnZyBYM
LdMMLDt5gc33PMKODCM9vAdcfg++4+EgdZ10rL2fkhIsiA0xv3iGFWo+GQSISuERnp86NTajpkkY
c3QOJ4W2iBtBxU95bswq110bAyYNqpeMIgsAisxDjJcRLVhcpqGs1bFf+62/4TDiTJ9zD9HhVzHi
UQi+9pEMAzqyc60bwdW3A+b/hijPuInpucX3XGz1yW8NfxeKm6PhDlENzyZU0AUo3sbMTAia8+IE
n7zt8wgN5/1f9Z4QZfQGJFrOJGwOZCZTONLYvh/yAlATKsFcDzD/3UHQ0EQGoUiEpUqJrCZ4yl/9
lQ4FEpiBnLzPil5sozCCLdBU/zGHFwNW94okOwDj3t1DosiwmuJKIQlpLbuZLyBnExCksaGAnnt0
gKsuPGUlF4UNmippyx3TAdVDflHphTK2+p4urLOd8ra0FKrDa30WG2uEwMvm/noFufILZ2bC5hi9
frfkhD63V1+I2qv0vtVOuCBEs0T0gClyMCWgSgFD5I7gsn13ja+QExzhPYtT+BsSoFp6cRatqVcN
PE4XbmPigmOi9HOU/Hr3pkF5kklRIn+ucjh0hOUj6XKE9M5gEYvYnQTwh69R1cD3c5AzKxcOz/uh
6P4ReZ/KlaFtZUMaaHE7I9ywAXkNH0U/rm3ctNAgRwzAHY69nc/Jtp6Q78fwFvqll76XgKS2wVaV
1ialjOme5S+eirqf9ixD1IkUFDaWQE7M3CnpT/2/jTB8P04k72wGBvkAsckloCJFxDjaVoPRqlwl
+YqlmlY5Jg14p566wNwm9vNCL5dRqKFl73zuxiw432l3bBjMi2Wh+8c1eSw02bVVnLPqkY9j7fU8
MZoamRuYnG1cVKxXhs8gCR4vI5Cew9nPhWyfEsMrrcfzeGycpF2S8aClrSGsQQzk7LAatFxIXtib
KpqDnJ4uAmGkvfU4GAoObX5c/xPmFHqdMzPHUENwrXVvS0i2QLacFGu+DH14dO+PKBEffbFEDIC4
39pPQqf6zQtkb0d1mmTqLqbeOdP5/CQY+Tl3ZqSFDxJrWnag8GXESWTIVuOX11tsnBoL0fzHZh7q
t49yOW7O/zHIEZQ/19MDNFlQfHoLc3Huhn5XhdfHi7XiGJTEx9Z+mkZQsYs2puOBL61AGiXXOzV+
Qr5JpjSdi2HbDBo0Dlb/bFaSncbm6KiafvRvjd9GnTOjw1eLOw/L0pDu4oo8M5xa+if5pJr7xsWF
9esg/GrrbsDwtJCMKcS00AUjQyOVqarGrGKmwy4a5Ope9v4ZfyLQ/dWf3l1Q4THNua0L0T+zIzUs
R0vALmSvNQvjZxKPaHu17wm+4pVvh4vdtqklrgCh7goDX3NQKKIiC2L3gCgKqP5VGEYKloNyXZCe
6LJZTKro+rGMz0Qedu/cEfoXEkutFh3qjbZWU+v270XxVb/oZZM1DamMT8rTY/DKUZspbk5cnSFo
4GgQXDCSVAxmSG9UmyyPbLYVn9QBTOCVyiG731/UfzZk74cO9fFtPu9kkL9kklA9JvbT+WhqCsCQ
sdBrfIdR9+KWuPMCSwqws9ahiS5bdsLuY/L7ZstS4jNFC/wQoiH+b+vgBsIOMfe2TDFmKNxyM2WC
2fFBcGcbAvYAyc5v3OplIMUlPS9xJ0GyatCTnRbsI4BSaTc2pAVAiUpPeAUwzQiJnsxB15dPrbaX
cpt46VpGM3RZYv2GEWF3yq9scp2CJqM3m2gO3G5aAIy39SNvF0ikpQ6sMtki6OJ8bQy5iURdlcbk
q0eNIrwRc7hHHP/saWju32tNcfrdKTnlGJ2vUcqpqSMhLvcDHM1B/8Lr+mCBfIbZiK613Kic8lfO
l116RV+rbFVISKK9pGBq7SeUg/YQ9GUiw6+zLWXZGdjdmdbmLvN87MPdihbSVuRbm7Jz0b4s0FHW
HIpDDMh0ZgP6zbrJRWhXsHSF3Cq3fxvsZmnBPqmGZ/6C0pUHPEP3NltNRg5Q9iZkAzRqMaKt8Thp
m4s4pi7ZEd7LZRSBQpUVr7+ZQApNBHMt9QhvbFB+ArmaszkpZ8HVDuRJjn/kv2WTlBavOjc+IEB1
uaDL4EdSI3DwNyCJXcxL9Xztzg4RZImr5tgvJz9PUywZbF3kdQlSIULVJOUhHjlPRuwxl6F3LdJZ
C6AOwoQKY4wt+D5touDWd+lEAelWx+c2jhAWA7P7jlqSRdTiB3S/NL2WZ9XjawOfHr+Ct7XHvhyr
PwJg4/EKofsBKzZlBmxBZ7W3Auugonq4ZmiTGQcm7SrOiYSUtAQ0OJuHTFmUrFiog5paIP0Zd7p/
tel7NukTan24ge1YYVKpX19ooA7bcp9Xg+jq0yICb7iIAwISwn3ZUFxP4IZj23L+PwdoAcofb+Rq
KUYY/mLMwmv1HafI9aTEc1hkc0F/LW7B1E0jNxZgn8W5j6G3N/wtl/+/xO+UqSiJOp97W5LB1+OI
J1EN45t5xt8rP5fKkVNYsg7yawe6h0N1ZNhBQJ9xVBZCWdrL/xd3SzneOVrD6wrl+YecVPgmhsEu
01BQEcs23yFOCacNsmcEQcensBjbzCSAD8x/9wZvlxOwPZ4Lu1PD9axbOJNvjGS+RiLCxavldBj4
ZwIY+jekVwlOZQG8Sv/GeWquGgYZfjw9rer4IpmUDm2atfbZlb25+Mp13mXxkzgMffdmEvb1adJY
Jj1OxR6iSds/OZ97L+iFlILNp+AmxiK8TLOz3drAsMjiId7ULEdQkcDFhS8Jh2HVFntvoYgkxyya
PK8XsF+HrVH33DB09Cogdi2im5cKD0fIZz+070XXKykxUxRUDzeShmBaaKy67nTzY/2F2Mg5ZaS9
PmTM8nhu30CcB0s2OmnizU9epHweyk/dQd+rEbpi/r/S8ARDVL9t39pWED429HlWU6eS7i2PLKkE
62ZGvpIZYTLqaDcoU5KO3NY22mW5zFNj7ZF/AQLwgThFFgkTjhQgnMr+cu7jwLXQ7NKcnHrmwABE
6+khFVwPYzcVk4Wz0UUbzMwDGZqSW/XQqrryTgdMNU5pGh0Sfy2vsnhXwrQH9gkcV988JJvq8Hlk
b7i8LG15JAqWoN6N4gVAEkRFm+Zn5trIhuTFPUH5Wlv8fcToNNJuQGzOKaKwihKiKXa9csFV/97W
EntKd3r1ctj67osxyL1j3dhEjmUR7NsjrySF2NyqaNSpjJJrYrViXRjw72JFMwlT2Ovghf5SPG+y
N3c0s+yhnNcawQ8/df+K+LAtquy/CtY/qCWZGBIJ83K75hlfRVtSDrp8NNa6ylcCIrqPV08K09UV
VXM4XALxhHq6zD8rWt8MS6eZ4qTemXcBUAS0b0Rp7cB5Mn8RCwNbyjmM87gVQ3BLvp37Vll/F6x5
3UwBN3TMmifTiRwrLFTdhcdFTel9BGz7CJcN+8tiIRrNqfRqZnSWYICla6EWGT3fr/ASshz9dQcw
vBClAPCagytkSdfBNR6zArP3fmPulU5OCXdTXRWHuiJ5gXYSi+xdS18/SJAkWUr9Xu+v/DgrDH5M
Vmg8+h1o1MnefNlPcmiePn6z7r4+KvtbILvE2rxJctYXlPCLW33gCn34pOH1HE51msnHBghLRTxJ
UJDKtLH/hNeXUX0g8d5UtfeJCJIODTfKCVZdBrIw66XKU80aUenvnfkDXIfQmC9+m2HxFuROobtw
8Axodu+TrG5QG3Hkrv/LTj9e0ZtONYYvZvbTyBtx37uFNldjhYHRg8fFji4DJ/YxGk/9+soQt/hT
/T/KBMcE3pdWRp+swqQQ5LOFA2TArrTSDZPrqotjgc8wxPok1eWLbv0j777Irwm53PzhoZRbrBnZ
j2CJs+qR/vO/PdCmvbY78RSzTdBSeINTY6zkhUPpH1ZzGrNjxnVXD3UKRmdL95J+VHC6SvmzUvQc
uQ4ePFJl4xzSHtdP2ErwGWlRH5RAICQwJaaestqjinxm+2kv3t0WZSC/cVrDkeIsRRv5bn0sHS/X
/8CE0QWPMZ/pp4K9LfwyIZaBUM7FVSLtclv0TC6EWsdSkMzs/t+4BgjEJcWXUtV0uop4AEtqKisu
vtNYmCTLm87Um6BnZCFCy5RUcl/htejlKuqm+SGQVh4Ht9tHydvvXUz1pmM4UomfKU3W1bFrf13V
rw3DMWWbobH12l21h+ezFOMbZ/ftSZ4S1tlAv3tTRyW4J+5Qx2ycAUM29rONhgEifAnVvSLghw+/
lyNblrYvi1ojXhhSFL/gsqqh2yXZ3i0nq5Wvk1+G96Fo+TSvZDjWJ0pBc1KadgF+zH46XQz3DMaW
n7UTHKYWi328QT4N8YwNs1BpmTiHGNPLs7HX8nL+NER0xCaHFwe+5COAEVQnChjC586DwuSdcKZs
BWRRJAbAGaHONBtmV2mI1QjFuCWTV9sFMpGCEG2/NfciudRywZCtVeQJFDY0WXY8J02IUt2mDyvb
rva+y9Ac1JMMLOUTzA+qwUWcpJPt8EdJhTQPj+/mqpfV6Mey2LS4mIgQusGrM+FCHYuf6iktot/k
skas1TmIAdzG4NIpNrhWQ5Cf83LRat2Q6T7g9NLsdQ8UHfS2lGrth5OvOnDl2Yhd6F2hWkaSus9B
43Vr5lHs3PXMuodJhPQV8jhaehjdC3sJMa+wmrnahJUekwVSIR4fSckIgXnEgDagES5ulZYDxK/q
UXweIAb9KpLpwpvt7CyBwcSoBaBZ/k6Wl8d7yOyVfnVESCVhQldQiplYl+uoXqLNnsKtBYCwqlGH
Pr4lKPC/lE2nJPkaz61ajwqs8EMD1usdheDkU5USx0UP9xIIgMbu0tkJMMB9Qpk90LpuVUEJjhGw
24d9BYxJjt6Ztc7V8sBqoSzn/1EeOw09FgN3KRJ1pCVWaFTMbZsOVG5cZCwmDQCgUvp/EPtth4SR
a2z6KvjHzHzp6nkabVPWPzHeU84Z3G4ISzUM/ukMi7HQbdYC6XtUZggYmkCEXSL0gRxYD+WpgjF+
kRnPz3RMehonX+T1IgFMhOEfpsvuda7I3wx9SUHov8X9oIppu0GZDkhgpu5qUiPePzazOJsE0wNe
KSp6f0RrK2EYKIR5S/3qAU6JVRrOBCBtFceRvnBY0RxFNf1u5n5QpGgo9hZo600kDQs1HLiCRdvI
Nd0ZNKwjVUlrlUJYlRdSNaWz+URLrUhq+vkEhbWuwHJBYCGsG8jIshyvopmhzTYnPWyZiYa1feT2
+UMCgqLlkgBhzEv/tak0QFI/45sKH02dGP0Doi8D8a25BmsEFoiUYX5Ac/f0udf7Bq5NRGzVgQfC
KE+odxVMMDTjga9lRqF13L6H/d++wLrxy2zysbSZWjzemR/4BH7kSq44cIQP2yrBe0Zaso7b/6PX
HtWn+thTc7nJFUvnu7FEa/YI63uka/Y2lg5UoVEy4rUkjqsXLvqrHDFeAwH6mtBigAdMtIj4A718
k/DZDRXMazVEK4vz5+0m8WiUjnVbJOQinjyAXF6sKtGvLz+aSKix9Eq6CRRXEsNTvKh0v2yRu4NZ
vzVCNhF3KM+j8Cxb27xKfJJG2BTj7dbqlITgNZz3gsFkvrNsy02I/SGaAhTkfYo0g0hoLvHYt4vL
bdqefRtIRnDnD6fLaNI4bayaOOsRtOEJb+3+gTzYZ6o0sHiT7XDNpaUlDDRsr539Mpn6ov143qzq
97YPiYT6tnPsyaKOG7VNML7U3uiKq4rLdcT1ic6bkGP61c4ZANCO1KJxnipEM3uYG/pEYc57obw/
HoIzYAQAqDp3UPhgjvDSPsgC6SaZw1pQyZpjhhj/N+57tnb4OiuQFZXhnJaHRwuTDXMLjKeFfMTf
8TPNHg+rC6ST/ce2WKlKgKQkDNCYWYfTXNalWaX5u/FkcWQ1YhytdccDlE6+7P9V7Q85jXwvlpE3
ARmaUne2kRL+t075HIuRHmKNN/pJ/qOSR8Iw0rvx8gxErVW2L8zDYZcb2RBHgTE3ogbNpkyQN0Kd
pC8kt8FKNi54CL1kd9fy8194s4WdhZTOQwRIReRqo4/2YaDpZpr0iGTtZSBAB5r1cr8HG/UX2QFQ
6hspbXTMr8AY2/qy0Gc/tLbTr3aRlee7KDOWP59m9BezT68dZyRIe3st+bS3ZUnnwnPJaIgZAPMb
pp3Mr4LRSsAFOJpOnD5IS9J/w4gzkEW1mXP3GU/GtHmKabOYN1ZXVb2pezGn82bPOVLtlznQvs/x
jmsxW5996gYoy41Kf5yjqzR6+Mu2SfSeoVon2LU6h5e0hR8EybyQfLjaO+On8ue9XMg2xg9vqNZp
41SoIkwfWhm1un1CMznFTVhAoolFZq+NJ606VviHs5u2ktbbmoabfIouFuHtxjJhTEdyf6boAFLI
NzFzUJWPnXQ/0JCrQHVQ0skzZuCZ25ya/lUJ70sUjlTOLQvMO+8WFfoyW4Rbczx+YD0q3rMiTR6Q
ucinUtk6igwRD8HRY+P0ikSneScNEDUM5q5kkcGhCqJzjikBqIQ1f46nI3ZPKJ7ThhBQL6/HlOm3
qjnqaeDUwIgiHCvRm9KNQ26pepJsODFpgDJtFwi44MlhxLp5w9SwSVYpP+jn2uFvpZyeDkBqgkpZ
5x5XAkJwmvtFYPv8FzX2p94UX8b5Yzso05rcez56N5ZTtEC5W5EIe7dzvXH/QcHqOBYoVP3gtpAL
Em710dk9RQGf6QDBy+vGHKJWZT8XXdVuApQ3im4MRGd8nkochrH0znsXppxVhazEORdfwxC4xGRR
Nt3q/xc+A85jCDeZ1JguyJdqFePJspTqrlzwXOkRRJoZrdytnjfJJLtDyOa9DVLtq93+GV+JVUV2
HC6kgtQdLqvk813FPINZ1n4PZ/uZSbQaSHeAOskFmeQZEkCIoxeMbVEItAn6XZlie+0BJjU4DF49
VmgAhgA+BNE3LiHuZMhJ4QmHPlZakIDl+u8E3u4TdsiAjExBFrv/O3mS2ddi0iyGoib5t2bnSNfG
ORprM/wfClQp7b6cKa9PIug2LlE5Pz7Jm2EtA+4l/98LeHc6FGM83aPSoZpM+7l1z2QLb77hjifJ
jFgypqVnElY6vwFVaRy2U72EE6lFBMY9PSaXLl5a0OyeeYYBzjJLU2CYjG57aGsPRTagYPExYALH
MjOgDqCHK3DXZFCznGuD9IBD+IKf29sYTGzqZtVdunCUnSwZVa4w34QzsHe9Ljtodi8c/y2OHFd+
F5293fXz9LO7EjL3WVTGQhLr6lDkbRcvUf/3iG7R0DO8NkjZTpHgcdzDrJJNla5DTJ/+eEjQK0qL
/bm4TFnUzTjZl4TVAAv5NEvAVZtletp4A9B6GI3DkMtR2RLUBZ3UfhJmgC6hUlHyEBXwJ2QqWJaJ
zu1WVFm4JT4Twb9BXVzNUfTSLkFW+CC2QCps8UT/Bg68wxNl05AklVWLSyAgQPKvrC9DsAs3/WBU
7XgZfYflfowcms5mlMQOuNFayZHlPPbTpDMCFcPCRmW4tj0oFJ5y/fZavff+Qy4hS/z0Bw2SD57t
OPixmsvVNG12HwK2xujzOLM8fgitJw1DS4Y0lll8l7ttGr053CPteyQXuyYZC7jGJcSM9BSPFVgg
oIozw/7V6BIbMqvdIHlF09bJKdaAtN3fOsf0wCM5SKQUNJtYr/nk0g3qka1pAPw4RsYTNgVzejh5
UUwuj/vEIRh5Tf4iIrNCnPAhxazIdUxunJtMuB/jOV2uX+K3oz/TA+oaRJacXIwwFHSL3VMr4Sw1
ShiENwx8FddnwDi54y8K5fw7UzBBd+T0IztL8QR9QUM8FyWcnC1ku2ex9Bc03tA+M4p4Q1cAyPol
6qHwUnwOois9xGVAxAl2Inz3aqYo/jBBDAHTcwCBtHtivxqkZP8DmhtsaJVulGe1JKoz5s0fzGYn
ecAaWy4h3UaOdqlNWJfBO0Gtk7sFRJoRm4pVLW+9XELH/QuCAMYJrmgKTAt8bM76spumFZqWvr1w
3haEUEEKEVK6znUTtngsx0l/C1BQF/E0f02z8iqqU0D8DrTEyjJWT16BbdsPQGEuijrQfwbylia5
6ZjwDdwRoN3Lj3UffiqwKBwW3d3epPiy1ZdziQnIUvmBCRQhRYfKuE4pfeS6pfnZ8YLiEmIu1/Jz
5HyTM6851oLNkqKRZGSerU8KtrY4grZIlL4PxBxv9qMTozI96uwou2vEgy8Ousnoy/X4S2s/azI3
Uj3pnwEyfJwIDHcxHBPnc6QJ07lKG2BIaRNiQ36AdZcWy7MrTIwOf7QguEjq0RjvFUYDTHSHwnDg
TuZCSb4wfVOBavAeGB+AC01qKdWk+yRBqo3jyIQo72BmAXUMmpdMgsW+I4/uINhBfEgTevRXO6Bt
u3ZPllYhyRU6sj0BvvtcmkKjBaAbL34j4uO81qhMhXbNbypbS3uEjPHC0DX6OymOLmCshRn1FBIL
G2nxqZrGyEGCMjdHg0uJwNooogMZG4IR/14dRs1wUWulCH7sj7MkFA95jQa0RMm11hAe2cd3z607
2ah/d50X9orPF0wsf3Hdx40thpTQhHpyWsIbpC80Rad9NTgXOxishz9uAv606iD6QVYv3yIDJ5XB
ydi3EXFS6RTKBvvK+txZZUzV6azjtg13m68NB+gsda2v+a80C610Ip6kk3DTCFWCJzU5HGkHXLaf
fkza9FelLYVVyaZuk1KseUm81m4lqVUbEPq7yDZH7gNMgO3dxulRqF5sAWFkrlUZtv0qFyCb1b3j
JeS8VnzIpNcHkyXyKRaULH4l2+fSqawWylrwPDdN1ViwjSrA15boJ3Pb5Q379Q3jC/2w36TJ8O6g
TwJTdWAQr6ktwxdoq7LYAWc/E9RoBJvKvx3qMPZADV+ntG0fazTvcx5w4x/c0ZZLohUmIGjSwUlJ
iKGz9N/kxxtwJ7+zrlUwh1NaW5pVp5EzwA7YsVML8u+YN3Uze0Hzjd8YljJS5oAGTpMGmQD2pJUh
bx7ERZyVQf70F3rwm9V/jVJEV4jZh0YWAIQdDbLFseSEDS4+r4XUjfVUyb5pw+D7MbITl147rDOz
loLW2x/M+B2BT+5S87j3dDn+tyqg6joN5FTnJZVYO6+JHaGGyXo4zJh/r3gC/T8gEdhPTgIjEECf
VRwG+8nOgqJ9baLE4nDsooHuLVil/HPuJ23Uzf6Y9kr1V3ENj6kyACj2dHRuud6Z9FJJO8DxmdhL
itESRalesprVqban5ig0uWNCkQ7E6FlpZshbs8LP5WMEjc7a2J7QhJz/XiEXCqTDC3MjQvZQS7TY
3fUZ2qWObcH8CxGpQu2SOBEcIATZvT5M1WzuXMh/4Sq294q7+W8X0Mum1m5+Cl9fHleUXMgmKecv
1p1wPxXqedLzJ3bTB3nXsx2CTmPHPgTB4VYhH8Tj4JW7p5sFowbiRakbPP4F+kURc6PvYRPkEfLj
PEAmaLVaUfEfGoQA1wQt6pVXCbTrvZA8PX04JKpHr1m93meLB9uD/ekFZ3HsC/4bbtgb9ub7YCE/
xOupEn+vwnnMLkukcSmaFDzpjTF8/JdoCKVOX/fXBAWFAXFynv1ozzdGBlsqagn3szH8GW2bRZ82
B5GYlwPB178euyl9t8j7gqPqozGQnjbogR5jV3F5KZelNQ5VzL1pZGABigOu+XA4s1bRQ2OwFUaB
c2ob9wpiG3Onsa8RLo5JJMqIQLiLZLiUIPtGcA5ee/bBeyM8enzAkZcpBkuX1R0JuKsQBjczXDqL
FlgVVFKjxtPwLuoGLbjxctMo7cBp0aQxintjh1xUK5APDNjR7E2/byQ59coEB8aEIiiHeVdoaCv6
dwuChESga90YCPGlT7kyrbWytl9DX4oqoj1fDffjaQrUXHcI44skXH3xe1ZpF3d3aWR110nmROvo
VcoJkJPc7ED1l8f13+Io9Wn0i/E79pylJgNx73VGz1xKycMJsKEolOl/mIM3i0XoGlaIwTBj2dtF
aQPrXVUBp+ZmAkS3IuUqkDhm/KIX1OSdE11daktERH3cRT5K1yyMOxAHJyuZKjA2Js/g1FDhuRvB
05huj53VNQst7kDS+1oYKaV5cQVwOZVjD44KzzI+D3jpywUQcXs/hssHgtN5KUoCdlr6WSzoqa+r
BRc5gjJgVbUptbg6Ch/5thJShBCZ3evTM44NFDqkxTtHqIHGxSipXjHx56Dtb2rhjUWpWL1wm3UL
2jtTCFrvXQ08BJBcjDVkHYxhcMi89y+Qkjbtnk4XjLVt4Y+R0nTDGibPP0BhWG3lZgoAVUZmyj1p
i0LH8LWErGxIrymESF2VrjQVWVtcTuOgdG4oiwHoA7CIb9q/qMOTt3IFL8sI0C2Wk46kuLo9vVOj
qTaR2/6f9xM4obQ4rcIwIKOSyTMMQpLhc3j/IFj7/cGHluhkD9j1vmYC911D5xyb6AkzHQIKfnWv
8RTf65IhaG5BUqMAlljgXAj4rco/sjVDpF9HuoGV20TeptJvLGi3ufwVEy44X121pqy8RE41ihbi
bor/RH0n+FbCteVYlQE/iQ/KCt7OIU4XsEEbQL6ir4/nrvWQ4MGhFiXpTUgXIHtzvVZjEZ11dWuW
1eNZYmxK808LSaDGOjGZ1TriTVEYekDgtLDjHX1GI7POWyBEhU+XUaUJ5jgD0bCtLdbBG5epa2Lp
A5oXYHVynjs9StD1EGfPVPf5bwUAIcT0ogOi6NYm8cmG3nLWhyhA1EDmfpnqjd5mhvJAvokBaTIx
KqJiLNlP4x3bGbB/fItpgspm7poi62wKq+EUbZwyONRcJ6AUDrmia0wH9osfAkhXontYnLil3fZt
LoQgOkylN5qgK1G1TLCfJ/o7JYNVq2Knz+sEjgHyVt/Zt/RU495H7DFRJmZs7r8AxoZd8nYW+g+K
hs18AlcBstehcnpcJWVJUHqmv0A/VdNKH00rhroGvlpk9pGYXTfeDFuLlpKlatAOqa0KrxhpV1fd
qZjdMIcyMgqTAT9eVg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\
     port map (
      i(15 downto 0) => d3_net(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\ : entity is "ip_scope_xlAsynRegister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\ is
  signal d1_net : STD_LOGIC;
  signal d2_net : STD_LOGIC;
  signal d3_net : STD_LOGIC;
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net => d1_net,
      r5_enable(0) => r5_enable(0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\
     port map (
      d1_net => d1_net,
      d2_net => d2_net,
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\
     port map (
      d2_net => d2_net,
      d3_net => d3_net,
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\
     port map (
      d3_net => d3_net,
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\ : entity is "ip_scope_xlAsynRegister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\ is
  signal d1_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 10 downto 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => d1_net(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\
     port map (
      i(10 downto 0) => d1_net(10 downto 0),
      o(10 downto 0) => d2_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\
     port map (
      i(10 downto 0) => d2_net(10 downto 0),
      o(10 downto 0) => d3_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\
     port map (
      i(10 downto 0) => d3_net(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\ : entity is "ip_scope_xlAsynRegister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\ is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r7_clear(0) => r7_clear(0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\ : entity is "ip_scope_xlAsynRegister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\ is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\
     port map (
      d1_net(0) => d1_net(0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0)
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0)
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d3_net(0) => d3_net(0),
      r8_full(0) => r8_full(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\ is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\ : entity is "ip_scope_xlconvert";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\ is
begin
\latency_test.reg\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2\(0) => \reg_array[0].fde_used.u2\(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => \fd_prim_array[15].bit_is_0.fdre_comp\(0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\
     port map (
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => \fd_prim_array[15].bit_is_0.fdre_comp\(0),
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
begin
\ramloop[0].ram.r\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(17 downto 0) => dina(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
\ramloop[1].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(13 downto 0) => dina(31 downto 18),
      douta(13 downto 0) => douta(31 downto 18),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\ : entity is "blk_mem_gen_generic_cstr";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\ is
begin
\ramloop[0].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(17 downto 0) => dina(17 downto 0),
      dinb(17 downto 0) => dinb(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      doutb(17 downto 0) => doutb(17 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
\ramloop[1].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(13 downto 0) => dina(31 downto 18),
      dinb(13 downto 0) => dinb(31 downto 18),
      douta(13 downto 0) => douta(31 downto 18),
      doutb(13 downto 0) => doutb(31 downto 18),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
oRZZmr1FovGU6594AbbfgAIlgSxjjXaB9dRsr0JYIj8TCaINtU/ce22nnAfu6w2dDkOABILMhlC9
Yta+pS5sPJBvYaIOa+n0mnqd6gfrpYHevUjCKJkfpWR0CEEq9o9ziQW50JYIL3zNSRSJVSAX4Axn
Nth+Oj0Tb1j4kr2qVy0o2/Zp32bVVFCCKeLLZiv2JOWtjNx0LdeRvyN6l9+MFhdJl5/N8Y4z3T9Q
qyjWhAcD9QiKRgzVGWuUm1oKk6HYnzhvWTL4gIomC0gRBDhvAfAqJXFmBcem1HnMJOdHigTO89GJ
3Y/HN83wiQjwtsn4a5veyOUxAt5MG19eyJSHTQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
N4YGNNMKV5hMr4FZXRl6wprHRS07BFgq/keSiPzskjMBstNhZEA2mRV+yPvKS0gQuAf2+fbY/zhD
esx2pjS6QKqCf8Lc4H4Y+aWpY4X9Iv7TSoL9Op/aeP1XV14X/cnLK5CU5TFaK/UPIu8WyC9pVmGc
GuZJUR491IRx/LOfvLHVPfjUUPak5Sadt9fpk2zxWCKFh8ELvSBQIqTMjhXGHLpjWWx4FXDwbUtr
il2XOgJwjcmah9/kj1whwziTGZ2P+7rBR2GvEGfa2GtzieseyjWQVIpxIQZKyY8SEkDE54YZA36R
tIKuJ57zba8PVbMtNzqaB43Ns0/+bedBdt0wzA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 880)
`protect data_block
PsXuhdCr9CLzj08bDFCZNMUJsZPrUm/2GcnvyfB0Tub1i4Q4hXO4Y6V7B9FN6KinVB/qlBbcQ26d
HRcYD49Uk8HAt+VW6p1WSJX37ghw2b+vJFVCPCdAb3VJXvIkH0PxP2GiAu1Lt+KKO+lBEjNW/cVK
3iI/vG3VL+Gjyu8J7LAQ5Yr2uOqAtjQbcG9kw72ytibCYdKlIRBX3RiN/KD0qt2UbWIYVhisubOZ
HADsJjiyMBH2DN97D1NsriYIif5hqfe3q5r6qQAYnDHg6j+hm2mmwGinEG9ddYbUqOgXFh5jJ/BQ
5iSH0GreVDX9ET6D7QZnye0cHGud9WQf22cz1FFLYe9K3M6FGsSdCwfd2W6OXgkza1BEoY6Le85A
CD4DQPx5YtQHSJkrIifIMYoGnmeVAdx5O0mKG/ew/ER7BbhEKG92oAFa7RvfsSwmn2TkOQEgCHid
i1BEtPnaRlznyOhQLJ+CBGoAWe+vR5tu7RvQcSd1F4VHvU8rTLyR7la5EfGgLdn5LzvPVOvgHZrk
dihhjmqZnjGec4b7Z8e5hPJ4BbsCkf++uhLnEeWBXfEIneophwIOY2FLAAWQxYBU9CYK5uaJdzoE
O+XaP5kHymN5oYI+7GyfVusklWV+caHzVnJDuFfmM9CjJgBddSXFEYGvZYK9Wvfjt6D1jjBOVRHB
jKd0bY6/MFDgQ+y5zLRswTrHsyW9/oH/iccuVHuTKDpe//82TYW2jmR3ILC/qtAiIu2q/YYPxK5x
qbHpRxzvumtk60DPURJK80FhDnMuk2W4KdkWxX7iRLhOI/OR8t/1jrkHKI6c+smJhTylO5YZm3iV
9/PSGTTMCMYuEIKxzSqIWrol5F5mYfdC0X0AFhf/IRFJLXZAB769jhptp9lgJnMsUQ6uDSXwuFm4
MpSbImiE9wvnolaBhi4wbbK+Z6G7Fn2PzHjy3KyY9lIG6bnnLHd7vK0oojtlitUO+V8mCEEIB8M2
AlalVc/sYBUHHJ/QcfbalqHtSxyWAfN7GmVLN0gqsEfFNskanc53n0e7eha/0Jkg6tk+4Htu6/Lz
140vT9RnBeciZ6n7rozG7r7SzHo2V172HHGxbaF61aSPlGNwUiBiQqXcDLqlaZqBUErgSsm+MjQJ
v0rlIfaHlpdPhToWVj1Sbhw5PLXWkqwjLw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
begin
\valid.cstr\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\ : entity is "blk_mem_gen_top";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\ is
begin
\valid.cstr\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YlbpIKTfWH5VkdLqyB5YTuQmWYyzginc3AUrNRV7Mhiej6ZgHymw+BTjpEKXpYhSnpXJDoWoTB4j
5gZADPHiZYXP1E0FQ7Vh/+G6QgY09fXf2oICtwiuT9YIRtT6tDQ+eyTjl2kPI2TV78JTRM9fkFOn
8SjfIRbjLcaQZE4XaTVg/aBOppfyb4wop4YTzmIeh0+dBV3btJcSqsdCmnOiS8VSYhDgF+yuQbr4
H+S1O4io28bd0R8M3a/6Cbm+0STtDiHwWuqq94FWyI2bLwAdMjFqszuaFIBUb5tBrdHeK8Q7BoEB
F5LvzyAWHdG7Lq6Kp+R1r+c5hSfFFAQryoWcEQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
2CXcAcP0LZkE39XNDbuOTOZH81KyrTVFsUBagTmNLC/oQcnTlEeAhxw8zdp2DpwODm7tiMl0Ndjg
SP8KviV86aHdH/n3nduXGLeWakJ6vnif/2OYVkDblfVIMInAqkJhNQpwJ0ivMUznN9Iw9TjlSN+i
agWCDKaKKjJdrD8VwQyJGp4haqBRK9B6nqm8UwXDeLkUN6U3mXp7iDMq96OPo68CS/b0nKU81pfm
6kIr0ajcxphucdmD7AHjYLJq+2yeJa3pMJneIGnyXDGvMibTXzdtlksA6Nti7e9vlFMCCkG1P4lW
6iSorPMyERzFul80r7zDmYi88Gbf2gfql8I98Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 880)
`protect data_block
PsXuhdCr9CLzj08bDFCZNMUJsZPrUm/2GcnvyfB0Tub1i4Q4hXO4Y6V7B9FN6KinVB/qlBbcQ26d
HRcYD49Uk8HAt+VW6p1WSJX37ghw2b+vJFVCPCdAb3VJXvIkH0PxP2GiAu1Lt+KKO+lBEjNW/cVK
3iI/vG3VL+Gjyu8J7LAQ5Yr2uOqAtjQbcG9kw72ytibCYdKlIRBX3RiN/KD0qoxMW5SuvJ15BsYL
vB5tcXGZlFsiFi9h7HYVz1my7gkr+W5m3f/SmL9fnWRTS3hhkupvD0cvwy+bVLDkxkRadFwDQX7C
kcMpas2vYrkLDK53VOOtuBUaqQjSnnor6FLiHsXSW0NqKxC2qhBrT2ANBhsdO0z3Zc9yW4ps7+6B
F5f/cByKPzzWXmeKoICk5xBxgfaF1zr2AcgTCjTeC0m21w/FN+BUPnG+ZHoMQAsQ0juWSWmyT9vZ
Ik1tPcJNsY5KO9gbBs5iwEYJEOS0QMePKYr6w//avWnGRY5P3X//37EtInQ0iKybl8KmkcBHmgXo
3lkW7qXiJXNOVHcau9fDvOQKnSLAahHqmF1a08V+gD7ZavbQ8EFEZzrUKO9MMO9kMlea7JgqAgIB
2hCBFCzxWpLKWvDXIew5Omy2iQDXQu0tlakpbggZ1fPfgKnYSnL7jKpZu/m+ljqNhd0Z9Ah5lSl0
YvmoTIP+TmQEUdESlbrQTOIGTcjK4xRWy2tj2MPyZdwOWQWUDaT/s0mjSuSn3ahkYDYNurJMk9rK
RxjVMIzINhXCVqfwT3gJ3gGIi2RfiPv2ig08KrHJfdS2/BToOnPFOqi/hBaSICCCPpZFRW6SMDDM
ji8x3xseTX1PQIHQjc+AC8j7mXDvPx8CHM0SpMAKVq7RxCsn3Q1wtWEXSRdm7DReZx54/eQQ+mvF
6Ss1yVaSzptM5vZx1/BCf/TP6zwVdnE5Z4ZHukt+d2fNNJKzUcLN0Q8a6oHWqR3p8EHEOkyAM9tC
Ef91chHKI+Jrj5c1Rk8wcVO46NCCEP3JQXgQ47iiiHODw2nHZs5QThdGP+VpnFY0j9PTsoHcjdGw
SyU/QfPkmO5R9hZ7GzX6vMibQQOX+MdxEv1l9mHnbSis7D0kKyMNoaryapfkFHmpG4EQo6x2/C3z
/5/UsztFF7ZsyAgZ7wpRkUJUG6TgNT7BlA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth__parameterized0\ : entity is "blk_mem_gen_v8_4_3_synth";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth__parameterized0\ is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
I27g6MYD4MW0QL8wlWDusLpmUQr++VIrJciW4kxAZvD1xu4xHc71nmmtQKn4RXprTItt3VZ4jlYb
W7sDjTzN9g==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZCd7ncP4pCL3bR7D848emv5B1F3GIhdyKdOrR8YDzkxL987Xx05dubKjo4b+YR7j9Ecj4dvvZe1H
6adMwXtSnoRoi30g04SXknicgV7eAYs7TVosxFfBt1CNgOyzN0bQbEUEFceUCCSfx8G96lJC7l6k
w+A3ZWFjWJVg1Vt6/vk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WdPinNhXowL0eItMQjILfN3ZtNRlAFPcgUKQgbQY6PjgOLOq8AYH8MaWMxwJ1Q3XLuR/CpJ3Msf8
9ck26xaGFODD49GyvXEm1m8jiFYiUuki4s8taTUnBUe8UmyAXLQtV80x3pecHd3LuDlAHzqlwHLb
RwNUJ8jmu/WUllodatqqpKOBuaenIRptuK0/OJ4m/EhxfYY9CrhNzvJ2OB+5wKW4GAF64RWui9+x
Bqw+bOAFz/60QdWNUEfYkftioYtQ1bPR23AMgCCwss6e7ZAS1ZDoUsWca/IzfDGAnuGA5fYYttxq
uoEZzpjupgu6Vfe6XH1ShFfcTJds9diYIBxF7w==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
J2PI9pkL6e7AvAtwdDAKsuYeJfRneKgQO+nHNIiAxTtlsTW9qYfsuK9HIkf0Sb62x0qE564ViGrS
3wuHHLwljmlXkNuh3H5s1WSIvBYog56SpodBhW9K9QsQbL95ZzCqaRj4TQCbxUYoSGwFsdPmmdM7
La05z8feuoK9AbXfQhrl8CwxQ/x9xQWu9KqCdtni2/rS3DBIIvXrfDnjMLkEKBjcfotyV3n5YDPS
xsl7G0OdCDCYK2w60G1P6raIszyzlL95Ntk3rgPPX40+b45hMQ7F7lMQBoxIOoJb8u1v/MHG31i3
lgLBBsa7tA8yQ8i1322megfZnBtOkWTPkmGATA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T2bPt/UsVlHeNg5Le1CxaL61udjoxzw96Kj3hgyH1tUYL6gN3XkvsqfcbtTEYOMh8h8ccS0iuxDR
u1AEFZYGDETWy4UIcIXJiMNJW9mKlMxeX0aYJmt2gk5SzH3rwCtKFV4BE7w8PwEho0CpeEjRbnf/
H/ZBsUMPSTkaly3dWhZ8iwOFxUFab/dTKOrq+/zYR0ymNFfbFDioA0t6O1QvpkQKgAmLKtz48cIZ
d8mpcUcwmUu2smVlnmcYowYxe9PRMHJNRJBKGeYM3CGauAWJ//wpfST4UNmOFt1mBZDKyRVEtdmb
fxus/Lw6a1mgHZoYg82xB4qQBrCBG6m9hlSvQA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
O9o1BY8Sbymv0CVlrYskQzxGr+xrqarG7EPRt/pk31lim//eAelugbq/q5/E+p/kQuJs9wJgGlt7
J4EL+RC/2F1gmfEvMCf58rYENifz93c15KzjI9WIzhK9r51ZA8fR9ZyTMeOrkrakuwslx8gz2Nd8
GtHVrcoDTocBpHaQfUc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
B1N78V5eMBUsTRlKv5Ei+E+GTZzSlJSEsn133UlYjp9l9fAT6QvueXKaAUXJstu/mKRIG6pF2HeT
10E1KqusBF+N1vx0mJm+BGQmw8VwnDo/1C0bMq/T7k3skIGTTlpmmE04UhvTFZ5eNWSXF3hpRbnX
7LVz1QBuLq3y5vrznsX3rcSujxWkx6qVpUyrOGyzLDbbTs8ND/8qOUNz+Tsj2+A/fjXiwPURl7Ci
pqZKDVAkBrWTm4hLakBaJZUjicezF1sJAkTUk1hm+9WjXbPsux0DtpZ2Gw6FtD0vv/mLn3UAKK8N
uj2yxGASMChllxrDzGBUQlWEDBX02i/b8OMnHA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
eUTzJxYNIkPLhvEX+hBwvo11tYek/FBce/FgBDMNEARt/JIrNRMVySvSBnKdzp4egQ5lxDIwyDOY
h8H8FlM2x0j7k15gtpzoYUoXI0xn4g4D8hgPf0ydqa32vPlubY6YZpipweOn9T14aPvraRxF6hzn
PJ2fwVq6m/NuSvdL8nT/kzv0srOB1J3PnDuvrr818Hmw6GmrJB6SkWFDw86PZFUTlbj4cBJbgj+L
rXe8sjl/cyxZ7xudYO78s9IAJeE/PQdLiozHtEJMgroE/AdG4J5UYniqSZhVTFIqed5KvrY43yqR
4IHP5POyVkJgatTpGCL4tJwRJmCHgXMoAi4Mfw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3aqLGi6R+Hs524SnRJRiLF/mtGgJP2i3OiewrWZDHJvLH1NmSCHFbzLFYTOVY58Q3u9xVGNPM8iK
QRX8UPex6RFkGQ6MuGY0jJ8708jxknH/WjmloDNgRLh/nQixl9u+cL2w8rAyYXfDH98jM5eROIWb
LhQPX/OvY2AflN5gBuN9bKJKAD48b0ONOZuIFOwQzKEKc5mpQmNJ4+YesAYOfuVxXt8kmfISVR1s
DaREM6xn5W0Mx3uEvOp5/11hjlfynAJbo2cx88Jk3BbZyaKV98yqlI4YFoypdiI0/F/QL4KnKQnk
4gYT0lXYWNv03wkswcnqEUvtaVo1y6s1W/vjRw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 880)
`protect data_block
t4Wp7/3WCOvNCKQVr/EwL3M//PfHfaDjkm+/UUt3nVxttxXxqe46R57ZfqnVWmf67tN2Cqd1FQJP
4Gx4h79oo/zoEA9gHIfbfZ9hulRMpGnMMHP3E8Y2MSJojnUUmJ3W+4wt8cceYBLqeqjT7LybEhZZ
JJytfK1auZRjjBpbdGGJgQiS01rChArP8kMRhmqzzOD0NFDZ+0VvxQyL4+GzNYOcPBFFwxCTZawn
He5jjfL2Rtf5eikRmvrTH+FUjbfRpA5rnDT9CfaOKEjmsFuF/5fhBUM3cIYrZTtDiAQSn7KJI9ZY
KdeFIPRTOFIdc1i2gAPA/hHfCx4+7GXvD29Dd22zZXS7MueaJ6WiAAR59IdyPK/g35Tkq2E2X5fM
mQA7LD63uJxRgV6GrwBj6JwoCAYZ6XaWIEf4LosCP+uChTtI36ArzM+5czesegWVoLe5BmYw+tz/
c4184PVcjakN6Y7H1lNjpQbo5tnDDaACZ3yrRMPWlNYGq2HSQFdzqDb0B9rp80Q9+6T1ECt9T4V+
8P1HkuFjGsAe68dd342WSxharrpQwlDMTQaSqxsfU7spS9NMViBnpp/M81H97UzHbaESINs3tIcD
b8wsNEGum3ZWwEErYGnZ4Dnad8xRmdLa2bd8tt0wXfYsVW8BEY1kIiQkF+dGSSIt2JVbo28/TiYu
eQ7FvJBXx3TbU6639h/aJP76/iNblWAhnbZqYJibgDFW/ltBS1M4JGTjMJARCjX59KqLmRpPXrfO
ctHWGZNg28oFV3GHVROhI8eHPPsTS4WfRUR7veTlCP0hYTd77vQsj0ScJw9gOCmd93m82AJuROc4
2Ckc/FKSb4DI/10ANN3S0h6mazEYlTXM1IYypC81PgzEQ/L/z2BVcWZU1ia2ud2ZVvdKXZ9F/7Wi
i+7QHjzCgFcjnzurQFs7HKqaJl3jNLizP5MeQAWeIEuqbndxQtZaGXQnEOripEAJe/D3dTXSHEXn
xxMV6p/7HblZPnSUZ20lmDmEWv/kA6ycRk0vena/eQksIhQdlI0+3Fp3/TtxTC94Z3HBUxLaPa9p
QZEvykl8R7TDXHKbCqzVzumDn6UVb8OSS/RUcMQ6VeZnxNBVxOuRab/YkNUge7+gQeJvQ1f3qdP7
Iy1N3eHdxmlKhprCtfdkH8IT4zT8w7GBOA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "Estimated Power for IP     :     5.6824 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "ip_scope_blk_mem_gen_i1.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "ip_scope_blk_mem_gen_i1.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "yes";
  attribute c_family : string;
  attribute c_family of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "artix7";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  doutb(31) <= \<const0>\;
  doutb(30) <= \<const0>\;
  doutb(29) <= \<const0>\;
  doutb(28) <= \<const0>\;
  doutb(27) <= \<const0>\;
  doutb(26) <= \<const0>\;
  doutb(25) <= \<const0>\;
  doutb(24) <= \<const0>\;
  doutb(23) <= \<const0>\;
  doutb(22) <= \<const0>\;
  doutb(21) <= \<const0>\;
  doutb(20) <= \<const0>\;
  doutb(19) <= \<const0>\;
  doutb(18) <= \<const0>\;
  doutb(17) <= \<const0>\;
  doutb(16) <= \<const0>\;
  doutb(15) <= \<const0>\;
  doutb(14) <= \<const0>\;
  doutb(13) <= \<const0>\;
  doutb(12) <= \<const0>\;
  doutb(11) <= \<const0>\;
  doutb(10) <= \<const0>\;
  doutb(9) <= \<const0>\;
  doutb(8) <= \<const0>\;
  doutb(7) <= \<const0>\;
  doutb(6) <= \<const0>\;
  doutb(5) <= \<const0>\;
  doutb(4) <= \<const0>\;
  doutb(3) <= \<const0>\;
  doutb(2) <= \<const0>\;
  doutb(1) <= \<const0>\;
  doutb(0) <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "Estimated Power for IP     :     10.698 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "ip_scope_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "ip_scope_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "blk_mem_gen_v8_4_3";
  attribute c_family : string;
  attribute c_family of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ : entity is "artix7";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\ is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
I27g6MYD4MW0QL8wlWDusLpmUQr++VIrJciW4kxAZvD1xu4xHc71nmmtQKn4RXprTItt3VZ4jlYb
W7sDjTzN9g==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZCd7ncP4pCL3bR7D848emv5B1F3GIhdyKdOrR8YDzkxL987Xx05dubKjo4b+YR7j9Ecj4dvvZe1H
6adMwXtSnoRoi30g04SXknicgV7eAYs7TVosxFfBt1CNgOyzN0bQbEUEFceUCCSfx8G96lJC7l6k
w+A3ZWFjWJVg1Vt6/vk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WdPinNhXowL0eItMQjILfN3ZtNRlAFPcgUKQgbQY6PjgOLOq8AYH8MaWMxwJ1Q3XLuR/CpJ3Msf8
9ck26xaGFODD49GyvXEm1m8jiFYiUuki4s8taTUnBUe8UmyAXLQtV80x3pecHd3LuDlAHzqlwHLb
RwNUJ8jmu/WUllodatqqpKOBuaenIRptuK0/OJ4m/EhxfYY9CrhNzvJ2OB+5wKW4GAF64RWui9+x
Bqw+bOAFz/60QdWNUEfYkftioYtQ1bPR23AMgCCwss6e7ZAS1ZDoUsWca/IzfDGAnuGA5fYYttxq
uoEZzpjupgu6Vfe6XH1ShFfcTJds9diYIBxF7w==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
J2PI9pkL6e7AvAtwdDAKsuYeJfRneKgQO+nHNIiAxTtlsTW9qYfsuK9HIkf0Sb62x0qE564ViGrS
3wuHHLwljmlXkNuh3H5s1WSIvBYog56SpodBhW9K9QsQbL95ZzCqaRj4TQCbxUYoSGwFsdPmmdM7
La05z8feuoK9AbXfQhrl8CwxQ/x9xQWu9KqCdtni2/rS3DBIIvXrfDnjMLkEKBjcfotyV3n5YDPS
xsl7G0OdCDCYK2w60G1P6raIszyzlL95Ntk3rgPPX40+b45hMQ7F7lMQBoxIOoJb8u1v/MHG31i3
lgLBBsa7tA8yQ8i1322megfZnBtOkWTPkmGATA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T2bPt/UsVlHeNg5Le1CxaL61udjoxzw96Kj3hgyH1tUYL6gN3XkvsqfcbtTEYOMh8h8ccS0iuxDR
u1AEFZYGDETWy4UIcIXJiMNJW9mKlMxeX0aYJmt2gk5SzH3rwCtKFV4BE7w8PwEho0CpeEjRbnf/
H/ZBsUMPSTkaly3dWhZ8iwOFxUFab/dTKOrq+/zYR0ymNFfbFDioA0t6O1QvpkQKgAmLKtz48cIZ
d8mpcUcwmUu2smVlnmcYowYxe9PRMHJNRJBKGeYM3CGauAWJ//wpfST4UNmOFt1mBZDKyRVEtdmb
fxus/Lw6a1mgHZoYg82xB4qQBrCBG6m9hlSvQA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
O9o1BY8Sbymv0CVlrYskQzxGr+xrqarG7EPRt/pk31lim//eAelugbq/q5/E+p/kQuJs9wJgGlt7
J4EL+RC/2F1gmfEvMCf58rYENifz93c15KzjI9WIzhK9r51ZA8fR9ZyTMeOrkrakuwslx8gz2Nd8
GtHVrcoDTocBpHaQfUc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
B1N78V5eMBUsTRlKv5Ei+E+GTZzSlJSEsn133UlYjp9l9fAT6QvueXKaAUXJstu/mKRIG6pF2HeT
10E1KqusBF+N1vx0mJm+BGQmw8VwnDo/1C0bMq/T7k3skIGTTlpmmE04UhvTFZ5eNWSXF3hpRbnX
7LVz1QBuLq3y5vrznsX3rcSujxWkx6qVpUyrOGyzLDbbTs8ND/8qOUNz+Tsj2+A/fjXiwPURl7Ci
pqZKDVAkBrWTm4hLakBaJZUjicezF1sJAkTUk1hm+9WjXbPsux0DtpZ2Gw6FtD0vv/mLn3UAKK8N
uj2yxGASMChllxrDzGBUQlWEDBX02i/b8OMnHA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
XpZ6wtWFp3IdF+LL6kLXoj6b9D1J5fT9cn7l0UcxKWmgmibALHvlIKTmkHhvSMWpzdLg+g2Y9cR9
Xdwa5yU7eASZfV+yZyF+7zjm0hv0U/abI20xCZMsyk6cjZv540cB4pOOAicTUHAfkhgyFKxOawEz
7gTDQqPDwY340N+P5kDG60IIhd1sckAkBHBuXZxTS+zeS9nl/ZRlKChZ7x1memlhAbZe1zPqyKzm
vbtRY42ysY2LlvZYQlNVBe+50XEhHoxs3sowMrq5InmyUQBgGnBjgrWq1tno9aG/vnfxeC0jT9nV
i2WRgX0x6WkkieTwL0MiL8tN4DBh6VW/5X4U0A==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jxWZkPru9Zowzm3/Gw+3wooIfUSeqHKKh1JMXnMsgOecidVNEonsdn/lEFYVCrlQpMgWDqoir5FJ
AbaBAri9/Ng1+HSVqxErpHV0hALuog9ph3bkmIuRkSEBliXna0CO6nkHrQMEBpnumtpcmltskqpr
7r0A+52KNP7tX/QdfxRgRSb5JJQmVSwAiG/HK/XjlNfAZ2fXKLz5ieFjAM5E2CXTwNiTMApRI0l5
Dn6ZxFRxWWSWiLErO4Fm8rZgoKfzwXyt7TYyv/IrUJI+qw8wbLqrrCMMgEPaiQ6RDCrKjzurgXkE
pRuNDS0c2ydfgY7ywq32XSXvSaN5JHc1lvr99g==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4976)
`protect data_block
t4Wp7/3WCOvNCKQVr/EwL3M//PfHfaDjkm+/UUt3nVxttxXxqe46R57ZfqnVWmf67tN2Cqd1FQJP
4Gx4h79oo/zoEA9gHIfbfZ9hulRMpGnMMHP3E8Y2MSJojnUUmJ3W+4wt8cceYBLqeqjT7LybEhZZ
JJytfK1auZRjjBpbdGGJgQiS01rChArP8kMRhmqzzOD0NFDZ+0VvxQyL4+GzNYOcPBFFwxCTZawn
He5jjfIygCmLkxZgE9g12x1i/djX8YtGgrQQpAP+Op6oynL+o+5Omkzg3ZNjJRLbraBiOx0Kgexk
yVKVqpkI5aI9ZXbJMzVwkg4TCROcMhTW3n4Tp0dX601BoSzlVqi6tnPRTxeazQ/9M1M1X5JzGHsv
314AovuSRcrFQHTgrs00DvJuglGh9dif+0ENaBkY5KgdwVTViurIJMZLjgs4ytvnsS6pPNM+wkQj
++wQVJmKFXxWUsulgIOI6ADWYy1Tpu6mnKbMgKUffmW2m1BRlD54hPIomr/UTracODC7sHXPj9i+
K9sSH7PvYAublmqAWkBffNZGFR1OT+SfyCr0IXSrWqfJ6Zg9SyyoicZbvYVs6Uzh32TO7ewUK1DE
rG+2S/k6tpilfBQzoqWgdSSMXD5FFqCR8pqwK/IXot3r3vQcwsdNdMdXLoT4Kr3tz3/hBKDi63cc
BCMYBLJt9LQngBKAvz8RdSeqnZc9mdgEvswqRC1I0VNn1YgdAbd+yPxQpkwcugaCggD1cl8plDsM
tDCZRxUXUrm64ixs9+WrIbVOinfuTVkMfHCV9rRI5Gb8UDbAlYFD3TYBTo+K950sQHzilqy1hJZC
0mKLsGRLSwxdKD1Or9QMyr9TFvocDmj43jpuXK6D9+3DEo49UAkTR7NOPYAFYQcYJoRdwzjN/880
6NQ54uN7ns/PNufXLkA+/jHnpHiZgmBhVI2YoXw1BQfKHXoUNylB+0StKg0Y9SiweIX4+eJC2PvS
HCtP946s2wFq2G9PYc717qMY5Jx10cu+WIGG3MzaZB6cAMuS7D5piq5XqgD2lcb0ASpTPhDAfe9Q
Rt7IORXTt/LRRBLZEzLdPOk4m1o/76DWQyPGDgD1I3h7Q0YU+bbo4dhuQJtbNrSF4a/bcQZdQHm9
ct21PVhgWtYWBWoi4b84FCNZ84+1nDzqimY9B/Kx/Des7kJBNj7OCwWuNCm0hNYscJAj5algH0FP
6TxYLh9iPI24gYNcZvAgaDgvGkOOJuhhq0CBuAiEx+EjX5iphCA2zdfSyCkno6pJo0xfpVgSGtn5
7oZiKxGA/YsMKcw9js8e4ApJXubwApYFymrKYXhrEnUKxYO/V6Y1XKmP3LKUwAsc+6oChuezfdVT
7nRzGElpOBb9HZ5/vFlRSXQdCxguY2WCOZbf6r6XEYrkxXJbHxeMoOMDWfdAQjb1D/dC36ZV43nI
7/DksddDqc+amqadUWYMyfY6f9UknJiCu9iYx8cVoV7VRHf9UzSSTL0HhlTNPkaD8d8TR28R6+H1
QvtO7vGkHN9PUzGQnMkjX2wGWdaOZieRJ681AuyRD/MghdW4NqeMkilFvk6bP21jQgKdv0gl1s2D
taDDVsC/h1JfPW4WXnXgcdQh1r83uxBc1NUvwz0agHRlQmVhlUaOaouM51IMoIKmNJev0uAhD4jQ
PSjHC1K7DMprkZJxQkssEfOx2oDCxTLrqu0x0DWrCj7cZtay0CxhSiqyy7KZkbdkoMMPMhmEKMnJ
GR05jL45hUmhXP4x49ifj3x7tetuMISmJoSgr/I49P+9BOqZQftHq+TxuKEeSjzNl14cKd0+KYMi
joq1cO/IS9iRiTeilrmJdUEzNS7/gpUkfbIcbsoTPSUcqkKkxoqZoHSW5YaCbfKKQO6thojEpX5W
mI9uZClPxaefLirNFp2dgFZD8rdC4y8LPpi5YByvuiVgR/oWu9WdugqzUcYE7HYga8QxrOFEP1hQ
BOAPyrNwrzgd0UZJQkz9x2kQ4jnBXaH+urkt62yhv5NM4Vjwz95AyNk3wgrKeKeAabgMjo+hD9J/
y1KI5d1XkGwqK9JK5AwYdalskMfSANWrj5l2w9EoDq5CP/lRz7am+feq6FcwR318CAEnS4lvqv+A
OmNtM8xFt/AQdpP4UZzPVw8PObL5InhWp4Qo+eBYIAjyA2pHUQBD+BR8NOn6ClkdIii577/RIQ3p
y5IUKUCOEvXAQ6hWxJb3jEgaH+7G/bCckreOlzK0j6n+B4Ft8eGdTIlVw3pEgPwZe1ah0jl6DjP7
aISXnM/tkCzMVy41nYT0EW92tPG+NmRIzSkvztAsocCCtaTqkwCGh03u/l++jm5fNgL3CgtdpYPy
SVl3UG57/31ZMu0NraXdd2hAR72DtyFZVqTdR1xTE4zrpvJstPqiza1zJpEXk60vKivIbIxIF2J0
bkyS4MYOYjDGtGmu+GvhilP15lzfa6BzPEy9344EeDa8RNoW21Rww/frmv/4ieU4TCg8ea1b57Cs
/Pz9zvsS1cIEA8UJEnKNK7xs2cmGVnQ2kCtlmqpfN6GXjL8sdoztWCPbGVJVmWnbDoH8eItV6TJK
9ZrYM+DTkBR0gDohLjJF/4S1ACqTmy6qEs1swtlwWfOA27iIrDYiwl+lyn5+pM6N91v9jADCE4cm
AJPof9TM6S1XU5RFv+64gdy/Ib7RW1UFPhpfT4XBOjJPDSTgKijzXJXdSq4XvQglcKeGHvjPoql0
BjT7l1zt4GTY2FNTTLUXCusLbfhqJoJFiwFGUhB11Jz9DBR+lGGDHdNZyzgDpkiffPYKoPL8I3Qn
p+rGImWwWCpVLD9RaUlj1k4hP2HK2lvEhrvZaoC0oKjQv0J4kmTn/n/SNO0Yo95hQj/ZH1ZgcOtW
5o2NO2/pXYrTBQAk5gY3cA7Uo8lR5Eifqn/MKGLKvGGka96ovMApsJ0hlFOINal39XRd+uMfF8dh
Qn1KOFqj1cAqrj4FdAxjcsB63/0RAZJ7pfj4jvGNcFia7QLH2tK+fFnVQmmi5/IFVnrQsPghaWC9
fkJ/I4i4sXG/hHR/i+LpZ4uOyJwMbFPueRi07++QcFSzbYTIHVmOx+J1dCWM1m6b/M61GIsJ5A4r
wH4v48Fruu2KximtIrCcJb1VbG0pP7yRySgBSmDKu1A0y3Ksh+YQfmA5ufqJKuzycrGCYssU876T
Xfhl/y9dzr6TDjoQ5qibh0oeBDuC7aOgtV4extUE3IMcquJNINnIBtRVllhQ2oe0d7UVd3siYRwm
FqbzPnlSJZTRIL/1xVCiLhp4Z87W9CRPhiBXRJnTwa/0J2rr6lEVbOoQYwhk9IYJm0BJ249KNtwi
vkR5jRcFko9q9fkb/xxlyb1NYEquEFixlrgznsoiMPcB9l04AkP/c/s9s9jet0Nm5tm5qi/iMEtz
AN8sZQgrsD2q5gM8iBzyYq6IONktcfEEX5SzlZksnuZq4LfWFaF/Yc1gvCFCJmnd/77Fz5w4Dau1
yZAkliYnTVtWArDkyHbPRNrY+Uxrr0PjArGT4BKgPIZKSrB/JnaVKdRPzNTR2raCigmKCz6jchNb
6RTwf+S22suYzNGzKS0dLdjGzXiYESr49ANEo7yyzch3QR8VyEN+GarIKNUgyEdWW6XD+IkOMvWM
9QzftU04nahmCLwgppn4h55dBOl20YZOqtUQZrnJld0Hj50bATGbAF2+ZB41wiMkOEQgxFe54q21
ecUTOgSuC6Tuq4PgOp/O+2bbXJu5XgKWvIRM6S8OB4pUCoepPn/pWNULjyOEhol4cfbQtDPYoehp
GCVVEj3XS3kS8PRMj0/VTEUFX76xgRFIz6EvEZGspXJd6ZkTobpyoENXsNLdeGd3ID7BoYZYJhmv
kej1ohX8hWj+qOOqa/LuPwFKaN0Rk883Q+szc18JbBszYT/my4PRtH3uzlHxU73F2QYgPN+LEVLx
lfojaZYdNv0lA3139av8ZHp/2ZVrJaQtnpJwhzE25xTyGIW3v854oB74nZ0FYfxnDeaoxVWemZ9D
H1ERUCL3I2H5fo8rx+Q2YQ+DwQNgisbHcq6+Mf1vn8gCTIkRgjksK3Yd6oI4oS2BCmMuxXvAS1Au
B2+b3qsrg7DfqMUUfnR0+jsnCBMTuq8vbMjk/k9bVrS/PVS46oDEq8hX3CINw6UZFheo2NBHxB6q
dLPp+uMRrmFpd9GdYkLdZtwcU8M1hglbBqNdo+pm3VirLKyV+lFPrXoI+FXGchinlOdYtMkwsgmh
1/94pHkvyDuD/FK+uAIyxFAbxLA25ies5bhOHqW3AdvYiYJ58fGUF2tIuPNZullUvKUybhiJQOaW
CGByJBhCVcev64HRDOfDJQ/MmS5+nkYhc9fBT/FynQbr7B/JLjstfsoDcMhd4hkjQDcmDBTCivjP
uTtyz4JkCf2NBkXldKg+QIPcoC3m1tgdyPnB6gzvsYb8LNPOI0sRfOLJWZ8/+0kQ1W8nbJKLuVqO
Vcw1kw2mDUXWOyMOko7TrozhC3UhEPXCaW7PJmbVq+hSLABK9sxNZ7md76sb00UER/fDfT7YpU4/
rheaEyLpB0cW4XEAYqvMsJs7HOL3lxAmUXBPDdzRu2VY7n+QXIOr0YVfyX9k8qF+7Voc0IpP5dZJ
ND/0kyU7FzoMyVePBXmHeg6pVz5Fw29E7cW6qDNnwVuaKI4V08d7we/a4uDBK2VpMoCaZmzL2/vS
jhDyfwI9wHtf9AJPQHysU7kaZ+CvWngHrLzi1FMqMkctk4l72hxifR8yXiwydeQ7QBdqb+BrUw6z
zwj66f/EgIt1nzeYibBpNeZxXMgZgu851D0Chdmf8xpxXxv8RpvPJP6m7g87LusexFVdBHje9URL
A8oDbgTX3HU9H+0uT+H3mhhldkOqq0CyVgIJSG89XX9kMBtu1bHUe8sUSJ814Ewo09TZFJWsWeFM
/Mrz77d464UkO8ia/GdFMq3az/iL5zlvpX6ALBU21CdsufjK4zBCpvltX5rQWx1l+kZjd90pGUoc
ditTbGfCvOuIpO22k2DfLE/d1f+MCWSwKVtKDVlbLOWXVd/Q1Qn7lFKthrMLIXDgCdMd1kA50Uzj
NwFMXgd6PZWiCFlOfnL4KVw0Q+O9p6HWwFu5eApwGbmCfkh7oBuO9OvGe7Vjrf8tyWPslW3S/5T4
XIQ3c6N0ZwTNHlFVM8gB9XVGZjoLqI5tontb/Mku4dNI/DcV/wA8gyJP/qCBuQa2oBTqP/ZZeH9Z
/MH+QPWfxV/y+JfKItYa8UU15RzX8GUVt38ORjw3eH4kO6T/qN2EPW0U0I09pJm6A20xPLrUkQ0j
/J28v3uS5v6jD6IDwHF1N8ExbOScYNLuZKvc/nDkMeeWHmeye8OsJoZRW4WKc5QVZ+Rliav1rNeE
9iLOJis8/OtCjNxPYBfqZIXudncRzXVsYOknrxXmdjXYrNvpHk7A/GMe2kGmzIzTzu/flF5uaMNE
IHUziF5GU1svdXd1U60HBhY6NbtgL+lbBsWQguM5cpW5S5mn5ukOdXHl8xv/X4SqHXqXoWhq9GP4
NKaHxGg1b+QPNrEF/lPmnBo75THxcIwOtI+7qLUb6ymTuOT+6rJwaV0LS01X4ifqVIBLYp+y8yDU
dl9i+MuPjCulczcS7dV84lcRiLB42KhxMgsDRRAsHgToXX13rUUa+NTqs3/jw9ozqyIIm8U1GTnZ
9+G6IVNsKHFDKgBxoZuRDMk+3rTbL3fUpC1Ml11qgu7Ua/Ddfct1p2T8v1AY9n5H8FkyL5GIz/jV
LKX5Ab00k6ULIfnhK9GGB1qF00VFZtkat7KKROPZB9ejlGIdITokU8qriCiBtyS9lGDipL1wGwAv
BlXAGMrbSEz7w1+Q3DOlzDQg2313Kr2IMub2yUykgPWgiJKM/EZfm1+/hZWtPEno7qHrdbLjCJ3Y
Aptyr/t/tFlye7fn34CgsSiuQ3SjiAVPw4aw4BD1Z8FFMZaN0rjMnXBaA0urgp/pgzSvwCmOPf4R
jpbjCJ9dcMdg1yA5kPAzKOh3iy2YmFSukTKC3Ato4mj6ckGT9rAARcPYJKvFjh0Jie0qwV+AGXq1
MaOt+MAaASxbo2S/NHPF8Pu2gYfoKTCZusyzX809mH/F6ED3M4xkWfVKdcq9s3VCZgSj8ysMr9d6
0vbG8jwaeg8+VqoPnObnzFNzVL5lgVt8Yh7xsCuH9wJ3hs2qt+3wPznsD1de0AVZoTUri83VKviB
6CfH62iXfTrfzkjHZViUVZFYZA36aamQSXsSbiU9/WVsKDWBffvMjGQ+ngfcG9h5bHWGt4XnmcYM
HNXh38DHDl0MjAcJnXj81M/Id7oYkDAYrdedwpYlqplnDr502aNY/7iCMYKQezPlMF7cQ7N3wpGc
9E8DwTL6AkO0PMEbQRox+M7gezwltsTp7fgw2JH2jvnun5GxrPENjqzifx/P/GPcILPqnsWC2ERU
2jZd6XBPXB7ySkHYSLapVg0Gy7KWmP2paIDQpFxokr9Ma1D7JlvvDsp6dvGQwllf7rCQ31K036sO
8zFSt04oWp/xut69xU+Kw5jJsj3AG6632uivdfy8jejX0Xqze6I6dLl5cS9WBqU9WpIM/q/hwuHw
1jh7tjOIC+qAgeZ1MCHk1dk=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 : entity is "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 : entity is "blk_mem_gen_v8_4_3,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     10.698 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 1;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_scope_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_scope_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 2;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of clkb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK";
  attribute X_INTERFACE_PARAMETER of clkb : signal is "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of enb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of addrb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of dinb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of doutb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
  attribute X_INTERFACE_INFO of web : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB WE";
begin
U0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => enb,
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(10 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(10 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(10 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(10 downto 0),
      s_axi_rdata(31 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 : entity is "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_3,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 : entity is "blk_mem_gen_v8_4_3,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_doutb_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     5.6824 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_scope_blk_mem_gen_i1.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_scope_blk_mem_gen_i1.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => B"00000000000",
      clka => clka,
      clkb => '0',
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => B"00000000000000000000000000000000",
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => NLW_U0_doutb_UNCONNECTED(31 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(10 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(10 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(10 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(10 downto 0),
      s_axi_rdata(31 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 10 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 11;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_i_synth_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 1;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 11;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "1";
  attribute c_has_load of i_synth : label is 1;
  attribute c_has_thresh0 of i_synth : label is 0;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 0;
  attribute c_thresh0_value of i_synth : label is "1";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
  THRESH0 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13_viv
     port map (
      CE => CE,
      CLK => CLK,
      L(10 downto 0) => L(10 downto 0),
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_i_synth_THRESH0_UNCONNECTED,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 : entity is "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 : entity is "c_counter_binary_v12_0_13,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 is
  signal NLW_U0_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 11;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "1";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 1;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 0;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "1";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of LOAD : signal is "xilinx.com:signal:data:1.0 load_intf DATA";
  attribute X_INTERFACE_PARAMETER of LOAD : signal is "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of SINIT : signal is "xilinx.com:signal:data:1.0 sinit_intf DATA";
  attribute X_INTERFACE_PARAMETER of SINIT : signal is "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of L : signal is "xilinx.com:signal:data:1.0 l_intf DATA";
  attribute X_INTERFACE_PARAMETER of L : signal is "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute X_INTERFACE_PARAMETER of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13
     port map (
      CE => CE,
      CLK => CLK,
      L(10 downto 0) => L(10 downto 0),
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_U0_THRESH0_UNCONNECTED,
      UP => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram is
  port (
    r9_douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r2_wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    r1_addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r3_dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram is
  signal \comp0.core_instance0_n_32\ : STD_LOGIC;
  signal \comp0.core_instance0_n_33\ : STD_LOGIC;
  signal \comp0.core_instance0_n_34\ : STD_LOGIC;
  signal \comp0.core_instance0_n_35\ : STD_LOGIC;
  signal \comp0.core_instance0_n_36\ : STD_LOGIC;
  signal \comp0.core_instance0_n_37\ : STD_LOGIC;
  signal \comp0.core_instance0_n_38\ : STD_LOGIC;
  signal \comp0.core_instance0_n_39\ : STD_LOGIC;
  signal \comp0.core_instance0_n_40\ : STD_LOGIC;
  signal \comp0.core_instance0_n_41\ : STD_LOGIC;
  signal \comp0.core_instance0_n_42\ : STD_LOGIC;
  signal \comp0.core_instance0_n_43\ : STD_LOGIC;
  signal \comp0.core_instance0_n_44\ : STD_LOGIC;
  signal \comp0.core_instance0_n_45\ : STD_LOGIC;
  signal \comp0.core_instance0_n_46\ : STD_LOGIC;
  signal \comp0.core_instance0_n_47\ : STD_LOGIC;
  signal \comp0.core_instance0_n_48\ : STD_LOGIC;
  signal \comp0.core_instance0_n_49\ : STD_LOGIC;
  signal \comp0.core_instance0_n_50\ : STD_LOGIC;
  signal \comp0.core_instance0_n_51\ : STD_LOGIC;
  signal \comp0.core_instance0_n_52\ : STD_LOGIC;
  signal \comp0.core_instance0_n_53\ : STD_LOGIC;
  signal \comp0.core_instance0_n_54\ : STD_LOGIC;
  signal \comp0.core_instance0_n_55\ : STD_LOGIC;
  signal \comp0.core_instance0_n_56\ : STD_LOGIC;
  signal \comp0.core_instance0_n_57\ : STD_LOGIC;
  signal \comp0.core_instance0_n_58\ : STD_LOGIC;
  signal \comp0.core_instance0_n_59\ : STD_LOGIC;
  signal \comp0.core_instance0_n_60\ : STD_LOGIC;
  signal \comp0.core_instance0_n_61\ : STD_LOGIC;
  signal \comp0.core_instance0_n_62\ : STD_LOGIC;
  signal \comp0.core_instance0_n_63\ : STD_LOGIC;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_3,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0
     port map (
      addra(10 downto 0) => r1_addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => axibusdomain_clk,
      clkb => signaldomain_clk,
      dina(31 downto 0) => r3_dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => r9_douta(31 downto 0),
      doutb(31) => \comp0.core_instance0_n_32\,
      doutb(30) => \comp0.core_instance0_n_33\,
      doutb(29) => \comp0.core_instance0_n_34\,
      doutb(28) => \comp0.core_instance0_n_35\,
      doutb(27) => \comp0.core_instance0_n_36\,
      doutb(26) => \comp0.core_instance0_n_37\,
      doutb(25) => \comp0.core_instance0_n_38\,
      doutb(24) => \comp0.core_instance0_n_39\,
      doutb(23) => \comp0.core_instance0_n_40\,
      doutb(22) => \comp0.core_instance0_n_41\,
      doutb(21) => \comp0.core_instance0_n_42\,
      doutb(20) => \comp0.core_instance0_n_43\,
      doutb(19) => \comp0.core_instance0_n_44\,
      doutb(18) => \comp0.core_instance0_n_45\,
      doutb(17) => \comp0.core_instance0_n_46\,
      doutb(16) => \comp0.core_instance0_n_47\,
      doutb(15) => \comp0.core_instance0_n_48\,
      doutb(14) => \comp0.core_instance0_n_49\,
      doutb(13) => \comp0.core_instance0_n_50\,
      doutb(12) => \comp0.core_instance0_n_51\,
      doutb(11) => \comp0.core_instance0_n_52\,
      doutb(10) => \comp0.core_instance0_n_53\,
      doutb(9) => \comp0.core_instance0_n_54\,
      doutb(8) => \comp0.core_instance0_n_55\,
      doutb(7) => \comp0.core_instance0_n_56\,
      doutb(6) => \comp0.core_instance0_n_57\,
      doutb(5) => \comp0.core_instance0_n_58\,
      doutb(4) => \comp0.core_instance0_n_59\,
      doutb(3) => \comp0.core_instance0_n_60\,
      doutb(2) => \comp0.core_instance0_n_61\,
      doutb(1) => \comp0.core_instance0_n_62\,
      doutb(0) => \comp0.core_instance0_n_63\,
      ena => '1',
      enb => '1',
      wea(0) => r2_wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram is
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_3,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_3,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1
     port map (
      addra(10 downto 0) => Q(10 downto 0),
      clka => signaldomain_clk,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => dinb(31 downto 0),
      ena => '1',
      wea(0) => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free is
  port (
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    LOAD : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free is
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_counter_binary_v12_0_13,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0
     port map (
      CE => '1',
      CLK => signaldomain_clk,
      L(10 downto 0) => B"00000000000",
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SINIT => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    o : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline is
  signal counter_op_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal relational_op_net : STD_LOGIC;
begin
counter: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free
     port map (
      LOAD => relational_op_net,
      Q(10 downto 0) => counter_op_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
relational: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88
     port map (
      LOAD => relational_op_net,
      Q(10 downto 0) => counter_op_net(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
single_port_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram
     port map (
      Q(10 downto 0) => counter_op_net(10 downto 0),
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain is
  port (
    addrb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \full_i_5_24_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain is
  signal \^full_i_5_24_reg[0]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register1_n_0 : STD_LOGIC;
  signal register1_n_1 : STD_LOGIC;
  signal register1_n_2 : STD_LOGIC;
  signal register1_n_20 : STD_LOGIC;
  signal register1_n_21 : STD_LOGIC;
  signal register1_n_22 : STD_LOGIC;
  signal register1_n_23 : STD_LOGIC;
  signal register1_n_3 : STD_LOGIC;
  signal register1_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register2_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register3_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register4_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register5_n_0 : STD_LOGIC;
  signal register5_n_1 : STD_LOGIC;
  signal register5_n_2 : STD_LOGIC;
  signal register5_n_20 : STD_LOGIC;
  signal register5_n_21 : STD_LOGIC;
  signal register5_n_22 : STD_LOGIC;
  signal register5_n_23 : STD_LOGIC;
  signal register5_n_3 : STD_LOGIC;
  signal register5_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register_q_net : STD_LOGIC;
  signal risingedgetrigger_n_0 : STD_LOGIC;
begin
  \full_i_5_24_reg[0]\(0) <= \^full_i_5_24_reg[0]\(0);
convert: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2\(0) => \^full_i_5_24_reg[0]\(0),
      signaldomain_clk => signaldomain_clk
    );
delayline: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline
     port map (
      dina(31 downto 16) => register2_q_net_x0(15 downto 0),
      dina(15 downto 0) => register3_q_net_x0(15 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
leveltriggerfifocontroller: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller
     port map (
      DI(3) => register1_n_0,
      DI(2) => register1_n_1,
      DI(1) => register1_n_2,
      DI(0) => register1_n_3,
      S(3) => register5_n_0,
      S(2) => register5_n_1,
      S(1) => register5_n_2,
      S(0) => register5_n_3,
      addrb(10 downto 0) => addrb(10 downto 0),
      \full_i_5_24_reg[0]\(0) => \^full_i_5_24_reg[0]\(0),
      register4_q_net(0) => register4_q_net(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk,
      \state_4_23_reg[1]\(3) => register5_n_23,
      \state_4_23_reg[1]\(2) => register1_n_20,
      \state_4_23_reg[1]\(1) => register1_n_21,
      \state_4_23_reg[1]\(0) => register1_n_22,
      \state_4_23_reg[1]_0\(3) => register1_n_23,
      \state_4_23_reg[1]_0\(2) => register5_n_20,
      \state_4_23_reg[1]_0\(1) => register5_n_21,
      \state_4_23_reg[1]_0\(0) => register5_n_22,
      \state_4_23_reg[1]_1\ => risingedgetrigger_n_0,
      web(0) => web(0)
    );
register1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\
     port map (
      DI(3) => register1_n_0,
      DI(2) => register1_n_1,
      DI(1) => register1_n_2,
      DI(0) => register1_n_3,
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2) => register1_n_20,
      \fd_prim_array[13].bit_is_0.fdre_comp\(1) => register1_n_21,
      \fd_prim_array[13].bit_is_0.fdre_comp\(0) => register1_n_22,
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => register1_n_23,
      o(15 downto 0) => register1_q_net_x0(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => register5_q_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => register2_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => register3_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register4: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
register5: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\
     port map (
      S(3) => register5_n_0,
      S(2) => register5_n_1,
      S(1) => register5_n_2,
      S(0) => register5_n_3,
      \fd_prim_array[13].bit_is_0.fdre_comp\(2) => register5_n_20,
      \fd_prim_array[13].bit_is_0.fdre_comp\(1) => register5_n_21,
      \fd_prim_array[13].bit_is_0.fdre_comp\(0) => register5_n_22,
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => register5_n_23,
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => register5_q_net(15 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => register1_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
risingedgetrigger: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger
     port map (
      \ff_2_17_reg[0]\ => risingedgetrigger_n_0,
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    r9_douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r2_wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    r1_addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r3_dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct is
  signal convert_dout_net_x1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal mcode_we_net : STD_LOGIC;
  signal register1_q_net : STD_LOGIC;
  signal register2_q_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal register3_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal single_port_ram_data_out_net : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slice1_y_net : STD_LOGIC_VECTOR ( 10 downto 0 );
begin
dual_port_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram
     port map (
      addrb(10 downto 0) => slice1_y_net(10 downto 0),
      axibusdomain_clk => axibusdomain_clk,
      dinb(31 downto 0) => single_port_ram_data_out_net(31 downto 0),
      r1_addra(10 downto 0) => r1_addra(10 downto 0),
      r2_wea(0) => r2_wea(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      signaldomain_clk => signaldomain_clk,
      web(0) => mcode_we_net
    );
register1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register1_q_net,
      r5_enable(0) => r5_enable(0),
      signaldomain_clk => signaldomain_clk
    );
register2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => register2_q_net(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register3_q_net(0),
      r7_clear(0) => r7_clear(0),
      signaldomain_clk => signaldomain_clk
    );
register4: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      full(0) => convert_dout_net_x1(0),
      r8_full(0) => r8_full(0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => register_q_net(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
signaldomain: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain
     port map (
      addrb(10 downto 0) => slice1_y_net(10 downto 0),
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      clear(0) => register3_q_net(0),
      dinb(31 downto 0) => single_port_ram_data_out_net(31 downto 0),
      enable(0) => register1_q_net,
      full(0) => convert_dout_net_x1(0),
      \full_i_5_24_reg[0]\(0) => full(0),
      i(15 downto 0) => register_q_net(15 downto 0),
      o(10 downto 0) => register2_q_net(10 downto 0),
      signaldomain_clk => signaldomain_clk,
      web(0) => mcode_we_net
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope is
  port (
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope is
  signal \<const0>\ : STD_LOGIC;
  signal r1_addra : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal r2_wea : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r3_dina : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r4_threshold : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r5_enable : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r6_delay : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal r7_clear : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r8_full : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r9_douta : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  axibusdomain_s_axi_bresp(1) <= \<const0>\;
  axibusdomain_s_axi_bresp(0) <= \<const0>\;
  axibusdomain_s_axi_rresp(1) <= \<const0>\;
  axibusdomain_s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
axibusdomain_axi_lite_interface: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => r5_enable(0),
      i(10 downto 0) => r6_delay(10 downto 0),
      q(0) => r8_full(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      \slv_reg_array_reg[0][0]\(0) => r7_clear(0),
      \slv_reg_array_reg[3][15]\(15 downto 0) => r4_threshold(15 downto 0),
      \slv_reg_array_reg[5][0]\(0) => r2_wea(0),
      \slv_reg_array_reg[6][10]\(10 downto 0) => r1_addra(10 downto 0)
    );
ip_scope_struct: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct
     port map (
      axibusdomain_clk => axibusdomain_clk,
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      full(0) => full(0),
      r1_addra(10 downto 0) => r1_addra(10 downto 0),
      r2_wea(0) => r2_wea(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0),
      r5_enable(0) => r5_enable(0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0),
      r7_clear(0) => r7_clear(0),
      r8_full(0) => r8_full(0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ip_scope_0_0,ip_scope,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sysgen";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ip_scope,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of axibusdomain_aresetn : signal is "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of axibusdomain_aresetn : signal is "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_clk : signal is "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of axibusdomain_clk : signal is "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID";
  attribute X_INTERFACE_PARAMETER of axibusdomain_s_axi_rvalid : signal is "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID";
  attribute X_INTERFACE_INFO of signaldomain_clk : signal is "xilinx.com:signal:clock:1.0 signaldomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of signaldomain_clk : signal is "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB";
  attribute X_INTERFACE_INFO of ch1 : signal is "xilinx.com:signal:data:1.0 ch1 DATA";
  attribute X_INTERFACE_PARAMETER of ch1 : signal is "XIL_INTERFACENAME ch1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of ch2 : signal is "xilinx.com:signal:data:1.0 ch2 DATA";
  attribute X_INTERFACE_PARAMETER of ch2 : signal is "XIL_INTERFACENAME ch2, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of ch_trigger : signal is "xilinx.com:signal:data:1.0 ch_trigger DATA";
  attribute X_INTERFACE_PARAMETER of ch_trigger : signal is "XIL_INTERFACENAME ch_trigger, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of full : signal is "xilinx.com:signal:interrupt:1.0 full INTERRUPT";
  attribute X_INTERFACE_PARAMETER of full : signal is "XIL_INTERFACENAME full, SENSITIVITY LEVEL_HIGH, PortWidth 1";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bresp(1 downto 0) => axibusdomain_s_axi_bresp(1 downto 0),
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rresp(1 downto 0) => axibusdomain_s_axi_rresp(1 downto 0),
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
