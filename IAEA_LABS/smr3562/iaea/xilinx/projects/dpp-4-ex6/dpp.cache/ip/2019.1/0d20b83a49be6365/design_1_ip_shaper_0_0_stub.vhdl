-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Feb 14 16:49:25 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_shaper_0_0_stub.vhdl
-- Design      : design_1_ip_shaper_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    data_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axi_clk_domain_clk : in STD_LOGIC;
    filter_clk_domain_clk : in STD_LOGIC;
    axi_clk_domain_aresetn : in STD_LOGIC;
    axi_clk_domain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_clk_domain_s_axi_awvalid : in STD_LOGIC;
    axi_clk_domain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_clk_domain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_clk_domain_s_axi_wvalid : in STD_LOGIC;
    axi_clk_domain_s_axi_bready : in STD_LOGIC;
    axi_clk_domain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_clk_domain_s_axi_arvalid : in STD_LOGIC;
    axi_clk_domain_s_axi_rready : in STD_LOGIC;
    impulse_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rect_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    shaper_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    axi_clk_domain_s_axi_awready : out STD_LOGIC;
    axi_clk_domain_s_axi_wready : out STD_LOGIC;
    axi_clk_domain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_clk_domain_s_axi_bvalid : out STD_LOGIC;
    axi_clk_domain_s_axi_arready : out STD_LOGIC;
    axi_clk_domain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_clk_domain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_clk_domain_s_axi_rvalid : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "data_in[15:0],axi_clk_domain_clk,filter_clk_domain_clk,axi_clk_domain_aresetn,axi_clk_domain_s_axi_awaddr[4:0],axi_clk_domain_s_axi_awvalid,axi_clk_domain_s_axi_wdata[31:0],axi_clk_domain_s_axi_wstrb[3:0],axi_clk_domain_s_axi_wvalid,axi_clk_domain_s_axi_bready,axi_clk_domain_s_axi_araddr[4:0],axi_clk_domain_s_axi_arvalid,axi_clk_domain_s_axi_rready,impulse_out[15:0],rect_out[15:0],shaper_out[15:0],axi_clk_domain_s_axi_awready,axi_clk_domain_s_axi_wready,axi_clk_domain_s_axi_bresp[1:0],axi_clk_domain_s_axi_bvalid,axi_clk_domain_s_axi_arready,axi_clk_domain_s_axi_rdata[31:0],axi_clk_domain_s_axi_rresp[1:0],axi_clk_domain_s_axi_rvalid";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "ip_shaper,Vivado 2019.1";
begin
end;
