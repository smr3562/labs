-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Feb 14 16:33:15 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_dc_stabilizer_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_dc_stabilizer_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dc_stabilizer is
  port (
    dbg_acc : out STD_LOGIC_VECTOR ( 15 downto 0 );
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 );
    resetn : in STD_LOGIC;
    feedback : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \acc321_carry__0_0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \acc321_inferred__0/i__carry__0_0\ : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dc_stabilizer;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dc_stabilizer is
  signal RESIZE : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal acc321 : STD_LOGIC;
  signal acc3210_in : STD_LOGIC;
  signal \acc321_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \acc321_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \acc321_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \acc321_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \acc321_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \acc321_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \acc321_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \acc321_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \acc321_carry__0_n_1\ : STD_LOGIC;
  signal \acc321_carry__0_n_2\ : STD_LOGIC;
  signal \acc321_carry__0_n_3\ : STD_LOGIC;
  signal acc321_carry_i_1_n_0 : STD_LOGIC;
  signal acc321_carry_i_2_n_0 : STD_LOGIC;
  signal acc321_carry_i_3_n_0 : STD_LOGIC;
  signal acc321_carry_i_4_n_0 : STD_LOGIC;
  signal acc321_carry_i_5_n_0 : STD_LOGIC;
  signal acc321_carry_i_6_n_0 : STD_LOGIC;
  signal acc321_carry_i_7_n_0 : STD_LOGIC;
  signal acc321_carry_i_8_n_0 : STD_LOGIC;
  signal acc321_carry_n_0 : STD_LOGIC;
  signal acc321_carry_n_1 : STD_LOGIC;
  signal acc321_carry_n_2 : STD_LOGIC;
  signal acc321_carry_n_3 : STD_LOGIC;
  signal \acc321_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \acc321_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \acc321_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \acc321_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \acc321_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \acc321_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \acc321_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \acc32[11]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[11]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[11]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[11]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[14]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[14]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[14]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[14]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[16]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[16]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[16]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[16]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[20]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[20]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[20]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[20]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[24]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[24]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[24]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[24]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[31]_i_1_n_0\ : STD_LOGIC;
  signal \acc32[31]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[31]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[31]_i_6_n_0\ : STD_LOGIC;
  signal \acc32[31]_i_7_n_0\ : STD_LOGIC;
  signal \acc32[3]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[3]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[3]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[3]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[7]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[7]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[7]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[7]_i_5_n_0\ : STD_LOGIC;
  signal acc32_reg : STD_LOGIC_VECTOR ( 30 downto 15 );
  signal \acc32_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[14]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[14]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[14]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[14]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[14]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[14]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[14]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[14]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[31]_i_3_n_1\ : STD_LOGIC;
  signal \acc32_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \acc32_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \acc32_reg[31]_i_3_n_4\ : STD_LOGIC;
  signal \acc32_reg[31]_i_3_n_5\ : STD_LOGIC;
  signal \acc32_reg[31]_i_3_n_6\ : STD_LOGIC;
  signal \acc32_reg[31]_i_3_n_7\ : STD_LOGIC;
  signal \acc32_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \^dbg_acc\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal en : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal p_2_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \result3_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_12_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__0_n_0\ : STD_LOGIC;
  signal \result3_carry__0_n_1\ : STD_LOGIC;
  signal \result3_carry__0_n_2\ : STD_LOGIC;
  signal \result3_carry__0_n_3\ : STD_LOGIC;
  signal \result3_carry__0_n_4\ : STD_LOGIC;
  signal \result3_carry__0_n_5\ : STD_LOGIC;
  signal \result3_carry__0_n_6\ : STD_LOGIC;
  signal \result3_carry__0_n_7\ : STD_LOGIC;
  signal \result3_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__1_n_0\ : STD_LOGIC;
  signal \result3_carry__1_n_1\ : STD_LOGIC;
  signal \result3_carry__1_n_2\ : STD_LOGIC;
  signal \result3_carry__1_n_3\ : STD_LOGIC;
  signal \result3_carry__1_n_4\ : STD_LOGIC;
  signal \result3_carry__1_n_5\ : STD_LOGIC;
  signal \result3_carry__1_n_6\ : STD_LOGIC;
  signal \result3_carry__1_n_7\ : STD_LOGIC;
  signal \result3_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__2_n_0\ : STD_LOGIC;
  signal \result3_carry__2_n_1\ : STD_LOGIC;
  signal \result3_carry__2_n_2\ : STD_LOGIC;
  signal \result3_carry__2_n_3\ : STD_LOGIC;
  signal \result3_carry__2_n_5\ : STD_LOGIC;
  signal \result3_carry__2_n_6\ : STD_LOGIC;
  signal \result3_carry__2_n_7\ : STD_LOGIC;
  signal \result3_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_11_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_14_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_15_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_16_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_17_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_18_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_19_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_20_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_21_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_22_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__3_n_0\ : STD_LOGIC;
  signal \result3_carry__3_n_1\ : STD_LOGIC;
  signal \result3_carry__3_n_2\ : STD_LOGIC;
  signal \result3_carry__3_n_3\ : STD_LOGIC;
  signal \result3_carry__4_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_11_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_12_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_13_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_14_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_15_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_16_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_17_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__4_n_0\ : STD_LOGIC;
  signal \result3_carry__4_n_1\ : STD_LOGIC;
  signal \result3_carry__4_n_2\ : STD_LOGIC;
  signal \result3_carry__4_n_3\ : STD_LOGIC;
  signal \result3_carry__5_i_14_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_15_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__5_n_0\ : STD_LOGIC;
  signal \result3_carry__5_n_1\ : STD_LOGIC;
  signal \result3_carry__5_n_2\ : STD_LOGIC;
  signal \result3_carry__5_n_3\ : STD_LOGIC;
  signal \result3_carry__6_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_11_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__6_n_1\ : STD_LOGIC;
  signal \result3_carry__6_n_2\ : STD_LOGIC;
  signal \result3_carry__6_n_3\ : STD_LOGIC;
  signal result3_carry_i_10_n_0 : STD_LOGIC;
  signal result3_carry_i_11_n_0 : STD_LOGIC;
  signal result3_carry_i_12_n_0 : STD_LOGIC;
  signal result3_carry_i_13_n_0 : STD_LOGIC;
  signal result3_carry_i_14_n_0 : STD_LOGIC;
  signal result3_carry_i_15_n_0 : STD_LOGIC;
  signal result3_carry_i_16_n_0 : STD_LOGIC;
  signal result3_carry_i_17_n_0 : STD_LOGIC;
  signal result3_carry_i_18_n_0 : STD_LOGIC;
  signal result3_carry_i_19_n_0 : STD_LOGIC;
  signal result3_carry_i_1_n_0 : STD_LOGIC;
  signal result3_carry_i_20_n_0 : STD_LOGIC;
  signal result3_carry_i_21_n_0 : STD_LOGIC;
  signal result3_carry_i_2_n_0 : STD_LOGIC;
  signal result3_carry_i_3_n_0 : STD_LOGIC;
  signal result3_carry_i_4_n_0 : STD_LOGIC;
  signal result3_carry_i_5_n_0 : STD_LOGIC;
  signal result3_carry_i_6_n_0 : STD_LOGIC;
  signal result3_carry_i_7_n_0 : STD_LOGIC;
  signal result3_carry_i_8_n_0 : STD_LOGIC;
  signal result3_carry_i_9_n_0 : STD_LOGIC;
  signal result3_carry_n_0 : STD_LOGIC;
  signal result3_carry_n_1 : STD_LOGIC;
  signal result3_carry_n_2 : STD_LOGIC;
  signal result3_carry_n_3 : STD_LOGIC;
  signal result3_carry_n_4 : STD_LOGIC;
  signal result3_carry_n_5 : STD_LOGIC;
  signal result3_carry_n_6 : STD_LOGIC;
  signal result3_carry_n_7 : STD_LOGIC;
  signal \result_reg[0]_C_n_0\ : STD_LOGIC;
  signal \result_reg[0]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[0]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[0]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[0]_P_n_0\ : STD_LOGIC;
  signal \result_reg[10]_C_n_0\ : STD_LOGIC;
  signal \result_reg[10]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[10]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[10]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[10]_P_n_0\ : STD_LOGIC;
  signal \result_reg[11]_C_n_0\ : STD_LOGIC;
  signal \result_reg[11]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[11]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[11]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[11]_P_n_0\ : STD_LOGIC;
  signal \result_reg[12]_C_n_0\ : STD_LOGIC;
  signal \result_reg[12]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[12]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[12]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[12]_P_n_0\ : STD_LOGIC;
  signal \result_reg[13]_C_n_0\ : STD_LOGIC;
  signal \result_reg[13]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[13]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[13]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[13]_P_n_0\ : STD_LOGIC;
  signal \result_reg[14]_C_n_0\ : STD_LOGIC;
  signal \result_reg[14]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[14]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[14]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[14]_P_n_0\ : STD_LOGIC;
  signal \result_reg[1]_C_n_0\ : STD_LOGIC;
  signal \result_reg[1]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[1]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[1]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[1]_P_n_0\ : STD_LOGIC;
  signal \result_reg[2]_C_n_0\ : STD_LOGIC;
  signal \result_reg[2]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[2]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[2]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[2]_P_n_0\ : STD_LOGIC;
  signal \result_reg[31]_C_n_0\ : STD_LOGIC;
  signal \result_reg[31]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[31]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[31]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[31]_P_n_0\ : STD_LOGIC;
  signal \result_reg[3]_C_n_0\ : STD_LOGIC;
  signal \result_reg[3]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[3]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[3]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[3]_P_n_0\ : STD_LOGIC;
  signal \result_reg[4]_C_n_0\ : STD_LOGIC;
  signal \result_reg[4]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[4]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[4]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[4]_P_n_0\ : STD_LOGIC;
  signal \result_reg[5]_C_n_0\ : STD_LOGIC;
  signal \result_reg[5]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[5]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[5]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[5]_P_n_0\ : STD_LOGIC;
  signal \result_reg[6]_C_n_0\ : STD_LOGIC;
  signal \result_reg[6]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[6]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[6]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[6]_P_n_0\ : STD_LOGIC;
  signal \result_reg[7]_C_n_0\ : STD_LOGIC;
  signal \result_reg[7]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[7]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[7]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[7]_P_n_0\ : STD_LOGIC;
  signal \result_reg[8]_C_n_0\ : STD_LOGIC;
  signal \result_reg[8]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[8]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[8]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[8]_P_n_0\ : STD_LOGIC;
  signal \result_reg[9]_C_n_0\ : STD_LOGIC;
  signal \result_reg[9]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[9]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[9]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[9]_P_n_0\ : STD_LOGIC;
  signal u32 : STD_LOGIC_VECTOR ( 27 downto 15 );
  signal NLW_acc321_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_acc321_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_acc321_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_acc321_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_acc32_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_result3_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_result3_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_result3_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_result3_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_result3_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \result3_carry__1_i_10\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \result3_carry__1_i_11\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \result3_carry__1_i_12\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \result3_carry__1_i_9\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \result3_carry__3_i_12\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \result3_carry__3_i_15\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \result3_carry__3_i_18\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \result3_carry__3_i_19\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \result3_carry__3_i_20\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \result3_carry__3_i_21\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \result3_carry__3_i_22\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \result3_carry__4_i_17\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of result3_carry_i_10 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of result3_carry_i_9 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \result[0]_C_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \result[10]_C_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \result[11]_C_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \result[12]_C_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \result[13]_C_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \result[14]_C_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \result[1]_C_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \result[2]_C_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \result[31]_C_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \result[3]_C_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \result[4]_C_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \result[5]_C_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \result[6]_C_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \result[7]_C_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \result[8]_C_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \result[9]_C_i_1\ : label is "soft_lutpair10";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \result_reg[0]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[10]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[11]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[12]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[13]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[14]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[1]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[2]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[31]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[3]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[4]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[5]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[6]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[7]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[8]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[9]_LDC\ : label is "LDC";
begin
  SR(0) <= \^sr\(0);
  dbg_acc(15 downto 0) <= \^dbg_acc\(15 downto 0);
acc321_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => acc321_carry_n_0,
      CO(2) => acc321_carry_n_1,
      CO(1) => acc321_carry_n_2,
      CO(0) => acc321_carry_n_3,
      CYINIT => '0',
      DI(3) => acc321_carry_i_1_n_0,
      DI(2) => acc321_carry_i_2_n_0,
      DI(1) => acc321_carry_i_3_n_0,
      DI(0) => acc321_carry_i_4_n_0,
      O(3 downto 0) => NLW_acc321_carry_O_UNCONNECTED(3 downto 0),
      S(3) => acc321_carry_i_5_n_0,
      S(2) => acc321_carry_i_6_n_0,
      S(1) => acc321_carry_i_7_n_0,
      S(0) => acc321_carry_i_8_n_0
    );
\acc321_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => acc321_carry_n_0,
      CO(3) => acc321,
      CO(2) => \acc321_carry__0_n_1\,
      CO(1) => \acc321_carry__0_n_2\,
      CO(0) => \acc321_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \acc321_carry__0_i_1_n_0\,
      DI(2) => \acc321_carry__0_i_2_n_0\,
      DI(1) => \acc321_carry__0_i_3_n_0\,
      DI(0) => \acc321_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_acc321_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \acc321_carry__0_i_5_n_0\,
      S(2) => \acc321_carry__0_i_6_n_0\,
      S(1) => \acc321_carry__0_i_7_n_0\,
      S(0) => \acc321_carry__0_i_8_n_0\
    );
\acc321_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => feedback(14),
      I1 => \acc321_carry__0_0\(14),
      I2 => feedback(15),
      I3 => \acc321_carry__0_0\(15),
      O => \acc321_carry__0_i_1_n_0\
    );
\acc321_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => feedback(12),
      I1 => \acc321_carry__0_0\(12),
      I2 => \acc321_carry__0_0\(13),
      I3 => feedback(13),
      O => \acc321_carry__0_i_2_n_0\
    );
\acc321_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => feedback(10),
      I1 => \acc321_carry__0_0\(10),
      I2 => \acc321_carry__0_0\(11),
      I3 => feedback(11),
      O => \acc321_carry__0_i_3_n_0\
    );
\acc321_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => feedback(8),
      I1 => \acc321_carry__0_0\(8),
      I2 => \acc321_carry__0_0\(9),
      I3 => feedback(9),
      O => \acc321_carry__0_i_4_n_0\
    );
\acc321_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => feedback(14),
      I1 => \acc321_carry__0_0\(14),
      I2 => \acc321_carry__0_0\(15),
      I3 => feedback(15),
      O => \acc321_carry__0_i_5_n_0\
    );
\acc321_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => feedback(12),
      I1 => \acc321_carry__0_0\(12),
      I2 => feedback(13),
      I3 => \acc321_carry__0_0\(13),
      O => \acc321_carry__0_i_6_n_0\
    );
\acc321_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => feedback(10),
      I1 => \acc321_carry__0_0\(10),
      I2 => feedback(11),
      I3 => \acc321_carry__0_0\(11),
      O => \acc321_carry__0_i_7_n_0\
    );
\acc321_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => feedback(8),
      I1 => \acc321_carry__0_0\(8),
      I2 => feedback(9),
      I3 => \acc321_carry__0_0\(9),
      O => \acc321_carry__0_i_8_n_0\
    );
acc321_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => feedback(6),
      I1 => \acc321_carry__0_0\(6),
      I2 => \acc321_carry__0_0\(7),
      I3 => feedback(7),
      O => acc321_carry_i_1_n_0
    );
acc321_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => feedback(4),
      I1 => \acc321_carry__0_0\(4),
      I2 => \acc321_carry__0_0\(5),
      I3 => feedback(5),
      O => acc321_carry_i_2_n_0
    );
acc321_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => feedback(2),
      I1 => \acc321_carry__0_0\(2),
      I2 => \acc321_carry__0_0\(3),
      I3 => feedback(3),
      O => acc321_carry_i_3_n_0
    );
acc321_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => feedback(0),
      I1 => \acc321_carry__0_0\(0),
      I2 => \acc321_carry__0_0\(1),
      I3 => feedback(1),
      O => acc321_carry_i_4_n_0
    );
acc321_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => feedback(6),
      I1 => \acc321_carry__0_0\(6),
      I2 => feedback(7),
      I3 => \acc321_carry__0_0\(7),
      O => acc321_carry_i_5_n_0
    );
acc321_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => feedback(4),
      I1 => \acc321_carry__0_0\(4),
      I2 => feedback(5),
      I3 => \acc321_carry__0_0\(5),
      O => acc321_carry_i_6_n_0
    );
acc321_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => feedback(2),
      I1 => \acc321_carry__0_0\(2),
      I2 => feedback(3),
      I3 => \acc321_carry__0_0\(3),
      O => acc321_carry_i_7_n_0
    );
acc321_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => feedback(0),
      I1 => \acc321_carry__0_0\(0),
      I2 => feedback(1),
      I3 => \acc321_carry__0_0\(1),
      O => acc321_carry_i_8_n_0
    );
\acc321_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \acc321_inferred__0/i__carry_n_0\,
      CO(2) => \acc321_inferred__0/i__carry_n_1\,
      CO(1) => \acc321_inferred__0/i__carry_n_2\,
      CO(0) => \acc321_inferred__0/i__carry_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry_i_1_n_0\,
      DI(2) => \i__carry_i_2_n_0\,
      DI(1) => \i__carry_i_3_n_0\,
      DI(0) => \i__carry_i_4_n_0\,
      O(3 downto 0) => \NLW_acc321_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_5_n_0\,
      S(2) => \i__carry_i_6_n_0\,
      S(1) => \i__carry_i_7_n_0\,
      S(0) => \i__carry_i_8_n_0\
    );
\acc321_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc321_inferred__0/i__carry_n_0\,
      CO(3) => acc3210_in,
      CO(2) => \acc321_inferred__0/i__carry__0_n_1\,
      CO(1) => \acc321_inferred__0/i__carry__0_n_2\,
      CO(0) => \acc321_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry__0_i_1_n_0\,
      DI(2) => \i__carry__0_i_2_n_0\,
      DI(1) => \i__carry__0_i_3_n_0\,
      DI(0) => \i__carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_acc321_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__0_i_5_n_0\,
      S(2) => \i__carry__0_i_6_n_0\,
      S(1) => \i__carry__0_i_7_n_0\,
      S(0) => \i__carry__0_i_8_n_0\
    );
\acc32[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(11),
      I1 => \^dbg_acc\(11),
      O => \acc32[11]_i_2_n_0\
    );
\acc32[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(10),
      I1 => \^dbg_acc\(10),
      O => \acc32[11]_i_3_n_0\
    );
\acc32[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(9),
      I1 => \^dbg_acc\(9),
      O => \acc32[11]_i_4_n_0\
    );
\acc32[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(8),
      I1 => \^dbg_acc\(8),
      O => \acc32[11]_i_5_n_0\
    );
\acc32[14]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(15),
      O => \acc32[14]_i_2_n_0\
    );
\acc32[14]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(14),
      I1 => \^dbg_acc\(14),
      O => \acc32[14]_i_3_n_0\
    );
\acc32[14]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(13),
      I1 => \^dbg_acc\(13),
      O => \acc32[14]_i_4_n_0\
    );
\acc32[14]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(12),
      I1 => \^dbg_acc\(12),
      O => \acc32[14]_i_5_n_0\
    );
\acc32[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(19),
      O => \acc32[16]_i_2_n_0\
    );
\acc32[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(18),
      O => \acc32[16]_i_3_n_0\
    );
\acc32[16]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(17),
      O => \acc32[16]_i_4_n_0\
    );
\acc32[16]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(16),
      O => \acc32[16]_i_5_n_0\
    );
\acc32[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(23),
      O => \acc32[20]_i_2_n_0\
    );
\acc32[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(22),
      O => \acc32[20]_i_3_n_0\
    );
\acc32[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(21),
      O => \acc32[20]_i_4_n_0\
    );
\acc32[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(20),
      O => \acc32[20]_i_5_n_0\
    );
\acc32[24]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(27),
      O => \acc32[24]_i_2_n_0\
    );
\acc32[24]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(26),
      O => \acc32[24]_i_3_n_0\
    );
\acc32[24]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(25),
      O => \acc32[24]_i_4_n_0\
    );
\acc32[24]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(24),
      O => \acc32[24]_i_5_n_0\
    );
\acc32[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => resetn,
      I1 => Q(5),
      O => \acc32[31]_i_1_n_0\
    );
\acc32[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => acc3210_in,
      I1 => acc321,
      O => en
    );
\acc32[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => \^dbg_acc\(15),
      O => \acc32[31]_i_4_n_0\
    );
\acc32[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(30),
      O => \acc32[31]_i_5_n_0\
    );
\acc32[31]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(29),
      O => \acc32[31]_i_6_n_0\
    );
\acc32[31]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(15),
      I1 => acc32_reg(28),
      O => \acc32[31]_i_7_n_0\
    );
\acc32[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(3),
      I1 => \^dbg_acc\(3),
      O => \acc32[3]_i_2_n_0\
    );
\acc32[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(2),
      I1 => \^dbg_acc\(2),
      O => \acc32[3]_i_3_n_0\
    );
\acc32[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(1),
      I1 => \^dbg_acc\(1),
      O => \acc32[3]_i_4_n_0\
    );
\acc32[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(0),
      I1 => \^dbg_acc\(0),
      O => \acc32[3]_i_5_n_0\
    );
\acc32[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(7),
      I1 => \^dbg_acc\(7),
      O => \acc32[7]_i_2_n_0\
    );
\acc32[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(6),
      I1 => \^dbg_acc\(6),
      O => \acc32[7]_i_3_n_0\
    );
\acc32[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(5),
      I1 => \^dbg_acc\(5),
      O => \acc32[7]_i_4_n_0\
    );
\acc32[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => feedback(4),
      I1 => \^dbg_acc\(4),
      O => \acc32[7]_i_5_n_0\
    );
\acc32_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[3]_i_1_n_7\,
      Q => \^dbg_acc\(0),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[11]_i_1_n_5\,
      Q => \^dbg_acc\(10),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[11]_i_1_n_4\,
      Q => \^dbg_acc\(11),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[7]_i_1_n_0\,
      CO(3) => \acc32_reg[11]_i_1_n_0\,
      CO(2) => \acc32_reg[11]_i_1_n_1\,
      CO(1) => \acc32_reg[11]_i_1_n_2\,
      CO(0) => \acc32_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => feedback(11 downto 8),
      O(3) => \acc32_reg[11]_i_1_n_4\,
      O(2) => \acc32_reg[11]_i_1_n_5\,
      O(1) => \acc32_reg[11]_i_1_n_6\,
      O(0) => \acc32_reg[11]_i_1_n_7\,
      S(3) => \acc32[11]_i_2_n_0\,
      S(2) => \acc32[11]_i_3_n_0\,
      S(1) => \acc32[11]_i_4_n_0\,
      S(0) => \acc32[11]_i_5_n_0\
    );
\acc32_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[14]_i_1_n_7\,
      Q => \^dbg_acc\(12),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[14]_i_1_n_6\,
      Q => \^dbg_acc\(13),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[14]_i_1_n_5\,
      Q => \^dbg_acc\(14),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[14]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[11]_i_1_n_0\,
      CO(3) => \acc32_reg[14]_i_1_n_0\,
      CO(2) => \acc32_reg[14]_i_1_n_1\,
      CO(1) => \acc32_reg[14]_i_1_n_2\,
      CO(0) => \acc32_reg[14]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => feedback(15 downto 12),
      O(3) => \acc32_reg[14]_i_1_n_4\,
      O(2) => \acc32_reg[14]_i_1_n_5\,
      O(1) => \acc32_reg[14]_i_1_n_6\,
      O(0) => \acc32_reg[14]_i_1_n_7\,
      S(3) => \acc32[14]_i_2_n_0\,
      S(2) => \acc32[14]_i_3_n_0\,
      S(1) => \acc32[14]_i_4_n_0\,
      S(0) => \acc32[14]_i_5_n_0\
    );
\acc32_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[14]_i_1_n_4\,
      Q => acc32_reg(15),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[16]_i_1_n_7\,
      Q => acc32_reg(16),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[14]_i_1_n_0\,
      CO(3) => \acc32_reg[16]_i_1_n_0\,
      CO(2) => \acc32_reg[16]_i_1_n_1\,
      CO(1) => \acc32_reg[16]_i_1_n_2\,
      CO(0) => \acc32_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => feedback(15),
      DI(2) => feedback(15),
      DI(1) => feedback(15),
      DI(0) => feedback(15),
      O(3) => \acc32_reg[16]_i_1_n_4\,
      O(2) => \acc32_reg[16]_i_1_n_5\,
      O(1) => \acc32_reg[16]_i_1_n_6\,
      O(0) => \acc32_reg[16]_i_1_n_7\,
      S(3) => \acc32[16]_i_2_n_0\,
      S(2) => \acc32[16]_i_3_n_0\,
      S(1) => \acc32[16]_i_4_n_0\,
      S(0) => \acc32[16]_i_5_n_0\
    );
\acc32_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[16]_i_1_n_6\,
      Q => acc32_reg(17),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[16]_i_1_n_5\,
      Q => acc32_reg(18),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[16]_i_1_n_4\,
      Q => acc32_reg(19),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[3]_i_1_n_6\,
      Q => \^dbg_acc\(1),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[20]_i_1_n_7\,
      Q => acc32_reg(20),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[16]_i_1_n_0\,
      CO(3) => \acc32_reg[20]_i_1_n_0\,
      CO(2) => \acc32_reg[20]_i_1_n_1\,
      CO(1) => \acc32_reg[20]_i_1_n_2\,
      CO(0) => \acc32_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => feedback(15),
      DI(2) => feedback(15),
      DI(1) => feedback(15),
      DI(0) => feedback(15),
      O(3) => \acc32_reg[20]_i_1_n_4\,
      O(2) => \acc32_reg[20]_i_1_n_5\,
      O(1) => \acc32_reg[20]_i_1_n_6\,
      O(0) => \acc32_reg[20]_i_1_n_7\,
      S(3) => \acc32[20]_i_2_n_0\,
      S(2) => \acc32[20]_i_3_n_0\,
      S(1) => \acc32[20]_i_4_n_0\,
      S(0) => \acc32[20]_i_5_n_0\
    );
\acc32_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[20]_i_1_n_6\,
      Q => acc32_reg(21),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[20]_i_1_n_5\,
      Q => acc32_reg(22),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[20]_i_1_n_4\,
      Q => acc32_reg(23),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[24]_i_1_n_7\,
      Q => acc32_reg(24),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[20]_i_1_n_0\,
      CO(3) => \acc32_reg[24]_i_1_n_0\,
      CO(2) => \acc32_reg[24]_i_1_n_1\,
      CO(1) => \acc32_reg[24]_i_1_n_2\,
      CO(0) => \acc32_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => feedback(15),
      DI(2) => feedback(15),
      DI(1) => feedback(15),
      DI(0) => feedback(15),
      O(3) => \acc32_reg[24]_i_1_n_4\,
      O(2) => \acc32_reg[24]_i_1_n_5\,
      O(1) => \acc32_reg[24]_i_1_n_6\,
      O(0) => \acc32_reg[24]_i_1_n_7\,
      S(3) => \acc32[24]_i_2_n_0\,
      S(2) => \acc32[24]_i_3_n_0\,
      S(1) => \acc32[24]_i_4_n_0\,
      S(0) => \acc32[24]_i_5_n_0\
    );
\acc32_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[24]_i_1_n_6\,
      Q => acc32_reg(25),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[24]_i_1_n_5\,
      Q => acc32_reg(26),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[24]_i_1_n_4\,
      Q => acc32_reg(27),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[31]_i_3_n_7\,
      Q => acc32_reg(28),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[31]_i_3_n_6\,
      Q => acc32_reg(29),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[3]_i_1_n_5\,
      Q => \^dbg_acc\(2),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[31]_i_3_n_5\,
      Q => acc32_reg(30),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[31]_i_3_n_4\,
      Q => \^dbg_acc\(15),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[24]_i_1_n_0\,
      CO(3) => \NLW_acc32_reg[31]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \acc32_reg[31]_i_3_n_1\,
      CO(1) => \acc32_reg[31]_i_3_n_2\,
      CO(0) => \acc32_reg[31]_i_3_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => feedback(15),
      DI(1) => feedback(15),
      DI(0) => feedback(15),
      O(3) => \acc32_reg[31]_i_3_n_4\,
      O(2) => \acc32_reg[31]_i_3_n_5\,
      O(1) => \acc32_reg[31]_i_3_n_6\,
      O(0) => \acc32_reg[31]_i_3_n_7\,
      S(3) => \acc32[31]_i_4_n_0\,
      S(2) => \acc32[31]_i_5_n_0\,
      S(1) => \acc32[31]_i_6_n_0\,
      S(0) => \acc32[31]_i_7_n_0\
    );
\acc32_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[3]_i_1_n_4\,
      Q => \^dbg_acc\(3),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \acc32_reg[3]_i_1_n_0\,
      CO(2) => \acc32_reg[3]_i_1_n_1\,
      CO(1) => \acc32_reg[3]_i_1_n_2\,
      CO(0) => \acc32_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => feedback(3 downto 0),
      O(3) => \acc32_reg[3]_i_1_n_4\,
      O(2) => \acc32_reg[3]_i_1_n_5\,
      O(1) => \acc32_reg[3]_i_1_n_6\,
      O(0) => \acc32_reg[3]_i_1_n_7\,
      S(3) => \acc32[3]_i_2_n_0\,
      S(2) => \acc32[3]_i_3_n_0\,
      S(1) => \acc32[3]_i_4_n_0\,
      S(0) => \acc32[3]_i_5_n_0\
    );
\acc32_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[7]_i_1_n_7\,
      Q => \^dbg_acc\(4),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[7]_i_1_n_6\,
      Q => \^dbg_acc\(5),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[7]_i_1_n_5\,
      Q => \^dbg_acc\(6),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[7]_i_1_n_4\,
      Q => \^dbg_acc\(7),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[3]_i_1_n_0\,
      CO(3) => \acc32_reg[7]_i_1_n_0\,
      CO(2) => \acc32_reg[7]_i_1_n_1\,
      CO(1) => \acc32_reg[7]_i_1_n_2\,
      CO(0) => \acc32_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => feedback(7 downto 4),
      O(3) => \acc32_reg[7]_i_1_n_4\,
      O(2) => \acc32_reg[7]_i_1_n_5\,
      O(1) => \acc32_reg[7]_i_1_n_6\,
      O(0) => \acc32_reg[7]_i_1_n_7\,
      S(3) => \acc32[7]_i_2_n_0\,
      S(2) => \acc32[7]_i_3_n_0\,
      S(1) => \acc32[7]_i_4_n_0\,
      S(0) => \acc32[7]_i_5_n_0\
    );
\acc32_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[11]_i_1_n_7\,
      Q => \^dbg_acc\(8),
      R => \acc32[31]_i_1_n_0\
    );
\acc32_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \acc32_reg[11]_i_1_n_6\,
      Q => \^dbg_acc\(9),
      R => \acc32[31]_i_1_n_0\
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(14),
      I1 => feedback(14),
      I2 => \acc321_inferred__0/i__carry__0_0\(15),
      I3 => feedback(15),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(12),
      I1 => feedback(12),
      I2 => feedback(13),
      I3 => \acc321_inferred__0/i__carry__0_0\(13),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(10),
      I1 => feedback(10),
      I2 => feedback(11),
      I3 => \acc321_inferred__0/i__carry__0_0\(11),
      O => \i__carry__0_i_3_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(8),
      I1 => feedback(8),
      I2 => feedback(9),
      I3 => \acc321_inferred__0/i__carry__0_0\(9),
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(14),
      I1 => feedback(14),
      I2 => feedback(15),
      I3 => \acc321_inferred__0/i__carry__0_0\(15),
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(12),
      I1 => feedback(12),
      I2 => \acc321_inferred__0/i__carry__0_0\(13),
      I3 => feedback(13),
      O => \i__carry__0_i_6_n_0\
    );
\i__carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(10),
      I1 => feedback(10),
      I2 => \acc321_inferred__0/i__carry__0_0\(11),
      I3 => feedback(11),
      O => \i__carry__0_i_7_n_0\
    );
\i__carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(8),
      I1 => feedback(8),
      I2 => \acc321_inferred__0/i__carry__0_0\(9),
      I3 => feedback(9),
      O => \i__carry__0_i_8_n_0\
    );
\i__carry_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(6),
      I1 => feedback(6),
      I2 => feedback(7),
      I3 => \acc321_inferred__0/i__carry__0_0\(7),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(4),
      I1 => feedback(4),
      I2 => feedback(5),
      I3 => \acc321_inferred__0/i__carry__0_0\(5),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(2),
      I1 => feedback(2),
      I2 => feedback(3),
      I3 => \acc321_inferred__0/i__carry__0_0\(3),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(0),
      I1 => feedback(0),
      I2 => feedback(1),
      I3 => \acc321_inferred__0/i__carry__0_0\(1),
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(6),
      I1 => feedback(6),
      I2 => \acc321_inferred__0/i__carry__0_0\(7),
      I3 => feedback(7),
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(4),
      I1 => feedback(4),
      I2 => \acc321_inferred__0/i__carry__0_0\(5),
      I3 => feedback(5),
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(2),
      I1 => feedback(2),
      I2 => \acc321_inferred__0/i__carry__0_0\(3),
      I3 => feedback(3),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \acc321_inferred__0/i__carry__0_0\(0),
      I1 => feedback(0),
      I2 => \acc321_inferred__0/i__carry__0_0\(1),
      I3 => feedback(1),
      O => \i__carry_i_8_n_0\
    );
result3_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result3_carry_n_0,
      CO(2) => result3_carry_n_1,
      CO(1) => result3_carry_n_2,
      CO(0) => result3_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => x(3 downto 0),
      O(3) => result3_carry_n_4,
      O(2) => result3_carry_n_5,
      O(1) => result3_carry_n_6,
      O(0) => result3_carry_n_7,
      S(3) => result3_carry_i_1_n_0,
      S(2) => result3_carry_i_2_n_0,
      S(1) => result3_carry_i_3_n_0,
      S(0) => result3_carry_i_4_n_0
    );
\result3_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => result3_carry_n_0,
      CO(3) => \result3_carry__0_n_0\,
      CO(2) => \result3_carry__0_n_1\,
      CO(1) => \result3_carry__0_n_2\,
      CO(0) => \result3_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => x(7 downto 4),
      O(3) => \result3_carry__0_n_4\,
      O(2) => \result3_carry__0_n_5\,
      O(1) => \result3_carry__0_n_6\,
      O(0) => \result3_carry__0_n_7\,
      S(3) => \result3_carry__0_i_1_n_0\,
      S(2) => \result3_carry__0_i_2_n_0\,
      S(1) => \result3_carry__0_i_3_n_0\,
      S(0) => \result3_carry__0_i_4_n_0\
    );
\result3_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(7),
      I1 => \result3_carry__0_i_5_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__0_i_6_n_0\,
      O => \result3_carry__0_i_1_n_0\
    );
\result3_carry__0_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dbg_acc\(15),
      I1 => acc32_reg(19),
      I2 => Q(3),
      I3 => acc32_reg(27),
      I4 => Q(4),
      I5 => \^dbg_acc\(11),
      O => \result3_carry__0_i_10_n_0\
    );
\result3_carry__0_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dbg_acc\(15),
      I1 => acc32_reg(22),
      I2 => Q(3),
      I3 => acc32_reg(30),
      I4 => Q(4),
      I5 => \^dbg_acc\(14),
      O => \result3_carry__0_i_11_n_0\
    );
\result3_carry__0_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dbg_acc\(15),
      I1 => acc32_reg(20),
      I2 => Q(3),
      I3 => acc32_reg(28),
      I4 => Q(4),
      I5 => \^dbg_acc\(12),
      O => \result3_carry__0_i_12_n_0\
    );
\result3_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(6),
      I1 => \result3_carry__0_i_7_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__0_i_5_n_0\,
      O => \result3_carry__0_i_2_n_0\
    );
\result3_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(5),
      I1 => \result3_carry__0_i_8_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__0_i_7_n_0\,
      O => \result3_carry__0_i_3_n_0\
    );
\result3_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(4),
      I1 => result3_carry_i_6_n_0,
      I2 => Q(0),
      I3 => \result3_carry__0_i_8_n_0\,
      O => \result3_carry__0_i_4_n_0\
    );
\result3_carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__0_i_9_n_0\,
      I1 => result3_carry_i_11_n_0,
      I2 => Q(1),
      I3 => \result3_carry__0_i_10_n_0\,
      I4 => Q(2),
      I5 => result3_carry_i_13_n_0,
      O => \result3_carry__0_i_5_n_0\
    );
\result3_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__0_i_11_n_0\,
      I1 => result3_carry_i_15_n_0,
      I2 => Q(1),
      I3 => \result3_carry__0_i_12_n_0\,
      I4 => Q(2),
      I5 => result3_carry_i_17_n_0,
      O => \result3_carry__0_i_6_n_0\
    );
\result3_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__0_i_12_n_0\,
      I1 => result3_carry_i_17_n_0,
      I2 => Q(1),
      I3 => result3_carry_i_15_n_0,
      I4 => Q(2),
      I5 => result3_carry_i_16_n_0,
      O => \result3_carry__0_i_7_n_0\
    );
\result3_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__0_i_10_n_0\,
      I1 => result3_carry_i_13_n_0,
      I2 => Q(1),
      I3 => result3_carry_i_11_n_0,
      I4 => Q(2),
      I5 => result3_carry_i_12_n_0,
      O => \result3_carry__0_i_8_n_0\
    );
\result3_carry__0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dbg_acc\(15),
      I1 => acc32_reg(21),
      I2 => Q(3),
      I3 => acc32_reg(29),
      I4 => Q(4),
      I5 => \^dbg_acc\(13),
      O => \result3_carry__0_i_9_n_0\
    );
\result3_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__0_n_0\,
      CO(3) => \result3_carry__1_n_0\,
      CO(2) => \result3_carry__1_n_1\,
      CO(1) => \result3_carry__1_n_2\,
      CO(0) => \result3_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => x(11 downto 8),
      O(3) => \result3_carry__1_n_4\,
      O(2) => \result3_carry__1_n_5\,
      O(1) => \result3_carry__1_n_6\,
      O(0) => \result3_carry__1_n_7\,
      S(3) => \result3_carry__1_i_1_n_0\,
      S(2) => \result3_carry__1_i_2_n_0\,
      S(1) => \result3_carry__1_i_3_n_0\,
      S(0) => \result3_carry__1_i_4_n_0\
    );
\result3_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(11),
      I1 => \result3_carry__1_i_5_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__1_i_6_n_0\,
      O => \result3_carry__1_i_1_n_0\
    );
\result3_carry__1_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => acc32_reg(23),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(15),
      O => \result3_carry__1_i_10_n_0\
    );
\result3_carry__1_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => acc32_reg(26),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(18),
      O => \result3_carry__1_i_11_n_0\
    );
\result3_carry__1_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => acc32_reg(24),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(16),
      O => \result3_carry__1_i_12_n_0\
    );
\result3_carry__1_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(10),
      I1 => \result3_carry__1_i_7_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__1_i_5_n_0\,
      O => \result3_carry__1_i_2_n_0\
    );
\result3_carry__1_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(9),
      I1 => \result3_carry__1_i_8_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__1_i_7_n_0\,
      O => \result3_carry__1_i_3_n_0\
    );
\result3_carry__1_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(8),
      I1 => \result3_carry__0_i_6_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__1_i_8_n_0\,
      O => \result3_carry__1_i_4_n_0\
    );
\result3_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__1_i_9_n_0\,
      I1 => \result3_carry__0_i_9_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__1_i_10_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__0_i_10_n_0\,
      O => \result3_carry__1_i_5_n_0\
    );
\result3_carry__1_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__1_i_11_n_0\,
      I1 => \result3_carry__0_i_11_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__1_i_12_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__0_i_12_n_0\,
      O => \result3_carry__1_i_6_n_0\
    );
\result3_carry__1_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__1_i_12_n_0\,
      I1 => \result3_carry__0_i_12_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__0_i_11_n_0\,
      I4 => Q(2),
      I5 => result3_carry_i_15_n_0,
      O => \result3_carry__1_i_7_n_0\
    );
\result3_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__1_i_10_n_0\,
      I1 => \result3_carry__0_i_10_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__0_i_9_n_0\,
      I4 => Q(2),
      I5 => result3_carry_i_11_n_0,
      O => \result3_carry__1_i_8_n_0\
    );
\result3_carry__1_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => acc32_reg(25),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(17),
      O => \result3_carry__1_i_9_n_0\
    );
\result3_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__1_n_0\,
      CO(3) => \result3_carry__2_n_0\,
      CO(2) => \result3_carry__2_n_1\,
      CO(1) => \result3_carry__2_n_2\,
      CO(0) => \result3_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => u32(15),
      DI(2 downto 0) => x(14 downto 12),
      O(3) => p_1_in,
      O(2) => \result3_carry__2_n_5\,
      O(1) => \result3_carry__2_n_6\,
      O(0) => \result3_carry__2_n_7\,
      S(3) => \result3_carry__2_i_2_n_0\,
      S(2) => \result3_carry__2_i_3_n_0\,
      S(1) => \result3_carry__2_i_4_n_0\,
      S(0) => \result3_carry__2_i_5_n_0\
    );
\result3_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__2_i_6_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__2_i_7_n_0\,
      O => u32(15)
    );
\result3_carry__2_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => acc32_reg(30),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(22),
      O => \result3_carry__2_i_10_n_0\
    );
\result3_carry__2_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => acc32_reg(28),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(20),
      O => \result3_carry__2_i_11_n_0\
    );
\result3_carry__2_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => acc32_reg(29),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(21),
      O => \result3_carry__2_i_12_n_0\
    );
\result3_carry__2_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => acc32_reg(27),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(19),
      O => \result3_carry__2_i_13_n_0\
    );
\result3_carry__2_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E21D"
    )
        port map (
      I0 => \result3_carry__2_i_7_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__2_i_6_n_0\,
      I3 => x(15),
      O => \result3_carry__2_i_2_n_0\
    );
\result3_carry__2_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(14),
      I1 => \result3_carry__2_i_8_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__2_i_7_n_0\,
      O => \result3_carry__2_i_3_n_0\
    );
\result3_carry__2_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(13),
      I1 => \result3_carry__2_i_9_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__2_i_8_n_0\,
      O => \result3_carry__2_i_4_n_0\
    );
\result3_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(12),
      I1 => \result3_carry__1_i_6_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__2_i_9_n_0\,
      O => \result3_carry__2_i_5_n_0\
    );
\result3_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__2_i_10_n_0\,
      I1 => \result3_carry__1_i_11_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__2_i_11_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__1_i_12_n_0\,
      O => \result3_carry__2_i_6_n_0\
    );
\result3_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__2_i_12_n_0\,
      I1 => \result3_carry__1_i_9_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__2_i_13_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__1_i_10_n_0\,
      O => \result3_carry__2_i_7_n_0\
    );
\result3_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__2_i_11_n_0\,
      I1 => \result3_carry__1_i_12_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__1_i_11_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__0_i_11_n_0\,
      O => \result3_carry__2_i_8_n_0\
    );
\result3_carry__2_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__2_i_13_n_0\,
      I1 => \result3_carry__1_i_10_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__1_i_9_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__0_i_9_n_0\,
      O => \result3_carry__2_i_9_n_0\
    );
\result3_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__2_n_0\,
      CO(3) => \result3_carry__3_n_0\,
      CO(2) => \result3_carry__3_n_1\,
      CO(1) => \result3_carry__3_n_2\,
      CO(0) => \result3_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \result3_carry__3_i_1_n_0\,
      DI(2) => \result3_carry__3_i_2_n_0\,
      DI(1) => \result3_carry__3_i_3_n_0\,
      DI(0) => \result3_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_result3_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \result3_carry__3_i_5_n_0\,
      S(2) => \result3_carry__3_i_6_n_0\,
      S(1) => \result3_carry__3_i_7_n_0\,
      S(0) => \result3_carry__3_i_8_n_0\
    );
\result3_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \result3_carry__3_i_9_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__3_i_10_n_0\,
      O => \result3_carry__3_i_1_n_0\
    );
\result3_carry__3_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__3_i_21_n_0\,
      I1 => \result3_carry__2_i_12_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__3_i_22_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__2_i_13_n_0\,
      O => \result3_carry__3_i_10_n_0\
    );
\result3_carry__3_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__3_i_22_n_0\,
      I1 => \result3_carry__2_i_13_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__2_i_12_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__1_i_9_n_0\,
      O => \result3_carry__3_i_11_n_0\
    );
\result3_carry__3_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__2_i_10_n_0\,
      I1 => Q(2),
      I2 => \result3_carry__1_i_11_n_0\,
      O => \result3_carry__3_i_12_n_0\
    );
\result3_carry__3_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8FFFFCDC80000"
    )
        port map (
      I0 => Q(3),
      I1 => \^dbg_acc\(15),
      I2 => Q(4),
      I3 => acc32_reg(24),
      I4 => Q(2),
      I5 => \result3_carry__2_i_11_n_0\,
      O => \result3_carry__3_i_13_n_0\
    );
\result3_carry__3_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8FFFFCDC80000"
    )
        port map (
      I0 => Q(3),
      I1 => \^dbg_acc\(15),
      I2 => Q(4),
      I3 => acc32_reg(26),
      I4 => Q(2),
      I5 => \result3_carry__2_i_10_n_0\,
      O => \result3_carry__3_i_14_n_0\
    );
\result3_carry__3_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__2_i_12_n_0\,
      I1 => Q(2),
      I2 => \result3_carry__1_i_9_n_0\,
      O => \result3_carry__3_i_15_n_0\
    );
\result3_carry__3_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8FFFFCDC80000"
    )
        port map (
      I0 => Q(3),
      I1 => \^dbg_acc\(15),
      I2 => Q(4),
      I3 => acc32_reg(23),
      I4 => Q(2),
      I5 => \result3_carry__2_i_13_n_0\,
      O => \result3_carry__3_i_16_n_0\
    );
\result3_carry__3_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8FFFFCDC80000"
    )
        port map (
      I0 => Q(3),
      I1 => \^dbg_acc\(15),
      I2 => Q(4),
      I3 => acc32_reg(25),
      I4 => Q(2),
      I5 => \result3_carry__2_i_12_n_0\,
      O => \result3_carry__3_i_17_n_0\
    );
\result3_carry__3_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__2_i_11_n_0\,
      I1 => Q(2),
      I2 => \result3_carry__1_i_12_n_0\,
      O => \result3_carry__3_i_18_n_0\
    );
\result3_carry__3_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__2_i_13_n_0\,
      I1 => Q(2),
      I2 => \result3_carry__1_i_10_n_0\,
      O => \result3_carry__3_i_19_n_0\
    );
\result3_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \result3_carry__3_i_11_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__3_i_9_n_0\,
      O => \result3_carry__3_i_2_n_0\
    );
\result3_carry__3_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => Q(3),
      I1 => \^dbg_acc\(15),
      I2 => Q(4),
      I3 => acc32_reg(24),
      O => \result3_carry__3_i_20_n_0\
    );
\result3_carry__3_i_21\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => Q(3),
      I1 => \^dbg_acc\(15),
      I2 => Q(4),
      I3 => acc32_reg(25),
      O => \result3_carry__3_i_21_n_0\
    );
\result3_carry__3_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => Q(3),
      I1 => \^dbg_acc\(15),
      I2 => Q(4),
      I3 => acc32_reg(23),
      O => \result3_carry__3_i_22_n_0\
    );
\result3_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \result3_carry__2_i_6_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__3_i_11_n_0\,
      O => \result3_carry__3_i_3_n_0\
    );
\result3_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \result3_carry__2_i_7_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__2_i_6_n_0\,
      O => \result3_carry__3_i_4_n_0\
    );
\result3_carry__3_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC3C9393C33C939"
    )
        port map (
      I0 => \result3_carry__3_i_12_n_0\,
      I1 => \result3_carry__3_i_10_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_13_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__3_i_14_n_0\,
      O => \result3_carry__3_i_5_n_0\
    );
\result3_carry__3_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC3C9393C33C939"
    )
        port map (
      I0 => \result3_carry__3_i_15_n_0\,
      I1 => \result3_carry__3_i_9_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_16_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__3_i_17_n_0\,
      O => \result3_carry__3_i_6_n_0\
    );
\result3_carry__3_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC3C9393C33C939"
    )
        port map (
      I0 => \result3_carry__3_i_18_n_0\,
      I1 => \result3_carry__3_i_11_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_12_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__3_i_13_n_0\,
      O => \result3_carry__3_i_7_n_0\
    );
\result3_carry__3_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC3C9393C33C939"
    )
        port map (
      I0 => \result3_carry__3_i_19_n_0\,
      I1 => \result3_carry__2_i_6_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_15_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__3_i_16_n_0\,
      O => \result3_carry__3_i_8_n_0\
    );
\result3_carry__3_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__3_i_20_n_0\,
      I1 => \result3_carry__2_i_11_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__2_i_10_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__1_i_11_n_0\,
      O => \result3_carry__3_i_9_n_0\
    );
\result3_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__3_n_0\,
      CO(3) => \result3_carry__4_n_0\,
      CO(2) => \result3_carry__4_n_1\,
      CO(1) => \result3_carry__4_n_2\,
      CO(0) => \result3_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \result3_carry__4_i_1_n_0\,
      DI(2) => \result3_carry__4_i_2_n_0\,
      DI(1) => \result3_carry__4_i_3_n_0\,
      DI(0) => \result3_carry__4_i_4_n_0\,
      O(3 downto 0) => \NLW_result3_carry__4_O_UNCONNECTED\(3 downto 0),
      S(3) => \result3_carry__4_i_5_n_0\,
      S(2) => \result3_carry__4_i_6_n_0\,
      S(1) => \result3_carry__4_i_7_n_0\,
      S(0) => \result3_carry__4_i_8_n_0\
    );
\result3_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \result3_carry__4_i_9_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__4_i_10_n_0\,
      O => \result3_carry__4_i_1_n_0\
    );
\result3_carry__4_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__4_i_16_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__4_i_15_n_0\,
      O => \result3_carry__4_i_10_n_0\
    );
\result3_carry__4_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \result3_carry__4_i_15_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__3_i_21_n_0\,
      I3 => Q(2),
      I4 => \result3_carry__2_i_12_n_0\,
      O => \result3_carry__4_i_11_n_0\
    );
\result3_carry__4_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__4_i_17_n_0\,
      I1 => \result3_carry__2_i_10_n_0\,
      I2 => Q(1),
      I3 => \result3_carry__3_i_20_n_0\,
      I4 => Q(2),
      I5 => \result3_carry__2_i_11_n_0\,
      O => \result3_carry__4_i_12_n_0\
    );
\result3_carry__4_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => acc32_reg(28),
      I1 => Q(2),
      I2 => Q(3),
      I3 => \^dbg_acc\(15),
      I4 => Q(4),
      I5 => acc32_reg(24),
      O => \result3_carry__4_i_13_n_0\
    );
\result3_carry__4_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => acc32_reg(30),
      I1 => Q(2),
      I2 => Q(3),
      I3 => \^dbg_acc\(15),
      I4 => Q(4),
      I5 => acc32_reg(26),
      O => \result3_carry__4_i_14_n_0\
    );
\result3_carry__4_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => acc32_reg(27),
      I1 => Q(2),
      I2 => Q(3),
      I3 => \^dbg_acc\(15),
      I4 => Q(4),
      I5 => acc32_reg(23),
      O => \result3_carry__4_i_15_n_0\
    );
\result3_carry__4_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => acc32_reg(29),
      I1 => Q(2),
      I2 => Q(3),
      I3 => \^dbg_acc\(15),
      I4 => Q(4),
      I5 => acc32_reg(25),
      O => \result3_carry__4_i_16_n_0\
    );
\result3_carry__4_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => Q(3),
      I1 => \^dbg_acc\(15),
      I2 => Q(4),
      I3 => acc32_reg(26),
      O => \result3_carry__4_i_17_n_0\
    );
\result3_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \result3_carry__4_i_11_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__4_i_9_n_0\,
      O => \result3_carry__4_i_2_n_0\
    );
\result3_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \result3_carry__4_i_12_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__4_i_11_n_0\,
      O => \result3_carry__4_i_3_n_0\
    );
\result3_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \result3_carry__3_i_10_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__4_i_12_n_0\,
      O => \result3_carry__4_i_4_n_0\
    );
\result3_carry__4_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC3C9393C33C939"
    )
        port map (
      I0 => \result3_carry__3_i_14_n_0\,
      I1 => \result3_carry__4_i_10_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__4_i_13_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_14_n_0\,
      O => \result3_carry__4_i_5_n_0\
    );
\result3_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC3C9393C33C939"
    )
        port map (
      I0 => \result3_carry__3_i_17_n_0\,
      I1 => \result3_carry__4_i_9_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__4_i_15_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_16_n_0\,
      O => \result3_carry__4_i_6_n_0\
    );
\result3_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC3C9393C33C939"
    )
        port map (
      I0 => \result3_carry__3_i_13_n_0\,
      I1 => \result3_carry__4_i_11_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_14_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_13_n_0\,
      O => \result3_carry__4_i_7_n_0\
    );
\result3_carry__4_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC3C9393C33C939"
    )
        port map (
      I0 => \result3_carry__3_i_16_n_0\,
      I1 => \result3_carry__4_i_12_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_17_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_15_n_0\,
      O => \result3_carry__4_i_8_n_0\
    );
\result3_carry__4_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \result3_carry__4_i_13_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__4_i_17_n_0\,
      I3 => Q(2),
      I4 => \result3_carry__2_i_10_n_0\,
      O => \result3_carry__4_i_9_n_0\
    );
\result3_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__4_n_0\,
      CO(3) => \result3_carry__5_n_0\,
      CO(2) => \result3_carry__5_n_1\,
      CO(1) => \result3_carry__5_n_2\,
      CO(0) => \result3_carry__5_n_3\,
      CYINIT => '0',
      DI(3) => \result3_carry__5_i_1_n_0\,
      DI(2) => \result3_carry__5_i_2_n_0\,
      DI(1) => \result3_carry__5_i_3_n_0\,
      DI(0) => \result3_carry__5_i_4_n_0\,
      O(3 downto 0) => \NLW_result3_carry__5_O_UNCONNECTED\(3 downto 0),
      S(3) => \result3_carry__5_i_5_n_0\,
      S(2) => \result3_carry__5_i_6_n_0\,
      S(1) => \result3_carry__5_i_7_n_0\,
      S(0) => \result3_carry__5_i_8_n_0\
    );
\result3_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => u32(26),
      O => \result3_carry__5_i_1_n_0\
    );
\result3_carry__5_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__6_i_11_n_0\,
      I1 => \result3_carry__4_i_14_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__5_i_15_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_16_n_0\,
      O => u32(25)
    );
\result3_carry__5_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__5_i_15_n_0\,
      I1 => \result3_carry__4_i_16_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__4_i_14_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_13_n_0\,
      O => u32(24)
    );
\result3_carry__5_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__4_i_14_n_0\,
      I1 => \result3_carry__4_i_13_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__4_i_16_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_15_n_0\,
      O => u32(23)
    );
\result3_carry__5_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__6_i_10_n_0\,
      I1 => \result3_carry__6_i_11_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__5_i_14_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__5_i_15_n_0\,
      O => u32(27)
    );
\result3_carry__5_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => Q(2),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(29),
      O => \result3_carry__5_i_14_n_0\
    );
\result3_carry__5_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => Q(2),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(27),
      O => \result3_carry__5_i_15_n_0\
    );
\result3_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => u32(25),
      O => \result3_carry__5_i_2_n_0\
    );
\result3_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => u32(24),
      O => \result3_carry__5_i_3_n_0\
    );
\result3_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => u32(23),
      O => \result3_carry__5_i_4_n_0\
    );
\result3_carry__5_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => u32(26),
      I1 => u32(27),
      O => \result3_carry__5_i_5_n_0\
    );
\result3_carry__5_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => u32(25),
      I1 => u32(26),
      O => \result3_carry__5_i_6_n_0\
    );
\result3_carry__5_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => u32(24),
      I1 => u32(25),
      O => \result3_carry__5_i_7_n_0\
    );
\result3_carry__5_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => u32(23),
      I1 => u32(24),
      O => \result3_carry__5_i_8_n_0\
    );
\result3_carry__5_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__5_i_14_n_0\,
      I1 => \result3_carry__5_i_15_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__6_i_11_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_14_n_0\,
      O => u32(26)
    );
\result3_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__5_n_0\,
      CO(3) => \NLW_result3_carry__6_CO_UNCONNECTED\(3),
      CO(2) => \result3_carry__6_n_1\,
      CO(1) => \result3_carry__6_n_2\,
      CO(0) => \result3_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \result3_carry__6_i_1_n_0\,
      DI(1) => \result3_carry__6_i_2_n_0\,
      DI(0) => \result3_carry__6_i_3_n_0\,
      O(3) => p_0_in,
      O(2 downto 0) => \NLW_result3_carry__6_O_UNCONNECTED\(2 downto 0),
      S(3) => \result3_carry__6_i_4_n_0\,
      S(2) => \result3_carry__6_i_5_n_0\,
      S(1) => \result3_carry__6_i_6_n_0\,
      S(0) => \result3_carry__6_i_7_n_0\
    );
\result3_carry__6_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \result3_carry__6_i_8_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__6_i_9_n_0\,
      O => \result3_carry__6_i_1_n_0\
    );
\result3_carry__6_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => Q(2),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(30),
      O => \result3_carry__6_i_10_n_0\
    );
\result3_carry__6_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => Q(2),
      I1 => Q(3),
      I2 => \^dbg_acc\(15),
      I3 => Q(4),
      I4 => acc32_reg(28),
      O => \result3_carry__6_i_11_n_0\
    );
\result3_carry__6_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"47444777"
    )
        port map (
      I0 => \result3_carry__6_i_9_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__6_i_10_n_0\,
      I3 => Q(1),
      I4 => \result3_carry__6_i_11_n_0\,
      O => \result3_carry__6_i_2_n_0\
    );
\result3_carry__6_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => u32(27),
      O => \result3_carry__6_i_3_n_0\
    );
\result3_carry__6_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \result3_carry__6_i_8_n_0\,
      I1 => Q(0),
      I2 => \^dbg_acc\(15),
      O => \result3_carry__6_i_4_n_0\
    );
\result3_carry__6_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C939"
    )
        port map (
      I0 => \result3_carry__6_i_9_n_0\,
      I1 => \result3_carry__6_i_8_n_0\,
      I2 => Q(0),
      I3 => \^dbg_acc\(15),
      O => \result3_carry__6_i_5_n_0\
    );
\result3_carry__6_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B487B4B4B4878787"
    )
        port map (
      I0 => \result3_carry__6_i_8_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__6_i_9_n_0\,
      I3 => \result3_carry__6_i_10_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__6_i_11_n_0\,
      O => \result3_carry__6_i_6_n_0\
    );
\result3_carry__6_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB88847444777"
    )
        port map (
      I0 => \result3_carry__6_i_9_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__6_i_10_n_0\,
      I3 => Q(1),
      I4 => \result3_carry__6_i_11_n_0\,
      I5 => u32(27),
      O => \result3_carry__6_i_7_n_0\
    );
\result3_carry__6_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => Q(1),
      I1 => Q(2),
      I2 => Q(3),
      I3 => \^dbg_acc\(15),
      I4 => Q(4),
      I5 => acc32_reg(30),
      O => \result3_carry__6_i_8_n_0\
    );
\result3_carry__6_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => Q(1),
      I1 => Q(2),
      I2 => Q(3),
      I3 => \^dbg_acc\(15),
      I4 => Q(4),
      I5 => acc32_reg(29),
      O => \result3_carry__6_i_9_n_0\
    );
result3_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(3),
      I1 => result3_carry_i_5_n_0,
      I2 => Q(0),
      I3 => result3_carry_i_6_n_0,
      O => result3_carry_i_1_n_0
    );
result3_carry_i_10: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => result3_carry_i_16_n_0,
      I1 => Q(2),
      I2 => result3_carry_i_19_n_0,
      O => result3_carry_i_10_n_0
    );
result3_carry_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dbg_acc\(15),
      I1 => acc32_reg(17),
      I2 => Q(3),
      I3 => acc32_reg(25),
      I4 => Q(4),
      I5 => \^dbg_acc\(9),
      O => result3_carry_i_11_n_0
    );
result3_carry_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => acc32_reg(29),
      I1 => \^dbg_acc\(13),
      I2 => Q(3),
      I3 => acc32_reg(21),
      I4 => Q(4),
      I5 => \^dbg_acc\(5),
      O => result3_carry_i_12_n_0
    );
result3_carry_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dbg_acc\(15),
      I1 => acc32_reg(15),
      I2 => Q(3),
      I3 => acc32_reg(23),
      I4 => Q(4),
      I5 => \^dbg_acc\(7),
      O => result3_carry_i_13_n_0
    );
result3_carry_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => acc32_reg(27),
      I1 => \^dbg_acc\(11),
      I2 => Q(3),
      I3 => acc32_reg(19),
      I4 => Q(4),
      I5 => \^dbg_acc\(3),
      O => result3_carry_i_14_n_0
    );
result3_carry_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dbg_acc\(15),
      I1 => acc32_reg(18),
      I2 => Q(3),
      I3 => acc32_reg(26),
      I4 => Q(4),
      I5 => \^dbg_acc\(10),
      O => result3_carry_i_15_n_0
    );
result3_carry_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => acc32_reg(30),
      I1 => \^dbg_acc\(14),
      I2 => Q(3),
      I3 => acc32_reg(22),
      I4 => Q(4),
      I5 => \^dbg_acc\(6),
      O => result3_carry_i_16_n_0
    );
result3_carry_i_17: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dbg_acc\(15),
      I1 => acc32_reg(16),
      I2 => Q(3),
      I3 => acc32_reg(24),
      I4 => Q(4),
      I5 => \^dbg_acc\(8),
      O => result3_carry_i_17_n_0
    );
result3_carry_i_18: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => acc32_reg(28),
      I1 => \^dbg_acc\(12),
      I2 => Q(3),
      I3 => acc32_reg(20),
      I4 => Q(4),
      I5 => \^dbg_acc\(4),
      O => result3_carry_i_18_n_0
    );
result3_carry_i_19: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => acc32_reg(26),
      I1 => \^dbg_acc\(10),
      I2 => Q(3),
      I3 => acc32_reg(18),
      I4 => Q(4),
      I5 => \^dbg_acc\(2),
      O => result3_carry_i_19_n_0
    );
result3_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(2),
      I1 => result3_carry_i_7_n_0,
      I2 => Q(0),
      I3 => result3_carry_i_5_n_0,
      O => result3_carry_i_2_n_0
    );
result3_carry_i_20: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => acc32_reg(25),
      I1 => \^dbg_acc\(9),
      I2 => Q(3),
      I3 => acc32_reg(17),
      I4 => Q(4),
      I5 => \^dbg_acc\(1),
      O => result3_carry_i_20_n_0
    );
result3_carry_i_21: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => acc32_reg(24),
      I1 => \^dbg_acc\(8),
      I2 => Q(3),
      I3 => acc32_reg(16),
      I4 => Q(4),
      I5 => \^dbg_acc\(0),
      O => result3_carry_i_21_n_0
    );
result3_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => x(1),
      I1 => result3_carry_i_8_n_0,
      I2 => Q(0),
      I3 => result3_carry_i_7_n_0,
      O => result3_carry_i_3_n_0
    );
result3_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAA9595555A959"
    )
        port map (
      I0 => x(0),
      I1 => result3_carry_i_9_n_0,
      I2 => Q(1),
      I3 => result3_carry_i_10_n_0,
      I4 => Q(0),
      I5 => result3_carry_i_8_n_0,
      O => result3_carry_i_4_n_0
    );
result3_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => result3_carry_i_11_n_0,
      I1 => result3_carry_i_12_n_0,
      I2 => Q(1),
      I3 => result3_carry_i_13_n_0,
      I4 => Q(2),
      I5 => result3_carry_i_14_n_0,
      O => result3_carry_i_5_n_0
    );
result3_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => result3_carry_i_15_n_0,
      I1 => result3_carry_i_16_n_0,
      I2 => Q(1),
      I3 => result3_carry_i_17_n_0,
      I4 => Q(2),
      I5 => result3_carry_i_18_n_0,
      O => result3_carry_i_6_n_0
    );
result3_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => result3_carry_i_17_n_0,
      I1 => result3_carry_i_18_n_0,
      I2 => Q(1),
      I3 => result3_carry_i_16_n_0,
      I4 => Q(2),
      I5 => result3_carry_i_19_n_0,
      O => result3_carry_i_7_n_0
    );
result3_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => result3_carry_i_13_n_0,
      I1 => result3_carry_i_14_n_0,
      I2 => Q(1),
      I3 => result3_carry_i_12_n_0,
      I4 => Q(2),
      I5 => result3_carry_i_20_n_0,
      O => result3_carry_i_8_n_0
    );
result3_carry_i_9: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => result3_carry_i_18_n_0,
      I1 => Q(2),
      I2 => result3_carry_i_21_n_0,
      O => result3_carry_i_9_n_0
    );
\result[0]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => result3_carry_n_7,
      O => p_2_in(0)
    );
\result[10]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__1_n_5\,
      O => p_2_in(10)
    );
\result[11]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__1_n_4\,
      O => p_2_in(11)
    );
\result[12]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__2_n_7\,
      O => p_2_in(12)
    );
\result[13]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__2_n_6\,
      O => p_2_in(13)
    );
\result[14]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__2_n_5\,
      O => p_2_in(14)
    );
\result[1]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => result3_carry_n_6,
      O => p_2_in(1)
    );
\result[2]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => result3_carry_n_5,
      O => p_2_in(2)
    );
\result[31]_C_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_0_in,
      O => p_2_in(31)
    );
\result[3]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => result3_carry_n_4,
      O => p_2_in(3)
    );
\result[4]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__0_n_7\,
      O => p_2_in(4)
    );
\result[5]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__0_n_6\,
      O => p_2_in(5)
    );
\result[6]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__0_n_5\,
      O => p_2_in(6)
    );
\result[7]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__0_n_4\,
      O => p_2_in(7)
    );
\result[8]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__1_n_7\,
      O => p_2_in(8)
    );
\result[9]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__1_n_6\,
      O => p_2_in(9)
    );
\result_reg[0]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[0]_LDC_i_2_n_0\,
      D => p_2_in(0),
      Q => \result_reg[0]_C_n_0\
    );
\result_reg[0]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[0]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[0]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[0]_LDC_n_0\
    );
\result_reg[0]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(0),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[0]_LDC_i_1_n_0\
    );
\result_reg[0]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(0),
      O => \result_reg[0]_LDC_i_2_n_0\
    );
\result_reg[0]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(0),
      PRE => \result_reg[0]_LDC_i_1_n_0\,
      Q => \result_reg[0]_P_n_0\
    );
\result_reg[10]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[10]_LDC_i_2_n_0\,
      D => p_2_in(10),
      Q => \result_reg[10]_C_n_0\
    );
\result_reg[10]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[10]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[10]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[10]_LDC_n_0\
    );
\result_reg[10]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(10),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[10]_LDC_i_1_n_0\
    );
\result_reg[10]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(10),
      O => \result_reg[10]_LDC_i_2_n_0\
    );
\result_reg[10]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(10),
      PRE => \result_reg[10]_LDC_i_1_n_0\,
      Q => \result_reg[10]_P_n_0\
    );
\result_reg[11]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[11]_LDC_i_2_n_0\,
      D => p_2_in(11),
      Q => \result_reg[11]_C_n_0\
    );
\result_reg[11]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[11]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[11]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[11]_LDC_n_0\
    );
\result_reg[11]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(11),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[11]_LDC_i_1_n_0\
    );
\result_reg[11]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(11),
      O => \result_reg[11]_LDC_i_2_n_0\
    );
\result_reg[11]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(11),
      PRE => \result_reg[11]_LDC_i_1_n_0\,
      Q => \result_reg[11]_P_n_0\
    );
\result_reg[12]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[12]_LDC_i_2_n_0\,
      D => p_2_in(12),
      Q => \result_reg[12]_C_n_0\
    );
\result_reg[12]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[12]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[12]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[12]_LDC_n_0\
    );
\result_reg[12]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(12),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[12]_LDC_i_1_n_0\
    );
\result_reg[12]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(12),
      O => \result_reg[12]_LDC_i_2_n_0\
    );
\result_reg[12]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(12),
      PRE => \result_reg[12]_LDC_i_1_n_0\,
      Q => \result_reg[12]_P_n_0\
    );
\result_reg[13]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[13]_LDC_i_2_n_0\,
      D => p_2_in(13),
      Q => \result_reg[13]_C_n_0\
    );
\result_reg[13]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[13]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[13]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[13]_LDC_n_0\
    );
\result_reg[13]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(13),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[13]_LDC_i_1_n_0\
    );
\result_reg[13]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(13),
      O => \result_reg[13]_LDC_i_2_n_0\
    );
\result_reg[13]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(13),
      PRE => \result_reg[13]_LDC_i_1_n_0\,
      Q => \result_reg[13]_P_n_0\
    );
\result_reg[14]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[14]_LDC_i_2_n_0\,
      D => p_2_in(14),
      Q => \result_reg[14]_C_n_0\
    );
\result_reg[14]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[14]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[14]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[14]_LDC_n_0\
    );
\result_reg[14]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(14),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[14]_LDC_i_1_n_0\
    );
\result_reg[14]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(14),
      O => \result_reg[14]_LDC_i_2_n_0\
    );
\result_reg[14]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(14),
      PRE => \result_reg[14]_LDC_i_1_n_0\,
      Q => \result_reg[14]_P_n_0\
    );
\result_reg[1]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[1]_LDC_i_2_n_0\,
      D => p_2_in(1),
      Q => \result_reg[1]_C_n_0\
    );
\result_reg[1]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[1]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[1]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[1]_LDC_n_0\
    );
\result_reg[1]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(1),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[1]_LDC_i_1_n_0\
    );
\result_reg[1]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(1),
      O => \result_reg[1]_LDC_i_2_n_0\
    );
\result_reg[1]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(1),
      PRE => \result_reg[1]_LDC_i_1_n_0\,
      Q => \result_reg[1]_P_n_0\
    );
\result_reg[2]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[2]_LDC_i_2_n_0\,
      D => p_2_in(2),
      Q => \result_reg[2]_C_n_0\
    );
\result_reg[2]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[2]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[2]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[2]_LDC_n_0\
    );
\result_reg[2]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(2),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[2]_LDC_i_1_n_0\
    );
\result_reg[2]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(2),
      O => \result_reg[2]_LDC_i_2_n_0\
    );
\result_reg[2]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(2),
      PRE => \result_reg[2]_LDC_i_1_n_0\,
      Q => \result_reg[2]_P_n_0\
    );
\result_reg[31]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[31]_LDC_i_2_n_0\,
      D => p_2_in(31),
      Q => \result_reg[31]_C_n_0\
    );
\result_reg[31]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[31]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[31]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[31]_LDC_n_0\
    );
\result_reg[31]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(15),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[31]_LDC_i_1_n_0\
    );
\result_reg[31]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(15),
      O => \result_reg[31]_LDC_i_2_n_0\
    );
\result_reg[31]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(31),
      PRE => \result_reg[31]_LDC_i_1_n_0\,
      Q => \result_reg[31]_P_n_0\
    );
\result_reg[3]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[3]_LDC_i_2_n_0\,
      D => p_2_in(3),
      Q => \result_reg[3]_C_n_0\
    );
\result_reg[3]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[3]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[3]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[3]_LDC_n_0\
    );
\result_reg[3]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(3),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[3]_LDC_i_1_n_0\
    );
\result_reg[3]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(3),
      O => \result_reg[3]_LDC_i_2_n_0\
    );
\result_reg[3]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(3),
      PRE => \result_reg[3]_LDC_i_1_n_0\,
      Q => \result_reg[3]_P_n_0\
    );
\result_reg[4]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[4]_LDC_i_2_n_0\,
      D => p_2_in(4),
      Q => \result_reg[4]_C_n_0\
    );
\result_reg[4]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[4]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[4]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[4]_LDC_n_0\
    );
\result_reg[4]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(4),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[4]_LDC_i_1_n_0\
    );
\result_reg[4]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(4),
      O => \result_reg[4]_LDC_i_2_n_0\
    );
\result_reg[4]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(4),
      PRE => \result_reg[4]_LDC_i_1_n_0\,
      Q => \result_reg[4]_P_n_0\
    );
\result_reg[5]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[5]_LDC_i_2_n_0\,
      D => p_2_in(5),
      Q => \result_reg[5]_C_n_0\
    );
\result_reg[5]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[5]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[5]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[5]_LDC_n_0\
    );
\result_reg[5]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(5),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[5]_LDC_i_1_n_0\
    );
\result_reg[5]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(5),
      O => \result_reg[5]_LDC_i_2_n_0\
    );
\result_reg[5]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(5),
      PRE => \result_reg[5]_LDC_i_1_n_0\,
      Q => \result_reg[5]_P_n_0\
    );
\result_reg[6]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[6]_LDC_i_2_n_0\,
      D => p_2_in(6),
      Q => \result_reg[6]_C_n_0\
    );
\result_reg[6]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[6]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[6]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[6]_LDC_n_0\
    );
\result_reg[6]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(6),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[6]_LDC_i_1_n_0\
    );
\result_reg[6]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(6),
      O => \result_reg[6]_LDC_i_2_n_0\
    );
\result_reg[6]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(6),
      PRE => \result_reg[6]_LDC_i_1_n_0\,
      Q => \result_reg[6]_P_n_0\
    );
\result_reg[7]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[7]_LDC_i_2_n_0\,
      D => p_2_in(7),
      Q => \result_reg[7]_C_n_0\
    );
\result_reg[7]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[7]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[7]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[7]_LDC_n_0\
    );
\result_reg[7]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(7),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[7]_LDC_i_1_n_0\
    );
\result_reg[7]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(7),
      O => \result_reg[7]_LDC_i_2_n_0\
    );
\result_reg[7]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(7),
      PRE => \result_reg[7]_LDC_i_1_n_0\,
      Q => \result_reg[7]_P_n_0\
    );
\result_reg[8]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[8]_LDC_i_2_n_0\,
      D => p_2_in(8),
      Q => \result_reg[8]_C_n_0\
    );
\result_reg[8]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[8]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[8]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[8]_LDC_n_0\
    );
\result_reg[8]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(8),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[8]_LDC_i_1_n_0\
    );
\result_reg[8]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(8),
      O => \result_reg[8]_LDC_i_2_n_0\
    );
\result_reg[8]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(8),
      PRE => \result_reg[8]_LDC_i_1_n_0\,
      Q => \result_reg[8]_P_n_0\
    );
\result_reg[9]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[9]_LDC_i_2_n_0\,
      D => p_2_in(9),
      Q => \result_reg[9]_C_n_0\
    );
\result_reg[9]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[9]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[9]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[9]_LDC_n_0\
    );
\result_reg[9]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(9),
      I1 => Q(5),
      I2 => resetn,
      O => \result_reg[9]_LDC_i_1_n_0\
    );
\result_reg[9]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(5),
      I1 => resetn,
      I2 => x(9),
      O => \result_reg[9]_LDC_i_2_n_0\
    );
\result_reg[9]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(9),
      PRE => \result_reg[9]_LDC_i_1_n_0\,
      Q => \result_reg[9]_P_n_0\
    );
\y16_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[0]_P_n_0\,
      I1 => \result_reg[0]_LDC_n_0\,
      I2 => \result_reg[0]_C_n_0\,
      O => RESIZE(0)
    );
\y16_reg[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[10]_P_n_0\,
      I1 => \result_reg[10]_LDC_n_0\,
      I2 => \result_reg[10]_C_n_0\,
      O => RESIZE(10)
    );
\y16_reg[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[11]_P_n_0\,
      I1 => \result_reg[11]_LDC_n_0\,
      I2 => \result_reg[11]_C_n_0\,
      O => RESIZE(11)
    );
\y16_reg[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[12]_P_n_0\,
      I1 => \result_reg[12]_LDC_n_0\,
      I2 => \result_reg[12]_C_n_0\,
      O => RESIZE(12)
    );
\y16_reg[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[13]_P_n_0\,
      I1 => \result_reg[13]_LDC_n_0\,
      I2 => \result_reg[13]_C_n_0\,
      O => RESIZE(13)
    );
\y16_reg[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[14]_P_n_0\,
      I1 => \result_reg[14]_LDC_n_0\,
      I2 => \result_reg[14]_C_n_0\,
      O => RESIZE(14)
    );
\y16_reg[15]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => \^sr\(0)
    );
\y16_reg[15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[31]_P_n_0\,
      I1 => \result_reg[31]_LDC_n_0\,
      I2 => \result_reg[31]_C_n_0\,
      O => RESIZE(15)
    );
\y16_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[1]_P_n_0\,
      I1 => \result_reg[1]_LDC_n_0\,
      I2 => \result_reg[1]_C_n_0\,
      O => RESIZE(1)
    );
\y16_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[2]_P_n_0\,
      I1 => \result_reg[2]_LDC_n_0\,
      I2 => \result_reg[2]_C_n_0\,
      O => RESIZE(2)
    );
\y16_reg[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[3]_P_n_0\,
      I1 => \result_reg[3]_LDC_n_0\,
      I2 => \result_reg[3]_C_n_0\,
      O => RESIZE(3)
    );
\y16_reg[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[4]_P_n_0\,
      I1 => \result_reg[4]_LDC_n_0\,
      I2 => \result_reg[4]_C_n_0\,
      O => RESIZE(4)
    );
\y16_reg[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[5]_P_n_0\,
      I1 => \result_reg[5]_LDC_n_0\,
      I2 => \result_reg[5]_C_n_0\,
      O => RESIZE(5)
    );
\y16_reg[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[6]_P_n_0\,
      I1 => \result_reg[6]_LDC_n_0\,
      I2 => \result_reg[6]_C_n_0\,
      O => RESIZE(6)
    );
\y16_reg[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[7]_P_n_0\,
      I1 => \result_reg[7]_LDC_n_0\,
      I2 => \result_reg[7]_C_n_0\,
      O => RESIZE(7)
    );
\y16_reg[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[8]_P_n_0\,
      I1 => \result_reg[8]_LDC_n_0\,
      I2 => \result_reg[8]_C_n_0\,
      O => RESIZE(8)
    );
\y16_reg[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[9]_P_n_0\,
      I1 => \result_reg[9]_LDC_n_0\,
      I2 => \result_reg[9]_C_n_0\,
      O => RESIZE(9)
    );
\y16_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(0),
      Q => y(0),
      R => \^sr\(0)
    );
\y16_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(10),
      Q => y(10),
      R => \^sr\(0)
    );
\y16_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(11),
      Q => y(11),
      R => \^sr\(0)
    );
\y16_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(12),
      Q => y(12),
      R => \^sr\(0)
    );
\y16_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(13),
      Q => y(13),
      R => \^sr\(0)
    );
\y16_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(14),
      Q => y(14),
      R => \^sr\(0)
    );
\y16_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(15),
      Q => y(15),
      R => \^sr\(0)
    );
\y16_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(1),
      Q => y(1),
      R => \^sr\(0)
    );
\y16_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(2),
      Q => y(2),
      R => \^sr\(0)
    );
\y16_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(3),
      Q => y(3),
      R => \^sr\(0)
    );
\y16_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(4),
      Q => y(4),
      R => \^sr\(0)
    );
\y16_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(5),
      Q => y(5),
      R => \^sr\(0)
    );
\y16_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(6),
      Q => y(6),
      R => \^sr\(0)
    );
\y16_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(7),
      Q => y(7),
      R => \^sr\(0)
    );
\y16_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(8),
      Q => y(8),
      R => \^sr\(0)
    );
\y16_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => RESIZE(9),
      Q => y(9),
      R => \^sr\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_dc_stabilizer_v1_0_AXI_DC_STAB is
  port (
    dbg_acc : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_AWREADY : out STD_LOGIC;
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    axi_dc_stab_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_dc_stab_rvalid : out STD_LOGIC;
    axi_dc_stab_bvalid : out STD_LOGIC;
    clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    resetn : in STD_LOGIC;
    axi_dc_stab_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_dc_stab_wvalid : in STD_LOGIC;
    axi_dc_stab_awvalid : in STD_LOGIC;
    axi_dc_stab_aclk : in STD_LOGIC;
    axi_dc_stab_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_dc_stab_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_dc_stab_arvalid : in STD_LOGIC;
    feedback : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axi_dc_stab_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_dc_stab_aresetn : in STD_LOGIC;
    axi_dc_stab_bready : in STD_LOGIC;
    axi_dc_stab_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_dc_stabilizer_v1_0_AXI_DC_STAB;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_dc_stabilizer_v1_0_AXI_DC_STAB is
  signal DCS_n_32 : STD_LOGIC;
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_araddr : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_awready_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \^axi_dc_stab_bvalid\ : STD_LOGIC;
  signal \^axi_dc_stab_rvalid\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal dc_stab_enable : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg0[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg0_D : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg0_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[9]\ : STD_LOGIC;
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg1_D : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[9]\ : STD_LOGIC;
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[9]\ : STD_LOGIC;
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair15";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  axi_dc_stab_bvalid <= \^axi_dc_stab_bvalid\;
  axi_dc_stab_rvalid <= \^axi_dc_stab_rvalid\;
DCS: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dc_stabilizer
     port map (
      Q(5) => dc_stab_enable,
      Q(4) => \slv_reg2_D_reg_n_0_[4]\,
      Q(3) => \slv_reg2_D_reg_n_0_[3]\,
      Q(2) => \slv_reg2_D_reg_n_0_[2]\,
      Q(1) => \slv_reg2_D_reg_n_0_[1]\,
      Q(0) => \slv_reg2_D_reg_n_0_[0]\,
      SR(0) => DCS_n_32,
      \acc321_carry__0_0\(15 downto 0) => slv_reg1_D(15 downto 0),
      \acc321_inferred__0/i__carry__0_0\(15 downto 0) => slv_reg0_D(15 downto 0),
      clk => clk,
      dbg_acc(15 downto 0) => dbg_acc(15 downto 0),
      feedback(15 downto 0) => feedback(15 downto 0),
      resetn => resetn,
      x(15 downto 0) => x(15 downto 0),
      y(15 downto 0) => y(15 downto 0)
    );
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBF00BF00BF00"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => axi_dc_stab_awvalid,
      I2 => axi_dc_stab_wvalid,
      I3 => aw_en_reg_n_0,
      I4 => axi_dc_stab_bready,
      I5 => \^axi_dc_stab_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => axi_awready_i_1_n_0
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => axi_dc_stab_araddr(0),
      I1 => axi_dc_stab_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(2),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => axi_dc_stab_araddr(1),
      I1 => axi_dc_stab_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(3),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => axi_araddr(2),
      S => axi_awready_i_1_n_0
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => axi_araddr(3),
      S => axi_awready_i_1_n_0
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axi_dc_stab_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => axi_awready_i_1_n_0
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => axi_dc_stab_awaddr(0),
      I1 => aw_en_reg_n_0,
      I2 => axi_dc_stab_wvalid,
      I3 => axi_dc_stab_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => axi_dc_stab_awaddr(1),
      I1 => aw_en_reg_n_0,
      I2 => axi_dc_stab_wvalid,
      I3 => axi_dc_stab_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_0_in(0),
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_0_in(1),
      R => axi_awready_i_1_n_0
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_dc_stab_aresetn,
      O => axi_awready_i_1_n_0
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => axi_dc_stab_wvalid,
      I2 => axi_dc_stab_awvalid,
      I3 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => axi_awready_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => axi_dc_stab_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => axi_dc_stab_wvalid,
      I4 => axi_dc_stab_bready,
      I5 => \^axi_dc_stab_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^axi_dc_stab_bvalid\,
      R => axi_awready_i_1_n_0
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[0]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[0]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[0]\,
      O => reg_data_out(0)
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[10]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[10]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[10]\,
      O => reg_data_out(10)
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[11]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[11]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[11]\,
      O => reg_data_out(11)
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[12]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[12]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[12]\,
      O => reg_data_out(12)
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[13]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[13]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[13]\,
      O => reg_data_out(13)
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[14]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[14]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[14]\,
      O => reg_data_out(14)
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[15]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[15]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[15]\,
      O => reg_data_out(15)
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[16]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[16]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[16]\,
      O => reg_data_out(16)
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[17]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[17]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[17]\,
      O => reg_data_out(17)
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[18]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[18]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[18]\,
      O => reg_data_out(18)
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[19]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[19]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[19]\,
      O => reg_data_out(19)
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[1]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[1]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[1]\,
      O => reg_data_out(1)
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[20]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[20]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[20]\,
      O => reg_data_out(20)
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[21]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[21]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[21]\,
      O => reg_data_out(21)
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[22]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[22]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[22]\,
      O => reg_data_out(22)
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[23]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[23]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[23]\,
      O => reg_data_out(23)
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[24]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[24]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[24]\,
      O => reg_data_out(24)
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[25]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[25]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[25]\,
      O => reg_data_out(25)
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[26]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[26]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[26]\,
      O => reg_data_out(26)
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[27]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[27]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[27]\,
      O => reg_data_out(27)
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[28]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[28]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[28]\,
      O => reg_data_out(28)
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[29]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[29]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[29]\,
      O => reg_data_out(29)
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[2]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[2]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[2]\,
      O => reg_data_out(2)
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[30]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[30]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[30]\,
      O => reg_data_out(30)
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => axi_dc_stab_arvalid,
      I2 => \^axi_dc_stab_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[31]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[31]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[31]\,
      O => reg_data_out(31)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[3]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[3]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[3]\,
      O => reg_data_out(3)
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[4]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[4]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[4]\,
      O => reg_data_out(4)
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[5]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[5]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[5]\,
      O => reg_data_out(5)
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[6]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[6]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[6]\,
      O => reg_data_out(6)
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[7]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[7]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[7]\,
      O => reg_data_out(7)
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[8]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[8]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[8]\,
      O => reg_data_out(8)
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[9]\,
      I1 => axi_araddr(2),
      I2 => \slv_reg2_reg_n_0_[9]\,
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[9]\,
      O => reg_data_out(9)
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => axi_dc_stab_rdata(0),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => axi_dc_stab_rdata(10),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => axi_dc_stab_rdata(11),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => axi_dc_stab_rdata(12),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => axi_dc_stab_rdata(13),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => axi_dc_stab_rdata(14),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => axi_dc_stab_rdata(15),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => axi_dc_stab_rdata(16),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => axi_dc_stab_rdata(17),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => axi_dc_stab_rdata(18),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => axi_dc_stab_rdata(19),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => axi_dc_stab_rdata(1),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => axi_dc_stab_rdata(20),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => axi_dc_stab_rdata(21),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => axi_dc_stab_rdata(22),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => axi_dc_stab_rdata(23),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => axi_dc_stab_rdata(24),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => axi_dc_stab_rdata(25),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => axi_dc_stab_rdata(26),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => axi_dc_stab_rdata(27),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => axi_dc_stab_rdata(28),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => axi_dc_stab_rdata(29),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => axi_dc_stab_rdata(2),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => axi_dc_stab_rdata(30),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => axi_dc_stab_rdata(31),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => axi_dc_stab_rdata(3),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => axi_dc_stab_rdata(4),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => axi_dc_stab_rdata(5),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => axi_dc_stab_rdata(6),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => axi_dc_stab_rdata(7),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => axi_dc_stab_rdata(8),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => axi_dc_stab_rdata(9),
      R => axi_awready_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => axi_dc_stab_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^axi_dc_stab_rvalid\,
      I3 => axi_dc_stab_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^axi_dc_stab_rvalid\,
      R => axi_awready_i_1_n_0
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => axi_dc_stab_wvalid,
      I2 => axi_dc_stab_awvalid,
      I3 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => axi_dc_stab_wstrb(1),
      O => \slv_reg0[15]_i_1_n_0\
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => axi_dc_stab_wstrb(2),
      O => \slv_reg0[23]_i_1_n_0\
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => axi_dc_stab_wstrb(3),
      O => \slv_reg0[31]_i_1_n_0\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => axi_dc_stab_wstrb(0),
      O => \slv_reg0[7]_i_1_n_0\
    );
\slv_reg0_D_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[0]\,
      Q => slv_reg0_D(0),
      R => DCS_n_32
    );
\slv_reg0_D_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[10]\,
      Q => slv_reg0_D(10),
      R => DCS_n_32
    );
\slv_reg0_D_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[11]\,
      Q => slv_reg0_D(11),
      R => DCS_n_32
    );
\slv_reg0_D_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[12]\,
      Q => slv_reg0_D(12),
      R => DCS_n_32
    );
\slv_reg0_D_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[13]\,
      Q => slv_reg0_D(13),
      R => DCS_n_32
    );
\slv_reg0_D_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[14]\,
      Q => slv_reg0_D(14),
      R => DCS_n_32
    );
\slv_reg0_D_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[15]\,
      Q => slv_reg0_D(15),
      R => DCS_n_32
    );
\slv_reg0_D_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[1]\,
      Q => slv_reg0_D(1),
      R => DCS_n_32
    );
\slv_reg0_D_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[2]\,
      Q => slv_reg0_D(2),
      R => DCS_n_32
    );
\slv_reg0_D_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[3]\,
      Q => slv_reg0_D(3),
      R => DCS_n_32
    );
\slv_reg0_D_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[4]\,
      Q => slv_reg0_D(4),
      R => DCS_n_32
    );
\slv_reg0_D_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[5]\,
      Q => slv_reg0_D(5),
      R => DCS_n_32
    );
\slv_reg0_D_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[6]\,
      Q => slv_reg0_D(6),
      R => DCS_n_32
    );
\slv_reg0_D_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[7]\,
      Q => slv_reg0_D(7),
      R => DCS_n_32
    );
\slv_reg0_D_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[8]\,
      Q => slv_reg0_D(8),
      R => DCS_n_32
    );
\slv_reg0_D_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[9]\,
      Q => slv_reg0_D(9),
      R => DCS_n_32
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(0),
      Q => \slv_reg0_reg_n_0_[0]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(10),
      Q => \slv_reg0_reg_n_0_[10]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(11),
      Q => \slv_reg0_reg_n_0_[11]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(12),
      Q => \slv_reg0_reg_n_0_[12]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(13),
      Q => \slv_reg0_reg_n_0_[13]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(14),
      Q => \slv_reg0_reg_n_0_[14]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(15),
      Q => \slv_reg0_reg_n_0_[15]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(16),
      Q => \slv_reg0_reg_n_0_[16]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(17),
      Q => \slv_reg0_reg_n_0_[17]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(18),
      Q => \slv_reg0_reg_n_0_[18]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(19),
      Q => \slv_reg0_reg_n_0_[19]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(1),
      Q => \slv_reg0_reg_n_0_[1]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(20),
      Q => \slv_reg0_reg_n_0_[20]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(21),
      Q => \slv_reg0_reg_n_0_[21]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(22),
      Q => \slv_reg0_reg_n_0_[22]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(23),
      Q => \slv_reg0_reg_n_0_[23]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(24),
      Q => \slv_reg0_reg_n_0_[24]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(25),
      Q => \slv_reg0_reg_n_0_[25]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(26),
      Q => \slv_reg0_reg_n_0_[26]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(27),
      Q => \slv_reg0_reg_n_0_[27]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(28),
      Q => \slv_reg0_reg_n_0_[28]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(29),
      Q => \slv_reg0_reg_n_0_[29]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(2),
      Q => \slv_reg0_reg_n_0_[2]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(30),
      Q => \slv_reg0_reg_n_0_[30]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(31),
      Q => \slv_reg0_reg_n_0_[31]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(3),
      Q => \slv_reg0_reg_n_0_[3]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(4),
      Q => \slv_reg0_reg_n_0_[4]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(5),
      Q => \slv_reg0_reg_n_0_[5]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(6),
      Q => \slv_reg0_reg_n_0_[6]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(7),
      Q => \slv_reg0_reg_n_0_[7]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(8),
      Q => \slv_reg0_reg_n_0_[8]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(9),
      Q => \slv_reg0_reg_n_0_[9]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_dc_stab_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_dc_stab_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_dc_stab_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => axi_dc_stab_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => axi_dc_stab_wvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_dc_stab_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_D_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[0]\,
      Q => slv_reg1_D(0),
      R => DCS_n_32
    );
\slv_reg1_D_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[10]\,
      Q => slv_reg1_D(10),
      R => DCS_n_32
    );
\slv_reg1_D_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[11]\,
      Q => slv_reg1_D(11),
      R => DCS_n_32
    );
\slv_reg1_D_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[12]\,
      Q => slv_reg1_D(12),
      R => DCS_n_32
    );
\slv_reg1_D_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[13]\,
      Q => slv_reg1_D(13),
      R => DCS_n_32
    );
\slv_reg1_D_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[14]\,
      Q => slv_reg1_D(14),
      R => DCS_n_32
    );
\slv_reg1_D_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[15]\,
      Q => slv_reg1_D(15),
      R => DCS_n_32
    );
\slv_reg1_D_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[1]\,
      Q => slv_reg1_D(1),
      R => DCS_n_32
    );
\slv_reg1_D_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[2]\,
      Q => slv_reg1_D(2),
      R => DCS_n_32
    );
\slv_reg1_D_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[3]\,
      Q => slv_reg1_D(3),
      R => DCS_n_32
    );
\slv_reg1_D_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[4]\,
      Q => slv_reg1_D(4),
      R => DCS_n_32
    );
\slv_reg1_D_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[5]\,
      Q => slv_reg1_D(5),
      R => DCS_n_32
    );
\slv_reg1_D_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[6]\,
      Q => slv_reg1_D(6),
      R => DCS_n_32
    );
\slv_reg1_D_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[7]\,
      Q => slv_reg1_D(7),
      R => DCS_n_32
    );
\slv_reg1_D_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[8]\,
      Q => slv_reg1_D(8),
      R => DCS_n_32
    );
\slv_reg1_D_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[9]\,
      Q => slv_reg1_D(9),
      R => DCS_n_32
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(0),
      Q => \slv_reg1_reg_n_0_[0]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(10),
      Q => \slv_reg1_reg_n_0_[10]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(11),
      Q => \slv_reg1_reg_n_0_[11]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(12),
      Q => \slv_reg1_reg_n_0_[12]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(13),
      Q => \slv_reg1_reg_n_0_[13]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(14),
      Q => \slv_reg1_reg_n_0_[14]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(15),
      Q => \slv_reg1_reg_n_0_[15]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(16),
      Q => \slv_reg1_reg_n_0_[16]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(17),
      Q => \slv_reg1_reg_n_0_[17]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(18),
      Q => \slv_reg1_reg_n_0_[18]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(19),
      Q => \slv_reg1_reg_n_0_[19]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(1),
      Q => \slv_reg1_reg_n_0_[1]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(20),
      Q => \slv_reg1_reg_n_0_[20]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(21),
      Q => \slv_reg1_reg_n_0_[21]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(22),
      Q => \slv_reg1_reg_n_0_[22]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(23),
      Q => \slv_reg1_reg_n_0_[23]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(24),
      Q => \slv_reg1_reg_n_0_[24]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(25),
      Q => \slv_reg1_reg_n_0_[25]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(26),
      Q => \slv_reg1_reg_n_0_[26]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(27),
      Q => \slv_reg1_reg_n_0_[27]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(28),
      Q => \slv_reg1_reg_n_0_[28]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(29),
      Q => \slv_reg1_reg_n_0_[29]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(2),
      Q => \slv_reg1_reg_n_0_[2]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(30),
      Q => \slv_reg1_reg_n_0_[30]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(31),
      Q => \slv_reg1_reg_n_0_[31]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(3),
      Q => \slv_reg1_reg_n_0_[3]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(4),
      Q => \slv_reg1_reg_n_0_[4]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(5),
      Q => \slv_reg1_reg_n_0_[5]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(6),
      Q => \slv_reg1_reg_n_0_[6]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(7),
      Q => \slv_reg1_reg_n_0_[7]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(8),
      Q => \slv_reg1_reg_n_0_[8]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(9),
      Q => \slv_reg1_reg_n_0_[9]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => axi_dc_stab_wstrb(1),
      I3 => p_0_in(0),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => axi_dc_stab_wstrb(2),
      I3 => p_0_in(0),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => axi_dc_stab_wstrb(3),
      I3 => p_0_in(0),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => axi_dc_stab_wstrb(0),
      I3 => p_0_in(0),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_D_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[0]\,
      Q => \slv_reg2_D_reg_n_0_[0]\,
      R => DCS_n_32
    );
\slv_reg2_D_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[1]\,
      Q => \slv_reg2_D_reg_n_0_[1]\,
      R => DCS_n_32
    );
\slv_reg2_D_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[2]\,
      Q => \slv_reg2_D_reg_n_0_[2]\,
      R => DCS_n_32
    );
\slv_reg2_D_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[3]\,
      Q => \slv_reg2_D_reg_n_0_[3]\,
      R => DCS_n_32
    );
\slv_reg2_D_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[4]\,
      Q => \slv_reg2_D_reg_n_0_[4]\,
      R => DCS_n_32
    );
\slv_reg2_D_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[5]\,
      Q => dc_stab_enable,
      R => DCS_n_32
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(0),
      Q => \slv_reg2_reg_n_0_[0]\,
      S => axi_awready_i_1_n_0
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(10),
      Q => \slv_reg2_reg_n_0_[10]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(11),
      Q => \slv_reg2_reg_n_0_[11]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(12),
      Q => \slv_reg2_reg_n_0_[12]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(13),
      Q => \slv_reg2_reg_n_0_[13]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(14),
      Q => \slv_reg2_reg_n_0_[14]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(15),
      Q => \slv_reg2_reg_n_0_[15]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(16),
      Q => \slv_reg2_reg_n_0_[16]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(17),
      Q => \slv_reg2_reg_n_0_[17]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(18),
      Q => \slv_reg2_reg_n_0_[18]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(19),
      Q => \slv_reg2_reg_n_0_[19]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(1),
      Q => \slv_reg2_reg_n_0_[1]\,
      S => axi_awready_i_1_n_0
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(20),
      Q => \slv_reg2_reg_n_0_[20]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(21),
      Q => \slv_reg2_reg_n_0_[21]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(22),
      Q => \slv_reg2_reg_n_0_[22]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => axi_dc_stab_wdata(23),
      Q => \slv_reg2_reg_n_0_[23]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(24),
      Q => \slv_reg2_reg_n_0_[24]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(25),
      Q => \slv_reg2_reg_n_0_[25]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(26),
      Q => \slv_reg2_reg_n_0_[26]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(27),
      Q => \slv_reg2_reg_n_0_[27]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(28),
      Q => \slv_reg2_reg_n_0_[28]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(29),
      Q => \slv_reg2_reg_n_0_[29]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(2),
      Q => \slv_reg2_reg_n_0_[2]\,
      S => axi_awready_i_1_n_0
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(30),
      Q => \slv_reg2_reg_n_0_[30]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => axi_dc_stab_wdata(31),
      Q => \slv_reg2_reg_n_0_[31]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(3),
      Q => \slv_reg2_reg_n_0_[3]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(4),
      Q => \slv_reg2_reg_n_0_[4]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(5),
      Q => \slv_reg2_reg_n_0_[5]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(6),
      Q => \slv_reg2_reg_n_0_[6]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => axi_dc_stab_wdata(7),
      Q => \slv_reg2_reg_n_0_[7]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(8),
      Q => \slv_reg2_reg_n_0_[8]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_dc_stab_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => axi_dc_stab_wdata(9),
      Q => \slv_reg2_reg_n_0_[9]\,
      R => axi_awready_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_dc_stabilizer_v1_0 is
  port (
    dbg_acc : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_AWREADY : out STD_LOGIC;
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    axi_dc_stab_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_dc_stab_rvalid : out STD_LOGIC;
    axi_dc_stab_bvalid : out STD_LOGIC;
    clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    resetn : in STD_LOGIC;
    axi_dc_stab_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_dc_stab_wvalid : in STD_LOGIC;
    axi_dc_stab_awvalid : in STD_LOGIC;
    axi_dc_stab_aclk : in STD_LOGIC;
    axi_dc_stab_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_dc_stab_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_dc_stab_arvalid : in STD_LOGIC;
    feedback : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axi_dc_stab_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_dc_stab_aresetn : in STD_LOGIC;
    axi_dc_stab_bready : in STD_LOGIC;
    axi_dc_stab_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_dc_stabilizer_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_dc_stabilizer_v1_0 is
begin
ip_dc_stabilizer_v1_0_AXI_DC_STAB_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_dc_stabilizer_v1_0_AXI_DC_STAB
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      axi_dc_stab_aclk => axi_dc_stab_aclk,
      axi_dc_stab_araddr(1 downto 0) => axi_dc_stab_araddr(1 downto 0),
      axi_dc_stab_aresetn => axi_dc_stab_aresetn,
      axi_dc_stab_arvalid => axi_dc_stab_arvalid,
      axi_dc_stab_awaddr(1 downto 0) => axi_dc_stab_awaddr(1 downto 0),
      axi_dc_stab_awvalid => axi_dc_stab_awvalid,
      axi_dc_stab_bready => axi_dc_stab_bready,
      axi_dc_stab_bvalid => axi_dc_stab_bvalid,
      axi_dc_stab_rdata(31 downto 0) => axi_dc_stab_rdata(31 downto 0),
      axi_dc_stab_rready => axi_dc_stab_rready,
      axi_dc_stab_rvalid => axi_dc_stab_rvalid,
      axi_dc_stab_wdata(31 downto 0) => axi_dc_stab_wdata(31 downto 0),
      axi_dc_stab_wstrb(3 downto 0) => axi_dc_stab_wstrb(3 downto 0),
      axi_dc_stab_wvalid => axi_dc_stab_wvalid,
      clk => clk,
      dbg_acc(15 downto 0) => dbg_acc(15 downto 0),
      feedback(15 downto 0) => feedback(15 downto 0),
      resetn => resetn,
      x(15 downto 0) => x(15 downto 0),
      y(15 downto 0) => y(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    feedback : in STD_LOGIC_VECTOR ( 15 downto 0 );
    resetn : in STD_LOGIC;
    clk : in STD_LOGIC;
    dbg_acc : out STD_LOGIC_VECTOR ( 15 downto 0 );
    axi_dc_stab_aclk : in STD_LOGIC;
    axi_dc_stab_aresetn : in STD_LOGIC;
    axi_dc_stab_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_dc_stab_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_dc_stab_awvalid : in STD_LOGIC;
    axi_dc_stab_awready : out STD_LOGIC;
    axi_dc_stab_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_dc_stab_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_dc_stab_wvalid : in STD_LOGIC;
    axi_dc_stab_wready : out STD_LOGIC;
    axi_dc_stab_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_dc_stab_bvalid : out STD_LOGIC;
    axi_dc_stab_bready : in STD_LOGIC;
    axi_dc_stab_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_dc_stab_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_dc_stab_arvalid : in STD_LOGIC;
    axi_dc_stab_arready : out STD_LOGIC;
    axi_dc_stab_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_dc_stab_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_dc_stab_rvalid : out STD_LOGIC;
    axi_dc_stab_rready : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ip_dc_stabilizer_0_0,ip_dc_stabilizer_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ip_dc_stabilizer_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of axi_dc_stab_aclk : signal is "xilinx.com:signal:clock:1.0 AXI_DC_STAB_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of axi_dc_stab_aclk : signal is "XIL_INTERFACENAME AXI_DC_STAB_CLK, ASSOCIATED_BUSIF AXI_DC_STAB, ASSOCIATED_RESET axi_dc_stab_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute x_interface_info of axi_dc_stab_aresetn : signal is "xilinx.com:signal:reset:1.0 AXI_DC_STAB_RST RST";
  attribute x_interface_parameter of axi_dc_stab_aresetn : signal is "XIL_INTERFACENAME AXI_DC_STAB_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of axi_dc_stab_arready : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARREADY";
  attribute x_interface_info of axi_dc_stab_arvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARVALID";
  attribute x_interface_info of axi_dc_stab_awready : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWREADY";
  attribute x_interface_info of axi_dc_stab_awvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWVALID";
  attribute x_interface_info of axi_dc_stab_bready : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BREADY";
  attribute x_interface_info of axi_dc_stab_bvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BVALID";
  attribute x_interface_info of axi_dc_stab_rready : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RREADY";
  attribute x_interface_info of axi_dc_stab_rvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RVALID";
  attribute x_interface_info of axi_dc_stab_wready : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WREADY";
  attribute x_interface_info of axi_dc_stab_wvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WVALID";
  attribute x_interface_info of axi_dc_stab_araddr : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARADDR";
  attribute x_interface_info of axi_dc_stab_arprot : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARPROT";
  attribute x_interface_info of axi_dc_stab_awaddr : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWADDR";
  attribute x_interface_parameter of axi_dc_stab_awaddr : signal is "XIL_INTERFACENAME AXI_DC_STAB, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of axi_dc_stab_awprot : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWPROT";
  attribute x_interface_info of axi_dc_stab_bresp : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BRESP";
  attribute x_interface_info of axi_dc_stab_rdata : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RDATA";
  attribute x_interface_info of axi_dc_stab_rresp : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RRESP";
  attribute x_interface_info of axi_dc_stab_wdata : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WDATA";
  attribute x_interface_info of axi_dc_stab_wstrb : signal is "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WSTRB";
begin
  axi_dc_stab_bresp(1) <= \<const0>\;
  axi_dc_stab_bresp(0) <= \<const0>\;
  axi_dc_stab_rresp(1) <= \<const0>\;
  axi_dc_stab_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_dc_stabilizer_v1_0
     port map (
      S_AXI_ARREADY => axi_dc_stab_arready,
      S_AXI_AWREADY => axi_dc_stab_awready,
      S_AXI_WREADY => axi_dc_stab_wready,
      axi_dc_stab_aclk => axi_dc_stab_aclk,
      axi_dc_stab_araddr(1 downto 0) => axi_dc_stab_araddr(3 downto 2),
      axi_dc_stab_aresetn => axi_dc_stab_aresetn,
      axi_dc_stab_arvalid => axi_dc_stab_arvalid,
      axi_dc_stab_awaddr(1 downto 0) => axi_dc_stab_awaddr(3 downto 2),
      axi_dc_stab_awvalid => axi_dc_stab_awvalid,
      axi_dc_stab_bready => axi_dc_stab_bready,
      axi_dc_stab_bvalid => axi_dc_stab_bvalid,
      axi_dc_stab_rdata(31 downto 0) => axi_dc_stab_rdata(31 downto 0),
      axi_dc_stab_rready => axi_dc_stab_rready,
      axi_dc_stab_rvalid => axi_dc_stab_rvalid,
      axi_dc_stab_wdata(31 downto 0) => axi_dc_stab_wdata(31 downto 0),
      axi_dc_stab_wstrb(3 downto 0) => axi_dc_stab_wstrb(3 downto 0),
      axi_dc_stab_wvalid => axi_dc_stab_wvalid,
      clk => clk,
      dbg_acc(15 downto 0) => dbg_acc(15 downto 0),
      feedback(15 downto 0) => feedback(15 downto 0),
      resetn => resetn,
      x(15 downto 0) => x(15 downto 0),
      y(15 downto 0) => y(15 downto 0)
    );
end STRUCTURE;
