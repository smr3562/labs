----------------------------------------------------------------------------------
-- Company: 
-- Engineer: M. Bogovac v1.0
-- 
-- Create Date: 04/07/2020 07:37:31 AM
-- Design Name: 
-- Module Name: test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: calculates the following
--		y(n) = x(n) - v(n)
--		v(n)= (1/2**m)*u(n)		 	
-- 		u(n) = acc(n) - acc(n-2**m)
--		acc(n) =  acc(n-1) + x(n)	
--		v(n) is moving average
--		v(n) = [x(n)+x(n-1)+...+x(n-2**(m-1))]/2**m
-- Dependencies: 
-- 		delay_line.vhdl
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;
use work.blr_pkg.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blr is 
	generic
		(
            BLR_DELAY_WIDTH	: integer	:= 10;
            BLR_ACCUMULATOR_WIDTH :	integer := 32;
            BLR_DATA_WIDTH	: integer	:= 16
		);
	port 
		(  
            x              : in std_logic_vector(BLR_DATA_WIDTH-1 downto 0);   --top modul does not support VHDL 2008   
            y              : out std_logic_vector(BLR_DATA_WIDTH-1 downto 0);           
            threshold_high : in std_logic_vector(BLR_DATA_WIDTH-1 downto 0);   
            threshold_low  : in std_logic_vector(BLR_DATA_WIDTH-1 downto 0);   
            resetn         : in std_logic;
            clk            : in std_logic;
            blr_enable     : in std_logic;
            m              : in std_logic_vector (f_log2(BLR_DELAY_WIDTH)-1 downto 0)
	  );
end blr;

architecture Behavioral of blr is
   
	signal en   : std_logic := '0';        
	signal y16  : signed(BLR_DATA_WIDTH-1 downto 0);        
	signal y32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);        
	signal x16  : signed(BLR_DATA_WIDTH-1 downto 0);        
	signal x32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);        
	signal u32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);        
	signal v32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);        
	signal acc32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0) := (others => '0');        
	signal acc32_m 	: std_logic_vector(BLR_ACCUMULATOR_WIDTH-1 downto 0) := (others => '0');
	signal threshold_high16 : signed(BLR_DATA_WIDTH-1 downto 0);        
	signal threshold_low16 : signed(BLR_DATA_WIDTH-1 downto 0);        
	signal delay10 : std_logic_vector(BLR_DELAY_WIDTH-1 downto 0);        

	component delay_line is
    generic
		(
			DL_DATA_WIDTH : integer :=32;
			DL_ADDRESS_WIDTH : integer :=10
		);

	port(   clk : in std_logic;
			en : in std_logic;
			resetn : in std_logic;	
			delay : in std_logic_vector(DL_ADDRESS_WIDTH-1 downto 0);
			dia : in std_logic_vector(DL_DATA_WIDTH-1 downto 0);
			dob : out std_logic_vector(DL_DATA_WIDTH-1 downto 0));
	end component;
   
begin

	-- inputs conversion to sign
	x16              <= signed(x);
	x32              <= resize(x16,BLR_ACCUMULATOR_WIDTH);
	threshold_high16 <= signed(threshold_high);
	threshold_low16  <= signed(threshold_low);

	-- outputs 		
	y <= std_logic_vector(y16);
	y16 <= resize(y32,BLR_DATA_WIDTH);
	
	--enable signal
	enable : process (clk)
	begin
		if resetn = '0' or blr_enable = '0' then
			en <= '0';
		elsif clk='1' and clk'event then
			if(x16 <  threshold_high16 and x16 > threshold_low16) then
				en <= '1'; 
			else 
				en <= '0'; 
			end if;
		end if;	
	end process;

	--calculate final result: y32(n) = x32(n)-v32(n)
	subtractor1 : process (clk)
		variable result: signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);
	begin
		if resetn = '0' or blr_enable = '0' then
			result := x32;
		elsif clk='1' and clk'event then
			result := x32 - v32;
			--check for + overflow in 16 bit sign arithmetic
			if(result(BLR_ACCUMULATOR_WIDTH-1)='0' and result(BLR_DATA_WIDTH-1)='1') then 
				--note: the maximal number is valid only for BLR_DATA_WIDTH=16
				--generalization requires to write function (TODO in the next version)
				result := x"00007fff";
			--check for - overflow in 16 bit sign arithmetic
			elsif(result(BLR_ACCUMULATOR_WIDTH-1)='1' and result(BLR_DATA_WIDTH-1)='0') then 		
				--note: the minimal number is valid only for BLR_DATA_WIDTH=16
				--generalization requires to write function (TODO in the next version)
				result := x"FFFF8000";
			end if;
		end if;
		y32 <= result;
	end process;

	-- calculate (1/2**m)*u32(n)
	v32 <= shift_right(u32,to_integer(unsigned(m)));

	--calculate u32(n) = acc32(n)-acc32(n-2**m))
	--overflow is not possible here
	subtractor2 : process (clk)
	begin
		if resetn = '0' or blr_enable = '0' then
			u32 <= (others => '0');		
		elsif clk='1' and clk'event then
			 if en = '1' then
				u32 <= acc32 - signed(acc32_m);
			 end if;
		end if;
	end process;
	 
	--calculate acc32(n)
	--overflof is not possible here
	accumulator : process (clk)
	begin
		if resetn = '0' or blr_enable = '0' then
			acc32 <= (others => '0');
		elsif clk='1' and clk'event then
			if en = '1' then
				acc32 <= acc32 + x32;
			end if;
		end if;
	end process;

	--calculate delay=2**m 
	--note: latency of our delay_line is 1, 
	--therefore delay must be 2**m-1 instead of 2**m
	--note: number 2**m-1 has 
	--all bits equal to 0 at bits' positions b(k)> m
	--all bits equal to 1 at bits' positions b(k)<=m
	--k is bit index in the interval [0,m-1]
	process(m)
		variable tmp: std_logic_vector (BLR_DELAY_WIDTH-1 downto 0);
	begin
		tmp := (others => '0');
		for I in tmp'low to tmp'high loop
			if(I < to_integer(unsigned(m))) then
				tmp(I) := '1';
			else
				tmp(I) := '0';
			end if;
		end loop;
		delay10 <= tmp;
	end process;
	
	--calculate acc32(n-2**m)
	--make delay of acc32(n) for 2**m clocks
	--the delay line is implemented as a BRAM
	--delay10 <= "0000011111";
	delay_line_inst : delay_line
	generic map(DL_DATA_WIDTH => BLR_ACCUMULATOR_WIDTH, DL_ADDRESS_WIDTH => BLR_DELAY_WIDTH)
	port map( clk=>clk, en=>en, resetn=> resetn, delay => delay10, dia => std_logic_vector(acc32),dob => acc32_m);

end Behavioral;