// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 17:57:52 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_bram_incr_0_0_sim_netlist.v
// Design      : design_1_bram_incr_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bram_incr
   (bram_we,
    bram_rst,
    bram_din,
    clk,
    peak_amp_rdy,
    bram_dout,
    rstn);
  output bram_we;
  output bram_rst;
  output [31:0]bram_din;
  input clk;
  input peak_amp_rdy;
  input [31:0]bram_dout;
  input rstn;

  wire \FSM_onehot_state[0]_i_1_n_0 ;
  wire \FSM_onehot_state[1]_i_1_n_0 ;
  wire \FSM_onehot_state_reg_n_0_[0] ;
  wire \FSM_onehot_state_reg_n_0_[1] ;
  wire [31:0]bram_din;
  wire [31:0]bram_dout;
  wire bram_rst;
  wire bram_we;
  wire clk;
  wire inc;
  wire peak_amp_rdy;
  wire [31:0]plusOp;
  wire rstn;
  wire \uSum_reg[12]_i_1_n_0 ;
  wire \uSum_reg[12]_i_1_n_1 ;
  wire \uSum_reg[12]_i_1_n_2 ;
  wire \uSum_reg[12]_i_1_n_3 ;
  wire \uSum_reg[16]_i_1_n_0 ;
  wire \uSum_reg[16]_i_1_n_1 ;
  wire \uSum_reg[16]_i_1_n_2 ;
  wire \uSum_reg[16]_i_1_n_3 ;
  wire \uSum_reg[20]_i_1_n_0 ;
  wire \uSum_reg[20]_i_1_n_1 ;
  wire \uSum_reg[20]_i_1_n_2 ;
  wire \uSum_reg[20]_i_1_n_3 ;
  wire \uSum_reg[24]_i_1_n_0 ;
  wire \uSum_reg[24]_i_1_n_1 ;
  wire \uSum_reg[24]_i_1_n_2 ;
  wire \uSum_reg[24]_i_1_n_3 ;
  wire \uSum_reg[28]_i_1_n_0 ;
  wire \uSum_reg[28]_i_1_n_1 ;
  wire \uSum_reg[28]_i_1_n_2 ;
  wire \uSum_reg[28]_i_1_n_3 ;
  wire \uSum_reg[31]_i_1_n_2 ;
  wire \uSum_reg[31]_i_1_n_3 ;
  wire \uSum_reg[4]_i_1_n_0 ;
  wire \uSum_reg[4]_i_1_n_1 ;
  wire \uSum_reg[4]_i_1_n_2 ;
  wire \uSum_reg[4]_i_1_n_3 ;
  wire \uSum_reg[8]_i_1_n_0 ;
  wire \uSum_reg[8]_i_1_n_1 ;
  wire \uSum_reg[8]_i_1_n_2 ;
  wire \uSum_reg[8]_i_1_n_3 ;
  wire [3:2]\NLW_uSum_reg[31]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_uSum_reg[31]_i_1_O_UNCONNECTED ;

  LUT3 #(
    .INIT(8'hBA)) 
    \FSM_onehot_state[0]_i_1 
       (.I0(bram_we),
        .I1(peak_amp_rdy),
        .I2(\FSM_onehot_state_reg_n_0_[0] ),
        .O(\FSM_onehot_state[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state[1]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[0] ),
        .I1(peak_amp_rdy),
        .O(\FSM_onehot_state[1]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "st3_increment:0100,st4_store:1000,st1_idle:0001,st2_read:0010" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[0]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[0] ),
        .S(bram_rst));
  (* FSM_ENCODED_STATES = "st3_increment:0100,st4_store:1000,st1_idle:0001,st2_read:0010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[1]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[1] ),
        .R(bram_rst));
  (* FSM_ENCODED_STATES = "st3_increment:0100,st4_store:1000,st1_idle:0001,st2_read:0010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg_n_0_[1] ),
        .Q(inc),
        .R(bram_rst));
  (* FSM_ENCODED_STATES = "st3_increment:0100,st4_store:1000,st1_idle:0001,st2_read:0010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(inc),
        .Q(bram_we),
        .R(bram_rst));
  LUT1 #(
    .INIT(2'h1)) 
    bram_rst_INST_0
       (.I0(rstn),
        .O(bram_rst));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[0] 
       (.CLR(1'b0),
        .D(plusOp[0]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[0]));
  LUT1 #(
    .INIT(2'h1)) 
    \uSum_reg[0]_i_1 
       (.I0(bram_dout[0]),
        .O(plusOp[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[10] 
       (.CLR(1'b0),
        .D(plusOp[10]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[11] 
       (.CLR(1'b0),
        .D(plusOp[11]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[12] 
       (.CLR(1'b0),
        .D(plusOp[12]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[12]));
  CARRY4 \uSum_reg[12]_i_1 
       (.CI(\uSum_reg[8]_i_1_n_0 ),
        .CO({\uSum_reg[12]_i_1_n_0 ,\uSum_reg[12]_i_1_n_1 ,\uSum_reg[12]_i_1_n_2 ,\uSum_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[12:9]),
        .S(bram_dout[12:9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[13] 
       (.CLR(1'b0),
        .D(plusOp[13]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[14] 
       (.CLR(1'b0),
        .D(plusOp[14]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[15] 
       (.CLR(1'b0),
        .D(plusOp[15]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[16] 
       (.CLR(1'b0),
        .D(plusOp[16]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[16]));
  CARRY4 \uSum_reg[16]_i_1 
       (.CI(\uSum_reg[12]_i_1_n_0 ),
        .CO({\uSum_reg[16]_i_1_n_0 ,\uSum_reg[16]_i_1_n_1 ,\uSum_reg[16]_i_1_n_2 ,\uSum_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[16:13]),
        .S(bram_dout[16:13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[17] 
       (.CLR(1'b0),
        .D(plusOp[17]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[18] 
       (.CLR(1'b0),
        .D(plusOp[18]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[19] 
       (.CLR(1'b0),
        .D(plusOp[19]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[1] 
       (.CLR(1'b0),
        .D(plusOp[1]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[20] 
       (.CLR(1'b0),
        .D(plusOp[20]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[20]));
  CARRY4 \uSum_reg[20]_i_1 
       (.CI(\uSum_reg[16]_i_1_n_0 ),
        .CO({\uSum_reg[20]_i_1_n_0 ,\uSum_reg[20]_i_1_n_1 ,\uSum_reg[20]_i_1_n_2 ,\uSum_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[20:17]),
        .S(bram_dout[20:17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[21] 
       (.CLR(1'b0),
        .D(plusOp[21]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[22] 
       (.CLR(1'b0),
        .D(plusOp[22]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[23] 
       (.CLR(1'b0),
        .D(plusOp[23]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[24] 
       (.CLR(1'b0),
        .D(plusOp[24]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[24]));
  CARRY4 \uSum_reg[24]_i_1 
       (.CI(\uSum_reg[20]_i_1_n_0 ),
        .CO({\uSum_reg[24]_i_1_n_0 ,\uSum_reg[24]_i_1_n_1 ,\uSum_reg[24]_i_1_n_2 ,\uSum_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[24:21]),
        .S(bram_dout[24:21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[25] 
       (.CLR(1'b0),
        .D(plusOp[25]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[26] 
       (.CLR(1'b0),
        .D(plusOp[26]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[27] 
       (.CLR(1'b0),
        .D(plusOp[27]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[28] 
       (.CLR(1'b0),
        .D(plusOp[28]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[28]));
  CARRY4 \uSum_reg[28]_i_1 
       (.CI(\uSum_reg[24]_i_1_n_0 ),
        .CO({\uSum_reg[28]_i_1_n_0 ,\uSum_reg[28]_i_1_n_1 ,\uSum_reg[28]_i_1_n_2 ,\uSum_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[28:25]),
        .S(bram_dout[28:25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[29] 
       (.CLR(1'b0),
        .D(plusOp[29]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[2] 
       (.CLR(1'b0),
        .D(plusOp[2]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[30] 
       (.CLR(1'b0),
        .D(plusOp[30]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[31] 
       (.CLR(1'b0),
        .D(plusOp[31]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[31]));
  CARRY4 \uSum_reg[31]_i_1 
       (.CI(\uSum_reg[28]_i_1_n_0 ),
        .CO({\NLW_uSum_reg[31]_i_1_CO_UNCONNECTED [3:2],\uSum_reg[31]_i_1_n_2 ,\uSum_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_uSum_reg[31]_i_1_O_UNCONNECTED [3],plusOp[31:29]}),
        .S({1'b0,bram_dout[31:29]}));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[3] 
       (.CLR(1'b0),
        .D(plusOp[3]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[4] 
       (.CLR(1'b0),
        .D(plusOp[4]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[4]));
  CARRY4 \uSum_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\uSum_reg[4]_i_1_n_0 ,\uSum_reg[4]_i_1_n_1 ,\uSum_reg[4]_i_1_n_2 ,\uSum_reg[4]_i_1_n_3 }),
        .CYINIT(bram_dout[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[4:1]),
        .S(bram_dout[4:1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[5] 
       (.CLR(1'b0),
        .D(plusOp[5]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[6] 
       (.CLR(1'b0),
        .D(plusOp[6]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[7] 
       (.CLR(1'b0),
        .D(plusOp[7]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[8] 
       (.CLR(1'b0),
        .D(plusOp[8]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[8]));
  CARRY4 \uSum_reg[8]_i_1 
       (.CI(\uSum_reg[4]_i_1_n_0 ),
        .CO({\uSum_reg[8]_i_1_n_0 ,\uSum_reg[8]_i_1_n_1 ,\uSum_reg[8]_i_1_n_2 ,\uSum_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(plusOp[8:5]),
        .S(bram_dout[8:5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \uSum_reg[9] 
       (.CLR(1'b0),
        .D(plusOp[9]),
        .G(inc),
        .GE(1'b1),
        .Q(bram_din[9]));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_bram_incr_0_0,bram_incr,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "bram_incr,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (bram_addr,
    bram_clk,
    bram_en,
    bram_dout,
    bram_rst,
    bram_we,
    bram_din,
    peak_amp,
    peak_amp_rdy,
    rstn,
    clk,
    segment);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MASTER_TYPE BRAM_CTRL, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, READ_LATENCY 1" *) output [13:0]bram_addr;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) output bram_clk;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB EN" *) output bram_en;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) input [31:0]bram_dout;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB RST" *) output bram_rst;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) output bram_we;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) output [31:0]bram_din;
  input [15:0]peak_amp;
  input peak_amp_rdy;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 rstn RST" *) (* x_interface_parameter = "XIL_INTERFACENAME rstn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input rstn;
  input clk;
  input segment;

  wire \<const0> ;
  wire \<const1> ;
  wire [31:0]bram_din;
  wire [31:0]bram_dout;
  wire bram_rst;
  wire bram_we;
  wire clk;
  wire [15:0]peak_amp;
  wire peak_amp_rdy;
  wire rstn;
  wire segment;

  assign bram_addr[13] = segment;
  assign bram_addr[12:2] = peak_amp[14:4];
  assign bram_addr[1] = \<const0> ;
  assign bram_addr[0] = \<const0> ;
  assign bram_clk = clk;
  assign bram_en = \<const1> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bram_incr U0
       (.bram_din(bram_din),
        .bram_dout(bram_dout),
        .bram_rst(bram_rst),
        .bram_we(bram_we),
        .clk(clk),
        .peak_amp_rdy(peak_amp_rdy),
        .rstn(rstn));
  VCC VCC
       (.P(\<const1> ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
