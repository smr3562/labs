// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 17:49:20 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_blr_0_0_sim_netlist.v
// Design      : design_1_ip_blr_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blr
   (y,
    clk,
    x,
    Q,
    resetn,
    en1_carry__0_0,
    \en1_inferred__0/i__carry__0_0 );
  output [15:0]y;
  input clk;
  input [15:0]x;
  input [4:0]Q;
  input resetn;
  input [15:0]en1_carry__0_0;
  input [15:0]\en1_inferred__0/i__carry__0_0 ;

  wire [4:0]Q;
  wire \acc32[0]_i_2_n_0 ;
  wire \acc32[0]_i_3_n_0 ;
  wire \acc32[0]_i_4_n_0 ;
  wire \acc32[0]_i_5_n_0 ;
  wire \acc32[12]_i_2_n_0 ;
  wire \acc32[12]_i_3_n_0 ;
  wire \acc32[12]_i_4_n_0 ;
  wire \acc32[12]_i_5_n_0 ;
  wire \acc32[16]_i_2_n_0 ;
  wire \acc32[16]_i_3_n_0 ;
  wire \acc32[16]_i_4_n_0 ;
  wire \acc32[16]_i_5_n_0 ;
  wire \acc32[20]_i_2_n_0 ;
  wire \acc32[20]_i_3_n_0 ;
  wire \acc32[20]_i_4_n_0 ;
  wire \acc32[20]_i_5_n_0 ;
  wire \acc32[24]_i_2_n_0 ;
  wire \acc32[24]_i_3_n_0 ;
  wire \acc32[24]_i_4_n_0 ;
  wire \acc32[24]_i_5_n_0 ;
  wire \acc32[28]_i_2_n_0 ;
  wire \acc32[28]_i_3_n_0 ;
  wire \acc32[28]_i_4_n_0 ;
  wire \acc32[28]_i_5_n_0 ;
  wire \acc32[4]_i_2_n_0 ;
  wire \acc32[4]_i_3_n_0 ;
  wire \acc32[4]_i_4_n_0 ;
  wire \acc32[4]_i_5_n_0 ;
  wire \acc32[8]_i_2_n_0 ;
  wire \acc32[8]_i_3_n_0 ;
  wire \acc32[8]_i_4_n_0 ;
  wire \acc32[8]_i_5_n_0 ;
  wire [31:0]acc32_reg;
  wire \acc32_reg[0]_i_1_n_0 ;
  wire \acc32_reg[0]_i_1_n_1 ;
  wire \acc32_reg[0]_i_1_n_2 ;
  wire \acc32_reg[0]_i_1_n_3 ;
  wire \acc32_reg[0]_i_1_n_4 ;
  wire \acc32_reg[0]_i_1_n_5 ;
  wire \acc32_reg[0]_i_1_n_6 ;
  wire \acc32_reg[0]_i_1_n_7 ;
  wire \acc32_reg[12]_i_1_n_0 ;
  wire \acc32_reg[12]_i_1_n_1 ;
  wire \acc32_reg[12]_i_1_n_2 ;
  wire \acc32_reg[12]_i_1_n_3 ;
  wire \acc32_reg[12]_i_1_n_4 ;
  wire \acc32_reg[12]_i_1_n_5 ;
  wire \acc32_reg[12]_i_1_n_6 ;
  wire \acc32_reg[12]_i_1_n_7 ;
  wire \acc32_reg[16]_i_1_n_0 ;
  wire \acc32_reg[16]_i_1_n_1 ;
  wire \acc32_reg[16]_i_1_n_2 ;
  wire \acc32_reg[16]_i_1_n_3 ;
  wire \acc32_reg[16]_i_1_n_4 ;
  wire \acc32_reg[16]_i_1_n_5 ;
  wire \acc32_reg[16]_i_1_n_6 ;
  wire \acc32_reg[16]_i_1_n_7 ;
  wire \acc32_reg[20]_i_1_n_0 ;
  wire \acc32_reg[20]_i_1_n_1 ;
  wire \acc32_reg[20]_i_1_n_2 ;
  wire \acc32_reg[20]_i_1_n_3 ;
  wire \acc32_reg[20]_i_1_n_4 ;
  wire \acc32_reg[20]_i_1_n_5 ;
  wire \acc32_reg[20]_i_1_n_6 ;
  wire \acc32_reg[20]_i_1_n_7 ;
  wire \acc32_reg[24]_i_1_n_0 ;
  wire \acc32_reg[24]_i_1_n_1 ;
  wire \acc32_reg[24]_i_1_n_2 ;
  wire \acc32_reg[24]_i_1_n_3 ;
  wire \acc32_reg[24]_i_1_n_4 ;
  wire \acc32_reg[24]_i_1_n_5 ;
  wire \acc32_reg[24]_i_1_n_6 ;
  wire \acc32_reg[24]_i_1_n_7 ;
  wire \acc32_reg[28]_i_1_n_1 ;
  wire \acc32_reg[28]_i_1_n_2 ;
  wire \acc32_reg[28]_i_1_n_3 ;
  wire \acc32_reg[28]_i_1_n_4 ;
  wire \acc32_reg[28]_i_1_n_5 ;
  wire \acc32_reg[28]_i_1_n_6 ;
  wire \acc32_reg[28]_i_1_n_7 ;
  wire \acc32_reg[4]_i_1_n_0 ;
  wire \acc32_reg[4]_i_1_n_1 ;
  wire \acc32_reg[4]_i_1_n_2 ;
  wire \acc32_reg[4]_i_1_n_3 ;
  wire \acc32_reg[4]_i_1_n_4 ;
  wire \acc32_reg[4]_i_1_n_5 ;
  wire \acc32_reg[4]_i_1_n_6 ;
  wire \acc32_reg[4]_i_1_n_7 ;
  wire \acc32_reg[8]_i_1_n_0 ;
  wire \acc32_reg[8]_i_1_n_1 ;
  wire \acc32_reg[8]_i_1_n_2 ;
  wire \acc32_reg[8]_i_1_n_3 ;
  wire \acc32_reg[8]_i_1_n_4 ;
  wire \acc32_reg[8]_i_1_n_5 ;
  wire \acc32_reg[8]_i_1_n_6 ;
  wire \acc32_reg[8]_i_1_n_7 ;
  wire clk;
  wire delay_line_inst_n_0;
  wire delay_line_inst_n_1;
  wire delay_line_inst_n_10;
  wire delay_line_inst_n_11;
  wire delay_line_inst_n_12;
  wire delay_line_inst_n_13;
  wire delay_line_inst_n_14;
  wire delay_line_inst_n_15;
  wire delay_line_inst_n_16;
  wire delay_line_inst_n_17;
  wire delay_line_inst_n_18;
  wire delay_line_inst_n_19;
  wire delay_line_inst_n_2;
  wire delay_line_inst_n_20;
  wire delay_line_inst_n_21;
  wire delay_line_inst_n_22;
  wire delay_line_inst_n_23;
  wire delay_line_inst_n_24;
  wire delay_line_inst_n_25;
  wire delay_line_inst_n_26;
  wire delay_line_inst_n_27;
  wire delay_line_inst_n_28;
  wire delay_line_inst_n_29;
  wire delay_line_inst_n_3;
  wire delay_line_inst_n_30;
  wire delay_line_inst_n_31;
  wire delay_line_inst_n_4;
  wire delay_line_inst_n_5;
  wire delay_line_inst_n_6;
  wire delay_line_inst_n_7;
  wire delay_line_inst_n_8;
  wire delay_line_inst_n_9;
  wire en;
  wire en0;
  wire en1;
  wire en10_in;
  wire [15:0]en1_carry__0_0;
  wire en1_carry__0_i_1_n_0;
  wire en1_carry__0_i_2_n_0;
  wire en1_carry__0_i_3_n_0;
  wire en1_carry__0_i_4_n_0;
  wire en1_carry__0_i_5_n_0;
  wire en1_carry__0_i_6_n_0;
  wire en1_carry__0_i_7_n_0;
  wire en1_carry__0_i_8_n_0;
  wire en1_carry__0_n_1;
  wire en1_carry__0_n_2;
  wire en1_carry__0_n_3;
  wire en1_carry_i_1_n_0;
  wire en1_carry_i_2_n_0;
  wire en1_carry_i_3_n_0;
  wire en1_carry_i_4_n_0;
  wire en1_carry_i_5_n_0;
  wire en1_carry_i_6_n_0;
  wire en1_carry_i_7_n_0;
  wire en1_carry_i_8_n_0;
  wire en1_carry_n_0;
  wire en1_carry_n_1;
  wire en1_carry_n_2;
  wire en1_carry_n_3;
  wire [15:0]\en1_inferred__0/i__carry__0_0 ;
  wire \en1_inferred__0/i__carry__0_n_1 ;
  wire \en1_inferred__0/i__carry__0_n_2 ;
  wire \en1_inferred__0/i__carry__0_n_3 ;
  wire \en1_inferred__0/i__carry_n_0 ;
  wire \en1_inferred__0/i__carry_n_1 ;
  wire \en1_inferred__0/i__carry_n_2 ;
  wire \en1_inferred__0/i__carry_n_3 ;
  wire en_i_2_n_0;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__0_i_6_n_0;
  wire i__carry__0_i_7_n_0;
  wire i__carry__0_i_8_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire p_0_in;
  wire p_1_in;
  wire [31:0]p_2_in;
  wire resetn;
  wire result3_carry__0_i_10_n_0;
  wire result3_carry__0_i_1_n_0;
  wire result3_carry__0_i_2_n_0;
  wire result3_carry__0_i_3_n_0;
  wire result3_carry__0_i_4_n_0;
  wire result3_carry__0_i_5_n_0;
  wire result3_carry__0_i_6_n_0;
  wire result3_carry__0_i_7_n_0;
  wire result3_carry__0_i_8_n_0;
  wire result3_carry__0_i_9_n_0;
  wire result3_carry__0_n_0;
  wire result3_carry__0_n_1;
  wire result3_carry__0_n_2;
  wire result3_carry__0_n_3;
  wire result3_carry__0_n_4;
  wire result3_carry__0_n_5;
  wire result3_carry__0_n_6;
  wire result3_carry__0_n_7;
  wire result3_carry__1_i_10_n_0;
  wire result3_carry__1_i_1_n_0;
  wire result3_carry__1_i_2_n_0;
  wire result3_carry__1_i_3_n_0;
  wire result3_carry__1_i_4_n_0;
  wire result3_carry__1_i_5_n_0;
  wire result3_carry__1_i_6_n_0;
  wire result3_carry__1_i_7_n_0;
  wire result3_carry__1_i_8_n_0;
  wire result3_carry__1_i_9_n_0;
  wire result3_carry__1_n_0;
  wire result3_carry__1_n_1;
  wire result3_carry__1_n_2;
  wire result3_carry__1_n_3;
  wire result3_carry__1_n_4;
  wire result3_carry__1_n_5;
  wire result3_carry__1_n_6;
  wire result3_carry__1_n_7;
  wire result3_carry__2_i_10_n_0;
  wire result3_carry__2_i_11_n_0;
  wire result3_carry__2_i_2_n_0;
  wire result3_carry__2_i_3_n_0;
  wire result3_carry__2_i_4_n_0;
  wire result3_carry__2_i_5_n_0;
  wire result3_carry__2_i_6_n_0;
  wire result3_carry__2_i_7_n_0;
  wire result3_carry__2_i_8_n_0;
  wire result3_carry__2_i_9_n_0;
  wire result3_carry__2_n_0;
  wire result3_carry__2_n_1;
  wire result3_carry__2_n_2;
  wire result3_carry__2_n_3;
  wire result3_carry__2_n_5;
  wire result3_carry__2_n_6;
  wire result3_carry__2_n_7;
  wire result3_carry__3_i_13_n_0;
  wire result3_carry__3_i_14_n_0;
  wire result3_carry__3_i_15_n_0;
  wire result3_carry__3_i_16_n_0;
  wire result3_carry__3_i_1_n_0;
  wire result3_carry__3_i_2_n_0;
  wire result3_carry__3_i_3_n_0;
  wire result3_carry__3_i_4_n_0;
  wire result3_carry__3_i_5_n_0;
  wire result3_carry__3_i_6_n_0;
  wire result3_carry__3_i_7_n_0;
  wire result3_carry__3_i_8_n_0;
  wire result3_carry__3_n_0;
  wire result3_carry__3_n_1;
  wire result3_carry__3_n_2;
  wire result3_carry__3_n_3;
  wire result3_carry__4_i_13_n_0;
  wire result3_carry__4_i_14_n_0;
  wire result3_carry__4_i_15_n_0;
  wire result3_carry__4_i_1_n_0;
  wire result3_carry__4_i_2_n_0;
  wire result3_carry__4_i_3_n_0;
  wire result3_carry__4_i_4_n_0;
  wire result3_carry__4_i_5_n_0;
  wire result3_carry__4_i_6_n_0;
  wire result3_carry__4_i_7_n_0;
  wire result3_carry__4_i_8_n_0;
  wire result3_carry__4_n_0;
  wire result3_carry__4_n_1;
  wire result3_carry__4_n_2;
  wire result3_carry__4_n_3;
  wire result3_carry__5_i_10_n_0;
  wire result3_carry__5_i_11_n_0;
  wire result3_carry__5_i_14_n_0;
  wire result3_carry__5_i_15_n_0;
  wire result3_carry__5_i_1_n_0;
  wire result3_carry__5_i_2_n_0;
  wire result3_carry__5_i_3_n_0;
  wire result3_carry__5_i_4_n_0;
  wire result3_carry__5_i_5_n_0;
  wire result3_carry__5_i_6_n_0;
  wire result3_carry__5_i_7_n_0;
  wire result3_carry__5_i_8_n_0;
  wire result3_carry__5_i_9_n_0;
  wire result3_carry__5_n_0;
  wire result3_carry__5_n_1;
  wire result3_carry__5_n_2;
  wire result3_carry__5_n_3;
  wire result3_carry__6_i_1_n_0;
  wire result3_carry__6_i_2_n_0;
  wire result3_carry__6_i_3_n_0;
  wire result3_carry__6_i_4_n_0;
  wire result3_carry__6_i_5_n_0;
  wire result3_carry__6_i_6_n_0;
  wire result3_carry__6_i_7_n_0;
  wire result3_carry__6_i_8_n_0;
  wire result3_carry__6_i_9_n_0;
  wire result3_carry__6_n_1;
  wire result3_carry__6_n_2;
  wire result3_carry__6_n_3;
  wire result3_carry_i_10_n_0;
  wire result3_carry_i_11_n_0;
  wire result3_carry_i_12_n_0;
  wire result3_carry_i_13_n_0;
  wire result3_carry_i_1_n_0;
  wire result3_carry_i_2_n_0;
  wire result3_carry_i_3_n_0;
  wire result3_carry_i_4_n_0;
  wire result3_carry_i_5_n_0;
  wire result3_carry_i_6_n_0;
  wire result3_carry_i_7_n_0;
  wire result3_carry_i_8_n_0;
  wire result3_carry_i_9_n_0;
  wire result3_carry_n_0;
  wire result3_carry_n_1;
  wire result3_carry_n_2;
  wire result3_carry_n_3;
  wire result3_carry_n_4;
  wire result3_carry_n_5;
  wire result3_carry_n_6;
  wire result3_carry_n_7;
  wire \result_reg[0]_C_n_0 ;
  wire \result_reg[0]_LDC_i_1_n_0 ;
  wire \result_reg[0]_LDC_i_2_n_0 ;
  wire \result_reg[0]_LDC_n_0 ;
  wire \result_reg[0]_P_n_0 ;
  wire \result_reg[10]_C_n_0 ;
  wire \result_reg[10]_LDC_i_1_n_0 ;
  wire \result_reg[10]_LDC_i_2_n_0 ;
  wire \result_reg[10]_LDC_n_0 ;
  wire \result_reg[10]_P_n_0 ;
  wire \result_reg[11]_C_n_0 ;
  wire \result_reg[11]_LDC_i_1_n_0 ;
  wire \result_reg[11]_LDC_i_2_n_0 ;
  wire \result_reg[11]_LDC_n_0 ;
  wire \result_reg[11]_P_n_0 ;
  wire \result_reg[12]_C_n_0 ;
  wire \result_reg[12]_LDC_i_1_n_0 ;
  wire \result_reg[12]_LDC_i_2_n_0 ;
  wire \result_reg[12]_LDC_n_0 ;
  wire \result_reg[12]_P_n_0 ;
  wire \result_reg[13]_C_n_0 ;
  wire \result_reg[13]_LDC_i_1_n_0 ;
  wire \result_reg[13]_LDC_i_2_n_0 ;
  wire \result_reg[13]_LDC_n_0 ;
  wire \result_reg[13]_P_n_0 ;
  wire \result_reg[14]_C_n_0 ;
  wire \result_reg[14]_LDC_i_1_n_0 ;
  wire \result_reg[14]_LDC_i_2_n_0 ;
  wire \result_reg[14]_LDC_n_0 ;
  wire \result_reg[14]_P_n_0 ;
  wire \result_reg[1]_C_n_0 ;
  wire \result_reg[1]_LDC_i_1_n_0 ;
  wire \result_reg[1]_LDC_i_2_n_0 ;
  wire \result_reg[1]_LDC_n_0 ;
  wire \result_reg[1]_P_n_0 ;
  wire \result_reg[2]_C_n_0 ;
  wire \result_reg[2]_LDC_i_1_n_0 ;
  wire \result_reg[2]_LDC_i_2_n_0 ;
  wire \result_reg[2]_LDC_n_0 ;
  wire \result_reg[2]_P_n_0 ;
  wire \result_reg[31]_C_n_0 ;
  wire \result_reg[31]_LDC_i_1_n_0 ;
  wire \result_reg[31]_LDC_i_2_n_0 ;
  wire \result_reg[31]_LDC_n_0 ;
  wire \result_reg[31]_P_n_0 ;
  wire \result_reg[3]_C_n_0 ;
  wire \result_reg[3]_LDC_i_1_n_0 ;
  wire \result_reg[3]_LDC_i_2_n_0 ;
  wire \result_reg[3]_LDC_n_0 ;
  wire \result_reg[3]_P_n_0 ;
  wire \result_reg[4]_C_n_0 ;
  wire \result_reg[4]_LDC_i_1_n_0 ;
  wire \result_reg[4]_LDC_i_2_n_0 ;
  wire \result_reg[4]_LDC_n_0 ;
  wire \result_reg[4]_P_n_0 ;
  wire \result_reg[5]_C_n_0 ;
  wire \result_reg[5]_LDC_i_1_n_0 ;
  wire \result_reg[5]_LDC_i_2_n_0 ;
  wire \result_reg[5]_LDC_n_0 ;
  wire \result_reg[5]_P_n_0 ;
  wire \result_reg[6]_C_n_0 ;
  wire \result_reg[6]_LDC_i_1_n_0 ;
  wire \result_reg[6]_LDC_i_2_n_0 ;
  wire \result_reg[6]_LDC_n_0 ;
  wire \result_reg[6]_P_n_0 ;
  wire \result_reg[7]_C_n_0 ;
  wire \result_reg[7]_LDC_i_1_n_0 ;
  wire \result_reg[7]_LDC_i_2_n_0 ;
  wire \result_reg[7]_LDC_n_0 ;
  wire \result_reg[7]_P_n_0 ;
  wire \result_reg[8]_C_n_0 ;
  wire \result_reg[8]_LDC_i_1_n_0 ;
  wire \result_reg[8]_LDC_i_2_n_0 ;
  wire \result_reg[8]_LDC_n_0 ;
  wire \result_reg[8]_P_n_0 ;
  wire \result_reg[9]_C_n_0 ;
  wire \result_reg[9]_LDC_i_1_n_0 ;
  wire \result_reg[9]_LDC_i_2_n_0 ;
  wire \result_reg[9]_LDC_n_0 ;
  wire \result_reg[9]_P_n_0 ;
  wire [31:0]u32;
  wire [31:0]u320;
  wire u320_carry__0_n_0;
  wire u320_carry__0_n_1;
  wire u320_carry__0_n_2;
  wire u320_carry__0_n_3;
  wire u320_carry__1_n_0;
  wire u320_carry__1_n_1;
  wire u320_carry__1_n_2;
  wire u320_carry__1_n_3;
  wire u320_carry__2_n_0;
  wire u320_carry__2_n_1;
  wire u320_carry__2_n_2;
  wire u320_carry__2_n_3;
  wire u320_carry__3_n_0;
  wire u320_carry__3_n_1;
  wire u320_carry__3_n_2;
  wire u320_carry__3_n_3;
  wire u320_carry__4_n_0;
  wire u320_carry__4_n_1;
  wire u320_carry__4_n_2;
  wire u320_carry__4_n_3;
  wire u320_carry__5_n_0;
  wire u320_carry__5_n_1;
  wire u320_carry__5_n_2;
  wire u320_carry__5_n_3;
  wire u320_carry__6_n_1;
  wire u320_carry__6_n_2;
  wire u320_carry__6_n_3;
  wire u320_carry_n_0;
  wire u320_carry_n_1;
  wire u320_carry_n_2;
  wire u320_carry_n_3;
  wire [25:15]v32;
  wire [15:0]x;
  wire [15:0]y;
  wire [3:3]\NLW_acc32_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:0]NLW_en1_carry_O_UNCONNECTED;
  wire [3:0]NLW_en1_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_en1_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_en1_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]NLW_result3_carry__3_O_UNCONNECTED;
  wire [3:0]NLW_result3_carry__4_O_UNCONNECTED;
  wire [3:0]NLW_result3_carry__5_O_UNCONNECTED;
  wire [3:3]NLW_result3_carry__6_CO_UNCONNECTED;
  wire [2:0]NLW_result3_carry__6_O_UNCONNECTED;
  wire [3:3]NLW_u320_carry__6_CO_UNCONNECTED;

  LUT2 #(
    .INIT(4'h6)) 
    \acc32[0]_i_2 
       (.I0(x[3]),
        .I1(acc32_reg[3]),
        .O(\acc32[0]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[0]_i_3 
       (.I0(x[2]),
        .I1(acc32_reg[2]),
        .O(\acc32[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[0]_i_4 
       (.I0(x[1]),
        .I1(acc32_reg[1]),
        .O(\acc32[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[0]_i_5 
       (.I0(x[0]),
        .I1(acc32_reg[0]),
        .O(\acc32[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[12]_i_2 
       (.I0(x[15]),
        .I1(acc32_reg[15]),
        .O(\acc32[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[12]_i_3 
       (.I0(x[14]),
        .I1(acc32_reg[14]),
        .O(\acc32[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[12]_i_4 
       (.I0(x[13]),
        .I1(acc32_reg[13]),
        .O(\acc32[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[12]_i_5 
       (.I0(x[12]),
        .I1(acc32_reg[12]),
        .O(\acc32[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[16]_i_2 
       (.I0(x[15]),
        .I1(acc32_reg[19]),
        .O(\acc32[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[16]_i_3 
       (.I0(x[15]),
        .I1(acc32_reg[18]),
        .O(\acc32[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[16]_i_4 
       (.I0(x[15]),
        .I1(acc32_reg[17]),
        .O(\acc32[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[16]_i_5 
       (.I0(x[15]),
        .I1(acc32_reg[16]),
        .O(\acc32[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[20]_i_2 
       (.I0(x[15]),
        .I1(acc32_reg[23]),
        .O(\acc32[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[20]_i_3 
       (.I0(x[15]),
        .I1(acc32_reg[22]),
        .O(\acc32[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[20]_i_4 
       (.I0(x[15]),
        .I1(acc32_reg[21]),
        .O(\acc32[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[20]_i_5 
       (.I0(x[15]),
        .I1(acc32_reg[20]),
        .O(\acc32[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[24]_i_2 
       (.I0(x[15]),
        .I1(acc32_reg[27]),
        .O(\acc32[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[24]_i_3 
       (.I0(x[15]),
        .I1(acc32_reg[26]),
        .O(\acc32[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[24]_i_4 
       (.I0(x[15]),
        .I1(acc32_reg[25]),
        .O(\acc32[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[24]_i_5 
       (.I0(x[15]),
        .I1(acc32_reg[24]),
        .O(\acc32[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[28]_i_2 
       (.I0(x[15]),
        .I1(acc32_reg[31]),
        .O(\acc32[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[28]_i_3 
       (.I0(x[15]),
        .I1(acc32_reg[30]),
        .O(\acc32[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[28]_i_4 
       (.I0(x[15]),
        .I1(acc32_reg[29]),
        .O(\acc32[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[28]_i_5 
       (.I0(x[15]),
        .I1(acc32_reg[28]),
        .O(\acc32[28]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[4]_i_2 
       (.I0(x[7]),
        .I1(acc32_reg[7]),
        .O(\acc32[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[4]_i_3 
       (.I0(x[6]),
        .I1(acc32_reg[6]),
        .O(\acc32[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[4]_i_4 
       (.I0(x[5]),
        .I1(acc32_reg[5]),
        .O(\acc32[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[4]_i_5 
       (.I0(x[4]),
        .I1(acc32_reg[4]),
        .O(\acc32[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[8]_i_2 
       (.I0(x[11]),
        .I1(acc32_reg[11]),
        .O(\acc32[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[8]_i_3 
       (.I0(x[10]),
        .I1(acc32_reg[10]),
        .O(\acc32[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[8]_i_4 
       (.I0(x[9]),
        .I1(acc32_reg[9]),
        .O(\acc32[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[8]_i_5 
       (.I0(x[8]),
        .I1(acc32_reg[8]),
        .O(\acc32[8]_i_5_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[0] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[0]_i_1_n_7 ),
        .Q(acc32_reg[0]));
  CARRY4 \acc32_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\acc32_reg[0]_i_1_n_0 ,\acc32_reg[0]_i_1_n_1 ,\acc32_reg[0]_i_1_n_2 ,\acc32_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(x[3:0]),
        .O({\acc32_reg[0]_i_1_n_4 ,\acc32_reg[0]_i_1_n_5 ,\acc32_reg[0]_i_1_n_6 ,\acc32_reg[0]_i_1_n_7 }),
        .S({\acc32[0]_i_2_n_0 ,\acc32[0]_i_3_n_0 ,\acc32[0]_i_4_n_0 ,\acc32[0]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[10] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[8]_i_1_n_5 ),
        .Q(acc32_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[11] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[8]_i_1_n_4 ),
        .Q(acc32_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[12] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[12]_i_1_n_7 ),
        .Q(acc32_reg[12]));
  CARRY4 \acc32_reg[12]_i_1 
       (.CI(\acc32_reg[8]_i_1_n_0 ),
        .CO({\acc32_reg[12]_i_1_n_0 ,\acc32_reg[12]_i_1_n_1 ,\acc32_reg[12]_i_1_n_2 ,\acc32_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(x[15:12]),
        .O({\acc32_reg[12]_i_1_n_4 ,\acc32_reg[12]_i_1_n_5 ,\acc32_reg[12]_i_1_n_6 ,\acc32_reg[12]_i_1_n_7 }),
        .S({\acc32[12]_i_2_n_0 ,\acc32[12]_i_3_n_0 ,\acc32[12]_i_4_n_0 ,\acc32[12]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[13] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[12]_i_1_n_6 ),
        .Q(acc32_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[14] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[12]_i_1_n_5 ),
        .Q(acc32_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[15] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[12]_i_1_n_4 ),
        .Q(acc32_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[16] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[16]_i_1_n_7 ),
        .Q(acc32_reg[16]));
  CARRY4 \acc32_reg[16]_i_1 
       (.CI(\acc32_reg[12]_i_1_n_0 ),
        .CO({\acc32_reg[16]_i_1_n_0 ,\acc32_reg[16]_i_1_n_1 ,\acc32_reg[16]_i_1_n_2 ,\acc32_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({x[15],x[15],x[15],x[15]}),
        .O({\acc32_reg[16]_i_1_n_4 ,\acc32_reg[16]_i_1_n_5 ,\acc32_reg[16]_i_1_n_6 ,\acc32_reg[16]_i_1_n_7 }),
        .S({\acc32[16]_i_2_n_0 ,\acc32[16]_i_3_n_0 ,\acc32[16]_i_4_n_0 ,\acc32[16]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[17] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[16]_i_1_n_6 ),
        .Q(acc32_reg[17]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[18] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[16]_i_1_n_5 ),
        .Q(acc32_reg[18]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[19] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[16]_i_1_n_4 ),
        .Q(acc32_reg[19]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[1] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[0]_i_1_n_6 ),
        .Q(acc32_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[20] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[20]_i_1_n_7 ),
        .Q(acc32_reg[20]));
  CARRY4 \acc32_reg[20]_i_1 
       (.CI(\acc32_reg[16]_i_1_n_0 ),
        .CO({\acc32_reg[20]_i_1_n_0 ,\acc32_reg[20]_i_1_n_1 ,\acc32_reg[20]_i_1_n_2 ,\acc32_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({x[15],x[15],x[15],x[15]}),
        .O({\acc32_reg[20]_i_1_n_4 ,\acc32_reg[20]_i_1_n_5 ,\acc32_reg[20]_i_1_n_6 ,\acc32_reg[20]_i_1_n_7 }),
        .S({\acc32[20]_i_2_n_0 ,\acc32[20]_i_3_n_0 ,\acc32[20]_i_4_n_0 ,\acc32[20]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[21] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[20]_i_1_n_6 ),
        .Q(acc32_reg[21]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[22] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[20]_i_1_n_5 ),
        .Q(acc32_reg[22]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[23] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[20]_i_1_n_4 ),
        .Q(acc32_reg[23]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[24] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[24]_i_1_n_7 ),
        .Q(acc32_reg[24]));
  CARRY4 \acc32_reg[24]_i_1 
       (.CI(\acc32_reg[20]_i_1_n_0 ),
        .CO({\acc32_reg[24]_i_1_n_0 ,\acc32_reg[24]_i_1_n_1 ,\acc32_reg[24]_i_1_n_2 ,\acc32_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({x[15],x[15],x[15],x[15]}),
        .O({\acc32_reg[24]_i_1_n_4 ,\acc32_reg[24]_i_1_n_5 ,\acc32_reg[24]_i_1_n_6 ,\acc32_reg[24]_i_1_n_7 }),
        .S({\acc32[24]_i_2_n_0 ,\acc32[24]_i_3_n_0 ,\acc32[24]_i_4_n_0 ,\acc32[24]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[25] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[24]_i_1_n_6 ),
        .Q(acc32_reg[25]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[26] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[24]_i_1_n_5 ),
        .Q(acc32_reg[26]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[27] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[24]_i_1_n_4 ),
        .Q(acc32_reg[27]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[28] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[28]_i_1_n_7 ),
        .Q(acc32_reg[28]));
  CARRY4 \acc32_reg[28]_i_1 
       (.CI(\acc32_reg[24]_i_1_n_0 ),
        .CO({\NLW_acc32_reg[28]_i_1_CO_UNCONNECTED [3],\acc32_reg[28]_i_1_n_1 ,\acc32_reg[28]_i_1_n_2 ,\acc32_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,x[15],x[15],x[15]}),
        .O({\acc32_reg[28]_i_1_n_4 ,\acc32_reg[28]_i_1_n_5 ,\acc32_reg[28]_i_1_n_6 ,\acc32_reg[28]_i_1_n_7 }),
        .S({\acc32[28]_i_2_n_0 ,\acc32[28]_i_3_n_0 ,\acc32[28]_i_4_n_0 ,\acc32[28]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[29] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[28]_i_1_n_6 ),
        .Q(acc32_reg[29]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[2] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[0]_i_1_n_5 ),
        .Q(acc32_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[30] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[28]_i_1_n_5 ),
        .Q(acc32_reg[30]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[31] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[28]_i_1_n_4 ),
        .Q(acc32_reg[31]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[3] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[0]_i_1_n_4 ),
        .Q(acc32_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[4] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[4]_i_1_n_7 ),
        .Q(acc32_reg[4]));
  CARRY4 \acc32_reg[4]_i_1 
       (.CI(\acc32_reg[0]_i_1_n_0 ),
        .CO({\acc32_reg[4]_i_1_n_0 ,\acc32_reg[4]_i_1_n_1 ,\acc32_reg[4]_i_1_n_2 ,\acc32_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(x[7:4]),
        .O({\acc32_reg[4]_i_1_n_4 ,\acc32_reg[4]_i_1_n_5 ,\acc32_reg[4]_i_1_n_6 ,\acc32_reg[4]_i_1_n_7 }),
        .S({\acc32[4]_i_2_n_0 ,\acc32[4]_i_3_n_0 ,\acc32[4]_i_4_n_0 ,\acc32[4]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[5] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[4]_i_1_n_6 ),
        .Q(acc32_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[6] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[4]_i_1_n_5 ),
        .Q(acc32_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[7] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[4]_i_1_n_4 ),
        .Q(acc32_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[8] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[8]_i_1_n_7 ),
        .Q(acc32_reg[8]));
  CARRY4 \acc32_reg[8]_i_1 
       (.CI(\acc32_reg[4]_i_1_n_0 ),
        .CO({\acc32_reg[8]_i_1_n_0 ,\acc32_reg[8]_i_1_n_1 ,\acc32_reg[8]_i_1_n_2 ,\acc32_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(x[11:8]),
        .O({\acc32_reg[8]_i_1_n_4 ,\acc32_reg[8]_i_1_n_5 ,\acc32_reg[8]_i_1_n_6 ,\acc32_reg[8]_i_1_n_7 }),
        .S({\acc32[8]_i_2_n_0 ,\acc32[8]_i_3_n_0 ,\acc32[8]_i_4_n_0 ,\acc32[8]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \acc32_reg[9] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(\acc32_reg[8]_i_1_n_6 ),
        .Q(acc32_reg[9]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_delay_line delay_line_inst
       (.DIADI(acc32_reg),
        .Q(Q[3:0]),
        .S({delay_line_inst_n_0,delay_line_inst_n_1,delay_line_inst_n_2,delay_line_inst_n_3}),
        .\acc32_reg[11] ({delay_line_inst_n_8,delay_line_inst_n_9,delay_line_inst_n_10,delay_line_inst_n_11}),
        .\acc32_reg[15] ({delay_line_inst_n_12,delay_line_inst_n_13,delay_line_inst_n_14,delay_line_inst_n_15}),
        .\acc32_reg[19] ({delay_line_inst_n_16,delay_line_inst_n_17,delay_line_inst_n_18,delay_line_inst_n_19}),
        .\acc32_reg[23] ({delay_line_inst_n_20,delay_line_inst_n_21,delay_line_inst_n_22,delay_line_inst_n_23}),
        .\acc32_reg[27] ({delay_line_inst_n_24,delay_line_inst_n_25,delay_line_inst_n_26,delay_line_inst_n_27}),
        .\acc32_reg[31] ({delay_line_inst_n_28,delay_line_inst_n_29,delay_line_inst_n_30,delay_line_inst_n_31}),
        .\acc32_reg[7] ({delay_line_inst_n_4,delay_line_inst_n_5,delay_line_inst_n_6,delay_line_inst_n_7}),
        .clk(clk),
        .en(en),
        .resetn(resetn));
  CARRY4 en1_carry
       (.CI(1'b0),
        .CO({en1_carry_n_0,en1_carry_n_1,en1_carry_n_2,en1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({en1_carry_i_1_n_0,en1_carry_i_2_n_0,en1_carry_i_3_n_0,en1_carry_i_4_n_0}),
        .O(NLW_en1_carry_O_UNCONNECTED[3:0]),
        .S({en1_carry_i_5_n_0,en1_carry_i_6_n_0,en1_carry_i_7_n_0,en1_carry_i_8_n_0}));
  CARRY4 en1_carry__0
       (.CI(en1_carry_n_0),
        .CO({en1,en1_carry__0_n_1,en1_carry__0_n_2,en1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({en1_carry__0_i_1_n_0,en1_carry__0_i_2_n_0,en1_carry__0_i_3_n_0,en1_carry__0_i_4_n_0}),
        .O(NLW_en1_carry__0_O_UNCONNECTED[3:0]),
        .S({en1_carry__0_i_5_n_0,en1_carry__0_i_6_n_0,en1_carry__0_i_7_n_0,en1_carry__0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    en1_carry__0_i_1
       (.I0(x[14]),
        .I1(en1_carry__0_0[14]),
        .I2(x[15]),
        .I3(en1_carry__0_0[15]),
        .O(en1_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    en1_carry__0_i_2
       (.I0(x[12]),
        .I1(en1_carry__0_0[12]),
        .I2(en1_carry__0_0[13]),
        .I3(x[13]),
        .O(en1_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    en1_carry__0_i_3
       (.I0(x[10]),
        .I1(en1_carry__0_0[10]),
        .I2(en1_carry__0_0[11]),
        .I3(x[11]),
        .O(en1_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    en1_carry__0_i_4
       (.I0(x[8]),
        .I1(en1_carry__0_0[8]),
        .I2(en1_carry__0_0[9]),
        .I3(x[9]),
        .O(en1_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    en1_carry__0_i_5
       (.I0(x[14]),
        .I1(en1_carry__0_0[14]),
        .I2(en1_carry__0_0[15]),
        .I3(x[15]),
        .O(en1_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    en1_carry__0_i_6
       (.I0(x[12]),
        .I1(en1_carry__0_0[12]),
        .I2(x[13]),
        .I3(en1_carry__0_0[13]),
        .O(en1_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    en1_carry__0_i_7
       (.I0(x[10]),
        .I1(en1_carry__0_0[10]),
        .I2(x[11]),
        .I3(en1_carry__0_0[11]),
        .O(en1_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    en1_carry__0_i_8
       (.I0(x[8]),
        .I1(en1_carry__0_0[8]),
        .I2(x[9]),
        .I3(en1_carry__0_0[9]),
        .O(en1_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    en1_carry_i_1
       (.I0(x[6]),
        .I1(en1_carry__0_0[6]),
        .I2(en1_carry__0_0[7]),
        .I3(x[7]),
        .O(en1_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    en1_carry_i_2
       (.I0(x[4]),
        .I1(en1_carry__0_0[4]),
        .I2(en1_carry__0_0[5]),
        .I3(x[5]),
        .O(en1_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    en1_carry_i_3
       (.I0(x[2]),
        .I1(en1_carry__0_0[2]),
        .I2(en1_carry__0_0[3]),
        .I3(x[3]),
        .O(en1_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    en1_carry_i_4
       (.I0(x[0]),
        .I1(en1_carry__0_0[0]),
        .I2(en1_carry__0_0[1]),
        .I3(x[1]),
        .O(en1_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    en1_carry_i_5
       (.I0(x[6]),
        .I1(en1_carry__0_0[6]),
        .I2(x[7]),
        .I3(en1_carry__0_0[7]),
        .O(en1_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    en1_carry_i_6
       (.I0(x[4]),
        .I1(en1_carry__0_0[4]),
        .I2(x[5]),
        .I3(en1_carry__0_0[5]),
        .O(en1_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    en1_carry_i_7
       (.I0(x[2]),
        .I1(en1_carry__0_0[2]),
        .I2(x[3]),
        .I3(en1_carry__0_0[3]),
        .O(en1_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    en1_carry_i_8
       (.I0(x[0]),
        .I1(en1_carry__0_0[0]),
        .I2(x[1]),
        .I3(en1_carry__0_0[1]),
        .O(en1_carry_i_8_n_0));
  CARRY4 \en1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\en1_inferred__0/i__carry_n_0 ,\en1_inferred__0/i__carry_n_1 ,\en1_inferred__0/i__carry_n_2 ,\en1_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_en1_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0}));
  CARRY4 \en1_inferred__0/i__carry__0 
       (.CI(\en1_inferred__0/i__carry_n_0 ),
        .CO({en10_in,\en1_inferred__0/i__carry__0_n_1 ,\en1_inferred__0/i__carry__0_n_2 ,\en1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0}),
        .O(\NLW_en1_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_5_n_0,i__carry__0_i_6_n_0,i__carry__0_i_7_n_0,i__carry__0_i_8_n_0}));
  LUT2 #(
    .INIT(4'h8)) 
    en_i_1
       (.I0(en10_in),
        .I1(en1),
        .O(en0));
  LUT2 #(
    .INIT(4'h7)) 
    en_i_2
       (.I0(resetn),
        .I1(Q[4]),
        .O(en_i_2_n_0));
  FDCE #(
    .INIT(1'b0)) 
    en_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(en_i_2_n_0),
        .D(en0),
        .Q(en));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_1
       (.I0(\en1_inferred__0/i__carry__0_0 [14]),
        .I1(x[14]),
        .I2(\en1_inferred__0/i__carry__0_0 [15]),
        .I3(x[15]),
        .O(i__carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_2
       (.I0(\en1_inferred__0/i__carry__0_0 [12]),
        .I1(x[12]),
        .I2(x[13]),
        .I3(\en1_inferred__0/i__carry__0_0 [13]),
        .O(i__carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_3
       (.I0(\en1_inferred__0/i__carry__0_0 [10]),
        .I1(x[10]),
        .I2(x[11]),
        .I3(\en1_inferred__0/i__carry__0_0 [11]),
        .O(i__carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_4
       (.I0(\en1_inferred__0/i__carry__0_0 [8]),
        .I1(x[8]),
        .I2(x[9]),
        .I3(\en1_inferred__0/i__carry__0_0 [9]),
        .O(i__carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5
       (.I0(\en1_inferred__0/i__carry__0_0 [14]),
        .I1(x[14]),
        .I2(x[15]),
        .I3(\en1_inferred__0/i__carry__0_0 [15]),
        .O(i__carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6
       (.I0(\en1_inferred__0/i__carry__0_0 [12]),
        .I1(x[12]),
        .I2(\en1_inferred__0/i__carry__0_0 [13]),
        .I3(x[13]),
        .O(i__carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_7
       (.I0(\en1_inferred__0/i__carry__0_0 [10]),
        .I1(x[10]),
        .I2(\en1_inferred__0/i__carry__0_0 [11]),
        .I3(x[11]),
        .O(i__carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_8
       (.I0(\en1_inferred__0/i__carry__0_0 [8]),
        .I1(x[8]),
        .I2(\en1_inferred__0/i__carry__0_0 [9]),
        .I3(x[9]),
        .O(i__carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_1
       (.I0(\en1_inferred__0/i__carry__0_0 [6]),
        .I1(x[6]),
        .I2(x[7]),
        .I3(\en1_inferred__0/i__carry__0_0 [7]),
        .O(i__carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_2
       (.I0(\en1_inferred__0/i__carry__0_0 [4]),
        .I1(x[4]),
        .I2(x[5]),
        .I3(\en1_inferred__0/i__carry__0_0 [5]),
        .O(i__carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_3
       (.I0(\en1_inferred__0/i__carry__0_0 [2]),
        .I1(x[2]),
        .I2(x[3]),
        .I3(\en1_inferred__0/i__carry__0_0 [3]),
        .O(i__carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_4
       (.I0(\en1_inferred__0/i__carry__0_0 [0]),
        .I1(x[0]),
        .I2(x[1]),
        .I3(\en1_inferred__0/i__carry__0_0 [1]),
        .O(i__carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5
       (.I0(\en1_inferred__0/i__carry__0_0 [6]),
        .I1(x[6]),
        .I2(\en1_inferred__0/i__carry__0_0 [7]),
        .I3(x[7]),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(\en1_inferred__0/i__carry__0_0 [4]),
        .I1(x[4]),
        .I2(\en1_inferred__0/i__carry__0_0 [5]),
        .I3(x[5]),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(\en1_inferred__0/i__carry__0_0 [2]),
        .I1(x[2]),
        .I2(\en1_inferred__0/i__carry__0_0 [3]),
        .I3(x[3]),
        .O(i__carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(\en1_inferred__0/i__carry__0_0 [0]),
        .I1(x[0]),
        .I2(\en1_inferred__0/i__carry__0_0 [1]),
        .I3(x[1]),
        .O(i__carry_i_8_n_0));
  CARRY4 result3_carry
       (.CI(1'b0),
        .CO({result3_carry_n_0,result3_carry_n_1,result3_carry_n_2,result3_carry_n_3}),
        .CYINIT(1'b1),
        .DI(x[3:0]),
        .O({result3_carry_n_4,result3_carry_n_5,result3_carry_n_6,result3_carry_n_7}),
        .S({result3_carry_i_1_n_0,result3_carry_i_2_n_0,result3_carry_i_3_n_0,result3_carry_i_4_n_0}));
  CARRY4 result3_carry__0
       (.CI(result3_carry_n_0),
        .CO({result3_carry__0_n_0,result3_carry__0_n_1,result3_carry__0_n_2,result3_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(x[7:4]),
        .O({result3_carry__0_n_4,result3_carry__0_n_5,result3_carry__0_n_6,result3_carry__0_n_7}),
        .S({result3_carry__0_i_1_n_0,result3_carry__0_i_2_n_0,result3_carry__0_i_3_n_0,result3_carry__0_i_4_n_0}));
  LUT6 #(
    .INIT(64'hB847B847FF0000FF)) 
    result3_carry__0_i_1
       (.I0(result3_carry__0_i_5_n_0),
        .I1(Q[1]),
        .I2(result3_carry__0_i_6_n_0),
        .I3(x[7]),
        .I4(result3_carry__0_i_7_n_0),
        .I5(Q[0]),
        .O(result3_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_10
       (.I0(u32[19]),
        .I1(u32[11]),
        .I2(Q[2]),
        .I3(u32[15]),
        .I4(Q[3]),
        .I5(u32[7]),
        .O(result3_carry__0_i_10_n_0));
  LUT6 #(
    .INIT(64'hFF00B84700FFB847)) 
    result3_carry__0_i_2
       (.I0(result3_carry__0_i_6_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_5_n_0),
        .I3(x[6]),
        .I4(Q[0]),
        .I5(result3_carry__0_i_7_n_0),
        .O(result3_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'hB847B847FF0000FF)) 
    result3_carry__0_i_3
       (.I0(result3_carry__0_i_6_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_5_n_0),
        .I3(x[5]),
        .I4(result3_carry__0_i_8_n_0),
        .I5(Q[0]),
        .O(result3_carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'hFF00B84700FFB847)) 
    result3_carry__0_i_4
       (.I0(result3_carry_i_5_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_6_n_0),
        .I3(x[4]),
        .I4(Q[0]),
        .I5(result3_carry__0_i_8_n_0),
        .O(result3_carry__0_i_4_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_5
       (.I0(u32[22]),
        .I1(u32[14]),
        .I2(Q[2]),
        .I3(u32[18]),
        .I4(Q[3]),
        .I5(u32[10]),
        .O(result3_carry__0_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_6
       (.I0(u32[20]),
        .I1(u32[12]),
        .I2(Q[2]),
        .I3(u32[16]),
        .I4(Q[3]),
        .I5(u32[8]),
        .O(result3_carry__0_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__0_i_7
       (.I0(result3_carry__0_i_9_n_0),
        .I1(Q[1]),
        .I2(result3_carry__0_i_10_n_0),
        .O(result3_carry__0_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__0_i_8
       (.I0(result3_carry__0_i_10_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_11_n_0),
        .O(result3_carry__0_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_9
       (.I0(u32[21]),
        .I1(u32[13]),
        .I2(Q[2]),
        .I3(u32[17]),
        .I4(Q[3]),
        .I5(u32[9]),
        .O(result3_carry__0_i_9_n_0));
  CARRY4 result3_carry__1
       (.CI(result3_carry__0_n_0),
        .CO({result3_carry__1_n_0,result3_carry__1_n_1,result3_carry__1_n_2,result3_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(x[11:8]),
        .O({result3_carry__1_n_4,result3_carry__1_n_5,result3_carry__1_n_6,result3_carry__1_n_7}),
        .S({result3_carry__1_i_1_n_0,result3_carry__1_i_2_n_0,result3_carry__1_i_3_n_0,result3_carry__1_i_4_n_0}));
  LUT6 #(
    .INIT(64'hB847B847FF0000FF)) 
    result3_carry__1_i_1
       (.I0(result3_carry__1_i_5_n_0),
        .I1(Q[1]),
        .I2(result3_carry__1_i_6_n_0),
        .I3(x[11]),
        .I4(result3_carry__1_i_7_n_0),
        .I5(Q[0]),
        .O(result3_carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__1_i_10
       (.I0(u32[23]),
        .I1(u32[15]),
        .I2(Q[2]),
        .I3(u32[19]),
        .I4(Q[3]),
        .I5(u32[11]),
        .O(result3_carry__1_i_10_n_0));
  LUT6 #(
    .INIT(64'hFF00B84700FFB847)) 
    result3_carry__1_i_2
       (.I0(result3_carry__1_i_6_n_0),
        .I1(Q[1]),
        .I2(result3_carry__0_i_5_n_0),
        .I3(x[10]),
        .I4(Q[0]),
        .I5(result3_carry__1_i_7_n_0),
        .O(result3_carry__1_i_2_n_0));
  LUT6 #(
    .INIT(64'hB847B847FF0000FF)) 
    result3_carry__1_i_3
       (.I0(result3_carry__1_i_6_n_0),
        .I1(Q[1]),
        .I2(result3_carry__0_i_5_n_0),
        .I3(x[9]),
        .I4(result3_carry__1_i_8_n_0),
        .I5(Q[0]),
        .O(result3_carry__1_i_3_n_0));
  LUT6 #(
    .INIT(64'hFF00B84700FFB847)) 
    result3_carry__1_i_4
       (.I0(result3_carry__0_i_5_n_0),
        .I1(Q[1]),
        .I2(result3_carry__0_i_6_n_0),
        .I3(x[8]),
        .I4(Q[0]),
        .I5(result3_carry__1_i_8_n_0),
        .O(result3_carry__1_i_4_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__1_i_5
       (.I0(u32[26]),
        .I1(u32[18]),
        .I2(Q[2]),
        .I3(u32[22]),
        .I4(Q[3]),
        .I5(u32[14]),
        .O(result3_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__1_i_6
       (.I0(u32[24]),
        .I1(u32[16]),
        .I2(Q[2]),
        .I3(u32[20]),
        .I4(Q[3]),
        .I5(u32[12]),
        .O(result3_carry__1_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__1_i_7
       (.I0(result3_carry__1_i_9_n_0),
        .I1(Q[1]),
        .I2(result3_carry__1_i_10_n_0),
        .O(result3_carry__1_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__1_i_8
       (.I0(result3_carry__1_i_10_n_0),
        .I1(Q[1]),
        .I2(result3_carry__0_i_9_n_0),
        .O(result3_carry__1_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__1_i_9
       (.I0(u32[25]),
        .I1(u32[17]),
        .I2(Q[2]),
        .I3(u32[21]),
        .I4(Q[3]),
        .I5(u32[13]),
        .O(result3_carry__1_i_9_n_0));
  CARRY4 result3_carry__2
       (.CI(result3_carry__1_n_0),
        .CO({result3_carry__2_n_0,result3_carry__2_n_1,result3_carry__2_n_2,result3_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({v32[15],x[14:12]}),
        .O({p_1_in,result3_carry__2_n_5,result3_carry__2_n_6,result3_carry__2_n_7}),
        .S({result3_carry__2_i_2_n_0,result3_carry__2_i_3_n_0,result3_carry__2_i_4_n_0,result3_carry__2_i_5_n_0}));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_1
       (.I0(result3_carry__2_i_6_n_0),
        .I1(result3_carry__2_i_7_n_0),
        .I2(Q[0]),
        .I3(result3_carry__2_i_8_n_0),
        .I4(Q[1]),
        .I5(result3_carry__2_i_9_n_0),
        .O(v32[15]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__2_i_10
       (.I0(result3_carry__2_i_8_n_0),
        .I1(Q[1]),
        .I2(result3_carry__2_i_9_n_0),
        .O(result3_carry__2_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__2_i_11
       (.I0(result3_carry__2_i_9_n_0),
        .I1(Q[1]),
        .I2(result3_carry__1_i_9_n_0),
        .O(result3_carry__2_i_11_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__2_i_2
       (.I0(v32[15]),
        .I1(x[15]),
        .O(result3_carry__2_i_2_n_0));
  LUT6 #(
    .INIT(64'hFF00B84700FFB847)) 
    result3_carry__2_i_3
       (.I0(result3_carry__2_i_7_n_0),
        .I1(Q[1]),
        .I2(result3_carry__1_i_5_n_0),
        .I3(x[14]),
        .I4(Q[0]),
        .I5(result3_carry__2_i_10_n_0),
        .O(result3_carry__2_i_3_n_0));
  LUT6 #(
    .INIT(64'hB847B847FF0000FF)) 
    result3_carry__2_i_4
       (.I0(result3_carry__2_i_7_n_0),
        .I1(Q[1]),
        .I2(result3_carry__1_i_5_n_0),
        .I3(x[13]),
        .I4(result3_carry__2_i_11_n_0),
        .I5(Q[0]),
        .O(result3_carry__2_i_4_n_0));
  LUT6 #(
    .INIT(64'hFF00B84700FFB847)) 
    result3_carry__2_i_5
       (.I0(result3_carry__1_i_5_n_0),
        .I1(Q[1]),
        .I2(result3_carry__1_i_6_n_0),
        .I3(x[12]),
        .I4(Q[0]),
        .I5(result3_carry__2_i_11_n_0),
        .O(result3_carry__2_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_6
       (.I0(u32[30]),
        .I1(u32[22]),
        .I2(Q[2]),
        .I3(u32[26]),
        .I4(Q[3]),
        .I5(u32[18]),
        .O(result3_carry__2_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_7
       (.I0(u32[28]),
        .I1(u32[20]),
        .I2(Q[2]),
        .I3(u32[24]),
        .I4(Q[3]),
        .I5(u32[16]),
        .O(result3_carry__2_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_8
       (.I0(u32[29]),
        .I1(u32[21]),
        .I2(Q[2]),
        .I3(u32[25]),
        .I4(Q[3]),
        .I5(u32[17]),
        .O(result3_carry__2_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_9
       (.I0(u32[27]),
        .I1(u32[19]),
        .I2(Q[2]),
        .I3(u32[23]),
        .I4(Q[3]),
        .I5(u32[15]),
        .O(result3_carry__2_i_9_n_0));
  CARRY4 result3_carry__3
       (.CI(result3_carry__2_n_0),
        .CO({result3_carry__3_n_0,result3_carry__3_n_1,result3_carry__3_n_2,result3_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({result3_carry__3_i_1_n_0,result3_carry__3_i_2_n_0,result3_carry__3_i_3_n_0,result3_carry__3_i_4_n_0}),
        .O(NLW_result3_carry__3_O_UNCONNECTED[3:0]),
        .S({result3_carry__3_i_5_n_0,result3_carry__3_i_6_n_0,result3_carry__3_i_7_n_0,result3_carry__3_i_8_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__3_i_1
       (.I0(v32[18]),
        .O(result3_carry__3_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_10
       (.I0(result3_carry__3_i_15_n_0),
        .I1(result3_carry__2_i_6_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_14_n_0),
        .I4(Q[1]),
        .I5(result3_carry__2_i_8_n_0),
        .O(v32[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_11
       (.I0(result3_carry__3_i_14_n_0),
        .I1(result3_carry__2_i_8_n_0),
        .I2(Q[0]),
        .I3(result3_carry__2_i_6_n_0),
        .I4(Q[1]),
        .I5(result3_carry__2_i_7_n_0),
        .O(v32[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_12
       (.I0(result3_carry__3_i_16_n_0),
        .I1(result3_carry__3_i_15_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_13_n_0),
        .I4(Q[1]),
        .I5(result3_carry__3_i_14_n_0),
        .O(v32[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_13
       (.I0(u32[31]),
        .I1(u32[25]),
        .I2(Q[2]),
        .I3(u32[29]),
        .I4(Q[3]),
        .I5(u32[21]),
        .O(result3_carry__3_i_13_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_14
       (.I0(u32[31]),
        .I1(u32[23]),
        .I2(Q[2]),
        .I3(u32[27]),
        .I4(Q[3]),
        .I5(u32[19]),
        .O(result3_carry__3_i_14_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_15
       (.I0(u32[31]),
        .I1(u32[24]),
        .I2(Q[2]),
        .I3(u32[28]),
        .I4(Q[3]),
        .I5(u32[20]),
        .O(result3_carry__3_i_15_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_16
       (.I0(u32[31]),
        .I1(u32[26]),
        .I2(Q[2]),
        .I3(u32[30]),
        .I4(Q[3]),
        .I5(u32[22]),
        .O(result3_carry__3_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__3_i_2
       (.I0(v32[17]),
        .O(result3_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__3_i_3
       (.I0(v32[16]),
        .O(result3_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__3_i_4
       (.I0(v32[15]),
        .O(result3_carry__3_i_4_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__3_i_5
       (.I0(v32[18]),
        .I1(v32[19]),
        .O(result3_carry__3_i_5_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__3_i_6
       (.I0(v32[17]),
        .I1(v32[18]),
        .O(result3_carry__3_i_6_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__3_i_7
       (.I0(v32[16]),
        .I1(v32[17]),
        .O(result3_carry__3_i_7_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__3_i_8
       (.I0(v32[15]),
        .I1(v32[16]),
        .O(result3_carry__3_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_9
       (.I0(result3_carry__3_i_13_n_0),
        .I1(result3_carry__3_i_14_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_15_n_0),
        .I4(Q[1]),
        .I5(result3_carry__2_i_6_n_0),
        .O(v32[18]));
  CARRY4 result3_carry__4
       (.CI(result3_carry__3_n_0),
        .CO({result3_carry__4_n_0,result3_carry__4_n_1,result3_carry__4_n_2,result3_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({result3_carry__4_i_1_n_0,result3_carry__4_i_2_n_0,result3_carry__4_i_3_n_0,result3_carry__4_i_4_n_0}),
        .O(NLW_result3_carry__4_O_UNCONNECTED[3:0]),
        .S({result3_carry__4_i_5_n_0,result3_carry__4_i_6_n_0,result3_carry__4_i_7_n_0,result3_carry__4_i_8_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__4_i_1
       (.I0(v32[22]),
        .O(result3_carry__4_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__4_i_10
       (.I0(result3_carry__4_i_15_n_0),
        .I1(result3_carry__3_i_16_n_0),
        .I2(Q[0]),
        .I3(result3_carry__4_i_14_n_0),
        .I4(Q[1]),
        .I5(result3_carry__3_i_13_n_0),
        .O(v32[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__4_i_11
       (.I0(result3_carry__4_i_14_n_0),
        .I1(result3_carry__3_i_13_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_16_n_0),
        .I4(Q[1]),
        .I5(result3_carry__3_i_15_n_0),
        .O(v32[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__4_i_12
       (.I0(result3_carry__5_i_11_n_0),
        .I1(result3_carry__4_i_15_n_0),
        .I2(Q[0]),
        .I3(result3_carry__4_i_13_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_14_n_0),
        .O(v32[23]));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__4_i_13
       (.I0(u32[29]),
        .I1(Q[2]),
        .I2(u32[31]),
        .I3(Q[3]),
        .I4(u32[25]),
        .O(result3_carry__4_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__4_i_14
       (.I0(u32[27]),
        .I1(Q[2]),
        .I2(u32[31]),
        .I3(Q[3]),
        .I4(u32[23]),
        .O(result3_carry__4_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__4_i_15
       (.I0(u32[28]),
        .I1(Q[2]),
        .I2(u32[31]),
        .I3(Q[3]),
        .I4(u32[24]),
        .O(result3_carry__4_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__4_i_2
       (.I0(v32[21]),
        .O(result3_carry__4_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__4_i_3
       (.I0(v32[20]),
        .O(result3_carry__4_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__4_i_4
       (.I0(v32[19]),
        .O(result3_carry__4_i_4_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__4_i_5
       (.I0(v32[22]),
        .I1(v32[23]),
        .O(result3_carry__4_i_5_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__4_i_6
       (.I0(v32[21]),
        .I1(v32[22]),
        .O(result3_carry__4_i_6_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__4_i_7
       (.I0(v32[20]),
        .I1(v32[21]),
        .O(result3_carry__4_i_7_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__4_i_8
       (.I0(v32[19]),
        .I1(v32[20]),
        .O(result3_carry__4_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__4_i_9
       (.I0(result3_carry__4_i_13_n_0),
        .I1(result3_carry__4_i_14_n_0),
        .I2(Q[0]),
        .I3(result3_carry__4_i_15_n_0),
        .I4(Q[1]),
        .I5(result3_carry__3_i_16_n_0),
        .O(v32[22]));
  CARRY4 result3_carry__5
       (.CI(result3_carry__4_n_0),
        .CO({result3_carry__5_n_0,result3_carry__5_n_1,result3_carry__5_n_2,result3_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({result3_carry__5_i_1_n_0,result3_carry__5_i_2_n_0,result3_carry__5_i_3_n_0,result3_carry__5_i_4_n_0}),
        .O(NLW_result3_carry__5_O_UNCONNECTED[3:0]),
        .S({result3_carry__5_i_5_n_0,result3_carry__5_i_6_n_0,result3_carry__5_i_7_n_0,result3_carry__5_i_8_n_0}));
  LUT5 #(
    .INIT(32'h47444777)) 
    result3_carry__5_i_1
       (.I0(result3_carry__5_i_9_n_0),
        .I1(Q[0]),
        .I2(result3_carry__5_i_10_n_0),
        .I3(Q[1]),
        .I4(result3_carry__5_i_11_n_0),
        .O(result3_carry__5_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    result3_carry__5_i_10
       (.I0(Q[2]),
        .I1(u32[31]),
        .I2(Q[3]),
        .I3(u32[28]),
        .O(result3_carry__5_i_10_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__5_i_11
       (.I0(u32[30]),
        .I1(Q[2]),
        .I2(u32[31]),
        .I3(Q[3]),
        .I4(u32[26]),
        .O(result3_carry__5_i_11_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__5_i_12
       (.I0(result3_carry__5_i_10_n_0),
        .I1(result3_carry__5_i_11_n_0),
        .I2(Q[0]),
        .I3(result3_carry__5_i_15_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_13_n_0),
        .O(v32[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__5_i_13
       (.I0(result3_carry__5_i_15_n_0),
        .I1(result3_carry__4_i_13_n_0),
        .I2(Q[0]),
        .I3(result3_carry__5_i_11_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_15_n_0),
        .O(v32[24]));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    result3_carry__5_i_14
       (.I0(u32[30]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(u32[31]),
        .I4(Q[3]),
        .I5(u32[28]),
        .O(result3_carry__5_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    result3_carry__5_i_15
       (.I0(Q[2]),
        .I1(u32[31]),
        .I2(Q[3]),
        .I3(u32[27]),
        .O(result3_carry__5_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__5_i_2
       (.I0(v32[25]),
        .O(result3_carry__5_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__5_i_3
       (.I0(v32[24]),
        .O(result3_carry__5_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__5_i_4
       (.I0(v32[23]),
        .O(result3_carry__5_i_4_n_0));
  LUT6 #(
    .INIT(64'hB487B4B4B4878787)) 
    result3_carry__5_i_5
       (.I0(result3_carry__5_i_14_n_0),
        .I1(Q[0]),
        .I2(result3_carry__5_i_9_n_0),
        .I3(result3_carry__5_i_10_n_0),
        .I4(Q[1]),
        .I5(result3_carry__5_i_11_n_0),
        .O(result3_carry__5_i_5_n_0));
  LUT6 #(
    .INIT(64'hB8BBB88847444777)) 
    result3_carry__5_i_6
       (.I0(result3_carry__5_i_9_n_0),
        .I1(Q[0]),
        .I2(result3_carry__5_i_10_n_0),
        .I3(Q[1]),
        .I4(result3_carry__5_i_11_n_0),
        .I5(v32[25]),
        .O(result3_carry__5_i_6_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__5_i_7
       (.I0(v32[24]),
        .I1(v32[25]),
        .O(result3_carry__5_i_7_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__5_i_8
       (.I0(v32[23]),
        .I1(v32[24]),
        .O(result3_carry__5_i_8_n_0));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    result3_carry__5_i_9
       (.I0(u32[29]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(u32[31]),
        .I4(Q[3]),
        .I5(u32[27]),
        .O(result3_carry__5_i_9_n_0));
  CARRY4 result3_carry__6
       (.CI(result3_carry__5_n_0),
        .CO({NLW_result3_carry__6_CO_UNCONNECTED[3],result3_carry__6_n_1,result3_carry__6_n_2,result3_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,result3_carry__6_i_1_n_0,result3_carry__6_i_2_n_0,result3_carry__6_i_3_n_0}),
        .O({p_0_in,NLW_result3_carry__6_O_UNCONNECTED[2:0]}),
        .S({result3_carry__6_i_4_n_0,result3_carry__6_i_5_n_0,result3_carry__6_i_6_n_0,result3_carry__6_i_7_n_0}));
  LUT3 #(
    .INIT(8'h47)) 
    result3_carry__6_i_1
       (.I0(result3_carry__6_i_8_n_0),
        .I1(Q[0]),
        .I2(result3_carry__6_i_9_n_0),
        .O(result3_carry__6_i_1_n_0));
  LUT3 #(
    .INIT(8'h47)) 
    result3_carry__6_i_2
       (.I0(result3_carry__6_i_9_n_0),
        .I1(Q[0]),
        .I2(result3_carry__5_i_14_n_0),
        .O(result3_carry__6_i_2_n_0));
  LUT3 #(
    .INIT(8'h47)) 
    result3_carry__6_i_3
       (.I0(result3_carry__5_i_14_n_0),
        .I1(Q[0]),
        .I2(result3_carry__5_i_9_n_0),
        .O(result3_carry__6_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFD)) 
    result3_carry__6_i_4
       (.I0(u32[30]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(u32[31]),
        .O(result3_carry__6_i_4_n_0));
  LUT4 #(
    .INIT(16'hC939)) 
    result3_carry__6_i_5
       (.I0(result3_carry__6_i_9_n_0),
        .I1(result3_carry__6_i_8_n_0),
        .I2(Q[0]),
        .I3(u32[31]),
        .O(result3_carry__6_i_5_n_0));
  LUT4 #(
    .INIT(16'hB487)) 
    result3_carry__6_i_6
       (.I0(result3_carry__6_i_8_n_0),
        .I1(Q[0]),
        .I2(result3_carry__6_i_9_n_0),
        .I3(result3_carry__5_i_14_n_0),
        .O(result3_carry__6_i_6_n_0));
  LUT4 #(
    .INIT(16'hB487)) 
    result3_carry__6_i_7
       (.I0(result3_carry__6_i_9_n_0),
        .I1(Q[0]),
        .I2(result3_carry__5_i_14_n_0),
        .I3(result3_carry__5_i_9_n_0),
        .O(result3_carry__6_i_7_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    result3_carry__6_i_8
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(u32[31]),
        .I3(Q[3]),
        .I4(u32[30]),
        .O(result3_carry__6_i_8_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    result3_carry__6_i_9
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(u32[31]),
        .I3(Q[3]),
        .I4(u32[29]),
        .O(result3_carry__6_i_9_n_0));
  LUT6 #(
    .INIT(64'hB847B847FF0000FF)) 
    result3_carry_i_1
       (.I0(result3_carry_i_5_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_6_n_0),
        .I3(x[3]),
        .I4(result3_carry_i_7_n_0),
        .I5(Q[0]),
        .O(result3_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_10
       (.I0(u32[12]),
        .I1(u32[4]),
        .I2(Q[2]),
        .I3(u32[8]),
        .I4(Q[3]),
        .I5(u32[0]),
        .O(result3_carry_i_10_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_11
       (.I0(u32[17]),
        .I1(u32[9]),
        .I2(Q[2]),
        .I3(u32[13]),
        .I4(Q[3]),
        .I5(u32[5]),
        .O(result3_carry_i_11_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_12
       (.I0(u32[15]),
        .I1(u32[7]),
        .I2(Q[2]),
        .I3(u32[11]),
        .I4(Q[3]),
        .I5(u32[3]),
        .O(result3_carry_i_12_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_13
       (.I0(u32[13]),
        .I1(u32[5]),
        .I2(Q[2]),
        .I3(u32[9]),
        .I4(Q[3]),
        .I5(u32[1]),
        .O(result3_carry_i_13_n_0));
  LUT6 #(
    .INIT(64'hFF00B84700FFB847)) 
    result3_carry_i_2
       (.I0(result3_carry_i_6_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_8_n_0),
        .I3(x[2]),
        .I4(Q[0]),
        .I5(result3_carry_i_7_n_0),
        .O(result3_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'hB847B847FF0000FF)) 
    result3_carry_i_3
       (.I0(result3_carry_i_6_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_8_n_0),
        .I3(x[1]),
        .I4(result3_carry_i_9_n_0),
        .I5(Q[0]),
        .O(result3_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'hAAAAA9595555A959)) 
    result3_carry_i_4
       (.I0(x[0]),
        .I1(result3_carry_i_10_n_0),
        .I2(Q[1]),
        .I3(result3_carry_i_8_n_0),
        .I4(Q[0]),
        .I5(result3_carry_i_9_n_0),
        .O(result3_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_5
       (.I0(u32[18]),
        .I1(u32[10]),
        .I2(Q[2]),
        .I3(u32[14]),
        .I4(Q[3]),
        .I5(u32[6]),
        .O(result3_carry_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_6
       (.I0(u32[16]),
        .I1(u32[8]),
        .I2(Q[2]),
        .I3(u32[12]),
        .I4(Q[3]),
        .I5(u32[4]),
        .O(result3_carry_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry_i_7
       (.I0(result3_carry_i_11_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_12_n_0),
        .O(result3_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_8
       (.I0(u32[14]),
        .I1(u32[6]),
        .I2(Q[2]),
        .I3(u32[10]),
        .I4(Q[3]),
        .I5(u32[2]),
        .O(result3_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry_i_9
       (.I0(result3_carry_i_12_n_0),
        .I1(Q[1]),
        .I2(result3_carry_i_13_n_0),
        .O(result3_carry_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[0]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry_n_7),
        .O(p_2_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[10]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__1_n_5),
        .O(p_2_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[11]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__1_n_4),
        .O(p_2_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[12]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__2_n_7),
        .O(p_2_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[13]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__2_n_6),
        .O(p_2_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[14]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__2_n_5),
        .O(p_2_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[1]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry_n_6),
        .O(p_2_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[2]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry_n_5),
        .O(p_2_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT1 #(
    .INIT(2'h2)) 
    \result[31]_C_i_1 
       (.I0(p_0_in),
        .O(p_2_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[3]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry_n_4),
        .O(p_2_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[4]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__0_n_7),
        .O(p_2_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[5]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__0_n_6),
        .O(p_2_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[6]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__0_n_5),
        .O(p_2_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[7]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__0_n_4),
        .O(p_2_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[8]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__1_n_7),
        .O(p_2_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[9]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__1_n_6),
        .O(p_2_in[9]));
  FDCE \result_reg[0]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[0]_LDC_i_2_n_0 ),
        .D(p_2_in[0]),
        .Q(\result_reg[0]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[0]_LDC 
       (.CLR(\result_reg[0]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[0]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[0]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[0]_LDC_i_1 
       (.I0(x[0]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[0]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[0]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[0]),
        .O(\result_reg[0]_LDC_i_2_n_0 ));
  FDPE \result_reg[0]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[0]),
        .PRE(\result_reg[0]_LDC_i_1_n_0 ),
        .Q(\result_reg[0]_P_n_0 ));
  FDCE \result_reg[10]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[10]_LDC_i_2_n_0 ),
        .D(p_2_in[10]),
        .Q(\result_reg[10]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[10]_LDC 
       (.CLR(\result_reg[10]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[10]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[10]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[10]_LDC_i_1 
       (.I0(x[10]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[10]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[10]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[10]),
        .O(\result_reg[10]_LDC_i_2_n_0 ));
  FDPE \result_reg[10]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[10]),
        .PRE(\result_reg[10]_LDC_i_1_n_0 ),
        .Q(\result_reg[10]_P_n_0 ));
  FDCE \result_reg[11]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[11]_LDC_i_2_n_0 ),
        .D(p_2_in[11]),
        .Q(\result_reg[11]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[11]_LDC 
       (.CLR(\result_reg[11]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[11]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[11]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[11]_LDC_i_1 
       (.I0(x[11]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[11]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[11]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[11]),
        .O(\result_reg[11]_LDC_i_2_n_0 ));
  FDPE \result_reg[11]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[11]),
        .PRE(\result_reg[11]_LDC_i_1_n_0 ),
        .Q(\result_reg[11]_P_n_0 ));
  FDCE \result_reg[12]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[12]_LDC_i_2_n_0 ),
        .D(p_2_in[12]),
        .Q(\result_reg[12]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[12]_LDC 
       (.CLR(\result_reg[12]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[12]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[12]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[12]_LDC_i_1 
       (.I0(x[12]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[12]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[12]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[12]),
        .O(\result_reg[12]_LDC_i_2_n_0 ));
  FDPE \result_reg[12]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[12]),
        .PRE(\result_reg[12]_LDC_i_1_n_0 ),
        .Q(\result_reg[12]_P_n_0 ));
  FDCE \result_reg[13]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[13]_LDC_i_2_n_0 ),
        .D(p_2_in[13]),
        .Q(\result_reg[13]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[13]_LDC 
       (.CLR(\result_reg[13]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[13]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[13]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[13]_LDC_i_1 
       (.I0(x[13]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[13]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[13]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[13]),
        .O(\result_reg[13]_LDC_i_2_n_0 ));
  FDPE \result_reg[13]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[13]),
        .PRE(\result_reg[13]_LDC_i_1_n_0 ),
        .Q(\result_reg[13]_P_n_0 ));
  FDCE \result_reg[14]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[14]_LDC_i_2_n_0 ),
        .D(p_2_in[14]),
        .Q(\result_reg[14]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[14]_LDC 
       (.CLR(\result_reg[14]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[14]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[14]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[14]_LDC_i_1 
       (.I0(x[14]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[14]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[14]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[14]),
        .O(\result_reg[14]_LDC_i_2_n_0 ));
  FDPE \result_reg[14]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[14]),
        .PRE(\result_reg[14]_LDC_i_1_n_0 ),
        .Q(\result_reg[14]_P_n_0 ));
  FDCE \result_reg[1]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[1]_LDC_i_2_n_0 ),
        .D(p_2_in[1]),
        .Q(\result_reg[1]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[1]_LDC 
       (.CLR(\result_reg[1]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[1]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[1]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[1]_LDC_i_1 
       (.I0(x[1]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[1]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[1]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[1]),
        .O(\result_reg[1]_LDC_i_2_n_0 ));
  FDPE \result_reg[1]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[1]),
        .PRE(\result_reg[1]_LDC_i_1_n_0 ),
        .Q(\result_reg[1]_P_n_0 ));
  FDCE \result_reg[2]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[2]_LDC_i_2_n_0 ),
        .D(p_2_in[2]),
        .Q(\result_reg[2]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[2]_LDC 
       (.CLR(\result_reg[2]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[2]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[2]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[2]_LDC_i_1 
       (.I0(x[2]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[2]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[2]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[2]),
        .O(\result_reg[2]_LDC_i_2_n_0 ));
  FDPE \result_reg[2]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[2]),
        .PRE(\result_reg[2]_LDC_i_1_n_0 ),
        .Q(\result_reg[2]_P_n_0 ));
  FDCE \result_reg[31]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[31]_LDC_i_2_n_0 ),
        .D(p_2_in[31]),
        .Q(\result_reg[31]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[31]_LDC 
       (.CLR(\result_reg[31]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[31]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[31]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[31]_LDC_i_1 
       (.I0(x[15]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[31]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[31]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[15]),
        .O(\result_reg[31]_LDC_i_2_n_0 ));
  FDPE \result_reg[31]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[31]),
        .PRE(\result_reg[31]_LDC_i_1_n_0 ),
        .Q(\result_reg[31]_P_n_0 ));
  FDCE \result_reg[3]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[3]_LDC_i_2_n_0 ),
        .D(p_2_in[3]),
        .Q(\result_reg[3]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[3]_LDC 
       (.CLR(\result_reg[3]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[3]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[3]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[3]_LDC_i_1 
       (.I0(x[3]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[3]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[3]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[3]),
        .O(\result_reg[3]_LDC_i_2_n_0 ));
  FDPE \result_reg[3]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[3]),
        .PRE(\result_reg[3]_LDC_i_1_n_0 ),
        .Q(\result_reg[3]_P_n_0 ));
  FDCE \result_reg[4]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[4]_LDC_i_2_n_0 ),
        .D(p_2_in[4]),
        .Q(\result_reg[4]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[4]_LDC 
       (.CLR(\result_reg[4]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[4]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[4]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[4]_LDC_i_1 
       (.I0(x[4]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[4]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[4]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[4]),
        .O(\result_reg[4]_LDC_i_2_n_0 ));
  FDPE \result_reg[4]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[4]),
        .PRE(\result_reg[4]_LDC_i_1_n_0 ),
        .Q(\result_reg[4]_P_n_0 ));
  FDCE \result_reg[5]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[5]_LDC_i_2_n_0 ),
        .D(p_2_in[5]),
        .Q(\result_reg[5]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[5]_LDC 
       (.CLR(\result_reg[5]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[5]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[5]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[5]_LDC_i_1 
       (.I0(x[5]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[5]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[5]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[5]),
        .O(\result_reg[5]_LDC_i_2_n_0 ));
  FDPE \result_reg[5]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[5]),
        .PRE(\result_reg[5]_LDC_i_1_n_0 ),
        .Q(\result_reg[5]_P_n_0 ));
  FDCE \result_reg[6]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[6]_LDC_i_2_n_0 ),
        .D(p_2_in[6]),
        .Q(\result_reg[6]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[6]_LDC 
       (.CLR(\result_reg[6]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[6]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[6]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[6]_LDC_i_1 
       (.I0(x[6]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[6]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[6]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[6]),
        .O(\result_reg[6]_LDC_i_2_n_0 ));
  FDPE \result_reg[6]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[6]),
        .PRE(\result_reg[6]_LDC_i_1_n_0 ),
        .Q(\result_reg[6]_P_n_0 ));
  FDCE \result_reg[7]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[7]_LDC_i_2_n_0 ),
        .D(p_2_in[7]),
        .Q(\result_reg[7]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[7]_LDC 
       (.CLR(\result_reg[7]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[7]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[7]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[7]_LDC_i_1 
       (.I0(x[7]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[7]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[7]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[7]),
        .O(\result_reg[7]_LDC_i_2_n_0 ));
  FDPE \result_reg[7]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[7]),
        .PRE(\result_reg[7]_LDC_i_1_n_0 ),
        .Q(\result_reg[7]_P_n_0 ));
  FDCE \result_reg[8]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[8]_LDC_i_2_n_0 ),
        .D(p_2_in[8]),
        .Q(\result_reg[8]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[8]_LDC 
       (.CLR(\result_reg[8]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[8]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[8]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[8]_LDC_i_1 
       (.I0(x[8]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[8]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[8]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[8]),
        .O(\result_reg[8]_LDC_i_2_n_0 ));
  FDPE \result_reg[8]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[8]),
        .PRE(\result_reg[8]_LDC_i_1_n_0 ),
        .Q(\result_reg[8]_P_n_0 ));
  FDCE \result_reg[9]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[9]_LDC_i_2_n_0 ),
        .D(p_2_in[9]),
        .Q(\result_reg[9]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[9]_LDC 
       (.CLR(\result_reg[9]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[9]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[9]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[9]_LDC_i_1 
       (.I0(x[9]),
        .I1(Q[4]),
        .I2(resetn),
        .O(\result_reg[9]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[9]_LDC_i_2 
       (.I0(Q[4]),
        .I1(resetn),
        .I2(x[9]),
        .O(\result_reg[9]_LDC_i_2_n_0 ));
  FDPE \result_reg[9]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[9]),
        .PRE(\result_reg[9]_LDC_i_1_n_0 ),
        .Q(\result_reg[9]_P_n_0 ));
  CARRY4 u320_carry
       (.CI(1'b0),
        .CO({u320_carry_n_0,u320_carry_n_1,u320_carry_n_2,u320_carry_n_3}),
        .CYINIT(1'b1),
        .DI(acc32_reg[3:0]),
        .O(u320[3:0]),
        .S({delay_line_inst_n_0,delay_line_inst_n_1,delay_line_inst_n_2,delay_line_inst_n_3}));
  CARRY4 u320_carry__0
       (.CI(u320_carry_n_0),
        .CO({u320_carry__0_n_0,u320_carry__0_n_1,u320_carry__0_n_2,u320_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(acc32_reg[7:4]),
        .O(u320[7:4]),
        .S({delay_line_inst_n_4,delay_line_inst_n_5,delay_line_inst_n_6,delay_line_inst_n_7}));
  CARRY4 u320_carry__1
       (.CI(u320_carry__0_n_0),
        .CO({u320_carry__1_n_0,u320_carry__1_n_1,u320_carry__1_n_2,u320_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(acc32_reg[11:8]),
        .O(u320[11:8]),
        .S({delay_line_inst_n_8,delay_line_inst_n_9,delay_line_inst_n_10,delay_line_inst_n_11}));
  CARRY4 u320_carry__2
       (.CI(u320_carry__1_n_0),
        .CO({u320_carry__2_n_0,u320_carry__2_n_1,u320_carry__2_n_2,u320_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(acc32_reg[15:12]),
        .O(u320[15:12]),
        .S({delay_line_inst_n_12,delay_line_inst_n_13,delay_line_inst_n_14,delay_line_inst_n_15}));
  CARRY4 u320_carry__3
       (.CI(u320_carry__2_n_0),
        .CO({u320_carry__3_n_0,u320_carry__3_n_1,u320_carry__3_n_2,u320_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(acc32_reg[19:16]),
        .O(u320[19:16]),
        .S({delay_line_inst_n_16,delay_line_inst_n_17,delay_line_inst_n_18,delay_line_inst_n_19}));
  CARRY4 u320_carry__4
       (.CI(u320_carry__3_n_0),
        .CO({u320_carry__4_n_0,u320_carry__4_n_1,u320_carry__4_n_2,u320_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(acc32_reg[23:20]),
        .O(u320[23:20]),
        .S({delay_line_inst_n_20,delay_line_inst_n_21,delay_line_inst_n_22,delay_line_inst_n_23}));
  CARRY4 u320_carry__5
       (.CI(u320_carry__4_n_0),
        .CO({u320_carry__5_n_0,u320_carry__5_n_1,u320_carry__5_n_2,u320_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(acc32_reg[27:24]),
        .O(u320[27:24]),
        .S({delay_line_inst_n_24,delay_line_inst_n_25,delay_line_inst_n_26,delay_line_inst_n_27}));
  CARRY4 u320_carry__6
       (.CI(u320_carry__5_n_0),
        .CO({NLW_u320_carry__6_CO_UNCONNECTED[3],u320_carry__6_n_1,u320_carry__6_n_2,u320_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,acc32_reg[30:28]}),
        .O(u320[31:28]),
        .S({delay_line_inst_n_28,delay_line_inst_n_29,delay_line_inst_n_30,delay_line_inst_n_31}));
  FDCE \u32_reg[0] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[0]),
        .Q(u32[0]));
  FDCE \u32_reg[10] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[10]),
        .Q(u32[10]));
  FDCE \u32_reg[11] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[11]),
        .Q(u32[11]));
  FDCE \u32_reg[12] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[12]),
        .Q(u32[12]));
  FDCE \u32_reg[13] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[13]),
        .Q(u32[13]));
  FDCE \u32_reg[14] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[14]),
        .Q(u32[14]));
  FDCE \u32_reg[15] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[15]),
        .Q(u32[15]));
  FDCE \u32_reg[16] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[16]),
        .Q(u32[16]));
  FDCE \u32_reg[17] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[17]),
        .Q(u32[17]));
  FDCE \u32_reg[18] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[18]),
        .Q(u32[18]));
  FDCE \u32_reg[19] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[19]),
        .Q(u32[19]));
  FDCE \u32_reg[1] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[1]),
        .Q(u32[1]));
  FDCE \u32_reg[20] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[20]),
        .Q(u32[20]));
  FDCE \u32_reg[21] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[21]),
        .Q(u32[21]));
  FDCE \u32_reg[22] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[22]),
        .Q(u32[22]));
  FDCE \u32_reg[23] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[23]),
        .Q(u32[23]));
  FDCE \u32_reg[24] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[24]),
        .Q(u32[24]));
  FDCE \u32_reg[25] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[25]),
        .Q(u32[25]));
  FDCE \u32_reg[26] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[26]),
        .Q(u32[26]));
  FDCE \u32_reg[27] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[27]),
        .Q(u32[27]));
  FDCE \u32_reg[28] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[28]),
        .Q(u32[28]));
  FDCE \u32_reg[29] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[29]),
        .Q(u32[29]));
  FDCE \u32_reg[2] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[2]),
        .Q(u32[2]));
  FDCE \u32_reg[30] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[30]),
        .Q(u32[30]));
  FDCE \u32_reg[31] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[31]),
        .Q(u32[31]));
  FDCE \u32_reg[3] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[3]),
        .Q(u32[3]));
  FDCE \u32_reg[4] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[4]),
        .Q(u32[4]));
  FDCE \u32_reg[5] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[5]),
        .Q(u32[5]));
  FDCE \u32_reg[6] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[6]),
        .Q(u32[6]));
  FDCE \u32_reg[7] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[7]),
        .Q(u32[7]));
  FDCE \u32_reg[8] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[8]),
        .Q(u32[8]));
  FDCE \u32_reg[9] 
       (.C(clk),
        .CE(en),
        .CLR(en_i_2_n_0),
        .D(u320[9]),
        .Q(u32[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[0]_INST_0 
       (.I0(\result_reg[0]_P_n_0 ),
        .I1(\result_reg[0]_LDC_n_0 ),
        .I2(\result_reg[0]_C_n_0 ),
        .O(y[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[10]_INST_0 
       (.I0(\result_reg[10]_P_n_0 ),
        .I1(\result_reg[10]_LDC_n_0 ),
        .I2(\result_reg[10]_C_n_0 ),
        .O(y[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[11]_INST_0 
       (.I0(\result_reg[11]_P_n_0 ),
        .I1(\result_reg[11]_LDC_n_0 ),
        .I2(\result_reg[11]_C_n_0 ),
        .O(y[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[12]_INST_0 
       (.I0(\result_reg[12]_P_n_0 ),
        .I1(\result_reg[12]_LDC_n_0 ),
        .I2(\result_reg[12]_C_n_0 ),
        .O(y[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[13]_INST_0 
       (.I0(\result_reg[13]_P_n_0 ),
        .I1(\result_reg[13]_LDC_n_0 ),
        .I2(\result_reg[13]_C_n_0 ),
        .O(y[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[14]_INST_0 
       (.I0(\result_reg[14]_P_n_0 ),
        .I1(\result_reg[14]_LDC_n_0 ),
        .I2(\result_reg[14]_C_n_0 ),
        .O(y[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[15]_INST_0 
       (.I0(\result_reg[31]_P_n_0 ),
        .I1(\result_reg[31]_LDC_n_0 ),
        .I2(\result_reg[31]_C_n_0 ),
        .O(y[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[1]_INST_0 
       (.I0(\result_reg[1]_P_n_0 ),
        .I1(\result_reg[1]_LDC_n_0 ),
        .I2(\result_reg[1]_C_n_0 ),
        .O(y[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[2]_INST_0 
       (.I0(\result_reg[2]_P_n_0 ),
        .I1(\result_reg[2]_LDC_n_0 ),
        .I2(\result_reg[2]_C_n_0 ),
        .O(y[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[3]_INST_0 
       (.I0(\result_reg[3]_P_n_0 ),
        .I1(\result_reg[3]_LDC_n_0 ),
        .I2(\result_reg[3]_C_n_0 ),
        .O(y[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[4]_INST_0 
       (.I0(\result_reg[4]_P_n_0 ),
        .I1(\result_reg[4]_LDC_n_0 ),
        .I2(\result_reg[4]_C_n_0 ),
        .O(y[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[5]_INST_0 
       (.I0(\result_reg[5]_P_n_0 ),
        .I1(\result_reg[5]_LDC_n_0 ),
        .I2(\result_reg[5]_C_n_0 ),
        .O(y[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[6]_INST_0 
       (.I0(\result_reg[6]_P_n_0 ),
        .I1(\result_reg[6]_LDC_n_0 ),
        .I2(\result_reg[6]_C_n_0 ),
        .O(y[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[7]_INST_0 
       (.I0(\result_reg[7]_P_n_0 ),
        .I1(\result_reg[7]_LDC_n_0 ),
        .I2(\result_reg[7]_C_n_0 ),
        .O(y[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[8]_INST_0 
       (.I0(\result_reg[8]_P_n_0 ),
        .I1(\result_reg[8]_LDC_n_0 ),
        .I2(\result_reg[8]_C_n_0 ),
        .O(y[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y[9]_INST_0 
       (.I0(\result_reg[9]_P_n_0 ),
        .I1(\result_reg[9]_LDC_n_0 ),
        .I2(\result_reg[9]_C_n_0 ),
        .O(y[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_comparator
   (CO,
    S);
  output [0:0]CO;
  input [3:0]S;

  wire [0:0]CO;
  wire [3:0]S;
  wire cmp0_carry_n_1;
  wire cmp0_carry_n_2;
  wire cmp0_carry_n_3;
  wire [3:0]NLW_cmp0_carry_O_UNCONNECTED;

  CARRY4 cmp0_carry
       (.CI(1'b0),
        .CO({CO,cmp0_carry_n_1,cmp0_carry_n_2,cmp0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_cmp0_carry_O_UNCONNECTED[3:0]),
        .S(S));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter
   (S,
    Q,
    cmp0_carry,
    CO,
    resetn,
    en,
    clk);
  output [3:0]S;
  output [9:0]Q;
  input [3:0]cmp0_carry;
  input [0:0]CO;
  input resetn;
  input en;
  input clk;

  wire [0:0]CO;
  wire [9:0]Q;
  wire [3:0]S;
  wire clk;
  wire [3:0]cmp0_carry;
  wire cmp0_carry_i_5_n_0;
  wire cmp0_carry_i_6_n_0;
  wire cmp0_carry_i_7_n_0;
  wire en;
  wire [9:0]plusOp;
  wire resetn;
  wire \tmp[9]_i_2_n_0 ;
  wire \tmp[9]_i_3_n_0 ;

  LUT4 #(
    .INIT(16'h9995)) 
    cmp0_carry_i_1
       (.I0(Q[9]),
        .I1(cmp0_carry[3]),
        .I2(cmp0_carry[1]),
        .I3(cmp0_carry[2]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'hFF80007F00000000)) 
    cmp0_carry_i_2
       (.I0(cmp0_carry[1]),
        .I1(cmp0_carry[0]),
        .I2(cmp0_carry[2]),
        .I3(cmp0_carry[3]),
        .I4(Q[6]),
        .I5(cmp0_carry_i_5_n_0),
        .O(S[2]));
  LUT4 #(
    .INIT(16'hE100)) 
    cmp0_carry_i_3
       (.I0(cmp0_carry[2]),
        .I1(cmp0_carry[3]),
        .I2(Q[3]),
        .I3(cmp0_carry_i_6_n_0),
        .O(S[1]));
  LUT6 #(
    .INIT(64'hFFFE000100000000)) 
    cmp0_carry_i_4
       (.I0(cmp0_carry[0]),
        .I1(cmp0_carry[1]),
        .I2(cmp0_carry[2]),
        .I3(cmp0_carry[3]),
        .I4(Q[0]),
        .I5(cmp0_carry_i_7_n_0),
        .O(S[0]));
  LUT6 #(
    .INIT(64'hAAA8000000025555)) 
    cmp0_carry_i_5
       (.I0(Q[7]),
        .I1(cmp0_carry[2]),
        .I2(cmp0_carry[1]),
        .I3(cmp0_carry[0]),
        .I4(cmp0_carry[3]),
        .I5(Q[8]),
        .O(cmp0_carry_i_5_n_0));
  LUT6 #(
    .INIT(64'hCCCCC00000000933)) 
    cmp0_carry_i_6
       (.I0(cmp0_carry[0]),
        .I1(Q[4]),
        .I2(cmp0_carry[1]),
        .I3(cmp0_carry[2]),
        .I4(cmp0_carry[3]),
        .I5(Q[5]),
        .O(cmp0_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'hAAAAAA8000000025)) 
    cmp0_carry_i_7
       (.I0(Q[1]),
        .I1(cmp0_carry[0]),
        .I2(cmp0_carry[1]),
        .I3(cmp0_carry[2]),
        .I4(cmp0_carry[3]),
        .I5(Q[2]),
        .O(cmp0_carry_i_7_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \tmp[0]_i_1 
       (.I0(Q[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tmp[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \tmp[2]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \tmp[3]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \tmp[4]_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[3]),
        .I4(Q[4]),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \tmp[5]_i_1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[5]),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tmp[6]_i_1 
       (.I0(\tmp[9]_i_3_n_0 ),
        .I1(Q[6]),
        .O(plusOp[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \tmp[7]_i_1 
       (.I0(\tmp[9]_i_3_n_0 ),
        .I1(Q[6]),
        .I2(Q[7]),
        .O(plusOp[7]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \tmp[8]_i_1 
       (.I0(Q[6]),
        .I1(\tmp[9]_i_3_n_0 ),
        .I2(Q[7]),
        .I3(Q[8]),
        .O(plusOp[8]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \tmp[9]_i_1 
       (.I0(Q[7]),
        .I1(\tmp[9]_i_3_n_0 ),
        .I2(Q[6]),
        .I3(Q[8]),
        .I4(Q[9]),
        .O(plusOp[9]));
  LUT2 #(
    .INIT(4'hB)) 
    \tmp[9]_i_2 
       (.I0(CO),
        .I1(resetn),
        .O(\tmp[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \tmp[9]_i_3 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\tmp[9]_i_3_n_0 ));
  FDCE \tmp_reg[0] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[0]),
        .Q(Q[0]));
  FDCE \tmp_reg[1] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[1]),
        .Q(Q[1]));
  FDCE \tmp_reg[2] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[2]),
        .Q(Q[2]));
  FDCE \tmp_reg[3] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[3]),
        .Q(Q[3]));
  FDCE \tmp_reg[4] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[4]),
        .Q(Q[4]));
  FDCE \tmp_reg[5] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[5]),
        .Q(Q[5]));
  FDCE \tmp_reg[6] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[6]),
        .Q(Q[6]));
  FDCE \tmp_reg[7] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[7]),
        .Q(Q[7]));
  FDCE \tmp_reg[8] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[8]),
        .Q(Q[8]));
  FDCE \tmp_reg[9] 
       (.C(clk),
        .CE(en),
        .CLR(\tmp[9]_i_2_n_0 ),
        .D(plusOp[9]),
        .Q(Q[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_delay_line
   (S,
    \acc32_reg[7] ,
    \acc32_reg[11] ,
    \acc32_reg[15] ,
    \acc32_reg[19] ,
    \acc32_reg[23] ,
    \acc32_reg[27] ,
    \acc32_reg[31] ,
    clk,
    en,
    DIADI,
    Q,
    resetn);
  output [3:0]S;
  output [3:0]\acc32_reg[7] ;
  output [3:0]\acc32_reg[11] ;
  output [3:0]\acc32_reg[15] ;
  output [3:0]\acc32_reg[19] ;
  output [3:0]\acc32_reg[23] ;
  output [3:0]\acc32_reg[27] ;
  output [3:0]\acc32_reg[31] ;
  input clk;
  input en;
  input [31:0]DIADI;
  input [3:0]Q;
  input resetn;

  wire [31:0]DIADI;
  wire [3:0]Q;
  wire [3:0]S;
  wire [3:0]\acc32_reg[11] ;
  wire [3:0]\acc32_reg[15] ;
  wire [3:0]\acc32_reg[19] ;
  wire [3:0]\acc32_reg[23] ;
  wire [3:0]\acc32_reg[27] ;
  wire [3:0]\acc32_reg[31] ;
  wire [3:0]\acc32_reg[7] ;
  wire clk;
  wire counter_clr;
  wire counter_inst_n_0;
  wire counter_inst_n_1;
  wire counter_inst_n_2;
  wire counter_inst_n_3;
  wire [9:0]counter_out;
  wire en;
  wire resetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_comparator comparator_inst
       (.CO(counter_clr),
        .S({counter_inst_n_0,counter_inst_n_1,counter_inst_n_2,counter_inst_n_3}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter counter_inst
       (.CO(counter_clr),
        .Q(counter_out),
        .S({counter_inst_n_0,counter_inst_n_1,counter_inst_n_2,counter_inst_n_3}),
        .clk(clk),
        .cmp0_carry(Q),
        .en(en),
        .resetn(resetn));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sdp_ram sdp_ram_inst
       (.DIADI(DIADI),
        .Q(counter_out),
        .S(S),
        .\acc32_reg[11] (\acc32_reg[11] ),
        .\acc32_reg[15] (\acc32_reg[15] ),
        .\acc32_reg[19] (\acc32_reg[19] ),
        .\acc32_reg[23] (\acc32_reg[23] ),
        .\acc32_reg[27] (\acc32_reg[27] ),
        .\acc32_reg[31] (\acc32_reg[31] ),
        .\acc32_reg[7] (\acc32_reg[7] ),
        .clk(clk),
        .en(en));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ip_blr_0_0,ip_blr_v3_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "ip_blr_v3_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (x,
    y,
    resetn,
    clk,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input [15:0]x;
  output [15:0]y;
  input resetn;
  input clk;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [3:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire clk;
  wire resetn;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [15:0]x;
  wire [15:0]y;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_blr_v3_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .clk(clk),
        .resetn(resetn),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[3:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[3:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .x(x),
        .y(y));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_blr_v3_0
   (y,
    S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    clk,
    x,
    resetn,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_aclk,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output [15:0]y;
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input clk;
  input [15:0]x;
  input resetn;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire clk;
  wire resetn;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [15:0]x;
  wire [15:0]y;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_blr_v3_0_S00_AXI ip_blr_v3_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .clk(clk),
        .resetn(resetn),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .x(x),
        .y(y));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_blr_v3_0_S00_AXI
   (y,
    S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    clk,
    x,
    resetn,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_aclk,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output [15:0]y;
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input clk;
  input [15:0]x;
  input resetn;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire blr_enable;
  wire clk;
  wire [1:0]p_0_in;
  wire [31:0]reg_data_out;
  wire resetn;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire \slv_reg0[15]_i_1_n_0 ;
  wire \slv_reg0[23]_i_1_n_0 ;
  wire \slv_reg0[31]_i_1_n_0 ;
  wire \slv_reg0[7]_i_1_n_0 ;
  wire [15:0]slv_reg0_D;
  wire \slv_reg0_reg_n_0_[0] ;
  wire \slv_reg0_reg_n_0_[10] ;
  wire \slv_reg0_reg_n_0_[11] ;
  wire \slv_reg0_reg_n_0_[12] ;
  wire \slv_reg0_reg_n_0_[13] ;
  wire \slv_reg0_reg_n_0_[14] ;
  wire \slv_reg0_reg_n_0_[15] ;
  wire \slv_reg0_reg_n_0_[16] ;
  wire \slv_reg0_reg_n_0_[17] ;
  wire \slv_reg0_reg_n_0_[18] ;
  wire \slv_reg0_reg_n_0_[19] ;
  wire \slv_reg0_reg_n_0_[1] ;
  wire \slv_reg0_reg_n_0_[20] ;
  wire \slv_reg0_reg_n_0_[21] ;
  wire \slv_reg0_reg_n_0_[22] ;
  wire \slv_reg0_reg_n_0_[23] ;
  wire \slv_reg0_reg_n_0_[24] ;
  wire \slv_reg0_reg_n_0_[25] ;
  wire \slv_reg0_reg_n_0_[26] ;
  wire \slv_reg0_reg_n_0_[27] ;
  wire \slv_reg0_reg_n_0_[28] ;
  wire \slv_reg0_reg_n_0_[29] ;
  wire \slv_reg0_reg_n_0_[2] ;
  wire \slv_reg0_reg_n_0_[30] ;
  wire \slv_reg0_reg_n_0_[31] ;
  wire \slv_reg0_reg_n_0_[3] ;
  wire \slv_reg0_reg_n_0_[4] ;
  wire \slv_reg0_reg_n_0_[5] ;
  wire \slv_reg0_reg_n_0_[6] ;
  wire \slv_reg0_reg_n_0_[7] ;
  wire \slv_reg0_reg_n_0_[8] ;
  wire \slv_reg0_reg_n_0_[9] ;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [15:0]slv_reg1_D;
  wire \slv_reg1_reg_n_0_[0] ;
  wire \slv_reg1_reg_n_0_[10] ;
  wire \slv_reg1_reg_n_0_[11] ;
  wire \slv_reg1_reg_n_0_[12] ;
  wire \slv_reg1_reg_n_0_[13] ;
  wire \slv_reg1_reg_n_0_[14] ;
  wire \slv_reg1_reg_n_0_[15] ;
  wire \slv_reg1_reg_n_0_[16] ;
  wire \slv_reg1_reg_n_0_[17] ;
  wire \slv_reg1_reg_n_0_[18] ;
  wire \slv_reg1_reg_n_0_[19] ;
  wire \slv_reg1_reg_n_0_[1] ;
  wire \slv_reg1_reg_n_0_[20] ;
  wire \slv_reg1_reg_n_0_[21] ;
  wire \slv_reg1_reg_n_0_[22] ;
  wire \slv_reg1_reg_n_0_[23] ;
  wire \slv_reg1_reg_n_0_[24] ;
  wire \slv_reg1_reg_n_0_[25] ;
  wire \slv_reg1_reg_n_0_[26] ;
  wire \slv_reg1_reg_n_0_[27] ;
  wire \slv_reg1_reg_n_0_[28] ;
  wire \slv_reg1_reg_n_0_[29] ;
  wire \slv_reg1_reg_n_0_[2] ;
  wire \slv_reg1_reg_n_0_[30] ;
  wire \slv_reg1_reg_n_0_[31] ;
  wire \slv_reg1_reg_n_0_[3] ;
  wire \slv_reg1_reg_n_0_[4] ;
  wire \slv_reg1_reg_n_0_[5] ;
  wire \slv_reg1_reg_n_0_[6] ;
  wire \slv_reg1_reg_n_0_[7] ;
  wire \slv_reg1_reg_n_0_[8] ;
  wire \slv_reg1_reg_n_0_[9] ;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire \slv_reg2_D[4]_i_1_n_0 ;
  wire \slv_reg2_D_reg_n_0_[0] ;
  wire \slv_reg2_D_reg_n_0_[1] ;
  wire \slv_reg2_D_reg_n_0_[2] ;
  wire \slv_reg2_D_reg_n_0_[3] ;
  wire \slv_reg2_reg_n_0_[0] ;
  wire \slv_reg2_reg_n_0_[10] ;
  wire \slv_reg2_reg_n_0_[11] ;
  wire \slv_reg2_reg_n_0_[12] ;
  wire \slv_reg2_reg_n_0_[13] ;
  wire \slv_reg2_reg_n_0_[14] ;
  wire \slv_reg2_reg_n_0_[15] ;
  wire \slv_reg2_reg_n_0_[16] ;
  wire \slv_reg2_reg_n_0_[17] ;
  wire \slv_reg2_reg_n_0_[18] ;
  wire \slv_reg2_reg_n_0_[19] ;
  wire \slv_reg2_reg_n_0_[1] ;
  wire \slv_reg2_reg_n_0_[20] ;
  wire \slv_reg2_reg_n_0_[21] ;
  wire \slv_reg2_reg_n_0_[22] ;
  wire \slv_reg2_reg_n_0_[23] ;
  wire \slv_reg2_reg_n_0_[24] ;
  wire \slv_reg2_reg_n_0_[25] ;
  wire \slv_reg2_reg_n_0_[26] ;
  wire \slv_reg2_reg_n_0_[27] ;
  wire \slv_reg2_reg_n_0_[28] ;
  wire \slv_reg2_reg_n_0_[29] ;
  wire \slv_reg2_reg_n_0_[2] ;
  wire \slv_reg2_reg_n_0_[30] ;
  wire \slv_reg2_reg_n_0_[31] ;
  wire \slv_reg2_reg_n_0_[3] ;
  wire \slv_reg2_reg_n_0_[4] ;
  wire \slv_reg2_reg_n_0_[5] ;
  wire \slv_reg2_reg_n_0_[6] ;
  wire \slv_reg2_reg_n_0_[7] ;
  wire \slv_reg2_reg_n_0_[8] ;
  wire \slv_reg2_reg_n_0_[9] ;
  wire [31:0]slv_reg3;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire slv_reg_rden;
  wire slv_reg_wren__2;
  wire [15:0]x;
  wire [15:0]y;

  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .S(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[0]_i_1 
       (.I0(\slv_reg1_reg_n_0_[0] ),
        .I1(\slv_reg0_reg_n_0_[0] ),
        .I2(slv_reg3[0]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[0] ),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[10]_i_1 
       (.I0(\slv_reg1_reg_n_0_[10] ),
        .I1(\slv_reg0_reg_n_0_[10] ),
        .I2(slv_reg3[10]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[10] ),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[11]_i_1 
       (.I0(\slv_reg1_reg_n_0_[11] ),
        .I1(\slv_reg0_reg_n_0_[11] ),
        .I2(slv_reg3[11]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[11] ),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[12]_i_1 
       (.I0(\slv_reg1_reg_n_0_[12] ),
        .I1(\slv_reg0_reg_n_0_[12] ),
        .I2(slv_reg3[12]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[12] ),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[13]_i_1 
       (.I0(\slv_reg1_reg_n_0_[13] ),
        .I1(\slv_reg0_reg_n_0_[13] ),
        .I2(slv_reg3[13]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[13] ),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[14]_i_1 
       (.I0(\slv_reg1_reg_n_0_[14] ),
        .I1(\slv_reg0_reg_n_0_[14] ),
        .I2(slv_reg3[14]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[14] ),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[15]_i_1 
       (.I0(\slv_reg1_reg_n_0_[15] ),
        .I1(\slv_reg0_reg_n_0_[15] ),
        .I2(slv_reg3[15]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[15] ),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[16]_i_1 
       (.I0(\slv_reg1_reg_n_0_[16] ),
        .I1(\slv_reg0_reg_n_0_[16] ),
        .I2(slv_reg3[16]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[16] ),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[17]_i_1 
       (.I0(\slv_reg1_reg_n_0_[17] ),
        .I1(\slv_reg0_reg_n_0_[17] ),
        .I2(slv_reg3[17]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[17] ),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[18]_i_1 
       (.I0(\slv_reg1_reg_n_0_[18] ),
        .I1(\slv_reg0_reg_n_0_[18] ),
        .I2(slv_reg3[18]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[18] ),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[19]_i_1 
       (.I0(\slv_reg1_reg_n_0_[19] ),
        .I1(\slv_reg0_reg_n_0_[19] ),
        .I2(slv_reg3[19]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[19] ),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[1]_i_1 
       (.I0(\slv_reg1_reg_n_0_[1] ),
        .I1(\slv_reg0_reg_n_0_[1] ),
        .I2(slv_reg3[1]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[1] ),
        .O(reg_data_out[1]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[20]_i_1 
       (.I0(\slv_reg1_reg_n_0_[20] ),
        .I1(\slv_reg0_reg_n_0_[20] ),
        .I2(slv_reg3[20]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[20] ),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[21]_i_1 
       (.I0(\slv_reg1_reg_n_0_[21] ),
        .I1(\slv_reg0_reg_n_0_[21] ),
        .I2(slv_reg3[21]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[21] ),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[22]_i_1 
       (.I0(\slv_reg1_reg_n_0_[22] ),
        .I1(\slv_reg0_reg_n_0_[22] ),
        .I2(slv_reg3[22]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[22] ),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[23]_i_1 
       (.I0(\slv_reg1_reg_n_0_[23] ),
        .I1(\slv_reg0_reg_n_0_[23] ),
        .I2(slv_reg3[23]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[23] ),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[24]_i_1 
       (.I0(\slv_reg1_reg_n_0_[24] ),
        .I1(\slv_reg0_reg_n_0_[24] ),
        .I2(slv_reg3[24]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[24] ),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[25]_i_1 
       (.I0(\slv_reg1_reg_n_0_[25] ),
        .I1(\slv_reg0_reg_n_0_[25] ),
        .I2(slv_reg3[25]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[25] ),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[26]_i_1 
       (.I0(\slv_reg1_reg_n_0_[26] ),
        .I1(\slv_reg0_reg_n_0_[26] ),
        .I2(slv_reg3[26]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[26] ),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[27]_i_1 
       (.I0(\slv_reg1_reg_n_0_[27] ),
        .I1(\slv_reg0_reg_n_0_[27] ),
        .I2(slv_reg3[27]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[27] ),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[28]_i_1 
       (.I0(\slv_reg1_reg_n_0_[28] ),
        .I1(\slv_reg0_reg_n_0_[28] ),
        .I2(slv_reg3[28]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[28] ),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[29]_i_1 
       (.I0(\slv_reg1_reg_n_0_[29] ),
        .I1(\slv_reg0_reg_n_0_[29] ),
        .I2(slv_reg3[29]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[29] ),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[2]_i_1 
       (.I0(\slv_reg1_reg_n_0_[2] ),
        .I1(\slv_reg0_reg_n_0_[2] ),
        .I2(slv_reg3[2]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[2] ),
        .O(reg_data_out[2]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[30]_i_1 
       (.I0(\slv_reg1_reg_n_0_[30] ),
        .I1(\slv_reg0_reg_n_0_[30] ),
        .I2(slv_reg3[30]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[30] ),
        .O(reg_data_out[30]));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[31]_i_2 
       (.I0(\slv_reg1_reg_n_0_[31] ),
        .I1(\slv_reg0_reg_n_0_[31] ),
        .I2(slv_reg3[31]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[31] ),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[3]_i_1 
       (.I0(\slv_reg1_reg_n_0_[3] ),
        .I1(\slv_reg0_reg_n_0_[3] ),
        .I2(slv_reg3[3]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[3] ),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[4]_i_1 
       (.I0(\slv_reg1_reg_n_0_[4] ),
        .I1(\slv_reg0_reg_n_0_[4] ),
        .I2(slv_reg3[4]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[4] ),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[5]_i_1 
       (.I0(\slv_reg1_reg_n_0_[5] ),
        .I1(\slv_reg0_reg_n_0_[5] ),
        .I2(slv_reg3[5]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[5] ),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[6]_i_1 
       (.I0(\slv_reg1_reg_n_0_[6] ),
        .I1(\slv_reg0_reg_n_0_[6] ),
        .I2(slv_reg3[6]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[6] ),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[7]_i_1 
       (.I0(\slv_reg1_reg_n_0_[7] ),
        .I1(\slv_reg0_reg_n_0_[7] ),
        .I2(slv_reg3[7]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[7] ),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[8]_i_1 
       (.I0(\slv_reg1_reg_n_0_[8] ),
        .I1(\slv_reg0_reg_n_0_[8] ),
        .I2(slv_reg3[8]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[8] ),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[9]_i_1 
       (.I0(\slv_reg1_reg_n_0_[9] ),
        .I1(\slv_reg0_reg_n_0_[9] ),
        .I2(slv_reg3[9]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg2_reg_n_0_[9] ),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blr blr_int
       (.Q({blr_enable,\slv_reg2_D_reg_n_0_[3] ,\slv_reg2_D_reg_n_0_[2] ,\slv_reg2_D_reg_n_0_[1] ,\slv_reg2_D_reg_n_0_[0] }),
        .clk(clk),
        .en1_carry__0_0(slv_reg1_D),
        .\en1_inferred__0/i__carry__0_0 (slv_reg0_D),
        .resetn(resetn),
        .x(x),
        .y(y));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[1]),
        .O(\slv_reg0[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[2]),
        .O(\slv_reg0[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[3]),
        .O(\slv_reg0[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[0]),
        .O(\slv_reg0[7]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[0] ),
        .Q(slv_reg0_D[0]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[10] ),
        .Q(slv_reg0_D[10]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[11] ),
        .Q(slv_reg0_D[11]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[12] ),
        .Q(slv_reg0_D[12]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[13] ),
        .Q(slv_reg0_D[13]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[14] ),
        .Q(slv_reg0_D[14]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[15] ),
        .Q(slv_reg0_D[15]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[1] ),
        .Q(slv_reg0_D[1]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[2] ),
        .Q(slv_reg0_D[2]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[3] ),
        .Q(slv_reg0_D[3]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[4] ),
        .Q(slv_reg0_D[4]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[5] ),
        .Q(slv_reg0_D[5]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[6] ),
        .Q(slv_reg0_D[6]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[7] ),
        .Q(slv_reg0_D[7]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[8] ),
        .Q(slv_reg0_D[8]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[9] ),
        .Q(slv_reg0_D[9]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg0_reg_n_0_[0] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg0_reg_n_0_[10] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg0_reg_n_0_[11] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg0_reg_n_0_[12] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg0_reg_n_0_[13] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg0_reg_n_0_[14] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg0_reg_n_0_[15] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg0_reg_n_0_[16] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg0_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg0_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg0_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg0_reg_n_0_[1] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg0_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg0_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg0_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg0_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg0_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg0_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg0_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg0_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg0_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg0_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg0_reg_n_0_[2] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg0_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg0_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg0_reg_n_0_[3] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg0_reg_n_0_[4] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg0_reg_n_0_[5] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg0_reg_n_0_[6] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg0_reg_n_0_[7] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg0_reg_n_0_[8] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg0_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[0] ),
        .Q(slv_reg1_D[0]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[10] ),
        .Q(slv_reg1_D[10]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[11] ),
        .Q(slv_reg1_D[11]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[12] ),
        .Q(slv_reg1_D[12]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[13] ),
        .Q(slv_reg1_D[13]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[14] ),
        .Q(slv_reg1_D[14]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[15] ),
        .Q(slv_reg1_D[15]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[1] ),
        .Q(slv_reg1_D[1]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[2] ),
        .Q(slv_reg1_D[2]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[3] ),
        .Q(slv_reg1_D[3]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[4] ),
        .Q(slv_reg1_D[4]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[5] ),
        .Q(slv_reg1_D[5]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[6] ),
        .Q(slv_reg1_D[6]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[7] ),
        .Q(slv_reg1_D[7]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[8] ),
        .Q(slv_reg1_D[8]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[9] ),
        .Q(slv_reg1_D[9]),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg1_reg_n_0_[0] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg1_reg_n_0_[10] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg1_reg_n_0_[11] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg1_reg_n_0_[12] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg1_reg_n_0_[13] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg1_reg_n_0_[14] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg1_reg_n_0_[15] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg1_reg_n_0_[16] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg1_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg1_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg1_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg1_reg_n_0_[1] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg1_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg1_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg1_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg1_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg1_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg1_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg1_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg1_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg1_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg1_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg1_reg_n_0_[2] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg1_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg1_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg1_reg_n_0_[3] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg1_reg_n_0_[4] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg1_reg_n_0_[5] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg1_reg_n_0_[6] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg1_reg_n_0_[7] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg1_reg_n_0_[8] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg1_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg2[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \slv_reg2_D[4]_i_1 
       (.I0(resetn),
        .O(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg2_D_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[0] ),
        .Q(\slv_reg2_D_reg_n_0_[0] ),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg2_D_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[1] ),
        .Q(\slv_reg2_D_reg_n_0_[1] ),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg2_D_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[2] ),
        .Q(\slv_reg2_D_reg_n_0_[2] ),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg2_D_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[3] ),
        .Q(\slv_reg2_D_reg_n_0_[3] ),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg2_D_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[4] ),
        .Q(blr_enable),
        .R(\slv_reg2_D[4]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg2_reg_n_0_[0] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg2_reg_n_0_[10] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg2_reg_n_0_[11] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg2_reg_n_0_[12] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg2_reg_n_0_[13] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg2_reg_n_0_[14] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg2_reg_n_0_[15] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg2_reg_n_0_[16] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg2_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg2_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg2_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg2_reg_n_0_[1] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg2_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg2_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg2_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg2_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg2_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg2_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg2_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg2_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg2_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg2_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg2_reg_n_0_[2] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg2_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg2_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg2_reg_n_0_[3] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg2_reg_n_0_[4] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg2_reg_n_0_[5] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg2_reg_n_0_[6] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg2_reg_n_0_[7] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg2_reg_n_0_[8] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg2_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(axi_awready_i_1_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sdp_ram
   (S,
    \acc32_reg[7] ,
    \acc32_reg[11] ,
    \acc32_reg[15] ,
    \acc32_reg[19] ,
    \acc32_reg[23] ,
    \acc32_reg[27] ,
    \acc32_reg[31] ,
    clk,
    en,
    Q,
    DIADI);
  output [3:0]S;
  output [3:0]\acc32_reg[7] ;
  output [3:0]\acc32_reg[11] ;
  output [3:0]\acc32_reg[15] ;
  output [3:0]\acc32_reg[19] ;
  output [3:0]\acc32_reg[23] ;
  output [3:0]\acc32_reg[27] ;
  output [3:0]\acc32_reg[31] ;
  input clk;
  input en;
  input [9:0]Q;
  input [31:0]DIADI;

  wire [31:0]DIADI;
  wire [9:0]Q;
  wire [3:0]S;
  wire [3:0]\acc32_reg[11] ;
  wire [3:0]\acc32_reg[15] ;
  wire [3:0]\acc32_reg[19] ;
  wire [3:0]\acc32_reg[23] ;
  wire [3:0]\acc32_reg[27] ;
  wire [3:0]\acc32_reg[31] ;
  wire [3:0]\acc32_reg[7] ;
  wire clk;
  wire [31:0]dob;
  wire en;
  wire NLW_RAM_reg_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_DOADO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "32768" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "1023" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "1023" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(36),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(36),
    .WRITE_WIDTH_B(36)) 
    RAM_reg
       (.ADDRARDADDR({1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DBITERR(NLW_RAM_reg_DBITERR_UNCONNECTED),
        .DIADI(DIADI),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_DOADO_UNCONNECTED[31:0]),
        .DOBDO(dob),
        .DOPADOP(NLW_RAM_reg_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_SBITERR_UNCONNECTED),
        .WEA({1'b1,1'b1,1'b1,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__0_i_1
       (.I0(DIADI[7]),
        .I1(dob[7]),
        .O(\acc32_reg[7] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__0_i_2
       (.I0(DIADI[6]),
        .I1(dob[6]),
        .O(\acc32_reg[7] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__0_i_3
       (.I0(DIADI[5]),
        .I1(dob[5]),
        .O(\acc32_reg[7] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__0_i_4
       (.I0(DIADI[4]),
        .I1(dob[4]),
        .O(\acc32_reg[7] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__1_i_1
       (.I0(DIADI[11]),
        .I1(dob[11]),
        .O(\acc32_reg[11] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__1_i_2
       (.I0(DIADI[10]),
        .I1(dob[10]),
        .O(\acc32_reg[11] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__1_i_3
       (.I0(DIADI[9]),
        .I1(dob[9]),
        .O(\acc32_reg[11] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__1_i_4
       (.I0(DIADI[8]),
        .I1(dob[8]),
        .O(\acc32_reg[11] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__2_i_1
       (.I0(DIADI[15]),
        .I1(dob[15]),
        .O(\acc32_reg[15] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__2_i_2
       (.I0(DIADI[14]),
        .I1(dob[14]),
        .O(\acc32_reg[15] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__2_i_3
       (.I0(DIADI[13]),
        .I1(dob[13]),
        .O(\acc32_reg[15] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__2_i_4
       (.I0(DIADI[12]),
        .I1(dob[12]),
        .O(\acc32_reg[15] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__3_i_1
       (.I0(DIADI[19]),
        .I1(dob[19]),
        .O(\acc32_reg[19] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__3_i_2
       (.I0(DIADI[18]),
        .I1(dob[18]),
        .O(\acc32_reg[19] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__3_i_3
       (.I0(DIADI[17]),
        .I1(dob[17]),
        .O(\acc32_reg[19] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__3_i_4
       (.I0(DIADI[16]),
        .I1(dob[16]),
        .O(\acc32_reg[19] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__4_i_1
       (.I0(DIADI[23]),
        .I1(dob[23]),
        .O(\acc32_reg[23] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__4_i_2
       (.I0(DIADI[22]),
        .I1(dob[22]),
        .O(\acc32_reg[23] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__4_i_3
       (.I0(DIADI[21]),
        .I1(dob[21]),
        .O(\acc32_reg[23] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__4_i_4
       (.I0(DIADI[20]),
        .I1(dob[20]),
        .O(\acc32_reg[23] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__5_i_1
       (.I0(DIADI[27]),
        .I1(dob[27]),
        .O(\acc32_reg[27] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__5_i_2
       (.I0(DIADI[26]),
        .I1(dob[26]),
        .O(\acc32_reg[27] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__5_i_3
       (.I0(DIADI[25]),
        .I1(dob[25]),
        .O(\acc32_reg[27] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__5_i_4
       (.I0(DIADI[24]),
        .I1(dob[24]),
        .O(\acc32_reg[27] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__6_i_1
       (.I0(DIADI[31]),
        .I1(dob[31]),
        .O(\acc32_reg[31] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__6_i_2
       (.I0(DIADI[30]),
        .I1(dob[30]),
        .O(\acc32_reg[31] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__6_i_3
       (.I0(DIADI[29]),
        .I1(dob[29]),
        .O(\acc32_reg[31] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry__6_i_4
       (.I0(DIADI[28]),
        .I1(dob[28]),
        .O(\acc32_reg[31] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry_i_1
       (.I0(DIADI[3]),
        .I1(dob[3]),
        .O(S[3]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry_i_2
       (.I0(DIADI[2]),
        .I1(dob[2]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry_i_3
       (.I0(DIADI[1]),
        .I1(dob[1]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h9)) 
    u320_carry_i_4
       (.I0(DIADI[0]),
        .I1(dob[0]),
        .O(S[0]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
