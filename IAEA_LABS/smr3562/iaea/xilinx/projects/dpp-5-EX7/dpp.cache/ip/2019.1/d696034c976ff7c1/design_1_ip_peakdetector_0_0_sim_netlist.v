// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 18:31:11 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_peakdetector_0_0_sim_netlist.v
// Design      : design_1_ip_peakdetector_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface
   (i,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    d,
    \slv_reg_array_reg[2][15] ,
    \slv_reg_array_reg[3][15] ,
    \slv_reg_array_reg[4][9] ,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rvalid,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wdata,
    axibusdomain_clk,
    axibusdomain_s_axi_awaddr,
    axibusdomain_aresetn,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_rready);
  output [15:0]i;
  output axibusdomain_s_axi_awready;
  output axibusdomain_s_axi_wready;
  output [0:0]d;
  output [15:0]\slv_reg_array_reg[2][15] ;
  output [15:0]\slv_reg_array_reg[3][15] ;
  output [9:0]\slv_reg_array_reg[4][9] ;
  output axibusdomain_s_axi_arready;
  output [31:0]axibusdomain_s_axi_rdata;
  output axibusdomain_s_axi_rvalid;
  output axibusdomain_s_axi_bvalid;
  input [3:0]axibusdomain_s_axi_wstrb;
  input [31:0]axibusdomain_s_axi_wdata;
  input axibusdomain_clk;
  input [4:0]axibusdomain_s_axi_awaddr;
  input axibusdomain_aresetn;
  input [4:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_awvalid;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_bready;
  input axibusdomain_s_axi_rready;

  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [4:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [4:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [0:0]d;
  wire [15:0]i;
  wire [15:0]\slv_reg_array_reg[2][15] ;
  wire [15:0]\slv_reg_array_reg[3][15] ;
  wire [9:0]\slv_reg_array_reg[4][9] ;

  LUT6 #(
    .INIT(64'h55555555C0000000)) 
    axi_bvalid_i_1
       (.I0(axibusdomain_s_axi_bready),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_awready),
        .I4(axibusdomain_s_axi_wready),
        .I5(axibusdomain_s_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h0F88)) 
    axi_rvalid_i_1
       (.I0(axibusdomain_s_axi_arready),
        .I1(axibusdomain_s_axi_arvalid),
        .I2(axibusdomain_s_axi_rready),
        .I3(axibusdomain_s_axi_rvalid),
        .O(axi_rvalid_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog inst
       (.axi_arready_reg_0(axibusdomain_s_axi_arready),
        .axi_awready_reg_0(axibusdomain_s_axi_awready),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .axi_rvalid_reg_0(axi_rvalid_i_1_n_0),
        .axi_wready_reg_0(axibusdomain_s_axi_wready),
        .axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .d(d),
        .i(i),
        .\slv_reg_array_reg[2][15]_0 (\slv_reg_array_reg[2][15] ),
        .\slv_reg_array_reg[3][15]_0 (\slv_reg_array_reg[3][15] ),
        .\slv_reg_array_reg[4][9]_0 (\slv_reg_array_reg[4][9] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog
   (axi_awready_reg_0,
    axi_wready_reg_0,
    axi_arready_reg_0,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_rvalid,
    i,
    d,
    \slv_reg_array_reg[2][15]_0 ,
    \slv_reg_array_reg[3][15]_0 ,
    \slv_reg_array_reg[4][9]_0 ,
    axibusdomain_s_axi_rdata,
    axibusdomain_clk,
    axi_bvalid_reg_0,
    axi_rvalid_reg_0,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_awaddr,
    axibusdomain_aresetn,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_awvalid);
  output axi_awready_reg_0;
  output axi_wready_reg_0;
  output axi_arready_reg_0;
  output axibusdomain_s_axi_bvalid;
  output axibusdomain_s_axi_rvalid;
  output [15:0]i;
  output [0:0]d;
  output [15:0]\slv_reg_array_reg[2][15]_0 ;
  output [15:0]\slv_reg_array_reg[3][15]_0 ;
  output [9:0]\slv_reg_array_reg[4][9]_0 ;
  output [31:0]axibusdomain_s_axi_rdata;
  input axibusdomain_clk;
  input axi_bvalid_reg_0;
  input axi_rvalid_reg_0;
  input [3:0]axibusdomain_s_axi_wstrb;
  input [31:0]axibusdomain_s_axi_wdata;
  input [4:0]axibusdomain_s_axi_awaddr;
  input axibusdomain_aresetn;
  input [4:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_awvalid;

  wire [4:0]axi_araddr;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire [4:0]axi_awaddr;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [4:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arvalid;
  wire [4:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [0:0]d;
  wire [2:0]dec_r__10;
  wire [2:0]dec_w__10;
  wire [15:0]i;
  wire p_0_in;
  wire [31:10]r4_x_delay;
  wire [31:16]r5_x_noise;
  wire [31:16]r6_x_min;
  wire [31:16]r7_x_max;
  wire [31:1]r8_en_pkd;
  wire \slv_reg_array[0][0]_i_1_n_0 ;
  wire \slv_reg_array[0][0]_i_2_n_0 ;
  wire \slv_reg_array[0][15]_i_1_n_0 ;
  wire \slv_reg_array[0][15]_i_2_n_0 ;
  wire \slv_reg_array[0][23]_i_1_n_0 ;
  wire \slv_reg_array[0][23]_i_2_n_0 ;
  wire \slv_reg_array[0][31]_i_1_n_0 ;
  wire \slv_reg_array[0][31]_i_2_n_0 ;
  wire \slv_reg_array[0][31]_i_3_n_0 ;
  wire \slv_reg_array[0][31]_i_5_n_0 ;
  wire \slv_reg_array[0][7]_i_1_n_0 ;
  wire \slv_reg_array[0][7]_i_2_n_0 ;
  wire \slv_reg_array[0][7]_i_3_n_0 ;
  wire \slv_reg_array[1][0]_i_1_n_0 ;
  wire \slv_reg_array[1][0]_i_2_n_0 ;
  wire \slv_reg_array[1][14]_i_1_n_0 ;
  wire \slv_reg_array[1][14]_i_2_n_0 ;
  wire \slv_reg_array[1][15]_i_1_n_0 ;
  wire \slv_reg_array[1][1]_i_1_n_0 ;
  wire \slv_reg_array[1][23]_i_1_n_0 ;
  wire \slv_reg_array[1][23]_i_2_n_0 ;
  wire \slv_reg_array[1][31]_i_1_n_0 ;
  wire \slv_reg_array[1][31]_i_2_n_0 ;
  wire \slv_reg_array[1][31]_i_4_n_0 ;
  wire \slv_reg_array[1][5]_i_1_n_0 ;
  wire \slv_reg_array[1][6]_i_1_n_0 ;
  wire \slv_reg_array[1][6]_i_2_n_0 ;
  wire \slv_reg_array[1][7]_i_1_n_0 ;
  wire \slv_reg_array[2][0]_i_1_n_0 ;
  wire \slv_reg_array[2][0]_i_2_n_0 ;
  wire \slv_reg_array[2][15]_i_1_n_0 ;
  wire \slv_reg_array[2][15]_i_2_n_0 ;
  wire \slv_reg_array[2][1]_i_1_n_0 ;
  wire \slv_reg_array[2][23]_i_1_n_0 ;
  wire \slv_reg_array[2][23]_i_2_n_0 ;
  wire \slv_reg_array[2][31]_i_1_n_0 ;
  wire \slv_reg_array[2][31]_i_2_n_0 ;
  wire \slv_reg_array[2][31]_i_4_n_0 ;
  wire \slv_reg_array[2][5]_i_1_n_0 ;
  wire \slv_reg_array[2][6]_i_1_n_0 ;
  wire \slv_reg_array[2][6]_i_2_n_0 ;
  wire \slv_reg_array[2][7]_i_1_n_0 ;
  wire \slv_reg_array[2][7]_i_2_n_0 ;
  wire \slv_reg_array[3][0]_i_1_n_0 ;
  wire \slv_reg_array[3][0]_i_2_n_0 ;
  wire \slv_reg_array[3][15]_i_1_n_0 ;
  wire \slv_reg_array[3][15]_i_2_n_0 ;
  wire \slv_reg_array[3][23]_i_1_n_0 ;
  wire \slv_reg_array[3][23]_i_2_n_0 ;
  wire \slv_reg_array[3][31]_i_1_n_0 ;
  wire \slv_reg_array[3][31]_i_2_n_0 ;
  wire \slv_reg_array[3][31]_i_4_n_0 ;
  wire \slv_reg_array[3][4]_i_1_n_0 ;
  wire \slv_reg_array[3][6]_i_1_n_0 ;
  wire \slv_reg_array[3][6]_i_2_n_0 ;
  wire \slv_reg_array[3][7]_i_1_n_0 ;
  wire \slv_reg_array[3][7]_i_2_n_0 ;
  wire \slv_reg_array[4][0]_i_1_n_0 ;
  wire \slv_reg_array[4][0]_i_2_n_0 ;
  wire \slv_reg_array[4][15]_i_1_n_0 ;
  wire \slv_reg_array[4][15]_i_2_n_0 ;
  wire \slv_reg_array[4][1]_i_1_n_0 ;
  wire \slv_reg_array[4][23]_i_1_n_0 ;
  wire \slv_reg_array[4][23]_i_2_n_0 ;
  wire \slv_reg_array[4][2]_i_1_n_0 ;
  wire \slv_reg_array[4][31]_i_1_n_0 ;
  wire \slv_reg_array[4][31]_i_2_n_0 ;
  wire \slv_reg_array[4][31]_i_4_n_0 ;
  wire \slv_reg_array[4][3]_i_1_n_0 ;
  wire \slv_reg_array[4][3]_i_2_n_0 ;
  wire \slv_reg_array[4][7]_i_1_n_0 ;
  wire \slv_reg_array[4][7]_i_2_n_0 ;
  wire [7:7]\slv_reg_array_reg[0] ;
  wire [7:7]\slv_reg_array_reg[1] ;
  wire [7:7]\slv_reg_array_reg[2] ;
  wire [15:0]\slv_reg_array_reg[2][15]_0 ;
  wire [7:7]\slv_reg_array_reg[3] ;
  wire [15:0]\slv_reg_array_reg[3][15]_0 ;
  wire [7:7]\slv_reg_array_reg[4] ;
  wire [9:0]\slv_reg_array_reg[4][9]_0 ;
  wire slv_reg_rden__0;
  wire [31:0]slv_wire_array;

  FDRE \axi_araddr_reg[0] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[0]),
        .Q(axi_araddr[0]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[1] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[1]),
        .Q(axi_araddr[1]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[2] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[2]),
        .Q(axi_araddr[2]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[3] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[3]),
        .Q(axi_araddr[3]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[4] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[4]),
        .Q(axi_araddr[4]),
        .R(p_0_in));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(axibusdomain_s_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[0] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[0]),
        .Q(axi_awaddr[0]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[1] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[1]),
        .Q(axi_awaddr[1]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[2] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[2]),
        .Q(axi_awaddr[2]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[3]),
        .Q(axi_awaddr[3]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[4]),
        .Q(axi_awaddr[4]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(axibusdomain_aresetn),
        .O(p_0_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(axibusdomain_s_axi_awvalid),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axi_awready_reg_0),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(p_0_in));
  FDRE axi_bvalid_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(axibusdomain_s_axi_bvalid),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[0]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [0]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[0]_i_2_n_0 ),
        .O(slv_wire_array[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [0]),
        .I1(\slv_reg_array_reg[2][15]_0 [0]),
        .I2(dec_r__10[1]),
        .I3(i[0]),
        .I4(dec_r__10[0]),
        .I5(d),
        .O(\axi_rdata[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[10]_i_1 
       (.I0(r4_x_delay[10]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[10]_i_2_n_0 ),
        .O(slv_wire_array[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [10]),
        .I1(\slv_reg_array_reg[2][15]_0 [10]),
        .I2(dec_r__10[1]),
        .I3(i[10]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[11]_i_1 
       (.I0(r4_x_delay[11]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[11]_i_2_n_0 ),
        .O(slv_wire_array[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [11]),
        .I1(\slv_reg_array_reg[2][15]_0 [11]),
        .I2(dec_r__10[1]),
        .I3(i[11]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[12]_i_1 
       (.I0(r4_x_delay[12]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[12]_i_2_n_0 ),
        .O(slv_wire_array[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [12]),
        .I1(\slv_reg_array_reg[2][15]_0 [12]),
        .I2(dec_r__10[1]),
        .I3(i[12]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[13]_i_1 
       (.I0(r4_x_delay[13]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[13]_i_2_n_0 ),
        .O(slv_wire_array[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [13]),
        .I1(\slv_reg_array_reg[2][15]_0 [13]),
        .I2(dec_r__10[1]),
        .I3(i[13]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[14]_i_1 
       (.I0(r4_x_delay[14]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[14]_i_2_n_0 ),
        .O(slv_wire_array[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [14]),
        .I1(\slv_reg_array_reg[2][15]_0 [14]),
        .I2(dec_r__10[1]),
        .I3(i[14]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[15]_i_1 
       (.I0(r4_x_delay[15]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[15]_i_2_n_0 ),
        .O(slv_wire_array[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [15]),
        .I1(\slv_reg_array_reg[2][15]_0 [15]),
        .I2(dec_r__10[1]),
        .I3(i[15]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[16]_i_1 
       (.I0(r4_x_delay[16]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[16]_i_2_n_0 ),
        .O(slv_wire_array[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(r5_x_noise[16]),
        .I1(r6_x_min[16]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[16]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[17]_i_1 
       (.I0(r4_x_delay[17]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[17]_i_2_n_0 ),
        .O(slv_wire_array[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(r5_x_noise[17]),
        .I1(r6_x_min[17]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[17]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[18]_i_1 
       (.I0(r4_x_delay[18]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[18]_i_2_n_0 ),
        .O(slv_wire_array[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(r5_x_noise[18]),
        .I1(r6_x_min[18]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[18]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[19]_i_1 
       (.I0(r4_x_delay[19]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[19]_i_2_n_0 ),
        .O(slv_wire_array[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_2 
       (.I0(r5_x_noise[19]),
        .I1(r6_x_min[19]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[19]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[1]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [1]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[1]_i_2_n_0 ),
        .O(slv_wire_array[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [1]),
        .I1(\slv_reg_array_reg[2][15]_0 [1]),
        .I2(dec_r__10[1]),
        .I3(i[1]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[20]_i_1 
       (.I0(r4_x_delay[20]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[20]_i_2_n_0 ),
        .O(slv_wire_array[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(r5_x_noise[20]),
        .I1(r6_x_min[20]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[20]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[21]_i_1 
       (.I0(r4_x_delay[21]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[21]_i_2_n_0 ),
        .O(slv_wire_array[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(r5_x_noise[21]),
        .I1(r6_x_min[21]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[21]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[22]_i_1 
       (.I0(r4_x_delay[22]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[22]_i_2_n_0 ),
        .O(slv_wire_array[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(r5_x_noise[22]),
        .I1(r6_x_min[22]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[22]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[23]_i_1 
       (.I0(r4_x_delay[23]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[23]_i_2_n_0 ),
        .O(slv_wire_array[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_2 
       (.I0(r5_x_noise[23]),
        .I1(r6_x_min[23]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[23]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[24]_i_1 
       (.I0(r4_x_delay[24]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[24]_i_2_n_0 ),
        .O(slv_wire_array[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(r5_x_noise[24]),
        .I1(r6_x_min[24]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[24]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[25]_i_1 
       (.I0(r4_x_delay[25]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[25]_i_2_n_0 ),
        .O(slv_wire_array[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(r5_x_noise[25]),
        .I1(r6_x_min[25]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[25]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[26]_i_1 
       (.I0(r4_x_delay[26]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[26]_i_2_n_0 ),
        .O(slv_wire_array[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(r5_x_noise[26]),
        .I1(r6_x_min[26]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[26]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[27]_i_1 
       (.I0(r4_x_delay[27]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[27]_i_2_n_0 ),
        .O(slv_wire_array[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(r5_x_noise[27]),
        .I1(r6_x_min[27]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[27]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[28]_i_1 
       (.I0(r4_x_delay[28]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[28]_i_2_n_0 ),
        .O(slv_wire_array[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(r5_x_noise[28]),
        .I1(r6_x_min[28]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[28]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[29]_i_1 
       (.I0(r4_x_delay[29]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[29]_i_2_n_0 ),
        .O(slv_wire_array[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(r5_x_noise[29]),
        .I1(r6_x_min[29]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[29]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[2]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [2]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[2]_i_2_n_0 ),
        .O(slv_wire_array[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [2]),
        .I1(\slv_reg_array_reg[2][15]_0 [2]),
        .I2(dec_r__10[1]),
        .I3(i[2]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[30]_i_1 
       (.I0(r4_x_delay[30]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[30]_i_2_n_0 ),
        .O(slv_wire_array[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(r5_x_noise[30]),
        .I1(r6_x_min[30]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[30]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[31]_i_1 
       (.I0(r4_x_delay[31]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[31]_i_3_n_0 ),
        .O(slv_wire_array[31]));
  LUT5 #(
    .INIT(32'h00000002)) 
    \axi_rdata[31]_i_2 
       (.I0(axi_araddr[4]),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[1]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[0]),
        .O(dec_r__10[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_3 
       (.I0(r5_x_noise[31]),
        .I1(r6_x_min[31]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[31]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_4 
       (.I0(axi_araddr[4]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[1]),
        .O(dec_r__10[1]));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_5 
       (.I0(axi_araddr[4]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[1]),
        .O(dec_r__10[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[3]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [3]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[3]_i_2_n_0 ),
        .O(slv_wire_array[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [3]),
        .I1(\slv_reg_array_reg[2][15]_0 [3]),
        .I2(dec_r__10[1]),
        .I3(i[3]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[4]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [4]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[4]_i_2_n_0 ),
        .O(slv_wire_array[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [4]),
        .I1(\slv_reg_array_reg[2][15]_0 [4]),
        .I2(dec_r__10[1]),
        .I3(i[4]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[5]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [5]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[5]_i_2_n_0 ),
        .O(slv_wire_array[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [5]),
        .I1(\slv_reg_array_reg[2][15]_0 [5]),
        .I2(dec_r__10[1]),
        .I3(i[5]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[6]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [6]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[6]_i_2_n_0 ),
        .O(slv_wire_array[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [6]),
        .I1(\slv_reg_array_reg[2][15]_0 [6]),
        .I2(dec_r__10[1]),
        .I3(i[6]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[7]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [7]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[7]_i_2_n_0 ),
        .O(slv_wire_array[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [7]),
        .I1(\slv_reg_array_reg[2][15]_0 [7]),
        .I2(dec_r__10[1]),
        .I3(i[7]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[8]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [8]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[8]_i_2_n_0 ),
        .O(slv_wire_array[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [8]),
        .I1(\slv_reg_array_reg[2][15]_0 [8]),
        .I2(dec_r__10[1]),
        .I3(i[8]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[9]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [9]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[9]_i_2_n_0 ),
        .O(slv_wire_array[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [9]),
        .I1(\slv_reg_array_reg[2][15]_0 [9]),
        .I2(dec_r__10[1]),
        .I3(i[9]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[0]),
        .Q(axibusdomain_s_axi_rdata[0]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[10] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[10]),
        .Q(axibusdomain_s_axi_rdata[10]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[11] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[11]),
        .Q(axibusdomain_s_axi_rdata[11]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[12] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[12]),
        .Q(axibusdomain_s_axi_rdata[12]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[13] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[13]),
        .Q(axibusdomain_s_axi_rdata[13]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[14] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[14]),
        .Q(axibusdomain_s_axi_rdata[14]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[15] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[15]),
        .Q(axibusdomain_s_axi_rdata[15]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[16] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[16]),
        .Q(axibusdomain_s_axi_rdata[16]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[17] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[17]),
        .Q(axibusdomain_s_axi_rdata[17]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[18] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[18]),
        .Q(axibusdomain_s_axi_rdata[18]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[19] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[19]),
        .Q(axibusdomain_s_axi_rdata[19]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[1] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[1]),
        .Q(axibusdomain_s_axi_rdata[1]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[20] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[20]),
        .Q(axibusdomain_s_axi_rdata[20]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[21] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[21]),
        .Q(axibusdomain_s_axi_rdata[21]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[22] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[22]),
        .Q(axibusdomain_s_axi_rdata[22]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[23] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[23]),
        .Q(axibusdomain_s_axi_rdata[23]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[24] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[24]),
        .Q(axibusdomain_s_axi_rdata[24]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[25] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[25]),
        .Q(axibusdomain_s_axi_rdata[25]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[26] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[26]),
        .Q(axibusdomain_s_axi_rdata[26]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[27] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[27]),
        .Q(axibusdomain_s_axi_rdata[27]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[28] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[28]),
        .Q(axibusdomain_s_axi_rdata[28]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[29] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[29]),
        .Q(axibusdomain_s_axi_rdata[29]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[2] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[2]),
        .Q(axibusdomain_s_axi_rdata[2]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[30] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[30]),
        .Q(axibusdomain_s_axi_rdata[30]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[31] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[31]),
        .Q(axibusdomain_s_axi_rdata[31]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[3] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[3]),
        .Q(axibusdomain_s_axi_rdata[3]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[4] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[4]),
        .Q(axibusdomain_s_axi_rdata[4]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[5] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[5]),
        .Q(axibusdomain_s_axi_rdata[5]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[6] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[6]),
        .Q(axibusdomain_s_axi_rdata[6]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[7] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[7]),
        .Q(axibusdomain_s_axi_rdata[7]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[8] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[8]),
        .Q(axibusdomain_s_axi_rdata[8]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[9] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[9]),
        .Q(axibusdomain_s_axi_rdata[9]),
        .R(p_0_in));
  FDRE axi_rvalid_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_rvalid_reg_0),
        .Q(axibusdomain_s_axi_rvalid),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(axibusdomain_s_axi_awvalid),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axi_wready_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hFDFFFDFDFD00FDFD)) 
    \slv_reg_array[0][0]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[0][31]_i_5_n_0 ),
        .I2(axibusdomain_s_axi_wdata[0]),
        .I3(\slv_reg_array[0][0]_i_2_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(d),
        .O(\slv_reg_array[0][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[0][0]_i_2 
       (.I0(\slv_reg_array_reg[0] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[0][0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \slv_reg_array[0][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[0] ),
        .I3(\slv_reg_array[0][31]_i_5_n_0 ),
        .O(\slv_reg_array[0][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[0][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[0] ),
        .O(\slv_reg_array[0][15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \slv_reg_array[0][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[0] ),
        .I3(\slv_reg_array[0][31]_i_5_n_0 ),
        .O(\slv_reg_array[0][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[0][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[0] ),
        .O(\slv_reg_array[0][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \slv_reg_array[0][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[0] ),
        .I3(\slv_reg_array[0][31]_i_5_n_0 ),
        .O(\slv_reg_array[0][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[0][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[0] ),
        .O(\slv_reg_array[0][31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \slv_reg_array[0][31]_i_3 
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .I2(axi_wready_reg_0),
        .I3(axi_awready_reg_0),
        .I4(axibusdomain_aresetn),
        .O(\slv_reg_array[0][31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFE1FFFFFFFF)) 
    \slv_reg_array[0][31]_i_4 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[0] ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAABBBE)) 
    \slv_reg_array[0][31]_i_5 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[1]),
        .I5(axi_awaddr[0]),
        .O(\slv_reg_array[0][31]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD0)) 
    \slv_reg_array[0][7]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[0][31]_i_5_n_0 ),
        .I2(\slv_reg_array[0][7]_i_2_n_0 ),
        .O(\slv_reg_array[0][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[0][7]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[0] ),
        .O(\slv_reg_array[0][7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[0][7]_i_3 
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .O(\slv_reg_array[0][7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h80FF808080008080)) 
    \slv_reg_array[1][0]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[0]),
        .I3(\slv_reg_array[1][0]_i_2_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(i[0]),
        .O(\slv_reg_array[1][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[1][0]_i_2 
       (.I0(\slv_reg_array_reg[1] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[1][0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000FB0000)) 
    \slv_reg_array[1][14]_i_1 
       (.I0(dec_w__10[2]),
        .I1(dec_w__10[0]),
        .I2(dec_w__10[1]),
        .I3(axibusdomain_aresetn),
        .I4(axibusdomain_s_axi_wstrb[1]),
        .I5(\slv_reg_array[0][31]_i_3_n_0 ),
        .O(\slv_reg_array[1][14]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[1][14]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[1] ),
        .O(\slv_reg_array[1][14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \slv_reg_array[1][14]_i_3 
       (.I0(axi_awaddr[0]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[3]),
        .I4(axi_awaddr[4]),
        .O(dec_w__10[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    \slv_reg_array[1][14]_i_4 
       (.I0(axi_awaddr[1]),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[2]),
        .O(dec_w__10[0]));
  LUT4 #(
    .INIT(16'h0100)) 
    \slv_reg_array[1][14]_i_5 
       (.I0(axi_awaddr[1]),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[3]),
        .O(dec_w__10[1]));
  LUT6 #(
    .INIT(64'hFFBFBFBF40000000)) 
    \slv_reg_array[1][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[1] ),
        .I3(axibusdomain_s_axi_wdata[15]),
        .I4(\slv_reg_array[1][31]_i_4_n_0 ),
        .I5(i[15]),
        .O(\slv_reg_array[1][15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80FF8000)) 
    \slv_reg_array[1][1]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[1]),
        .I3(\slv_reg_array[1][6]_i_2_n_0 ),
        .I4(i[1]),
        .O(\slv_reg_array[1][1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[1][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[1] ),
        .I3(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[1][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[1] ),
        .O(\slv_reg_array[1][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[1][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[1] ),
        .I3(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[1][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[1] ),
        .O(\slv_reg_array[1][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000010FFFFFFFF)) 
    \slv_reg_array[1][31]_i_3 
       (.I0(axi_awaddr[0]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[3]),
        .I4(axi_awaddr[4]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[1] ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \slv_reg_array[1][31]_i_4 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[1]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[1][31]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h80FF8000)) 
    \slv_reg_array[1][5]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[5]),
        .I3(\slv_reg_array[1][6]_i_2_n_0 ),
        .I4(i[5]),
        .O(\slv_reg_array[1][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \slv_reg_array[1][6]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(dec_w__10[2]),
        .I2(\slv_reg_array[0][31]_i_3_n_0 ),
        .I3(dec_w__10[1]),
        .I4(dec_w__10[0]),
        .I5(\slv_reg_array[1][6]_i_2_n_0 ),
        .O(\slv_reg_array[1][6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[1][6]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[1] ),
        .O(\slv_reg_array[1][6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h80FF8000)) 
    \slv_reg_array[1][7]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[7]),
        .I3(\slv_reg_array[1][6]_i_2_n_0 ),
        .I4(i[7]),
        .O(\slv_reg_array[1][7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEFEEE0EE)) 
    \slv_reg_array[2][0]_i_1 
       (.I0(\slv_reg_array[2][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[0]),
        .I2(\slv_reg_array[2][0]_i_2_n_0 ),
        .I3(axibusdomain_aresetn),
        .I4(\slv_reg_array_reg[2][15]_0 [0]),
        .O(\slv_reg_array[2][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[2][0]_i_2 
       (.I0(\slv_reg_array_reg[2] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[2][0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[2][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[2] ),
        .I3(\slv_reg_array[2][31]_i_4_n_0 ),
        .O(\slv_reg_array[2][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[2][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[2] ),
        .O(\slv_reg_array[2][15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[2][1]_i_1 
       (.I0(\slv_reg_array[2][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[1]),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[2][15]_0 [1]),
        .O(\slv_reg_array[2][1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[2][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[2] ),
        .I3(\slv_reg_array[2][31]_i_4_n_0 ),
        .O(\slv_reg_array[2][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[2][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[2] ),
        .O(\slv_reg_array[2][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[2][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[2] ),
        .I3(\slv_reg_array[2][31]_i_4_n_0 ),
        .O(\slv_reg_array[2][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[2][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[2] ),
        .O(\slv_reg_array[2][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000002FFFFFFFF)) 
    \slv_reg_array[2][31]_i_3 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[2] ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \slv_reg_array[2][31]_i_4 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[2]),
        .I5(axi_awaddr[3]),
        .O(\slv_reg_array[2][31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[2][5]_i_1 
       (.I0(\slv_reg_array[2][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[5]),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[2][15]_0 [5]),
        .O(\slv_reg_array[2][5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \slv_reg_array[2][6]_i_1 
       (.I0(\slv_reg_array[2][6]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[2][31]_i_4_n_0 ),
        .O(\slv_reg_array[2][6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[2][6]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[2] ),
        .O(\slv_reg_array[2][6]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[2][7]_i_1 
       (.I0(\slv_reg_array[2][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[7]),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[2][15]_0 [7]),
        .O(\slv_reg_array[2][7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFDFF)) 
    \slv_reg_array[2][7]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(dec_w__10[0]),
        .I2(dec_w__10[2]),
        .I3(dec_w__10[1]),
        .I4(\slv_reg_array[0][31]_i_3_n_0 ),
        .O(\slv_reg_array[2][7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFEEE0EE)) 
    \slv_reg_array[3][0]_i_1 
       (.I0(\slv_reg_array[3][6]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[0]),
        .I2(\slv_reg_array[3][0]_i_2_n_0 ),
        .I3(axibusdomain_aresetn),
        .I4(\slv_reg_array_reg[3][15]_0 [0]),
        .O(\slv_reg_array[3][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[3][0]_i_2 
       (.I0(\slv_reg_array_reg[3] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[3][0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[3][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[3] ),
        .I3(\slv_reg_array[3][31]_i_4_n_0 ),
        .O(\slv_reg_array[3][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[3] ),
        .O(\slv_reg_array[3][15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[3][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[3] ),
        .I3(\slv_reg_array[3][31]_i_4_n_0 ),
        .O(\slv_reg_array[3][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[3] ),
        .O(\slv_reg_array[3][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[3][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[3] ),
        .I3(\slv_reg_array[3][31]_i_4_n_0 ),
        .O(\slv_reg_array[3][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[3] ),
        .O(\slv_reg_array[3][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000008FFFFFFFF)) 
    \slv_reg_array[3][31]_i_3 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[3] ));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \slv_reg_array[3][31]_i_4 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[2]),
        .O(\slv_reg_array[3][31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[3][4]_i_1 
       (.I0(\slv_reg_array[3][6]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[4]),
        .I2(\slv_reg_array[3][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[3][15]_0 [4]),
        .O(\slv_reg_array[3][4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[3][6]_i_1 
       (.I0(\slv_reg_array[3][6]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[6]),
        .I2(\slv_reg_array[3][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[3][15]_0 [6]),
        .O(\slv_reg_array[3][6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFF7F)) 
    \slv_reg_array[3][6]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(dec_w__10[1]),
        .I2(dec_w__10[0]),
        .I3(dec_w__10[2]),
        .I4(\slv_reg_array[0][31]_i_3_n_0 ),
        .O(\slv_reg_array[3][6]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \slv_reg_array[3][7]_i_1 
       (.I0(\slv_reg_array[3][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[3][31]_i_4_n_0 ),
        .O(\slv_reg_array[3][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[3][7]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[3] ),
        .O(\slv_reg_array[3][7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFEEE0EE)) 
    \slv_reg_array[4][0]_i_1 
       (.I0(\slv_reg_array[4][3]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[0]),
        .I2(\slv_reg_array[4][0]_i_2_n_0 ),
        .I3(axibusdomain_aresetn),
        .I4(\slv_reg_array_reg[4][9]_0 [0]),
        .O(\slv_reg_array[4][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[4][0]_i_2 
       (.I0(\slv_reg_array_reg[4] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[4][0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[4][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[4] ),
        .I3(\slv_reg_array[4][31]_i_4_n_0 ),
        .O(\slv_reg_array[4][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[4][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[4] ),
        .O(\slv_reg_array[4][15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[4][1]_i_1 
       (.I0(\slv_reg_array[4][3]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[1]),
        .I2(\slv_reg_array[4][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[4][9]_0 [1]),
        .O(\slv_reg_array[4][1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[4][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[4] ),
        .I3(\slv_reg_array[4][31]_i_4_n_0 ),
        .O(\slv_reg_array[4][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[4][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[4] ),
        .O(\slv_reg_array[4][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[4][2]_i_1 
       (.I0(\slv_reg_array[4][3]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[2]),
        .I2(\slv_reg_array[4][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[4][9]_0 [2]),
        .O(\slv_reg_array[4][2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[4][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[4] ),
        .I3(\slv_reg_array[4][31]_i_4_n_0 ),
        .O(\slv_reg_array[4][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[4][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[4] ),
        .O(\slv_reg_array[4][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000010FFFFFFFF)) 
    \slv_reg_array[4][31]_i_3 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[4] ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \slv_reg_array[4][31]_i_4 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(\slv_reg_array[0][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[4][3]_i_1 
       (.I0(\slv_reg_array[4][3]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[3]),
        .I2(\slv_reg_array[4][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[4][9]_0 [3]),
        .O(\slv_reg_array[4][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFBFF)) 
    \slv_reg_array[4][3]_i_2 
       (.I0(dec_w__10[1]),
        .I1(dec_w__10[2]),
        .I2(\slv_reg_array[0][31]_i_3_n_0 ),
        .I3(axibusdomain_s_axi_wstrb[0]),
        .I4(dec_w__10[0]),
        .O(\slv_reg_array[4][3]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \slv_reg_array[4][7]_i_1 
       (.I0(\slv_reg_array[4][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[4][31]_i_4_n_0 ),
        .O(\slv_reg_array[4][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[4][7]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[4] ),
        .O(\slv_reg_array[4][7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[0][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[0][0]_i_1_n_0 ),
        .Q(d),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(r8_en_pkd[10]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(r8_en_pkd[11]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(r8_en_pkd[12]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(r8_en_pkd[13]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(r8_en_pkd[14]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[15]),
        .Q(r8_en_pkd[15]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r8_en_pkd[16]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r8_en_pkd[17]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r8_en_pkd[18]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r8_en_pkd[19]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[1]),
        .Q(r8_en_pkd[1]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r8_en_pkd[20]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r8_en_pkd[21]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r8_en_pkd[22]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r8_en_pkd[23]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r8_en_pkd[24]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r8_en_pkd[25]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r8_en_pkd[26]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r8_en_pkd[27]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r8_en_pkd[28]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r8_en_pkd[29]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[2]),
        .Q(r8_en_pkd[2]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r8_en_pkd[30]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r8_en_pkd[31]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[3]),
        .Q(r8_en_pkd[3]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[4]),
        .Q(r8_en_pkd[4]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[5]),
        .Q(r8_en_pkd[5]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[6]),
        .Q(r8_en_pkd[6]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[7]),
        .Q(r8_en_pkd[7]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(r8_en_pkd[8]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(r8_en_pkd[9]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][0]_i_1_n_0 ),
        .Q(i[0]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(i[10]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(i[11]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(i[12]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(i[13]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(i[14]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][15] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][15]_i_1_n_0 ),
        .Q(i[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r7_x_max[16]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r7_x_max[17]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r7_x_max[18]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r7_x_max[19]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][1] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][1]_i_1_n_0 ),
        .Q(i[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r7_x_max[20]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r7_x_max[21]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r7_x_max[22]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r7_x_max[23]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r7_x_max[24]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r7_x_max[25]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r7_x_max[26]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r7_x_max[27]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r7_x_max[28]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r7_x_max[29]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[2]),
        .Q(i[2]),
        .S(\slv_reg_array[1][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r7_x_max[30]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r7_x_max[31]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[3]),
        .Q(i[3]),
        .S(\slv_reg_array[1][6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[4]),
        .Q(i[4]),
        .S(\slv_reg_array[1][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][5] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][5]_i_1_n_0 ),
        .Q(i[5]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[6]),
        .Q(i[6]),
        .S(\slv_reg_array[1][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][7] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][7]_i_1_n_0 ),
        .Q(i[7]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(i[8]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(i[9]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[2][15]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(\slv_reg_array_reg[2][15]_0 [10]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(\slv_reg_array_reg[2][15]_0 [11]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(\slv_reg_array_reg[2][15]_0 [12]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(\slv_reg_array_reg[2][15]_0 [13]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(\slv_reg_array_reg[2][15]_0 [14]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[15]),
        .Q(\slv_reg_array_reg[2][15]_0 [15]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r6_x_min[16]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r6_x_min[17]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r6_x_min[18]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r6_x_min[19]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][1] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][1]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[2][15]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r6_x_min[20]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r6_x_min[21]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r6_x_min[22]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r6_x_min[23]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r6_x_min[24]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r6_x_min[25]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r6_x_min[26]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r6_x_min[27]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r6_x_min[28]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r6_x_min[29]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[2]),
        .Q(\slv_reg_array_reg[2][15]_0 [2]),
        .R(\slv_reg_array[2][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r6_x_min[30]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r6_x_min[31]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[3]),
        .Q(\slv_reg_array_reg[2][15]_0 [3]),
        .R(\slv_reg_array[2][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[4]),
        .Q(\slv_reg_array_reg[2][15]_0 [4]),
        .R(\slv_reg_array[2][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][5] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][5]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[2][15]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[6]),
        .Q(\slv_reg_array_reg[2][15]_0 [6]),
        .R(\slv_reg_array[2][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][7] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][7]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[2][15]_0 [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(\slv_reg_array_reg[2][15]_0 [8]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(\slv_reg_array_reg[2][15]_0 [9]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(\slv_reg_array_reg[3][15]_0 [10]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(\slv_reg_array_reg[3][15]_0 [11]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(\slv_reg_array_reg[3][15]_0 [12]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(\slv_reg_array_reg[3][15]_0 [13]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(\slv_reg_array_reg[3][15]_0 [14]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[15]),
        .Q(\slv_reg_array_reg[3][15]_0 [15]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r5_x_noise[16]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r5_x_noise[17]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r5_x_noise[18]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r5_x_noise[19]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[1]),
        .Q(\slv_reg_array_reg[3][15]_0 [1]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r5_x_noise[20]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r5_x_noise[21]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r5_x_noise[22]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r5_x_noise[23]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r5_x_noise[24]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r5_x_noise[25]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r5_x_noise[26]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r5_x_noise[27]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r5_x_noise[28]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r5_x_noise[29]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[2]),
        .Q(\slv_reg_array_reg[3][15]_0 [2]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r5_x_noise[30]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r5_x_noise[31]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[3]),
        .Q(\slv_reg_array_reg[3][15]_0 [3]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][4] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][4]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[5]),
        .Q(\slv_reg_array_reg[3][15]_0 [5]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][6] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][6]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[7]),
        .Q(\slv_reg_array_reg[3][15]_0 [7]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(\slv_reg_array_reg[3][15]_0 [8]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(\slv_reg_array_reg[3][15]_0 [9]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[4][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[4][9]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(r4_x_delay[10]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(r4_x_delay[11]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(r4_x_delay[12]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(r4_x_delay[13]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(r4_x_delay[14]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[15]),
        .Q(r4_x_delay[15]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r4_x_delay[16]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r4_x_delay[17]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r4_x_delay[18]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r4_x_delay[19]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[4][1] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][1]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[4][9]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r4_x_delay[20]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r4_x_delay[21]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r4_x_delay[22]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r4_x_delay[23]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r4_x_delay[24]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r4_x_delay[25]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r4_x_delay[26]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r4_x_delay[27]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r4_x_delay[28]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r4_x_delay[29]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[4][2] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][2]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[4][9]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r4_x_delay[30]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r4_x_delay[31]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[4][3] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][3]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[4][9]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[4]),
        .Q(\slv_reg_array_reg[4][9]_0 [4]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[5]),
        .Q(\slv_reg_array_reg[4][9]_0 [5]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[6]),
        .Q(\slv_reg_array_reg[4][9]_0 [6]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[7]),
        .Q(\slv_reg_array_reg[4][9]_0 [7]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(\slv_reg_array_reg[4][9]_0 [8]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(\slv_reg_array_reg[4][9]_0 [9]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(axibusdomain_s_axi_arvalid),
        .I1(axibusdomain_s_axi_rvalid),
        .I2(axi_arready_reg_0),
        .O(slv_reg_rden__0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convert_func_call_ip_peakdetector_xlconvert
   (O,
    \xmax_i_12_24_reg[9] ,
    \xmax_i_12_24_reg[13] ,
    CO,
    \xmax_i_12_24_reg[15] ,
    Q,
    S);
  output [3:0]O;
  output [3:0]\xmax_i_12_24_reg[9] ;
  output [3:0]\xmax_i_12_24_reg[13] ;
  output [0:0]CO;
  output [1:0]\xmax_i_12_24_reg[15] ;
  input [13:0]Q;
  input [0:0]S;

  wire [0:0]CO;
  wire [3:0]O;
  wire [13:0]Q;
  wire [0:0]S;
  wire inp_carry__0_n_0;
  wire inp_carry__0_n_1;
  wire inp_carry__0_n_2;
  wire inp_carry__0_n_3;
  wire inp_carry__1_n_0;
  wire inp_carry__1_n_1;
  wire inp_carry__1_n_2;
  wire inp_carry__1_n_3;
  wire inp_carry__2_n_3;
  wire inp_carry_n_0;
  wire inp_carry_n_1;
  wire inp_carry_n_2;
  wire inp_carry_n_3;
  wire [3:0]\xmax_i_12_24_reg[13] ;
  wire [1:0]\xmax_i_12_24_reg[15] ;
  wire [3:0]\xmax_i_12_24_reg[9] ;
  wire [3:1]NLW_inp_carry__2_CO_UNCONNECTED;
  wire [3:2]NLW_inp_carry__2_O_UNCONNECTED;

  CARRY4 inp_carry
       (.CI(1'b0),
        .CO({inp_carry_n_0,inp_carry_n_1,inp_carry_n_2,inp_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Q[0]}),
        .O(O),
        .S({Q[3:1],S}));
  CARRY4 inp_carry__0
       (.CI(inp_carry_n_0),
        .CO({inp_carry__0_n_0,inp_carry__0_n_1,inp_carry__0_n_2,inp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\xmax_i_12_24_reg[9] ),
        .S(Q[7:4]));
  CARRY4 inp_carry__1
       (.CI(inp_carry__0_n_0),
        .CO({inp_carry__1_n_0,inp_carry__1_n_1,inp_carry__1_n_2,inp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\xmax_i_12_24_reg[13] ),
        .S(Q[11:8]));
  CARRY4 inp_carry__2
       (.CI(inp_carry__1_n_0),
        .CO({NLW_inp_carry__2_CO_UNCONNECTED[3],CO,NLW_inp_carry__2_CO_UNCONNECTED[1],inp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({NLW_inp_carry__2_O_UNCONNECTED[3:2],\xmax_i_12_24_reg[15] }),
        .S({1'b0,1'b1,Q[13:12]}));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ip_peakdetector_0_0,ip_peakdetector,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "sysgen" *) 
(* X_CORE_INFO = "ip_peakdetector,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (x,
    signaldomain_clk,
    axibusdomain_clk,
    axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready,
    peak_amp,
    peak_amp_dac,
    peak_amp_rdy,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 x DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME x, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}" *) input [15:0]x;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signaldomain_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input signaldomain_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input axibusdomain_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input axibusdomain_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR" *) input [4:0]axibusdomain_s_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID" *) input axibusdomain_s_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA" *) input [31:0]axibusdomain_s_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB" *) input [3:0]axibusdomain_s_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID" *) input axibusdomain_s_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY" *) input axibusdomain_s_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR" *) input [4:0]axibusdomain_s_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID" *) input axibusdomain_s_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY" *) input axibusdomain_s_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 peak_amp DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME peak_amp, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}" *) output [15:0]peak_amp;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 peak_amp_dac DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME peak_amp_dac, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}" *) output [13:0]peak_amp_dac;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 peak_amp_rdy DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME peak_amp_rdy, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}, PortType data, PortType.PROP_SRC false" *) output [0:0]peak_amp_rdy;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY" *) output axibusdomain_s_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY" *) output axibusdomain_s_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP" *) output [1:0]axibusdomain_s_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID" *) output axibusdomain_s_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY" *) output axibusdomain_s_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA" *) output [31:0]axibusdomain_s_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP" *) output [1:0]axibusdomain_s_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output axibusdomain_s_axi_rvalid;

  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [4:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [4:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire [1:0]axibusdomain_s_axi_bresp;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire [1:0]axibusdomain_s_axi_rresp;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [15:0]peak_amp;
  wire [13:0]peak_amp_dac;
  wire [0:0]peak_amp_rdy;
  wire signaldomain_clk;
  wire [15:0]x;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector inst
       (.axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_arready),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_awready),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_bready),
        .axibusdomain_s_axi_bresp(axibusdomain_s_axi_bresp),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_rready),
        .axibusdomain_s_axi_rresp(axibusdomain_s_axi_rresp),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_wready),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .peak_amp(peak_amp),
        .peak_amp_dac(peak_amp_dac),
        .peak_amp_rdy(peak_amp_rdy),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector
   (x,
    signaldomain_clk,
    axibusdomain_clk,
    axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready,
    peak_amp,
    peak_amp_dac,
    peak_amp_rdy,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid);
  input [15:0]x;
  input signaldomain_clk;
  input axibusdomain_clk;
  input axibusdomain_aresetn;
  input [4:0]axibusdomain_s_axi_awaddr;
  input axibusdomain_s_axi_awvalid;
  input [31:0]axibusdomain_s_axi_wdata;
  input [3:0]axibusdomain_s_axi_wstrb;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_bready;
  input [4:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_rready;
  output [15:0]peak_amp;
  output [13:0]peak_amp_dac;
  output [0:0]peak_amp_rdy;
  output axibusdomain_s_axi_awready;
  output axibusdomain_s_axi_wready;
  output [1:0]axibusdomain_s_axi_bresp;
  output axibusdomain_s_axi_bvalid;
  output axibusdomain_s_axi_arready;
  output [31:0]axibusdomain_s_axi_rdata;
  output [1:0]axibusdomain_s_axi_rresp;
  output axibusdomain_s_axi_rvalid;

  wire \<const0> ;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [4:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [4:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [15:0]peak_amp;
  wire [13:0]peak_amp_dac;
  wire [0:0]peak_amp_rdy;
  wire [9:0]r4_x_delay;
  wire [15:0]r5_x_noise;
  wire [15:0]r6_x_min;
  wire [15:0]r7_x_max;
  wire [0:0]r8_en_pkd;
  wire signaldomain_clk;
  wire [15:0]x;

  assign axibusdomain_s_axi_bresp[1] = \<const0> ;
  assign axibusdomain_s_axi_bresp[0] = \<const0> ;
  assign axibusdomain_s_axi_rresp[1] = \<const0> ;
  assign axibusdomain_s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface axibusdomain_axi_lite_interface
       (.axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_arready),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_awready),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_bready),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_rready),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_wready),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .d(r8_en_pkd),
        .i(r7_x_max),
        .\slv_reg_array_reg[2][15] (r6_x_min),
        .\slv_reg_array_reg[3][15] (r5_x_noise),
        .\slv_reg_array_reg[4][9] (r4_x_delay));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_struct ip_peakdetector_struct
       (.Q(peak_amp),
        .axibusdomain_clk(axibusdomain_clk),
        .peak_amp_dac(peak_amp_dac),
        .peak_amp_rdy(peak_amp_rdy),
        .r4_x_delay(r4_x_delay),
        .r5_x_noise(r5_x_noise),
        .r6_x_min(r6_x_min),
        .r7_x_max(r7_x_max),
        .r8_en_pkd(r8_en_pkd),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

(* CHECK_LICENSE_TYPE = "ip_peakdetector_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0
   (clka,
    ena,
    wea,
    addra,
    dina,
    douta);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [15:0]dina;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [15:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [15:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [15:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.57205 mW" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ip_peakdetector_blk_mem_gen_i0.mem" *) 
  (* C_INIT_FILE_NAME = "ip_peakdetector_blk_mem_gen_i0.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "16" *) 
  (* C_READ_WIDTH_B = "16" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "READ_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "16" *) 
  (* C_WRITE_WIDTH_B = "16" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_family = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[15:0]),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[15:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "c_addsub_v12_0_13,Vivado 2019.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0
   (A,
    B,
    CLK,
    CE,
    S);
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [16:0]A;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [16:0]B;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [16:0]S;

  wire [16:0]A;
  wire [16:0]B;
  wire CE;
  wire CLK;
  wire [16:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "17" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "00000000000000000" *) 
  (* c_b_width = "17" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "17" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "ip_peakdetector_c_addsub_v12_0_i0" *) 
(* X_CORE_INFO = "c_addsub_v12_0_13,Vivado 2019.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2
   (A,
    B,
    CLK,
    CE,
    S);
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [16:0]A;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [16:0]B;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [16:0]S;

  wire [16:0]A;
  wire [16:0]B;
  wire CE;
  wire CLK;
  wire [16:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "17" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "00000000000000000" *) 
  (* c_b_width = "17" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "17" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ip_peakdetector_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "c_counter_binary_v12_0_13,Vivado 2019.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0
   (CLK,
    CE,
    SINIT,
    LOAD,
    L,
    Q);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 sinit_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef" *) input SINIT;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [9:0]L;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [9:0]Q;

  wire CE;
  wire CLK;
  wire [9:0]L;
  wire LOAD;
  wire [9:0]Q;
  wire SINIT;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_delayline
   (douta,
    DI,
    S,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    signaldomain_clk,
    x,
    \op_mem_37_22_reg[0] ,
    o);
  output [15:0]douta;
  output [3:0]DI;
  output [3:0]S;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  input signaldomain_clk;
  input [15:0]x;
  input [9:0]\op_mem_37_22_reg[0] ;
  input [15:0]o;

  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [3:0]DI;
  wire [3:0]S;
  wire counter_n_10;
  wire [9:0]counter_op_net;
  wire [15:0]douta;
  wire [15:0]o;
  wire [9:0]\op_mem_37_22_reg[0] ;
  wire relational_op_net;
  wire signaldomain_clk;
  wire [15:0]x;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlcounter_free counter
       (.LOAD(relational_op_net),
        .Q(counter_op_net),
        .S(counter_n_10),
        .\op_mem_37_22_reg[0] (\op_mem_37_22_reg[0] [9]),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_3828661dd3 relational
       (.LOAD(relational_op_net),
        .Q(counter_op_net[8:0]),
        .S(counter_n_10),
        .\op_mem_37_22_reg[0]_0 (\op_mem_37_22_reg[0] [8:0]),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlspram single_port_ram
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ),
        .DI(DI),
        .Q(counter_op_net),
        .S(S),
        .douta(douta),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_format_dac
   (O,
    \xmax_i_12_24_reg[9] ,
    \xmax_i_12_24_reg[13] ,
    CO,
    \xmax_i_12_24_reg[15] ,
    peak_amp_dac,
    Q,
    S,
    d,
    signaldomain_clk);
  output [3:0]O;
  output [3:0]\xmax_i_12_24_reg[9] ;
  output [3:0]\xmax_i_12_24_reg[13] ;
  output [0:0]CO;
  output [1:0]\xmax_i_12_24_reg[15] ;
  output [13:0]peak_amp_dac;
  input [13:0]Q;
  input [0:0]S;
  input [13:0]d;
  input signaldomain_clk;

  wire [0:0]CO;
  wire [3:0]O;
  wire [13:0]Q;
  wire [0:0]S;
  wire [12:0]convert4_dout_net;
  wire [13:0]d;
  wire [13:13]fully_2_1_bit;
  wire [13:0]peak_amp_dac;
  wire signaldomain_clk;
  wire [3:0]\xmax_i_12_24_reg[13] ;
  wire [1:0]\xmax_i_12_24_reg[15] ;
  wire [3:0]\xmax_i_12_24_reg[9] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlconvert convert4
       (.CO(CO),
        .D({fully_2_1_bit,convert4_dout_net}),
        .O(O),
        .Q(Q),
        .S(S),
        .d(d),
        .signaldomain_clk(signaldomain_clk),
        .\xmax_i_12_24_reg[13] (\xmax_i_12_24_reg[13] ),
        .\xmax_i_12_24_reg[15] (\xmax_i_12_24_reg[15] ),
        .\xmax_i_12_24_reg[9] (\xmax_i_12_24_reg[9] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_logical_80d79f42b8 logical
       (.D({fully_2_1_bit,convert4_dout_net}),
        .peak_amp_dac(peak_amp_dac),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_math
   (S,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    DI,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    \x[14] ,
    \x[6] ,
    \x[14]_0 ,
    \x[6]_0 ,
    x,
    o,
    signaldomain_clk,
    douta);
  output [3:0]S;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  output [3:0]DI;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  output [3:0]\x[14] ;
  output [3:0]\x[6] ;
  output [3:0]\x[14]_0 ;
  output [3:0]\x[6]_0 ;
  input [15:0]x;
  input [15:0]o;
  input signaldomain_clk;
  input [15:0]douta;

  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [3:0]DI;
  wire [3:0]S;
  wire [15:0]douta;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;
  wire [3:0]\x[14]_0 ;
  wire [3:0]\x[6] ;
  wire [3:0]\x[6]_0 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub__xdcDup__1 addsub
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ),
        .DI(DI),
        .S(S),
        .douta(douta),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub addsub1
       (.douta(douta),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ),
        .\x[14]_0 (\x[14]_0 ),
        .\x[6] (\x[6] ),
        .\x[6]_0 (\x[6]_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_peakdetector
   (Q,
    d,
    \xmax_i_12_24_reg[2] ,
    peak_amp_rdy,
    signaldomain_clk,
    x,
    o,
    DI,
    rel_61_16_carry__0,
    \state_15_23[0]_i_3 ,
    \state_15_23[0]_i_3_0 ,
    rel_35_16_carry__0,
    S,
    \state_15_23[1]_i_2 ,
    \state_15_23[1]_i_2_0 ,
    rel_55_30_carry__0,
    rel_55_30_carry__0_0,
    pkd_i_16_23_reg,
    pkd_i_16_23_reg_0,
    rel_55_16_carry__0,
    rel_55_16_carry__0_0,
    pkd_i_16_23_reg_1,
    pkd_i_16_23_reg_2,
    \op_mem_37_22_reg[0] ,
    q,
    CO,
    O,
    \reg_array[12].fde_used.u2 ,
    \reg_array[7].fde_used.u2 ,
    \reg_array[11].fde_used.u2 );
  output [15:0]Q;
  output [13:0]d;
  output [0:0]\xmax_i_12_24_reg[2] ;
  output [0:0]peak_amp_rdy;
  input signaldomain_clk;
  input [15:0]x;
  input [15:0]o;
  input [3:0]DI;
  input [3:0]rel_61_16_carry__0;
  input [3:0]\state_15_23[0]_i_3 ;
  input [3:0]\state_15_23[0]_i_3_0 ;
  input [3:0]rel_35_16_carry__0;
  input [3:0]S;
  input [3:0]\state_15_23[1]_i_2 ;
  input [3:0]\state_15_23[1]_i_2_0 ;
  input [3:0]rel_55_30_carry__0;
  input [3:0]rel_55_30_carry__0_0;
  input [3:0]pkd_i_16_23_reg;
  input [3:0]pkd_i_16_23_reg_0;
  input [3:0]rel_55_16_carry__0;
  input [3:0]rel_55_16_carry__0_0;
  input [3:0]pkd_i_16_23_reg_1;
  input [3:0]pkd_i_16_23_reg_2;
  input [9:0]\op_mem_37_22_reg[0] ;
  input [0:0]q;
  input [0:0]CO;
  input [3:0]O;
  input [1:0]\reg_array[12].fde_used.u2 ;
  input [3:0]\reg_array[7].fde_used.u2 ;
  input [3:0]\reg_array[11].fde_used.u2 ;

  wire [0:0]CO;
  wire [3:0]DI;
  wire [3:0]O;
  wire [15:0]Q;
  wire [3:0]S;
  wire [13:0]d;
  wire delayline_n_16;
  wire delayline_n_17;
  wire delayline_n_18;
  wire delayline_n_19;
  wire delayline_n_20;
  wire delayline_n_21;
  wire delayline_n_22;
  wire delayline_n_23;
  wire delayline_n_24;
  wire delayline_n_25;
  wire delayline_n_26;
  wire delayline_n_27;
  wire delayline_n_28;
  wire delayline_n_29;
  wire delayline_n_30;
  wire delayline_n_31;
  wire math_n_0;
  wire math_n_1;
  wire math_n_10;
  wire math_n_11;
  wire math_n_12;
  wire math_n_13;
  wire math_n_14;
  wire math_n_15;
  wire math_n_16;
  wire math_n_17;
  wire math_n_18;
  wire math_n_19;
  wire math_n_2;
  wire math_n_20;
  wire math_n_21;
  wire math_n_22;
  wire math_n_23;
  wire math_n_24;
  wire math_n_25;
  wire math_n_26;
  wire math_n_27;
  wire math_n_28;
  wire math_n_29;
  wire math_n_3;
  wire math_n_30;
  wire math_n_31;
  wire math_n_4;
  wire math_n_5;
  wire math_n_6;
  wire math_n_7;
  wire math_n_8;
  wire math_n_9;
  wire mcode_cmp_en_net;
  wire mcode_cmp_rst_net;
  wire mcode_pkd_net;
  wire [15:0]o;
  wire [9:0]\op_mem_37_22_reg[0] ;
  wire [0:0]peak_amp_rdy;
  wire [3:0]pkd_i_16_23_reg;
  wire [3:0]pkd_i_16_23_reg_0;
  wire [3:0]pkd_i_16_23_reg_1;
  wire [3:0]pkd_i_16_23_reg_2;
  wire [0:0]q;
  wire [3:0]\reg_array[11].fde_used.u2 ;
  wire [1:0]\reg_array[12].fde_used.u2 ;
  wire [3:0]\reg_array[7].fde_used.u2 ;
  wire [3:0]rel_35_16_carry__0;
  wire [3:0]rel_55_16_carry__0;
  wire [3:0]rel_55_16_carry__0_0;
  wire [3:0]rel_55_30_carry__0;
  wire [3:0]rel_55_30_carry__0_0;
  wire [3:0]rel_61_16_carry__0;
  wire risingedgetrigger_n_0;
  wire signaldomain_clk;
  wire [15:0]single_port_ram_data_out_net;
  wire [3:0]\state_15_23[0]_i_3 ;
  wire [3:0]\state_15_23[0]_i_3_0 ;
  wire [3:0]\state_15_23[1]_i_2 ;
  wire [3:0]\state_15_23[1]_i_2_0 ;
  wire [15:0]x;
  wire [0:0]\xmax_i_12_24_reg[2] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_delayline delayline
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ({delayline_n_24,delayline_n_25,delayline_n_26,delayline_n_27}),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ({delayline_n_28,delayline_n_29,delayline_n_30,delayline_n_31}),
        .DI({delayline_n_16,delayline_n_17,delayline_n_18,delayline_n_19}),
        .S({delayline_n_20,delayline_n_21,delayline_n_22,delayline_n_23}),
        .douta(single_port_ram_data_out_net),
        .o(o),
        .\op_mem_37_22_reg[0] (\op_mem_37_22_reg[0] ),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_math math
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ({math_n_4,math_n_5,math_n_6,math_n_7}),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ({math_n_12,math_n_13,math_n_14,math_n_15}),
        .DI({math_n_8,math_n_9,math_n_10,math_n_11}),
        .S({math_n_0,math_n_1,math_n_2,math_n_3}),
        .douta(single_port_ram_data_out_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] ({math_n_16,math_n_17,math_n_18,math_n_19}),
        .\x[14]_0 ({math_n_24,math_n_25,math_n_26,math_n_27}),
        .\x[6] ({math_n_20,math_n_21,math_n_22,math_n_23}),
        .\x[6]_0 ({math_n_28,math_n_29,math_n_30,math_n_31}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_a80b0cfdd5 mcode
       (.DI({math_n_8,math_n_9,math_n_10,math_n_11}),
        .S({math_n_0,math_n_1,math_n_2,math_n_3}),
        .SR(mcode_cmp_rst_net),
        .mcode_cmp_en_net(mcode_cmp_en_net),
        .mcode_pkd_net(mcode_pkd_net),
        .peak_amp_rdy(peak_amp_rdy),
        .peak_amp_rdy_0_sp_1(risingedgetrigger_n_0),
        .pkd_i_16_23_reg_0(pkd_i_16_23_reg),
        .pkd_i_16_23_reg_1(pkd_i_16_23_reg_0),
        .pkd_i_16_23_reg_2(pkd_i_16_23_reg_1),
        .pkd_i_16_23_reg_3(pkd_i_16_23_reg_2),
        .q(q),
        .rel_35_16_carry__0_0(rel_35_16_carry__0),
        .rel_35_16_carry__0_1(S),
        .rel_47_16_carry__0_0({math_n_28,math_n_29,math_n_30,math_n_31}),
        .rel_47_16_carry__0_1({math_n_20,math_n_21,math_n_22,math_n_23}),
        .rel_55_16_carry__0_0(rel_55_16_carry__0),
        .rel_55_16_carry__0_1(rel_55_16_carry__0_0),
        .rel_55_30_carry__0_0(rel_55_30_carry__0),
        .rel_55_30_carry__0_1(rel_55_30_carry__0_0),
        .rel_61_16_carry__0_0(DI),
        .rel_61_16_carry__0_1(rel_61_16_carry__0),
        .rel_61_30_carry__0_0({delayline_n_16,delayline_n_17,delayline_n_18,delayline_n_19}),
        .rel_61_30_carry__0_1({delayline_n_20,delayline_n_21,delayline_n_22,delayline_n_23}),
        .rel_63_21_carry__0_0({math_n_12,math_n_13,math_n_14,math_n_15}),
        .rel_63_21_carry__0_1({math_n_4,math_n_5,math_n_6,math_n_7}),
        .signaldomain_clk(signaldomain_clk),
        .\state_15_23[0]_i_3_0 ({delayline_n_24,delayline_n_25,delayline_n_26,delayline_n_27}),
        .\state_15_23[0]_i_3_1 ({delayline_n_28,delayline_n_29,delayline_n_30,delayline_n_31}),
        .\state_15_23[0]_i_3_2 (\state_15_23[0]_i_3 ),
        .\state_15_23[0]_i_3_3 (\state_15_23[0]_i_3_0 ),
        .\state_15_23[1]_i_2_0 (\state_15_23[1]_i_2 ),
        .\state_15_23[1]_i_2_1 (\state_15_23[1]_i_2_0 ),
        .\state_15_23[2]_i_2_0 ({math_n_24,math_n_25,math_n_26,math_n_27}),
        .\state_15_23[2]_i_2_1 ({math_n_16,math_n_17,math_n_18,math_n_19}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c3d4c1851 mcode1
       (.CO(CO),
        .O(O),
        .Q(Q),
        .SR(mcode_cmp_rst_net),
        .d(d),
        .mcode_cmp_en_net(mcode_cmp_en_net),
        .\reg_array[11].fde_used.u2 (\reg_array[11].fde_used.u2 ),
        .\reg_array[12].fde_used.u2 (\reg_array[12].fde_used.u2 ),
        .\reg_array[7].fde_used.u2 (\reg_array[7].fde_used.u2 ),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\xmax_i_12_24_reg[2]_0 (\xmax_i_12_24_reg[2] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_risingedgetrigger risingedgetrigger
       (.\ff_2_17_reg[0] (risingedgetrigger_n_0),
        .mcode_pkd_net(mcode_pkd_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_risingedgetrigger
   (\ff_2_17_reg[0] ,
    mcode_pkd_net,
    signaldomain_clk);
  output \ff_2_17_reg[0] ;
  input mcode_pkd_net;
  input signaldomain_clk;

  wire \ff_2_17_reg[0] ;
  wire mcode_pkd_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c4ff59dc3 mcode
       (.\ff_2_17_reg[0]_0 (\ff_2_17_reg[0] ),
        .mcode_pkd_net(mcode_pkd_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_signaldomain
   (Q,
    peak_amp_rdy,
    peak_amp_dac,
    signaldomain_clk,
    x,
    o,
    DI,
    rel_61_16_carry__0,
    \state_15_23[0]_i_3 ,
    \state_15_23[0]_i_3_0 ,
    rel_35_16_carry__0,
    S,
    \state_15_23[1]_i_2 ,
    \state_15_23[1]_i_2_0 ,
    rel_55_30_carry__0,
    rel_55_30_carry__0_0,
    pkd_i_16_23_reg,
    pkd_i_16_23_reg_0,
    rel_55_16_carry__0,
    rel_55_16_carry__0_0,
    pkd_i_16_23_reg_1,
    pkd_i_16_23_reg_2,
    \op_mem_37_22_reg[0] ,
    q);
  output [15:0]Q;
  output [0:0]peak_amp_rdy;
  output [13:0]peak_amp_dac;
  input signaldomain_clk;
  input [15:0]x;
  input [15:0]o;
  input [3:0]DI;
  input [3:0]rel_61_16_carry__0;
  input [3:0]\state_15_23[0]_i_3 ;
  input [3:0]\state_15_23[0]_i_3_0 ;
  input [3:0]rel_35_16_carry__0;
  input [3:0]S;
  input [3:0]\state_15_23[1]_i_2 ;
  input [3:0]\state_15_23[1]_i_2_0 ;
  input [3:0]rel_55_30_carry__0;
  input [3:0]rel_55_30_carry__0_0;
  input [3:0]pkd_i_16_23_reg;
  input [3:0]pkd_i_16_23_reg_0;
  input [3:0]rel_55_16_carry__0;
  input [3:0]rel_55_16_carry__0_0;
  input [3:0]pkd_i_16_23_reg_1;
  input [3:0]pkd_i_16_23_reg_2;
  input [9:0]\op_mem_37_22_reg[0] ;
  input [0:0]q;

  wire [3:0]DI;
  wire [15:0]Q;
  wire [3:0]S;
  wire [13:0]\convert4/result ;
  wire format_dac_n_0;
  wire format_dac_n_1;
  wire format_dac_n_10;
  wire format_dac_n_11;
  wire format_dac_n_12;
  wire format_dac_n_13;
  wire format_dac_n_14;
  wire format_dac_n_2;
  wire format_dac_n_3;
  wire format_dac_n_4;
  wire format_dac_n_5;
  wire format_dac_n_6;
  wire format_dac_n_7;
  wire format_dac_n_8;
  wire format_dac_n_9;
  wire [15:0]o;
  wire [9:0]\op_mem_37_22_reg[0] ;
  wire [13:0]peak_amp_dac;
  wire [0:0]peak_amp_rdy;
  wire peakdetector_n_30;
  wire [3:0]pkd_i_16_23_reg;
  wire [3:0]pkd_i_16_23_reg_0;
  wire [3:0]pkd_i_16_23_reg_1;
  wire [3:0]pkd_i_16_23_reg_2;
  wire [0:0]q;
  wire [3:0]rel_35_16_carry__0;
  wire [3:0]rel_55_16_carry__0;
  wire [3:0]rel_55_16_carry__0_0;
  wire [3:0]rel_55_30_carry__0;
  wire [3:0]rel_55_30_carry__0_0;
  wire [3:0]rel_61_16_carry__0;
  wire signaldomain_clk;
  wire [3:0]\state_15_23[0]_i_3 ;
  wire [3:0]\state_15_23[0]_i_3_0 ;
  wire [3:0]\state_15_23[1]_i_2 ;
  wire [3:0]\state_15_23[1]_i_2_0 ;
  wire [15:0]x;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_format_dac format_dac
       (.CO(format_dac_n_12),
        .O({format_dac_n_0,format_dac_n_1,format_dac_n_2,format_dac_n_3}),
        .Q(Q[15:2]),
        .S(peakdetector_n_30),
        .d(\convert4/result ),
        .peak_amp_dac(peak_amp_dac),
        .signaldomain_clk(signaldomain_clk),
        .\xmax_i_12_24_reg[13] ({format_dac_n_8,format_dac_n_9,format_dac_n_10,format_dac_n_11}),
        .\xmax_i_12_24_reg[15] ({format_dac_n_13,format_dac_n_14}),
        .\xmax_i_12_24_reg[9] ({format_dac_n_4,format_dac_n_5,format_dac_n_6,format_dac_n_7}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_peakdetector peakdetector
       (.CO(format_dac_n_12),
        .DI(DI),
        .O({format_dac_n_0,format_dac_n_1,format_dac_n_2,format_dac_n_3}),
        .Q(Q),
        .S(S),
        .d(\convert4/result ),
        .o(o),
        .\op_mem_37_22_reg[0] (\op_mem_37_22_reg[0] ),
        .peak_amp_rdy(peak_amp_rdy),
        .pkd_i_16_23_reg(pkd_i_16_23_reg),
        .pkd_i_16_23_reg_0(pkd_i_16_23_reg_0),
        .pkd_i_16_23_reg_1(pkd_i_16_23_reg_1),
        .pkd_i_16_23_reg_2(pkd_i_16_23_reg_2),
        .q(q),
        .\reg_array[11].fde_used.u2 ({format_dac_n_8,format_dac_n_9,format_dac_n_10,format_dac_n_11}),
        .\reg_array[12].fde_used.u2 ({format_dac_n_13,format_dac_n_14}),
        .\reg_array[7].fde_used.u2 ({format_dac_n_4,format_dac_n_5,format_dac_n_6,format_dac_n_7}),
        .rel_35_16_carry__0(rel_35_16_carry__0),
        .rel_55_16_carry__0(rel_55_16_carry__0),
        .rel_55_16_carry__0_0(rel_55_16_carry__0_0),
        .rel_55_30_carry__0(rel_55_30_carry__0),
        .rel_55_30_carry__0_0(rel_55_30_carry__0_0),
        .rel_61_16_carry__0(rel_61_16_carry__0),
        .signaldomain_clk(signaldomain_clk),
        .\state_15_23[0]_i_3 (\state_15_23[0]_i_3 ),
        .\state_15_23[0]_i_3_0 (\state_15_23[0]_i_3_0 ),
        .\state_15_23[1]_i_2 (\state_15_23[1]_i_2 ),
        .\state_15_23[1]_i_2_0 (\state_15_23[1]_i_2_0 ),
        .x(x),
        .\xmax_i_12_24_reg[2] (peakdetector_n_30));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_struct
   (Q,
    peak_amp_dac,
    peak_amp_rdy,
    signaldomain_clk,
    x,
    r4_x_delay,
    axibusdomain_clk,
    r5_x_noise,
    r6_x_min,
    r7_x_max,
    r8_en_pkd);
  output [15:0]Q;
  output [13:0]peak_amp_dac;
  output [0:0]peak_amp_rdy;
  input signaldomain_clk;
  input [15:0]x;
  input [9:0]r4_x_delay;
  input axibusdomain_clk;
  input [15:0]r5_x_noise;
  input [15:0]r6_x_min;
  input [15:0]r7_x_max;
  input [0:0]r8_en_pkd;

  wire [15:0]Q;
  wire axibusdomain_clk;
  wire [13:0]peak_amp_dac;
  wire [0:0]peak_amp_rdy;
  wire [9:0]r4_x_delay;
  wire [15:0]r5_x_noise;
  wire [15:0]r6_x_min;
  wire [15:0]r7_x_max;
  wire [0:0]r8_en_pkd;
  wire register1_n_0;
  wire register1_n_1;
  wire register1_n_2;
  wire register1_n_20;
  wire register1_n_21;
  wire register1_n_22;
  wire register1_n_23;
  wire register1_n_24;
  wire register1_n_25;
  wire register1_n_26;
  wire register1_n_27;
  wire register1_n_28;
  wire register1_n_29;
  wire register1_n_3;
  wire register1_n_30;
  wire register1_n_31;
  wire register1_n_32;
  wire register1_n_33;
  wire register1_n_34;
  wire register1_n_35;
  wire register1_n_36;
  wire register1_n_37;
  wire register1_n_38;
  wire register1_n_39;
  wire register1_n_40;
  wire register1_n_41;
  wire register1_n_42;
  wire register1_n_43;
  wire register1_n_44;
  wire register1_n_45;
  wire register1_n_46;
  wire register1_n_47;
  wire [15:0]register1_q_net;
  wire register2_n_0;
  wire register2_n_1;
  wire register2_n_10;
  wire register2_n_11;
  wire register2_n_12;
  wire register2_n_13;
  wire register2_n_14;
  wire register2_n_15;
  wire register2_n_2;
  wire register2_n_3;
  wire register2_n_4;
  wire register2_n_5;
  wire register2_n_6;
  wire register2_n_7;
  wire register2_n_8;
  wire register2_n_9;
  wire register3_n_0;
  wire register3_n_1;
  wire register3_n_10;
  wire register3_n_11;
  wire register3_n_12;
  wire register3_n_13;
  wire register3_n_14;
  wire register3_n_15;
  wire register3_n_2;
  wire register3_n_3;
  wire register3_n_4;
  wire register3_n_5;
  wire register3_n_6;
  wire register3_n_7;
  wire register3_n_8;
  wire register3_n_9;
  wire [0:0]register4_q_net;
  wire [9:0]register_q_net;
  wire signaldomain_clk;
  wire [15:0]x;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister register1
       (.DI({register1_n_20,register1_n_21,register1_n_22,register1_n_23}),
        .S({register1_n_0,register1_n_1,register1_n_2,register1_n_3}),
        .axibusdomain_clk(axibusdomain_clk),
        .\fd_prim_array[14].bit_is_0.fdre_comp ({register1_n_24,register1_n_25,register1_n_26,register1_n_27}),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 ({register1_n_32,register1_n_33,register1_n_34,register1_n_35}),
        .\fd_prim_array[14].bit_is_0.fdre_comp_1 ({register1_n_44,register1_n_45,register1_n_46,register1_n_47}),
        .\fd_prim_array[6].bit_is_0.fdre_comp ({register1_n_36,register1_n_37,register1_n_38,register1_n_39}),
        .\fd_prim_array[6].bit_is_0.fdre_comp_0 ({register1_n_40,register1_n_41,register1_n_42,register1_n_43}),
        .o(register1_q_net),
        .r5_x_noise(r5_x_noise),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] ({register1_n_28,register1_n_29,register1_n_30,register1_n_31}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_0 register2
       (.DI({register2_n_8,register2_n_9,register2_n_10,register2_n_11}),
        .S({register2_n_0,register2_n_1,register2_n_2,register2_n_3}),
        .axibusdomain_clk(axibusdomain_clk),
        .\fd_prim_array[14].bit_is_0.fdre_comp ({register2_n_4,register2_n_5,register2_n_6,register2_n_7}),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 ({register2_n_12,register2_n_13,register2_n_14,register2_n_15}),
        .r6_x_min(r6_x_min),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_1 register3
       (.DI({register3_n_8,register3_n_9,register3_n_10,register3_n_11}),
        .S({register3_n_0,register3_n_1,register3_n_2,register3_n_3}),
        .axibusdomain_clk(axibusdomain_clk),
        .\fd_prim_array[14].bit_is_0.fdre_comp ({register3_n_4,register3_n_5,register3_n_6,register3_n_7}),
        .r7_x_max(r7_x_max),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] ({register3_n_12,register3_n_13,register3_n_14,register3_n_15}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_2 register4
       (.axibusdomain_clk(axibusdomain_clk),
        .q(register4_q_net),
        .r8_en_pkd(r8_en_pkd),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_3 register_x0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(register_q_net),
        .r4_x_delay(r4_x_delay),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_signaldomain signaldomain
       (.DI({register1_n_20,register1_n_21,register1_n_22,register1_n_23}),
        .Q(Q),
        .S({register1_n_0,register1_n_1,register1_n_2,register1_n_3}),
        .o(register1_q_net),
        .\op_mem_37_22_reg[0] (register_q_net),
        .peak_amp_dac(peak_amp_dac),
        .peak_amp_rdy(peak_amp_rdy),
        .pkd_i_16_23_reg({register3_n_12,register3_n_13,register3_n_14,register3_n_15}),
        .pkd_i_16_23_reg_0({register3_n_4,register3_n_5,register3_n_6,register3_n_7}),
        .pkd_i_16_23_reg_1({register2_n_12,register2_n_13,register2_n_14,register2_n_15}),
        .pkd_i_16_23_reg_2({register2_n_4,register2_n_5,register2_n_6,register2_n_7}),
        .q(register4_q_net),
        .rel_35_16_carry__0({register1_n_36,register1_n_37,register1_n_38,register1_n_39}),
        .rel_55_16_carry__0({register2_n_8,register2_n_9,register2_n_10,register2_n_11}),
        .rel_55_16_carry__0_0({register2_n_0,register2_n_1,register2_n_2,register2_n_3}),
        .rel_55_30_carry__0({register3_n_8,register3_n_9,register3_n_10,register3_n_11}),
        .rel_55_30_carry__0_0({register3_n_0,register3_n_1,register3_n_2,register3_n_3}),
        .rel_61_16_carry__0({register1_n_40,register1_n_41,register1_n_42,register1_n_43}),
        .signaldomain_clk(signaldomain_clk),
        .\state_15_23[0]_i_3 ({register1_n_28,register1_n_29,register1_n_30,register1_n_31}),
        .\state_15_23[0]_i_3_0 ({register1_n_44,register1_n_45,register1_n_46,register1_n_47}),
        .\state_15_23[1]_i_2 ({register1_n_32,register1_n_33,register1_n_34,register1_n_35}),
        .\state_15_23[1]_i_2_0 ({register1_n_24,register1_n_25,register1_n_26,register1_n_27}),
        .x(x));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister
   (S,
    o,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    \x[14] ,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[6].bit_is_0.fdre_comp ,
    \fd_prim_array[6].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[14].bit_is_0.fdre_comp_1 ,
    x,
    r5_x_noise,
    axibusdomain_clk,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]\x[14] ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  input [15:0]x;
  input [15:0]r5_x_noise;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire axibusdomain_clk;
  wire [15:0]d1_net;
  wire [15:0]d2_net;
  wire [15:0]d3_net;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  wire [15:0]o;
  wire [15:0]r5_x_noise;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_38 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r5_x_noise(r5_x_noise));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_39 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41 synth_reg_inst_3
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp_0 ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_1 (\fd_prim_array[14].bit_is_0.fdre_comp_1 ),
        .\fd_prim_array[6].bit_is_0.fdre_comp (\fd_prim_array[6].bit_is_0.fdre_comp ),
        .\fd_prim_array[6].bit_is_0.fdre_comp_0 (\fd_prim_array[6].bit_is_0.fdre_comp_0 ),
        .i(d3_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_0
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    x,
    r6_x_min,
    axibusdomain_clk,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  input [15:0]x;
  input [15:0]r6_x_min;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire axibusdomain_clk;
  wire [15:0]d1_net;
  wire [15:0]d2_net;
  wire [15:0]d3_net;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [15:0]r6_x_min;
  wire signaldomain_clk;
  wire [15:0]x;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_30 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r6_x_min(r6_x_min));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_31 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_32 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_33 synth_reg_inst_3
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp_0 ),
        .i(d3_net),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_1
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    DI,
    \x[14] ,
    x,
    r7_x_max,
    axibusdomain_clk,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]DI;
  output [3:0]\x[14] ;
  input [15:0]x;
  input [15:0]r7_x_max;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire axibusdomain_clk;
  wire [15:0]d1_net;
  wire [15:0]d2_net;
  wire [15:0]d3_net;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [15:0]r7_x_max;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_22 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r7_x_max(r7_x_max));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_23 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_24 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_25 synth_reg_inst_3
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .i(d3_net),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_2
   (q,
    r8_en_pkd,
    axibusdomain_clk,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]r8_en_pkd;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire [0:0]q;
  wire [0:0]r8_en_pkd;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_14 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r8_en_pkd(r8_en_pkd));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_15 synth_reg_inst_1
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_16 synth_reg_inst_2
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_17 synth_reg_inst_3
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_3
   (o,
    r4_x_delay,
    axibusdomain_clk,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]r4_x_delay;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [9:0]d1_net;
  wire [9:0]d2_net;
  wire [9:0]d3_net;
  wire [9:0]o;
  wire [9:0]r4_x_delay;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r4_x_delay(r4_x_delay));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_8 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_9 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_10 synth_reg_inst_3
       (.i(d3_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub
   (\x[14] ,
    \x[6] ,
    \x[14]_0 ,
    \x[6]_0 ,
    douta,
    o,
    signaldomain_clk,
    x);
  output [3:0]\x[14] ;
  output [3:0]\x[6] ;
  output [3:0]\x[14]_0 ;
  output [3:0]\x[6]_0 ;
  input [15:0]douta;
  input [15:0]o;
  input signaldomain_clk;
  input [15:0]x;

  wire [16:0]core_s;
  wire [15:0]douta;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;
  wire [3:0]\x[14]_0 ;
  wire [3:0]\x[6] ;
  wire [3:0]\x[6]_0 ;

  (* CHECK_LICENSE_TYPE = "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "c_addsub_v12_0_13,Vivado 2019.1" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0 \comp0.core_instance0 
       (.A({douta[15],douta}),
        .B({o[15],o}),
        .CE(1'b1),
        .CLK(signaldomain_clk),
        .S(core_s));
  LUT5 #(
    .INIT(32'h4000DDDC)) 
    rel_47_16_carry__0_i_1
       (.I0(x[14]),
        .I1(x[15]),
        .I2(core_s[14]),
        .I3(core_s[15]),
        .I4(core_s[16]),
        .O(\x[14]_0 [3]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry__0_i_2
       (.I0(x[12]),
        .I1(x[13]),
        .I2(core_s[13]),
        .I3(core_s[12]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[14]_0 [2]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry__0_i_3
       (.I0(x[10]),
        .I1(x[11]),
        .I2(core_s[11]),
        .I3(core_s[10]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[14]_0 [1]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry__0_i_4
       (.I0(x[8]),
        .I1(x[9]),
        .I2(core_s[9]),
        .I3(core_s[8]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[14]_0 [0]));
  LUT5 #(
    .INIT(32'h82424241)) 
    rel_47_16_carry__0_i_5
       (.I0(x[14]),
        .I1(x[15]),
        .I2(core_s[16]),
        .I3(core_s[15]),
        .I4(core_s[14]),
        .O(\x[14] [3]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry__0_i_6
       (.I0(x[12]),
        .I1(x[13]),
        .I2(core_s[13]),
        .I3(core_s[12]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[14] [2]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry__0_i_7
       (.I0(x[10]),
        .I1(x[11]),
        .I2(core_s[11]),
        .I3(core_s[10]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[14] [1]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry__0_i_8
       (.I0(x[8]),
        .I1(x[9]),
        .I2(core_s[9]),
        .I3(core_s[8]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[14] [0]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry_i_1
       (.I0(x[6]),
        .I1(x[7]),
        .I2(core_s[7]),
        .I3(core_s[6]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[6]_0 [3]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry_i_2
       (.I0(x[4]),
        .I1(x[5]),
        .I2(core_s[5]),
        .I3(core_s[4]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[6]_0 [2]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry_i_3
       (.I0(x[2]),
        .I1(x[3]),
        .I2(core_s[3]),
        .I3(core_s[2]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[6]_0 [1]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry_i_4
       (.I0(x[0]),
        .I1(x[1]),
        .I2(core_s[1]),
        .I3(core_s[0]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[6]_0 [0]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry_i_5
       (.I0(x[6]),
        .I1(x[7]),
        .I2(core_s[7]),
        .I3(core_s[6]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[6] [3]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry_i_6
       (.I0(x[4]),
        .I1(x[5]),
        .I2(core_s[5]),
        .I3(core_s[4]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[6] [2]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry_i_7
       (.I0(x[2]),
        .I1(x[3]),
        .I2(core_s[3]),
        .I3(core_s[2]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[6] [1]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry_i_8
       (.I0(x[0]),
        .I1(x[1]),
        .I2(core_s[1]),
        .I3(core_s[0]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[6] [0]));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xladdsub" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub__xdcDup__1
   (S,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    DI,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    x,
    o,
    signaldomain_clk,
    douta);
  output [3:0]S;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  output [3:0]DI;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  input [15:0]x;
  input [15:0]o;
  input signaldomain_clk;
  input [15:0]douta;

  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [3:0]DI;
  wire [3:0]S;
  wire [16:0]core_s;
  wire [15:0]douta;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;

  (* CHECK_LICENSE_TYPE = "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "c_addsub_v12_0_13,Vivado 2019.1" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2 \comp0.core_instance0 
       (.A({x[15],x}),
        .B({o[15],o}),
        .CE(1'b1),
        .CLK(signaldomain_clk),
        .S(core_s));
  LUT5 #(
    .INIT(32'h73331110)) 
    rel_63_21_carry__0_i_1
       (.I0(douta[14]),
        .I1(core_s[16]),
        .I2(core_s[14]),
        .I3(core_s[15]),
        .I4(douta[15]),
        .O(DI[3]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_63_21_carry__0_i_2
       (.I0(douta[12]),
        .I1(douta[13]),
        .I2(core_s[13]),
        .I3(core_s[12]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(DI[2]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_63_21_carry__0_i_3
       (.I0(douta[10]),
        .I1(douta[11]),
        .I2(core_s[11]),
        .I3(core_s[10]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(DI[1]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry__0_i_4
       (.I0(douta[8]),
        .I1(douta[9]),
        .I2(core_s[9]),
        .I3(core_s[16]),
        .I4(core_s[8]),
        .I5(core_s[15]),
        .O(DI[0]));
  LUT5 #(
    .INIT(32'h84442221)) 
    rel_63_21_carry__0_i_5
       (.I0(douta[14]),
        .I1(core_s[16]),
        .I2(core_s[15]),
        .I3(core_s[14]),
        .I4(douta[15]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_63_21_carry__0_i_6
       (.I0(douta[12]),
        .I1(douta[13]),
        .I2(core_s[13]),
        .I3(core_s[12]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_63_21_carry__0_i_7
       (.I0(douta[10]),
        .I1(douta[11]),
        .I2(core_s[11]),
        .I3(core_s[10]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry__0_i_8
       (.I0(douta[8]),
        .I1(douta[9]),
        .I2(core_s[9]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[8]),
        .O(S[0]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry_i_1
       (.I0(douta[6]),
        .I1(douta[7]),
        .I2(core_s[7]),
        .I3(core_s[16]),
        .I4(core_s[6]),
        .I5(core_s[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [3]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry_i_2
       (.I0(douta[4]),
        .I1(douta[5]),
        .I2(core_s[5]),
        .I3(core_s[16]),
        .I4(core_s[4]),
        .I5(core_s[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [2]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry_i_3
       (.I0(douta[2]),
        .I1(douta[3]),
        .I2(core_s[3]),
        .I3(core_s[16]),
        .I4(core_s[2]),
        .I5(core_s[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [1]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry_i_4
       (.I0(douta[0]),
        .I1(douta[1]),
        .I2(core_s[1]),
        .I3(core_s[16]),
        .I4(core_s[0]),
        .I5(core_s[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [0]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry_i_5
       (.I0(douta[6]),
        .I1(douta[7]),
        .I2(core_s[7]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[6]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [3]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry_i_6
       (.I0(douta[4]),
        .I1(douta[5]),
        .I2(core_s[5]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[4]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [2]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry_i_7
       (.I0(douta[2]),
        .I1(douta[3]),
        .I2(core_s[3]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[2]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [1]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry_i_8
       (.I0(douta[0]),
        .I1(douta[1]),
        .I2(core_s[1]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[0]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlconvert
   (O,
    \xmax_i_12_24_reg[9] ,
    \xmax_i_12_24_reg[13] ,
    CO,
    \xmax_i_12_24_reg[15] ,
    D,
    Q,
    S,
    d,
    signaldomain_clk);
  output [3:0]O;
  output [3:0]\xmax_i_12_24_reg[9] ;
  output [3:0]\xmax_i_12_24_reg[13] ;
  output [0:0]CO;
  output [1:0]\xmax_i_12_24_reg[15] ;
  output [13:0]D;
  input [13:0]Q;
  input [0:0]S;
  input [13:0]d;
  input signaldomain_clk;

  wire [0:0]CO;
  wire [13:0]D;
  wire [3:0]O;
  wire [13:0]Q;
  wire [0:0]S;
  wire [13:0]d;
  wire signaldomain_clk;
  wire [3:0]\xmax_i_12_24_reg[13] ;
  wire [1:0]\xmax_i_12_24_reg[15] ;
  wire [3:0]\xmax_i_12_24_reg[9] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg \latency_test.reg 
       (.D(D),
        .d(d),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convert_func_call_ip_peakdetector_xlconvert \std_conversion_generate.convert 
       (.CO(CO),
        .O(O),
        .Q(Q),
        .S(S),
        .\xmax_i_12_24_reg[13] (\xmax_i_12_24_reg[13] ),
        .\xmax_i_12_24_reg[15] (\xmax_i_12_24_reg[15] ),
        .\xmax_i_12_24_reg[9] (\xmax_i_12_24_reg[9] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlcounter_free
   (Q,
    S,
    signaldomain_clk,
    LOAD,
    \op_mem_37_22_reg[0] );
  output [9:0]Q;
  output [0:0]S;
  input signaldomain_clk;
  input LOAD;
  input [0:0]\op_mem_37_22_reg[0] ;

  wire LOAD;
  wire [9:0]Q;
  wire [0:0]S;
  wire [0:0]\op_mem_37_22_reg[0] ;
  wire signaldomain_clk;

  (* CHECK_LICENSE_TYPE = "ip_peakdetector_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "c_counter_binary_v12_0_13,Vivado 2019.1" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0 \comp0.core_instance0 
       (.CE(1'b1),
        .CLK(signaldomain_clk),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(LOAD),
        .Q(Q),
        .SINIT(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    \op_mem_37_22[0]_i_2 
       (.I0(Q[9]),
        .I1(\op_mem_37_22_reg[0] ),
        .O(S));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlspram
   (douta,
    DI,
    S,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    signaldomain_clk,
    Q,
    x,
    o);
  output [15:0]douta;
  output [3:0]DI;
  output [3:0]S;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  input signaldomain_clk;
  input [9:0]Q;
  input [15:0]x;
  input [15:0]o;

  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [3:0]DI;
  wire [9:0]Q;
  wire [3:0]S;
  wire [15:0]douta;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;

  (* CHECK_LICENSE_TYPE = "ip_peakdetector_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0 \comp0.core_instance0 
       (.addra(Q),
        .clka(signaldomain_clk),
        .dina(x),
        .douta(douta),
        .ena(1'b1),
        .wea(1'b1));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry__0_i_1
       (.I0(douta[14]),
        .I1(o[14]),
        .I2(douta[15]),
        .I3(o[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry__0_i_2
       (.I0(douta[12]),
        .I1(o[12]),
        .I2(o[13]),
        .I3(douta[13]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry__0_i_3
       (.I0(douta[10]),
        .I1(o[10]),
        .I2(o[11]),
        .I3(douta[11]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry__0_i_4
       (.I0(douta[8]),
        .I1(o[8]),
        .I2(o[9]),
        .I3(douta[9]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry__0_i_5
       (.I0(douta[14]),
        .I1(douta[15]),
        .I2(o[15]),
        .I3(o[14]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry__0_i_6
       (.I0(douta[12]),
        .I1(o[13]),
        .I2(douta[13]),
        .I3(o[12]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry__0_i_7
       (.I0(douta[10]),
        .I1(o[11]),
        .I2(douta[11]),
        .I3(o[10]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry__0_i_8
       (.I0(douta[8]),
        .I1(o[9]),
        .I2(douta[9]),
        .I3(o[8]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry_i_1
       (.I0(douta[6]),
        .I1(o[6]),
        .I2(o[7]),
        .I3(douta[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry_i_2
       (.I0(douta[4]),
        .I1(o[4]),
        .I2(o[5]),
        .I3(douta[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry_i_3
       (.I0(douta[2]),
        .I1(o[2]),
        .I2(o[3]),
        .I3(douta[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry_i_4
       (.I0(douta[0]),
        .I1(o[0]),
        .I2(o[1]),
        .I3(douta[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry_i_5
       (.I0(douta[6]),
        .I1(o[7]),
        .I2(douta[7]),
        .I3(o[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry_i_6
       (.I0(douta[4]),
        .I1(o[5]),
        .I2(douta[5]),
        .I3(o[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry_i_7
       (.I0(douta[2]),
        .I1(o[3]),
        .I2(douta[3]),
        .I3(o[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry_i_8
       (.I0(douta[0]),
        .I1(o[1]),
        .I2(douta[1]),
        .I3(o[0]),
        .O(S[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_logical_80d79f42b8
   (peak_amp_dac,
    D,
    signaldomain_clk);
  output [13:0]peak_amp_dac;
  input [13:0]D;
  input signaldomain_clk;

  wire [13:0]D;
  wire [13:0]peak_amp_dac;
  wire signaldomain_clk;

  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[0]),
        .Q(peak_amp_dac[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][10] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[10]),
        .Q(peak_amp_dac[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][11] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[11]),
        .Q(peak_amp_dac[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][12] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[12]),
        .Q(peak_amp_dac[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][13] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[13]),
        .Q(peak_amp_dac[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][1] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[1]),
        .Q(peak_amp_dac[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][2] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[2]),
        .Q(peak_amp_dac[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][3] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[3]),
        .Q(peak_amp_dac[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][4] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[4]),
        .Q(peak_amp_dac[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][5] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[5]),
        .Q(peak_amp_dac[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][6] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[6]),
        .Q(peak_amp_dac[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][7] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[7]),
        .Q(peak_amp_dac[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][8] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[8]),
        .Q(peak_amp_dac[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][9] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[9]),
        .Q(peak_amp_dac[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c3d4c1851
   (Q,
    d,
    \xmax_i_12_24_reg[2]_0 ,
    mcode_cmp_en_net,
    x,
    CO,
    O,
    \reg_array[12].fde_used.u2 ,
    \reg_array[7].fde_used.u2 ,
    \reg_array[11].fde_used.u2 ,
    SR,
    signaldomain_clk);
  output [15:0]Q;
  output [13:0]d;
  output [0:0]\xmax_i_12_24_reg[2]_0 ;
  input mcode_cmp_en_net;
  input [15:0]x;
  input [0:0]CO;
  input [3:0]O;
  input [1:0]\reg_array[12].fde_used.u2 ;
  input [3:0]\reg_array[7].fde_used.u2 ;
  input [3:0]\reg_array[11].fde_used.u2 ;
  input [0:0]SR;
  input signaldomain_clk;

  wire [0:0]CO;
  wire [3:0]O;
  wire [15:0]Q;
  wire [0:0]SR;
  wire [13:0]d;
  wire mcode_cmp_en_net;
  wire [3:0]\reg_array[11].fde_used.u2 ;
  wire [1:0]\reg_array[12].fde_used.u2 ;
  wire [3:0]\reg_array[7].fde_used.u2 ;
  wire rel_19_28;
  wire rel_19_28_carry__0_i_1_n_0;
  wire rel_19_28_carry__0_i_2_n_0;
  wire rel_19_28_carry__0_i_3_n_0;
  wire rel_19_28_carry__0_i_4_n_0;
  wire rel_19_28_carry__0_i_5_n_0;
  wire rel_19_28_carry__0_i_6_n_0;
  wire rel_19_28_carry__0_i_7_n_0;
  wire rel_19_28_carry__0_i_8_n_0;
  wire rel_19_28_carry__0_n_1;
  wire rel_19_28_carry__0_n_2;
  wire rel_19_28_carry__0_n_3;
  wire rel_19_28_carry_i_1_n_0;
  wire rel_19_28_carry_i_2_n_0;
  wire rel_19_28_carry_i_3_n_0;
  wire rel_19_28_carry_i_4_n_0;
  wire rel_19_28_carry_i_5_n_0;
  wire rel_19_28_carry_i_6_n_0;
  wire rel_19_28_carry_i_7_n_0;
  wire rel_19_28_carry_i_8_n_0;
  wire rel_19_28_carry_n_0;
  wire rel_19_28_carry_n_1;
  wire rel_19_28_carry_n_2;
  wire rel_19_28_carry_n_3;
  wire signaldomain_clk;
  wire [15:0]x;
  wire xmax_i_12_2400_out;
  wire [0:0]\xmax_i_12_24_reg[2]_0 ;
  wire [3:0]NLW_rel_19_28_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_19_28_carry__0_O_UNCONNECTED;

  LUT4 #(
    .INIT(16'h5A9A)) 
    inp_carry_i_1
       (.I0(Q[2]),
        .I1(Q[15]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\xmax_i_12_24_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[0].fde_used.u2_i_1 
       (.I0(O[0]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[0]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[10].fde_used.u2_i_1 
       (.I0(\reg_array[11].fde_used.u2 [2]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[10]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[11].fde_used.u2_i_1 
       (.I0(\reg_array[11].fde_used.u2 [3]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[11]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[12].fde_used.u2_i_1 
       (.I0(\reg_array[12].fde_used.u2 [0]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[12]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \reg_array[13].fde_used.u2_i_1 
       (.I0(CO),
        .O(d[13]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[1].fde_used.u2_i_1 
       (.I0(O[1]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[1]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[2].fde_used.u2_i_1 
       (.I0(O[2]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[2]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[3].fde_used.u2_i_1 
       (.I0(O[3]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[3]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[4].fde_used.u2_i_1 
       (.I0(\reg_array[7].fde_used.u2 [0]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[4]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[5].fde_used.u2_i_1 
       (.I0(\reg_array[7].fde_used.u2 [1]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[5]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[6].fde_used.u2_i_1 
       (.I0(\reg_array[7].fde_used.u2 [2]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[6]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[7].fde_used.u2_i_1 
       (.I0(\reg_array[7].fde_used.u2 [3]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[7]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[8].fde_used.u2_i_1 
       (.I0(\reg_array[11].fde_used.u2 [0]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[8]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[9].fde_used.u2_i_1 
       (.I0(\reg_array[11].fde_used.u2 [1]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[9]));
  CARRY4 rel_19_28_carry
       (.CI(1'b0),
        .CO({rel_19_28_carry_n_0,rel_19_28_carry_n_1,rel_19_28_carry_n_2,rel_19_28_carry_n_3}),
        .CYINIT(1'b0),
        .DI({rel_19_28_carry_i_1_n_0,rel_19_28_carry_i_2_n_0,rel_19_28_carry_i_3_n_0,rel_19_28_carry_i_4_n_0}),
        .O(NLW_rel_19_28_carry_O_UNCONNECTED[3:0]),
        .S({rel_19_28_carry_i_5_n_0,rel_19_28_carry_i_6_n_0,rel_19_28_carry_i_7_n_0,rel_19_28_carry_i_8_n_0}));
  CARRY4 rel_19_28_carry__0
       (.CI(rel_19_28_carry_n_0),
        .CO({rel_19_28,rel_19_28_carry__0_n_1,rel_19_28_carry__0_n_2,rel_19_28_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({rel_19_28_carry__0_i_1_n_0,rel_19_28_carry__0_i_2_n_0,rel_19_28_carry__0_i_3_n_0,rel_19_28_carry__0_i_4_n_0}),
        .O(NLW_rel_19_28_carry__0_O_UNCONNECTED[3:0]),
        .S({rel_19_28_carry__0_i_5_n_0,rel_19_28_carry__0_i_6_n_0,rel_19_28_carry__0_i_7_n_0,rel_19_28_carry__0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry__0_i_1
       (.I0(Q[14]),
        .I1(x[14]),
        .I2(Q[15]),
        .I3(x[15]),
        .O(rel_19_28_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry__0_i_2
       (.I0(Q[12]),
        .I1(x[12]),
        .I2(x[13]),
        .I3(Q[13]),
        .O(rel_19_28_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry__0_i_3
       (.I0(Q[10]),
        .I1(x[10]),
        .I2(x[11]),
        .I3(Q[11]),
        .O(rel_19_28_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry__0_i_4
       (.I0(Q[8]),
        .I1(x[8]),
        .I2(x[9]),
        .I3(Q[9]),
        .O(rel_19_28_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h8421)) 
    rel_19_28_carry__0_i_5
       (.I0(x[14]),
        .I1(x[15]),
        .I2(Q[14]),
        .I3(Q[15]),
        .O(rel_19_28_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry__0_i_6
       (.I0(x[12]),
        .I1(x[13]),
        .I2(Q[13]),
        .I3(Q[12]),
        .O(rel_19_28_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry__0_i_7
       (.I0(x[10]),
        .I1(x[11]),
        .I2(Q[11]),
        .I3(Q[10]),
        .O(rel_19_28_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry__0_i_8
       (.I0(x[8]),
        .I1(x[9]),
        .I2(Q[9]),
        .I3(Q[8]),
        .O(rel_19_28_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry_i_1
       (.I0(Q[6]),
        .I1(x[6]),
        .I2(x[7]),
        .I3(Q[7]),
        .O(rel_19_28_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry_i_2
       (.I0(Q[4]),
        .I1(x[4]),
        .I2(x[5]),
        .I3(Q[5]),
        .O(rel_19_28_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry_i_3
       (.I0(Q[2]),
        .I1(x[2]),
        .I2(x[3]),
        .I3(Q[3]),
        .O(rel_19_28_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry_i_4
       (.I0(Q[0]),
        .I1(x[0]),
        .I2(x[1]),
        .I3(Q[1]),
        .O(rel_19_28_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry_i_5
       (.I0(x[6]),
        .I1(x[7]),
        .I2(Q[7]),
        .I3(Q[6]),
        .O(rel_19_28_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry_i_6
       (.I0(x[4]),
        .I1(x[5]),
        .I2(Q[5]),
        .I3(Q[4]),
        .O(rel_19_28_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry_i_7
       (.I0(x[2]),
        .I1(x[3]),
        .I2(Q[3]),
        .I3(Q[2]),
        .O(rel_19_28_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry_i_8
       (.I0(x[0]),
        .I1(Q[1]),
        .I2(x[1]),
        .I3(Q[0]),
        .O(rel_19_28_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    \xmax_i_12_24[15]_i_1 
       (.I0(rel_19_28),
        .I1(mcode_cmp_en_net),
        .O(xmax_i_12_2400_out));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[0] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[10] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[11] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[12] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[13] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[14] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[15] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[1] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[2] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[3] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[4] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[5] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[6] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[7] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[8] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[9] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c4ff59dc3
   (\ff_2_17_reg[0]_0 ,
    mcode_pkd_net,
    signaldomain_clk);
  output \ff_2_17_reg[0]_0 ;
  input mcode_pkd_net;
  input signaldomain_clk;

  wire \ff_2_17_reg[0]_0 ;
  wire mcode_pkd_net;
  wire signaldomain_clk;

  FDRE #(
    .INIT(1'b0)) 
    \ff_2_17_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(mcode_pkd_net),
        .Q(\ff_2_17_reg[0]_0 ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_a80b0cfdd5
   (mcode_pkd_net,
    mcode_cmp_en_net,
    SR,
    peak_amp_rdy,
    rel_63_21_carry__0_0,
    rel_63_21_carry__0_1,
    DI,
    S,
    rel_61_30_carry__0_0,
    rel_61_30_carry__0_1,
    \state_15_23[0]_i_3_0 ,
    \state_15_23[0]_i_3_1 ,
    rel_61_16_carry__0_0,
    rel_61_16_carry__0_1,
    \state_15_23[0]_i_3_2 ,
    \state_15_23[0]_i_3_3 ,
    rel_47_16_carry__0_0,
    rel_47_16_carry__0_1,
    \state_15_23[2]_i_2_0 ,
    \state_15_23[2]_i_2_1 ,
    rel_35_16_carry__0_0,
    rel_35_16_carry__0_1,
    \state_15_23[1]_i_2_0 ,
    \state_15_23[1]_i_2_1 ,
    rel_55_30_carry__0_0,
    rel_55_30_carry__0_1,
    pkd_i_16_23_reg_0,
    pkd_i_16_23_reg_1,
    rel_55_16_carry__0_0,
    rel_55_16_carry__0_1,
    pkd_i_16_23_reg_2,
    pkd_i_16_23_reg_3,
    signaldomain_clk,
    q,
    peak_amp_rdy_0_sp_1);
  output mcode_pkd_net;
  output mcode_cmp_en_net;
  output [0:0]SR;
  output [0:0]peak_amp_rdy;
  input [3:0]rel_63_21_carry__0_0;
  input [3:0]rel_63_21_carry__0_1;
  input [3:0]DI;
  input [3:0]S;
  input [3:0]rel_61_30_carry__0_0;
  input [3:0]rel_61_30_carry__0_1;
  input [3:0]\state_15_23[0]_i_3_0 ;
  input [3:0]\state_15_23[0]_i_3_1 ;
  input [3:0]rel_61_16_carry__0_0;
  input [3:0]rel_61_16_carry__0_1;
  input [3:0]\state_15_23[0]_i_3_2 ;
  input [3:0]\state_15_23[0]_i_3_3 ;
  input [3:0]rel_47_16_carry__0_0;
  input [3:0]rel_47_16_carry__0_1;
  input [3:0]\state_15_23[2]_i_2_0 ;
  input [3:0]\state_15_23[2]_i_2_1 ;
  input [3:0]rel_35_16_carry__0_0;
  input [3:0]rel_35_16_carry__0_1;
  input [3:0]\state_15_23[1]_i_2_0 ;
  input [3:0]\state_15_23[1]_i_2_1 ;
  input [3:0]rel_55_30_carry__0_0;
  input [3:0]rel_55_30_carry__0_1;
  input [3:0]pkd_i_16_23_reg_0;
  input [3:0]pkd_i_16_23_reg_1;
  input [3:0]rel_55_16_carry__0_0;
  input [3:0]rel_55_16_carry__0_1;
  input [3:0]pkd_i_16_23_reg_2;
  input [3:0]pkd_i_16_23_reg_3;
  input signaldomain_clk;
  input [0:0]q;
  input peak_amp_rdy_0_sp_1;

  wire [3:0]DI;
  wire [3:0]S;
  wire [0:0]SR;
  wire cmp_en_i_18_26_i_1_n_0;
  wire cmp_rst_i_17_27_i_1_n_0;
  wire mcode_cmp_en_net;
  wire mcode_pkd_net;
  wire [0:0]peak_amp_rdy;
  wire peak_amp_rdy_0_sn_1;
  wire pkd_i_16_23_i_1_n_0;
  wire [3:0]pkd_i_16_23_reg_0;
  wire [3:0]pkd_i_16_23_reg_1;
  wire [3:0]pkd_i_16_23_reg_2;
  wire [3:0]pkd_i_16_23_reg_3;
  wire [0:0]q;
  wire rel_35_16;
  wire [3:0]rel_35_16_carry__0_0;
  wire [3:0]rel_35_16_carry__0_1;
  wire rel_35_16_carry__0_n_1;
  wire rel_35_16_carry__0_n_2;
  wire rel_35_16_carry__0_n_3;
  wire rel_35_16_carry_n_0;
  wire rel_35_16_carry_n_1;
  wire rel_35_16_carry_n_2;
  wire rel_35_16_carry_n_3;
  wire rel_47_16;
  wire [3:0]rel_47_16_carry__0_0;
  wire [3:0]rel_47_16_carry__0_1;
  wire rel_47_16_carry__0_n_1;
  wire rel_47_16_carry__0_n_2;
  wire rel_47_16_carry__0_n_3;
  wire rel_47_16_carry_n_0;
  wire rel_47_16_carry_n_1;
  wire rel_47_16_carry_n_2;
  wire rel_47_16_carry_n_3;
  wire rel_55_16;
  wire [3:0]rel_55_16_carry__0_0;
  wire [3:0]rel_55_16_carry__0_1;
  wire rel_55_16_carry__0_n_1;
  wire rel_55_16_carry__0_n_2;
  wire rel_55_16_carry__0_n_3;
  wire rel_55_16_carry_n_0;
  wire rel_55_16_carry_n_1;
  wire rel_55_16_carry_n_2;
  wire rel_55_16_carry_n_3;
  wire rel_55_30;
  wire [3:0]rel_55_30_carry__0_0;
  wire [3:0]rel_55_30_carry__0_1;
  wire rel_55_30_carry__0_n_1;
  wire rel_55_30_carry__0_n_2;
  wire rel_55_30_carry__0_n_3;
  wire rel_55_30_carry_n_0;
  wire rel_55_30_carry_n_1;
  wire rel_55_30_carry_n_2;
  wire rel_55_30_carry_n_3;
  wire rel_61_16;
  wire [3:0]rel_61_16_carry__0_0;
  wire [3:0]rel_61_16_carry__0_1;
  wire rel_61_16_carry__0_n_1;
  wire rel_61_16_carry__0_n_2;
  wire rel_61_16_carry__0_n_3;
  wire rel_61_16_carry_n_0;
  wire rel_61_16_carry_n_1;
  wire rel_61_16_carry_n_2;
  wire rel_61_16_carry_n_3;
  wire rel_61_30;
  wire [3:0]rel_61_30_carry__0_0;
  wire [3:0]rel_61_30_carry__0_1;
  wire rel_61_30_carry__0_n_1;
  wire rel_61_30_carry__0_n_2;
  wire rel_61_30_carry__0_n_3;
  wire rel_61_30_carry_n_0;
  wire rel_61_30_carry_n_1;
  wire rel_61_30_carry_n_2;
  wire rel_61_30_carry_n_3;
  wire rel_63_21;
  wire [3:0]rel_63_21_carry__0_0;
  wire [3:0]rel_63_21_carry__0_1;
  wire rel_63_21_carry__0_n_1;
  wire rel_63_21_carry__0_n_2;
  wire rel_63_21_carry__0_n_3;
  wire rel_63_21_carry_n_0;
  wire rel_63_21_carry_n_1;
  wire rel_63_21_carry_n_2;
  wire rel_63_21_carry_n_3;
  wire signaldomain_clk;
  wire [2:0]state_15_23;
  wire \state_15_23[0]_i_2_n_0 ;
  wire [3:0]\state_15_23[0]_i_3_0 ;
  wire [3:0]\state_15_23[0]_i_3_1 ;
  wire [3:0]\state_15_23[0]_i_3_2 ;
  wire [3:0]\state_15_23[0]_i_3_3 ;
  wire \state_15_23[0]_i_3_n_0 ;
  wire [3:0]\state_15_23[1]_i_2_0 ;
  wire [3:0]\state_15_23[1]_i_2_1 ;
  wire \state_15_23[1]_i_2_n_0 ;
  wire \state_15_23[1]_i_3_n_0 ;
  wire [3:0]\state_15_23[2]_i_2_0 ;
  wire [3:0]\state_15_23[2]_i_2_1 ;
  wire \state_15_23[2]_i_2_n_0 ;
  wire \state_15_23[2]_i_3_n_0 ;
  wire [2:0]state_join_28_5;
  wire [3:0]NLW_rel_35_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_35_16_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_47_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_47_16_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_55_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_55_16_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_55_30_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_55_30_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_61_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_61_16_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_61_30_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_61_30_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_63_21_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_63_21_carry__0_O_UNCONNECTED;

  assign peak_amp_rdy_0_sn_1 = peak_amp_rdy_0_sp_1;
  LUT5 #(
    .INIT(32'hC0800080)) 
    cmp_en_i_18_26_i_1
       (.I0(state_15_23[0]),
        .I1(state_15_23[1]),
        .I2(q),
        .I3(state_15_23[2]),
        .I4(mcode_cmp_en_net),
        .O(cmp_en_i_18_26_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    cmp_en_i_18_26_reg
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(cmp_en_i_18_26_i_1_n_0),
        .Q(mcode_cmp_en_net),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hCF7F0F7F)) 
    cmp_rst_i_17_27_i_1
       (.I0(state_15_23[0]),
        .I1(state_15_23[1]),
        .I2(q),
        .I3(state_15_23[2]),
        .I4(SR),
        .O(cmp_rst_i_17_27_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    cmp_rst_i_17_27_reg
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(cmp_rst_i_17_27_i_1_n_0),
        .Q(SR),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \peak_amp_rdy[0]_INST_0 
       (.I0(mcode_pkd_net),
        .I1(peak_amp_rdy_0_sn_1),
        .O(peak_amp_rdy));
  LUT6 #(
    .INIT(64'hAAAA000000800000)) 
    pkd_i_16_23_i_1
       (.I0(q),
        .I1(rel_55_30),
        .I2(rel_55_16),
        .I3(state_15_23[1]),
        .I4(state_15_23[2]),
        .I5(mcode_pkd_net),
        .O(pkd_i_16_23_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pkd_i_16_23_reg
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(pkd_i_16_23_i_1_n_0),
        .Q(mcode_pkd_net),
        .R(1'b0));
  CARRY4 rel_35_16_carry
       (.CI(1'b0),
        .CO({rel_35_16_carry_n_0,rel_35_16_carry_n_1,rel_35_16_carry_n_2,rel_35_16_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_35_16_carry__0_0),
        .O(NLW_rel_35_16_carry_O_UNCONNECTED[3:0]),
        .S(rel_35_16_carry__0_1));
  CARRY4 rel_35_16_carry__0
       (.CI(rel_35_16_carry_n_0),
        .CO({rel_35_16,rel_35_16_carry__0_n_1,rel_35_16_carry__0_n_2,rel_35_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_15_23[1]_i_2_0 ),
        .O(NLW_rel_35_16_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_15_23[1]_i_2_1 ));
  CARRY4 rel_47_16_carry
       (.CI(1'b0),
        .CO({rel_47_16_carry_n_0,rel_47_16_carry_n_1,rel_47_16_carry_n_2,rel_47_16_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_47_16_carry__0_0),
        .O(NLW_rel_47_16_carry_O_UNCONNECTED[3:0]),
        .S(rel_47_16_carry__0_1));
  CARRY4 rel_47_16_carry__0
       (.CI(rel_47_16_carry_n_0),
        .CO({rel_47_16,rel_47_16_carry__0_n_1,rel_47_16_carry__0_n_2,rel_47_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_15_23[2]_i_2_0 ),
        .O(NLW_rel_47_16_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_15_23[2]_i_2_1 ));
  CARRY4 rel_55_16_carry
       (.CI(1'b0),
        .CO({rel_55_16_carry_n_0,rel_55_16_carry_n_1,rel_55_16_carry_n_2,rel_55_16_carry_n_3}),
        .CYINIT(1'b1),
        .DI(rel_55_16_carry__0_0),
        .O(NLW_rel_55_16_carry_O_UNCONNECTED[3:0]),
        .S(rel_55_16_carry__0_1));
  CARRY4 rel_55_16_carry__0
       (.CI(rel_55_16_carry_n_0),
        .CO({rel_55_16,rel_55_16_carry__0_n_1,rel_55_16_carry__0_n_2,rel_55_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(pkd_i_16_23_reg_2),
        .O(NLW_rel_55_16_carry__0_O_UNCONNECTED[3:0]),
        .S(pkd_i_16_23_reg_3));
  CARRY4 rel_55_30_carry
       (.CI(1'b0),
        .CO({rel_55_30_carry_n_0,rel_55_30_carry_n_1,rel_55_30_carry_n_2,rel_55_30_carry_n_3}),
        .CYINIT(1'b1),
        .DI(rel_55_30_carry__0_0),
        .O(NLW_rel_55_30_carry_O_UNCONNECTED[3:0]),
        .S(rel_55_30_carry__0_1));
  CARRY4 rel_55_30_carry__0
       (.CI(rel_55_30_carry_n_0),
        .CO({rel_55_30,rel_55_30_carry__0_n_1,rel_55_30_carry__0_n_2,rel_55_30_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(pkd_i_16_23_reg_0),
        .O(NLW_rel_55_30_carry__0_O_UNCONNECTED[3:0]),
        .S(pkd_i_16_23_reg_1));
  CARRY4 rel_61_16_carry
       (.CI(1'b0),
        .CO({rel_61_16_carry_n_0,rel_61_16_carry_n_1,rel_61_16_carry_n_2,rel_61_16_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_61_16_carry__0_0),
        .O(NLW_rel_61_16_carry_O_UNCONNECTED[3:0]),
        .S(rel_61_16_carry__0_1));
  CARRY4 rel_61_16_carry__0
       (.CI(rel_61_16_carry_n_0),
        .CO({rel_61_16,rel_61_16_carry__0_n_1,rel_61_16_carry__0_n_2,rel_61_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_15_23[0]_i_3_2 ),
        .O(NLW_rel_61_16_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_15_23[0]_i_3_3 ));
  CARRY4 rel_61_30_carry
       (.CI(1'b0),
        .CO({rel_61_30_carry_n_0,rel_61_30_carry_n_1,rel_61_30_carry_n_2,rel_61_30_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_61_30_carry__0_0),
        .O(NLW_rel_61_30_carry_O_UNCONNECTED[3:0]),
        .S(rel_61_30_carry__0_1));
  CARRY4 rel_61_30_carry__0
       (.CI(rel_61_30_carry_n_0),
        .CO({rel_61_30,rel_61_30_carry__0_n_1,rel_61_30_carry__0_n_2,rel_61_30_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_15_23[0]_i_3_0 ),
        .O(NLW_rel_61_30_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_15_23[0]_i_3_1 ));
  CARRY4 rel_63_21_carry
       (.CI(1'b0),
        .CO({rel_63_21_carry_n_0,rel_63_21_carry_n_1,rel_63_21_carry_n_2,rel_63_21_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_63_21_carry__0_0),
        .O(NLW_rel_63_21_carry_O_UNCONNECTED[3:0]),
        .S(rel_63_21_carry__0_1));
  CARRY4 rel_63_21_carry__0
       (.CI(rel_63_21_carry_n_0),
        .CO({rel_63_21,rel_63_21_carry__0_n_1,rel_63_21_carry__0_n_2,rel_63_21_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(DI),
        .O(NLW_rel_63_21_carry__0_O_UNCONNECTED[3:0]),
        .S(S));
  LUT5 #(
    .INIT(32'h4FFF7FFF)) 
    \state_15_23[0]_i_2 
       (.I0(rel_47_16),
        .I1(state_15_23[1]),
        .I2(state_15_23[0]),
        .I3(q),
        .I4(rel_35_16),
        .O(\state_15_23[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7333777777777777)) 
    \state_15_23[0]_i_3 
       (.I0(state_15_23[1]),
        .I1(q),
        .I2(rel_61_16),
        .I3(rel_61_30),
        .I4(rel_63_21),
        .I5(state_15_23[0]),
        .O(\state_15_23[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h70C040C0)) 
    \state_15_23[1]_i_2 
       (.I0(rel_47_16),
        .I1(state_15_23[1]),
        .I2(q),
        .I3(state_15_23[0]),
        .I4(rel_35_16),
        .O(\state_15_23[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000008880000)) 
    \state_15_23[1]_i_3 
       (.I0(state_15_23[0]),
        .I1(rel_63_21),
        .I2(rel_61_16),
        .I3(rel_61_30),
        .I4(q),
        .I5(state_15_23[1]),
        .O(\state_15_23[1]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \state_15_23[2]_i_2 
       (.I0(rel_47_16),
        .I1(state_15_23[0]),
        .I2(q),
        .I3(state_15_23[1]),
        .O(\state_15_23[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000022222AAA)) 
    \state_15_23[2]_i_3 
       (.I0(q),
        .I1(state_15_23[0]),
        .I2(rel_61_30),
        .I3(rel_61_16),
        .I4(rel_63_21),
        .I5(state_15_23[1]),
        .O(\state_15_23[2]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_15_23_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(state_join_28_5[0]),
        .Q(state_15_23[0]),
        .R(1'b0));
  MUXF7 \state_15_23_reg[0]_i_1 
       (.I0(\state_15_23[0]_i_2_n_0 ),
        .I1(\state_15_23[0]_i_3_n_0 ),
        .O(state_join_28_5[0]),
        .S(state_15_23[2]));
  FDRE #(
    .INIT(1'b0)) 
    \state_15_23_reg[1] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(state_join_28_5[1]),
        .Q(state_15_23[1]),
        .R(1'b0));
  MUXF7 \state_15_23_reg[1]_i_1 
       (.I0(\state_15_23[1]_i_2_n_0 ),
        .I1(\state_15_23[1]_i_3_n_0 ),
        .O(state_join_28_5[1]),
        .S(state_15_23[2]));
  FDRE #(
    .INIT(1'b0)) 
    \state_15_23_reg[2] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(state_join_28_5[2]),
        .Q(state_15_23[2]),
        .R(1'b0));
  MUXF7 \state_15_23_reg[2]_i_1 
       (.I0(\state_15_23[2]_i_2_n_0 ),
        .I1(\state_15_23[2]_i_3_n_0 ),
        .O(state_join_28_5[2]),
        .S(state_15_23[2]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_3828661dd3
   (LOAD,
    signaldomain_clk,
    S,
    \op_mem_37_22_reg[0]_0 ,
    Q);
  output LOAD;
  input signaldomain_clk;
  input [0:0]S;
  input [8:0]\op_mem_37_22_reg[0]_0 ;
  input [8:0]Q;

  wire LOAD;
  wire [8:0]Q;
  wire [0:0]S;
  wire \op_mem_37_22[0]_i_3_n_0 ;
  wire \op_mem_37_22[0]_i_4_n_0 ;
  wire \op_mem_37_22[0]_i_5_n_0 ;
  wire [8:0]\op_mem_37_22_reg[0]_0 ;
  wire \op_mem_37_22_reg[0]_i_1_n_1 ;
  wire \op_mem_37_22_reg[0]_i_1_n_2 ;
  wire \op_mem_37_22_reg[0]_i_1_n_3 ;
  wire result_12_3_rel;
  wire signaldomain_clk;
  wire [3:0]\NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_3 
       (.I0(\op_mem_37_22_reg[0]_0 [6]),
        .I1(Q[6]),
        .I2(Q[8]),
        .I3(\op_mem_37_22_reg[0]_0 [8]),
        .I4(Q[7]),
        .I5(\op_mem_37_22_reg[0]_0 [7]),
        .O(\op_mem_37_22[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_4 
       (.I0(\op_mem_37_22_reg[0]_0 [3]),
        .I1(Q[3]),
        .I2(Q[5]),
        .I3(\op_mem_37_22_reg[0]_0 [5]),
        .I4(Q[4]),
        .I5(\op_mem_37_22_reg[0]_0 [4]),
        .O(\op_mem_37_22[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_5 
       (.I0(\op_mem_37_22_reg[0]_0 [0]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(\op_mem_37_22_reg[0]_0 [2]),
        .I4(Q[1]),
        .I5(\op_mem_37_22_reg[0]_0 [1]),
        .O(\op_mem_37_22[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_37_22_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(result_12_3_rel),
        .Q(LOAD),
        .R(1'b0));
  CARRY4 \op_mem_37_22_reg[0]_i_1 
       (.CI(1'b0),
        .CO({result_12_3_rel,\op_mem_37_22_reg[0]_i_1_n_1 ,\op_mem_37_22_reg[0]_i_1_n_2 ,\op_mem_37_22_reg[0]_i_1_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED [3:0]),
        .S({S,\op_mem_37_22[0]_i_3_n_0 ,\op_mem_37_22[0]_i_4_n_0 ,\op_mem_37_22[0]_i_5_n_0 }));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_11
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_12
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_13
   (o,
    r4_x_delay,
    axibusdomain_clk);
  output [9:0]o;
  input [9:0]r4_x_delay;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [9:0]o;
  wire [9:0]r4_x_delay;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_18
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]d3_net;
  input signaldomain_clk;

  wire [0:0]d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d3_net),
        .Q(q),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_19
   (d3_net,
    d2_net,
    signaldomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input signaldomain_clk;

  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d2_net),
        .Q(d3_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_20
   (d2_net,
    d1_net,
    signaldomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d1_net),
        .Q(d2_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_21
   (d1_net,
    r8_en_pkd,
    axibusdomain_clk);
  output [0:0]d1_net;
  input [0:0]r8_en_pkd;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]r8_en_pkd;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r8_en_pkd),
        .Q(d1_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_26
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    DI,
    \x[14] ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]DI;
  output [3:0]\x[14] ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire [15:0]register3_q_net;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(register3_q_net[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(register3_q_net[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(register3_q_net[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(register3_q_net[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(register3_q_net[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(register3_q_net[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(register3_q_net[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(register3_q_net[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(register3_q_net[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(register3_q_net[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(register3_q_net[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(register3_q_net[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(register3_q_net[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(register3_q_net[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(register3_q_net[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(register3_q_net[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry__0_i_1
       (.I0(x[14]),
        .I1(register3_q_net[14]),
        .I2(x[15]),
        .I3(register3_q_net[15]),
        .O(\x[14] [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry__0_i_2
       (.I0(x[12]),
        .I1(register3_q_net[12]),
        .I2(register3_q_net[13]),
        .I3(x[13]),
        .O(\x[14] [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry__0_i_3
       (.I0(x[10]),
        .I1(register3_q_net[10]),
        .I2(register3_q_net[11]),
        .I3(x[11]),
        .O(\x[14] [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry__0_i_4
       (.I0(x[8]),
        .I1(register3_q_net[8]),
        .I2(register3_q_net[9]),
        .I3(x[9]),
        .O(\x[14] [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry__0_i_5
       (.I0(register3_q_net[14]),
        .I1(x[15]),
        .I2(register3_q_net[15]),
        .I3(x[14]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry__0_i_6
       (.I0(register3_q_net[12]),
        .I1(x[13]),
        .I2(register3_q_net[13]),
        .I3(x[12]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry__0_i_7
       (.I0(register3_q_net[10]),
        .I1(x[11]),
        .I2(register3_q_net[11]),
        .I3(x[10]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry__0_i_8
       (.I0(register3_q_net[8]),
        .I1(x[9]),
        .I2(register3_q_net[9]),
        .I3(x[8]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry_i_1
       (.I0(x[6]),
        .I1(register3_q_net[6]),
        .I2(register3_q_net[7]),
        .I3(x[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry_i_2
       (.I0(x[4]),
        .I1(register3_q_net[4]),
        .I2(register3_q_net[5]),
        .I3(x[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry_i_3
       (.I0(x[2]),
        .I1(register3_q_net[2]),
        .I2(register3_q_net[3]),
        .I3(x[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry_i_4
       (.I0(x[0]),
        .I1(register3_q_net[0]),
        .I2(register3_q_net[1]),
        .I3(x[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry_i_5
       (.I0(register3_q_net[6]),
        .I1(x[7]),
        .I2(register3_q_net[7]),
        .I3(x[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry_i_6
       (.I0(register3_q_net[4]),
        .I1(x[5]),
        .I2(register3_q_net[5]),
        .I3(x[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry_i_7
       (.I0(register3_q_net[2]),
        .I1(x[3]),
        .I2(register3_q_net[3]),
        .I3(x[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry_i_8
       (.I0(register3_q_net[0]),
        .I1(x[1]),
        .I2(register3_q_net[1]),
        .I3(x[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_27
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_28
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_29
   (o,
    r7_x_max,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r7_x_max;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r7_x_max;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_34
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp_1 ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  wire [15:0]i;
  wire [15:0]register2_q_net;
  wire signaldomain_clk;
  wire [15:0]x;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(register2_q_net[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(register2_q_net[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(register2_q_net[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(register2_q_net[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(register2_q_net[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(register2_q_net[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(register2_q_net[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(register2_q_net[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(register2_q_net[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(register2_q_net[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(register2_q_net[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(register2_q_net[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(register2_q_net[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(register2_q_net[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(register2_q_net[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(register2_q_net[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry__0_i_1
       (.I0(register2_q_net[14]),
        .I1(x[14]),
        .I2(register2_q_net[15]),
        .I3(x[15]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry__0_i_2
       (.I0(register2_q_net[12]),
        .I1(x[12]),
        .I2(x[13]),
        .I3(register2_q_net[13]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry__0_i_3
       (.I0(register2_q_net[10]),
        .I1(x[10]),
        .I2(x[11]),
        .I3(register2_q_net[11]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry__0_i_4
       (.I0(register2_q_net[8]),
        .I1(x[8]),
        .I2(x[9]),
        .I3(register2_q_net[9]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry__0_i_5
       (.I0(register2_q_net[14]),
        .I1(x[15]),
        .I2(register2_q_net[15]),
        .I3(x[14]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry__0_i_6
       (.I0(register2_q_net[12]),
        .I1(x[13]),
        .I2(register2_q_net[13]),
        .I3(x[12]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry__0_i_7
       (.I0(register2_q_net[10]),
        .I1(x[11]),
        .I2(register2_q_net[11]),
        .I3(x[10]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry__0_i_8
       (.I0(register2_q_net[8]),
        .I1(x[9]),
        .I2(register2_q_net[9]),
        .I3(x[8]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry_i_1
       (.I0(register2_q_net[6]),
        .I1(x[6]),
        .I2(x[7]),
        .I3(register2_q_net[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry_i_2
       (.I0(register2_q_net[4]),
        .I1(x[4]),
        .I2(x[5]),
        .I3(register2_q_net[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry_i_3
       (.I0(register2_q_net[2]),
        .I1(x[2]),
        .I2(x[3]),
        .I3(register2_q_net[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry_i_4
       (.I0(register2_q_net[0]),
        .I1(x[0]),
        .I2(x[1]),
        .I3(register2_q_net[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry_i_5
       (.I0(register2_q_net[6]),
        .I1(x[7]),
        .I2(register2_q_net[7]),
        .I3(x[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry_i_6
       (.I0(register2_q_net[4]),
        .I1(x[5]),
        .I2(register2_q_net[5]),
        .I3(x[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry_i_7
       (.I0(register2_q_net[2]),
        .I1(x[3]),
        .I2(register2_q_net[3]),
        .I3(x[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry_i_8
       (.I0(register2_q_net[0]),
        .I1(x[1]),
        .I2(register2_q_net[1]),
        .I3(x[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_35
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_36
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_37
   (o,
    r6_x_min,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r6_x_min;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r6_x_min;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_42
   (S,
    o,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    \x[14] ,
    \fd_prim_array[14].bit_is_0.fdre_comp_1 ,
    \fd_prim_array[6].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[6].bit_is_0.fdre_comp_1 ,
    \fd_prim_array[14].bit_is_0.fdre_comp_2 ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]\x[14] ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_1 ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_2 ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_2 ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_1 ;
  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry__0_i_1
       (.I0(o[14]),
        .I1(x[14]),
        .I2(o[15]),
        .I3(x[15]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry__0_i_2
       (.I0(o[12]),
        .I1(x[12]),
        .I2(x[13]),
        .I3(o[13]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry__0_i_3
       (.I0(o[10]),
        .I1(x[10]),
        .I2(x[11]),
        .I3(o[11]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry__0_i_4
       (.I0(o[8]),
        .I1(x[8]),
        .I2(x[9]),
        .I3(o[9]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry__0_i_5
       (.I0(o[14]),
        .I1(x[15]),
        .I2(o[15]),
        .I3(x[14]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry__0_i_6
       (.I0(o[12]),
        .I1(x[13]),
        .I2(o[13]),
        .I3(x[12]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry__0_i_7
       (.I0(o[10]),
        .I1(x[11]),
        .I2(o[11]),
        .I3(x[10]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry__0_i_8
       (.I0(o[8]),
        .I1(x[9]),
        .I2(o[9]),
        .I3(x[8]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry_i_1
       (.I0(o[6]),
        .I1(x[6]),
        .I2(x[7]),
        .I3(o[7]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry_i_2
       (.I0(o[4]),
        .I1(x[4]),
        .I2(x[5]),
        .I3(o[5]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry_i_3
       (.I0(o[2]),
        .I1(x[2]),
        .I2(x[3]),
        .I3(o[3]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry_i_4
       (.I0(o[0]),
        .I1(x[0]),
        .I2(x[1]),
        .I3(o[1]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry_i_5
       (.I0(o[6]),
        .I1(x[7]),
        .I2(o[7]),
        .I3(x[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry_i_6
       (.I0(o[4]),
        .I1(x[5]),
        .I2(o[5]),
        .I3(x[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry_i_7
       (.I0(o[2]),
        .I1(x[3]),
        .I2(o[3]),
        .I3(x[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry_i_8
       (.I0(o[0]),
        .I1(x[1]),
        .I2(o[1]),
        .I3(x[0]),
        .O(S[0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry__0_i_1
       (.I0(x[14]),
        .I1(o[14]),
        .I2(x[15]),
        .I3(o[15]),
        .O(\x[14] [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry__0_i_2
       (.I0(x[12]),
        .I1(o[12]),
        .I2(o[13]),
        .I3(x[13]),
        .O(\x[14] [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry__0_i_3
       (.I0(x[10]),
        .I1(o[10]),
        .I2(o[11]),
        .I3(x[11]),
        .O(\x[14] [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry__0_i_4
       (.I0(x[8]),
        .I1(o[8]),
        .I2(o[9]),
        .I3(x[9]),
        .O(\x[14] [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry__0_i_5
       (.I0(o[14]),
        .I1(x[15]),
        .I2(o[15]),
        .I3(x[14]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_2 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry__0_i_6
       (.I0(o[12]),
        .I1(x[13]),
        .I2(o[13]),
        .I3(x[12]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_2 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry__0_i_7
       (.I0(o[10]),
        .I1(x[11]),
        .I2(o[11]),
        .I3(x[10]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_2 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry__0_i_8
       (.I0(o[8]),
        .I1(x[9]),
        .I2(o[9]),
        .I3(x[8]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_2 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry_i_1
       (.I0(x[6]),
        .I1(o[6]),
        .I2(o[7]),
        .I3(x[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry_i_2
       (.I0(x[4]),
        .I1(o[4]),
        .I2(o[5]),
        .I3(x[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry_i_3
       (.I0(x[2]),
        .I1(o[2]),
        .I2(o[3]),
        .I3(x[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry_i_4
       (.I0(x[0]),
        .I1(o[0]),
        .I2(o[1]),
        .I3(x[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry_i_5
       (.I0(o[6]),
        .I1(x[7]),
        .I2(o[7]),
        .I3(x[6]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_1 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry_i_6
       (.I0(o[4]),
        .I1(x[5]),
        .I2(o[5]),
        .I3(x[4]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_1 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry_i_7
       (.I0(o[2]),
        .I1(x[3]),
        .I2(o[3]),
        .I3(x[2]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_1 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry_i_8
       (.I0(o[0]),
        .I1(x[1]),
        .I2(o[1]),
        .I3(x[0]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_1 [0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_43
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45
   (o,
    r5_x_noise,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r5_x_noise;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r5_x_noise;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e
   (D,
    d,
    signaldomain_clk);
  output [13:0]D;
  input [13:0]d;
  input signaldomain_clk;

  wire [13:0]D;
  wire [13:13]convert4_dout_net;
  wire [13:0]d;
  wire signaldomain_clk;

  LUT1 #(
    .INIT(2'h1)) 
    \latency_pipe_5_26[0][13]_i_1 
       (.I0(convert4_dout_net),
        .O(D[13]));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[0].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[0]),
        .Q(D[0]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[10].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[10]),
        .Q(D[10]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[11].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[11]),
        .Q(D[11]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[12].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[12]),
        .Q(D[12]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[13].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[13]),
        .Q(convert4_dout_net),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[1].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[1]),
        .Q(D[1]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[2].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[2]),
        .Q(D[2]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[3].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[3]),
        .Q(D[3]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[4].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[4]),
        .Q(D[4]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[5].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[5]),
        .Q(D[5]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[6].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[6]),
        .Q(D[6]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[7].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[7]),
        .Q(D[7]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[8].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[8]),
        .Q(D[8]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[9].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[9]),
        .Q(D[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg
   (D,
    d,
    signaldomain_clk);
  output [13:0]D;
  input [13:0]d;
  input signaldomain_clk;

  wire [13:0]D;
  wire [13:0]d;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e \partial_one.last_srlc33e 
       (.D(D),
        .d(d),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0
   (o,
    r4_x_delay,
    axibusdomain_clk);
  output [9:0]o;
  input [9:0]r4_x_delay;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [9:0]o;
  wire [9:0]r4_x_delay;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_13 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r4_x_delay(r4_x_delay));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_10
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_14
   (d1_net,
    r8_en_pkd,
    axibusdomain_clk);
  output [0:0]d1_net;
  input [0:0]r8_en_pkd;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]r8_en_pkd;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_21 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r8_en_pkd(r8_en_pkd));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_15
   (d2_net,
    d1_net,
    signaldomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_20 \latency_gt_0.fd_array[1].reg_comp 
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_16
   (d3_net,
    d2_net,
    signaldomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input signaldomain_clk;

  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_19 \latency_gt_0.fd_array[1].reg_comp 
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_17
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]d3_net;
  input signaldomain_clk;

  wire [0:0]d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_18 \latency_gt_0.fd_array[1].reg_comp 
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_22
   (o,
    r7_x_max,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r7_x_max;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r7_x_max;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_29 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r7_x_max(r7_x_max));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_23
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_28 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_24
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_27 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_25
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    DI,
    \x[14] ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]DI;
  output [3:0]\x[14] ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [15:0]i;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_26 \latency_gt_0.fd_array[1].reg_comp 
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .i(i),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_30
   (o,
    r6_x_min,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r6_x_min;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r6_x_min;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_37 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r6_x_min(r6_x_min));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_31
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_36 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_32
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_35 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_33
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire signaldomain_clk;
  wire [15:0]x;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_34 \latency_gt_0.fd_array[1].reg_comp 
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_1 (\fd_prim_array[14].bit_is_0.fdre_comp_0 ),
        .i(i),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_38
   (o,
    r5_x_noise,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r5_x_noise;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r5_x_noise;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r5_x_noise(r5_x_noise));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_39
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_43 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41
   (S,
    o,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    \x[14] ,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[6].bit_is_0.fdre_comp ,
    \fd_prim_array[6].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[14].bit_is_0.fdre_comp_1 ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]\x[14] ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_42 \latency_gt_0.fd_array[1].reg_comp 
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_1 (\fd_prim_array[14].bit_is_0.fdre_comp_0 ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_2 (\fd_prim_array[14].bit_is_0.fdre_comp_1 ),
        .\fd_prim_array[6].bit_is_0.fdre_comp_0 (\fd_prim_array[6].bit_is_0.fdre_comp ),
        .\fd_prim_array[6].bit_is_0.fdre_comp_1 (\fd_prim_array[6].bit_is_0.fdre_comp_0 ),
        .i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_8
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_12 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_9
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_11 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width \ramloop[0].ram.r 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init \prim_init.ram 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_32 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_33 ;
  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;
  wire [15:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DIADI(dina),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO(douta),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP({\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_32 ,\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_33 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(ena),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr \valid.cstr 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* C_ADDRA_WIDTH = "10" *) (* C_ADDRB_WIDTH = "10" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "0" *) (* C_COUNT_18K_BRAM = "1" *) 
(* C_COUNT_36K_BRAM = "0" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.57205 mW" *) 
(* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "1" *) (* C_HAS_ENB = "0" *) 
(* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
(* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) (* C_HAS_REGCEA = "0" *) 
(* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) (* C_HAS_RSTB = "0" *) 
(* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) (* C_INITA_VAL = "0" *) 
(* C_INITB_VAL = "0" *) (* C_INIT_FILE = "ip_peakdetector_blk_mem_gen_i0.mem" *) (* C_INIT_FILE_NAME = "ip_peakdetector_blk_mem_gen_i0.mif" *) 
(* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "1" *) (* C_MEM_TYPE = "0" *) 
(* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) (* C_READ_DEPTH_A = "1024" *) 
(* C_READ_DEPTH_B = "1024" *) (* C_READ_LATENCY_A = "1" *) (* C_READ_LATENCY_B = "1" *) 
(* C_READ_WIDTH_A = "16" *) (* C_READ_WIDTH_B = "16" *) (* C_RSTRAM_A = "0" *) 
(* C_RSTRAM_B = "0" *) (* C_RST_PRIORITY_A = "CE" *) (* C_RST_PRIORITY_B = "CE" *) 
(* C_SIM_COLLISION_CHECK = "ALL" *) (* C_USE_BRAM_BLOCK = "0" *) (* C_USE_BYTE_WEA = "0" *) 
(* C_USE_BYTE_WEB = "0" *) (* C_USE_DEFAULT_DATA = "0" *) (* C_USE_ECC = "0" *) 
(* C_USE_SOFTECC = "0" *) (* C_USE_URAM = "0" *) (* C_WEA_WIDTH = "1" *) 
(* C_WEB_WIDTH = "1" *) (* C_WRITE_DEPTH_A = "1024" *) (* C_WRITE_DEPTH_B = "1024" *) 
(* C_WRITE_MODE_A = "READ_FIRST" *) (* C_WRITE_MODE_B = "WRITE_FIRST" *) (* C_WRITE_WIDTH_A = "16" *) 
(* C_WRITE_WIDTH_B = "16" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* c_family = "artix7" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [9:0]addra;
  input [15:0]dina;
  output [15:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [9:0]addrb;
  input [15:0]dinb;
  output [15:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [9:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [15:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [15:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [9:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  assign dbiterr = \<const0> ;
  assign doutb[15] = \<const0> ;
  assign doutb[14] = \<const0> ;
  assign doutb[13] = \<const0> ;
  assign doutb[12] = \<const0> ;
  assign doutb[11] = \<const0> ;
  assign doutb[10] = \<const0> ;
  assign doutb[9] = \<const0> ;
  assign doutb[8] = \<const0> ;
  assign doutb[7] = \<const0> ;
  assign doutb[6] = \<const0> ;
  assign doutb[5] = \<const0> ;
  assign doutb[4] = \<const0> ;
  assign doutb[3] = \<const0> ;
  assign doutb[2] = \<const0> ;
  assign doutb[1] = \<const0> ;
  assign doutb[0] = \<const0> ;
  assign rdaddrecc[9] = \<const0> ;
  assign rdaddrecc[8] = \<const0> ;
  assign rdaddrecc[7] = \<const0> ;
  assign rdaddrecc[6] = \<const0> ;
  assign rdaddrecc[5] = \<const0> ;
  assign rdaddrecc[4] = \<const0> ;
  assign rdaddrecc[3] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[9] = \<const0> ;
  assign s_axi_rdaddrecc[8] = \<const0> ;
  assign s_axi_rdaddrecc[7] = \<const0> ;
  assign s_axi_rdaddrecc[6] = \<const0> ;
  assign s_axi_rdaddrecc[5] = \<const0> ;
  assign s_axi_rdaddrecc[4] = \<const0> ;
  assign s_axi_rdaddrecc[3] = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[15] = \<const0> ;
  assign s_axi_rdata[14] = \<const0> ;
  assign s_axi_rdata[13] = \<const0> ;
  assign s_axi_rdata[12] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth inst_blk_mem_gen
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* C_AINIT_VAL = "0" *) (* C_BORROW_LOW = "1" *) (* C_CE_OVERRIDES_BYPASS = "1" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) 
(* C_LATENCY = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* c_a_type = "0" *) (* c_a_width = "17" *) (* c_add_mode = "1" *) 
(* c_b_constant = "0" *) (* c_b_type = "0" *) (* c_b_value = "00000000000000000" *) 
(* c_b_width = "17" *) (* c_bypass_low = "0" *) (* c_has_bypass = "0" *) 
(* c_has_c_in = "0" *) (* c_has_c_out = "0" *) (* c_out_width = "17" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [16:0]A;
  input [16:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [16:0]S;

  wire \<const0> ;
  wire [16:0]A;
  wire [16:0]B;
  wire CE;
  wire CLK;
  wire [16:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "17" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "00000000000000000" *) 
  (* c_b_width = "17" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "17" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_AINIT_VAL = "0" *) (* C_BORROW_LOW = "1" *) (* C_CE_OVERRIDES_BYPASS = "1" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) 
(* C_LATENCY = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* ORIG_REF_NAME = "c_addsub_v12_0_13" *) (* c_a_type = "0" *) (* c_a_width = "17" *) 
(* c_add_mode = "1" *) (* c_b_constant = "0" *) (* c_b_type = "0" *) 
(* c_b_value = "00000000000000000" *) (* c_b_width = "17" *) (* c_bypass_low = "0" *) 
(* c_has_bypass = "0" *) (* c_has_c_in = "0" *) (* c_has_c_out = "0" *) 
(* c_out_width = "17" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [16:0]A;
  input [16:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [16:0]S;

  wire \<const0> ;
  wire [16:0]A;
  wire [16:0]B;
  wire CE;
  wire CLK;
  wire [16:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "17" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "00000000000000000" *) 
  (* c_b_width = "17" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "17" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13_viv__2 xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_LOAD = "1" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "1" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "0" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "0" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "10" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [9:0]L;
  output THRESH0;
  output [9:0]Q;

  wire \<const0> ;
  wire CE;
  wire CLK;
  wire [9:0]L;
  wire LOAD;
  wire [9:0]Q;
  wire SINIT;
  wire NLW_i_synth_THRESH0_UNCONNECTED;

  assign THRESH0 = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13_viv i_synth
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_i_synth_THRESH0_UNCONNECTED),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UrnrJN3o8USoKrF7Lqoq7dKSHsU+2r2vrpOLb6lCJjE1IUV6HN8nEma/JtHAFFNt4FgXGnCMJg7v
DQAkB8v6mlWjc5V6NDYWrFczD2deajpfVTfulsx0VvZDnrGXNt09IqaPjDSWb8VLV7vG192tYH9c
xKz7N6LUe0BybsN7jfFlSajk5HWWa4cYqYafqjvEg2vBfo0EnJzkiRCHtJSP8yUzmcJZ47/VvLu4
70Hj+s0MXkEIrBWRHx7IyfPU3HDlOcN2MOsxmdhicGKYkx3Q6Q770Yp5hpLSe0hTYMOVag4xPtT/
8sWUnZ3TYLCnyDnYXnhbAN5ToT6/vfR+RQNTaA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ow4D+RzNrfydznjoCQm3yF9bD0Evxq+weWRkxuyyFOzUQluVMYyAJmyvMVk7K0PJFy8uWO+2yMTV
/oOLIrYhpRc9vQ34paDFjYXpdel2f8H5HbZu3iNV99G65ZQNO8N41eZ8cOOwvk0Gbxp7b3revuO3
JrjkULYnLfweDFQh2UBvJ24ulZI0QoYhAcQCJy3TmVsb7QPq1TEi/EkgUzNKrb370pBzZ8hFr0Vh
WJoR851vqBub9NAAC0bZuxhcn5nwqP4w4yPXGpY7HII6R9SSzAobtwtlvdEcPy82YQtqXXk+g9p+
wowTMqKzY845tJ/hLGtZVkn7GzjjVGfGGrQghQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 40944)
`pragma protect data_block
QisF4bIXjkhhezlFUIKk4f3kRXe2v1MAiWdsjGeYm7lX8EJ2fLxg7UDIt7T1fbWzRqTfRX7grnHs
Kcr52zKywti2AowotXb/rjUfsIKaq5CCJoQW9f9V75b/RqTdtUjnzlhUmxvdLD+1TjnPRpwMc2uZ
1pAXeFc1rV93cGStQKaxeaSM0DQ6XRY2rfvVlt2A6SVKDiBX+kTzNtFsi8xJ7pDKiXYiNkJ9yQxO
X1iZ6+FyYhU659H/4NJFHGG8gERoDCKZEYdkdkBRMi2L+VkTwDqzNK6HXpgL3t0zVQ+lKhQjBTK7
C86giwUpuGdaAqkYR0HMDzKpwBCWPWZB+XOiQb+xZsEN85OCJKjq43wnxTmPLSLfIArL5YvRKYax
jCOG2uCuqa9f7/nLEAEsNy/RUa7Z2kowqnJLZn6FuxsjukjVVQmRZ1s5L4WxGEDY80/WhaTu6w/E
tyRMckJh7W0IhaLgKNDV3pyjwhZMbnM6bqr8jFK9FnxRmtHMioenZU5GhV4+ewl66WWEkUK4AQPB
BXYeW1NY3UkJq40H14bWPv860C9QFwSbkhhXeQe+vxKcLsA7kvhJgqGEeqfECfKgrMd6y/KLX8hU
sIkfwAdtd5Lt53axm/mJTBotuUWYAELFLb4Fr3HxtpyByuqM2rszlvPh0qFRxX0qGio2NkYRR6Nv
9nDQJtGRMdVqYAshzLnncld/NdG4J98n4kKM0+Q6AGluwvpf8F4cWk1yhdduOT2Cpn/j7j5ro76A
YMbAHt0/qJbtnilAqAHgKMwNCMhNripG4TW5Xc0dvEYQ+RtvKzXecomYRGvydNfd8Ncxn7Bs40KU
FOGGG9rWjPrlcXRCFjS9wW70p9fB+cf1YGsd9gvD5hHdmMuYYRQM+0x87X3lRSZzhQLYnfat8xx3
RQYjExQqfK5pxR6prCDzRM0Pe4xZvrjH0bIdYhNvT8ncygBWe0ct8iqRHGU+WGK9pOXxagLm/tN0
j5WLQTdt4tY+IKd7SuXcbYYN7Z0vnw9wMiBc6XNRd2FLrjX9zfRyhbGqb4PFr5ow9PYpcBl8izdf
1QzZiHQNoK4atD3W1Gejd8GTwwXZ7JobRWM9jhQx2OyGiBA4Gd5Z3EHIGw4QsQhbao5uRPRWkl0s
iX+roGKVLjKx3Zfxt+l0M1K9AnyhEe9txxg9MVzb0jEtKrkTtyA0+rkAIVzAiQUINy63mvchS9Tf
aXFVAgXnQAIKUDJm7c8Ks/pDUb/iHBEk3WjfoknKhLHNf0nKPaMkkzt23dhRsEesCC0J2/9IA/ZP
JNK95dciv1F+Z/j0Ucym3AHtdXhtbNz8a/Vu7M015szLw2nEcI3RhwBXRAfiKl2pNOryBtt2bPhR
/jW4qW2FRZekJMrWL3qC0LTkyXsYQDawU2FWkFbJfCIhf+fcottbP20y5nk1EB9/L4pSLQwF16/2
WzngXnUlItzQxjq7v4dZsATLGf5ztWRVKo6cBTciLXHFYGSBTOJjy75X6YsvA/R/5/AnmGhVNGj5
Sj1f2Hk5CsWiFsjU/EEacrTB4f4BWmyWl5zZu4SqqYVTUjf1hYKSnd4MJ0LpWgDll5EM3p0ik5WY
02YaqP18NnkhabTEz6XFa/IE/u0CGSJwwWsJcDKFgOTQ6RbbpGhRLJGZf90cET0CnHi7qkrTa3Mu
DVsuLBu2ItWfBkkshck/yg5JvfAawpWjhc7r+aIVRzeRjgWe6TJHycDt4dy73AW4gjlKn3YvvWqE
6YNryIrKCOk06JX0eDjc2MImfBMHkp56tE7Joc56ZKStqYCLfv5O3Az4ylhnvZ5mYCK+yGCyJIU1
jjphaoYzaZSrCpcQjphdwvHHtGBehwLhr7R3vH4wzysx2b88gXUiUee/moQGFnn3OcJPhEmgfkJE
s+c6eDTkgeBhxoS560rJU3rnr5Fu/Qxr6cPQ8jWGr1UXEUKoHtR8KXSDuLHdzLGDvLuA76uAwYji
GMznvX04B4EVfONpfaASrbWnF7Cotfi/R/meP+MYcfE98UX2cMCFNNkhEd8sSdnO2Jbb5gBsLP02
CaVjvJq6nXcO8Alwv4O6zMABXZGuKBMiWLgVDizhjvrIkkYIE5f3LfQiCuRf6DUpnqMqTVZVqKYM
cU7cUeSWTJ8yGdfNuMggAYXcB1dtSD/i7K5iT63cHBjlG4jpoxDL8ghi0zmk4KfOxCmLs5T59kpr
L3hf3LbB5OJP0Rs46z4jBMsrUQOtZMTGfATGAAthY+8M1K1nxq/PrEdmfDGm2PBHXNeXIa1Y3A7m
TEmziVc3SK304L+XVXqlOwvbJV2QMGxt8CobrwgqXgWGr/zrY9YtxLNor2zVvDzaFihOV63QD+Fd
k3NWkOgcMC/hyHgseUKMIl3gMxHNliYuN/2U3NIUYEDkJOeYaJ5EKepuRvWzJfJSRKZBj863bG6f
8jrtALRz2V/DTadPFkUpGmJ1G9ntWHvH8jPcB3rsSmp3FBkqyWMBHSPbOoT8RHWH9FBSpy05XgHJ
5VgbFQHOe0Az6ErJvq9WWtASYuf2aMGc7qKO+eJLtR7RTBfVqLjVDqCkdEi+l8EgRjAUM42v/+gQ
IzCOytac/rR5YIh4Ea07Zd7dib21STVkSJ+K/HCoSe+wLo4FHPbY14XVU6ipua3Ppa+DB20H6M+h
imxhnKBwGD7baw8SsaHMcgkzBD8iSGCEWwM8RuRAQ8/gsoq3rNfEkUCIzK8uWFTPevLEwDLRJGPg
D2UAxy/Al+CL+T2fpaREeZ7zvYFAbfdT7TedxaJM3BKNzEI7z3X7TsNTaB9hdQ6lgCKv5Hu3xTaW
5cD6aYBnFZEVduheb8A+yQn1KR32YI2dqltoErAj/TG9pqv5wb1E1o85p5CaVY7XDo8eJhaXm6dF
EMLhQuPqg9PZMFPAFLRqOPC3d+C9KmkY0qjU1C6oG/BbkUtL+UPC+GVxX7VKgO69owWzlCH+2szB
AqQ63I5DErg8hc/EaNawpH7V8f1fPp2e8ZYImVcmZgV5ekkC8xuQdHA2q1IOgb56ymNPhMjKvYUT
DMeXZrP7r4UOLFVhW0OCfI3V/WQ8r1Gfn++AAzQZiLJ+hXIv02HbYj9FV+d4MBtcAXg1YXtfJGI5
xNr2DBpzJbfrhLfjg7xF6Dtu28hyGn4HrH4z2Z8H7mMig+5pUiupPeMAE5/o7trVNdnbEeuVhFDz
K+nsMKMLIWVnJmSvq3fDJI8tQb+L3u8r7gQdQ/jHJz6VezUedzYut5HeWydt/Hd4EQS4gDPf6xvb
y3drb6fx1s7UCjE2/Ye0XEID8MU+XGMn9AcRaxaTOLJyB7rl6AxSC+n/HLTjFXGZ3JBscQTAFaMI
aa06uP80q8qZLNNLVDH3nX68z7FaF6NPrA9MS4LXJtvmxZZktv6rTOaXkA1KZYYSWtLSthEj0J3d
5/YWs95/U+7S59sdXeUr+CuSeQjkqyeX3bGd+cXr6b8uJOhlIW1pNWHeW7tXiTA4DR3VqB9dYlxJ
c/kcm12eo7sdOyiP4WIm7LFD03N+5/mjhv7rE1Pb9LlTz24L/roOYlHUpzwIw7Q70/hBgwZaTt9b
/K2ffnNgk/23zWhbqAzQ1hobB8LIKGA2vEfydJmWhQmY2ICeDcwQCPzFfeSj5iNm0SdJfkqtyuBM
hV/luQQSkO5kX3/ebin3RbutLCl6kpXjBzpmzfN1FPDqmKwSC+4M3io63090AADB2dhKOFkfphMf
0irXEXTCBEO5ezS1gbHcpqT9KbRpsGReQ7lZc10wo2sJgcEqHGx+hppAMlVMCXyf1qeH2zmtUXdX
gEvI9XAMMGKLc8S4XPLydoLW6aKjEAjJx/DEwaQ5WWPix77w5YNW1mppgTmli69HAmiFbG/Vs3aP
+WUY9TWTRzr1qZEiuz3PdgcZuzJBTO3tS68v0Y2J4s858fPmrwUMwMS03XH6LOQ/AYVMxz7h2BtS
s8wy+t8GgrjMnX+oT73kX6UtDRA4fZON9oOyYFpu4XVgqXVFzTIhaiSLb7O/6GjS9fIWZYBA000N
AU5i+48HU4jByLrJwTn4XDbVdjJYoFw78iJuBj39wUvRGOrFK1pPNwoOQNl5QhOGZQd5VyCN4kMH
043sRaCHa5FwYDWCHu1YLYqVCaXDS6jDIzF8LRgyBG3BcVrRDp9s5JOSjc4O1M98LMCiIJldDnH4
7WpG4zLXau34CwcllAoV0mT8I+XCNavuq1AODzgAIneTrtX6ct5gv1cbeb/QiwdWonQjPbKncCje
1SBhI0nrfujlnyt0xUNvuXDiHnEe6tasCQHuHGjf9chIU277dFdZOutNKqmeL+xJ85p2deAp07VH
gSv9hixqyAO7ENF6KsWjQU3YfwcrXPxPDy/LJ5+PZeTEJGstg1Vc7X7hDghfAMvifv40wy45z+at
PdQh0Ro7LornxcYVlPtaact1Wy0yeA5GwvNaEZt0eLuTO8CQ/uRbqsT68BY2y2RMnkm93nwm4rsV
Qvy8Pnm3NWeu3rucEPx60TpeQ8UyLau612cn1C1lwYcjuNsWDHjlx/FnNRawYwDjoRsZnswxRn+9
yeFUJPXHH9a6houvVsKaf6bTAZqFsZlSO2mURjcqHX/5Z5wLnI4gUm07mpF0vIWesyNe/sImRSpX
TdJXWORGYuN/+TIKIXywnqbQhbd8GSQf2gx3WqbfR4bbq78EzsPqNajnYZz5z1GnrkgUKD6muVoe
6RRvOpir27/hruzeXtUZ/sWuuV7M8G+cy4530TE0FQ6E+XO7+fwKqQMRvEMDXo6EiqfBs4Nmz7SJ
7M3epPpob3Besb3yurARZYEUUaHbP/vi88rNHhlTkqfZk7ZUhPHNrhQEzrTmWELIFHzK522HMKVW
P3vpOLdF/e6Kli2jmq4XTPOoXTmJ/ZVezFIN6eITsIqXxwAPxwPr2tTQUxEVjUF/0OhSEmbBsXbd
imwCBUGLLznUd34xiaX5OvWnDWyJq6aCCiEbIMCv5ehtYw139RMAwWB70CpGFTJq64aEei/0ow4B
qIPkL7q2UILcHTutZpLTMDkcC8yWjjGep+MJ2JFvVWtqnLvWPqtbH+A2L2OFrZGrhgpy1t1t9GNv
2YKi/gymgZ73ti+/4z5Do7+NS3cUeYtMR7CYPmVM4kHpmm1CPwbpNaNJKKEh8a7rSi8//MEZ8VwC
5D72PitESQibHAoZbTxjTlqvQLTRg3uKksopUbI7Uqpl8zJU6RgDCbspONb4LpsHynQx6kHt51p7
yqJznd2YgL2R5pHVV2knLJOfgIN+oLo+TrrONrXAPfsOFoheFwwjDuXf2nJ0fbLMuPR7tRpJTkl6
//drBni5NCzBPKEOF0DY1YMoonTvDhqFYLFuX0g4rV1+VgOSYlDe+TfNdRYZJcM1IPOug8lCh2D6
t+fEv563eKy+voh3dxoIUI/NVIP7ZSifGJ+gKdrj5N5cgLEtaLrKOmWAoFSOjo6xHZqz1BeteLYM
BbtW+nT2vMuZcY3CHXK5154JAIm0ItWJ9Dqp7caH1SEZEvEcfbjfMhZBqZ4tfFDNPpLkW0ALw5eC
9Q4UeQKo7k6uIKho/E0jVAbhUSzyqtI2wUhUF6y0MmwIjpMOG/jJyul1D8gvMsvlhZVwspz0tb0G
/FggDseoC5WOnZBNpmtMX3w22FccBp/E/vqM6cYQgH1b4Nx0hGQVPhfq/2SqokXWPj4LgHZfQgy6
hG4UK29m0u6vXUj9ne8LLTceoBpa058x9FNx1BsKLBXBdFTCth4btbD39xKMmqZ+CsZ0/M7Mlk76
Y3Y0TzC4ZzRQMyjQchDL71Q91/RfyQZVqrW35/HxSJvm52FEoLdwlU903IYKOWMxHRIUS6Tfsd2F
gYg0q4EaR01NEmHFUvNWuO/snliPS09RxIuhF20YHi0ZUMM0Zd3nhDuukhwQXJOGXywj0/HXsPaN
Y5YI3BvEU9nyMIIJarOBOqDJvKw+cpD8JiY3pxgLLpKMKFE3WjHgaoXW0neSz94cA/AwAwc/URqg
GdjjfTP7NlOglZ8nfvCmTab3QDT5QuFOHvrYa2l0NDuFPhr8iIo1MAXR52PDb9OiiFDUy82ekyM/
QFImewM6stWWHcowjLeEZg2C64kIuLCeeVnHCODvX38iKRlgmZRgIsLHBU+6t+IQWw58sghqtGHl
fYGBvaIhuauhT7aYqUiP+C/fUpf9/IxujJU8WZHgiAk0+12mVm/rAtPFmbxxpThOtwQlHdmi8pRR
rRXF7gJB1Ks+eukI/IhmAqFF/5d9z5h4yTldN0kQNXv04wcbK0DmQ2QropH8F5fnY8rUsEM85t6b
LxA0p5t8RAhuySJICrDl3qNd0N/y42eTyIEKYt87Z4bUK3cHbjt9OlKtg9ta36hNZYfnw2mkUTWU
H7bMY2cqRhPtKzCjStjS9954cb7+grvkW34R/WaetkUStLH4ObNFPTWXAFqCGy1ifgYfvTISzKqO
k7+MhufvkzUD6vvAfkm3I9U9VjuEs1f3LNSXAkVYbux3ebyV3yAlOyP7NYkD6odYPONIX0i+v3Lx
Ea/lDt53GlOirubjD8BsMlCRXmTQW+lq1/VnK2RKaGXpBjBD1R+F9NvO6pqe9xOlrlZafzPZ9Z76
cP12wE7tVdHDRCLc9OA0wtKcymLIHADVfEgiKbmswsCBADfqU34ElXveoyQWHJMvpbygrqOM88Iv
LwsLfTLYqYZ4+X/3OaQDPh0amlF8wQroMS3a+TMYFL9m/HwpZvvncAOS7fti0MGprjLbRgUlip4L
HR1qXqyXF9itf8TIyeei70zgIw3Qq21OstiUUGHlVR5kRsVBHFQild3OcWY1f9W1pJ6Adlmq+tdn
0eCitnjcBwpP0ioS66JHuf2h/BztH21faaiac9dC4OdbFVxzwBuHEH3TD2OOMgUCBinhVNxwJlwP
3aGFDb/MkPLS4BSGvBChi243wiTcD5CT8xb14iLM0G3JLf5QaFqA3qrkClld4b25HZ386vJGilxl
pOzEhbPm2JBk1ps+CuGFtwRqDJuu8EoyDM6Czzi2Q5RVD21rHHVFjnnUBTVK9c0L5wx1AGnLOLAC
G+iac0oj+O2lJz4aZj+jGz03RthbLD+7v2VsR8xUjH1+vlgT2BD5bYtxH2wbnrSeNUzHfPT9GG1Z
FMNYRsZ3uOv5blFiEEWlGCNNM+HFL0w+VYLmnfmDnSEZvJ0DNEUQdWHs4x6LqkGUFTehkw+v8l8K
x2Vz/geeF/fk1ruy31m0cBcGDJWBbIEZ7m4EkkU09GAwG2UK/RD2bGlo1swIsvw1AOSn/99ldCz8
xDIpRR4mMI0zl+KBsaEOIc+Jage0w322fjoMFHPGsTELMoUtUKXAy6bMYjzXfn9Dp5odat2ETIQZ
tz0YuNenSce/vurG5i28qqcXPy+rIHwA+ePHS/zJW6gqlA1PU91nvvEiRPta6FKS7eFWAkgLJOPW
l0NETVeoDNShmysOd3uFDfUFE5PBLKJ3YQ8dqVicD+RVeMdxlsvcrsQkrHkBJe7E9WPNjar0g6e4
eZw19Sr7+AbowQ9aHGPrp3vKL+zKx+RJ+G6pCAOnD5Jr7UW/SRHvfsXMys3XW+oqM7KSDvumvm8y
iQHBkoBfg+phv+9bOm/20R83sn1CL+HT/V6DxKj+tc4U5ssatRRbKPa0jktHG7QOENOElj7Hpxe5
HyjL44wGUM6HKUoPIFOyHdnTPFWJ1uTyfP8oKJprP2QErvttbyHK7+q7C94zvgkA2FXFSBACtPTh
MXzy1UgEJ7W5jqw9T9mSFySFmswW2xQWpnYfb8DvU+4WsrqKvX0QkvzEDXAca8LIrZsKGnNBav1m
LpXINwY3pAcksEJO6+TlQa0jm+1P8WNrUAWwCGLFnus4NeUax3bcN3lgvXure4lT0sql1QKMJNL+
71yW/fffdWWR79ZpgNsvCnYgCNJBMqtrlla3Dyhg9tdip78W6I+Fpii3bB+Md6FPiukreEN9dHOz
TTe1kvRlzK1avL9IIaX1A0cgLwXv//1ZbjH5UPE7CeAvuXc/tz66vSFT6e59yyjiB41tcRsEh+U4
9bD2UmEAi0lULc8qYItW7tGmgPJlLrdAHzcOU1WSmdsy86Avo7mkO2DiPrrkv3yAi7JzBSsNzWxI
L08GsOn1AQsAHZ27B9blCjKPOv642+vFPKCFYnZgWBaA7yvumKHuOReNscjoCXB5YLVyoW8mDPR3
YPrjnqilqceqcf/GBB22B+AZTRpjBT4pRkLJZ4DWJR+fBMR/V/cyGQjjgtWVo+wOSmBti62W1sKr
6Q5QxtlqRm9YAKm6fV4ANDaYDmjwhX1w72SwZvchGppb1R+lqCQpZJTB+Ci8uY7XU0RmqmC6+Owt
y31EODvrP9muhf8xlD87QIUx0bxa8Y28wlGeTDZECfUAv/vhhQgzyzhw2L95xk0t8htTBG5Y0Z5a
0vwt4q7BarCmCjiUL6LjcCiAh2d4InNOA0nD3noMDAHCTh1/8tfFqdDKou3oOKh5kdDtJuKxjlJM
MI2Bc9Ts/QqIk03FfmM4hl5DWCVFG48nWZ7rv+O4Iqhp2ecc7l9hTl6OMgMPuAOnwFN9V9ggIX7I
vtv1VcltZVlutMZT8+KIaKoZ5h0HMBtGJiUOB4h+8Bskw6owt7IAGTTsNvjxLcYzMbURXSxUhfqt
TS/gB/Au7L7jOONJa2lkBq6qgF7I/NyfShsqrL2i2LY/z4Kz+OZox4DoX2ISrh766zJvKRKw2R3g
bQSW/INb0feU80bKwA6ZMIEds5mijy0fVfxt3EdI3gijxaRPEVpzj1p+C9lXGqP+2VT0C3O6c+NN
83xd1L0Vbtg1340Q1jAP6aK7jA//2w+7JosuuIKlGymezr42cf+UyaE7jVwsavDwUMuICFRJcL9L
xRPE0Tk13r+7SsZXsZUtXuL9RMk6ZoBrZ8w3Jx9l9/EsrvDVnxeQPzJBb4AeXoS9pEv9rvjr/Z/l
g1K0laCBzarPEbea0IkGFIpdeYXqLoAGTxXhw5Pz3ri/aNH4t2CeX7GY1T9/yxjy+JACZvWNUdkf
aqSsiPCT4uhzmXH05SCeA97r9+uBYAFDhH6uX9HuNK1tZojQaq4nRkCeJrua4RIiu4i6dlpJiEw8
UKlhLTfsWat948HgriIzWfxnLVugn/j8v0GRXHPzf6SmN3Lh9/2kSjOMz0zM6GasSv+Lel7XHWF8
L0xRGOB7MuaeuxD281LJMH4Q5MvXivHFS2PD09XzxC3XaCq2hCmIcrNXg/qzlPOGJqH2c2/+Ml+2
e3Q1Wah9z310XoeKZTOEeEUQqQznAJQiWWiOocea4bfxwKvQOjuoNuCBAIpBVnuGoD5s892c4Uo7
LtVn2n78+tbusIGvtdtx2bDlwN25YP+FLnlXCpnV7wBtbf0ByP8+eEAbrzQ82pl4tpk0NTalUqtd
RZifePHLFpWDgoZv/xRuZXZYoeDw+PBvxOI9oDJko7X8j/hKzA8pHsaGZM7VyYIlc24qO/l+J7vf
v3dSaNy4IBjcmP8dFvuHBXrFVMRalXV2B86CMcj6q9jHYHZbToMw0+CKlitlWBb0S7Y3jYslG+j3
5Lrh5TF9xsLyRq2IMmBFysNH9H9xDo7mhNSgtFoalMFxSWQ6WZz1cr/CaYtKRCYd2JFXwhoxgr5H
vUV/sIF0TkfcDColfj7wkLyeav9kcJ3sV9dwXo9/Inx30m6Rr3aKNCF+3cy/sTjP6nWt7J+8UkEo
KJEgYquOnrb9MzDZsYjNpqmcXfb51CDp6tt/OGZCNhPXg+V1j4dkvriHJg+YiFD5huAz+IQIRF38
SVyAQeODLm+o3LkOkz7HXMph0L2laTrEQGvT2bvKCT2bEmIBxQMjav2EaQ61w3ybmpGF+Brl0FxD
hEDx+8swfBfrOE6Yn7QRwg8irCIYPE4DARUCUdOxl0qDmvMWvxAbtNg9eXIy1DGRruqYH7rR5Tnz
iW3Gk6PfKSlVtMcm9brQ/xENEIVIWbbnR4p5wE4YOpw9kbRvKfBBwx5uaJ8qMie8T1xgZFoQ+H25
pnwfYgtbA/Tn4t4xSHMXolZiw8C5jFocv5Bqd433XO5Vz3iBvNdw4epUNta2A4Vs1rirYP4WN8Kd
714FOLfFEBq+qMsJ65cnzwX2x8aZELIejFfAb51A+HZDRBLy+WOeRy56yZy86aEnzZv6LBC0C5i8
Z5na91e8fcjGDDqDnqAIdHBhhCorCsBTW0UIX0mamxNr5y2mdzlS2p23PtJyVO/KNxquJKKN8YZP
Jg7yRPzB7vZESbKinrz3uizkmBqysBoiyEZPthWcbeqVD3Mbaakx3mhpYCYOKHrGkT4DqidJc+1e
RmBduUhhgEar3LWQKgrT2ct+6cdjf7glav62PIm/Ti8XFl6nwWt8Wncq2CVPtSeP+PoB3aYbDwEx
6BALs7Xh4kSd+b6enQngpZQ8LcvKzI4oWyilt6bVStA1kKMIrlOd31u2ZOYVycPYggovG3UprJLs
efUP2ewJ7qtbn6e5fhxL0Toc0ht1YVWJTMvbaYha9Vm5YT4nv5IS6k8kfiMB8mlFSCQEVESPgP5V
Qb7+VGTOWrGKJlmwh1t8KJ79dS5TNoyesIRQboNX2dH9/G1BaUEA9kpgfcCfVU7sfVlwrGCfJ2nZ
VdAzuAo8LD2DjDG5Onj6nlzgrfuwy8j2wg+7WEIRANf8cAUylVWG8tGKMQBZ5CYmsHuuGP7123LT
DKbSdcREwrRz43ssBZri/MYRhFkvFh+mAbayvX02VT7ff2g5NIXPTy7LFRCweWcdQ5SSRpDgncl5
VKjOAxFhZTZOhlBcpLyIuzc2r+XRzflpG0o5nLFygSnI0duJzLgOq6Dr9f3cmNiRckAlSXI7GmSI
+WK359N7G8+Ow9eiisDmoTEvSKMVC2BzVXLGpWkCDQnXYVLRLTf/6InXH54dc7ncve0KyYWNAOTh
aus2Ar5BSsGpeWRFcPl52r3MXbukVJ3fbzRFh4U+SPhj76g5SVg9mW3mMaDVlj0cWvsSAfa2wXLO
cAy3UVq+yYlqfbTlIiZnHoITfTCqoykayv5vCCflcMW1lWJdhnvI0rGswYnnGFT0tbtXNSJyNGcq
Pz5xQ4RuFRnTY16gM1YkUBH56secuw+fRegXnMSWN8TF3mM00YAk2yixrs0YWyZTDVPE8Q8t3Fnz
YCh2ZWZrsK0TV6aERlWNiRA8OvHBu6TjcTVE0+QRs9LuNZxb91Y2lFltr1MteECBqAhQ4knyvSAE
6B/rhgYaFUEAsFDsH+qKV1RxLG6z/ebPdAOsdBhAwSSdXxXpOT7gNn6kd7r/M4RRXb/b5gK5KyTp
CKKFKi1hkBy2nVeDOONCsWYU7nYYaRsrGc+U6b4fGJuoSEi6/uUQWg3/Xz+FSQ9wT0u5YubHH5Q6
Pf6fDT98TrHP58YVjGDOwAyPuLeuMnefMQ6WZP3zP3gaCCLyOQPFyvW3FSbAsedda5/b6BFHTMxM
SikdwrGgJKK3M5JIULRD3aikmgtianJ2ThivO4TKK79WRUgBKKDb+qExwsk7i2wl+iEb0gSQwMC2
PvMRumFBwk2FhqyAQtXFKz+S5rmGaCNzYkW5O1li7nEZfxlS192YpcDPBxfUDt1uQaBauX3wHjBv
3UhRgopVBsUU8ZlXkMEo1LddfzMtsDGtw0TMbe8KOUa1YFClN2Jlkc7yRdhYJszXLan+3udONHy6
zZmW0vHwzI+X3n0cJ7qsY1ab4+1csuRO8T40hRKQVOxH7gLnUsZT6DcbM7L+PpXIunJ0YmFNLTr2
MdYLck56kr91QWkQx+j+ODS1diNC/WSudc3M9zOkS1tduvVaXpJHGQGCGDsMIlQngJGl6NFKjS7t
saL/xtuo6DaOc9INwu1Gcuz3TYrE+5bV4b4endyt6hTxvnusDRxKUAudOfTaGspLiyJbtfwzO+NU
jLRzeVf9bHlXOsSu0bVhnN1I99L64u8R+Zg7MrQmpnr44rDKZZriGFRR0I4Lnu9CS0wO6Ic8b24P
ZOBrA6+parhxwkZywsZrVXnOxQZDoXRG52u5eHYuLhZTCCdQ3RkKQ6/bxVtgY4qHAVjIAuJ2/mWh
UEoBiORZqY1FfBt+OQDILiBU21fHchX/u6xWuC9vGV0HbvPg/wqh1cpAe8OSFkd0eafpBTVMsuZe
nejXX1+hyc4lgPZWKiO0i+iS0AOk9+IPiEAKPK8DKtC+BdGj/WV6X39Lng7CNJptNQC1FGep6taz
5Pb7SJpT8RQg6y2o+MbEy0ZM7nDctV/g7bfDKALjdd2k4c8BCGQfiTr9lOFjX4VbKulsYil9hKA3
Uq4SgkxzHZDSTtd0ro16bSEs3oSsAnNlMwHWqg1c3JivY1DLCcH9hSUbxK8urcmMResDMV4C+ueL
Ef80ffUtE/OkTI5KceEyjiUeWvEM/8DOFLWkSowDfU1fwsfSToJXCQtnRq9y+wQYcoDCYjslA8mV
cmcS3eQOJ+mPrmtOjF1wxysMm68pNPNlqZRNoJn8zxaoucjDFSnHGREI42dmydnkOIda5y1rm2Vq
cx54+BauI2lFEciuG/zK8MIw3wvi7RAPxI6H/8xYSwlQls6lMBnLaLwxsa7K+Z4Aqh8zwUs2Es/A
/rmgFtkwJbFsbhpFA2rq8IY4rZ0KUOGlRq9qRnFmEnXBi31/JQXe3d7ieFkNjQJl8pWDwyIuwHN/
gxfnQEQqAOT+YIvUYizSzbeA6m/fvGv0shce0HqCWK8aJAjwDdFATJcAXSCsTLhpI/dykJUaJOmP
vjs5HK+m8lEqHxMps84gzuiSlvVytyTID7PVw6rSQS4dZChVMEveddFiNjPs9lfYYLO/7QsPBjQ/
vQaXV42HOGvxHiryEoEEUsK+sx1XQ23U0wpL4u/nCCI3B3giVGWjve73xX4il4n6gc/nxMWD1NvX
/t5LKlM0noJWrqO9hR2MAzVa0h8krjMqaSSuiGHWxreP8fTa5VN6dLJRe1lrk8A2oldc1xPM5+I/
8O23Hnyv4NZ3Vy1aMs4WZ7uj0jQ34XxvfePw/3HspvpBOF/hV8pddectcM5Mdkiuaaap7TF58egr
IGgEOP8jnjS7mjATUv5kIMNgSBS9nM8P87qkJpmgafXRSvshaqweWM5vLdISrhempQ09P1WVrWU5
th3QWBrCKPie/BuZF3e5YlK8qqtoZZS+f89pprkjZRzX5pBpgIvD8b776Z2Kg8n5FAl3VSml73N/
Cx4zZxUyW2nE/OnPCoop06OIwVKtawEHrpq3onBzb8iBYp0IgjZfDfIy5CAoJZkqCjW3nn3aN6PT
M2clweLcDqCVtZ4JdwQsMYs5BKP/tUvFb15KVX4UazWvFpsDyyJRte7YyLm5seNxZ5EBxpq+RXUv
YeSgul1Oa+yNytJrRLI1/wydHdOXVafHLiGjKeISMdB91v00dYsEENLMYaaW8owq/ixagp1H7XM8
G61rgUF0W/40E5BMkC0NIpMwdlM0aQm1MIl/3pkvk3kGJJQcwld8APb3nzKa68DJtZv/Hm3uuELO
0RcOZnmkX7lRWAyl856H8IIIiEjIHI1CUmmPExQCd1VWv7Lgj6gi+v/06XdeVKHIyOTaVUYxhaJW
6QmW2zisGR0b8msJB2ypi4LrfHZtoTWWj5N/HqkZT6w7Kwf3iMSzpM0nf2LTDy1WM0eDmuTtdMfA
N34fajZXoWqiBJMaKntzgMKKkUmTBCYzDH3GWGvVAYmnk7LN9fLaMqrXXuNnwC0PtQP0YyfLCeN+
oDKtb3KscnpoUpRFW4nF0vQf/pBcl4n9Lg2AuBKYp69TAhnbfQLyUJlcu1QN1gvir2CgtvU2Ybvo
kb6XwgzZby/1ApEKJSM5p9KfQQxGrH78G/KOt76S5L+6UxonT7fomDjF8gtBNW9B2I+/kplo6im/
Uf39D+rgDwTyKWhky8qkBmRtUwFdvNxgJWc0bb11cXfQCnsTaclJQ10GsJXEiGHsc7lFLKzCxsdb
nPZjDLaBrK7ihRWV61nilTcQcf88c2GueFZjvPeaVJsSb2GOybScrDFQX/XgbovECu9Lkqmihzre
vCkiKl24QMsFkDtEUERXAtUn02xvFBpBNI9vBP4NFrY+RSQjk4CPD74dANLaVOlh4SSvzYgMkM2d
F4C/Z/AULBxus5JuBbjGGYyL9JVakL+fA8sEeoDbZlvw2ipoOppbcpyDAucoU43g1aXqCEx/O+hE
80z91hr8cDzwkGwqvERJWSC6XVrl7I7aXCr0SUQAO8wJwSy1kl1zaX1bojCTtTAXNzKFKADLlLts
u0ev4lWmXkXqq3K40a6KXtJQLAkeXo8JL+MsQbbTaAubGKWP1PsRXJV3xOA9USktazn4B8j13hd2
XUvfQ2P32Nmbj5GdVK4qaKZ8iqeu1b0p7XLWz+LXOhlvJIzcZjQ8n0z+l56ibzc9ncqP0MdC4N1J
30U2/nrtshHo5/NGM+wmwR+jnN2sOvxWMxFzSLGYttUwJUxF27xtzNSj+g+MGcrLmjkFXmAbPVI6
sLKkMDvx4TGeSPVfOE30hsLLtYg1PYDm0OTpoVjArDs+CXd0NSRHoUY4w7romFUDO4dupnsGGO7G
Ha4b4YlqeeWV9no+ScAL/ZSnBpqp1VY6AwkEpyVzcLyct6z7y9PAYL3Ai0PjlRLDFc3RRuUGCIwA
V/xUlOwv+8tFsTmXR1gwPH6GGOqkhpX9kAm+Kpwzapq11ZNjOHOV1cmAXrvuYFT4md9PfxkGsW0Z
qpP6xy9dyl5/ANCvRRnXifK0OfsshQFMDGTYFa9nomxXNMI8kU539OCGChfn104mXZWt5HpOmyFf
KaJOIrmyWbx9UwtdFYNCfxhfOlb/Q865iFifyQqyySXdzuz1Z7o5SE3PILOEgsmeCPPUUTrNLNPm
JaX4OkfBaAD29/kO4YwpHgN1ry0S8BQH4ENWZwY8rZyYxSFSxtaY7+KlMhRGA25mDgIelB/qD2rd
+TiH4pgmVSkr3Wa/4yVJL24LuTDhGkOgsG0cXPuNJBsbxjp7LXKWaxrscmw8J2r8OV7Pp4uCBg6a
rro02e3EA2LwtG0l/Rcd2OEP0q+KTK/V2PJ9Kg+Ja+L86YdFveZxBSvMXAa01Gx7jmBIqkOeUtAL
A7oqQ/MKXkLcB65oMN3UwgTrogqsJheoEB/LQl+uCND/S+fi9dQQFVhYI6TWN/dJ48Y5wJccVLRs
Vhm5cGaSNQ2Fl+9OxAGDQ3nmwKbzWKScrm3HG1IdAnRsI5VyLTdw7feZbuXwGQndU+edzOF4uXQA
doeIWzdkHqlHcZign4oQyOnW9Ti0Zohb2yIh3JRLwNXuEyGzhMwuok7NvsxCdqGVg0y/LXMnvYD1
p5iUhrFDFTR2vvlCa6+ApEtSz9Ba4t+YUHSPLhPEi8/K0NvEYnM7iud+NZzEuNAaz+hKfYTbcr+w
CBxSFHJjkbir5AKbHktqydZJYW5cLuJAU7hccp5G/SHCD5/DmipI4JclZeXCDRFDkLgXCnAd5mPt
v3+KtlLeOKFB83EHA62SYGdHe03HWEBRxGgFFwuQ2eWxt9ZSGMhC7IsgC9QqY95pyjbUaLBcjb3P
FUmwmnmBsP1WhAsVTKOnEke6wtqg7JfrrGYQW4ac1nZlc5v004BDnvs4svmCfrK9MvybGFjSrGFH
T2IxFHVdfJ/Rl05SeMG/kKm9S1Y1zsQ1K1CGHrfMRfeXByxCOfSmAurEgVTFgPYo+ZUMSxCLZjkp
1SSD82KzUefInxU/BfvO1tN95lkZ22moDcynyfyN7xnGN6HatWEV5HoUEg7JVz3F/H1bGHnfCdqV
CWeD0e4k4aGxULfNpo1Eb1BT8UWWN6tW8BZyEMZqhO3dgl+ESfL66ulQY3VjcJNWY4n/HRd8+rfW
fgw5WDQt//ATXFju4HZl3HPg2M7/8+xSQFTMz3A0o/Hrr+OT26RQ42fIgxsmLS8551ZNvwCp1ujJ
7ZCRr+zH9Lg8ZF9Wh7fhYQrpz+jTTWKYFD9NY8jhc3ZoYu+jwjOlBYcDevUvBERp3vrkPq5dDla/
1la+OEji4juFgC5mpmgeCcn9RpSloP5agBTIAGXweHp9W2YeNZyfdvwwkKuNZ4NJ2iRXUqn3Kw+g
k4z9wHLdU5ckZaqz3bhtRLifCvK3fpLArwt/IIHsYRtnLe9zuFJ+k6rlLJxtgL0rIX78sG9ANXPU
sZ2b2t3hL6kGSJudLRGGKOU9pV7kZBbW7rUp4thgblhGoukG+lWuzN5qp29E95yNPTReaptAhXEb
TJj85O16zQXqizJHxU3OXmWio1E2a6OStZpiAU14oPMJaf/P65YOr1vQ1Uok1N/yOA98KCm/qasY
4RpfWZ/alK9/AY8xQGYRn9owgPOIo4BVGbELTLA42YX3oLgGOhU7AlmlxCtcoESAQ+DBqhnNLvwi
E/7/UFykWnTwcBqWRbmZqf7cXEsbC+9wRVryyjJmuTVkdXF6KKlnzuRLeKGXImUfsTxqIHU1QTHa
LvVJOcUzBiE03WAKDxN/tb8FXj3wkRWfuYrqRUyWalbn/jnDjece0skxvPeZ9nm4+fhFTCfq8b+v
G0UR+6+uIf9+P59HalXhxywt4IbnbjEuzQzyc23x4lfaqYZqSw2GtJmcxjOXYPHhD1+hARuoM8MR
o/VvlwZ5S5YOY8IRgEzmE/lv1Znjf2CcdTlidXX/zPxCPsfjzXpfRJTZANdvJr6shylpNYFvF2Hk
ubhS6pRmnKPiGqp9FZUuq/pCETkT1crMjJzOWOSQ9ooIC1JH6gyE14CovA3+5EKjqJQW+qcqKjjx
J1MdGJ/TwRGT+XpifrY2GllDv3wgCIYc5pKlsm7ZTCbVyz9tz1umcPa3KoSwQj5byD6iCzF4Rvqf
n+QsGcaKWPbTOuxV+PVVk0u45V0IwNDE4PFcXbLylR9WLk3gmEKy33sxUXd7JZPMT3RVYreEqLvX
yhDG4T+7NPOswY1tdUqoOjTuyFQf9CjFEItAI2YaxtdSnaGCoDgm9uipEuhU/0e9XaiyGFXb6+2m
+SDHxNFS8pN/+y3LcsPMJ84p67gR0oP/psqFvBboDC46i1dEANdBhEOMXaDfRcCz/RBMQ2bKoviu
5SwBbm+8ptc23K7sdw50PIt+JekPpfj7AbJnpdTrulIUyirSv3aUyjGhC1qNgygc12pQjW6+6rdW
IBGF8T5K7HdECDDHP63u2aRal6hZ5pLtsRTU0Yh/8vwCsD5rKDkysMHCZmwb3TLeye5abwamGD9J
eg5pbcQKHJU4gjCw+ZCFqJpCU/JsUTfRJMW8Mti6oWIdhDsWzU2wQgvuWoxNOd1HikDs3eZIR1Dv
VpJR4jBEaUFMuxKOnYmCdrVLoi4/rFwfxpdiU+h+MXoS2KjFq8d4M5+pQHANuOSZGVJob3tTJfQI
v7Woo25FQmO1eLdhZvxCljZ+OGaNCTZuXIE+Yj3clAi2Vlm5TP4ctF1YQXcZKoZANRF6tgjWRXUi
R+gWoP72Uw9r/mGTlWb1wAklZUitPFc7B7CrLXmurSmuib5RwZntG/DlZGWm4+EIqrX244ADIGhr
e7AMYaeCo70lrq2w1oiCY8fKG2my+IgfxVLpslLiGQ4i+/9HkRFNJ643m8PAIudgVv8GeSBPlh43
eoivhUz0doaTFc2jLnN4if5AGZLD04MlPZEWltLZnaMSSDE/D7ssvM0u8DodNUxT6Tm8ocuroHxb
kECeQr5vncdE9CvPO4cAlDLIPxSOxwU6no5OhaL0fCU1wXJRViBafne7bg0p0ydJxBCO0G4tWaaS
TO9Gx0awTZMabXQgLhyDEV/c0BYEy4grt3yM+Oa6pAEXcDLjVB82Nq6Naitw2iT03WXGFUfaQfQV
hfdYitX6rmZY1xMe+YOl4MQ/5rCzuqYUupLltngLU76Rj4EKENs/WtE85CCTR8jEkMznd4mGT9gv
I5mRorJtlvLyfJGcdFVuPjzvRoXTWFE8FnuDPAkXHeeWW2HJeygaQtfb9E2Agg0GKtXuhsvv4fhf
g+f8PtXnWIJobEGqnewOXf02ZRwwYS5qrXElIbS7mGcDW9893iKyGSni6a4J4GZTLHAFNRUrHRw5
FjRWvFmzgK0ZBa0ZCvIZWnGkMH8pVOQNhAI9aj2C/OmVPUWzhooxsNGmp1vudSiDd2mE4e+0Ohfd
3L8ZWmeuYtVp8AIVPp7tJjiLetxsJ81YJW8iu8cE/HRM1DnUUmTB6HvmHMsFQbrcYQxIiAicJ6fq
6YEeVf1utnqG9JkRktiIPGgiJf1B+W7f2y48Q6mqy1/8BMCEHuPmC1Z4Ed1h0bQ4kqnrIZfSpcD3
JjHYKBh2WBtni+No53A8QvYwoCZKXY+Bnl1PCNh6+aYOnp7D/uTyhA5T3etaHACN3fuPgprM4oDL
H6Yr89Rd57sGG6uUrCveIccb+XZWuIViqEFRC6HkP6wQ7g/+KF6Gp+nTtdoLd+pWkwTaXEQVJETR
GqLPHfaRkbMFIQeC+2711e2UJBVbMsxVJITx9mRMyo1IczRimswS4evg3g9IsHgHlW77DgkBY3NA
Qxoty9E25iVXbfbkYvLArxBDEdKekVXzaCMSJu32iubtsJeq07vX71xdx+kOh8E0A8h6I6HanLDz
WoIwLRB/sNZ5PZTMleTQ+TTuKHb0KzlHqqvX6vE+JMaMEKzZqtoPj3XAzsFXfK0EFE14Fkpcw8Ky
L/Eu9tir7+I8fEzsJ+iwjMsu+pvOYRTmOgrIDyTpOFdhfMzihULnWGA9/N/erO6WhyFBELlaQ7ml
bix7/UNYQOLQrcDGmN/FBaIRTLMRdlCdJmELi6znvwXEpvZ6+aZAfo6lrlc7Y/1zMoXF9bzxWOXO
EKJenzWUBZBvb4uTLWlBl+YEo63kYyAGhlnXCBY7QA8X75utXZeh1iahUpG5ZM3fbrHwBRBq39PK
RVzxg8pN7fOosX3/887GajOREbRoLuUDUq5OzglVAYRDMs2B5AMn5DoXmN6+FGgAgXrf6U6dk31m
sXa5EPFpi7uemnHUN4myRTrKFbMKZbaH8NlLsG6xvHwHqYe60NfKAVBEAsgrlkVfdnADcqpHEg7J
As2B6aCm9lHM0e7D0GzM+We8fBPSnATZk1WjrNjpArPeYnFqWIm3G9XlMOXtVopNF+X7x/Lec71x
dme+8uYXQmQ7rKYW8B84Sc2gtQ+XzzlbZdo9SzfGTAE2FJCHgl/flTAJSEGp05vc3YlGWfn28GbP
/TD7r9EaEL3K6rOzAYGIgQvok+Y+y9rGmePQdj0AkFCej4ZMcibpKgJxYPJSW9wqtKvMoC+z5cvf
o3nsgfCamwsbXN1JSB+L1H+SoSX3837mOvx+TdPDAcllG5XbImkKpcN5xbSHokAY/rapX0O3+xAo
fvGjOWRR0XFXBY6Jsnw81yGNis6fCiGtKJdVnvsNTlQJEfyG3fIajYgAphXawFnhbu60rx2+zq0J
z3nT8xysn/Eg0tgK5a4wm5KhS6AXRwAd1tKrFLA4dL9QvpE0mwUfJ6c++6mc6SbGepJN6aFp3v8z
7H5bq6AxYDCNaMB+6RTK/EFNvuR8v/FQ6X5XWPO6VQsRK89e/fIX4bB5i00o01s/pWjqbSaxmxjp
g7duFpqdtWKfajQOmQl6FOzkRbEFrAv0hOtF5GsOIZV0btbSfHfwUJ/5jLu2rYvp81UZDkcXAMnz
eg3bkETeDT0NvfO2QBcnT9gtFfiIlFYlbkN3kUC5qpW4JV1QxoItV+PbO3PAJ8N5GG6OUoQNGS3K
exHnCd1cHyZbyc+4ZBn967oIjdgNNOlLrmtOnOSm5D7RIlpAuT5lca1yYo+JQ8im9jlBd8Af0C3r
QD6UbjJahgGSW3c2SmFk/Og+cl/ptaKAeyNB1BYSQ/QAw9QKRo6/KQY+WIEYNW6Jsvr23rQLMUUP
A8cbM3LelXHFe4+47WrDkIO7RslsAxdPTuZ3IoECP8X6KYzY6lVx5hiOdV8kbLHYrPWJTGd0FVSf
5vWbpqy0h/jCU9AltMQbYJ84xhxUk/AlXQgbZyc2aGHixn/6s1xmvlAInT7gX7z91IeKTGHNal1p
aKE2BHIPIrRPqfwC/BXVe9Qz0lEr5V5ATeKGHzrAvzjgjurF8rYzPJEUMRQkhDdZhRGMCIKzSqJb
+GoDVhkaer8DHigLuQoF2i+Q99Vq+5lO3d1GeB14LLwJxWE8ULmnPCzNGY7r+D3XqW1Rh2T2hTpQ
ejBfsb2nBuIRLTEzDWYhZLz8YdkV+KaEleyWxDFPKfPG0vdMXd5he3shDLJ44q2P3Niu8SKSZApQ
kPRxkCS87CTQNmAzoWfgO58LEWb8CodDwspGbL2+T/1z++QPQOsRHzEWVdHjhecz9kGmz8sbPvVh
zCVCIkKb8r+m3349dmJrkio6ZoAPbEtKR9UzjCIbUFAzCtPEVWhSl4K5GztXY5o4LvkbKcQpEtoL
ZeGqFX6cqw7dnLIvErth+bUiQ8BZ0e6oZ8IaYFZt8Gt2MeCQJPdnxUtd8dQch65uww7t4FhfUv2O
XyeazjIiFc1TyriTf9UOGsjbAc5uMLD7oLoHMKisucmKmybRyYGSd++F58/itmwAtT7A6Vdsrtie
c/Mx5fa5khWSjzQgLBCvoeKA7faS9LWh9/oOUEcbeBYuZ+LzWiFCuxLE8p+6nYQmqkkUfKqotezW
JyLZWIprDNgkfXkkygzVY9J+3duEuM+GfWluYbUmI9uWQMvTUGjZNuZ6IfFznjcpgQQcy45y4Sdo
Tv03fq1lBGmN2yFqCmqbisgcrlnn3tK4n2bjyZjRADorfT7LKyDrLSR9ZpiLsen278SXYnhiL3Xo
HwSWJfUGruoSuyYsKNA8rJZJIy37rot4GP6iDEJ8Jo8v8wxmo6hPkNPDw1WNcnXjoAQRtD7XHqy7
E9aq+wty6tU7bYBnPMImrEKBVLaFD7/LD9Lk5S+/o/EnPC9TEGSe7MsMRCCvcPG6Y2TTP0O97LDi
IpDd1JclRHzdXUg6HetepfJk53MNHSGmxWnwdBtK9k2pN7kJO6w8iPPF8RIPAoqraTFoiQVaPQYR
JRxVjukLsULAZDFb+8DweoDlTegYhdYjpPt8Y0myt4TriRdzg5mE5x5hmAUlD4+BmYgAfANBMhSo
hSXP8M0zPY7cqZ2WYSKcq1vH/RbQdsPsBLq0ARQZoTBKVoC7OR+PP0X8vdEpkraczYJULcOzTOOV
xSqLRjMzEQui9J86zCqxVxX2eRvNCnYPYtAslp1PBfNAIt18Okz9szViEp/eyFUimzPuHqI9qPX5
lgsJIGpIxVs2tCfJ8mwwSR4UNcUS9laZKmRn8xUx0zEMrqErqXJGGLcdT2wMVUEKXRCE4GOrNms3
kuqWn6483+uopphbI/gXk6hEOpgdo3EaHFQCNbtNXr3hmga7NBYt9WD1caNY/a+bnLPj07j9iXj2
RwAwf8cXfsaYN5Bq2MI+AUX/zXQs82Orb4B2GG7BsGMQV2LdP/PvPNc6sWY1IAy8wMCwmBIwHEjM
Xgj3kl03Hb/dw7KeV5Lv9gD8uj8miEeppclUczuuyL8dOkxY0SZh6Dd+8J+aZ0QARhPz3KbazQqk
6jw5Ajmirn60m+vLJ72WDtWMR3wntWn+cOMbUXNYXQ/QFiqEXeyTvf+yw8/yv/gXdA2tO8+3BBpj
E5cTCdUBQKVWDHvnYIUQ/T3KIyk6yj8mjo2iB2aq3Cid+enNyxj7orW4cr6HDZAypRLxiGh428kq
52ayteULByUPt6S6xpN+ek1rCXsGbW7DxItS3iXdWeiPsc2wP1BJM5l75pngsTcWvRH+X5Uz+fVk
pYA73ehLJVWJEBaHcXFaYSbv4USX0rChZexbIOe4REClrhEGJIk+0oiDSjukKqA/dq+KM0Ih+4/S
HeBHHfAzS59m5+E28/UO5xczGCp5znnkA8poa5JsFafnn7/QSZB3vfgz2p0Q6P12+8fV4dt+YJ7y
bihWh2p8adV1a48yBz3cAw/QONTbJpRH5FOwUqkNzmdcpFQ60IFoilMB2HUcpbbQDGjmM7thh5qr
tmdO+R4ZjCDjwFpZXcC6ujd9cImpi81PxU4bZvWdoG26vYEuQu8xY32bYYJ219qcR5gnF0KlLR4l
TpYfvjq/7hvkAdlpiByhJq24UmX93cOulDxKst/P+9lPv9WMGY1cxOssTyvnfmbGMVeGajci0D3/
22LlLl90dsJQfiVfL3XwyZU19ULSJcCVwwSb1QXLzmIiYooRXJ6kmap9hztzXZHZBlfhWC8RyQ1X
EFCfzVZzG6mGVwwxtEeR9DnYSmO+5BWI+aET6y963pUnx887BFSyJTXUqI3eXMxhARPlcyqFWIS3
OlIE3YLt9SUARiiQdjqKBAtmthj+xj25CAcWrHHt/Ad3VSDDIzAREEM/iesHJy1zydlrLOEb6XXb
9jbKIAyp1y4uA2VRZH49OwF16Eu7JAa8p4t7Qs/47X3kkupmt4KB+UcpZy6UMoNva7wLcI4bHWz7
ga8L0UPKMSAeKouZTcj1C3s476iOLu2sZeEYWrI7z8gjn508EJnd5ILzDdnsAUvUW6Ytagz3xVc4
7lYZIsrFJezu7xiNdg88jp3NY34KVuETc6FTOPYoOEDho5nxao+oOiZewz92rTeuEShIiD4Nis86
R4M+WIcSMHISH+eXe/VTZl0rAKRCUCtbplmT41nplXBzJ7QXgAhNr2Wy1zNDYRyYenvmePvInh72
6Rfw2qtJc4mU/OPQtQZPUFRV5krlRDJxtGp/o1LVz5a+VK+7BkZNTzxFiWIclgu8EXPTtGqji9v8
EqC6oCmGvhdT97oEKCfooISKIZUv9Ru14+oZZlA0xElaAfDppt6yN6o4+4XvaesGDvtb3kTa3tb8
1lI+rtceMvI1aH+/ImMZ0AGMS4hyX7DstzfWuXqt1ss2wjIW/79khrRPeNINk7gMHuPZzwzL0NoL
09wHxHRSCoXfF9UM2uCpnN3VS8OBu44fgbW3JNml4AxpUrifPPqs3aEskT0LV8VeLswDfVRvfV1N
ccWwyV36rMyA+luYeUroVuDmQ8QUh6IEhPClfo9bMa6dymef0LjeGCYudTIFsgQ2kekZYHxTyeuo
cjYFbDe/V+I+CSUjUcqcx8oyE6Q9bnJTuXFPjGgTVBKNdzRElR96gai3zdt62pBWDP6oEvK9f3+K
vTNX3vGyVi7AZAUd7FWSwCM47EbGuXSuGuu8hcfADap6TO0pV7rh/fLs/bHVgxSCmlvUYdzTscKO
u2Xaqz8lzH+wMmn0t8Bgy/ndrQhLKdOk2+SLjUEWuUtS2D7L+fyckA0pUh1cyq3C/iOt6ReHaEY/
qDmx5ksYvUKrB5wnfLqqwfQe9+J9ZqLdnv5IuoKzYEm11E+WlvD8aSxoCJ0IvzHSFi7lqXX60LeG
PUCfn5dm9XlhmF0VM3mLkHiKI3TwAC7TsBK+3Rzup7r8I+x2QCJVncnnwvIRR3+dC2MlxT31f+ZE
eAsK2UuDgpy6zXB2h+7VF/OuMb4DN2iBvz9UXMBSmLMUaWyTZRIiSutFoW/o+AQAeHBsA4pexuEv
58cZ8r2D8ZMfy/RZ+ocqXURiu8J6zDwF9yBv3lh+5PgRnurD8MrjWt9VRFE4Lkr6w3kxrgrmaEJ6
gZcoawZxCKouJrgWaIXw4UJsDRNpFHsCkjNKDSWQS+VQsT2veht3PoVjjdtLim6JPdN6/yu1xueT
XfZP05Hq9sjbnzemVfjtwc7LPdEQi2bkyLDEFge3aJ+cwL3bVt4F4wpSDRU0qZ0HmdZc0mdvvI2w
oJr36H6PX4D6pxD/JpxbjF0vq72KO472saZPsqW8gcV86tUOcyQ7iMPwEULGT7xN8H7iDQJoMj6g
ijEUn+UTP2WhM3hdgg+S0fvgJo9UAJ5AhInHiemdoawhtK7tppu4Rg9fLwxHG+4FAhTxR0XrodVO
d7WxjmJX7cWBCXzVLIJNocLgg9dh2FaRyXORcmM3mtooETjozJicWLLy8me2a4ek78vagttmK59g
p6KsxCy5U1hLf54dxH3Xx9ikTi49UWj+v3iX2WSAAeBXeVKdQEbwr8jUyZq0GTbq8IDMpfp0jmcp
Rp5QNf6i5Nt+xJl9CrhB6TyiIWYNJvOSJCIs/5mEU/APENtEyJ2f0yXvONq6jTfuZHh5RTLsTeEk
Dz1XRZXcvA01HWLfjYCAH3PKSgsuuyGSJZgW2VKKxR/4cLc1s1ELcd1ffkD6GPIk9suX28Pxn6YF
pH3qvJeATM1X9oenZlWjS59WSZ1YQIuNWz48ZTl8vRElwgkKGYy06Y3pfAX7QGHSs/21uZCnC073
yprymUh1Tk3acYI6PYQGKELDypxyZIOig7FD7xKcVFok9Q7GUEJis2D2ZY6xo5GAJKgoi1cT8Td4
n0ZnmHX/ZzB5zl8xpYUUtrIpH8mbBraKS30DFabeP5Vure8Ii7sjzY2QuECcVnJ0tEzXcAbK1VDD
w+UqOMTBtk6iaaM8vlDTtWUW44G9jdvmy95lzjwO416O6WTrxrunhqdqyfpkM8qduO3x9d3pjU8T
ngyWSTk1M8AURglC2WyIltAAM/JL51YiXOsF5swJ1MedaOVp6D1yWuIxZR/RsmQPesYMoOi09MW3
ORq8qHv9vBHzsbv6bqJ7ojEoEnWeuHmIRiuwayf7zruO9Bw2WLnsphbKvYTnKu+/n6g05MTm4A60
C9IVPPjOLQ/kd/x+k3qqTvEhEqr8/ftaqlaMoh9Hxxz64/kLUG/lrVYywWrfhG1226aeM9betzGQ
yt8YdYbha9ye2IkzJj+3qtixq+qrQSXi+gLXOXe4eWZr54BKJdywIvJIf3USErg/C0u8paYUsieO
yFMpMfeHRhlUz3DjijKYBXlsn2BdxWMetrW/ngG0/E1IPHu26WzOmB2eJusoRF/AHTEIq4hIg0zE
KMZhWhmah/9um4MOtH/HNTHmSOjzmxOeUNvIPZJ43uc/VaHAIO3IYviYFVlo8QR4hmQ8DNYxW/Mw
gaB5e0awXOd0K0AdiPWytN1W0qFklbw+siykOZtx4rcAUiNQ7SLE3P8qXRdZEyF9jJLqndXcmR1S
Fd8lp+0exhrRCjjOjnxonjUd3ra002wTef/e5QmrcSrrgCFfXkmfsbCrBQ1O/1qIabv6ZcV4o1Pw
UFnTrvPyjzPjp58pE6VXCVuLsqpdyOI+eLQSlR4+W/cdasbbfGGA6x2MaJDNQJE0N7GhALFz6lFk
1pxz25QqT3JXfP1zXsoP5XgvguIgptlJQ9WKY3oPacuaGS5Dq1mR/SxHaI4wkSTNYfCfg4p42T5L
S0arNr2wuMQ0detIWmb/psoR1RylJ8YtLWqA+PR0z7HY1kfNIJUtNknYmWicAxHevHhnI+8EhZEk
VpLclDYjOXVsYvW+64+hf4dOh/SmI/d5f+vH7Y3TAjnCXwP5KiX6Kv054azN8qGAXOmRLuwSIQFy
ig2BIUAUHeB76zER5rDe9q9tBZRu1QjWjwAogiun7QoO1Tjtd+TlLrRn3NC8aPi2ACWFiGc1JIam
1tHM4x3UmUWRwjLlfaRQwPMma+ZL8L5OJq8JN9EyS3g5HdV4MdPK7MsHVwIIOpYxnzxmY0Vawh/9
ah4j/aKBmQimivunK5X1N4oY50YDVac4q4npA0Vh7M3cf/W8lBn94jSROUIQncV5A6E49udkylxa
g/GXZ+NKygbjoKu/QR+jJR+CjcuhxpFr1mpkHN+oc64q1cZbagdB1BSkLOzutyNQXC8Tyb2kHIhy
SJUyf5rKmwRW1hRbFt/AcDl1Cu5MWtg9GP/3gllC3A0vfiQLaD2OetYRDluWelDc34DRXRu1ieX0
qDGLkjz3KnrbQbXSPcl0SS+ljeHFRZT76uUw+C0VWWwYcmIRPkysO1+/KcwVTvyHEFJdG64E3ee1
hUU2IgIrlTxnC90mAJ/GWBHw89W47MzAeTMyYtcMAvk/ASNfBygzhfAmHVFsCqnvS5ZDTWvA9h1O
jpr8ks3YYW3BePWSWMGX1u9D4HsWPTRPXaZnavlfvCQWlmh+E6KDxARTnvyPiBPqRKbqBEoPPghL
ezu63AUa+VabB1XMmEWFOyFRY9fYNQt5xen28vdYjdcgPvHi6vZzVvf1WT5qpAQ9Q0+TJkmmd9td
7urFefzjRWgYFOWmk8xqSqrfUBBi+IVwSV/2MZyXWV0A/ryqtciWv/651/LqWfoVKR1pu3EhTlFm
fqvfMOFjCXkzdZflciiAEn8Kfms2WN8SselXeNRBCQdWf8bzE9t/TS9jqeZCYMXNuhR/Q5J8XCph
xigfdmEAgKqsXkv8k++n9Yf0LlVG+RcAKA4A8tpCbgen3fVGFyje9Fg+9CDqL5Ts06u4S1OuLFx/
93EP8cVQ9ryeqv3ey6e+mpHIkBovlPSYulf28IpKSUH4BWvoxb0OQ2DhTb8wF59zq68VRX3tJIXe
i4d4Ts1NBEnpScosXs1CvxXIat+rQrmDCcG5yBkk070GfKQ2oAL9qe4xV0f+y+L8qK5MCGfVMyl5
4uCqjdaXy9TTaXi4c79yzN/cO4yX3sPejZ1dV44sc6Mh48mLjtkmmm/Z956KEzad+hSlv5bc0esp
nHALmm0n+mOhCXTM4om67QR49ihC4BmRSxGK8ii5SQXGDi46txVlwaSptMB9D2/euUs6PE4Cclxx
+fkbK24Q88OofkXCoPp7ZTm5ui07tt5+TQd2LbGyMBHUxvkvp57qH1Gd2eLpx95wAM35SY+G0QBe
TA7E1QxkxNJLgPwYAp1Z3lXQhpRcFxwUMio2CrBf+G6P6Nnnu5JAR/GDohbz2Nsiwqm6RElzEXEf
sqI/5zA9SoxXy4sh//QKUKldpT7aZwvrriw4DQ9daul1cTsNtcs8RGfPYcy/3iLgWPihgYTWkNmH
hfqPyeAKg9jaDRVMImFfN8QBJR/DJOklNi4sDEeHffrD62VZhgHfKd33klkNUhOYy9nRVBVBbkUq
fZHr4s8O955G658E1h2Eexe3AkIEFLwRicZz4GNMoPQXxq74m1Cz/7inHwFdRa7S3xuTsGIuSLYf
VVoy4ldTebI89iPW1oujcpbThew6khNKhVOxqqT+vgvNO19Wi+hix5Zk2GIVqL3sSAX7dwgudsSH
0C3KtjRg4BBooXJamNcf7JPMeLIc3LvHfJTXtpc6WDuyo0QK2ZtQICK6frB+NRoCpiiWHy+HdSjE
aCkpx9cs4CbaYaSpRW2wi/HNrf2e2yZ3U5emQeVqLMrK15xuXwJjNGmv2oGIKZoVy8n8yUiuBKNo
3bQd9hL3VHgOkCn1gDfrc2/k1r7FYDSajwMlSaJi6ee6+OaqOsjNEYQwYWbb1JQyP8UFsDBpsyfS
Yqd9tFRvaoLvuJZMULMOa8kAZDrKGRXi8CowZcIdaFiK4KXq+XqMO6wWJq/AE8nC/h05xuR7okQA
u3U1I+d2p51US9cxXWUGIbEN4loEO4I3OJSM6dThpLlIzVHHsYjHRSBnAu1zerkB6ETBrzehswtC
/ZqbtSDpMwDoN/8Bnoopim49u0D8pP98IsQEOQkcpJW2crdmf8vOnnQmsgYAmkO2LVlN8JklkYr1
7Jk7iwVrcIcGDdCLUiOxzvLxdp6AJTIUU3Kz0aNbWpj4YuHNfsYXOLR8NDO4AcVOev3HiiEQ8UHC
VPMEexsZUOavsep3Mdf6riwuuyeeTQZnF2NOdEao/mS1+WTD732oE9M+7zyyyfaiEl7ZUg448JOw
8x/duewKrVj4d4s4Q675iLdkfzNn41wPrPQauc1jTZq6YC8pgdKfWhdQAEcviYErYJUODPcf+EmQ
TmCqjjzBnYFywVY3iIiGCRPXO8/3edUaDa5VOSOz6rj/+dPuYrcftAN9pBsqoSxO938UbSpzrKQ+
+aekZ9xKp0RkfJafPKNahQuC9TAV5RadayJ31EKvOWtOYu9gmn8BWGHezGNB/QFMwaceJmgbVzbl
gFhqEzkqzlv94VM5Wk20tCWv1w0Uu29NV6jifhfMDnx2uH8HoFAfif5D/PlE82mmHifBMnWD9JvZ
fcKDwCaqKMzP1J0XmxFX51IwI0UiKiZxRhSybYNjJsQcwEWe2Dkxe7hTSAPnB0CfKetHiwds3LzS
bwlIJyYLkb63kLxXw2Urxxr7/qQe1yC7VHrgtKWs4dxBdIzbSjtT1JueQN8uPn8b2gkn4wElvQrn
4QlN0DoUmWceEF9193nYR38rhXx7ZY8MTewUnfMNmBckXk4G2gKzBa94zHQuaG0yClzMJ42D8k5g
Trb728ednYCnvtfeVUaQkmGSwQstmSOth8E07fpV/oZX2fOU9Dw3DjMWVtS7h6UPKX89+xYjbH8C
dsuh8bItIY7sMwA6/ycD45Gcj09iP8lH1uTzHrr+7+tDRPHNrYWUHa2h3d41gypiqi66kNCynPHe
pMoF5YcMJOGq3T61vR4PHAO5XWRyv+SxTreQlf6iKIo+oB7b74+uhxPlE5hb8KURs/BSA5rx1jyw
NhEvEs6rV5hRRCG4/ATezkk5OpCBaXS4T+f7+qJm0fJiH2E+N1g4Wq9nTDr3du1zcy1lblJkStmJ
fv8YLZ6S6ZRAl7ikwTnpk151pTRo1t5COSjjBQgVk4CMinK8vtuomGC2QuHoQrJUmggQ4WVEo0Iz
gpjd2LrWFUcqeT0s37VdjWP+FEGheAffe25fMrade1jMbscYc8SWaaLZZQPuAqnbbaiNtjHxc5N9
Ivr2EioP1ANAHyX8giKiJsio12E1jeevSMZ4kVsTPcxTGT8083SDl0nXPSEVkdjDP3cplhcFI9QC
0Ht9VS7bBnpeK262oc0AOQ3C/H3fJnJ+77cquUTthuJPjh9UJAxN1Wo+lGb787D6Bj0otXEoneCQ
ZnIGzAZzp8xVUG9PA33GV4DNs7uWPXMAtxJZP5DA9/B7PxvFS5+Sw8C6y1mJCnFh5TYTNgG0Grhg
iYbkGg5yycQmZ11ByfZetyB7RUTxeuBLcfd+wyabggzlDhGKuR+PCjLTtjr/fJ1Q7xQtBMY8g80S
+O0WRzRsCCUbFwQJHBIAXGVuTzARQ3VSD5izVs4T717lhkFoUAWlvJ19X+Mc2O1hzDMBrNldxhxP
YZlWXOErM2LYiftHGtlKojp7rbQuSWwko/1tcSwXOpEF2+ed0Ndw8O0MuNQ5OJTuV4uZioNJlxZI
es/81HGZDWsncSbtcQlFUYetEBWpetblKXRiihvowjP8MTwWKkrXsOR06rsejb44WTIaYBIT7yPG
apmgp8KE9DVWGldgwjspj9FQvZ4+akOIa5Y4mDxK3YrW5PrRNZ4r10COvqIeLiocH+SY+4Trx0lM
FOTgkkcAtIPGPYZS7n87GbPfsOVt2ka8bmKENq4V7yVBvgJX9GoN6+ZKUjWoYMGEgHkYl9tUMG+o
y0o44bbVsFbldTGlP7riyLn2q70YSa3+T7nEJyAVe9sbfFFOiCMYxe80v6ziQLlgwHJRG5EwclkT
aeAM5wd7HgNtCwwZgmDrJikOv+bA4S6QpwL89d6c4hAIwFu/VYYNqkB296xFZ9YyRHatlsZwYu2d
Nhm/fd4zhp/fcMHpivx3Vum4qJWTMUpgsrfPLybMEX7GWu7FGuC7l3HvMLujx35RQGQDPH5F2srF
f8DOcfzbQusWJSDmRBgi3dzheI5a17X40TBJ6itWZjdsWr/9xfQzY1l3z3pXw71WEb90KyN0Mzpt
oNmP/XJkbrSKmJHyb4LfbRM4ONEp0bl36lrLHwXKbtl0suKgI7C0Ee6lm6RYrPogd9ONWmuJhnyG
O0KEzUBkT/pcBbnDOeBsuDYngeTsqbuYz5EHNvtWB1vfwgkFlQVu/440CL3amRJXcW7qFgkJdtsc
hWIVs+mVC8tkHuy9kEk0Y8YGUCNlgeFu57lQzPKMq41uNm/X1BCIIdAvXtSFVZLZY4pY6ST9u02u
nFtCAF1/dXNNUyIG+a3gFWUC3zoQHk2WyHqSF/7tOwVIZet6IwypjX671wvkflRUUSK7zI0bjGvy
YayDBqn6alhEXGdUWYTuX11V9Mo8cbsU9wRB13x9apdpDpBQj7K4fy27vTj+ROxK0nmHm4OSLT8x
5SObdtmv7dTAog30OZ2pd8+I293TzsJjo0d/j7Pe/4ykTSBxm3D1GFFPiDurFh7LcA3PZLOvfduA
P3HDpSQebqRTyhQCcZIRLb9dd+ztWyw1+l13Eme3nMuZjSfl+YkZhe4EHNBt87WOaIwbHxtzoes6
oaKG64xA85s9pkSJWGiWshNoim5k8wt5nk0b+jijSfQauPXFEitJtw+UtRw1ERcHYJoY3ZUOGPYg
LhZsiYLSi7O9kQ/9uvtB5Cy2RL+W/f2WHZ1qUADvLNnha1Z54ZhSrYRNDvCJJ1+9IVY2TQRdcWhc
gSH1KWP4rtOvzj8Uwj3GBWFFuej0TMWS9IIPUczvhlwqjmVbzh2fGYPer9HoHY1ehecQBZ+QtpzU
2ifUGPpHA1q3NRAknXWMIkqx99gb1C5iFWQ9DK4bB6cQAmveUl6QSaTGNI1E4gKcp3EFCzOgLuKk
M/qcaNHbiZylKIxx2uo4O5ct2ZXNGMKpQFfccr7BcNX/4p2D+EciGWiVXHqKLCAOU/9BpoepNnDn
0fiQdefFSL/Z1g68Wa04XsYDLBe7S8tj6rjeR2Z3NvN+F8b8cfPqpOXbFUxuN0NNDAJBlh8iax3/
O83tv23xvxZ7+FfHZr5BF8KKrxShGZjXwNCeiDke+IST6goJJp2RQlqPENDWBmmQu/f+0SY/NVg1
/8QaH1g7HD9McDilOibeEVvsVJ2KOh95hBKQhWcx89TkMVK2h2xdYhxKPxWj5LgnhvZqpkevcYgZ
wepObq7vxrijJyt3S45sUb1gF9wtzvE33ZXbtPSihM+FwoY6VLnff0p5uOq2OtWkCfyL5I8XhjGZ
z19ydcBQ4rG5LEIHPe/ustklRwiVaVJ23roMaRjCrn2yeBNPYk4bLS9Jg0rX84tFgf3SMrvBjn48
Dfo6TEmgyado9sE7zc9O8i6NIN0QHBXY3/PNrQHOXdwK/dknaDzlOk4RZHSUxLCjT9v1m6F2Y/WL
uf4FD6e1ELTfaz5j/ZNbao+voEfpZAGiEvqzPHYaFj6J2LWfBFr7NtWJD3RjaPLszN0FpHW8VVCn
RnBe68zH6tkXN00I+rsyv52huFXU+e0QD0J4vqUYLxswhJokOpz4x2ADtMZq+A++sWE+hp101OxQ
K4hyCiVuidOHYH0Rfjr9raYj/tDANcjt5QQiZ5yr9rDrIU/GSRDyKaSjByV30bObgRg5Y+KNCyIX
4Zx7O3ahR77zBlEnItV9ZAdk+KMj7NfFTvVVkrdhASH6JJ9M98e1VL2CgMLAI+H/i3tFTkhAy6ir
E448UGXS0TZ+gqrf+3FZ7ZY/QBLbpi7gtkMXzx3OP5tChEW8eDdSxD1JRfW4+V9HGzdWXDroCuJj
sToBIy5F7/7Ewa8RSBZ1fFRUtjx0WhlqK8o8cEl4yuAv+LpqLY2MC8zlT7A9DFNAIiesJ3iYgZsm
tmJW+rqzBZfmc9yZvQ5n5R27FmYvUuzcRSV7pa0QXzUqEZAgCMvhtM4XAdldozlKxYDRGpVR2q2A
xj3ZIGuLPrtoNISPaGItImKxqQffCSRqTUfr/VukVvKj6NvmRuOqiB69MNmRF+b+GRHSy4mA3jiQ
VqnxmqhGoM3YwhFT1BGbcV3eWdl1+ZSuvkI7xeNQG/DlILoJzEZqs319VgToFpU+Vd44lTt4U+oQ
YBt54RkxR1oUzLQ4cJQy+8OkFNsfJFhMnZxRWLikbicgS/05Rjg+u0NOT3YLxi64EQ2qiz1By2ko
KDRlyF6cfYxHhhocXoiFEwG+/dCVdqZm6fvMbfK7ODKvROmOEFXYkz0ku738EzOJHTmz4cRoQd7j
yFGZ+r81hdz+HSwT1MHHWFkmUF+6cdFmT0xL+7DoirXvbun2A50ovVcvIUco2dYY6TQL2pSgoEjq
SpXEIN/wxoa56qyq2m73ot1KZ4qzDKSrzSKOQ6oICF2Q4Sg45mF86yyRPiYT4ve1gR32BH/h6+ai
VlBFjPnH3lDWJ6YQWD7bvO4CGDxwcDKRY9WI+jPiufxTioy8cf6yKOQI1MbwVPQobv6FU7IM7nkL
rrJ9qQtC6OLIxyfJBEiZg33AtggecQ+Dn//3lCo0LXAA8xzn9n7wvQnOCHAdQf9Tc4vekZVbqBE0
Q3RF70V0G7nn49r+VDV7KGDZbK/8PACkwS1TqryDo62JMeCi5f91ZF7Zpy0XCNh+VUQ2ZkDMeLJz
hmgGPWA3urOdBtm1TA8oDO2R/ffwbxow92Iw5iX6Hb4f9quu5a/RYqFTslfDkhmolUdmAg5fRtpm
iMjdUS7J/JxGW8Tsj7B7RLDCf1mwMXs6rI8Y3BAvTxEwmrGYNwTsVNLVWDmeo7wmtFdrzqNbgZcg
tgDaUqDKdDq7rmcdnvUiS7jt1tUe8NaHI+tyweqKq19xz7MHab9S5eaXDmlw6m6hJeOI1z7ZMjqj
H/KU2a+2jYr08jR8MyU+mSnB7UxspzPxZoh0eo2YWBMG04UagWIvQmuuNfwkqbA0B/iQvIiSvmXO
VeTIN9EzGYv4n1qhq+9id/bH44BgsVK7iWRX8sw0m62aKAznxiUCqn50S0dUCGkAUFzJcag+4BvC
89ix9wom65+DExbuuarXuJEM+5U/bqX/VKHvdSS4NHjNS6U2GECT5k00m7rsJpQNcVSkozP47y6E
HXd14yIAVLa4XgHOYOyGL1cl2YBfL76mmReusRYQbW817SqLZT/EuIM9eFk0xGCzu7ofWF3syYKy
Y0SuzLfcwojSmaHuWi/fw9lQt2YBJwY3rY/79Pe7GIi5QTU4OOyH+mPqplyEh4a23s+Af/uLXFlN
kpEEzAwcM/pkdc3MvVUeF9le8erVdYOyGYPkY2DzRDawGv0oZ8PZjUVg06ug3efcC58UJnH9+l5h
C3yqM5/sCaNo/0vbQ9Sq5/QLj5ze89FZby5lf0CTfeBCqIQZrKoW4RZYXmtKjZlDTmaewICrHbYU
WirYinCHNl2oVlugscho+KBuvuc4pFniN7gQADp0W/lEe41GbTcHBlas2E0LXi1F3aDtIx/6hCYT
85vlQK89ztPnbOGxkqGYVpZulKgOTw8mGpiTXXPOgnh0M5u9F2tjt1dUvFkUupC5R8RRggTRh3zE
V0nQw/MSaMfQdVsfcQYQd6XpIeSZV/qI+f4khY46TclEBNeYqEKM8RWi/79hTW60liHnMHqqCLpR
KvCB46YmTyFRuEi+dYC3lBouCoN+NKxKJ+2WjLi/0vSK2YbBwLiLoAzfLbVRDdkm11VQgsZnlh2c
TbR5D5Ob5crbIhcNYaHOtZyr6662AU5OGtMb/DD76CexFyeRuyuBHR8lJb/+cn/P2zm0H0EBurp2
rb1kHTzAJAZXHr8KOCnbRT5VAIF3ullOp7JEmoVfucW6yzSsj70JClwMqMbTFDEmzlKdRhtwJwPg
V9XEP2poA9Tq6MuKbOds1gesB4Vw+FiJp91zFXBKHiVVUJxY6wtmp7VQinPWWkmQWuC7ur29qAa8
AEyOHl4s2n/3eueVCsmphXCgGEEfD1HjpeGAIR6SmmHh7E5b75egENaKkFD5zB06iMX/pNsVqH5F
lgGVps1aixRLwKs/oGMD8F3V1Egg+IgrvYQzu0QfYvNSXnPdE94T/1YNOo/L3nfmjNs7c3RG0VmB
zgzx1Vi5ABrFDRGKcQmEvHR1lfA7Nw/pw8aKljNvPVeISmUVSakSPrtM2Flp7NMCt9SfNOcXcr5l
CPEkFDqdA2TATnZgyyr+1nido2hQZm5NVQUEqTukU6C0IVmpS+VUOfiqZDhpFi3urFEAocBbAMQG
gkzqbTew7k0Si3ty1cJbnjv3OK9hertOV3P2lw7a2O1hAtA4QRdTpJnmRKkW2zqEZFwg6sHK//pp
GteJhLK2ei6c7oS/Y8gw8UhJJDZNxOf8/IXx+Rj3+5ufNNPr41M9p8f3AdBVhLLQyE0aXO0zi/gt
F7IiXz0/Ko+7zZR2FlHY539rip1kU+i5LcjhBTKIfLChbTNuABVolFU9d7zCs+yd7wZpS8DKCU6q
R1/+BLci4w9ej6ZMl5QK9rPCvwEbF2qgmya3wb5EU/hXn2LParf5Ezjq9nTcuXEC+BBWYxXFtEtY
d9CBJHqO1lZE0eiU8FvHjKEHl2JR43pgfzjlpOMfubhbhiuXTZlQ366LT0+K2bzSv3LulrdPSx+x
jlG7anP7z40eWhzpDrXci46jFWjjly7CbhisgBxPOzC+HKvarRLcdbJu144yh0pU/9g+GUmkZKYL
Ra3AJ2Mq3G2G3N90+KWb0G0JUHViIcd6qBbYkEqLqaMMOLQ9/nTlsSmVjhXug9fY7LlzjsQBrz36
ZGyYusZMMghVnbT6C5gCv8xVJjPtdgDib2bWjoONK18m5mo0Kc85Pu5ZEpaf2C16xg6N+7UTPSoB
DQU+V/tv+01/oByizhMUNl97IjGUESD1HfHI1JCC2Qq/fyRUTwwG/jAZf15sCneDW9eW8UaAb/bL
5G6V2kYGoTfmTVRQxtCxsuVwawOsm8nYbxaZeb0QiYUg6FvmjXJzjuewWZS7NAosSIAeU4EW0M6x
RG5CsvqAP7GzFBT24JokAMOWi17subKIm1bFCtPLQbE5aVaOJza/0+22Mxwza7M96DPjEkJO/Mdt
9K/fNNN9nC9KwmD8t2lTzKZuG1N53e8J1wNk7g2BmVyXHWbxoKGDyRJcFMyYXCKTGiPkQtcxYmPX
s/JcIuV/mkiv7aNESsZcDtkXrBgynNTfTVsDToIecH+U0Y2AkiyIl7xHcpR8IcKKJEOeGDFr5cLY
38PHbeVGMvUgmNOUWIbhmT0qqnYJju41dYOhL7UtShr9iW2+5GYqt/cVJhCw6eiWZZ8+u91T0+af
a0L4xhemmkLLHFO/+bb6UgnFG+L4I91nFcE/iVaGu3KMxEsVfm/o8wUgPkZWcw7QcD+Z0O1gz+K1
x1PZ+ImAL65fQkajL8wl4kuV+j9vBYZbXAVk224D3xgsIcRsX/yHCGdkOrEb/xCaYlFF7ykVm8HN
nFiEcID9VNGLcGh2BIO/YjZSwNrttDMTW0P2lDDdKmigvS5st4f/UIgphrPkgg3YiAv2fL/yEok7
Q4BAssA2D95JIJU0YtKrdYDAkNNWvYNZ+Js74RpkKbTcsY3IGA8ii4bb/ix3azQOepKtYyKIknEu
ngA0ih46VCGFCNtBMb1cdT06eLl1KYa11jDaR1AU+1z3/ocyVX6EJKAkn5vqnK7DXdj0uNhv1egZ
0CFxZCHbh3wotn6AJ0UCsaV7AmPpFHI626QZVu56TlhhcbGJ9CLFr1HxPUe9sX1rTwKEKh4gK2aM
O2cNTkW+EQqN7TF4LAgydpOzDPtzvdkqFf5iP6HLGZIDK9D899uIKsIPruu2238x6KmmWN6AIXtM
GEBvJ9fCTfJThbMd1QsrIMYJmCdNsugWd3rNwOC83/1UNeY0hBYlEr4J0y2SKyDAUkYbmFN1d6KA
EpvwTJN3EJzE3jH4QGPsFrq7jrbVLPJ9tjX+o6VLa5bJ0hCceBKKAqcZ5dZWMJ/CUgG6RJxd9xg4
ai4YQVpH7/M/9Cbhekpw0ZKnSo4du8/48NCL1JYzMugC9HvV6JZTRg/4nbx8Fe6iJJZV8b6QlDld
NHG07x/AnhjUUBNefM+K2TollV0JuqrZS9SiaCAK5V6xXunfl4te7NTOmjgogsltfvq7JBqiaJUu
lLGTOFFZ03JBoeSieuDTSl+HwmwSmuMdCkfl5rH7WNKAmwLJeugWeieatJg0c3eVdbbU9gERxat9
H/Eah/Zx+csK9CLspqefrCjvJXa4b9IVxdLquC1qkgOUmN2ChnAjpCH7wLwvwiV9LMkG3N0Hhu8x
5v/Ey0ZVemy660uJxGeNFEk5YQiDs4umHb3o5lbJJTTEJZeskRXnU2MiGGKhbcRw6izDp4QdFe8o
f/8nrizqytEAcxARJE+1rytZ5j8bFXCHlYw4cg4ZmjACV0zq3OL/tYr4yiIu3Fu9i7+9zk8tEsqX
jl+tk6i4No2ENDgxdEKHbqM9ecnY8gH9mMt7njgEHF02oRMoBTH2tptzWSbKyruXh2Rm+dcTyCGp
QBDO9Ia4GWHOYe0puf1QQe3AKbuhDZ/XbBq1tKCne+pWhmkqC990xTWd/CVthB34qLyQrC9rvGTK
ZVNh6MxJ67ZDjxWhUVWA3l5sDRFD2h3QiIDX0d6uQDPZ8P9g2Tnm+GxaTnUifNU2Jhsg5TI7GB75
tyTLioV/fqq9J6Qu9AoiCrmvnMx7O8TSI/8rdEJk9awRDcyhjY521gcNjr1zGZu9gNR3HW8Y04Vu
X0ieIxTFsnh7rPcKvWvzyalYANy/7c5xwoYBX0nzUFkflYWpoGwGgJX8P+QWA/gCGDL/v9cohAys
pjjwrBYthojiIawzHwS2k3TkOZ7/W/mnXOQNLJNBPpi8JuGJQF7HAv2Dh+vcppkc8hYd/QW8RL+U
o7upOCkQ0KTEz3995/N4UDAiG13czvMlV1qatTsNLPQBnGX5sC+L7Xypl8yHaGMKGh/lCUpZq5YX
e9HAmbdPDlqYIa2Bt2hmIO8qj4sWmKCRdzCfLUuH9lZN/VPgMch3w1+TDjNGfFOXpQAofkzUGhd7
OaZut9u5RMo373ShiE21byO6OeEUQYXMCk7yNpskw2wqObXKHqpWm/2N++paAL0C/DHzG8R3Ot7f
6e1IBDL75asGjdqlsFrLsRdsiEnhe438+nJ7hP0y3OsjASybT+MoKOoj2vOOV3+6TedQVAvx915U
ss+D2ASCUrxjThAMrBS5SIJ9NfT/GEXyJDUKPLvwid4h1xwS54+i927TWZSbbNtWmYQr69G4Sn1d
01D+Qv7UCT/S4IzAud16XD1IKX0gXmWQ91D409ojlWe4JG8tb76jZMpL87teVPyuvmD1cBkOzSdb
E9nJx9XP5S4Feyj7Q7+s1ttrgUClnUyS81HZI3GzR474GbYCAcXHcwTG4/g1N25BgRNv3X300HkX
VuM/kiGsUD8GqUpH34OyGscqRuTBiwXHPDmCugVooBkN5v4OVMniDUjsyIa5p11lUjfjcrysYduA
2uFatV213aAV7doEbqpySIRyX0YgN+9YLVmdNDuEcxk0jViBd2iGuqi4EW+3wNL0uU3CarJRqWqM
c5q+77cRey/PTovPUT3kkyFva3XnbVFUZypqhNv061CASD1ZtsaiLEdU+phVWNMt90+PTAJqOnVW
1U2QxyZSzPugb1kO1POmSCD9UPJ8dGqN5lp1+fzLbB7z+WkZ6r5CyXsbL9nvk4pkEBEo/Kt7MUkG
uTSqrQckYN9mfusKuaifGv2bk+ITRvfiTFKrx2f0uZxBiP+/Hj2Z3KOYsmeOMf1eIIUF44qvL+ub
e6Iwnn3zfd62xI7//+UGwpytmt/Xec7kKO+bofsdVj9CM5a2BxZ4fHwB641LBuevcNGJqz5Jz4AE
wzShT2xRr0WNih1+DbaXGShvIw5Wu0hcNQloWwZdfbzOoAqSfj9x247/D8G1N9dtlFGsbmypfDq/
aEV9eb19+PgBtLMVGk5HgFmwIET+a5iFe5bLGsbiiVid8hHn4touGsyavc/TFouKTFtFvnn16Z+a
T3YCXAi2YX2Enjb8l3/Dms+Cw9QMrD2yD48/PUOeqR3RnCZhekyMWZPQm3VwRUDMV/eEiOWRFQvp
lnWiTMrlkVZEEdiP0vGHwHgec7kD75NcSFogUxxoZLx8x/WftnjyiA9TQbBkucFFf/IR1dRiwWA2
6ogMtpzN7J9PzisfRl17GReQCltzPZAUMImtqUteXKYwcjzCvBYmWYrVAM/0kBCi31Zhd+u82UB4
mzZpocrJDkliEVsrFRHAgFL+ASu2dLWHkZdRjSwt/bYnz3eUwaTIimk+YKxeb0JV0qxmjtB1fD6t
ijQDz8zYdblDmwZOzMPjpFYzi7kRkT1V0EEXtXNkxVMgYNdRj3h1LL2tNmIqNMp5OD7K3Y4p2XRh
F4ZCvnXcUTU89whaxj3P3rGwuc+n6ECuZXfm5nEUAKkp4Z16PdQasHwYaJgItJuvSPonT6UT8CBg
C2Lghaawrs5U9QBQXuEPhC4zxYE+doyCn19Ed6l7QfGWk7t0k1GX+dtkXrX7Vy67UdzybrKF1oAA
XIa5MpuiXWJ5F3hPsiga4GGrGJkjs6cale879C7a2s4r7isMShs1x5B/ozaKOHuNaoanvQzDt3p4
gYvitlsSmOl1rKeMv7JEqo4uJwfSTgQRgJlNg0BPVMpgL/pPptjhVMI+aw0vp/Mse0juoigtC23Y
Bdtkw1aCXWGPNy1U6FwxTnPGOyU+r+j+Mp4LsCiKYAlPiF9qexoGByzatx9RKnNxMLCdzQWetMYu
F80C25odGbCkRI3FEXLdDsq5PJ+wt80Qs5Ero4ssnH+KwZQ+49vTu50oVG9W03t1cOPyMX2o3x6P
pZeKKnIsJF7wytG7I41OLzwjQCiNnz6wcww6MS2B2/+8ts+vhEODVfcqMa7aD8l25PD7Lq/UMa8S
UWvdeDP40ZkJ35NUl8HEcE6o1qm/V2kTwVMaPC8Iq9mTrwIZb2Zeq1oPZA4PUNZroJH/Lb2PMMvL
I/PhJX6oZ1iOjfbLbzPkVgwxEh+y++9ud2uY6oLIgfQZZocH8Wdi63ZtIhDqttUYhIQTRjmrUL8k
BVu/pZa+PDLR/K6j2L/dbcS4li+DIE242u4QMNEusA/BPk6cYtUiGhzP5H+zKXGkbq4AbtmZKNSL
3J5Yyjv80z5NvIZm+lofui+AXMgJTEwQoE6KDrVgbyydglk65VzvA9yZI8XCBnKwCQfbkoN3ffTb
1AKLpiyk0GyeSafHaG6ueoFcRUHRnY6RAjjMexCYJStV2RwVbTOgIdF9BPCkFrIYCUJvXZITpFHl
y06ZQgnJBk+0pQ+Yu24K9aPV6r6cF0okkFpVV1DK+6OSfJwk45SS4p/eOqQIQYu22q1gxuMQH+hI
Xr4+zMvV7/FV1nw3k2ihA2RN6Oe5jnpI11QXWieqXJagNTREs4oVVLpaHTIGi+udQMKcE0h4a65f
PdG3pnVW4vyL10sOqZfumZXp5dGpjgaV766O6qRQVFWzHyWXFffZEIaVOD+TG/27syusNmMHx/RJ
TNrKidurwcvU2HL97qmr89lbzaj9jHW4tjNC/qmRSO2mt3QB9S4s6BN/xrTxeeI2MdD+6b5hJKk0
YkGa0AmkwIMtuPLE29mv/B9Hj2v+65t3BXb6hCezYwiWlOQNEXZ4lEmr8fDX3FeEoeGErEgjtVpK
KZSdIcsBO0J0NJ2xGHJJwQJJ3S43eCqy0cOm7DxmUJDU54D4je//q8jKyjCIXG883oD36dzwj70f
qxezlb95nuh+aiHL8QHWHO0mBS29QDc2gg4Kcpu1XtgjDo3ZmClKWDUTLEm/CKs29I6tEfNNJVka
/ieTNG+q9tkIBlxW1P/tLFuCBJ/UwGqfWvdbQCHVKtLSCgULtTE0z//Ue88ox7CEZ0LTR9zQdxZp
g36nhqASee2TWuH8oFTcPVfaFHOF/7lfRPJlVkDbq+HL/gXEQLrkBmCX6hs6q7iscPk4A9NH5f25
sxbVa6izkeQ9Lyq+IuHg2pb4jCSb3nV3S5M6lt3i3eModpCMpoUcI4nEV+hFMSXyiVMkR6gxjHH7
52WLoTvJPue/GZovJRfOX/bTmAMiZ/8hzI+2ld/9TkAZp9tgo2N2WfZhiMub268XBkttdymQrBke
hOPGrJLiGFFbZw106jLQ5v8GeRIL//sPREBfZSxzQm9hFjo5Bl5IxoNYGYmjUk4JmfGXpsk9A2H5
gVj/6GqTFSZn1wfWrWZD3H6U6GZRJU7IMoxd8T0nTKCI0H2WqJNj2wcQKXfQzR6c1hMrL/xRNk0H
5K7n6wNOw00BgD6br9/Em38kBvRTBzXtxfp5TT6LOiyKmvdDRjdiy//3nBCKo1hsav0tNTSpCZpJ
1qMI8F3j+36m9KzJaBxxIc0Bl3U8ZnNkYTrlenzUq7+D67XszORjxZU2POVbPI4aCsNaqxXW50hr
H6/VS8AX2Ck52ZWoP3RpCxbwMvs6i040VR0zWZ+qniyHgkKNc2wB3F/tNQHvQlKTgT130NS3h7Kb
YhurneGIRknXdMsiUmzFCrLuD3KcqN6Tbz/R3fB7jucRfkMh7+5GUd/JO6qiPTCQrdaMJfmL2jnm
FU/nxVCp0RVijndSC0HimmDf8xa4AwD7sg34zcE/RZ1kzgBLPnc2SZkYHtmXEHjZkt7tXsYBUruM
KuDNq/BaD4effyaOMN04T/JWGBXe7+uUBdivDNvqFyfQ+PVrHIFrbhuiWHuuz9W/TvblxtEiaPkU
EVWrTS61aiZPbuKzauAVNysJWVCjjzjENHWZRzQJmvTyfQH3J56Xojd48hV4nv2kMB25SD2Y5H6u
p9o0Z7dCLhzwvA7qBzGBcWApAAhjbjI/2FlVDxa/tW2m4wi3bszEbybGW6VzUR+9M4eEjUcCRb+4
3B4T6r5mesXrj6lKnNK50hVAGCY7/rpSOXSAXW97u9Ey62lt/Y7DP8OfgZFKLMQmj3TmT18maztW
H8XWU2ONQpbBex5d2xLUn3gaeRhF7DmqdfJsM29lEUS6gs4sT949otYtdedMr5BW8YSzB86NCero
L3YVLwHC7i9OnDbKSgnaEVBRzDNWsmb1WPspiXUgpjcOcN6+AvUBEHTiQT/taRtw7FQvU4c5z7Qm
+uRJ/n5FI2WIKmoCwQSA/kNvlTHFlyLV+Vlad+Q0V350HSd/CPX75Sda5OI1RJvXzIRraiN4RqHS
NiQP+pFIPQYZ5tKh6ljvRqnW+VgeD3WX2HzhcKAJOBtjNp11WUIuGxegS04RGbzQxyQJr40EcgKm
Zk+bWVVBcGgb1QbV+KS/1yRCpJ0yn7+rs18TqacZURf0fN6W+0oXKhrwQ9SwTXsb+/FCv13+5VQc
rCRC62IGh4nB+K9Oxog5q5wgUZbm70XTMU5hgLb+8ACz7XcnpF2MBqTA5Z/XGVtn29wYUEGcwXPW
6MJCrwrBthiLlJS+7II91ylStoynSe6m9pzIf6K+VhmrwMG0mJEc6Xl3SnX/UgJ9a/s5pVqGPLqf
Mo4aGkTpb1J09MzHFGDL2Sh5PJ0W9nCktuUvV7PRIMzIWi+cKDPt4w0e+FS6qsrGp8FvhDkpnV71
4u0k6ut9gLXUO+gm9e+MQW814jF76nf3NHs8iYo0wUpNrKF03W78N/lxtjNh0xafGGYsFCwgnNAS
usUXdb2Orj2iqjbmB0+XfIIDGePoNYT653WxtCQtUR0SGp6TiMEVFFpCBUUntZciO0PiTroB8QpZ
99WBzqNIxDlo/PcyGmlW6TqMxDiK0Q01JCng8K/2Dea5FBlPjNzzbluqAkAkRCI/VZOHEh3PkPFY
3aNywRrn8pQ5oe4sFeE8nc1EZ3939j94Qy21+ruR7mF7TmaFLgzd1Hmw54Q25PftepIhD6vPjJzf
Vm6mTXeHhR4Y1bxhI+VWq1d//g3UjpYytlmskt37Mv3PqE+4RzZNEMJLVvxdvWnuAphsMjFZGEFY
o07p8UUMc5LbY2tgohN/M0Dpe9TbeHDUz6NkUJ0IXDqIvcW3lxPguam03QbkuIF2E23EvfC2TD7X
e+Nc4ZQ5l+R72c8ShOaBUeRtMvdu2Le1TmdWAQ67kADKxp296whxrhOj5lVSPzuG/v7tuJjF7rh6
HBTR6lyrjAo4yjRjwUk4Jo8WZ2fLyviJi6eC4FZFVWGXElEMeBphyhSBilsFf3eanDNLegoJcnwQ
fJDz9QsTxVf1fifDfmVHz/1tUGC+dqVcjHu1b32xgaJtjbgtto368X3OmziLnzTfx22JEXG4/kdQ
657kLM5S4dLqrYJS72I9cYqeHUFHQvLbjlcGot236zPSBQZkfp8Vwo/8tWky9tch5m0/Rxx/huA2
wDq76uBrHyFDEqagedT1B80gDoBfD19ImI5K7IMyYbdK2s5FVG8nBBQ5UDpSR4Corqd8yCo6AHPh
j00xNNMuJAMrPwAvx2ubhA04HsvpEcme5d7GF15cFC49UzwM33hseWQDNUEp1yeaI4Aj6ILTdwn9
H4A/SBd1nBv714AV5BmTGjL0DVlHNWGvq4jJD+bUVT1SgJnCwPoTA3uX3mPzudcz8iHaqHk/K28k
RSxXgjhhHeY1qP8FAmadIIb6u5IO0IHZMvw1z3iOUa5fOZvjMfwmsDn3d+XuG859l/M8cuzNjuI0
nkEBQZfRKXDZBOG77lYa06mn0G2K4OQK2LuiOu0oaYzjKhNcyJ5oKAJBoXaP1421cGiDMUMaQd9T
lvCzmH7UjvaYdVxkDb85HWGnHgGMKK3Gq5UjbPdXmd+9U0gkf+INr57ExZb5R2o7d2GuDAbDFKRR
JCCr152SHkZa1QD2gHFEksl7bGEUr9hkmeyiBUO2iSkya9fkJrnYRNt+GrbIWPCNEw7gm09q1KLz
hs6PVrERKxEQbB+WrFNyKxo60KVElV8RE7+14pAJgRG4nWbFWEg36chhn+askmC0dIV6Jal2tMF6
MZyYJ5oKkk7kO52pdHWHPkP9G9OcLBomYbAai3l5gUunsOmv5rdtd4uzIXNJc0t6DBU+SpyD8ndE
Euk8TVe1HA8h5ukfm7lrK6aMqCTSc0x0jPNgrVbA0i1LERchIDHsRnSE9IWARuXT5DPBSLpFRraR
ZfQQQ2nYCIgy6kcYX4CoZCx+YDiJWtESYlcXp20O6HKWDGEaHobK2qVSArf7WKUUPATDjcmFuIw6
m02ftIXNQqZIcqPmXZb/oTmFdPIHTNV4/ZgczdtQcHFJlpY2XNrx/loDgqkhJp4VQ+zL4hCAkYM2
wniuEHQMznYZvD4sk0M67PRdlLwjFI7pufEKNYeNMV2LzwQ1unBGriKZRRqIhr3QYL8xsCWqJaFM
OD8mzbm33/NwWznzW6RwfeBYn82V44tO+kL/w5n38ogfSe+HApBaPuuMAnbX/At1tX8/cAyn7NP1
Vd8V60CbrddW6Vj4LRubnou6LhvPkWFGeJZ0WdkTP/EwAzGBbRCZMdNj+lT5SEMTxeeMgQ2Y5c0j
Vprg8A7wBNxe7YupuriUoOdX/LojLTOGRjj9h+PCFNkPnN0vKY+56ZgLHAshTqSBN96vxTVUy/PN
uWOHxRB2lDfFlA8yyDqqlDwIkid0nY0Ve2XI2zmeDRGIpfQlfWr+jCzHywC1IDHjLn0DvC4jB7nG
iIp754GxwMBuYBQZZi6FBNlIPvbqH+jNV4sK1VOmsPhFT7LInZGHYgZ/T6WXIjM0uAqOQ7DUgd5c
hU1SV/8MDudMP/LXbE/9kAEXzKrESUiKmtmX8nlq13zqaTvwJZVdEWTtxNoTbsIDPdXLPCGD+TaL
Ri1sJr7qgX48eOVvUmFebvmVVjX1iy7k7XMPGPJ1sr8s1NVAfo3L1+fJjvDlYMm4sciuiLVoC5+c
321Mv3INvxGFd/aiY+cI7jTgoHhjIsaQFAnQEnfoTQfio3qmKRz9Cov53qQ9oElbQpxBM2TX5I41
bzEdgiKjj2gtORwBGH1QnDcFrhaGcYskQjhSeLXt4PsRUw4e+0sxeLKpiB5hCkj+vwL3JOJlveS2
WI3A4Gl7rD1dvEvRhBzdjlN8Pnhel3OkXvMPdVMwz7H3m58q+zsErJip6imBqIEiFdHNu1DDQvmW
KKotnQMG6vLbhM87MZHFRNxGQudYK1e0pUW9oGeiujA3XzHpntSu9rNrhXFTbcmcEUiuwObNQnVC
5FnFvwvBNDYCT5G1axXIHi7JzeQZ1843Ao/kef2QID5RPGKzGWvIvqnjlvILMz612YPAmNj96fZH
egvATSAdVuzD40XX2P4c0kiLHbuy/DG/OgrU2LkLapBMo85K6nKy+X5DQcCkfFYNigsXG04tkdeo
YxWt9cWt4rgoNzfBrJQYT7OBdsl3cMa9IaZGww5WjHBhufoeKa/PbuOF3AngfBIKK3JVNqYbuMul
OhCYMbHWKtfGZ6H1qt2tll6IoB5SGbcoPHWLbhJsmDM3yFsP7q3wlGnu9eWb0V7CRTqRCVftA9Jx
Ne9saCmPWCWLr0nDl6yD1v4W2UpEHMTFN2yNdrs49AkPiMQDjtuUuBH4nQWvX9/oQTr8dUa0hA4n
Y0+hdc/cr4uJB7X5SHbsBEcC3NDWt2+SvlElK/OhwRV6fVuLk7XG496XuIi3kWNB6j1Tg5rQzEg5
FrNcElsFYZXmB4jtoJz87viLib7KE4Ihe+YzCMCJyRkQCSK//6UrO22PEoKGy+/hr0+3MM138xNF
vbA4QolbohqLvaRt8T9NtEoxkXag4SqtKXN/Uxhs8JzRQRWvG5fGbgQmH+MciJdOMhRPsBqbCDVV
mYF4+77KldxpfBxMt8LpVsOpB2YLPO2+XT+h8U5iVx+o/kZI1by/aY1TPFVKlWHGO1A8EdzBRFJm
rDpihRFbVaFlyugi+nRkYOwhlv1QTrfUG+3Db1tg0P8RRFxcjCq6dGdZaXEPMyDfqaYM1aVzXQJI
lNgch20mewXINb2RHiXurbAd4VmlE53HgFGSb27DeRqPwv+vBEYzDH8GprLYkB62JiUVw0jnDT26
lCL0UmiRIfcL1vHym4IdKYDa6ck4WB0qkGxYYoQ3Yx06+UMplKgnJ7WLssTV1+4Ml1Ora0+vZfG3
6noUpAjWMT0D7nNAZlxm9NTEAX/Tmk3B0HWFWYR2JoTQRJlXW1bHJjiMkVffkW04GlxCM2Xvn5Db
DMol8ur/shpgFI3Ta4c7Ab+cwrTKJd4pc9NMGX9i3MIPnf3OzHLQuW+2EhXEGzMbQbfEJOgHTM90
ZYw6w5c0Iy5GLi1J2EiSt3oVdfgFG7A2PvxXFr/DPZdaQUyUbvfdwpeBmRIGNDLABsOxBkzddYwO
69UEWOD/4mah848VMDxToe7nEnjZ0QAqWuPxCM9UGfKRzSlkSIiDmnZBlo9lxpAj/byib/N1QElj
buqffnOuIYqqLcsSDeDwO/5MEzIpIullVcAEOzwm36LkfTcteJ4QNi0rOxseyteaJJTYpudfFejd
j3PE7uSo0rSLuNxABrkPaNJzRAt1q9/NnUWvfSL2zLabSyq0mxeEl8QDgO6/CyNPI2WPlIq3B8od
l3nvgaSUVE0z112MQLRyfV6bBOwsud0Rxw7kyKSL6vHLwy/Bsm/wYjtH4zWjZexgMKm5Hr2K2rvk
LxS5FmVc1oM7+2VD1oIPYztS7oExOpfor8bbeBB8DvEQVMV/tN7kf+mpZT/VYIwCTsOoSz8Q5LNy
T0/BdgOs7VWgMA5tW6uoK2iRCWYieACroAnGOcoUCLUlq5fhyXIBvg4GBS5PxZkCExmYmcDcnwMQ
JTDHVY+YNV2B/ILONeG3UzmSqPMVGtrfZ3yzGx+yZ6ODTjlbm4UXhoaAekkTZBwid6ws3ezyM5j1
Jw0Bvs5SJ2XIh1inVb+0zGmZcob+Rn9t5fsmy4ZyRip+1WahWqvXqjR/2tFAZYGsqLUa+x2KRVVR
fYpdp7enqXF7/HbeoL1OWShxlASV1sLghKbPSm5SmWREscpV6+yD46Nnw3ET70OfULu1ybeanvNJ
8sb8dK4QSEiwDqCU3lmKGDGA2wzM0jSfMsfFaaAgcznKrEGwYO8fzzVuW8xS+LRjI7XXgiM4WiDA
fow03lu+IKugOcLWA38SU+R2uZXgqfFzTSb9Q0KtFVwAb55AYv4gSlPrMccTd1zl5Hkd1sIPA30G
+NP3MVYnsZdyF54MM7MpINptQvTjVqJWB7y7r+/QdDzZ2Wp4kiDopQdknT4OkURRuV4H1fw962Ou
6YGY3gcKnZUXo6dod3NdWn7c0xO447Vs07IpL4RgS6lDbmMsQKXUvWexGxTgFdgdGtrMkcTx0DdX
nrUXBgQPaGfUVjUoYbf1j1bMDa6TyI3IPxUpRkXEWuhwGYZjDXvV5TVYx9cXAQugAFk8VDQ334SQ
NMHlC7FYqaenB97LdZqWXRKK1NAoaTeQDHWOjhcXDKMrJdOODKsAe3Srn6yVm320G7dG9prZSkvT
t4V5CDiIAeO5sQVHxNO6aknWlSGL4Je617PD5kX72YhPSkBwxE5TnOkZhq4SYBUuUi8f7RCtLxUF
CxWtSKnjD3xu1p1oV5jsu67U8ExFjeFvdlkG+dhLlHq+xxavfcCxQVqRjRn2EU5KxKl3ugbTYgVJ
hkjseY/frbww3NmDoXlypJq5DX0p2J/hwY6iaaBkj8HupXxnrrF3OBD4gvN1U5BW6RmN0Sdy67xc
IHbNnKylPxslJJzat2SQbWfFSsVVlKOgDTUdiwXsuaL90gNwPxMP8dzsyEJxSC8fhNsKijUXcy06
YK3VJ+AgTT/vYHUhT9LbcfdhYWPTXKBDDcHc29bMQh1bviBPTyV2jy+VxgK0QQcH1zK+axDlDZAT
HWWzWibu857lGUqaU79YaS7feSdWAVY8FITVwPsJcpYBv/pvRtQYqZYDeojouf73UvYI4MVEm9gM
oMQ+sGTfEhlJpwljZbipMcayS/hjAZUXshMlnZjEtBN/n73TKg7DbSdEqWtZ8gsbLgLQq1LBzT7y
DgDGLZC5HaUhf6Hh/9eE4N7Cu9n22bQq0VIt0BDC0qKg/cCYRAustoIb6YaMErjq1k2V2MJqXjn/
T79p7CxdKUu859z51kXvbM2ePkBxLb6jfesIOEGKWBN3/YcZbkEIZhylWGEpQAXx4Py9s3MBjyb6
0J55I5OttB6tOz9c+ps4/EoVjNAan1ZJHPpFNMi8QKDynyIyfrVu6plJ79ng4scblapEgU/Z3vZX
duFvTK881O2Qj3iboT3wU5wguaVi4Io/GPT9kjYz94rosZWFGOjVcqgdMXgDPNxQlm+YF16oXSy/
PbcPm7ovZ9JxaFAqYHx0Dcr1f4SMM8MwjB6MpKJAd4lpfdAPV9PMoVjoNdATfRydG8DUeEQUIGSA
AJG5h2/80vqu35wkWof6TBXwpSP3cKSoGPJu7GmZ5S7Q5dVT0irvCjYgGNk8NErLWruAI9pU1doK
fyvbYPtwaFO+QHmV61JPDO9TuOkFGcxCSfYFUsAXvYreT78pLKCM1nUjy3TdntXI/ns9Dypt32hJ
aUkSlmauRthqXVi/T15UkEj0t3Vkw0i1sSq1olmPGyuEoON8vHqW4ZIbH0cJbhNHP2mLmaQ+B1Xi
HpMffvqxTWn8YhfJWu6YKMyYbcSNTSIoaw3qENlbM+3gw4M7uGj9znyxjFVd/EXwsd4XXDu6PfIi
ppUF4b+87jma0tjveTamqdPFluVQ+UI/NdUar0IUznYExQQSH0upySIn4q3wBMyfuaq3NCtx2f4x
/gZQsLr7gADiOIop3oTliFaY4oXQyu0XMw/JKxsoewJ4+IiO/QbNs5ziXu7pFacZWweuZfAsgOiG
yb3iA8YN5Bxdhj/S5c3QrsBRS03D2hIs8SxYyeQYcaxi0imq0Rsmz40t8Eju/aMlmv1fx1udzXUV
cxzyb+XFJucVZvHpNLFF+ayp6a35IWUHBTyibaL8h7dcr6w2rY7cHBMKJSq4TXe7NYFuNzvJDQpn
Em245h3NiLFbfvYickmeKtq3keSL+cc5lcSjPowWkbCY/MqwkkgBxuqKAU/gXgZUjn23aHLRWG7p
vPtiw431FnJxGE2rDb0HJusZWK2Tgi2kZl3QP5i2Ype8oB6ippjNbzTmPjCBsZEdDY0YX4szi/aw
2gfKoFjdtGweHDxDFAKwaI+OEXFxctRQ4mhtBrGaIl9gw/Khwbw1s80wbwdV4gjMHYTFuDRyY2vX
YlrzhGaPEiEOEapmUaQ7QBbEq5ot8unQgVEoMEY/Zm8E5TAGOE9jEDmcLUSn/9u+pRPg8KF5O0a0
yB0bREo0NU70BP6/G0z3JxQUxEmm+h37EUupc4P0/0Gc+acl2rKJRDLaE1PFh07/3dTWzMrsibYY
0HAko5C/1QEUm1YPOuxT16eusjPuCHzuH2ova9Gi6FM73Fdh9jQcrWGDv0v7TqavZtWnyt4LOWPe
ZutlTvrLPGPNDqMROf2OgimBTyiYwk7jFkKkx9+ZXPaFRhy7Qq8aRVajEcKWSMpAcTQh/nyyKYwh
PZUeavuiB3h6p3rornC20v5X0924chyUsA64d9y+lTfxPX4MEirvAaWGG/8pRyiaU2DgjHNcjTSy
YvghvGUjLQyj7GimDykvAeyj5UScZvTmuh9SC5sns/PYAjekJgXak1apCg69QyRDvv6C1dZoqaDa
DvpypoGH4JNC6Ot5QgWxCdvcCRjxrTbnCgMVACkobPN8FWTOzJ9eOWpOuX89o3mGuKO51K13Amxb
4aFRxWSjtm6BxRpoMdc49mxjEgSobVTxRaK47u3taGWhmTO00OIH+rS7pwxf6LkiHMqQ/l1a+vik
Gr5P2R7GqhJtILwpS31fSLC6VB4lvTAn+SqBx1fAcP7uIk9pT6yOy3r+cRTaSw0h8YFwFhHYQGWG
jtlrW6Fa1ZuRxiGkIPx5bXE/51zT8Il+neHUkN6mWSBqcUYOCQPHkpJe5Iuc99zW9265YHmVG9p7
mvAdDHPamesQKFvRqulKfpDgFlbwNecWQpxZ+El4vfPqWvQitth0SOSAkJUsbLUKkSZ3px6+xIFI
CeRF90Bh9fQqB6LvPCtt3PYvh35kKWBi1afglV/y3SDP76Hqi4sgvx0brrqEJORqnuuqgcRaJDI8
sBF7N1pEpGwa+Q8RIJPSpZJWXDUQvI9R/yD4xX0qJqZFAA8tBOnatZW23n7WqhSJA2ZuqCB9Z/0m
RHtektsW3lKa9fmYs/LoquxHwON5biXb03xWZy/h3JKhsPJqBVdfkmJcmtLxdreHpAUF7iJoWEb6
xN40LaMpi1vlr9bQCyiIV5mWJLzD5hH6vqh19T95YyU1DSIfK6FDuMISBBrkF7Ges8B1sv4eiY54
Li98GKhLcRD7fycLOgsn+sOg/ABEr1UP4huy5KhRaXyBsk3KdCbYJk/TefDU1sNlXEZKTYGQMRxT
yCVej+3p068GWBm8gyKZBkIuK+YShe5gYDd7mu/8aqT05bsjK6zqElw1Lf2OrtoddgmWe7qEHS+y
MLGQ5VYYl68TZNNoUoaeiMe92wYcp5eoj8Sftp94d1OUrCD+stLjXlIkfcoJw7IsSxYcLDMj9He6
o+KgbRYUPRZgLb/o+jrODe8IVf2SRSNE5j21SMu1rUPNattifGm4nbMe8+emDrF12h91ShDbbkup
pqP0Tl0t2HQMvRcppEGaz82uredrJsKB/FsTJ2MVb+A4SfrbkaDLm62ebU9i3kwRBdeQQtvlH6ah
ISdz2GYudr0FGj2tz6rVJt4X/AbPd/PY1pY01STYYMTN7mxYpcFnOPHUivwxwUPBcl9iR5NuscBy
EH1pD+FacSu8immixkbadWWYXtioR/nYezebldV+XFddmwVdrCxgRZ0XnwfsLnoN2dHxMFNxLQ+b
RvnrP6FZAXH5F0l+3xlN77or+dDdT2z1mCcmCtMZ95hRrnco+P3PzwB0qM1U0lfRCROsW+HmlyRH
b4DaOtHMIz2x20zSkKBL5qkywP4PKo5/D07Nhw28gutEojsZL/hu7GVux3H1RRR7CABouP9xY0o9
NL0GToDv6/hXMg9Ne2wOK5gJu1t1cPdtvhC+pSF0g4Bt/vRqQNz4u+vHNrZtSTKvC8THhBLWrOVY
MVAoB6XhCaBf0bz4+xhakfZ82JYl8OYMnudrMOIUxIpVyDBJkmkNDzorGSOxbHrimqhb9+2rEK6L
0By8vkQ4XxErP6Bus6VAxwRmK+l5aIQ7YEqBBvSo3kc/pLxbSaehHQ734It/ric4+jSEZY9/yLQ4
WDbOGOWLsnRWmRGLGYldZDlDlVYJGqkjOhxjsd6ohgx8KhUynyyr+R+69Kl0N8CjP7P1UkeLTe86
hEfLZO72Bb0EdAAl+182u3zhV+ls4C+hiKoehLCFUDuWRc3+UU8wEpwsFQlrbf8OztNmiDAZk3L0
SKT/vSIigRBXXUmbH9Yqdbi+rslntNxJmOE63CtgJMjw9/fBmGOKsLwBygwEwOMSjQJ3iq/bppP9
mTp2/2SZF5/ozrhOy1HrRn5AP83vP+7aYa9b6xJ24toCBds0QbUqOM4mPP6eCWGaOLzRg2P4713w
rbWMc+/g1JUxYUU2NvWklt27pBkk8aQFMA3PoZaMmFiJfUiKbScureWBOoCK1ZIKYPz+HAdLfg6m
GokVkX6yya3Qt7UXg2vc/vOHgnVHPGBXvsPtRysjdsc/KMmcySDTXCGx9XQprqI8TefWYcWoackn
bVrbzfjGfrPn+sjmxW38PiRMduMTk2NpT7VPwMAjh1X0zdPrcvqbqlTDwTOTDPtc433aD0NjHlKS
NysnxUdZ9ldXGk1HS0W1s+WQEIOALzm5RooBIkT1v2jAirafxbCe3yBDdaqYLzUwP5ug6Y1yhpEt
zsk4rsJBWZ2LJGt55WyMbqii5gFW3AuiEkb+iOC3DkFXOxziuK5gqML4msO29Of5LcZKpPwGCKVg
PMZm9Gu+Eza8I7kcwSrAOWFlghUCBpfFVKwJzu0864VV/MF+CB+/FVnVUVt0GM7zwCm+OP6IJRPx
G0EPKnpwv5uSzHqyM4JTcdx37BV8C+AsaQDCa7hjswjfvkQiZ3umPN1cH9cO2bcAthA+SWU3nxCq
OSzGZOQ/6LgxUJw0aQIHW/WwqHIaikBDvM+itiRjgNIkegJYa15Q+kbBddHo4LPEjokbyjIr7rtG
cgpqKrflr8LO6mbZWXFXFdZ7oXBhHKDlZfdQvQcyxAtqabaa3Aw9On8Jz6/7Cecxwn8pOhbLHIYQ
fQd8CGsx/h3GuKV9SOAeDiBJxOopYUgTysFjAe5Wg24dtObRJeYfj9+4oroL6HnrRiDvh7ImgZvC
S/bb4x0yKRUyU6cJ0g+gzLPxHF3YCtRtmptTg5x9FNZyXpbIipTt+PrGplVXeSZibAv91O+5OdaV
SC4X8cvmGcb0YJwi1fClW4L0dVeyf7YFKjoNJgRO5Ac+7cDtO9RhMLFufCZLAKn8f9jQtA58iY1H
VVKqV5H/L4xgw/P4sb2sD+zp+mxr0HDi+kzaX9VxCN1HSzwRRzkKPi/14ozz+Y1PZtmWBOa8ldlv
iK1rSUVbrFZNxqigjY9bMT8x/xjJ52kTt56Rek5OVf3in2Ah2e9NNysX4p2xc+l5PcnU3+/gj0Rz
CH612RtZYfnGLiHs0KyADyYC2kBKX45p/UOZ4QmBpHJ4rue+c0nBg/ZCu3hyTMY8PCrp6i3fwfgp
wRrA7Ds8NZNIiztlYMNnQT7b3T4pov8U9rk0eAf3mNMuyNfWRal8G1yvx9K30WR5xykQXWpC8B44
R7LRyxSnG55G7Hx9GCxzbpDh8wCP4pXjcyvZboz4LCC5GxJyEsLDX18Xq1Ry/UKqTTmUIPjCQtJr
FqG+2SnTHDUC4sSHte9OeGwAC3PYPaEDVYBKQO7u/GTCtMlZanbZssNTCsGMdiE/infwqzJCLA0U
m2uYbWqiqoszolUEMNYwwV5IJWyemRt1ldaEKauHA/xeH3709r+BmdC6X0Xg0SpQsx8LXTc1TCcI
4jVNVRRG0YgmAsjVZIaJc/YUES4xXAEHru4ZbiiPJE5ZUoHCycoRt7NTe/XMEo8XUJwe4HwVVf3j
FxAfMjSfd+CbcSeAWVUVBcnvZ12cYSA0foyCTszL7pLbZaRiXE48ytKXQd6YsPZYznPs8bt/mE9r
m/xfcjmqfJw/Fwu1rs9ZdnjoMGaBaRvW6PU/WOg4UGLW3XfGaaAzQj7vOHqawvMcwDrz3zyS7FVr
Cz1YzIOG7yLeJWTKm98QgehYku3cdlUikXqEAtAEtfRNmjg8siC6nCzFH3+eEdSxmoHtr7VI7jYC
IddjKooR3vnqbuSw+cMBZWWKYqWW4bU3Fjp87Z2X1X2f3To8vU2rdW91IA3kFcffk3ndxIzi0+gU
39kNOEaf5CSolwi9AvNFiw6pkP055jQBtCUg5DYJrVF0dtQaLl72kykB7jNkYBHNfxoRWufqi4/p
rrBMfE8hHx7/0xhJNLnKs2pGcdG7GhgIKe3b26idE/VFLhWTIEjCJIuv2byUstRYjXqNdh+dODBO
viuwCS+4W+NpafRWaYZBqpzsDBDD8d6iMF57PnPPdB9hJvwtuAbo2tleblxnnztvLLGllYRrYzgD
Aze7fTnPJMlI+C5iVoThtl+A20mCP3D/JJ1IEGU2Ja3XHHvSsP6w/Ri4h9wQhaoBMI33W1+goeRk
DI8L3cdF8fPesDhgP6vM3x6Z648t4c5+G87mGM8o82qvJlOVP4yXqhEEY3Cw6j3gnOvVKF0ec6Xx
Nozap23YR6eban43KSM0gQtPkykoTo291sXQsC8dSLUXjOXclUaNp6LWMomvGvzctu6tyLlNhPi4
EtVyB+71kvAsaclTFwTQe1/zRNKRx1Usot2veL0fZFKHV+ER12sfHCet6DUFWHzWlFZn27wNfFJ+
+HAajNaS41sDuYG6NHqo2/drlKlwU8WdSrZizxkyv8VXpLFKWT+fpm3e6r+VaSx4Yz/XU3qdcOA2
iLbw0Z0midhwxWMTbFllNTdDClPKN3dDhr0r8VLok2qaqCjm9eezLb9jqHa9F+6m34x/IwyZx7i4
Dp12MVZgqnCwIKLXz5A/BzlnMKMgmQZdjFGGriqQFSismrAocOwXuF/8Sfrf92eA8d6xUbqRBz9t
ISRU71YN7fYRLltwXls/HeH13+DcJEbPO036yfInpN1E9SjuR/MlENYkXLrN4U+E60RUZef9gZAe
bvnsVoYcsFMTrGP/XKW5WfZb3bLVDDiIdb7Hux14+iMYDIeKWxEKZ0z3Wi5HRpDs8ZHAtk+tHTRy
OG5u8gEuOZkEs5+wjrqgO5wqurn5hqQA/rKpg0kgwj9oDYVkziSes8yhWU5bK1RQOmi+TlQHFe+3
GI8LvpskgTdUnXmlOZj7Y26U0o9ZiHWXQT/7Pzr11vQXbY0l1H3Kcp8w1GgRr0ClMfSBwBaJyqN2
M5+FtdxxGPChQ0wyRj92TDL8KyrPiqCxPpq+rqKSbP4oHqHWkk2xsY6Z7vbBG2uKlywwqE9z1OVf
z4sWogwBUkx0wxIIXGtKzPzH7gsfC6bU5osMRDeFik3RyozBVHowG2ryI/BKV5EXGTZEZaIH+GpG
B7jEbkXS0HgY3l6LjnP2KW1lQEihRp5nfwbV6d3kQ3DST2vWTSAYtRBCJ17rEDw7QqpLWKdullrl
cqN+Q+rje8InvarchVtjFDo9wKA8YFvhG0QCcDOkO4YYGAcUqnaPcPTKJnFnX8J1f5hljfVy69b+
ctiuo+kXJsYCya6QbJFgH+LQi/ATlQfJZ0xDDFoMf/vysSbSTKWwAvh26Cp5m/ukMihQUtoSvZsa
+TM3zyzBc+xObEBXXFsD3YUDg+UFShhgykjiKcw06husz+gKbn/nOUqKSEXDeNbq6p/TUYExlC+1
bA1w+xINtupVxQGLBV5OGnGkuCzmsWrZfdazBuJ7awAMFwlcZxfjiAzCv7TlXy+kvA0Z1q1pkC9X
5zcPKeWF9wK2sU3T3uyY45DIXieZue73NIhaYCQnr3DhJT3tSHuxFzBiw1eoCmNyrMzYgwMJ/Il8
TSrt2M9ps0xhZ3nnJNsynVLMJmKwmHxGqkhcNvycNG07/WBcYQLubD9ZWlFYS6wd76V1YgixooKr
DI6A+MbL0//i0WGHNVkhcSDsrriZPw5n2Dtfkt3O4pJD1GmZ3RXdZtuq04bNzJaUMyqn7BhmfPfD
T1NiXJAn553Ngp+a53yyFK09N9GOCSrJvPOJq8KpCV89UlffZsoTH4WaSi5eOtDsixVBCKECOqqp
ew3kL/xd6D2wTTj1yYifeol8kBzlCHy2PdopOvvHkCuVU/ZZrbtzGGQ6FDSTdt1L8sqRHPHa/IHE
gCT83pjgbFZuOw44X60pq0Cc2BM9ev+xVI+d2IewoyTiEI2CV1w8ME3fCdvup2HK1ti5nA7NAHRy
MPdI+A/JpFfDYb5ajgpF07FiSJhyCQ61Nvl6aZrtS6E8Zld0ODV7R4pgFOsxZVcZpdsHJp8moX0c
S+iem+KSGlv/ir34F6t+OVHoiOH3mBWIhruGaKP2+44ZhuInFqu0baWn4EDRPtMv5h8rZVYXRtpd
Dzrj1gLZ4Hw/gvuWRbM3DitLu536aKVu8geTgDdLdrFfr2lnwNAriji8D0yceE4Pv++hxk+uZ0WR
9iFw8+pjBSf1v3DUM2ujPCnpGEjF+GfSlYTvG+MYDbJluV1eImIK4HVek6aJPtDWUi4GoNQ3IQUc
95hiRpI1aIjeVCHXM8HwLqygmZZ+GN3nqQO2uTQkoxLP5uyaXEmf3KKaePQjCApXe51nUKIRLTUX
/hc0exmxOTWA7DaP6vxcN7tNs9laziqvxC5XWCqmA6b+vYAtPXWEyPnaxhjkkRT74Ucn3NZFNdRa
bXVcx84weY4YwakKNNbs3Rqc
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
