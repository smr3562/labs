-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 18:31:13 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_peakdetector_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_peakdetector_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog is
  port (
    axi_awready_reg_0 : out STD_LOGIC;
    axi_wready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[2][15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg_array_reg[3][15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg_array_reg[4][9]_0\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_awvalid : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog is
  signal axi_araddr : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^d\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dec_r__10\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \dec_w__10\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^i\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal r4_x_delay : STD_LOGIC_VECTOR ( 31 downto 10 );
  signal r5_x_noise : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r6_x_min : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r7_x_max : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r8_en_pkd : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg_array[0][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][14]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][3]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array_reg[0]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \slv_reg_array_reg[1]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \slv_reg_array_reg[2]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \^slv_reg_array_reg[2][15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg_array_reg[3]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \^slv_reg_array_reg[3][15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg_array_reg[4]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \^slv_reg_array_reg[4][9]_0\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal slv_wire_array : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[10]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[12]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[13]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[14]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[16]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[17]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[18]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \axi_rdata[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[20]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[21]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \axi_rdata[22]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[23]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \axi_rdata[24]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \axi_rdata[25]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \axi_rdata[26]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \axi_rdata[28]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \axi_rdata[29]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \axi_rdata[2]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[30]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[5]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[6]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[7]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \axi_rdata[9]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[0][7]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[1][14]_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[1][14]_i_4\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[2][7]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[4][3]_i_2\ : label is "soft_lutpair0";
begin
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  d(0) <= \^d\(0);
  i(15 downto 0) <= \^i\(15 downto 0);
  \slv_reg_array_reg[2][15]_0\(15 downto 0) <= \^slv_reg_array_reg[2][15]_0\(15 downto 0);
  \slv_reg_array_reg[3][15]_0\(15 downto 0) <= \^slv_reg_array_reg[3][15]_0\(15 downto 0);
  \slv_reg_array_reg[4][9]_0\(9 downto 0) <= \^slv_reg_array_reg[4][9]_0\(9 downto 0);
\axi_araddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(0),
      Q => axi_araddr(0),
      R => p_0_in
    );
\axi_araddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(1),
      Q => axi_araddr(1),
      R => p_0_in
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(2),
      Q => axi_araddr(2),
      R => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(3),
      Q => axi_araddr(3),
      R => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(4),
      Q => axi_araddr(4),
      R => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => p_0_in
    );
\axi_awaddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(0),
      Q => axi_awaddr(0),
      R => p_0_in
    );
\axi_awaddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(1),
      Q => axi_awaddr(1),
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(2),
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(3),
      Q => axi_awaddr(3),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(4),
      Q => axi_awaddr(4),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axibusdomain_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_awvalid,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => p_0_in
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => axibusdomain_s_axi_bvalid,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(0),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[0]_i_2_n_0\,
      O => slv_wire_array(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(0),
      I1 => \^slv_reg_array_reg[2][15]_0\(0),
      I2 => \dec_r__10\(1),
      I3 => \^i\(0),
      I4 => \dec_r__10\(0),
      I5 => \^d\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(10),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[10]_i_2_n_0\,
      O => slv_wire_array(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(10),
      I1 => \^slv_reg_array_reg[2][15]_0\(10),
      I2 => \dec_r__10\(1),
      I3 => \^i\(10),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(11),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[11]_i_2_n_0\,
      O => slv_wire_array(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(11),
      I1 => \^slv_reg_array_reg[2][15]_0\(11),
      I2 => \dec_r__10\(1),
      I3 => \^i\(11),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(12),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[12]_i_2_n_0\,
      O => slv_wire_array(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(12),
      I1 => \^slv_reg_array_reg[2][15]_0\(12),
      I2 => \dec_r__10\(1),
      I3 => \^i\(12),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(13),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[13]_i_2_n_0\,
      O => slv_wire_array(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(13),
      I1 => \^slv_reg_array_reg[2][15]_0\(13),
      I2 => \dec_r__10\(1),
      I3 => \^i\(13),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(14),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[14]_i_2_n_0\,
      O => slv_wire_array(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(14),
      I1 => \^slv_reg_array_reg[2][15]_0\(14),
      I2 => \dec_r__10\(1),
      I3 => \^i\(14),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(15),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[15]_i_2_n_0\,
      O => slv_wire_array(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(15),
      I1 => \^slv_reg_array_reg[2][15]_0\(15),
      I2 => \dec_r__10\(1),
      I3 => \^i\(15),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(16),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[16]_i_2_n_0\,
      O => slv_wire_array(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(16),
      I1 => r6_x_min(16),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(16),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(17),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[17]_i_2_n_0\,
      O => slv_wire_array(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(17),
      I1 => r6_x_min(17),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(17),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(18),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[18]_i_2_n_0\,
      O => slv_wire_array(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(18),
      I1 => r6_x_min(18),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(18),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(19),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[19]_i_2_n_0\,
      O => slv_wire_array(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(19),
      I1 => r6_x_min(19),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(19),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(1),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[1]_i_2_n_0\,
      O => slv_wire_array(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(1),
      I1 => \^slv_reg_array_reg[2][15]_0\(1),
      I2 => \dec_r__10\(1),
      I3 => \^i\(1),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(20),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[20]_i_2_n_0\,
      O => slv_wire_array(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(20),
      I1 => r6_x_min(20),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(20),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(21),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[21]_i_2_n_0\,
      O => slv_wire_array(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(21),
      I1 => r6_x_min(21),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(21),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(22),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[22]_i_2_n_0\,
      O => slv_wire_array(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(22),
      I1 => r6_x_min(22),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(22),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(23),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[23]_i_2_n_0\,
      O => slv_wire_array(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(23),
      I1 => r6_x_min(23),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(23),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(24),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[24]_i_2_n_0\,
      O => slv_wire_array(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(24),
      I1 => r6_x_min(24),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(24),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(25),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[25]_i_2_n_0\,
      O => slv_wire_array(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(25),
      I1 => r6_x_min(25),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(25),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(26),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[26]_i_2_n_0\,
      O => slv_wire_array(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(26),
      I1 => r6_x_min(26),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(26),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(27),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[27]_i_2_n_0\,
      O => slv_wire_array(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(27),
      I1 => r6_x_min(27),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(27),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(28),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[28]_i_2_n_0\,
      O => slv_wire_array(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(28),
      I1 => r6_x_min(28),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(28),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(29),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[29]_i_2_n_0\,
      O => slv_wire_array(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(29),
      I1 => r6_x_min(29),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(29),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(2),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[2]_i_2_n_0\,
      O => slv_wire_array(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(2),
      I1 => \^slv_reg_array_reg[2][15]_0\(2),
      I2 => \dec_r__10\(1),
      I3 => \^i\(2),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(30),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[30]_i_2_n_0\,
      O => slv_wire_array(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(30),
      I1 => r6_x_min(30),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(30),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(31),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[31]_i_3_n_0\,
      O => slv_wire_array(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => axi_araddr(4),
      I1 => axi_araddr(2),
      I2 => axi_araddr(1),
      I3 => axi_araddr(3),
      I4 => axi_araddr(0),
      O => \dec_r__10\(2)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(31),
      I1 => r6_x_min(31),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(31),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(4),
      I1 => axi_araddr(0),
      I2 => axi_araddr(3),
      I3 => axi_araddr(1),
      O => \dec_r__10\(1)
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(4),
      I1 => axi_araddr(0),
      I2 => axi_araddr(2),
      I3 => axi_araddr(1),
      O => \dec_r__10\(0)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(3),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[3]_i_2_n_0\,
      O => slv_wire_array(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(3),
      I1 => \^slv_reg_array_reg[2][15]_0\(3),
      I2 => \dec_r__10\(1),
      I3 => \^i\(3),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(4),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[4]_i_2_n_0\,
      O => slv_wire_array(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(4),
      I1 => \^slv_reg_array_reg[2][15]_0\(4),
      I2 => \dec_r__10\(1),
      I3 => \^i\(4),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(5),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[5]_i_2_n_0\,
      O => slv_wire_array(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(5),
      I1 => \^slv_reg_array_reg[2][15]_0\(5),
      I2 => \dec_r__10\(1),
      I3 => \^i\(5),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(6),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[6]_i_2_n_0\,
      O => slv_wire_array(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(6),
      I1 => \^slv_reg_array_reg[2][15]_0\(6),
      I2 => \dec_r__10\(1),
      I3 => \^i\(6),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(7),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[7]_i_2_n_0\,
      O => slv_wire_array(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(7),
      I1 => \^slv_reg_array_reg[2][15]_0\(7),
      I2 => \dec_r__10\(1),
      I3 => \^i\(7),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(8),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[8]_i_2_n_0\,
      O => slv_wire_array(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(8),
      I1 => \^slv_reg_array_reg[2][15]_0\(8),
      I2 => \dec_r__10\(1),
      I3 => \^i\(8),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(9),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[9]_i_2_n_0\,
      O => slv_wire_array(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(9),
      I1 => \^slv_reg_array_reg[2][15]_0\(9),
      I2 => \dec_r__10\(1),
      I3 => \^i\(9),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(0),
      Q => axibusdomain_s_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(10),
      Q => axibusdomain_s_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(11),
      Q => axibusdomain_s_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(12),
      Q => axibusdomain_s_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(13),
      Q => axibusdomain_s_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(14),
      Q => axibusdomain_s_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(15),
      Q => axibusdomain_s_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(16),
      Q => axibusdomain_s_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(17),
      Q => axibusdomain_s_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(18),
      Q => axibusdomain_s_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(19),
      Q => axibusdomain_s_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(1),
      Q => axibusdomain_s_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(20),
      Q => axibusdomain_s_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(21),
      Q => axibusdomain_s_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(22),
      Q => axibusdomain_s_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(23),
      Q => axibusdomain_s_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(24),
      Q => axibusdomain_s_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(25),
      Q => axibusdomain_s_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(26),
      Q => axibusdomain_s_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(27),
      Q => axibusdomain_s_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(28),
      Q => axibusdomain_s_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(29),
      Q => axibusdomain_s_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(2),
      Q => axibusdomain_s_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(30),
      Q => axibusdomain_s_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(31),
      Q => axibusdomain_s_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(3),
      Q => axibusdomain_s_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(4),
      Q => axibusdomain_s_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(5),
      Q => axibusdomain_s_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(6),
      Q => axibusdomain_s_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(7),
      Q => axibusdomain_s_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(8),
      Q => axibusdomain_s_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(9),
      Q => axibusdomain_s_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^axibusdomain_s_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_awvalid,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => p_0_in
    );
\slv_reg_array[0][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFFFDFDFD00FDFD"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      I2 => axibusdomain_s_axi_wdata(0),
      I3 => \slv_reg_array[0][0]_i_2_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^d\(0),
      O => \slv_reg_array[0][0]_i_1_n_0\
    );
\slv_reg_array[0][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[0]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[0][0]_i_2_n_0\
    );
\slv_reg_array[0][15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[0]\(7),
      I3 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[0]\(7),
      O => \slv_reg_array[0][15]_i_2_n_0\
    );
\slv_reg_array[0][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[0]\(7),
      I3 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[0]\(7),
      O => \slv_reg_array[0][23]_i_2_n_0\
    );
\slv_reg_array[0][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[0]\(7),
      I3 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[0]\(7),
      O => \slv_reg_array[0][31]_i_2_n_0\
    );
\slv_reg_array[0][31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_3_n_0\
    );
\slv_reg_array[0][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE1FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[0]\(7)
    );
\slv_reg_array[0][31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAABBBE"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => axi_awaddr(1),
      I5 => axi_awaddr(0),
      O => \slv_reg_array[0][31]_i_5_n_0\
    );
\slv_reg_array[0][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      I2 => \slv_reg_array[0][7]_i_2_n_0\,
      O => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[0]\(7),
      O => \slv_reg_array[0][7]_i_2_n_0\
    );
\slv_reg_array[0][7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      O => \slv_reg_array[0][7]_i_3_n_0\
    );
\slv_reg_array[1][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FF808080008080"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(0),
      I3 => \slv_reg_array[1][0]_i_2_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^i\(0),
      O => \slv_reg_array[1][0]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[1]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[1][0]_i_2_n_0\
    );
\slv_reg_array[1][14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000FB0000"
    )
        port map (
      I0 => \dec_w__10\(2),
      I1 => \dec_w__10\(0),
      I2 => \dec_w__10\(1),
      I3 => axibusdomain_aresetn,
      I4 => axibusdomain_s_axi_wstrb(1),
      I5 => \slv_reg_array[0][31]_i_3_n_0\,
      O => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array[1][14]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[1]\(7),
      O => \slv_reg_array[1][14]_i_2_n_0\
    );
\slv_reg_array[1][14]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => axi_awaddr(0),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(2),
      I3 => axi_awaddr(3),
      I4 => axi_awaddr(4),
      O => \dec_w__10\(2)
    );
\slv_reg_array[1][14]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(2),
      O => \dec_w__10\(0)
    );
\slv_reg_array[1][14]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(3),
      O => \dec_w__10\(1)
    );
\slv_reg_array[1][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBFBFBF40000000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[1]\(7),
      I3 => axibusdomain_s_axi_wdata(15),
      I4 => \slv_reg_array[1][31]_i_4_n_0\,
      I5 => \^i\(15),
      O => \slv_reg_array[1][15]_i_1_n_0\
    );
\slv_reg_array[1][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80FF8000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(1),
      I3 => \slv_reg_array[1][6]_i_2_n_0\,
      I4 => \^i\(1),
      O => \slv_reg_array[1][1]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[1]\(7),
      I3 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[1]\(7),
      O => \slv_reg_array[1][23]_i_2_n_0\
    );
\slv_reg_array[1][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[1]\(7),
      I3 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[1]\(7),
      O => \slv_reg_array[1][31]_i_2_n_0\
    );
\slv_reg_array[1][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000010FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(0),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(2),
      I3 => axi_awaddr(3),
      I4 => axi_awaddr(4),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[1]\(7)
    );
\slv_reg_array[1][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(2),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][31]_i_4_n_0\
    );
\slv_reg_array[1][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80FF8000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(5),
      I3 => \slv_reg_array[1][6]_i_2_n_0\,
      I4 => \^i\(5),
      O => \slv_reg_array[1][5]_i_1_n_0\
    );
\slv_reg_array[1][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \dec_w__10\(2),
      I2 => \slv_reg_array[0][31]_i_3_n_0\,
      I3 => \dec_w__10\(1),
      I4 => \dec_w__10\(0),
      I5 => \slv_reg_array[1][6]_i_2_n_0\,
      O => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array[1][6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[1]\(7),
      O => \slv_reg_array[1][6]_i_2_n_0\
    );
\slv_reg_array[1][7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80FF8000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(7),
      I3 => \slv_reg_array[1][6]_i_2_n_0\,
      I4 => \^i\(7),
      O => \slv_reg_array[1][7]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEEE0EE"
    )
        port map (
      I0 => \slv_reg_array[2][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[2][0]_i_2_n_0\,
      I3 => axibusdomain_aresetn,
      I4 => \^slv_reg_array_reg[2][15]_0\(0),
      O => \slv_reg_array[2][0]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[2]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[2][0]_i_2_n_0\
    );
\slv_reg_array[2][15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[2]\(7),
      I3 => \slv_reg_array[2][31]_i_4_n_0\,
      O => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array[2][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[2]\(7),
      O => \slv_reg_array[2][15]_i_2_n_0\
    );
\slv_reg_array[2][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[2][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(1),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[2][15]_0\(1),
      O => \slv_reg_array[2][1]_i_1_n_0\
    );
\slv_reg_array[2][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[2]\(7),
      I3 => \slv_reg_array[2][31]_i_4_n_0\,
      O => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array[2][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[2]\(7),
      O => \slv_reg_array[2][23]_i_2_n_0\
    );
\slv_reg_array[2][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[2]\(7),
      I3 => \slv_reg_array[2][31]_i_4_n_0\,
      O => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[2]\(7),
      O => \slv_reg_array[2][31]_i_2_n_0\
    );
\slv_reg_array[2][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000002FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[2]\(7)
    );
\slv_reg_array[2][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      I5 => axi_awaddr(3),
      O => \slv_reg_array[2][31]_i_4_n_0\
    );
\slv_reg_array[2][5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[2][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(5),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[2][15]_0\(5),
      O => \slv_reg_array[2][5]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => \slv_reg_array[2][6]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[2][31]_i_4_n_0\,
      O => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[2]\(7),
      O => \slv_reg_array[2][6]_i_2_n_0\
    );
\slv_reg_array[2][7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[2][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(7),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[2][15]_0\(7),
      O => \slv_reg_array[2][7]_i_1_n_0\
    );
\slv_reg_array[2][7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFDFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \dec_w__10\(0),
      I2 => \dec_w__10\(2),
      I3 => \dec_w__10\(1),
      I4 => \slv_reg_array[0][31]_i_3_n_0\,
      O => \slv_reg_array[2][7]_i_2_n_0\
    );
\slv_reg_array[3][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEEE0EE"
    )
        port map (
      I0 => \slv_reg_array[3][6]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[3][0]_i_2_n_0\,
      I3 => axibusdomain_aresetn,
      I4 => \^slv_reg_array_reg[3][15]_0\(0),
      O => \slv_reg_array[3][0]_i_1_n_0\
    );
\slv_reg_array[3][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[3]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[3][0]_i_2_n_0\
    );
\slv_reg_array[3][15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[3]\(7),
      I3 => \slv_reg_array[3][31]_i_4_n_0\,
      O => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array[3][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[3]\(7),
      O => \slv_reg_array[3][15]_i_2_n_0\
    );
\slv_reg_array[3][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[3]\(7),
      I3 => \slv_reg_array[3][31]_i_4_n_0\,
      O => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array[3][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[3]\(7),
      O => \slv_reg_array[3][23]_i_2_n_0\
    );
\slv_reg_array[3][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[3]\(7),
      I3 => \slv_reg_array[3][31]_i_4_n_0\,
      O => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array[3][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[3]\(7),
      O => \slv_reg_array[3][31]_i_2_n_0\
    );
\slv_reg_array[3][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000008FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[3]\(7)
    );
\slv_reg_array[3][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[3][31]_i_4_n_0\
    );
\slv_reg_array[3][4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[3][6]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(4),
      I2 => \slv_reg_array[3][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(4),
      O => \slv_reg_array[3][4]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[3][6]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(6),
      I2 => \slv_reg_array[3][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(6),
      O => \slv_reg_array[3][6]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFF7F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \dec_w__10\(1),
      I2 => \dec_w__10\(0),
      I3 => \dec_w__10\(2),
      I4 => \slv_reg_array[0][31]_i_3_n_0\,
      O => \slv_reg_array[3][6]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => \slv_reg_array[3][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[3][31]_i_4_n_0\,
      O => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array[3][7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[3]\(7),
      O => \slv_reg_array[3][7]_i_2_n_0\
    );
\slv_reg_array[4][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEEE0EE"
    )
        port map (
      I0 => \slv_reg_array[4][3]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[4][0]_i_2_n_0\,
      I3 => axibusdomain_aresetn,
      I4 => \^slv_reg_array_reg[4][9]_0\(0),
      O => \slv_reg_array[4][0]_i_1_n_0\
    );
\slv_reg_array[4][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[4]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[4][0]_i_2_n_0\
    );
\slv_reg_array[4][15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[4]\(7),
      I3 => \slv_reg_array[4][31]_i_4_n_0\,
      O => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[4]\(7),
      O => \slv_reg_array[4][15]_i_2_n_0\
    );
\slv_reg_array[4][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[4][3]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(1),
      I2 => \slv_reg_array[4][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[4][9]_0\(1),
      O => \slv_reg_array[4][1]_i_1_n_0\
    );
\slv_reg_array[4][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[4]\(7),
      I3 => \slv_reg_array[4][31]_i_4_n_0\,
      O => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array[4][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[4]\(7),
      O => \slv_reg_array[4][23]_i_2_n_0\
    );
\slv_reg_array[4][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[4][3]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(2),
      I2 => \slv_reg_array[4][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[4][9]_0\(2),
      O => \slv_reg_array[4][2]_i_1_n_0\
    );
\slv_reg_array[4][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[4]\(7),
      I3 => \slv_reg_array[4][31]_i_4_n_0\,
      O => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array[4][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[4]\(7),
      O => \slv_reg_array[4][31]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000010FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[4]\(7)
    );
\slv_reg_array[4][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => \slv_reg_array[0][31]_i_3_n_0\,
      O => \slv_reg_array[4][31]_i_4_n_0\
    );
\slv_reg_array[4][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[4][3]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(3),
      I2 => \slv_reg_array[4][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[4][9]_0\(3),
      O => \slv_reg_array[4][3]_i_1_n_0\
    );
\slv_reg_array[4][3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBFF"
    )
        port map (
      I0 => \dec_w__10\(1),
      I1 => \dec_w__10\(2),
      I2 => \slv_reg_array[0][31]_i_3_n_0\,
      I3 => axibusdomain_s_axi_wstrb(0),
      I4 => \dec_w__10\(0),
      O => \slv_reg_array[4][3]_i_2_n_0\
    );
\slv_reg_array[4][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => \slv_reg_array[4][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[4][31]_i_4_n_0\,
      O => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array[4][7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[4]\(7),
      O => \slv_reg_array[4][7]_i_2_n_0\
    );
\slv_reg_array_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[0][0]_i_1_n_0\,
      Q => \^d\(0),
      R => '0'
    );
\slv_reg_array_reg[0][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => r8_en_pkd(10),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => r8_en_pkd(11),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => r8_en_pkd(12),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => r8_en_pkd(13),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => r8_en_pkd(14),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(15),
      Q => r8_en_pkd(15),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r8_en_pkd(16),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r8_en_pkd(17),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r8_en_pkd(18),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r8_en_pkd(19),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(1),
      Q => r8_en_pkd(1),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r8_en_pkd(20),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r8_en_pkd(21),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r8_en_pkd(22),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r8_en_pkd(23),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r8_en_pkd(24),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r8_en_pkd(25),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r8_en_pkd(26),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r8_en_pkd(27),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r8_en_pkd(28),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r8_en_pkd(29),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(2),
      Q => r8_en_pkd(2),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r8_en_pkd(30),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r8_en_pkd(31),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(3),
      Q => r8_en_pkd(3),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(4),
      Q => r8_en_pkd(4),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(5),
      Q => r8_en_pkd(5),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(6),
      Q => r8_en_pkd(6),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(7),
      Q => r8_en_pkd(7),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => r8_en_pkd(8),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => r8_en_pkd(9),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[1][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][0]_i_1_n_0\,
      Q => \^i\(0),
      R => '0'
    );
\slv_reg_array_reg[1][10]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => \^i\(10),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][11]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => \^i\(11),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][12]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => \^i\(12),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][13]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => \^i\(13),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][14]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => \^i\(14),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][15]_i_1_n_0\,
      Q => \^i\(15),
      R => '0'
    );
\slv_reg_array_reg[1][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r7_x_max(16),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r7_x_max(17),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r7_x_max(18),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r7_x_max(19),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => \^i\(1),
      R => '0'
    );
\slv_reg_array_reg[1][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r7_x_max(20),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r7_x_max(21),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r7_x_max(22),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r7_x_max(23),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r7_x_max(24),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r7_x_max(25),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r7_x_max(26),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r7_x_max(27),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r7_x_max(28),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r7_x_max(29),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(2),
      Q => \^i\(2),
      S => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array_reg[1][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r7_x_max(30),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r7_x_max(31),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(3),
      Q => \^i\(3),
      S => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array_reg[1][4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(4),
      Q => \^i\(4),
      S => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array_reg[1][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][5]_i_1_n_0\,
      Q => \^i\(5),
      R => '0'
    );
\slv_reg_array_reg[1][6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(6),
      Q => \^i\(6),
      S => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array_reg[1][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][7]_i_1_n_0\,
      Q => \^i\(7),
      R => '0'
    );
\slv_reg_array_reg[1][8]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => \^i\(8),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][9]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => \^i\(9),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[2][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[2][15]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[2][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => \^slv_reg_array_reg[2][15]_0\(10),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => \^slv_reg_array_reg[2][15]_0\(11),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => \^slv_reg_array_reg[2][15]_0\(12),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => \^slv_reg_array_reg[2][15]_0\(13),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => \^slv_reg_array_reg[2][15]_0\(14),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(15),
      Q => \^slv_reg_array_reg[2][15]_0\(15),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r6_x_min(16),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r6_x_min(17),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r6_x_min(18),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r6_x_min(19),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[2][15]_0\(1),
      R => '0'
    );
\slv_reg_array_reg[2][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r6_x_min(20),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r6_x_min(21),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r6_x_min(22),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r6_x_min(23),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r6_x_min(24),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r6_x_min(25),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r6_x_min(26),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r6_x_min(27),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r6_x_min(28),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r6_x_min(29),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(2),
      Q => \^slv_reg_array_reg[2][15]_0\(2),
      R => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array_reg[2][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r6_x_min(30),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r6_x_min(31),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(3),
      Q => \^slv_reg_array_reg[2][15]_0\(3),
      R => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array_reg[2][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(4),
      Q => \^slv_reg_array_reg[2][15]_0\(4),
      R => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array_reg[2][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[2][15]_0\(5),
      R => '0'
    );
\slv_reg_array_reg[2][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(6),
      Q => \^slv_reg_array_reg[2][15]_0\(6),
      R => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array_reg[2][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][7]_i_1_n_0\,
      Q => \^slv_reg_array_reg[2][15]_0\(7),
      R => '0'
    );
\slv_reg_array_reg[2][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => \^slv_reg_array_reg[2][15]_0\(8),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => \^slv_reg_array_reg[2][15]_0\(9),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[3][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => \^slv_reg_array_reg[3][15]_0\(10),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => \^slv_reg_array_reg[3][15]_0\(11),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => \^slv_reg_array_reg[3][15]_0\(12),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => \^slv_reg_array_reg[3][15]_0\(13),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => \^slv_reg_array_reg[3][15]_0\(14),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(15),
      Q => \^slv_reg_array_reg[3][15]_0\(15),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r5_x_noise(16),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r5_x_noise(17),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r5_x_noise(18),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r5_x_noise(19),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(1),
      Q => \^slv_reg_array_reg[3][15]_0\(1),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r5_x_noise(20),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r5_x_noise(21),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r5_x_noise(22),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r5_x_noise(23),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r5_x_noise(24),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r5_x_noise(25),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r5_x_noise(26),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r5_x_noise(27),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r5_x_noise(28),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r5_x_noise(29),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(2),
      Q => \^slv_reg_array_reg[3][15]_0\(2),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r5_x_noise(30),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r5_x_noise(31),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(3),
      Q => \^slv_reg_array_reg[3][15]_0\(3),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(4),
      R => '0'
    );
\slv_reg_array_reg[3][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(5),
      Q => \^slv_reg_array_reg[3][15]_0\(5),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(6),
      R => '0'
    );
\slv_reg_array_reg[3][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(7),
      Q => \^slv_reg_array_reg[3][15]_0\(7),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => \^slv_reg_array_reg[3][15]_0\(8),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => \^slv_reg_array_reg[3][15]_0\(9),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[4][9]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[4][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => r4_x_delay(10),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => r4_x_delay(11),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => r4_x_delay(12),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => r4_x_delay(13),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => r4_x_delay(14),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(15),
      Q => r4_x_delay(15),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r4_x_delay(16),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r4_x_delay(17),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r4_x_delay(18),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r4_x_delay(19),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[4][9]_0\(1),
      R => '0'
    );
\slv_reg_array_reg[4][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r4_x_delay(20),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r4_x_delay(21),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r4_x_delay(22),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r4_x_delay(23),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r4_x_delay(24),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r4_x_delay(25),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r4_x_delay(26),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r4_x_delay(27),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r4_x_delay(28),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r4_x_delay(29),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[4][9]_0\(2),
      R => '0'
    );
\slv_reg_array_reg[4][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r4_x_delay(30),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r4_x_delay(31),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[4][9]_0\(3),
      R => '0'
    );
\slv_reg_array_reg[4][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(4),
      Q => \^slv_reg_array_reg[4][9]_0\(4),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(5),
      Q => \^slv_reg_array_reg[4][9]_0\(5),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(6),
      Q => \^slv_reg_array_reg[4][9]_0\(6),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(7),
      Q => \^slv_reg_array_reg[4][9]_0\(7),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => \^slv_reg_array_reg[4][9]_0\(8),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => \^slv_reg_array_reg[4][9]_0\(9),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axibusdomain_s_axi_rvalid\,
      I2 => \^axi_arready_reg_0\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convert_func_call_ip_peakdetector_xlconvert is
  port (
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[9]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[13]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \xmax_i_12_24_reg[15]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 13 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convert_func_call_ip_peakdetector_xlconvert;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convert_func_call_ip_peakdetector_xlconvert is
  signal \inp_carry__0_n_0\ : STD_LOGIC;
  signal \inp_carry__0_n_1\ : STD_LOGIC;
  signal \inp_carry__0_n_2\ : STD_LOGIC;
  signal \inp_carry__0_n_3\ : STD_LOGIC;
  signal \inp_carry__1_n_0\ : STD_LOGIC;
  signal \inp_carry__1_n_1\ : STD_LOGIC;
  signal \inp_carry__1_n_2\ : STD_LOGIC;
  signal \inp_carry__1_n_3\ : STD_LOGIC;
  signal \inp_carry__2_n_3\ : STD_LOGIC;
  signal inp_carry_n_0 : STD_LOGIC;
  signal inp_carry_n_1 : STD_LOGIC;
  signal inp_carry_n_2 : STD_LOGIC;
  signal inp_carry_n_3 : STD_LOGIC;
  signal \NLW_inp_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_inp_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
begin
inp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => inp_carry_n_0,
      CO(2) => inp_carry_n_1,
      CO(1) => inp_carry_n_2,
      CO(0) => inp_carry_n_3,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => Q(0),
      O(3 downto 0) => O(3 downto 0),
      S(3 downto 1) => Q(3 downto 1),
      S(0) => S(0)
    );
\inp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => inp_carry_n_0,
      CO(3) => \inp_carry__0_n_0\,
      CO(2) => \inp_carry__0_n_1\,
      CO(1) => \inp_carry__0_n_2\,
      CO(0) => \inp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \xmax_i_12_24_reg[9]\(3 downto 0),
      S(3 downto 0) => Q(7 downto 4)
    );
\inp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \inp_carry__0_n_0\,
      CO(3) => \inp_carry__1_n_0\,
      CO(2) => \inp_carry__1_n_1\,
      CO(1) => \inp_carry__1_n_2\,
      CO(0) => \inp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \xmax_i_12_24_reg[13]\(3 downto 0),
      S(3 downto 0) => Q(11 downto 8)
    );
\inp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \inp_carry__1_n_0\,
      CO(3) => \NLW_inp_carry__2_CO_UNCONNECTED\(3),
      CO(2) => CO(0),
      CO(1) => \NLW_inp_carry__2_CO_UNCONNECTED\(1),
      CO(0) => \inp_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3 downto 2) => \NLW_inp_carry__2_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => \xmax_i_12_24_reg[15]\(1 downto 0),
      S(3 downto 2) => B"01",
      S(1 downto 0) => Q(13 downto 12)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_logical_80d79f42b8 is
  port (
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    D : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_logical_80d79f42b8;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_logical_80d79f42b8 is
begin
\latency_pipe_5_26_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(0),
      Q => peak_amp_dac(0),
      R => '0'
    );
\latency_pipe_5_26_reg[0][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(10),
      Q => peak_amp_dac(10),
      R => '0'
    );
\latency_pipe_5_26_reg[0][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(11),
      Q => peak_amp_dac(11),
      R => '0'
    );
\latency_pipe_5_26_reg[0][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(12),
      Q => peak_amp_dac(12),
      R => '0'
    );
\latency_pipe_5_26_reg[0][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(13),
      Q => peak_amp_dac(13),
      R => '0'
    );
\latency_pipe_5_26_reg[0][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(1),
      Q => peak_amp_dac(1),
      R => '0'
    );
\latency_pipe_5_26_reg[0][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(2),
      Q => peak_amp_dac(2),
      R => '0'
    );
\latency_pipe_5_26_reg[0][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(3),
      Q => peak_amp_dac(3),
      R => '0'
    );
\latency_pipe_5_26_reg[0][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(4),
      Q => peak_amp_dac(4),
      R => '0'
    );
\latency_pipe_5_26_reg[0][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(5),
      Q => peak_amp_dac(5),
      R => '0'
    );
\latency_pipe_5_26_reg[0][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(6),
      Q => peak_amp_dac(6),
      R => '0'
    );
\latency_pipe_5_26_reg[0][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(7),
      Q => peak_amp_dac(7),
      R => '0'
    );
\latency_pipe_5_26_reg[0][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(8),
      Q => peak_amp_dac(8),
      R => '0'
    );
\latency_pipe_5_26_reg[0][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(9),
      Q => peak_amp_dac(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c3d4c1851 is
  port (
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    d : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \xmax_i_12_24_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    mcode_cmp_en_net : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_array[12].fde_used.u2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_array[7].fde_used.u2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_array[11].fde_used.u2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c3d4c1851;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c3d4c1851 is
  signal \^q\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rel_19_28 : STD_LOGIC;
  signal \rel_19_28_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_n_1\ : STD_LOGIC;
  signal \rel_19_28_carry__0_n_2\ : STD_LOGIC;
  signal \rel_19_28_carry__0_n_3\ : STD_LOGIC;
  signal rel_19_28_carry_i_1_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_2_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_3_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_4_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_5_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_6_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_7_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_8_n_0 : STD_LOGIC;
  signal rel_19_28_carry_n_0 : STD_LOGIC;
  signal rel_19_28_carry_n_1 : STD_LOGIC;
  signal rel_19_28_carry_n_2 : STD_LOGIC;
  signal rel_19_28_carry_n_3 : STD_LOGIC;
  signal xmax_i_12_2400_out : STD_LOGIC;
  signal NLW_rel_19_28_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_19_28_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \reg_array[0].fde_used.u2_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \reg_array[10].fde_used.u2_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \reg_array[11].fde_used.u2_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \reg_array[12].fde_used.u2_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \reg_array[13].fde_used.u2_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \reg_array[1].fde_used.u2_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \reg_array[2].fde_used.u2_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \reg_array[3].fde_used.u2_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \reg_array[4].fde_used.u2_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \reg_array[5].fde_used.u2_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \reg_array[6].fde_used.u2_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \reg_array[7].fde_used.u2_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \reg_array[8].fde_used.u2_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \reg_array[9].fde_used.u2_i_1\ : label is "soft_lutpair24";
begin
  Q(15 downto 0) <= \^q\(15 downto 0);
inp_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5A9A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(15),
      I2 => \^q\(1),
      I3 => \^q\(0),
      O => \xmax_i_12_24_reg[2]_0\(0)
    );
\reg_array[0].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => O(0),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(0)
    );
\reg_array[10].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[11].fde_used.u2\(2),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(10)
    );
\reg_array[11].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[11].fde_used.u2\(3),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(11)
    );
\reg_array[12].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[12].fde_used.u2\(0),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(12)
    );
\reg_array[13].fde_used.u2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => CO(0),
      O => d(13)
    );
\reg_array[1].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => O(1),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(1)
    );
\reg_array[2].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => O(2),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(2)
    );
\reg_array[3].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => O(3),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(3)
    );
\reg_array[4].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[7].fde_used.u2\(0),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(4)
    );
\reg_array[5].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[7].fde_used.u2\(1),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(5)
    );
\reg_array[6].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[7].fde_used.u2\(2),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(6)
    );
\reg_array[7].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[7].fde_used.u2\(3),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(7)
    );
\reg_array[8].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[11].fde_used.u2\(0),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(8)
    );
\reg_array[9].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[11].fde_used.u2\(1),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(9)
    );
rel_19_28_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_19_28_carry_n_0,
      CO(2) => rel_19_28_carry_n_1,
      CO(1) => rel_19_28_carry_n_2,
      CO(0) => rel_19_28_carry_n_3,
      CYINIT => '0',
      DI(3) => rel_19_28_carry_i_1_n_0,
      DI(2) => rel_19_28_carry_i_2_n_0,
      DI(1) => rel_19_28_carry_i_3_n_0,
      DI(0) => rel_19_28_carry_i_4_n_0,
      O(3 downto 0) => NLW_rel_19_28_carry_O_UNCONNECTED(3 downto 0),
      S(3) => rel_19_28_carry_i_5_n_0,
      S(2) => rel_19_28_carry_i_6_n_0,
      S(1) => rel_19_28_carry_i_7_n_0,
      S(0) => rel_19_28_carry_i_8_n_0
    );
\rel_19_28_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_19_28_carry_n_0,
      CO(3) => rel_19_28,
      CO(2) => \rel_19_28_carry__0_n_1\,
      CO(1) => \rel_19_28_carry__0_n_2\,
      CO(0) => \rel_19_28_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \rel_19_28_carry__0_i_1_n_0\,
      DI(2) => \rel_19_28_carry__0_i_2_n_0\,
      DI(1) => \rel_19_28_carry__0_i_3_n_0\,
      DI(0) => \rel_19_28_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_rel_19_28_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \rel_19_28_carry__0_i_5_n_0\,
      S(2) => \rel_19_28_carry__0_i_6_n_0\,
      S(1) => \rel_19_28_carry__0_i_7_n_0\,
      S(0) => \rel_19_28_carry__0_i_8_n_0\
    );
\rel_19_28_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(14),
      I1 => x(14),
      I2 => \^q\(15),
      I3 => x(15),
      O => \rel_19_28_carry__0_i_1_n_0\
    );
\rel_19_28_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(12),
      I1 => x(12),
      I2 => x(13),
      I3 => \^q\(13),
      O => \rel_19_28_carry__0_i_2_n_0\
    );
\rel_19_28_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(10),
      I1 => x(10),
      I2 => x(11),
      I3 => \^q\(11),
      O => \rel_19_28_carry__0_i_3_n_0\
    );
\rel_19_28_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(8),
      I1 => x(8),
      I2 => x(9),
      I3 => \^q\(9),
      O => \rel_19_28_carry__0_i_4_n_0\
    );
\rel_19_28_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8421"
    )
        port map (
      I0 => x(14),
      I1 => x(15),
      I2 => \^q\(14),
      I3 => \^q\(15),
      O => \rel_19_28_carry__0_i_5_n_0\
    );
\rel_19_28_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(12),
      I1 => x(13),
      I2 => \^q\(13),
      I3 => \^q\(12),
      O => \rel_19_28_carry__0_i_6_n_0\
    );
\rel_19_28_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(10),
      I1 => x(11),
      I2 => \^q\(11),
      I3 => \^q\(10),
      O => \rel_19_28_carry__0_i_7_n_0\
    );
\rel_19_28_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(8),
      I1 => x(9),
      I2 => \^q\(9),
      I3 => \^q\(8),
      O => \rel_19_28_carry__0_i_8_n_0\
    );
rel_19_28_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(6),
      I1 => x(6),
      I2 => x(7),
      I3 => \^q\(7),
      O => rel_19_28_carry_i_1_n_0
    );
rel_19_28_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(4),
      I1 => x(4),
      I2 => x(5),
      I3 => \^q\(5),
      O => rel_19_28_carry_i_2_n_0
    );
rel_19_28_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(2),
      I1 => x(2),
      I2 => x(3),
      I3 => \^q\(3),
      O => rel_19_28_carry_i_3_n_0
    );
rel_19_28_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(0),
      I1 => x(0),
      I2 => x(1),
      I3 => \^q\(1),
      O => rel_19_28_carry_i_4_n_0
    );
rel_19_28_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(6),
      I1 => x(7),
      I2 => \^q\(7),
      I3 => \^q\(6),
      O => rel_19_28_carry_i_5_n_0
    );
rel_19_28_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(4),
      I1 => x(5),
      I2 => \^q\(5),
      I3 => \^q\(4),
      O => rel_19_28_carry_i_6_n_0
    );
rel_19_28_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(2),
      I1 => x(3),
      I2 => \^q\(3),
      I3 => \^q\(2),
      O => rel_19_28_carry_i_7_n_0
    );
rel_19_28_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(0),
      I1 => \^q\(1),
      I2 => x(1),
      I3 => \^q\(0),
      O => rel_19_28_carry_i_8_n_0
    );
\xmax_i_12_24[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => rel_19_28,
      I1 => mcode_cmp_en_net,
      O => xmax_i_12_2400_out
    );
\xmax_i_12_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(0),
      Q => \^q\(0),
      R => SR(0)
    );
\xmax_i_12_24_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(10),
      Q => \^q\(10),
      R => SR(0)
    );
\xmax_i_12_24_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(11),
      Q => \^q\(11),
      R => SR(0)
    );
\xmax_i_12_24_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(12),
      Q => \^q\(12),
      R => SR(0)
    );
\xmax_i_12_24_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(13),
      Q => \^q\(13),
      R => SR(0)
    );
\xmax_i_12_24_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(14),
      Q => \^q\(14),
      R => SR(0)
    );
\xmax_i_12_24_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(15),
      Q => \^q\(15),
      R => SR(0)
    );
\xmax_i_12_24_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(1),
      Q => \^q\(1),
      R => SR(0)
    );
\xmax_i_12_24_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(2),
      Q => \^q\(2),
      R => SR(0)
    );
\xmax_i_12_24_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(3),
      Q => \^q\(3),
      R => SR(0)
    );
\xmax_i_12_24_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(4),
      Q => \^q\(4),
      R => SR(0)
    );
\xmax_i_12_24_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(5),
      Q => \^q\(5),
      R => SR(0)
    );
\xmax_i_12_24_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(6),
      Q => \^q\(6),
      R => SR(0)
    );
\xmax_i_12_24_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(7),
      Q => \^q\(7),
      R => SR(0)
    );
\xmax_i_12_24_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(8),
      Q => \^q\(8),
      R => SR(0)
    );
\xmax_i_12_24_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(9),
      Q => \^q\(9),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c4ff59dc3 is
  port (
    \ff_2_17_reg[0]_0\ : out STD_LOGIC;
    mcode_pkd_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c4ff59dc3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c4ff59dc3 is
begin
\ff_2_17_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => mcode_pkd_net,
      Q => \ff_2_17_reg[0]_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_a80b0cfdd5 is
  port (
    mcode_pkd_net : out STD_LOGIC;
    mcode_cmp_en_net : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_63_21_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_63_21_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_30_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_30_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_16_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_47_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_47_16_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[2]_i_2_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[2]_i_2_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_35_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_35_16_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_3 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    q : in STD_LOGIC_VECTOR ( 0 to 0 );
    peak_amp_rdy_0_sp_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_a80b0cfdd5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_a80b0cfdd5 is
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal cmp_en_i_18_26_i_1_n_0 : STD_LOGIC;
  signal cmp_rst_i_17_27_i_1_n_0 : STD_LOGIC;
  signal \^mcode_cmp_en_net\ : STD_LOGIC;
  signal \^mcode_pkd_net\ : STD_LOGIC;
  signal peak_amp_rdy_0_sn_1 : STD_LOGIC;
  signal pkd_i_16_23_i_1_n_0 : STD_LOGIC;
  signal rel_35_16 : STD_LOGIC;
  signal \rel_35_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_35_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_35_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_35_16_carry_n_0 : STD_LOGIC;
  signal rel_35_16_carry_n_1 : STD_LOGIC;
  signal rel_35_16_carry_n_2 : STD_LOGIC;
  signal rel_35_16_carry_n_3 : STD_LOGIC;
  signal rel_47_16 : STD_LOGIC;
  signal \rel_47_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_47_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_47_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_47_16_carry_n_0 : STD_LOGIC;
  signal rel_47_16_carry_n_1 : STD_LOGIC;
  signal rel_47_16_carry_n_2 : STD_LOGIC;
  signal rel_47_16_carry_n_3 : STD_LOGIC;
  signal rel_55_16 : STD_LOGIC;
  signal \rel_55_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_55_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_55_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_55_16_carry_n_0 : STD_LOGIC;
  signal rel_55_16_carry_n_1 : STD_LOGIC;
  signal rel_55_16_carry_n_2 : STD_LOGIC;
  signal rel_55_16_carry_n_3 : STD_LOGIC;
  signal rel_55_30 : STD_LOGIC;
  signal \rel_55_30_carry__0_n_1\ : STD_LOGIC;
  signal \rel_55_30_carry__0_n_2\ : STD_LOGIC;
  signal \rel_55_30_carry__0_n_3\ : STD_LOGIC;
  signal rel_55_30_carry_n_0 : STD_LOGIC;
  signal rel_55_30_carry_n_1 : STD_LOGIC;
  signal rel_55_30_carry_n_2 : STD_LOGIC;
  signal rel_55_30_carry_n_3 : STD_LOGIC;
  signal rel_61_16 : STD_LOGIC;
  signal \rel_61_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_61_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_61_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_61_16_carry_n_0 : STD_LOGIC;
  signal rel_61_16_carry_n_1 : STD_LOGIC;
  signal rel_61_16_carry_n_2 : STD_LOGIC;
  signal rel_61_16_carry_n_3 : STD_LOGIC;
  signal rel_61_30 : STD_LOGIC;
  signal \rel_61_30_carry__0_n_1\ : STD_LOGIC;
  signal \rel_61_30_carry__0_n_2\ : STD_LOGIC;
  signal \rel_61_30_carry__0_n_3\ : STD_LOGIC;
  signal rel_61_30_carry_n_0 : STD_LOGIC;
  signal rel_61_30_carry_n_1 : STD_LOGIC;
  signal rel_61_30_carry_n_2 : STD_LOGIC;
  signal rel_61_30_carry_n_3 : STD_LOGIC;
  signal rel_63_21 : STD_LOGIC;
  signal \rel_63_21_carry__0_n_1\ : STD_LOGIC;
  signal \rel_63_21_carry__0_n_2\ : STD_LOGIC;
  signal \rel_63_21_carry__0_n_3\ : STD_LOGIC;
  signal rel_63_21_carry_n_0 : STD_LOGIC;
  signal rel_63_21_carry_n_1 : STD_LOGIC;
  signal rel_63_21_carry_n_2 : STD_LOGIC;
  signal rel_63_21_carry_n_3 : STD_LOGIC;
  signal state_15_23 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \state_15_23[0]_i_2_n_0\ : STD_LOGIC;
  signal \state_15_23[0]_i_3_n_0\ : STD_LOGIC;
  signal \state_15_23[1]_i_2_n_0\ : STD_LOGIC;
  signal \state_15_23[1]_i_3_n_0\ : STD_LOGIC;
  signal \state_15_23[2]_i_2_n_0\ : STD_LOGIC;
  signal \state_15_23[2]_i_3_n_0\ : STD_LOGIC;
  signal state_join_28_5 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_rel_35_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_35_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_47_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_47_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_55_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_55_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_55_30_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_55_30_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_61_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_61_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_61_30_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_61_30_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_63_21_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_63_21_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  SR(0) <= \^sr\(0);
  mcode_cmp_en_net <= \^mcode_cmp_en_net\;
  mcode_pkd_net <= \^mcode_pkd_net\;
  peak_amp_rdy_0_sn_1 <= peak_amp_rdy_0_sp_1;
cmp_en_i_18_26_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C0800080"
    )
        port map (
      I0 => state_15_23(0),
      I1 => state_15_23(1),
      I2 => q(0),
      I3 => state_15_23(2),
      I4 => \^mcode_cmp_en_net\,
      O => cmp_en_i_18_26_i_1_n_0
    );
cmp_en_i_18_26_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => cmp_en_i_18_26_i_1_n_0,
      Q => \^mcode_cmp_en_net\,
      R => '0'
    );
cmp_rst_i_17_27_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CF7F0F7F"
    )
        port map (
      I0 => state_15_23(0),
      I1 => state_15_23(1),
      I2 => q(0),
      I3 => state_15_23(2),
      I4 => \^sr\(0),
      O => cmp_rst_i_17_27_i_1_n_0
    );
cmp_rst_i_17_27_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => cmp_rst_i_17_27_i_1_n_0,
      Q => \^sr\(0),
      R => '0'
    );
\peak_amp_rdy[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^mcode_pkd_net\,
      I1 => peak_amp_rdy_0_sn_1,
      O => peak_amp_rdy(0)
    );
pkd_i_16_23_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAA000000800000"
    )
        port map (
      I0 => q(0),
      I1 => rel_55_30,
      I2 => rel_55_16,
      I3 => state_15_23(1),
      I4 => state_15_23(2),
      I5 => \^mcode_pkd_net\,
      O => pkd_i_16_23_i_1_n_0
    );
pkd_i_16_23_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => pkd_i_16_23_i_1_n_0,
      Q => \^mcode_pkd_net\,
      R => '0'
    );
rel_35_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_35_16_carry_n_0,
      CO(2) => rel_35_16_carry_n_1,
      CO(1) => rel_35_16_carry_n_2,
      CO(0) => rel_35_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_35_16_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_35_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_35_16_carry__0_1\(3 downto 0)
    );
\rel_35_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_35_16_carry_n_0,
      CO(3) => rel_35_16,
      CO(2) => \rel_35_16_carry__0_n_1\,
      CO(1) => \rel_35_16_carry__0_n_2\,
      CO(0) => \rel_35_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_15_23[1]_i_2_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_35_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_15_23[1]_i_2_1\(3 downto 0)
    );
rel_47_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_47_16_carry_n_0,
      CO(2) => rel_47_16_carry_n_1,
      CO(1) => rel_47_16_carry_n_2,
      CO(0) => rel_47_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_47_16_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_47_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_47_16_carry__0_1\(3 downto 0)
    );
\rel_47_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_47_16_carry_n_0,
      CO(3) => rel_47_16,
      CO(2) => \rel_47_16_carry__0_n_1\,
      CO(1) => \rel_47_16_carry__0_n_2\,
      CO(0) => \rel_47_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_15_23[2]_i_2_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_47_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_15_23[2]_i_2_1\(3 downto 0)
    );
rel_55_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_55_16_carry_n_0,
      CO(2) => rel_55_16_carry_n_1,
      CO(1) => rel_55_16_carry_n_2,
      CO(0) => rel_55_16_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => \rel_55_16_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_55_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_55_16_carry__0_1\(3 downto 0)
    );
\rel_55_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_55_16_carry_n_0,
      CO(3) => rel_55_16,
      CO(2) => \rel_55_16_carry__0_n_1\,
      CO(1) => \rel_55_16_carry__0_n_2\,
      CO(0) => \rel_55_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => pkd_i_16_23_reg_2(3 downto 0),
      O(3 downto 0) => \NLW_rel_55_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => pkd_i_16_23_reg_3(3 downto 0)
    );
rel_55_30_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_55_30_carry_n_0,
      CO(2) => rel_55_30_carry_n_1,
      CO(1) => rel_55_30_carry_n_2,
      CO(0) => rel_55_30_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => \rel_55_30_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_55_30_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_55_30_carry__0_1\(3 downto 0)
    );
\rel_55_30_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_55_30_carry_n_0,
      CO(3) => rel_55_30,
      CO(2) => \rel_55_30_carry__0_n_1\,
      CO(1) => \rel_55_30_carry__0_n_2\,
      CO(0) => \rel_55_30_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => pkd_i_16_23_reg_0(3 downto 0),
      O(3 downto 0) => \NLW_rel_55_30_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => pkd_i_16_23_reg_1(3 downto 0)
    );
rel_61_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_61_16_carry_n_0,
      CO(2) => rel_61_16_carry_n_1,
      CO(1) => rel_61_16_carry_n_2,
      CO(0) => rel_61_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_61_16_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_61_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_61_16_carry__0_1\(3 downto 0)
    );
\rel_61_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_61_16_carry_n_0,
      CO(3) => rel_61_16,
      CO(2) => \rel_61_16_carry__0_n_1\,
      CO(1) => \rel_61_16_carry__0_n_2\,
      CO(0) => \rel_61_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_15_23[0]_i_3_2\(3 downto 0),
      O(3 downto 0) => \NLW_rel_61_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_15_23[0]_i_3_3\(3 downto 0)
    );
rel_61_30_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_61_30_carry_n_0,
      CO(2) => rel_61_30_carry_n_1,
      CO(1) => rel_61_30_carry_n_2,
      CO(0) => rel_61_30_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_61_30_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_61_30_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_61_30_carry__0_1\(3 downto 0)
    );
\rel_61_30_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_61_30_carry_n_0,
      CO(3) => rel_61_30,
      CO(2) => \rel_61_30_carry__0_n_1\,
      CO(1) => \rel_61_30_carry__0_n_2\,
      CO(0) => \rel_61_30_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_15_23[0]_i_3_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_61_30_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_15_23[0]_i_3_1\(3 downto 0)
    );
rel_63_21_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_63_21_carry_n_0,
      CO(2) => rel_63_21_carry_n_1,
      CO(1) => rel_63_21_carry_n_2,
      CO(0) => rel_63_21_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_63_21_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_63_21_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_63_21_carry__0_1\(3 downto 0)
    );
\rel_63_21_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_63_21_carry_n_0,
      CO(3) => rel_63_21,
      CO(2) => \rel_63_21_carry__0_n_1\,
      CO(1) => \rel_63_21_carry__0_n_2\,
      CO(0) => \rel_63_21_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => \NLW_rel_63_21_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\state_15_23[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FFF7FFF"
    )
        port map (
      I0 => rel_47_16,
      I1 => state_15_23(1),
      I2 => state_15_23(0),
      I3 => q(0),
      I4 => rel_35_16,
      O => \state_15_23[0]_i_2_n_0\
    );
\state_15_23[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7333777777777777"
    )
        port map (
      I0 => state_15_23(1),
      I1 => q(0),
      I2 => rel_61_16,
      I3 => rel_61_30,
      I4 => rel_63_21,
      I5 => state_15_23(0),
      O => \state_15_23[0]_i_3_n_0\
    );
\state_15_23[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"70C040C0"
    )
        port map (
      I0 => rel_47_16,
      I1 => state_15_23(1),
      I2 => q(0),
      I3 => state_15_23(0),
      I4 => rel_35_16,
      O => \state_15_23[1]_i_2_n_0\
    );
\state_15_23[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000008880000"
    )
        port map (
      I0 => state_15_23(0),
      I1 => rel_63_21,
      I2 => rel_61_16,
      I3 => rel_61_30,
      I4 => q(0),
      I5 => state_15_23(1),
      O => \state_15_23[1]_i_3_n_0\
    );
\state_15_23[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => rel_47_16,
      I1 => state_15_23(0),
      I2 => q(0),
      I3 => state_15_23(1),
      O => \state_15_23[2]_i_2_n_0\
    );
\state_15_23[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000022222AAA"
    )
        port map (
      I0 => q(0),
      I1 => state_15_23(0),
      I2 => rel_61_30,
      I3 => rel_61_16,
      I4 => rel_63_21,
      I5 => state_15_23(1),
      O => \state_15_23[2]_i_3_n_0\
    );
\state_15_23_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => state_join_28_5(0),
      Q => state_15_23(0),
      R => '0'
    );
\state_15_23_reg[0]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \state_15_23[0]_i_2_n_0\,
      I1 => \state_15_23[0]_i_3_n_0\,
      O => state_join_28_5(0),
      S => state_15_23(2)
    );
\state_15_23_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => state_join_28_5(1),
      Q => state_15_23(1),
      R => '0'
    );
\state_15_23_reg[1]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \state_15_23[1]_i_2_n_0\,
      I1 => \state_15_23[1]_i_3_n_0\,
      O => state_join_28_5(1),
      S => state_15_23(2)
    );
\state_15_23_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => state_join_28_5(2),
      Q => state_15_23(2),
      R => '0'
    );
\state_15_23_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \state_15_23[2]_i_2_n_0\,
      I1 => \state_15_23[2]_i_3_n_0\,
      O => state_join_28_5(2),
      S => state_15_23(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_3828661dd3 is
  port (
    LOAD : out STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    \op_mem_37_22_reg[0]_0\ : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 8 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_3828661dd3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_3828661dd3 is
  signal \op_mem_37_22[0]_i_3_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_4_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_5_n_0\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal result_12_3_rel : STD_LOGIC;
  signal \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\op_mem_37_22[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \op_mem_37_22_reg[0]_0\(6),
      I1 => Q(6),
      I2 => Q(8),
      I3 => \op_mem_37_22_reg[0]_0\(8),
      I4 => Q(7),
      I5 => \op_mem_37_22_reg[0]_0\(7),
      O => \op_mem_37_22[0]_i_3_n_0\
    );
\op_mem_37_22[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \op_mem_37_22_reg[0]_0\(3),
      I1 => Q(3),
      I2 => Q(5),
      I3 => \op_mem_37_22_reg[0]_0\(5),
      I4 => Q(4),
      I5 => \op_mem_37_22_reg[0]_0\(4),
      O => \op_mem_37_22[0]_i_4_n_0\
    );
\op_mem_37_22[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \op_mem_37_22_reg[0]_0\(0),
      I1 => Q(0),
      I2 => Q(2),
      I3 => \op_mem_37_22_reg[0]_0\(2),
      I4 => Q(1),
      I5 => \op_mem_37_22_reg[0]_0\(1),
      O => \op_mem_37_22[0]_i_5_n_0\
    );
\op_mem_37_22_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => result_12_3_rel,
      Q => LOAD,
      R => '0'
    );
\op_mem_37_22_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result_12_3_rel,
      CO(2) => \op_mem_37_22_reg[0]_i_1_n_1\,
      CO(1) => \op_mem_37_22_reg[0]_i_1_n_2\,
      CO(0) => \op_mem_37_22_reg[0]_i_1_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\(3 downto 0),
      S(3) => S(0),
      S(2) => \op_mem_37_22[0]_i_3_n_0\,
      S(1) => \op_mem_37_22[0]_i_4_n_0\,
      S(0) => \op_mem_37_22[0]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_11\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_11\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_11\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_11\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_12\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_12\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_12\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_12\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_13\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    r4_x_delay : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_13\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_13\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_13\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_18\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_18\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_18\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_18\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_19\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_19\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_19\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_19\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_20\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_20\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_20\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_20\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_21\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r8_en_pkd : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_21\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_21\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_21\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r8_en_pkd(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_26\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_26\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_26\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_26\ is
  signal register3_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => register3_q_net(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => register3_q_net(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => register3_q_net(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => register3_q_net(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => register3_q_net(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => register3_q_net(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => register3_q_net(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => register3_q_net(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => register3_q_net(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => register3_q_net(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => register3_q_net(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => register3_q_net(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => register3_q_net(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => register3_q_net(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => register3_q_net(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => register3_q_net(9),
      R => '0'
    );
\rel_55_30_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(14),
      I1 => register3_q_net(14),
      I2 => x(15),
      I3 => register3_q_net(15),
      O => \x[14]\(3)
    );
\rel_55_30_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(12),
      I1 => register3_q_net(12),
      I2 => register3_q_net(13),
      I3 => x(13),
      O => \x[14]\(2)
    );
\rel_55_30_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(10),
      I1 => register3_q_net(10),
      I2 => register3_q_net(11),
      I3 => x(11),
      O => \x[14]\(1)
    );
\rel_55_30_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(8),
      I1 => register3_q_net(8),
      I2 => register3_q_net(9),
      I3 => x(9),
      O => \x[14]\(0)
    );
\rel_55_30_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(14),
      I1 => x(15),
      I2 => register3_q_net(15),
      I3 => x(14),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3)
    );
\rel_55_30_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(12),
      I1 => x(13),
      I2 => register3_q_net(13),
      I3 => x(12),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(2)
    );
\rel_55_30_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(10),
      I1 => x(11),
      I2 => register3_q_net(11),
      I3 => x(10),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(1)
    );
\rel_55_30_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(8),
      I1 => x(9),
      I2 => register3_q_net(9),
      I3 => x(8),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(0)
    );
rel_55_30_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(6),
      I1 => register3_q_net(6),
      I2 => register3_q_net(7),
      I3 => x(7),
      O => DI(3)
    );
rel_55_30_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(4),
      I1 => register3_q_net(4),
      I2 => register3_q_net(5),
      I3 => x(5),
      O => DI(2)
    );
rel_55_30_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(2),
      I1 => register3_q_net(2),
      I2 => register3_q_net(3),
      I3 => x(3),
      O => DI(1)
    );
rel_55_30_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(0),
      I1 => register3_q_net(0),
      I2 => register3_q_net(1),
      I3 => x(1),
      O => DI(0)
    );
rel_55_30_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(6),
      I1 => x(7),
      I2 => register3_q_net(7),
      I3 => x(6),
      O => S(3)
    );
rel_55_30_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(4),
      I1 => x(5),
      I2 => register3_q_net(5),
      I3 => x(4),
      O => S(2)
    );
rel_55_30_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(2),
      I1 => x(3),
      I2 => register3_q_net(3),
      I3 => x(2),
      O => S(1)
    );
rel_55_30_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(0),
      I1 => x(1),
      I2 => register3_q_net(1),
      I3 => x(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_27\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_27\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_27\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_27\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_28\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_28\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_28\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_28\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_29\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r7_x_max : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_29\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_29\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_29\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_34\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_34\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_34\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_34\ is
  signal register2_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => register2_q_net(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => register2_q_net(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => register2_q_net(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => register2_q_net(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => register2_q_net(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => register2_q_net(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => register2_q_net(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => register2_q_net(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => register2_q_net(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => register2_q_net(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => register2_q_net(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => register2_q_net(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => register2_q_net(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => register2_q_net(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => register2_q_net(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => register2_q_net(9),
      R => '0'
    );
\rel_55_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(14),
      I1 => x(14),
      I2 => register2_q_net(15),
      I3 => x(15),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(3)
    );
\rel_55_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(12),
      I1 => x(12),
      I2 => x(13),
      I3 => register2_q_net(13),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(2)
    );
\rel_55_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(10),
      I1 => x(10),
      I2 => x(11),
      I3 => register2_q_net(11),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(1)
    );
\rel_55_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(8),
      I1 => x(8),
      I2 => x(9),
      I3 => register2_q_net(9),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(0)
    );
\rel_55_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(14),
      I1 => x(15),
      I2 => register2_q_net(15),
      I3 => x(14),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3)
    );
\rel_55_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(12),
      I1 => x(13),
      I2 => register2_q_net(13),
      I3 => x(12),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(2)
    );
\rel_55_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(10),
      I1 => x(11),
      I2 => register2_q_net(11),
      I3 => x(10),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(1)
    );
\rel_55_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(8),
      I1 => x(9),
      I2 => register2_q_net(9),
      I3 => x(8),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(0)
    );
rel_55_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(6),
      I1 => x(6),
      I2 => x(7),
      I3 => register2_q_net(7),
      O => DI(3)
    );
rel_55_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(4),
      I1 => x(4),
      I2 => x(5),
      I3 => register2_q_net(5),
      O => DI(2)
    );
rel_55_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(2),
      I1 => x(2),
      I2 => x(3),
      I3 => register2_q_net(3),
      O => DI(1)
    );
rel_55_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(0),
      I1 => x(0),
      I2 => x(1),
      I3 => register2_q_net(1),
      O => DI(0)
    );
rel_55_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(6),
      I1 => x(7),
      I2 => register2_q_net(7),
      I3 => x(6),
      O => S(3)
    );
rel_55_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(4),
      I1 => x(5),
      I2 => register2_q_net(5),
      I3 => x(4),
      O => S(2)
    );
rel_55_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(2),
      I1 => x(3),
      I2 => register2_q_net(3),
      I3 => x(2),
      O => S(1)
    );
rel_55_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(0),
      I1 => x(1),
      I2 => register2_q_net(1),
      I3 => x(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_35\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_35\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_35\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_35\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_36\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_36\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_36\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_36\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_37\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r6_x_min : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_37\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_37\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_37\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_42\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_42\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_42\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_42\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(15 downto 0) <= \^o\(15 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => \^o\(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => \^o\(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_35_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(14),
      I1 => x(14),
      I2 => \^o\(15),
      I3 => x(15),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(3)
    );
\rel_35_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(12),
      I1 => x(12),
      I2 => x(13),
      I3 => \^o\(13),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(2)
    );
\rel_35_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(10),
      I1 => x(10),
      I2 => x(11),
      I3 => \^o\(11),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(1)
    );
\rel_35_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(8),
      I1 => x(8),
      I2 => x(9),
      I3 => \^o\(9),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(0)
    );
\rel_35_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(14),
      I1 => x(15),
      I2 => \^o\(15),
      I3 => x(14),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3)
    );
\rel_35_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(12),
      I1 => x(13),
      I2 => \^o\(13),
      I3 => x(12),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(2)
    );
\rel_35_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(10),
      I1 => x(11),
      I2 => \^o\(11),
      I3 => x(10),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(1)
    );
\rel_35_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(8),
      I1 => x(9),
      I2 => \^o\(9),
      I3 => x(8),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(0)
    );
rel_35_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(6),
      I1 => x(6),
      I2 => x(7),
      I3 => \^o\(7),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_0\(3)
    );
rel_35_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(4),
      I1 => x(4),
      I2 => x(5),
      I3 => \^o\(5),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_0\(2)
    );
rel_35_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(2),
      I1 => x(2),
      I2 => x(3),
      I3 => \^o\(3),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_0\(1)
    );
rel_35_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(0),
      I1 => x(0),
      I2 => x(1),
      I3 => \^o\(1),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_0\(0)
    );
rel_35_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(6),
      I1 => x(7),
      I2 => \^o\(7),
      I3 => x(6),
      O => S(3)
    );
rel_35_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(4),
      I1 => x(5),
      I2 => \^o\(5),
      I3 => x(4),
      O => S(2)
    );
rel_35_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(2),
      I1 => x(3),
      I2 => \^o\(3),
      I3 => x(2),
      O => S(1)
    );
rel_35_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(0),
      I1 => x(1),
      I2 => \^o\(1),
      I3 => x(0),
      O => S(0)
    );
\rel_61_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(14),
      I1 => \^o\(14),
      I2 => x(15),
      I3 => \^o\(15),
      O => \x[14]\(3)
    );
\rel_61_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(12),
      I1 => \^o\(12),
      I2 => \^o\(13),
      I3 => x(13),
      O => \x[14]\(2)
    );
\rel_61_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(10),
      I1 => \^o\(10),
      I2 => \^o\(11),
      I3 => x(11),
      O => \x[14]\(1)
    );
\rel_61_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(8),
      I1 => \^o\(8),
      I2 => \^o\(9),
      I3 => x(9),
      O => \x[14]\(0)
    );
\rel_61_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(14),
      I1 => x(15),
      I2 => \^o\(15),
      I3 => x(14),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_2\(3)
    );
\rel_61_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(12),
      I1 => x(13),
      I2 => \^o\(13),
      I3 => x(12),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_2\(2)
    );
\rel_61_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(10),
      I1 => x(11),
      I2 => \^o\(11),
      I3 => x(10),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_2\(1)
    );
\rel_61_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(8),
      I1 => x(9),
      I2 => \^o\(9),
      I3 => x(8),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_2\(0)
    );
rel_61_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(6),
      I1 => \^o\(6),
      I2 => \^o\(7),
      I3 => x(7),
      O => DI(3)
    );
rel_61_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(4),
      I1 => \^o\(4),
      I2 => \^o\(5),
      I3 => x(5),
      O => DI(2)
    );
rel_61_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(2),
      I1 => \^o\(2),
      I2 => \^o\(3),
      I3 => x(3),
      O => DI(1)
    );
rel_61_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(0),
      I1 => \^o\(0),
      I2 => \^o\(1),
      I3 => x(1),
      O => DI(0)
    );
rel_61_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(6),
      I1 => x(7),
      I2 => \^o\(7),
      I3 => x(6),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_1\(3)
    );
rel_61_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(4),
      I1 => x(5),
      I2 => \^o\(5),
      I3 => x(4),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_1\(2)
    );
rel_61_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(2),
      I1 => x(3),
      I2 => \^o\(3),
      I3 => x(2),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_1\(1)
    );
rel_61_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(0),
      I1 => x(1),
      I2 => \^o\(1),
      I3 => x(0),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_1\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_43\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_43\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_43\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_43\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_x_noise : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e is
  port (
    D : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \^d\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e is
  signal convert4_dout_net : STD_LOGIC_VECTOR ( 13 to 13 );
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \reg_array[0].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \reg_array[0].fde_used.u2\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \reg_array[0].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[10].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[10].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[10].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[11].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[11].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[11].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[12].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[12].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[12].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[13].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[13].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[13].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[1].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[1].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[1].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[2].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[2].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[2].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[3].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[3].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[3].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[4].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[4].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[4].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[5].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[5].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[5].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[6].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[6].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[6].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[7].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[7].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[7].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[8].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[8].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[8].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[9].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[9].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[9].fde_used.u2\ : label is "TRUE";
begin
\latency_pipe_5_26[0][13]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => convert4_dout_net(13),
      O => D(13)
    );
\reg_array[0].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(0),
      Q => D(0),
      R => '0'
    );
\reg_array[10].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(10),
      Q => D(10),
      R => '0'
    );
\reg_array[11].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(11),
      Q => D(11),
      R => '0'
    );
\reg_array[12].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(12),
      Q => D(12),
      R => '0'
    );
\reg_array[13].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(13),
      Q => convert4_dout_net(13),
      R => '0'
    );
\reg_array[1].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(1),
      Q => D(1),
      R => '0'
    );
\reg_array[2].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(2),
      Q => D(2),
      R => '0'
    );
\reg_array[3].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(3),
      Q => D(3),
      R => '0'
    );
\reg_array[4].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(4),
      Q => D(4),
      R => '0'
    );
\reg_array[5].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(5),
      Q => D(5),
      R => '0'
    );
\reg_array[6].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(6),
      Q => D(6),
      R => '0'
    );
\reg_array[7].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(7),
      Q => D(7),
      R => '0'
    );
\reg_array[8].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(8),
      Q => D(8),
      R => '0'
    );
\reg_array[9].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(9),
      Q => D(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init is
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_32\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_33\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 4) => addra(9 downto 0),
      ADDRARDADDR(3 downto 0) => B"0000",
      ADDRBWRADDR(13 downto 0) => B"00000000000000",
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DIADI(15 downto 0) => dina(15 downto 0),
      DIBDI(15 downto 0) => B"0000000000000000",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 0) => douta(15 downto 0),
      DOBDO(15 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\(15 downto 0),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_32\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_33\,
      DOPBDOP(1 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
klQ5h9hgPT4p1YBkLUYG8lguTKRhX0z2xODTfvC0iZ4ANS2ufXQ4XxirL31R7wyoNNR1hd7rMjwq
bhFP3bRYRg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
m0Wn8/fGtoK5/3nNxNBBvNhfylkjw8jAoTERkIzypaD+FUTSQmf52TpAbVY1LPDo/g6d8tzIrR8n
vZ2G5ckAbcwDRGFF5wpZ/kzyANCTceWo7S+WbzLbzhWkG5fkJbJk/tafbSvqruxodmgtrUI2IZDv
n7Mrf4GRIdC1iTgv+oM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mC2bSkADVOO0t7I1BTHPnOuMcPIdeypycvBkfsUtXFD/WSov2Pt6qF5xMTcs6woq+465yEDvey00
mpX9JC1ypEuHaZI+dtH7s+A4vYLXy34kFLryTKJtqycAy6XqNtTegvy4giaDFwDn7LgsamHSf/4D
cVGL06Ul6gm+ZOMvzok6Y9TBltlEkZOZ4GncKspHPcn3EJar4GdDxKcPll1RqSFbA2m7bWz6jPV3
uOD6IWnIflyobdLIsuB8hse3kjXBG5sl6X8AP4armpo9TNNnRIRG2FoI6g16Sj6zldS9XF54ge/w
jnsP0vce7kfzvP1LjtFrWdW0raW61dIddZVz5A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
LC8srWyP8+H+elFwWxNw8UhEF94Tx3vV4OlwBcwFszd9xuvwYRipLEwY4ar7HEoCwJI7beW6k6I1
ntJSXGEnPMgIDe7PadSQIz6gi9ErRHWRhWD6bEDNd/ZjFQuOakXk78jzxOTKuOpQ+ExjdHImyPzK
mDR0BnkVc/QiSO7zpOf3yjx9Vzb0Fba6OgXg0iLPz1MUr0ahYIc0MAkw4pRBo6IH5ilqVbkgNQcT
G4z/0OSzsBxipE9o1elAez7CnehtEQ66g6i3JpIIwqwnpZNQ9otSnS8LN/spRcQLOUuBq9ynGkPL
sF7EUF1gLkTFFMGgnZgawWi0V0jhiHQkJuLKPQ==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ijV0yStg7uRIl3uzK8/hlbIaWGHa9aPC5Eu/o1vErrwtArYsGFt3RCyG/S90FB6jkuLgqwPR8ZlQ
P9t/F2FWmEkwwjGbdrRKFfpbkjh5HVn0vvLKCP3SiVHXCOWxxb5z8BV+yCNdpgdnsHFecK1M8ydQ
C530kRu3UD1LcnZcWJi41LcJAc5rvlw/SP1gbl+I1qsRNEHsb+MK5vyjgwBZAqKyqi7/UK1VEPdq
myeWeCRrU0GqEq5y/PHBMknv1SqNe0d5qzG1rmAtC4df+iivMCc9xuHsCA7iqoe+ZKnMmnA/8F6+
nY+gx8AQNplCeFxWppKH952fIYplHtE2rKQyVQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
saHlQAxOuJkLItpv5N3fJoJP3EdR34QNYHtUBFx7fcQy8HAUaZ/9Tlt2kfpn9r/pk1MmJ7uf4z73
LyaG/PWw7v3yH/KIVSLeIxiaSHoVfdWH1/RDH6k3DTDNbWSJDOjGSvMTThUKe899F8IXTD5jKxCh
frGB3Io0W4klQV7ADEw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RFiy0rrSiVrZMC725S3XkUSDuMS7Z+xog1sOwADnIBmdCChtYLUQVO9oPjXIujfg3bBFTH3qzII3
Y63fFMSZnnzk+ups3RPMBqEPPdMQSwlpDzsvhCOwYHe/rsOPZvqd7lL6QOKoA3mS9TZIP+mOaTKd
vzQiVyfS0rs+QHdJcw49jD5y7Dc3clQHD9xZMAMUHa5v8hzX4IOcnkSUOLpsZptR/WM7rcMnTzCL
x6m2UC+xSrwrb5vEz9cePMHx9NJyO2DBG0HTEDzGQQkrOCJJjvBxtB3r/E7/3eWGHvMwphSJnfCd
PfvYXz88X+ZVCW9dTNfc75jI7ogVMSunmtUuMw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FPan88B99xr55m27F39AaewnsN/TmxJHSLBzhaXJ/wXHLeDe36Ee66nu4bB44TQWI05BQdNc5q+b
wggmw8Z0KFIH8XHa5tsekQb0ndUIwEckpnTuhS6beWC5+vXIGMlPjvesHyXo61Ks6asXZOJiOF6I
MSzY5b+ec3BQ4VoYYAS2kcN+0JC40iZ5iETYN8/Z4v5G3gV5bt5LZpETWRTb24kEFkSrOnZqxaGH
WCfHSQTpf3TLN5bb8uDkDvMoXUzo0++UbKhdPWb/dqWQF2V3u6BXpN8j58KpYTn4qcYNaq1/zx/m
iNHy+ITWhwFVwrJMVmLRhRyF+vmPhGUmwrWDgA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
DU1mqZT6snDGsJmtHIgkv5uyzvHZ4eHhckTs/sTHLymwkxEAOkR33Uh9rYYrVwfBUigg4Y/ANPiP
4/NIl0mZcOtjnoECYxqWyJRh2HbD1pYFw4m01sHCrjpfZlespt8ug3XaRd4Dx3r/00YAnCaWQwaC
In6dFtOSRQYiJJ09LPV6OYlUtEbAQ7yCsAP19VvxEm7GBq4syKewWO+7MVppI+1zAkUit5JGKvKg
WdpNBDFy9jqmF6lq9UZlGicBibPMA4LFK0bfUKZ3n1ugtP6x8yyugi9kKw8gxzXQJfhsCPllvBQu
b7M15Kh8X5aK6LqQVMouRIsQ7suoL1wB0i9hAw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11264)
`protect data_block
hfAu9OTuR8Z3jJTlDApREX+hROz0XBOa3xQ+KjDjbBUFl6nLv7XfC3D6AND/rDsWFuoe9mvSsRMr
DgzuLKo7UDJGoF6cZr04THiCBPC4tYdhTzm/yjFJkPwL3oFxXsSu0F9lYJp2n7S8BTnz7tD9Zfnk
xxQFPC+heOEk6lfYTg5MNKyFdWwxdFK35unQeSIiX13FYrOxmt7CAMGiACHMUhXLUPCrDmQeLerP
khw5jLz8/DK9mS4X1lNg0jb3Ze1+mGcvbRtNETtrT17jJust+Kop3kr3oTCT6aGbeijWYOOAKbE8
U5Q+Op6c2VqfFe25+taybkKPIaer1sdea2CeKvqextEbdcJJt2c4B7XUkDdAY+59cZuImuG1gjU0
4rO5fJJwMJBeLbtJ2CK7CoShwB4YKCDziJm227irjClEvUDPDfG5GTnpW71Jh7xNyug3tNAFmNOB
Tr52gDsa/TPElk4RTMa/uqvzWAW2kXqluPTNnk4v6g6KUQvbNirJq/gC3+/ikeqShrgUZeOGwui+
Js0xLH+72CjfU2dbveJv+dx9OLACb1chqtPNhzSJuU7LUaTABbNe23Rw4CF/ACf/tXl33AZOpa/a
ZndymNsYqH/Hx3+F1iAprOGBWDF5K3YFFOzX4yrJCnE4hRXputEKxnw+Jsd7DI17msqQhJRU0nph
pUl8TZdSCYBBu2BG2RCantQ340nXCPoOx1p6wBVZUFFIaDW6WWHQIbwuRM+nMdU3zJLiIETbjQgq
k3EWt9OXo/fgrpV3vQO7hW2pSynFXo3UKXpeXFTTDXpaNS7G0w++F7YLNKjdRpAN44XjJRkDF6Ek
vseDjT4ViBalkd29L0UXMKtZG+5fMaNrV3ZKjdgKrK37IKa/EKDWZOGdUFoHBzORZ3A1s+AchIjv
TF+iXvvKRa/SxXpZdGiVuqHZhspfH6w0x8BXtBvW/eUVSVvgmrUvDBJYROUfZb5+8rzpnojns9Bc
Pc3KSSCl6/w0zLXsp0NF09Oel/nXmoNC+gwmzadxv9R70Jw6X0u+y5KgfVIy5daFgt6DbQZvkCHh
dK4wXcGue/RaK8mEQrw2XdZVLreHjUrKuVabL99fCpcVYExPTm8ME5abNzDsHxtw82s1IBogbepk
G+QOcresUuBOB6jDRUOpp1/uGB/77vzyCf+YtY/pvoa24wVOlue+PTqzvPHRyrYlAnkpMv8IJ0pS
Kzo4AtdE0KP8N/g+VpZsiRs1mwPhy+tSKg6yGPGk9mkRGofRDd+ftv7w2U2ieExccmY/8Z90a/gC
6UibTfY2W2Sn/k21RBgQkgr0Z7lCgh69XomMKCu7twuWYFYhfFNFg72x65u982OOy/kGy7UvVAM8
Oztk8MZnJ2fOYDf2lnXHiYL//PmdbE5qf+yE3f1+HRwZ4xXvJJvdEQqTvw4IVKID5BymeqLnSb4A
wD2z67SQDOvIjtlHNvp5j+o4++Mn/hFD0u29YjZrP3jKdF3a1c6KlEC/hg4DdjmKIvXmadooaKdT
kIi/TTkoOS7kctJoIBI9d9lOBQorJEfrtKQSmHvrKXN7wMLRGT23ptSbBsgUXtr2VD5C3kRQNV7B
ZkhiVmwtjXPtMMJYM7/xgSJ5jp3nAsQbpnQ3nyFpHRRpqtEEBUuyp/Rx7nfaFExE0cD5QhbY457y
mNAYM4WskJ3MbYiY+XZkKkPJH+5sqLb8ydFNNM70oSFIBsWL1C0SmPVHjfAegQpbEns+4DDnkama
GZjpIxNIQEXrAxzGWP1VDL6p9O8NJihCSZSniaM/ng2Rv/nCssXAFa7OPi/+qaxI5nz8T+8rMX5a
NJ6FmkXdftYf23gjXMs3RciFltQ6pkakVza2RuOz0i4mlI9Rsoh1NqNDPQBCzbWfH6LGg4vxFksT
d9LL5+6FGLHEmAe9A+pH1fHjczIwxT0SkRfJ2PnWib8XM/o7KWbCGPiQ3O0WkwmgvuWMdPsVVmaP
yXKYv9G1YKuxUZ2UN90A0j/eTgqKArTYyKKjWKDy65QJ+VfctCwuABY9BvJ93IG/lWnW7UaI7IOj
jw8IzyY96msAA55V3Z4JFHfFUegQngT3nj8HKCyP0xAMD+y0D58NMqK6Plw0hEIS7GjzUTE/g6CQ
dB87/L/atsZ2M/OW5E3WnGciY4RVVH4miAdryg4hAn+VRMbi8xFS7LVg7ZEn6lGg17PhdKQ/rlBt
nA27655BVojYoqHYMrDKHpBQn26Av0BQR3tiolv6oWiWpQyK+wJo7rM6rmZqvg7f6rq+gf6UN9bl
kyPIDcQbpw2EQzr8kVi3i1uCjuezuZjZ9WWCROfV9mBB7cz5lpAiB24YBY/79gXNA5CEM8kjpeVN
p3sh1xsdHp3XKMr1zrSBF3x5C4Y9sp3t4t208Y9cCjdxBKCySNxIG7h0m9KaCIUHjvYVYBVle0fF
m6Kt5GZD8PHl1i+a6s5SCCguZ9zdYN8jAPbkyrC6cVyJsbIX1KV9i2xgvBzN+ercdUJw0B1kvLqX
odXwtjPYumZex1sjd9dvDAvS+e/4ouoLUdd0PvWobFLNi8ibN7kQMab6knlnz6Qf0cFg6HP8QQh9
CvlwCbwu6jHnEKbToqVRFf2BcdTXlhdJgetLkAVF410XKFfqlujfZt3ILgqVv4a4bDYMcgvBxocQ
ntp9Xwp59HRMPN6fBwdSUoUJnkllfFhWgIUFWohWJWqlhiHk6/o5woPUSHs+m5j4fQADzUuBo2Pp
esnOAz8sRyhRNOoa2+GqzMukOzGMizqJ5UFZm3RWCYAUsRhGLNFzqaIzR363TTzuA8wW/2AZi5f5
08ggUOyEUi0MVXzUyc4TsmlSrHvaHTrjzFP0YkWRERMhZ45rOa6UC7yOp6zLY43LIPwDxGaIbH7F
XAgQlBiIXUHQuCqfHhGHV9klcCrsqh7WyRq8KZwVHATiNprS6wRmXa0d/WAfvXdypP31lJytTqX0
0s3NNyu46YnhCmZzPtNrS6JROJ15qJXdkr5H3K4glx0RuwmgmiF2+MeHhdVaaxc6NCG7PVFQT+wM
Rmvm+ilbWFrB8a1Te5sKU0p2kEkmASr2T8Pt76WYkj+VfWkMVeY+juNZU07W5iPf4PEUi09sBNxH
s2cccIrI2flQwXVPs5+s9HMb/Fg8R2L5y6t5KjMf60k49tcOj4cctgRcAvj1NvelD5HIhQvagNkj
BpLkpH3I0rdIlmG+LhFu3/ELy+sLenQkxdKFOaOotk6dNIm+BPNiTs6f/CXP1uOfZBZZgzV9G/8k
m9HlJVwEXqUvDwxEq7wlLHEhbjRmYofOVDtuS1Hrh2QeAAQtPOpSMFctv8Oix6OftxvtTYnq4RkJ
aK6sBP6YWC/x/C0OpyS6GeUfNgTZWhy+fW9eKVfT4KnLNG4HmaPixLaRfCaPakaQnM0BRAWE80BP
rwAsYd7M2e68NAjNDw2zX9HogMplT2PcHcThXQncSKofJ5eM2hlQM/PQhvdf5fRfPILq9v3cX7In
KRCRIKzfruPYhyvpXOk5p0UMHisVOy+EsxE2zBn27VvsL9/RFXo1XpnsmQ6HQkFs950JgcQNyhg7
GONZQusvZXiqbciY6fvhEvFRAuRFJjHBK51JfGs97fp1QrE5UGdSgIDnJLXBBpZTrUX+JI9r7gs5
TcgCgJzQuOigPAC46dLrbLOfMnbzIgwxz/uEoWuiir3zUQtfNAI3yxRCYQ7TBnAIMAs8U5wk9teA
Uu/7fBh7WxSA81r1Fyuyfjh+vCHOQ+Lx6MNAN8mFa/INJNxD2bg/XzdgpfhBmBGiyKLVW9cZNaA6
jC9QZWLviOY3nn1XYHBL3pxuGtFLTLlBv1xeyoPgxh8hj8mxw/9Q7to6y/O/2+ERg1DIe7Jr/wVs
8//BH2yn1CGG6+tMc4z5Q8e8F35V70kKJ0pGe9gtlw29s9FUZRp3qW4AEjnrgTLPt+RZNEKX1NBv
MYMjWhMIieQAtwPxgYdzS6AarVfkEpKSXWL5H3f++whoMIziFlY+0MEPbLbqQDdKzdlIrL5BZdhu
TOrH1VOl37/vpBJ9Nlx4ocgKJ9wnRBLXk4dOS1x6m28F+WZUB54X1YDVIHPCUm8qQZcr3rTHMctG
wYeZVziuTgdIiaX7jSlvuuVa+theHOqP06YghCtM2NwM80a0GLOh1r/T3Tnjar0FD5yWBe++RoyD
LSWt19EGk/1+r8Qz9BaNX5Ds8Br4WnP2cGHKjfQs0Tr76fUZMahf3yLhawZx1ZnjossZQUNgq6cJ
5qj3pTtQG8Q524pYrWENXbTmKGKU7TmHMUYqnFZhx+3OKC+9B2MbI1pXT2daEbyfbZ+kw7QmLjSb
XC5/omQWXZQ6u7gPg/twR1dedLxm8+FJ+udO4c39Rfi9Y/lVIfP/FJvSGfWcLEH4J0Ci6aXzE+Ux
MuuvfJRqd+HwcgLfYeR0vn9P1ghIrxBQexhF4bAudapyprO2Q5u+9cGwz/4qt+I62yZoYj0I9ywb
Gcj8xgFuRGN2VOlSnJcKyk0UxKJBrlSzYJMeVVGNzT+jX030z4cLNDXLzGSr/tM8kbsdi6bt08DB
0XQvIhoJdOvp1Opej3uggn3C8SNXW2hECRm67yLIedn/LeK6nGL666PbP/uakDAn6Lmb1OA14na9
mNT9+EFFZgfYtHXBlchJJm0ovg27UpssybPVjaCo+c6wHEhaDx1mV9/BiNlwV7MJuvyulm1AJG2l
Qrhibc3qWfgxwfpX/4SSuMSd3tRyRsBA+MhOKVxRNRNymUqilBCg2XDGRDMAISNN7ZFSm0ewels0
xLDvoXJoVwhjKAzt+DevKhllXidcwKiptr+5NKXO+nqIZ1wFttxbv4UhAEkNxqB3kkOicPUPRksk
T5H06p90XB3IWVvud2/QJgSTFdbNjZRd+FU8iTCUEtRRT880Jq0Tmx7HHhrBHBTw/790ANyEhT/R
ZDrg48BNp1Msvf96uNVZOnXFTMM6Dju9VPuZKq1uCC27qhwQasedZ6QoVgc5grb1kZ4EAY67nCt0
BbfvAnnPdb6mXJqDihe43XNbaxIY4i3K1joU6nqZKVDH5J0qyobUq02TpnM0eC5saxH+CSWj/ubo
M9tMARD8AxJo0Is3Q5ntPSwAS5hSnDiz6NmX4RJU2O+w9dErT4/67/d7OZkZx8PSYAIy9YwOwZ5M
iPcvClZbxH4/dLbN16zTETZRzXDqQVcM3cVQiMTbTH2hRI+Bv4hBO7e9QbbVPxecJbSr6Pvhvq6P
jPRK+gyhvJSijR/rbMQI/YXD2x7c6Xwx3M3zmu85BDtXWeErY2Tq+TiejhnM2SKExoTe+/2gm8tH
kSXqOGsmPLXSstW1VIi5/HZI8OrBXmd8J2wN/EE66G78vx06NdXnbEPQ5sXucNfMxD3UBNsSjhNf
dmzJCuUDpTbGMF9jixCZa+lrbjn/j8EfjrVmTnoCGaZmLNo+AxpielkFTEvQA0H7uP1AaO0n+Q/e
Hs7omfcGA+gnTiGeOuPfUyMJGJH+YL389//fET4xFRD9TjliTMj20CVltjhnh7DmRyZHhnEc58LQ
3DVFddHAdh143spiTsqDE79wHjNFtXNwgn9ldqW5bTtdkD6sM43y0yihu/rsypChpuz8uBiMhsE9
ql8Kw/rRW44aMZg0PPBYTtdm0SRbqshEyzR1+At71w652zEeKJ1i1FmpZ44h3LVc2fKdJ81A7Dl0
jl0EKy5QC7QP1r/1Hmf4RfdDdP5AZStHSCOF6IT1BnISVyIsP80nq5vUmtrK1OPHLypcYJLqjMuK
YPsB4Ca8I5bbaVopxtaOoG7n0O960/nDLqMNmwuahvLZEokiLLkxIcS+Vd+tLLX+34ptnTJjJR3G
wTlpKD3P+VkHkgh61MYVWCemjYN5C3rhrc40Cos7xHaKuTGgwZMxwqvRdAtCMJSKEQqDMj65kY0P
1F201CpKDklqSZMBcgp6UxsbR/yJdQvKVpJUjegkhydXXj33z36CsNdBt2Nh05qR49J5Hnc7KBML
uJbX2Olf/3wjmyhOK2vsXwSt9hykJWclzpEphgYdnjiLdwThgzjV0EQBm3z7BAGDffnoCFrz6kEw
dB2jkFWUs6S1qs362MfNyfexlPFohgn+i7qE6C2V4GUrrPBIlp/zRg40lLqjeyAZoOOueQHsXCGK
H4c217i9skOKE4viQ7e3UzPvYyDc3C2V6g9lTTHWM3rtTBRwpuKsqNb9zI/9mPmrWGAKKcmCirGO
Ssdyv2ZcKpiYBjWxMblXUufH80QFXH2yWJW70z/bvu9c/bmdMvvmpFldZdZMCshP1Fg619OlAlfY
E0MJXdmBfp6WsTTncNwV2jMq+aw/p5IlvWb+JyRWgGP8wli0jdgKQ8j0gn/AsEyk6VyBer7y2qCe
krccO/R6NXlY/F7gZQmqTyC2ePuoijOmVi3xQmjTdpa4tDlM+FxYNAjWaNfIQyyikxfluv6DsX2p
m/LFTiTzTJ1PCvW3E2G9O4N7Ej4gPdRQGV7Ua+zxqsw/Kxl09bI3gT0gSbNOnXRxsJjyziMiDxpS
9rxv5tyfhlmE+IZ2+92pBJ1tkvgNSuSPX+Y3HEPN8XAKbfBsYRBpLAGUeGH6flxgUVtiuDbgAQNA
Udsq5eL7loW5Z61eTCuUE072kg8A68rZ0IIH8RHYK6kTRBel5pGiE+N73f9vyJCw+ERK8f86aX83
b00crD4R0gGxRwl4X29OPLxh70k2G9N6EBgP2y5CA3qDfO/DLQMSmw6ghda3hzBhBvhlaoIImXYJ
/tbsMXpQGKyMrU14te7gNMHwq6gcU5RbNSFqUfWGwT4p/jzTLQW5eYXBySxU6SIsMPcsrUxq3k7e
+dOS64eRVifm5aGiyzUmU2izTzx69Hd49I1YqPs66Gi++A39VBPiSa5GINFRWePCIYDqwtaK22O4
YLw4kpLtI4FqoDNIpt5u2s02nlK8GKOid13ifPF3a2COCP1usm4HB0sF2fGlOaktKjD6MwC+AmWH
ImRCfMF48VzX+YUTWkhb3eHNLpMGawaN/gCYJ1OcPd5G6MdZhXueCveu5WRj2hhn8sljqnULsXtu
y+iuz/8lVPcDJWUqGBbc2NCBdLO4ao8a+xuktdGC+GYPrJn5uVkq9cam5+od/uORTzp7Pc3iaYDi
4z1wwHxoqdyeIyWN7cKmUDM/RHxt21QReyhmhYHLsdo60+mtAw4u0rj8SuX5o6JdEd1uK81xmkGc
e9ks3v7oFt5CsUDLY4rRn7tK9njcUdvmtAVuqJ4THEyOptux3hlUnnj8KHlouy87JFU2qHa+pDyI
gE5J8rbkU92DwW7QW/3dgc3TSpegmZBNedYShnMJ7P43loKbm+9Z04J15kyc7K8Ou4fdunjBpn+S
G+l9NaeEKvlz3pxzpwonNTi3Hr/1Wqggo7Nlnpwjl5LeemK6KN20d04SVbDmQlSTnO+6xcirHyEA
pNKQBnjyfwf10NOMBUSr1PW7WIVNh3I7SQRKdzhrqsLhtnbQUKPAOzga4seRbj1tOo52yNnIYLCr
Cqfk/yKsIIJ0UFuLIEPMaEE2V8zayziyvBFmyBI6i9DYRbVZEACTYF4+jUj3LX4/L8iehoZJ2qgN
GMM172p6X/g10sCJ1Kpwd/5ZsI2LuVeULeiRU2SMMhEqw1fBSmF0JGOaK9D2IdWUPpKuyCXDgnHt
5pm739XTFGK5B0zOMuS/eWVQx8KigZ1GgbrQMzs2YV0r/HfK53pCYCwAKaDsIoFZh+fxPQP/zBM7
ZX6MmgV6viyh6zrGFg5ov96ijcERr9vqn2erET9x9rI7AT5GXpR7MhVLfBAhsFWsLSzpcAZYkUIu
Q2xlzbVyQAZjg3w/ktdEeM2mbDjzHUoUPkWyW4Z0j5qriHdGmCfOpiK/0ksD2nGaYpgkT6aquNA5
4MWesiDYp6hM987sa/d7YbQuAyfH/fYiG+o80GE/Lxe3Cvu7Ekqu5yNTJqhYPFrJbcDK+/rrUTYe
jInvJh7nuP9jUZXGvZoyRxLAS5x1McwWAU3RYFZ7xlfJEshatUc+4kWKYIpgYphRb+4OG92tDuLM
47La6sxgro5KdFjttPIeA4QlURywIAWvtYTVHFZI2LOn7rzK2O9ybKBFghkCGWQvyP6+aw18UPhr
4g1ieoWef3iz/PF7J8koPUz1oa5jY6xZ4dJNQr85lOGgoJNUbA0cHOoKZiuqQccjsGy1FReZ0DSU
lmFejzrtRiEvPnDEfO7c8izej5JOsPotbohjnNe/ROYyxt5VGy53zyNaNzOHdE638/vIEVNYeMY6
uLTsmdnRxid1I6ocrdAVvSy6Qv/cSJeK5qZEz3iisneTLoicD7nKAG0kgnqmNGhZdcO0JeYzMAMS
cIWSOdJ73uZ7/1wVqEvivQ0mFnm4xwBZpuoU3J8vomPDqu/GzVb5//LB9fcWIQ17kFR4WuI4nuWc
duF9K3x8A/1009NPWS5Jr8uYRWNWYMpfGsHPgG67DdQ1sEYbmeUOS0b6kH+3jaOIdmhC1zBOi8B0
xB5vAJ7xPoJnDPDhhOkGXT5bmakmRX7l9leRtep60wKEOc5O7ClMIYW4AEN3gpdCRwFAobIYjt9+
QxGgxuYRP4+jYmU6JVV8F3B+xDvDNoF9vJ1FXYeVjetSjPJqxSXcMWs35greT6lU8kEGK5yHhf4d
2MC9gOGzdFo0YI7j9BD4JbitgTbqmpXhoi7yDoDhY5Rqkzd48mRPnVd5SHhL9Oc+SkjlCGa4KuR+
c+cm5K5HY2keyEHXe+VK2jC4Kpze1YFwyjAnoOqUsnCJXGxz6S5ocE6sMbj56VociWINYjmNNLfM
pB+sngjnb4Rfdq7qtFES2loH5tQ/AphpzuHnMWOBzTZuBic3Ow4OW651/SUYIFGk/w8/zfOw53mQ
QtGoTF2KSUiJK2HPYT6lys8W9t7DZ5wvOsQwAjzbIMx0goQWJWooDPZPY9DSrxWu/FIuFhyD+TC2
TY39LClBdYFEA5Wjw574+XSBCf9NsDDFjA4DABCxK6HQLVUGG2nHLgDv0TJCrV+HbUQky5kESwkh
JGqXCs/C2BFFy0EScAv6FBL7bIFhoiD8aucpBkpMWQ3HdxL+ossenwN/90sc+hndKZdjY++HWvDw
V+FFGhRs3NKVpyX20n3/o1SJqVYfwHObdmRn1Qm46HSUyXSyUpbPTG7cZiFTfA8/m3bzd5AmcSVb
jglSDFsnvNlLGRg82Gc8D01GdyRbPHtvttCk7PQ949wT3fX1hTkK4OFMxSoOs2UdrUZyJHLKYmqd
1uHJfEPjNmNznvmeTQusG+q/QbRTKYnYjLmu+Fp/DcxS0zE5Y7Sg6YltCByVbZgql35h1zmBUE+P
vxOucRahG5PsvKfqQG3qTpkP60kObCcJmcPFfWIrEK/HN18Fwx2A8tZnBjLboRNe++Pj1QGpYKMC
RHfaUQU8RJu/LfczCzwDAEYaOGzeMuISDUElRTkBW6LfM8kLkTbBWZSIJbJSaeCJ+WdzaL5Xr3k2
As/7UDUSneakULc4xvdqNoT6BscLy8YCZl3YBU/uF5Z4KvBq9a+jDtNAjBCPvO761ZAK6fGFVEmv
+f9TtOMyoPuulXHyJ9uMmZtoBMttZd5N0APqjHHk355ztqnLUa/Ce3btoTRadXTGx1jUtPTjk9Su
di1UEt1Dz0Wsh89ajkq/uJXlGxMTSQIJB+KhiIY+xwWwGNvUyqzxuiFw61zayqAFIjFy7TSzDkff
OGK99uCWjY1R8etWPs4M+JyCVN2aLkS57fgIrFFQBXt8iT7IuencpJtydymRQjSm0VWTN7dB8QIw
Y6sYdjDzulPNJdvWpyCybOSqjDSPETylsmMBTL6nQtxZQmfUf4m1kYVsEOKtMwF4h1IzZTi9loco
P90QOTlkqrjiE77Nev3sJCNF6pSMkiESeWy+Q0CnDu/IqfqI4cEIOlo+TK+QRx1bU6Rc3eXvY+Lq
3GkH3y0b11CgfCTm8aRSq9Jm+DI9d5r9uzi5Ec7cxVpkFbsGX0FY2vRf2CE1yL46Al0PT6eBj+A4
6uAAWmArOeqVnvRZm5MWCQafetizPl9U1T5Z8b1kdcYXntWrE9K1aW93iI2jjIj4W6HV/KI6dkvX
9IRKEYLKV1oOcp0XnKiZZwZ5qgJfR2mFiZEAyfLgcMOqJw4lKNi7SQ6XCckzIXxae2RsLhHFWTvN
8TmKqZx1PVVswbgoAxiNMAP2q5pW7VJXIfC/X4GbTjvxUyg7QEJld7HAbq4Z06soPl3rw1kROBah
1XBLYA96aFr4JXqbM01f/U0ms9okktmRa9B/4gvO1UssTzW9d69VC5oecjU1vwtn6cLnhbrzpehb
dGEHH6Ocq6XeOrW785pEPz3SRsX3/bf4QTadP51omddsAGg7do4Yok3T9y4zE6+ftxHkbCKjmG2b
1p2g/8RDenMgW173U1uaTsLxpa6i/+1qfpliDJV/cdGoUXmhwPhOb4jKVNP0PBLyj+cWiJvMTwGe
aXJWofnp92QtoEpva5Xsh2EgBiJrfnPX3g3Msy/BPCaNTjTOp5OqzqJ4iGbqJbtpo/tmAYcmqSpT
OC+yo24mOamJ0ceDxJjVpXyOsGSp2olajzdnx8B9OosxxIzQDn0v9/oSBYCWqLdY2VrYn+ghxC2a
mkknJ3INJfJO8CM/OSfl8OUWcWdLHR/YP5a2m9t5U3Mx13Ns4RtALybws4vQVrYg8hDAZzsTYEwY
4DjVE28QX6y//euM4We6l2XIZIqL1e7cJVcgSwCUpoBvpKbzLqcf+Bo53lv3pUltXAZIXJz2K8FJ
+vpuVjOvsrA4niR7PxqoGpd+k7+WPlMuxuKm0HhtrOx8vbG6JiEd00jj7rjMbVBFEz8vMaU8mT9h
WHX7/HDdPcqCXFGdEfLid7oYu7l3SmZw8iG4i9stg9iInfrsJUsYjn8hTu1kBozUsfn5SwkasJTX
d4RiH/jB6Z/q4zzG4kzYTo/75606c4X0/DG4Wj6gOjzzOXgVTmcIu2lvcMOp59e3VyhbzN7RI/9b
KQAECkiSYHx5TGnNnuh+L2O/pnAI/WwIPDgCta8g/6dd0qJXENvC1ul48ME7mN+XkvL9WH+dZvhk
VZEVnfi3pyOv4d5x/zZQ5NYrhuonRV0pOVwqZzoywCpez3iDG5CNmWZ4dcnP8G6xEWJMjo5ihJAY
37NySWLXxKjG7FMPVPnmRY6uR90qwa85kjzkMQgI5O2YhiUPlxEVUUCVk7d3fCdbu47G1IYdQmg8
sOxc/Kqkw8ZBIBv2hn0ohPS5tOobyFRnlKmqI8Np5e56pKCoC7QCensP89OcYMWWY/CCOiEMVTHV
WUEVMMhuH1xP3UCRSIw37dcP3pk/gyhTLQIQNxIGuVgpWRURWNSYf/i48Z+neY0yr9UEfUcXp55X
K115fLHa7b2HfjnoJhwNyqQmOJ8oD8Bs9TfOvq0W/JUYzu6lzcaZN6kTvOMXSj/0NyN9EVd/kqsV
tQBCbsVXs93WL0hOO4V2eP9nbf/3FoL8O3JmcIf3iM/yZ7wZoNtffuDGAXxYuozn9q7v0jM0SvXO
jrsOkLsGoMl/EjY8mcPxWaVUwbCI1j7AF0iPAfUwt8+Zy8FZjKcRWzY3bHhw/2kK93GuhYRdLE7T
2k8RZSXr0Frv9rUDGtExspQr4DJ130kQkXo59gXURzVqHZcINe68pU48L/6piHFTpX6SdpzWJ/Kd
Nh0kTX3uEnc39cgB0+XAg6qF7QAEIAT3rhbb+UXo9FSSOolM5L9RvDzmd+79yHscsHlcLt3/QFCg
nsLA4sLLepeYLh7fsDNT9Y50uAFm7bPI1tpCb72kZL8HqjxXNH2xZFcnT5CPD+3ssjIqxDfLBUeo
Q9bwfof4Y0KNcdKYxmItbOPndbwXwdTWTkH6W+lUlsktrUDBy5JF3BWeUkTs+lLXjZw6DsW/3A6M
eWfMy8E0Lo1VsbUaHHrIYDrOibmo6Q2E8nsMqoX2u90CxUB0uGyQFFcqi8U+/PAWPGg00/Gvyuh3
6HbJfmONNP5bIqD6I7ancw1157vOp9DFHRuTcT8YiQduTAnipIfp5LPe253OoHN77UV2nZp1q5UD
SnBdVbQJNiTaaHoQQS18nGbeS6dyyZWtBPYThHO6bZ36FIW+t4OJvUPzv8vfqNa6WlVy88h4pASD
QEArjHw2N2fNs+Sgxg1mZfYUvtUibq9CtplNBeij9oPxjdDhyhKcngaRubMJ7OKzDxeNG9FQEhGg
UIw7Oe7Ly+WXycqH5ycZAwh+GutvrspRsJPoFJb9r0znNNAi7Oz3CGhzJAEUBGCPTzw3rnVXa+pA
FmfwblmxCgrz96CpMWRllOtGpznExCYXVHSN2HfTmNhJc5uxJ4FoIKDQwGAPdFgputOBojOXsoBw
++PH6oH5RiDoeUhdvz6KGWQDUJBUgxhVoyIiQ1g9EG1jLHOgFbNhu/fROSd0a347SiQOqu77Wfj2
anRNdtVyjDVltyD+Y72SIB58kftMrw9DjBjatBvIGX4vdmmhDxuFRJ4j0TNIh5qhVi15OxbvtnWv
4Uu1nUWLMRxy0z59zjHEsUxIKKt/Ut+9ajnZ9NmxIYXpsQ1Rb27Ats1UHlch2WS6AgDT99qOGFaa
tCgJgSf/vSdSxM/Efsuw/G+5T9MsV0Jr00fTfOAIUwghWarUg4entMw0E2kJ0xCDb9S01Bj4d/EC
iTrOhOy06lYsJ4kQaVr6vJQIa+je+FW4Xy6nhGzq8etHoP8/qgym99/tdqZcpmcEGPacUCgnLOvj
B4GAycGAm8ckUf7OcvxEoGE/0j1ZvfhJy4VkD6Ob3+/XVPZzX6idaZVFevCIN5BhyEi7UboO82zj
IWc370pnUl47rAMawIbrdyljkEBzIyWvsUi+d1b4HbY7b6mpPJJG8l44pLLZSVmwXtMBZFJdXb5s
n6SzaMFhzRfBurwnKwsV5HaxK21+5w/hr+m9Y1B63XbyM5V4u+pTrQG82/+LwIz0a5bJttKN+zl4
zK4kLY/1MlR3BiByh/kUtHgBAtYis7X2aw3G2h74whuGdEn7WX9QQ2MAxkNRGkCtkAXirEwbx8kO
96IIqXoNRsCiX7VrlnvUpDsz5nZAyadXETgiH6CplcNzdf/O5K2FcwcQ62HrAgXKnqLd0fH78foc
lQDapEHvF5dncVGJBq7swGUA2X0ZSkzWItUU+HaPjRn4bmsea3D1eoJnbAn+mWSA3yX9r70MQ3ex
k9TDcPzzMJg0/ucE4reQB4fyAoOpoTZMINJK2dJolKQS470BPjfXn5iAij/5MBBYW5HmTGg/1HCo
9tENdbrAdfmHKWExdxCyBD9CmxpF6qkw1ksgTXHxvpr3VnmFoDYRiXvz8w1BcACsy+F2UN0eZ0rE
mX3r8pcwwbZD3o7LB5B0ACAEwChydfk+zAPTxesPLlF8WUv+iXNACA/hPAZlPI1NAo1+aNLN2fwD
Ep7PQNE7ORP/O1A88eTfX+a3ZWVmj94SF4Kr8lhyZx9PYmBDgziTh4BsuynuhecZ611bA+iW8sIL
xAoJle0x0Xbu16gNmitcF72fw2aITMZ+o4VYuCDgIViky259qa8fZ3xpper1VOz11DH/6j2dQ+p/
YXAUax/ZkbnCJU61ERjyOLns31C/aswi0/BGN3zj3V4SsmV0wh6zv+A7yqi8XUzCYuolO04FILWC
bGp3uTcWmBJdczVAfN7DfVR27XcKOv4i/Xq+3K45tBXIuWWL9EP9Mtk6sAejdc8b7UCRO6cpryBV
R08wl1mH+fjF4zvMTM8gBHbO0h85TR9vMn5V3NSuGD8GcU1lpheIh91cm3T9EVaoSWoJWP7Opn+2
f41RTTq1GTo1LO5CSneICFITC8rO7xA6vkGB06QA08tGjihc7KUatIFmXsRhJMFzMx+vrC1Ceq5G
XUFAtUfy/32fRtioENVrn9QQkCGopWQVy1/xg6cQq09RefA2nr16JoVrVJ1C0jT4EcHQRM7nvfjr
XzWx2SZ7bPADUD5QQXQvTzVc6UhaIe246VhNfcAmY60RcMXn+/onjYaMrABRWcZD/zf/c2g8OnQq
w59tUxTX2CfTuGkKrfKu2o17TH3uECCtPUtUUgPrxmXHsmuvMgEtmHUrypL5RrLCe6S9vID+orSM
xcnb9LP8l8IKibzvBUXyGfNckezTOd5ZuNbrwIfQnx3iXm/o6ztZSus1s/iksNPObI8Cc5BB03HO
M1GQ0AqGerpt9cV/LMIEgzuSNYoadO/Z26cPmxTJuFa/ndxdLVmkg4dx6wEa/dZTePBRJIOLjPHu
hZhUBC5O92uCaokwNd1mWNJbzKtDgxHIbsAJy/A7Klyw5iW5EAXNTYzTTPZmlfxCSzEM7DbWzOQF
1jSpk0mCTIkeug0e4bJF3osEEfWKFcgdbsQTqGPBHRYWG1Ax2keBlQPjxoY+oZkAToh+v1aqGXWt
yaPmWi03rtywnoWPSqTB3SiNM7Y5MXiTj/4gMaXGsErcaNXIb413EzNjw1qj+7IgYtWhV357xq9H
cMnPTaskCgrlpce86jwpdHaNP5lF1ALkstt0WcfO4q3AjEtq/ODzU02GC4oe0rvexdtvyOkVaRQ8
k/J/m2XzI6tWDm6Iotq5EQS4U2c8knGVrF/bB78kfePiLWTxwfpMXq+DBmXqr6+Jr7I+otAl0vrL
ZRyokbnX7R+SM2uYkgqOnPi5ImJdz9OLjZdLaZfkdrPlu2JnMVRN90VmoLAzuuRMsBSgEqMqwAHD
3oPhHPo7HwESuQq+kmH5+zBL1Qgt6Kg1J/FYqvHYpyNrM6pf1v4UyFwHdZtlw812rp1WikMUVweN
497rXdoU620ZAI4Gw+0rg1psFp/jyibTjGLv6FiobHjq2YzeMPK6xiQUrlwlrdW8WHlNSLUUZRyI
WFWhJRzNUeNc8e41NusELqonsfe/U56EfXxT59GS054pqhebKtbh0gChyk5lXVdzGM46kNowUsLO
2TQ6QQXa2Tqpgha59U32uRjjR+dOkFr0wH/hXU/YiTpEW9Y=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface is
  port (
    i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[2][15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg_array_reg[3][15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg_array_reg[4][9]\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface is
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^axibusdomain_s_axi_arready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_awready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_bvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_wready\ : STD_LOGIC;
begin
  axibusdomain_s_axi_arready <= \^axibusdomain_s_axi_arready\;
  axibusdomain_s_axi_awready <= \^axibusdomain_s_axi_awready\;
  axibusdomain_s_axi_bvalid <= \^axibusdomain_s_axi_bvalid\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  axibusdomain_s_axi_wready <= \^axibusdomain_s_axi_wready\;
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555C0000000"
    )
        port map (
      I0 => axibusdomain_s_axi_bready,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axibusdomain_s_axi_awready\,
      I4 => \^axibusdomain_s_axi_wready\,
      I5 => \^axibusdomain_s_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0F88"
    )
        port map (
      I0 => \^axibusdomain_s_axi_arready\,
      I1 => axibusdomain_s_axi_arvalid,
      I2 => axibusdomain_s_axi_rready,
      I3 => \^axibusdomain_s_axi_rvalid\,
      O => axi_rvalid_i_1_n_0
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog
     port map (
      axi_arready_reg_0 => \^axibusdomain_s_axi_arready\,
      axi_awready_reg_0 => \^axibusdomain_s_axi_awready\,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      axi_wready_reg_0 => \^axibusdomain_s_axi_wready\,
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(4 downto 0) => axibusdomain_s_axi_araddr(4 downto 0),
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(4 downto 0) => axibusdomain_s_axi_awaddr(4 downto 0),
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bvalid => \^axibusdomain_s_axi_bvalid\,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rvalid => \^axibusdomain_s_axi_rvalid\,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => d(0),
      i(15 downto 0) => i(15 downto 0),
      \slv_reg_array_reg[2][15]_0\(15 downto 0) => \slv_reg_array_reg[2][15]\(15 downto 0),
      \slv_reg_array_reg[3][15]_0\(15 downto 0) => \slv_reg_array_reg[3][15]\(15 downto 0),
      \slv_reg_array_reg[4][9]_0\(9 downto 0) => \slv_reg_array_reg[4][9]\(9 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_risingedgetrigger is
  port (
    \ff_2_17_reg[0]\ : out STD_LOGIC;
    mcode_pkd_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_risingedgetrigger;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_risingedgetrigger is
begin
mcode: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c4ff59dc3
     port map (
      \ff_2_17_reg[0]_0\ => \ff_2_17_reg[0]\,
      mcode_pkd_net => mcode_pkd_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg is
  port (
    D : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \^d\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg is
begin
\partial_one.last_srlc33e\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e
     port map (
      D(13 downto 0) => D(13 downto 0),
      \^d\(13 downto 0) => \^d\(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    r4_x_delay : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_13\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(9 downto 0) => o(9 downto 0),
      r4_x_delay(9 downto 0) => r4_x_delay(9 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_10\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_10\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_10\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_10\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\
     port map (
      i(9 downto 0) => i(9 downto 0),
      o(9 downto 0) => o(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_14\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r8_en_pkd : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_14\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_14\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_14\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_21\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r8_en_pkd(0) => r8_en_pkd(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_15\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_15\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_15\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_15\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_20\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_16\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_16\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_16\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_16\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_19\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_17\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_17\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_17\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_17\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_18\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_22\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r7_x_max : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_22\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_22\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_22\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_29\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r7_x_max(15 downto 0) => r7_x_max(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_23\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_23\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_23\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_23\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_28\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_24\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_24\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_24\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_24\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_27\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_25\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_25\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_25\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_25\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_26\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      i(15 downto 0) => i(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_30\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r6_x_min : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_30\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_30\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_30\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_37\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r6_x_min(15 downto 0) => r6_x_min(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_31\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_31\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_31\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_31\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_36\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_32\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_32\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_32\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_32\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_35\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_33\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_33\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_33\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_33\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_34\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0),
      i(15 downto 0) => i(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_38\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_x_noise : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_38\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_38\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_38\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r5_x_noise(15 downto 0) => r5_x_noise(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_39\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_39\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_39\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_39\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_43\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_42\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_2\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0),
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[6].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[6].bit_is_0.fdre_comp_1\(3 downto 0) => \fd_prim_array[6].bit_is_0.fdre_comp_0\(3 downto 0),
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_8\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_8\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_8\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_8\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_12\
     port map (
      i(9 downto 0) => i(9 downto 0),
      o(9 downto 0) => o(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_9\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_9\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_9\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_9\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_11\
     port map (
      i(9 downto 0) => i(9 downto 0),
      o(9 downto 0) => o(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
begin
\prim_init.ram\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FILVIFH11Bse4/sHNZWPVkom6/kq/nkKgpJdfvngfvmfxfHoRGKKL6t68950lk5Vxu8HtKO8ka2s
t2TSBpkjz+LyX+ej6an6Qr0X/bBS/dMrLHJ6d51dPebkg4a7m6iHjTbbvz025a9PgltlylgsMLnP
9iQ92Etjc9VY8qaeP8pasgHGjafq0ZObHr0aIaKtKf+FfTK3Q2cRbPK4IY3zRu/EpMf0f4qy3wdn
PNGDodzFeDs71bArODQmYNk+cxDEwRdfdaIUCLCA7qEHqAyYiNpltFyxwqQPNp9KrS++T6V9wOGf
Cwx+lLD3KnY7rHbbgbEUTMRjGSPWhLZovys3SQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
pTVP9dSCgyRVsF3eT356fmi+/t0DEo7YGQrxVHRwub9DpvX+B6zKPGkbn1R49ihS3EowcpHl3YJM
IaebQDdKx3Q8SAPwjyD4FoRcirE8cbzwlmjlIe2MPWsxZ+FcUSX19F6PNj0q1UJFQ+1zyiv6eZeX
nNt3LvrbeODft0W8syIl6Dba8ut7mjIzy2hzDratLhDc0Wz0F19s0dChdzQE2cijtCNVDz2aW/Zv
VNtyWP3dP2WwAutsc9xAf26Gx9uUL5m1TAOeVFrBbRIKD8+AaR46gCiN9gpo70Uy2rt2xK1V0TIi
+kSKExT6wVEyjBK1XT0X2GSWCtrg84H+SYtySw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 36992)
`protect data_block
Lspro1kkZdDz6BQ3yOEw68nuQQcPP7lq+RhfwucsEiS7HHxK/lSbqOtK8uNYn1jRrSP7RSQTViCH
hxiFZaOBNfLGMJu26e1omhyuXikhcrXtVHeV4a2cZRjscVyf2RFBORV4NsqkoEcxTl6Np+C/LUhD
ozQHBBbtGBxVqvS6O/VKhWN3gWpqWgz+PVfgWaAXNaIninigg4SR3eAc2D4mCfTzBUfpEDZat/c9
K00IKGGaiptv2/DUIleVyF7//aLSqlpXW3WJx/JCDtIINfpB8WBKSpxKcIIwPlm4okdRvkCnfOrY
OTW8+CG2Kd5nAJEiDJF6t4DSS/6xlmuK6mNvGHZWRew1RfeWFpnC58f6ydX715q99YfJD+QP08eK
czTRLoTz1TnbzgioHxihU9t0HWO+Zdl5PqL2AOdK/jiX5m6fTDezV6M39gP2QGwmbzRW5VcM0q7w
MQFBOJ2+WX6WSwfEwpdDY0gOEDuWaqdjLE617YiVMjPNIY/vtJFSyaQpitXRps/UfUByYShWddso
c7tY98ommz73FqwU2hRCRZYT7esXiCH+Fs6dz2URV7yhw6tKVztmqbtkBvompCe5kMb177rhBzsy
DFA6s05a6PJXy8yyjXj2S2EzmFEH9q4UcucCSjRnXQ+a+RgUkcvciIT20EOBYj5EcOEpRBk0slQJ
gvby8TPXN9zSq6JIz/XvP1TWPlsB5tqDwkb4keJDYOuPPWF9dqQ/eSGlAUkG7az+lWr3VdtEg33R
uXo6cD0jCY62Z7Omg5rveHOzgoPCOGno4rAm1of+QOqS56FsIv2zjb5J4IVsXsv0o8PbEpyY9SaR
Dzx1OH6RqnkbjhJYXVv2uOxe6yGCvxrdNgw6Cqew+rsXpK/8uOPaf74pS+J4eIOk9Sn1hQez1kXP
CdZlX4/laxIi2L0c2gOZsSKPy+OdXUakB83R5EcumAHMCG8TSVSR/UH6Z9VgIRvVyihNEWiAs59n
84MuN1oBbcJZcyWH7wMXmAE4j38+jjc9Zn6eATwtGWUDbTxz9ZnU473MawpuXtUtzy2vigRYF6Bk
E58/j07AjaHhhNyoLMpiIsUwACPcZSCO9epHI9FxLzxE7zWJaG8/1/iRDonEr8FY0h0fBf5b6XI8
mlDWa/JFMG+RO7ZV3T/OtgnpznPnNExGKoyeM6elUHBRJnFmjiqKbt69CTndj/scnTEkbYa2ABPm
3wpd7DLhVjJ9hf6J6hIUx6jflO5u6bOkSHI5l8jUXNjSBPKQjhiWNse+PuAHVfQ3C6Kck2Im/tnC
VjxzhDyDq20aKuyioKUwlGmlbkUmi0jVpicQU1LJtFGNMeMU/him5jMb6T4WaHQHeNE8g1SUNrdo
9SZMHM4Jt6l4xt2MBIdkspflovRp5rfvoqCZL/5cVwd6cMd9rloS6a8qCS8EWgSEkuVNzBfdv3TT
xuZWdyiDSKP5rVgKBqBJBj5PPWxHloCMDsZAHxbTcmfy3fhiRStTmwG9dhkV07rjF3fRmeHg9h7w
1HNyJU2YVF0TZYT3V4j80YVr12aYk/h1vvWljByRep7RMOlryf3OIr871dguBKlZ+KvPMd7eR2vQ
yjrmj8LhKd8+5C6mUpTEgMBX8uVqtbVx2ERFVc2JGeOX+RBRNYf17CxO8oePUAQNqM8wTfis5tei
htaB/ai//5Uwu7r4lrW3QXr4aEO0+JGTX9lffWWTl0A976vJ3sRBIgzNa1wom8Kgg1X821HhLCuT
Dm62WdEKjCteLqkW7gJ6yRlIGmZnJ2lGTPL32gO2bYkxTuRJ/T3NTHf1Xaun6V6+CyxI+fMPYJ9B
DG7BPEqvFgRfneo2ooSIZa4dzcdxcBC7lkTCVO5qdcplxV3CjbBsUbgpjTfN5LZioP8bU1nePWKd
gjMqWxL82Sjrp3E2k1v7rA8MiESOBGUfDdPyjvxVL8UuTgM2yxjF12ZU7V+fPq5RX78NfyYqhCvX
LsjzhI+noS1VKcgJeFFP6aknHODJszXPAu0cmGsyBZ0gPMa7AW5nOxlJ2hZcVvhh2m4E8knx4A5k
DLsrHYZYiCmMdzwTb8YmVLbtYZCb8m7+orjft15UkVM2GTWQBQw8vpl5qpj4Rr9VE3+/SdY4p9ib
ZzNangCG8n+wAfQedEUEd1s7tZ4pr923iI67Ep23dBxQfo2iaXD4OvME+STyBLzm81/FNJuEX6Zu
R2oN7e3sEqGJJMf13UlLG3sig0/MAR3/0g9YZZPBnY044OyVCGvgTxWw5ted8wlD6x8zot9zUia5
oYptZ5UiYXvhEhl3zPAq3jFZTg7fyDeQ7mxY3O+GQ/zIH+LlP0LR+GSG8xQ/9BV5VXaKZXkWFn7s
tQcEwvIfJRpa0HSyVmWjmRLa+zg0LlBYn1a/Ok7YkpdDtEqHxLDp0naxhkvPdeesdWVk0+8Plzpz
LLUSUVdSDqd9HA7iAnH0++5kGWLlY73JUZoWkujIZgkQ/4/Kgoa3GbqGmwj82SaRTtD7JGykA8Hl
do0feoy8YHxDIKivXd3yplN8SSwebND5XlDKf09xc+0uhRJbVv+MunhU3+Ec+IIlVcJJAiOpgIAt
pKJtk5NW85veJ96YtC+A79C95iG/cpfi2/F0mbEXhl+PY4OcVrNaHE4fNHlDKZRkdm8H9st/hNhh
wAhTueDLbb3gGssOBDnVV6PmcYV3yDyVnnmGeKH+g+qH0/urCvjkE37NjXk0c8AOrOez8WHGFyk8
bkndZCgvzYJS9KgEe0Tb3Cx6jO0IhfNLxqoAu9MfpELm6Ui9Fy+CZeitlzMFiD1g1Hv9KdqYJQlQ
iuU+ryBICAdyvE2z+IQx7xanAzK2u/KwAFi2OEWWmCjbnHysltwoKHiQ9bvPhbICOoPvuwnnDAy4
W36hzdmFmIEClr2RXNWW40rtZNP+jN1XzVOsobJu83Jtk8vDPEKjG/RyrdEb4LC55B9BCm8lCCWN
U2fjZGtRgZaoFABtasgWPFh6rX0b0vrY7JFpa//AePl3Y1hgTr7P4u1R5w1gojS/TQjbYsT0rAp0
TQiL2kfElQm7ZLhk0YttKiLivQggexMA2YiyBMGGw5QwAI7nfdkMmzM02zkK4PYZp0Snj9gsUh3M
KSuaFsbrUoChaJl6DapuzN8tRkqMtV1iIwbFV8BHwrjskkW+1hk0nBAkP+D11Sa6MdcjYmttmYsw
i7J+UyhirrDG5NHAtF/f+O1ob9VuzEkRfVxf55pPv0Fglhl9iFJNKNAnLx0Xsyh7BcxN7J2pvuPi
bvI9CxwgQ7csHlf5H900PssJxhmGr3tfilWbFYtR/iJggIDhi+JcueemHku/+BSInX9Khf6Q+rGB
2COsujEDTtmK4uYT28qhIMqrjTTzFqcsCrr1oY4OzcnBdVLiVUpcvvKy6IGEYaqD8CaccOkvTcyT
LkQuOJVyOuAnc48HidLx8Jp+XyNjaDDY/6YlXCdN5LEIoQUpEuXYYMsO5umomh9Gy0duDIt3sBRn
Edl/4qMDOTQS4e4yhAaaGOmwnrJBz4MxOhnDnr0GPKU9/nRCgu93rc3WO18BRLduMkfibz9hcYtP
jMfyxi21i2HyzppyJQFVOFQ/6fsAubDlZ2oFR1Yckt+FGqXRuzyt/3lUXr5GC8JkAE0vSg3cUHk2
6kra41nF105xq6Ou7UFSI1Oqhnsx/0tDYjr7JfduC2jTd4jUMzx8Se8yPN03uCsc+71Mm8rWApTu
XTxPXvk3VbRaB6J3Xb8/t4/RONP29ZusNfnbZ8Rc/h1Mi5c8FJTWHGQwaoyiUUVmqt8jXsgdhixb
XmpPtF1vMEeAeumeoPx2+z0V1suQ6infR7w0ocLxm4Fbs/qypUjAoaUeanQmlkOrIEuZ9UZqrnLS
srfXJX4jpD/BiBXTCVZdl5JXrte5bfvzWCyNZ3R3EXFJYvFwwemvQuzUJ6Pf3QeSod2FiHcbV66A
M6uOmErGmhJXdMMIP7zhwqvP9lTmAIaI7clS54UO6x5GlVczIoXxnyGEtBcHQKAQN4kQDA1dYYmu
4+E3ySY3JHliwK1ktsUKeu//1GssqINcOyFKWOhZ+6rYzMxarTauaUmWOqj6H9XUwmmbUxkY5PZB
BCqZx4x9LvKEXQEfG1dz9v8i577u4GmRKPpa6XfP1+WImbPksUGuU+ZXj8292oNfJr8jHv08DI6P
PsOmAQi9k8qJnAHtelW/YqglVNRQ+oQ+cjNChMp5OLufmdRMgDjdPA7LtEJCuHJbt1elwm2d7R7n
vGU+XD1SEcDDjWte9Rjv1ytj0raCiosvs4xyDPPU2Gbq3qhAAwtiQ7TUXTITuTAQ4ARY1HpeyL+i
NQB59QFfsQ4vU8zVdVbZLfNOplCGqizZ0UcRrfrw9f/0l0JHsn814svbFsl1hlF6Ss7Qz/+LxqJH
0g1jc1nK7yklW8yoWxwalK3CsZCC8tTW9iQvofYsWFfcdAxFQc/ZTOTfXNZkvu70JDy2WNhqTLwC
nTVJGXgq/gpHJQB/EoW3ZIgqp291r5bGgPip6EkAg9Fbx+9EOuJd2VYrQ15M4qXvaTkSEeR8RIuv
5a28jgIGr46l+GJf3gTzaOuFA3vx+gJzR72ORpGp6899mm3Jk1zvii6BgXZfdvTqtYWOyKpYoKY4
dqheC8rkMlCFDr9RQJTB7T0vA2ToO8k2m9q9LjKWHKNnOI9I/7tCAUC9altYchDWKiXqSqFNoLJl
ZnSvv/JEsXbHKlG4kjBqp18C9i3Z1M0qKKuPDCQ+334emucQ4tU+qqHrTmiAmjyyzbAZ5BdGQV8h
JGQ8Wr937HvWJFXsvSqS6lrnDds0yCArfG1A1Kq9MzzZflct6AoKtu5a4mtM2b4cECpX9cCmhq+m
qDrf/O3EKj/SNKioixUandszeuYLWItEGCSJCkdF7Wiqo4vwZ15sp5OmFEESeT32dYzhixncJIuK
rRaNLhycipCt4GNjd6A5hIXPSbkxaMcOF1zc9gbQvM6pATudiJwR4+bxjf/OsVZ+6H0mnMKZTsQo
eIzXHVkikNzfV+/I1+E241MANINxDR9QXLYrOE5eLVceynpBoyzdQ3tTYODsB81mFD0ANQ+AZMlr
xcN4p8B5qrZuc/99kkpkMmKHdSpNi8aY3ATl8nWzQ+unGkSDqVNFfA0ULLVCYvvszsxGbBpQg5T+
Yl9zDhBp3mCmcFMv6qi5GQB6QQUJ+B8rZPUNlr1EAVtyalkvoZOdxh/psYGtOo3UTod1fRHA0CoL
fjyG6HoZbcDDEgmU1x88nrgWQl49dVFN2WwtvZdjm4HW9Sye+genJaiwfGai5fX5oKlTW2Z1M/38
16uYnQ3dqrboNpmktHMZxT2iOi+pvP1YgE3R9FhdpCPqWhrFRSTk2o7TWTNDi8FFeA3KGMchdZHt
pzIhlDS6o3QGsFN3bCQGi3YC+UDnZndl3qt0MZyG2nHTIVDe1mLBgUtEIXo2ejeid5KwJ3aOHz8k
9CBQjU7A6B0G7byZw79TlD6+bAus/j/mZezI81jGR9/P5MILVNh/51MeVecflDYuZE8i9JxsvWar
+UcrYY/9722kEgcizpiNmttidtmkBfbowaOXbFxpuGshrgzpYhMljbqe8m4wm+3nVMDTy3gq6z+4
vZQzwB/CXl9eCcYSfaTkPp5jMQ8Zk5V1IXriLRP6SYYmeZ2PwfvxNLy7N/gGWukicA/CpY++izu8
RmXVIAx1fLchFS6RUg4qWYYgNHNGkKJUHOMo1DBgIAc0DQfyIER1wIBJRjuDa1CugZRCg439bhup
MuMrB8R6ox5SSTI3dxH1xCyNA6+wFlAfMOMHKCSm34fDSVmiuQ009qcCdXb4QPe9As60GQWAn7UX
TwDmndRFQ9fiXXedMasbFYrGJMnlqNt27auB3/yP1bxndVYK/fUO1mmH539xhxNzJzy2GYDJyUB7
bpRWRpvwAY1n1ieY+QiBWJPlaeEi8WfUxHjLPogsx2Khx7NpUiNdWL0rR8Gid0RW+SbYKzhpB9A4
A/BtGOnZ7bdg1BbeFwTqPeru+mDXv7q1ejUedsi6VnAzTyIUiRIwlHVC950QezY9AVsYqhUDoIjD
hEo9spzdwzwwT0td1oGGACKdWwHIJc25Jd5UIFr7MWZfrtmidIvhEfXpSU2qE4GXXX/SLd0pz2IW
FOu9mfhXQpvPw9IOwSx+Jp3mB8+essv+0UDkJl5LbKJfttJUac5f9YeGiBFPzDdzgx6z2InzCGCs
BypBkJjb4hY5hENkkWM6FKblue0hBbC16olKdta7Zi3SHHOjSCF6pGHnl3bbxmF5XyQGVeIYa6e+
63CB9+O0AA6dWm1i1y0DsrrXljn6mvTTUBYNnpDKa2NEXKDQSbyPnFemiP8NiSCrGElDtLn30LUm
+HHSR6cDPT3WeYt3qDPgAvMdawS6X8wXT2Ohj/fcjv+qz4vNvWDYtGy559kQrDQzuJbq1sn/Y9kn
6S3D6It/VFS9X+zxZvHlmsH+yBQC+fwCdqiu29eDKJYif300bW6rYg8+k/b+Pu50Tj4Rjut3wsD7
ktUiGuNhkfCbO0FHnf7PkQUWotk9jpyXI7KxC83acr33Kpsvu3Ddqq4bOALrAOJ/rx329nCYFZar
Q/W97LgTgVspNu+E3rX9Ly6OvTnqvA4z+zvrwaKAqORhGHluJlRYI7eCCfFDrx5CiH5zthyiKqPo
uEcRuLVUiyWrpQFvYj9UUTLErkkr2SlHlBiu5tXGh3+jxk/iblzkaRAKr8bdkCT4ks3I3TIVUjdw
2+jJcmMGnZ3gYwSmniP+knHHHuMEu6TFynArsBKJvS0qFLlCt6/tvl8bltPRMkJ0Pas4oYWMsQUo
AvHoS/ZcElrkpcpkSsRgaWKNQGBvLIz2dh/MeJfjmILAClvsbpLn8kiF+9yMWxP/YKVxzkfmKVuD
8TPGGwb0ScClXLyc4s8w6hxrh4FXTjKFQ/fWYI6IwaZj+i+luWltU4wtx38vUzXwAp5rPzxgcwqe
W/K/r7dAx+6ScuUW6KY2/sllkwc7O3VnnY+TODHt1nTrn9cLEgGZ9Xlojy+b40VXL1Q4jdYEFchx
N42JTDkXuGKhfCBcXduPni7iorggp2pOclvmLoorvGmaiS3GihLcPMClULasjL11Zx3RoJrntK+i
leWwgShfKQOJ756Jj5lsM14PxgBMFhdQ8EuTgNtNgnts3WzlUU+Rz6pFeuXyJO0yC1P/habAInHe
VBy+qQxpNCUgW0S4pfNnpP04XYGZNlMgWb3UA5LuvUjPw67sGS0oAkcqKjt+0qIDowtmSvS0ddJl
ec57HBL0w7sFg+vzPAshM7eZJ6TET5nrK7uez1JTBnVhK+BaCX2pbzKHwmfXXolQz3Oc6ocEnUQe
QZIaTISOzQUgU+mk1JTuXi2d38S+67ljlFkD7Ye/b2U4yqPbLmd9FUmjS8zH+jAeLUNlK9yAQedO
jSB+mtJJ1eXHiRk9HPXXdyt11WhbORIxFj0a6EeBMg29uoEwHm44RuUc89/xpjS+O3AAraduLTfm
pSRo4Q6ddfwRQo3oNcEq/+n/XcS7xD4NRKANuQYeP5o9Z5v4D9Ld3scaIeJVaXNEzYTFDX049UOb
nzkL69vUwEPWceQyCNsBzkMOf9/tiKD9kFR5VmY64TDGYDGnd45i2ExpSH2ukgdP60uxmTxnhDK0
dEZKMoMDvIxUNBmRYktyb5GK5M0Ql3+40ZqpQN1OgmkiLh79Dn0NXfI2jdwoPJ+oTsd57/vsTkRU
CLhlY7xisAijKIiHPyKQQHLallEHd9J1cIRI7Uie2TJYcsBiG9dyqmja2vLrkVI4kw/24dabBU3D
t6encrgFH9Xx9iq1j1yM7m+zy7hDMUdl/AHANYOTdJrrh0RZXRtHykCE8nycDBY7t1G2qjlt6jMw
hfei43aS3nu9AEYqT2wjXUYj2TC9K/TBw9cTjeU1QUc56N8POSfJ37GVC/ga8arJewO4LS20G3Ju
bMhsfWrKtl21nfH61ObIdsnzcryW2g9l92EPFgAm4/KInqF15O6N8+gUVFVIOd8iI+pgY+q8dH18
w8lB4rM+uNUqyeshFkQoXE8Qqz3So9wEiH6ZM7GxLkdiWj5TDGVWheGxE3d4EsjX6FyyJeofBHqf
RyH0o4xD566OUPCWHRBV4o79JchfvaUxnkHnp47VDRafRQ/t7hkO0K9xaqaQ66n9cHH69Chjfrtt
QVKa5vI575XICWDvdPG2HMNXoXgE+Jw466WzXl9h3nkMeepRSI9Gq85LEQpC3bGh9HMPS4ond6rB
6NK8z7MPa3HFKDimvwu6IncH+A50wqO+IImcMfWIdVl+t254+pDTkymEOMl6ATc63zmA00ZJ21zu
SEuZCZ0zK41qiIKfqnNC/oFTm+4iNLsLHfRXWfaZSyu9oqq2RAMTP61TUBEY3gu6T3U47FBMz9WJ
m3NOopZLNMjBjjK/K5PJgkw6PINKzGHsoD368FhG0WLWJ1yh2zUsVMMfW6u8ZEO1Oq5hmz3S3wCj
Gh+g2vdaNJBnX2ZHFzELggTfrIQddWepeSprhs6CLL1JAgjP3PCJ1jgBYLoXmVB95x1mnaPhA7Gz
YN4IxTIu8bxLfdvPb/gsXWOvexXeiDOCZ1zWbPdmObD1jFbTKaniq7wie4pS1xDkFWleKiDX2xBU
vfrqEzhu0cVlmTdvlK+iv6qpGbCTjNJJR0l6rXDLNCWAdETUAxfIhUYgzZTE3ITbR+B22Qnq+riu
stTXuzq54a5pPV9MEgiM5qQ2Zxt6pZ6OK52alAo8cjYDrbpvvI7xDdy8nxuIjgufkKw7Dj0Xh4UR
/mZ85wz3Z0nl8/Y0QbOtLjrD9MzI26HZrn+ZNlORGypajUKBxGboQo7PREsQXijJGoBhyeuohY7I
icevDdcCkhARc9N9oKkDugrcU5yik9xvrQ9vuWRtmWEPH8GFwoI6QLWOZ/cUijLbhVzUh5gxvHeJ
zvMv6yv4HEqMYKmNT/5KaH6C+NgjrIKM7J4yIS3rzYEJLI7QXXbSiBm4rkdq+rcHDSVb3/PS/dA4
/QsQ6yV2OCrfslnn+bByFT4De3DZIJvEOiNr8lQSpoXLR8QHIQWAtBtBaNS5s0bpUVBU+Twd9Ll7
YA7/6MoZ8VjA+zKQaOoDJ/sb6/rxBk+XSekLiMJ7Et3mDOou6AgCU4UEQJA/EHyxE+qz90HlwATu
y6OT7QnV/u7/7TSmMpndQC+ZPbuTezRJkSNhGqGOzHsVKysHgeg4E5Q7HZvA/hLujTjikt+cIlXK
qS25W/jpKuS7MA3OaFWpJztiX2tw/5DEX8bzxtH7P8qMEe2H6eBBJqvQwkBQaOJ2Qh/EKyVD7h3m
VwABL0cclI3nSd3lgCUvdOQ3v2Mr2EcXQRBTl6QH89OOYht9KHZFyX3SxxK5LAeMiDXyBAv7jqTF
k/EV/5Yzf2QBuRiSc8/OonsXDczL0NGuJRwGUv+ULO6CUAaxYqXmaB198cftAClH4dyla4uhm6n2
IqXb1MbVMw+SGzQG+TPseRhIx4sZh0wfkjsiiDl4bH+DUsKNY+cN+WFTPblquU6HcNIqVveWorAp
zKrk/cUnsqpXO5HZGjijM8yCINSFNk7zb/KdPxlRr4vHl2H351aNCcd+DmDzrtzZ4HsPEvWmADvV
TsCVFAw45pvwQTb9JUNh6ayJ+QS35Bi7kS0vT/W7pX0RV2dtVzWQYwhRl0M1jbREAE5BnoJBuJYJ
CxL5886Yfe9MamoLDWU7SlhsfwwEaLP8ddN3bAz3dRUViDqEx+5cinOhknjasom9neoTxuZBVfev
/aRWTrIFpix6IjodtaFlwnXzDMs7PLFB3oVww/nvb2eHgLVmnYgYTd3nto4uQyl6nQiLrJ4NVGtO
rkrMEPhcPZb37gIQvwizLXYW7yFPWGE7D7QQ2VL2q6Kp3BVqqGPWjn0rJsFbyoqI6oFA1u18W9OC
0bp76VBoa9PSlr902hNGqjiuiQ5+fxQeIN8Ms4AKpAHNCsOD62/8amCWuU+bnubvt8uXIKly6TLh
9ZfMmjaOhNu9+xlqeXAsHahSKCRFbihqN9vmwSaIWQyK72/YZLazQl0ceiUmYdptds4Y/qYq9oyN
8dvP6EQTwG15ngQvA2Rl/UiEEKeDv5Xcc/7C98y1F2aKMY68X4w669IEp1d1Vxj3jev4WbO63lg8
w+Thc3XnVlcQ2zUM9mUdllCOkZ6YE3IjrCirI0wzh+1FhTG0yqD647dXosmevUEQ4zgPRjSYv5aE
v/Rb2aEq0TdSI6FfNOYUniuP+DhdWBu20fUVvgM4332S2tdl17utwja69VrZYWAa/DDwEHNlSdoL
Dhb66OJrZvOiWHn0XMbj5HL5UZTRrzl6UQtHYI7LGDp5Q74bHf2Ipfks/jZKsnpCr3IEzd8gZhMS
eR5OvnrPzXxy8HzeYf64qRt1OJgR/8/ZijBzs0xTM9qZKN2TuXZrbvLDgRYvRD28TxgCFAkhaI4y
AEZScZe8zj0amlhTJw3CiCgYmwiDIWu3hzYpqzIy+pZs3n5FRdpVb9z94RyVF729eOxtoeVOH7G6
PqsWsmj8ueCJhiY/D/UO218AR5yqL71oMjunJZQeP/Rbk2CLUKUh4WTlCe4evIhu7A53NsjVRp2p
fLa2d4okrYn+5KpKVCpxvaD/K7SPD0X2Oa+skh2qHnUR67fkKa5l20MD62OrH+4kLIuCvLFryfN2
NZwhLQdaKNeJAd3RkoJW1fiO4M2ROTifM/EbmTJ1XxEinhtJFsSiAkF6MlNVOmXaoOzk1jUMzEeB
DoUST3cYezzBKWyGn/jNBmVTnD1vsBiNoUeYJHS+zEuv2w6AW5gMyWYlaRAFnY71/5yf/OrEVuU9
XObjiAV0JRYuOIkioN2mJ1eKbPx0ayaShXgjitWflSN6vBekTYUj1sK8TWeXwqon0hixIK1VnKaj
pC/ghupnd2qAwpsj0zqCWbSIHPRrnwJyC6H9aherSUfkWNSJMMQ4PB44UyXvyb9F4x8NwLeKZP9U
YeB9dzP4co6zXZVSw2+KIW9GoqgVhIHl5+vBLP4jIYFOVC37B5HIf4GBpXttgMquSx4Pn6y08LBc
6hQw8QuZPzBJb25M2cFmTdeHdeUMvsqTSUfapAB8+kV4lw1b9/FpOx3mYy9qEaS4U5im8qbPxc2A
4/DYm077xkQW7TgGlzz9U/zc82Nm8KEztoFdECjwSRjsIICZB5oBDWTpABCY2FesDS+RKliTG0iu
tQogoJ17TQJj8NbYuSLDct/eEg0qBc40bZwGihvAjjR4s4kgZmO/etXUqKkg4nT2Qp7Wjfu4R+Q7
jCJn+xTxGLMsdhUX1HZtDVFAPim8VMHV3Pshq8/VSYQg3r5JGSpIeHOggFBKmM5qnBjYh8XGqw7+
gLi/BQyJqLgVixzz855W8l+s48WqDAMyhh4iYCkgX3IezRB5hwkMotu5IlUV76AtBH8jGnC37c2P
yCc2/BYzi4rzqLvx2vK3dh9gbjt0HURDS9IX3w3cA+OwWn3Oszv6sbyrAj8jpedl7pRiQlqPRq6u
I32BHv5mseM5C3uRBEIJ86BYz1WQTzkrFavieoVu/bBIdatU/AKZ4aPAtsR2gWDZBykPNW6qthwV
QwpBlQ7EQEXjfIfQnoGXXbHKO2rfGBw5veM+uRQbpitokuM3Z7Q3dn2JWT/hbbELUdzXG6meHtE/
Kui540ldRqMfUAcIPklAAi/iPFaCMh9W+xOP4i6hiVJVOATDvlaXRbBONufdCzMIHwe1EW9iE/lR
GYqfHECPZsS2vL51B/KxsBrIpoh000Rck85ko4bzapv7WuUyldrsItr4s97Jc9VMQthPVqYxi0Eg
1YQoHXMCxWPng04/MCS4N567HrsTvve0Go5VCPumRW4Yu+2ehDaA5MZtIElm3+6Dr6D9iUD3PwjU
daztMPUJb6rAjSJPQ0YFg9VaoaCuDnlxzbKEFb/IC39lYBPlwzKzJAprFLsP7Cb52xOMiPPmerQz
c3ImYPXSBkFCLj+5ahsaA/+H9sKYHJsnk2sRT/Wd2pFnvLSfPDFHpwOwsxwgzCBu7EPgGhHEYDoy
CmN3xluqcofRYPS1MC1Ffiw55NiPiTiLJO2fD2jzDhln5N0yORvm/2eFmwgi+iyiuYaMVwW+wNXh
wzVMSevoV/mLSQ/gmCFPFbBBa15ovN993KdEm22F4uKISUzwjGKLq9ILPNzkB94DqTQQQkrNBKJp
Xra2WgdFpQT8oor88UYBaxZU8etavmvmoNkEvcqRZTvB9S507PW3yjN+V3Zyhp42CkJrgjG/xd23
PPwF7DrXLYN+OkvkXhuujSn9bXR0Puk4jJwQBUd1mUUHicfXZKbNW4ip+rS7LNLWM6n7W2UefVMV
+SAPmj+PAVPSnwpX9Bvt5n14nc7SNls8Pr9Mhi7lF6jlvJeC3EotzAAeAgPlOgqVXIFKurcSR/Zg
BfWJO2490wHOzYX9miDemXA4HEZF7Jt1FPR7+wnSbhRgU+OifuXklFmdItzxDFCet/kTaT/syNnf
EUUpDiKyfk7kJ/4Luc46gl63uM0nmxcN2Ky+Ah/HmnU2hIJHG0xWg0bpvHFcg9uD4XWBmyylXcEL
vzNtN8Rb+Tdipeuk3DCwfb2bjveYk0QhDFhFETCPEjrdBIWA0Czk0d03tu7WuQzZ7fBS4OIrgFWn
8rU7ep7ohJ33S6PwBtH4YOPrVek34LPSrz/WLgeF87G3HS/ZurtD/3fIAQHbiKZY6vF78xwjEZ4B
kak1IP3ZP9j0XQtxvq19/e5EoKd9tzmfGZqz5/KmaRJLdPzK9hgMg4L3tv+i6C9cS/oj/4B5I5Xo
tyPLWb7zIcwWf8sqIFDMt+PeYWRAxiB61jUnu1x+nJloB0vQlhTeVI8Sg1YLAatHXcemyLJGcrtU
G5xogNyH231eSey6jT0bzBCUoLJ3PWgGc22rBOmSlV4DlbXuLAgbQyyCtlEd+zTmRjzAjSNPskIr
L/u+Ibbuo09mLOF2n36gsMcbB5f/5ZU1yRkIH8y3BE/Ma9KugX46ybXTjqojjRU+Dm/DshA1venC
v4YH996zrAvXJcN3KSnocbSbNs/1nwTNIl0GR0l1vjZrwuWpfUYPf2HCMfU6OEqlgcdMk01EvQrl
4lmYJAcscjM/TdwEk72VfjjP8QRSVC72K195glm/DbjGdAsJgHms4oBZ61V4a6Gzv/uFP5JaO6qt
z5TL9oiA2bTVKuJRAAyT69mNlUDZ3QrBgUy3VhdX6nKL87eIpk6OU2fV/gBZMlS+EbWfZInLxj9d
Im93lqc3wjHwfRkqnj8oo5dlFnCqIc7bkQCcOnA6/B8d4KngRwZ6oa6UAmftIxVzBhr/SYyMxLTx
HTN46ddZmePQ0O0+PABNraOpXG4ic/k1ZiEtYjszRreJI1bINxUbDsXEwSp3FttrCM4xqavfhEfT
Bykq0nXcDJUXXD8wlW9Xu4GWet5U4rzbB6DHv9SOxZBYt2kdVG7xSeP44LavoYYtHi7O7cxk7kZ/
L9vYpdM9SMC/9W0nYnBjV0iKgewmq8eV1Lgy6m41q4d5H91dY7ZTI60WsG+aQijH9+YRtgyh9ssF
/H9TCHi2SCiFFA8IUsEEyWVcWVCt6Gmms6mXcf010X4BCCiqKl9OawP14qn2wgfoYVtC81cDnlV9
eyHZf1CsMYVM9MWWInf18VsdXSn28TfwaiAeO8Rug8NzmgrQ8bAdz+9wodfNpCFTm3FC4DJjsNus
a4dtVYPh9y7LLCL539yLgNU/hDSp43gQkkjREz6OZqR+z6ye5Zy/7dO/2G78KZCviG/d8GSHmI7Q
EnlRAsJYM+Hunp0X/neFaoZyAlkmOcfLTi3iBdLE8mBCzrOpXwQEEk1+YVbkrRHP+nWdHHVJj9SK
OCbn7KaA9O8thMdn5q+aewD52wq2A8egW10JzZp0v4jSaAzrMpV2A9Q8opn5NBB2HKSgn3gCG4oW
WQIVxFdLuGiJXEXnQ4nY52xGIX3aHBJvTCtPqke2Xjjuj88fw5fFzazZJoJ1JXTVhvxOaEtV6sqx
6KBePs5watxiHfvVpQzEm+Gn8hviKAnmf9uBnBd7UdzFoif/U6fovESmbmAeNxToxyqTL6YX4miZ
E4ph5WxKUfLlU16M9x+jM2MYb8xfAjRe/lfgVY8kIwzXGvtJJb/DzJ7g0WnqWO6gwl6pZ8kU6dYZ
RUaA5X7rhaxCH2uw415n6LfWigAVG0ES4doidIbRMuANuVmnOgh6MIm3Uf4axiK/LO1nvLsEu0+R
/ZuoJGE2jnfSKbNVNv1InF3yQZh5S76tPjqHxO5ENi7h0L5+p8x/jqgGjafp8865B9MLp8Ue6q5q
1Z/DGvIEEl6ww+JdSI8LFK12+ESq7W0dmP8n+EVNJoehH6+Ytz6XE80s3Jbo+iXngTiLFapT3Cgj
XpjaBCUThvU7vHRrYF+lUT8kQsO5C1QrNiascyNNhEyy0hokebc+CgQupwuWUY35chhaPFEOzGJw
kmwHXmSDGf+64myIL+81CHSMAjlWT4LBwIER4syXA48rN49CKtpNbo2i9ySpfo0s8xMQ7D9ingD6
V3qPb+zy1g3bpdk8R6I0fuxph/sU82/yIRlIUSC9+t+ejKRmP8Xf4Vz78OByLKeMr+JlTuR+JS1v
2IQsLwFxBaVNZ97HH5J4aF2dUfp5a65IaOA6Ko7vXZ/vmaq/LtWf1816n0Rf3v8rXSr/DubUNaL7
8V0c70dXN+VgLJNChWcWsXKVlCeas6sjZ6s/QsJZd/s2WgPajO51ifipuisFvTx5vsm71OwHb2mK
1d6FrQ7Prseo9/Dst5x406WyDzLFM7hwzNiGYQJxX1GP6mfOM1wnTBc4lywoPwmVdOgI5PvZOUak
/xvRDxanya6uqwKzHLfY7D71cGpeSsfpDflwvb825nUpoJv+wGJjwNIBHMjG4VpsbRsGVD3xjvhf
1/7Gib8nqYNUyEpOE+HfUDCwjC364y9XG5R7gFix9BYIYIj0zLq6LJxPl+DdIRUyfbtRvPoCYgqV
fAjcrFVQmQLTfzLXW3G+IZYin//AFTN/LxCUbRUt5qzYr1m84W3bJyHrNY3LpgHjNtQKrh8oUBww
i9qMgfKWBkqbHMLFNZ/8F7doF+kt25ZQP7NQRbaO9VUyko45viX32PURlyLA47NocI6PquKzdCeS
VrD2B/VWwuUtxiPkGMVGrdJhrWLquRHQ9UvKaxUalCvuYsKxLSJ5UX4bprq4+P8HSRfuo5O55oOy
AkLUum1ZU4GmiLX+5NJwRERWf18To+2fUcFZW+YWNmQIrl8dj7qXsfdMQXSWP0Q1aZ1SZ2vI6NUO
Cu2/KIzQroHYLUjUpOncB0H4CcBk9rAo0Msfg0tD8cFJ8931Gvp7eLKs2T2pKQtK4AOORCVmMNFl
6IrgyqGahk7QP0qJ/IAENwSmCvXt70BOsfv8aGL74lhacbSJ7GCiOVvc1vf4Fa9Qu6qwCOMbt/0O
u+1I/Dv3mBsi2HcbDjhl5rWZs3Py578+dirKTC73RKA6S5kZzZHqZDQR45CFFcmlqmzdM5NKLcgL
ldPviPABXJPL9wLJ7JWKdXzebVtWnWYwAsSO1cSUFGU457XsRAZN6K6Iwp9+4G7LUJjx81rID66S
nCROsXvSGDyFvAeuQU8xJ/Yp3WYl+8N2jag0ri424IDYn6v1QQncI/+U9BZ5kAJlHqOal6eFajfh
D4qSuTrpmFWbDFyNy90mJIeodhWDhJNtC1ZzQR7X+9P7qMy4oEyHLVEp0DQixzoXHLqI0b88BRBn
7ARuZvwk8tNFM5bPotAV7DF5VPbeXJ9XldeRp/YHraqVdw6GRE3UwlvkQLse+zyzh1UsLOeuptmo
qmwFxVk0jek0m1h9f/dMwNJppltoXgpWE/01nXea2iyxDW86JIw/T3OPM9M/mzbrigfyCNY+CWjK
Yc3ucFU29fS5NORV/1xl+pAo5zFkmYQNJ1B4IEwFbXvKQ39NkJ1RQp5OkvXysdEyN0tkY7JumGwq
EZOynco5c4Eb4G3iLzu0mBqLqe+mARH6RPsEBRxPg6kPyG8ziKhwze1VEtQHq3mEA2QbsMZczHGG
7StL3LlsPNS65IThOdS78B0vLfC3QJXyjcb4ZD1qP2vimXiM/0IRu6KKsgdGNogWkL78d2SFUH7L
fw/+CmTiOe++JEwuiPz4MTygvlNl2BZ9SdB1JDJKrBrpYq5N6FyLpNoNrkTITW906JzWX7eiOGLL
qYfkLC2IAIm9jHQ7lUtslWcmvErSmMGZ1NI4sLRf6QIm9CxDXUkdwJMt3zg61r/IvuCz6/UVpCuq
wwXggFnyjzC0Oe+FHJlxVyViJewF8nfxEgrwzi59eVL2PzNPibL5OcqcFk7SQnbE8DEP4N7EFDHn
5mRzshcKtqiMi1etw0/V+pocoZaOZmvSOPs4QJc1Crp2tV1xIxBPQvfvdM05o4n0UAN/9nCH95oG
qeSwiDbvKU02psnlGCQWA4BjYXx+xyfK/dMRK0XgUp2Ntbh74gO75FIfHFHWo5qCTG+CZtrBljAK
hWV8xQ8FSqwW7OOvJPseEiaIGPsDMtEFZqYA538plh936RWt4xWUu4QoW2aBu0FSHQmG/sxrDiXQ
gXjO9XflFpAUJRST4gHZG779ed6/1R5Qg1gaG7uAzKRlU/Mv3CTIxeOANp1h8JiKrmhdzFQu+p/N
6wPxItFmNBzaB4u0cGyEpNv0eY7DDdGJ44elPURtIAulrDVOsLA9gmvxM57lThxw1ImnjYLGn9YF
ia5Ellt83lheWbyXfy4M0kDoLD8XqDe7rP2exGFH69Kc8/6lAcNLOz0FC7yhBLP/Az53REa3G8ac
hVdMBrCuZeoJjvVI1Xq3CRNm2LzgHJDg8HUcbHRVdSKJ7eeMYBF5LvS83+4PDivI2z14vFoeY6Pr
BI5yWNOcGwakw+pDrm7L/ies/uHKXq6/E7DST3DFUgAtxv/cIJdycym4R+g7nTB04kBf6o63KGAH
TzoTAW0+YbJ4NUCFrnPDhs1Ry7JlWBX97QBgET/QXu4t9B4EJPL1VeHAApmuNwi8Czfq3ivUKKXg
YvsEJI3m3BEb+xTCANWxtqgcpRjBuhxLznwwcCFg+paZuYu0uU6V48UzEVurG4CThdV/TZoAfiQZ
UQYfcSFihzTQ8xgI7qlw2ASxBmwyS3dYcHqnTah3pD13UrlRzO7atqhFvSBd6SOSAADnZYyCzsGC
tnJEJ/nOE0Dep1KJN0/mSljMLdLwDvMboxTYqGwZPNHk4hxZquwJflOtsCfnMs7bxGx5ak+FiYz7
1L5Gp8QdlYvF4mVVXU4qIKslIdSuzApLYluIDCndhc5AkJmlpbiGQdCcjn4Wercd8aqedHV85vcR
0Z4NUKAWI6APESqpwdtW/BuZ7uhzOOy81RWgOWxZ4aaKZm+KuhJY8ZhA0xI50wdj0gRUcCkPmtvs
P+BS9ApGwaUA44wnskQ1Td+l5Ixnuu8l5VKJnLzh4GfvfZlb5HqIENHCTl3SEp24feVVomJZc2xX
8/EBztrHv2LsQoUwimHhU4xT9+wvh80vCR8XyRKc6JGGea/ytpgZyv58e83oK4DPEv6mMh1CGrck
fg2CBQ/wJ6WcgwxUMaz3sGpgAhDKZ8yH3KGg4vqyLl29QhSLBF4HaJ9ZlFHadRZTQh3LzrI+VjXK
Iri0AtVlXk45dP0vYu946ogZLygQcHGamluvZjidgEUUpWThBqCoujghAy91LtQs+IdwKBkLG/p9
rCITRFVTakjINzgVj8Gb7joZtbkLJSMW3k6FoAt/85bfnksVYzefrAcLTBxLPMcB8yyDixT0Pk1z
3baoaHoEg66+rnNrs5XyHR0aGQTpmb0lhtev2e87GTkGfrt7eDhwnogUE9w3M0jexMzD+6P2JVdr
gZ5RAYcfSD4fkujDqoNg8XKrujbUNSf3Q4XZ9e7f+joLJ4+YYQUN2OdLn610Y+AyaHxv/EqJoY0p
YtAprXt0q/CI93dju4jkmivXL3NM3BDkX/C3JYMXrelaIMVEsA0Fhyhmx6JphMKc/ZUh2AmzpwBJ
L3CKGChxYNaJZMpLcJ27ZbIyuLesPuXtjQTO9WVvJHSMMEfPy9el+B4IrZC8lY3wseGZRSYltt9l
oXw4tRCPWw4HmwrYd5jtZ+aN1wlHObWzq5eK1b6DD4+9ia9/m/2Oqfy0hbCU72BbFXf3Vr1P3JFQ
BqMwLC3CfGg3uVrnnGcuieY2v/VNU1mxPj+Sx9v6WZ1S95UhQccjv6f9TtB0qz8sWAWnqD2VPnaP
EI9sdmu537am3dJK/lDWC4UCuWmyrqSrJLIXPB0FYWJ+Uv99vLF24dTSfED75u1H4JgQqbHFIccy
S7RyvcmoNMCECyA1810+x2iU5VtKHBwtCD3s8O+1mys98mVey1oaAnRNiyeDduqx7rVmRGyVtYwn
BX9yNl0rmb4S8TVaAp/OiOh6pfjRpLThB+IGtezIbHO/3lXZ/BRArfc+pajd7Ev8p5FDTEYzlLTC
w82eJiWIhBCQStHV0dxBqSZSnSC/x1bFzNzMe1y9SvDWwPuVWtNPn/AqkBEfm86YzWi7N58dBpPD
OBuxF3XCuFIJrCfmx/m0Q/bRS98f7jLKZQUupDDGdPWd2hdmwFfaG/tB3rYTrOZgD8AOJ0i0PFSl
UJA3PGh03RJ0PKgteBEVSVt3ZOrhSaNXQMiAWcXl/OL7Yf9WITEtUxjyJunCOIVDOWaDSks5tcSi
Y6wEfreCaxs4cLk8bN9XQD/JIizWnrvdvR93vvQdSCU0aNs7MiEUXI0p5mEIaLmFJrME14sKwgLn
rOG7thcqvw4AzY0QaKS69zkKPYoO/nU5UKNp4E90TWcUS30wJgm2AWsumsw94LeQuwnkwnhU2Ir6
1qKjEvCStY94anS1EAPIeR+tanAg+//FHuDs3f/ZBEIitvGUnrtLKhlFuz3kCn2kZ3xdbu+RH5pw
A86ps+5i7seY/ceaQfQqWaq/ZNZghlabS62MqyXRlS4jXgG8YcMO7eIguXHy3sNZjAGEY6+proXn
pqVog47NdYQyZyGtgPspWRIg50KmlAYPquvGP5xM1v+kGGxfzcxyeyy/gzshFbTOywGQQxo1QPoh
4i5/VDEDV+78ijxyuvnatA2BIJ+Y/CRxYshmPCk43s5favUcTju4b/p1H4Hvh6/JQ1BDnZCGNmMV
xfTX3X7LIzXFzy9XyhAowxWVLEElfldA1VMWa/EIGW4SirQd6v5SqY7rrtjYxF0E9F/2bgLEVjFE
/rbVGiuXZmkZLRsnU0GrBSO1LflpwAHBJI9GjzCwZhOo2Zv99NkshraYIgVcXPUDDkMuOOjhJPs4
hoxbjrmL+tIuLg47/fb5pVF4XjjNuzTwT5xfuK+Fi8o2FK6uUjWqrDSh+FFzn3xX2roQLc4bQJdP
URQdRbAfce6b6d9JVEZZcZ8WXTNfSH799Ctpz6erigRajVHNaIwLog7F++yk9UPPD5s4jq1+Gw7a
+96kxdsDVz7gdflZObEs1gufoNGMuPWJlc0Xy5YiozV+VoVtmEH0NN8Ws/+pvTBty5u8GivE8HXt
38LF/DcIxeLOcivSxTDlNrjlatlKhjGzsU2nnJSrrQCuDaeFKmOPfJm0uPF0AFlnfHcjqNPSWVV6
4yt2RGU826E4DisVO3nM/wDIUUScsx3IuTDNYvHB6F3RgmCPbQjoqofODmGIf5shGy5eDN2p2KmE
ObY5iojdyxlVpoz8zmowOrtboF2VYnrBDBkDmr34+bmZ+ENwnSZIh7HaLXf08c8vBemICOeGx6o6
as9UvkRLywen1AqoGi/J69tEfkhBKje41UqDkWQqAJ97UKWdQnGTOyGz3hN40tGUjZIU1fzQ53dg
LXcvjL5whZok0yiTDaQ9SyxXRWwfUwdm/A1xkUr+KNwUAn6kExNNe0txfAKs3q3rjyb37HOZGBZM
VDbuTmMtH2WpZdbPSiYiYIfuKd8VzKr9lmz6Ch453BDIp+uTI0ly5RTqMDusV/1xVE8D7Wwl9lQw
l0aZwjWNLrA6dJynYHS6o1ox1MIiCY0r2ha1hhILv5LqT0Xn6jthFf7PYO/dXhr1QCjmwjjPj3nw
Ron4fivMLTo/zTZgeq3B7Frulazjriuf+vCPq/HtS73llcYvN6q48cYDGa7aHUAr7pR69T0lKah1
rAHg8gfflxRNWOx4YOcnAq71dc6KYTuPb+XJJq+N9dTb3AU0NVYviTdBEk5ii6TTiDFC6mZOALwq
0oTBrGPO9vuT2QTcZnjremBpmfdY6EfveS5bYlIB6XBueg5nLbPbydC2sJUIairj9S9MSGRkxlBZ
wV6lIMXf11cFnKo2/6FzM9Eds3pQ6Y+qJLqFulGI8rRRNZxGjv8eRcW5LIO0NdBUvah/z7w8PAdF
PP6qBRRn+Tgid1VUOs2EXQh04+0iknYhRhS9QAJN+dv2SJB/HuN4PKuL1BvzasUWlm8DO4JLY+70
/VMYhk1nL4KBd9eLKjin7CKp4CVY3gqSg2gz+5d/E7zhoHduPP9RIAROvIRn7h0m3mk43VfC3YUv
y2KYE5IdUFfH/YSQIlO5a1tnPi3/13wS7WmhxcJZGh1SkTVX2/cZUiz3CiKoKUTcXWfgeqrGeWTS
lwfgjddnMfqqgNykGLF/KSHKUiJ4Mb5r45Xd4qH4iSAXqHdB4BiAvly70iR0MUVY81sC4vjVLtIN
LfVwdAbuqLv21ISBkAwFWjQlonmMyqaW7UXIVhgoY93+dj/iUccriWMiBX7TMGwKsOZo+IW9RITL
RCGyuhLc3PfBvynBDi5d6I8+kmSjVhn0X8ko8lI1n1NIiHwMp4o8BUsUGlup/PD0nbKW6Q/wY4OL
WTB8qWiWSp5BVVLtkIr46c7dkJ53diJJbz0SmA8sPWB2Gv9jnsHRciL843MRdBEu80UPqokArFMg
njF8H/fUb5OsJbd6OeDlWV2tOUfBEuDdJdRRqBhBh9+qPjn+Ha/tvfRUnKJfLv2/hPq6G8+bq+5c
V2d7xXzksPtQtW4Z8nWWBZ2SFbuv7g8EGVUSe/kA7kqTmTJBvM8zV8x9Qh4+Ji8j1iBXX/sPLN0k
QHCJdeVohWmL61a1OMlPJwg47DHS1wvYmqd2U3DEwLXTa40r6O2CEvDXVZq/QbHE1hDCMZjjiIY9
9g1vPGBlt8BVutn7MAVI/y9+fDUz7XOqlVakKSzf6it1mwJ3nDbxl3BZ+uCIuOPZtoAB13hPX/Jy
PbVRl4L6wyKFdmW09G6O+470U6PA+mZpzktLoVOBZIp/ov1oFoUfLkAW3TXkn7C1X2CfYuJh/bvc
dkYPUjX3Riuy4FzcXqtuFt6xbvpxC56jjMDq4xEa/3PKv70blDbsPqgbnmhntHvl34XgEc3ZHx9F
ZO0n9EfDwA+32oN95WJTvRgjDbshjaUjmXa8HUjOTb4nkz2r4YXW/QSKhbToiwm8/aN5vRm+UhqW
KKeJWU9fuDedrHyrRwxA9kht3bzfS2YSPVj8gl1A3njKkRKGdyobjdu9VqcUE1F383LBNx61lzZI
0narXF2dNY5kYJrglI+MUhdhFjNDbg4PzSV3UckgDjZf6TJvimybyzspcPhTcU6DrYAz3tamYfR+
CCdRTv8Tltb+502wuw1fb7mqwYKABEBn+i2CPQsVIQ6/3x308wevrnW6HRsoeZgtUNA8XbyJRigI
dvZnFt8OkyZJtsPrGoZnjxPmz13flaDwrp8XC7ZA7On4sDBmTmn9j9iYH5ZRux7eLJE/POKT9B3K
TepF0ZbqRQRG0urOAjMOtJsJ++MgfdC03IymMaZentb+bEQ4dVTZiBy0NiOJXfyT5Vnpl4H+FgIo
kU+i2iOt6ldyFIZl1VPJJbPt8qLestHdsVBewHo7OGgO7sJU1lMBmuFeG7UKDbQOTHEK2e+e7Lwg
MT+t9nJrH6oW0zmuX0drkY/bN2zeCyqsbaJABgaLpGpvV5I5L3ddkau1NT5InQQgSsqenU/hlOTJ
wDMok8T600WiFIhtCWUzaDzK39rPKdbOWlSLfEXAoXa+pLR48FMR7Imb3i9JITRRMyEtvo73BmaK
GV+4tfb3sd6SIUmHMab0n97F4QvudjNZaAO24YmUhRSDOHw9/OjwsNyYxcmqSFWQ6JhLXKuqcGN7
3zKGa8IsWvjodX7pYeLofPUk0BX8plP6D41Uzc9ZV0gSrBbKzeAbfB2eFVDKk+iQ562b5ak8aVJ5
8U1jD3kdHlObSVfN1YZYVolEjwxDgMhS7fOLZKUEnZN9t6z02/Rl4qcZ4BCm807Qg/UvMF8b5jCm
KKdz+FdTf7Wp9mq9h8IHr31M7pfTG/3lX8/jGwSQfE+pJU6yyp7Z3/Xb9DOAj2z/IYsxf+DVpT3t
lfqguwJewQKHxNN/8lxJbiFVU2wlFFr4CKGYVZd1/PiTH6n0RONEhwLzFE0uA4uYrTsKEEzjiEk/
U3tGOzTxGeKqwNrcblCFiEVbPYQOpx95TVXydwjoxQ1+mBHpbQeBsNoAzn2eRhuzzaIJNFCJtqrI
fqmt7JLgXWOAWT43kPF2FTCUEX5pqMsDOfpQ4NqOMUHrOuZ1zWJa3HM3IBCmPo/5PqWdxrAJiOMg
txtsPqnPbfYOB++gmi0nRaX8sk4SXze9PkNYSkh8G5ML7uCAVkBpjCpAT1CfrYckmyIFrmtOmGAV
0lxj9w/TVIFdd/eO7Ke46iCnRdnFeVJ2HoMUjtgw2VkDlKnpln6OcxRGmZnaQ22jM0wyXvIuJ0ul
jKbfcglHRUHS+id5MWw8YHgi8cw7tTvKq3lIsHcTCmnlEyRryMEFinljAVLWHsSyEoUHUPOamK5u
gWfx3OwGb1CAyCxhvZ/a5J+7sKkPcKzaiJkZcg97m6WX8rlFONrbZILK9/T+rQN1e6KuITHpJ1Gf
z2kSLDTOXRptFcH0Qbo9PLAuHyDwgQMxX0DdlQeKWjv3tuWdYiYX3CJptQJ1WKSGHtXgK3tTDSfz
tKkd8AsymYkNtgylBpTGVpLg/5fgCwYqJhcmWZdd3E+TzKzy8pwzvmnncziAjlI6q0YxXI5PEq1Z
28hyoZVyJ8ZRNBvLWzZT3wxsdlJSladaTRIHJ8Z3TqqCjcReQsqI5HzwYusYpvNkZFNJhmzB7KgQ
8tihhkF8NrGHuaUrF845V1jY/WToX3QAffczdhfPxN6sDX9Bm/MtmpRpBLLVdH6Leg0R4O1DiXKZ
wXwLZN44EcKeMJFnn0qjaepEDdf7SXTu8WymA1UCYu8RL3VpYHFZDzHkfXVJKBWUo4nA9HmwONMG
9rZso/y7JZDxRj1tLrjoZrKgojPRdgcscLlsEX4mdnJhNxYOYAK590NxtlRHEeDBoUWWb+B/vDvp
/6pQ++v5qkzy/N/6rEC6A/00DQ34FJGmRC/Ex50ey/POLJUOgNcmN7BGrE+9eyi/mboFlSYG+3i4
mldn837OrDNAVvu529McEf8joMLnbRlnMcPggJAKWEbUtV/Ta2KYr/6mVgxyr/gUmIn2j4o+0cr7
SKIHHWoX8S94hGLVLYNeXYcjMOxlwUn/kNZxQG/+lsapbVg50EhmE42Jd9ljCEkPtWSnvdkUrK+x
/X7Nh8MfMKzm1EbWOoXb3QP6ar4OFOkJouAOuJhPJiceRaJXdw38RC9KNsKWiN1xzafnDdeCAlXg
OWZNLtaZD+wHRq8wVbrp09sDHhQ45pgZfdMHco2g0UtMrinmbQU8lclkn9OPQIzxOhlBx5zHCRMN
f5UDew0sUvXTN22piOsfH0RVE7RJYlK5G5ieoE2X5xSjBg81IjRB7Nw0uXr5uVGHccq8UV2T8/k6
uV6XcN0f16T2WD/0yH+RQdxUUsKG3girF9rwTsXSosP6PfLBSMwu1LTLj+1m4qEBjqWxRZpbAKOz
OASJTwxIpdVYQTf7RnYPZlYhurWxFeI3wAD8J2BUf+cw9LhAGBFp0usUDUaFs1jwkMFwz9jWbZBp
eov/mUR1reNkPmS2syuwhIbgHox5GzrXgAlHFDuSu1dtdkSw6spRmabTj8k26QgUi2dDUBVKYN0s
ACfzHkdlH8Wyo9rAm2Kojdx9FWDQaVLGMF06wVeTo8xleX/LRtCCdnNSLZjI2c/k6AeTczPcZQ3M
MOKtadlO7w+ZWYaEd6L6KUGu+VMKdRADbYE8sPiOaoEok3QkK7hosIPpuHH8olJm3aakv/z6/FPN
h0eCSvtRyDHfC196SdqpUj8zeUbZZnHgPomJAt9utUBblRcI3ONknhLw19itx2EBvcuCqxTHYKhk
YTmZXE6/+8qvM+flhEm4pCXsysNto6e9GKFj6GoYQ4P7DFnwWVMMv5gnGhnhlGn7COwimrJb0tTc
xrLdFddWnt+aQQMisDDu7dKSpbwNbcQrfToJYPcgMfuz1rNzVcHTvtAnJhgsmTROoHuuthIQ3t4x
NUWxIJ0vIokf0ug/K/zP2obYBy+j9KvM4HZrwfe8uF+rNzG7X2Eo+96G4aCbc2ImJ/TISSoFJOx8
J3xs0a0mNgirzmSXGn+TAD6WUgND+zTm23BoBRMZ/V1G26w4FbYd7R4G9Pzn2xAQrRjAgnSYgFKm
UBK4sxczG02l819Hyz9rBzvST1EX/ejBEKtUlc8FkV/hfxF1uqSRP67wjVnuLEJBYDlg7i/jsOo7
Sk+kWDsTtS6Tv68WmlYq5Y2D6F32ev+6oKuy/lelVzRr3yNDkrMQX8MqP81GaOpOCsEahA0c/WFn
nVIMYrE0IWcNF4njKaazroK/6Rd93cpWaJH1Pw1Uv5t2eTK2oMNHdRif7XY568FMXYlcnzgiVf94
rcyXeIQIyA5EhQdVJ7FVPok4KrN7FfU3r4IC7bAYnJ+zBraBSye0rFnWiGUiJFP4QDIQRBV7pgfU
U7Fo6xRk3KzVTGxVbLurn7a8kTHDMGn7IX6Bni0hvieren/QIZTu7SeJt0YTxxFTgekyBbaozhPr
l8Vf5gCeETqxNhcjs+K5YHITzk+z7+CQgbZflPfIICCtn/UcLxxUrnqBng6AL9Fy77mONOoOaCeg
V3g0XC2e63fjt4QG+hyVV4VY7iJougI9zG3ybP8x+3MIvAL0mk0+qRscdocv0Pp1aED7qK8OpLSX
rHAo682HuYEkoXLsyYTkzTXprDmwtmwGiY5tELDTlk/Ukt+Ycw1R/ACD6y2BgYtdrVsU7WJRJBu4
HQSfL5yE57xFZNvLhRFgq1tHA6+5VtqCpGt0Swg+O4JEN5LKkovoaKoy5NHtlQ4iA483TRW/Ms45
W6Cz0a3LMJ24S5tEzGvNw9YIgfhebCzLu1oQmFkKuKIXPxlec5C/5tmWZSvIr2VhAFesEgtwjH/L
lEUwLeg21JSbCRXXdKctuF2qHPhiGlK1arxmtP80qMlr/Aecdt1Eu9cozL8qRDH3w5bDZY6Q6cOK
vN8Tlq+0iPO5N1gJSaTJB7gt+xA0DEH1hpNDJwPcJ6VY8gOPtq9PSUX83bBQsUnnTt09lMP0cihb
ff1rUlxMaIDGylTdOuPVzrGAQRAnGkEKrmm1RsZDk2pgwTWMSQKf3KglDRfT3fT5tw8I6wQQ+8qf
2Hhpl1az/xqtW87MIS1lGvrEbA4iD0KhznQlYlH6Rss//KJIOez6g/OGR7kPmSaVEkCrI9DvUGMv
5bvfdyLRrAime3xlqtdyg1i4FU5/JECXpu2u4iH72RuKiJpgsyoTAu7K2d2cav0HgswFoig6Rf53
RJLteYQ/pqioHMMZ/cfceoNk6AC/NE4bm6ysZifWlDz0cypG8IVowvUBCIbImLpQnRF23wLJMrrk
z8HOeag74ISxm79rGexkvZkFkmhqaBz60MsGY7XGMYHSNM3odB0T/o4jQcNAbkHjdn0CLm8v+mqW
y+H9qpJiq+tZigOFlIbVMsXMlulU4cEYzQ959NVqi5iw9TKKx3PHT1tMdOE3lAPgUuqGQXihgeGw
Bd0wO8Rgh+0VCAr5KjoxQkH8PSUQuYRdKYIIU5lk2IGPVDF1WUzaWFBGAoe+ktJR7P8j1L+CDk84
jJ09UDqXbbwGtMdaKOoiWR9/2YyPgj3L7CK6QVeRWEKM8niMEdH3I3s5QZcrxI0PLch3VfL/ZFbR
J8DT5ssGxQvgF6VamUsxGoFMO4RKAsMLJ0SGToqlGM5iJmQxpdYTITyvtzOod9RFEUzxra+VV4DS
7vNdAbdEAt8L/4jpETPeo78AW+aDGj15mjNr06w/iFv6InM/n+HCrg5YjJ7dMAXkNQHf5wFzg6iK
k92p8DHHN7Fk4WOsDyXSpAAu3eiALuZRWTXjBww1+pXOIvl9tGXDaVgU2hxTcBoPw+0QAsErDy8C
8KIUgcoSp+jGNeVAOuGbyNxuA8vGb+pVkG0GH4HhYrJKRGrENUd25ONGTbFl2cMe3wPK89XHpXRM
r0I1kPXqzlRLgKORuP08ncg6wH9A6u919wh/+pJ22EgMHJkTx4kfE1iCxXhobRjyEA6o/oIPjmvn
hdFpj6zVNyzCyRVCI9k9l4lpE1qWD0dBMq2mcPV4dT1pUNeVOFMQCLTg/0NTFqMgervwaeeMXt0p
brmFrgo1l2mZc0uInavgH5BR4FUzq8nqT41m8TmA4HCsk07s2Cdyj/fzCSANzjdl38nHEg/RvV4x
ronZLy5mhiGwKoBBeSItmLPjgUywL+ESec+XbavKAeG0RFoI7lOfTYdHkxH2uTFyBuCOZBqGnEaj
G1iqylEqtPReDxgs66EnSiJBtxOUnd9ouwvNuCXR1v/lb1U7KJMmqrnqrE06NwI8ayluIpbPkubF
65vuu8F49O8KMLK2L2C96AdRam3fUmowZp9jtc4pMqfivC7+H1+6fK2lzqqjfwwTmt3xs+c4Lmzr
YxMm6Xu0j1ZQ02+bpWln1sUAUm2VSjjFcV6uxi988zFVOmH7p1Ni2UF7YvjmkNPO2HEBBSox5gEA
xFPSEBBTDLvn4aOAy19PnYS/5nlK9g3UaULlcANyywmP5kLnAs7p+hpzsQKnnh8jSIq+rs3LHvoM
D3DYDaNa16pH3qB4JWXch23dm/2U2CBrVNMOoAJtFjsa+fZW23r621oS9cO/BWvp7afjQRvfcdR9
F4Kws44wMpBrf4kZKJMfNL9nqmVtUrC0pZMkE10r/+y3CGxpPAFRc/U2VNad+PY8bIsBuD3ySixt
1CVnlNbiHoEWdrSPGq5R/vwGOjSqmVEHSe3syuwBawOMseF0GejL56cUhhlC7ET9hIM2MbJylqrO
a7CQmPm2/98dGeiAQGkWLRCniQFYWsqK0jlc6VGt8ZB78TqmqGsviJLbKsR+yy+Q/sIZdchrm2Eh
9w/oGO8nHjPfcwCQ2jCIGCD7NN+9QI8PVD4CdR1CGTyAmL9zYqtZMTmp3eYChfzZpyMA0c8T1vCH
KzDJH4wTQ1jCPfcznfnt7WYksjptdsRH5guVaS6zM1H9PWbaaRvwYvSm6HnCV8Vl/nUksoNaR6hW
+vRetbbpEWax7ec+xMttNA8po7eYN1iN97QhPRanVLXTHytHjfGYkmD5VS/uuE3ueQLFgXEBb+yV
LJpAZRt9zuQR+ekoJ2atgkpUou39cw3qyQp8hX10pPPgLznntVQlJYmkowYzSpZNKna5RlgtPKhJ
OvdH1YAR+cP0zBgjRDLshm0WLSbfzaphQXCSaIgam56pARoov7Y0Ywo/pKQh5+9ZoIg8gdHHE0w5
aAwdCAUjuwaA1t7SYPIpdKW8QIJARdLB8nXibOFOibDW/gTodVnx9823LynuXA+vYC4r1dfIff0C
OUOkbSIA1g9MScwJg7M9cIID8EIqJt+dROqJF29kkThWoEARp8dRVct+BvyvTuoIIPVJ9Xy/nseM
GRzp5bfdBDktzQRKRJ1u6Co2UTF2A0UoJ8ir5JtidqFb/oJEku7xVTc2KpJu+dDKRjCcwEUAnOfs
2sJbPSjRS9XS9K1liN3ozQDgNuU+oEbofdIokjiN6XpmBJD1y+lLBDHqbfYK9rsMKReWJQn1jykJ
FtYX3ir9+5imjRyLkv24Dp0BOCRv1Qq+JWp1Pyd27CU5qsvWXOELsg6PkqlSkum4SFP51ZYWSm1o
pRLnXR/aVsL5UrzRpCA6GM9vVMphTSjB2QL6VFI+s8D+BhQhF0knK9AjDFIrIM/3TrGARLgDrzJY
BfxXdFaUwJ9Y418RAbGN/x1wUJf7vuLwyI10zsOPQKf9iorjiqzgcNAp536TyehLyDX5bIVTsATs
rPfTxRmbRDSGbDJXeNtcjBuGac1m0YrXPCmZIK7hd3OTuYnBGIJ9XNXKenHOZOUNxP1UaAjDEOhP
60wCovHqdSVdq8PsmexqDzzF0qpFc2AgqBSSL4V9k5n7OySe82QnIUzI8rs9UxfmtK+M3s7q9Plx
v9mQFW6u5fc1EvhucutRrenjCT4GOxzWVhvHFUf1UOV3/GM7jILWosgC6Cob7/WaP2R7kUcjv82B
633Sj/caUxs0d7L96JkR55sXGOVLXonf+VmDl5MsSp2D03ITfnRYZCS2Ti1nz82iHyUacZWn97F2
iyK1VKcH1CgbR0P3IVEu4yxLBtu8Dqu7k9BoV3D6986JxsZ4RiC+kimUrBvWe2xAggC8LPbz3Any
+NrwuWfW9bCg0uqhrwPcQPQGmqVlL4ozQLwd3ssrYUrQ8xTgayjjeVJzdAIxXZ7K27rAxLzKE+Tb
UihopBXH9Xf7RoQgCZfU677IXzIuBk5/06mTqNBExFdZdSEt9zyY9BDfoB4ZKU17JRzWUQpzWxkA
OMySmnCuYodCvlhMG3CwYD+n8IkJlcVAx1JZXCSvLPmUs9x/219O/DkiRI2z7glSobNBS9+uYE70
Hr+gppCe2B6u1evOGwbhdwQ3jjy92zBC9IXSqeGSWu9qNMgOmDQiZwR8YHXMCYijW/ioMkeIkYfD
H7gH4MepIs4uPL5Emql7dvFaioII9w4++QQtNrQvXiWD/9tnkoYWaMR4NEECl9zM5ibwUEW0aPG2
VQUJ+EZ4julxjKEVEeCT98SqkfaTts1jAlvXJw9C0NtgJ369IIrILy98Ic0TB3fbQlCgtvdv9Q9i
/4TNfUMKQeHBOj/tGwkMnOHJqgr6gq+yqa2BrpW2SEenOEMiXkWEtlMoyHQzGwQ6EzwG6Hi4XS6b
FkhHwrcLGyU2KtVM9DuIdHwchiWgR3g3NhvD5JjEUq/cs7mTZ+MV4y7daVzl+xtlBExai67mEwxe
ez5OWeTkM1wOCiL0e4KwrBq7nCqMT7VS2I3pqv+Q7Dsu/Cp4OmSAOYcMq3V238AEi64vVG54jtV7
TJyrrX7wsKcgbe+vwgu4oxXLjR9cvIak5emWkL1Bar+dWPLhfQGWQM159i7nvuw9OxLabhcFnRjL
UdbTtcs02GzcYI25WwXjFIohSOhWOPrOdZR+sHms366WwDU7o+RumuWzSKGKGkHBfFR2qweu9Szz
cJdabM1Dxis1kz2jO8fr4WUpokQ0T+QxIBuijhT4lyK74lbit1Gur69BK6zuDzVLC3nbz/96AM4j
ADA0WchqyItg7xgoVbBBcv2bbkilTySNdC9sXZrWiDHcTfG/kP86T3A3dXZsq6yRvu2j/xRAFP4Q
Koy3SPJc5V50QMv32q/ODr47llcHJcELQkv6qy+5GlcVCuuJxpF5OO7iWMHefqI8RhgHe0hUmG7x
Uvw0Me8PpgBJrq7BDWXDrTYrzV3+A62dLbILyD056tX247Jo///+6ojnL63WrlkqgOhfF7Eqve5K
CJQb7MwFKn/us6W4PXmGVVctH2ayDZPzPdLpG6FMRT5zWV7GdJ3iNe5NtJ3LUU7AJcQhVtto4cIY
LRMS9sJRqJO2nO/GYoXhzI1N/xjLg373EWv6Z3yqNlsoZwIU2lyXth898NMNNRX4USgqjwnvcSpg
X3IRHGWPbqNse0qnZHTQZ66ipNswTXSdlUPRTKNmPOBdBMdIt6YAwVq664/E9me7khTH3xAWd0vs
SHFz2PIQgGPxlEwLLo5mpRCFsd+FPpJozyzhr1cububS+7Bl5Y9hiaZO+ZTFmEA7GIm37Jge6K5I
WGWQLPbQLMmq2dzNRP0X7j6h06XyIwqvZoWeA47ofy5rnNrZ4kfkEPOMzEeMingYdkwEJ5Bovi9s
tydtT+UnPwPCak28mgvFOwDV/YaZc5lDC0CaWqqYK0nyaIJJzApNkCXGH4OKo0lAiCkENugBna5W
K8n2lj/HOpMKVmwMv5HQfPQVc/h/EV+NbPv+SsIBUJRNeL7XL5e5EgivAY7xcCI/U3SboOpQKKB/
rW3Y/isWz841FrU+qxmVY0mxdzMFEKRF8teG0wSMTnm4lPa21illAYQuEYKqzvHIXm0TQrW/Doh1
ctNFX19+OzAgSS8IiNVWO4Rva1jLaHOaFdEJiuEFVAOIhKutdjRzCE4iI4kcKviT6j4ByJ6Rk1KX
xXAtPyH5knEwCH7eyRWXr/Z6T8xZfmorLKcVTcNpzIv+WSqcZV2WYovTGiajStm/hoi41v7I8z3D
1DSNRTOJJzNksTNRzstHdbc1aJsr72Az0ttj6TXXF12jAPsouMGjcwGNNHwvjsP2B0bJ/yMbr3kC
0ZVXd1XJzfktM8v89HnSAxABiRBrLANBTgntyqA8vybM/zLrEcNX6fKAj6PbQnLCDS9PHr5UVRZs
oPWLQcOoTnPripbhXIW/sD+lF0l8Lz6NOeDDKUI8n1F6KuXj0iYW4nywtSlw5Z2IYrgV1wdXQvMZ
Dh4zUiZ4a/icEtsr6UxQ48kayHj/Rx5o8djUsDrnibb15D1/RsmmTz7rjv7Od3O+3hayyLQVcYYY
Cihs3psUWpnu8IxZRoDH1Mrf4XdLAlWcVmV7QQDN8gtezZbHzTS4QZD+4Up9erm1Y39ASTHWfsMT
wfby4dX7+qpBBF8La+zif58SsA85T/ejVXc5exTx/tsDTujMEbF+xTEgCuuNcV/gRmF89hcBATb8
vWoUki7k0hIgTLgj0vQDs9U8N6uvPtUBMQppFIQoSBttEuwbEF+HJBqrjahlMFtq36COOY/dbSBs
yAeYSMiMBl0MYTVsXPk+Ln7Jv6I0N6DK2IUTJpo8tKtNAmhx8HtXMJgXFHnsk5VvTgx4VXxFe2h6
ULq5dSh2yQ8e2FpJwGzdO0MCxg/HFT5t7jj2dOFmuQRnIlJq+JCKGkIIh6zekNr+K6BUwHGr8krx
PyFZkfudO5PHJNt0J6sqSRRS6KYIc/zk8kmoDabEXmogRChVgAUUqCFj/o5uyeYI0+s0o3M0G/0I
iOVpQuSyyyA96CAKyfavrgjyMYusbrs0g/VgrfB9HqVjsGlD4KrB4ymLuFWMcnexDGHQUpGoEZQh
MctiOHtrOIFqGUpHhG6PJ4W98P2fPqzkAVd4hzA45OEkf1rUcLM6tJOFR8EGQKX3NElAM5RhDwgK
C73M8ZB2T21BtIaA6T6cvnH2OmHy/gT5dDXHudAVBDb0TBOlXBAmOCKgOqks26FzaUscl6d8yXYO
8MYKT824U5BEmL2go9SZdm59WQHgzQpo9Y3JoouU+VqCFodtde2hFPnzVKNsrtsQ4ATtsh3NEfuS
emem8rfb5UUqI9XecddrPh6nKyuTL9SUB+4ElCySa60KsCtGAgFYUdxW1invf0NzZfdA4+zKipsw
SftVmj/knE9bWcwSFatvU4dblzcOuCBQ/UfXXTEx5NcdTR+bcE4bd20n4BtnIvwMMhDVRUKCt5+/
aEb2f2JoCcFb0u8xhqF9qtHL7z9JdZi/CSLiSQDGBIEq+b/4UBrhDnLPfNKK7ZoZlODZcMQyV1fA
/zA+YEjCyfaylXMthSSVxp+tyy1aDw4lwhKE2+mUGN9mnKOQaaMH/4vv/XbtOFgCbTVh75Mq4fHo
VFGCEl6lFowbPD9l5gnPqf4Kev0EcRfAVpSi3nmpmMN49IInk/7UHTYvMAegxPr/162Q4nrjToQI
9uXM84oBjIxFvyEB/G1wGUWSg0YCofLdwIOQHBWNkaBGpiWrdwVgnC9da4VBvNWzTqjUqtB0EUcf
gwb0Qxn8Eaue4c0OhABB7CIChfic5buR0y28ngYQ4Vb7JRUeoqFvJHXRmLFXa/HWWWjrhBmWUtsf
J/0HxGiZbF2RnDx8tkJIY4CG3OuNR99LXcApGYq7zXlaB63XQp7yyGQ5P8ZtxJcwziCXcUwPzhRc
cf8M4Y6SesOQY6+3FOrYvSvdE82/YVGSKlJveTpRO6jiS+2qAkZd8gov9dv73PHFqKYX87O+dK4N
ZKB31sidPyhrZI/fVLLY1NdsAAnByBpHLG7J7TkG2nt/SDiBLSjr+DBD2o4c5XGknMe5dohAzIFd
F91YEMKg8prOrk6Q/gsUXzUAEiDiXJd9AUroRlbGy3+Si1XKUiCQJijyoGvBAzjqGg0LaSWO10Eb
oVudWCwDHzEbVi3WcMMz8ElbDJngfbW4GabUAmQMwVmKhaYQMOLPwd3M/ysR4DMeZObBvFCzuoGH
tf8u8vzs5atgJQfanFmQWDypkP2mz/y4K8/5DaDx5B/aDN8GeBFsAwBkoTBSvbeGaweYnyRG+A6T
lm7JNFrZpw8FSo60X7NdjtSakiZtFTC9VJPaJKymrloa9EjYXf7d2vlnXaequqXXyIgAKtu+/Fx6
JdEYk4NWyqNz/eG+9bO6bOHPClfhLHO+AYwDnX6xHRSYaVDnsEpJN/z0L1T7Q7P91GzNOTw2ihKt
MdYD1l4QjiY4VKh1iNrwqsJidLNfKCBi8/O3q4rEDZ2aZwRkWbGrEBm+cAQXUr9mGFeZPgvwi2Qw
2zvsdq+QjDW9xkxOPEoqZfQzu77kijGLksEirxiv+5tdfudUIVhFt8JkdRSsKoFXFNJUExPiJgXN
3qSAgMCrlaNHGhD9hZbuJCmgN8NpsW7HFsMrX5dQH9iXiKyqLHLot4DTN7+FbB6WQHbhRSidTpih
v3ZccHUDHYEvXOa3gj66EkPpJLRcVe9f2cYhS+tgQGgsR3QKLeKaf3a1uW5VZfTrDETYxmRkeBs6
M4EIl0qvCp6XUyV9NwceZAJJZJsDlIsz8sHe9ACA8YTEpmzw3wvyE7WoL+bp2rh0YAyH/8oyrMfF
5O7Sqy3CYun2VgTliwb6tdACCubafpVcHUTA377jJ2Rl9QSqdQ8vSuk/GBs31kX+gfJoa9l/+ciJ
FAf0ay3OzA25GjR92r8qrqzhca471c4EM2/qg9U+kD69ODrAOudIWe3srew+NO5CRup3dBjFiyb8
itRrqEx2jjNJHvgv2geuKXsWzaO3c/HWPuEfHyszQDAUa8MDk5DYIaiVlMn7NldY/9kFehfEDbA+
ZJV0lbbyy08jfh61ovZmvloJfr/oGf/5FAWmqnxtfNPSYsLPz7iqQ/gJx81EcYtu8g0e8/nw+G53
LszpVHbNVU+qkqYKhvC5fDUYOFK9CuV8jeeIsuj/t/EOKAaT7q9CDjzmMO/3vboXBUEPyhzvjFeg
luMzN4vl2G7EQr61z3UpeMsXgE7GPbENguyygihcQGK6/IvUz2cYoA1IHqiGkNdrEnbY2FuaSSGn
cdbB5sxOc1tsK+rOYTdsYGwXfIjm8OGzZaAfmSTRQB83TKuJlg7iKABTf8yORS4PDbesTFd4LHWk
LqfjUOBxorDgxM2yU/3S5onhsKeLmmPiJVLQqNO70KX6nxi5zYjorUA829FOAGnzqIPKi031+rKQ
Y/PfH9s5TtDUPZbJmuoJjYH20ZYV1kHx5+x41VYZyACPv2+01sZcjE570jIJMK2gTNU8KajtDwme
1GufYhw90Lxum+JZBvIKHTH0SxH6+SMT3mlzM7TGwYLtlUvXN5Ns1NICM7ULQ4Z1Tg5ZiJXpEMW8
oU27JkR1Uu+9idAADdaWgmH0jUEp0Dx9L4GhqLVwVOd58fKyozy62xffvMGCqq7pD21GC29SNMcN
oBOUycYgrjXRtj5YtAAjGr7AAcBYo9VoRztg1wLJZ6IMe0563bK/R/eGQ0vLU1AxwATjnDTMxivu
YLBjB/W1cEshL8LgG7HJTACiUDuTkBb0uZ30Q01nGfTYpbrfct4HABAq/+0kxUXjO2MlvGXfStss
/LY0WuS6eeaTtcDBA0+DIPNgcZxuGBm77hlAAdGUPX3SJWS/AZj4Bi6fbERgZfOmRdQ7vDXRkYz0
QHF0wHYJ8cFc4nOjDzKdxtI3bDlY4mtn038jkf8jvXN2rOR9Iuss1Fac7UWjwr4vA3ig0MeZYsUe
RJU3sG2BNUDArYOvk3QjMFUciq+u9QXnfqcKYtZs1AnS/eVdvzZgA6qqspjn1pbHo/8alizKYVJJ
YTceWamiE8F0TJZCemTJEmxWAt/aSrMbuLufBppOeaQuoTpPi1ky3mSkhp7ethG6OVAhb5YBUPe2
PFpD2gGH2JwzJ+Cg6TnBQm4DPLBTml4jwJYMHlC/FVq67iHUDsQU7c/vrm+uxKy6ovp6vjtlHxhB
4pb6wSonH/ctVZo+0MgdxmA7MD/QqBMQ7PuM8lSWmtp/Lz57h3a4C+TNy52ER24t23mi+JcP25dT
0mQgseXONtapDJEL9M4INL3AhlLk67aWuO8Ty9JaGNkJrCruTIaDZk4i3KuCW/TfbqhGgpnqjSgK
a95gRoHC8a1OYzlgdKwhSHK5ancvJNBEMs3Ax5yNYBIX078pC73Mjp7g0vXJLR8Lu6tDVQoC7pJ+
goeR+NcGqTTbmDGsz4XlsuEXIZnzQVhXfU+NITLmlTQzRvtF7SqVs2Fyyiq2GYcg3kVbJuUX7P7U
Ebj8mm3DjEfob0oMbVNodOdJdK0AMvYG/aMhC0Gb7MIF9f5dCcaWoK9GfECh9eOkRLCl68A2JRBd
73Wx8yVUAnzjhDU7Ra2hJAKS95u68RLeXccxqZ4q7tveeOiw5pnjPsi+gUMYHwgoUAMhTmk4cO/O
dPjvU3vkKcxSckdIt3t2/9tWrOvp4CBcUzqOCGXKasr4rsjIhzBdZXUuvr9ekxJ1B6tXXZBbTLq+
tVkoupdzHHkItmmUryPHh9VZznFHQWTHvZzoTorVJrbCf9xw6I700n/nvfGhwYzE/3Dxdug0bOXh
/Ff2UAk3qalVenq4pkCy4nPS5xqoCEfOMmrvAQGj87au23HCtdKTGQVQt5m8mjriNpHJlBbVzsrH
Xsf8HS893n6S1FXMB80C4e83Wmdt7gA3ijRXhRG6Ht/m3ZQ8kjUKtnJ4BwoJA0wu7iw3n32YjqtT
nfdBcixYfKDapuNXLLAIRjJoQ9+a4lwmVbbZJkv6ZgVshWloifRzAzwVa+GT/+Il4fsuV6kySUtl
o0tY+ZjyDKUiLBBL1g/81z8wEyObktlj4y7okbalFfaLjpG/iA99sOFh24bCYMJDY+a3r3foKevy
ByLBI2LNUim8pnnczWlKQDNfcY2eDwLLTEsbpZkXCIc4qZma+P6Bh7hV75rZAONIJ5ZyskPg+ARt
8dH7id+sd95b6mSRHt4SasjcHOQN+DePBhzqYSyyyENqTEW3k6f86gXdugVPNVr9CnQtT3iE0wRZ
L2vm53XcEw30QNiiSjU7QxJvPuqRtg33WXPEzFsK0y3UJuJKtgiR+q/xiUqaQlIT+NdLwIirQfpl
HrnO2nT1zjziu0WYs3BW0ZeoWMXi52T+bgUBrzsuFjlkBN6GC5EvJSKwmfdS4+zr7Af5rzdDLtpj
VOyy2XHp5Xud+zR51O3z6TpBEy6gJ+jHiP3NvRrRg562OeHkHEZ2KHDJqHT5djFyE8+UeQ1lXPMs
CV1clFjP0yGPLOc2Map8fuBchTJ0OrZEw601W8qjpxMOOHuDNi+FFyGgF8UeKhI+0VVbi5BB6bXm
XaGZsPo0tAP+ihZPjG++E+BxC4Bbvcj5WFXUx2PXWiHSfbQTkpbnwXYpaOdqE22ale+ivam3MuPR
/MXYxVXGtTloNNG8+4bQagi+ZL3GFDlrqygyGVW0sYQc/rdwQ/pn6/9zcnsnnYIKhTtJXPhCNCvH
axZ/AaebZ3r8iNYe/sXGpdjOYW/3xuOeAkFB48EuH+xJOeY8GnhZMs7NSiLauhhBMsrN/UanaEEP
n0vwAVJuaJzwPrlZj1TObAQnl715HtwfRPBOQmKMrD6QIkaCO44qr2vxPUMrJZDxnESgmvxCA2RG
fDxP92PmblAsIxjBfdrr3o1ZS/l+VqAI0pwcv52F3igbIA0b5N8cRVTu7vD/TI/PS7TOKNTqjbmf
HqnWNixTGeEuH++GLnHseR9wIofEFoZb5zo8YNnxk2VJGr7fb9TevZZFMqxz1zNsLzUF2BQ9AsfP
JgVANh97wUd9K3JP29mkL7EQc22ZNWgXdeUebbtzsj5d2zlz1kiR3CM3MVRqLtr/7Opgwh9aD2vb
f71c5ojRf41kZI7rZsRWk+2Bv1oZ5WZB1nqEnwB0k8hXz1hpEK5eD2ODEZCvK1YEr3vdGiXqGYPL
PGXgRDG++U3Rgrism9ZEDJDich6w+MOAMpCcV9xV6iJKWvzx+tVVXSYNm1UZ7c7QeJoBv8oO2s8D
IzeFpXevvxYpRHZi1q7i0t8omxM4pxiHXsrFHJqVd9DMpIzo+1p/KSeWEkRIUiMmGW9ujMhXGANc
m5BQIBVxnt0o2NUhI1g+PcS6IpP+k1PPgmueHEJRuwbhVLYOxtJah7gNbTyKWRtCrCUIqmojfW5G
VKlyl171J0Xs3bwjTvpUMT/pFzlt4ru7lZoc6toulqsw/9ey0Ozt6ZlVEL7rUfZVQ8gjZOAapD/l
Oj3O2XFD6mEZOD27/GRVP0CL8nLtDXyxpwy0Qm78DTfNZrTnhJXT1W0GiPVgA/SUN/HkSL8ZNGZ9
QTo3Fw+34Rm0C2gnVU/+trXySVSCjdOR0gRlLLxtEW1s0law1SZVc8FjbMjUVNVmcNAvoH/ta7Us
Q9sj5P0L4dMZkZ58F7BytiEws+obJxFy4rZ3/9ZTFi5uB0MhCBZsMqXD6c9Axx7LhYG7VtLfAikF
8aNeG6U1I1ia3/Os+4SmWBfsGRiz39BUBqQIB1+nwbNF+HFEMw1JZ1COMg80ExvvVr1w2CcjRUno
bU3yRaSd7I/vQQizfZPn2/PZ3mMCRqhrr/it1hqCVYjEXjnPrVLHa4tQadh9WeFrjyr9PCkhpKXI
hM16Lhd7IrgbpUzr/zb8QgHbpnG47tw3KQn2zudI04KNk2Y8N1ug4i9ADl/bfzJPRzKzTF0suezX
P+0tJj0YWGfvqD9PXCR1BSOwvf4b06ejPaqpk5BkDCpPgWiJ7lkBfnyus32LBKF985Dt3IU2Tcaz
3C4LxNgV7Yv1J72p8h4BWwAnUqwt/AuVUCBn8852RXrvnAXkeh1+ryfIQWwdhV7TfLm7zBqtv+LD
ANzyzx4YmIrf3NIGCn3Zj4L3UtWyFiBecLFRquGWR067n1lQfAXKN96vKv+G9DDdgZxVEOX2rC03
CfOYBEyH7BMtjTxVrEyhTvjI6Ev/WW7mbxG9jZtmvimHXHymFUogIiHb/69OCsYYoi2v0hhZat5U
dGKHU9m/T3y2ljnr6n3P58zGs4EluLcPfJZkqRPIhi76zeV8geJJ2JCFZnc94K1+juBQCWuuT7bH
1y+c2oxM4W03RGRx4Hd1zFuy7bSa0ReKUdicbGX+gC6J9Os1yfk4lnAhktEPYyiKqFkLPexRD2rX
7zW9ecxYx11I7JdSL/khW0oujntn1nG2BRQAb73NrtaoUxd3CkHD1swH3KQFHSoztdTqGA8iqy4D
C0EJsmIwiMcIyhyGcKbq38Fetf7BEPLkcV1cTCiE/CQcd1qCkAiKWeqpzZdkqdqiS5pA6JF9Y8sY
mIGyckZUxKN8WJUicG6uaSQm+uLUVPIB0hQO0WJUkTw2TjJmx03e3tiH7oUTjtJC5fUZig5x569u
nrA/nex5zpk5pWmqstWEGkmxIajgVwHvkJJTVkpfoozUCjwDwKu6w3v+0XvbbveNCT5uhkeyMUun
rRcyQgr+PyVVPohbQW5ErQDB91ugRpiQpD9QAPOcG21ULg8xVuLyd+KaSpDuZRqR+EN71Lo9dx5q
QY6txPjsvrdk52rWPm80uqAgRw7Gds9mzyzT7QozW1oxOOhdlhzMzJVQKucilNmnh5bFite4RtWg
y1TbQ49ZD0ybOmoxJQ8WBQtre40pQoKpsFOjvkZ1H9nU5TqhkRoIV5LQxLngyiYAX5sllegY7r61
WMOxL8jJ8wyukR8TXNTVlwjQRHV/qO08rP6vhMvPgatT19mWd262uOJ7GLjDnxH+NV0mE9zjjYhW
QrWbnt+eYxL9VDAR6vcHxV6WCKuf2+80o9wa+PyaVfXDmUhGVwQs2cQvE9RsLTWA+RZ+Zts1Ed7S
ObtbXXjzFBe2KUUAYkqElFBGMd07JW/wXpx5Hv9fEw1nPZbewOEUgG/L1lknWYo8b9GMuiRvYzXn
zVDG8S4aHWgcDcs6IGOgYMtmE/bMf3QzxBsafliT3RTyVJnpoLBqW5EGbKFPX8M02N+5E6Yh3Muh
bieCvXbM7MsOCvQcENApVU7abaXK6JGFaC7kUzPmXzNaFLGFeg3RK6jCZmVJ1GKnsRgG/mLfcL0s
tUYlNvK4LA1hpqMeDKlgdwbQx6GDlfn0c03NUAruVHwiZPXKdYrVgs6DYLXQg6V2phbmcL+e8Wkn
7P0Y3qu+FvrI50Lig/L00jxlzOkVQQAe21KiP1YwMe11TsfIXd18ZdRnNdzrBsfLGYzQ6bymUoye
x/su+k8JzmnBXbxDYYb1k/bAjyC8SEQmU8YJFUBgRA5x0ixTFyt2nG1DU4FJvQnL2jtdQZhIUZj4
d955ilQ1jvKVJ0j8z3Wg1MVP2zYrhdgLtos+IukCcvnvQm8YS94Jj82hgLgAaPknB2LDSMQ0cG6m
J4AZ0tc70LF9ngcE4SKFLH6nE61zkijHBU+P+sBVsXIPXnDOVaDgUqlLtCrG8UWeQ2Tc340duEpZ
V0lxd/zwwZtZZuGmggyncL6CjhDWC7actUmTJL0ut7vif3rkxb61Z2bA8BWz1B7Rq6m6YF+P3jVJ
HRV6SxP03pBOARCvIH44ORItbdJjTZx05+8ecKWk7uOG7EyKYj6SYysKJw3w1DcgFcyJkaIHlfiD
eJ2TjEge/RDw4pTTDlDU9OIAepI1MBPsTzQegBqfe25VE0jQW1cD2eIeCGRg2R+T2IPti0nk7hJO
yNdAGjCUg30vDUpxHVLAHZMXEDZfkMfZHo3iacv3lGoLVlkEmgyope+aNP764U2FT3MVrKgr3jTI
YOuA7KBwo/jXR1BPlpI5mog5U30zoDFOAoNiSU1lihQL8WwSodoJ8cZsG4Y9iC6CO9nKWe3Sd6hx
km3BAFwwhx0KQSNDyHCvbXSWtfiirOuJGM+SWouTs3FJg7ZMnMh53eMmje1jey8Ip4Pumdy/GG/k
Ll8xplTlhr6FymJh8Tt4k01yOunacBlOQWqJHlcDh531XcuwquP4f6pQ2VR7bv0V9j9cPVN3dNB8
VZlWnzKC7VKVr3mztIsIXmPKYUGfXl1Ao/s6DoV6SZIoSgQzVOM75rCgauLg1yXzFU3R83qt+PnC
M0dsiKESkoiYwFgjSoOCPTG0n6DcDv2rRZJyBsjdckJOFzxS92geyBJR/g3yiIW6v7RN0oE7V2KV
hg1kIY1q7fug+J4O2Lcsb/sAr20Jjap4vdicjqtdxA9fg55xsiVliUe5bCbMxYx+oT8U1/5rX9Wi
DEp6VcZ9h+XpCFLh9+7J7l49mN1v8TaWNbdvREieDjJcZ1wqkv2ICOX7fruXMQRPdnKnFMkyUMAY
e+NEbWkUssL3QJtZ0oFaClu8e+q9KaGVVn5/W/0a9Pi1lVi2JuPHITSkSHLAkg50FFLpiT6YTnVl
pA+jHJCovxLJs1zIjtWEpp0Fk38pyepY9rnMuiil5amzhaIus9yzL6Qaga+dvBZVnTiUscbySI6y
pLeRbpI/n36xw12m8fIIFmHxeSuab15OrLKfJ0DF2McbzTKcXgAB0EuzGrjkLxDPJR3PfpaTaq8w
y3kgvIhWraqmPxICpWUSRsesCJ9TX9conK7NC6gxG6TDbkmfn7yDTvX9mFhsUFhDmrjPF1glShMi
ecSYlIPpM5JIxcGP2hYZki3vgtmjV0TZWZdk3Hb0hKNa7s7F59b6k/S7Ee/xMMYTTP9o11a5qoAr
8R1FWpIESot4gVGaiunEP7OKZt7sK3ZMoQb/VquUlzL4PmYttIeL9nkIByl9uil7oCRnbTTSoCaV
Zz5jH49DOCcqRjyRDWYO01DxEwZu0WU5qKjFI1N7AiG9SgPWNgLiGqv3rtXlgVGgFfbvPHdzQgw0
A3RSztTPtNF1ydG/ko2qLIzV2ieLM5MUdloXP8hcwvaaLw94ZIik/Bk3bHPJGxeEBzhzSDmge8os
Mr5ePZgLASsW2bQ05+jcN0h87dbndDHcUOKCkRbdXkkxY14WfOIAzz5sYSnc1UCZnPkZr/kPbkYV
q0ebalZLBcE/S9JO5alln0xuhnqPfyEnQ5RL0IzLZ+UJRBrbx0vASUn+6Sry6Ur90qr88hZ3jqyd
qcy1YBOTbGebb1yyRhUd+rHOam354Zv+KDBu1IeV9oPeH6TYDZ82DBCj+1QL0ucB/POnH3lXCfzF
MWiDNUpzEtf9U9fMT0rwmof4x5zkQQ8rCJx56G8zOxURw8DZWfNWkiJbKk7Ofi6S/8Lpilmz4Pme
IEYGdzw396QpOG1tw4TDBRAhtVf0bPkMeoHhUWI547/GDujH5Ug2sOG/opViRpzQu5bwNbudmsn6
hc0Pp/X/ogKDUSot/4oHoCJMhWsTIYCImjyy2P4C2nTKiXpvKq+qKxe2I2iVRRAbKntKLyoEwpYS
5m1Rk8aJj6+AmN2jsMcNDM/A/qJpeuasho3KHQgaP8/TRgkhM6UWCkoLV3Gwd9Kw/aPTQ8b0nxph
iezkucXIEizqLNEFiqefcR/ikRHultOVvG2cV6fwQV8TlWTQ9Q1b/ZzwVarE14Fxktuo6AKiCc2Q
FUp0+LKPiX5ZLLKTgYgkhOD9C0JK2TG9arArp4ngF/3qeOO/0S2Ad4qhwTP8PDi6OWvVn8SwuZJ+
9kDq0p0cuu3h4H5xEJ5jBpUOKmFFhKXsisUegmZYnDgcQE1E2t//JwsriDbQ19A18KU045nSzV3N
cqsVgp0vb50m/woKzs+dLayb9KpH3sliAOJHGYCUjNHwC9iqjeOTt401qUWXPKYalw/fg9Rnc4WC
ayweXnhnJMzMj4T0xzkn4z4FU8pmrYtKJmSd/l8wrNu1jz9RxyDpKRqNx7CvLEq0zLBbcNW7Xdni
2UVMvOlmuUUuJ6v1uZQQldZat7WJGZM0aaun1mXmZzGrdwFzZOWQJCsz11fTcFQpMhCLMFET1akj
91fboiv/5HP8OUK5VLy1gsuncG+Us7p8Qgqpl/N22y+IQi42Pq++fuDeNI1YUvGf0jZCUPJKe1O0
jp/JZ6kFUoWaDgS6LTeR4rUrXCCzJt7NVNjAn0ZfTGvYMADsF/TEiek9i1st8Kat50grE7HgDDJj
Z6YkPhoxAeqZ8GwtDuQDm7A1hWv3jm5bsaKDeLYY/Jtl6fZ5Cup5yg70keiqPGhiud3TAjqYiHk9
1mZMwhprbGUAa0ghiJrs3gG59eGMyVrjgE2haJAolBBoHeE6xijHLgqNLW+UR9IfPKLPv7fjhL0Z
M0iT/H9wskr82TaDZc0bW5nAb0pBN66na5paNAnsar71bpLeb6VtNleOw9xqmdWskkOzeM3GgyB6
I6ff6ygpg+lvDTNPyo9A8l2CG0WYMcIX4rneOhP7vHlLypwu/jvH9Yrr1IFewVCr72zHOGyg12vX
9QYeqI/dnIXiwhcUgMRbzAujwexHhm//Z6huZug+Mgdt27IVX55ehkQLD6BMDFdFLgbDgyb11B82
xSbFvWVhh9hmACFdrs+d5hQhf8TSqeXPA2je3g+A2E8fNBDXBvpGVsSbc273Wgb51pUxPrgTclVV
KA+zn5NRt0g1JLvZ2SrlLQ1HqEvtHO+HnMNujKbP4HS6Ft+ZL7a56yw6Pr9eGGMZuyonsDiVhXVJ
deOAA6wKDOSlP73uHaYd5soNBY5B825XPFh48Fhfhs+JE668FLCiqoxluGAabz8Lmxj2CChBMISf
N+FT2iEm29tjWMvWFqhiJ4SwEJrL9qRdcyD5L97EgfyYXkrIaFgniMEp6W8IO7+IdjGgonLRMaTs
RNjaFOAc2jFIupPBQiOCzGcfChjhv4uFnx4O48mhLfQ5l2vSLMKYXpc66mIl0FIj4M/GWpxPWHdd
MeLd0FoZkwS4YcV8Ajy7x8IU5FCRc+nyopNIgODCCniv40NqpVLK+mkF0kO+eEdsCcsSsuffdc8i
HSoA4jaZCsuB/3HqIQrglkPidqVnAuc2sCn0FTo5oBGjPNjiAt14TRw8FktbJrHQk1SefXIy2HD/
kgEhluD/CMFr7j5BmIQ120qt4v+3b6WbVnoFzCyXfx8KPoTvFvBUaG/SUemzdGZlAcua7IY+Gaer
xz4DGZBwVTRFHHHY1/q5uuQUzKzRUuvfr4CepxLP3a6dy0J9y7CSnwmtOBXzrKQmhfcm1/cU4/T9
+nATHinI9Qe/OronrQDQLX26d54TgNGFGLPNuTxIpH7ZqYeMqpyK9LgkuMxPN7JEFmyEYhtpSBRv
RS23RzKDRt6UEv3uq6szPOdV+4IznZZhq2NPqwayBJg4Danzme9fUD9G6/P6atl+BmSnbXtuftZS
FhtJTYF2+ZgGzJb8MAZdF69eSJFkHJRduMEizfnPxokk+whR+PZwMeUn7kyvK1YOXxICwebQE0vm
ZtFoDjyrevbExim8+IrctNBOAAbNNgqdFpKnmxC0EWvmuzGbMWA26JggDWyfCq5tllW2O4YCfmre
u4rQ8O3LsSLX4Tx/rUCMuzT2RtTUP2oxqrZD6IKX+rGznvikBJIAoHrcgyrVm2wjl6UvEvY7WPCL
X4BX7AbtJpugXwB7o/U5xZ8aM3z3feRfi3NsF3PBsUxjEG2PJdJO+Y+Mn0HrR4MnSSZwolg/7Yv4
WJZDW7tjjjfRzrpo+VUF/D4Go+Zi72jOER6zo0XWyTJ/oUvggBefVpk8Nb707CxkXVMr/a61n2+v
srKxU2YGao6l6rPJ3P5J2O7Azc0XbkyJSovnSyg8Zk8JdAcIJIOuyhAe9QxOQmg2IXBD372C5I19
RC9QHKxB810KX8Qxqct0s7xPtRnqHiDtVoW/S/xN7GxTWBUvfwLMBbOszCUMK8JRy6Y2mrKy/vmd
zYiVBNkcAGKGEipX4l0AaESwwNSBmYo835NgcloCm7m2ku/gkZG88FMcZz+B4Ai2oQU0B0jApng4
oe/AFYG4jJTB901xDVr1j7lroKLJefWawLCwMVDXtETAedvXpbOgqgQd5dYxsf5GkWaD4tEtLkgO
aXMZ3+eIToZ19Zf0Q48oSMX8cm+eCftAUH2tI6TJnwDXgaLfehY9lwLCjIGvWcH7zEXv73dxwws6
qyVNM+OmGEA80nTj2jQoOCgIQas+W51Pytc/tkBBCEXIupryLhIAb6jHNKuVQA3jXGAsPg/5SXst
ufAXD9ytSYAoL62/GOiPSnPt8M1eH4haurP8g7aoIj+tZfBaZ36nZWvnLiIYE6VeY7GrBT1lGkNp
JUiWaCQ7s6Rul72yVbD+CqyeHqd+0xw1xxM1hmdMJlrdgeOce/nFH3FnX1SyDa+rcFALUFDt5pY/
qg9a/pFIbShXsI3QPxaUr/wBTDqtd2gi0lLoi/UoKG4+gcELLjI7qKnhhACfQqkI76XR/iO6X/lE
X4ieGA9vObSTYHRwAs5q9mYJkjFedM9QmRLTe0qZhBAm9pCjPIPUaQXeMaZrXV7wud4SyVnvX4WG
tDHuCP91kGtgLqMu6VdzmzdONSMNjsSovnW6GltnUtvjyy6dCkYmaUwUa8JyBLlPn8kpfZZ3lj2C
AjRrQjVyZtK6LotNY6GuqTs1yD58DAEg9iDy8NdlzekzjihiKv5Ei73cIoKGHhn2Y7Vv2766euV3
Lorxm+bK0wS4IOfeHuSBEUHCyXeV4hmvdJHKQEfBd4vm+Am5zuxeAM8zgEcLiwhu+T/+3ht+4/Uh
imN13BbrQjbiG7l19Xjt9/3fuK+cvkBTVb+CN4ih87lgyDdtMMUdo4exPwhoWFqJho/CoDJOGJSv
bQuHT6s7f4j/MgP9JxWjYeuGGS/61ZTVk9isp3qbEIJPSjeNIjG2MkNvEyNjA2r/hepMbsqQLI9Z
3wuK9Oh7BnhNDGGQINrLQLazK8SU8J9wQ17xF6uB0oadHZCbCu+MEjOWiWU4AX9APtHgXXCZ3rgm
uhGJ/BD/3Ylt5veAM1frZuwK+5ar5girhqqLX98yTP9xxXFkvwFY3ELTDW1W+755XrRvYuEZAlie
kFjBO5nriB0AOyEKFLvBvcFiyu3tQm1Jh9HlS4a/9KEE83q693/6houH0LH+DoAWd+SGEjZGOA07
FMq2vPxY9ls04yUgOUmp4BDVAWChKEIG63in93L36xXiKNyVvZilAiTmdwKYNvjktPchVLG7gk/C
BbcymYqzk8t89XeE6qp43SwpjOCzyvY0AltbSTfh91cEMNNKPFRmWuZwczSua+uhx+5jw7kIogtR
L3NTxE5b0u/axqw+3TFaLqEA0HjEik8PuLywQFfDyYLVoDWANl1zctcQ0/1BUUPtfU90DrPbFVen
neB3Oee4Q+LyqZgY22bf7YdMi1UTLGkFIR2bfVljSFLRTEfP1iM/RaVdUyFyZ/qCa4D+jl931A6D
O5pnHpAPXoj1q4fkLTixH4clkIOYFtnDUkMGTKdhmElK9ob7sDMunJ6qTXJ4h6H+Jvz/AKdSwVoX
SoYGTRnGQQM3AeHq2dVPpqCCdqx7FL0flOsXCB3u0uK4AsIWvNx7O3Gk21hS8E571OH6cZDV8GHL
6roZeJC3YEG3u+tFv3m3h/WRaZmjnmZRmWr1ubcFZzx2IfpLduU8i7zjLL3wNDYu5EAfBuSYvVz7
eHMGHjyxZFAkZXfc70JfsG/EDcqvFwAso2doRBUaivK9Bu9HIi55UufSXVQJ061jmOtChV7F8FEB
9NTqQcVzoiTgYQ004uvmV1UcaPkSOUSp6h+lNjWbwZnjpDOGFUg/A3XJ8tW9TlcTrX+FYg0hsM38
DmqttrqKyEj82YlrYFDDAQNXog4HNHnEquH6gZQJjJYVtcVXMhuMLoRXCYftCPgjP6B7b/HegyKZ
KlRKC6g/iUSmzuO2Z9n/GAFBVhlWu6RJ8SGP4e8NmxTLgcxtAUhoNWRhUextHpBu1onOlbkpEKpU
b5B6y/aPVIsl2J3i3Wef4LqRSvjlG5IbYkFlMApg6/8X/5/hbaWYmdHo2O9OE4T8e1NtkftpsGmi
KZUo2RbRbS4EeP7qRi+lDMmcICNZWKKhjsJLWcaRWjh2vOX/q+ovpqICeebhozW31isb6oA8lW13
jR7FC84QcHIaHFR/LAdQWZRkOOBAXhqCjLGKJfPJEbPhRqAYCV+xjdgw8s8szYW+omFG2mG2KGWh
RnLYSmc1SUt8LOZi4rEg7S48c0CG4X/p2YYTcQ2f2HX5K66CGx6H/97rHfZnVOpBARsyFavpDK1+
3sNSfofSPeUba9XTBXh1dXAu28xBUHbfPAmEmANsqzhvBdaDqWcM7eRlpDIIX3Ji4tyjjyPNwL93
yjieNTKbrPrBHRULAzVe68pvP1cAA1daRg5C6cwlgjOdglxZKM9gWgPnnhHgqS+DavYLFtsUbmOM
hZJEfNl451klufZ93M3rH3SCJ4l8n/njva0LcjLXBidBDkEq6UVlA5Blrk6AkELbhHO/hpBEOfW8
TuNpwYXIwY0D9WWOXW+1K1fDFOQwFO65l8Ocp2g3MZ+A5a21Yn7FEO8ojnb6r3RWTGC+Fr5pmOqn
jzuuNT4DxnuiNq7b91B4v6mnYy/MC+YBv6LDUSGbxl4qa4HBNnds4QNtwYOEDFcCRzUCHkEp6DlT
cqrKjkfjM4L+Fr/BBIbW/OD5e84dzLo0Lzzpv/iJqpCjYZ1dhj/ur9xj1f3mUJxosgcyq0yRmFBI
jLPyN2tkZlzoM9gEfpSZ7TZrsgrn/8MU0xy1rkjYtiucDcIetOrFD8AEokpaSn4b2wWroCom5dlN
aNirgqxp116oKrFVO74FEU0l2X6eD80jDXSjo+AB8AwBnFcFsb/1Q5MM4e4fAI/vVrKDzaodT5sN
ILZCmx6hXHBHGDktsfLwb9ig773DoUYKn18Cdpc8pRNcQhOWTqnERQBrN3/pb/TKtx44hRKbKHc0
OmPpW3fSMpEs+IRaMLkpnS081jFwBY6M3IoTTVsWMIl6NFyzQ343kmUgA3iTEzxO8BbVcf6qAU5Z
8HMJXmZLORC5aWI7+t1oe27cWnGYCsLF3ZyNjYd+dSdAI9immRUqMN3y20rSoEO64AznHLLsASPD
+arpY2EygPRIPRHL66sQdiN8ir1919j4woaYupEPEyGqc3ZmL05+RYjicWi7W2kyXc77YYT/2JD+
TcYW5U8xNclKrikPanPMmvt4ujNJ7zP5t8viybNMrEuLBOqMwISWXPDSXSKb21XJFlVu9O8ONR7U
Dmd3Mou+8BnSPGcyXNrSO06lx5wZ4Y1+fn8cCTuFoLhH4vQORtWAZ4Wrbm9C3oaAcSAFoWvtm0mC
bhyWGJYNjYoBLsgveVGu9w/op0a5GteCJCrbpSsxseyzzaZ2S0x+drJXT+6o/NM1xrV/dWU2CU1B
iOvnxcHNjm2xhg+rTQ730aEgfdQxEKjU6ucaWGBAsKM/BdogmDaQtItl3jG1ERxusgU0hHxVSEqJ
0MJByCHma7g4mG3d73g4TnXJQnhek/lz/P4N73kJmRPvk00CBaqEnG2LC315jebc8D8IDcHDKty9
Ee3DGVIxCPET6U2QI4VUjiwQ3lwak598ASh67/iFjO6ZStDTHOq5eUHOiANTkjCeTv3NRaT6pH8s
lKzjL+qzdaQh8BsQEHc7Qgz5z7je2hwDKUpqdNOpEHVX82wYgm+F49OBzK8YfCE/8j/hsT7iSKK3
owIvQ8rV+7QIXjF+yo2VhwM6AQUvsP+/NLgu/Z8p+ogo8HjYRoAdCRKBHJKRwvy30+AuSVFsmd+s
UriZVfBbRnhUrTMXDXEijlwXExkjSyWYIkS0VSDyM3CSOiKRq4YNa1gc7SpTYyzECZcfNrPCa/J+
uqpmE2ywXGH06bTchMbD2Cqr6dRwAPtxxwjY8vYfrYEBkDSf4utDGanMPlcoDwa0WaFRIcEA54A3
JZN0R2U9aSDIHwTPimIJGFYsvhuRTkR1r0bQTXHSze5aiBs4agePmbTkDc8TH1kklX3V6lyFsQv9
h8xsTo+byL00DEw2fiOOLFNa89q5DZZxX/xBQgnC0ZYbBrEZSUQ2Slh/vi1baaT1embDKoJDo9gP
O0C7fnJZEMnA6NO8okQuGry4ASeOdE4EF6yqxyi/hNG9T5ak0ezKfmgT4xbvvqBpoXLReaZFU/n/
OFUrrFgR9lI/KLXQm6GOh9b5lNT3qSwZH2TqjCd2WvrxtAcED9HLGp4MZ/QCbJxYF6WhxkRBfOGX
eDWI+jbkBqDTqizbOkJWUHNnOe2tVdR3FdCdwUMV9Wy8nOiliJEzvqmP4PR3uuihFCbSc9s2Whbg
EVcFXXnYpZAIKgAAIM5Kx6a84xXKtTx8o14vb3tfuT+ba0bNdycYd1wlHfiEZ0NapSBp4IhgKCq6
hp2uMaSTbVzKxq+JmdYK+ZecX0P6PfvXtDxYM+MESQ3Lz3Gjfsdn94Hz4I0izWV3NIOybEJM2iep
VPkrksKbeOLO8VRzkKL3lzoiTddUIeif/QdKI9Nu6HtkINVNIt1l+Did8dGMy2C5qJARfyzBSPNy
60PZ+bkcbwO4Tx/OpmbIzV2rpCOzx8t4Lch3A/N78THPQen5OiiJ8TbNYF9rW5Q+/Nt1g2433kg1
N2YIzctqce/Rpl31E7qJHL4aOUL225S85XOoon3ANtYlhGdE9xru50lYUYAazfBQdtmqt223dWcE
xRqTc22e94zT5pJP2Fo2dfbfQVR5XB/p9pLdw4YoSjizx2IUZuwj363URhRl8G/XKM4OoWRTjVKf
4qzNfZkuN5jOSm/ZZG6bwUqTNa7KmlsvdYckAicn4qb93czWo3hBOvvVtdIZXm0zpHRm+6dc3SjH
WyXGl/YSo/Mxl4VIEEqWUGz1SccV6n6C0wNzXtAhj72Ex6p7AP5pS0Ay5PoUky53kWEgGus+o9C/
Lehs0w1aR+1v+IvHTGtGYCjO99KMvLbeNrD/56Cj/O3PClgVz/Tb6gtysrezmDkYzhPGJl6XMy1p
QMaj5QfiHRAV9/sWMbRgGVNZKx9ufc8ekx0oZKTaQB3UDYiKDMLcum/zpkw0iftMl2H8Co5ec5jv
xyFJB5cXA29N4/AstfkX6M7rbr/LksX+P+KGxzxeQkAkiBdbyVZy83RjRrjyF3unO7WErDBVtQPF
+0zPxx8jXp2sdC0xttvJDhDksCQQ/mDJIyTYH5Q748mnoDjahbd6SUUC2oTHGk83nSWUTT6Ox7xI
LaWCS/8ibNs+raSK8Bw1c4qp3PWFlUbIxGyIgt8GTA+qB9557xKZMxDuke9Zr2AKNAbYpiifHQws
495LS7wPU2Rn02BLaDBzPruZMRp7IAbJborTGfoesiEwEb5JhtqXY7N6aE58vkEyFjPqZI0Y839Z
5JH2qHa1gBfSyJGL88Shg0Jt70m/9VNdfs6PcxAcLCyZfjEOTwk/x3GJtXHP0Hcmzlfc5wuXSl9F
rmHfqvcReTej2QOXdodkK3nrEMJQWeQQwwk5RBi+jMCrhDYCMIAVk53Yb8wFJ6B9fyrI7FvebVbp
Fu01c3gAzpa3liK94UY1Ltzr4pvJcz+R5+gKmEWWULxvloFBFtn4aT9oT6Ahc8ed2nPoJmIcLKBG
JMfsCS7p+VNOxO7UJEG/xkhWNHeBtWkz/VBZqsMAAl8W6MMTBGZFVYXkFDBpAUQUjgeIa51YkysH
rZb5BPplNIfnw2Uye0v7HEJAZdk6sBG9iUaR9U0u1O9mxXyl8L2h4z98RVVIcApHVCbvT8jFSlVr
f4l1WdmZ0jVEM217XRVxLJbKEXyajPLIXwmxk9eNbx+giu9G5Q9RnVc8Xyub8kKDUmawTdKsF1iD
iDgCtYe9DTKKuWV49D4HFiVOPA06SWfDxpMFd1deH/jJXd5FWbj1hjuTJCfB27xQ9+ns6LYTlwE=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_x_noise : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_38\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r5_x_noise(15 downto 0) => r5_x_noise(15 downto 0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_39\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0),
      \fd_prim_array[6].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[6].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[6].bit_is_0.fdre_comp_0\(3 downto 0),
      i(15 downto 0) => d3_net(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_0 is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r6_x_min : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_0 : entity is "ip_peakdetector_xlAsynRegister";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_0 is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_30\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r6_x_min(15 downto 0) => r6_x_min(15 downto 0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_31\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_32\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_33\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0),
      i(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_1 is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r7_x_max : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_1 : entity is "ip_peakdetector_xlAsynRegister";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_1 is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_22\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r7_x_max(15 downto 0) => r7_x_max(15 downto 0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_23\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_24\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_25\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      i(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_2 is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r8_en_pkd : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_2 : entity is "ip_peakdetector_xlAsynRegister";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_2 is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_14\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r8_en_pkd(0) => r8_en_pkd(0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_15\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_16\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_17\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_3 is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    r4_x_delay : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_3 : entity is "ip_peakdetector_xlAsynRegister";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_3 is
  signal d1_net : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 9 downto 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(9 downto 0) => d1_net(9 downto 0),
      r4_x_delay(9 downto 0) => r4_x_delay(9 downto 0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_8\
     port map (
      i(9 downto 0) => d1_net(9 downto 0),
      o(9 downto 0) => d2_net(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_9\
     port map (
      i(9 downto 0) => d2_net(9 downto 0),
      o(9 downto 0) => d3_net(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_10\
     port map (
      i(9 downto 0) => d3_net(9 downto 0),
      o(9 downto 0) => o(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlconvert is
  port (
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[9]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[13]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \xmax_i_12_24_reg[15]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    D : out STD_LOGIC_VECTOR ( 13 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 13 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    \^d\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlconvert;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlconvert is
begin
\latency_test.reg\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg
     port map (
      D(13 downto 0) => D(13 downto 0),
      \^d\(13 downto 0) => \^d\(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
\std_conversion_generate.convert\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convert_func_call_ip_peakdetector_xlconvert
     port map (
      CO(0) => CO(0),
      O(3 downto 0) => O(3 downto 0),
      Q(13 downto 0) => Q(13 downto 0),
      S(0) => S(0),
      \xmax_i_12_24_reg[13]\(3 downto 0) => \xmax_i_12_24_reg[13]\(3 downto 0),
      \xmax_i_12_24_reg[15]\(1 downto 0) => \xmax_i_12_24_reg[15]\(1 downto 0),
      \xmax_i_12_24_reg[9]\(3 downto 0) => \xmax_i_12_24_reg[9]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
begin
\ramloop[0].ram.r\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mEOQc1jYdukOpbND7qhcLTAfjQhkN/Eb+RKIafT5Xbswe4rMMUUenKie3BYa7qDkpKrdEVWJHNUs
d4dVNenSiQY8V2kBFChktF1dfBjAkniCGfPqLx5QxAU0Z6+9IGNsFIvkmjQu64LVCiYSECOPujRa
0Kv5bwag4YsIPuCNk22VRhukiFaoWb0UvPzATMhzk9F5F3LzUaJXxdypO3iK4FeNGa0bBoKG1tXm
5jh/HgkjTotVEF7VGPPt34dwvA6gSYETXgShuJaskGNwdZYkAqhSr8c471byXt6CsK5Ncs2KlpKv
q0pfDhb5KQjrYwxhMXmG+t7m96LIT+f3pc9jCA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
14Nqh8iy9iAL3wGHz2hNvOqYVspmjfxLFBMSduJCauMC1EfO7xhW7/G/d2iNIkAcL3YgcWTIjpjj
x276GxLvQZlFJYOKnu+WfVyAtwIAmPf5+HUgaiYtRkkbMeHYQ6sCutmydwT3St/3Gh7KcccxCOuF
13zYNWXbKl3Af6XvTBhMi1sJ73gaxkLCi3IzxbJR45sXFfdXG2uNdWnOHjt0w8EwBcMOEzcHEkmm
T6GqWCJNi120iaqacCcg5w5SyrYYSVF0JfCHpktIqLRo85e2gESsTtF5TR/Jkr6ayTUSeuLpYYlG
No084hirHs+CMtXa//sl4uROVqqzLO5zQXVgmA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 3152)
`protect data_block
Lspro1kkZdDz6BQ3yOEw68nuQQcPP7lq+RhfwucsEiS7HHxK/lSbqOtK8uNYn1jRrSP7RSQTViCH
hxiFZaOBNfLGMJu26e1omhyuXikhcrXtVHeV4a2cZRjscVyf2RFBORV4NsqkoEcxTl6Np+C/LUhD
ozQHBBbtGBxVqvS6O/VKhWN3gWpqWgz+PVfgWaAXNaIninigg4SR3eAc2D4mCTryH/Iln1Bv3Anq
o61WsVqQ7QzgG/+YZs932ZIYXoRDvNqtyrmwS2hI3SoMoKZ2Hm53uKDDdTMTD00IL9YoQ3Ciz/Go
YK0gXX2BQYy4GDHOdhDX20fqgIwBpR1ZU2F/ulzBKQys6iD3G3nZAfpvCCtLMd9W51ZauCGQTIbH
HlQArBHYl9YIdVSZS16I7jTy7MRtNSon5Rgm+wWqcb+ylXm+jmps+XRhR6Dx0CCmMCGBAofC02Bz
scK5v+6aBJ29rDqKhUKPFYMrBStOKP3NzrpFwK4edRNm2WGm55By5XIxAyFluib6D1lxc1OYlQZo
FPodmZoiEzkkpdrpz8+zUs/C8I0VD2rjya1PLWZmdOu9tGFSDWDkLsUt5lm6/QB+tWgd50gQmriJ
4qeIZdSCTI/XfxYcwhKWwlEZ/fOM19opcXQ3rNrhVGCHE6J9eOsJBmf7GLiQ1cxI1cWxtDLrK8AQ
VrU91s2CK63+hNoJZTz3FvXXNZlUIL2vUkq7KTY2UORJy0k7d4yol3A2POE9LIKvWkWJ3xw9R9u5
2Vytx59h1pwVqt2R4D3LlHk+RYdpPDifXTIP2/UCwefSqZQOULy0sKBDFpf0VgQ01aotrXBuzBgP
s2rgIA7PV6mNz3M7CA+UFTjXdtGq9XYXprVnti+H9pLGALP10teOOTXXX1FrZycVdo1Qmzkm2bGY
XfOQA2OKNzK/jt5uqniA2ZXvkOYL6H8VlZm5F/7+iR7QIYzd+MfFPPINDfqFugdlG8RUkR78nIMV
A6AZ43dtmsCmMFKtI+ql1mMmsa4oUeo3Evoq/xcfIV0nXDX46qDDORRU5WK3Dtfs0AXrqQ03b0zc
yN0wIvQ4vR4AE6z3gUaMZPi3K/nqw52C+24sYCZj8Zj6i7gnej6SqZKv4ShR743/sA32JQfqYbD4
fho72/JuXvaMHeZNyMoudJvqeBDPwbA18LRlW+PIhUWcl9f7hcI/hIpHuJd7+FtHjVCaN8a/V1xP
YUkMIqCXaX3AgXGJXcLMz498vTKZe96pXfC3FWtxegDz8pay6SubstBtzNh6Fii1oaRztzdVi6Aa
ZlyNI41L0SAJbXWlSLnFLMoMpEeB0QGDY7CP5ZK9K6JFTE7/PeI4PtMqOnZZjJA2MjRc4ZD3Zlhd
YI8i+OT8uaL57TAH2BTVO4mlzXa+aeTZ2CAymCh5oED+sx3yVrDTi1hDkHTZH9J3LpMEO4qA+hc8
cGA2QKDd280eKx0o43UQ/z0MyYzKUueoSVsRtkNNWr7oHqGeE4ULCfeyxbunzIi6TcjyorNuPk7G
ASp9VBb3BmmNom33peq+dQtwYEjm0jy0NqkzbII2Ytkk6T7Wp71dvcyjl6Zzf9XIPAVPowL07Cqo
dde26H+imj5axN9Ne1csAwrwcSn4DHsp9gyTsuKkqXjhOP8c+IwqaTxYlNw7xV514/1a9scyzDK/
gRM2o53wf+Ddspgk0c22+9zfXj+fMNEQGj1OVvRxo1HDhEpyEVrgzSwHeNq4paerWYAqPnrZ/bvM
cYjOuBiVzg5YYnccSFpUKZhCGejyv3W39hoyMXMUlDUnUtdwE+V/jdcNY1PmKHJMb83kTTBKJB7I
0FmSuAfLhJV0oUB/zIiC3tRNnv9OLpwLRoK+c4VyppuScW7aLt5K7d/SYzZDoDOavf8QJDhf01D9
JhWT3LN4RIAPxO5OtHswkHM0N9+8IJwg4QlrNqcjEImd73wjTdJ/Rw8s3ZZVO/stzHTFMFeemaga
qQZFpN8d3WnxijfHa23lF+M7RGb/fw5na/jBmDkhOyU78N6eia8nTQbOjDdArSpsOUgO/TOkpY3H
OM0Ji2wjEk0FjETjkWzI+nOb4sY1LZan1TkPt+qkC5d9wjF68fjFETqnHJjpK5LO9Kur5qD8SHcn
YzIFhJgj/TwARNAVaPnGFrUchVWMX7gOOd16o40AsQ4ptLU9EHOW6BUgZJF6qorIdPXTtzX0IM3+
pLjuUGKKo9dro/6+EOdVJHS/xSAwe5Hza8Dj088NpUNMa5aDoeAdKEDaSFBYTgEi1rjzfQsrFyWL
9Af044pEdpK1siac7K6loCzvh+bm7fYJP1kkK/jbK1gkxjYMvpVrAGkIY30lmA6wk6tnwD3Q6ZLN
wzuhU9Xlhyf8sLI2Ds4gC0ELPMzQJX37aM1wG1vz5p8/P8sLt1pfzXoWFMRIcX/WksW/RtzfkeXS
74gJGUYzjX7hlUDlfr2QpoyWajhRocJ25CFPiqwQQvNAqCmNLqb7C3a1DXLxIxDn33ge4X/iidGj
l3eWRAZtVo6EJjPRQMrlnv/j42hMtBvxQQX3N7/39So9wltcHy8ESt7Zm8pqD9FfycWOhwZjRI4J
I1ca1WbNoulrqZMavNYa17L4ZZU/d4S+TB1J4LsfhyjWx0Q8Rie6fSrDW4auvvr4RwSVSbxrtyDi
0gmeg342MDH8Lamf/0wwOa4Uga8oRP0MGYsYE7jxIMZ09vxkDvrlNp5O7cwZNecoLx3nmH2JcAR5
S3cy42A96Fi0EMOMA+M5IlffVCRT/mGAVc++gs0xtiwpImGgGJlD0avfXLU6YIuFWJNi5Mu6qldG
+3oBZ6jocC591+At2vReAwiEulUWbj3oGUEYLNVTWbIf2/ltSpoH32Smv3c5ner9ZZ43q4B97qor
zXFf+JszHC48YKKAw9l8mUzyMIEqlcXvl8dgX7VJY0hrwqCa/mzay/5VsFuV0whE9C7RvHSXfgsk
KnGQWFXFPKNo2FBhe7LZutoHsuE7oyEBnHqmWrvezLnD+hOa/vfkcSIGk0Ou6g2A0bx/rdsdpa+I
eDsPDX9zGMyszH7+PS9fF8/B+AKsvFeQnyNeo0l8bOCAiRp7dXvW3wS0su54xzcOMRGuYYyz//TS
cHTbdZOn9E7ZVfeHiCPWwHhITgu8161uqBzBmyqvP3w6xLS1D94GE8IbW25ckNQTu2OCKVyFfHnf
Ek2JWNpuX5N706WuaFE6VJeSpWbO46MHQFysYS3uyaVy0XH66wZ38QCXBfqtdFQGJv7Uee4Krexm
vyYeHXtedhNG8IxB1L7xBZkTDN2Gl7zAFqnNzyNnn5q1ILUhvW2r/fTyY6nRqdllP2zDtNoSLYK8
LVZK1dya0RvgDd//QibsdZMJA/O0m40tuTo/IGBzROK7Yc90m2g7liAJ0m19Lirrh7DSpZKNd3x7
jj+IILniezfr0Dcj99MRYHlzVdil06WJkty65Qih7eKAx69nOPyCKJrQpzJ8NSN+U4shnnqwqlrF
CKk/C8Ju1MhHiguvyt/ht/aTaDmWVcMGD8Aavf+L3v1P9MP1lW4vLh7opHzD6WzdFZydYuG5TSzD
GePEXRsq86Gk+ZhCo/n1Pk7VpuTBwVvoV1msErmtF2C3J33ZAcYyKQTivBHtqnLuepVBavnuacK9
s+s28T+I2TUUjjEpKs3mVsib5xKSxM6yhjCCXzsILhtVn6BHSZP67WyyCHASLTJgCPfEviZC+z+P
GdjBQBAbHxzOFBp3M5aWI5zjaxpXicRskK6sTpbaedCqcaie2DnBG9T1xi9vlwqV4IfzYy2H1t+Q
1yTz/zO33iPQ6n4QAHtWlL1rewnMVOh4snp/huVSW0G474J6j4DhGz8bUbe3y3UBfbJAAbOz0J74
ceUoAusAqTvJ76PzJxPUcBJ779F5/FUwecROZR78xriLTKT/ULNBVHjb6H1v0ndORK+sr7hhze+G
73IzzUoLigo+jPUFKsHHYXJzHIvC/bDfm8dY6KJLhPj68yPLYWjCsK7rQtukd38LFO/ETL6DC7Sb
m4Ij6aYqeLTPw0W01y1Z728qjhcfDLmdk/+1WmoNsCQ/M3w8C35P1uivqXQd9O8X092yf8RNqW8t
7KUN0xtWT4sd4iuBaJBbP1DlYzg7HgXJeyUAwuAQh6d93Qv1bLF4V+Qx9u+xSVrL7sbZwk0ogRe0
w2z8G3sFc2tJuEPhe7oAlsc=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_format_dac is
  port (
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[9]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[13]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \xmax_i_12_24_reg[15]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 13 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    d : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_format_dac;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_format_dac is
  signal convert4_dout_net : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal fully_2_1_bit : STD_LOGIC_VECTOR ( 13 to 13 );
begin
convert4: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlconvert
     port map (
      CO(0) => CO(0),
      D(13) => fully_2_1_bit(13),
      D(12 downto 0) => convert4_dout_net(12 downto 0),
      O(3 downto 0) => O(3 downto 0),
      Q(13 downto 0) => Q(13 downto 0),
      S(0) => S(0),
      \^d\(13 downto 0) => d(13 downto 0),
      signaldomain_clk => signaldomain_clk,
      \xmax_i_12_24_reg[13]\(3 downto 0) => \xmax_i_12_24_reg[13]\(3 downto 0),
      \xmax_i_12_24_reg[15]\(1 downto 0) => \xmax_i_12_24_reg[15]\(1 downto 0),
      \xmax_i_12_24_reg[9]\(3 downto 0) => \xmax_i_12_24_reg[9]\(3 downto 0)
    );
logical: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_logical_80d79f42b8
     port map (
      D(13) => fully_2_1_bit(13),
      D(12 downto 0) => convert4_dout_net(12 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
begin
\valid.cstr\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KxeZPxC+vTtee6wYlaXzd/8chqkhEsjYu49YmF2qJ51aRUiEHJnB3Zmp82q7SZrrVCwZC243FLKu
N5a76dEVo7iU4rbvhrZWeQ8Q0tW7oBHVXDkrzdK2cNjTVnmT1Pi2sJzCaacYjUXXNT7YQ7pizkON
Lm/u4/Y+Od6v6C0r7/LCvVZ0jnBq76zjYICmDELWVXzTumFjQFq14/WaWc+Rkf5PJYln2fwppx7H
66rzv4Ppnq+Dv7EbZNOLs6O0Led/s7CfFSgAZ480v7aNcVyKAUZ9PgFuIDIb2TbbnxAZIpWw6HAu
vam3z31Wx97sxz0b5YtveD3dLYxNnrVMUvEZeQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
O385eW0/Frcul2f0ObTUAw1/5bSyCYvezDmg4QpBvvWTPhZlcAVFwvXM8/0ztkeFRt/BR2jAi3b7
WHaB0VjIkaMlFuIqd04CpirpHoT8MfsZP2IKJMYPcBnEmHh897gMuTsnUq1O9/fvIoAyF2l8XHU6
ehOxDvqbpIVexV6zA/A1eLGqbYHquGi5eQBoA+x57fvxZn/bskeq8hWECmD4ry87WpabLotDfWqs
cFOZVwQXiKN3CH0OrC/HbLDk2o5QO8/TK7qeX+9yyyWl6jM02DiUTkutrZg6Enq6WHNueM9o9cGc
qfQGpC3Chnfu7bsJFQiRh7wDBMPt9fN/Fd1Knw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 3088)
`protect data_block
Lspro1kkZdDz6BQ3yOEw68nuQQcPP7lq+RhfwucsEiS7HHxK/lSbqOtK8uNYn1jRrSP7RSQTViCH
hxiFZaOBNfLGMJu26e1omhyuXikhcrXtVHeV4a2cZRjscVyf2RFBORV4NsqkoEcxTl6Np+C/LUhD
ozQHBBbtGBxVqvS6O/VKhWN3gWpqWgz+PVfgWaAXNaIninigg4SR3eAc2D4mCQK5B++RKsFmSJvi
MbDB/AqKTaD+D3HuOhwW1+c2GC7x/E7WyNjUG0LdkvPaeUQbgAvvaNkIhmnWnoSskJ0AacRRDpLM
WpQhWpGHcdGaPIcx4p9mBNqIcsfUjUoDeQaP6ghNSbLcEAvlqyjMThpDZ+/miKMIhaSWjnmLGye7
Ujg5pvMxah8FzBtxeDb9gNcl5b1YHJi0GKS8rVwxmMJbV7vRi+pwi620pvND5kBMMGIu8qzFLtYv
lXES1ZoD7SZ0qP2hwEzrKPSgMFdG9jLPJtxytpcfY7Wrg50gimh2ayx3vs1fwSN2VOHOPmdFdnxY
SpnMrCiZR7hs1WH7ufwvivHFWQXkIhcWz7tnbiLuPZlIK3WTUg9NTGZUcFj7yTFpZMSEhLkTlzWr
Lu9WC3/crAiKDdMRQBfZUMUzjcB+3A0hVnzVOCGZXgSURhepkh4eOy4P9TfJHw5Qgc5rHut6lvfn
d8Fg4yxVFI9mRsWdecSVdZ+zi4HL7Td816EmOTVcmGfRBhbOwZ9sVwEliUEpG/WDT1FMcdUfzXKY
QGU2tCLhp8cfOQnLevJ6qQ2qxGHU81BrKp3Awf5bmqa2j2V5FMMGiCvLNSaN0FMrZ/jVDTRIvhEw
xpsQKZ40H3wpWdHe2GaaVGxPtj6mcboRyP95sMPLauH5tTlIXstUn8aJd33BCx/ZA822+24fSdFW
WqpyqhamaRiCUMtjJNrthXJgCX24e/AYPEzjUcTqd7CSuJE/+J4Xe5zHQIN+0Rgfml1DbiP7695r
7W+YJ3MsJIPRmFa6TKFPn7eq1ZotaMq+Jg4yzSHQ1p2QCyVrlJDvjLLpzzZY2BxKEE/bTZ8wGtal
kUVG8iiiz4mMwZ60GHhbNdAvAz0a4fHeUpbCYmdz7uvTBK5uodHrxoAjIeYoPP+S2WMzKPf9Zofb
OuWdia3ijPdeuOQWL+418Zko1ottG5FhxmF8HhktoWpzayak9vFTwYIBwgnIekJ8FPmL52MM2nDq
aoIR9BNBjhj/Fh2ftYEMYxaqp0T4AJYqCHQ3Cp5YfTXzaZyqAhAg6DpG521o01gDtJaWAIIxSjaM
RzAWOvn/8bOcmc3GFHS6qAUGp99+MBt2zj0AsnV0Ct1HLJRT86K/3OzvI5ZpCXeyiJE5VX9QncbI
wXe9awUKckHSG55WGvFtdvw7ZGkbAAL5fB5oxxg/exET/h0XPAjkfrX5N9wY+BTmyqF6s8fjCqAy
VY/2a0AV43j/XPLe0Ojf+xGTWNP5Vj1PvBY1QvaLExQdttqOx2FaA930blcNOqXSZ/Q26b6NF7ZU
NAZECnOOI1elSIHBsOiRwwSDWOW2wULvpN0+xAQas2ze5m1e7a26tCBbJ0zdyIiMTD+IQw8J21SK
6bPi8N86xqtHadBykteaGRwplwb18SGG7aYyK2PqZkqNAUWDb9CD3XugJlw4VF1Qu7qkTMD9+UKy
S7womYpjwHwgHDGxIbmAw5xoFjjPUWAvd1225xUHqmQIpybipwrD2+32cdNGJavnRBXd1Iwv2CRz
ECnwI6nQtZAqoFzoq66DNf2PhcbHYlEV21eI3ZiiY8sKe+dRVwUSQODw/GWkY17ypk5zcTV8FhZj
30JB73gwrXpWev9J7Hjc7wJn3NNjTtkSg1CLH2Rm/HnvtflKLhHdimD+q80WDhhMr3SddUFPhbh0
dq9fNDyI27fOYj4pLjxCkW0c+CY64Tif65xinBSuKhY/iY4bjjgD1pxakQkx7Nsr5LsMoLGZfGk6
3jpo/IqnaQR+9AbxUv7zaWsz35FGWfoJxQdKDJPDGzOVleuw8CEYTHppAM/1pWoUjiKDv0F/j94P
7YXz5VyrSQVGzSvkatFndycxHdIeHXYCnelUGuQRNBsies2k17gOrywd3tC1ADfiNSXpu57CcdUI
zST7+b9LbtfntG7kFE4dIGPzrjQz8FW5wyEGVGOmYM9vSxSXAfO/1SwgSqDFQW9ZEJw+GHcHAp+P
ud9AokEWr+F59UrOgdlje39tn1rdUY9lx8mrMAFKO5Exp4TUVaSrPtvRZwEAT4rpYvNDB2fnIVbm
qT3erIy6ew2Pn9Tg4avQe9zcDBpEwu21FEZ0FSyFPddIX31WZNrNV8jk/0G2/ayrROv6phgJL419
xxpurRFI2trrn9anLPRRcX7yketH4CXCED22402bDzAijosfxBizf0J+z4yx2p82Hnhf1Ui/DzB1
AsBurbCL6c+SSfqGZjbZ+BKbSYDyKtGd5/VcIV/PQtb5jOWcjNlyhl1pBxUrTYHXF0VOI2Eddwte
VoAKtA3aURWWTWGGHgbZ4VZ1/FwErg4qCl8KhElApRLlo3LCowgFKQmqJXzrNocqZRVDX7W1caWS
55aLpCdZSsROEC51tVJh3q+EUCcxPYElCW8eFJBowcqroPnyDF/Kw8pxT3IbXHzpIoSTHsH+xaaD
8cKiuLa8jmC+8z+G6qq/j9imiEq1ZcXxqmBMaZVdytPW9V+68td4Tb9cg7eNedzvMyYRgIW//9sR
WIKte3hmYGdWW9JSRATMQIPtkjZuM3dMVwAgnTmOfOIa7zqjbIdDIdBJuYQyRS1qph+Sn+oX5+vb
zW4kPIJ6Kq04gRYI85ds7MQCphMkukqhbEIg29o6y/Zbgdy6i99HfXm4IO1Kiop4fNcGUAQgoinT
npWfNwwjxxf9awsUIPR4zYVZ+1KxsdclgbTbhiSrGSTwbVPmT8juFGFg9AZm5JsEBAWGL9vfx/DC
R2Ph0mQ1F5QS0s7EXPH4EftdoA9AgYs2cVuIk2RLYnCmPiAlQYRPSTJ2bXM8pj5DwMvsMvjBi6d2
m/EtnBxbBWacp9Z+yavfLOFNM1Xz/zoNxFqn2Q0f48X8RkGIYUlnQsb/O/I5RYWeryzzbLD27tqr
9axUpUuhHqH+/gzzCmpvgcptHURKlytpAnuQWg9r9w7FC6kXhAE8JyzimTasmFQdwP+oamai47Q9
HvUffyJJy1zRRiaEkOEHklbTqAtOg6kyWxct4E77qrkEfPsfOswy7PviDxY+2m+RACyFdr2KvSzH
391LuX0gMNNzgdpbMQtMnZ/3vuIzVaZiTMNHQ3zgGM7cOYaIcuW2saCT3ML0W2fia+moFefx5qwT
BFW4BPmz039L14A4/GmSV2jqFtkh3YR212ok2vFzBLYqMp0aE6N+7xEcLyYwRqgulo/P4qWbzBVo
O4xTLgtSO8x33UOMn/EYY4DoV8k4vrAsX/VoZ2eUYSepw6tnlXYfoGSDRZTkbDTAL21ZcPddTXql
2M3/VVHs2tQnO0bHmsevXBhqEYp3dxdUTVD6gjahL/dIfZqQFKk3cIK4rOuw0WTPCAW9ZHyw+sWi
uZ6QqtdbJyZCCu8/eNQYnAvBgrISoM8A6ZiBCP5mGHIMsGEnOgeiNDpli5h9n9gFPxhvX5afdoZT
kVWKQJAMtyizMTQ/bExLpPucTgPLjG97cJWMlOBix4pt7B4G2hI5BAbUco/Hth/XNCxLZsc4WfL6
q8PEtO/x+fX1BKxY+vxPVu8KEyebX2ejcimsQHa+g53HKGyYsMmpQC28n/TFnZ1vFVga4hxt8FgN
nTSfxcSCNelTL5b/w7I2nQaWzmlN+Uoj53V4kqHGsAc1tj1SY2zjltcVv49GGdl6WdOdxy6LNo+8
P67bGG9H6QB08g3qt5uTa0HQn0juHpYOERDTMJlq9Vy/iud7xU30R5R2GDGdFaXNEqliNSEW6JCM
V8ZS2uup5QXXU0geNZL7awr7qF7PVQ3CuBJTxqsfFs0y+6k1yt5mTAJx2DZI4Q3zeuNRwZ1oUG10
/qcB7t2BGu1Ms7ioBTYsivzwfPlqLctaGRQ4rvMJu6K0iBejvsRct2vwX6ce1hpAdxOJu3ZQe3fu
X2spoHtPsCBDMA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
EySeB7Vfc5NT68oPfPsWtq1X+osO8STRl/6pCQCVzG5C4LY1PVbzeL0nyV5+K3FxuBnZE0786vua
mJSBMR0uHPuVsk9eknvRedRzVahQ8E/Zpx7WsUKvS9RWxcpUp4NKSmZUyO2EW7Zg1uCk8afzM780
YAlxUCXLlSqWe7EQ4vAKehY1DMjrsQXdSfWO63NLGbg2i2OZO0J/3dXf5UsywOhLXkgA83D2TDq5
NdvznOhTZDB2jdluzwTDBqnhsujWW3LSpxFFW0jBIrlxWZ48LWR56p0GM1a5wPprGaJqxPchAF2j
RMcl95NS2k+PRvgf5YQp+SCP3A+ws6UXtGAj7g==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
b9vQ6VnLu4cbeikssKoDPUfbO+rAyFD3RxB/aIxfS2yNkFWG2sJIsPqftzeG+XuypJGooSCSD1xd
rjiBUEzim+yAIrRpStROlI9hLRCf+B5/S889X0TdxrpTXYrYrP0bYVG/7KVnenNOZ6XaGNewn6EP
PW5a/has9+PoLsPyZL7orqgb7qucSzbctP+HmIgzRbY3fozJ86zSigkvseeXCISDMCEG3ykC8X5R
+DJLba532TOjmgl1l1IM+KLiYqc6fO0lwJ75Qs6H/ERxCiYxz3ADVpwOiRy4DW9MzTK0dgn6d14H
yB+2qVVWYHpxYkb1RDGfROqtotZZBbjsuleheQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11952)
`protect data_block
Lspro1kkZdDz6BQ3yOEw68nuQQcPP7lq+RhfwucsEiS7HHxK/lSbqOtK8uNYn1jRrSP7RSQTViCH
hxiFZaOBNfLGMJu26e1omhyuXikhcrXtVHeV4a2cZRjscVyf2RFBORV4NsqkoEcxTl6Np+C/LUhD
ozQHBBbtGBxVqvS6O/VKhWN3gWpqWgz+PVfgWaAXNaIninigg4SR3eAc2D4mCWZqwzLV2snO+l4S
GOFVW3CkA1NFu7yQYtKJyJXVimqv1I4V748/Ax0wY5SQv1eiGqZ70NvZy/1kykFEhHZ7DVqs6EDZ
EKPsiMQlHuWdZEKEFnhwOHuqS1vXmqWGn4nbJzblfack0CoblKp8n0yqypPpzvis3jy8QbLcUADb
mYwPO6cou7mHBjiz5pqb/YtNTmW1X32HJuEoPZ97WTpRERcTAZemcvjnrx5t7TeiCR/Owi+Xl6fx
wK+sVQFYPUAQxMg95YbwhQRo2bMcGNlXMNYeHhNuAliS14zM2bS2pJHdKFFa0yQuc/9pvL9bNNJZ
i3MdaIlnPnAKGe8hBuTkUTdBHwO59QDFFmg4Jc0shoZrk6kFRvLGjEya8ePALO817n4mfRlouAXz
DXOdZ8pXYsLRmBKpSqURYUpUxHvI29hHi41XBidMh5f9Gsg5vEn5YSumG+SeNrm0BfTqe4XUy1Lr
5YQ9Z9SYsg1ODcnTqn64fnfJ81gStATXw65tpI2b4nCGubiuYj3TitvrwyOdQEQxXYgX/9hZajT4
VVmNwa44ClHVSROxddkE0W5abGM0sn/E576RdQ3hKOPFEU3BqKk6BOssfdUa3v6KQjVs1f4vz2cN
igsr04kotLpOknxhJqOZMqSJrxb1X/6ID8e7rBFmGRQzz4ON7j0Sh3/AACes6etj3QNEo9ub08JZ
dSDFQr9Hzn47axNSAwPtAyjeGo0Qc/zhwtx+yjx5G6hrRPf7e4StyVvAzYFuUvel16r5idLwi8A7
DcVvWzqgUOEWXXu/c2z7/+tCMB7EmMNp1prxLmMjggMPWaIpZsxwCTniiXcK1fws51guexDpOufO
vqvypsKCu36V/ylCgo7inaHpJVfGynoBgJyLBBBMop7vMAdiBvoY9nPnDE2aYQROG51gg4WAZz2+
/uOi7dGJtTX092ZDgbVm0LAgqL35p27GxagNB29x1OgnfxjI1JHQttISLppVDcaRL9DrNbxfGr6X
/HFXCLWH4ptX0XNfPMnReFcVVoW5gHZAPE2JEsNtjoUBqvDSnPbPAir5tmTo0E7JF8MgEnCsny4l
jsX/e5SLrYILWtdoro2Avu/msM358PutDpDP4WS0JZLwEkTKsTas/b8Dl88qoVsTsZvWU9etoCdl
a/NsPnjvhWdJLZ7wHydZ2gyyrcZnlXpxdb915XtiQq1o7N0gm9za+tmcBX5RzDXN6uBEj8NXmeho
oU5HnNV4knNn22HcPIDcaK/ZgFxdxNQ9wAXvPLOQNmaet6XOXjbxIZli590XL7Vk1IPISZoWvNa5
i+m92zImqrF2HcAYBdR6KqloAEgQQIcXix3U6nFM4OGvRj67yrhR08lIs7t8OZ7Ojd6UfF/iYvn8
u465NcX/xUAeARMljg1Yk+UZ39aYSzaeQ5/hVtsIxA5V9ykR+5R5nqUkeWQ4NONjjIjlUjhSVI8C
1w2qPKK1YrbgB8Xn/ccJwKEtJDpMvjgt737Z/ppXfOOSCkwd3jUhCi/K+uyj7cN7M6VWW406UCO1
MY1SYFAwhCVyh3zUEuD3DxygsJFf6GUZYLOcPiBeMh584DIFDSCG7rpFgRkgFhiwF+nA1i6urKnm
R+OZFq891ugY95B2BdexyqHJDQ3MPoiZ4tsXtWg3cFEMEoLPxLhiJb81jfPq8FmvnmREILHCP28E
05RS+qnqNwrk67JiS7lO8+YDJ3QnKUSic5P9HMJ7eaESdd6Q18W2//QYkMiQFIf2jqm7HZgeHbO2
VNvcIy9o1KXJ1mYZATu2h+bsnRfaXD79yN5n22L7zGHxXaC5SeeDH6+5/h7udldrFysYbLbN+Rqr
Ukoqr2jmG/yJp96v7u3stw4Gz1SJkBwPJqNJnkgGk1dT84oEk75b4LIXT/fWGDVhsVy6K1g94Y2h
CmhDIUmLo6nFIP679VV13b2HPXKCSb4XlalOtUM5rN9vmF9yDb+89ROruaJtnjkirzd2m4t9JcfE
1vcuHuN5Ny+sy9uLzmooPNz8T81849zHyclgt7SRPay3ebWXUUO0d7cilKjlaGOqJjbH4heGRitv
g6WA3b869RRhj/s1TcmTADhKEjiWG6A/ZGtKm/1L+iLj4hmON5hJ9hYPqRBCAn3GWSCidpgTt/3r
psktyIwcQkP1fa0yXX7QdO5FXSAD9Z+ZyNmLMFFYjSbESHiEp5vNgR4aqghD/E1zOGmTNtNy22nm
C6Lgi8DgaA2ttEJwolNVMbMEJh66LFmP0M7EIm0rV1DJc2cRGYFRDHRNvYxaNegsZZ6NJva5RJfh
vWLoeBglxt+yE8FxaMzuidYX8Ys7hdBLXG/e6dqCGcW0HyW0fEv+EG3b6Dt13iz8MMNL85mK7wI2
x9dM0TUM6KDnAIWVqA9Ezh6Rl3IM89cAqf5oDG229ywADP1+0dFQoQ0kRZX4mmPGHPwkEdUGEGqM
A8QzFXWqLDZ0Mu35D9wa0MFKup6tByjaujpDiMIfPtsXSXwyUosMb6jFT5krZo2yTDqClxBpWuvn
Jsl5kgHgKiorSk+xmAmLYAeTSJD4UXFr0LBIN6/gtJFCYogdyA7TzYQLeTTYpsCmBF8tETbz0OUD
6RIYhGPZJqCNpxtg1hpYaeNJTtPm6O3lsZ2/2fbe8D/5v62Ov2zXlavGao9AtuCKv/+W1XZERZg4
Gmq22h5muB7tCAqly7ZmIpv7DtC9QW0+LlTFQWmwJtCjQjFE/OONqKXO9m9mtkTpsRaS2hfc3wdY
IudGfMNojabDtIhuD3GU+hJJ4wdeV80d/mrjZwkJw1KsnexrrMIPRA6La80MDerPOohU09gVlpHW
qo6b23LxFqarH3zuox93Cz9+7xUEmYpRUiGpLt6xPRLDMFqAw91rahDRIWISzHhK1wTZxdXK6WaL
q/9BBVJY+mLBAbxgPTl7W37oHKUxNOQEiPM9h/9XxMVBOFFdMnfzHWsqf/aSRMyoXQ/EOkf90aq7
/RHn4/OJybq7+slTG640SBSSS8NaMnM8n5qyyCoAsgwBZqOpGKXwP+h/n9GgeTqFJ2/KiEwJI0RP
2evfAoVmyVecYW85ZdfIvYUh7/7/gtolMjHJ6YAQT4k945PXZK0vkxuvMHvxgbm4lhwWI66vRORa
61mxUp7/e+TCRQTAchjQiQoLfLksdgNGLdRtBM/L3eiMzUhlrHztAky191tvaCvsdx/oLrQ5ZwPx
ZEyClxFJ9H7ieGXJQqcm67p3UHBi4CGnkOVgBfUlzpFJIoXB9PIkPIdlz9p0/nIkZA9mVILiwG+A
kG0khO1EcO8RCZqHp+fI0pJLM61hxOX8JHJxY+RsIHkncCVoAt+8itPVssqQtlGL7DXMrShsR/Zb
KsKwZguxHqRvdJq4Ng0qdEQj95HswqQ0Ys6R0+CLGtk/TqhEGQMo8TeqRle7oLJ1I7yF/IHzBsKy
ARl3lLQw69+wOO1ISHWDnmhhSOD39LKX1QO54TuzXMm2YufiuNWKe7AtkNnTAplMhN5HE3grb9S9
wiS4+lmwxyFcF8DDtQ3Bug6MOgfAQj6iwKjZ5x98uzbhTsxsbZ+62xasGLbAkGDmUFrTBDc3ae1H
NmqV1jTGcMKfqe3JehG3mQ+tw9sNbcg31WyH/eGxuJg2VFd5L/Rzut9rKDHptBKcd4EPAzv3+Cs7
cPWxHWOF2I0RZ1iEwlF1wjhGNLPQgJG8IhG30Aplq7ucYXWaRex/PgA3t+bN9J6BzptJLbQObNQs
UA9qpPHhEXdIfO4MinEN3558rZcGspbYedb4O8kqmgO65PkLgmXjB1CN1JguYHz197WmlZzzm4UR
q4yTYcFzQlQbZcfIjeuqPiuEc24BWx8kF1eJDJVpKzka/UaAtIq/l++/+Ly6Z/P4mdLuXhDDRytn
+P4NNOjmALwD+0IWZ9p6nz21MbF+krVdvlQDTeZHAckR80S2lcTMBI05ngfGv3VvxopCjUArcAsN
oh19PH1x1Vpyndyugx6c4qzYOXR6ba7OCF6RYCwLShIK41lRIGgDUZ2HZZ7NjhUwV059gvBWHLuh
yhm62yVmbtHtakJBfhEP/WK5YGDV9OGi4xYVQMuyTDa/FqEYQ3cnNyyxSIkb5UMs28GKYzGWDFlf
uNiRZU+9ghEtjFBqrVUYs/+zNWvl0S++c8V4v1bdQt4wjX7PEFbGKNdvDXWxveIJt+MH9dxfrOwB
vNUdTOldo5M7/r7T/8RbJOu/vrniOGdTIYKhRnTBkZIzItHzOXmbh+dbOUZiCmgFhUeVKK1l2gue
YmF5fFFettDO33vcTU6HYGuUu1Qfn1wMbrOiQKyDa5x6GfJcwvR3T9izHnKaMZ4kXO3DB429Nerg
GiqLCv3CS3jK6q4YwFFqytXwabAEgZd1Joso7Tw0whLUe4LWxVK/zXKMghsyoaFm0MgsZVCeDwCz
sDSVfUEftvSxQfqltC6JH7HQLhHtEc2XkbJ3daaGSilSNJ9weDT4aq/IWRhyPVN2PTVJp4XuKWuW
IBm+5RIBI8FAEMTCUI0pYNPdO3lgMelg+f4UDoqp0bfIfttEYRja4JLBeBini9EsCNU4CX3FhM76
8f49zASbDOQWkJ6Y2AkiRRS1ccgNNu9t9Yq8hjkwQ+wEACRHHYsZE2WzCJUk4QewYdQoWfPy7fP8
Fox3IAZdTzBWknaYPz/QeMNqb6XeqIU0LUDYckvRf6ZONo8o6H8rEIP1kEAzw3wOIVim/hJWfDxC
EhPtmFX1y8W+u/4KtNxnlC+UthlMlxbnjrntkj6zadt9lje5i3TUnD0x3t/ITMR4JUSfIQl6MVN5
Pe9G5yNRQ7gHXWKbg2fmr9nGofRIYAfxzLWCQp+D0L4pNrH+8keoXDV89rWobJr8k3wlfSIWFewD
6Kjhv6pCUl3fnPhqaueSgXao7UfPfq+ViE0p+edJIKOUp8Zq0iBMkYnk3PG+Mw69a5T2a3SKbxK3
SD4JiWlYqAHM8aHHPRt0hG7jWZHnz71TLv/UGEXrc3LhL2e0dBPZVsrNQAQU6qCgwvi+18n49rVK
Fe0insucvmSw1/tveMc0UjRKmHQXNtc8E1yd33cJhO5a1ZGBI0BSO8OD2Jr/JOIF4Cw2A80Q97fb
79wTcom28MIKFQ0quY6ZuTjPYQZz6fYEF0AtNQ/1X320u4GOowN5bKdPctaa3xG1yJa+qlHmqEd5
bBDeI09HdQtmb05RNdRso2zntLGKryPSLBpbX59jxQLnNUXfM2vXWanGPHRAs3Oj0h1cqZKpHLNr
dC7NingjK92uQOYqg/4Eh42457f8SkI2xfY+zmc8mFNcdD0O57ltr7LCTVmyG00iGPeWkRY1PYpG
fuaIzwUXxmTnqpFT8yOBOLX0Q8G9dfixIb+KFHyBO+ZPvz6QzoG08qZ1Cjl99ZZ7+Su3MyHbE2+7
2IZv07M95D+jSCsFQKhBs+DmOpXLnNQKvsVjpwN6jSof+zTzSER7A701SKsl5/X+gHQ066kRwd2M
H1FdFNMqk6LxfLkkLNDOjnHn/L3u76o+UVasNZhQj+1GL6QaLvgPvQPfm3KeWiNzE69nsxgM2cD8
9vX3mPVkf62nVGLpWi4fNv06HhSGbgqz5SXRwj3AKh4ZCeFi0wcutFcO9+TTvdV/fZKynRnhLOEE
TlTdWhNKszhDtsSrTzLR/8o/58fyIuHv0jq1xRXq9wjV4QRC3yZy3TEaOGqT9Q5hmWGvb1UhkUrM
rudkSkGDcrV+6bmSEDmh2PGyVFMj9CGqIVc0LCnnUr4Ookn7Q+a8JYttuKZDB56+NAprKLqDNpac
Zngibfx6c16+iHAymq94YLl2qByUWD4UrFZpWF/zAq1VEFVeHCVP2u8XlnYD89rowvu5DhnGmvO1
GnQxtrvdDUZkv02RB07ty/zX6u358wwzKWpjYLCYHkswKQCL+vsL0qAfZ3uhVYSxuTNBxUUMHDDZ
nmBM/WuR/kl9BO1epXO/mpzpXerPYJMPDCRp3GJsFIxBidDo5IhXuOqX0J7JMVJ84PdUEC9+K2a1
WSuI2yugpPJRu9DKd7ybWDFSlLdVC3wItOgZ6WGz5O9u/qwwZsLeORbPMbYkMTeQlhihOPPPFxOd
s/YVzZ7lJxxvHNHhHJbQUAP1MsGwfaFj7KeVF1hA2QunNvctc06O1jGtxNyCBj4RaTC8fhAGD982
ij5AF7CROgPOwfFUFL2kSdyWF8w5FQV6e2deb7drStsF9TafCjVf02KaNOIAuXGrdHsKvsfmcywB
YJJ/LjKsoSmeO4cESKcbHfFNQPsq3bzJwz2Q7klw3Q6Dr35Z+HLRZlLQg+hnm7IFKQ7yYDb/v4ZH
VD3DYfs4Ae1HeIYLkEEmltc89+tsYRpnevFKPuPD+RvdEfRlrmnk2DvM1alvi/afsg/X8BcBfuT5
E6nOw6SlWdMV8iPDw1NIHdlvUcn+rBgJIw/eo6qXzlCd4QXKL1isauuHNuc8I9pFJfhJA5tnEkYX
LVY5B/HL0EaR1uUsNdUVEwBEoL3bAvQQLO1Bc685/CoQnSLqBBlQ9jlDMjSCsGDe7hm3mlmF1VoK
XYl/kZAYX4xqJlE/f/NNa7Tn8Von+lQVsyNHDZNBO2yDDpSodOmFKO8Guh5P11tva/TZ83JhK/Dx
bs8+MMwRVAXt8VvTURfHL2cNiQgRXYIRkLjrhRfGtgLyO+z6MMrmdjC+ML7De+K1OMdFjq8wmbCu
8Ci9xCBKbM+uz190uJ0iRA2Zkb24fBxZpAdYUD0zE2TWoDQNctoUegq50Tw5wQYlT3aYn7ZJ8fKc
DSDINRDKxQy3RMmVbyUu0Su9Y4G8aKBfhCv7SBtdRw3d02EFMYNb+99agkTgYtDaGePMA34cO+ip
d/TWOZrfJ4Sv0SOoLzIgW6+d4lyDYAM4beRNaz+qVhGMJJn9bWAvZApoDDZ3SZziJZxKHX9qi01E
JG1COcIDE7SyGjjCNtYJiMbUvUHDj5QqTp+0SdzF9vKCxhIIi+zCwqyQ1q109DgMQwg1sji1WhT1
xIPgbdyUJ1wI8ve+70aWcroxcuc5m05BkWN57jbNwcdmWKyiRlssHSfWkOt8H7PxQ8+qsQkSHbYC
UBp6ESrTKk/wnW7Z5hwFrRgGf/QkvAE6Vs5f2JSEoP2WXRCTFzg+OKemlJX8Huux11QiHynpZW6p
USmvjUzo4ui5dCcUXFiZ2jEjgUJNBqsw+rSqmN1IFAblGwv+K5R2Ru1j/XGH2jnrTSoMn4kN54Cq
UqYJ5uMe4ZI6wpbeGHFdALplZ6t41D3qU6D/oYL64R8gs8Igp6Zvqg/nxX6yqmI6FsLNl36geCzn
O2bH7m55srSjU2FV2OjnY+a9uxbYseNF8PA3IJboTfZIEZ5Q6+A66OPIV7a7d8Om0vrvEeiKeCJF
UuZa981s/4t/FYuntvK6Vj3xRni+Qk1ST80BiTnf8fei301NentYm7jZFt72P91jEmoXVMqMdoxa
DCL0lV3kMqbbP/DiV4XS8eZwTmuuT2uZw9HD7GYMmayWBtsPja4MpVCc4DAfSTgUCbkboLl7SjeP
X5xffMrKorGrcWcDsqLutvtWAkzli4ysZR0eXUoKLoBeGxfc9t0dFGRhabzrj+hZeyddk1g6fzH8
JoWrcE7D8HpZbS28Ka+06TAPAGBDlMYJJQ2hvw3wtZhrAzz4a45VGv+eZpNC+lJLSKSKFO284VJH
MzhwCUxmtUjPuritNqVFpPXCWCBXqKthfMyA+sZtCCHTh0g9Yo9ACMvzNGSg9iTSyVBixe4pxyVm
mslOi25YGWaNLbP0wc3Fyo8kGpwvSLf6mnWIzmhY9yysw++woaqYTaKAwUjGQMNHNbOKFGZKRLUw
t4aODU1CgsvNWT1H6sqFo6FRQbmsbnFz61CdyalRohz8IlBbC7R6B/KLaPkrd44CrTRGjjdyHGfA
SpKNGvQzY7d3wmTGUUELzaiDMMYuSI+GGFJey4q8+Q9XoqG+iXg46ScujxLq9TQMdeNPAOZhPMRF
43qNFntrBgpPVoukeCq2J4UZ/7pMWqm1aq+rwn3zUTN0V3bixn6nuMTvxiTA9f6s5SpzYZznB44X
7c6Bd6UmiV0mG4avEkfJcJU+mZAph2xSH+Nmu7apThfh5vLQMsBkKni/MY/sSFWnuf+l4tX/P6Go
6h+UqyAcivyB8xYL4flxYV+UulI55fgjB/VmbJdJO1xtZl9J9N2hBs38Q376nvgQLgDocCxjFUnn
ptUVfsbwKtFbtzXzh/KKHhZ1w4SCOGp80tRPFNtYAtiicVtFL/d+1kD+49BdxhIblGHgJ6KdV62V
Jp06BtqyM8o/VwmA9pPvUEUHOjzWYb02FODc2Wn0F6b/ZGxeSJj7BXgUm61H9bSX0Z+Mb0SINmG0
x36O/R0Mgbu/AprEBYUHuSKP4eS0AG1Y+r/FuFwZaRrEbwl83j6P5yRmU7PsfeZQYOKYflwAqbiW
Z8b7IHd6USOAjLIpikHUoWCbUkeNaD72MyWwnPwgX+QkQ4m5+Umq8zz0CUeQCyKJS4IhH/3d+gkS
ygDG0+WKaWCUciNCPe4O9dJckFLKXBsPRRkTPzrhMGNukVMHPR9erSbolnwsk69BnwcupAo7o9KF
8cE1hHGvIYpUXohH5KVhNK4BnzropgUKRqJmF0gid4mZS19qH10SEoHrD/X12ZkRDEBOk38qkTzA
lcbzuSxFXGu9J0eTppcb7GtQvhFz3MK4RpQpWjcWyTOVN8YXzrnNVm4gTOxizbNx6Mh9FsT17/l7
KhbTXFJIF2S1saF4KaR2DTGMo/jKGkGCSmw8Br1CF82nun2jgAZvcudKR0SFN/3GdgHs6GuoxhuE
XEOrtAWAXGTYLggvyDQB85lLVO6dh4+Z9ZP6dNKc5QnyZxFwY+dPqlPaPWUaKHT2mDFOm94u2Mnx
MrCcOayNlnzB11mibo1TJwNS433W6h3aDvR74+h4Xkf1vRu0pEPzPY6Ku3xZzxUxQlBZ1bGCAd6i
SuSbAYruyTPTjkTHMe4DkN7nsoxDYqSAqF04oOcSgttDLg8T2eWxAYDOR5fDOg6Dl41kmbYYUjXB
wG56bMCnCjpuqwrl3flhoU4voqaocd/X2aXh0llYRqFoUToy9gUGiUlLyNtRStJ3h3lGBgOSUaPm
WaHxY1Zov+VsRuLmyWBWnoo5pjs+kQ2ZU1z5uIpW7eop8v8s9hSYMS99puZdBWtVQ+xhwuP/FOeZ
GLvodmdNSvKQp7Lt2SV2eMPPZbxMxN5GhRn1SyPfSLjHtXQozBV0uoQz+7shNMqdizENHFEXm5Du
QCFmqdyhnh8OXnN/DO4XHeuww+4gvT61+qx5GlQzX8d+1i2IRIqQrm9N/94cZVCrXCZ8VKC5TJdB
nKUNwajxIPed21VfNIk7cJ3cLxckBiwdUHT4acJhTJ/a6T7YxLzlhNMIcDNy6YmZ2B2KUsLirjl/
q3Q6CB7O4AvRJEfXvNyJ71WrQyRZi6LqZ3wuy/CU6n+z1TsgpAJPCwyFCNOWF/JKOuHKViRRrhJx
WXI/61PEPmNHimLW6IiSP8m0zQlQtw5DvIM1qTDmOTQdhxzsM05EPTey6up2rltft375Th8DJMVT
BbA6HGLAree4FwpYj1t0qZymUNY6fCpVFuQ0FtzNs8+V3T25dvFtDaEwfGknw6SeETdeMuMTZ3wn
2m/Q0I2iZgcGxI5zWUiNeOgfFFeDz80L2mJ2b4coqAmDDk7YkQYoJxaUCbDfaxquL93coUBNqU9k
2gSUx8VbVaTuFuIaK/2aK9otTfUDnpXn6zTr83jD6Kp9dMqE3U9thdw58ScM4FKZCBmbj8xzSCp1
ilhXXys/VN1gfOk0NxhkskMjs1N+vKkLyumP2kk1Bp91auxWutY0zAOCjd/trThqdYnHigHsaKr9
T65SmvZB7w9cmSUsna0MzcSZmHR4VgKIDv9DsokxjxMc4t5+u8xfqDESaMDHA5NJLheuXk+jXa7q
s6sxZtbG8tnmF6z4V35RTSJwlbE4Es7+XgUP7nlYhL61yxAkaRKtSfqpAgTfsCYpFvcUfbotDhy3
Nil4psLeJiwOKjn9N1H17rlRr6X7fQcRlcSlt6PYa1X0vyBlqwuVl6RT+Tb0RaOOwSeUMo5ig2jM
f7R44kNBUz0B1+ehEy0R2/ZwixBwBxw7h7Fi22AsITs3nVifh+85E1enjwTC0dX6IgCGWbnyeS+4
Pc8Pi5DeT02B6/RrnKv3pQVXIob71dat4BqkSmRxAzFr3FXdlsAF+1312Py46qp1sq5H20909fd5
73D47NXf6Spm59hMuO1rxwC63f/Z2JkBCTIB/moY53QySDMQ4dXg5dDgvH0+w0jP3xtViFRCec+9
OqCzOoM1UDmvoFL2u9znL+9WiAZFv+A5pS4+/yzBVlmCQN7LapfyGMWQ5ms8tW27Q4Hqn6bHyjqr
UO8QCdyAybl/cOsIbTgvDcpzqinlMahmEjDYeHSTn7tEI9IEmg6dyAc1n4CAFraut5McMeqEq+Yo
J9/knBJKah/hMYeKl/SV3CPMHm4KuHPOdojMv1w7yuX/XrvGvqCyhHrygAPl56WS6H37jDwboMe9
lbdVZKBr6o/ptxOrcDNbrgz7GeLGCc5lmbMMBDoEo0A0KOuPO5r5eB0dpe+UOmTy49ArJCEfz2DK
Nc7XAFPXkIR9YCLwKWOYyTlb2xAPtksMGRGKfAkbwBEbP4+gMSr/YTmLKa9MS8GbnnUAznP11D6W
bvFeTawQrHxNYBJSRb9A3HzF/YiJROIaiZ1HMXEGB/WGbRFjGtSwQU7BDbBn8FIrbVVvgKHYNbyO
EOfbYPRL/2QrRIY/4FqHti32/qKcikl3Rv2L7QO9Kuw95twvPxjND5z59PvXPrX5p2ErJfcGpgEW
AHh2vKzowxYhwFXJajZlKXyD1K/47suxEZ0kI2VkXDmYm5WzRW24uCHJStBwSGZ4ihPVnXiIEpIV
e+50dF3CNgTbJM4momme3yudDUxveJprn5qhClQRp3dqAjzEktxpmhAFy4xa7xEpbNMrUfokZ+FD
AenKWC9MLbmB4tR4OWYkaWQcMss0ZedjvKqpeLx7egZqqC0exo9/q9Q3sdj/V/axxvcQJZro0+eb
vyO+8Ijgx72yYfKGticAqH3ZM9I6Paznquc001/2iKEG122MvVOjpOMs4/sZXTHAyfgD7mshcnoX
oyC5yx2BkVFDNS2uFXx3E/b0CQvOwQV/eRMnjK66PlCWDiYSSw6JQzYW1u3lwwVa72nwc+QzR18D
iXrYAwg9s2BHUCS0hfB06ec/LdFnY85bm1rJ/ptQPWWnUvvxaF4zHCuLAMh7mAmEPMKRAkE2nBye
kn4ek766+yfpkxy5f8UvuPQouYRs07KNrJigoJGJSYZZP8ZtHIrzy5BZUT2o9ZHh83VsLDWF0DKN
dd0efCd+FM6OqVOi1qh+zi055gQ8Qkn+V/JvB7Z3eeSeufGNvCmVPcsY9q7Eu6VvCm2UlDs8yWUz
WS8TqoveEpNTcuw2rpUIqtROMKVhVKEgxRFbUyg8pyeVy4Lo9h7rgFy6C4TPRUq/FbChm5TffHYZ
YZ1Fd+lZ4YNwmqHLh/Avk7dFtxce/iIMRZDb0vOYCZnYizWIPb1SWAqSRKOKQZuBbedON6/Xj1qc
DtQqfDnsUVl21OgEBm/yN4Rf5GI7a6/VfUaYHz/Gb4d2s5U2GyAJ28+jfEzj/8t+tptWpDXGlYA9
Ta1lqmG5Bq2pb0wUGTBDJ/U8KVGB5oZ1l106lReFWibvcG487Wjc+BN6Vyi61eutEylxz8o9Jog+
1QgfvlzE0tEUMLII1/AMpbaKLbFBlGblearjFAprm8Ygr/g2x6DFHYwNr1X4Jeqiaj1zGMKrXkpD
Wx0zmAbZaqEwPuSYho3O5pTcGl4pQMEV+1EbI3+3dWetW7CIZOZUvA7iz5KGkpDd+gaO3Nr8aWjG
ciJbGd+2cviBYKoWOcI/03XXDNcXWDh9qRvB+u/eAPHGHF2YEpqz/GXAo0iP7YiD9hyO8KXvtNK7
Xb1MZxNo3YLdN+ZeCPh/gCGv24X1rI8RPW8ZP0jk3EepJGgeH1CsmkJoUYMSAn5Vgk1H3YrY6RFt
crsEQjqY1FfG3qx7+opsAOrnQQez3xP3mhL3pJBp3CKnWBRhOxLYItG0H8CHVdd1jd6Ign0yDg39
9ZFxDmacLBdz9LktKTvDzF6idJPWOJuGhloNhPHzl3DINq7EyINQozbwyZbj6cogXwjLzs17SHZ6
vWj7LI8xz9ar6xK9bKkLEtZDBTU6ofSdQGBEza2lLmVgx4JVV4f57knq82Rf5QVFmjTl6VZ4S1c/
ySnMOQeXBqe4V/DJsXf+TbXk+/f984DYzTllEBOx+vOzwIXDWHRVSxdE+CdzbCZ/s59oVNn6akaf
FIMY3foddIx3Y7w/Dg0Z1wbRArPgz07RJz6PAqR+sTFbbG+vR2wl36X+PRJs8DhLnM+oUEDqe88R
VpE/ZUo7hZ9z7GvosF0GlZN9TQ+Egp2AYYNhfQ8c/x0gF47nANxqnjXYcEn3OFHCUzyhexhCVO9z
TWH9n3lQ6Avv0ie7XS8JFDHYy6GvnlEZ3sCufkO1LwT/Xoji6TGmTDwhqVIhiIu9cMks05gtnooH
ZV9Rg8rooNkguStkDFJVMi6klE0BuNSg1MViXBHexNpiXJij7GGG74bCXrKIPibAaJVrqT+PNxwV
KW1DwrHtGLWf/CZHhOvmPGIAAeCW/5i1qxbrU9/awQSDwkrmX9dbc/OhHlcZ8gI+IGyvXp99wA2W
UkYfcAAVkYDc8wYhLAiwcTV8WnE5qTG8hfYAFcHzUsvprBDLs747rO7to4lv6hl1DsD05VJvpV9s
FswMZhNI4+wipi57TEZ/OXyJLfLVsfE7f36bZc9Ij5FtNvL7F0YHViJ3+2WkobpbQm0vgQjLfiDu
4XXtGc16KqbVU+6fmRmAiNbJ4nMT4r+qvjZOEfqx3H0R8O+BISlNmEq+HlI6jaoZxEfSnmZFEjZf
pOoFy7oaePKWkYilLT09M1fpOGS8x4XPBESFBaiJYk+cpeFwWET1JxdDTc2sv8CSeHWqfOlnuqzz
7L6FN+EZ+60Pp1Q8nLLK3pAHcr3SMqXz/lHfZMg6Yg3w5ZCk9l4kUucK3uqGVSpH4JyyyaiVKJJH
zTYDsmX6zulvCdqUlLvOvdnDLzFs6neEV69Fd96CswlE5wMe4KymvhOn+nCDhu8CjYudZ7ggZL5j
8VQOyxwibJA6KkYbHhGI7jVQwLM1Ehz+TAF6eA8C7P97Y29JTQ2pqWco8+VTzY+07nakM4lXAx4l
FnRhxrEOht3/Op6OEonXWb/d2XiKjxwYPPzGJv0YBb1sfa1Vks016FH5kTF812KvfWKhxTXwidtK
Qf+gK8ZjUCLb0r2OBlGOSyaDp4uB4o4PdvKfp5jV33WpApTSeadfxS05NC/fWXj3qNyaWp2lSOAa
Y110HGYQgX08NpkwLf2JVOsNrh8otR1TEJAo4Alh8Dh/en6zpNZJ2BW0gwWiKVeIk7QfY5/8OVOu
6MVEG0ovRiuEB9CG1nJe1fBd0F5bDv8ugOmD/UnL/UIW7WcgPxgR1rx7ugcvdX0kX8fv2hbA42B6
h0CDwrm7J9GU7aGcx9Vync3woy4YVwM1YKuRB2O8St3D0/ebGuUVKtN30EV+9q5/yWVX6twtRYms
0Sc81UGZhPA4B2jCuBIo4EY16LrcvA04jYhodDobpMFKUDoOXgRNEV7bvDn90pTKmM0bm20+HHMk
M9W18ZA9zeT6lKZDXgj1xKBmI1whxYLmukLvqSaG2gfYTUo7YVdngQG9tIwKWWWhJk6/i5GEQvO7
EAPaWFzdPmpk9AhubZoDwWrAzGJeCGOWCxbc9lBJufzW9Cz0DCaZVskq3z2efYrszi14ilxlU77q
wSqEfda16dFzVa7h0l1PQLjzENyXmScleycOCitFDA24E8YHLXwHKjpDWJZfxLQyb4sv+/Yd6NTK
yZGX77zWAblcJ5CA+768NrC+m+LydcPXz+kX8vP6avwVwWR/AnwPxJ5hqCD5aF2s/GWbNOkyQxWz
QJTjKeIurtv93DwpaI+PWdsVO2aM3ujg0ISmzezRRiO5hMY7LsVBnwMFAXqhDtDDZyEI/7BGY8Ac
vHKpqHBgYmOcsML2Ah8P/54SEO2OTL9eEaM+fR0HPDOqruxpZocFtcKgpk5epAjy+SBhsw4HmAsv
BZ99+HeKtyvjdi1SY09vIaK2UVMHjkeG4f1HVDwSAqIB37YlEpj3A/FyQ0nlgZhHVOrhg40QvYU2
wG1JDwCMV2S0m/ntPZJUv/qb7LeRaEM/aH/ksgfURPAt4UbPNs+IixUEDsVV5wT7HjnTMZVaEJ9d
ByYHWqVi2QRSFDR5F5v/SR7QuSGiaA+CNa9+YzCWmHBmCr87kWa3/q8aw59c6GU6IDFdvFvsBdTf
6FaNv63WM4fsUaIR3fZIwi9c6tnGMXrZ1rq/czkgpEUHcZihxsQg0e9ux9rpLFMMs0X/9FUn42Jc
y8PTUU+0HjevE0g5KcIvy9TfrLGKj38satQCaMeJ72wXJuecWkCiIKCAJrKTBhcJWAZyBKjtelyZ
dQmsEVMbhSfyNzWkFtIs9xsnSTqtsQmyhesuYQtnVPdi+0h2c7Bc66iA4EDmp2udwP1ZnYUfYtte
MZLDRfiVG2AboW7G+RZLM4VOQ0Ws0zYcWdv51tKGAhu4C8wu2HkTUIM3ix/Gs8B7VtZtQG+xO5BL
PS2aNK4oqG8Aa55j45rOyCcvPfkiu+zo+y2DG13aDL73B0iFDiO9+nLhF35mlf6IMmccA+ZqzLlZ
hMFtEoL1RaKCTbohzPsEJjUq5tVVDS+SYfV8ryB3NweLehJouibFdpf35zzi2yIUV0Yi+byjy1c6
iTuf+YHCpDtol/HhG2naOL4WYBRTxQO2ywnfwc4ZB05ooxQS25Q2L+zSaLM72RwfrK24d0u8ywtO
Xyr9+SNfIhbvTVKV66JdgGD5OZpXqP8yByqzw8styAwR7GFsbpssvpZtDr4CLhqyRLEGYVE0BKTg
WhxHNCZmjQTnXEStqDWLh8aZSCs7aTeslpWIiHQC60SGXECWqskk2ZLRiIlurxgzNgsItQnnz4wx
D+IP83a+AZkKZ8zFjuae33kjofQzzANwdG+MbZDBCa/FkwYVHpX/dYydb95y9s5a48gDZscRwLYO
WZ5YmKYa7u6DGTapImU3cLDJE5Ocpe22IJbrDKY9rpoyRIEU1PF96ApO7nKNmOJJGAgE16ulsGkc
p7Fs/WgTqzFqYRGsMazeJsbJeRnmQpiMQcX+Pe4z4UDhK9Wt1x2zzTpkoqsMqyikokeRRfzPaO5X
UYCFhcXPDHkKfhw1zwHsnY1IX9u5c5vgoHvGBOu4/CGATxQwyAoBdzGA8FU8GK1y2kSscdGGEfeE
S8Z7K2KAaBWZnhqW3aUjd8Ndlid8m0pKLMDGpBZO7Ke4MFByPThpn2eI/mfUmcys9qmsBR/eudR+
9/5r22X5Ijo17kdmGtTOWUUNMNlOrJiFCUKnjB6bXyQzGmbwLjS6IdQg4o5IMlFel+W9wuZXCMSV
mjTbxp8xKa0CRnlpcUID671p0Rq8Oz78jTE1kf50F4oQIcnwlSqck75NK+alb9T1jhHFC8RA5Zv4
gdoajSjOCgTkk8uUoOcGHfLDcoFvIaT25N7iBHwjZoFgDZlbTLJL
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 15 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 15 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 9 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 10;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 10;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "1";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "Estimated Power for IP     :     1.57205 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "ip_peakdetector_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "ip_peakdetector_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1024;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1024;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 16;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 16;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1024;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 1024;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 16;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is 16;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "yes";
  attribute c_family : string;
  attribute c_family of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 : entity is "artix7";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  doutb(15) <= \<const0>\;
  doutb(14) <= \<const0>\;
  doutb(13) <= \<const0>\;
  doutb(12) <= \<const0>\;
  doutb(11) <= \<const0>\;
  doutb(10) <= \<const0>\;
  doutb(9) <= \<const0>\;
  doutb(8) <= \<const0>\;
  doutb(7) <= \<const0>\;
  doutb(6) <= \<const0>\;
  doutb(5) <= \<const0>\;
  doutb(4) <= \<const0>\;
  doutb(3) <= \<const0>\;
  doutb(2) <= \<const0>\;
  doutb(1) <= \<const0>\;
  doutb(0) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 is
  port (
    A : in STD_LOGIC_VECTOR ( 16 downto 0 );
    B : in STD_LOGIC_VECTOR ( 16 downto 0 );
    CLK : in STD_LOGIC;
    ADD : in STD_LOGIC;
    C_IN : in STD_LOGIC;
    CE : in STD_LOGIC;
    BYPASS : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    C_OUT : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 1;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is "yes";
  attribute c_a_type : integer;
  attribute c_a_type of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute c_a_width : integer;
  attribute c_a_width of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 17;
  attribute c_add_mode : integer;
  attribute c_add_mode of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute c_b_type : integer;
  attribute c_b_type of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute c_b_value : string;
  attribute c_b_value of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is "00000000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 17;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 0;
  attribute c_out_width : integer;
  attribute c_out_width of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 : entity is 17;
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_xst_addsub_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of xst_addsub : label is "0";
  attribute C_BORROW_LOW of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_BYPASS of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_SCLR of xst_addsub : label is 0;
  attribute C_HAS_CE of xst_addsub : label is 1;
  attribute C_HAS_SCLR of xst_addsub : label is 0;
  attribute C_HAS_SINIT of xst_addsub : label is 0;
  attribute C_HAS_SSET of xst_addsub : label is 0;
  attribute C_IMPLEMENTATION of xst_addsub : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of xst_addsub : label is 1;
  attribute C_SINIT_VAL of xst_addsub : label is "0";
  attribute C_VERBOSITY of xst_addsub : label is 0;
  attribute C_XDEVICEFAMILY of xst_addsub : label is "artix7";
  attribute c_a_type of xst_addsub : label is 0;
  attribute c_a_width of xst_addsub : label is 17;
  attribute c_add_mode of xst_addsub : label is 1;
  attribute c_b_constant of xst_addsub : label is 0;
  attribute c_b_type of xst_addsub : label is 0;
  attribute c_b_value of xst_addsub : label is "00000000000000000";
  attribute c_b_width of xst_addsub : label is 17;
  attribute c_bypass_low of xst_addsub : label is 0;
  attribute c_has_bypass of xst_addsub : label is 0;
  attribute c_has_c_in of xst_addsub : label is 0;
  attribute c_has_c_out of xst_addsub : label is 0;
  attribute c_latency of xst_addsub : label is 1;
  attribute c_out_width of xst_addsub : label is 17;
  attribute downgradeipidentifiedwarnings of xst_addsub : label is "yes";
begin
  C_OUT <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
xst_addsub: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13_viv
     port map (
      A(16 downto 0) => A(16 downto 0),
      ADD => '0',
      B(16 downto 0) => B(16 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_xst_addsub_C_OUT_UNCONNECTED,
      S(16 downto 0) => S(16 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ is
  port (
    A : in STD_LOGIC_VECTOR ( 16 downto 0 );
    B : in STD_LOGIC_VECTOR ( 16 downto 0 );
    CLK : in STD_LOGIC;
    ADD : in STD_LOGIC;
    C_IN : in STD_LOGIC;
    CE : in STD_LOGIC;
    BYPASS : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    C_OUT : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is "c_addsub_v12_0_13";
  attribute c_a_type : integer;
  attribute c_a_type of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_a_width : integer;
  attribute c_a_width of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 17;
  attribute c_add_mode : integer;
  attribute c_add_mode of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_b_type : integer;
  attribute c_b_type of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_b_value : string;
  attribute c_b_value of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is "00000000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 17;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_out_width : integer;
  attribute c_out_width of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ : entity is 17;
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\ is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_xst_addsub_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of xst_addsub : label is "0";
  attribute C_BORROW_LOW of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_BYPASS of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_SCLR of xst_addsub : label is 0;
  attribute C_HAS_CE of xst_addsub : label is 1;
  attribute C_HAS_SCLR of xst_addsub : label is 0;
  attribute C_HAS_SINIT of xst_addsub : label is 0;
  attribute C_HAS_SSET of xst_addsub : label is 0;
  attribute C_IMPLEMENTATION of xst_addsub : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of xst_addsub : label is 1;
  attribute C_SINIT_VAL of xst_addsub : label is "0";
  attribute C_VERBOSITY of xst_addsub : label is 0;
  attribute C_XDEVICEFAMILY of xst_addsub : label is "artix7";
  attribute c_a_type of xst_addsub : label is 0;
  attribute c_a_width of xst_addsub : label is 17;
  attribute c_add_mode of xst_addsub : label is 1;
  attribute c_b_constant of xst_addsub : label is 0;
  attribute c_b_type of xst_addsub : label is 0;
  attribute c_b_value of xst_addsub : label is "00000000000000000";
  attribute c_b_width of xst_addsub : label is 17;
  attribute c_bypass_low of xst_addsub : label is 0;
  attribute c_has_bypass of xst_addsub : label is 0;
  attribute c_has_c_in of xst_addsub : label is 0;
  attribute c_has_c_out of xst_addsub : label is 0;
  attribute c_latency of xst_addsub : label is 1;
  attribute c_out_width of xst_addsub : label is 17;
  attribute downgradeipidentifiedwarnings of xst_addsub : label is "yes";
begin
  C_OUT <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
xst_addsub: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13_viv__2\
     port map (
      A(16 downto 0) => A(16 downto 0),
      ADD => '0',
      B(16 downto 0) => B(16 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_xst_addsub_C_OUT_UNCONNECTED,
      S(16 downto 0) => S(16 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
I27g6MYD4MW0QL8wlWDusLpmUQr++VIrJciW4kxAZvD1xu4xHc71nmmtQKn4RXprTItt3VZ4jlYb
W7sDjTzN9g==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZCd7ncP4pCL3bR7D848emv5B1F3GIhdyKdOrR8YDzkxL987Xx05dubKjo4b+YR7j9Ecj4dvvZe1H
6adMwXtSnoRoi30g04SXknicgV7eAYs7TVosxFfBt1CNgOyzN0bQbEUEFceUCCSfx8G96lJC7l6k
w+A3ZWFjWJVg1Vt6/vk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WdPinNhXowL0eItMQjILfN3ZtNRlAFPcgUKQgbQY6PjgOLOq8AYH8MaWMxwJ1Q3XLuR/CpJ3Msf8
9ck26xaGFODD49GyvXEm1m8jiFYiUuki4s8taTUnBUe8UmyAXLQtV80x3pecHd3LuDlAHzqlwHLb
RwNUJ8jmu/WUllodatqqpKOBuaenIRptuK0/OJ4m/EhxfYY9CrhNzvJ2OB+5wKW4GAF64RWui9+x
Bqw+bOAFz/60QdWNUEfYkftioYtQ1bPR23AMgCCwss6e7ZAS1ZDoUsWca/IzfDGAnuGA5fYYttxq
uoEZzpjupgu6Vfe6XH1ShFfcTJds9diYIBxF7w==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
J2PI9pkL6e7AvAtwdDAKsuYeJfRneKgQO+nHNIiAxTtlsTW9qYfsuK9HIkf0Sb62x0qE564ViGrS
3wuHHLwljmlXkNuh3H5s1WSIvBYog56SpodBhW9K9QsQbL95ZzCqaRj4TQCbxUYoSGwFsdPmmdM7
La05z8feuoK9AbXfQhrl8CwxQ/x9xQWu9KqCdtni2/rS3DBIIvXrfDnjMLkEKBjcfotyV3n5YDPS
xsl7G0OdCDCYK2w60G1P6raIszyzlL95Ntk3rgPPX40+b45hMQ7F7lMQBoxIOoJb8u1v/MHG31i3
lgLBBsa7tA8yQ8i1322megfZnBtOkWTPkmGATA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T2bPt/UsVlHeNg5Le1CxaL61udjoxzw96Kj3hgyH1tUYL6gN3XkvsqfcbtTEYOMh8h8ccS0iuxDR
u1AEFZYGDETWy4UIcIXJiMNJW9mKlMxeX0aYJmt2gk5SzH3rwCtKFV4BE7w8PwEho0CpeEjRbnf/
H/ZBsUMPSTkaly3dWhZ8iwOFxUFab/dTKOrq+/zYR0ymNFfbFDioA0t6O1QvpkQKgAmLKtz48cIZ
d8mpcUcwmUu2smVlnmcYowYxe9PRMHJNRJBKGeYM3CGauAWJ//wpfST4UNmOFt1mBZDKyRVEtdmb
fxus/Lw6a1mgHZoYg82xB4qQBrCBG6m9hlSvQA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
O9o1BY8Sbymv0CVlrYskQzxGr+xrqarG7EPRt/pk31lim//eAelugbq/q5/E+p/kQuJs9wJgGlt7
J4EL+RC/2F1gmfEvMCf58rYENifz93c15KzjI9WIzhK9r51ZA8fR9ZyTMeOrkrakuwslx8gz2Nd8
GtHVrcoDTocBpHaQfUc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
B1N78V5eMBUsTRlKv5Ei+E+GTZzSlJSEsn133UlYjp9l9fAT6QvueXKaAUXJstu/mKRIG6pF2HeT
10E1KqusBF+N1vx0mJm+BGQmw8VwnDo/1C0bMq/T7k3skIGTTlpmmE04UhvTFZ5eNWSXF3hpRbnX
7LVz1QBuLq3y5vrznsX3rcSujxWkx6qVpUyrOGyzLDbbTs8ND/8qOUNz+Tsj2+A/fjXiwPURl7Ci
pqZKDVAkBrWTm4hLakBaJZUjicezF1sJAkTUk1hm+9WjXbPsux0DtpZ2Gw6FtD0vv/mLn3UAKK8N
uj2yxGASMChllxrDzGBUQlWEDBX02i/b8OMnHA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
hE0awnb6TTeK/UyS8JJHKmF3PAUQ/rFgL58eom42D78HYca43nXuw4Ma7Drq9Tz6SKt5+mD/Zge8
x7WpwrwaYniwiOMNGMNj7rs1Pg719QAdVT0pLLtVpV+DPV/g4stEdcZoD7zAiFz9bdmj9VOWlFdZ
ISKC4GRcrViZtQipGBLZ4oLRiyV3IIyENz0MELWXG8c8W0IEtNL9Sy5+Vxf6w9wmv2ZeVeEHs1mc
GE+i3jFMUOXAIx8yvlQ0/O+F4HKocRZsp3MU+2Hur8XglQ0Hswh/Ga4WXAEyVGbXai6+KsWUD/bJ
x/k4yJv360qXPTkUWO5YzVMqebs4071edSqJmw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
H8IIzLRpC7jpfOz948bip1M0eS7gUPV+8ce/UXmNdTd/Jg9M2qFLh60tTz1iCRSIfXjN6uP8ygH9
Ia8qF+VJS4IpOFyilmLRF1zZIcoMiUwiIBBiLn44z03gJxpDE6sPFJ4jWNe9CkZzI4SCOjzRJrEM
K2L9iN9YOK8ciMUpSMbd7J16AwlRdUOVEWuzlv8zrX7uBZOYrvlDhsVkAd+8Kpzkkw4VKlOyOIkL
GcuthqRxDsgHVj9QMHUGC344n3X9umOkSK/+kbCfNocVjETThWNmkSBDLYJ07xw92cqUUDrLoSsy
t7tlRmJmhM4/zsVK/NN4Z++6sLtiF0iwEbuZSQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4976)
`protect data_block
nkyyC6vlliF9Ja1/RA/oC2B7mTzGAqc0g2vE2vH1cQL5p3tpBjkY6rHZzIp/M6pSUEBaIevCixKI
Veytytm2FhpY4Pd7+JOa+D0sxpIs8c6ibNOgMUxDAZECFIpXHgCWBp3cDoS2U8nwMhkmoy1iNDTd
Ts2dex1IYTfcFAL8APnOLo/OwcrZKWctNEdlX9vS68bBRuVAQYI0wY7WHgNm78RzhZGpxeyBPiaW
ffYFNPgqAY0+kXY8/rqLR7c8HzN9vYdp2U/PZkGswg+4y1lrgqUKEtLmBCrCOhNM7AIkoHd8CwVh
2nhNtzojEndQwVPWTY9d8jFLhjCJNtO0i6c/Jn8CekUFsEdQclHI1c6K8zuB+K7x8a7nA4p9WZQd
E2/GzQj7A5h+b4qoaoQkje+jwnDv9P73uholkC4q9J8TkZgNDA1MoGaNOdWoO4KovJiTG1wt1GoF
NM5s0HH7YrdHb7evO/mgBfYCwq+O4ZrN6lQeKYKRhzLXVroi9EkkeUCdcdGXhJm7LVhaYKuWaF+N
DytGNcgNt13BI7wPLS8GM9u50n1WkyPZ3HEJRoEnYV6Jw9GxgIWROuEqx62123WDLQINMd+2FRlD
ItwBFj2xsjnwsAxSctqngwT5gT9fS3eRpXuok8Wm2IRSv84DNcElGwHy6XkKPewqGMX9ThEQCowJ
cLfb9RSq4JVJz35RlHFfDr6rf6xBTqHNh//e/GpKdQnPRtVH0p7PKF3bqTBoy0tRMBVmBlDAD32V
2DDZ5qZl6nIrrI7xfU0vQazMsNCd+bVnDAS4X0LjfeEneatuQ/cr2fMVK2/nQNZBupXfZRcVfUO1
myXtajb/+ZBKRpYuXdhO+Naylinf2IOPYNTP8T633S4qI11C4mtGsxWROJd4rvApq7ltvQZbFAsY
i7WnIQX9bX7QGBXijBLcdxJJSarBfVRPJ+Yp9w7k8mk6bOt+2gEEZnL1HEqRA0K9sTP1oyakCmlt
m3YqKV2tto7fGo3Szd6MePfNnPWn9C3DaEqjH7Mr5rICfrs4y8OtuXdJWGI057BYMybltdZMhXgJ
o5F9ClWv1x52vQefn8guaI+Yldgf7+6XRQUl3bzN+YgVaztNCRWrNbxJ1RE2UA4G87KGVK6BKDcs
/+Wxgr0Gm/z6073Madf/0jUnL/D2YCV73ZMIgNa0sCfjK9jTPu1oJ7Zcr+hJuCnai6y2H9fihOg1
i9un/dzyzeZXI4WWfPqx5i2kYISneH6bO7VtxbNP4bSbS7tKgRqbyJms2A+n/DTZdoVK/1QpqIkf
F5ROjlG5tSYfkfZv7NdS1F0CJlAhEX5apOjxht8eOCN8pkjh76tXAeLoSayAGzvPjcahU4bfqt4m
fZt7+ZeP8SPz/Te1WIwPRIUhOz/vzSrYfyzVbggU+DkStOPeyQIJwCWK/sdmUjSPnyOiuaNphkpN
UwlFeecHIi7XLAwvc+Nn3DQOaZfINJhWiT/NUNzBZPVRoyayx5o+uBYvElmhzmTnB9C2mpJ7PzFe
RTGhBhWYtffKbYOfpWt+gLxoVypeyR1fXmmE4lVQ9Y3tdOMwfFL816FdWoD78Lbh/WGvUY9MwLAL
I9H8o4fsy3zCRI9HXngj3Man05uaMDpUq2H8pxTHfmI+HpeiMEwnbdQEW3BeF/Z5IXr2Ke+kg8nk
Eqis+UzY9qXz6kdBIDICDq0z0AalgvN4G5pCe+rl+QmmYA61nOCv1l+Ju2mO2VrXfAm8Ao5txKuE
myygMfLW9rLLHmAULq84Rhgm9FmKvjcAs1ZJWVU5YIvGa1RhLSlMIHxlXuhDVgyPJe26IbPncsyC
VXRvVLsRtn8wwceq6/+CL0a4zXux+mmRXCAuRbkNFfsRWXOYJKsxaNKtMqfx9P/S+zz3u7a0j/26
NiwZUkJcVDKS8IrOk6lv4XzS+btALnrvrZohdnd0qC4QQ2UQrliv1jE5q7XZaYkchMRN6mIVi6NB
veX/VRXeIfs/FCTTTWBKjeSI7HTU8x88FgljxkvJqW8q1n7SWVcsiD0FCBRYe9v22/mJBgAuIELG
ZZshKWvAINdjM4O5zJBlrUUs6BJL819EWdQry9VyylrUDNn2Lj13nVBPkphCCmq0he3UBfTHLYsh
ZY7Ck4IUWplvtJ3owx8dD+Ctip4ZqSAOupHBU0FPSJ17O67AMR1QkgP0d0o76N9pbBNq/wU9oo0e
wvMaf0GxlLyiiaq99oGgnBRRb5iCFjCKKfMB2M3y4d3Yy1j45CqTGJ6q6h1E/XOcrPPW0rabr62J
Nozt7ZmAMxZHI1CIpxZdXRvCvrDuZh3xpzhfWUtlHg8/1rvps5X3/8pfStLys5pubhArjhkCApF3
20T5wFw0gLcE1+LySm2++dMhSh5Ct7UYEfHS0qTq08dwUd6XZzfDZMZS0IuZ2+stmUIrd+wKUVwO
14iN+HZCfcmaOrj3HWgiWDwWlWA9+0tGRsCNIq5lc2YT0P9IJQrCCBGOfanRwxIYFSBWuCOdDQZo
DlIhb71gK6VA9JEWKY0ke3QBgGcS7vbFu02x+V94BZbqqw+GH/UH6afSa/UeEqSZkOcH8y8Iml+n
s/pgM8cH2fR7Au7iVU2txQHIzkRqHzqyWvsdYy9xgWJfx+O9DSadT5T0o38KewjYtDu1bgFIWVmR
2MEKEmzO4iS/OeRyo+CIYIlW+2FcOqWIZHurnc0j+RrOvoXTT6Lb+nMa2J1zfH4VdZhn7YkN0rDi
AJ0wvKlofVDH92cQxBE46vtuzUzM52Pie8Xm/SrSPr2hwEs+rRGLDNpS1SGVKK8tyACjrJKywKoE
st8BKSYTOI0rdz9qkgnjp+E8SvWbXrIb2zM+0cQU5x+fYGRzGqag65KQ6xaqGFvC/cTDekTsg+9x
g14WNMKx42+orph2MF6TdJxyzO5XA4KfwV6FC7WdhfmvDR2NT/HqX7HFzoLmVU2qwvbcboIB08/2
ctlKgsDvA0/AGyA17ZDk0wCGpLincsxIiV/ZMDgrFWUDJXRlEdXxPOlDIR1X1OVxftHsXEPqDXIX
lBAiIQixSjS9mg7XOz1TiaTRCZLh0sDXLwQ1nCYVb5a6AakqUKeM/0xoXgu23CeLQ+hqlZl3yqKD
Zu6L+ylqyCQbTQdg62JNN9AKu5HaKicglcVnpmamMAx0GkXP6pUkv0eeZ+lElMhisMx5H69aQXPI
/8XV8bZF7bfLD6tRAgGPNy6yKo1j8t1dNWj3oqnv9HOumKDC/4+gTeztN441i8VyyCv2xHcWug8y
hY+pK2Ph9iuZZUEedLU4j5JHKU9asU7Kb1+7UhYF+pYU8XaEfa8r0xMb6jDZ+EDV0078Ceam6i/l
aP2/5XPOnKe73loKIpaGfM/mQl/M8zOwgaVXQ8bd1yjlRGAtSZnDLb+1F+9QD/9xznYSKEGZjoyA
nLNXkzY+tMBR+q56V+CE1RbO8VITXhQUjf7yZuMg1conUkuIznzb2+qjEwCsJA17j97xamP9APJy
275SO5RzBYYRoWe1+ks/q3m/orDCwTUfyUl6mLlAPw1EOF6HHAv/XTBrXD7JR7upfR/rRqzg9VBN
C7bvCOg+QLf8RbhoqMAQqrRJHWMzq73ixaRd/B+BT2LvzofcxImkP/6mdIXEDPEzAY1YNF604Wvl
AjsGatvHmwzwU8N2wtAceFQbMOgqMwlMpUyExmfJbtyhcYF0C8v/Ty0E9BddKeC6Y/r+PaYDzs9o
M7kf1jDZUrGE3A0cy6TXcQVzT4hOFja9qKf+ZrcLqrpip+OExcHZTdrVOnH5wr6Yol7c38nIfuiL
JO/p6y2jZ0W8ohq59F3ewIMu+tg9Lndzpge6RmkjCDwqmyiJTn7XPD7vBAkT9coLT3bo34yErV+R
LzsLCzLofYQCA3lYfMqoVE3NO6KnW9XDW4Ra6vP1Op1iQiHW2LOgUtjxq9D9Y3czFPHCPicFHGqM
mTlBFvuLkrliVJcVLq8ge2ViBjpXtMPev7fBeTQ7VgJ86cDBwc6heNBb4Hz4yb+gYvJSXkdgVIyW
xD9gso8F/PIGZvWKGn4SpDjDk0HhA09wkOBhe9OgaFERpo4uk/g4hu31BYiNRDYQf/HOp8AGUfnb
S96uijhWkjw3AVkKwjN6D3juoChKnW4HZFQuROxsvw1GFs50v8UJ++h13ZR53Kuniczcxs8oqrxk
MPVaofVqljdKoygSUYSSEnX1Fqxa79j+X1xppVD9WsFRP9TfAe/TDs6aSG1LJF1k5RKK3VkYOSjb
eTwnIqUixPhMggrBsNL00CdoWkBgOFsksLAOgQ1lxmqMQq1DGu7CGXsZxtW9pqw0ySzW27cayUM5
TJqYP9G12tnjYjuQooUoR2JlQk5LXCAocgDf6FkoLLwcWoIUS8NStVlpEbRh16r5k1/s7ZcoU/ZE
hfFnsG6dBFDMr6hYcOlNq/SzsnWw+jX1bsqeSn82JL2DfRkJDQo9PmllB0Dzj0Ym8FaXLz+0lHpG
BN4uZLwqiMaQmzPXZ/veokzOghFoQjwv6tVquiGm+WLj8NFttaRI3XPgYDgHSp3ERfe7M7857qBX
TXkBE37oo1G+LWWp4sImWnxr3g0SYXw3QT87/qNU7BZHKKn8TnXKfnPZAsXO852TVa+erdgBOw8k
M6sqxACEzlKYFrdZnPpJ4nkF+Q/+rmhb15kGWDIT1UM4r3s4ONJvcWVd1CjM2uIAThc4vJtCT8cN
UA/BqgU5gYGtZGgvqs+sKqDs+Ft1E5u2YWRavgmocDu//kknuGZu8RL9X6+Ds8rpsN0sfToyYXgc
2VSYdw8I0YGpR9fzIBr33FjG3VBwiJzmjsl2on2Q++SwdM/04wqcoUfaE9J2ECUih0IQZs+j4Hl4
FPXiNCWebJrLKurwLf4BtGQsEuKumWpqqPlV8AY5mLGQWRJERF8UA3zYA5H/R5DQ71CRWdTRM3Ge
QWx9VsLlDAiTpWoZBcc39ObcZfsm4jDgaml4aiFkijz/QHpSS+cH+W5MAryTXHYNH/vOHZs2OLom
TIjclF6zu9iwBJA42iqBoAs+mAzggQf51l0UCbP9zgucQ/0RlGdEVuUqJdtAVi6lGMP428EnmMje
c2tp6qcyARaF+aAZ5hYv81/qw1azfdagMWeHRJZmFbgRot22NtWr8ybAM85FQ2IaXgMXDg2T4d3s
QT/WnulcPzpqwbJX9hr89EGMasHWsKuvy8BpgSLGmv0+tTIovPr10sS+LAj9QEPaUxkfU6QGYkxV
5u3FSvhaU2O2zw/oCd876+HtU2/CDocLsnayVvO31i+Pny20BgPhSY3Bx6e+mwiQAOEd/xfBMv9l
XmM5A74wNFn6xSTwwTSu7iZHxjyDf8z5IqPF8+HQeVZxwQfM4O41DB297Fy5CO34qSIBcUMbZd35
FkUf448b7J77+o2sxOKwLIJccZEy1MMzArYRDaRWXo4wOaxLMW74Lo26d/ERi4GS+ycLCLh0rkQz
JGQx823f7Rl6Nj2jRizzVhomf2mR2VR4QgD702encWpAqNbmTM3LaM8j6A9y6kPdXq9A/cIrXM/4
CQyceNJC/OOqXecBNrOaq9ibyIMVzcwbG63pIHTLVjusKvwIKdlH47Er4+rJIncYbdGuYSV1j7ky
iV3Z6MxDT3ZnWEiBVlkhuBelK1Venju90v6Cwj38DNgvsS2YnVSLrHIAsSB6qZE3inGjQZWhYj4S
nbyotFOQJuZSHCYqUVFzygh/8WXCuhI41/0cxteuAAFcBI9hgsuoLRgzj9OYfgdbuCxSsjQC5XbG
9As/4cGtv+hjPKPAi3BLPtxQ0Tk1Xebw+M/PT2Bsp722S4Db+98VlY7rfP5fnK6f7/QOSHrZwlDo
aSZa14ZCVr1HYBtxkrUgtEb4qD7E2bIaTTkFEHY/T46JBIWyrw4+VRwNp6CPyedEQczmoiq5weGO
0ua2qtNS6mxmtOIdD2NGbjjFuqdn1Xo55wjS4BXrdObj1v1j74QiOXgd/5EK/IgpbAQizr61SeAV
e36cWEWM/KS3oS4y172qbmgs7v4wCH52rNnfzi2it8+/yOxBBOsjcR1nMlPBmMjerR40shi6emkR
afvMWG+NYxBNsgmdfhiZ40KAFuobUdERrWbvOnI3sWP/uO8pFTlPrKhgSQwPKGX8y2xirRh8oALS
xVPRKjc64/0MHEoBkw3AK8crldTeEKbu7d+VVfPfSG8lre4Dt7a8BfL9jdnrRgRABK/DOLjhquzs
JPnyLCsv99JP9b0rDBthHtlYzlvBBbqkc169kJUJfL1whFVJRvPn37KTMToOi0j0xM+LVFMwM2rM
ttLUjDTrADV8CvVLms3h7/Y84bI0ih5WqD76XlR+YR7PZIa7y8LLsA4DXLCEFuDZIJahIA8Nio+N
NVPvod3RJiqmqoYPXcc2tFTO9HqPqHPM6hdZlamwNao2pfhjBEz2uRAb/FIKLrlN2AaMdaFUgOx7
YGrdcyAWSOB5Gk+hydoQbgr1vpnt3XtcnC47qlAJKA9IIGjDUbbLGXmBGfb60yogGlFYTGaaB59C
ma7aH1N34xEQ5cdVZkImEaQhnnHGGCyLRWDltvoDoLz4dqk4XlwIDBLjH6eQGNOyf4kxzpMo6LGs
bAKK50PJQRvm5HvbxsDfPYs=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0 : entity is "ip_peakdetector_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0 : entity is "blk_mem_gen_v8_4_3,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_doutb_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 10;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 10;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "1";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "0";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     1.57205 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_peakdetector_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_peakdetector_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 1024;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 1024;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 16;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 16;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 1024;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 1024;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 16;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 16;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      addrb(9 downto 0) => B"0000000000",
      clka => clka,
      clkb => '0',
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(15 downto 0) => dina(15 downto 0),
      dinb(15 downto 0) => B"0000000000000000",
      douta(15 downto 0) => douta(15 downto 0),
      doutb(15 downto 0) => NLW_U0_doutb_UNCONNECTED(15 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(9 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(9 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(9 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(9 downto 0),
      s_axi_rdata(15 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(15 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(15 downto 0) => B"0000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0 is
  port (
    A : in STD_LOGIC_VECTOR ( 16 downto 0 );
    B : in STD_LOGIC_VECTOR ( 16 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0 : entity is "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0 : entity is "c_addsub_v12_0_13,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0 is
  signal NLW_U0_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of U0 : label is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of U0 : label is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_a_type : integer;
  attribute c_a_type of U0 : label is 0;
  attribute c_a_width : integer;
  attribute c_a_width of U0 : label is 17;
  attribute c_add_mode : integer;
  attribute c_add_mode of U0 : label is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of U0 : label is 0;
  attribute c_b_type : integer;
  attribute c_b_type of U0 : label is 0;
  attribute c_b_value : string;
  attribute c_b_value of U0 : label is "00000000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of U0 : label is 17;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of U0 : label is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of U0 : label is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of U0 : label is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_out_width : integer;
  attribute c_out_width of U0 : label is 17;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of A : signal is "xilinx.com:signal:data:1.0 a_intf DATA";
  attribute X_INTERFACE_PARAMETER of A : signal is "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of B : signal is "xilinx.com:signal:data:1.0 b_intf DATA";
  attribute X_INTERFACE_PARAMETER of B : signal is "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of S : signal is "xilinx.com:signal:data:1.0 s_intf DATA";
  attribute X_INTERFACE_PARAMETER of S : signal is "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13
     port map (
      A(16 downto 0) => A(16 downto 0),
      ADD => '1',
      B(16 downto 0) => B(16 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_U0_C_OUT_UNCONNECTED,
      S(16 downto 0) => S(16 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2\ is
  port (
    A : in STD_LOGIC_VECTOR ( 16 downto 0 );
    B : in STD_LOGIC_VECTOR ( 16 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2\ : entity is "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2\ : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2\ : entity is "ip_peakdetector_c_addsub_v12_0_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2\ : entity is "c_addsub_v12_0_13,Vivado 2019.1";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2\ is
  signal NLW_U0_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of U0 : label is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of U0 : label is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_a_type : integer;
  attribute c_a_type of U0 : label is 0;
  attribute c_a_width : integer;
  attribute c_a_width of U0 : label is 17;
  attribute c_add_mode : integer;
  attribute c_add_mode of U0 : label is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of U0 : label is 0;
  attribute c_b_type : integer;
  attribute c_b_type of U0 : label is 0;
  attribute c_b_value : string;
  attribute c_b_value of U0 : label is "00000000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of U0 : label is 17;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of U0 : label is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of U0 : label is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of U0 : label is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_out_width : integer;
  attribute c_out_width of U0 : label is 17;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of A : signal is "xilinx.com:signal:data:1.0 a_intf DATA";
  attribute X_INTERFACE_PARAMETER of A : signal is "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of B : signal is "xilinx.com:signal:data:1.0 b_intf DATA";
  attribute X_INTERFACE_PARAMETER of B : signal is "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of S : signal is "xilinx.com:signal:data:1.0 s_intf DATA";
  attribute X_INTERFACE_PARAMETER of S : signal is "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef";
begin
U0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_13__2\
     port map (
      A(16 downto 0) => A(16 downto 0),
      ADD => '1',
      B(16 downto 0) => B(16 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_U0_C_OUT_UNCONNECTED,
      S(16 downto 0) => S(16 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 9 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is 10;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_i_synth_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 1;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 10;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "1";
  attribute c_has_load of i_synth : label is 1;
  attribute c_has_thresh0 of i_synth : label is 0;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 0;
  attribute c_thresh0_value of i_synth : label is "1";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
  THRESH0 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13_viv
     port map (
      CE => CE,
      CLK => CLK,
      L(9 downto 0) => L(9 downto 0),
      LOAD => LOAD,
      Q(9 downto 0) => Q(9 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_i_synth_THRESH0_UNCONNECTED,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 9 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0 : entity is "ip_peakdetector_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0 : entity is "c_counter_binary_v12_0_13,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0 is
  signal NLW_U0_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 10;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "1";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 1;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 0;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "1";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of LOAD : signal is "xilinx.com:signal:data:1.0 load_intf DATA";
  attribute X_INTERFACE_PARAMETER of LOAD : signal is "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of SINIT : signal is "xilinx.com:signal:data:1.0 sinit_intf DATA";
  attribute X_INTERFACE_PARAMETER of SINIT : signal is "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of L : signal is "xilinx.com:signal:data:1.0 l_intf DATA";
  attribute X_INTERFACE_PARAMETER of L : signal is "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute X_INTERFACE_PARAMETER of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13
     port map (
      CE => CE,
      CLK => CLK,
      L(9 downto 0) => L(9 downto 0),
      LOAD => LOAD,
      Q(9 downto 0) => Q(9 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_U0_THRESH0_UNCONNECTED,
      UP => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub is
  port (
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[6]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[6]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    douta : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub is
  signal core_s : STD_LOGIC_VECTOR ( 16 downto 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_addsub_v12_0_13,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0
     port map (
      A(16) => douta(15),
      A(15 downto 0) => douta(15 downto 0),
      B(16) => o(15),
      B(15 downto 0) => o(15 downto 0),
      CE => '1',
      CLK => signaldomain_clk,
      S(16 downto 0) => core_s(16 downto 0)
    );
\rel_47_16_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4000DDDC"
    )
        port map (
      I0 => x(14),
      I1 => x(15),
      I2 => core_s(14),
      I3 => core_s(15),
      I4 => core_s(16),
      O => \x[14]_0\(3)
    );
\rel_47_16_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(12),
      I1 => x(13),
      I2 => core_s(13),
      I3 => core_s(12),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[14]_0\(2)
    );
\rel_47_16_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(10),
      I1 => x(11),
      I2 => core_s(11),
      I3 => core_s(10),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[14]_0\(1)
    );
\rel_47_16_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(8),
      I1 => x(9),
      I2 => core_s(9),
      I3 => core_s(8),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[14]_0\(0)
    );
\rel_47_16_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82424241"
    )
        port map (
      I0 => x(14),
      I1 => x(15),
      I2 => core_s(16),
      I3 => core_s(15),
      I4 => core_s(14),
      O => \x[14]\(3)
    );
\rel_47_16_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(12),
      I1 => x(13),
      I2 => core_s(13),
      I3 => core_s(12),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[14]\(2)
    );
\rel_47_16_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(10),
      I1 => x(11),
      I2 => core_s(11),
      I3 => core_s(10),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[14]\(1)
    );
\rel_47_16_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(8),
      I1 => x(9),
      I2 => core_s(9),
      I3 => core_s(8),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[14]\(0)
    );
rel_47_16_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(6),
      I1 => x(7),
      I2 => core_s(7),
      I3 => core_s(6),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[6]_0\(3)
    );
rel_47_16_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(4),
      I1 => x(5),
      I2 => core_s(5),
      I3 => core_s(4),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[6]_0\(2)
    );
rel_47_16_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(2),
      I1 => x(3),
      I2 => core_s(3),
      I3 => core_s(2),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[6]_0\(1)
    );
rel_47_16_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(0),
      I1 => x(1),
      I2 => core_s(1),
      I3 => core_s(0),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[6]_0\(0)
    );
rel_47_16_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(6),
      I1 => x(7),
      I2 => core_s(7),
      I3 => core_s(6),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[6]\(3)
    );
rel_47_16_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(4),
      I1 => x(5),
      I2 => core_s(5),
      I3 => core_s(4),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[6]\(2)
    );
rel_47_16_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(2),
      I1 => x(3),
      I2 => core_s(3),
      I3 => core_s(2),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[6]\(1)
    );
rel_47_16_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(0),
      I1 => x(1),
      I2 => core_s(1),
      I3 => core_s(0),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[6]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub__xdcDup__1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    douta : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub__xdcDup__1\ : entity is "ip_peakdetector_xladdsub";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub__xdcDup__1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub__xdcDup__1\ is
  signal core_s : STD_LOGIC_VECTOR ( 16 downto 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_addsub_v12_0_13,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_addsub_v12_0_i0__2\
     port map (
      A(16) => x(15),
      A(15 downto 0) => x(15 downto 0),
      B(16) => o(15),
      B(15 downto 0) => o(15 downto 0),
      CE => '1',
      CLK => signaldomain_clk,
      S(16 downto 0) => core_s(16 downto 0)
    );
\rel_63_21_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"73331110"
    )
        port map (
      I0 => douta(14),
      I1 => core_s(16),
      I2 => core_s(14),
      I3 => core_s(15),
      I4 => douta(15),
      O => DI(3)
    );
\rel_63_21_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => douta(12),
      I1 => douta(13),
      I2 => core_s(13),
      I3 => core_s(12),
      I4 => core_s(15),
      I5 => core_s(16),
      O => DI(2)
    );
\rel_63_21_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => douta(10),
      I1 => douta(11),
      I2 => core_s(11),
      I3 => core_s(10),
      I4 => core_s(15),
      I5 => core_s(16),
      O => DI(1)
    );
\rel_63_21_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(8),
      I1 => douta(9),
      I2 => core_s(9),
      I3 => core_s(16),
      I4 => core_s(8),
      I5 => core_s(15),
      O => DI(0)
    );
\rel_63_21_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"84442221"
    )
        port map (
      I0 => douta(14),
      I1 => core_s(16),
      I2 => core_s(15),
      I3 => core_s(14),
      I4 => douta(15),
      O => S(3)
    );
\rel_63_21_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => douta(12),
      I1 => douta(13),
      I2 => core_s(13),
      I3 => core_s(12),
      I4 => core_s(16),
      I5 => core_s(15),
      O => S(2)
    );
\rel_63_21_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => douta(10),
      I1 => douta(11),
      I2 => core_s(11),
      I3 => core_s(10),
      I4 => core_s(16),
      I5 => core_s(15),
      O => S(1)
    );
\rel_63_21_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(8),
      I1 => douta(9),
      I2 => core_s(9),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(8),
      O => S(0)
    );
rel_63_21_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(6),
      I1 => douta(7),
      I2 => core_s(7),
      I3 => core_s(16),
      I4 => core_s(6),
      I5 => core_s(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3)
    );
rel_63_21_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(4),
      I1 => douta(5),
      I2 => core_s(5),
      I3 => core_s(16),
      I4 => core_s(4),
      I5 => core_s(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(2)
    );
rel_63_21_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(2),
      I1 => douta(3),
      I2 => core_s(3),
      I3 => core_s(16),
      I4 => core_s(2),
      I5 => core_s(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(1)
    );
rel_63_21_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(0),
      I1 => douta(1),
      I2 => core_s(1),
      I3 => core_s(16),
      I4 => core_s(0),
      I5 => core_s(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(0)
    );
rel_63_21_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(6),
      I1 => douta(7),
      I2 => core_s(7),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(6),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3)
    );
rel_63_21_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(4),
      I1 => douta(5),
      I2 => core_s(5),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(4),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(2)
    );
rel_63_21_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(2),
      I1 => douta(3),
      I2 => core_s(3),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(2),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(1)
    );
rel_63_21_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(0),
      I1 => douta(1),
      I2 => core_s(1),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(0),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlspram is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlspram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlspram is
  signal \^douta\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_peakdetector_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_3,Vivado 2019.1";
begin
  douta(15 downto 0) <= \^douta\(15 downto 0);
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_blk_mem_gen_i0
     port map (
      addra(9 downto 0) => Q(9 downto 0),
      clka => signaldomain_clk,
      dina(15 downto 0) => x(15 downto 0),
      douta(15 downto 0) => \^douta\(15 downto 0),
      ena => '1',
      wea(0) => '1'
    );
\rel_61_30_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(14),
      I1 => o(14),
      I2 => \^douta\(15),
      I3 => o(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3)
    );
\rel_61_30_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(12),
      I1 => o(12),
      I2 => o(13),
      I3 => \^douta\(13),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(2)
    );
\rel_61_30_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(10),
      I1 => o(10),
      I2 => o(11),
      I3 => \^douta\(11),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(1)
    );
\rel_61_30_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(8),
      I1 => o(8),
      I2 => o(9),
      I3 => \^douta\(9),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(0)
    );
\rel_61_30_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(14),
      I1 => \^douta\(15),
      I2 => o(15),
      I3 => o(14),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3)
    );
\rel_61_30_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(12),
      I1 => o(13),
      I2 => \^douta\(13),
      I3 => o(12),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(2)
    );
\rel_61_30_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(10),
      I1 => o(11),
      I2 => \^douta\(11),
      I3 => o(10),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(1)
    );
\rel_61_30_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(8),
      I1 => o(9),
      I2 => \^douta\(9),
      I3 => o(8),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(0)
    );
rel_61_30_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(6),
      I1 => o(6),
      I2 => o(7),
      I3 => \^douta\(7),
      O => DI(3)
    );
rel_61_30_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(4),
      I1 => o(4),
      I2 => o(5),
      I3 => \^douta\(5),
      O => DI(2)
    );
rel_61_30_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(2),
      I1 => o(2),
      I2 => o(3),
      I3 => \^douta\(3),
      O => DI(1)
    );
rel_61_30_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(0),
      I1 => o(0),
      I2 => o(1),
      I3 => \^douta\(1),
      O => DI(0)
    );
rel_61_30_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(6),
      I1 => o(7),
      I2 => \^douta\(7),
      I3 => o(6),
      O => S(3)
    );
rel_61_30_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(4),
      I1 => o(5),
      I2 => \^douta\(5),
      I3 => o(4),
      O => S(2)
    );
rel_61_30_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(2),
      I1 => o(3),
      I2 => \^douta\(3),
      I3 => o(2),
      O => S(1)
    );
rel_61_30_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(0),
      I1 => o(1),
      I2 => \^douta\(1),
      I3 => o(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_math is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[6]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[6]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    douta : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_math;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_math is
begin
addsub: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub__xdcDup__1\
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3 downto 0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3 downto 0),
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3 downto 0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3 downto 0),
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
addsub1: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xladdsub
     port map (
      douta(15 downto 0) => douta(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0),
      \x[14]_0\(3 downto 0) => \x[14]_0\(3 downto 0),
      \x[6]\(3 downto 0) => \x[6]\(3 downto 0),
      \x[6]_0\(3 downto 0) => \x[6]_0\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlcounter_free is
  port (
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 );
    S : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    \op_mem_37_22_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlcounter_free;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlcounter_free is
  signal \^q\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_peakdetector_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_counter_binary_v12_0_13,Vivado 2019.1";
begin
  Q(9 downto 0) <= \^q\(9 downto 0);
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_c_counter_binary_v12_0_i0
     port map (
      CE => '1',
      CLK => signaldomain_clk,
      L(9 downto 0) => B"0000000000",
      LOAD => LOAD,
      Q(9 downto 0) => \^q\(9 downto 0),
      SINIT => '0'
    );
\op_mem_37_22[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(9),
      I1 => \op_mem_37_22_reg[0]\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_delayline is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \op_mem_37_22_reg[0]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_delayline;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_delayline is
  signal counter_n_10 : STD_LOGIC;
  signal counter_op_net : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal relational_op_net : STD_LOGIC;
begin
counter: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlcounter_free
     port map (
      LOAD => relational_op_net,
      Q(9 downto 0) => counter_op_net(9 downto 0),
      S(0) => counter_n_10,
      \op_mem_37_22_reg[0]\(0) => \op_mem_37_22_reg[0]\(9),
      signaldomain_clk => signaldomain_clk
    );
relational: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_3828661dd3
     port map (
      LOAD => relational_op_net,
      Q(8 downto 0) => counter_op_net(8 downto 0),
      S(0) => counter_n_10,
      \op_mem_37_22_reg[0]_0\(8 downto 0) => \op_mem_37_22_reg[0]\(8 downto 0),
      signaldomain_clk => signaldomain_clk
    );
single_port_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlspram
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3 downto 0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3 downto 0),
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3 downto 0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3 downto 0),
      DI(3 downto 0) => DI(3 downto 0),
      Q(9 downto 0) => counter_op_net(9 downto 0),
      S(3 downto 0) => S(3 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_peakdetector is
  port (
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    d : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \xmax_i_12_24_reg[2]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \^o\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_35_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \op_mem_37_22_reg[0]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \^q\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_array[12].fde_used.u2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_array[7].fde_used.u2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_array[11].fde_used.u2\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_peakdetector;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_peakdetector is
  signal delayline_n_16 : STD_LOGIC;
  signal delayline_n_17 : STD_LOGIC;
  signal delayline_n_18 : STD_LOGIC;
  signal delayline_n_19 : STD_LOGIC;
  signal delayline_n_20 : STD_LOGIC;
  signal delayline_n_21 : STD_LOGIC;
  signal delayline_n_22 : STD_LOGIC;
  signal delayline_n_23 : STD_LOGIC;
  signal delayline_n_24 : STD_LOGIC;
  signal delayline_n_25 : STD_LOGIC;
  signal delayline_n_26 : STD_LOGIC;
  signal delayline_n_27 : STD_LOGIC;
  signal delayline_n_28 : STD_LOGIC;
  signal delayline_n_29 : STD_LOGIC;
  signal delayline_n_30 : STD_LOGIC;
  signal delayline_n_31 : STD_LOGIC;
  signal math_n_0 : STD_LOGIC;
  signal math_n_1 : STD_LOGIC;
  signal math_n_10 : STD_LOGIC;
  signal math_n_11 : STD_LOGIC;
  signal math_n_12 : STD_LOGIC;
  signal math_n_13 : STD_LOGIC;
  signal math_n_14 : STD_LOGIC;
  signal math_n_15 : STD_LOGIC;
  signal math_n_16 : STD_LOGIC;
  signal math_n_17 : STD_LOGIC;
  signal math_n_18 : STD_LOGIC;
  signal math_n_19 : STD_LOGIC;
  signal math_n_2 : STD_LOGIC;
  signal math_n_20 : STD_LOGIC;
  signal math_n_21 : STD_LOGIC;
  signal math_n_22 : STD_LOGIC;
  signal math_n_23 : STD_LOGIC;
  signal math_n_24 : STD_LOGIC;
  signal math_n_25 : STD_LOGIC;
  signal math_n_26 : STD_LOGIC;
  signal math_n_27 : STD_LOGIC;
  signal math_n_28 : STD_LOGIC;
  signal math_n_29 : STD_LOGIC;
  signal math_n_3 : STD_LOGIC;
  signal math_n_30 : STD_LOGIC;
  signal math_n_31 : STD_LOGIC;
  signal math_n_4 : STD_LOGIC;
  signal math_n_5 : STD_LOGIC;
  signal math_n_6 : STD_LOGIC;
  signal math_n_7 : STD_LOGIC;
  signal math_n_8 : STD_LOGIC;
  signal math_n_9 : STD_LOGIC;
  signal mcode_cmp_en_net : STD_LOGIC;
  signal mcode_cmp_rst_net : STD_LOGIC;
  signal mcode_pkd_net : STD_LOGIC;
  signal risingedgetrigger_n_0 : STD_LOGIC;
  signal single_port_ram_data_out_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
delayline: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_delayline
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3) => delayline_n_24,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(2) => delayline_n_25,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(1) => delayline_n_26,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(0) => delayline_n_27,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3) => delayline_n_28,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(2) => delayline_n_29,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(1) => delayline_n_30,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(0) => delayline_n_31,
      DI(3) => delayline_n_16,
      DI(2) => delayline_n_17,
      DI(1) => delayline_n_18,
      DI(0) => delayline_n_19,
      S(3) => delayline_n_20,
      S(2) => delayline_n_21,
      S(1) => delayline_n_22,
      S(0) => delayline_n_23,
      douta(15 downto 0) => single_port_ram_data_out_net(15 downto 0),
      o(15 downto 0) => \^o\(15 downto 0),
      \op_mem_37_22_reg[0]\(9 downto 0) => \op_mem_37_22_reg[0]\(9 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
math: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_math
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3) => math_n_4,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(2) => math_n_5,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(1) => math_n_6,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(0) => math_n_7,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3) => math_n_12,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(2) => math_n_13,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(1) => math_n_14,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(0) => math_n_15,
      DI(3) => math_n_8,
      DI(2) => math_n_9,
      DI(1) => math_n_10,
      DI(0) => math_n_11,
      S(3) => math_n_0,
      S(2) => math_n_1,
      S(1) => math_n_2,
      S(0) => math_n_3,
      douta(15 downto 0) => single_port_ram_data_out_net(15 downto 0),
      o(15 downto 0) => \^o\(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3) => math_n_16,
      \x[14]\(2) => math_n_17,
      \x[14]\(1) => math_n_18,
      \x[14]\(0) => math_n_19,
      \x[14]_0\(3) => math_n_24,
      \x[14]_0\(2) => math_n_25,
      \x[14]_0\(1) => math_n_26,
      \x[14]_0\(0) => math_n_27,
      \x[6]\(3) => math_n_20,
      \x[6]\(2) => math_n_21,
      \x[6]\(1) => math_n_22,
      \x[6]\(0) => math_n_23,
      \x[6]_0\(3) => math_n_28,
      \x[6]_0\(2) => math_n_29,
      \x[6]_0\(1) => math_n_30,
      \x[6]_0\(0) => math_n_31
    );
mcode: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_a80b0cfdd5
     port map (
      DI(3) => math_n_8,
      DI(2) => math_n_9,
      DI(1) => math_n_10,
      DI(0) => math_n_11,
      S(3) => math_n_0,
      S(2) => math_n_1,
      S(1) => math_n_2,
      S(0) => math_n_3,
      SR(0) => mcode_cmp_rst_net,
      mcode_cmp_en_net => mcode_cmp_en_net,
      mcode_pkd_net => mcode_pkd_net,
      peak_amp_rdy(0) => peak_amp_rdy(0),
      peak_amp_rdy_0_sp_1 => risingedgetrigger_n_0,
      pkd_i_16_23_reg_0(3 downto 0) => pkd_i_16_23_reg(3 downto 0),
      pkd_i_16_23_reg_1(3 downto 0) => pkd_i_16_23_reg_0(3 downto 0),
      pkd_i_16_23_reg_2(3 downto 0) => pkd_i_16_23_reg_1(3 downto 0),
      pkd_i_16_23_reg_3(3 downto 0) => pkd_i_16_23_reg_2(3 downto 0),
      q(0) => \^q\(0),
      \rel_35_16_carry__0_0\(3 downto 0) => \rel_35_16_carry__0\(3 downto 0),
      \rel_35_16_carry__0_1\(3 downto 0) => S(3 downto 0),
      \rel_47_16_carry__0_0\(3) => math_n_28,
      \rel_47_16_carry__0_0\(2) => math_n_29,
      \rel_47_16_carry__0_0\(1) => math_n_30,
      \rel_47_16_carry__0_0\(0) => math_n_31,
      \rel_47_16_carry__0_1\(3) => math_n_20,
      \rel_47_16_carry__0_1\(2) => math_n_21,
      \rel_47_16_carry__0_1\(1) => math_n_22,
      \rel_47_16_carry__0_1\(0) => math_n_23,
      \rel_55_16_carry__0_0\(3 downto 0) => \rel_55_16_carry__0\(3 downto 0),
      \rel_55_16_carry__0_1\(3 downto 0) => \rel_55_16_carry__0_0\(3 downto 0),
      \rel_55_30_carry__0_0\(3 downto 0) => \rel_55_30_carry__0\(3 downto 0),
      \rel_55_30_carry__0_1\(3 downto 0) => \rel_55_30_carry__0_0\(3 downto 0),
      \rel_61_16_carry__0_0\(3 downto 0) => DI(3 downto 0),
      \rel_61_16_carry__0_1\(3 downto 0) => \rel_61_16_carry__0\(3 downto 0),
      \rel_61_30_carry__0_0\(3) => delayline_n_16,
      \rel_61_30_carry__0_0\(2) => delayline_n_17,
      \rel_61_30_carry__0_0\(1) => delayline_n_18,
      \rel_61_30_carry__0_0\(0) => delayline_n_19,
      \rel_61_30_carry__0_1\(3) => delayline_n_20,
      \rel_61_30_carry__0_1\(2) => delayline_n_21,
      \rel_61_30_carry__0_1\(1) => delayline_n_22,
      \rel_61_30_carry__0_1\(0) => delayline_n_23,
      \rel_63_21_carry__0_0\(3) => math_n_12,
      \rel_63_21_carry__0_0\(2) => math_n_13,
      \rel_63_21_carry__0_0\(1) => math_n_14,
      \rel_63_21_carry__0_0\(0) => math_n_15,
      \rel_63_21_carry__0_1\(3) => math_n_4,
      \rel_63_21_carry__0_1\(2) => math_n_5,
      \rel_63_21_carry__0_1\(1) => math_n_6,
      \rel_63_21_carry__0_1\(0) => math_n_7,
      signaldomain_clk => signaldomain_clk,
      \state_15_23[0]_i_3_0\(3) => delayline_n_24,
      \state_15_23[0]_i_3_0\(2) => delayline_n_25,
      \state_15_23[0]_i_3_0\(1) => delayline_n_26,
      \state_15_23[0]_i_3_0\(0) => delayline_n_27,
      \state_15_23[0]_i_3_1\(3) => delayline_n_28,
      \state_15_23[0]_i_3_1\(2) => delayline_n_29,
      \state_15_23[0]_i_3_1\(1) => delayline_n_30,
      \state_15_23[0]_i_3_1\(0) => delayline_n_31,
      \state_15_23[0]_i_3_2\(3 downto 0) => \state_15_23[0]_i_3\(3 downto 0),
      \state_15_23[0]_i_3_3\(3 downto 0) => \state_15_23[0]_i_3_0\(3 downto 0),
      \state_15_23[1]_i_2_0\(3 downto 0) => \state_15_23[1]_i_2\(3 downto 0),
      \state_15_23[1]_i_2_1\(3 downto 0) => \state_15_23[1]_i_2_0\(3 downto 0),
      \state_15_23[2]_i_2_0\(3) => math_n_24,
      \state_15_23[2]_i_2_0\(2) => math_n_25,
      \state_15_23[2]_i_2_0\(1) => math_n_26,
      \state_15_23[2]_i_2_0\(0) => math_n_27,
      \state_15_23[2]_i_2_1\(3) => math_n_16,
      \state_15_23[2]_i_2_1\(2) => math_n_17,
      \state_15_23[2]_i_2_1\(1) => math_n_18,
      \state_15_23[2]_i_2_1\(0) => math_n_19
    );
mcode1: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_0c3d4c1851
     port map (
      CO(0) => CO(0),
      O(3 downto 0) => O(3 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      SR(0) => mcode_cmp_rst_net,
      d(13 downto 0) => d(13 downto 0),
      mcode_cmp_en_net => mcode_cmp_en_net,
      \reg_array[11].fde_used.u2\(3 downto 0) => \reg_array[11].fde_used.u2\(3 downto 0),
      \reg_array[12].fde_used.u2\(1 downto 0) => \reg_array[12].fde_used.u2\(1 downto 0),
      \reg_array[7].fde_used.u2\(3 downto 0) => \reg_array[7].fde_used.u2\(3 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \xmax_i_12_24_reg[2]_0\(0) => \xmax_i_12_24_reg[2]\(0)
    );
risingedgetrigger: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_risingedgetrigger
     port map (
      \ff_2_17_reg[0]\ => risingedgetrigger_n_0,
      mcode_pkd_net => mcode_pkd_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_signaldomain is
  port (
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_35_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \op_mem_37_22_reg[0]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \^q\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_signaldomain;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_signaldomain is
  signal \^q_1\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \convert4/result\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal format_dac_n_0 : STD_LOGIC;
  signal format_dac_n_1 : STD_LOGIC;
  signal format_dac_n_10 : STD_LOGIC;
  signal format_dac_n_11 : STD_LOGIC;
  signal format_dac_n_12 : STD_LOGIC;
  signal format_dac_n_13 : STD_LOGIC;
  signal format_dac_n_14 : STD_LOGIC;
  signal format_dac_n_2 : STD_LOGIC;
  signal format_dac_n_3 : STD_LOGIC;
  signal format_dac_n_4 : STD_LOGIC;
  signal format_dac_n_5 : STD_LOGIC;
  signal format_dac_n_6 : STD_LOGIC;
  signal format_dac_n_7 : STD_LOGIC;
  signal format_dac_n_8 : STD_LOGIC;
  signal format_dac_n_9 : STD_LOGIC;
  signal peakdetector_n_30 : STD_LOGIC;
begin
  Q(15 downto 0) <= \^q_1\(15 downto 0);
format_dac: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_format_dac
     port map (
      CO(0) => format_dac_n_12,
      O(3) => format_dac_n_0,
      O(2) => format_dac_n_1,
      O(1) => format_dac_n_2,
      O(0) => format_dac_n_3,
      Q(13 downto 0) => \^q_1\(15 downto 2),
      S(0) => peakdetector_n_30,
      d(13 downto 0) => \convert4/result\(13 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      signaldomain_clk => signaldomain_clk,
      \xmax_i_12_24_reg[13]\(3) => format_dac_n_8,
      \xmax_i_12_24_reg[13]\(2) => format_dac_n_9,
      \xmax_i_12_24_reg[13]\(1) => format_dac_n_10,
      \xmax_i_12_24_reg[13]\(0) => format_dac_n_11,
      \xmax_i_12_24_reg[15]\(1) => format_dac_n_13,
      \xmax_i_12_24_reg[15]\(0) => format_dac_n_14,
      \xmax_i_12_24_reg[9]\(3) => format_dac_n_4,
      \xmax_i_12_24_reg[9]\(2) => format_dac_n_5,
      \xmax_i_12_24_reg[9]\(1) => format_dac_n_6,
      \xmax_i_12_24_reg[9]\(0) => format_dac_n_7
    );
peakdetector: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_peakdetector
     port map (
      CO(0) => format_dac_n_12,
      DI(3 downto 0) => DI(3 downto 0),
      O(3) => format_dac_n_0,
      O(2) => format_dac_n_1,
      O(1) => format_dac_n_2,
      O(0) => format_dac_n_3,
      Q(15 downto 0) => \^q_1\(15 downto 0),
      S(3 downto 0) => S(3 downto 0),
      d(13 downto 0) => \convert4/result\(13 downto 0),
      \^o\(15 downto 0) => o(15 downto 0),
      \op_mem_37_22_reg[0]\(9 downto 0) => \op_mem_37_22_reg[0]\(9 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      pkd_i_16_23_reg(3 downto 0) => pkd_i_16_23_reg(3 downto 0),
      pkd_i_16_23_reg_0(3 downto 0) => pkd_i_16_23_reg_0(3 downto 0),
      pkd_i_16_23_reg_1(3 downto 0) => pkd_i_16_23_reg_1(3 downto 0),
      pkd_i_16_23_reg_2(3 downto 0) => pkd_i_16_23_reg_2(3 downto 0),
      \^q\(0) => \^q\(0),
      \reg_array[11].fde_used.u2\(3) => format_dac_n_8,
      \reg_array[11].fde_used.u2\(2) => format_dac_n_9,
      \reg_array[11].fde_used.u2\(1) => format_dac_n_10,
      \reg_array[11].fde_used.u2\(0) => format_dac_n_11,
      \reg_array[12].fde_used.u2\(1) => format_dac_n_13,
      \reg_array[12].fde_used.u2\(0) => format_dac_n_14,
      \reg_array[7].fde_used.u2\(3) => format_dac_n_4,
      \reg_array[7].fde_used.u2\(2) => format_dac_n_5,
      \reg_array[7].fde_used.u2\(1) => format_dac_n_6,
      \reg_array[7].fde_used.u2\(0) => format_dac_n_7,
      \rel_35_16_carry__0\(3 downto 0) => \rel_35_16_carry__0\(3 downto 0),
      \rel_55_16_carry__0\(3 downto 0) => \rel_55_16_carry__0\(3 downto 0),
      \rel_55_16_carry__0_0\(3 downto 0) => \rel_55_16_carry__0_0\(3 downto 0),
      \rel_55_30_carry__0\(3 downto 0) => \rel_55_30_carry__0\(3 downto 0),
      \rel_55_30_carry__0_0\(3 downto 0) => \rel_55_30_carry__0_0\(3 downto 0),
      \rel_61_16_carry__0\(3 downto 0) => \rel_61_16_carry__0\(3 downto 0),
      signaldomain_clk => signaldomain_clk,
      \state_15_23[0]_i_3\(3 downto 0) => \state_15_23[0]_i_3\(3 downto 0),
      \state_15_23[0]_i_3_0\(3 downto 0) => \state_15_23[0]_i_3_0\(3 downto 0),
      \state_15_23[1]_i_2\(3 downto 0) => \state_15_23[1]_i_2\(3 downto 0),
      \state_15_23[1]_i_2_0\(3 downto 0) => \state_15_23[1]_i_2_0\(3 downto 0),
      x(15 downto 0) => x(15 downto 0),
      \xmax_i_12_24_reg[2]\(0) => peakdetector_n_30
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_struct is
  port (
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_x_delay : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r5_x_noise : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r6_x_min : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r7_x_max : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r8_en_pkd : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_struct;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_struct is
  signal register1_n_0 : STD_LOGIC;
  signal register1_n_1 : STD_LOGIC;
  signal register1_n_2 : STD_LOGIC;
  signal register1_n_20 : STD_LOGIC;
  signal register1_n_21 : STD_LOGIC;
  signal register1_n_22 : STD_LOGIC;
  signal register1_n_23 : STD_LOGIC;
  signal register1_n_24 : STD_LOGIC;
  signal register1_n_25 : STD_LOGIC;
  signal register1_n_26 : STD_LOGIC;
  signal register1_n_27 : STD_LOGIC;
  signal register1_n_28 : STD_LOGIC;
  signal register1_n_29 : STD_LOGIC;
  signal register1_n_3 : STD_LOGIC;
  signal register1_n_30 : STD_LOGIC;
  signal register1_n_31 : STD_LOGIC;
  signal register1_n_32 : STD_LOGIC;
  signal register1_n_33 : STD_LOGIC;
  signal register1_n_34 : STD_LOGIC;
  signal register1_n_35 : STD_LOGIC;
  signal register1_n_36 : STD_LOGIC;
  signal register1_n_37 : STD_LOGIC;
  signal register1_n_38 : STD_LOGIC;
  signal register1_n_39 : STD_LOGIC;
  signal register1_n_40 : STD_LOGIC;
  signal register1_n_41 : STD_LOGIC;
  signal register1_n_42 : STD_LOGIC;
  signal register1_n_43 : STD_LOGIC;
  signal register1_n_44 : STD_LOGIC;
  signal register1_n_45 : STD_LOGIC;
  signal register1_n_46 : STD_LOGIC;
  signal register1_n_47 : STD_LOGIC;
  signal register1_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register2_n_0 : STD_LOGIC;
  signal register2_n_1 : STD_LOGIC;
  signal register2_n_10 : STD_LOGIC;
  signal register2_n_11 : STD_LOGIC;
  signal register2_n_12 : STD_LOGIC;
  signal register2_n_13 : STD_LOGIC;
  signal register2_n_14 : STD_LOGIC;
  signal register2_n_15 : STD_LOGIC;
  signal register2_n_2 : STD_LOGIC;
  signal register2_n_3 : STD_LOGIC;
  signal register2_n_4 : STD_LOGIC;
  signal register2_n_5 : STD_LOGIC;
  signal register2_n_6 : STD_LOGIC;
  signal register2_n_7 : STD_LOGIC;
  signal register2_n_8 : STD_LOGIC;
  signal register2_n_9 : STD_LOGIC;
  signal register3_n_0 : STD_LOGIC;
  signal register3_n_1 : STD_LOGIC;
  signal register3_n_10 : STD_LOGIC;
  signal register3_n_11 : STD_LOGIC;
  signal register3_n_12 : STD_LOGIC;
  signal register3_n_13 : STD_LOGIC;
  signal register3_n_14 : STD_LOGIC;
  signal register3_n_15 : STD_LOGIC;
  signal register3_n_2 : STD_LOGIC;
  signal register3_n_3 : STD_LOGIC;
  signal register3_n_4 : STD_LOGIC;
  signal register3_n_5 : STD_LOGIC;
  signal register3_n_6 : STD_LOGIC;
  signal register3_n_7 : STD_LOGIC;
  signal register3_n_8 : STD_LOGIC;
  signal register3_n_9 : STD_LOGIC;
  signal register4_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register_q_net : STD_LOGIC_VECTOR ( 9 downto 0 );
begin
register1: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister
     port map (
      DI(3) => register1_n_20,
      DI(2) => register1_n_21,
      DI(1) => register1_n_22,
      DI(0) => register1_n_23,
      S(3) => register1_n_0,
      S(2) => register1_n_1,
      S(1) => register1_n_2,
      S(0) => register1_n_3,
      axibusdomain_clk => axibusdomain_clk,
      \fd_prim_array[14].bit_is_0.fdre_comp\(3) => register1_n_24,
      \fd_prim_array[14].bit_is_0.fdre_comp\(2) => register1_n_25,
      \fd_prim_array[14].bit_is_0.fdre_comp\(1) => register1_n_26,
      \fd_prim_array[14].bit_is_0.fdre_comp\(0) => register1_n_27,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3) => register1_n_32,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(2) => register1_n_33,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(1) => register1_n_34,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(0) => register1_n_35,
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(3) => register1_n_44,
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(2) => register1_n_45,
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(1) => register1_n_46,
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(0) => register1_n_47,
      \fd_prim_array[6].bit_is_0.fdre_comp\(3) => register1_n_36,
      \fd_prim_array[6].bit_is_0.fdre_comp\(2) => register1_n_37,
      \fd_prim_array[6].bit_is_0.fdre_comp\(1) => register1_n_38,
      \fd_prim_array[6].bit_is_0.fdre_comp\(0) => register1_n_39,
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(3) => register1_n_40,
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(2) => register1_n_41,
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(1) => register1_n_42,
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(0) => register1_n_43,
      o(15 downto 0) => register1_q_net(15 downto 0),
      r5_x_noise(15 downto 0) => r5_x_noise(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3) => register1_n_28,
      \x[14]\(2) => register1_n_29,
      \x[14]\(1) => register1_n_30,
      \x[14]\(0) => register1_n_31
    );
register2: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_0
     port map (
      DI(3) => register2_n_8,
      DI(2) => register2_n_9,
      DI(1) => register2_n_10,
      DI(0) => register2_n_11,
      S(3) => register2_n_0,
      S(2) => register2_n_1,
      S(1) => register2_n_2,
      S(0) => register2_n_3,
      axibusdomain_clk => axibusdomain_clk,
      \fd_prim_array[14].bit_is_0.fdre_comp\(3) => register2_n_4,
      \fd_prim_array[14].bit_is_0.fdre_comp\(2) => register2_n_5,
      \fd_prim_array[14].bit_is_0.fdre_comp\(1) => register2_n_6,
      \fd_prim_array[14].bit_is_0.fdre_comp\(0) => register2_n_7,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3) => register2_n_12,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(2) => register2_n_13,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(1) => register2_n_14,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(0) => register2_n_15,
      r6_x_min(15 downto 0) => r6_x_min(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
register3: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_1
     port map (
      DI(3) => register3_n_8,
      DI(2) => register3_n_9,
      DI(1) => register3_n_10,
      DI(0) => register3_n_11,
      S(3) => register3_n_0,
      S(2) => register3_n_1,
      S(1) => register3_n_2,
      S(0) => register3_n_3,
      axibusdomain_clk => axibusdomain_clk,
      \fd_prim_array[14].bit_is_0.fdre_comp\(3) => register3_n_4,
      \fd_prim_array[14].bit_is_0.fdre_comp\(2) => register3_n_5,
      \fd_prim_array[14].bit_is_0.fdre_comp\(1) => register3_n_6,
      \fd_prim_array[14].bit_is_0.fdre_comp\(0) => register3_n_7,
      r7_x_max(15 downto 0) => r7_x_max(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3) => register3_n_12,
      \x[14]\(2) => register3_n_13,
      \x[14]\(1) => register3_n_14,
      \x[14]\(0) => register3_n_15
    );
register4: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_2
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register4_q_net(0),
      r8_en_pkd(0) => r8_en_pkd(0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_xlAsynRegister_3
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(9 downto 0) => register_q_net(9 downto 0),
      r4_x_delay(9 downto 0) => r4_x_delay(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
signaldomain: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_signaldomain
     port map (
      DI(3) => register1_n_20,
      DI(2) => register1_n_21,
      DI(1) => register1_n_22,
      DI(0) => register1_n_23,
      Q(15 downto 0) => Q(15 downto 0),
      S(3) => register1_n_0,
      S(2) => register1_n_1,
      S(1) => register1_n_2,
      S(0) => register1_n_3,
      o(15 downto 0) => register1_q_net(15 downto 0),
      \op_mem_37_22_reg[0]\(9 downto 0) => register_q_net(9 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      pkd_i_16_23_reg(3) => register3_n_12,
      pkd_i_16_23_reg(2) => register3_n_13,
      pkd_i_16_23_reg(1) => register3_n_14,
      pkd_i_16_23_reg(0) => register3_n_15,
      pkd_i_16_23_reg_0(3) => register3_n_4,
      pkd_i_16_23_reg_0(2) => register3_n_5,
      pkd_i_16_23_reg_0(1) => register3_n_6,
      pkd_i_16_23_reg_0(0) => register3_n_7,
      pkd_i_16_23_reg_1(3) => register2_n_12,
      pkd_i_16_23_reg_1(2) => register2_n_13,
      pkd_i_16_23_reg_1(1) => register2_n_14,
      pkd_i_16_23_reg_1(0) => register2_n_15,
      pkd_i_16_23_reg_2(3) => register2_n_4,
      pkd_i_16_23_reg_2(2) => register2_n_5,
      pkd_i_16_23_reg_2(1) => register2_n_6,
      pkd_i_16_23_reg_2(0) => register2_n_7,
      \^q\(0) => register4_q_net(0),
      \rel_35_16_carry__0\(3) => register1_n_36,
      \rel_35_16_carry__0\(2) => register1_n_37,
      \rel_35_16_carry__0\(1) => register1_n_38,
      \rel_35_16_carry__0\(0) => register1_n_39,
      \rel_55_16_carry__0\(3) => register2_n_8,
      \rel_55_16_carry__0\(2) => register2_n_9,
      \rel_55_16_carry__0\(1) => register2_n_10,
      \rel_55_16_carry__0\(0) => register2_n_11,
      \rel_55_16_carry__0_0\(3) => register2_n_0,
      \rel_55_16_carry__0_0\(2) => register2_n_1,
      \rel_55_16_carry__0_0\(1) => register2_n_2,
      \rel_55_16_carry__0_0\(0) => register2_n_3,
      \rel_55_30_carry__0\(3) => register3_n_8,
      \rel_55_30_carry__0\(2) => register3_n_9,
      \rel_55_30_carry__0\(1) => register3_n_10,
      \rel_55_30_carry__0\(0) => register3_n_11,
      \rel_55_30_carry__0_0\(3) => register3_n_0,
      \rel_55_30_carry__0_0\(2) => register3_n_1,
      \rel_55_30_carry__0_0\(1) => register3_n_2,
      \rel_55_30_carry__0_0\(0) => register3_n_3,
      \rel_61_16_carry__0\(3) => register1_n_40,
      \rel_61_16_carry__0\(2) => register1_n_41,
      \rel_61_16_carry__0\(1) => register1_n_42,
      \rel_61_16_carry__0\(0) => register1_n_43,
      signaldomain_clk => signaldomain_clk,
      \state_15_23[0]_i_3\(3) => register1_n_28,
      \state_15_23[0]_i_3\(2) => register1_n_29,
      \state_15_23[0]_i_3\(1) => register1_n_30,
      \state_15_23[0]_i_3\(0) => register1_n_31,
      \state_15_23[0]_i_3_0\(3) => register1_n_44,
      \state_15_23[0]_i_3_0\(2) => register1_n_45,
      \state_15_23[0]_i_3_0\(1) => register1_n_46,
      \state_15_23[0]_i_3_0\(0) => register1_n_47,
      \state_15_23[1]_i_2\(3) => register1_n_32,
      \state_15_23[1]_i_2\(2) => register1_n_33,
      \state_15_23[1]_i_2\(1) => register1_n_34,
      \state_15_23[1]_i_2\(0) => register1_n_35,
      \state_15_23[1]_i_2_0\(3) => register1_n_24,
      \state_15_23[1]_i_2_0\(2) => register1_n_25,
      \state_15_23[1]_i_2_0\(1) => register1_n_26,
      \state_15_23[1]_i_2_0\(0) => register1_n_27,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector is
  port (
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    peak_amp : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector is
  signal \<const0>\ : STD_LOGIC;
  signal r4_x_delay : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal r5_x_noise : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r6_x_min : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r7_x_max : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r8_en_pkd : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  axibusdomain_s_axi_bresp(1) <= \<const0>\;
  axibusdomain_s_axi_bresp(0) <= \<const0>\;
  axibusdomain_s_axi_rresp(1) <= \<const0>\;
  axibusdomain_s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
axibusdomain_axi_lite_interface: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(4 downto 0) => axibusdomain_s_axi_araddr(4 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(4 downto 0) => axibusdomain_s_axi_awaddr(4 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => r8_en_pkd(0),
      i(15 downto 0) => r7_x_max(15 downto 0),
      \slv_reg_array_reg[2][15]\(15 downto 0) => r6_x_min(15 downto 0),
      \slv_reg_array_reg[3][15]\(15 downto 0) => r5_x_noise(15 downto 0),
      \slv_reg_array_reg[4][9]\(9 downto 0) => r4_x_delay(9 downto 0)
    );
ip_peakdetector_struct: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector_struct
     port map (
      Q(15 downto 0) => peak_amp(15 downto 0),
      axibusdomain_clk => axibusdomain_clk,
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      r4_x_delay(9 downto 0) => r4_x_delay(9 downto 0),
      r5_x_noise(15 downto 0) => r5_x_noise(15 downto 0),
      r6_x_min(15 downto 0) => r6_x_min(15 downto 0),
      r7_x_max(15 downto 0) => r7_x_max(15 downto 0),
      r8_en_pkd(0) => r8_en_pkd(0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    peak_amp : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ip_peakdetector_0_0,ip_peakdetector,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sysgen";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ip_peakdetector,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of axibusdomain_aresetn : signal is "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of axibusdomain_aresetn : signal is "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_clk : signal is "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of axibusdomain_clk : signal is "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID";
  attribute X_INTERFACE_PARAMETER of axibusdomain_s_axi_rvalid : signal is "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID";
  attribute X_INTERFACE_INFO of signaldomain_clk : signal is "xilinx.com:signal:clock:1.0 signaldomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of signaldomain_clk : signal is "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB";
  attribute X_INTERFACE_INFO of peak_amp : signal is "xilinx.com:signal:data:1.0 peak_amp DATA";
  attribute X_INTERFACE_PARAMETER of peak_amp : signal is "XIL_INTERFACENAME peak_amp, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}";
  attribute X_INTERFACE_INFO of peak_amp_dac : signal is "xilinx.com:signal:data:1.0 peak_amp_dac DATA";
  attribute X_INTERFACE_PARAMETER of peak_amp_dac : signal is "XIL_INTERFACENAME peak_amp_dac, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}";
  attribute X_INTERFACE_INFO of peak_amp_rdy : signal is "xilinx.com:signal:data:1.0 peak_amp_rdy DATA";
  attribute X_INTERFACE_PARAMETER of peak_amp_rdy : signal is "XIL_INTERFACENAME peak_amp_rdy, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}, PortType data, PortType.PROP_SRC false";
  attribute X_INTERFACE_INFO of x : signal is "xilinx.com:signal:data:1.0 x DATA";
  attribute X_INTERFACE_PARAMETER of x : signal is "XIL_INTERFACENAME x, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_peakdetector
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(4 downto 0) => axibusdomain_s_axi_araddr(4 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(4 downto 0) => axibusdomain_s_axi_awaddr(4 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bresp(1 downto 0) => axibusdomain_s_axi_bresp(1 downto 0),
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rresp(1 downto 0) => axibusdomain_s_axi_rresp(1 downto 0),
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      peak_amp(15 downto 0) => peak_amp(15 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
