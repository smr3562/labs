// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 17:55:17 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_timers_0_0_sim_netlist.v
// Design      : design_1_ip_timers_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_lt_enable
   (geqOp,
    \slv_reg5_reg[3] ,
    lt_clk_en_i_reg_0,
    lt_clk_en_i_reg_1,
    ce,
    \tmp_reg[16]_0 ,
    clk,
    \tmp_reg[16]_1 ,
    lt_dir,
    Q,
    clk_1kHz_en,
    clk_1kHz_enb,
    clk_1kHz_enc,
    lt_clk_en_i_reg_2);
  output geqOp;
  output \slv_reg5_reg[3] ;
  output lt_clk_en_i_reg_0;
  output lt_clk_en_i_reg_1;
  output ce;
  input \tmp_reg[16]_0 ;
  input clk;
  input \tmp_reg[16]_1 ;
  input [1:0]lt_dir;
  input [5:0]Q;
  input clk_1kHz_en;
  input clk_1kHz_enb;
  input clk_1kHz_enc;
  input lt_clk_en_i_reg_2;

  wire [5:0]Q;
  wire ce;
  wire clk;
  wire clk_1kHz_en;
  wire clk_1kHz_enb;
  wire clk_1kHz_enc;
  wire clk_lt_en;
  wire geqOp;
  wire lt_clk_en_i_i_1_n_0;
  wire lt_clk_en_i_reg_0;
  wire lt_clk_en_i_reg_1;
  wire lt_clk_en_i_reg_2;
  wire [1:0]lt_dir;
  wire \slv_reg5_reg[3] ;
  wire \tmp[0]_i_10_n_0 ;
  wire \tmp[0]_i_11_n_0 ;
  wire \tmp[0]_i_12_n_0 ;
  wire \tmp[0]_i_13_n_0 ;
  wire \tmp[0]_i_14_n_0 ;
  wire \tmp[0]_i_15_n_0 ;
  wire \tmp[0]_i_6_n_0 ;
  wire \tmp[0]_i_7_n_0 ;
  wire \tmp[0]_i_8_n_0 ;
  wire \tmp[0]_i_9_n_0 ;
  wire \tmp[12]_i_2_n_0 ;
  wire \tmp[12]_i_3_n_0 ;
  wire \tmp[12]_i_4_n_0 ;
  wire \tmp[12]_i_5_n_0 ;
  wire \tmp[16]_i_2_n_0 ;
  wire \tmp[4]_i_2_n_0 ;
  wire \tmp[4]_i_3_n_0 ;
  wire \tmp[4]_i_4_n_0 ;
  wire \tmp[4]_i_5_n_0 ;
  wire \tmp[8]_i_2_n_0 ;
  wire \tmp[8]_i_3_n_0 ;
  wire \tmp[8]_i_4_n_0 ;
  wire \tmp[8]_i_5_n_0 ;
  wire [16:0]tmp_reg;
  wire \tmp_reg[0]_i_2_n_0 ;
  wire \tmp_reg[0]_i_2_n_1 ;
  wire \tmp_reg[0]_i_2_n_2 ;
  wire \tmp_reg[0]_i_2_n_3 ;
  wire \tmp_reg[0]_i_2_n_4 ;
  wire \tmp_reg[0]_i_2_n_5 ;
  wire \tmp_reg[0]_i_2_n_6 ;
  wire \tmp_reg[0]_i_2_n_7 ;
  wire \tmp_reg[12]_i_1_n_0 ;
  wire \tmp_reg[12]_i_1_n_1 ;
  wire \tmp_reg[12]_i_1_n_2 ;
  wire \tmp_reg[12]_i_1_n_3 ;
  wire \tmp_reg[12]_i_1_n_4 ;
  wire \tmp_reg[12]_i_1_n_5 ;
  wire \tmp_reg[12]_i_1_n_6 ;
  wire \tmp_reg[12]_i_1_n_7 ;
  wire \tmp_reg[16]_0 ;
  wire \tmp_reg[16]_1 ;
  wire \tmp_reg[16]_i_1_n_7 ;
  wire \tmp_reg[4]_i_1_n_0 ;
  wire \tmp_reg[4]_i_1_n_1 ;
  wire \tmp_reg[4]_i_1_n_2 ;
  wire \tmp_reg[4]_i_1_n_3 ;
  wire \tmp_reg[4]_i_1_n_4 ;
  wire \tmp_reg[4]_i_1_n_5 ;
  wire \tmp_reg[4]_i_1_n_6 ;
  wire \tmp_reg[4]_i_1_n_7 ;
  wire \tmp_reg[8]_i_1_n_0 ;
  wire \tmp_reg[8]_i_1_n_1 ;
  wire \tmp_reg[8]_i_1_n_2 ;
  wire \tmp_reg[8]_i_1_n_3 ;
  wire \tmp_reg[8]_i_1_n_4 ;
  wire \tmp_reg[8]_i_1_n_5 ;
  wire \tmp_reg[8]_i_1_n_6 ;
  wire \tmp_reg[8]_i_1_n_7 ;
  wire [3:0]\NLW_tmp_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_tmp_reg[16]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hAFBFBFBFAAAAAAAA)) 
    lt_clk_en_i_i_1
       (.I0(geqOp),
        .I1(\slv_reg5_reg[3] ),
        .I2(lt_clk_en_i_reg_2),
        .I3(Q[5]),
        .I4(Q[2]),
        .I5(clk_lt_en),
        .O(lt_clk_en_i_i_1_n_0));
  LUT5 #(
    .INIT(32'h0000D888)) 
    lt_clk_en_i_i_2
       (.I0(Q[1]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\slv_reg5_reg[3] ));
  FDCE lt_clk_en_i_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(\tmp_reg[16]_1 ),
        .D(lt_clk_en_i_i_1_n_0),
        .Q(clk_lt_en));
  LUT4 #(
    .INIT(16'h0E01)) 
    \tmp[0]_i_10 
       (.I0(lt_dir[1]),
        .I1(lt_dir[0]),
        .I2(geqOp),
        .I3(tmp_reg[1]),
        .O(\tmp[0]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h010E)) 
    \tmp[0]_i_11 
       (.I0(lt_dir[1]),
        .I1(lt_dir[0]),
        .I2(geqOp),
        .I3(tmp_reg[0]),
        .O(\tmp[0]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \tmp[0]_i_12 
       (.I0(tmp_reg[11]),
        .I1(tmp_reg[14]),
        .O(\tmp[0]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h8880888088808080)) 
    \tmp[0]_i_13 
       (.I0(tmp_reg[10]),
        .I1(tmp_reg[9]),
        .I2(tmp_reg[8]),
        .I3(tmp_reg[7]),
        .I4(\tmp[0]_i_14_n_0 ),
        .I5(\tmp[0]_i_15_n_0 ),
        .O(\tmp[0]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \tmp[0]_i_14 
       (.I0(tmp_reg[0]),
        .I1(tmp_reg[1]),
        .I2(tmp_reg[2]),
        .I3(tmp_reg[4]),
        .I4(tmp_reg[3]),
        .O(\tmp[0]_i_14_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \tmp[0]_i_15 
       (.I0(tmp_reg[5]),
        .I1(tmp_reg[6]),
        .O(\tmp[0]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp[0]_i_1__3 
       (.I0(clk_lt_en),
        .I1(Q[2]),
        .I2(clk_1kHz_en),
        .O(lt_clk_en_i_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp[0]_i_1__4 
       (.I0(clk_lt_en),
        .I1(Q[1]),
        .I2(clk_1kHz_enb),
        .O(lt_clk_en_i_reg_1));
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp[0]_i_1__5 
       (.I0(clk_lt_en),
        .I1(Q[0]),
        .I2(clk_1kHz_enc),
        .O(ce));
  LUT6 #(
    .INIT(64'h8888888888888880)) 
    \tmp[0]_i_5 
       (.I0(tmp_reg[16]),
        .I1(tmp_reg[15]),
        .I2(\tmp[0]_i_12_n_0 ),
        .I3(tmp_reg[13]),
        .I4(tmp_reg[12]),
        .I5(\tmp[0]_i_13_n_0 ),
        .O(geqOp));
  LUT3 #(
    .INIT(8'h01)) 
    \tmp[0]_i_6 
       (.I0(lt_dir[0]),
        .I1(lt_dir[1]),
        .I2(geqOp),
        .O(\tmp[0]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'h54)) 
    \tmp[0]_i_7 
       (.I0(geqOp),
        .I1(lt_dir[0]),
        .I2(lt_dir[1]),
        .O(\tmp[0]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_8 
       (.I0(tmp_reg[3]),
        .I1(geqOp),
        .O(\tmp[0]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_9 
       (.I0(tmp_reg[2]),
        .I1(geqOp),
        .O(\tmp[0]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_2 
       (.I0(tmp_reg[15]),
        .I1(geqOp),
        .O(\tmp[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_3 
       (.I0(tmp_reg[14]),
        .I1(geqOp),
        .O(\tmp[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_4 
       (.I0(tmp_reg[13]),
        .I1(geqOp),
        .O(\tmp[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_5 
       (.I0(tmp_reg[12]),
        .I1(geqOp),
        .O(\tmp[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[16]_i_2 
       (.I0(tmp_reg[16]),
        .I1(geqOp),
        .O(\tmp[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_2 
       (.I0(tmp_reg[7]),
        .I1(geqOp),
        .O(\tmp[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_3 
       (.I0(tmp_reg[6]),
        .I1(geqOp),
        .O(\tmp[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_4 
       (.I0(tmp_reg[5]),
        .I1(geqOp),
        .O(\tmp[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_5 
       (.I0(tmp_reg[4]),
        .I1(geqOp),
        .O(\tmp[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_2 
       (.I0(tmp_reg[11]),
        .I1(geqOp),
        .O(\tmp[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_3 
       (.I0(tmp_reg[10]),
        .I1(geqOp),
        .O(\tmp[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_4 
       (.I0(tmp_reg[9]),
        .I1(geqOp),
        .O(\tmp[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_5 
       (.I0(tmp_reg[8]),
        .I1(geqOp),
        .O(\tmp[8]_i_5_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[0] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[0]_i_2_n_7 ),
        .Q(tmp_reg[0]));
  CARRY4 \tmp_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\tmp_reg[0]_i_2_n_0 ,\tmp_reg[0]_i_2_n_1 ,\tmp_reg[0]_i_2_n_2 ,\tmp_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\tmp[0]_i_6_n_0 ,\tmp[0]_i_7_n_0 }),
        .O({\tmp_reg[0]_i_2_n_4 ,\tmp_reg[0]_i_2_n_5 ,\tmp_reg[0]_i_2_n_6 ,\tmp_reg[0]_i_2_n_7 }),
        .S({\tmp[0]_i_8_n_0 ,\tmp[0]_i_9_n_0 ,\tmp[0]_i_10_n_0 ,\tmp[0]_i_11_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[10] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[8]_i_1_n_5 ),
        .Q(tmp_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[11] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[8]_i_1_n_4 ),
        .Q(tmp_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[12] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[12]_i_1_n_7 ),
        .Q(tmp_reg[12]));
  CARRY4 \tmp_reg[12]_i_1 
       (.CI(\tmp_reg[8]_i_1_n_0 ),
        .CO({\tmp_reg[12]_i_1_n_0 ,\tmp_reg[12]_i_1_n_1 ,\tmp_reg[12]_i_1_n_2 ,\tmp_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[12]_i_1_n_4 ,\tmp_reg[12]_i_1_n_5 ,\tmp_reg[12]_i_1_n_6 ,\tmp_reg[12]_i_1_n_7 }),
        .S({\tmp[12]_i_2_n_0 ,\tmp[12]_i_3_n_0 ,\tmp[12]_i_4_n_0 ,\tmp[12]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[13] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[12]_i_1_n_6 ),
        .Q(tmp_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[14] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[12]_i_1_n_5 ),
        .Q(tmp_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[15] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[12]_i_1_n_4 ),
        .Q(tmp_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[16] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[16]_i_1_n_7 ),
        .Q(tmp_reg[16]));
  CARRY4 \tmp_reg[16]_i_1 
       (.CI(\tmp_reg[12]_i_1_n_0 ),
        .CO(\NLW_tmp_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_tmp_reg[16]_i_1_O_UNCONNECTED [3:1],\tmp_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,\tmp[16]_i_2_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[1] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[0]_i_2_n_6 ),
        .Q(tmp_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[2] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[0]_i_2_n_5 ),
        .Q(tmp_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[3] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[0]_i_2_n_4 ),
        .Q(tmp_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[4] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[4]_i_1_n_7 ),
        .Q(tmp_reg[4]));
  CARRY4 \tmp_reg[4]_i_1 
       (.CI(\tmp_reg[0]_i_2_n_0 ),
        .CO({\tmp_reg[4]_i_1_n_0 ,\tmp_reg[4]_i_1_n_1 ,\tmp_reg[4]_i_1_n_2 ,\tmp_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[4]_i_1_n_4 ,\tmp_reg[4]_i_1_n_5 ,\tmp_reg[4]_i_1_n_6 ,\tmp_reg[4]_i_1_n_7 }),
        .S({\tmp[4]_i_2_n_0 ,\tmp[4]_i_3_n_0 ,\tmp[4]_i_4_n_0 ,\tmp[4]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[5] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[4]_i_1_n_6 ),
        .Q(tmp_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[6] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[4]_i_1_n_5 ),
        .Q(tmp_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[7] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[4]_i_1_n_4 ),
        .Q(tmp_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[8] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[8]_i_1_n_7 ),
        .Q(tmp_reg[8]));
  CARRY4 \tmp_reg[8]_i_1 
       (.CI(\tmp_reg[4]_i_1_n_0 ),
        .CO({\tmp_reg[8]_i_1_n_0 ,\tmp_reg[8]_i_1_n_1 ,\tmp_reg[8]_i_1_n_2 ,\tmp_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[8]_i_1_n_4 ,\tmp_reg[8]_i_1_n_5 ,\tmp_reg[8]_i_1_n_6 ,\tmp_reg[8]_i_1_n_7 }),
        .S({\tmp[8]_i_2_n_0 ,\tmp[8]_i_3_n_0 ,\tmp[8]_i_4_n_0 ,\tmp[8]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[9] 
       (.C(clk),
        .CE(\tmp_reg[16]_0 ),
        .CLR(\tmp_reg[16]_1 ),
        .D(\tmp_reg[8]_i_1_n_6 ),
        .Q(tmp_reg[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clock_1KHz_enable
   (clk_1kHz_en,
    eqOp,
    clk,
    clr1_out,
    \tmp_reg[15]_0 );
  output clk_1kHz_en;
  output eqOp;
  input clk;
  input clr1_out;
  input \tmp_reg[15]_0 ;

  wire clk;
  wire clk_1kHz_en;
  wire clk_1kHz_en_i_i_3_n_0;
  wire clk_1kHz_en_i_i_4_n_0;
  wire clk_1kHz_en_i_i_5_n_0;
  wire clr1_out;
  wire eqOp;
  wire \tmp[0]_i_3_n_0 ;
  wire \tmp[0]_i_4__0_n_0 ;
  wire \tmp[0]_i_5__0_n_0 ;
  wire \tmp[0]_i_6__0_n_0 ;
  wire \tmp[0]_i_7__0_n_0 ;
  wire \tmp[12]_i_2__0_n_0 ;
  wire \tmp[12]_i_3__0_n_0 ;
  wire \tmp[12]_i_4__0_n_0 ;
  wire \tmp[12]_i_5__0_n_0 ;
  wire \tmp[4]_i_2__0_n_0 ;
  wire \tmp[4]_i_3__0_n_0 ;
  wire \tmp[4]_i_4__0_n_0 ;
  wire \tmp[4]_i_5__0_n_0 ;
  wire \tmp[8]_i_2__0_n_0 ;
  wire \tmp[8]_i_3__0_n_0 ;
  wire \tmp[8]_i_4__0_n_0 ;
  wire \tmp[8]_i_5__0_n_0 ;
  wire [15:0]tmp_reg;
  wire \tmp_reg[0]_i_2__0_n_0 ;
  wire \tmp_reg[0]_i_2__0_n_1 ;
  wire \tmp_reg[0]_i_2__0_n_2 ;
  wire \tmp_reg[0]_i_2__0_n_3 ;
  wire \tmp_reg[0]_i_2__0_n_4 ;
  wire \tmp_reg[0]_i_2__0_n_5 ;
  wire \tmp_reg[0]_i_2__0_n_6 ;
  wire \tmp_reg[0]_i_2__0_n_7 ;
  wire \tmp_reg[12]_i_1__0_n_1 ;
  wire \tmp_reg[12]_i_1__0_n_2 ;
  wire \tmp_reg[12]_i_1__0_n_3 ;
  wire \tmp_reg[12]_i_1__0_n_4 ;
  wire \tmp_reg[12]_i_1__0_n_5 ;
  wire \tmp_reg[12]_i_1__0_n_6 ;
  wire \tmp_reg[12]_i_1__0_n_7 ;
  wire \tmp_reg[15]_0 ;
  wire \tmp_reg[4]_i_1__0_n_0 ;
  wire \tmp_reg[4]_i_1__0_n_1 ;
  wire \tmp_reg[4]_i_1__0_n_2 ;
  wire \tmp_reg[4]_i_1__0_n_3 ;
  wire \tmp_reg[4]_i_1__0_n_4 ;
  wire \tmp_reg[4]_i_1__0_n_5 ;
  wire \tmp_reg[4]_i_1__0_n_6 ;
  wire \tmp_reg[4]_i_1__0_n_7 ;
  wire \tmp_reg[8]_i_1__0_n_0 ;
  wire \tmp_reg[8]_i_1__0_n_1 ;
  wire \tmp_reg[8]_i_1__0_n_2 ;
  wire \tmp_reg[8]_i_1__0_n_3 ;
  wire \tmp_reg[8]_i_1__0_n_4 ;
  wire \tmp_reg[8]_i_1__0_n_5 ;
  wire \tmp_reg[8]_i_1__0_n_6 ;
  wire \tmp_reg[8]_i_1__0_n_7 ;
  wire [3:3]\NLW_tmp_reg[12]_i_1__0_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h8000000000000000)) 
    clk_1kHz_en_i_i_1
       (.I0(clk_1kHz_en_i_i_3_n_0),
        .I1(tmp_reg[1]),
        .I2(tmp_reg[0]),
        .I3(tmp_reg[3]),
        .I4(tmp_reg[2]),
        .I5(clk_1kHz_en_i_i_4_n_0),
        .O(eqOp));
  LUT4 #(
    .INIT(16'h0004)) 
    clk_1kHz_en_i_i_3
       (.I0(tmp_reg[7]),
        .I1(tmp_reg[6]),
        .I2(tmp_reg[5]),
        .I3(tmp_reg[4]),
        .O(clk_1kHz_en_i_i_3_n_0));
  LUT5 #(
    .INIT(32'h10000000)) 
    clk_1kHz_en_i_i_4
       (.I0(tmp_reg[12]),
        .I1(tmp_reg[13]),
        .I2(tmp_reg[14]),
        .I3(tmp_reg[15]),
        .I4(clk_1kHz_en_i_i_5_n_0),
        .O(clk_1kHz_en_i_i_4_n_0));
  LUT4 #(
    .INIT(16'h1000)) 
    clk_1kHz_en_i_i_5
       (.I0(tmp_reg[11]),
        .I1(tmp_reg[10]),
        .I2(tmp_reg[9]),
        .I3(tmp_reg[8]),
        .O(clk_1kHz_en_i_i_5_n_0));
  FDCE clk_1kHz_en_i_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(clr1_out),
        .D(eqOp),
        .Q(clk_1kHz_en));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_3 
       (.I0(tmp_reg[0]),
        .I1(eqOp),
        .O(\tmp[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_4__0 
       (.I0(tmp_reg[3]),
        .I1(eqOp),
        .O(\tmp[0]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_5__0 
       (.I0(tmp_reg[2]),
        .I1(eqOp),
        .O(\tmp[0]_i_5__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_6__0 
       (.I0(tmp_reg[1]),
        .I1(eqOp),
        .O(\tmp[0]_i_6__0_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \tmp[0]_i_7__0 
       (.I0(tmp_reg[0]),
        .I1(eqOp),
        .O(\tmp[0]_i_7__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_2__0 
       (.I0(tmp_reg[15]),
        .I1(eqOp),
        .O(\tmp[12]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_3__0 
       (.I0(tmp_reg[14]),
        .I1(eqOp),
        .O(\tmp[12]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_4__0 
       (.I0(tmp_reg[13]),
        .I1(eqOp),
        .O(\tmp[12]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_5__0 
       (.I0(tmp_reg[12]),
        .I1(eqOp),
        .O(\tmp[12]_i_5__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_2__0 
       (.I0(tmp_reg[7]),
        .I1(eqOp),
        .O(\tmp[4]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_3__0 
       (.I0(tmp_reg[6]),
        .I1(eqOp),
        .O(\tmp[4]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_4__0 
       (.I0(tmp_reg[5]),
        .I1(eqOp),
        .O(\tmp[4]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_5__0 
       (.I0(tmp_reg[4]),
        .I1(eqOp),
        .O(\tmp[4]_i_5__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_2__0 
       (.I0(tmp_reg[11]),
        .I1(eqOp),
        .O(\tmp[8]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_3__0 
       (.I0(tmp_reg[10]),
        .I1(eqOp),
        .O(\tmp[8]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_4__0 
       (.I0(tmp_reg[9]),
        .I1(eqOp),
        .O(\tmp[8]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_5__0 
       (.I0(tmp_reg[8]),
        .I1(eqOp),
        .O(\tmp[8]_i_5__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[0] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[0]_i_2__0_n_7 ),
        .Q(tmp_reg[0]));
  CARRY4 \tmp_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\tmp_reg[0]_i_2__0_n_0 ,\tmp_reg[0]_i_2__0_n_1 ,\tmp_reg[0]_i_2__0_n_2 ,\tmp_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\tmp[0]_i_3_n_0 }),
        .O({\tmp_reg[0]_i_2__0_n_4 ,\tmp_reg[0]_i_2__0_n_5 ,\tmp_reg[0]_i_2__0_n_6 ,\tmp_reg[0]_i_2__0_n_7 }),
        .S({\tmp[0]_i_4__0_n_0 ,\tmp[0]_i_5__0_n_0 ,\tmp[0]_i_6__0_n_0 ,\tmp[0]_i_7__0_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[10] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[8]_i_1__0_n_5 ),
        .Q(tmp_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[11] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[8]_i_1__0_n_4 ),
        .Q(tmp_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[12] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[12]_i_1__0_n_7 ),
        .Q(tmp_reg[12]));
  CARRY4 \tmp_reg[12]_i_1__0 
       (.CI(\tmp_reg[8]_i_1__0_n_0 ),
        .CO({\NLW_tmp_reg[12]_i_1__0_CO_UNCONNECTED [3],\tmp_reg[12]_i_1__0_n_1 ,\tmp_reg[12]_i_1__0_n_2 ,\tmp_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[12]_i_1__0_n_4 ,\tmp_reg[12]_i_1__0_n_5 ,\tmp_reg[12]_i_1__0_n_6 ,\tmp_reg[12]_i_1__0_n_7 }),
        .S({\tmp[12]_i_2__0_n_0 ,\tmp[12]_i_3__0_n_0 ,\tmp[12]_i_4__0_n_0 ,\tmp[12]_i_5__0_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[13] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[12]_i_1__0_n_6 ),
        .Q(tmp_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[14] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[12]_i_1__0_n_5 ),
        .Q(tmp_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[15] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[12]_i_1__0_n_4 ),
        .Q(tmp_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[1] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[0]_i_2__0_n_6 ),
        .Q(tmp_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[2] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[0]_i_2__0_n_5 ),
        .Q(tmp_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[3] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[0]_i_2__0_n_4 ),
        .Q(tmp_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[4] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[4]_i_1__0_n_7 ),
        .Q(tmp_reg[4]));
  CARRY4 \tmp_reg[4]_i_1__0 
       (.CI(\tmp_reg[0]_i_2__0_n_0 ),
        .CO({\tmp_reg[4]_i_1__0_n_0 ,\tmp_reg[4]_i_1__0_n_1 ,\tmp_reg[4]_i_1__0_n_2 ,\tmp_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[4]_i_1__0_n_4 ,\tmp_reg[4]_i_1__0_n_5 ,\tmp_reg[4]_i_1__0_n_6 ,\tmp_reg[4]_i_1__0_n_7 }),
        .S({\tmp[4]_i_2__0_n_0 ,\tmp[4]_i_3__0_n_0 ,\tmp[4]_i_4__0_n_0 ,\tmp[4]_i_5__0_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[5] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[4]_i_1__0_n_6 ),
        .Q(tmp_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[6] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[4]_i_1__0_n_5 ),
        .Q(tmp_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[7] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[4]_i_1__0_n_4 ),
        .Q(tmp_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[8] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[8]_i_1__0_n_7 ),
        .Q(tmp_reg[8]));
  CARRY4 \tmp_reg[8]_i_1__0 
       (.CI(\tmp_reg[4]_i_1__0_n_0 ),
        .CO({\tmp_reg[8]_i_1__0_n_0 ,\tmp_reg[8]_i_1__0_n_1 ,\tmp_reg[8]_i_1__0_n_2 ,\tmp_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[8]_i_1__0_n_4 ,\tmp_reg[8]_i_1__0_n_5 ,\tmp_reg[8]_i_1__0_n_6 ,\tmp_reg[8]_i_1__0_n_7 }),
        .S({\tmp[8]_i_2__0_n_0 ,\tmp[8]_i_3__0_n_0 ,\tmp[8]_i_4__0_n_0 ,\tmp[8]_i_5__0_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[9] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[8]_i_1__0_n_6 ),
        .Q(tmp_reg[9]));
endmodule

(* ORIG_REF_NAME = "clock_1KHz_enable" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clock_1KHz_enable_0
   (clk_1kHz_enb,
    eqOp,
    clk,
    \tmp_reg[0]_0 ,
    \tmp_reg[15]_0 );
  output clk_1kHz_enb;
  output eqOp;
  input clk;
  input \tmp_reg[0]_0 ;
  input \tmp_reg[15]_0 ;

  wire clk;
  wire clk_1kHz_en_i_i_3__0_n_0;
  wire clk_1kHz_en_i_i_4__0_n_0;
  wire clk_1kHz_en_i_i_5__0_n_0;
  wire clk_1kHz_enb;
  wire eqOp;
  wire \tmp[0]_i_3__0_n_0 ;
  wire \tmp[0]_i_4__1_n_0 ;
  wire \tmp[0]_i_5__1_n_0 ;
  wire \tmp[0]_i_6__1_n_0 ;
  wire \tmp[0]_i_7__1_n_0 ;
  wire \tmp[12]_i_2__1_n_0 ;
  wire \tmp[12]_i_3__1_n_0 ;
  wire \tmp[12]_i_4__1_n_0 ;
  wire \tmp[12]_i_5__1_n_0 ;
  wire \tmp[4]_i_2__1_n_0 ;
  wire \tmp[4]_i_3__1_n_0 ;
  wire \tmp[4]_i_4__1_n_0 ;
  wire \tmp[4]_i_5__1_n_0 ;
  wire \tmp[8]_i_2__1_n_0 ;
  wire \tmp[8]_i_3__1_n_0 ;
  wire \tmp[8]_i_4__1_n_0 ;
  wire \tmp[8]_i_5__1_n_0 ;
  wire [15:0]tmp_reg;
  wire \tmp_reg[0]_0 ;
  wire \tmp_reg[0]_i_2__1_n_0 ;
  wire \tmp_reg[0]_i_2__1_n_1 ;
  wire \tmp_reg[0]_i_2__1_n_2 ;
  wire \tmp_reg[0]_i_2__1_n_3 ;
  wire \tmp_reg[0]_i_2__1_n_4 ;
  wire \tmp_reg[0]_i_2__1_n_5 ;
  wire \tmp_reg[0]_i_2__1_n_6 ;
  wire \tmp_reg[0]_i_2__1_n_7 ;
  wire \tmp_reg[12]_i_1__1_n_1 ;
  wire \tmp_reg[12]_i_1__1_n_2 ;
  wire \tmp_reg[12]_i_1__1_n_3 ;
  wire \tmp_reg[12]_i_1__1_n_4 ;
  wire \tmp_reg[12]_i_1__1_n_5 ;
  wire \tmp_reg[12]_i_1__1_n_6 ;
  wire \tmp_reg[12]_i_1__1_n_7 ;
  wire \tmp_reg[15]_0 ;
  wire \tmp_reg[4]_i_1__1_n_0 ;
  wire \tmp_reg[4]_i_1__1_n_1 ;
  wire \tmp_reg[4]_i_1__1_n_2 ;
  wire \tmp_reg[4]_i_1__1_n_3 ;
  wire \tmp_reg[4]_i_1__1_n_4 ;
  wire \tmp_reg[4]_i_1__1_n_5 ;
  wire \tmp_reg[4]_i_1__1_n_6 ;
  wire \tmp_reg[4]_i_1__1_n_7 ;
  wire \tmp_reg[8]_i_1__1_n_0 ;
  wire \tmp_reg[8]_i_1__1_n_1 ;
  wire \tmp_reg[8]_i_1__1_n_2 ;
  wire \tmp_reg[8]_i_1__1_n_3 ;
  wire \tmp_reg[8]_i_1__1_n_4 ;
  wire \tmp_reg[8]_i_1__1_n_5 ;
  wire \tmp_reg[8]_i_1__1_n_6 ;
  wire \tmp_reg[8]_i_1__1_n_7 ;
  wire [3:3]\NLW_tmp_reg[12]_i_1__1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h8000000000000000)) 
    clk_1kHz_en_i_i_1__0
       (.I0(clk_1kHz_en_i_i_3__0_n_0),
        .I1(tmp_reg[1]),
        .I2(tmp_reg[0]),
        .I3(tmp_reg[3]),
        .I4(tmp_reg[2]),
        .I5(clk_1kHz_en_i_i_4__0_n_0),
        .O(eqOp));
  LUT4 #(
    .INIT(16'h0004)) 
    clk_1kHz_en_i_i_3__0
       (.I0(tmp_reg[7]),
        .I1(tmp_reg[6]),
        .I2(tmp_reg[5]),
        .I3(tmp_reg[4]),
        .O(clk_1kHz_en_i_i_3__0_n_0));
  LUT5 #(
    .INIT(32'h10000000)) 
    clk_1kHz_en_i_i_4__0
       (.I0(tmp_reg[12]),
        .I1(tmp_reg[13]),
        .I2(tmp_reg[14]),
        .I3(tmp_reg[15]),
        .I4(clk_1kHz_en_i_i_5__0_n_0),
        .O(clk_1kHz_en_i_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h1000)) 
    clk_1kHz_en_i_i_5__0
       (.I0(tmp_reg[11]),
        .I1(tmp_reg[10]),
        .I2(tmp_reg[9]),
        .I3(tmp_reg[8]),
        .O(clk_1kHz_en_i_i_5__0_n_0));
  FDCE clk_1kHz_en_i_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(\tmp_reg[0]_0 ),
        .D(eqOp),
        .Q(clk_1kHz_enb));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_3__0 
       (.I0(tmp_reg[0]),
        .I1(eqOp),
        .O(\tmp[0]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_4__1 
       (.I0(tmp_reg[3]),
        .I1(eqOp),
        .O(\tmp[0]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_5__1 
       (.I0(tmp_reg[2]),
        .I1(eqOp),
        .O(\tmp[0]_i_5__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_6__1 
       (.I0(tmp_reg[1]),
        .I1(eqOp),
        .O(\tmp[0]_i_6__1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \tmp[0]_i_7__1 
       (.I0(tmp_reg[0]),
        .I1(eqOp),
        .O(\tmp[0]_i_7__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_2__1 
       (.I0(tmp_reg[15]),
        .I1(eqOp),
        .O(\tmp[12]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_3__1 
       (.I0(tmp_reg[14]),
        .I1(eqOp),
        .O(\tmp[12]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_4__1 
       (.I0(tmp_reg[13]),
        .I1(eqOp),
        .O(\tmp[12]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_5__1 
       (.I0(tmp_reg[12]),
        .I1(eqOp),
        .O(\tmp[12]_i_5__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_2__1 
       (.I0(tmp_reg[7]),
        .I1(eqOp),
        .O(\tmp[4]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_3__1 
       (.I0(tmp_reg[6]),
        .I1(eqOp),
        .O(\tmp[4]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_4__1 
       (.I0(tmp_reg[5]),
        .I1(eqOp),
        .O(\tmp[4]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_5__1 
       (.I0(tmp_reg[4]),
        .I1(eqOp),
        .O(\tmp[4]_i_5__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_2__1 
       (.I0(tmp_reg[11]),
        .I1(eqOp),
        .O(\tmp[8]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_3__1 
       (.I0(tmp_reg[10]),
        .I1(eqOp),
        .O(\tmp[8]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_4__1 
       (.I0(tmp_reg[9]),
        .I1(eqOp),
        .O(\tmp[8]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_5__1 
       (.I0(tmp_reg[8]),
        .I1(eqOp),
        .O(\tmp[8]_i_5__1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[0] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[0]_i_2__1_n_7 ),
        .Q(tmp_reg[0]));
  CARRY4 \tmp_reg[0]_i_2__1 
       (.CI(1'b0),
        .CO({\tmp_reg[0]_i_2__1_n_0 ,\tmp_reg[0]_i_2__1_n_1 ,\tmp_reg[0]_i_2__1_n_2 ,\tmp_reg[0]_i_2__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\tmp[0]_i_3__0_n_0 }),
        .O({\tmp_reg[0]_i_2__1_n_4 ,\tmp_reg[0]_i_2__1_n_5 ,\tmp_reg[0]_i_2__1_n_6 ,\tmp_reg[0]_i_2__1_n_7 }),
        .S({\tmp[0]_i_4__1_n_0 ,\tmp[0]_i_5__1_n_0 ,\tmp[0]_i_6__1_n_0 ,\tmp[0]_i_7__1_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[10] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[8]_i_1__1_n_5 ),
        .Q(tmp_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[11] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[8]_i_1__1_n_4 ),
        .Q(tmp_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[12] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[12]_i_1__1_n_7 ),
        .Q(tmp_reg[12]));
  CARRY4 \tmp_reg[12]_i_1__1 
       (.CI(\tmp_reg[8]_i_1__1_n_0 ),
        .CO({\NLW_tmp_reg[12]_i_1__1_CO_UNCONNECTED [3],\tmp_reg[12]_i_1__1_n_1 ,\tmp_reg[12]_i_1__1_n_2 ,\tmp_reg[12]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[12]_i_1__1_n_4 ,\tmp_reg[12]_i_1__1_n_5 ,\tmp_reg[12]_i_1__1_n_6 ,\tmp_reg[12]_i_1__1_n_7 }),
        .S({\tmp[12]_i_2__1_n_0 ,\tmp[12]_i_3__1_n_0 ,\tmp[12]_i_4__1_n_0 ,\tmp[12]_i_5__1_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[13] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[12]_i_1__1_n_6 ),
        .Q(tmp_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[14] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[12]_i_1__1_n_5 ),
        .Q(tmp_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[15] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[12]_i_1__1_n_4 ),
        .Q(tmp_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[1] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[0]_i_2__1_n_6 ),
        .Q(tmp_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[2] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[0]_i_2__1_n_5 ),
        .Q(tmp_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[3] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[0]_i_2__1_n_4 ),
        .Q(tmp_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[4] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[4]_i_1__1_n_7 ),
        .Q(tmp_reg[4]));
  CARRY4 \tmp_reg[4]_i_1__1 
       (.CI(\tmp_reg[0]_i_2__1_n_0 ),
        .CO({\tmp_reg[4]_i_1__1_n_0 ,\tmp_reg[4]_i_1__1_n_1 ,\tmp_reg[4]_i_1__1_n_2 ,\tmp_reg[4]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[4]_i_1__1_n_4 ,\tmp_reg[4]_i_1__1_n_5 ,\tmp_reg[4]_i_1__1_n_6 ,\tmp_reg[4]_i_1__1_n_7 }),
        .S({\tmp[4]_i_2__1_n_0 ,\tmp[4]_i_3__1_n_0 ,\tmp[4]_i_4__1_n_0 ,\tmp[4]_i_5__1_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[5] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[4]_i_1__1_n_6 ),
        .Q(tmp_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[6] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[4]_i_1__1_n_5 ),
        .Q(tmp_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[7] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[4]_i_1__1_n_4 ),
        .Q(tmp_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[8] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[8]_i_1__1_n_7 ),
        .Q(tmp_reg[8]));
  CARRY4 \tmp_reg[8]_i_1__1 
       (.CI(\tmp_reg[4]_i_1__1_n_0 ),
        .CO({\tmp_reg[8]_i_1__1_n_0 ,\tmp_reg[8]_i_1__1_n_1 ,\tmp_reg[8]_i_1__1_n_2 ,\tmp_reg[8]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[8]_i_1__1_n_4 ,\tmp_reg[8]_i_1__1_n_5 ,\tmp_reg[8]_i_1__1_n_6 ,\tmp_reg[8]_i_1__1_n_7 }),
        .S({\tmp[8]_i_2__1_n_0 ,\tmp[8]_i_3__1_n_0 ,\tmp[8]_i_4__1_n_0 ,\tmp[8]_i_5__1_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[9] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(\tmp_reg[0]_0 ),
        .D(\tmp_reg[8]_i_1__1_n_6 ),
        .Q(tmp_reg[9]));
endmodule

(* ORIG_REF_NAME = "clock_1KHz_enable" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clock_1KHz_enable_1
   (clk_1kHz_enc,
    eqOp,
    clk,
    clr0_out,
    \tmp_reg[15]_0 );
  output clk_1kHz_enc;
  output eqOp;
  input clk;
  input clr0_out;
  input \tmp_reg[15]_0 ;

  wire clk;
  wire clk_1kHz_en_i_i_3__1_n_0;
  wire clk_1kHz_en_i_i_4__1_n_0;
  wire clk_1kHz_en_i_i_5__1_n_0;
  wire clk_1kHz_enc;
  wire clr0_out;
  wire eqOp;
  wire \tmp[0]_i_3__1_n_0 ;
  wire \tmp[0]_i_4__2_n_0 ;
  wire \tmp[0]_i_5__2_n_0 ;
  wire \tmp[0]_i_6__2_n_0 ;
  wire \tmp[0]_i_7__2_n_0 ;
  wire \tmp[12]_i_2__2_n_0 ;
  wire \tmp[12]_i_3__2_n_0 ;
  wire \tmp[12]_i_4__2_n_0 ;
  wire \tmp[12]_i_5__2_n_0 ;
  wire \tmp[4]_i_2__2_n_0 ;
  wire \tmp[4]_i_3__2_n_0 ;
  wire \tmp[4]_i_4__2_n_0 ;
  wire \tmp[4]_i_5__2_n_0 ;
  wire \tmp[8]_i_2__2_n_0 ;
  wire \tmp[8]_i_3__2_n_0 ;
  wire \tmp[8]_i_4__2_n_0 ;
  wire \tmp[8]_i_5__2_n_0 ;
  wire [15:0]tmp_reg;
  wire \tmp_reg[0]_i_2__2_n_0 ;
  wire \tmp_reg[0]_i_2__2_n_1 ;
  wire \tmp_reg[0]_i_2__2_n_2 ;
  wire \tmp_reg[0]_i_2__2_n_3 ;
  wire \tmp_reg[0]_i_2__2_n_4 ;
  wire \tmp_reg[0]_i_2__2_n_5 ;
  wire \tmp_reg[0]_i_2__2_n_6 ;
  wire \tmp_reg[0]_i_2__2_n_7 ;
  wire \tmp_reg[12]_i_1__2_n_1 ;
  wire \tmp_reg[12]_i_1__2_n_2 ;
  wire \tmp_reg[12]_i_1__2_n_3 ;
  wire \tmp_reg[12]_i_1__2_n_4 ;
  wire \tmp_reg[12]_i_1__2_n_5 ;
  wire \tmp_reg[12]_i_1__2_n_6 ;
  wire \tmp_reg[12]_i_1__2_n_7 ;
  wire \tmp_reg[15]_0 ;
  wire \tmp_reg[4]_i_1__2_n_0 ;
  wire \tmp_reg[4]_i_1__2_n_1 ;
  wire \tmp_reg[4]_i_1__2_n_2 ;
  wire \tmp_reg[4]_i_1__2_n_3 ;
  wire \tmp_reg[4]_i_1__2_n_4 ;
  wire \tmp_reg[4]_i_1__2_n_5 ;
  wire \tmp_reg[4]_i_1__2_n_6 ;
  wire \tmp_reg[4]_i_1__2_n_7 ;
  wire \tmp_reg[8]_i_1__2_n_0 ;
  wire \tmp_reg[8]_i_1__2_n_1 ;
  wire \tmp_reg[8]_i_1__2_n_2 ;
  wire \tmp_reg[8]_i_1__2_n_3 ;
  wire \tmp_reg[8]_i_1__2_n_4 ;
  wire \tmp_reg[8]_i_1__2_n_5 ;
  wire \tmp_reg[8]_i_1__2_n_6 ;
  wire \tmp_reg[8]_i_1__2_n_7 ;
  wire [3:3]\NLW_tmp_reg[12]_i_1__2_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h8000000000000000)) 
    clk_1kHz_en_i_i_1__1
       (.I0(clk_1kHz_en_i_i_3__1_n_0),
        .I1(tmp_reg[1]),
        .I2(tmp_reg[0]),
        .I3(tmp_reg[3]),
        .I4(tmp_reg[2]),
        .I5(clk_1kHz_en_i_i_4__1_n_0),
        .O(eqOp));
  LUT4 #(
    .INIT(16'h0004)) 
    clk_1kHz_en_i_i_3__1
       (.I0(tmp_reg[7]),
        .I1(tmp_reg[6]),
        .I2(tmp_reg[5]),
        .I3(tmp_reg[4]),
        .O(clk_1kHz_en_i_i_3__1_n_0));
  LUT5 #(
    .INIT(32'h10000000)) 
    clk_1kHz_en_i_i_4__1
       (.I0(tmp_reg[12]),
        .I1(tmp_reg[13]),
        .I2(tmp_reg[14]),
        .I3(tmp_reg[15]),
        .I4(clk_1kHz_en_i_i_5__1_n_0),
        .O(clk_1kHz_en_i_i_4__1_n_0));
  LUT4 #(
    .INIT(16'h1000)) 
    clk_1kHz_en_i_i_5__1
       (.I0(tmp_reg[11]),
        .I1(tmp_reg[10]),
        .I2(tmp_reg[9]),
        .I3(tmp_reg[8]),
        .O(clk_1kHz_en_i_i_5__1_n_0));
  FDCE clk_1kHz_en_i_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(clr0_out),
        .D(eqOp),
        .Q(clk_1kHz_enc));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_3__1 
       (.I0(tmp_reg[0]),
        .I1(eqOp),
        .O(\tmp[0]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_4__2 
       (.I0(tmp_reg[3]),
        .I1(eqOp),
        .O(\tmp[0]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_5__2 
       (.I0(tmp_reg[2]),
        .I1(eqOp),
        .O(\tmp[0]_i_5__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[0]_i_6__2 
       (.I0(tmp_reg[1]),
        .I1(eqOp),
        .O(\tmp[0]_i_6__2_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \tmp[0]_i_7__2 
       (.I0(tmp_reg[0]),
        .I1(eqOp),
        .O(\tmp[0]_i_7__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_2__2 
       (.I0(tmp_reg[15]),
        .I1(eqOp),
        .O(\tmp[12]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_3__2 
       (.I0(tmp_reg[14]),
        .I1(eqOp),
        .O(\tmp[12]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_4__2 
       (.I0(tmp_reg[13]),
        .I1(eqOp),
        .O(\tmp[12]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[12]_i_5__2 
       (.I0(tmp_reg[12]),
        .I1(eqOp),
        .O(\tmp[12]_i_5__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_2__2 
       (.I0(tmp_reg[7]),
        .I1(eqOp),
        .O(\tmp[4]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_3__2 
       (.I0(tmp_reg[6]),
        .I1(eqOp),
        .O(\tmp[4]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_4__2 
       (.I0(tmp_reg[5]),
        .I1(eqOp),
        .O(\tmp[4]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[4]_i_5__2 
       (.I0(tmp_reg[4]),
        .I1(eqOp),
        .O(\tmp[4]_i_5__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_2__2 
       (.I0(tmp_reg[11]),
        .I1(eqOp),
        .O(\tmp[8]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_3__2 
       (.I0(tmp_reg[10]),
        .I1(eqOp),
        .O(\tmp[8]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_4__2 
       (.I0(tmp_reg[9]),
        .I1(eqOp),
        .O(\tmp[8]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tmp[8]_i_5__2 
       (.I0(tmp_reg[8]),
        .I1(eqOp),
        .O(\tmp[8]_i_5__2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[0] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[0]_i_2__2_n_7 ),
        .Q(tmp_reg[0]));
  CARRY4 \tmp_reg[0]_i_2__2 
       (.CI(1'b0),
        .CO({\tmp_reg[0]_i_2__2_n_0 ,\tmp_reg[0]_i_2__2_n_1 ,\tmp_reg[0]_i_2__2_n_2 ,\tmp_reg[0]_i_2__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\tmp[0]_i_3__1_n_0 }),
        .O({\tmp_reg[0]_i_2__2_n_4 ,\tmp_reg[0]_i_2__2_n_5 ,\tmp_reg[0]_i_2__2_n_6 ,\tmp_reg[0]_i_2__2_n_7 }),
        .S({\tmp[0]_i_4__2_n_0 ,\tmp[0]_i_5__2_n_0 ,\tmp[0]_i_6__2_n_0 ,\tmp[0]_i_7__2_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[10] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[8]_i_1__2_n_5 ),
        .Q(tmp_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[11] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[8]_i_1__2_n_4 ),
        .Q(tmp_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[12] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[12]_i_1__2_n_7 ),
        .Q(tmp_reg[12]));
  CARRY4 \tmp_reg[12]_i_1__2 
       (.CI(\tmp_reg[8]_i_1__2_n_0 ),
        .CO({\NLW_tmp_reg[12]_i_1__2_CO_UNCONNECTED [3],\tmp_reg[12]_i_1__2_n_1 ,\tmp_reg[12]_i_1__2_n_2 ,\tmp_reg[12]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[12]_i_1__2_n_4 ,\tmp_reg[12]_i_1__2_n_5 ,\tmp_reg[12]_i_1__2_n_6 ,\tmp_reg[12]_i_1__2_n_7 }),
        .S({\tmp[12]_i_2__2_n_0 ,\tmp[12]_i_3__2_n_0 ,\tmp[12]_i_4__2_n_0 ,\tmp[12]_i_5__2_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[13] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[12]_i_1__2_n_6 ),
        .Q(tmp_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[14] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[12]_i_1__2_n_5 ),
        .Q(tmp_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[15] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[12]_i_1__2_n_4 ),
        .Q(tmp_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[1] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[0]_i_2__2_n_6 ),
        .Q(tmp_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[2] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[0]_i_2__2_n_5 ),
        .Q(tmp_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[3] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[0]_i_2__2_n_4 ),
        .Q(tmp_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[4] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[4]_i_1__2_n_7 ),
        .Q(tmp_reg[4]));
  CARRY4 \tmp_reg[4]_i_1__2 
       (.CI(\tmp_reg[0]_i_2__2_n_0 ),
        .CO({\tmp_reg[4]_i_1__2_n_0 ,\tmp_reg[4]_i_1__2_n_1 ,\tmp_reg[4]_i_1__2_n_2 ,\tmp_reg[4]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[4]_i_1__2_n_4 ,\tmp_reg[4]_i_1__2_n_5 ,\tmp_reg[4]_i_1__2_n_6 ,\tmp_reg[4]_i_1__2_n_7 }),
        .S({\tmp[4]_i_2__2_n_0 ,\tmp[4]_i_3__2_n_0 ,\tmp[4]_i_4__2_n_0 ,\tmp[4]_i_5__2_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[5] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[4]_i_1__2_n_6 ),
        .Q(tmp_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[6] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[4]_i_1__2_n_5 ),
        .Q(tmp_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[7] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[4]_i_1__2_n_4 ),
        .Q(tmp_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[8] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[8]_i_1__2_n_7 ),
        .Q(tmp_reg[8]));
  CARRY4 \tmp_reg[8]_i_1__2 
       (.CI(\tmp_reg[4]_i_1__2_n_0 ),
        .CO({\tmp_reg[8]_i_1__2_n_0 ,\tmp_reg[8]_i_1__2_n_1 ,\tmp_reg[8]_i_1__2_n_2 ,\tmp_reg[8]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[8]_i_1__2_n_4 ,\tmp_reg[8]_i_1__2_n_5 ,\tmp_reg[8]_i_1__2_n_6 ,\tmp_reg[8]_i_1__2_n_7 }),
        .S({\tmp[8]_i_2__2_n_0 ,\tmp[8]_i_3__2_n_0 ,\tmp[8]_i_4__2_n_0 ,\tmp[8]_i_5__2_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \tmp_reg[9] 
       (.C(clk),
        .CE(\tmp_reg[15]_0 ),
        .CLR(clr0_out),
        .D(\tmp_reg[8]_i_1__2_n_6 ),
        .Q(tmp_reg[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_comparator
   (CO,
    DI,
    S,
    cmp0_carry__1_0,
    cmp0_carry__1_1,
    \slv_reg7_reg[0] ,
    \slv_reg7_reg[0]_0 ,
    \slv_reg7_reg[0]_1 ,
    \slv_reg7_reg[0]_2 );
  output [0:0]CO;
  input [3:0]DI;
  input [3:0]S;
  input [3:0]cmp0_carry__1_0;
  input [3:0]cmp0_carry__1_1;
  input [3:0]\slv_reg7_reg[0] ;
  input [3:0]\slv_reg7_reg[0]_0 ;
  input [3:0]\slv_reg7_reg[0]_1 ;
  input [3:0]\slv_reg7_reg[0]_2 ;

  wire [0:0]CO;
  wire [3:0]DI;
  wire [3:0]S;
  wire cmp0_carry__0_n_0;
  wire cmp0_carry__0_n_1;
  wire cmp0_carry__0_n_2;
  wire cmp0_carry__0_n_3;
  wire [3:0]cmp0_carry__1_0;
  wire [3:0]cmp0_carry__1_1;
  wire cmp0_carry__1_n_0;
  wire cmp0_carry__1_n_1;
  wire cmp0_carry__1_n_2;
  wire cmp0_carry__1_n_3;
  wire cmp0_carry__2_n_1;
  wire cmp0_carry__2_n_2;
  wire cmp0_carry__2_n_3;
  wire cmp0_carry_n_0;
  wire cmp0_carry_n_1;
  wire cmp0_carry_n_2;
  wire cmp0_carry_n_3;
  wire [3:0]\slv_reg7_reg[0] ;
  wire [3:0]\slv_reg7_reg[0]_0 ;
  wire [3:0]\slv_reg7_reg[0]_1 ;
  wire [3:0]\slv_reg7_reg[0]_2 ;
  wire [3:0]NLW_cmp0_carry_O_UNCONNECTED;
  wire [3:0]NLW_cmp0_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_cmp0_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_cmp0_carry__2_O_UNCONNECTED;

  CARRY4 cmp0_carry
       (.CI(1'b0),
        .CO({cmp0_carry_n_0,cmp0_carry_n_1,cmp0_carry_n_2,cmp0_carry_n_3}),
        .CYINIT(1'b1),
        .DI(DI),
        .O(NLW_cmp0_carry_O_UNCONNECTED[3:0]),
        .S(S));
  CARRY4 cmp0_carry__0
       (.CI(cmp0_carry_n_0),
        .CO({cmp0_carry__0_n_0,cmp0_carry__0_n_1,cmp0_carry__0_n_2,cmp0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(cmp0_carry__1_0),
        .O(NLW_cmp0_carry__0_O_UNCONNECTED[3:0]),
        .S(cmp0_carry__1_1));
  CARRY4 cmp0_carry__1
       (.CI(cmp0_carry__0_n_0),
        .CO({cmp0_carry__1_n_0,cmp0_carry__1_n_1,cmp0_carry__1_n_2,cmp0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(\slv_reg7_reg[0] ),
        .O(NLW_cmp0_carry__1_O_UNCONNECTED[3:0]),
        .S(\slv_reg7_reg[0]_0 ));
  CARRY4 cmp0_carry__2
       (.CI(cmp0_carry__1_n_0),
        .CO({CO,cmp0_carry__2_n_1,cmp0_carry__2_n_2,cmp0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(\slv_reg7_reg[0]_1 ),
        .O(NLW_cmp0_carry__2_O_UNCONNECTED[3:0]),
        .S(\slv_reg7_reg[0]_2 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter
   (S,
    \axi_araddr_reg[4] ,
    \tmp_reg[31]_0 ,
    clk,
    clr1_out,
    \axi_rdata_reg[0] ,
    sel0,
    \axi_rdata_reg[0]_0 ,
    \axi_rdata_reg[0]_1 ,
    D,
    \axi_rdata_reg[3] ,
    \axi_rdata_reg[3]_0 ,
    \axi_rdata_reg[4] ,
    \axi_rdata_reg[7] ,
    \axi_rdata_reg[5] ,
    \axi_rdata_reg[6] ,
    \axi_rdata_reg[7]_0 ,
    \axi_rdata_reg[8] ,
    \axi_rdata_reg[11] ,
    \axi_rdata_reg[9] ,
    \axi_rdata_reg[10] ,
    \axi_rdata_reg[11]_0 ,
    \axi_rdata_reg[12] ,
    \axi_rdata_reg[15] ,
    \axi_rdata_reg[13] ,
    \axi_rdata_reg[14] ,
    \axi_rdata_reg[15]_0 ,
    \axi_rdata_reg[16] ,
    \axi_rdata_reg[19] ,
    \axi_rdata_reg[17] ,
    \axi_rdata_reg[18] ,
    \axi_rdata_reg[19]_0 ,
    \axi_rdata_reg[20] ,
    \axi_rdata_reg[23] ,
    \axi_rdata_reg[21] ,
    \axi_rdata_reg[22] ,
    \axi_rdata_reg[23]_0 ,
    \axi_rdata_reg[24] ,
    \axi_rdata_reg[27] ,
    \axi_rdata_reg[25] ,
    \axi_rdata_reg[26] ,
    \axi_rdata_reg[27]_0 ,
    \axi_rdata_reg[28] ,
    \axi_rdata_reg[31] ,
    \axi_rdata_reg[29] ,
    \axi_rdata_reg[30] ,
    \axi_rdata_reg[31]_0 );
  output [1:0]S;
  output [29:0]\axi_araddr_reg[4] ;
  input \tmp_reg[31]_0 ;
  input clk;
  input clr1_out;
  input \axi_rdata_reg[0] ;
  input [2:0]sel0;
  input \axi_rdata_reg[0]_0 ;
  input \axi_rdata_reg[0]_1 ;
  input [29:0]D;
  input \axi_rdata_reg[3] ;
  input [0:0]\axi_rdata_reg[3]_0 ;
  input \axi_rdata_reg[4] ;
  input [3:0]\axi_rdata_reg[7] ;
  input \axi_rdata_reg[5] ;
  input \axi_rdata_reg[6] ;
  input \axi_rdata_reg[7]_0 ;
  input \axi_rdata_reg[8] ;
  input [3:0]\axi_rdata_reg[11] ;
  input \axi_rdata_reg[9] ;
  input \axi_rdata_reg[10] ;
  input \axi_rdata_reg[11]_0 ;
  input \axi_rdata_reg[12] ;
  input [3:0]\axi_rdata_reg[15] ;
  input \axi_rdata_reg[13] ;
  input \axi_rdata_reg[14] ;
  input \axi_rdata_reg[15]_0 ;
  input \axi_rdata_reg[16] ;
  input [3:0]\axi_rdata_reg[19] ;
  input \axi_rdata_reg[17] ;
  input \axi_rdata_reg[18] ;
  input \axi_rdata_reg[19]_0 ;
  input \axi_rdata_reg[20] ;
  input [3:0]\axi_rdata_reg[23] ;
  input \axi_rdata_reg[21] ;
  input \axi_rdata_reg[22] ;
  input \axi_rdata_reg[23]_0 ;
  input \axi_rdata_reg[24] ;
  input [3:0]\axi_rdata_reg[27] ;
  input \axi_rdata_reg[25] ;
  input \axi_rdata_reg[26] ;
  input \axi_rdata_reg[27]_0 ;
  input \axi_rdata_reg[28] ;
  input [3:0]\axi_rdata_reg[31] ;
  input \axi_rdata_reg[29] ;
  input \axi_rdata_reg[30] ;
  input \axi_rdata_reg[31]_0 ;

  wire [29:0]D;
  wire [1:0]S;
  wire [29:0]\axi_araddr_reg[4] ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire \axi_rdata_reg[0]_1 ;
  wire \axi_rdata_reg[10] ;
  wire [3:0]\axi_rdata_reg[11] ;
  wire \axi_rdata_reg[11]_0 ;
  wire \axi_rdata_reg[12] ;
  wire \axi_rdata_reg[13] ;
  wire \axi_rdata_reg[14] ;
  wire [3:0]\axi_rdata_reg[15] ;
  wire \axi_rdata_reg[15]_0 ;
  wire \axi_rdata_reg[16] ;
  wire \axi_rdata_reg[17] ;
  wire \axi_rdata_reg[18] ;
  wire [3:0]\axi_rdata_reg[19] ;
  wire \axi_rdata_reg[19]_0 ;
  wire \axi_rdata_reg[20] ;
  wire \axi_rdata_reg[21] ;
  wire \axi_rdata_reg[22] ;
  wire [3:0]\axi_rdata_reg[23] ;
  wire \axi_rdata_reg[23]_0 ;
  wire \axi_rdata_reg[24] ;
  wire \axi_rdata_reg[25] ;
  wire \axi_rdata_reg[26] ;
  wire [3:0]\axi_rdata_reg[27] ;
  wire \axi_rdata_reg[27]_0 ;
  wire \axi_rdata_reg[28] ;
  wire \axi_rdata_reg[29] ;
  wire \axi_rdata_reg[30] ;
  wire [3:0]\axi_rdata_reg[31] ;
  wire \axi_rdata_reg[31]_0 ;
  wire \axi_rdata_reg[3] ;
  wire [0:0]\axi_rdata_reg[3]_0 ;
  wire \axi_rdata_reg[4] ;
  wire \axi_rdata_reg[5] ;
  wire \axi_rdata_reg[6] ;
  wire [3:0]\axi_rdata_reg[7] ;
  wire \axi_rdata_reg[7]_0 ;
  wire \axi_rdata_reg[8] ;
  wire \axi_rdata_reg[9] ;
  wire clk;
  wire clr1_out;
  wire [2:0]sel0;
  wire \tmp[0]_i_3__4_n_0 ;
  wire [31:0]tmp_reg;
  wire \tmp_reg[0]_i_2__4_n_0 ;
  wire \tmp_reg[0]_i_2__4_n_1 ;
  wire \tmp_reg[0]_i_2__4_n_2 ;
  wire \tmp_reg[0]_i_2__4_n_3 ;
  wire \tmp_reg[0]_i_2__4_n_4 ;
  wire \tmp_reg[0]_i_2__4_n_5 ;
  wire \tmp_reg[0]_i_2__4_n_6 ;
  wire \tmp_reg[0]_i_2__4_n_7 ;
  wire \tmp_reg[12]_i_1__4_n_0 ;
  wire \tmp_reg[12]_i_1__4_n_1 ;
  wire \tmp_reg[12]_i_1__4_n_2 ;
  wire \tmp_reg[12]_i_1__4_n_3 ;
  wire \tmp_reg[12]_i_1__4_n_4 ;
  wire \tmp_reg[12]_i_1__4_n_5 ;
  wire \tmp_reg[12]_i_1__4_n_6 ;
  wire \tmp_reg[12]_i_1__4_n_7 ;
  wire \tmp_reg[16]_i_1__1_n_0 ;
  wire \tmp_reg[16]_i_1__1_n_1 ;
  wire \tmp_reg[16]_i_1__1_n_2 ;
  wire \tmp_reg[16]_i_1__1_n_3 ;
  wire \tmp_reg[16]_i_1__1_n_4 ;
  wire \tmp_reg[16]_i_1__1_n_5 ;
  wire \tmp_reg[16]_i_1__1_n_6 ;
  wire \tmp_reg[16]_i_1__1_n_7 ;
  wire \tmp_reg[20]_i_1__0_n_0 ;
  wire \tmp_reg[20]_i_1__0_n_1 ;
  wire \tmp_reg[20]_i_1__0_n_2 ;
  wire \tmp_reg[20]_i_1__0_n_3 ;
  wire \tmp_reg[20]_i_1__0_n_4 ;
  wire \tmp_reg[20]_i_1__0_n_5 ;
  wire \tmp_reg[20]_i_1__0_n_6 ;
  wire \tmp_reg[20]_i_1__0_n_7 ;
  wire \tmp_reg[24]_i_1__0_n_0 ;
  wire \tmp_reg[24]_i_1__0_n_1 ;
  wire \tmp_reg[24]_i_1__0_n_2 ;
  wire \tmp_reg[24]_i_1__0_n_3 ;
  wire \tmp_reg[24]_i_1__0_n_4 ;
  wire \tmp_reg[24]_i_1__0_n_5 ;
  wire \tmp_reg[24]_i_1__0_n_6 ;
  wire \tmp_reg[24]_i_1__0_n_7 ;
  wire \tmp_reg[28]_i_1__0_n_1 ;
  wire \tmp_reg[28]_i_1__0_n_2 ;
  wire \tmp_reg[28]_i_1__0_n_3 ;
  wire \tmp_reg[28]_i_1__0_n_4 ;
  wire \tmp_reg[28]_i_1__0_n_5 ;
  wire \tmp_reg[28]_i_1__0_n_6 ;
  wire \tmp_reg[28]_i_1__0_n_7 ;
  wire \tmp_reg[31]_0 ;
  wire \tmp_reg[4]_i_1__4_n_0 ;
  wire \tmp_reg[4]_i_1__4_n_1 ;
  wire \tmp_reg[4]_i_1__4_n_2 ;
  wire \tmp_reg[4]_i_1__4_n_3 ;
  wire \tmp_reg[4]_i_1__4_n_4 ;
  wire \tmp_reg[4]_i_1__4_n_5 ;
  wire \tmp_reg[4]_i_1__4_n_6 ;
  wire \tmp_reg[4]_i_1__4_n_7 ;
  wire \tmp_reg[8]_i_1__4_n_0 ;
  wire \tmp_reg[8]_i_1__4_n_1 ;
  wire \tmp_reg[8]_i_1__4_n_2 ;
  wire \tmp_reg[8]_i_1__4_n_3 ;
  wire \tmp_reg[8]_i_1__4_n_4 ;
  wire \tmp_reg[8]_i_1__4_n_5 ;
  wire \tmp_reg[8]_i_1__4_n_6 ;
  wire \tmp_reg[8]_i_1__4_n_7 ;
  wire [3:3]\NLW_tmp_reg[28]_i_1__0_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hBBB8B8B8B8B8B8B8)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata_reg[0] ),
        .I1(sel0[2]),
        .I2(\axi_rdata[0]_i_3_n_0 ),
        .I3(\axi_rdata_reg[0]_0 ),
        .I4(sel0[1]),
        .I5(sel0[0]),
        .O(\axi_araddr_reg[4] [0]));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[0]_i_3 
       (.I0(tmp_reg[0]),
        .I1(\axi_rdata_reg[0]_1 ),
        .I2(D[0]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[10]_i_2 
       (.I0(tmp_reg[10]),
        .I1(\axi_rdata_reg[11] [2]),
        .I2(D[8]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[11]_i_2 
       (.I0(tmp_reg[11]),
        .I1(\axi_rdata_reg[11] [3]),
        .I2(D[9]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[12]_i_2 
       (.I0(tmp_reg[12]),
        .I1(\axi_rdata_reg[15] [0]),
        .I2(D[10]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[13]_i_2 
       (.I0(tmp_reg[13]),
        .I1(\axi_rdata_reg[15] [1]),
        .I2(D[11]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[14]_i_2 
       (.I0(tmp_reg[14]),
        .I1(\axi_rdata_reg[15] [2]),
        .I2(D[12]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[15]_i_2 
       (.I0(tmp_reg[15]),
        .I1(\axi_rdata_reg[15] [3]),
        .I2(D[13]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[16]_i_2 
       (.I0(tmp_reg[16]),
        .I1(\axi_rdata_reg[19] [0]),
        .I2(D[14]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[17]_i_2 
       (.I0(tmp_reg[17]),
        .I1(\axi_rdata_reg[19] [1]),
        .I2(D[15]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[18]_i_2 
       (.I0(tmp_reg[18]),
        .I1(\axi_rdata_reg[19] [2]),
        .I2(D[16]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[19]_i_2 
       (.I0(tmp_reg[19]),
        .I1(\axi_rdata_reg[19] [3]),
        .I2(D[17]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[20]_i_2 
       (.I0(tmp_reg[20]),
        .I1(\axi_rdata_reg[23] [0]),
        .I2(D[18]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[21]_i_2 
       (.I0(tmp_reg[21]),
        .I1(\axi_rdata_reg[23] [1]),
        .I2(D[19]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[22]_i_2 
       (.I0(tmp_reg[22]),
        .I1(\axi_rdata_reg[23] [2]),
        .I2(D[20]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[23]_i_2 
       (.I0(tmp_reg[23]),
        .I1(\axi_rdata_reg[23] [3]),
        .I2(D[21]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[24]_i_2 
       (.I0(tmp_reg[24]),
        .I1(\axi_rdata_reg[27] [0]),
        .I2(D[22]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[25]_i_2 
       (.I0(tmp_reg[25]),
        .I1(\axi_rdata_reg[27] [1]),
        .I2(D[23]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[26]_i_2 
       (.I0(tmp_reg[26]),
        .I1(\axi_rdata_reg[27] [2]),
        .I2(D[24]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[27]_i_2 
       (.I0(tmp_reg[27]),
        .I1(\axi_rdata_reg[27] [3]),
        .I2(D[25]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[28]_i_2 
       (.I0(tmp_reg[28]),
        .I1(\axi_rdata_reg[31] [0]),
        .I2(D[26]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[29]_i_2 
       (.I0(tmp_reg[29]),
        .I1(\axi_rdata_reg[31] [1]),
        .I2(D[27]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[30]_i_2 
       (.I0(tmp_reg[30]),
        .I1(\axi_rdata_reg[31] [2]),
        .I2(D[28]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[31]_i_3 
       (.I0(tmp_reg[31]),
        .I1(\axi_rdata_reg[31] [3]),
        .I2(D[29]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[3]_i_2 
       (.I0(tmp_reg[3]),
        .I1(\axi_rdata_reg[3]_0 ),
        .I2(D[1]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[4]_i_2 
       (.I0(tmp_reg[4]),
        .I1(\axi_rdata_reg[7] [0]),
        .I2(D[2]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[5]_i_2 
       (.I0(tmp_reg[5]),
        .I1(\axi_rdata_reg[7] [1]),
        .I2(D[3]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[6]_i_2 
       (.I0(tmp_reg[6]),
        .I1(\axi_rdata_reg[7] [2]),
        .I2(D[4]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[7]_i_2 
       (.I0(tmp_reg[7]),
        .I1(\axi_rdata_reg[7] [3]),
        .I2(D[5]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[8]_i_2 
       (.I0(tmp_reg[8]),
        .I1(\axi_rdata_reg[11] [0]),
        .I2(D[6]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[9]_i_2 
       (.I0(tmp_reg[9]),
        .I1(\axi_rdata_reg[11] [1]),
        .I2(D[7]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  MUXF7 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(\axi_rdata_reg[10] ),
        .O(\axi_araddr_reg[4] [8]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(\axi_rdata_reg[11]_0 ),
        .O(\axi_araddr_reg[4] [9]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(\axi_rdata_reg[12] ),
        .O(\axi_araddr_reg[4] [10]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(\axi_rdata_reg[13] ),
        .O(\axi_araddr_reg[4] [11]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(\axi_rdata_reg[14] ),
        .O(\axi_araddr_reg[4] [12]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata_reg[15]_0 ),
        .O(\axi_araddr_reg[4] [13]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16] ),
        .O(\axi_araddr_reg[4] [14]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17] ),
        .O(\axi_araddr_reg[4] [15]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18] ),
        .O(\axi_araddr_reg[4] [16]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19]_0 ),
        .O(\axi_araddr_reg[4] [17]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20] ),
        .O(\axi_araddr_reg[4] [18]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21] ),
        .O(\axi_araddr_reg[4] [19]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22] ),
        .O(\axi_araddr_reg[4] [20]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23]_0 ),
        .O(\axi_araddr_reg[4] [21]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24] ),
        .O(\axi_araddr_reg[4] [22]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25] ),
        .O(\axi_araddr_reg[4] [23]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26] ),
        .O(\axi_araddr_reg[4] [24]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27]_0 ),
        .O(\axi_araddr_reg[4] [25]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28] ),
        .O(\axi_araddr_reg[4] [26]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29] ),
        .O(\axi_araddr_reg[4] [27]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30] ),
        .O(\axi_araddr_reg[4] [28]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_2 
       (.I0(\axi_rdata[31]_i_3_n_0 ),
        .I1(\axi_rdata_reg[31]_0 ),
        .O(\axi_araddr_reg[4] [29]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(\axi_rdata_reg[3] ),
        .O(\axi_araddr_reg[4] [1]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\axi_rdata_reg[4] ),
        .O(\axi_araddr_reg[4] [2]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(\axi_rdata_reg[5] ),
        .O(\axi_araddr_reg[4] [3]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(\axi_rdata_reg[6] ),
        .O(\axi_araddr_reg[4] [4]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(\axi_rdata_reg[7]_0 ),
        .O(\axi_araddr_reg[4] [5]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(\axi_rdata_reg[8] ),
        .O(\axi_araddr_reg[4] [6]),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(\axi_rdata_reg[9] ),
        .O(\axi_araddr_reg[4] [7]),
        .S(sel0[2]));
  LUT1 #(
    .INIT(2'h1)) 
    \tmp[0]_i_3__4 
       (.I0(tmp_reg[0]),
        .O(\tmp[0]_i_3__4_n_0 ));
  FDCE \tmp_reg[0] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[0]_i_2__4_n_7 ),
        .Q(tmp_reg[0]));
  CARRY4 \tmp_reg[0]_i_2__4 
       (.CI(1'b0),
        .CO({\tmp_reg[0]_i_2__4_n_0 ,\tmp_reg[0]_i_2__4_n_1 ,\tmp_reg[0]_i_2__4_n_2 ,\tmp_reg[0]_i_2__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\tmp_reg[0]_i_2__4_n_4 ,\tmp_reg[0]_i_2__4_n_5 ,\tmp_reg[0]_i_2__4_n_6 ,\tmp_reg[0]_i_2__4_n_7 }),
        .S({tmp_reg[3],S,\tmp[0]_i_3__4_n_0 }));
  FDCE \tmp_reg[10] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[8]_i_1__4_n_5 ),
        .Q(tmp_reg[10]));
  FDCE \tmp_reg[11] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[8]_i_1__4_n_4 ),
        .Q(tmp_reg[11]));
  FDCE \tmp_reg[12] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[12]_i_1__4_n_7 ),
        .Q(tmp_reg[12]));
  CARRY4 \tmp_reg[12]_i_1__4 
       (.CI(\tmp_reg[8]_i_1__4_n_0 ),
        .CO({\tmp_reg[12]_i_1__4_n_0 ,\tmp_reg[12]_i_1__4_n_1 ,\tmp_reg[12]_i_1__4_n_2 ,\tmp_reg[12]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[12]_i_1__4_n_4 ,\tmp_reg[12]_i_1__4_n_5 ,\tmp_reg[12]_i_1__4_n_6 ,\tmp_reg[12]_i_1__4_n_7 }),
        .S(tmp_reg[15:12]));
  FDCE \tmp_reg[13] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[12]_i_1__4_n_6 ),
        .Q(tmp_reg[13]));
  FDCE \tmp_reg[14] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[12]_i_1__4_n_5 ),
        .Q(tmp_reg[14]));
  FDCE \tmp_reg[15] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[12]_i_1__4_n_4 ),
        .Q(tmp_reg[15]));
  FDCE \tmp_reg[16] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[16]_i_1__1_n_7 ),
        .Q(tmp_reg[16]));
  CARRY4 \tmp_reg[16]_i_1__1 
       (.CI(\tmp_reg[12]_i_1__4_n_0 ),
        .CO({\tmp_reg[16]_i_1__1_n_0 ,\tmp_reg[16]_i_1__1_n_1 ,\tmp_reg[16]_i_1__1_n_2 ,\tmp_reg[16]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[16]_i_1__1_n_4 ,\tmp_reg[16]_i_1__1_n_5 ,\tmp_reg[16]_i_1__1_n_6 ,\tmp_reg[16]_i_1__1_n_7 }),
        .S(tmp_reg[19:16]));
  FDCE \tmp_reg[17] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[16]_i_1__1_n_6 ),
        .Q(tmp_reg[17]));
  FDCE \tmp_reg[18] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[16]_i_1__1_n_5 ),
        .Q(tmp_reg[18]));
  FDCE \tmp_reg[19] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[16]_i_1__1_n_4 ),
        .Q(tmp_reg[19]));
  FDCE \tmp_reg[1] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[0]_i_2__4_n_6 ),
        .Q(S[0]));
  FDCE \tmp_reg[20] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[20]_i_1__0_n_7 ),
        .Q(tmp_reg[20]));
  CARRY4 \tmp_reg[20]_i_1__0 
       (.CI(\tmp_reg[16]_i_1__1_n_0 ),
        .CO({\tmp_reg[20]_i_1__0_n_0 ,\tmp_reg[20]_i_1__0_n_1 ,\tmp_reg[20]_i_1__0_n_2 ,\tmp_reg[20]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[20]_i_1__0_n_4 ,\tmp_reg[20]_i_1__0_n_5 ,\tmp_reg[20]_i_1__0_n_6 ,\tmp_reg[20]_i_1__0_n_7 }),
        .S(tmp_reg[23:20]));
  FDCE \tmp_reg[21] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[20]_i_1__0_n_6 ),
        .Q(tmp_reg[21]));
  FDCE \tmp_reg[22] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[20]_i_1__0_n_5 ),
        .Q(tmp_reg[22]));
  FDCE \tmp_reg[23] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[20]_i_1__0_n_4 ),
        .Q(tmp_reg[23]));
  FDCE \tmp_reg[24] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[24]_i_1__0_n_7 ),
        .Q(tmp_reg[24]));
  CARRY4 \tmp_reg[24]_i_1__0 
       (.CI(\tmp_reg[20]_i_1__0_n_0 ),
        .CO({\tmp_reg[24]_i_1__0_n_0 ,\tmp_reg[24]_i_1__0_n_1 ,\tmp_reg[24]_i_1__0_n_2 ,\tmp_reg[24]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[24]_i_1__0_n_4 ,\tmp_reg[24]_i_1__0_n_5 ,\tmp_reg[24]_i_1__0_n_6 ,\tmp_reg[24]_i_1__0_n_7 }),
        .S(tmp_reg[27:24]));
  FDCE \tmp_reg[25] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[24]_i_1__0_n_6 ),
        .Q(tmp_reg[25]));
  FDCE \tmp_reg[26] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[24]_i_1__0_n_5 ),
        .Q(tmp_reg[26]));
  FDCE \tmp_reg[27] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[24]_i_1__0_n_4 ),
        .Q(tmp_reg[27]));
  FDCE \tmp_reg[28] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[28]_i_1__0_n_7 ),
        .Q(tmp_reg[28]));
  CARRY4 \tmp_reg[28]_i_1__0 
       (.CI(\tmp_reg[24]_i_1__0_n_0 ),
        .CO({\NLW_tmp_reg[28]_i_1__0_CO_UNCONNECTED [3],\tmp_reg[28]_i_1__0_n_1 ,\tmp_reg[28]_i_1__0_n_2 ,\tmp_reg[28]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[28]_i_1__0_n_4 ,\tmp_reg[28]_i_1__0_n_5 ,\tmp_reg[28]_i_1__0_n_6 ,\tmp_reg[28]_i_1__0_n_7 }),
        .S(tmp_reg[31:28]));
  FDCE \tmp_reg[29] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[28]_i_1__0_n_6 ),
        .Q(tmp_reg[29]));
  FDCE \tmp_reg[2] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[0]_i_2__4_n_5 ),
        .Q(S[1]));
  FDCE \tmp_reg[30] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[28]_i_1__0_n_5 ),
        .Q(tmp_reg[30]));
  FDCE \tmp_reg[31] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[28]_i_1__0_n_4 ),
        .Q(tmp_reg[31]));
  FDCE \tmp_reg[3] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[0]_i_2__4_n_4 ),
        .Q(tmp_reg[3]));
  FDCE \tmp_reg[4] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[4]_i_1__4_n_7 ),
        .Q(tmp_reg[4]));
  CARRY4 \tmp_reg[4]_i_1__4 
       (.CI(\tmp_reg[0]_i_2__4_n_0 ),
        .CO({\tmp_reg[4]_i_1__4_n_0 ,\tmp_reg[4]_i_1__4_n_1 ,\tmp_reg[4]_i_1__4_n_2 ,\tmp_reg[4]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[4]_i_1__4_n_4 ,\tmp_reg[4]_i_1__4_n_5 ,\tmp_reg[4]_i_1__4_n_6 ,\tmp_reg[4]_i_1__4_n_7 }),
        .S(tmp_reg[7:4]));
  FDCE \tmp_reg[5] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[4]_i_1__4_n_6 ),
        .Q(tmp_reg[5]));
  FDCE \tmp_reg[6] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[4]_i_1__4_n_5 ),
        .Q(tmp_reg[6]));
  FDCE \tmp_reg[7] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[4]_i_1__4_n_4 ),
        .Q(tmp_reg[7]));
  FDCE \tmp_reg[8] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[8]_i_1__4_n_7 ),
        .Q(tmp_reg[8]));
  CARRY4 \tmp_reg[8]_i_1__4 
       (.CI(\tmp_reg[4]_i_1__4_n_0 ),
        .CO({\tmp_reg[8]_i_1__4_n_0 ,\tmp_reg[8]_i_1__4_n_1 ,\tmp_reg[8]_i_1__4_n_2 ,\tmp_reg[8]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[8]_i_1__4_n_4 ,\tmp_reg[8]_i_1__4_n_5 ,\tmp_reg[8]_i_1__4_n_6 ,\tmp_reg[8]_i_1__4_n_7 }),
        .S(tmp_reg[11:8]));
  FDCE \tmp_reg[9] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(clr1_out),
        .D(\tmp_reg[8]_i_1__4_n_6 ),
        .Q(tmp_reg[9]));
endmodule

(* ORIG_REF_NAME = "counter" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter_2
   (D,
    \axi_araddr_reg[4] ,
    \tmp_reg[31]_0 ,
    clk,
    \tmp_reg[31]_1 ,
    sel0,
    \axi_rdata_reg[2] ,
    bram_seg,
    S,
    \axi_rdata_reg[2]_0 );
  output [31:0]D;
  output [0:0]\axi_araddr_reg[4] ;
  input \tmp_reg[31]_0 ;
  input clk;
  input \tmp_reg[31]_1 ;
  input [2:0]sel0;
  input \axi_rdata_reg[2] ;
  input bram_seg;
  input [0:0]S;
  input [0:0]\axi_rdata_reg[2]_0 ;

  wire [31:0]D;
  wire [0:0]S;
  wire [0:0]\axi_araddr_reg[4] ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2] ;
  wire [0:0]\axi_rdata_reg[2]_0 ;
  wire bram_seg;
  wire clk;
  wire [2:0]sel0;
  wire \tmp[0]_i_3__5_n_0 ;
  wire \tmp_reg[0]_i_2__5_n_0 ;
  wire \tmp_reg[0]_i_2__5_n_1 ;
  wire \tmp_reg[0]_i_2__5_n_2 ;
  wire \tmp_reg[0]_i_2__5_n_3 ;
  wire \tmp_reg[0]_i_2__5_n_4 ;
  wire \tmp_reg[0]_i_2__5_n_5 ;
  wire \tmp_reg[0]_i_2__5_n_6 ;
  wire \tmp_reg[0]_i_2__5_n_7 ;
  wire \tmp_reg[12]_i_1__5_n_0 ;
  wire \tmp_reg[12]_i_1__5_n_1 ;
  wire \tmp_reg[12]_i_1__5_n_2 ;
  wire \tmp_reg[12]_i_1__5_n_3 ;
  wire \tmp_reg[12]_i_1__5_n_4 ;
  wire \tmp_reg[12]_i_1__5_n_5 ;
  wire \tmp_reg[12]_i_1__5_n_6 ;
  wire \tmp_reg[12]_i_1__5_n_7 ;
  wire \tmp_reg[16]_i_1__2_n_0 ;
  wire \tmp_reg[16]_i_1__2_n_1 ;
  wire \tmp_reg[16]_i_1__2_n_2 ;
  wire \tmp_reg[16]_i_1__2_n_3 ;
  wire \tmp_reg[16]_i_1__2_n_4 ;
  wire \tmp_reg[16]_i_1__2_n_5 ;
  wire \tmp_reg[16]_i_1__2_n_6 ;
  wire \tmp_reg[16]_i_1__2_n_7 ;
  wire \tmp_reg[20]_i_1__1_n_0 ;
  wire \tmp_reg[20]_i_1__1_n_1 ;
  wire \tmp_reg[20]_i_1__1_n_2 ;
  wire \tmp_reg[20]_i_1__1_n_3 ;
  wire \tmp_reg[20]_i_1__1_n_4 ;
  wire \tmp_reg[20]_i_1__1_n_5 ;
  wire \tmp_reg[20]_i_1__1_n_6 ;
  wire \tmp_reg[20]_i_1__1_n_7 ;
  wire \tmp_reg[24]_i_1__1_n_0 ;
  wire \tmp_reg[24]_i_1__1_n_1 ;
  wire \tmp_reg[24]_i_1__1_n_2 ;
  wire \tmp_reg[24]_i_1__1_n_3 ;
  wire \tmp_reg[24]_i_1__1_n_4 ;
  wire \tmp_reg[24]_i_1__1_n_5 ;
  wire \tmp_reg[24]_i_1__1_n_6 ;
  wire \tmp_reg[24]_i_1__1_n_7 ;
  wire \tmp_reg[28]_i_1__1_n_1 ;
  wire \tmp_reg[28]_i_1__1_n_2 ;
  wire \tmp_reg[28]_i_1__1_n_3 ;
  wire \tmp_reg[28]_i_1__1_n_4 ;
  wire \tmp_reg[28]_i_1__1_n_5 ;
  wire \tmp_reg[28]_i_1__1_n_6 ;
  wire \tmp_reg[28]_i_1__1_n_7 ;
  wire \tmp_reg[31]_0 ;
  wire \tmp_reg[31]_1 ;
  wire \tmp_reg[4]_i_1__5_n_0 ;
  wire \tmp_reg[4]_i_1__5_n_1 ;
  wire \tmp_reg[4]_i_1__5_n_2 ;
  wire \tmp_reg[4]_i_1__5_n_3 ;
  wire \tmp_reg[4]_i_1__5_n_4 ;
  wire \tmp_reg[4]_i_1__5_n_5 ;
  wire \tmp_reg[4]_i_1__5_n_6 ;
  wire \tmp_reg[4]_i_1__5_n_7 ;
  wire \tmp_reg[8]_i_1__5_n_0 ;
  wire \tmp_reg[8]_i_1__5_n_1 ;
  wire \tmp_reg[8]_i_1__5_n_2 ;
  wire \tmp_reg[8]_i_1__5_n_3 ;
  wire \tmp_reg[8]_i_1__5_n_4 ;
  wire \tmp_reg[8]_i_1__5_n_5 ;
  wire \tmp_reg[8]_i_1__5_n_6 ;
  wire \tmp_reg[8]_i_1__5_n_7 ;
  wire [3:3]\NLW_tmp_reg[28]_i_1__1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[2]_i_2 
       (.I0(D[2]),
        .I1(bram_seg),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(S),
        .I5(\axi_rdata_reg[2]_0 ),
        .O(\axi_rdata[2]_i_2_n_0 ));
  MUXF7 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(\axi_rdata_reg[2] ),
        .O(\axi_araddr_reg[4] ),
        .S(sel0[2]));
  LUT1 #(
    .INIT(2'h1)) 
    \tmp[0]_i_3__5 
       (.I0(D[0]),
        .O(\tmp[0]_i_3__5_n_0 ));
  FDCE \tmp_reg[0] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[0]_i_2__5_n_7 ),
        .Q(D[0]));
  CARRY4 \tmp_reg[0]_i_2__5 
       (.CI(1'b0),
        .CO({\tmp_reg[0]_i_2__5_n_0 ,\tmp_reg[0]_i_2__5_n_1 ,\tmp_reg[0]_i_2__5_n_2 ,\tmp_reg[0]_i_2__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\tmp_reg[0]_i_2__5_n_4 ,\tmp_reg[0]_i_2__5_n_5 ,\tmp_reg[0]_i_2__5_n_6 ,\tmp_reg[0]_i_2__5_n_7 }),
        .S({D[3:1],\tmp[0]_i_3__5_n_0 }));
  FDCE \tmp_reg[10] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[8]_i_1__5_n_5 ),
        .Q(D[10]));
  FDCE \tmp_reg[11] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[8]_i_1__5_n_4 ),
        .Q(D[11]));
  FDCE \tmp_reg[12] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[12]_i_1__5_n_7 ),
        .Q(D[12]));
  CARRY4 \tmp_reg[12]_i_1__5 
       (.CI(\tmp_reg[8]_i_1__5_n_0 ),
        .CO({\tmp_reg[12]_i_1__5_n_0 ,\tmp_reg[12]_i_1__5_n_1 ,\tmp_reg[12]_i_1__5_n_2 ,\tmp_reg[12]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[12]_i_1__5_n_4 ,\tmp_reg[12]_i_1__5_n_5 ,\tmp_reg[12]_i_1__5_n_6 ,\tmp_reg[12]_i_1__5_n_7 }),
        .S(D[15:12]));
  FDCE \tmp_reg[13] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[12]_i_1__5_n_6 ),
        .Q(D[13]));
  FDCE \tmp_reg[14] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[12]_i_1__5_n_5 ),
        .Q(D[14]));
  FDCE \tmp_reg[15] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[12]_i_1__5_n_4 ),
        .Q(D[15]));
  FDCE \tmp_reg[16] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[16]_i_1__2_n_7 ),
        .Q(D[16]));
  CARRY4 \tmp_reg[16]_i_1__2 
       (.CI(\tmp_reg[12]_i_1__5_n_0 ),
        .CO({\tmp_reg[16]_i_1__2_n_0 ,\tmp_reg[16]_i_1__2_n_1 ,\tmp_reg[16]_i_1__2_n_2 ,\tmp_reg[16]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[16]_i_1__2_n_4 ,\tmp_reg[16]_i_1__2_n_5 ,\tmp_reg[16]_i_1__2_n_6 ,\tmp_reg[16]_i_1__2_n_7 }),
        .S(D[19:16]));
  FDCE \tmp_reg[17] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[16]_i_1__2_n_6 ),
        .Q(D[17]));
  FDCE \tmp_reg[18] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[16]_i_1__2_n_5 ),
        .Q(D[18]));
  FDCE \tmp_reg[19] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[16]_i_1__2_n_4 ),
        .Q(D[19]));
  FDCE \tmp_reg[1] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[0]_i_2__5_n_6 ),
        .Q(D[1]));
  FDCE \tmp_reg[20] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[20]_i_1__1_n_7 ),
        .Q(D[20]));
  CARRY4 \tmp_reg[20]_i_1__1 
       (.CI(\tmp_reg[16]_i_1__2_n_0 ),
        .CO({\tmp_reg[20]_i_1__1_n_0 ,\tmp_reg[20]_i_1__1_n_1 ,\tmp_reg[20]_i_1__1_n_2 ,\tmp_reg[20]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[20]_i_1__1_n_4 ,\tmp_reg[20]_i_1__1_n_5 ,\tmp_reg[20]_i_1__1_n_6 ,\tmp_reg[20]_i_1__1_n_7 }),
        .S(D[23:20]));
  FDCE \tmp_reg[21] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[20]_i_1__1_n_6 ),
        .Q(D[21]));
  FDCE \tmp_reg[22] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[20]_i_1__1_n_5 ),
        .Q(D[22]));
  FDCE \tmp_reg[23] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[20]_i_1__1_n_4 ),
        .Q(D[23]));
  FDCE \tmp_reg[24] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[24]_i_1__1_n_7 ),
        .Q(D[24]));
  CARRY4 \tmp_reg[24]_i_1__1 
       (.CI(\tmp_reg[20]_i_1__1_n_0 ),
        .CO({\tmp_reg[24]_i_1__1_n_0 ,\tmp_reg[24]_i_1__1_n_1 ,\tmp_reg[24]_i_1__1_n_2 ,\tmp_reg[24]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[24]_i_1__1_n_4 ,\tmp_reg[24]_i_1__1_n_5 ,\tmp_reg[24]_i_1__1_n_6 ,\tmp_reg[24]_i_1__1_n_7 }),
        .S(D[27:24]));
  FDCE \tmp_reg[25] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[24]_i_1__1_n_6 ),
        .Q(D[25]));
  FDCE \tmp_reg[26] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[24]_i_1__1_n_5 ),
        .Q(D[26]));
  FDCE \tmp_reg[27] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[24]_i_1__1_n_4 ),
        .Q(D[27]));
  FDCE \tmp_reg[28] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[28]_i_1__1_n_7 ),
        .Q(D[28]));
  CARRY4 \tmp_reg[28]_i_1__1 
       (.CI(\tmp_reg[24]_i_1__1_n_0 ),
        .CO({\NLW_tmp_reg[28]_i_1__1_CO_UNCONNECTED [3],\tmp_reg[28]_i_1__1_n_1 ,\tmp_reg[28]_i_1__1_n_2 ,\tmp_reg[28]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[28]_i_1__1_n_4 ,\tmp_reg[28]_i_1__1_n_5 ,\tmp_reg[28]_i_1__1_n_6 ,\tmp_reg[28]_i_1__1_n_7 }),
        .S(D[31:28]));
  FDCE \tmp_reg[29] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[28]_i_1__1_n_6 ),
        .Q(D[29]));
  FDCE \tmp_reg[2] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[0]_i_2__5_n_5 ),
        .Q(D[2]));
  FDCE \tmp_reg[30] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[28]_i_1__1_n_5 ),
        .Q(D[30]));
  FDCE \tmp_reg[31] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[28]_i_1__1_n_4 ),
        .Q(D[31]));
  FDCE \tmp_reg[3] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[0]_i_2__5_n_4 ),
        .Q(D[3]));
  FDCE \tmp_reg[4] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[4]_i_1__5_n_7 ),
        .Q(D[4]));
  CARRY4 \tmp_reg[4]_i_1__5 
       (.CI(\tmp_reg[0]_i_2__5_n_0 ),
        .CO({\tmp_reg[4]_i_1__5_n_0 ,\tmp_reg[4]_i_1__5_n_1 ,\tmp_reg[4]_i_1__5_n_2 ,\tmp_reg[4]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[4]_i_1__5_n_4 ,\tmp_reg[4]_i_1__5_n_5 ,\tmp_reg[4]_i_1__5_n_6 ,\tmp_reg[4]_i_1__5_n_7 }),
        .S(D[7:4]));
  FDCE \tmp_reg[5] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[4]_i_1__5_n_6 ),
        .Q(D[5]));
  FDCE \tmp_reg[6] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[4]_i_1__5_n_5 ),
        .Q(D[6]));
  FDCE \tmp_reg[7] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[4]_i_1__5_n_4 ),
        .Q(D[7]));
  FDCE \tmp_reg[8] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[8]_i_1__5_n_7 ),
        .Q(D[8]));
  CARRY4 \tmp_reg[8]_i_1__5 
       (.CI(\tmp_reg[4]_i_1__5_n_0 ),
        .CO({\tmp_reg[8]_i_1__5_n_0 ,\tmp_reg[8]_i_1__5_n_1 ,\tmp_reg[8]_i_1__5_n_2 ,\tmp_reg[8]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[8]_i_1__5_n_4 ,\tmp_reg[8]_i_1__5_n_5 ,\tmp_reg[8]_i_1__5_n_6 ,\tmp_reg[8]_i_1__5_n_7 }),
        .S(D[11:8]));
  FDCE \tmp_reg[9] 
       (.C(clk),
        .CE(\tmp_reg[31]_0 ),
        .CLR(\tmp_reg[31]_1 ),
        .D(\tmp_reg[8]_i_1__5_n_6 ),
        .Q(D[9]));
endmodule

(* ORIG_REF_NAME = "counter" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter_3
   (\tmp_reg[0]_0 ,
    S,
    \tmp_reg[7]_0 ,
    \tmp_reg[11]_0 ,
    \tmp_reg[15]_0 ,
    \tmp_reg[19]_0 ,
    \tmp_reg[23]_0 ,
    \tmp_reg[27]_0 ,
    \tmp_reg[31]_0 ,
    DI,
    \slv_reg4_reg[7] ,
    \tmp_reg[14]_0 ,
    \slv_reg4_reg[15] ,
    \tmp_reg[22]_0 ,
    \slv_reg4_reg[23] ,
    \tmp_reg[30]_0 ,
    \slv_reg4_reg[31] ,
    \axi_araddr_reg[4] ,
    ce,
    clk,
    clr0_out,
    \slv_reg7_reg[0] ,
    sel0,
    \axi_rdata_reg[1] ,
    D,
    CO,
    \axi_rdata_reg[1]_0 );
  output \tmp_reg[0]_0 ;
  output [2:0]S;
  output [3:0]\tmp_reg[7]_0 ;
  output [3:0]\tmp_reg[11]_0 ;
  output [3:0]\tmp_reg[15]_0 ;
  output [3:0]\tmp_reg[19]_0 ;
  output [3:0]\tmp_reg[23]_0 ;
  output [3:0]\tmp_reg[27]_0 ;
  output [3:0]\tmp_reg[31]_0 ;
  output [3:0]DI;
  output [3:0]\slv_reg4_reg[7] ;
  output [3:0]\tmp_reg[14]_0 ;
  output [3:0]\slv_reg4_reg[15] ;
  output [3:0]\tmp_reg[22]_0 ;
  output [3:0]\slv_reg4_reg[23] ;
  output [3:0]\tmp_reg[30]_0 ;
  output [3:0]\slv_reg4_reg[31] ;
  output [0:0]\axi_araddr_reg[4] ;
  input ce;
  input clk;
  input clr0_out;
  input [31:0]\slv_reg7_reg[0] ;
  input [2:0]sel0;
  input \axi_rdata_reg[1] ;
  input [0:0]D;
  input [0:0]CO;
  input [0:0]\axi_rdata_reg[1]_0 ;

  wire [0:0]CO;
  wire [0:0]D;
  wire [3:0]DI;
  wire [2:0]S;
  wire [0:0]\axi_araddr_reg[4] ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1] ;
  wire [0:0]\axi_rdata_reg[1]_0 ;
  wire ce;
  wire clk;
  wire clr0_out;
  wire [2:0]sel0;
  wire [3:0]\slv_reg4_reg[15] ;
  wire [3:0]\slv_reg4_reg[23] ;
  wire [3:0]\slv_reg4_reg[31] ;
  wire [3:0]\slv_reg4_reg[7] ;
  wire [31:0]\slv_reg7_reg[0] ;
  wire \tmp[0]_i_3__3_n_0 ;
  wire \tmp_reg[0]_0 ;
  wire \tmp_reg[0]_i_2__3_n_0 ;
  wire \tmp_reg[0]_i_2__3_n_1 ;
  wire \tmp_reg[0]_i_2__3_n_2 ;
  wire \tmp_reg[0]_i_2__3_n_3 ;
  wire \tmp_reg[0]_i_2__3_n_4 ;
  wire \tmp_reg[0]_i_2__3_n_5 ;
  wire \tmp_reg[0]_i_2__3_n_6 ;
  wire \tmp_reg[0]_i_2__3_n_7 ;
  wire [3:0]\tmp_reg[11]_0 ;
  wire \tmp_reg[12]_i_1__3_n_0 ;
  wire \tmp_reg[12]_i_1__3_n_1 ;
  wire \tmp_reg[12]_i_1__3_n_2 ;
  wire \tmp_reg[12]_i_1__3_n_3 ;
  wire \tmp_reg[12]_i_1__3_n_4 ;
  wire \tmp_reg[12]_i_1__3_n_5 ;
  wire \tmp_reg[12]_i_1__3_n_6 ;
  wire \tmp_reg[12]_i_1__3_n_7 ;
  wire [3:0]\tmp_reg[14]_0 ;
  wire [3:0]\tmp_reg[15]_0 ;
  wire \tmp_reg[16]_i_1__0_n_0 ;
  wire \tmp_reg[16]_i_1__0_n_1 ;
  wire \tmp_reg[16]_i_1__0_n_2 ;
  wire \tmp_reg[16]_i_1__0_n_3 ;
  wire \tmp_reg[16]_i_1__0_n_4 ;
  wire \tmp_reg[16]_i_1__0_n_5 ;
  wire \tmp_reg[16]_i_1__0_n_6 ;
  wire \tmp_reg[16]_i_1__0_n_7 ;
  wire [3:0]\tmp_reg[19]_0 ;
  wire \tmp_reg[20]_i_1_n_0 ;
  wire \tmp_reg[20]_i_1_n_1 ;
  wire \tmp_reg[20]_i_1_n_2 ;
  wire \tmp_reg[20]_i_1_n_3 ;
  wire \tmp_reg[20]_i_1_n_4 ;
  wire \tmp_reg[20]_i_1_n_5 ;
  wire \tmp_reg[20]_i_1_n_6 ;
  wire \tmp_reg[20]_i_1_n_7 ;
  wire [3:0]\tmp_reg[22]_0 ;
  wire [3:0]\tmp_reg[23]_0 ;
  wire \tmp_reg[24]_i_1_n_0 ;
  wire \tmp_reg[24]_i_1_n_1 ;
  wire \tmp_reg[24]_i_1_n_2 ;
  wire \tmp_reg[24]_i_1_n_3 ;
  wire \tmp_reg[24]_i_1_n_4 ;
  wire \tmp_reg[24]_i_1_n_5 ;
  wire \tmp_reg[24]_i_1_n_6 ;
  wire \tmp_reg[24]_i_1_n_7 ;
  wire [3:0]\tmp_reg[27]_0 ;
  wire \tmp_reg[28]_i_1_n_1 ;
  wire \tmp_reg[28]_i_1_n_2 ;
  wire \tmp_reg[28]_i_1_n_3 ;
  wire \tmp_reg[28]_i_1_n_4 ;
  wire \tmp_reg[28]_i_1_n_5 ;
  wire \tmp_reg[28]_i_1_n_6 ;
  wire \tmp_reg[28]_i_1_n_7 ;
  wire [3:0]\tmp_reg[30]_0 ;
  wire [3:0]\tmp_reg[31]_0 ;
  wire \tmp_reg[4]_i_1__3_n_0 ;
  wire \tmp_reg[4]_i_1__3_n_1 ;
  wire \tmp_reg[4]_i_1__3_n_2 ;
  wire \tmp_reg[4]_i_1__3_n_3 ;
  wire \tmp_reg[4]_i_1__3_n_4 ;
  wire \tmp_reg[4]_i_1__3_n_5 ;
  wire \tmp_reg[4]_i_1__3_n_6 ;
  wire \tmp_reg[4]_i_1__3_n_7 ;
  wire [3:0]\tmp_reg[7]_0 ;
  wire \tmp_reg[8]_i_1__3_n_0 ;
  wire \tmp_reg[8]_i_1__3_n_1 ;
  wire \tmp_reg[8]_i_1__3_n_2 ;
  wire \tmp_reg[8]_i_1__3_n_3 ;
  wire \tmp_reg[8]_i_1__3_n_4 ;
  wire \tmp_reg[8]_i_1__3_n_5 ;
  wire \tmp_reg[8]_i_1__3_n_6 ;
  wire \tmp_reg[8]_i_1__3_n_7 ;
  wire [3:3]\NLW_tmp_reg[28]_i_1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[1]_i_2 
       (.I0(D),
        .I1(CO),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(\axi_rdata_reg[1]_0 ),
        .I5(S[0]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  MUXF7 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\axi_rdata_reg[1] ),
        .O(\axi_araddr_reg[4] ),
        .S(sel0[2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__0_i_1
       (.I0(\tmp_reg[15]_0 [2]),
        .I1(\slv_reg7_reg[0] [14]),
        .I2(\slv_reg7_reg[0] [15]),
        .I3(\tmp_reg[15]_0 [3]),
        .O(\tmp_reg[14]_0 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__0_i_2
       (.I0(\tmp_reg[15]_0 [0]),
        .I1(\slv_reg7_reg[0] [12]),
        .I2(\slv_reg7_reg[0] [13]),
        .I3(\tmp_reg[15]_0 [1]),
        .O(\tmp_reg[14]_0 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__0_i_3
       (.I0(\tmp_reg[11]_0 [2]),
        .I1(\slv_reg7_reg[0] [10]),
        .I2(\slv_reg7_reg[0] [11]),
        .I3(\tmp_reg[11]_0 [3]),
        .O(\tmp_reg[14]_0 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__0_i_4
       (.I0(\tmp_reg[11]_0 [0]),
        .I1(\slv_reg7_reg[0] [8]),
        .I2(\slv_reg7_reg[0] [9]),
        .I3(\tmp_reg[11]_0 [1]),
        .O(\tmp_reg[14]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__0_i_5
       (.I0(\slv_reg7_reg[0] [15]),
        .I1(\tmp_reg[15]_0 [3]),
        .I2(\slv_reg7_reg[0] [14]),
        .I3(\tmp_reg[15]_0 [2]),
        .O(\slv_reg4_reg[15] [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__0_i_6
       (.I0(\slv_reg7_reg[0] [13]),
        .I1(\tmp_reg[15]_0 [1]),
        .I2(\slv_reg7_reg[0] [12]),
        .I3(\tmp_reg[15]_0 [0]),
        .O(\slv_reg4_reg[15] [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__0_i_7
       (.I0(\slv_reg7_reg[0] [11]),
        .I1(\tmp_reg[11]_0 [3]),
        .I2(\slv_reg7_reg[0] [10]),
        .I3(\tmp_reg[11]_0 [2]),
        .O(\slv_reg4_reg[15] [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__0_i_8
       (.I0(\slv_reg7_reg[0] [9]),
        .I1(\tmp_reg[11]_0 [1]),
        .I2(\slv_reg7_reg[0] [8]),
        .I3(\tmp_reg[11]_0 [0]),
        .O(\slv_reg4_reg[15] [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__1_i_1
       (.I0(\tmp_reg[23]_0 [2]),
        .I1(\slv_reg7_reg[0] [22]),
        .I2(\slv_reg7_reg[0] [23]),
        .I3(\tmp_reg[23]_0 [3]),
        .O(\tmp_reg[22]_0 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__1_i_2
       (.I0(\tmp_reg[23]_0 [0]),
        .I1(\slv_reg7_reg[0] [20]),
        .I2(\slv_reg7_reg[0] [21]),
        .I3(\tmp_reg[23]_0 [1]),
        .O(\tmp_reg[22]_0 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__1_i_3
       (.I0(\tmp_reg[19]_0 [2]),
        .I1(\slv_reg7_reg[0] [18]),
        .I2(\slv_reg7_reg[0] [19]),
        .I3(\tmp_reg[19]_0 [3]),
        .O(\tmp_reg[22]_0 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__1_i_4
       (.I0(\tmp_reg[19]_0 [0]),
        .I1(\slv_reg7_reg[0] [16]),
        .I2(\slv_reg7_reg[0] [17]),
        .I3(\tmp_reg[19]_0 [1]),
        .O(\tmp_reg[22]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__1_i_5
       (.I0(\slv_reg7_reg[0] [23]),
        .I1(\tmp_reg[23]_0 [3]),
        .I2(\slv_reg7_reg[0] [22]),
        .I3(\tmp_reg[23]_0 [2]),
        .O(\slv_reg4_reg[23] [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__1_i_6
       (.I0(\slv_reg7_reg[0] [21]),
        .I1(\tmp_reg[23]_0 [1]),
        .I2(\slv_reg7_reg[0] [20]),
        .I3(\tmp_reg[23]_0 [0]),
        .O(\slv_reg4_reg[23] [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__1_i_7
       (.I0(\slv_reg7_reg[0] [19]),
        .I1(\tmp_reg[19]_0 [3]),
        .I2(\slv_reg7_reg[0] [18]),
        .I3(\tmp_reg[19]_0 [2]),
        .O(\slv_reg4_reg[23] [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__1_i_8
       (.I0(\slv_reg7_reg[0] [17]),
        .I1(\tmp_reg[19]_0 [1]),
        .I2(\slv_reg7_reg[0] [16]),
        .I3(\tmp_reg[19]_0 [0]),
        .O(\slv_reg4_reg[23] [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__2_i_1
       (.I0(\tmp_reg[31]_0 [2]),
        .I1(\slv_reg7_reg[0] [30]),
        .I2(\slv_reg7_reg[0] [31]),
        .I3(\tmp_reg[31]_0 [3]),
        .O(\tmp_reg[30]_0 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__2_i_2
       (.I0(\tmp_reg[31]_0 [0]),
        .I1(\slv_reg7_reg[0] [28]),
        .I2(\slv_reg7_reg[0] [29]),
        .I3(\tmp_reg[31]_0 [1]),
        .O(\tmp_reg[30]_0 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__2_i_3
       (.I0(\tmp_reg[27]_0 [2]),
        .I1(\slv_reg7_reg[0] [26]),
        .I2(\slv_reg7_reg[0] [27]),
        .I3(\tmp_reg[27]_0 [3]),
        .O(\tmp_reg[30]_0 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry__2_i_4
       (.I0(\tmp_reg[27]_0 [0]),
        .I1(\slv_reg7_reg[0] [24]),
        .I2(\slv_reg7_reg[0] [25]),
        .I3(\tmp_reg[27]_0 [1]),
        .O(\tmp_reg[30]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__2_i_5
       (.I0(\slv_reg7_reg[0] [31]),
        .I1(\tmp_reg[31]_0 [3]),
        .I2(\slv_reg7_reg[0] [30]),
        .I3(\tmp_reg[31]_0 [2]),
        .O(\slv_reg4_reg[31] [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__2_i_6
       (.I0(\slv_reg7_reg[0] [29]),
        .I1(\tmp_reg[31]_0 [1]),
        .I2(\slv_reg7_reg[0] [28]),
        .I3(\tmp_reg[31]_0 [0]),
        .O(\slv_reg4_reg[31] [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__2_i_7
       (.I0(\slv_reg7_reg[0] [27]),
        .I1(\tmp_reg[27]_0 [3]),
        .I2(\slv_reg7_reg[0] [26]),
        .I3(\tmp_reg[27]_0 [2]),
        .O(\slv_reg4_reg[31] [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry__2_i_8
       (.I0(\slv_reg7_reg[0] [25]),
        .I1(\tmp_reg[27]_0 [1]),
        .I2(\slv_reg7_reg[0] [24]),
        .I3(\tmp_reg[27]_0 [0]),
        .O(\slv_reg4_reg[31] [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry_i_1
       (.I0(\tmp_reg[7]_0 [2]),
        .I1(\slv_reg7_reg[0] [6]),
        .I2(\slv_reg7_reg[0] [7]),
        .I3(\tmp_reg[7]_0 [3]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry_i_2
       (.I0(\tmp_reg[7]_0 [0]),
        .I1(\slv_reg7_reg[0] [4]),
        .I2(\slv_reg7_reg[0] [5]),
        .I3(\tmp_reg[7]_0 [1]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry_i_3
       (.I0(S[1]),
        .I1(\slv_reg7_reg[0] [2]),
        .I2(\slv_reg7_reg[0] [3]),
        .I3(S[2]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    cmp0_carry_i_4
       (.I0(\tmp_reg[0]_0 ),
        .I1(\slv_reg7_reg[0] [0]),
        .I2(\slv_reg7_reg[0] [1]),
        .I3(S[0]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry_i_5
       (.I0(\slv_reg7_reg[0] [7]),
        .I1(\tmp_reg[7]_0 [3]),
        .I2(\slv_reg7_reg[0] [6]),
        .I3(\tmp_reg[7]_0 [2]),
        .O(\slv_reg4_reg[7] [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry_i_6
       (.I0(\slv_reg7_reg[0] [5]),
        .I1(\tmp_reg[7]_0 [1]),
        .I2(\slv_reg7_reg[0] [4]),
        .I3(\tmp_reg[7]_0 [0]),
        .O(\slv_reg4_reg[7] [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry_i_7
       (.I0(\slv_reg7_reg[0] [3]),
        .I1(S[2]),
        .I2(\slv_reg7_reg[0] [2]),
        .I3(S[1]),
        .O(\slv_reg4_reg[7] [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    cmp0_carry_i_8
       (.I0(\slv_reg7_reg[0] [1]),
        .I1(S[0]),
        .I2(\slv_reg7_reg[0] [0]),
        .I3(\tmp_reg[0]_0 ),
        .O(\slv_reg4_reg[7] [0]));
  LUT1 #(
    .INIT(2'h1)) 
    \tmp[0]_i_3__3 
       (.I0(\tmp_reg[0]_0 ),
        .O(\tmp[0]_i_3__3_n_0 ));
  FDCE \tmp_reg[0] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[0]_i_2__3_n_7 ),
        .Q(\tmp_reg[0]_0 ));
  CARRY4 \tmp_reg[0]_i_2__3 
       (.CI(1'b0),
        .CO({\tmp_reg[0]_i_2__3_n_0 ,\tmp_reg[0]_i_2__3_n_1 ,\tmp_reg[0]_i_2__3_n_2 ,\tmp_reg[0]_i_2__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\tmp_reg[0]_i_2__3_n_4 ,\tmp_reg[0]_i_2__3_n_5 ,\tmp_reg[0]_i_2__3_n_6 ,\tmp_reg[0]_i_2__3_n_7 }),
        .S({S,\tmp[0]_i_3__3_n_0 }));
  FDCE \tmp_reg[10] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[8]_i_1__3_n_5 ),
        .Q(\tmp_reg[11]_0 [2]));
  FDCE \tmp_reg[11] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[8]_i_1__3_n_4 ),
        .Q(\tmp_reg[11]_0 [3]));
  FDCE \tmp_reg[12] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[12]_i_1__3_n_7 ),
        .Q(\tmp_reg[15]_0 [0]));
  CARRY4 \tmp_reg[12]_i_1__3 
       (.CI(\tmp_reg[8]_i_1__3_n_0 ),
        .CO({\tmp_reg[12]_i_1__3_n_0 ,\tmp_reg[12]_i_1__3_n_1 ,\tmp_reg[12]_i_1__3_n_2 ,\tmp_reg[12]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[12]_i_1__3_n_4 ,\tmp_reg[12]_i_1__3_n_5 ,\tmp_reg[12]_i_1__3_n_6 ,\tmp_reg[12]_i_1__3_n_7 }),
        .S(\tmp_reg[15]_0 ));
  FDCE \tmp_reg[13] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[12]_i_1__3_n_6 ),
        .Q(\tmp_reg[15]_0 [1]));
  FDCE \tmp_reg[14] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[12]_i_1__3_n_5 ),
        .Q(\tmp_reg[15]_0 [2]));
  FDCE \tmp_reg[15] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[12]_i_1__3_n_4 ),
        .Q(\tmp_reg[15]_0 [3]));
  FDCE \tmp_reg[16] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[16]_i_1__0_n_7 ),
        .Q(\tmp_reg[19]_0 [0]));
  CARRY4 \tmp_reg[16]_i_1__0 
       (.CI(\tmp_reg[12]_i_1__3_n_0 ),
        .CO({\tmp_reg[16]_i_1__0_n_0 ,\tmp_reg[16]_i_1__0_n_1 ,\tmp_reg[16]_i_1__0_n_2 ,\tmp_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[16]_i_1__0_n_4 ,\tmp_reg[16]_i_1__0_n_5 ,\tmp_reg[16]_i_1__0_n_6 ,\tmp_reg[16]_i_1__0_n_7 }),
        .S(\tmp_reg[19]_0 ));
  FDCE \tmp_reg[17] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[16]_i_1__0_n_6 ),
        .Q(\tmp_reg[19]_0 [1]));
  FDCE \tmp_reg[18] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[16]_i_1__0_n_5 ),
        .Q(\tmp_reg[19]_0 [2]));
  FDCE \tmp_reg[19] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[16]_i_1__0_n_4 ),
        .Q(\tmp_reg[19]_0 [3]));
  FDCE \tmp_reg[1] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[0]_i_2__3_n_6 ),
        .Q(S[0]));
  FDCE \tmp_reg[20] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[20]_i_1_n_7 ),
        .Q(\tmp_reg[23]_0 [0]));
  CARRY4 \tmp_reg[20]_i_1 
       (.CI(\tmp_reg[16]_i_1__0_n_0 ),
        .CO({\tmp_reg[20]_i_1_n_0 ,\tmp_reg[20]_i_1_n_1 ,\tmp_reg[20]_i_1_n_2 ,\tmp_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[20]_i_1_n_4 ,\tmp_reg[20]_i_1_n_5 ,\tmp_reg[20]_i_1_n_6 ,\tmp_reg[20]_i_1_n_7 }),
        .S(\tmp_reg[23]_0 ));
  FDCE \tmp_reg[21] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[20]_i_1_n_6 ),
        .Q(\tmp_reg[23]_0 [1]));
  FDCE \tmp_reg[22] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[20]_i_1_n_5 ),
        .Q(\tmp_reg[23]_0 [2]));
  FDCE \tmp_reg[23] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[20]_i_1_n_4 ),
        .Q(\tmp_reg[23]_0 [3]));
  FDCE \tmp_reg[24] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[24]_i_1_n_7 ),
        .Q(\tmp_reg[27]_0 [0]));
  CARRY4 \tmp_reg[24]_i_1 
       (.CI(\tmp_reg[20]_i_1_n_0 ),
        .CO({\tmp_reg[24]_i_1_n_0 ,\tmp_reg[24]_i_1_n_1 ,\tmp_reg[24]_i_1_n_2 ,\tmp_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[24]_i_1_n_4 ,\tmp_reg[24]_i_1_n_5 ,\tmp_reg[24]_i_1_n_6 ,\tmp_reg[24]_i_1_n_7 }),
        .S(\tmp_reg[27]_0 ));
  FDCE \tmp_reg[25] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[24]_i_1_n_6 ),
        .Q(\tmp_reg[27]_0 [1]));
  FDCE \tmp_reg[26] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[24]_i_1_n_5 ),
        .Q(\tmp_reg[27]_0 [2]));
  FDCE \tmp_reg[27] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[24]_i_1_n_4 ),
        .Q(\tmp_reg[27]_0 [3]));
  FDCE \tmp_reg[28] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[28]_i_1_n_7 ),
        .Q(\tmp_reg[31]_0 [0]));
  CARRY4 \tmp_reg[28]_i_1 
       (.CI(\tmp_reg[24]_i_1_n_0 ),
        .CO({\NLW_tmp_reg[28]_i_1_CO_UNCONNECTED [3],\tmp_reg[28]_i_1_n_1 ,\tmp_reg[28]_i_1_n_2 ,\tmp_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[28]_i_1_n_4 ,\tmp_reg[28]_i_1_n_5 ,\tmp_reg[28]_i_1_n_6 ,\tmp_reg[28]_i_1_n_7 }),
        .S(\tmp_reg[31]_0 ));
  FDCE \tmp_reg[29] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[28]_i_1_n_6 ),
        .Q(\tmp_reg[31]_0 [1]));
  FDCE \tmp_reg[2] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[0]_i_2__3_n_5 ),
        .Q(S[1]));
  FDCE \tmp_reg[30] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[28]_i_1_n_5 ),
        .Q(\tmp_reg[31]_0 [2]));
  FDCE \tmp_reg[31] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[28]_i_1_n_4 ),
        .Q(\tmp_reg[31]_0 [3]));
  FDCE \tmp_reg[3] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[0]_i_2__3_n_4 ),
        .Q(S[2]));
  FDCE \tmp_reg[4] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[4]_i_1__3_n_7 ),
        .Q(\tmp_reg[7]_0 [0]));
  CARRY4 \tmp_reg[4]_i_1__3 
       (.CI(\tmp_reg[0]_i_2__3_n_0 ),
        .CO({\tmp_reg[4]_i_1__3_n_0 ,\tmp_reg[4]_i_1__3_n_1 ,\tmp_reg[4]_i_1__3_n_2 ,\tmp_reg[4]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[4]_i_1__3_n_4 ,\tmp_reg[4]_i_1__3_n_5 ,\tmp_reg[4]_i_1__3_n_6 ,\tmp_reg[4]_i_1__3_n_7 }),
        .S(\tmp_reg[7]_0 ));
  FDCE \tmp_reg[5] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[4]_i_1__3_n_6 ),
        .Q(\tmp_reg[7]_0 [1]));
  FDCE \tmp_reg[6] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[4]_i_1__3_n_5 ),
        .Q(\tmp_reg[7]_0 [2]));
  FDCE \tmp_reg[7] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[4]_i_1__3_n_4 ),
        .Q(\tmp_reg[7]_0 [3]));
  FDCE \tmp_reg[8] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[8]_i_1__3_n_7 ),
        .Q(\tmp_reg[11]_0 [0]));
  CARRY4 \tmp_reg[8]_i_1__3 
       (.CI(\tmp_reg[4]_i_1__3_n_0 ),
        .CO({\tmp_reg[8]_i_1__3_n_0 ,\tmp_reg[8]_i_1__3_n_1 ,\tmp_reg[8]_i_1__3_n_2 ,\tmp_reg[8]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_reg[8]_i_1__3_n_4 ,\tmp_reg[8]_i_1__3_n_5 ,\tmp_reg[8]_i_1__3_n_6 ,\tmp_reg[8]_i_1__3_n_7 }),
        .S(\tmp_reg[11]_0 ));
  FDCE \tmp_reg[9] 
       (.C(clk),
        .CE(ce),
        .CLR(clr0_out),
        .D(\tmp_reg[8]_i_1__3_n_6 ),
        .Q(\tmp_reg[11]_0 [1]));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ip_timers_0_0,ip_timers_v2_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "ip_timers_v2_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    lt_dir,
    timers_enabled,
    bram_seg,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input clk;
  input [1:0]lt_dir;
  output timers_enabled;
  output bram_seg;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 8, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [4:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire bram_seg;
  wire clk;
  wire [1:0]lt_dir;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire timers_enabled;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_timers_v2_0 U0
       (.\FSM_onehot_state_reg[1] (timers_enabled),
        .S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .bram_seg(bram_seg),
        .clk(clk),
        .lt_dir(lt_dir),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fsm_auto_mode
   (bram_seg,
    lt_dir_0_sp_1,
    \slv_reg5_reg[7] ,
    \FSM_onehot_state_reg[1]_0 ,
    \slv_reg5_reg[6] ,
    \slv_reg5_reg[5] ,
    \slv_reg5_reg[3] ,
    clr0_out,
    \slv_reg5_reg[9] ,
    clr1_out,
    lt_dir,
    geqOp,
    Q,
    CO,
    \tmp_reg[16] ,
    eqOp,
    eqOp_0,
    eqOp_1,
    clk);
  output bram_seg;
  output lt_dir_0_sp_1;
  output \slv_reg5_reg[7] ;
  output \FSM_onehot_state_reg[1]_0 ;
  output \slv_reg5_reg[6] ;
  output \slv_reg5_reg[5] ;
  output \slv_reg5_reg[3] ;
  output clr0_out;
  output \slv_reg5_reg[9] ;
  output clr1_out;
  input [1:0]lt_dir;
  input geqOp;
  input [10:0]Q;
  input [0:0]CO;
  input \tmp_reg[16] ;
  input eqOp;
  input eqOp_0;
  input eqOp_1;
  input clk;

  wire [0:0]CO;
  wire \FSM_onehot_state[0]_i_1_n_0 ;
  wire \FSM_onehot_state[1]_i_1_n_0 ;
  wire \FSM_onehot_state[2]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[1]_0 ;
  wire \FSM_onehot_state_reg_n_0_[0] ;
  wire \FSM_onehot_state_reg_n_0_[2] ;
  wire [10:0]Q;
  wire auto_clr;
  wire bram_seg;
  wire clk;
  wire clr0_out;
  wire clr1_out;
  wire en;
  wire eqOp;
  wire eqOp_0;
  wire eqOp_1;
  wire geqOp;
  wire [1:0]lt_dir;
  wire lt_dir_0_sn_1;
  wire \slv_reg5_reg[3] ;
  wire \slv_reg5_reg[5] ;
  wire \slv_reg5_reg[6] ;
  wire \slv_reg5_reg[7] ;
  wire \slv_reg5_reg[9] ;
  wire \tmp[0]_i_4_n_0 ;
  wire \tmp_reg[16] ;

  assign lt_dir_0_sp_1 = lt_dir_0_sn_1;
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_onehot_state[0]_i_1 
       (.I0(Q[1]),
        .O(\FSM_onehot_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFAF80000)) 
    \FSM_onehot_state[1]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(CO),
        .I2(\FSM_onehot_state_reg_n_0_[0] ),
        .I3(auto_clr),
        .I4(Q[1]),
        .O(\FSM_onehot_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFF020000)) 
    \FSM_onehot_state[2]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(CO),
        .I2(\FSM_onehot_state_reg_n_0_[0] ),
        .I3(auto_clr),
        .I4(Q[1]),
        .O(\FSM_onehot_state[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "st1_idle:001,st2_clear:010,st3_count:100," *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[0]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[0] ),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "st1_idle:001,st2_clear:010,st3_count:100," *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[1]_i_1_n_0 ),
        .Q(auto_clr),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "st1_idle:001,st2_clear:010,st3_count:100," *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[2]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[2] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'hE)) 
    clk_1kHz_en_i_i_2
       (.I0(Q[10]),
        .I1(auto_clr),
        .O(clr1_out));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'hE)) 
    clk_1kHz_en_i_i_2__0
       (.I0(Q[9]),
        .I1(auto_clr),
        .O(\slv_reg5_reg[9] ));
  LUT2 #(
    .INIT(4'hE)) 
    clk_1kHz_en_i_i_2__1
       (.I0(Q[8]),
        .I1(auto_clr),
        .O(clr0_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_t_ff t_ff_inst
       (.Q(Q[1]),
        .auto_clr(auto_clr),
        .bram_seg(bram_seg),
        .en(en),
        .temp_reg_0(\FSM_onehot_state_reg_n_0_[0] ),
        .temp_reg_1(\FSM_onehot_state_reg_n_0_[2] ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h04)) 
    temp_i_2
       (.I0(auto_clr),
        .I1(\FSM_onehot_state_reg_n_0_[2] ),
        .I2(\FSM_onehot_state_reg_n_0_[0] ),
        .O(en));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h00FF0004)) 
    timers_enabled_INST_0
       (.I0(auto_clr),
        .I1(\FSM_onehot_state_reg_n_0_[2] ),
        .I2(\FSM_onehot_state_reg_n_0_[0] ),
        .I3(CO),
        .I4(Q[0]),
        .O(\FSM_onehot_state_reg[1]_0 ));
  LUT4 #(
    .INIT(16'hFFD0)) 
    \tmp[0]_i_1 
       (.I0(lt_dir[0]),
        .I1(lt_dir[1]),
        .I2(\tmp[0]_i_4_n_0 ),
        .I3(geqOp),
        .O(lt_dir_0_sn_1));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \tmp[0]_i_1__0 
       (.I0(\FSM_onehot_state_reg[1]_0 ),
        .I1(Q[7]),
        .I2(eqOp),
        .O(\slv_reg5_reg[7] ));
  LUT3 #(
    .INIT(8'hF8)) 
    \tmp[0]_i_1__1 
       (.I0(\FSM_onehot_state_reg[1]_0 ),
        .I1(Q[6]),
        .I2(eqOp_0),
        .O(\slv_reg5_reg[6] ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \tmp[0]_i_1__2 
       (.I0(\FSM_onehot_state_reg[1]_0 ),
        .I1(Q[5]),
        .I2(eqOp_1),
        .O(\slv_reg5_reg[5] ));
  LUT6 #(
    .INIT(64'hFFFFFB510000FB51)) 
    \tmp[0]_i_3__2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(clr0_out),
        .I3(\slv_reg5_reg[9] ),
        .I4(Q[4]),
        .I5(clr1_out),
        .O(\slv_reg5_reg[3] ));
  LUT6 #(
    .INIT(64'h00FF00F000880080)) 
    \tmp[0]_i_4 
       (.I0(Q[4]),
        .I1(Q[7]),
        .I2(en),
        .I3(CO),
        .I4(Q[0]),
        .I5(\tmp_reg[16] ),
        .O(\tmp[0]_i_4_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_timers_v2_0
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    \FSM_onehot_state_reg[1] ,
    bram_seg,
    s00_axi_rvalid,
    s00_axi_bvalid,
    clk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_aclk,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    lt_dir,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output \FSM_onehot_state_reg[1] ;
  output bram_seg;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input clk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [1:0]lt_dir;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire \FSM_onehot_state_reg[1] ;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire bram_seg;
  wire clk;
  wire [1:0]lt_dir;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_timers_v2_0_S00_AXI ip_timers_v2_0_S00_AXI_inst
       (.\FSM_onehot_state_reg[1] (\FSM_onehot_state_reg[1] ),
        .S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .bram_seg(bram_seg),
        .clk(clk),
        .lt_dir(lt_dir),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_timers_v2_0_S00_AXI
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    \FSM_onehot_state_reg[1] ,
    bram_seg,
    s00_axi_rvalid,
    s00_axi_bvalid,
    clk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_aclk,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    lt_dir,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output \FSM_onehot_state_reg[1] ;
  output bram_seg;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input clk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [1:0]lt_dir;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire \FSM_onehot_state_reg[1] ;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire a_mode;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_4_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire b_mode;
  wire bram_seg;
  wire c_mode;
  wire cea;
  wire ceb;
  wire cec;
  wire clk;
  wire clra;
  wire clrb;
  wire clrc;
  wire [31:0]\counter_inst_B/tmp_reg ;
  wire [31:0]\counter_inst_C/tmp_reg ;
  wire [1:0]lt_dir;
  wire p_0_in;
  wire [2:0]p_0_in_0;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:0]sel0;
  wire [31:0]slv_reg4;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire \slv_reg5_reg_n_0_[0] ;
  wire \slv_reg5_reg_n_0_[11] ;
  wire \slv_reg5_reg_n_0_[12] ;
  wire \slv_reg5_reg_n_0_[13] ;
  wire \slv_reg5_reg_n_0_[14] ;
  wire \slv_reg5_reg_n_0_[15] ;
  wire \slv_reg5_reg_n_0_[16] ;
  wire \slv_reg5_reg_n_0_[17] ;
  wire \slv_reg5_reg_n_0_[18] ;
  wire \slv_reg5_reg_n_0_[19] ;
  wire \slv_reg5_reg_n_0_[1] ;
  wire \slv_reg5_reg_n_0_[20] ;
  wire \slv_reg5_reg_n_0_[21] ;
  wire \slv_reg5_reg_n_0_[22] ;
  wire \slv_reg5_reg_n_0_[23] ;
  wire \slv_reg5_reg_n_0_[24] ;
  wire \slv_reg5_reg_n_0_[25] ;
  wire \slv_reg5_reg_n_0_[26] ;
  wire \slv_reg5_reg_n_0_[27] ;
  wire \slv_reg5_reg_n_0_[28] ;
  wire \slv_reg5_reg_n_0_[29] ;
  wire \slv_reg5_reg_n_0_[30] ;
  wire \slv_reg5_reg_n_0_[31] ;
  wire [31:0]slv_reg6;
  wire [31:0]slv_reg7;
  wire slv_reg_rden;
  wire slv_reg_wren__2;
  wire tc;

  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(p_0_in));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[2]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(sel0[0]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(sel0[1]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(sel0[2]),
        .S(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in_0[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in_0[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[4]_i_1 
       (.I0(s00_axi_awaddr[2]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in_0[2]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in_0[0]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in_0[1]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(p_0_in_0[2]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[0]_i_2 
       (.I0(\slv_reg5_reg_n_0_[0] ),
        .I1(slv_reg7[0]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[0]),
        .I5(slv_reg6[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[10]_i_3 
       (.I0(clra),
        .I1(slv_reg7[10]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[10]),
        .I5(slv_reg6[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[11]_i_3 
       (.I0(\slv_reg5_reg_n_0_[11] ),
        .I1(slv_reg7[11]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[11]),
        .I5(slv_reg6[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[12]_i_3 
       (.I0(\slv_reg5_reg_n_0_[12] ),
        .I1(slv_reg7[12]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[12]),
        .I5(slv_reg6[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[13]_i_3 
       (.I0(\slv_reg5_reg_n_0_[13] ),
        .I1(slv_reg7[13]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[13]),
        .I5(slv_reg6[13]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[14]_i_3 
       (.I0(\slv_reg5_reg_n_0_[14] ),
        .I1(slv_reg7[14]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[14]),
        .I5(slv_reg6[14]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[15]_i_3 
       (.I0(\slv_reg5_reg_n_0_[15] ),
        .I1(slv_reg7[15]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[15]),
        .I5(slv_reg6[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[16]_i_3 
       (.I0(\slv_reg5_reg_n_0_[16] ),
        .I1(slv_reg7[16]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[16]),
        .I5(slv_reg6[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[17]_i_3 
       (.I0(\slv_reg5_reg_n_0_[17] ),
        .I1(slv_reg7[17]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[17]),
        .I5(slv_reg6[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[18]_i_3 
       (.I0(\slv_reg5_reg_n_0_[18] ),
        .I1(slv_reg7[18]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[18]),
        .I5(slv_reg6[18]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[19]_i_3 
       (.I0(\slv_reg5_reg_n_0_[19] ),
        .I1(slv_reg7[19]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[19]),
        .I5(slv_reg6[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[1]_i_3 
       (.I0(\slv_reg5_reg_n_0_[1] ),
        .I1(slv_reg7[1]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[1]),
        .I5(slv_reg6[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[20]_i_3 
       (.I0(\slv_reg5_reg_n_0_[20] ),
        .I1(slv_reg7[20]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[20]),
        .I5(slv_reg6[20]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[21]_i_3 
       (.I0(\slv_reg5_reg_n_0_[21] ),
        .I1(slv_reg7[21]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[21]),
        .I5(slv_reg6[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[22]_i_3 
       (.I0(\slv_reg5_reg_n_0_[22] ),
        .I1(slv_reg7[22]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[22]),
        .I5(slv_reg6[22]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[23]_i_3 
       (.I0(\slv_reg5_reg_n_0_[23] ),
        .I1(slv_reg7[23]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[23]),
        .I5(slv_reg6[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[24]_i_3 
       (.I0(\slv_reg5_reg_n_0_[24] ),
        .I1(slv_reg7[24]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[24]),
        .I5(slv_reg6[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[25]_i_3 
       (.I0(\slv_reg5_reg_n_0_[25] ),
        .I1(slv_reg7[25]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[25]),
        .I5(slv_reg6[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[26]_i_3 
       (.I0(\slv_reg5_reg_n_0_[26] ),
        .I1(slv_reg7[26]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[26]),
        .I5(slv_reg6[26]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[27]_i_3 
       (.I0(\slv_reg5_reg_n_0_[27] ),
        .I1(slv_reg7[27]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[27]),
        .I5(slv_reg6[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[28]_i_3 
       (.I0(\slv_reg5_reg_n_0_[28] ),
        .I1(slv_reg7[28]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[28]),
        .I5(slv_reg6[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[29]_i_3 
       (.I0(\slv_reg5_reg_n_0_[29] ),
        .I1(slv_reg7[29]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[29]),
        .I5(slv_reg6[29]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[2]_i_3 
       (.I0(c_mode),
        .I1(slv_reg7[2]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[2]),
        .I5(slv_reg6[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[30]_i_3 
       (.I0(\slv_reg5_reg_n_0_[30] ),
        .I1(slv_reg7[30]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[30]),
        .I5(slv_reg6[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[31]_i_4 
       (.I0(\slv_reg5_reg_n_0_[31] ),
        .I1(slv_reg7[31]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[31]),
        .I5(slv_reg6[31]),
        .O(\axi_rdata[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[3]_i_3 
       (.I0(b_mode),
        .I1(slv_reg7[3]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[3]),
        .I5(slv_reg6[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[4]_i_3 
       (.I0(a_mode),
        .I1(slv_reg7[4]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[4]),
        .I5(slv_reg6[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[5]_i_3 
       (.I0(cec),
        .I1(slv_reg7[5]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[5]),
        .I5(slv_reg6[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[6]_i_3 
       (.I0(ceb),
        .I1(slv_reg7[6]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[6]),
        .I5(slv_reg6[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[7]_i_3 
       (.I0(cea),
        .I1(slv_reg7[7]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[7]),
        .I5(slv_reg6[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[8]_i_3 
       (.I0(clrc),
        .I1(slv_reg7[8]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[8]),
        .I5(slv_reg6[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[9]_i_3 
       (.I0(clrb),
        .I1(slv_reg7[9]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(slv_reg4[9]),
        .I5(slv_reg6[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'h00002000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in_0[0]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00002000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in_0[0]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00002000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in_0[0]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg4[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT5 #(
    .INIT(32'h00002000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in_0[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg4[0]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg4[1]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg4[2]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg4[3]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg4[4]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg4[5]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(p_0_in_0[0]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(p_0_in_0[0]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(p_0_in_0[0]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(p_0_in_0[0]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg5_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(clra),
        .R(p_0_in));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg5_reg_n_0_[11] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg5_reg_n_0_[12] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg5_reg_n_0_[13] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg5_reg_n_0_[14] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg5_reg_n_0_[15] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg5_reg_n_0_[16] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg5_reg_n_0_[17] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg5_reg_n_0_[18] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg5_reg_n_0_[19] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg5_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg5_reg_n_0_[20] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg5_reg_n_0_[21] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg5_reg_n_0_[22] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg5_reg_n_0_[23] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg5_reg_n_0_[24] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg5_reg_n_0_[25] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg5_reg_n_0_[26] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg5_reg_n_0_[27] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg5_reg_n_0_[28] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg5_reg_n_0_[29] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(c_mode),
        .R(p_0_in));
  FDRE \slv_reg5_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg5_reg_n_0_[30] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg5_reg_n_0_[31] ),
        .R(p_0_in));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(b_mode),
        .R(p_0_in));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(a_mode),
        .R(p_0_in));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(cec),
        .R(p_0_in));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(ceb),
        .R(p_0_in));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(cea),
        .R(p_0_in));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(clrc),
        .R(p_0_in));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(clrb),
        .R(p_0_in));
  FDCE \slv_reg6_reg[0] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [0]),
        .Q(slv_reg6[0]));
  FDCE \slv_reg6_reg[10] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [10]),
        .Q(slv_reg6[10]));
  FDCE \slv_reg6_reg[11] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [11]),
        .Q(slv_reg6[11]));
  FDCE \slv_reg6_reg[12] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [12]),
        .Q(slv_reg6[12]));
  FDCE \slv_reg6_reg[13] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [13]),
        .Q(slv_reg6[13]));
  FDCE \slv_reg6_reg[14] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [14]),
        .Q(slv_reg6[14]));
  FDCE \slv_reg6_reg[15] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [15]),
        .Q(slv_reg6[15]));
  FDCE \slv_reg6_reg[16] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [16]),
        .Q(slv_reg6[16]));
  FDCE \slv_reg6_reg[17] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [17]),
        .Q(slv_reg6[17]));
  FDCE \slv_reg6_reg[18] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [18]),
        .Q(slv_reg6[18]));
  FDCE \slv_reg6_reg[19] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [19]),
        .Q(slv_reg6[19]));
  FDCE \slv_reg6_reg[1] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [1]),
        .Q(slv_reg6[1]));
  FDCE \slv_reg6_reg[20] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [20]),
        .Q(slv_reg6[20]));
  FDCE \slv_reg6_reg[21] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [21]),
        .Q(slv_reg6[21]));
  FDCE \slv_reg6_reg[22] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [22]),
        .Q(slv_reg6[22]));
  FDCE \slv_reg6_reg[23] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [23]),
        .Q(slv_reg6[23]));
  FDCE \slv_reg6_reg[24] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [24]),
        .Q(slv_reg6[24]));
  FDCE \slv_reg6_reg[25] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [25]),
        .Q(slv_reg6[25]));
  FDCE \slv_reg6_reg[26] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [26]),
        .Q(slv_reg6[26]));
  FDCE \slv_reg6_reg[27] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [27]),
        .Q(slv_reg6[27]));
  FDCE \slv_reg6_reg[28] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [28]),
        .Q(slv_reg6[28]));
  FDCE \slv_reg6_reg[29] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [29]),
        .Q(slv_reg6[29]));
  FDCE \slv_reg6_reg[2] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [2]),
        .Q(slv_reg6[2]));
  FDCE \slv_reg6_reg[30] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [30]),
        .Q(slv_reg6[30]));
  FDCE \slv_reg6_reg[31] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [31]),
        .Q(slv_reg6[31]));
  FDCE \slv_reg6_reg[3] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [3]),
        .Q(slv_reg6[3]));
  FDCE \slv_reg6_reg[4] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [4]),
        .Q(slv_reg6[4]));
  FDCE \slv_reg6_reg[5] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [5]),
        .Q(slv_reg6[5]));
  FDCE \slv_reg6_reg[6] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [6]),
        .Q(slv_reg6[6]));
  FDCE \slv_reg6_reg[7] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [7]),
        .Q(slv_reg6[7]));
  FDCE \slv_reg6_reg[8] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [8]),
        .Q(slv_reg6[8]));
  FDCE \slv_reg6_reg[9] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_B/tmp_reg [9]),
        .Q(slv_reg6[9]));
  FDCE \slv_reg7_reg[0] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [0]),
        .Q(slv_reg7[0]));
  FDCE \slv_reg7_reg[10] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [10]),
        .Q(slv_reg7[10]));
  FDCE \slv_reg7_reg[11] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [11]),
        .Q(slv_reg7[11]));
  FDCE \slv_reg7_reg[12] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [12]),
        .Q(slv_reg7[12]));
  FDCE \slv_reg7_reg[13] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [13]),
        .Q(slv_reg7[13]));
  FDCE \slv_reg7_reg[14] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [14]),
        .Q(slv_reg7[14]));
  FDCE \slv_reg7_reg[15] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [15]),
        .Q(slv_reg7[15]));
  FDCE \slv_reg7_reg[16] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [16]),
        .Q(slv_reg7[16]));
  FDCE \slv_reg7_reg[17] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [17]),
        .Q(slv_reg7[17]));
  FDCE \slv_reg7_reg[18] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [18]),
        .Q(slv_reg7[18]));
  FDCE \slv_reg7_reg[19] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [19]),
        .Q(slv_reg7[19]));
  FDCE \slv_reg7_reg[1] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [1]),
        .Q(slv_reg7[1]));
  FDCE \slv_reg7_reg[20] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [20]),
        .Q(slv_reg7[20]));
  FDCE \slv_reg7_reg[21] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [21]),
        .Q(slv_reg7[21]));
  FDCE \slv_reg7_reg[22] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [22]),
        .Q(slv_reg7[22]));
  FDCE \slv_reg7_reg[23] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [23]),
        .Q(slv_reg7[23]));
  FDCE \slv_reg7_reg[24] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [24]),
        .Q(slv_reg7[24]));
  FDCE \slv_reg7_reg[25] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [25]),
        .Q(slv_reg7[25]));
  FDCE \slv_reg7_reg[26] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [26]),
        .Q(slv_reg7[26]));
  FDCE \slv_reg7_reg[27] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [27]),
        .Q(slv_reg7[27]));
  FDCE \slv_reg7_reg[28] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [28]),
        .Q(slv_reg7[28]));
  FDCE \slv_reg7_reg[29] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [29]),
        .Q(slv_reg7[29]));
  FDCE \slv_reg7_reg[2] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [2]),
        .Q(slv_reg7[2]));
  FDCE \slv_reg7_reg[30] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [30]),
        .Q(slv_reg7[30]));
  FDCE \slv_reg7_reg[31] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [31]),
        .Q(slv_reg7[31]));
  FDCE \slv_reg7_reg[3] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [3]),
        .Q(slv_reg7[3]));
  FDCE \slv_reg7_reg[4] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [4]),
        .Q(slv_reg7[4]));
  FDCE \slv_reg7_reg[5] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [5]),
        .Q(slv_reg7[5]));
  FDCE \slv_reg7_reg[6] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [6]),
        .Q(slv_reg7[6]));
  FDCE \slv_reg7_reg[7] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [7]),
        .Q(slv_reg7[7]));
  FDCE \slv_reg7_reg[8] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [8]),
        .Q(slv_reg7[8]));
  FDCE \slv_reg7_reg[9] 
       (.C(clk),
        .CE(tc),
        .CLR(p_0_in),
        .D(\counter_inst_C/tmp_reg [9]),
        .Q(slv_reg7[9]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_timers timers_inst
       (.CO(tc),
        .D(\counter_inst_B/tmp_reg ),
        .\FSM_onehot_state_reg[1] (\FSM_onehot_state_reg[1] ),
        .Q({clra,clrb,clrc,cea,ceb,cec,a_mode,b_mode,c_mode,\slv_reg5_reg_n_0_[1] ,\slv_reg5_reg_n_0_[0] }),
        .\axi_araddr_reg[4] (reg_data_out),
        .\axi_rdata_reg[0] (\axi_rdata[0]_i_2_n_0 ),
        .\axi_rdata_reg[10] (\axi_rdata[10]_i_3_n_0 ),
        .\axi_rdata_reg[11] (\axi_rdata[11]_i_3_n_0 ),
        .\axi_rdata_reg[12] (\axi_rdata[12]_i_3_n_0 ),
        .\axi_rdata_reg[13] (\axi_rdata[13]_i_3_n_0 ),
        .\axi_rdata_reg[14] (\axi_rdata[14]_i_3_n_0 ),
        .\axi_rdata_reg[15] (\axi_rdata[15]_i_3_n_0 ),
        .\axi_rdata_reg[16] (\axi_rdata[16]_i_3_n_0 ),
        .\axi_rdata_reg[17] (\axi_rdata[17]_i_3_n_0 ),
        .\axi_rdata_reg[18] (\axi_rdata[18]_i_3_n_0 ),
        .\axi_rdata_reg[19] (\axi_rdata[19]_i_3_n_0 ),
        .\axi_rdata_reg[1] (\axi_rdata[1]_i_3_n_0 ),
        .\axi_rdata_reg[20] (\axi_rdata[20]_i_3_n_0 ),
        .\axi_rdata_reg[21] (\axi_rdata[21]_i_3_n_0 ),
        .\axi_rdata_reg[22] (\axi_rdata[22]_i_3_n_0 ),
        .\axi_rdata_reg[23] (\axi_rdata[23]_i_3_n_0 ),
        .\axi_rdata_reg[24] (\axi_rdata[24]_i_3_n_0 ),
        .\axi_rdata_reg[25] (\axi_rdata[25]_i_3_n_0 ),
        .\axi_rdata_reg[26] (\axi_rdata[26]_i_3_n_0 ),
        .\axi_rdata_reg[27] (\axi_rdata[27]_i_3_n_0 ),
        .\axi_rdata_reg[28] (\axi_rdata[28]_i_3_n_0 ),
        .\axi_rdata_reg[29] (\axi_rdata[29]_i_3_n_0 ),
        .\axi_rdata_reg[2] (\axi_rdata[2]_i_3_n_0 ),
        .\axi_rdata_reg[30] (\axi_rdata[30]_i_3_n_0 ),
        .\axi_rdata_reg[31] (\axi_rdata[31]_i_4_n_0 ),
        .\axi_rdata_reg[3] (\axi_rdata[3]_i_3_n_0 ),
        .\axi_rdata_reg[4] (\axi_rdata[4]_i_3_n_0 ),
        .\axi_rdata_reg[5] (\axi_rdata[5]_i_3_n_0 ),
        .\axi_rdata_reg[6] (\axi_rdata[6]_i_3_n_0 ),
        .\axi_rdata_reg[7] (\axi_rdata[7]_i_3_n_0 ),
        .\axi_rdata_reg[8] (\axi_rdata[8]_i_3_n_0 ),
        .\axi_rdata_reg[9] (\axi_rdata[9]_i_3_n_0 ),
        .bram_seg(bram_seg),
        .clk(clk),
        .lt_dir(lt_dir),
        .sel0(sel0),
        .\slv_reg7_reg[0] (slv_reg4),
        .tmp_reg(\counter_inst_C/tmp_reg ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_t_ff
   (bram_seg,
    en,
    temp_reg_0,
    temp_reg_1,
    auto_clr,
    Q);
  output bram_seg;
  input en;
  input temp_reg_0;
  input temp_reg_1;
  input auto_clr;
  input [0:0]Q;

  wire [0:0]Q;
  wire auto_clr;
  wire bram_seg;
  wire en;
  wire temp_i_1_n_0;
  wire temp_reg_0;
  wire temp_reg_1;

  LUT5 #(
    .INIT(32'h0BF40000)) 
    temp_i_1
       (.I0(temp_reg_0),
        .I1(temp_reg_1),
        .I2(auto_clr),
        .I3(bram_seg),
        .I4(Q),
        .O(temp_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    temp_reg
       (.C(en),
        .CE(1'b1),
        .D(temp_i_1_n_0),
        .Q(bram_seg),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_timers
   (tmp_reg,
    D,
    CO,
    bram_seg,
    \FSM_onehot_state_reg[1] ,
    \axi_araddr_reg[4] ,
    clk,
    lt_dir,
    Q,
    \axi_rdata_reg[0] ,
    sel0,
    \slv_reg7_reg[0] ,
    \axi_rdata_reg[1] ,
    \axi_rdata_reg[2] ,
    \axi_rdata_reg[3] ,
    \axi_rdata_reg[4] ,
    \axi_rdata_reg[5] ,
    \axi_rdata_reg[6] ,
    \axi_rdata_reg[7] ,
    \axi_rdata_reg[8] ,
    \axi_rdata_reg[9] ,
    \axi_rdata_reg[10] ,
    \axi_rdata_reg[11] ,
    \axi_rdata_reg[12] ,
    \axi_rdata_reg[13] ,
    \axi_rdata_reg[14] ,
    \axi_rdata_reg[15] ,
    \axi_rdata_reg[16] ,
    \axi_rdata_reg[17] ,
    \axi_rdata_reg[18] ,
    \axi_rdata_reg[19] ,
    \axi_rdata_reg[20] ,
    \axi_rdata_reg[21] ,
    \axi_rdata_reg[22] ,
    \axi_rdata_reg[23] ,
    \axi_rdata_reg[24] ,
    \axi_rdata_reg[25] ,
    \axi_rdata_reg[26] ,
    \axi_rdata_reg[27] ,
    \axi_rdata_reg[28] ,
    \axi_rdata_reg[29] ,
    \axi_rdata_reg[30] ,
    \axi_rdata_reg[31] );
  output [31:0]tmp_reg;
  output [31:0]D;
  output [0:0]CO;
  output bram_seg;
  output \FSM_onehot_state_reg[1] ;
  output [31:0]\axi_araddr_reg[4] ;
  input clk;
  input [1:0]lt_dir;
  input [10:0]Q;
  input \axi_rdata_reg[0] ;
  input [2:0]sel0;
  input [31:0]\slv_reg7_reg[0] ;
  input \axi_rdata_reg[1] ;
  input \axi_rdata_reg[2] ;
  input \axi_rdata_reg[3] ;
  input \axi_rdata_reg[4] ;
  input \axi_rdata_reg[5] ;
  input \axi_rdata_reg[6] ;
  input \axi_rdata_reg[7] ;
  input \axi_rdata_reg[8] ;
  input \axi_rdata_reg[9] ;
  input \axi_rdata_reg[10] ;
  input \axi_rdata_reg[11] ;
  input \axi_rdata_reg[12] ;
  input \axi_rdata_reg[13] ;
  input \axi_rdata_reg[14] ;
  input \axi_rdata_reg[15] ;
  input \axi_rdata_reg[16] ;
  input \axi_rdata_reg[17] ;
  input \axi_rdata_reg[18] ;
  input \axi_rdata_reg[19] ;
  input \axi_rdata_reg[20] ;
  input \axi_rdata_reg[21] ;
  input \axi_rdata_reg[22] ;
  input \axi_rdata_reg[23] ;
  input \axi_rdata_reg[24] ;
  input \axi_rdata_reg[25] ;
  input \axi_rdata_reg[26] ;
  input \axi_rdata_reg[27] ;
  input \axi_rdata_reg[28] ;
  input \axi_rdata_reg[29] ;
  input \axi_rdata_reg[30] ;
  input \axi_rdata_reg[31] ;

  wire [0:0]CO;
  wire [31:0]D;
  wire \FSM_onehot_state_reg[1] ;
  wire [10:0]Q;
  wire [31:0]\axi_araddr_reg[4] ;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[10] ;
  wire \axi_rdata_reg[11] ;
  wire \axi_rdata_reg[12] ;
  wire \axi_rdata_reg[13] ;
  wire \axi_rdata_reg[14] ;
  wire \axi_rdata_reg[15] ;
  wire \axi_rdata_reg[16] ;
  wire \axi_rdata_reg[17] ;
  wire \axi_rdata_reg[18] ;
  wire \axi_rdata_reg[19] ;
  wire \axi_rdata_reg[1] ;
  wire \axi_rdata_reg[20] ;
  wire \axi_rdata_reg[21] ;
  wire \axi_rdata_reg[22] ;
  wire \axi_rdata_reg[23] ;
  wire \axi_rdata_reg[24] ;
  wire \axi_rdata_reg[25] ;
  wire \axi_rdata_reg[26] ;
  wire \axi_rdata_reg[27] ;
  wire \axi_rdata_reg[28] ;
  wire \axi_rdata_reg[29] ;
  wire \axi_rdata_reg[2] ;
  wire \axi_rdata_reg[30] ;
  wire \axi_rdata_reg[31] ;
  wire \axi_rdata_reg[3] ;
  wire \axi_rdata_reg[4] ;
  wire \axi_rdata_reg[5] ;
  wire \axi_rdata_reg[6] ;
  wire \axi_rdata_reg[7] ;
  wire \axi_rdata_reg[8] ;
  wire \axi_rdata_reg[9] ;
  wire bram_seg;
  wire ce;
  wire clk;
  wire clk_1kHz_en;
  wire clk_1kHz_enb;
  wire clk_1kHz_enc;
  wire clk_lt_enable_inst_n_1;
  wire clk_lt_enable_inst_n_2;
  wire clk_lt_enable_inst_n_3;
  wire clr0_out;
  wire clr1_out;
  wire counter_inst_C_n_32;
  wire counter_inst_C_n_33;
  wire counter_inst_C_n_34;
  wire counter_inst_C_n_35;
  wire counter_inst_C_n_36;
  wire counter_inst_C_n_37;
  wire counter_inst_C_n_38;
  wire counter_inst_C_n_39;
  wire counter_inst_C_n_40;
  wire counter_inst_C_n_41;
  wire counter_inst_C_n_42;
  wire counter_inst_C_n_43;
  wire counter_inst_C_n_44;
  wire counter_inst_C_n_45;
  wire counter_inst_C_n_46;
  wire counter_inst_C_n_47;
  wire counter_inst_C_n_48;
  wire counter_inst_C_n_49;
  wire counter_inst_C_n_50;
  wire counter_inst_C_n_51;
  wire counter_inst_C_n_52;
  wire counter_inst_C_n_53;
  wire counter_inst_C_n_54;
  wire counter_inst_C_n_55;
  wire counter_inst_C_n_56;
  wire counter_inst_C_n_57;
  wire counter_inst_C_n_58;
  wire counter_inst_C_n_59;
  wire counter_inst_C_n_60;
  wire counter_inst_C_n_61;
  wire counter_inst_C_n_62;
  wire counter_inst_C_n_63;
  wire eqOp;
  wire eqOp_0;
  wire eqOp_1;
  wire fsm_auto_mode_inst_n_1;
  wire fsm_auto_mode_inst_n_2;
  wire fsm_auto_mode_inst_n_4;
  wire fsm_auto_mode_inst_n_5;
  wire fsm_auto_mode_inst_n_6;
  wire fsm_auto_mode_inst_n_8;
  wire geqOp;
  wire [1:0]lt_dir;
  wire [2:0]sel0;
  wire [31:0]\slv_reg7_reg[0] ;
  wire [31:0]tmp_reg;
  wire [2:1]tmp_reg_2;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_lt_enable clk_lt_enable_inst
       (.Q(Q[7:2]),
        .ce(ce),
        .clk(clk),
        .clk_1kHz_en(clk_1kHz_en),
        .clk_1kHz_enb(clk_1kHz_enb),
        .clk_1kHz_enc(clk_1kHz_enc),
        .geqOp(geqOp),
        .lt_clk_en_i_reg_0(clk_lt_enable_inst_n_2),
        .lt_clk_en_i_reg_1(clk_lt_enable_inst_n_3),
        .lt_clk_en_i_reg_2(\FSM_onehot_state_reg[1] ),
        .lt_dir(lt_dir),
        .\slv_reg5_reg[3] (clk_lt_enable_inst_n_1),
        .\tmp_reg[16]_0 (fsm_auto_mode_inst_n_1),
        .\tmp_reg[16]_1 (fsm_auto_mode_inst_n_6));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clock_1KHz_enable clock_1KHz_enable_inst_A
       (.clk(clk),
        .clk_1kHz_en(clk_1kHz_en),
        .clr1_out(clr1_out),
        .eqOp(eqOp),
        .\tmp_reg[15]_0 (fsm_auto_mode_inst_n_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clock_1KHz_enable_0 clock_1KHz_enable_inst_B
       (.clk(clk),
        .clk_1kHz_enb(clk_1kHz_enb),
        .eqOp(eqOp_0),
        .\tmp_reg[0]_0 (fsm_auto_mode_inst_n_8),
        .\tmp_reg[15]_0 (fsm_auto_mode_inst_n_4));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clock_1KHz_enable_1 clock_1KHz_enable_inst_C
       (.clk(clk),
        .clk_1kHz_enc(clk_1kHz_enc),
        .clr0_out(clr0_out),
        .eqOp(eqOp_1),
        .\tmp_reg[15]_0 (fsm_auto_mode_inst_n_5));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_comparator comparator_inst
       (.CO(CO),
        .DI({counter_inst_C_n_32,counter_inst_C_n_33,counter_inst_C_n_34,counter_inst_C_n_35}),
        .S({counter_inst_C_n_36,counter_inst_C_n_37,counter_inst_C_n_38,counter_inst_C_n_39}),
        .cmp0_carry__1_0({counter_inst_C_n_40,counter_inst_C_n_41,counter_inst_C_n_42,counter_inst_C_n_43}),
        .cmp0_carry__1_1({counter_inst_C_n_44,counter_inst_C_n_45,counter_inst_C_n_46,counter_inst_C_n_47}),
        .\slv_reg7_reg[0] ({counter_inst_C_n_48,counter_inst_C_n_49,counter_inst_C_n_50,counter_inst_C_n_51}),
        .\slv_reg7_reg[0]_0 ({counter_inst_C_n_52,counter_inst_C_n_53,counter_inst_C_n_54,counter_inst_C_n_55}),
        .\slv_reg7_reg[0]_1 ({counter_inst_C_n_56,counter_inst_C_n_57,counter_inst_C_n_58,counter_inst_C_n_59}),
        .\slv_reg7_reg[0]_2 ({counter_inst_C_n_60,counter_inst_C_n_61,counter_inst_C_n_62,counter_inst_C_n_63}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter counter_inst_A
       (.D({D[31:3],D[0]}),
        .S(tmp_reg_2),
        .\axi_araddr_reg[4] ({\axi_araddr_reg[4] [31:3],\axi_araddr_reg[4] [0]}),
        .\axi_rdata_reg[0] (\axi_rdata_reg[0] ),
        .\axi_rdata_reg[0]_0 (\FSM_onehot_state_reg[1] ),
        .\axi_rdata_reg[0]_1 (tmp_reg[0]),
        .\axi_rdata_reg[10] (\axi_rdata_reg[10] ),
        .\axi_rdata_reg[11] (tmp_reg[11:8]),
        .\axi_rdata_reg[11]_0 (\axi_rdata_reg[11] ),
        .\axi_rdata_reg[12] (\axi_rdata_reg[12] ),
        .\axi_rdata_reg[13] (\axi_rdata_reg[13] ),
        .\axi_rdata_reg[14] (\axi_rdata_reg[14] ),
        .\axi_rdata_reg[15] (tmp_reg[15:12]),
        .\axi_rdata_reg[15]_0 (\axi_rdata_reg[15] ),
        .\axi_rdata_reg[16] (\axi_rdata_reg[16] ),
        .\axi_rdata_reg[17] (\axi_rdata_reg[17] ),
        .\axi_rdata_reg[18] (\axi_rdata_reg[18] ),
        .\axi_rdata_reg[19] (tmp_reg[19:16]),
        .\axi_rdata_reg[19]_0 (\axi_rdata_reg[19] ),
        .\axi_rdata_reg[20] (\axi_rdata_reg[20] ),
        .\axi_rdata_reg[21] (\axi_rdata_reg[21] ),
        .\axi_rdata_reg[22] (\axi_rdata_reg[22] ),
        .\axi_rdata_reg[23] (tmp_reg[23:20]),
        .\axi_rdata_reg[23]_0 (\axi_rdata_reg[23] ),
        .\axi_rdata_reg[24] (\axi_rdata_reg[24] ),
        .\axi_rdata_reg[25] (\axi_rdata_reg[25] ),
        .\axi_rdata_reg[26] (\axi_rdata_reg[26] ),
        .\axi_rdata_reg[27] (tmp_reg[27:24]),
        .\axi_rdata_reg[27]_0 (\axi_rdata_reg[27] ),
        .\axi_rdata_reg[28] (\axi_rdata_reg[28] ),
        .\axi_rdata_reg[29] (\axi_rdata_reg[29] ),
        .\axi_rdata_reg[30] (\axi_rdata_reg[30] ),
        .\axi_rdata_reg[31] (tmp_reg[31:28]),
        .\axi_rdata_reg[31]_0 (\axi_rdata_reg[31] ),
        .\axi_rdata_reg[3] (\axi_rdata_reg[3] ),
        .\axi_rdata_reg[3]_0 (tmp_reg[3]),
        .\axi_rdata_reg[4] (\axi_rdata_reg[4] ),
        .\axi_rdata_reg[5] (\axi_rdata_reg[5] ),
        .\axi_rdata_reg[6] (\axi_rdata_reg[6] ),
        .\axi_rdata_reg[7] (tmp_reg[7:4]),
        .\axi_rdata_reg[7]_0 (\axi_rdata_reg[7] ),
        .\axi_rdata_reg[8] (\axi_rdata_reg[8] ),
        .\axi_rdata_reg[9] (\axi_rdata_reg[9] ),
        .clk(clk),
        .clr1_out(clr1_out),
        .sel0(sel0),
        .\tmp_reg[31]_0 (clk_lt_enable_inst_n_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter_2 counter_inst_B
       (.D(D),
        .S(tmp_reg_2[2]),
        .\axi_araddr_reg[4] (\axi_araddr_reg[4] [2]),
        .\axi_rdata_reg[2] (\axi_rdata_reg[2] ),
        .\axi_rdata_reg[2]_0 (tmp_reg[2]),
        .bram_seg(bram_seg),
        .clk(clk),
        .sel0(sel0),
        .\tmp_reg[31]_0 (clk_lt_enable_inst_n_3),
        .\tmp_reg[31]_1 (fsm_auto_mode_inst_n_8));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter_3 counter_inst_C
       (.CO(CO),
        .D(D[1]),
        .DI({counter_inst_C_n_32,counter_inst_C_n_33,counter_inst_C_n_34,counter_inst_C_n_35}),
        .S(tmp_reg[3:1]),
        .\axi_araddr_reg[4] (\axi_araddr_reg[4] [1]),
        .\axi_rdata_reg[1] (\axi_rdata_reg[1] ),
        .\axi_rdata_reg[1]_0 (tmp_reg_2[1]),
        .ce(ce),
        .clk(clk),
        .clr0_out(clr0_out),
        .sel0(sel0),
        .\slv_reg4_reg[15] ({counter_inst_C_n_44,counter_inst_C_n_45,counter_inst_C_n_46,counter_inst_C_n_47}),
        .\slv_reg4_reg[23] ({counter_inst_C_n_52,counter_inst_C_n_53,counter_inst_C_n_54,counter_inst_C_n_55}),
        .\slv_reg4_reg[31] ({counter_inst_C_n_60,counter_inst_C_n_61,counter_inst_C_n_62,counter_inst_C_n_63}),
        .\slv_reg4_reg[7] ({counter_inst_C_n_36,counter_inst_C_n_37,counter_inst_C_n_38,counter_inst_C_n_39}),
        .\slv_reg7_reg[0] (\slv_reg7_reg[0] ),
        .\tmp_reg[0]_0 (tmp_reg[0]),
        .\tmp_reg[11]_0 (tmp_reg[11:8]),
        .\tmp_reg[14]_0 ({counter_inst_C_n_40,counter_inst_C_n_41,counter_inst_C_n_42,counter_inst_C_n_43}),
        .\tmp_reg[15]_0 (tmp_reg[15:12]),
        .\tmp_reg[19]_0 (tmp_reg[19:16]),
        .\tmp_reg[22]_0 ({counter_inst_C_n_48,counter_inst_C_n_49,counter_inst_C_n_50,counter_inst_C_n_51}),
        .\tmp_reg[23]_0 (tmp_reg[23:20]),
        .\tmp_reg[27]_0 (tmp_reg[27:24]),
        .\tmp_reg[30]_0 ({counter_inst_C_n_56,counter_inst_C_n_57,counter_inst_C_n_58,counter_inst_C_n_59}),
        .\tmp_reg[31]_0 (tmp_reg[31:28]),
        .\tmp_reg[7]_0 (tmp_reg[7:4]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fsm_auto_mode fsm_auto_mode_inst
       (.CO(CO),
        .\FSM_onehot_state_reg[1]_0 (\FSM_onehot_state_reg[1] ),
        .Q(Q),
        .bram_seg(bram_seg),
        .clk(clk),
        .clr0_out(clr0_out),
        .clr1_out(clr1_out),
        .eqOp(eqOp),
        .eqOp_0(eqOp_0),
        .eqOp_1(eqOp_1),
        .geqOp(geqOp),
        .lt_dir(lt_dir),
        .lt_dir_0_sp_1(fsm_auto_mode_inst_n_1),
        .\slv_reg5_reg[3] (fsm_auto_mode_inst_n_6),
        .\slv_reg5_reg[5] (fsm_auto_mode_inst_n_5),
        .\slv_reg5_reg[6] (fsm_auto_mode_inst_n_4),
        .\slv_reg5_reg[7] (fsm_auto_mode_inst_n_2),
        .\slv_reg5_reg[9] (fsm_auto_mode_inst_n_8),
        .\tmp_reg[16] (clk_lt_enable_inst_n_1));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
