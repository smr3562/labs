-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 17:57:59 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_dbg_pha_0_0_stub.vhdl
-- Design      : design_1_ip_dbg_pha_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    peak_amp_rdy_fast : in STD_LOGIC;
    rejectn : in STD_LOGIC;
    peak_amp_rdy_slow : in STD_LOGIC;
    peak_det_signal : in STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy_fast_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rejectn_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy_slow_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_det_signal_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "peak_amp_rdy_fast,rejectn,peak_amp_rdy_slow,peak_det_signal[15:0],peak_amp_rdy_fast_out[15:0],rejectn_out[15:0],peak_amp_rdy_slow_out[15:0],peak_det_signal_out[15:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "top,Vivado 2019.1";
begin
end;
