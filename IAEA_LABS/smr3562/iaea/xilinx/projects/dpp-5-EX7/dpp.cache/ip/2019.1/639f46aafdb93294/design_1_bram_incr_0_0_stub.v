// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 17:57:52 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_bram_incr_0_0_stub.v
// Design      : design_1_bram_incr_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "bram_incr,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(bram_addr, bram_clk, bram_en, bram_dout, 
  bram_rst, bram_we, bram_din, peak_amp, peak_amp_rdy, rstn, clk, segment)
/* synthesis syn_black_box black_box_pad_pin="bram_addr[13:0],bram_clk,bram_en,bram_dout[31:0],bram_rst,bram_we,bram_din[31:0],peak_amp[15:0],peak_amp_rdy,rstn,clk,segment" */;
  output [13:0]bram_addr;
  output bram_clk;
  output bram_en;
  input [31:0]bram_dout;
  output bram_rst;
  output bram_we;
  output [31:0]bram_din;
  input [15:0]peak_amp;
  input peak_amp_rdy;
  input rstn;
  input clk;
  input segment;
endmodule
