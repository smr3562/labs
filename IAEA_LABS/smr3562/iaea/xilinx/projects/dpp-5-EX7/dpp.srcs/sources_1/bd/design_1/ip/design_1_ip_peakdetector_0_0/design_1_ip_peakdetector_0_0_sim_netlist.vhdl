-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 18:31:24 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_peakdetector_0_0/design_1_ip_peakdetector_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_peakdetector_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface_verilog is
  port (
    axi_awready_reg_0 : out STD_LOGIC;
    axi_wready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[2][15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg_array_reg[3][15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg_array_reg[4][9]_0\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_awvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface_verilog : entity is "axibusdomain_axi_lite_interface_verilog";
end design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface_verilog;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface_verilog is
  signal axi_araddr : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^d\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dec_r__10\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \dec_w__10\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^i\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal r4_x_delay : STD_LOGIC_VECTOR ( 31 downto 10 );
  signal r5_x_noise : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r6_x_min : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r7_x_max : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r8_en_pkd : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg_array[0][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][14]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][3]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array_reg[0]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \slv_reg_array_reg[1]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \slv_reg_array_reg[2]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \^slv_reg_array_reg[2][15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg_array_reg[3]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \^slv_reg_array_reg[3][15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg_array_reg[4]\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \^slv_reg_array_reg[4][9]_0\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal slv_wire_array : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[10]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[12]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[13]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[14]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[16]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[17]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[18]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \axi_rdata[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[20]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[21]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \axi_rdata[22]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[23]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \axi_rdata[24]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \axi_rdata[25]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \axi_rdata[26]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \axi_rdata[28]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \axi_rdata[29]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \axi_rdata[2]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[30]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[5]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[6]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[7]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \axi_rdata[9]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[0][7]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[1][14]_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[1][14]_i_4\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[2][7]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[4][3]_i_2\ : label is "soft_lutpair0";
begin
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  d(0) <= \^d\(0);
  i(15 downto 0) <= \^i\(15 downto 0);
  \slv_reg_array_reg[2][15]_0\(15 downto 0) <= \^slv_reg_array_reg[2][15]_0\(15 downto 0);
  \slv_reg_array_reg[3][15]_0\(15 downto 0) <= \^slv_reg_array_reg[3][15]_0\(15 downto 0);
  \slv_reg_array_reg[4][9]_0\(9 downto 0) <= \^slv_reg_array_reg[4][9]_0\(9 downto 0);
\axi_araddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(0),
      Q => axi_araddr(0),
      R => p_0_in
    );
\axi_araddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(1),
      Q => axi_araddr(1),
      R => p_0_in
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(2),
      Q => axi_araddr(2),
      R => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(3),
      Q => axi_araddr(3),
      R => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(4),
      Q => axi_araddr(4),
      R => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => p_0_in
    );
\axi_awaddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(0),
      Q => axi_awaddr(0),
      R => p_0_in
    );
\axi_awaddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(1),
      Q => axi_awaddr(1),
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(2),
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(3),
      Q => axi_awaddr(3),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(4),
      Q => axi_awaddr(4),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axibusdomain_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_awvalid,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => p_0_in
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => axibusdomain_s_axi_bvalid,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(0),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[0]_i_2_n_0\,
      O => slv_wire_array(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(0),
      I1 => \^slv_reg_array_reg[2][15]_0\(0),
      I2 => \dec_r__10\(1),
      I3 => \^i\(0),
      I4 => \dec_r__10\(0),
      I5 => \^d\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(10),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[10]_i_2_n_0\,
      O => slv_wire_array(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(10),
      I1 => \^slv_reg_array_reg[2][15]_0\(10),
      I2 => \dec_r__10\(1),
      I3 => \^i\(10),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(11),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[11]_i_2_n_0\,
      O => slv_wire_array(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(11),
      I1 => \^slv_reg_array_reg[2][15]_0\(11),
      I2 => \dec_r__10\(1),
      I3 => \^i\(11),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(12),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[12]_i_2_n_0\,
      O => slv_wire_array(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(12),
      I1 => \^slv_reg_array_reg[2][15]_0\(12),
      I2 => \dec_r__10\(1),
      I3 => \^i\(12),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(13),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[13]_i_2_n_0\,
      O => slv_wire_array(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(13),
      I1 => \^slv_reg_array_reg[2][15]_0\(13),
      I2 => \dec_r__10\(1),
      I3 => \^i\(13),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(14),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[14]_i_2_n_0\,
      O => slv_wire_array(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(14),
      I1 => \^slv_reg_array_reg[2][15]_0\(14),
      I2 => \dec_r__10\(1),
      I3 => \^i\(14),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(15),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[15]_i_2_n_0\,
      O => slv_wire_array(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(15),
      I1 => \^slv_reg_array_reg[2][15]_0\(15),
      I2 => \dec_r__10\(1),
      I3 => \^i\(15),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(16),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[16]_i_2_n_0\,
      O => slv_wire_array(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(16),
      I1 => r6_x_min(16),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(16),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(17),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[17]_i_2_n_0\,
      O => slv_wire_array(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(17),
      I1 => r6_x_min(17),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(17),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(18),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[18]_i_2_n_0\,
      O => slv_wire_array(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(18),
      I1 => r6_x_min(18),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(18),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(19),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[19]_i_2_n_0\,
      O => slv_wire_array(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(19),
      I1 => r6_x_min(19),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(19),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(1),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[1]_i_2_n_0\,
      O => slv_wire_array(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(1),
      I1 => \^slv_reg_array_reg[2][15]_0\(1),
      I2 => \dec_r__10\(1),
      I3 => \^i\(1),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(20),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[20]_i_2_n_0\,
      O => slv_wire_array(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(20),
      I1 => r6_x_min(20),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(20),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(21),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[21]_i_2_n_0\,
      O => slv_wire_array(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(21),
      I1 => r6_x_min(21),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(21),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(22),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[22]_i_2_n_0\,
      O => slv_wire_array(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(22),
      I1 => r6_x_min(22),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(22),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(23),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[23]_i_2_n_0\,
      O => slv_wire_array(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(23),
      I1 => r6_x_min(23),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(23),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(24),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[24]_i_2_n_0\,
      O => slv_wire_array(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(24),
      I1 => r6_x_min(24),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(24),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(25),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[25]_i_2_n_0\,
      O => slv_wire_array(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(25),
      I1 => r6_x_min(25),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(25),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(26),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[26]_i_2_n_0\,
      O => slv_wire_array(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(26),
      I1 => r6_x_min(26),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(26),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(27),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[27]_i_2_n_0\,
      O => slv_wire_array(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(27),
      I1 => r6_x_min(27),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(27),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(28),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[28]_i_2_n_0\,
      O => slv_wire_array(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(28),
      I1 => r6_x_min(28),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(28),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(29),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[29]_i_2_n_0\,
      O => slv_wire_array(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(29),
      I1 => r6_x_min(29),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(29),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(2),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[2]_i_2_n_0\,
      O => slv_wire_array(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(2),
      I1 => \^slv_reg_array_reg[2][15]_0\(2),
      I2 => \dec_r__10\(1),
      I3 => \^i\(2),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(30),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[30]_i_2_n_0\,
      O => slv_wire_array(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(30),
      I1 => r6_x_min(30),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(30),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => r4_x_delay(31),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[31]_i_3_n_0\,
      O => slv_wire_array(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => axi_araddr(4),
      I1 => axi_araddr(2),
      I2 => axi_araddr(1),
      I3 => axi_araddr(3),
      I4 => axi_araddr(0),
      O => \dec_r__10\(2)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r5_x_noise(31),
      I1 => r6_x_min(31),
      I2 => \dec_r__10\(1),
      I3 => r7_x_max(31),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(4),
      I1 => axi_araddr(0),
      I2 => axi_araddr(3),
      I3 => axi_araddr(1),
      O => \dec_r__10\(1)
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(4),
      I1 => axi_araddr(0),
      I2 => axi_araddr(2),
      I3 => axi_araddr(1),
      O => \dec_r__10\(0)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(3),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[3]_i_2_n_0\,
      O => slv_wire_array(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(3),
      I1 => \^slv_reg_array_reg[2][15]_0\(3),
      I2 => \dec_r__10\(1),
      I3 => \^i\(3),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(4),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[4]_i_2_n_0\,
      O => slv_wire_array(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(4),
      I1 => \^slv_reg_array_reg[2][15]_0\(4),
      I2 => \dec_r__10\(1),
      I3 => \^i\(4),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(5),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[5]_i_2_n_0\,
      O => slv_wire_array(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(5),
      I1 => \^slv_reg_array_reg[2][15]_0\(5),
      I2 => \dec_r__10\(1),
      I3 => \^i\(5),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(6),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[6]_i_2_n_0\,
      O => slv_wire_array(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(6),
      I1 => \^slv_reg_array_reg[2][15]_0\(6),
      I2 => \dec_r__10\(1),
      I3 => \^i\(6),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(7),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[7]_i_2_n_0\,
      O => slv_wire_array(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(7),
      I1 => \^slv_reg_array_reg[2][15]_0\(7),
      I2 => \dec_r__10\(1),
      I3 => \^i\(7),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(8),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[8]_i_2_n_0\,
      O => slv_wire_array(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(8),
      I1 => \^slv_reg_array_reg[2][15]_0\(8),
      I2 => \dec_r__10\(1),
      I3 => \^i\(8),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg_array_reg[4][9]_0\(9),
      I1 => \dec_r__10\(2),
      I2 => \axi_rdata[9]_i_2_n_0\,
      O => slv_wire_array(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(9),
      I1 => \^slv_reg_array_reg[2][15]_0\(9),
      I2 => \dec_r__10\(1),
      I3 => \^i\(9),
      I4 => \dec_r__10\(0),
      I5 => r8_en_pkd(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(0),
      Q => axibusdomain_s_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(10),
      Q => axibusdomain_s_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(11),
      Q => axibusdomain_s_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(12),
      Q => axibusdomain_s_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(13),
      Q => axibusdomain_s_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(14),
      Q => axibusdomain_s_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(15),
      Q => axibusdomain_s_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(16),
      Q => axibusdomain_s_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(17),
      Q => axibusdomain_s_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(18),
      Q => axibusdomain_s_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(19),
      Q => axibusdomain_s_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(1),
      Q => axibusdomain_s_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(20),
      Q => axibusdomain_s_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(21),
      Q => axibusdomain_s_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(22),
      Q => axibusdomain_s_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(23),
      Q => axibusdomain_s_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(24),
      Q => axibusdomain_s_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(25),
      Q => axibusdomain_s_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(26),
      Q => axibusdomain_s_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(27),
      Q => axibusdomain_s_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(28),
      Q => axibusdomain_s_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(29),
      Q => axibusdomain_s_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(2),
      Q => axibusdomain_s_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(30),
      Q => axibusdomain_s_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(31),
      Q => axibusdomain_s_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(3),
      Q => axibusdomain_s_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(4),
      Q => axibusdomain_s_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(5),
      Q => axibusdomain_s_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(6),
      Q => axibusdomain_s_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(7),
      Q => axibusdomain_s_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(8),
      Q => axibusdomain_s_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(9),
      Q => axibusdomain_s_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^axibusdomain_s_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_awvalid,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => p_0_in
    );
\slv_reg_array[0][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFFFDFDFD00FDFD"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      I2 => axibusdomain_s_axi_wdata(0),
      I3 => \slv_reg_array[0][0]_i_2_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^d\(0),
      O => \slv_reg_array[0][0]_i_1_n_0\
    );
\slv_reg_array[0][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[0]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[0][0]_i_2_n_0\
    );
\slv_reg_array[0][15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[0]\(7),
      I3 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[0]\(7),
      O => \slv_reg_array[0][15]_i_2_n_0\
    );
\slv_reg_array[0][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[0]\(7),
      I3 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[0]\(7),
      O => \slv_reg_array[0][23]_i_2_n_0\
    );
\slv_reg_array[0][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[0]\(7),
      I3 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[0]\(7),
      O => \slv_reg_array[0][31]_i_2_n_0\
    );
\slv_reg_array[0][31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_3_n_0\
    );
\slv_reg_array[0][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE1FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[0]\(7)
    );
\slv_reg_array[0][31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAABBBE"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => axi_awaddr(1),
      I5 => axi_awaddr(0),
      O => \slv_reg_array[0][31]_i_5_n_0\
    );
\slv_reg_array[0][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      I2 => \slv_reg_array[0][7]_i_2_n_0\,
      O => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[0]\(7),
      O => \slv_reg_array[0][7]_i_2_n_0\
    );
\slv_reg_array[0][7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      O => \slv_reg_array[0][7]_i_3_n_0\
    );
\slv_reg_array[1][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FF808080008080"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(0),
      I3 => \slv_reg_array[1][0]_i_2_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^i\(0),
      O => \slv_reg_array[1][0]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[1]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[1][0]_i_2_n_0\
    );
\slv_reg_array[1][14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000FB0000"
    )
        port map (
      I0 => \dec_w__10\(2),
      I1 => \dec_w__10\(0),
      I2 => \dec_w__10\(1),
      I3 => axibusdomain_aresetn,
      I4 => axibusdomain_s_axi_wstrb(1),
      I5 => \slv_reg_array[0][31]_i_3_n_0\,
      O => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array[1][14]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[1]\(7),
      O => \slv_reg_array[1][14]_i_2_n_0\
    );
\slv_reg_array[1][14]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => axi_awaddr(0),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(2),
      I3 => axi_awaddr(3),
      I4 => axi_awaddr(4),
      O => \dec_w__10\(2)
    );
\slv_reg_array[1][14]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(2),
      O => \dec_w__10\(0)
    );
\slv_reg_array[1][14]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(3),
      O => \dec_w__10\(1)
    );
\slv_reg_array[1][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBFBFBF40000000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[1]\(7),
      I3 => axibusdomain_s_axi_wdata(15),
      I4 => \slv_reg_array[1][31]_i_4_n_0\,
      I5 => \^i\(15),
      O => \slv_reg_array[1][15]_i_1_n_0\
    );
\slv_reg_array[1][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80FF8000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(1),
      I3 => \slv_reg_array[1][6]_i_2_n_0\,
      I4 => \^i\(1),
      O => \slv_reg_array[1][1]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[1]\(7),
      I3 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[1]\(7),
      O => \slv_reg_array[1][23]_i_2_n_0\
    );
\slv_reg_array[1][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[1]\(7),
      I3 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[1]\(7),
      O => \slv_reg_array[1][31]_i_2_n_0\
    );
\slv_reg_array[1][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000010FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(0),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(2),
      I3 => axi_awaddr(3),
      I4 => axi_awaddr(4),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[1]\(7)
    );
\slv_reg_array[1][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(2),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][31]_i_4_n_0\
    );
\slv_reg_array[1][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80FF8000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(5),
      I3 => \slv_reg_array[1][6]_i_2_n_0\,
      I4 => \^i\(5),
      O => \slv_reg_array[1][5]_i_1_n_0\
    );
\slv_reg_array[1][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \dec_w__10\(2),
      I2 => \slv_reg_array[0][31]_i_3_n_0\,
      I3 => \dec_w__10\(1),
      I4 => \dec_w__10\(0),
      I5 => \slv_reg_array[1][6]_i_2_n_0\,
      O => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array[1][6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[1]\(7),
      O => \slv_reg_array[1][6]_i_2_n_0\
    );
\slv_reg_array[1][7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80FF8000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(7),
      I3 => \slv_reg_array[1][6]_i_2_n_0\,
      I4 => \^i\(7),
      O => \slv_reg_array[1][7]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEEE0EE"
    )
        port map (
      I0 => \slv_reg_array[2][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[2][0]_i_2_n_0\,
      I3 => axibusdomain_aresetn,
      I4 => \^slv_reg_array_reg[2][15]_0\(0),
      O => \slv_reg_array[2][0]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[2]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[2][0]_i_2_n_0\
    );
\slv_reg_array[2][15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[2]\(7),
      I3 => \slv_reg_array[2][31]_i_4_n_0\,
      O => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array[2][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[2]\(7),
      O => \slv_reg_array[2][15]_i_2_n_0\
    );
\slv_reg_array[2][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[2][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(1),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[2][15]_0\(1),
      O => \slv_reg_array[2][1]_i_1_n_0\
    );
\slv_reg_array[2][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[2]\(7),
      I3 => \slv_reg_array[2][31]_i_4_n_0\,
      O => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array[2][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[2]\(7),
      O => \slv_reg_array[2][23]_i_2_n_0\
    );
\slv_reg_array[2][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[2]\(7),
      I3 => \slv_reg_array[2][31]_i_4_n_0\,
      O => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[2]\(7),
      O => \slv_reg_array[2][31]_i_2_n_0\
    );
\slv_reg_array[2][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000002FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[2]\(7)
    );
\slv_reg_array[2][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      I5 => axi_awaddr(3),
      O => \slv_reg_array[2][31]_i_4_n_0\
    );
\slv_reg_array[2][5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[2][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(5),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[2][15]_0\(5),
      O => \slv_reg_array[2][5]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => \slv_reg_array[2][6]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[2][31]_i_4_n_0\,
      O => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[2]\(7),
      O => \slv_reg_array[2][6]_i_2_n_0\
    );
\slv_reg_array[2][7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[2][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(7),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[2][15]_0\(7),
      O => \slv_reg_array[2][7]_i_1_n_0\
    );
\slv_reg_array[2][7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFDFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \dec_w__10\(0),
      I2 => \dec_w__10\(2),
      I3 => \dec_w__10\(1),
      I4 => \slv_reg_array[0][31]_i_3_n_0\,
      O => \slv_reg_array[2][7]_i_2_n_0\
    );
\slv_reg_array[3][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEEE0EE"
    )
        port map (
      I0 => \slv_reg_array[3][6]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[3][0]_i_2_n_0\,
      I3 => axibusdomain_aresetn,
      I4 => \^slv_reg_array_reg[3][15]_0\(0),
      O => \slv_reg_array[3][0]_i_1_n_0\
    );
\slv_reg_array[3][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[3]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[3][0]_i_2_n_0\
    );
\slv_reg_array[3][15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[3]\(7),
      I3 => \slv_reg_array[3][31]_i_4_n_0\,
      O => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array[3][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[3]\(7),
      O => \slv_reg_array[3][15]_i_2_n_0\
    );
\slv_reg_array[3][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[3]\(7),
      I3 => \slv_reg_array[3][31]_i_4_n_0\,
      O => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array[3][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[3]\(7),
      O => \slv_reg_array[3][23]_i_2_n_0\
    );
\slv_reg_array[3][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[3]\(7),
      I3 => \slv_reg_array[3][31]_i_4_n_0\,
      O => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array[3][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[3]\(7),
      O => \slv_reg_array[3][31]_i_2_n_0\
    );
\slv_reg_array[3][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000008FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[3]\(7)
    );
\slv_reg_array[3][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[3][31]_i_4_n_0\
    );
\slv_reg_array[3][4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[3][6]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(4),
      I2 => \slv_reg_array[3][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(4),
      O => \slv_reg_array[3][4]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[3][6]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(6),
      I2 => \slv_reg_array[3][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(6),
      O => \slv_reg_array[3][6]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFF7F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \dec_w__10\(1),
      I2 => \dec_w__10\(0),
      I3 => \dec_w__10\(2),
      I4 => \slv_reg_array[0][31]_i_3_n_0\,
      O => \slv_reg_array[3][6]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => \slv_reg_array[3][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[3][31]_i_4_n_0\,
      O => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array[3][7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[3]\(7),
      O => \slv_reg_array[3][7]_i_2_n_0\
    );
\slv_reg_array[4][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEEE0EE"
    )
        port map (
      I0 => \slv_reg_array[4][3]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[4][0]_i_2_n_0\,
      I3 => axibusdomain_aresetn,
      I4 => \^slv_reg_array_reg[4][9]_0\(0),
      O => \slv_reg_array[4][0]_i_1_n_0\
    );
\slv_reg_array[4][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A000000030000000"
    )
        port map (
      I0 => \slv_reg_array_reg[4]\(7),
      I1 => axibusdomain_aresetn,
      I2 => \slv_reg_array[0][7]_i_3_n_0\,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_s_axi_wstrb(0),
      O => \slv_reg_array[4][0]_i_2_n_0\
    );
\slv_reg_array[4][15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[4]\(7),
      I3 => \slv_reg_array[4][31]_i_4_n_0\,
      O => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array_reg[4]\(7),
      O => \slv_reg_array[4][15]_i_2_n_0\
    );
\slv_reg_array[4][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[4][3]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(1),
      I2 => \slv_reg_array[4][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[4][9]_0\(1),
      O => \slv_reg_array[4][1]_i_1_n_0\
    );
\slv_reg_array[4][23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[4]\(7),
      I3 => \slv_reg_array[4][31]_i_4_n_0\,
      O => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array[4][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array_reg[4]\(7),
      O => \slv_reg_array[4][23]_i_2_n_0\
    );
\slv_reg_array[4][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[4][3]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(2),
      I2 => \slv_reg_array[4][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[4][9]_0\(2),
      O => \slv_reg_array[4][2]_i_1_n_0\
    );
\slv_reg_array[4][31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[4]\(7),
      I3 => \slv_reg_array[4][31]_i_4_n_0\,
      O => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array[4][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array_reg[4]\(7),
      O => \slv_reg_array[4][31]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000010FFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array_reg[4]\(7)
    );
\slv_reg_array[4][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(2),
      I2 => axi_awaddr(4),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => \slv_reg_array[0][31]_i_3_n_0\,
      O => \slv_reg_array[4][31]_i_4_n_0\
    );
\slv_reg_array[4][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => \slv_reg_array[4][3]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wdata(3),
      I2 => \slv_reg_array[4][7]_i_2_n_0\,
      I3 => \^slv_reg_array_reg[4][9]_0\(3),
      O => \slv_reg_array[4][3]_i_1_n_0\
    );
\slv_reg_array[4][3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBFF"
    )
        port map (
      I0 => \dec_w__10\(1),
      I1 => \dec_w__10\(2),
      I2 => \slv_reg_array[0][31]_i_3_n_0\,
      I3 => axibusdomain_s_axi_wstrb(0),
      I4 => \dec_w__10\(0),
      O => \slv_reg_array[4][3]_i_2_n_0\
    );
\slv_reg_array[4][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => \slv_reg_array[4][7]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[4][31]_i_4_n_0\,
      O => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array[4][7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => \slv_reg_array[0][7]_i_3_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \slv_reg_array_reg[4]\(7),
      O => \slv_reg_array[4][7]_i_2_n_0\
    );
\slv_reg_array_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[0][0]_i_1_n_0\,
      Q => \^d\(0),
      R => '0'
    );
\slv_reg_array_reg[0][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => r8_en_pkd(10),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => r8_en_pkd(11),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => r8_en_pkd(12),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => r8_en_pkd(13),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => r8_en_pkd(14),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(15),
      Q => r8_en_pkd(15),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r8_en_pkd(16),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r8_en_pkd(17),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r8_en_pkd(18),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r8_en_pkd(19),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(1),
      Q => r8_en_pkd(1),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r8_en_pkd(20),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r8_en_pkd(21),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r8_en_pkd(22),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r8_en_pkd(23),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r8_en_pkd(24),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r8_en_pkd(25),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r8_en_pkd(26),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r8_en_pkd(27),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r8_en_pkd(28),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r8_en_pkd(29),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(2),
      Q => r8_en_pkd(2),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r8_en_pkd(30),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r8_en_pkd(31),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(3),
      Q => r8_en_pkd(3),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(4),
      Q => r8_en_pkd(4),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(5),
      Q => r8_en_pkd(5),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(6),
      Q => r8_en_pkd(6),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(7),
      Q => r8_en_pkd(7),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => r8_en_pkd(8),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => r8_en_pkd(9),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[1][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][0]_i_1_n_0\,
      Q => \^i\(0),
      R => '0'
    );
\slv_reg_array_reg[1][10]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => \^i\(10),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][11]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => \^i\(11),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][12]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => \^i\(12),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][13]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => \^i\(13),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][14]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => \^i\(14),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][15]_i_1_n_0\,
      Q => \^i\(15),
      R => '0'
    );
\slv_reg_array_reg[1][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r7_x_max(16),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r7_x_max(17),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r7_x_max(18),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r7_x_max(19),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => \^i\(1),
      R => '0'
    );
\slv_reg_array_reg[1][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r7_x_max(20),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r7_x_max(21),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r7_x_max(22),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r7_x_max(23),
      R => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array_reg[1][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r7_x_max(24),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r7_x_max(25),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r7_x_max(26),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r7_x_max(27),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r7_x_max(28),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r7_x_max(29),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(2),
      Q => \^i\(2),
      S => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array_reg[1][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r7_x_max(30),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r7_x_max(31),
      R => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array_reg[1][3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(3),
      Q => \^i\(3),
      S => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array_reg[1][4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(4),
      Q => \^i\(4),
      S => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array_reg[1][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][5]_i_1_n_0\,
      Q => \^i\(5),
      R => '0'
    );
\slv_reg_array_reg[1][6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(6),
      Q => \^i\(6),
      S => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array_reg[1][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][7]_i_1_n_0\,
      Q => \^i\(7),
      R => '0'
    );
\slv_reg_array_reg[1][8]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => \^i\(8),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[1][9]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][14]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => \^i\(9),
      S => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array_reg[2][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[2][15]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[2][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => \^slv_reg_array_reg[2][15]_0\(10),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => \^slv_reg_array_reg[2][15]_0\(11),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => \^slv_reg_array_reg[2][15]_0\(12),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => \^slv_reg_array_reg[2][15]_0\(13),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => \^slv_reg_array_reg[2][15]_0\(14),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(15),
      Q => \^slv_reg_array_reg[2][15]_0\(15),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r6_x_min(16),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r6_x_min(17),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r6_x_min(18),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r6_x_min(19),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[2][15]_0\(1),
      R => '0'
    );
\slv_reg_array_reg[2][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r6_x_min(20),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r6_x_min(21),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r6_x_min(22),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r6_x_min(23),
      R => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array_reg[2][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r6_x_min(24),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r6_x_min(25),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r6_x_min(26),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r6_x_min(27),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r6_x_min(28),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r6_x_min(29),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(2),
      Q => \^slv_reg_array_reg[2][15]_0\(2),
      R => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array_reg[2][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r6_x_min(30),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r6_x_min(31),
      R => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array_reg[2][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(3),
      Q => \^slv_reg_array_reg[2][15]_0\(3),
      R => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array_reg[2][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(4),
      Q => \^slv_reg_array_reg[2][15]_0\(4),
      R => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array_reg[2][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[2][15]_0\(5),
      R => '0'
    );
\slv_reg_array_reg[2][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][6]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(6),
      Q => \^slv_reg_array_reg[2][15]_0\(6),
      R => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array_reg[2][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][7]_i_1_n_0\,
      Q => \^slv_reg_array_reg[2][15]_0\(7),
      R => '0'
    );
\slv_reg_array_reg[2][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => \^slv_reg_array_reg[2][15]_0\(8),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[2][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => \^slv_reg_array_reg[2][15]_0\(9),
      R => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[3][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => \^slv_reg_array_reg[3][15]_0\(10),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => \^slv_reg_array_reg[3][15]_0\(11),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => \^slv_reg_array_reg[3][15]_0\(12),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => \^slv_reg_array_reg[3][15]_0\(13),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => \^slv_reg_array_reg[3][15]_0\(14),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(15),
      Q => \^slv_reg_array_reg[3][15]_0\(15),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r5_x_noise(16),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r5_x_noise(17),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r5_x_noise(18),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r5_x_noise(19),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(1),
      Q => \^slv_reg_array_reg[3][15]_0\(1),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r5_x_noise(20),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r5_x_noise(21),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r5_x_noise(22),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r5_x_noise(23),
      R => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array_reg[3][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r5_x_noise(24),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r5_x_noise(25),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r5_x_noise(26),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r5_x_noise(27),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r5_x_noise(28),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r5_x_noise(29),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(2),
      Q => \^slv_reg_array_reg[3][15]_0\(2),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r5_x_noise(30),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r5_x_noise(31),
      R => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array_reg[3][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(3),
      Q => \^slv_reg_array_reg[3][15]_0\(3),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(4),
      R => '0'
    );
\slv_reg_array_reg[3][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(5),
      Q => \^slv_reg_array_reg[3][15]_0\(5),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(6),
      R => '0'
    );
\slv_reg_array_reg[3][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(7),
      Q => \^slv_reg_array_reg[3][15]_0\(7),
      R => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array_reg[3][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => \^slv_reg_array_reg[3][15]_0\(8),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[3][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => \^slv_reg_array_reg[3][15]_0\(9),
      R => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[4][9]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[4][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(10),
      Q => r4_x_delay(10),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(11),
      Q => r4_x_delay(11),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(12),
      Q => r4_x_delay(12),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(13),
      Q => r4_x_delay(13),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(14),
      Q => r4_x_delay(14),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(15),
      Q => r4_x_delay(15),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(16),
      Q => r4_x_delay(16),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(17),
      Q => r4_x_delay(17),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(18),
      Q => r4_x_delay(18),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(19),
      Q => r4_x_delay(19),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[4][9]_0\(1),
      R => '0'
    );
\slv_reg_array_reg[4][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(20),
      Q => r4_x_delay(20),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(21),
      Q => r4_x_delay(21),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(22),
      Q => r4_x_delay(22),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(23),
      Q => r4_x_delay(23),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(24),
      Q => r4_x_delay(24),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(25),
      Q => r4_x_delay(25),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(26),
      Q => r4_x_delay(26),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(27),
      Q => r4_x_delay(27),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(28),
      Q => r4_x_delay(28),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(29),
      Q => r4_x_delay(29),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[4][9]_0\(2),
      R => '0'
    );
\slv_reg_array_reg[4][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(30),
      Q => r4_x_delay(30),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(31),
      Q => r4_x_delay(31),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[4][9]_0\(3),
      R => '0'
    );
\slv_reg_array_reg[4][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(4),
      Q => \^slv_reg_array_reg[4][9]_0\(4),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(5),
      Q => \^slv_reg_array_reg[4][9]_0\(5),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(6),
      Q => \^slv_reg_array_reg[4][9]_0\(6),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(7),
      Q => \^slv_reg_array_reg[4][9]_0\(7),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(8),
      Q => \^slv_reg_array_reg[4][9]_0\(8),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => axibusdomain_s_axi_wdata(9),
      Q => \^slv_reg_array_reg[4][9]_0\(9),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axibusdomain_s_axi_rvalid\,
      I2 => \^axi_arready_reg_0\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_convert_func_call_ip_peakdetector_xlconvert is
  port (
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[9]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[13]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \xmax_i_12_24_reg[15]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 13 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_convert_func_call_ip_peakdetector_xlconvert : entity is "convert_func_call_ip_peakdetector_xlconvert";
end design_1_ip_peakdetector_0_0_convert_func_call_ip_peakdetector_xlconvert;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_convert_func_call_ip_peakdetector_xlconvert is
  signal \inp_carry__0_n_0\ : STD_LOGIC;
  signal \inp_carry__0_n_1\ : STD_LOGIC;
  signal \inp_carry__0_n_2\ : STD_LOGIC;
  signal \inp_carry__0_n_3\ : STD_LOGIC;
  signal \inp_carry__1_n_0\ : STD_LOGIC;
  signal \inp_carry__1_n_1\ : STD_LOGIC;
  signal \inp_carry__1_n_2\ : STD_LOGIC;
  signal \inp_carry__1_n_3\ : STD_LOGIC;
  signal \inp_carry__2_n_3\ : STD_LOGIC;
  signal inp_carry_n_0 : STD_LOGIC;
  signal inp_carry_n_1 : STD_LOGIC;
  signal inp_carry_n_2 : STD_LOGIC;
  signal inp_carry_n_3 : STD_LOGIC;
  signal \NLW_inp_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_inp_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
begin
inp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => inp_carry_n_0,
      CO(2) => inp_carry_n_1,
      CO(1) => inp_carry_n_2,
      CO(0) => inp_carry_n_3,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => Q(0),
      O(3 downto 0) => O(3 downto 0),
      S(3 downto 1) => Q(3 downto 1),
      S(0) => S(0)
    );
\inp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => inp_carry_n_0,
      CO(3) => \inp_carry__0_n_0\,
      CO(2) => \inp_carry__0_n_1\,
      CO(1) => \inp_carry__0_n_2\,
      CO(0) => \inp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \xmax_i_12_24_reg[9]\(3 downto 0),
      S(3 downto 0) => Q(7 downto 4)
    );
\inp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \inp_carry__0_n_0\,
      CO(3) => \inp_carry__1_n_0\,
      CO(2) => \inp_carry__1_n_1\,
      CO(1) => \inp_carry__1_n_2\,
      CO(0) => \inp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \xmax_i_12_24_reg[13]\(3 downto 0),
      S(3 downto 0) => Q(11 downto 8)
    );
\inp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \inp_carry__1_n_0\,
      CO(3) => \NLW_inp_carry__2_CO_UNCONNECTED\(3),
      CO(2) => CO(0),
      CO(1) => \NLW_inp_carry__2_CO_UNCONNECTED\(1),
      CO(0) => \inp_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3 downto 2) => \NLW_inp_carry__2_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => \xmax_i_12_24_reg[15]\(1 downto 0),
      S(3 downto 2) => B"01",
      S(1 downto 0) => Q(13 downto 12)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_sysgen_logical_80d79f42b8 is
  port (
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    D : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_sysgen_logical_80d79f42b8 : entity is "sysgen_logical_80d79f42b8";
end design_1_ip_peakdetector_0_0_sysgen_logical_80d79f42b8;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_sysgen_logical_80d79f42b8 is
begin
\latency_pipe_5_26_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(0),
      Q => peak_amp_dac(0),
      R => '0'
    );
\latency_pipe_5_26_reg[0][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(10),
      Q => peak_amp_dac(10),
      R => '0'
    );
\latency_pipe_5_26_reg[0][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(11),
      Q => peak_amp_dac(11),
      R => '0'
    );
\latency_pipe_5_26_reg[0][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(12),
      Q => peak_amp_dac(12),
      R => '0'
    );
\latency_pipe_5_26_reg[0][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(13),
      Q => peak_amp_dac(13),
      R => '0'
    );
\latency_pipe_5_26_reg[0][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(1),
      Q => peak_amp_dac(1),
      R => '0'
    );
\latency_pipe_5_26_reg[0][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(2),
      Q => peak_amp_dac(2),
      R => '0'
    );
\latency_pipe_5_26_reg[0][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(3),
      Q => peak_amp_dac(3),
      R => '0'
    );
\latency_pipe_5_26_reg[0][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(4),
      Q => peak_amp_dac(4),
      R => '0'
    );
\latency_pipe_5_26_reg[0][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(5),
      Q => peak_amp_dac(5),
      R => '0'
    );
\latency_pipe_5_26_reg[0][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(6),
      Q => peak_amp_dac(6),
      R => '0'
    );
\latency_pipe_5_26_reg[0][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(7),
      Q => peak_amp_dac(7),
      R => '0'
    );
\latency_pipe_5_26_reg[0][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(8),
      Q => peak_amp_dac(8),
      R => '0'
    );
\latency_pipe_5_26_reg[0][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => D(9),
      Q => peak_amp_dac(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c3d4c1851 is
  port (
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    d : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \xmax_i_12_24_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    mcode_cmp_en_net : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_array[12].fde_used.u2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_array[7].fde_used.u2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_array[11].fde_used.u2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c3d4c1851 : entity is "sysgen_mcode_block_0c3d4c1851";
end design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c3d4c1851;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c3d4c1851 is
  signal \^q\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rel_19_28 : STD_LOGIC;
  signal \rel_19_28_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \rel_19_28_carry__0_n_1\ : STD_LOGIC;
  signal \rel_19_28_carry__0_n_2\ : STD_LOGIC;
  signal \rel_19_28_carry__0_n_3\ : STD_LOGIC;
  signal rel_19_28_carry_i_1_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_2_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_3_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_4_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_5_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_6_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_7_n_0 : STD_LOGIC;
  signal rel_19_28_carry_i_8_n_0 : STD_LOGIC;
  signal rel_19_28_carry_n_0 : STD_LOGIC;
  signal rel_19_28_carry_n_1 : STD_LOGIC;
  signal rel_19_28_carry_n_2 : STD_LOGIC;
  signal rel_19_28_carry_n_3 : STD_LOGIC;
  signal xmax_i_12_2400_out : STD_LOGIC;
  signal NLW_rel_19_28_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_19_28_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \reg_array[0].fde_used.u2_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \reg_array[10].fde_used.u2_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \reg_array[11].fde_used.u2_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \reg_array[12].fde_used.u2_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \reg_array[13].fde_used.u2_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \reg_array[1].fde_used.u2_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \reg_array[2].fde_used.u2_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \reg_array[3].fde_used.u2_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \reg_array[4].fde_used.u2_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \reg_array[5].fde_used.u2_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \reg_array[6].fde_used.u2_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \reg_array[7].fde_used.u2_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \reg_array[8].fde_used.u2_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \reg_array[9].fde_used.u2_i_1\ : label is "soft_lutpair24";
begin
  Q(15 downto 0) <= \^q\(15 downto 0);
inp_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5A9A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(15),
      I2 => \^q\(1),
      I3 => \^q\(0),
      O => \xmax_i_12_24_reg[2]_0\(0)
    );
\reg_array[0].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => O(0),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(0)
    );
\reg_array[10].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[11].fde_used.u2\(2),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(10)
    );
\reg_array[11].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[11].fde_used.u2\(3),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(11)
    );
\reg_array[12].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[12].fde_used.u2\(0),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(12)
    );
\reg_array[13].fde_used.u2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => CO(0),
      O => d(13)
    );
\reg_array[1].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => O(1),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(1)
    );
\reg_array[2].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => O(2),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(2)
    );
\reg_array[3].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => O(3),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(3)
    );
\reg_array[4].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[7].fde_used.u2\(0),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(4)
    );
\reg_array[5].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[7].fde_used.u2\(1),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(5)
    );
\reg_array[6].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[7].fde_used.u2\(2),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(6)
    );
\reg_array[7].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[7].fde_used.u2\(3),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(7)
    );
\reg_array[8].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[11].fde_used.u2\(0),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(8)
    );
\reg_array[9].fde_used.u2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \reg_array[11].fde_used.u2\(1),
      I1 => CO(0),
      I2 => \reg_array[12].fde_used.u2\(1),
      O => d(9)
    );
rel_19_28_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_19_28_carry_n_0,
      CO(2) => rel_19_28_carry_n_1,
      CO(1) => rel_19_28_carry_n_2,
      CO(0) => rel_19_28_carry_n_3,
      CYINIT => '0',
      DI(3) => rel_19_28_carry_i_1_n_0,
      DI(2) => rel_19_28_carry_i_2_n_0,
      DI(1) => rel_19_28_carry_i_3_n_0,
      DI(0) => rel_19_28_carry_i_4_n_0,
      O(3 downto 0) => NLW_rel_19_28_carry_O_UNCONNECTED(3 downto 0),
      S(3) => rel_19_28_carry_i_5_n_0,
      S(2) => rel_19_28_carry_i_6_n_0,
      S(1) => rel_19_28_carry_i_7_n_0,
      S(0) => rel_19_28_carry_i_8_n_0
    );
\rel_19_28_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_19_28_carry_n_0,
      CO(3) => rel_19_28,
      CO(2) => \rel_19_28_carry__0_n_1\,
      CO(1) => \rel_19_28_carry__0_n_2\,
      CO(0) => \rel_19_28_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \rel_19_28_carry__0_i_1_n_0\,
      DI(2) => \rel_19_28_carry__0_i_2_n_0\,
      DI(1) => \rel_19_28_carry__0_i_3_n_0\,
      DI(0) => \rel_19_28_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_rel_19_28_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \rel_19_28_carry__0_i_5_n_0\,
      S(2) => \rel_19_28_carry__0_i_6_n_0\,
      S(1) => \rel_19_28_carry__0_i_7_n_0\,
      S(0) => \rel_19_28_carry__0_i_8_n_0\
    );
\rel_19_28_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(14),
      I1 => x(14),
      I2 => \^q\(15),
      I3 => x(15),
      O => \rel_19_28_carry__0_i_1_n_0\
    );
\rel_19_28_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(12),
      I1 => x(12),
      I2 => x(13),
      I3 => \^q\(13),
      O => \rel_19_28_carry__0_i_2_n_0\
    );
\rel_19_28_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(10),
      I1 => x(10),
      I2 => x(11),
      I3 => \^q\(11),
      O => \rel_19_28_carry__0_i_3_n_0\
    );
\rel_19_28_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(8),
      I1 => x(8),
      I2 => x(9),
      I3 => \^q\(9),
      O => \rel_19_28_carry__0_i_4_n_0\
    );
\rel_19_28_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8421"
    )
        port map (
      I0 => x(14),
      I1 => x(15),
      I2 => \^q\(14),
      I3 => \^q\(15),
      O => \rel_19_28_carry__0_i_5_n_0\
    );
\rel_19_28_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(12),
      I1 => x(13),
      I2 => \^q\(13),
      I3 => \^q\(12),
      O => \rel_19_28_carry__0_i_6_n_0\
    );
\rel_19_28_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(10),
      I1 => x(11),
      I2 => \^q\(11),
      I3 => \^q\(10),
      O => \rel_19_28_carry__0_i_7_n_0\
    );
\rel_19_28_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(8),
      I1 => x(9),
      I2 => \^q\(9),
      I3 => \^q\(8),
      O => \rel_19_28_carry__0_i_8_n_0\
    );
rel_19_28_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(6),
      I1 => x(6),
      I2 => x(7),
      I3 => \^q\(7),
      O => rel_19_28_carry_i_1_n_0
    );
rel_19_28_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(4),
      I1 => x(4),
      I2 => x(5),
      I3 => \^q\(5),
      O => rel_19_28_carry_i_2_n_0
    );
rel_19_28_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(2),
      I1 => x(2),
      I2 => x(3),
      I3 => \^q\(3),
      O => rel_19_28_carry_i_3_n_0
    );
rel_19_28_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^q\(0),
      I1 => x(0),
      I2 => x(1),
      I3 => \^q\(1),
      O => rel_19_28_carry_i_4_n_0
    );
rel_19_28_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(6),
      I1 => x(7),
      I2 => \^q\(7),
      I3 => \^q\(6),
      O => rel_19_28_carry_i_5_n_0
    );
rel_19_28_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(4),
      I1 => x(5),
      I2 => \^q\(5),
      I3 => \^q\(4),
      O => rel_19_28_carry_i_6_n_0
    );
rel_19_28_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(2),
      I1 => x(3),
      I2 => \^q\(3),
      I3 => \^q\(2),
      O => rel_19_28_carry_i_7_n_0
    );
rel_19_28_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => x(0),
      I1 => \^q\(1),
      I2 => x(1),
      I3 => \^q\(0),
      O => rel_19_28_carry_i_8_n_0
    );
\xmax_i_12_24[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => rel_19_28,
      I1 => mcode_cmp_en_net,
      O => xmax_i_12_2400_out
    );
\xmax_i_12_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(0),
      Q => \^q\(0),
      R => SR(0)
    );
\xmax_i_12_24_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(10),
      Q => \^q\(10),
      R => SR(0)
    );
\xmax_i_12_24_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(11),
      Q => \^q\(11),
      R => SR(0)
    );
\xmax_i_12_24_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(12),
      Q => \^q\(12),
      R => SR(0)
    );
\xmax_i_12_24_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(13),
      Q => \^q\(13),
      R => SR(0)
    );
\xmax_i_12_24_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(14),
      Q => \^q\(14),
      R => SR(0)
    );
\xmax_i_12_24_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(15),
      Q => \^q\(15),
      R => SR(0)
    );
\xmax_i_12_24_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(1),
      Q => \^q\(1),
      R => SR(0)
    );
\xmax_i_12_24_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(2),
      Q => \^q\(2),
      R => SR(0)
    );
\xmax_i_12_24_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(3),
      Q => \^q\(3),
      R => SR(0)
    );
\xmax_i_12_24_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(4),
      Q => \^q\(4),
      R => SR(0)
    );
\xmax_i_12_24_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(5),
      Q => \^q\(5),
      R => SR(0)
    );
\xmax_i_12_24_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(6),
      Q => \^q\(6),
      R => SR(0)
    );
\xmax_i_12_24_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(7),
      Q => \^q\(7),
      R => SR(0)
    );
\xmax_i_12_24_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(8),
      Q => \^q\(8),
      R => SR(0)
    );
\xmax_i_12_24_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => xmax_i_12_2400_out,
      D => x(9),
      Q => \^q\(9),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c4ff59dc3 is
  port (
    \ff_2_17_reg[0]_0\ : out STD_LOGIC;
    mcode_pkd_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c4ff59dc3 : entity is "sysgen_mcode_block_0c4ff59dc3";
end design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c4ff59dc3;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c4ff59dc3 is
begin
\ff_2_17_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => mcode_pkd_net,
      Q => \ff_2_17_reg[0]_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_sysgen_mcode_block_a80b0cfdd5 is
  port (
    mcode_pkd_net : out STD_LOGIC;
    mcode_cmp_en_net : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_63_21_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_63_21_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_30_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_30_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_16_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_47_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_47_16_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[2]_i_2_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[2]_i_2_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_35_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_35_16_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_3 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    q : in STD_LOGIC_VECTOR ( 0 to 0 );
    peak_amp_rdy_0_sp_1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_sysgen_mcode_block_a80b0cfdd5 : entity is "sysgen_mcode_block_a80b0cfdd5";
end design_1_ip_peakdetector_0_0_sysgen_mcode_block_a80b0cfdd5;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_sysgen_mcode_block_a80b0cfdd5 is
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal cmp_en_i_18_26_i_1_n_0 : STD_LOGIC;
  signal cmp_rst_i_17_27_i_1_n_0 : STD_LOGIC;
  signal \^mcode_cmp_en_net\ : STD_LOGIC;
  signal \^mcode_pkd_net\ : STD_LOGIC;
  signal peak_amp_rdy_0_sn_1 : STD_LOGIC;
  signal pkd_i_16_23_i_1_n_0 : STD_LOGIC;
  signal rel_35_16 : STD_LOGIC;
  signal \rel_35_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_35_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_35_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_35_16_carry_n_0 : STD_LOGIC;
  signal rel_35_16_carry_n_1 : STD_LOGIC;
  signal rel_35_16_carry_n_2 : STD_LOGIC;
  signal rel_35_16_carry_n_3 : STD_LOGIC;
  signal rel_47_16 : STD_LOGIC;
  signal \rel_47_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_47_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_47_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_47_16_carry_n_0 : STD_LOGIC;
  signal rel_47_16_carry_n_1 : STD_LOGIC;
  signal rel_47_16_carry_n_2 : STD_LOGIC;
  signal rel_47_16_carry_n_3 : STD_LOGIC;
  signal rel_55_16 : STD_LOGIC;
  signal \rel_55_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_55_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_55_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_55_16_carry_n_0 : STD_LOGIC;
  signal rel_55_16_carry_n_1 : STD_LOGIC;
  signal rel_55_16_carry_n_2 : STD_LOGIC;
  signal rel_55_16_carry_n_3 : STD_LOGIC;
  signal rel_55_30 : STD_LOGIC;
  signal \rel_55_30_carry__0_n_1\ : STD_LOGIC;
  signal \rel_55_30_carry__0_n_2\ : STD_LOGIC;
  signal \rel_55_30_carry__0_n_3\ : STD_LOGIC;
  signal rel_55_30_carry_n_0 : STD_LOGIC;
  signal rel_55_30_carry_n_1 : STD_LOGIC;
  signal rel_55_30_carry_n_2 : STD_LOGIC;
  signal rel_55_30_carry_n_3 : STD_LOGIC;
  signal rel_61_16 : STD_LOGIC;
  signal \rel_61_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_61_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_61_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_61_16_carry_n_0 : STD_LOGIC;
  signal rel_61_16_carry_n_1 : STD_LOGIC;
  signal rel_61_16_carry_n_2 : STD_LOGIC;
  signal rel_61_16_carry_n_3 : STD_LOGIC;
  signal rel_61_30 : STD_LOGIC;
  signal \rel_61_30_carry__0_n_1\ : STD_LOGIC;
  signal \rel_61_30_carry__0_n_2\ : STD_LOGIC;
  signal \rel_61_30_carry__0_n_3\ : STD_LOGIC;
  signal rel_61_30_carry_n_0 : STD_LOGIC;
  signal rel_61_30_carry_n_1 : STD_LOGIC;
  signal rel_61_30_carry_n_2 : STD_LOGIC;
  signal rel_61_30_carry_n_3 : STD_LOGIC;
  signal rel_63_21 : STD_LOGIC;
  signal \rel_63_21_carry__0_n_1\ : STD_LOGIC;
  signal \rel_63_21_carry__0_n_2\ : STD_LOGIC;
  signal \rel_63_21_carry__0_n_3\ : STD_LOGIC;
  signal rel_63_21_carry_n_0 : STD_LOGIC;
  signal rel_63_21_carry_n_1 : STD_LOGIC;
  signal rel_63_21_carry_n_2 : STD_LOGIC;
  signal rel_63_21_carry_n_3 : STD_LOGIC;
  signal state_15_23 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \state_15_23[0]_i_2_n_0\ : STD_LOGIC;
  signal \state_15_23[0]_i_3_n_0\ : STD_LOGIC;
  signal \state_15_23[1]_i_2_n_0\ : STD_LOGIC;
  signal \state_15_23[1]_i_3_n_0\ : STD_LOGIC;
  signal \state_15_23[2]_i_2_n_0\ : STD_LOGIC;
  signal \state_15_23[2]_i_3_n_0\ : STD_LOGIC;
  signal state_join_28_5 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_rel_35_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_35_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_47_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_47_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_55_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_55_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_55_30_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_55_30_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_61_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_61_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_61_30_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_61_30_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_rel_63_21_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_63_21_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  SR(0) <= \^sr\(0);
  mcode_cmp_en_net <= \^mcode_cmp_en_net\;
  mcode_pkd_net <= \^mcode_pkd_net\;
  peak_amp_rdy_0_sn_1 <= peak_amp_rdy_0_sp_1;
cmp_en_i_18_26_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C0800080"
    )
        port map (
      I0 => state_15_23(0),
      I1 => state_15_23(1),
      I2 => q(0),
      I3 => state_15_23(2),
      I4 => \^mcode_cmp_en_net\,
      O => cmp_en_i_18_26_i_1_n_0
    );
cmp_en_i_18_26_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => cmp_en_i_18_26_i_1_n_0,
      Q => \^mcode_cmp_en_net\,
      R => '0'
    );
cmp_rst_i_17_27_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CF7F0F7F"
    )
        port map (
      I0 => state_15_23(0),
      I1 => state_15_23(1),
      I2 => q(0),
      I3 => state_15_23(2),
      I4 => \^sr\(0),
      O => cmp_rst_i_17_27_i_1_n_0
    );
cmp_rst_i_17_27_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => cmp_rst_i_17_27_i_1_n_0,
      Q => \^sr\(0),
      R => '0'
    );
\peak_amp_rdy[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^mcode_pkd_net\,
      I1 => peak_amp_rdy_0_sn_1,
      O => peak_amp_rdy(0)
    );
pkd_i_16_23_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAA000000800000"
    )
        port map (
      I0 => q(0),
      I1 => rel_55_30,
      I2 => rel_55_16,
      I3 => state_15_23(1),
      I4 => state_15_23(2),
      I5 => \^mcode_pkd_net\,
      O => pkd_i_16_23_i_1_n_0
    );
pkd_i_16_23_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => pkd_i_16_23_i_1_n_0,
      Q => \^mcode_pkd_net\,
      R => '0'
    );
rel_35_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_35_16_carry_n_0,
      CO(2) => rel_35_16_carry_n_1,
      CO(1) => rel_35_16_carry_n_2,
      CO(0) => rel_35_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_35_16_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_35_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_35_16_carry__0_1\(3 downto 0)
    );
\rel_35_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_35_16_carry_n_0,
      CO(3) => rel_35_16,
      CO(2) => \rel_35_16_carry__0_n_1\,
      CO(1) => \rel_35_16_carry__0_n_2\,
      CO(0) => \rel_35_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_15_23[1]_i_2_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_35_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_15_23[1]_i_2_1\(3 downto 0)
    );
rel_47_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_47_16_carry_n_0,
      CO(2) => rel_47_16_carry_n_1,
      CO(1) => rel_47_16_carry_n_2,
      CO(0) => rel_47_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_47_16_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_47_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_47_16_carry__0_1\(3 downto 0)
    );
\rel_47_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_47_16_carry_n_0,
      CO(3) => rel_47_16,
      CO(2) => \rel_47_16_carry__0_n_1\,
      CO(1) => \rel_47_16_carry__0_n_2\,
      CO(0) => \rel_47_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_15_23[2]_i_2_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_47_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_15_23[2]_i_2_1\(3 downto 0)
    );
rel_55_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_55_16_carry_n_0,
      CO(2) => rel_55_16_carry_n_1,
      CO(1) => rel_55_16_carry_n_2,
      CO(0) => rel_55_16_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => \rel_55_16_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_55_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_55_16_carry__0_1\(3 downto 0)
    );
\rel_55_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_55_16_carry_n_0,
      CO(3) => rel_55_16,
      CO(2) => \rel_55_16_carry__0_n_1\,
      CO(1) => \rel_55_16_carry__0_n_2\,
      CO(0) => \rel_55_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => pkd_i_16_23_reg_2(3 downto 0),
      O(3 downto 0) => \NLW_rel_55_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => pkd_i_16_23_reg_3(3 downto 0)
    );
rel_55_30_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_55_30_carry_n_0,
      CO(2) => rel_55_30_carry_n_1,
      CO(1) => rel_55_30_carry_n_2,
      CO(0) => rel_55_30_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => \rel_55_30_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_55_30_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_55_30_carry__0_1\(3 downto 0)
    );
\rel_55_30_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_55_30_carry_n_0,
      CO(3) => rel_55_30,
      CO(2) => \rel_55_30_carry__0_n_1\,
      CO(1) => \rel_55_30_carry__0_n_2\,
      CO(0) => \rel_55_30_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => pkd_i_16_23_reg_0(3 downto 0),
      O(3 downto 0) => \NLW_rel_55_30_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => pkd_i_16_23_reg_1(3 downto 0)
    );
rel_61_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_61_16_carry_n_0,
      CO(2) => rel_61_16_carry_n_1,
      CO(1) => rel_61_16_carry_n_2,
      CO(0) => rel_61_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_61_16_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_61_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_61_16_carry__0_1\(3 downto 0)
    );
\rel_61_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_61_16_carry_n_0,
      CO(3) => rel_61_16,
      CO(2) => \rel_61_16_carry__0_n_1\,
      CO(1) => \rel_61_16_carry__0_n_2\,
      CO(0) => \rel_61_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_15_23[0]_i_3_2\(3 downto 0),
      O(3 downto 0) => \NLW_rel_61_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_15_23[0]_i_3_3\(3 downto 0)
    );
rel_61_30_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_61_30_carry_n_0,
      CO(2) => rel_61_30_carry_n_1,
      CO(1) => rel_61_30_carry_n_2,
      CO(0) => rel_61_30_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_61_30_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_61_30_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_61_30_carry__0_1\(3 downto 0)
    );
\rel_61_30_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_61_30_carry_n_0,
      CO(3) => rel_61_30,
      CO(2) => \rel_61_30_carry__0_n_1\,
      CO(1) => \rel_61_30_carry__0_n_2\,
      CO(0) => \rel_61_30_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_15_23[0]_i_3_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_61_30_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_15_23[0]_i_3_1\(3 downto 0)
    );
rel_63_21_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_63_21_carry_n_0,
      CO(2) => rel_63_21_carry_n_1,
      CO(1) => rel_63_21_carry_n_2,
      CO(0) => rel_63_21_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \rel_63_21_carry__0_0\(3 downto 0),
      O(3 downto 0) => NLW_rel_63_21_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => \rel_63_21_carry__0_1\(3 downto 0)
    );
\rel_63_21_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_63_21_carry_n_0,
      CO(3) => rel_63_21,
      CO(2) => \rel_63_21_carry__0_n_1\,
      CO(1) => \rel_63_21_carry__0_n_2\,
      CO(0) => \rel_63_21_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => \NLW_rel_63_21_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\state_15_23[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FFF7FFF"
    )
        port map (
      I0 => rel_47_16,
      I1 => state_15_23(1),
      I2 => state_15_23(0),
      I3 => q(0),
      I4 => rel_35_16,
      O => \state_15_23[0]_i_2_n_0\
    );
\state_15_23[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7333777777777777"
    )
        port map (
      I0 => state_15_23(1),
      I1 => q(0),
      I2 => rel_61_16,
      I3 => rel_61_30,
      I4 => rel_63_21,
      I5 => state_15_23(0),
      O => \state_15_23[0]_i_3_n_0\
    );
\state_15_23[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"70C040C0"
    )
        port map (
      I0 => rel_47_16,
      I1 => state_15_23(1),
      I2 => q(0),
      I3 => state_15_23(0),
      I4 => rel_35_16,
      O => \state_15_23[1]_i_2_n_0\
    );
\state_15_23[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000008880000"
    )
        port map (
      I0 => state_15_23(0),
      I1 => rel_63_21,
      I2 => rel_61_16,
      I3 => rel_61_30,
      I4 => q(0),
      I5 => state_15_23(1),
      O => \state_15_23[1]_i_3_n_0\
    );
\state_15_23[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => rel_47_16,
      I1 => state_15_23(0),
      I2 => q(0),
      I3 => state_15_23(1),
      O => \state_15_23[2]_i_2_n_0\
    );
\state_15_23[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000022222AAA"
    )
        port map (
      I0 => q(0),
      I1 => state_15_23(0),
      I2 => rel_61_30,
      I3 => rel_61_16,
      I4 => rel_63_21,
      I5 => state_15_23(1),
      O => \state_15_23[2]_i_3_n_0\
    );
\state_15_23_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => state_join_28_5(0),
      Q => state_15_23(0),
      R => '0'
    );
\state_15_23_reg[0]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \state_15_23[0]_i_2_n_0\,
      I1 => \state_15_23[0]_i_3_n_0\,
      O => state_join_28_5(0),
      S => state_15_23(2)
    );
\state_15_23_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => state_join_28_5(1),
      Q => state_15_23(1),
      R => '0'
    );
\state_15_23_reg[1]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \state_15_23[1]_i_2_n_0\,
      I1 => \state_15_23[1]_i_3_n_0\,
      O => state_join_28_5(1),
      S => state_15_23(2)
    );
\state_15_23_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => state_join_28_5(2),
      Q => state_15_23(2),
      R => '0'
    );
\state_15_23_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \state_15_23[2]_i_2_n_0\,
      I1 => \state_15_23[2]_i_3_n_0\,
      O => state_join_28_5(2),
      S => state_15_23(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_sysgen_relational_3828661dd3 is
  port (
    LOAD : out STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    \op_mem_37_22_reg[0]_0\ : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 8 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_sysgen_relational_3828661dd3 : entity is "sysgen_relational_3828661dd3";
end design_1_ip_peakdetector_0_0_sysgen_relational_3828661dd3;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_sysgen_relational_3828661dd3 is
  signal \op_mem_37_22[0]_i_3_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_4_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_5_n_0\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal result_12_3_rel : STD_LOGIC;
  signal \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\op_mem_37_22[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \op_mem_37_22_reg[0]_0\(6),
      I1 => Q(6),
      I2 => Q(8),
      I3 => \op_mem_37_22_reg[0]_0\(8),
      I4 => Q(7),
      I5 => \op_mem_37_22_reg[0]_0\(7),
      O => \op_mem_37_22[0]_i_3_n_0\
    );
\op_mem_37_22[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \op_mem_37_22_reg[0]_0\(3),
      I1 => Q(3),
      I2 => Q(5),
      I3 => \op_mem_37_22_reg[0]_0\(5),
      I4 => Q(4),
      I5 => \op_mem_37_22_reg[0]_0\(4),
      O => \op_mem_37_22[0]_i_4_n_0\
    );
\op_mem_37_22[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \op_mem_37_22_reg[0]_0\(0),
      I1 => Q(0),
      I2 => Q(2),
      I3 => \op_mem_37_22_reg[0]_0\(2),
      I4 => Q(1),
      I5 => \op_mem_37_22_reg[0]_0\(1),
      O => \op_mem_37_22[0]_i_5_n_0\
    );
\op_mem_37_22_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => result_12_3_rel,
      Q => LOAD,
      R => '0'
    );
\op_mem_37_22_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result_12_3_rel,
      CO(2) => \op_mem_37_22_reg[0]_i_1_n_1\,
      CO(1) => \op_mem_37_22_reg[0]_i_1_n_2\,
      CO(0) => \op_mem_37_22_reg[0]_i_1_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\(3 downto 0),
      S(3) => S(0),
      S(2) => \op_mem_37_22[0]_i_3_n_0\,
      S(1) => \op_mem_37_22[0]_i_4_n_0\,
      S(0) => \op_mem_37_22[0]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_11\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_11\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_11\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_11\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_12\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_12\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_12\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_12\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_13\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    r4_x_delay : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_13\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_13\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_13\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_x_delay(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_18\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_18\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_18\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_18\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_19\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_19\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_19\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_19\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_20\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_20\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_20\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_20\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_21\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r8_en_pkd : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_21\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_21\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_21\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r8_en_pkd(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_26\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_26\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_26\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_26\ is
  signal register3_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => register3_q_net(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => register3_q_net(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => register3_q_net(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => register3_q_net(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => register3_q_net(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => register3_q_net(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => register3_q_net(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => register3_q_net(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => register3_q_net(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => register3_q_net(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => register3_q_net(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => register3_q_net(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => register3_q_net(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => register3_q_net(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => register3_q_net(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => register3_q_net(9),
      R => '0'
    );
\rel_55_30_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(14),
      I1 => register3_q_net(14),
      I2 => x(15),
      I3 => register3_q_net(15),
      O => \x[14]\(3)
    );
\rel_55_30_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(12),
      I1 => register3_q_net(12),
      I2 => register3_q_net(13),
      I3 => x(13),
      O => \x[14]\(2)
    );
\rel_55_30_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(10),
      I1 => register3_q_net(10),
      I2 => register3_q_net(11),
      I3 => x(11),
      O => \x[14]\(1)
    );
\rel_55_30_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(8),
      I1 => register3_q_net(8),
      I2 => register3_q_net(9),
      I3 => x(9),
      O => \x[14]\(0)
    );
\rel_55_30_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(14),
      I1 => x(15),
      I2 => register3_q_net(15),
      I3 => x(14),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3)
    );
\rel_55_30_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(12),
      I1 => x(13),
      I2 => register3_q_net(13),
      I3 => x(12),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(2)
    );
\rel_55_30_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(10),
      I1 => x(11),
      I2 => register3_q_net(11),
      I3 => x(10),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(1)
    );
\rel_55_30_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(8),
      I1 => x(9),
      I2 => register3_q_net(9),
      I3 => x(8),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(0)
    );
rel_55_30_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(6),
      I1 => register3_q_net(6),
      I2 => register3_q_net(7),
      I3 => x(7),
      O => DI(3)
    );
rel_55_30_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(4),
      I1 => register3_q_net(4),
      I2 => register3_q_net(5),
      I3 => x(5),
      O => DI(2)
    );
rel_55_30_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(2),
      I1 => register3_q_net(2),
      I2 => register3_q_net(3),
      I3 => x(3),
      O => DI(1)
    );
rel_55_30_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(0),
      I1 => register3_q_net(0),
      I2 => register3_q_net(1),
      I3 => x(1),
      O => DI(0)
    );
rel_55_30_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(6),
      I1 => x(7),
      I2 => register3_q_net(7),
      I3 => x(6),
      O => S(3)
    );
rel_55_30_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(4),
      I1 => x(5),
      I2 => register3_q_net(5),
      I3 => x(4),
      O => S(2)
    );
rel_55_30_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(2),
      I1 => x(3),
      I2 => register3_q_net(3),
      I3 => x(2),
      O => S(1)
    );
rel_55_30_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register3_q_net(0),
      I1 => x(1),
      I2 => register3_q_net(1),
      I3 => x(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_27\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_27\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_27\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_27\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_28\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_28\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_28\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_28\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_29\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r7_x_max : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_29\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_29\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_29\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_x_max(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_34\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_34\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_34\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_34\ is
  signal register2_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => register2_q_net(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => register2_q_net(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => register2_q_net(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => register2_q_net(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => register2_q_net(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => register2_q_net(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => register2_q_net(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => register2_q_net(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => register2_q_net(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => register2_q_net(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => register2_q_net(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => register2_q_net(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => register2_q_net(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => register2_q_net(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => register2_q_net(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => register2_q_net(9),
      R => '0'
    );
\rel_55_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(14),
      I1 => x(14),
      I2 => register2_q_net(15),
      I3 => x(15),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(3)
    );
\rel_55_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(12),
      I1 => x(12),
      I2 => x(13),
      I3 => register2_q_net(13),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(2)
    );
\rel_55_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(10),
      I1 => x(10),
      I2 => x(11),
      I3 => register2_q_net(11),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(1)
    );
\rel_55_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(8),
      I1 => x(8),
      I2 => x(9),
      I3 => register2_q_net(9),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(0)
    );
\rel_55_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(14),
      I1 => x(15),
      I2 => register2_q_net(15),
      I3 => x(14),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3)
    );
\rel_55_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(12),
      I1 => x(13),
      I2 => register2_q_net(13),
      I3 => x(12),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(2)
    );
\rel_55_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(10),
      I1 => x(11),
      I2 => register2_q_net(11),
      I3 => x(10),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(1)
    );
\rel_55_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(8),
      I1 => x(9),
      I2 => register2_q_net(9),
      I3 => x(8),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(0)
    );
rel_55_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(6),
      I1 => x(6),
      I2 => x(7),
      I3 => register2_q_net(7),
      O => DI(3)
    );
rel_55_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(4),
      I1 => x(4),
      I2 => x(5),
      I3 => register2_q_net(5),
      O => DI(2)
    );
rel_55_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(2),
      I1 => x(2),
      I2 => x(3),
      I3 => register2_q_net(3),
      O => DI(1)
    );
rel_55_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => register2_q_net(0),
      I1 => x(0),
      I2 => x(1),
      I3 => register2_q_net(1),
      O => DI(0)
    );
rel_55_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(6),
      I1 => x(7),
      I2 => register2_q_net(7),
      I3 => x(6),
      O => S(3)
    );
rel_55_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(4),
      I1 => x(5),
      I2 => register2_q_net(5),
      I3 => x(4),
      O => S(2)
    );
rel_55_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(2),
      I1 => x(3),
      I2 => register2_q_net(3),
      I3 => x(2),
      O => S(1)
    );
rel_55_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => register2_q_net(0),
      I1 => x(1),
      I2 => register2_q_net(1),
      I3 => x(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_35\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_35\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_35\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_35\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_36\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_36\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_36\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_36\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_37\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r6_x_min : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_37\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_37\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_37\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_x_min(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_42\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_42\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_42\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_42\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(15 downto 0) <= \^o\(15 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => \^o\(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => \^o\(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_35_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(14),
      I1 => x(14),
      I2 => \^o\(15),
      I3 => x(15),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(3)
    );
\rel_35_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(12),
      I1 => x(12),
      I2 => x(13),
      I3 => \^o\(13),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(2)
    );
\rel_35_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(10),
      I1 => x(10),
      I2 => x(11),
      I3 => \^o\(11),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(1)
    );
\rel_35_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(8),
      I1 => x(8),
      I2 => x(9),
      I3 => \^o\(9),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_1\(0)
    );
\rel_35_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(14),
      I1 => x(15),
      I2 => \^o\(15),
      I3 => x(14),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3)
    );
\rel_35_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(12),
      I1 => x(13),
      I2 => \^o\(13),
      I3 => x(12),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(2)
    );
\rel_35_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(10),
      I1 => x(11),
      I2 => \^o\(11),
      I3 => x(10),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(1)
    );
\rel_35_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(8),
      I1 => x(9),
      I2 => \^o\(9),
      I3 => x(8),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_0\(0)
    );
rel_35_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(6),
      I1 => x(6),
      I2 => x(7),
      I3 => \^o\(7),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_0\(3)
    );
rel_35_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(4),
      I1 => x(4),
      I2 => x(5),
      I3 => \^o\(5),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_0\(2)
    );
rel_35_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(2),
      I1 => x(2),
      I2 => x(3),
      I3 => \^o\(3),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_0\(1)
    );
rel_35_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^o\(0),
      I1 => x(0),
      I2 => x(1),
      I3 => \^o\(1),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_0\(0)
    );
rel_35_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(6),
      I1 => x(7),
      I2 => \^o\(7),
      I3 => x(6),
      O => S(3)
    );
rel_35_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(4),
      I1 => x(5),
      I2 => \^o\(5),
      I3 => x(4),
      O => S(2)
    );
rel_35_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(2),
      I1 => x(3),
      I2 => \^o\(3),
      I3 => x(2),
      O => S(1)
    );
rel_35_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(0),
      I1 => x(1),
      I2 => \^o\(1),
      I3 => x(0),
      O => S(0)
    );
\rel_61_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(14),
      I1 => \^o\(14),
      I2 => x(15),
      I3 => \^o\(15),
      O => \x[14]\(3)
    );
\rel_61_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(12),
      I1 => \^o\(12),
      I2 => \^o\(13),
      I3 => x(13),
      O => \x[14]\(2)
    );
\rel_61_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(10),
      I1 => \^o\(10),
      I2 => \^o\(11),
      I3 => x(11),
      O => \x[14]\(1)
    );
\rel_61_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(8),
      I1 => \^o\(8),
      I2 => \^o\(9),
      I3 => x(9),
      O => \x[14]\(0)
    );
\rel_61_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(14),
      I1 => x(15),
      I2 => \^o\(15),
      I3 => x(14),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_2\(3)
    );
\rel_61_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(12),
      I1 => x(13),
      I2 => \^o\(13),
      I3 => x(12),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_2\(2)
    );
\rel_61_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(10),
      I1 => x(11),
      I2 => \^o\(11),
      I3 => x(10),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_2\(1)
    );
\rel_61_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(8),
      I1 => x(9),
      I2 => \^o\(9),
      I3 => x(8),
      O => \fd_prim_array[14].bit_is_0.fdre_comp_2\(0)
    );
rel_61_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(6),
      I1 => \^o\(6),
      I2 => \^o\(7),
      I3 => x(7),
      O => DI(3)
    );
rel_61_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(4),
      I1 => \^o\(4),
      I2 => \^o\(5),
      I3 => x(5),
      O => DI(2)
    );
rel_61_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(2),
      I1 => \^o\(2),
      I2 => \^o\(3),
      I3 => x(3),
      O => DI(1)
    );
rel_61_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => x(0),
      I1 => \^o\(0),
      I2 => \^o\(1),
      I3 => x(1),
      O => DI(0)
    );
rel_61_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(6),
      I1 => x(7),
      I2 => \^o\(7),
      I3 => x(6),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_1\(3)
    );
rel_61_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(4),
      I1 => x(5),
      I2 => \^o\(5),
      I3 => x(4),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_1\(2)
    );
rel_61_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(2),
      I1 => x(3),
      I2 => \^o\(3),
      I3 => x(2),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_1\(1)
    );
rel_61_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^o\(0),
      I1 => x(1),
      I2 => \^o\(1),
      I3 => x(0),
      O => \fd_prim_array[6].bit_is_0.fdre_comp_1\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_43\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_43\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_43\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_43\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_x_noise : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_x_noise(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_xil_defaultlib_srlc33e is
  port (
    D : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \^d\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_xil_defaultlib_srlc33e : entity is "xil_defaultlib_srlc33e";
end design_1_ip_peakdetector_0_0_xil_defaultlib_srlc33e;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_xil_defaultlib_srlc33e is
  signal convert4_dout_net : STD_LOGIC_VECTOR ( 13 to 13 );
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \reg_array[0].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \reg_array[0].fde_used.u2\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \reg_array[0].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[10].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[10].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[10].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[11].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[11].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[11].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[12].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[12].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[12].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[13].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[13].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[13].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[1].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[1].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[1].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[2].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[2].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[2].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[3].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[3].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[3].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[4].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[4].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[4].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[5].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[5].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[5].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[6].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[6].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[6].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[7].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[7].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[7].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[8].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[8].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[8].fde_used.u2\ : label is "TRUE";
  attribute XILINX_LEGACY_PRIM of \reg_array[9].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch of \reg_array[9].fde_used.u2\ : label is "true";
  attribute syn_black_box of \reg_array[9].fde_used.u2\ : label is "TRUE";
begin
\latency_pipe_5_26[0][13]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => convert4_dout_net(13),
      O => D(13)
    );
\reg_array[0].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(0),
      Q => D(0),
      R => '0'
    );
\reg_array[10].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(10),
      Q => D(10),
      R => '0'
    );
\reg_array[11].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(11),
      Q => D(11),
      R => '0'
    );
\reg_array[12].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(12),
      Q => D(12),
      R => '0'
    );
\reg_array[13].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(13),
      Q => convert4_dout_net(13),
      R => '0'
    );
\reg_array[1].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(1),
      Q => D(1),
      R => '0'
    );
\reg_array[2].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(2),
      Q => D(2),
      R => '0'
    );
\reg_array[3].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(3),
      Q => D(3),
      R => '0'
    );
\reg_array[4].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(4),
      Q => D(4),
      R => '0'
    );
\reg_array[5].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(5),
      Q => D(5),
      R => '0'
    );
\reg_array[6].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(6),
      Q => D(6),
      R => '0'
    );
\reg_array[7].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(7),
      Q => D(7),
      R => '0'
    );
\reg_array[8].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(8),
      Q => D(8),
      R => '0'
    );
\reg_array[9].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \^d\(9),
      Q => D(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_blk_mem_gen_prim_wrapper_init is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_blk_mem_gen_prim_wrapper_init : entity is "blk_mem_gen_prim_wrapper_init";
end design_1_ip_peakdetector_0_0_blk_mem_gen_prim_wrapper_init;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_blk_mem_gen_prim_wrapper_init is
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_32\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_33\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 4) => addra(9 downto 0),
      ADDRARDADDR(3 downto 0) => B"0000",
      ADDRBWRADDR(13 downto 0) => B"00000000000000",
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DIADI(15 downto 0) => dina(15 downto 0),
      DIBDI(15 downto 0) => B"0000000000000000",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 0) => douta(15 downto 0),
      DOBDO(15 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\(15 downto 0),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_32\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_33\,
      DOPBDOP(1 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
klQ5h9hgPT4p1YBkLUYG8lguTKRhX0z2xODTfvC0iZ4ANS2ufXQ4XxirL31R7wyoNNR1hd7rMjwq
bhFP3bRYRg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
m0Wn8/fGtoK5/3nNxNBBvNhfylkjw8jAoTERkIzypaD+FUTSQmf52TpAbVY1LPDo/g6d8tzIrR8n
vZ2G5ckAbcwDRGFF5wpZ/kzyANCTceWo7S+WbzLbzhWkG5fkJbJk/tafbSvqruxodmgtrUI2IZDv
n7Mrf4GRIdC1iTgv+oM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mC2bSkADVOO0t7I1BTHPnOuMcPIdeypycvBkfsUtXFD/WSov2Pt6qF5xMTcs6woq+465yEDvey00
mpX9JC1ypEuHaZI+dtH7s+A4vYLXy34kFLryTKJtqycAy6XqNtTegvy4giaDFwDn7LgsamHSf/4D
cVGL06Ul6gm+ZOMvzok6Y9TBltlEkZOZ4GncKspHPcn3EJar4GdDxKcPll1RqSFbA2m7bWz6jPV3
uOD6IWnIflyobdLIsuB8hse3kjXBG5sl6X8AP4armpo9TNNnRIRG2FoI6g16Sj6zldS9XF54ge/w
jnsP0vce7kfzvP1LjtFrWdW0raW61dIddZVz5A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
LC8srWyP8+H+elFwWxNw8UhEF94Tx3vV4OlwBcwFszd9xuvwYRipLEwY4ar7HEoCwJI7beW6k6I1
ntJSXGEnPMgIDe7PadSQIz6gi9ErRHWRhWD6bEDNd/ZjFQuOakXk78jzxOTKuOpQ+ExjdHImyPzK
mDR0BnkVc/QiSO7zpOf3yjx9Vzb0Fba6OgXg0iLPz1MUr0ahYIc0MAkw4pRBo6IH5ilqVbkgNQcT
G4z/0OSzsBxipE9o1elAez7CnehtEQ66g6i3JpIIwqwnpZNQ9otSnS8LN/spRcQLOUuBq9ynGkPL
sF7EUF1gLkTFFMGgnZgawWi0V0jhiHQkJuLKPQ==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ijV0yStg7uRIl3uzK8/hlbIaWGHa9aPC5Eu/o1vErrwtArYsGFt3RCyG/S90FB6jkuLgqwPR8ZlQ
P9t/F2FWmEkwwjGbdrRKFfpbkjh5HVn0vvLKCP3SiVHXCOWxxb5z8BV+yCNdpgdnsHFecK1M8ydQ
C530kRu3UD1LcnZcWJi41LcJAc5rvlw/SP1gbl+I1qsRNEHsb+MK5vyjgwBZAqKyqi7/UK1VEPdq
myeWeCRrU0GqEq5y/PHBMknv1SqNe0d5qzG1rmAtC4df+iivMCc9xuHsCA7iqoe+ZKnMmnA/8F6+
nY+gx8AQNplCeFxWppKH952fIYplHtE2rKQyVQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
saHlQAxOuJkLItpv5N3fJoJP3EdR34QNYHtUBFx7fcQy8HAUaZ/9Tlt2kfpn9r/pk1MmJ7uf4z73
LyaG/PWw7v3yH/KIVSLeIxiaSHoVfdWH1/RDH6k3DTDNbWSJDOjGSvMTThUKe899F8IXTD5jKxCh
frGB3Io0W4klQV7ADEw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RFiy0rrSiVrZMC725S3XkUSDuMS7Z+xog1sOwADnIBmdCChtYLUQVO9oPjXIujfg3bBFTH3qzII3
Y63fFMSZnnzk+ups3RPMBqEPPdMQSwlpDzsvhCOwYHe/rsOPZvqd7lL6QOKoA3mS9TZIP+mOaTKd
vzQiVyfS0rs+QHdJcw49jD5y7Dc3clQHD9xZMAMUHa5v8hzX4IOcnkSUOLpsZptR/WM7rcMnTzCL
x6m2UC+xSrwrb5vEz9cePMHx9NJyO2DBG0HTEDzGQQkrOCJJjvBxtB3r/E7/3eWGHvMwphSJnfCd
PfvYXz88X+ZVCW9dTNfc75jI7ogVMSunmtUuMw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WLp36jZAcoyydHm4PcND2CmxtXbSdZuVdTwLa1Xerek2tPBEqIJBiZjk5NJZqtoHDp8QQ358xIN6
pf2fZFgkBqsAllT8AS6dtRqWsplpu9A8jX1GPIRzE+g23zLt7Y1iNm+RsRXp5xjgbNQCAWwSAMD4
tFHsCEmV3OFfyk9wWi2pOTho2S5lk6dyCFWq+jjelow1XQWNqIgftVK5iXC1NvHVaCyOkMi3DiKg
B9cCxWqktF3PUaw5+5+PWBpTktWLHPtnBv63IPBPWLsvYrkefwv8zmUKbZnZ5ZyCW0zjhc2IT183
haUrp/tBDXboJsWGDgzoPbfWLit/vzw0v5KIsA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
EYwELeHG4USXT9LgtB5n+52CF5304Uh+glUw89WaKb4O6bE+AhR6nkE4miQ4h+RDVPikeufKj2V1
m9zzxM/Y1/v1Emrp5nyIZcJAIS9xvqG48xc5dz5jthGXU0oZyOWw6KvjyAQVlKN/81baVMhO+8+e
AeNVYkn/BdOho6mTlPGkxBUMSTwjHw9iw4O+MlFXpepr/7rPhGs8NhD0987xWEflvtnZd26ruf7J
iB+QLZRzLjf53sbunm3X2Fx4+WmvU1KcO41+e/TcGNNn7Isba2cXTe8oEYcdZECqP6qCzKqcXwSu
bBtNZw9k2jLXUPNQwTuNvIaghE3fLkXRgHxH3Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11280)
`protect data_block
2s53Uja2323WECJcxocW9hnQcB7e4iqjvihiLRi3x1N8k5xG+mWjNAzxKLJWGppONQd5iMhUGROH
HiZBWaf2eVU1xmtJrCs/6p5YYsVIjQ7yPPPnKmBsIAHt3FQtHUP2HB+I4XMU0PTJ3BNQwFopmcPh
+mPT0hjkLDDjdMEbznUIadoh5XYRdqVM/pTTmw1n2OeJ2MboS0asVE7wlaXUaG3YoWOd69dT4o/e
dQdQMRzC+AjjMCcx4/9kJYwlWt9aE5QzBFVhRi5k5f4Y5DpNEkuLlz7Hmh7n44CMzUU0fwVfQrmD
F8s/Q0hniAN8WTlpu5gQCizm0w8v2pnaJxSaLpjyij4iHARyAar4H7YE61Mu16+mWdhbO7mKdlkX
s70K5WWa6/KS6e23POAp1PgpnVYOG/QxLBtpHVwTLGjC7CnLZHEH8F8awSgACiYyVtQ2nXHQENZR
SCa/L5RYB3mk3/v3XEcYoato56uCRWMOPxG0K4Gnl5ipE8EfgHeDZ1OiQG9M0N/PbPnyHRO/k8R0
rjCINLfCuxLyJqZtAIRhYIBgJ/5+0+GLBQuoHlVwXvHVkPZsFt016vklYPA5ePAT1KzWeGdkoUFV
OpzzS+e8fLjjZ7MmFyReG4tXmQMPByX/qqE2y5+owtvl0FAlDE0TbEVBlyozMcHCwISAsDwv9s/n
o1yBjsZ8bdj8iLBPpzgB2IzTmcTlqkTtkTPPrQroXmPIvXr0TP+Ekcgh1m/LgdzWf7fS9moClTrC
+ykl2YApVDuawR9tK57OfhSWT9azzeIoufZUbysAIrts+Rp4t+WY1xP0GUw85H+9mU2zdk3azGhl
DgG9ERUSkv8a3UyU69PhG6kBBD7Rq7a+aym1wOXpz4tW4QlXXfBwBISfmw+dpK4VEMAf99ACIubL
GZZsZj5vIH0QJH6kEyehx+nELaxZfE93lptM9Cm2to8/clYLbXvGX7rTUzJLGqchDxAb6aewBOwK
Q9J7lUSbio+Una6Kx8J3a+mhWvSduWNBc4XORZYFGcKU8kl2XesWu1DdOAZWv0teLCXFpBFxgxRk
u0UYfSSIRjLJDFi4Lc2hAf7bJJmMIvaFIpU43o7Jm62nW36udF7SGigG8xonDDhNCQiSU/QFp0fK
pJybQzHQRNqqbD1jijyi6Tf2Dlnri1mwwhzXAbB7KtOZoPQaRxibkuHsQoVOSYt57Q0mB28CNRYi
iwETP6Rjna97QG5RTQOGCruZk4XcTVz0oqgEJbh2WjgMKBqz+Dlx0yDrn70I8MrcSyA1cbUCWQ/m
/5R11k5z/e0Vk+sRWortgGsVIIsTgVQKCeIItFwVeGLGtai0LPif6+dAOnL35QPQSZni1EKbZOio
8X8pUSFnLUPPWzssScuJ5duIBqKf0j+JEO82Syo+HHL9MNeN4U/9UBkQerKDyuwvL275ml+cAylC
Yv2dASrJ36duIXmdpfgYVvnGNPZ/GujqUqa9sMqBph0rN+/vSB6lwYmXLWadAvPfDAyZpmUq1Uq3
jWRCETgMaxCA103CHmuEOB35GF+G8AHKT1I8Clm1VOTAad4d4UWrBEvfntigLccDlGISAU+xD/3E
Bu4qVgUr/UNOxWRT2wnnK1DhuxEKOb7QR5D0+1SVSlQgNtaF1a3G7cJlwfr4DiI+6LY+/GPo1YwT
U5c5fhNOHwyuR1oZu9GZoaApFUX+DPJaqNLwHknAm58b0Y/c7fzyEyfUxCBPuCam44cBwOSYppkz
7jVCQmkfN0Dg9+tgHOkEibWTjwI3HppYt9EW2O/kOzaUf5TtVA7GxzKGSssdk77VH72bVzdDaYOb
ujcb1u0CLTA5ClBTV7BGI7OIbZga6w6hms0Mk0ipV58KyjGKnHwhgTLhJiXjB9z6s/1XjAZLHc54
kvtnlcHwS3I/1KdXKee1HQaxiLjbG3q8WS3dy2VQ/8uym+3unZQX/NQW19wfhR0z27/MBh+oO7B+
wK8GKNOpqWNVpqi0hPZJJ1Rwk2GdKLjDZyyTmqNiSazp9vyrjcq3Fops0Fhsv5GoevMbmTLO2hxu
kwsChxMKgo5wzjNLnd9226zA6n0rjWzMZIbHJBggRdh8Y+Gn6z6Arr3zGLXerngi/l+y6a7drJ4r
dYzW2NQMIPwXimVHH+oz9j0TTtInXWNRE2pslZ8MQ57Tv+vM7Zes4etRqcbmuW05fo0sQm0ZpSSd
YqD5fmWc/+ENxiboRMZdnwsHRB5R5tZd3kwt9fC9zgOUO3rLi9/t2XbVYb3hLyan/nlhMjZklAVJ
BC8jCdGv7J9SEA3C+ppApr7iESKkpAq1zsGiT/j9HBYmf1QgGJMsxLbAILTUZ6OH3YUdRrb9dNa2
lLiPjRPCfk+BCnh2kipu7DDEav0SLiYrqnzixq1ZbyBXB65OW6qgsSCtDuapUixK70SnnIeXvvKG
DbDAA1UzBkR6SczTybMzzQqiXiO5HhMpZjSfhhXaxlx0i+QA1QIB2jnRSeAFQwoFSYyzIuPJWZeo
Scim/NUC2+vBezE13Q/LnmMOLMUxzZa+/iW1PuREOzy5zvlAsoBP1g7o1hhGlzsHg2TpIquF1EA3
2YBrHcSJ4ElrOZFjJuuVPf5LKj4gkbOHIhO13OiH7YveNwbvt/WoJoKqGfpy5dWlag+6V34mQm3I
xWCXTxbURxutJ6haxB99wD0YDn/ErdkZvV6ztb4RruxeNmRgR2ZIkIDyqB1ThdKfI4UJdc021sIA
hJCuRjUzu1+QkMpKfRSzaEJMq/eme320Mvm9ljy6KXG5wUKkEHFjGEspP0imbGf5frN7oHTWbbnp
roxDZt88kXfOvmIsmib/QX4M6TuNHpk5m6bW1R2MltKOXsq6fc3k6VnMmZfQVvrA/tSVVxd6Lxa9
FKZ+rm00eN+MX3fqkZnzcbF+ULzRSsgvRby49mEvkzWxoHq+H12b+BWKap1oeV39e+pTZCX1upB+
aVJgXWswersLDdbJGAgjH+Qkn4U+OUkeSfmEMChqxjxmAWeUXsSs/nR2pHLfWVktlqAa9I7K7reD
D6sGyt7QWrx43RushwWyLdlIXW7aWkRZTs8YvJy6tRLb9FFaNsUgUbSPWYraeqQUtvaaQtP/P/YM
FirZh7nj0zsQ3m405KPL5Fg/7ZXcsai1tTi6jchbZdNDqrnH5i+n3rPYd35QXXMYBBW5eOQkUX5k
rot1Wgst3FpRj2qhopaRDvvsloJLmHyBa4Wv91gaPy1dyQT6RmnGde9mFLTuXwgLiSERkDumxh4u
r3moaIj3y3Q2o9VIUwUkdcEFFZs/MEd0PBb/nyROX+K/UXcFppcazqFoZ6JsALamdg5EzrJPwbvA
/qzlxUEnlfPDEEh4hiIkG01vsvTD4etUSB3I3pMZe/WocLgF+VfWubMaDGoGyYubi2J23Roxce7G
GFLw+pmua+A/g+U3920ygiDPVbJ7OkArCfeeTDzP6d6AbbBW1yo4OK+XFPWjXlg97EEfd9l7U5LH
gTdjy5VCCgeO+XmpFAjJmtLUMmYs6mhTntGUja+cUrQ4LtN8I4gXDq+D+vJE46IhiMUgEkUxfLBD
WZRgFTAp4zTpiCIIRglSorDJtGHSgHV0U4Pe56BnCT8s8pwczs1qrmFJaOaTziw9mn6y8ArCWzXY
JLbTJ3pmNESR0FQX0KHlORwdclodz16OQxMqqzmg0IK7xRUrxwgbgWQyXRTRPUXsCy7PNQ4686Y1
r2nbKMyV4HfYRJRSku4W4zo5uPiMuTrg+2MJOJeDgF563lFpaX4Gtd57I3Tvd/y23XDJycgsxzIA
7P2HT0g5Pg+T4dQ5qFsPZ4a/glwLkfFQ/D3Jh0W+VVTw9ZMsq64KdO8ssOggUOQeN+E8vxFT6D+k
YssMjG84lxF9JLySY5AqE+wrxQFe4Q4lDLUPU1s4xJBI9v2p4ktEsAKMw/zGPeYzXBpiPKN+71yw
OE8OYxvMPQ6glg346C1ffB+nJAKKv9MdMs1DKb4zUz+1APvExzcWJrhgMp5rfiLnx/UR+48ypclN
bcWeCEShVOlfMLR108FJqLOf47vP0MN9bP09ya1nnG21+mMtvxf90hbno/xUosbwC+owAY30EqjG
8lDq0C+oq48xtet8J0TVnR9QxiYLRXCtITPcouO9cdVbvuhiOPlprT6SBk0o29WG1KVN4l5EXyLt
MyvPYh0HZuHw6fpqLKLLIBYJ++SIg1Up58QtLTaumcARWA2aLZwhEJoMdZLKsn+tphxlwKy9/7SP
5TsZ0PWm6yniXYuahHTpUbJt+7xsPFfrbBldnL2aijbAz7pc3JvG9Nxa/oDbLYYVYWb2BHj0Nwam
8vy13jvnqMoikInxrnWYT0kI/uAOCVym2DbBI/BVbOygBciT3lct1xVN6NQYsYOQ+txTB/v8qftv
wAjPWlDtPRAQ+yQA9qYWM3hgnkDLxjsEfy+URd0yYAnJGwFdAXJFWmCmDGGIYbnBfAFqStTmOfUJ
wSckLol0QY+/SaSDrVO0uh1goAzU7R8R9MLHUxfOrTTAEaQi/6rkZzI5ilHSD40wpUbSI7YPGocf
pO4xJu0QkC82FKCPq3ulI7DrHExIiE5sxQTgJ7ZS/f1c7AwbF3mB3RIklYO6Rl8iTh7RX/U2z2os
PwUTbHhGg9BXecL4QZvNDu9AHRUv2Z2lad+6rcWwWRlUEgQwDyMnN2BpreFZ2qrwFORUgYSmCRZW
2khtZ8uFPsUXT8RIE8uDJp6mJOXGcyIozmoRip7ZHQmmGK4qLyg7AwL/lIdPveVKnxeE2UxYTYu4
VWGwKmfQHT2aWrLBq7PGUu0nvxt0tPB8JMF788+YKh+kXipv+Z0hZH/vJ4M8nE8c8sB1ZNQ8cGnz
/GIPKXgOWGR6+OiqOTHA9IaSg5e4uWqKb/yl0t3YIBTX2IT9ty1dXnXBZUfb8ZhbiFj9+Bn0IRjP
rfmTONnk4RNQGMJo02P9mO9VfrbIm3T5nELWNZGsNwBmYXEDocObaCTdWzQwUL3Svvo2w7pD6916
owekUGo0/yAyDRx0IELPHeF2RrJOza1Q8ZbW4gcsX6lcD3g2xJtep/kMTEz/99AhGDk5LjMtUNWC
rPUj6aGzb0B66kerQdoxdTv+j2Px2oj9TLbWlmUYM3hJNWiSTT3/lCNB3WJ2wL2c+CoLVdgi9Wxw
Z6qPiRWqVkpXEEIfe7cXIqTtreRwzKC5v8dCL6GBfcpn/v4rAW/n7PD/oNKcOnerwwk3AZM2p08d
jY+4s7JGhqvtqH8p8du6TYMxmY+hTd568MPdX0HO9xNmlpJ2ps0rLIsmMwjoR+Wu7OlYDxzsTOeb
xxqKuxbQ/HUS82O7KwDfVZlc5asKTY4/TPzqzJZ97VwBvs3HZiFnuKW8vhL/MsLK4BOZPifXULu8
dKdRGt/Ij/z1BqHQUOyfX0bYdxJ+VlMQlnCWKe56HUGbBYLwNLij6dj5JWwltLrVhk7zp0LZrsCX
li5oFGHKGcfcpS5Wmqm/KFCbJUme9DZUiSBY8p2m78hpUasJri99leIA75BORY9EbjgvNeRqZzMp
xUOOPZ36RdUKHlgq2Bf2rBq1H0CyUs5NGPO/iiCZqp2GcUf8OMjjk+DlCsaRHg3uA+7GCrRU3R7n
Eif8x2MbsxdYIYWuEYoJ52+b5gByJvKM99wL8RKa6u69ouT4pzrUmEDnPPFP2vfd1mUPqpt7ExGt
cQkAxWtIsqz+8O1rLrfo3LM0UjdzOjvgDmjL9phQPVdEoNzrWliZ7RRM0DoEBDYgJYO8iZGfiL4A
KFlJZfRr09M7GVdNhJAngnwMZKL4TbUU01a+A5riP9ViGGKK2wTZ0Uin4OYYuKY5UoNbmKlP6usA
KnVsORr9t9MCqAGVLiv7vXxiyPntMRb9UCz6un5dAxsaJWFZzMRXZNvvbsI5FLvJW1kRsmE+Xp3p
gOeoTkWkr/gaxuGAvqmiaCS0ZiyVisN/l19wyh1NxAzB/cUz0a5z/qjmDnXV1xcA4DFmGXLNhi0h
cWcOcGOqU2EQdeNG2M2UJvk1RQU4gulrobPsWmtvqSAHTtl3ZWkLYOxLDNB5Baj8svv+AlW1KEbI
KWtbkOn5BSaiXu8jai8NMePFiAVHDs4h+oIw697TfpEPVEtk1tN/xDw4WHE6Crd6DsZOX7m/yQgY
Nvbvfyf48ccGPftKT8m1g1j33epoQHhJS5Zkjd+E/njSRNpBP5oXwW+2rqpvAAhMX8o3b85B77bb
wl4lWx05aTfwyksAa2TanbwDChOWNVou2il0BP2tbu49TxlAGO42afYB0to4HTbMN2arZZ2Y7g4X
62QadD6TPLnlDyqESMx07FSCwzKsgce+XucBwu5o3j9i7mC52T9fkfuoqWCvpVJ+7k/rHui1vnym
2I8X4+xV9n8ICQJmsEtAbj1rg4ElFdDKg9KnhXUE5cVqo/uba0d6Zsf2W16yRpDIFVgukIHt7CL9
KuQqk85D0Wpgvca8sIRBhDZY5KZVpZ4yDKGv0toeKfKyeFcsaZSLA6A8bHglu3m1LazpGJidpqR7
6szJbawaqit+f5925cBhUt8lxth44CzCrG1JjwIMwsx6cmN28uAybbQKeO4JgtNeJRqjHwds38tP
b9EXvMyOGQfdLa0olCsozjV75pVzrQ17egKWsLqH1HvhgOgzSveWxkIWWo4BuXPR0ddJFLPOI3x3
fpdVL7oIB4dcqjnWh47RbPaqd+O9SexIs2ie/p9cGfxpFQmKXYaFLLx2cTG9rpCu8tHdvQsgXdV/
MZw1Ns2yjyLgVIu+diz/qPqBeAITuL/JsKpbzAf7Qcb+XZyXQqXljHmtOp3GP3XHwFy3bF2myauh
QkJ24YB8hIoT1Elp8qfVJkUNWhRqKQrH4adY98WJgrVQ8ZpqPoQMHp6Tg7+i35iO2tGErd5rs1gh
jR5WaVYo6+f/zK8duML7o8kbGyeqfFcyRvET31bFPueHvVJd/sd5scsnZ1FkmUS0JzOdNb6JCrNy
tQ8eyB1ilsqTeN4xe36zJfKZT3XA1oic87ElDigPA9y27H54AlvHCvZCkuHCOaD9iGo7YhihpOLZ
XB5Zo34AuI6XamfmbTRwrJHsbq3tzOO7wewNNeTT4x/RWkxgeWmuqP5f4S0N7S6EwYvE6RUfO/Bi
oJZoqMuDGCRl9o5vvsqfzq9Qe7aGdFhMlG9dSgk1slRZ+lUf1AYWp2Pc8wXTq0QLYRgfgKSkuyrp
riwtkqvWZzst4Pl/BBQZiUPRbJphJFFxB+OAZ2X31LEr2Qh0SmEleEuXI9x9zWrsxTyi2ypSWiAh
lOdnksEDEXARZJy5Kmsw6guMHAsjc3j+BA4ZrtpeRXs9NfzcOvw5HtfFvDeiPxQBKGjbodQ0JSb/
ZOwc81HmAO8Vi48kJq7mJ0PYOqZ3Qwt2rqPOA8cH+i/WqKmNsWHUMx0s152hszDnRNq9/KMMX7ct
nhLnpZ42JVp7Fw0lhblFWDeuU2TTT5mzcTel+MYxfexVWJW40Hh94oawiTAH4gu7UDZTq9EhjKlG
eV2WPgnifbRyuH+DlPjclHcxuA0Ew81XedOaG4xyWQgaGaNB/RUHeX9lNatlC4muPMNNJVtReIeV
EFqiEYCnsFDtmZpUTWdhU/XmPMI/yvJt/Hsru+4K3H5WaTePkbdzYXfzI/eckM5LAkOqKiFzNEFD
LhdJejJoq38Pe5XWoR5+5DSEqg/8PKFUvWPk47o0y4g0/DLvPptcXmSUcqCPu7N3hvsR8JTn8oTj
eoMDwwhZQLWFloRTVk+7Jv/WRb+Q+2etuk7ejUe50sJuiRJyAiHr3uyL5oOOmFygiGtICK1Jk7V0
SEtxQrlZO6oX3yyhhwTsJzAosJx8DTGJWr0GAs05Q4/XKLqXWLgu3zh5hdpj7HWi0M9RoGXFaIUb
uyXiGfDEUJpWqPFB4KXR5BTYqPRO3juS7DLS6+kC81Xboel+tQ2knVRouBXjEpN0GEc8fgowkQyl
gt53bYhS/gnU8Fcf0rvyfWZ+hA9iVmVAN99VA8dRba3pAKr3N8wIgIb8EyKiryLqWZD54g/kVUde
lIf4uWl1FvkSTc5Oz9Tc3NSaagytm1zuKNTl0lMyQy2i6KFU8bejbFBtxdTTqDrnoaevI2xrVi6p
gZXV4QoHtigu9V3CCh6YcNn6GoHGLq+loWE1aG1pPofAv6ulTO1uedw0mFdCFB6cmo/yJrzRnHF3
i+GZaRvdrT+goAgni+EAs1aTqMoNyr+oswD0VKvbdey7WxEKGmJCDxrjs/Twx1nduY9GA9mFGEwf
1wcHEOSquQN9GjZRxirL015qdzy4YpIFxfsKq2PLdVD5mPYQJ5TuXD3/bXn2dDZUY4bZ4vOJiUOS
x43kdcdapmHN836h4ytTyKl8bdkaTCUePpx9khTINVyNB5e+YMwSDHVSqQZ7R41dg9ca0plzfQ+s
ll7weBvONyPcqP8KICO2JWsjQkcTR97QyPvWD6pZerfRzIoqvxCBnDXEjYhfZB1uJxLH5h/CiL57
THptcaYiy6yfYRyEHXKD3c1Dyb3szNEkigtjXZ3O/U1OVY8l2LkbB8MpJv0Z68kzmMxS9DOQT5hS
nHG2uCYRFV0zOKtkcEo3PR4MNRRgAmiPZZDXXYzGwDXB4FXreQYFdL2FjMjl6t+5MSP7YA0FUYOF
+p+t59KXqnef0Wf2YGA9c2yWsOYScZnmtuKIwHPhU+XmA/zIjB65JnTrEcmg/jPVKojCKm1V8RPC
BSCF6/ohFKMihwffjweGjWbJZVVw9OvOPnZ21ggxbO3/BbQxBKwxDIJbsy3HKMbKxaSefh9jsp+R
nOW3JN1iYzG/xtAWf3zFcKpOqzJYI5pANohijHA1x7lPzjJfuSUeqmGNuoMMpPTXEv9JhjpeJSFF
TlMdv2SfRR4H+MosRcUxJYM2VezSc2T508TcSWqo9V75iiHUOvrVvAVlSxZFEW19PETUqFEX34yo
bJ+KWQfwMIeZI5vQgHk3yeRvf0CjZ4lD8Wa+PGHTJOqfuHHve15vUHZtEuxjSs5uYym5ySIr014v
tcTTzGzD65wrqehikV/7MpOHGTE/uE4KSqDREsRN555rF/fNYgIbScqHiEATkCjmgDY+sYLQRbBU
+cPODKQQC7c7GukW5KZZB/Bty1oE2wYkGZL6IgVS/iFpkezeMwLYmNC1xDshVy2jz4Y1VK0VkjmM
TIUzXwsD9RSzVDS60xHs8y32pzDtDJOy6Pti7nX3Po4dPfxEZFcNKkxAOy1Z4d8j+UUi5+hu+lQZ
i2aUao3JV9uoW5L09C3h1+nTbQQtGUvtHBv/yOsMQTlH/2CVKPWUswbnMcfX1JJYViL0yLxV0VBA
KD/7ix4jVRE7c2W5ZSg0ZVeRoPHkyG/nfQxqtXfB+bCMpwVQYzR9yXprzmv4WeZgzECmPHdzD+jy
vCEEQ7Mb5tJ/FJfxGthnthNo4gR17bmIlptgEihY2+gXMhMrLhPe2ZQwJVD+Ix0d1GEE5tm+R7fW
V2N4bZ1b8Gv/1xDDpdultLm8Hg2xcOtA+LkuTZ3KshizUqi5cwok+t8ViP1yA/CoskVaU5w39Wre
RlQQacHSbEOHgamM2LQdpisc52/zlMM7t1HQyptO0drkBcJ8LEblesS9SNiS+t6mQx5RxahRPGXG
6/zrbeuxU7DMYjMnSoKgJ94T1rI7V8rzBOb0c/xpXkK+kJG41dT6+DNyUuvzGlMDY+AHBAgqeCLh
Tb8j+3fhgh7bT4lfml4TDQ/q2YgqFmgseZoSQvjZCYIKrFZbkZg0hPuJ0e7lGXmU9XGh4m9+cOlm
JsdnzKn8Q39NqRhJWyMaw0UzYx+o5z/Gce+GqRA0AadDdZsJ2aj6ohg4OSSIPXBWvTVqs+Vs+Exd
+8J5yFW4GDRZQVShAtSjjRFO6nZJCLIG2tLdkvYMCHBE337sEEMozmkKTpqYEgX8FLSdflnlmYoE
bv63oGTmXxc0We9CHx3nQDmNhuzshnB+etfGCT+kMIGde2ynQxPsA13NvPHwrvq1vP2G1Xdowr9Z
9Dwa8FiizxMpkzf1NFBdVQXj4qZBGJU6O64l1Gre+2jl23rdrQylBagRgL3dXgEbFQ2IMSPNz9cQ
KDHkuTfE9U+HG1PdZMguH5AkZHOmxf3x/di0ky6yMFkL1B6Gm65t9y+IGaQnjF2dzsQIkOCSZuTt
xjbeOZxJQXFrxLFzHua2cm6JER1diQXcn3KEIGWPJmymvvo2gJ1d0lK5cQN0vs5lsNdNtAeTUoKC
+Ks+LqlpybdIBgnbOe0/2NVJja3siWj/2ZiOG7YI0HuMYt6MbA80u464I05uOAW5/099rYEQwk7u
1D9yX+fTJK6XKSKL8nB6qRGZUmMDf1WVXtO9sROBPPhBZ/nlzh73nQHHqUfenL+CJ7FSyrzePBMy
w8U6ngJ8DW9L9TtMDSYMKLPV9V8PRxZyjWydfG2ONdbSL1pE5kfiCOhunot9KFeCx6LcAb55kCLc
zgrymrRYo3fr+dHLoxg+VjXfzfsSsfFg1gNkU7DaCfnwrNSPWSLiWCFkkzl/tmjTQRmvIUqR8mA/
ePuNqyDzG8fAhTBc0t2JgA2knGANhx/5pdfoK4U/djQlqqV1rwyOxYwD1FaTx8y5JEDB+84tsXQ0
6Gin8DXGgPnRQlEzY06O9n8uTVOtFOU9nzOAYJStevemFjAOFPhAOSjTs7p/Mk2faYlelzV0lxee
gCcTvRKPkeDOWtuaKoXqVC07UZQKCMgZstaLS50yawALXS/eMtPgrnlXLZDDA5vr1RK3TR7ynQ31
YsvIQFxDQ7rrBMvD4jxC/oFffycOwaKVvCV5XHpvnag8yl7wo+Wrd3lNOXixUeg2S29fu1LKOAvg
yya+hoH2Uk+AJnqkyVzOdohmHQnl6bcknX2MbvWDs/Yw4jPBFOAh0CNMCQqreZCp4XILei8UiqY4
MX5AFDbOTxVAjCNj2hqzS/f600XPPtE4SAiN+micoMILcNsurySKPAWF2m5iZPhv4GGhpl4cfhAt
97fQlb05L5LNX+Ol4RjOc6XvihbZHUxLJTkZ3iUNpp9EBAtf5u36FuNpkO4j4Pe4GCbEHg4IpolR
E7XuQOsMs2bD/nKPhOeO1JHd3SfmZmUbyHVSAbNYf4huqUD1EzQlj3DR8ndfp0NWaJ2I27e014aQ
BG/02zYEmlWFaHapAyCr/QxaT+0ziG9/BCUvWg2TMRjW+ziwE1JX6+sCCGDYg46bmoBQZi5dfQqb
j66ivOYzRI+c90QCFK5Wj8ui9poJ0bd0IVfBqFpH/9L/NnQVg6eX0Zyjx7GI93AOQR4PtC5aEMO9
47SuJRkS8pXUVXhmM94e/gJnRoSd8BdZo6zkMIuoq3cQdCgdFZksg/2ZL2bT4R8HfxAC2hDKypNU
6I9V84osH2o1/flEXmLeEYWovme6PI+jEnzGhl9S6vDvBxwC9xcy93Ev2s4pp1ji3NADu8ws5uQT
DrHvs9s0SePqk67u7E0UlMQQRzcRxp2NoIQhvDaOFkpi4nJ2fC2rK6hA6g18600NPiP/X3o/30MB
CiAw3i85XNkf0z6D5zVMTS9Zaa6wRCYnRO/k5WYNMLUIOgbFw0EZT63LKyboZPZypNdB1HH2Gz+B
7O9Rs2GT5IuexiVrl03wLIw05MltTtgtxZFuv9o5PIFAivS23JSsFe9L5lfuOSuwrj02i3DaAwDb
uBKEeGtHPwh9g1BT1Idny9nK/dlWBCAuKECWQb0lQe816ynUeQOXU9uHH2vZK9UVfBBbo1anjuGE
BDnQAShd+IrQdXv60U67gAlxUQYJhb8snwzvFs/wf3ifIONVGHeLuHXe8yIIhM3+z5lK3m3GWA50
q54mNmtM96Oo9F98B4+o/UrR98fVLkotD8VQgtWdTeMR/TdWpmW3l9ZSM2uX7/SXn1hH/DjGpVug
4lHIjaCyFCKQSoxkBAGNnhGqvfeaiLNJVXA1r1lR9hZQqN7C4L7DW30QzTajAIniZtZjHmGaKjiM
wNwh/AUVtWopYJbiZHIXgBXMsWQeSNBmVY9WqaP2zW++OE0IeR70w94JGdJuch2CiUwOrYEGJy5a
wOyNH3dyYWgFWX6PTzxK/cWdIGlrUWiuTgt5CDiPaZZg8ecUdXjNk/RX0V6mM4CS/ikRJXQMCOyl
L7Axl6i3D4X5/ifPmEXbgDu5akLvQxdiBsanqsv9lK+QrBeRE3ROJat8XcgpBM6ZOs/vroykH4vK
r81+KjlWm/njxGrt9hCThoJLkJDcQQ6l+Fa4y1aLU90QDTx2nwNj8H66IAYTLj4F7eNEwsBnDwZY
kUvFAmibG/itPApUmO04dPW+wPSp/hanPkOJ/yAhrYm2z6dXrtpmpmOwW+YtRiq+MdG7WObxfZHz
qgB/jjsqpNABUxZ8lV+gsSH41tsnm99WY9gbrHivdy/IbXXKCp01ShX+XPe3trhif+BryLIUcaTs
IWQ9Yix4/2lDG8hHU10NiSXEjYkf4uHmGWa7agUhYROiquZz16O5uBdIJOKRS++DGfZEtqtCI0Ot
/QR1mnbygHf3v08FGLh7oijH5b5mgQ/3U6aNLoysvTTVdf3oHKstZBQ/l8AsloGZbL750gr8yTtf
6nTPZ9+FmAV2oVHPF91PZhp4rHsh8skp0wAXsvjzzCOhMG0HfLsbnu5jQR748PpM0FcEcMlwo39Q
JrfGC+NCR7SlVNBhBrTsep5rSbb01dmNjU5qgxCCFpglakJaOGa/sdaVU93AOHAnMvX5C/xoiPEz
slLwNGcKn/Hras7m1SmyHy1Xy1rkJSIYp9gvAM+KIpIBtX5O4FROk3kEl8Bq++MdzgqepyMGAh56
M0HQz79DmXAr7vkcV1Z4lDq9hqS1WQBnu5lzs/k6kkElk5o1aA4vxsuKCTPZzJLABebTjShuVYeL
2BjZh9UC/gq0x23zqjeSpTsySnR52koh4rKYrZ9hxr5rhJRf89PMko+y6lpEVpb9+NjM7BrcLrfc
Y9DXTgkRfEP5Z4XB2oGtH4AI0xAGCOmhor9l0DGXh1GvVGGqKMJdODuisPVMYe0N9lNM55Zr8lBr
Zc8EdIM7vo8kWwnchJiYusaszwY3YqGgrAhF812B/PKgzOo0NlPJajllIGsbuo8qGaZWJ6hYyY1w
cvDpPCpnDkIAOOb4NgP/TZFuLdU1ppW6K6kvtwL4aXrvLX3hb0OCVt7f5kGiAH/4XvVGxpzSqK9J
5jI4sPb4kS2L6458lOMwmQ1e9DPknAYfXsZbPNWzR0l3+OIZTPak+kWxSeuZ9TcS36F9Ixo2RdMT
duXZTrEGrh+rUn5Oq6y3pcqufID1PlxE1xXFrI2E6+Wna+klCzzhxBXZk3a6FgSOWlMZu6j3mjkS
z4BsnXS0o2R3D8bMrbBL46OIlFncuDAcyGRtQqgVW2vqKF2XAq2Vm+342KqiUrE8cmG4lVef2fQ5
L56/j55WeSNHTLvbCXgIhSsWKwsYn3Ox6Oy/hCh8qBUzD0wI6a2PRbvAZ6O4weU83weOfJd+Iy9i
lHHKqd5Bu7ay44yz+JR/pNRKGGsEtyINNCDssJ2NUMGdgy/Rg6Fb1B9Xt8GSzUyXz0+ryUjsuKv7
NxCcSj4Kjygvz/OdMQZNleJ8gewSNO8NF6i7wdqzovXEVfKvNrEIUrHwW0uGBih3hEinU7IunfM9
OFQMaVaYc1kAyB06TB0xW89F+lT5Iw2Cf0IxkMPWXn3r6Xnjf/nAjTWIUkXPygxvBlMrzgU+zaJd
8YjRjovgf170kiewtjsJIEhGfwrXe+r6Pw4Ty7G7XCnX212CRNAwoBNxUroHYMAVq2R3y7t4eyWB
DFzX3EWQrXfeIJp52OHewzPTt5cKJT2qgtbDzMjewsOYFTJZTXMMAnNfIY8dNxd6l1rTtKL6V8MW
xjfOgYayNRZucHaFneSM+pmhrTbFb+LaZJtg0iiyebPgimEFsIa8cauirVWTHlVERUnCgZyzZEcb
QYjuNN00HsBfGu47HNDU+EymVdkNMwf8pbCsHhnHZCc8Z0Y9VodKgCDFsXJ7IkjpdRtesdq2reeo
e/XVNxCHTwarGOkcBy243QH/HCFJCGrBTZO870z1RKuioCu0lFEI98OgJQvYrPRiJYnRl15ErKNO
0ZtWKKUahG3pSCUTRiCkAcd6f9+JLHNamCEnby1uJVKabysu7iMePU2D4a3eLQ+P+IXJ8hwt9qA3
HZFrQsJbourjWpaQoYxLB0Yg3JGded8pWF6X6snyeDJIwy51GMiheuYlvr9uFdpjmvAsSPn4z/W5
wQ4SxFNoAe44h7q3LjD2C3LcQrs1QD19OG4+vSDQ1RtRS+ugiQCR9J3QQm/RchKdMzsVLYoVcEp5
4IjP4KQExgpv/5+Bzqa8r/0H09ZRT5ObAD3M+sk/eyJkd2jU5vD6y+dNzBxdXwgf9R/rVotmhOx1
tBJ/Vc9nxNk3V8rm6abhWJZcR0Pg0zuq2XPz36ayINvmiLobJZcDTZs8cfhTI4VL22LpDjGc971K
IgXm3OW6I6vc17PtnJvW1N6g1w1T0i0YtHANZzbLGYKmqYRyFD7+yWhoXrbR+rvTciMFRhlOnTC7
UbNQKkmP5Fp6bIsOthxB8VXxq25JitB9nlw8bKpiAso5F9AFjkaJgtG8DutPQi5+M/3BnhV2uHwq
nKcPgesV9WaHakx0J/UJgAGK0eKJcj1TVSR/+y8DpUT5Mmkc/vqx/ixBV36kKyc3chtflfc9rUv2
ZPJdmq93t/k6kHyPEmeA8g3YnVa+IGC/kdFK6zmTe/UOEcQvVI3l8eGY+LShOG83+mlDJljV5Th8
ZUUllIVQstJutgeN6nRZLgUcuIXGKZhquC5SQoAZ8w+DxqUDlDJFNRt1tmpbmzcRA99mikEV4ZYH
ajNyfkFJsC86t69i8T3x7lZ/znXavQLBYavAEQBv1XVEnRgz1txfbyelNAeMWAQec9bMfIhgFdYd
Hx5hyhayNYoMmdVW3CVf4adl34xNH1mIzUkHDoCipFOHP6/79A61xSFyfEXY+Vr9d+g5
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface is
  port (
    i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[2][15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg_array_reg[3][15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg_array_reg[4][9]\ : out STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface : entity is "axibusdomain_axi_lite_interface";
end design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface is
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^axibusdomain_s_axi_arready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_awready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_bvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_wready\ : STD_LOGIC;
begin
  axibusdomain_s_axi_arready <= \^axibusdomain_s_axi_arready\;
  axibusdomain_s_axi_awready <= \^axibusdomain_s_axi_awready\;
  axibusdomain_s_axi_bvalid <= \^axibusdomain_s_axi_bvalid\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  axibusdomain_s_axi_wready <= \^axibusdomain_s_axi_wready\;
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555C0000000"
    )
        port map (
      I0 => axibusdomain_s_axi_bready,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axibusdomain_s_axi_awready\,
      I4 => \^axibusdomain_s_axi_wready\,
      I5 => \^axibusdomain_s_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0F88"
    )
        port map (
      I0 => \^axibusdomain_s_axi_arready\,
      I1 => axibusdomain_s_axi_arvalid,
      I2 => axibusdomain_s_axi_rready,
      I3 => \^axibusdomain_s_axi_rvalid\,
      O => axi_rvalid_i_1_n_0
    );
inst: entity work.design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface_verilog
     port map (
      axi_arready_reg_0 => \^axibusdomain_s_axi_arready\,
      axi_awready_reg_0 => \^axibusdomain_s_axi_awready\,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      axi_wready_reg_0 => \^axibusdomain_s_axi_wready\,
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(4 downto 0) => axibusdomain_s_axi_araddr(4 downto 0),
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(4 downto 0) => axibusdomain_s_axi_awaddr(4 downto 0),
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bvalid => \^axibusdomain_s_axi_bvalid\,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rvalid => \^axibusdomain_s_axi_rvalid\,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => d(0),
      i(15 downto 0) => i(15 downto 0),
      \slv_reg_array_reg[2][15]_0\(15 downto 0) => \slv_reg_array_reg[2][15]\(15 downto 0),
      \slv_reg_array_reg[3][15]_0\(15 downto 0) => \slv_reg_array_reg[3][15]\(15 downto 0),
      \slv_reg_array_reg[4][9]_0\(9 downto 0) => \slv_reg_array_reg[4][9]\(9 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_risingedgetrigger is
  port (
    \ff_2_17_reg[0]\ : out STD_LOGIC;
    mcode_pkd_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_risingedgetrigger : entity is "ip_peakdetector_risingedgetrigger";
end design_1_ip_peakdetector_0_0_ip_peakdetector_risingedgetrigger;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_risingedgetrigger is
begin
mcode: entity work.design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c4ff59dc3
     port map (
      \ff_2_17_reg[0]_0\ => \ff_2_17_reg[0]\,
      mcode_pkd_net => mcode_pkd_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg is
  port (
    D : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \^d\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg : entity is "xil_defaultlib_synth_reg";
end design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg is
begin
\partial_one.last_srlc33e\: entity work.design_1_ip_peakdetector_0_0_xil_defaultlib_srlc33e
     port map (
      D(13 downto 0) => D(13 downto 0),
      \^d\(13 downto 0) => \^d\(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    r4_x_delay : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_13\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(9 downto 0) => o(9 downto 0),
      r4_x_delay(9 downto 0) => r4_x_delay(9 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_10\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_10\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_10\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_10\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0\
     port map (
      i(9 downto 0) => i(9 downto 0),
      o(9 downto 0) => o(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_14\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r8_en_pkd : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_14\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_14\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_14\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_21\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r8_en_pkd(0) => r8_en_pkd(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_15\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_15\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_15\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_15\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_20\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_16\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_16\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_16\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_16\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_19\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_17\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_17\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_17\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_17\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_18\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_22\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r7_x_max : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_22\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_22\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_22\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_29\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r7_x_max(15 downto 0) => r7_x_max(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_23\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_23\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_23\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_23\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_28\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_24\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_24\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_24\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_24\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_27\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_25\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_25\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_25\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_25\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_26\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      i(15 downto 0) => i(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_30\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r6_x_min : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_30\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_30\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_30\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_37\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r6_x_min(15 downto 0) => r6_x_min(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_31\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_31\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_31\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_31\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_36\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_32\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_32\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_32\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_32\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_35\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_33\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_33\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_33\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_33\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_34\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0),
      i(15 downto 0) => i(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_38\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_x_noise : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_38\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_38\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_38\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r5_x_noise(15 downto 0) => r5_x_noise(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_39\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_39\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_39\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_39\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_43\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_42\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_2\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0),
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[6].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[6].bit_is_0.fdre_comp_1\(3 downto 0) => \fd_prim_array[6].bit_is_0.fdre_comp_0\(3 downto 0),
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_8\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_8\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_8\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_8\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_12\
     port map (
      i(9 downto 0) => i(9 downto 0),
      o(9 downto 0) => o(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_9\ is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    i : in STD_LOGIC_VECTOR ( 9 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_9\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_9\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_9\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_11\
     port map (
      i(9 downto 0) => i(9 downto 0),
      o(9 downto 0) => o(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_blk_mem_gen_prim_width is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_blk_mem_gen_prim_width : entity is "blk_mem_gen_prim_width";
end design_1_ip_peakdetector_0_0_blk_mem_gen_prim_width;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_blk_mem_gen_prim_width is
begin
\prim_init.ram\: entity work.design_1_ip_peakdetector_0_0_blk_mem_gen_prim_wrapper_init
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Mj0rXuaety3sQZ95p4FG+D98ZjD0tV2rfKp9uvLOX8nlEWg6kXxqq4PZc7yrpjLmICREbXFdkhvQ
ZmyzWa9MYt+83dTE+ARo0/1tP1m+JUv4GCostrTxqQp066vrhcA8OV/kyDgjv5sFP/Cn1mSo9vEs
EYqQR5pignL8fUA2v1Oi/Me/9GGw3uirJ+EcY62XPyrPn/VigtzNgMTLzuTZK3vaVOgD7RFdB6Eb
Ytvb5tkBvAbjd7AySNHDWbntyI8vTN+UhCmMZMyqSOLVsq8DVkViGyK9Gd94meRbeve+BrC08Qhc
4RVGdlPxRLZVbsdZ+nFb4VfixHPBJn5gW4l1Rg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
4UpwZT26x430d1gllrjn/4MigsAvv4dvItX73r/qVC/JVBP+XakDe95R7rANCoqF6fuU+3YRiEmQ
dQYr1bMWkgeUNzqFmXaq98GIRojaHK6sdOW2BPBw7RyNOh468Vt+XWXMXYsmaH5Fuwf53WMYQ1v7
WaMq6eDj4Oea5ge8VNzcuIjxvs4J0PsmIuQvF51/L5wNq+3/cFq39xy8/auVxeimpjykr46dWxjb
DNrZR+KTuFEvf7ZYek7Lg0Nnmn0G6EwKIxtDnGJ+ke9OXL5ELxDsmkEVeWIvldDYo0y+AwbtekyV
w/xcInP4vVEtX5p5RbCrWtoy6dpvRxN8nf2P4A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 36976)
`protect data_block
rI7XJjArEVGGswM+EsE6aQdZ7DGbpaKPGgsLuKR89p6w+azIPbg1rQJjAtSJTJ0zlKwaadIbRnJ9
wgFN1FS3D8dEQCjvQb4e1NFXpB29dDEPBodtwqcLWJBC9iCFkHVpMfLTq3Qto5ZY125y/CIgdONh
221MCTZndwEC7OSVxfg+IS5nfULP2HDFawJl3TQqTXi9lw+BJjBHIuXvc81iYleSllJGtj7kxTPt
eoYu0zpySa4Guu9KC4oqypbJIXFYv+tjuJpwxpKj3SC2XdMNo4/sIL+hjO66WEnJIHPOH7PdF8Ax
y9YAJU0ma+52qJKUF7hBYcvIBLYseBbkjIYMmX4RUzCjuo9VI8AvcZou7pptRGMutnSGXyS2vStY
p0uH5iNPf1G4sLd/tDj9L/EVL2yQtFv7LWFxQ12nng04TcsT9/LMR+JQ+lddSq5BZOB/izeE/7lA
NPkeSpja5Y+Z5es+xwbjRvuYipnqtENr4UJ82UiCtbGzC4XKzFpPn1NXZhet47QLzY2xRonJ3xBw
davdSDpVavDSdXkMMZMouwZlhUbC8TIQWHdlceO9ztRL4c611EyYsGamd84z0ZwCB+4hGHWPAGmm
yW4GTxjjj5o2WIMOs+jINwfaCujxkgjYyO3oPMeyQd3ZO1OdIdmeOkJY4qa9uX0hrWLTMTQmBb/i
0kJjc0mab7Q9MqN9555NvO8g+xpi/73AiiW2VVheHAFRATSEEWOHq/iXuuu0pmipt9IU/p2oPL8I
FktGI0FOxw2IwwAK33PAcSsCNAASo9x7nOufXG5/QkEG1Lfg9Tqd4OoeN5wGcIytkZTX401ioFJp
GK0D9+GoosWMr1dryjkYur8kEcC21Auz9V9ToOLpRrANwhXvs/V47Odw9w5mz96iBLRSduvyj2FY
LX6GNH+a+MGEAlD+GTosrBU61bbEWxLYov4AaH6yNB4+X4i3FWSWaA3DvzLBsOUDwi4K6HnfpY8a
C1avqfCc/AfVt+vCHbuSES+WvsC/KF9iuSPPx9Bb2osoPhOZ6XeW5ppOh7y9/AH9w9d8oI8rHuy8
Kpc7ahWnAiMFhjqGTeJjDNZsihhRcb8rVc884pe905YPrx0H824lH7y702pUVXUvzr7QiGPl3l/S
9gASxs+OINcTOEcjJ2mEWUOoJyqMjDMqfG6azGd1XSoAa0mmz/hWJS9+sYHIcUB0Q0Gu8lt+1sBC
NaqF+LaEF+APfeUuRx0cqbVKMzI1N/zoAArXB7lq+ryVa/9+i0WtaNR+B1Isn6YuAJ0u3c8oDcJ6
4wDRy8bdf1YByfnwjZlOSorcNRXFreUggghKcn02+vBDI5JFeCAkSXhtMGlRnagTDfvhyEsaUvXL
Y2N+gx7BNKjr9ghsrkM7ArgYOBOvGGGUdTuLJzY9ca0kfrPljTxMBU4EaU837QSUU6PeByGKC3Kb
c3+xwNBpvJr7cKK07i0eENl42b7w/Iz9Va5a8J5GDpIcqPdmW6vZhid1T0PpPZmNK+/g4WmvMvAy
O1+22pLTafJJDPsGjqiNilP6MD2/MXgQ3fE+qPm9w6in3NVEMksUvTFellH3KQ/fJN+76smb9aeJ
hx2VHyOCSDJFDhTdT6BnHOgT2qYCzY1n+W3SH2NR57rwPMz+yMA7kZ+jg5B043LGTDRA16yyHili
uecr8AhPs0CxIPDRi0Uyady5WhDFfTm3dA6n75CuHQkkNEVwp0/8AGvrcZ1QGUj/qqEZYBofXFGZ
rpjqE53kzVDjjN1u/Ziw9eefZKRBT0bVD4Jdudy4DLtqNQZS2mLXdmg/NJm3NuWr7l+vwuUvsxHi
Jnsx6rcKvnze8MdIBqfOK8mh1Hn/IVadEuRym6BXQzAfADCKJpqKtkf8gCJleHAo2N8+c7PYrnA4
yIxIJx1SvgfdIzLHjzUwMJhtUtzTfPjCRHe+vtDNdurcdqBHdk0yE6aFeBRKgAw0eEPi6T6yiNV+
t2zcWJ1zY0ovoccrKNeH5vcJa45y0zCzDwQ54LSI1D3xnuUduEBmFWedy5AaK+DoByEqeL9tK66Z
QYXk4Nq5MA9aDOM4d1+SAYBoq9yIrk9ZIrDip+rwRE1kcVqKJrw8JWh7uzuq6qeB+UvwgW/w2XQM
pp3TvFaFBLe37/koraJ0q5Bzrj+43QVY9GwStBRMEkd5ItJRrxrqSWrLd9i26J+eIHy7yxVsHAmg
eHUstTn/sjcQ0R11igc4tZXvF+EDoWGQoHGzJHieRMZaQbIYW3h0+wsiGfLShEHxwE3JyDffGvL2
AhEjrMCvHlB6NryWs4vrkwNgi8hFqOiSnNeyuiNrfOnw8rR9J63OUd1MXiU+yrq8lgSoZx+UN5U0
nJiaX1uKGT6NH6ATM0CHJB6WzotbcDsgViPbR+a0ElTkTnjJNwnt1H0rLDVLpZViVLR4BB6anyZm
+z2iqfOH3e7LAK45h9LLU530OIUjvWgjo+hBhutNcNpItkoN7vFkLbHZzaQ5nupRx2doFiG9JyO7
9oQo96xGKCRk0fW8tOQH1ZfcMmb4WzN61Vh/UX7ZucWRO+FLL1mD1j8D9HA3gsVyEt4mrjSI6gIw
d0avsbqVVekBZ/51z4TKPSmBQLRA2gQKv+jhlhMpDpUka4oTC3A/kVDOCG8Vy7B/HvTRf0DiJvOy
vTikrrpMlEmUEDjH4xNXw8LigsB/dcB+cMGvtEv1gQ8YRCp67hDapsCeVc+FeTMPbkyNdhwrQ5wA
iS0dOQzxcd2T1Q9kyFHMMCtLzty98Nvp3RzOw+E7kvJdwq+mZ/Id5qkL3br1zFLrz2prFYiMTK+E
o4hhqm8kbtXbCKBKsiy6r04jCjc/xu+ZvoAg8zSlagsA10D5YTr4GaC3fqoPyf9U3M9SjqqdzREn
mozCjs2Jw7O5ADcXABsrN4Rae35ysWKqoew0XHxc71fvVVZCVebXmb/ktcTa5yR/4p/E4Sk0iXWI
rU51XsE+NrtkDwPk2iBBp8pWlFCgFT/7ZP0x9Czg3TCyuzUMRCsd1jtdBGdViVsF1eCoFXjQkq+5
n74iq26XzxuZliwnvQHl4+SWQuXVUFLl29eMpGOv75tpnOyRK3zXQAO97ZHp0F5lruEFlHSUerJj
hZoyLQ1sHq1sRHyy8MaByRtElaDahjAATAARpNVBrWxpCX6yvda3ka/r+jJstd1wtUfVDhMt/AMx
taT0T4ea1rfeDQYdyHuGN/k1ALVRs7CBBq1kjxliGRpFk5/eWa0IWtsuum4Hq7zN381OoRKUEqdR
tyCUYGrSitCG8N1ZR3pffA6KCFHXPqNp0bB2wqyLHc6BwNR+9NsJWHM3Nky0401uL5XWb/sFyN17
4a7yASQ3ABiviFhhZbwfc0BST4PcV/ntapN50bfPgBbuqxp1JPqH8m4/KG3pkGZzwXcaXyt5QM6f
C8Gd2aYiaW18rR/MPNVcjmVXYfzR+LjxKER/2MITQnpb6fNYZqeMeeBALhopaRKKGbQA6824PhAq
XPR/7QikBhwhP5R8ASFqdmNxfwTFQ3BvzyAebMs6X8vYerYGgHAoRxfV3YoFb3DXbzm6eefCKQb9
VWcx3PdnDHg6XqmdQB0M5xHtIi4GI+2rykunZt0ruIN4WqiUx+cyYl9Lu+hY6pIf0lS1WFuF3vmc
SlISFW2lZTKhEyOu/sHdJ4aveA7wAF47Ox3aGb80J7L1bC0mxFD8VZOnZxInQl1svihIaRmcwc+v
cZOF0kChOTsm2KGgCgawg8Us9K/9YgfhwByiDpce3Nrlw3EWkF6nPaOQUdqqhSsvcIOVjp7wdQA8
qjQNIlhOahXJ2BjqOmkWAFZZcBbAH4fxmM4k1SlMrrPBd+y5m074JLiAaZ279bet8+q/GN61KAOw
dchrQOhkbxULt9dbkMUYEWOZXpc79PUB9SMZW+M7h5vNmPAwG+d/d+umBCM3xGBwOUPvB/FblRPI
hZiuZx8PXHDRQjdpE3W65kFoSk8GhqODTR9UFqglK3joMp4/XosD1x7fknDc5jnbv48hHtnxJRdc
q6tFdiwKYnaEqzhwhcqo6FZO/ElmP5IWurjmZKPY8wVGQ0eno1OCIPBKmjadp7r22LfqjM2Qp8lS
74WKETgeOVDdx+JegdZHF+ISZnwMedkjy3kAX7lON8ccVSoFyLdeMaPv/gK7v/8YC/iUVM/9Pwqc
BnH/s42Au7KfEnz+W1NUy5bLfjnjy4tmPp7in2f0wPVqtBbLphkSMcr8n29XMrzK4iECEhH/4Ydd
z8gh924AqEqjIKS20W8jT1SxynjQa0lsggmCUUTNbKen8dhsdTXryHpRXsPG2r5IRRbAQdaiJTWG
73hyOK4cEvlKQFU4W03H6DJ7Lu6VVPox0p9sILyppZI5O89jRKTz3/k6bspCoPquenuJaqK1sp9O
vS+iCB6N1QLdiVOkAmTCDtdrtHViVvOWkd4V8oQ1FhsuhwP19hp5MybngV71AZ98mBZFSbgHokHP
FHe1H54cQK3oGuwDBQK0rbh2sHvfmLUlxz6n465PLGr48pDnKL2CyawfV9CuQR2BnA4Ov0ggtRV1
fE9kMYVhUOf1xOgWQfA436Nsi17KL8NAo9uMOSSxS2s6FfO5hOqwhe+tz3WLxoaQioChIfnTTh34
G7RXEC+giSE96qe51YQDfhMUp7IwfVHAQsLiYFlAOWVxfDVc8X9YUocRbCOuOQuV1qUJ3+SPzMCU
zWvRtUWrGfwgEfa81zyK7HBMBNfGkPN944z3piq/bnqK7i6HW4waTCGr5HEFinWyBmFwcLvOqMhW
XeLu5jnLDf1wswZt/n+ZKazx+W7KE+DkfY8UDn3oWpSP9Il0I7A9xjX0/zh1aKQYzReE8IPEvuPD
Y0USYYrPFP+yApAvORmD8X/aEWcomXwRgckgyya9LrIhFnGxL3krK9CelvpbD1FyIQJaeX+LUMVn
5VL6MHNl0wOVIc3eH9j/ZrmvQVXXW1W2nFaW0fpyXlkKqi3sD+KMFOrveA0RRGDd9iBWUqCUppnr
9pZNBAwwa+gE9gdnkT7K/ZF28Rngyc78wKiFLmUy3y+jV4kD4nzpzg4fVUjqsk59pl7V039e2kxT
pGm6ynmleecVmF718PqsmFixOSHsM7EljU7D1bxptyzfIs51FhQeIVgaPVNuA804IOoluPHCmf3C
LFby7wRgZ0bfGKgCzBBsnS950+lKzs6nLipwnCPlLPPPFZOQwZTP8+rXl2YrNqup4m8DkZYukFjY
TUp0nsfBmuN5a9e/dCgFDwrfuvKlm+RvI6EibQoKOFkFnMNpHglRTNxGNKhpyodOOo4nvvAe0hlP
iECfGD9PL6sZPBVPOvtoaesCqRNQdFvU7l8BsDXaP6lh+SgTWmbZ6f4J7H9jYZNYDIb89KDJLHBw
g8BFmZvJ4/dpQ92ST4x7QrUvAYQb5B9TZUTTIbvvO2GejL+gkcIbH8XP1D2cl1dRbSXI45fVsqHu
0tT1MEIUncEtMreuxSob8FVDbqAMaw3ZLiXinw/hCYpRQrivlUse1oCuSh6B2GpRpR7HCpcgJaXE
oDucqZ1mvJTF5xR8Zi74IIJ3rd4Hnvx1SrxXR7Ov/b5Nj3mBexKJJvjaUOI+VjBL9NWXCbb5yf5Q
mzxJxRlb2cvmnFGyJWlHFa/rBAYTEIwT+6PpfGdoiT+lJCkXQi+fEpkMhDEiSdM53/vI8rPjLn+N
KCksE9uiZUK2aV8JkKWk27rOqrPaz5DADeYFoMEQ/Y3IyFGbvmnUrH4JFc0qrQ23Xw6kzgTC8yF5
JUqS4+nwmIIgQQFlU+OzHM/yzQvJ3zfoCkWOUYrurHGHVv87V/YvNjwJ4wc7Z4qxI6ycHqvYgBmZ
Vfyes/oLXxFdB0lmEyi6ojHIhsjUYxRhm1Kem7RjTLhD9OdqaE7sQwbz7VWBJ4wk7qZrasWc5h++
kS76l/DK6VhUVRUvTDjB7BSIRMTt9EKIlgC/VXXzMW/BPCT4PHxHySAaX051v8pvUpCJ5bLIcfb5
ZUJrYHQyYeETkB0aDrrWV89Z7sPLUKzCUR5p31KQ66s0mff15KR50E9jUbxABGgencgIfQrxTrX3
feJtL9mbU5Lrwo88YQ9+0Sh06HBZRk5YNOxh4TDl883v/CD3ly2QpuM7Ry7QpTHwoeJdjuLVcp6V
95FUSt24v84SR2zg3cyjFqWDtOxyEAxo5Ypn1Krcimy3zBLof8GicdQ/dl9guMJ1Vt7wJ3Zp3GrF
LZCkMgHvRNbsMSgp23m6ZTwM7u+hGlECdclvQKkjUQk8J1I9ZfMEW37WYOcZOcInlHrL+htBltOS
8xrULKjgwfYl/Z6/a0RvYQ9tC/3BzVizhNx6siUXcD6SCaOhNu1vqSrxX8v2qt6JsV9ujY7+Fj0a
QH7G28RtW3Lq85NG0LMQEpRSVVsYQgimz07EUKtv50a/7oGg65f7qk1yZ1Ug2h1QVZs2VrwLFjYK
ekeHI93aV9t4LQNssOmILmshDZG3T0s5y+3T/YHrKoBk8TK5nzmn9S8XehDmlM8f7ASOZBGRfSn2
6ANnk4N89v8xq61kanJmQwmszpAg4xXLVt4Enk/8WNRk+yIZpl/q5xQm61/MT64u6DP7V4VytDsh
7Zvct2lJxgxGdxftHxF/rO70QPpc5LxXa1r6D20Go7bEDEmTo3mxm8lo6eJYfXb89teWEh/uCzwm
Hfa0rmrg2zVpkNyCPjzNdPtju215kmTrlw37RSUwmgBj3H9tYqInwhLkNqTl97ZlZCwDqhcPmf75
ducmrDXIur6XXdUXOFgcRzv7TBiz+zVDTAI2c3r1qNj3tsg1FtNoVZhSqd4CXc/TlD6MkyAdag/1
hldpGUEdeiIcqtBgzMUBOEBY5gki70UNX4/NB952mNEITBGRjqSyNl4V2I1WnkiMwSldQYLMqkBb
tgaXxXxBKvrNJhXXyZLn7kE9Hl8+8TlsCVRHCGXCDAqivi3YeTWF8xDxITz9z8StW/LQeGGStUn4
Z80oQrFA6KJQ53cBbe+HCH7oL5k/DEp06av3dGARmcasjakGJY1vHuVK6HL2yw66Cfez5ams4YGD
V8N1H8w10kLjDoFxLGq2Y9L1gahfni9R35Z4ec97ieyUgkJZM5xBzj3j9kzWcnCm84hiIEDEGptB
50uRltSeYXq7nNlyMzKUfZPITKoJOz5/prvNhie+N3PYC8KaNaeG0goNhvVjnXjXEen5ZKYt9uJU
KAbKMaJ0B1QjYh1/PbwQoP5S9XQhzSK1tDEJKf5nOE3OWZ1+2sXqiTHkVLwkOVKAxzH1VYGHDx1d
61yataDrFmw1LBa5T79jJOItF07dns+Hh9dyMHLDJCiV4IOUvlOBN79x0BfZ++Yqmep3coQzh4Fb
icgodTTkolkJ+ZCzk//jr6yALZbFXbZsOOTJbpsZFGA0o26vXQs+5XE5hK65KexpiGlT0lfRpkGl
gZIH4PRe0Dp+Ka4yMc18gJxWAY8niPAnS9WdgMcHJvTWMqyYvRBgvuaqQswAnaW84C2WYiW138W1
h8TEOTdXW/uJ4qXyIIahVypTkdlt5s2vK1i1plh+PQWkSmvOi3+21bEodSVlm8X6IUdAhin5gb+j
S3KJHGfvrdwc5gsN6XDA/JxFbfhk5YJsBCdgKaUXzZGhUYaRSKcxJ3Zp3KQ2rOlniJeoBh8Kj9wA
pcLXURV8kZQHO1w1Vx7mStlcDVxxyewFpVPzBiv3ScCmPyKKmNSqVUc8h7slnred7r4BJ3FEEVnI
QnNm+fWnIJz71wKgumCv+Cl0M/UJkks1A9LOk/WTUrCcoucnw+NwHwO761vwHLjlacxasr4VjFEm
itSquiHr/P60eZlzBT14t41347hghENH9cz4dsujACRQXyE0+zLqTfLMM5GIaPvA+0za+nZ+xz8y
193aDdWW+tULaTSgnvyCtr740QIVqNgqTW1Pd4+5L/vLo8lAB4Gm6LtYc4GFnG9+FidR4NkwNXIf
1ZTIpjAqnSXTbAn2wILafu4yCWcYLCcrJYgNlB1Nec5atavzRA+WHOgShzjZg9/RdikLl+vo8RD9
90UwAfBZ+KqEnYacOmn816iGSiVKPNMdzUVT+Qdk6E/314fk4OklEqEsHagTnMNtIsCNbuBhuXXq
8UBRPQRGLho/eAaVYzSFilPo+EMvdqLSjdVrCubEXwO1XcsGsK61HvUtaCxq8TFnF3xIrI0cr2wy
Apz9EB8udhWuPoHQ0g7yvjqwdWbi5UBlFbHygr3hJjwT6hSy63J4qHypOqfarSi+6QHrWhQPt1a3
71cralq+TuL9r4wI38ZFkPQNlHZao8c2v6tnswpCbZJpooiLSLlOr3usKY6b8WKKTUbxqBltVdqG
xf+ov2P2xnkxTXHDNIEs+gANgJYdIZqrkE+AZm4YhmxroPH8Z7cT8lZPSkPVJxc0vuEcFTnQZrHg
v6+7p5Dxc5w28HZbjQ4mvnk15I1sGMdmcxq/qPIuV3tiTl1BDnFAPX8oD83XLSw7ZNTwH7tqlLlF
obCW8PjzLY+QauHzrib2AEvSQ7z9csTOed67wPMGw5OBcDb7SYp3stEGrDRF1dmQYt+c2nnjS+Pc
d+k9iJTip8TR3HLxebN8V2NJi+jkZmRDsbOCt5zOJGJDmbYmPf1/92vQ4aEmxM8DMfNNGBszKiDQ
qv29ayWEpZpBnoMJ0nv4Or5/58hxrsxVT/+jU7I8eYHFyO+JT5r05fT5qPwBatrJxTKIUQZRQT6z
B7kxzn3MIJfRMHeQsulO6mKOY3YdVipqdBxEMVuzwFk9uikWZSoHSIQ3jpXDvJiCoiVVqcCLKT7r
yh8lUqGdnP9Gdcr8O40cMNcqD8fPXBLL1ZxufFsAcc4ZqU8uKkWVbpnxwBVqA2m+UhH3xoBzlmOP
WU6lAsHB87/ot1l6RP0l+HL0/DceTKuhOXrjVk/ZRhpuNnoAavPYOYruzUE54BoHRTLHaBM93A2u
1slG0Sh4SLbybzWzlwGxnNH9s6z9sj2bSdzkFDUjAdqk7kV/tbkee6MlUMIml2z0GAgGDI/FXiYt
FqzqB2fhN91Qz2Qv07hW/RQoJvWWZtWbV143C3hBh+JP5y0uWVf4JHz8WK62eMvhY+rkrAe0kxs1
6AVXB7rmSlVXZWwncKdVzjEens8tpF0Dlyeq450OD4zgI9o+iKqK8XSXHnUB47PWoIDhMPD7aqI8
qWpVR2YTIdXYF9aAfuCFdBKQCn8F8c2Gyfb9ai/kFyM76AgGrzr3WmtrjbBu0oSwO617ayyeSCV0
gNTuBk+TAv1HZF5/reomz8AYRjKcq/17XmQixb1di/LewG/IgNYmf2+ek1pajrqiOWErIV5eFrPu
YgkRWB9feJV2d6aaVSaPjz0ZQFaD7hsvsifI2BW3/cwaqqf6fGLNywBhnubqzfQaTBErqcOWiPgv
UfOjhVF1H7CQ06SKXv0CvEf0ZzemPlWFQbUiST+gaSKEFj/+1WeCSe1BIoN+tTZXF1tTnSWpt3u5
iuH5wiDPN9cEnEifMQxql7cVKajVWyEghez3MDA1FBnrSe4ndbwVHomMFdCWhO47tYLBGM/ja/MD
L+trTrz/s1tYz38OiHtKAvKjOjm5g6nGn4xCzpoMnbMBCfS9tTxSFleV9Cb0OBisQsGZ81Ijwft0
WWn8M3zzvr+vknq5VcbjF5IS389hrwzwMx/qmU+A+gftXYGMlmuTU9y1JYMdWd6SRRovA4GLLOuz
eTBZXERwORVbcRU0cmA1Uq6fIHe1FJMzDn2ioP4vhQNvnYqJZPNRWLXDg2B52Ud1AOH/5bRoVrQY
NbpXK4MUyZnEyk6/bneeFA4iQ7ZdnliHp3rDCLxmiFZEYyO+ochbsz1WzbxV6cJfJva7lL+TnHg8
y05FNSJC/YGF2EB86UeL32Z+88auZrqN3CjKxscy7IZWS18ypwN0nhSh+JYXkQgti3n5ZRCUbXC0
/VF9cdhkSbItOVeDEk2nHTwZVUwgl4eUKlSql3p3eJkzp40VQVWqseT/rl2jiJyBbxa661HUKpIX
stpR04V81ytjK9luTayZPIjonXBMJmEUPckYpmLZsR4lDZUCqLqr4u2F8qKaIpclvOkejKe875/Z
jCSWbrsoX3pz3T64sPzccjbB71iDji2a0oB900XBjn6jd8/wmu9hhn299tsEMEZ740Bnd/yQW0KE
1vQsz15794d2QYsxTvCVkzcmM0ZlQGVCDr3M91339vtTLkx27jgBZHbRg/oA/uEvl+5U2pcFi8Xi
fl4max8c9lACk8KqKAyIXTXfOxsWYX9lGdsUfCBNiC5sADXcLKV20x0M1CHUKGPJjakYGSg/K9NJ
uns+4dCVE5PvfocmjawdO+NeryC7JLsA05XupPS/nY1G5zoG/QDHh+wNK5Yw4wr18ojOffgEnc2W
sI7+GKNisEU6G/kUR9yJBzFd09468Cj8Qawd7RsAXsTcyYrt6vybgjDHVrqFzHmLIi9bmF13dR39
Qap5wlgopw1YaTeqILCU0VPDojWKEIbdOXKSjEGAKxy0tyh2LjbgmD7iAkPGTMTzO/zKow/EbOxp
RboO7MmDLhDh+CiQ8V6cgydzUbpOWfBOOXrFl3nlAx5hTU4KL3RWXhhVtlYookvTjTFaY1cCXHNh
gZzBBW0XM0uTNbIEgx51Xrjsqb5J6g9jUh1ugJowrj26whlSwnp7Ovb2q0YgfCgrxz/Ts57njeow
GlLsoTZFxMwWwul0Rryqz7eoeqesMmGuCKkSvbbLcUlpvAknnkTm/RVJCwrPf0lDMP+nMbstJxnY
6pE61g/+cLkrcDHLFmE0BcfVsPO1Kd8Avfhk0MIIikQq1ahJ+QgSQHj+GfgZOdP3rmUrl7W/BrJp
8QET/C2NnA/JnacmVDZBk0alR1XwD5aVuFKyHkgzX90AqZh0qTDg6L9gpRVCIuum2JnUvg4ZChZy
+vJRLZwnEcOWPkcOilcWORwszerrQevJ7/0WjtrqNmGt5gvgIQkBbREY0OKxyeTffyZxaN9RUldD
m9qDyE1EwAfFLN4ZfNpwxU111raPpZfNktx7zV4ge0KK4NdT9finyBlCGMEYpRNV5z7AAonNRhah
+xmm1LYo8pNKe429fYRPdNL8QAzkSaXUJIvkzeYtZTEdtk/0zSX4EAF6Uj9tsmnZqO2zrMa+c+zq
cZ1vZ/gMoEnTeMHLa5rmSDu+dpRXK4gL1eWXmrodIB6voSFU/Tpm+zQyCwz5ZGDmLOze0uGc92cc
vOgO3bah8qHGLkCYyIIKVIlAv49gcHC9Qc7lploRlK14QEPYOd3l99F89VfD4UZIpHEdxCbAA2zj
ixluzEmqwAfeWBS+ZrW2G2RBTBw7HR6yWKAIMQRdveGtfOrIdXSuu5OfCnvq0z2+Fm0x4sgjAKLQ
7zPk2yBVWLo8IeIon8V/aZdhYmU30m7jmGsS2sAX9C53ftj4uDFmmuoT4jnFG+l4opav4qeOm48s
Lnyj1JBVIf+QOal+zin6wGtNnbSzGbhyPE3YeHtIvepyUeahGdRbJkl4XeW2ov4QN11ysjJXHbaT
MAFPfOBO1JWapkeJt3YmX4zgMqjHY3UktdvJu4/H4BFMVDqUYjW0nbd5vw7bnjjitcD21DjtBSEo
KC/HADsAMGXpval7SKxhUNxgqbjfn92nJYLnGSyNaRUG8pISfEgm5+HtunDeoNYYrMumLooChb9w
jL/2Frx2dSr6KhRA0AejFLQdtNJTVegyV05wOiBJ87Fu9wbrumOpdNfM4N22K5v3+ZOB0e3eQWVr
J1e8ofMI9K9q6d+MVhrC0CXgI4vgzXYrTZcYVXYJ/DgFzJMAnThcq2xlVSOV1w+LqujS2S1yLQVD
nfTt5F9AKwo1bfeXz2VcDD6BTxAT3ooqE3KjHoN8UPvE1WzBnTavTigSLWi0bQdnUgdzkbXlk8Ti
ni12citx/RJIb19r6KxWIznyLJ61ZCLYsqSRYB/mWTASylKIHHUoG+jgWHw3Sx4M1R/acnHKRW6m
QjvlQ3J2Q47fQvORoXqeXBBZyRxS3x2uQ0Rs6wj1+9WyxWsS41iwBwZdudOcKow86a6sTgen58JI
tq9Tl85uHojwkxFHy5JsOy64xv0u8bo8nPutyqgflsYNLuwNqWXu9KTZ8Ks+TUY8LdfwZH4PrIWm
b8HqduV4ob2/gKZBNxIkJtY8uUQHLK4UAjyyP0FK2P9fceIAUosAYH1sUtQNBso1Hs/2OZQnYd0i
9sAagGZerzoPEd5wFxiVezK4XIwfMQx73ArQC9Iv1CR5NflmH0fLfvUnZKpxPxnoSZeKvwpSRTXA
wWDX7nylQvqpvz5UoirIRcV4IVouOKxg0+nalGAjW1Vl+9uI6CcFYjIwdKLrPVl+DWiSSdYXuuyt
3yCg2B07gHego2Ya3F+ZidtSS4lg0x1qkrTthI4SVllxQmIYX8RDlB0x+mGXmLpMeHRVVJK5otCH
grzB/ocDr3JUSYDD/7Oxx1YE60X2h3i5McNGZPBSc1kUo0y1qD0Wj2mROgdm60qxUlakxDvWLoNo
VOt+poue1+UQ07JFUNr70KMfQHS6riykaay3g+yl29Cd01owaKKIvbUtD9yl2I8+aAQUDPAE7g+M
wwekyLuh8C6u6QlGLpeC5wVp+8wuGYQOkqpK8usXVJGEvvMPHgg4J5TkMjkRlz3B0nr6dC2rUGrE
L7fk+Ep9KgfYWlA5iTevrX2oLGGTasMhMdVzN4+Wi1dMpxc9ZV7+lQez/FFBuVnCVuJs0j13zuFS
qj3ejbLRHOWiwKGSZTJukFaLSoGMpQHwhJM3EYiJWrGm22ebuvn8wIF+alo+yEPY5ljLrzGv4eD9
8GL9WpGX11uIrk7v6qjhWm+/jalIYZpXgMCZhcY3L1sn9Huxk8IFgel3c/p/zMYgF08P44DE9m1n
zgVZg+fhcu/lsslF+Ll5smaPKjjsnk2wrEnSXNNNZ677YmsO3YllQ1erR2KEG9nv3Tj78NHY5rcW
FS2sCQau5Qqu0B5sAEgLY9zveVHJubuvapYB70aicP1efNLm8xvET3EI7H+VIxeYgwz153URQO2x
jZWgEEVyQ0hxJzdBUga76AfW+C4mgqme/J+mf9svSyvouwhDe+KGVgHKu8hIBmt7+Ck+xVVC4Ybw
ncnFV2YDlMJRDB92tHOpmurTPothyhgjvBwHNTJO8slURQVySdJt58aDwpEaoAP6Nfngfn0OlJJ5
fOJTK7X13c448hrxW74l15TTqz9wXkxUFhj/eLSgy1jOKJQNqbHiID1u1JtUVdN3r/LYERfOy7xf
oq9NqKsXwROyoXAk2TMnmAnUhZT1ZXppQ78W7qSLONRKJlqWdooiYCcBWflQYVhkTzkm4ao9ZViF
ju3e7vUjxzLlWM0LrhK3ppIc8/Cq4zFtFtsJ2FVsJvtFu3r9AUV6yvNbU0ajXQ4Ddq/7/yvcEB9I
EhZ1V2rpNxLxWJv+6ownUCH6PmkllOA/BmEeme5tHIpVZoqqy/KisQ3LXbqqDZ1Y44gXsWwJuGa/
tJnagZt73PK0Rmkf+xQMGo5ejKXekIyaK19uJAhfosTv54X0KJzM4JB3BmKxlvmlINDRtlyvQ8Ct
YWBey7957F0w9MALRmfsvz7ltBuwH0dvwI/mUbS/yjjoEeo7MvuWQmyTni+ZHfEB+fjL3F5FISCi
aqNSN1l59E/xTrLwym7aiTBfvZNlU+we5Vb9w086u/rD0OPbefkg7uUqdup/B5zg/92nGR6EN6tE
K31OudsgVtFr6j/WZogR3g64n0oD16D6nJb1tFH9kQ4+yw4T6jNYsqWDGVv3qq9BLk0OodiAPFTx
iaU3QJFB81rnbJlrhJ1y8rjyYcVliORne3iHtHYgq5ddAZwfcIg4DQmiOQVF1mo0TrVVTeN3C3qI
L2VavkCUoIQqB3e0zN6GTTDsoDZmWUOsWyxwbUV1XL4WeLzJeKySWjZS2zv20PXoZhIWWBTIxdOW
BjNiqzuK0zAFx/LnPYMuW71Srr/AMbzWryWQGh6H7oPaZZ8Gukf1j5eOwqnwa1M64pwab40f22PZ
sU6xgg7l2Yd3Z6heBolY9Vldk5+bOryMOoxwYngz9gVO5XBIM+nW+ucKNAzhF5e3wGibje0X56N+
JOdTnBlrgcLnIuyvClGZ0a4ef2tlKre6WZnu8kK1ne9vnnvPVcJEUyir4PhJenFtk4u0xbziKPBd
7RZSFuESTNJEKtPjMr5GIG8pAbLism5QJCDKZhLlm7egyrAuPL2KEP9Q3KFZYEo8AnvrIHMvYVb0
dhNU8129d3KFP3DfyIJ+itdODpXKsg3MkK5Rk44o4vBOshQO4/vqXYmGHnu6W6YPjlgM/nwLKAXN
3Zhi2wQf3B0yFF98M+EAlut4scMVW84QFFuDYE4Q8Xq+mShc1BQU3OQK8XkOJDnFU6RclJy2Rlcc
yRo4h+sTAi5ZAvaAyWX8cEkdOCZqC8JTCFiaqqLeQ6mCQ8Wm5cWsC42NH2WIqyhph247QVp+1yUs
RZY2HPbUKB0pHRduK2fFq9JeasGcE1Vsmkqw4MXe9h29GhUtxynNuydvpZcvlP6qUu5LEUgu+zKS
rd534BvYycu0OGsFfOpvxF4m+ks7F3LG661eigoLnIuEvmefg97E+4kBqAiTWhW8dFcuJNgWRytL
g4QDpfZ/9408Z44CNgElz7kTfm6Q5NguEqY8GDTzKe3iNUD2USPbT3ur5pttHhdhDY4cFdh2S/PV
1QxVhRRFG0V1K3yQ3VMZZAnReSBeTsI27R9dINOklaKboXVrzuoOewar33T3S4aGyqfNeoZw73NU
afJFAnqUdXc/Z7T3hB+CtL3n670sMQ2t2I7b0B0u6eTylQTaJhIo1OjBfh1bf5AcQwl7VOSZ1fpG
vEorD9l84j5smQqncoQuGxC4DJrS9QCT0XaxdgA/S/C8uMS+wrPQQAuMEOMCib21Y/lgzgE2W8RP
Af8lzV2uGOiY+TjC/wo6BhuAhVg75vRXmvQg4EQzjzX/ixCKqAW+5Yz2ULEb5L96MGbO964G8A+b
UgoguhcWVwDs6TT5LsZ30XgGNMVCKnCr5hOyS6FlfFGNCD/xNl277Fvu/sicnBVWnLMRD+dZ+3BX
tRHjLGx+144rFukw5Y8igeNbVvhrHYZa9EJ9YoOJoIx1F2CZZD+3kMWFLp46oHVMtiUb6xRCpRZ9
l2J86fpBOG3j0Rpi+mao4l3RIECM+3JP3D8+zPsB3KD8OufILDlIbAc3b7ZZUaFdyc4iFX9rpqEa
QfOPffAPMt0jyP/dEx+KNlFGF3snNJYvLEHRpmyz/xI0KRe9JMoRcsTTUYfIRL6ipt8WEh2x1HCa
NGRet6PJvWGIO41WHNP71c8HjVQpNOKndVjLlxTwDKkyshsXLQtdOnFRrtx4pMtDLzTs/r2B1F6I
WYUL3EWnkSHLWdVys0aGscdtLITrzjdHjKsNE8jzoJ1dXT8rNKsHq+K+rZiW18HxudU63PHJMuYQ
sPsQJ96LBXWLCkjMY1fclwl2c42QLDhlinvEivokK/abA2YSb/QemLqUWiu1wL4kyfCTJcAVR9F4
M1SCjLWXIphjFqCkQNizo2QFMNg/D8FeWB6OBs2CuHfrQz/G+q8iGufT4sbuG8eFI+0GYay3dhbS
8/G0knJKU8tdt+X4atCkqTHHJsJSdUoW9gStZ43kOxwwOeURYmTim3FX24v3XEaAFA2xZc1wZ26+
oBipUBbZGbr28cAchPmzcQEvvZ9f7V8XGfNRm7JjSJB0O+e4v+xbqLi6pmQ74IaN+CNSmYYOfftj
zYNXJUM2+H6G13zT1c3JzzqobWFTzJ8xMlOWWGfRj/H7sqRV1GHZNK2/osxIIH8Do9NkmTWW7Guy
lfmSmeX/kmrXTL6QA/mhz2et82Llep47P380mZROXYP68oOWDKe/B9FC9w8eWE/YpfnErVbCqJdg
twD+Cl/y6xmgD0olQb+FITC9wuF74J/LKwxl+6NI3vPTOauJHbP6HEm/ffsfxT6qvobmFSYGiXAG
TxfkYxEb5Vauh9WGoJ7lX2384X8akWz24TX1Xztx1gxC9f4KR0GraqTppPtCiEgPa3WHesaAuMgg
VMQc5DKyH99dsGUttEhcTOMC3oDeNgS1K8i9LZHltu5mebqhLA6cI6AoTObuqiK+2RXIKISCJ2JQ
b4Dc5Oaf/Cqo3SCyUEIBBuR7JHuW/+PIVHdI+ZhHPdMo0RhuF/OVQYoo8gR2EnTCHw9U9AZDf3F3
WGRF00CS+Ih2O/rqOLh6M1X/JC87dFHfW5N/gr+XMUC2ic7ZLRbh4ijBfqJRV5UdBtYvcLFluGsQ
Fv9y8By7LlzCZltFT3TjTY9brx8WZ8xoylGQSA5xvqwKLv53/dEYqzbvARD/RIPY2zL2qBji5WlP
zVbRoqjCZhSOoQZ//l+nDNED9i+uie2OQsLuYBHSQANOnkpfYt+dcrbL9XbbMPl07tlGcWo+PV+I
RELrKMiHthLCZOdvb6FvkDF1FMprtuNyQUvzPeJEVb9PVKz7iAYfPKO40OPm6w/PLwEVtlXuFjtn
kD09fldTWKZYpJwVeiHHZTHOONpJvQ9LncvBIE6N63OR6FRnL7EZTE4OkzgfVc98jk3lF64KKPZI
vBnOWNXfHQctA65aDfHF1ZAzr/2UggbkiuszpkmrgCSe8GqLdMS06fd1WnDssJG8UL2RTgluOsk6
ME9GNl4qVcrZnUo/nmwFKrevIsLBZdeCSqtpWK+Q8hE/YXSV7ikll6n+carbAQyb3/gU9dvnW9Jt
14GEweQcolmZ2d3iIhGD3HWAq9JginTGiss8K800bVETb5Th3DuIZQiOxbtGtnPixCRF/QjMX7L+
rBDYo2CjN52bjT7dkM3xTgthqN3kwWVYSCapEs87LWQiH7T72LSk0KWH5PSjfC2VZXcJIKfL8JQ5
El7DPBHylGOo6EcauzA9QKhRItY5/kBDJV+g6VgjW7F1wfDJLVeitbJlg0n4eiRBahPe2znqimus
Na0eh0efQ1UnE0ykkSeOxwuC4qh09HjUXw9pzH5/acYs1TQ3hVW2KItxwmRRb6+9lJQ1zLRgKt36
jBNVF1P6bxZ8ZBEV0Ipqbx8kbwHw+fxonw4IHFT5qd56EVZPqSLpSfaCT8Jn3rdt0egpSym9khag
ZXHZOKj9P1Yroa3+nJf/a8A/kW2tsly7B1/bS73tFGyoszyyYi8NFJc26qcPJsEns9OdWcJe4KpK
ol5kNeDsSywSe2hGVFcJjYKivwGJY2FwMYhNxjYzh4nzeslXKao43ntWQn+q0oyt6gPOSJy161+6
UZd6tODqusGxPo1cwouQKRGq5eCeMROb+YTGoQm85WQVnN1kZCI17l+5I4kgufnp3r4kOeSqI5vv
u9HlZ+ahLAWW0v3n0uA4dqmRh4AM0kYhjeG9TAvD5tfokFnF+b/p6HaFckNKdoaeJM2WTEb2cLAB
xTohL4CkyIqLEi8nrmS+Le1nHB78o3/TziJ36htmWaQzYfHy1GK+6IOH5gbMqb7421ti9uB36O/o
E3Xlt2t5QnZKEC5WBwBlSPyAgOpxyRK4YuyhIteGyQWXwDPao1mjy0QDkQw8OSEnLR58HS/DJIXc
ukOwxVKN2PVbmWDflmu1NIoEI1cO4EM7vGMrLjcSiGtzC7AwyWjmiRgKXWQFNEW8tuRUwhxeY150
r1U6TUQjuA4m+e/6gJBzkHsJOFWjMgcGTC9XHU3bb34YrZzAe9YIPgqr9KsGM/4lz1pr1PrhHsP9
/qAWTZDf9hzVw8szwbBvQashq9Z3hjPPctO7uin+43Au1WqQGOWTD6yjTAhrMVe1Gtl8GHSqDtj3
hPHycDpfGoK5nP5UnqanYd49V6Bb6hNX7UZwt66a95QIQz3Ire6kxh6Y7aUBDYyzSynoDOVgbytw
LOsggrS59jTUUfWMYo/o7+ZzxlZacP3QG0n7XCyrglQI9OTCWSHwxDcdkWjz3WGrpKYzvVkkRPXV
XTQUnjJK6hErIs50lCgc0rbeFir200X5xTJkGqWGO58ULN4RthnkYaLjDUAeT0GYsiYGH4gwU50s
LlgTC7+a0FBJgepcwHVQakL+B44phVqPBWUhJS8s7l5sbYMF6rO/RoIgnIUof58XT+9LPBsv8CNJ
fua39hQjB5m51/UlCe0d9f75C6znkN9G2yPpDx86hGz2dX5KDqueNPoeqbKzetlwIIt8PfFXVEEH
63WhKog9+ebuOQeUGxuG1zObeP9r8sR1hxG5cJbo6IY/bpZgvtO1IT0xqEkhwOG8/JFf2fm2XAA8
E3EfGQbn+KYpr9C4OkiUnbva67xahYFn/IWQynW2EKePm3QV5edQBtsY2aWMP9OHVLimhXDvm3hZ
h8Jby6iQ7fu5BloH5W05bat/n6QkU3Wo6wamo5YW3NYQ3frZ6ZRf5NqMq76TwMypRZ2hMI2hYq6A
IQ/HHFJQZBEKtmgzW101u5OYrsYOVMYrAddUvpADzyLMDWc1+w35PzShbd3SfUfqd5XLaGYgNU49
KWvUkYrpH2YeQqjrpTTEYc9J4uhvweFy5HCKojgMvXxJiDMvvoae8NLHvmIyIufDWml6ZI+7oz51
hpzPspWCPVlHMj5Yo94jvBnjQVbRI8sk1fStcYzWfxwY1Z7M+NV2lnfyXnLpCU5seOrcQWzF8M8X
pUM3dYMOsfjCRccK4YiPRGO3HJHmILAGpAB0K4fYcmAG/OUzEPnUF/HEoWxqnpCjN3d4eCaXRYbP
YojKJ5aTtNp0EHLATw20L/06SLqtUPHM0IV03oJqc+N+K5AAFlzmywh7tj7Qsoux53UMnq/nbTwI
fX6kiDZ1bmUI8m785avgeiYm5t/8rZL/LlPo+TFd8U9Z0J2Ycuk5GgSZCX9wxLhnbH/khTondABR
yb4tvc/OP3c7MVNuKU7XJzk2K8fKek25yf+396/zAqNeV6HnD+GXDKz8vq3NJPk1BF4jI56pG2BA
sL79hqeyKsvc1LNlUD/+KV5kexCkp/eqQANxCJ75da+SfO8910GTW6ol/PulBl2PikDJRFzU8V2S
hhROyOEbK33E7RFpwtn3+8PdmkcqDDyFRnO7uL/xTEtaIFldz1YyRnCBa0Ka0Gt135ExIGzANaoP
AdbTQ1TyMd7+dl1gZYXCnfwV9CFtfQjE7TrxzWEaBef3Bc+mhKfZ6oDWS34100GwGb3WLdUiDVvL
KTHcy3Q1rL82MU3j6kIPsYduI2DAXgShpq7oO9IX8onahrZbdkyugPFX5H0eiw7oCHgT3McuTrsn
A3FeYT94sVVdhsCxjwktpAl5+pQ6OkCUMgbpjyO6c6x3jFSwW4QY/wx3vAfsoWBI/BRqaar79vje
p/qiQem7oVCKBCtaTgitpp/urXGeyy3rUZ9U2Y8qyPfM6LTJjfOas4x50XcyfaZv/gWQT+KqR0Lu
uHztbfhDFyXiMcnsLNfRdLMGSO188VjHvgqEo/cnUNYdJhIj05MtCQN43JfQPVNu3uR2os+sTEnm
eX0uS+YPLEoT1cL8o8IvFdsihiqpMYylmFgwK3STrrIdEhg6LyaMR1qAywWyYSbNznFwf2b559JH
lV7vh+8OI2HxCItjhXamKo7xkT30ETZP/fIxd1uRg/h+mIKE1GHeS3V+QPg8cZIfCbjlw3sOVAKD
Nsyuqzz2XaJYqLcKoWK/sXzbIXEcyhMuU9tQznPs+SKcZ8bxf13OBvumHd187KflRDInkxABQHFx
KKmUNlWeYZRruPSi6E2Z0j38ALPfPclN+zQVy1QsYHXA28Mtc1x+a44xcWj7xd9SQ5HNQv7dj+kE
kdwcdddDcvFAoODRl+i8k4+zkBSpxShmtigqtFIr3XY6uUp10WkJnW2rzaW2/Uwr6LjLdwjMNY5a
gGKIwpz3nc8Ac9Abn+Bp0PB1xBQsx4wytWSfT8iwEq+OJKDMXlrI54ezzxfAFW7KZfBGn4VzlN4B
QfedBmcn2wLCVwX7aAMXI01FLYGs1Iu6Ebefg6me9pYyRO5/ier3ujzt2bFUKQtrpsCZ0ji86O/4
rZ5R+AekzNgYtXBD2HIkEwTyLo0mvMuhmBGGAeWgSljoyid2N92dukcsxR3lE5DlxVOdD0ELZ5XV
Kk8pMXsOfErUVzYVRJhSREmnOtI1QyO0a0tQWc/VQlN6eIMy8w7EIEamwU/D+6HqGz4zrUj4I5u9
w0Bx43tc+Q0D9dgdlQzFok7YBgRdwht8XJtUaDyQHXx4Y2wCuCO32P4BaP4P4CIo/8ekVJJ3lXcQ
ttLIkvGG8Q0eTUcpugFcz093/vuzE8JMgRtfIfvxUtxalxumzbTZa3Cj3bXLe4sh8GKGRlhQ6aHB
4YJ6swsh8AxzqhsYG+SqhYXhRrlV9900yfAV40dHmXyXtWgtCGPGKQeNS4hoiQ6dTJURZI1V/xN0
0KVHdqfOQ/phWOrYfQ3cN8xR62ZLAPDXbRkYs9QlLSZizlwkrQNSmPTdVIOYJ5WNeDmomD7ZaKU6
ZIeDGUkjhRftf9YxM4Uix5FeYn7HK5B13VDJOb7VKJ0LgJvXuF9FBYp+1T4jOD8klVAr9BOnNGdh
QH81hOcXNr3FF4dNG3PI+EZ/o1S7sMhnagD2ZUMqibi9P6VO/X70HvIHRrW/x7OQggSPQ42kHPrE
es1gwh77qnv5KuU3mXMIlvtlyreeGLt6HWw0Xi2QpZMROW5wc5n7/W5UIyAnsidJCmtauhf55ssu
+TDng6Z89wrZ2aTJVbgGyjfLVeOoU1g4fXdHgJxADphp3UwwV4/cGU/ij+HyiYcR3WJxEkMsqoq1
RrPNkNuHaQjfU+79Pk9XdUCO5osfd5PugcXmDummhKP4Ohpx3EKM1yhVGrSoWyoXqnU0q4XP1nT4
C5nd8zhWSNsS7h0YrjFcVYq9hwSsN7KYCLHkoXxK0EI1/tJ5WS2VByc5wHNPXCCaWzMKobmub9QD
5tivE8ZG/ovdF6ibEcENrIoDzLK9j+oi1oyvas+PQKFhCZQgIXTFDUQ71uKAGf5yg/GKSk4/jcCT
X9YXiGSt/bUSbxdZN97Uk1R3Rbvhv55uKaWguzNmjzsbuFrpgNyY7Sdi0mm2fwzM+sfq6vob+ULI
aTLQe1LrX57xn/86v6uOTYYV5Qqx98NQxHI3KSgcy4lpZlbnaavEcdeeEw8h8G2+z9/ckJHi8wA6
U2GdeHwJLNg71ImXDVH2DAqVoEcDdinEo/CT5im5JktHOT5X9yYM3IHRHX+kMcQSiWYIbZH0GmhH
Q8nI8QGqUYTfL2L8mtEIatiADcATLPwmyZnSRlJZh0rgJ9tpmUKlR9djYiFy/plP2aTM6ILz1GAs
Y+JLZCGX4IPEfLw0GXymHGyTdH1cPAWAJgI5FjYgiIWYsw0a3iCj/pAfcXSugdmJ5HVGqB4ME9FP
2DcaLpwnnLxztMAaVEAKVrrYGjKCZFi/+8UpCVXHkquXjlqZMI8/mG5Ua7RwyNfa/rDIrSE0dS95
8LKrXoVSZ+2GlJfuvg4WhJRHh/6lKBpzJvm0OyDWbSiD6t6oywQT7Kr1BldYJdzzTf9FPGu4BrG2
8//PAKzgmjaLYPf02u3HJojM3InkxOQNSyocrSRkKoY7eDp1KZFjYnTy1U55x8MJJPYSOTMeiVBO
6Veo2+qmejPcele4nuXqzbp6D3B1h0AtCYWWHT8bz1wEhXlaIzmZMi1IP9y4iz8JO6nwNMo1Ehah
49Fu10F35dJUwJ8WUux+Bwyq/Uo4xq56ncK+YxiyYREsmyM8yB96ILXLMx7+2U6Oxo1fNPT122uJ
2F8eyjA4Z/7RaG+Y+3MqbPvnbZq9TkN+aSrnBuUdwl43wqZM7X2valkzorMBIRPKvtlVGUq5H4EL
qmsyU5ZJQG+zSBDoc/3LqJAlKDiw2lPMw4OJyRicA/nhvtwLHs23UPadAqgtMtKIGkX/kBgkP99b
NYQ76Lpuz1b9YM5r2Mgum04+U15wdK2Aax6ya8QDEk3ejGFUR+p96Mz3ktNuCbp77E2AOVhA6kw+
lxxgsXI5ju5klDtzmKWTF5uQ3xnqZOIQxqG4FuJaqqnzxxVwaaCMm37Fx4f8wlrWrjHXItgFWr2C
/dSNUwkOqNjcbY2yTWkn7HzgBJEwQ21xboPFblBELk6IMSE4MohqqboR2CYb6AdbAv51tuXLqfdN
UDZrvwF+6KLSJh8v5XYaizMy0CIVtQ1g/CcWywasQZ6j3pfpL6WJRAXs1oN/ykWWbaaNwcWpNSTq
fvIvrumpeBgNTrCSh+VuwBSLHHccm+UASc/+txe7toiW0L3FdRPXE6ApcWly9IxcMjZJP1d1KK0V
fRwqJxmEYC0nOsDMG1M8TDWFPxKQOWcmHAK2NUPy8jKf4LI0hZK44ty5RnP0abfJMagguQAFwfgC
U0gJlKDSKNVAC8cVZgjm2fC+Ii9PNX0dS8bmuIDRU6tr1yWGBG6n/TCjOY66akTZLQfyxCAn+Ca0
6VwWgUMsSzWfII/apVRp3MrS/+4CV+Y/h79TzojSM8Zie8ea8hlRVsSgmSodOonrrrDm/f0DhpqG
dr8iIHL4os4cAXXMDur6mJAsKrdbPOJHgauDTNlRIP4TNgZyNZ0+LEi5nH4vJv0cd9DxVsjLoW9G
abm0OU4oM2TGKknZyZ3ks5xgvizmx/Ny8/XYXdl6SGypb90Ih8nwHBtLLa87uq3DPH0IEftjaS/j
XB9MT0qhYUIilMc9c4r5oQGXgH800Cn/hide+4tkWnGE8K5ZIOZ3yqEFQe/HhwFKy6W+uyPouduN
JKlM7VuhHO0bb1O7/o7MBEXzV+DN49PbH52fwLc1hOlopbwUHl7yEtj+gHlyERtfRiCA0NGh3qtP
FHDtKuG5okGwWGJyoqfUUybn7sbRgNOG41u2QzmkjTzBMhbEG5EZXE7rYnhakEWof7m6iYMNz1GV
NW4vODn52ojnZQgte31UH18VuXCt7fOrh2CkGoZ+nJLxLK/4r8MAp1p+QlYZ2ZkHoZ7xzVAmlo8o
F8qtTD9tQsxK0O07pSsXhIUpqYvodpPMZDCvAAB601RxEDTrLAetYrBjDRp2lSPKdVSukeiODtxZ
MCuTZv6xfl53HR1iiiD7AYSlP8yHELMpfsw7uHY2kf2X1+mf5Iu5Oq9o+7039kAGrADlhfMmZlUD
WdqRrZxlSV3yuKk+BFBo/fwL4VWbawnue5Nb0CtdrNussmnq0U1eGQHKPSvT49U4iIpDeXqa7zS6
6yCLls+DZ7bsEsVS3z3xttWvouuAGhyQmTU+uywR1uRSVglL8swk7W2hcGGQRqWgO4PXFjXZOLtN
S612iX957YvjbTDrnQ5zsi1FnqXHEYghKZIH11pP+EGM4XwPQNP80+1gMyKYReeL+p6XI0kqjlTi
W0asyIWOKmB88DBZdLJX2NQmwcv1rE8QAudQ2WLB7LA7asqhm6Kfd+PF2EZPTkWuMUtVkmDimG6U
71gU3i/G/BSlfDtnqUdsRl2DOx/boTIE+NZrjXbURzpVvXbd5wzwZRZ8toBRLhcR3rF9gR4f+eKs
gjE0dyDk2hEv/xB7XjIMzJ/m5lx12J63ITXErwIHcH0tfq7e9beoszU6NRr7RJkvN8Stf+lD5WXA
N86Vh23281iVZhL240K+honLiMC61ZuwD3whBB84wb30oIJrMIqf1pE0VRyUcjmbFJRX4Jnt5Jes
CExCKOc2rv1q7BLAf4T6ZxRlZ1Di1G5oF4TkMIQUe24TBLWT+LWEVhSvMEr1H58K1SVWAZ8vUN1y
noN7deJew05CrlMMUERKYMB21CBYa7s8S6HFYqWVDWSNH4FHJJBy/0aZDFCuqGREX5WQJl0CCvvS
iXq2U1d3V1ApLvqR+w000fqfYgpyPoBwjRElIQ7Nq8SpMZ2pL1rgcxOaSKqSw53hMb7+lsa07srL
t5b9RGHnQM+o6eMitC3FSr0FktC/U5xP6rrRJ2VEDgZM0HYG+vo6u5hekyU1sLrAcSuFwUdK3ngE
GM0POIawK9doZDFVBDRJpAe9ufpKouUustjZYJV4uO1bb0K91CTW/j4c1mzFY1JfXS2FPZi2ngmo
YMYLi5Ip4pgpNajqZcxS3D7/aozSEcrKWEySK2QM2WnA4nwTZ/3I0QHiXAFNAg02CP5wl4xa63wM
hOcG8k9s6Hc0m/PE7D2jUufAVsyKINgoAyc1zAE2Fd69YXnz14FEcYbqNtkHbv08AKc7l3Xji4Le
WnGw394MeG2bTs1xbB8I8ah5LuECY3DBOA8y+YZVDBciC3dm6+liDt98sfM8qev06ggRJmQUPNHD
KNcdGIec+Xh2pTOZWyy6+zvXF/QIv5vpCpUa9GA+4J3B8k+E4xElk792hZ1h8Pnn2j3b/5hDvmH+
d6/fRXm01l5RloGl5ARxmcuIABvnQLG9X6nmSPUnPmtGYtb/+0m7HyTvP5CNRXTgAFsCoQptvjpQ
gcTLqpdus9gl1PWYNC7jG6iRhRpV7tx6Saiu7c0jNmcUqOcmjEtJ865n1j0wpBdXTRtG2oJzDWRl
0ffXtqxU0BlmoI8mo/RCL1pQdPLzANHd3h/QGdt/qZM0UluwzAJt929C5KYIL+cxciJEaRYX+SBG
guh+tjAKmQA/5QqmUnvCLcm1Xaf+akrvlFFq9HQOVdq/xKWvxjIyuD9ymPrDPyqFJ3cHSnArxWsi
imWsKneSBcufK8hqkjQOutHiwun6wAkDZC5FT2Es+Yc+Tcse80pl3SIM39E3VTPWlT+10ys/fE6w
yNoja3bigeJ9TRCC9jdDWlXvaNMzFRpakAwWo8vAyu7J19itiQTHCt9HtrrQfceo9PS2I229C3Ye
0SuCLA30Rz6ugaZAM9lHb1zgkCFY+Pt70httUsO1cjWVRaAk3mmd270sW/nd+ijeEkbYmgxsiDQ6
bOb4vKgXeBqapdZQuZ0LWaLIkzeXlYHjz9bflf4q1CSWwxhOya3rF5cjjeMec+h/rFpx5z9QWfGo
oWYoc8MorRppmG5h0Z0JcJfBsR1bLn/gYMpyEQZEh4geiH8JHHmk/KGVRLcC2VIhTg+YfUTtEfNh
rZzmfD3fS/Gf+rWwCf8gChfqH6QTCrWz3ge5zkEGr07yCMOgIH/bxj4tcATKsStvLT/YMj/gL62/
MMlxHPIDhydoDK7Y8Sp1/9NAL2TNW8NcGfmaoD4SaCLuIyqL1cQS+IEa93M9fg9W0VjUe+RhKxT4
5mmPa2PbGPxts/CXpqTWRhOks5+z1NSBqBDTlQqMJpHiUvsY5LG4YmrpVHykFf6w0YpW+tBCylxs
EOd3VAZ3cd2bW3SjBvBbwOznpk5bVHMW5EDapiIe5T8GIXoqG12vLGQJXncTisKwQGMCg6oIS7xO
AHXe0GihBSrCtsBUjnOAy4MXSVG+7AxTtE1+rM6jgdtXK+roxIdSIe/AGtxKS/oTsJE/eg/Ul6Mx
RIjSsI/a6UW6+Z8H6GOk+BYO3zqf0e3qOiKENHgVAGb+CBgwmhfwZmpVCcDE31Vc22yK1tkNnB5r
EmrGKiNpgrqLLe6t480eU18lZujFQFVMdtzOZ2P7OBlGaoc2l5Dm1idLV3/uI/oWmwX74Jz2zdcs
xkDSP5GTtv1Bv+BL3CqDCS4HG+JB3PhauJwgt1fk11M4VuZi8aEAyURa+OGq2d+7eBzp3CNl+MeJ
CbejRdGuFRq2VW4hRlrKAg6r1BwyePsreXuUn2pXC43zkbZJ6R29tcB3cW1Uo3jGlYsaikZWX2Yx
c0xujqh4MiwWg0JlyHYw2SJaO1hhTxDOgEwPDHtTuoe6donX611DRS/BISWBFHzfgIRZKNVEnE67
I5Xum0L16CaBpjm8IMesGz3y7wCZocu1bKY/aUqO8R0LdEYA7w4I6ojxvy6Oe/GsbO+pY9FMvIc2
hrPqxSSFhfy5GXqrpjT+5FxTJx3ZomFG3RnFARxKh8rzcNTK9/xOTiG6HlW+O1BhRJ/w6VL0aVwp
m6qwLmMMf3sybtumhmRFrisw64Gj31DE0JxrtwOY1ShTINzFfPeZongpbxnhu0H9xgt3KeoxF4L6
x1WrFzKD/zl5+KH3nvZY9Wi3auVk+RoqYVMBCMp6KrK9ew6OyT/oMK2aQpZbanQl1kRwXoHuAv0l
YTxxMXPLBbbVms3roZ29FG7L8Vy2/IFmsetv1vLPS/kf81i+/x50iGjpYWC7uNMev0EN4HJnhDTV
/73NnC8gdmWd2dE6/iSF86rG/B8/z/aX9UANp1vv9xvDqdpeyYDx3TpHPq2ORyZF5Og3QbAtvmWl
gQzCBoXBRE4gSIjjmAAFFKBoKCUS7lSE5Kug75uk83f5yQxg8hsVBL2S49ttK7hXWnMcmrDhjCO7
uAhG6TwmI+ShpFe8HXPEkaUyVMGTgiFNZ/aKCR18ft3+t9hhZZeFucuKLIjX5Uyurl70JIYmaSTb
g8S0+fzMwCLBCnD8fQd2ZgAn6QwWzjVjmQQZoatTzL662shE8H8AlIYS87QW19O+AK9w2TON7kzU
jjbR0oG3+eFH+msFeFcmG7ktr/MdTceGsWTf8o+yu6WaZYU2nULIy+deJ7psCfbVUeaF9K10yuqI
8m9q7PfmptUcTtw/ZSPWTrSzgJXmE+GCdpKLKqO2NoXcSHOlb/c7WZzegFsXeHQVOuxOeERvVPNm
in7Y+8EXtVlCmnz3pa2UNv+UAaZrBqoi/uA3VGMncDbqFsJ3Mw6yt+C9ya/jGYnnjyVAA0qS85Cx
Rx8rIbRCeCab30CBdk4xeup5CR/hJeQcnb21lB9NhLe7pMWNsX+MxOLmOCJuFgZKMdCop1YeIvWp
a1UKYUThJDQriGizB4/L2/Rxp7nuFvM7fX5EGYxkppRVElk8OKTUmfY6dwT1ZgU0ETXUdVSu5RXX
U2LVfRsemz+fbOHRVVPGo98JoYmK2CfGGtmWadS1Xe7GVCeCyXgYoMsz3UyK12QnQrr3t2gd0UjJ
2JIPS2yeCnObEp+r17JTlXE43f7iLOGY5EAeke7EoUxXb7BIAJ4bdfAv7b3mvQ9i1XA3V2szZy1s
zIuPISa3E6R/MGcbyi3FSGJEiZcjcr71wD5hF/gmYLwIyie34cacFyF0xX6evPoTDBqGyckwaeac
HqcIUBs1aiXZIdFUMfyUH+h+n7I4rJAZd85IzkMONub0BSO070Mdy6zSYhwhWmD6+JpMLB4pOmqQ
2tBYNZQq4rAa9VwrM8f2+xMmTj3wow6+3QBtN87LDvWmvwmIP0ogB3F+X25fJERBUHcJYaZb/fd8
eoTrLPaUQdTInkbunhaCQQ2TVp7kFWAb/7nOBIrBGCn8yWuK4A8Gl5y/EZ32OzzHbUsNsFlTQPtw
NiIF5IAEM+zY+J/klJHU8lxQDNVqoqgXTRqMhResEKbhIjANIPF700VK7p8U/tu1ipt+AQrl34iB
rq83qc7eMVYDx9pu+ABmutGU3w763gWcRVwN5cpxHHH++CVfnz3GPnZaQ1wuCAjYuC8Woj08SXvq
k2G4xHQ1aw5EqLEstRx9DEZXIRDVQX0sUd3uPP+DQEhjRNIcmAW8lXiCozw1OaZyZQjREV7maijE
rZS4WOAJViuXDjdP+klri/t/ifm+KPQj1wNEP1LD2ftAaNqnHJpw+MFmmYuPDdG0qugxbPBxVzmO
MlxgY9RVtcndXHpuQPvBYmhizA5hqWSvJh5anx7OcmMau4A800BQWy508ItdC0J2Xzqps3oR27rZ
jRjjhNs6qS6k5qCKjZqPVSx8F1662CfZV3b50+mEua/1wAZ8JdeQ97h2o9tgPLZky19ZAWZ/EDzu
zLyw4MGG0CNGdwdg9/NVKvvlsGF7JXR2UYG+KzFfNXpOSetZcqiZBTXGw64uUg+16E3sVouEL+CS
SxwEOPt6t5xSVMlmnQDDbAO+nsuwSQA8BmT5Wp/3YGLZsXvRq2w6JWxtdYfNebC4NL0iKdgLmA4L
+nCHXong/zv+eLr75sYUnuKAZsHk9T5NhyL/IkwZ0qG+y3KII4VuTVRMB5K0ep2qjZeGgYlZMayD
KHZYYWZy9l2aEVB/q2RroKxOsnj5vexR3S9cxlyIQKy/sef8hs5Kr/zOCDsT1TEq+BVyVTuxY4wo
qqyx8ZhzskLmb5NY8J6QCnzmppZj8Hk9tlTEbfQtVh3sudJUvp48Ixp6t9ReME+dZ83eb4K745h3
pQBPHrZR9KhQuh0LvdRxv6D7VDAq1LO+uR6J90dhfds9Y4Nb0Wntjg0bLOzQuTdLrl43y8WQV2a2
6d/AFeqmak+bDurrjxXgON1vyX7Tx8Dnf49HEermAQ/D2XzwT3MnkVew8RclxP1NUcdZ11sri05K
F8m/VGOB+JsptQX1tagoUYk6c4FU8geNc3aw4oF4VHG+kPa+9r0kegUJT7puelJPokU/W7qcIrd1
JdTvkULSIlsgiAUJbkzzIzAG1J7zqZrvMu5X0kbyAn3b2eQ8n7l6V/spYG/VNrMZvBCpoPrvC4+b
PKNozGFDno7wbTZhpKd41QC1gQQYSsqMWtqcfikAahtI3EO6Bc3OOGrkIpVNXwZdq2NE3PtVTmC4
C82YLKgql0B5x2iyWPwehscFGP0usOEd7qo768OLIdGQtq5Hf0MeKsWClftDZUjclUd7o9qcNHHa
57uSaMHHKgr4gbbh04DGLpSqO7gEdfrRhMiwoVT9V9Tv6U1AewRafKnSfiuIejttGXpnDpDIdUqs
G6H+urWCV5Wq3Dnf0Vh7Jg3M6WrbH3dGlmGSBxUxGTmG+Hmj3LNWxGjJCsUWfTMEJrkd9+/4aN0x
xmv/SyHNONNGqZAsidiT/P5w7Ebr2mMf9jWNSe7vOOq1ju+7Oa1sJyHqnRdUkCimdt0jc6G9klbE
Iyadq6MFLcmRrKHrNnDasvb9Q3ZmbM7HL7N3/m7wLw2QlP1r/77TnS4rAKUbma9utIRYwSE/pKbH
jcQ+LtEP0I0hR/b0gDrzTZ8BixBkhQOuGvmW8N3t3o0nz5r+aheZYPsfSVmK7dwM7tRSXcbFcRcN
oomcHJQLip0lS6FBIxByPqVhPbmaoCJ4tU+3f18RZBelURyZlp3Jjcc+1HNSr52jSzI1nj4lB+Ti
H6LGATYmI3eO6HSQTG2q45xGmyI80RALNU6wr1X1oAjkzbpTLHshO4tv2bkxertjA/zLgwa1Dggd
UtMFAiyIMlCAZ+ETXjyZjGrV02KIeg6oXspHvOYfoQIm88/OBpmUaMAumYYZ1frwI47QT1HZutbn
Ppwgy1OZTNh28w7H8gJPLh0rD6U53HHekO1oniAMzfRRhsJR2vNi33kJse+lQZWR2euA9R1G3B9f
QV8F0dPs7h8u/9fOsBhafa20pMhKX8/UoLgtBzNjR/hsZTv2fPc872upMFHHDVnlqLJ37ZOPddKg
z9IK16g+CaMydnlC4cArSNWI+lS0/b8WiqQlJDq/3zZGGKiw073UKjsv4s82juqk/qzjeC7sYuJb
fXRlwVnz+Ck1yKFaSvfrZhR2giJGZSI4Tqtob/7VVKbczt/5Yw4mIx5/S/Lc4eWQL4o6FTnibSMn
Sok0D5gNFFKlJ2qPZEDiyyJo2E1nVW/2yI4/BmC4FXBrj+uZdJBVgEx9ezSAKN9Vc0OMCQ8N+abs
fC1dXcb33A9HICNl7fbP6QnjHdwgWECk5qV2qlSBZStGAdEL5DefKhw4AWIuwRNAIvit7wxjvnFu
SyTSojghIcg2gskyLp8zwqkhDkSdbr6V7GkN7LW/OTWWI5CVJYMnmxvJ1LiIVXE/HedUoAS70L1N
+mvulKni3lT9cOSbLN3AW40sTI/Mc1zHE2uVN31bs1RBEvkcgXoWRgnYABRAiHrHpAfnqJxwNE0y
83scVzM8jUrU6SzJJUNAy8yzvDYcokYUg+GGcAJKaffzF5eoOzZIasgYd5PUtwFqeQhRlaiCwgpS
xu3BaBYdrNpOkqZm82R7BbVMJMYYIeYJwP/AL2Rh8TkNchwUwsQTGOngj/wmlpYO96iBVhCNjCxa
k81ZeB1m2Sk5dWucdf49FTPIbD2d2k6XQL2+XyRaFq1eZZczIJy0JpSvQ9e9JvmFY2fPm0N5mksZ
Thrz7JQNvJaGZX7hM1VLqP59CxfQ/3sMXgS8Mp+wAFK9V2McbahAF4/E/YUN8vH88lve1vWzwamF
97y1vIlFJwerIxnpiJn8AetOATjZgRNFg1BGD+5PXZpT7hyw+8sNipbfqWPV66QRFOei1bcVnURf
qXh7V+ZZfK/yycJEzCjROVatf+c5i9IR6SGtUJxrlsMgnAGZli938aWCZxPQal+4BkrVzJCadHlI
+A0OaYFXnQV/Nk4g+TLRhWvqXAc+qRQ2gNw1U54YLNvGQDW78vhLqSr7p+gl9LlHYOSi78DRDKqx
L1P8aETRKXg7db6crvz7cnPc4nA6CxLYDhcPemiuPatQf051/siIoAqlA92dzkT6OHRp3jnTCL5u
/tFIGVMOUNPkL8D8M27c+c2QkQdrZ4dbRi9GNsA+REx/aZ4z7q6x017FO5C59LYgPvfeLjm2upro
zpQQwA+AxA9tlMGmHALAlifLB6h8wmAhZXhiZMHtpOqMsHnE2LpEsH6IO3Ucye24zSzD36OVgG5E
3YTGYn4jVriKd44cs8s3vLPFduxa6qp6im0mQIXA77x4ssjjySmE8H4G9XT1XcX8epe5zpUgGA97
E+v4UYCc5ANTW1yVXpEWowXDha4nYAC6FXD/cU7eIGppIdj20nEjJQYBVHVIPv8SzO7mUM8XiLo/
EB4vnrAo73oLx4tJpy7AXcDqDFwVRNT/o4tVYXZxyl6VsUGlreDlPdmg9/ezkCO77YBq12gspf1b
aRwpuceu6Gdfb1YcNLNHJVZH7kJwXzO8MWzAm6TrowgIex7FoLY06LRe4ikHlBNbG+1iZbkNoddl
Lb3MiI1cPrYDR5gV0ETXzPUG2grN0MDhnY9gYgl0bcxBs1+jgdm3Van/DzWl4FqcFXAEshSzruF9
9x00r21PUndZK3bo3wtzzauJ6dixXnkQCW8DlHgBMU+0vgJjuBZTQJ2k1Sw3pnwjA1VzWVdBCNyy
Cpqh5zUm8AzaPgPlCCrcRdgNJ9wLa2s6tZaaP1iGrKM/x66oa9ezHqKL6xH0XODrTF6AvKTPNm9o
cvzQ8abh0p+mobXZZaRnwiWP7w55PBpmVvXuU1v0W5JvSxVkl545CujcTma+DtTuAkU/uLU2I4G1
2ko66DxYFTm8fnibK/Hp38XrwOiHjTfBa4YeYVgGA9X+2ioiCiRJ2nHaB78qrec+PaYjIiHU1CVk
JktYOIKtvKDI9hTC0px9Sq3vp2MRDofOk4cKHnTd6itxgbDvjdQXwgUlbZChY2LSv5Bvuwlvtlsv
67IfAEoa+i5B00vVbSVht39S0UZYt2QbWclunvgGH23wTaautYuaGB5nI5s4yAVaSiECyrUsjds1
O0tQgtYHVhtVdzQsYsYWsSsl50uU9smGAbbBuilPT0nr97wjhpOsIXvvDeD8k+tT+vk6H0MTPRNW
jzVR0cPAHjV4QWOZPI9vZ+N8Kset7DWnPVKID/1cljiYx1BWKjrxBYjybWSwYju9x/Ei9XpILz7M
ELYeA1j5pmPLhUuo6S65QW5sE8VwDo3SGgznwl1JeSYXeq1XQOLiqPHQcwRNHfrVFE2bvXCiD20B
GmoSaycNPFeNigofBM5iCgGsnjskwQ36GglEByXj40VJaXPbahRK6aLkE8JLjXTTF4s/UzZSuTsZ
Mn2RaeZzL4m1ZnKYdLJzGTgxc2PqowVL5D7m8QYlL8dHnGSW/I8zVO5gE+U/9Z/fi7IsT3BWyc77
6YwRqejUSHcbG9y8PzogQ4pOLkx4+1EME+gxh0Y8J1c8w0Bz404kWdjjsLvxRrBHgRHncKWlwo/o
ZYOV0ddLHHmDMwheSbMo9liCG9VcQsXDLRZR5tHSmGsmwZCpKukZS5SYIGXCH9htwC9IFJOYLHjx
NT5p5GLpXXGyi+bd3pXF3Bu+9NtYbD6+yxU3wxcpftsH8ZOQe9a4Xch/2eCJ0fUtSkFNuwglESpq
ejQ0pa7Q1JuhAmmnXUGnfWuoSW0NXr9J1KHeXyo1vk+FZHdqhSgr0eY8TxmoAD/iqlHZe5brGFIl
hZQPtWHS/cdd1OyPX9iJXC6dspGD8g7A0kh9V7tvegnSZkoTNvi3joAfyXPvhFAIB6JM2kO2UQBo
eAYGlAVGtzgPh206y1iND8IK+9FyH8AWI6PWzGqWdeCQw8TXdBuglYiEFD8UGjaBk5lJvCcn+t4k
I6jZkKAEANyYTIWNBW5Z32IKDum7/zFXEs7d5kGmgMnip67b3wVWW0udiL3tbqdSSfm8q+Z89Ma8
C1a+rLdZusJkQysoPEFHdi7EoR5l7GzmeV+xhDwycnZiBp6UTT7kB74Fuc33kwIq1E4/ThQSkUwr
+ze0HsGiGcrmRzhtnNwgj9ef6GBve8GGNbyHoIg8IqTOhxoVpffv1L/IhcteUZZZ2Qrxf6AX1eiP
08sLYo7bBki3fISyYgTMukppo6YDdmUtBklLkPzkrDXwWQuUx/BpRFMiWGLjLMZ2OehBizFfiqv5
1hWkS848xOc+Kz5gN0RLUh+6YMpPY7JTyryHqlPcg8e7dJnUN6PQl04XCmoenzZ+KavYw+UCGoS6
bFLbK9RVerwnReRjy4HgFKVM775ol3hRxUTo37KPx5bEj4VG/K03+XGYRD5sLL0aT3MHftNzD+gT
2pJBjQ7pMqauo23g34/1WEv2HX01RgpCA1T6wVf8lSw4EmlT2xUWI6L5B7EX/3Yj7SozijphaAog
a3LR9jlMh32H/eSyDKliaI/OWOt7RfSSiIkZ2RQyBBXc682vkJMa64R5xU3r3GQmMT7HIr2P755N
rixD3zORMCevA3pDBpw1CAcXPiotM0XPyC6bKfM1lm1DSNwTb1eolS7WT8nBvd46FRExKyLWDRaN
+YJuT/IT82m5EEhI3xdBWZJUgxxxnFGOazgu7s36zYDmcIGcRHPrYYKfXh7xf2bYTx/eq/j4+Agh
uJmk5sTnAdp6yZz+rwHgqPvc2bpFho4FjFn24wdUO3G52i7lQwsr5JIxDBnI5zBpmOsMHHwbifb5
43ItHD5RuxIvsD4CGigdKvor5X3PRRz20hdX+rchA9Yg1VcUtZBSWEBJ+TmM1AdFwRpdyRVCldly
3Dl8KjJB7s9w5Oacl0wolvf6p5Wt3c/PCQugmFp+7Ztdezi92UrQYCtRZyrDCkrQcac5mGV7GeF1
l/26ry0AGdG7gKLF3+pXypAgYbFNXAL4OUPoTevfjDeJ3mPOgNZwz6srDkNjrbtVCHVjNC2fry+b
9qOI4ffcdeJJ1H4fs0kChdTbTu/KuL1K6mY3wEtmNx2kggHproynzCMcI7IQzM8/JVn7Kx7pZQzX
Z05Nr3h6Ucu7A0N/iIPS1JM9aJgRtWKCgNRy34ROF6AIieTAQMUwr2MgqZX8OzD+sDL369Qc0lHK
aKUekB71DbyABXjP7kp5SXFH1z0b6BtPmnHAbfxS/uNLHOzIUzVACDoqR2ovqzzV3OZwQ1Cc0C41
X1D0oTverRCdT0fJ3tUAgKNUd3+6xR79N2Nl74OuQJn22c86abm4krG0HUGv/pnPK09/eWVOvO5G
tXwPDsF7ZXNKfQxxIy51cL44L4XmoUs/92aSbDBbLKNlim1xxxRTcigO0E/1p629WQjqZAVWhZ6M
QmPg3xYsV/6chjVGQh+HpYITa0KgynTY71oek68NF5A8WtE7wX9Wy94NBlffL86uNiXjabBHNRk7
5R16EU9NGu5n9RYZzszv5BpBtxsqwExUuxj0llr63zDbN9uxiMdCgjWAiGSMvXhOcpH/NSaJxyQG
DnQ3R8E8UiwsYNF8iIVxhqTLVwwUm1olh5dYl53DzND/BnhU+e3aSNYnVEG/rC9TXYL+Kqu+v6qN
ZE95ENBhdbgza3fNmMyTFdnkvI21Zx98bEgP5izbaUsfCQRXQJm6VgdE/facNvnfbEufLBibjVY9
dRsfabxBYoU0NMThr3urp+WbY1b1EEWmpVcbAeJk+BbJC2BJ9tZdLsW2E7+z5okau8m2AAVBAp1Z
nz7Nz4qdbOJ2jcMIaKokH+Gk01pN86eHS2K0DwmDnys8HjxMXi8Bh8TL7CzipSGu02DBCBcFabj5
k0fcJsL1jKfvkBjA/fF04ZjZSyYYrrv5wnBbfHpzWTOoV3MtdFaiOHMBs+nT3GZEvxK0P/KIayZe
ZKr1DnprYBKctoEHBLfyq6kgdpZKwa7hUY//S7vTgOkB+5n6fRbu97dXlkXa1IWxXEGO/f01YvPG
yDKtvVLLJO26fcHg12m242ZdpA6aFQoO7AgdJ9pHBf++1IGIvvMELGOJ7dw1EKKfJ/jIyWgXjQld
BNxgPZEHkNxpR2d5j1n+r0+kxfKBSKVhHUvAHt5S4VC/Gwc6UUMN/3vvv5Us/PZh+dzVqL+Wq3kI
Gh5h8x1R1YNJPIQdlpG/66yKfu4Jbg9ABnVWARF928XC01+LMUCQ7ldyUNSefufJAmoRCfTabQyz
NgiQBkXhz+LM4L8NOruz3gR0C4I9I7x6p43szhySfQexwIoJM2ECgzpmgN5mJm5mMULraPME/EGk
cL+chFnrOwChg4O500yQqoOvo4Q3YtOH1PkqH009m5cB7uOI/GIHH/a2jVOvVVG3kW2rojMMecOJ
YSBpVLsMYzkCgNFOfqGXCbbr/uFCQEmAwrE+o2cRh8UZeODSiUIlE2sN0URInSACskERpBoqQtqS
eQVQrw5KAUdhvfyKPhublsRlVwCJOrbnSYXmP5cfUD00oudx5CksMkyQghET9kiWy6+0paNqBVf3
5p1SHoSHpIpA7CmJH4qeX/42cVXcVy7CSv3g5vdLmVFQhx9hxPTzvurXe3v1Zj1pByo2ebnH3dCG
zp56aOpXtElgz//rgaZH0jqWi4t5Nea2krNIgu3ZChr7bzc+DvavZ4j+MDXZpjOuWm5PVaiwyF5s
s/7y2FlizsuvUJuSdDRTrPST5BbJTLJl2JSqFwltkBjJNp7THq2mIo0WA7NqwK4EHloF8O1Mpqwt
GZ1VuIlfAzFzBpq4owvWM8CO6s2XO6J+yh4OHmAMkud0W8vPQiM9xzJdvqSEmVKUb9qb5ynylYDK
mUCooJFPaXR6thyFkYAOADV1LtXAByYRgLbliHatPDbvu1DswnuLCSX11MXYtz91RUuBEszYzjHK
/J3l/EytlTfr7bLjlAyI4CABovXzB91l1FsV79r8w+AzKE4UBa7Q/zXgyP+M+28S270eJbHV56fd
hhY+IvJQlVwq5k2nEGiawL9Z1S0MDvWRdbp+vVfs0gYglbQEdIZZT1X3NcAR0lU/0qNixpd2xPHR
ktxJorF8VLPGuB3hFxboRmQT1L7ilKn/Bt8P2ZoQyZIvZt6jiseYH+uLO7+fyWLiKkdOB0BKsnLe
vPPCJzZ4Vtxllm61NK6eTMCOlBG+0+uUUVqeLqU/zDgw1ZWZjQI8ivdt8E5xFYjEVK2Ttfj62Gnf
9/D6RI3MHDxkKVLoZpmsPTWVhyTI6Ydv/FWpOS+QMO7o+dtDOjAzQ4soh5PnB0/nj5JmKToD9kBs
n5Mp8ImNOrfZ+wBQef3bBhz20d4ZR+uHwAcDtpYFUzR8aD98yk7IDZoMQ90bDACItq19DkX6FSfH
Mj13jUyCoUWMLhDnNXGRI/JOi/bGYVD9jtXAREl5Uv+31a/JcW8moVV2C2FKT5m2CZ58aZMjVTtF
YR+4w/cXHAjtWS6RpbwR7IOumV8gieuzGPDTHNS+QzLzJlMqfQMl1JWjrASdFc9cAQwTHh61NZc2
v06D9yJfRGTi1ER2yslBx1ViQxKQGq/2iGfezZ0wGDylS8fnAnXpY8cWt/+kKtRjdpkMIQjwH2Eb
VoIP0yUaKbCPRRhgGzRkiacfte1SahHq+JVfxp6tqyS7ToPUhW9KAbOIVfa1eL/vEe45OPQoECAr
ChryB3uRW58701Xox5FGusoSQOE/dQNVMBelG1JFhYpDmus5c4KhIGeC6SOm6DVPtWYlnJtf5r4o
7A4dU9FihPs3tuOc1Ftv3DI1kAxMsgtgqlTOfLVWdbcTyTAaIQx4xfKED5KvdAWIX9CdVfmPtRJq
k2+YcOJoshl6ZVBRQn0iFl1MIOwrH0chp9eQyydYXtCXv2gGytPiaHIyfKxNtXCxL6UzwyUBwD3c
FmNvClA50g80XPfAKD/gWs7f0zpdAeWe2rDsMayRVtdT4OlVRfANtfRKTomjrSXeFWkXDfIqGL8o
0f3VYQQuCE6PDziWbFHqBlFU65qi0Y4/cpS3RW3SUJ7z0MDiPfDMGv5v+wtOqCWZYHqXc+kQFfAN
SHYSSez8GaLuOQitOTVPssWkqB1f9t5/+wuyEdObKezAWUxZ8yfrqCUS/Lb5I0FJE3ecVjUFjv+v
6NzddIoGCgnTHAuURS7eEfJAykgsJj7K3bNAOZ8MbGG0UTR6F+PM+1rZj6AZbmpo2+RipG6iofBF
McXkEueXYnZNJFbgMlvHf3rlqVPc+LpsJ10s0ZLbLOlsP3lHcx/dfHKwShMaIfbt6NG0acHpovE2
2hDnyozk6uJPmVXfPkMq9wEA11KKdh2n6+Ix9e84TWaGC1ljxq+YLo1nHn3FggYkYgVwQXzd8VAC
rI1neZmE7HsZTBZccuER509KTeCdJWm/NjFb+BjneLDN/ZzvsSkxM3ILcstcIIfjDpOu+vF2EF/0
GGWWhu0A7uAJc27x/muW5OV0EUr+q5ulZ4ERsH1JYK3WK3BfDs/hlb1nZXiZjbgJd9aNd7Mleziq
wDU3vK87S34PTadQKAsobA923/3Qc6pInLs4t4ALXiZBewPxIF4JVyqNso9Lb319R+be+gl5cj0q
PlQvU7p8WA/IK2uCA88wF+8oegaqjHvnIfSgyubj7GuusAalPa+NDYJFJ7vx3HM3HYgQsGaUZqqv
wF83zPLO01opb64lq/+x21S28Bih649OE+MZS66HYpj2L2+1i9zDUYcTcLpp2kCAHe3XWZR+n9DD
eNWMFbPwaT2/mzQPsQNE9t13KgoC5l3X2QZa2TVNB+1wbbB7hSceYO12ezgQ6jt8CA1E+ZrbtkH4
ZIT4KZrwy/spBJm+Vep+q+2zVQIAzdl8+sEXe2Xh7biE5M/bAi4qhHSmI7DDjPu3psG4WR1KggNF
pagEVlqqYhm5hOKf93rjiJ35TTOGkTktE8mHBATEWOv2q1GZp0owOh3HkihT7EtpLvzPSTHxKnKY
fHKFAKKnDsPgy+vi19jLp6HJl4cr7lUSzDC/B2v4ucyyX0IZXmb+xvE3yVHgDMBige6/Ojw0oimb
zIFPSgD4cjMMxnpNlgKdn8tBvFF3jRpKybXJsR1/r6giHI1gfdLYnqqQvI5sgX9LedSb1QJx79PZ
1QFQ3f+AoR0I4zJulOI+lWRwL361OrQ3YddlfHxaLFKCG2V/MQRqwK12ty1XPEVL51rJpG+a3tk/
axoAvwTP8QFHqrrGOs9aPJazzOh0OCHoeKhjn3oLCh2b7uE3I6Vq7PwWD3MlNHbj0ETfenK7Kx9I
suHUYva/dKrM5cX+hz5AavbX7nMCI5pxI202n4GFtlZEwRzH20dRN17FYJbf5RLAP1K16qJV4WNm
TiPxVM4zkSwPF9i3tysugyLmQ1sV/HNiwzvyrNayngBDjFYCVjoWE8m1du9//429L3e3PwOyeTUu
qdlI2g5ceLwRqHoi1KZvtZJ5oSoqxDZ8i6lxc7t/UEXMUoEhP7/n2Ym9wsy4Zf07dxp9RBIdK2C3
filUErbE7PbKi1O1IGy2Z5WRCY4AUS61stFXl7WcGVDWrWgWRLXhT/NVj7dtOQP8r70EPByiNRtc
HBF2Gisg61/JnuwxUlMqzv345o/RdU8IxcEkzMo6Ey4CBcB9T7q7gAHuM6M25no1zYKKmU35Fski
8J8WRCBb56Jbtzr4XNTF0q0/nFaELLVzc1vWXsFmZj0ptnepIk+jZvBxb8PvBy63K1F4OJCljcSf
7D2+E2lV4J5ifZAfYznuFgB/rza6phZCmDxYrRHUHoW/NeWb/V1ucU9GX6XEE9bLw2qoMXiV/PmL
sxcCz/TgKFUBl++b3zauacdPKBdF593IQY6DGyDUrneqlol6Hm9b+WX1Wcq12L0/xcO4Bx8oJoQy
tRMu+OoMvw1Oby3URnVNKNgleY7ZSnGm7+aDtx50PpZJzotzKg8wscGXfbJ4kBjC1Ac/qb10HPSl
k8ImWXu9rdmAi50mPvdGh0PJN0Vhpxb+TqwZMWf6H5eAujhGI+8dLPiixR27QRCx68gVzPPWCa6P
lBfMsUAp5dd5WkI4Foq8o09gooA9yk9/IQDv/UJC3AqvaU7UfKedSfD0nV3D7KSg7r/FDkBF5uJQ
NlvJ0VNzsXTRWrY1/k8KtSBla9dd6CoL6fn1tDghxVCxBnh0+zWCiaMlfWnrs61eo80axLsWrUkd
YGqZNQOmsTqt/htg3pnPCd6sJAvgIZAi9LWhOT9La8dPtL65JdaA9KGfsYSQGiy6Vbwbqk6fmWW6
kKKd784nPzgkz6767ktzdHSQrb1JySQxYxJ7JvqeKc3ojTfRA5beJ78oUYTWHon0xUg8w8kUa49c
OWzM3YJLcTnT3p/bh9FbSc1cWuX18uNAjfbsu7eEox0gQkiiG0uGQgDg3sb6CA3LKkMY0NQk3VFg
KBtKImysk89BC2gz6PlrM8MMT6MEVAtcSsQfXlGMZDS1fvhjXnB5imH5aZB/PhU0RWB5UUbfsji3
4loc/PvyTtFCunqCWlt1qcMgbSdzRit75QHuP353ZMTLFCuVmbwBW5JM5b8oIFNm4MdH3RgD9vRM
D+BrsqLMAr4+s40fgi81AThUsc4FvchbTABNL6ySle8LDxgnHBEv2zV2XQOKQFBWSDACA4nz1Kti
pDYzjc9MUmLDBjffnmIuGPndWWkwGAZBrnLcVokzQMVowt/hWLt239CFxTwpKbBvTqJCilGy0aMl
Qk8hzq0MGmUzZJ4tXrb60UFphKMXPoQ3a1bzSEPyJWTlAS2UZaU+nEZwWZLyHdpyroqfC2Ea9Iq5
2ExBjDA0L023vVfXrwtfmS3mLyOuulPQHdHGiU0iCELJSM9Lvb8UucvFeki2w96nHy1lalfkamHR
zawzUephbyiD06g5GcS3Ck5DVjuwe16GlN09HXCg/qt68BOP/khonGY0yBH31ciZlmbKtRC5esAa
US95+Cz4sYnYiUo88wpeB+ItUXHo70MqeE94JgmBeqCeSVwv7sBw9FW4PiUX8IHVnJVenegERePf
YJrVACRAq3TQ3NV7GV+ljqQaujJ5yZWyC6wRfniUBsCU2mTKDeMdUuaXPRid/DEvu2GY3Q/N1izL
t/R1KiZMfi+3dbS16y1AWCCbK+Dq3jntpJTdrrQasCf9uXL0lqyRAQuscm4/LRbgQo5nZni/E8GX
7x09K6cfoWwvPIGKJZS8LC5/DsEH8pK7ahAeCFEu+BEQB2aALZfhjrUukHl9ZT4W45MOGl4abhCE
+7QjsVQPxz9+88uK+6VOY6/5a4MZ9j4smjnzyfxxeAqFsUOKxeM3WZpzMZathYEvTcWGCQNnRry8
RvyGbIoKf+/hQI8f/Tg5QTwTtatg+Y7q5+lNW14wEV/8aPyofGxm8u10rZzIhtUbgf6+twn3jQDa
QKQ5moXPN5xcoa8cKRlZLIgZQ9baY/RfjNnDTFrCTDIV7ItFgC45nOaSVI4UaeK/1l9l9m6pC9ri
buKjH0pJE6WGU2RE13r4jN1rfGwOieoAstCJeg1kq2fefsfswC/m99CCgvMic/he1hkDTbNZoOnz
GoDcmcfsAp+CgsUofVyaNhNtKxWW3iP/j4jLy1JwR+hN4zI2orez7uxgoZR/9fUHRIvfI3Hc2NUn
wDyt6+nd4dmzkdsqLXbJsvydNPuoHv9bGEuQAg9mVReaAV5Y+jOPB1TfH8n+tJgQdTbs5Av4DN8x
0CXEnKXFyjUVUP8rgp4OT1MTrgpTIhXjSPeX9XH2EDzUfFlC3o11C3oCtd6yovrTchPhx4ma0Nf2
+pliALbKMgPLOobmCvciGdnZjheQziiqdIpc4ZCsHMmnoc+FMipM5y5Loa+sykGzzkyqGGswaX8p
ZmqIVpbJ11I5R8Ac08VYQDjrtQyoqP0pwUk8YN9aMJOvzxR7SeoUH+QW/eqBLkVxEK0FG3b1WPLD
jqFYRIMDd24KMSjav2CNDBFsfYiwYrey5Mk9BQ/AoDFZhncnHmz1xs0keoRM6o4GP45tfWsE3alu
AN90coQvc4Vu34U9Mp69toerrfGoviRKrJCOo62enI5gCJmX4MMPiyx+gwSerSJzU8HgFk34EM38
A845CamY+x1EzWFIQgKqywh0THIJ55tW3REbCrVWuuEB3M9lFqjNEs6b7ulaoPCHBkH6IoYJ4NUQ
HlKsvnIeb+/JYAD+xARK4Eoyf3rWBT4gUn5j4vg6u2888IBkUDjxKzLPd9x2XUsHu/rv2/j5Zr0f
Q6967B0sXBX+6KXMi22MuRwGxBAb6LyHlmy2iYWNFarNIMdIXPecFW6R1aGc5lKai6sTr3HrK5pA
+xBij1JYUxsikYE8dRF62F0T7a3c9UFNCtEBIT2P4DNfICHxfLhIM9DRrjEkTeQ1kb2/TA4TNZiJ
P2gS2r5p0txtyEHa51d5keBx79JUoIdIahGhKPgrxauhTV+K2pPqbExI1M0q20olr5SdpFJ4VfFR
hs1SOOuPs38hC6g74d0GqDxJTqBWNV1lnBx+i52emUWSnobCwL3dPvH2ZJ4DJ3TV5LVG6dK+iZCA
J/tBhG8PJpNr0Hmd+kcqSaDvy0SzulENsqW+AIQtqkTNadbaSTBPpbytcwAv8HgRrMnIghaHij6j
DAbxJBRkHWptHWWd1QPIvTR+Dyj9ciarpApHQSS7nQbw9mGsCDDA1DqU/jN+Q81u1+qn8PhFiD3L
wRQBXS1jg8+2QV1B4Id1Xbjo0fiCcO4ghLAbROyjwxIWlBTjm+VAfWBOELs+SJGvxmxL+uP9AXIE
6BzZFXQ7eFBed+N1hL1pvOUNOkiJih8wz9pDOe9FHqa3NSa6CPkq08eRco7w5ItifnnGd4dasqGT
UIHYckvjsG58Boghy4SgnpoErCC4dSb/4MOnypXSU6W99O1JiasNmDS2skB7RNwWGHXN+3dlFI9l
/DdAekwnSb0ozbxJXMBcl/Cxa2lrZP38oWM761nlU98ClE8MipAMj1YEKYC+z/Accf1WMM6HYzBh
/aP47BpVfqGXT91hm++e6msyF7J4CekFtJZZ9IQ5LePTvpoIXtL49QRYSoQ//grN8ivx9XFjVFeN
mPzV3ZxikKnLPfZTeffzVI9Fy2+vST69ZascjCFaHPbq06z0VbqSF2nL/B1vV1+kwxQeVB+Xa0AX
rU8PSUxynPBDAcl+AWfN4b523m6nGOxap/GCTASiljMq09XSZhHKbUmg98lQVxkmn3eO8XPmdFG3
G7yFjn6rwJr5tb+AiMptS9eey9TEFl4CE16p9jv4o+hk5o2t64K9u6OtvJ278gaf3GVxT0QrOWEA
CZZSd7Y2CFoNDmtA536q2Yy7FGOcsVoMCoy+8Ah3sz1Efy93N3a5uI4Trq/1UD5Q3fVGHfAi0XbL
ISt9DYK0ICG4RXcYzRYVC1i1AIbhLD4QO4RN2VmN0D2nLPvgwjGS+Qca00xsoH3qcyHdCV8yiq0q
L9xkfjdPsCdsiqR85DZoLl9F/z/Y7bxRM1i5RtafuxLb9mZYAJCdh+pb1d1BvMbWU4+hTCPR39/D
Z9HgcJgMEyddKq8sqFgWDE5zc1w7smqK9hO2Ne05hgj4DPPcjW2iyntU/e8dbRe57z3fSuwZ6opj
+32nZhT1FI4AeDdGxJ9dV4wMpIEAl3ogCuMHGEYwYYMm4/uscWaOozwoaNAe7dZprHojtAHBuIPb
4r9uodtRbKQPn4N5MKUste1VxAilmfFU5Cz1mRMNrGnjE6UouN6VRJpWETBmRADgIqlMb3L+Mg8S
HTVj9ifSBcBBBgVyCupupPqJfBaUmVMPDqw9q9AF4mkIexYMXZQT8/ZMXTB4os9b/gCibnkD6iSr
ewckX6ZYpzewqfJv94SrgABGlb8Nhq7Nh2xyT8apw+TIhVnQw43yPjb96pIaBWbWCt2GYPRfvS1S
fhokuYNLEH1rS9+oc8iNO05cXWjDSbvF5NIjVZ60GTZgQeRqGEIB3c7Q44N1g+U+QUUCV51KkuEy
x0uCQ7na6o9Otm1OdTUSNZT/haJzohpUfU3fy1ZIJr7Z2PMx4l/5j8AahVE9pf3xOAudWs81hHeQ
MDPSPT4crNjsZnxkZj3M5oyHzMCVOfKyF6JuoL+5aKmnl6XJ5eiNp6qfsMFODwGYb6HKqwA8wr4E
xAAv4p0qq7bgu5gbVJ7CSE5tPjnwgxW676YD5m4hF1CBUKtAl5/9/KMAHxpLUIA0y5F60446Fvm7
Fwp0AzheALvagvQjG5qi51X3elSa504ab4OLAu9nvZiahYV1Hy3TbUIHSFp+0WcpWYyPpqy0mQw+
RDpYjdZSPuBFoVqiVJGsD0606D0rtdkmivoMv6QGF5SENtfiwBo4/ZCwJhweU2gCq2Sfv+P1Y2Ir
cPXO+Uckzejp5c6uuaCY75K6CXtXogID8JQJaurjGEXIt5n9hinEjQeyPjawm8mmiRjGFyCPIRUo
7542z0/hsXmKeBjgj2Iqc8TUbWgitMWIG4WeqeU5t9NpsXCgAKIjfWNZuCTZeG/pEQI1BtSoRA7h
NCAlRjATu3BjutfF6KMDsswVhtccXys+AuU4Zmcle/k70VFL2ogi4eOAFGWEsMApf7ctEXb2Lo2t
wBwySSTYhfA/wopCGCUYf+0Uny6RPy0WfdF1I9NffwjeP0+gsv0ACGcGMvGa68bYM75YedPyAADn
QIeKRRcnE8p7QV1OADOPtDhaRN7CxqyIu8cDIxqWJXaCgZeWq6CXDoRurZ9I/ebbxl5SoMQQNUlp
/CsVGTzlLXaNDVtoOYzD25xZSWqCD3siF+ypdtYT7pK20HgBY8uwAFtnrFYd/JsrCSI8T2tN/9GZ
pZHnMUwTSQkYF5EzHuHR++vy3BWLTdqg3UtU03FE8U8dHo/H1JY8YS/AJaSdG8B20pbDBZzJ9woW
751xVjJmNT62za4GpB2pqZMnO5qX33fOB/PFYsI4LWNPYpA7jYU9Mutig6q2PhK31IUgpGHVJxXE
wL4d4IoUOnWXMLHsHjNTB13VoolD5DAr3KSnQcjsN6XojVygNnkEV8UIjU0c/v+v44qdDdQfOGAt
e3TRS/UKlqPkl6AufIo9SK+RPYfNrKTjf+QTa++9eq85T0x/F3aaZwmvTXnMsJN1MPsNTh715kMO
tbuOcg4CGWMn7dLEPxRs0CoKIRhy2oY2AiKG12XJoZsWFBp9VkuA9Lm1k5CASvY/mwLfHoPKvhjW
BGWmHaycr/JR6/dso2WN8E6m/s+EWms2yK2lUMxbhsa4qqiW8tUDcyUMaMMxUYr7ViF+Oyrg838v
Dj7cZiDakPA1l2Lg466yKrCdS7K0fdjznv8X1YwIdFHgwKfcKxcyMujHznOZj8S0MQ8L2F/FhpO+
HfGvHQnqMyYgbEUygsYFElJwVSSZ+1SSLgJPPHEbBUBph0lg5YssIxwi/1eFybQ2MkHJEOB0A7Ew
bY6MJ0Rdt7lNKPQQKDRv7HEpZDAlz1uu9eQWxT1OiAbkVBTAJIYNZBvqco7mOVrrzTzTJJi+Bu0E
g2lzUNj9woTYZHLrQxjyuFUW0Tt5rvnJaz0av96+71xp+9Ra9Q9uGqLLK62mIBgKXilT2ijtsjxw
Nh2bmSo1koo6LhShRt8B0uF0D3fHBEo5gW2DpIXJWP1ZTEiaV7ihBvA0BmYPK3vdgrsWnq1Pv3m9
vkcnfhCRahWpP7R+79liJSqz2zV7EsYKV3kXT/ANB1+SgHw4P9hBNOt4IGvgNUmr+WaNR3akJUgX
9Of7P6QvWw2jOtTkKvQ5S8h6YwgZzgdqOUfxISWcmuZqMcTg/VmyZBCuw/WCw4Fg/IlrvCrxJjFk
3AQgFpLpXLosrga42M38x+ZKdPFIyCkfc5S4338r2z3lFF3yYYsYUEZP7FObAOE/urnQnO3jUymI
ONp2LTzpMVvVQP3EyRQ/9JpnjVvcgaPSktdW6+peKYQ8hPlDoJwwxXGqXddNywcCOyLLFbdWDL5j
S5yMe/oN3ksU62BFl92TfdthiOqxHyJtWrxUrBHHkaCjEhk5mW9m0AOzzhmjXttqWcfHjrzEUWjn
/ydFwmxSQvRyuPT/5TT22Bgw6zLPrE096F290ZSCF3ve/efqNCyYRStsHvRTH22QhAJFyaZufqsj
IMMum0PHUh4MOFjv1Up0PEmVymukGaARumLsbnzCygNqDPOMXNFTyT9AL5p3oS4RwpztUnnpa4GN
eq60qCjNl9zP6rjONZqpwQ6b4wxhLa2TsCBLCwxtkikwp0O7l7w2zxWE/7O7r8bIdzJ0N/BnUA7q
W8vx99BxxqY9f1cJtJdDfVCUktR+3eNgXCi0ndJYmeshMOGGAT9ptkwJVYMcD4VDEd6YpLZGWN+Y
owOHeEe6wkMzBMqI2VY//al4YjI0qGkWrjFG9CFt7kOfVnkX96oUcRsaWAm9u6WImmF8cAqa4k6S
tpUXfyu/y3sqBWwZ/Bo2BCIKQDSS/oQI/i/MFEzu/jH/tmUjuAO2i4qFc8o+IWTv7jT2Du9gmvKB
j+fznoyn5MQ6HUibmHaDOwOYtARZyG029R1Vmbd+kluqEkhPlNF6jxPyDpU/Sz7M+a1fYTxpZf1A
vqeitZACXoJJd8Z779ey2CoBfjGTTlJXUd+tSG+b2TWwpfBjijaxrCIk/SitTyLUl78OJZPaqt08
92W08sOwp0+xGq6rGlHXx002fSbUtVP8sYH5jFRmleXt0YFdlolIwIfjpdnGWt5aImvUWM5xKDqW
3mu72+HUX4BidJ/a36yF/xVqmk5nu4nZYtHiDdf2JNGU6AYuE0UMhj4WhIY4424I/5WukaHGa4oP
j1XNBvny7ZhhBsLXZDU29j5wZbF7Jrk0oQzM8la5Q4RjBDmEo64BeODzIaB3jGhLGl3AN43OgipR
h5qG2ckx4dUFI80bXiRIkmF5rjjpL1BbrhZhT1y3RmJqfkPtz0sZQJ3zQGemcIcIVKYtSeROKPlJ
kwczUDog70p4Yn8Wr7x2PGHGZRE4qbOpm0wVVNMXYDQH+cSPaadxyTgeY3dd3NCM/2rtKDNyzovl
B8S05xQ3myYdl1W+ZV1DhTZbsQ+Ev7sOhJy8iIM50kKyIBXyKfWwWD2eyPkyVddFD7SYjAhTRFGS
Lz99IpzJgMR7tFbvhm6/pUHMOaqD8eU/6ckt5Z2utPYqanblCAdgpakL7z/jgJqWvL/VUeie5Sp1
o0gxJktO04ldnX+2OaWr+tjAucy5yKhNBS0q4vASzUuKAd3DBGLgVXlwK5IhHCkpTrJT1Ek65LF7
HIclH4yJlQ36Kl3M5k2TPuzrYym9PUaHnEcFNmbmbHoYL51qV5FYdUqE2kXHNUdqAIrSFNOXIrF+
MSXZZgEuLHlfqYi2B1WoBhc96kvgcemDDBem4YjLNmTMFC2f284bUl9NWjahaKg76Y6fudx0wL8A
ARW51lGJYtxLMI5Z2q7U+7v5IAMTvsAtYqT9dZ4NOrldWtypFrdHlkA+mecuFfsvb3Jnw8T2RVlb
LyhF4YRxAVvKXM7DFnZ7Km9VrLMA1HBt/gT5ffqJCS6lYHR5gflu3kCcs/N9Hk7bndaWJUci25rr
H9tdWH8f65em5o9k3BQGZzMGaWSqeWhpS6d8/WsweC43QR5oKQ87cf/b8k0g/9ZOV9DhFN9uQ0SA
Bm3gwFgRoCDaXZeCeBCGxYr/SX0k2EfKYFY/t8MigEdpQsBVPEQOOAwMr3TafQQIJhqL+GpPTpDD
Z+YtWsN+zCcA5qfekhvc2wwRVFd4p4FYPw7quMgHf4aSx/Dbs/HMW1COBGrmnDakUDS25BqGZ7es
zvQZ+Sc/r9noyi1EwjmbkbFtMqVpKmu/HJkM6gJouCh5VMkenWAJDI9jGsOAzYfgKXbNx6YtaV1x
r0qlPbb3qcHlBqDFMg3Q6ye8wul2d/h2FQuuAgPB6IJlagQ6Hn6MTcl6K3B116cQgpqwEJeb5WX+
V3uksqzrXh/jNM+oeSS3ShCavaUseEt3bESvMo6kcMkoYZCzfH8EicGA6GP6XqHXe0YCsK5TBQid
+C9kPHTxo4V/icwZFj2n/xJ7icMZWKTrn6toJHMLh/wnXhR19bvZ7egS4TZ5tB4Wqx2fHpYp2mQr
AXIBtf6EouwwGYSQUu88Cys0UpXfhh3TFiuO2102xCvzMQUMFWofHtz+9VmFCsKSAeC2/CMHCMtq
kru03Fb8t7bYNZncbQRtlBDLQaD41pcZIy98p8rzfrsfxVsiuGabPW9T4HWUeD0AGC35wUJn29Ye
Bmjhx09kYDDU09AK7P2hLelJWRrwooPR9wvm0blDxu16VFTgLdrmXWp5GRVXihb05aKIeIRvAnLp
rmz7/n0NJYEmPRDYpORaF4A4sNJSeeMVTN9oVNJ82/XJWplcnGxjw4XdqvrtR1PJ0bvzoLZIXII5
llt1jAlqwk9ntkP1KPV7psJaRvAqDTnN8OwmhaQ+4jYcXZU0OdIgy2aEUY3GGt2KJqehPoaXOIDO
pocWS4CbkFaTnUEOp2I7qym1MhcGr/sI97q8DZiBO8nEDyUl2pgaCVmSjrLM4BZbFWNS/S5vJpxb
rWwlLVNrtJ9QYCNqGFo0D8H+hWkew5PAfmYIZJJkJguSa+L6b95yKtUkQBVybsRbzF5/9MVlq26H
RAo0e6G6HIE31elITK+tdhYHCBDSOC5IUhTwCip1ljPV1YkHCZKNMUgPqKXYsdzhctnr90ktpyge
yun60fkQM6mBQQZXINyEnTGOXO7KHqb+/kIvcgbhZOXB/LnH86/YmC4cu2efZDxAtFBEXOx2MSwX
6y54XA8B530IYFTzx8lOXDguUHF6XGyiry/abQd6MJeF83+mZk/UCMHDhQ4xfQYMxhbLXiksPtK7
h0BPB4A0k6kFkcGldQpmpYntsLv5gyUnZ/BG0PGTMby0WBnHgrX2tRQhgupkscIyORsJtpOTzsAn
LmquLjDlhuxpDj/fCHY2YjLu3nAxLamsstjSdKaJAc5NxMEk0o3AfGiym82lmmWTg4s8sCe4AnlH
eKYuTeT/r4h2+HrpD5ObRjQjYIzt7vaCG+kca8d4p3hAHICn9+LtNJH0wIAiC74xC94vD/aC6POm
6xDMo2lTh+onPC6PW5jPkJuOeNc6kpLsgywM+Yffze2jVVU8ar8AC3YM0UjnUjitVYI/XyI4Ht3I
HKS4qkozcysZaAywZlxtj/nG5jd85SdzBeYFH72wnjT/EWsIuF2K68TyxlNWUJc4MwlzKkaBdkBG
UhRPcQiyWg4tmekTUf81cXxEFqWtiU9qru3KPYP22nNrP36zzkgCLFgYg8PMKtTTdQIvLGFFP7YE
+icxt24Yc6cSbnjcDiQtoG2CjmcfIvpusYmjw3v7NGYQpjRyxnHMu3Las/wg5CcceQHnx1KMthvZ
6gjlpM7eHKj0Ui3iiTrTkKlV9xb9Nc8g3Jkr9XpNkiDdvHSucjAhxIVMl6oR/5AQu0TbEaw5m4rL
efuSc7HawNKPXtvwQkJ/Edir2M8FdxBkkXI2QZXT2N44qvZ7mUPOSH3qC1ZTlWz/rTvBzdfzP/56
+3e7ZsJsGkoJPcCRDpfS1DBt4ry1z3JFS0cNEVTtRXbKJdak/IWPVKNgaJPi2zwmQevWE24hvL6i
agCQ/A0G3NlRmR3nQ+1xcd0hAevxNMIpKebRJDx2yEO7QNnMdsgCte9m+q0dIk3vp3IKVHcGDN3O
UftePySI1MozJPSfm0Een++l+YDFPqEkHkMH8WAU4R8eIskBg33Iwi6pwbBUx/1ARAwty9Ppr2pO
CzS4znMYTFShMne0LVb7cpuPILi+VKsSaCQDLEvDsejMPXQOWnPHmSzjLB55oVoHyC0NwdQzdEXS
2gfO38lUMV1pATS6Z4Yd23s5aP1oAobl6SV6gf5iaO8iZXGWJO6mUwbQoVoJojWiwfLtn5HC5aZW
NhUjS50A85wayLaAVP9PTSoRGWWMN4bJrlBniX9SpxN5jcCd/tAlEOCpHXkyhZ/EdQSNApQSue+C
sjo8NilnZoYeAgvlCZrhJDHmHm/Co3AgTFt0HZ6O2XeCemEhzRz5h6ImfJA8QO3A9NTm8ujFLVMG
5QkgPatwcAA9dZx/oH12CtZlFPMs7ZiEAgKkVan4zz0gI2t+lfSI+TKPtkGawEv6UQt4tr5BXOZP
xxRpR5ifpiJalFpG2yGe/W2Nf0UOjXKe0KXq7v7UDrTbt7OKXofD98zySUknmszIAVFvG/CTZp5B
mnFZ6+HotvRYOXWyGWG4UseKuuyQ4tWmRSj3pqrneOgqv9B2pEdr+oazkakUK8yqJHrwjKKwfx8O
/5e3djr3QYekeWIsMsE+TbLexlBRd+J3itIzgNmRAqEbkdVffCDawUm0nn/ql1bZvtxJkxmmrNnh
8+Vgae35LAkQMDtRS4wII9LdN1CQ6LhK7Ph9P9g/PYU/sERIUkHYeE8Bz87y9bVCiIky0BrOJgdG
QgYEWIGQ7uV+Vijez7kl9ICA992n/gZSEdJXMRMsXi/VzC2OVEHxkt6Qc31QjUr3NIwxAx2srgfb
LHsrtzTK/pbbQ/AjQaLAtnzWxwAyDlrgR/0iuDG91MAl068Xkp9Rv5oovtMYn2tadRFpIaOK3wsE
EV3zTejChHMtrIuScwyMwoGFmDQfyHb9VUAV0n93HFqK4+WBWniuPJae38/biQaRTAUc0RZtO810
ALeFHTRyNRR3kHvbcibGfo1UH7DtP4uklcI7c7Fj+vazXL+2WrByt9v8x8SciZFscY17mK43ClL8
NcypqUCOl9gm56i5Kn8k51TW3fh4CTZW7SBfsV+5GLE99taZ85EJyhPNqJYeUONPk+S4CT22SoYi
Xg/V5FtNhWg+TDEIQyiroVK+MetMlQFNPhweJLPRwMQDrmeKekcl8T/p2Xqdu9jUilY34aqJz4LM
w8c/aLrtaP4TJXIioWhPrcfBv8iTcLcYLM8FWAkjeW3zWY381PSmjQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[6].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_x_noise : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister : entity is "ip_peakdetector_xlAsynRegister";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_38\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r5_x_noise(15 downto 0) => r5_x_noise(15 downto 0)
    );
synth_reg_inst_1: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_39\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_1\(3 downto 0),
      \fd_prim_array[6].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[6].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[6].bit_is_0.fdre_comp_0\(3 downto 0),
      i(15 downto 0) => d3_net(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_0 is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r6_x_min : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_0 : entity is "ip_peakdetector_xlAsynRegister";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_0;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_0 is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_30\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r6_x_min(15 downto 0) => r6_x_min(15 downto 0)
    );
synth_reg_inst_1: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_31\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_32\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_33\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp_0\(3 downto 0),
      i(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_1 is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[14].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r7_x_max : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_1 : entity is "ip_peakdetector_xlAsynRegister";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_1;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_1 is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_22\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r7_x_max(15 downto 0) => r7_x_max(15 downto 0)
    );
synth_reg_inst_1: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_23\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_24\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_25\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[14].bit_is_0.fdre_comp\(3 downto 0),
      i(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_2 is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r8_en_pkd : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_2 : entity is "ip_peakdetector_xlAsynRegister";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_2;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_2 is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_14\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r8_en_pkd(0) => r8_en_pkd(0)
    );
synth_reg_inst_1: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_15\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_16\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_17\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_3 is
  port (
    o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    r4_x_delay : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_3 : entity is "ip_peakdetector_xlAsynRegister";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_3;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_3 is
  signal d1_net : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 9 downto 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(9 downto 0) => d1_net(9 downto 0),
      r4_x_delay(9 downto 0) => r4_x_delay(9 downto 0)
    );
synth_reg_inst_1: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_8\
     port map (
      i(9 downto 0) => d1_net(9 downto 0),
      o(9 downto 0) => d2_net(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_9\
     port map (
      i(9 downto 0) => d2_net(9 downto 0),
      o(9 downto 0) => d3_net(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_10\
     port map (
      i(9 downto 0) => d3_net(9 downto 0),
      o(9 downto 0) => o(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xlconvert is
  port (
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[9]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[13]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \xmax_i_12_24_reg[15]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    D : out STD_LOGIC_VECTOR ( 13 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 13 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    \^d\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xlconvert : entity is "ip_peakdetector_xlconvert";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xlconvert;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xlconvert is
begin
\latency_test.reg\: entity work.design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg
     port map (
      D(13 downto 0) => D(13 downto 0),
      \^d\(13 downto 0) => \^d\(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
\std_conversion_generate.convert\: entity work.design_1_ip_peakdetector_0_0_convert_func_call_ip_peakdetector_xlconvert
     port map (
      CO(0) => CO(0),
      O(3 downto 0) => O(3 downto 0),
      Q(13 downto 0) => Q(13 downto 0),
      S(0) => S(0),
      \xmax_i_12_24_reg[13]\(3 downto 0) => \xmax_i_12_24_reg[13]\(3 downto 0),
      \xmax_i_12_24_reg[15]\(1 downto 0) => \xmax_i_12_24_reg[15]\(1 downto 0),
      \xmax_i_12_24_reg[9]\(3 downto 0) => \xmax_i_12_24_reg[9]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_blk_mem_gen_generic_cstr is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_blk_mem_gen_generic_cstr : entity is "blk_mem_gen_generic_cstr";
end design_1_ip_peakdetector_0_0_blk_mem_gen_generic_cstr;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_blk_mem_gen_generic_cstr is
begin
\ramloop[0].ram.r\: entity work.design_1_ip_peakdetector_0_0_blk_mem_gen_prim_width
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
D35pVru0M80ELqJgnR88jDhn6S59DRYuGeGcG8A6wVfXBslvl8Yh4tkSTIxKs73J9o9VE+Xi8/Q9
LOs6uTgocl5DcU8+iJRO1M5trqxBBk0bizDEfm8zNtTFRCRFTLDiLOeBLxhBVU8yeOXLexDyv2AQ
dJrWDPq0zKACdgLCRC7ty2S9fDtNeku/W0ekUjeebpzUWmBjfFueEWumqb5B9G+OUrF797+gHva1
hAihgCfGoj7VdmJpREfsKQYuX1X9ach2GTBD+muxug7fMsVdhcBg9wKckpx4oJiQ3bqL6gHT9Qfi
8MpEJxiimm250FUfjlwj4jPCOQXltDWejPs9XQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
h4uQdXC8kK1GkfJz67vaqxb50+q1NYSx0lUNtAr0T2xljcFyf6KFqRZDji/Q39aFJOOSs8JyK9RI
hdF5z1JVonF+PAFbON2Gw+gaoEn/QZAn4GNVpVKascsLqeZsM8/7pAan+bv5CdRNaQSQAdnsLYq6
SBzam05obvwj+KbYopGHV6WZPnYbJvrNtWP++7bcXnrMKfqac0LCUz9jEzy5ccU/lVxF9SgGQ5wj
O/vL7bHOHYQ3z9z1Hhwwb0z4bZCM2Wzmi0LjP58yorE7gy6G6BB+6csIp4xbAbaPadliUbq5c9/R
f7fzsOWkwb0ADEI2Ctvywv3lWeTQwqJK0oFjeA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 3152)
`protect data_block
rI7XJjArEVGGswM+EsE6aQdZ7DGbpaKPGgsLuKR89p6w+azIPbg1rQJjAtSJTJ0zlKwaadIbRnJ9
wgFN1FS3D8dEQCjvQb4e1NFXpB29dDEPBodtwqcLWJBC9iCFkHVpMfLTq3Qto5ZY125y/CIgdONh
221MCTZndwEC7OSVxfg+IS5nfULP2HDFawJl3TQqya7A0qWwiiOuKBNvL2QpVRYt9/kFMwjeKPLM
Pr2kQzKbjdQLF3sBZJKv+vavAIVVrtkB43BSjw+2mXTVVQahwv3bY4pWni7R+KhsX86vMkkCwUoF
SfXT5LVfh+SAAlYhs3ARgdKUcDf90gBLD9BiZAqR5yOrkAUafwvKQQJBfLgh9ng8/5ehL6W45sF2
oKk/2/U0q0wkB9G31Ve0zKz1gQ7EwBe73BZdkxrGnhcw5XZCUyAS4uOmLPFfBXZlQQULZCjvlC15
ckjVhF0UsRfyn9FgLGl+jKic2fG9X/SxHkJ/3QgG1bGaFBMNscrd/45SuYGtpmfcDzu8M/Bb7/xM
dttQ2qyc7fy9U36cC63tcPgBZg4xkmriergk5nsGDEZwhetII0mdriI4HcBzdnZgkcwx3t87ooQQ
lDBM8jdaHCkPnvgzt4x5CEP5p60BMowYEBfXpzzdAZp8U2i+hZ142SgNH4flIW6UMPrnNr/dsWiF
69Xs9gdzTnbIAwUV3jcyu4u1k2a5GrUij7u58QOkgdLdJYk5MG/uVhsvFODveKT8kciX/4LGER8D
gtamyKkZ3yGRLbF/nzIrpfhS1E7uNR4LnTtmtfhC/1ZLNlpE1lTJnILSPQH09pRwj2c+ziwW9I2b
/uLUfPRVzkNMjBIx7aDZRT3zIsHijOIM+OpHOMvyZNH0g/+WU4w0kVPZTo4I8KphReRpYFrORZV/
DOtQmpmlCHlAdUUEaoHRsWp6rQy/r+MpNGS9wO8h93gwEXxayBfuLDGFcYIJRvdYG11ihYB9I8ED
LMv3gQkyLwpffCxa3OQMFnK+UL34lOpTuyCubT+ANGcBI1YBAeR2Xc7hojtql72oUb/354KQ6MyG
BZXJfYdJYxX5OBaMISUWTP31oXdNQNU6PkwQuENKpZ89OnAjk11B+Ap4t/RcY99PAUwEPo9VPZzV
CyJyqJifDfaCfIVFQrIOUYVWnBRkUI/wO0Pfap4pOtnt22L7oFskKxNlH5lBvs4MtxOMcnFqC0oP
XQrzDD/t0jd7Za5HynPWPJE7uLFeWfCaXsl/Jt0vNfCdGSZwZBDQOFIRD1bD19bIExktDwOOPjF0
4vwX5rACjIWLJ1GP3KTEAneSb5QLMCCdhxc1uVlAolopk2vAkev5ycEOkqWJx1yVp98WBJzGTFGH
I9pSXzORqOZdbJ1rNdF3PN70ZIgiC9YxdIgh8FF/ENE6xR9oxurs9KUqGp06O5mlHwOpvtGRVds9
+J8tuE0Om4daIgifP7qb2U/ZUVUW6VfM0zCnAu0crnOKqM9AKIobejmj6/v2G4WzKOPDMijY2Gmy
u+YjxkzRype2TlbKuAkg077zImdP0TsM4MEmx6sC5Vv7hT1pqjxlb9KAeuIMP6zoSK7vOVmUncEM
B+weqnjdi+yYFrggrOAyH4OlAJcyosMD7e86kPK/U24tQxGWFWJh904ZsDldd74wWGGSJhjzXIXS
O0ekBxLnMtNByPYecNZuXmG8judp3jM55GX+Xs4BV1e35FV6kwkqA0XaKfs/yjDGfwanI2YE/Kjz
greJ1/irvwh77iRG66//8U7FZmD6qFtYm+JZdwyxjSinWsy+mPW1vzx0PCoM9cZ8/6uCPYIDdIgV
QmAwk5eSBElZsdiUpEky3wJbbozTqvmKsSlfYsPMlkMMOSkaY8hhhglIUGbdYE+ffLgIkYhjJyX6
JJLC8+1CIa7bK2JMTaNwGEQSujNjMnJSCRmcHaRPG0m0xNzkZdkiEpKGw9gER9aG7QLX6DlApcKd
DK+cLCwBzl4OoIkiL3b3QarNZaAlf4VgD+xJ8fJU2txIJgdOa0Izbm8ZmEmCRpV9J92pQootEbDl
nkQ9k//frAfxeRAaB2FkK7F/8bbAws1QNYBC1kMfHwNLJKaTrHpgLoq/AfyB/7M2t6l+qtOAvLCo
0KntP1tKp7Wsg2M3+rjmRFqRYWD0+Z/NYX8Qi8oFclXStWO3p/XSmGUiEhflLUJ5QbX/2XVYfmJV
Mb7vCvwqB7zPdTWOKxTaSZkyHw4T2CFjTCZMThN0dmFgsYrg2hs0LUKPeRyyOq8vdsILCo9d4EVZ
/qejCqukx0g3vaPZo4SFHPbEfJV9tpZO5eUaaIJLObupEyHHdK93BMwlRfaJzwVSjN4SxDkLwQbT
gA03l/2JPIMms2kMaWznQqkJyf9rcNstCLAPH8zlaarja20i1GJdkZiCDC1foureTukbLuGCQQli
m/05s4JgdAxtEc+j5d60uSAu/DyAFJoppJp6iEpXxTBAhmMCUNwARFdFQY20kRZau/fJUPvHeUTt
vJaNMcoVsnDmjcKWNdRXukOgrmqnFK8pI++DzTce5DDgBW1OOWPh+J4sFGircl2iVhrMwhE2l/dH
9AY+S3jk7DiBxrE0lMN06SHcbF0gk4oOMYfy7lXijyK1IlootPsCWWn+WVOGF5nFPH4LDLb5pYyK
rMyB/tLi6NUmnA5ykguMNbt5xbSOttbUQFOKlfpzlXVXz0zcAE7q6BG54R85sp20WgJZ0GsxAUZm
y0wD8w5S0sPxSBjRRhLIQszbqrkM1RZiZqYUEKDAgDwLHwU6xEd8bzoHd5ouMrLoCqVdBcPEE1mC
n8qzhG+dEsQjA2UaP4pRwqkzw50cL9d+/1C0hOw+20SgKvQbKTSt9Cinz0XzQNqkJ7bok60OnBik
/bEMASuPhFaWdoS5z0L+v9HKFuM3Uk/qY4Ez29VQWuycYzsRIE04tYMCipcS8x1p/CUz2Y19TVlT
nXV6tzUK7VRHSfc9GutL5mPMYRHDUcZZyVSP7HOVebOUtkxiVNZsEie215bxBNqhR7nUcnC16b63
5E9jDiAo2rLI8dvpiYE2mwWofYONdAHNVXI+CIPX3Dn7EbK2rBpCHzhv/PMnPvR0qn3UpGwGTFm4
f8tgZjUivoP1L5BgaKB/05MsOEg0izcv7Y7oF3/QT+pfPROO/qRexTV+4t6p0FgkwcMhEQvhFzFI
1vc8tu4Mupvcs+Ge6VOq2Kn5y1kcVFn7DZVLWhqF2jMHXxfK1mkyhTm3DVd2QX79Ysu28Gm7mQes
BtajrlTI+x17Wt7Dzh9aWhnRYJzyGslYq97rFgMheGhoHeZTfGpYn/xNI00VBYAcPtTB5QWizal1
By8bpA/dMnxtMMo+Vzp7zBZV/QmDjE8Yp9zj9HtW0NGzNSb9OQRRlw3HOUGrLWgao9hstmfq8FAX
+tb38gGR8BrEGZXzFlymxIDQHw5ZKs9B27Th64CVeirPeLas2FFE63tPzlX3sm48nPbNXUuAm5ct
Sz5WvYrweUs47ul5D/6GO9vcm+Mg3KXSz64IvPCBzP1DKWbTBteJLPNN+AhUoWCR3M3B7VFBsdD+
LEVF/qnVSK+7aWN9GKScF5vdBOF2UQyGDAg+BUjkULzNUOIHIxXFqnQyRrhhHyScYwAIZv9d/YJU
cVZCzet7eFKIJo2pP6zxa6nGI8NP/4/AvzocyAaELR7zxDIbyOk0cA28mQwGdwgEZ+/wmz5Mn8sD
BiUxvovz5alDKFTPmW6jDjV+qyF1YtG9JEUgCdFQECj84jsF+PJk694+4rOXovpPmyrQxv3h4nV4
7Io5X3yhMRUiIVYATxxsW6bgWS/C5ca/91GzSOP/eByazqAAyddbjm5TtTOqvjhIvB4TVxUH4fJe
qwoo7XBSYN2Y+9F2jEyih0KKgS/GMpOAdjt5pag8wa9La3H3gVToHSG9Hxb7xcL8MSo2C9D1tbK2
pdLGKWopYyHc7ojHxHHQJAKsLa1pv/HDUGcy36ncqvw8CSXmNsb6SXGJRHHeJ8XofIk1FsDjtxKV
cfj6yZmNGdRLBgLiXJbejX06RlzIBU0ZVLpBZ0u+/E6wRI2ISFQLZ/fZrnzQ1T/VIAwSugPj+IoB
pml+2jL2XOqJ0eqj/VjRaQg6FsP7GPKp4bQ6pf4MIu2KjllYcl+VdNaB9ZORUZYOUH1Rog6dQTu2
bjrfzsgZc3adm74dPQD4eBg=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_format_dac is
  port (
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[9]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \xmax_i_12_24_reg[13]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \xmax_i_12_24_reg[15]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 13 downto 0 );
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    d : in STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_format_dac : entity is "ip_peakdetector_format_dac";
end design_1_ip_peakdetector_0_0_ip_peakdetector_format_dac;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_format_dac is
  signal convert4_dout_net : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal fully_2_1_bit : STD_LOGIC_VECTOR ( 13 to 13 );
begin
convert4: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xlconvert
     port map (
      CO(0) => CO(0),
      D(13) => fully_2_1_bit(13),
      D(12 downto 0) => convert4_dout_net(12 downto 0),
      O(3 downto 0) => O(3 downto 0),
      Q(13 downto 0) => Q(13 downto 0),
      S(0) => S(0),
      \^d\(13 downto 0) => d(13 downto 0),
      signaldomain_clk => signaldomain_clk,
      \xmax_i_12_24_reg[13]\(3 downto 0) => \xmax_i_12_24_reg[13]\(3 downto 0),
      \xmax_i_12_24_reg[15]\(1 downto 0) => \xmax_i_12_24_reg[15]\(1 downto 0),
      \xmax_i_12_24_reg[9]\(3 downto 0) => \xmax_i_12_24_reg[9]\(3 downto 0)
    );
logical: entity work.design_1_ip_peakdetector_0_0_sysgen_logical_80d79f42b8
     port map (
      D(13) => fully_2_1_bit(13),
      D(12 downto 0) => convert4_dout_net(12 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_blk_mem_gen_top is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_blk_mem_gen_top : entity is "blk_mem_gen_top";
end design_1_ip_peakdetector_0_0_blk_mem_gen_top;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_blk_mem_gen_top is
begin
\valid.cstr\: entity work.design_1_ip_peakdetector_0_0_blk_mem_gen_generic_cstr
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bUh7/raZIliCaLpMT9cwQKZhcRstzIPrX3v2z3pGMwze0bJ72y0NJaK2507a8dNV7XtQ/ZwOJb6h
Pq6a/dWIGjQM0cPb6G+N8qxkBkWhtNFTIe+RkkLrCRuh73xHWgx0AoTSCXLQGM6sNaCiq+YqIh3h
HQGGqRI01xdeE7/nUSBCQlaNGTCwRV7s+GkxMvA9VoNkNU4Sm475jt8hUN8mOGAOH6rJ6JtxB5I7
yJW5JbMtz71qWRkgwhmj3hZio/Z+6FL9UovmaEyQdN4Y8jrJXwgoV2G39ODFb0gJR8mg8QNeH/vH
Gd18+U47v3Q67GAJS8Xx81UcXSlaYHvqY5EwkA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
JCgeIDrXTafK/UGmteRy2oDjs4HWsW5z3fl+J2Ik2dZaIxhrelDq84WJoA+C6BOO4PV/jOYRWkBB
LYylxUchQM5ORkg2TL8wYHmp4sUf+FLxN++n+ahAUUvZdK9E8QgrV13859N/va+1o/LqK1flhoWH
woypMI9CKissq7ULDY9bkeLln/HIiBGhfpUT9YDW60i3pG0M15SEQSDST+CbZ3vMRGq2+GxppjKg
9xnKiNiLb5E4Ko9WTv3Gei+FuW2JvZVauZTorEHxaqVxRuv9RSlLrYVaU3q0hfT5SLZJdtWO8oY1
UqOg1DYURWayDrKSkiwLi8/hkln5XfEge+WfQA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 3056)
`protect data_block
rI7XJjArEVGGswM+EsE6aQdZ7DGbpaKPGgsLuKR89p6w+azIPbg1rQJjAtSJTJ0zlKwaadIbRnJ9
wgFN1FS3D8dEQCjvQb4e1NFXpB29dDEPBodtwqcLWJBC9iCFkHVpMfLTq3Qto5ZY125y/CIgdONh
221MCTZndwEC7OSVxfg+IS5nfULP2HDFawJl3TQqFXo/4fXl12AhDwn07RXnSmdbftIDSTCLYkPu
W7oLsNk3G6rmMPXvnzjNABuOXGQCDKa5XQOUziNsUGh7N4+GWi6CBMq5uJfhXe7VvtXGa+dTNwZk
z44TGBbS5t+zmSybND8r+dVbLsT7cwigDTjs/OS48nF616rhhLuCqhQOdrHpoZVC3ICkN/dEZsQL
oq6s0yWUvYqc/zdi/4iySilzFqr/cE2Q/8N2TPw1bODDOzNVevOFumn8QbEJfGVERBDW0JtecRSs
JFtWIoDzAmwIO1vQiPc+NU137c0gMqxvc2hvxXyyULzvv96gAgEu4pxeufdfFE86QnqYSmpWaNdM
xz6cLS0AQIlTA1NFsGAmkbDrEBbpSuFjRVgrC0ax83sGeHpMqwCqYUlLuf4wnTFjAb90NqHHVWfP
bfu/A6oYVRzgS0dlTXIl74n0Af/gV6vCkT6gUwNcNBkZvuBLXQYs8wAb2p04zIqLCX5mYvNiKop7
0EsWhCgegLy6U7jxJkhxw3kBt5YYB4sQakbc8c5rXbtLwKE4I8PXH+zgdmsSlFFs91wNqFMGx98F
hIDenMbDh3Piw/Optoc5eEsgTS+NIQl8RfgDNppMAj96mL3lOygT16Ybc6IorZfC4CSgSdioxcPY
vN7Vbo2vh3Vkuk7ovicNFJGeFaQvczSSnIFm8rZLDbjZqlKlFuiKM4oLKWoAuYxc7jP7OyeJynh3
ylqw8tuikHdTVTpLkW0idDqKnwclYM3WtU6LgJrnYUy1fJjk07nIJgwOe3BZoqhftoOlsNDcupf4
nfMARWAcaQEu2YUfbmHL6YJwKGkkzbJ6YGVqY9GOkzgY+PhCDHhhV1VBGzL2B8Xl8/JMnGEtx1xB
xXAlQBgOx/cupgalMF7qL8JqBRR2tXhFuWrXEQAE6qOYTbjpDSCJmiQUY7MozMtz0LvqhJkVmefo
BKUjS+ehafa2V9AMeeRd9K9628P63GeUyk3/IrAiXrX1V/Fj+H+lFb6DLbHblT/K/KKbPmehyfT3
gJEgoz8zIdkOB3SfuhQ01R7Ntg0PVpfpJKr5qvIXTYj/iSfjHleU9aqvBouOm1OrIb4Mfp2DsgQZ
/tdM83QCpFdGUlIsbJfd14b1vHYfLbL9Q3/Uf26Lewt+vWTmLnWCAWKqEbmkHjFKaM6ldGsnfqii
NimK+2cjHZqGFG5XMrWRBDy121xOsN14nd0cE51TcO75XOOKAe5nnJ5MVoLvQlqtbaGRsU2ZQk6M
jFnQ94v9t0mGJKKyeijJStmO+rGHy8dR5xi0z9tPd9TXXxLjA3nI0Qzxk8VpfnnR+z8us4OwsCQh
4Ltn/U8GBJpfX7xs/v5tubeuzH1+7zflc26Ip7rpaNbhkDhgZHYOe+boV5oWMsk+a9oVLhkBXIU7
gfEtjTZCHHxYVgTOhuDBH/Cq87ziuCYrjSxBxn+wV8pEzpfPzBtLkZonej2p/BVorv8zIjoBPJts
kFtp9vuqZlWl6usqBZ5EyPDtxKC0+J5SyqHJElWxVC/HNO3JS23aSjCAe8nyzL0aQeTGxnUr9h79
vEuI+3REjtkOf+Z8Md+irn7rjlHD04/mO/sUcyoQ+tHU03+sXtzKtHcKjuWHR89cqdOKFFX0bVAI
zPDJz4LaWUX21N39EU3/IA2kCOZ7Tg3sG0dDotpHVJaso1JWN78cmOvB48wnvKo55872o1jdcxaO
MagyshzKqXvTEpPs0kaXCkbQCURiSeMO4Mfvp4OkZSoWH+bhLEEIKHedRaZ87zX0i78vzI/FCmAk
pEiTAkx4kQyjHlbIfjN+9au0tCh97ub6CGbdh9D/7gs/FuvI0vAURYRPjIDPmxs+OIs2DAfj5laE
oO0dVxNMQLFontyvN7f6mmSFZDNcphaf2s76QgZON8T1e1clqF9FFd9cWuTSQ8dfMdqneC0PBHAw
Wlh/rTHrlCsSeaFoYdYOwNMxbY3YwtbAo/tJgKRR6Ntm7SdA1p/G5RuwbnqMyYoXSyM6QrbI/1N1
842o4R1q3rWyX3c/j81/u0AJvZULdbzsPVWxIvurBsv5eZMP0CPi7DE7FFcd/duzi3RqUHepzO5v
LY8NawIb5z1aeaBP3Op31lWQr69Ff7nvtHdUpebYkCVoyeCtuUpoFTDqHYGfEUmHxyvGQoRzyy+g
+lI4KMrw7hwnhfvYBxJv5f/tzMTDy10QZNjxWQsRWjhOuNCEhEBbmvyJmhFLSWGXL5vffgFJ6r6x
X8oDafp01JxbTDUgvD01hMGO8QqPSIZe/SVgNkuXivONa3c+qIIaI3Kc51CfLTMIxDqvsmM2aMEs
nhTKjk9vKqXt4ZkHGR1Dsxtm/xD4UWj7Cb08TRDwnMbe7s0ZehunfWB6l2hWPvcdwDlcg+CNDSG4
WBz2Hqu+aAe2xBGJ+DrbzodEvMzp4SINlrZ1/f9U5S0KJh8lka5sl21160ML0Q2ZYXLvvCBUXETB
Yl+8Vjd6+qWXgZd9w5amaguOzc7ueodw+YFRhENsd3Pc2R/y795FbuCfn8yk5XU5OMfEtjveEb7n
UAhmBsa9S3fFvDPIMHq7ceR1k84SQKb2lHyckjL403Id0m5AJAE3IBCF4PAAB5bKRck6FTAGKqh4
1HBcFu2hch4REAyZKRgr9lDnnefTqvNU+lLTCrUsvu8Q9Xcok2oKbEzdALrShjVunK1ZBB1j/WJj
XvxRNlQE9wDPKTWCCOgfG0rIBTGSTvJ0tbzJpuTOXeerwOSzTLGR36WxgSV06UDLBK0yFVXD4o6L
Z52JEx78keSRaWTAz3uRU68WA8jfJ1YQ3jflbM4qe2v2RzhEltAGrW5mKdXD+igtZxJC+llPj7OT
g+Td3BlPiCzXuOKyvSXi8MkjFwNwRKwaf3XK0vPbzerzoU7ObfOMD6SAYJaEtgyRS341DRnRfRev
2VVoMBz0Zw3iium1X1UnOJDewpjTn2h74T6z/MAvMCJ6JFijWlOAWrtdl42kNftzfVA22zs+xGBd
axQ/kjrVs7bJamkGHjzZuBFOogXPHlYp4yuJxcf3Mqdk8jd4usJltxPiLanDosHZODJ2xLTnWzMi
BbnXdnE1vsB7I6tc/iDHoR1bqRIxGqHnVEyvg41mumaRlZzdd06isN+Uba1Rfmb3ymy/6vQx+PtG
UMAbCU+D3xz6bI4uxCahDgzm0dOwFBQCDCN1hTycHnSKQyBuwAnWJg7Nsb7CXUxgTpZhOiVJmVa4
FlGfewcLF6aSMvpPbcZ+Yciw6R+NyVMD/O5nmH55Pl/2z5CqY1aTtpBrSwonHL5EAB3SFQEqkl+q
vMknHumKPuHrwBPAvgO0SROrtDThsiwQYZgjO/Cp4m01MTu29rvMWOetR2NLXws+hicYdRNsKfna
qi+nBJjboS/eW4orq69/bhhsxU9Clj81fTH5fFMydJh3CwXEMx19eEg69NtS+fL0PY3d9dcPWubL
oZqyxHnvGjDVmFD17OdhTBzo5VeTp2XsUG5DpGApICXThdjOqoLXOVC7j2w1kbGrwabYdP0IkYWL
HlR3TjkokNLllZhkIo/YSnX8H3hjsJfr43kYzz3YLSPINYubgz4gMv3JdSbmkP/g0+3DATixYSxq
6o5aV60NBeFMM+AJ6EyJQx524WlX/C9khwB1Gyu+WXiiedEMeFwvJtZtq6Vct0MtRepFT5nN5ZKu
y4jZJo0JvAaoUe9Dl4eDshtyBPugvjT8nkxjOOiaCBJ85MSlS0ta6xp7bMbD2I6vnTvoSH7H50S7
dSLAuFfwiJd0yMUCcktJiyEREwX9GwdLPl5eZKqKkcF0GujtjKpREorz3I5ZgBQIx/uPdKLb3lFx
h/Blaw6FYn+c5ux2Dd/5oULAX9Zn1Q+EZijmVmzn2qX4Or0=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3_synth is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3_synth : entity is "blk_mem_gen_v8_4_3_synth";
end design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3_synth;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3_synth is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.design_1_ip_peakdetector_0_0_blk_mem_gen_top
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HwdksXhXCf5Xo7L+5z3wWTkfUn0ldwCGK2+094SJ2SZ2+lxWk3inpt8QgEebXRumJcQ5fzxndx0t
zZ8fln3LnJqlLZuYiwdS7FLQrBS63X03ebpD6XgH6Or+7OMgJYu6lLlMKJxetl9yWCNq5Js8f8Nk
qUhIQNF/lT3qEagrtK9bJS3BpkL9obVuYQ+6UzDUIZcZrdCyvkniLCJEQU26g7/BeXbLo68K7QwA
lECMhhadfOiLwR7i59e874qbo26FURpCufGKjpu10+JDH/w4SWaSmbIAH36n5a/EKMHVrKvsXCir
3jwONacAvpi+toi68nq4x9g0kHTirUb+dC92nA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
dQO0c+QjGwVSV/EtabM0BGKFq43iQQUMgJfcUf0po79MNftynEH323Jf9dY99XD2pq6c1Py2PAcK
W8RNW9sMSa1SOSEWZmGvvRiC2a1+bEjDWBGLPZih3kfDGirCVRKCFHEPBHoOYC0qZ4/s5tKo8e9L
1X0rDB3RnBczFjtvy0seey0oaK4nlWdzOmFRV2uWgMUSYvlTSuVHI2j0UFEhJlLZrfhU/jD5w5vC
PcFTF2y43/7gWGs88vNR5bm4k2NkcUO/aIPgYlFrInXzEDz7IayqXCkAi1IUMQzNtgpoj3sFwhV1
gdZMQgEJkab+6TCBwbLwzFWWZjlU60jYDxlFDw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11360)
`protect data_block
rI7XJjArEVGGswM+EsE6aQdZ7DGbpaKPGgsLuKR89p6w+azIPbg1rQJjAtSJTJ0zlKwaadIbRnJ9
wgFN1FS3D8dEQCjvQb4e1NFXpB29dDEPBodtwqcLWJBC9iCFkHVpMfLTq3Qto5ZY125y/CIgdONh
221MCTZndwEC7OSVxfg+IS5nfULP2HDFawJl3TQqit6XYkhVUsJToop84pyv8R2AAahPMTZwfRLF
arXnqwo1+5rH2BB7tTZmsSrrSz81ZlN0glGoUn9NKNPolf8G6bmOZs5J2g+ZqmixeG0hJpEbVGmI
5Ov40TWECnsGjnK/f73iIPXB7sbEAW4yYbd1X1TjTjtzu+1ntuzoAWTQV8dDkg6LzwhOT7mJvYRb
31KNf+9mkkDH/gdaBL6HL6RW8QEgYkn39WjW3g53B9OYptmOkZm8RtsGIl+iBpyOge9cC74nCFkK
EC6fKn77bUulEeI3y7D/Um3hO9asmPo7/9cXiwz52RdNNYnEDs50NRPvK72fP6MuAvaBAxQktrTw
Cete4t0ZeAiB/utxBZvPqxMuq5ichv/rW9YFYAHEjW+XuE95GcY9fgwv0t64Y7gTMHxK91ccLfBh
N831qjWNRwEVh8xzq6lBmC1+6v5aD3WOKXGLcmiMWzsZpWdYGDXRoX7q51+io0BnOuNnAecFJIc8
GDlM61OYql1YcxAZl1cMf81DKqSKAq1RkSHAw/9LPb12IyibVyuSDTmMvHT6uMiUj/iwewtAO2mr
CRXQrO0HgCCPRs9LOIzDpZF3ENjx5hHfmtxP47cEeZI6KaGB4rJhh8mq/pOWzJod0aU89LHhHi2o
sAVKJDSQbG+N03lIJp3laZTrApz//Vw+fdfyzLcDEfCwg229sN5bKBe0i/POkMAK8Lxu2KQ1fb+l
MUcQEp+K2cJpS8zohMrU/gOaAlbWE4Hk4wZ1AL51h+wMCO205qRYvM28jEWlx/V2/Wk5F/ohzRzJ
pQhObrEHFhM+OuPCDQjUAOB+AP/SAmvo3W9EvoKGLgRbM6KWVtHm9QWKzUbfDETzFZT2iKmRnNNM
oXV31Mf7rDuD1u3GXaKftg5OVZliSSm9vZVHhTgMJd+vZabp3XBQba3vXrzwFFt37d5/P7KV3o4z
HX7fJa5LRMx4asH7brzHcokrnUp3/4tbH5TCuX/X5Pmw1sm0tvIL9VhNRpEIxX+pUaBGZkmYhAfU
PePST5DT3tdFtJcfTt6VX6VDqCfYSXsrMOlChLACJJOruaBZlp+Ct4cM6D4jD68xcuS8PZHy3ocm
WwPTImBpnrcPlBGP26JcRTRtZ3Wbev/j6qstgbM2Lwm1CCuYP77OviVCio7zBOV2fzEzjbAl/mJz
D5I/hTwHEhazsbs9tx1rURNdqSsZqqFqLCggDnLDhZdurP6bhAmvuaKgFFgrb7Ey+yVDBCuMtHy1
4vZkFFnMi7xkdrs63+GNtaN6uNQIqrNDDJLWjH+Ra973n6rM/ri6f75bJdiOSowFOHFmcIRl+aGH
bAsv++PWGTdyGSCGInMQAMpXRVX2DYQa5yDt6QC/T7Gkvm4DX2+yKoC2j21NfAYXebAs4wWySNVF
XhGdCE09Eo1neOHsmDR9YLkNVIUFvSvLlN4dFV19Ql+hOuQNHuyHS5YNMfb9jIZO6gynwyV3IVr5
ETmZrxA58O4QX/VRE7FL8tai+NluL78RFk7Ha704LpFVuphstvIBvep93Y8rIJA/lrAOGjS/U09j
k9UYz3phk8jFL2QvHe7ngpokyfhQutfU/3bdIsE0UiUEuQogDO8mlLEU+StvC7deznbUpgDrMP2z
jIdlvLUgk24MbeZlPqtTZ8loD8F/4XWRHHIFcAsgTHOruZ1qxDkWArWbB+6zL2V6rtK9zab5kf3j
kExhUvrLt7s9gLRPi4Rj3qM2Lix4tnNQ7ekTZ/fAyUgMGVwFWgM8dDuvZSBzJCJea/W0BminoPjv
qiT3Xjz3MxkjF224wCoTTGRT8LFNadmNEL/7Tcemc3rKJnG0+7dTgrq/bIPM3SSrInha1/sdrL2F
H3m+h125B93M9Ksl2TQ2TX702g95YWH62wJJirg+PNhgyqDbXOq5k15VfWeIecVUCJTvOOP+xdkH
Lz3nkWsmUhAlonE7AeIqM2UYGuV/JpEkvcLnx5kJhp3lBRbK8kMIBuuVp+HJYFe+0szsWVzNcJjf
VeD2DXjdT/Ergjebhf7aRevl8oUW7P0BJsAaJiBcdI3dXQdrdbpb4qBVMQtMQvNMbW8KrdCZXRBs
kx0FEnTxA0NlCL0QBO/95DNdpmuFzgrHDBHr+ATD+1IKAcBrx3TXtCMYMOnXdYasvXtZdmNnGOBT
b9NZm/c8AS9yq7Y5+BT92DjM9pZmtNeQWxgiO/vK3J3EluwLarHRpgxP8iAaxUssOgAI7msUhFs+
9WV7QCyMYUbcP9GqiB1EkcdOESiHORGzuFL69dlv6psohxIn7SFk+J2WmuD0/dhkRCMKuaA/OQuJ
yt5kLjfe7O+1PyKRF+sw78pr3IAnq1o21SFVna6fsBifPGA+s0iNuEBxm0a8N8EviymY1uc4NswA
43NTBqsruLM5epvfpvjskR7SieYHssvXeJEF9ja2cRbaKP2mTEkxTmHcYw1dIIP5vVIB1mjR6U5T
UTKX/EV1Oc6NLaXGGELTseLhanQxS+AC8ETkmsPFyg+/GZLbPA7WvhIHIqhPAcXG5P5pi2q38EKP
BMkHsgSAnH0F495cU2BlQ8/nZ0czSwgO5ws+I8dNaYBO0KBRSNEUfUylDQtn9eX/ZWx1fbpThOsv
dPNhAm4WJdT1LTt4gxTXAU7ATbbvzJlM0Yo9TcnBkTHWYXoEy4zf2IsWp6xPOTGAFCwzxR5BfsEx
Wh2yATwxXHknnvBWpcYnobVTMzTR2gQ53QRPJN44zp2brKzThrHudmbRiSLLNCFIGmurdcVF4cPH
rj2PsxXzoJ+BW+sCMeJp56E4zBXjDg8o2Eqdip4N6khzg4xIyX/1HJ92q9g/vA8S2hfli0lC+lt8
6le6SWDndSt12MXOtl3LARCHK00rrP96FFFZMygOf5cVIY76jekpjaZJvTCYN6uIjV1BjowWc5Cu
HXdmjv/Ws3/SXEKxv/1+YsvQd0Zm0FNBf1/kzUSPXy3t7wYy2ln/kok7rSRb855oFrHa+vNefEg5
jve6595zVGk88c44trxJOnoI9dd/Tbz8fmdO3qz+hwFRvZAFy2vW7SGytZZhipQCJXEMNUxljdQK
1xhpA+WY32Rhu68hCQp3LozXu54vBGVg9NFw/3F5USicGiBdlJ/W0XmjPJAqsx5q60iwoCtBgrmg
/ccRa06EC8lc4soKjAwbIZ3djBX4p98psctB11Ch9ud73eib89O5WkMPKGSeX+Em21xx0fbxzcPP
43Bgz3Ga18KnL0g2tQ1KSQcsJ8VXGtNHlGFlT3A1rJnKtDD9RUA9+32XjZlkR/qRnzT3fkmPmfg9
ti38VoR+Hd3IOtNqWeEF12R+Gzy158nE5TWQbqFM+YoNxGG4Zrlrx8E82NIQy7FLQqpAZXQz5R8Q
YnNNFDXDlmcZpFv1Bb1+7NcmkPSGnxvVNYA39z+gniIFPp+0cC+O0xnIaZgo2V5MRGyBsfh1EdrB
YRZqsdgafeK8ZBJkO4qjJKrAty7ehN4AG7xRw31BvO985uxChpDGOuYXEImn6WgC4pHI8pP9eYTM
uGzBTr9L/1HYU0ITsSJyUIkjQnG7SATcv4Jg6KxhsHRbpFwe/bkbgRRLG8rMRiYgXL6B1sAtAPhy
MbTCpdsCNOCEWpqVyUy33nDa1KRzTPRDQRWz7Gbm3FAh+0YDB5egBQEaywCYi8IJ/H2aekYZYnsX
YwLHCWR6RjmGwVuUpAApUwag5VCk8Rc42i1CIsIoFYfy7hpzV5GwuAr8uuBXgDWvSvYWcBL4Mw4l
XUt7vsH8WwvmdxKxCRV9yEkaYS8qy/SiePE3H/mEnEzY0VdoTfOzYnon2JI7vueJaw+MjNpay1cV
bH+tt2RevChOfcrCM24Sv6VewvSfCsI5D7FJ4qZQQ3kgcWkVdWvQ0IvBWkANs+rmFNWf2o4ZclvD
sj7zSmnfVnowxAhbuK7ygS67ZeA4vD4spou2dro204woQrxxBfHU3I+YMAAnG5Hdxncyf/nxypQr
p0XKgdFE2RsJbyTBDV9TlAl1PsjbVXcjVC5TYr3bJpLUbsxmx8TCDPZDS6fFG48fm175zi6OWH0M
yY+zKhgfu3hSZiQzmyUwvhb4KwlSV48qBrymGCVSLq9v4ehxs7f3MuVsBHxQZNjGG8gomHuEsUtr
rzfIVMlF9rvQi60R10+HXtiFXuReBCg1YzISC6QbaTlJWbuLxtdr2bBxiEnIxh/da18A6SKHp+qj
hbagDZvvUwtU/B+R/APlno3IgPyTPNk5iR6zQ5RMecMZzQiq6JPFepcFZ9zicgjzE0x+4jbj6oJa
rA6D9CqCCWyde86mRa8iF2aDX+VlA7KdHsO1iyxdz+xl56BKKV+1a1a9RrxY36Bd0LJl1xnfdBsk
swuiXmJt2C+A7H2A3hvk2EaIcHkfcClgYlzSKdNzL2xYpdGWLHhIuVBcpL2ZxixuXdHsImA3UiU+
6bTYcwMHJ/aH3UIEILoi4ngDWdkCoG+sgPcUec9RdQSNrisRJcYxPBu/S9aMHEUaqtB1gGDK5Jqx
8ppySfgYMtHvIBMjxLNkGBddac3H82ziv4z/XFCoKbJ7UOroN5jSScBSUmqpr6neYKQ5vDbsd6nS
ufCD00UbU7HoZK3NowIJWca60N+gBeoWZNPL0Vi8no6rwVhjsdTT7qs6vzLoa6Y1zMlGGmN9VqlX
9sikkaqldg/JjQXxGFuCZwx/+u5m1+WtAMBoyMx9HyUDnPFMxhOX02NHcwQ9GkKu34xekjwq7txr
CoGIhffpyQd6a7ctvDRjkj/l/cfkqhGoscCgxKK80frlPO26C8o5HcR0jfHBopt7oZ0a/fBrUwnt
vXOlB8Bv20sHfJKbQGDcwQatbSlXA6KJQ7oOduvR1D9+CoaaKjxNvVuRELrePjwMI2jagehgFQP3
oWKVD+FMR1WMzKuHtfVRaSWC0ftQt0bsutaxKMLyxLnKvT9pfIcAhv5M2UACF59xckgt2hFF1Jcx
ZioybYghRRgaMHqEPwyMw/YJryIcyPoSGgAy9jrnueexrWfSrr8RzSq/rynkRlQjB3FYLwNzE2OX
Notl5You4/TlEM4ka748eFaF2Q3g1QXV+qzeSAB3pURo0tkOVuYwvGeOTf1ZbYcfxVjC2xvkvjif
fyi6SiwbA2lW16tZPuW11GtDTT4lNNGMhcIYbf/Aodo3OsCrUwQ8QAsF3r+A04u81iS6Zsoepydd
zi9RmDqGuhZKKkdm5gWFVdFl/AGxKdu/7N9cA5xehsDUA2UK/cirdqHsWREvJs7kZ0f/T1AD6sT0
ixPneQrnTO2KLAqPXaziaHZBCiSIZwpzNhmArBSx0yDeSDRPdpKqGLGF54FQQREFLosAhuG5dG93
AQAjL6SHliERRnKN40BzlcUroii+sTF6wrelCDIotgphvXTQm7XMA4VlKDAYs5RMESBtTixSxegF
o3/0dunyR5gTvVTc6Ki2/vGz01gTTUzVKi9hEEf85IQWRXbHyDjP5pZlmkesS56WHETbZgNXv95j
tBozwWBE7rMJaMGVdrxkuFfRX7R+A7VDPDvQBLDdw40oNXAUmGg5pGyqSQ8s1ckQQi4DbcfP4rQ2
DTIg/qvfpmCZ26AJb//gJvmZnve8VchfeZ94M9gV3mj6CZWXeMnvX4hF4iWPNoc2qgOioDzifF4f
81hYTFymdAv4YDfh23U0pV+2epZiLobX1ar4pBMhdWV69Y0AfTPc/ivW5zlQVDFuwm3qUX4PZymE
1XQSCOXm5A4zY83hHKV+jHQL4lL8IjzJvwsXh2zomoWZPKIjFhjfG1K8Z1CLbyafIFS7zWpKBtCv
+IBZt9ZHWqKab0Q8lzW6v7YVrGfrY7J9YwQXMGOe24XyeBdllDHfxqlE/JiA5cLdzSLyeiP/pgDY
f5NvcRyvqi9BBL4L1sNluqcTSZyDXLOqmekrPdgztWYsfdYiwCOtKchLJpeXiUiN9vqArzVPC56R
oj3eniO17RjTKDS3sV+AvtEtjjP9geKO1zZT7WT1qgtN7XbFcT4ZcLx4TiYXgtUF/r6yPLPYvrEH
gP/Gum7H7Sm3K8SHRNAl7VmufRZUS/6+W0DcC/YgzPfuQrEAI+bH29XfW3V24q4j2ifyg9w66nVK
bzfgvXmkiKXfoCbIqzJ6aiNO/7svybWc8nJ3ZQRJg7zTy0m4r6UOcS/l9EoxTqAmEgCaU16m7rzj
tvradnYZCpG46DxjVVx+3oKd48g2ZAdSWc/gxc5JP6rE/qlXoM2tPd1NhBp1hSi8CKbWblqaV2RH
ghLPIdYZyfQbMZTq0I0duth585CZ7Q+enbIjiO7mh74ZKzINRVxD2SYOdytDNEYR3ALnkqqW386n
o3iGBRmflc7fuLShAPnuHFVi+/30sfQ3n4RqpyJpUnNTUt1nNp5kXtCMrq5nzAjYj6Zsp4q45Qb6
+tyEpW+X6Rd/SWypDiwWPGCGGoUP/I2RkOmuOUemOfGq0JjPR4gtHpa+sZVV79OQK3Bc0R77WCiv
wH7LFEwklNgHAw1asJdpfyuISBun5MzmkY5OL/3sk6ne8KUXVILoTM1IYKEhAdi2ZEj3OKdB+nmu
FpZvUFUzcGQu4orjawY8SJaISOBBUYdoHMPLeUf1nAV7MRUpGSaRQBYFJQWHzOdpEj1UOQZhUiXV
+LOAJ4qtHShGIoRyDl8DfN0KHCs8O6g6m8KJCZhH8Fy6slOi+7D7zmEcGRa5neb7gBMYD7/2Bn21
x0ZK+fEXiDEGua7djSVIgAEZHOK9aoocNw9rA2KMGDuouFU0gqAE6zNKCtaDAu1i47e/SgfW+fO7
O5u4EHMt8e0Ogzdhw363OTeTI+Al8VJPk5WXINDsE8X0Qtlt/fRrfnF7oVDU/Alngj7KsPEaoIt3
nh+i+r2PrtKvoOMd/DTH+DJYTaL0IZd9vBfuJnzdSa/48Q0YlcoW8a67cCKZGfgBDFziFeLzPVJQ
kOuwvnR2wVVSjowfZuP0Tw6xpVk4spALo0C1zzVTikFB+CrzvoLwnovh8T17ogPMduJYNATfuipI
kqt54WCg5vRlSdE0i67ycMPGce2z3RxFfAVnxrL3dLr07nv9lqIOgjL5JfSYBVcy0yfSdF49vi8e
hcB4NDAv54vzLtzThPhgWMZ0h1e3xRCWQvbsWPsXbpGYzc0zZ1m7zzkAfe5qfsW45+ssTU9upzJ7
HEKrBBpbdKstwBA13ezU2qa3PIR4BAp9MElOXfEuTRtlq4nr7AD4M9pUigCazexEuL9M03Th8uR6
h+pJEveI7COrKjwOabxySSuVJcqIqlkoy69LizHS0N0szqyq0mXZx0cj9U0ge5R80CTFeyil2Ile
R2AbH6MSfcRiPxUUqFaSS/5wsUGpKNQMP8GBOGej/ht4sf2mhwM8f4vztkjDk/SEVGwmfNGJ/bar
bY/NqyeUg3mgLEVHXKFCdsx9QZ84h6e3AcoOAQy7/ZzOQoj46wfAsTVo8nIqNy9zvuwniVMpt8Bw
SmUtMSx2e0a3KYgJJa5v78Gb9fwOHJN6fGInT51vkhChDsoAfCcs3wb6Dybn2HlJJFi5VuVJtTnm
HqoGbQ2+7vtw4G8QVbGgdUQUyMQq/marHa1LLMyteqKNb1qrNgvUx9ouJqrTiGylsiokhIefYeeA
GeZIqRLpLFgYZiGeE7o/56wsv6Oz0jqUp0/R1VEuAqvcxiUl/PVRjFLaX1figvs4Vauqu3yQCJkM
STdlW+RtmbhHXIQLa9P3RmXSjFCAPX1znH9J93iPizHpv/os6dGX+HRXwDwWTjPfrgiFX0tO8bno
pPTfX0nOD0Wc+8qArZatqhadAq0aI3AAWMwQ5J+BEaqRxGmNpRIj3KIHlCQWjzjkd+vJcqiA0tCF
Dv1DFtPK0dV0/G7yV0KmY29ZaSyqvNhFQ10OnxIXYean5YC3Ye/h9Oy9KSTSe7Quh5xEQ2rz5Unl
S/kOUVS3DQOH5MEAU0rGJFztge4FDvBndqW8q5ciu+hKNyrMu13125wyXCEGM3ghErpaLXI81X3T
cdixYUAu30NMneQs5VEz8Ys+bWZEXG3uXoQNAHKK4tMYw1w4L6KXUhWOhFPA9KwsvHNkdNY6vyz5
02NWUt7eUUSgbzAumz0rKgT7VVcYYZFiYN4P3Ivdt8WlXpckhKrlTzrLpJblWCcSBRPnWks19/Ru
MEvf8wCJqyyxWptLbGOm7Aa26P2uTfkH0ebBlmUyuNvvfLoGSrU9E+KS9XtArBWOlZnxLsZRpw3M
6h113BH+3N7lY5QJrVz0Im7zOILHvfYAm44/8HMP5Nd+4DCIU+Of0I5MFofdApaxmmnK+ztduccm
4lDRTTdh8VJP3vhc54Pv/5RG/R3FwtKQC48iMLQTnrhzEfhaKAlTmxKXtb0YAlXa0J49JtAUYaqh
cklQTUd3VFo8B56QUY+kkHxFGBtyWcrlPYtXeVKc88FB2l3kbuAfeOCpKbCTj42KAY2bxj19X5JV
5GjN9t+QJS8BeYX4V3AdxC3szgdk7eDwPHcEUY8ZVkIe9PVYtZwI58fIp/9sU2V2EW7IqSwuM98v
/94zOkqp1aC6/cFcnPQ72LmpBwZbTNrGmUbBHmpMTU11I9xsXBXM7KWeKSeU8YzJId7z7bTzED8a
jxdq6DlfUwKyP3jNWGtLIqhTdD/pj28GpGn6hJbTNJuqwd8n+SyeNSLEYA0tBXM7/Qf+rdY4aauP
7tAeSV50ZNApfrj5Pkb6igI0fNb060anYbET5QcvBh6IxdCR56RklGC5Lc5VDHUmjczQjfxNipcp
9mWK6NwN9QDz6u15Pg06ojTmZsvDgmKazlR1JdVuvhpKYu579BRzJr4Vxr+h4mlYUaJljPTq6w9h
ds6wtXFOxlE0B3X5vpr39v1w1J8zSzDyYIycBmAiXYE6XtG3oZ8DqxDX1i+sGiDgbGFjrbZzJiXr
zkCSGSrEwDuTcNMjJThpOZQ0XM4Tn3w9/E4MtFvx+e0waV6QYI7c5piUZNxvg+I3AdnxomDdrIy5
+xkjCF+8IavBdwhmpFHdIRNH56eCk2lX4LBexJMgAl9rzqhfTxokjtQ3n2jlPjSHsN+LP8Bpdy5Z
WuO4dhNN6MeA0oX36kn+c3IfG5UJG4FkSO5qwYeVzjYv88vNnyMRtaSYhk/d9gAamrOG1KcGRERH
g7eMQsGFgArJ7It8Bupm6cpH1iSl6UQ/tjxl/aYnCLcHJfbwTW5tFsvimuioMOyHfEpO7Apc6MuX
ESSs93mTXSeQXkeZz3T4tefZCyxw7f6EGMratFqueMSD9/hZR/Z0q0Oo37/1zMESF37RKZ16Yfb3
G7fNg5MjTkZvHm3LqYKH/eBrcZsYGOPbBlDY+1odaPuGV0F93rB2Naz1E098UHEzuXQ9N6/7bOAI
bXw5Bvd8NK/HTDS00ozoASoz5Ljk/PtzMeDV4YtbUC20F20wabaQbEcFH7NNQGGhQdme9wadTiEB
wxVwFLe+MVd4ywB0nQB2jou3YVyHGG3Bu0CX6sTto13zlQeAEUU/VUOBNxnB0poCvR0CidXCuERP
A6o1opYXgHOE96XDNErL9jhudR7DqAjVjHsfXV9RpstLcjh1FaydoG9Sxqyy5QhPy+ex+mIt1Zu+
7bEXy4U1vycP/M4wJyeC8NBVF+oL1R0gYLieowAeMsozgP0yZQEV6++wHw0XuZ89xoOwp2esuHR8
2RqYfD8dI50+0ODTI06ZH+BXOPqqPFw8d8qruF19Y7GsHjua82yGOw+WgJZUARN3Es3yB37JYpxp
TYmTi6FSNI+g3YxkPmmtkP8ibxGfqLHolvO9rO7lht0+o0IB1a1XIyCmwdRG2j2ZaE/xf1FR0dTl
Th7x36l+N8WYpYc/Ts0+PC2cVffGChACOxCYzA4vzcPfbL8NwW71tkH3ohatdVb0Ropea+s2s8Of
EKBI3atlJpdSJwLC8LGjtAqEl7FI1OgCG7U+egVk9rUVvgeP1AhB9KNaEXKty91xCM4IZ96WW1x7
U/k4eQW4QwfZag+IsgEEQMRTd9xX/f7vJaM4R1iLluU5Xh2a2aJ6XKdN1gKdILUP6La3p846Acmh
ucKm7ksRy4ZO785vNg13/7Xfae9zZM2qtArcP8IeiyrClX8mEzwJzn5IqIXD8+S24LnpjlyeY+2a
ob71bgqHB8SS9yIeCWOOx7W+uwWM5HJoYMsGY6QO6VlGkQGOBFoSzT0uM96lwndK+70fNZxnYXvg
cgs6JX3/z3wOGe47tyctSV+ZUcnjpUTbAVEO9ecNE3bc1H3h7+uBfbm3gKgHXpHUMrugvTAkvSGo
UrUQ4Xqg8adxO3PHiBaX/8l6YhFblFhA0ChW8ADyutUTaWL5Lo5U46vJgANkVSoeXqYjLtC4Aq6s
AG0eN4AGCB1aduVinUi3j+L0QWAZaRimR913gQ4ssnobAlfGluGGGfe8C+RtHx+cHyq0YtDKYrN3
DHdEeExkfdvXlslg0u9wm/h7oRuOrbQX6177XWjrktu9GXoUn1iXHimCog7bQOtwf5qXyL7p8GUL
cGUQGfximucBmQ8jnSza/PuWNQt3EDlgUEsp1rlijfCA326NFDUOUtpVMjXj8GI/2wlRI1bT5A54
wk1+ILokB+zZw1mYkt0Rfkl7awmA3KP/St2vdAxwQmkVV92UieVchrHbLWjzSjC0OpzwU/aI8H+O
RBtLpev5G/TCYSheTvZjkZb47YUFzDlvp1j/L9G+h3VPPFC4x3DCLEN+C5crSoiKsZNGtW5AcPk0
E8m54GLQljiVKqJ0+nJBxF4RQTWIM3G2uoaUoiT7Rn1iGvfNwBhzpvH54qN4L6KFNNp3U40o0u7N
2jfd5zUO9WzZY3sOUFvrn0f+lhbKBfPgKxk18gUnJmDcoFkKyzHDsdzuKjvC/ZpDgRQNI74USP+M
p72CzjaeepDVzZ5qdC8ds9UoVLWepUhIO7ue3yeJI29hjdDeiYrk8HNl7gXbXBMUcogH1v/uJl3z
dL+3eNuXpC9OHDMkogJKGXkAClGeiAcDPkE4bjRzZgbgqqlABnvwRFQQvZhK3OoeuiwMyHGbsqCG
lKBo9JwMi+RCuOicsXvrCKSZO4TI1US3GT91L+Oq7hIDjHdnmOEEEpkRQyHExLhy+yoAEEPrqLHi
Q6tYMn8yg5wngAqAfqm+g6+BxeHM0HaVQY0RYQMRlbV5OeKREV8bm63l72cafOML7/6G3CUtCmMq
TyjFE9poMc0AGxEz/BPRjgSZgbK7i7Ty2IpNEdCeSuUVWVJ0NQikySS6SI6st9QbeOaLgZJprYt+
X+jILAJ3MFr1QEpupQ3asjcXCU1+uG+FTbWYWaBPZW+CyCeU3wnICZbbGqB57NFzJpnbFGtLWtxP
vN/scmnIlIc73g0itFzFm/DDUlzfHiNEyAUOpCuwXPikGMScueaXqjXsoizNqtD6u4iYTu2bECLV
jL0jvR0cLWJJZMNPWjfsYZRO+XTn/lr06vaBNuSGz8D2mGUIYe3R8uValtI9DvWjGDdewSw/ahyN
rsmAI0fRl6xyQ5AwLjrUfv1az7KcVZOeOnD4O8ddz7AQTEnr2tDVwugIrDmpXxm37r0UL2uhYzgy
DPesRhNyqmVQS7E5xsmsz2h7+WXQSaA3IyZPkmZ1DCf0WPZZcBk/coqTdrVRG61kVMj587RVZYrs
4oIbz5yOh2MGonQd9nO+yUz+QAVDKtfYteOOJ6nXrdYYmUPNoYJXGLJ2o5km3GIIJ6CEKZGuBY/w
rtEfPB69uzufz0FqDy7tbsvcpmzNUxCZ4OGkMqXo/9LifgWhRZlLw433PFLAJJMNgWWWH5pqlCxw
BFEI6+dVzpfr40WCy14G6MYYcqCw1avy2cr00JZQ0NPqnUZGvpN+5HiZs83I/NdxAF6LcnZrhTu9
fcMjwwn+7SDoK86I48wPO5O9/sg4mEqPq1eT7biWYHTl4DYJTGr3DVSJiljfu9rMDfEnWLUnQ5QA
ajbMX7vte1fCRxh3QJGajzBC2PCvAVuNOlH4BtgNPQhMCivHjPuIMdCBkQ38ODxD22bG/LzJbnBb
I1eN8I/3w5Rsb+Capo23481sbhPqSO7rbkpRq6rnJLfPYsKiQ2zt/d7PIXdu/Q0WSrWflXOjSAJ+
yRuS/w3xJ2U5mFclgwPQ60TkGjEV1jytcj6S4/KAKvto74T1CD9tnmw7gfgEsoAjl/Abjz0QlMzE
g1e6MA6/g5BSumOpgGj0n8asnDj0pNeQ58wGC8R8Olto2stoB6aigqi6B9mBsp9wXuT9mAF8f5Zr
R24EAUJYep3Wz4Lf2rVKwmjK69dH+2xLNGKt1vUKp/jLR/BoYN6xao1aryXNnJ3/y4lfPevCgO1e
nT4LG2CwXO6tmqe3FBJuDglUTAvsu3325pz331BPNw5vyJg50N2Dfv0l8Djv0dZ3NiB3id5+Thlo
kgTlLpVJHNBy5/5v+KKofN1DcnnpjLgeq6L7YFUF/fkN7dpG92sZO1D/pcCbiwhRXWnG5bP7wsvH
J5pSb+f/deADGXvF0zpqzgVNSG8/36AcUhZIEE3vogsljvpsCIdmm0Omh8R8dII78X4si/zK0C9a
cdcXDCgicARx9TiUNBDH/ZhYhe2ysTay13JrXn6Wz8QVXlASRjW0evigUGdtkPoQ4O70s5A6BPOy
PtQhGS+NgXaagH5+FAJo6o/BnJMkLhBrRlKhJvrDgB1wd3gUJcfaQZAHG8IWUs+d8+bzJ+8C1Dwf
g8NpN3m1u1tqf6dY0otUIVlWhGrkm7CiU8MvDiWJA1KcRZAXFb14dB/XqDIoYRM3u55T6CdeJjLg
xLz3Bn4eYSc7/5lc0B4WcUPJtGodDKd61I1DCBHOSwRfHTyvkNTpUW/nE8aEFihnGYTSde4xkqLQ
Ie5pqUTxyS0JobjofWifOfX9dWqQ/fdUDHp0R5znoWq3VrDQXZa6kvdd0snwziNUzo87X8/ncOVZ
b280Pv4tLraUx8ZNgdbH+/yUFZZy+0jtNJ+uR02NCKCD0uZUGVGxB2JYk7ig7mK7X9/uGH4qrvW6
r56qDq4QiMekVZPOew3/HbDXZg3FmAV9qkK4g02TKT4bcPIvZn5LQ4cQKH+KnJcVOJK8ep7Bpeog
7SXgOYD3c0K3/2ekSXyrWlDy8eRu5LB+8pTv/TEoyRS2zBJP/UNLed36XF0ghlW0ODauIYKjIGlw
nGszOZSh9P6Whx7vlHgA9W2sGMqqUan2uXN58aXK8CvPgt7VfdnPshj3L6+VOzmaoTKTALJX8+TF
juC3RlVafURWX2hkgtjF3I83VTDM5eWRDUDsK2pmYiScDjK2C7RbnwNgTriCjeVSn/cRinPulPkh
mHAJ/JqDU2XtKOqELPIXyYQ27hhHDWZcEf2UUkaRQ9ng2IPqTJZEW5rMZx51aC8HBj5izj7DvmgF
yicbEqQqipScPxAPf6m1N0RUoUHBFOcVk4MsyFc11k36Is1pIneJIr2BrW87hdkn1bM2zB45+Ejm
xw82EjrcMb2nz3cU7p841Fhb7+1W95hxg2TDwsQqX1Accg0jN67P2K6Uk73be+j0BKGpngJ16qlf
LypexJbatwnj+7aO+AhpY+I9Vz1olXd0lftZFZYvluhvel1e8Bt90EzcxH527thc9fcG+7E9MZBz
g3nOj7qMREXsYoJHa8cTue79Wd6pviyTPPr6xYuopuBjSrF8Wv6MJB4raOYwLdp00uhNEpq+CbQP
4WAw0V36blZPcOJMeytuUaAY63LuVHbE2VUXrRpRwweXhT8CqOEiBMicsWNmNRiavTaiZ4XTtbrZ
RBDXOnqppjV7KqqW7p4h5eE89hRm/17xZoa6RR1SsbKalcrq01RfT2ZZ0/gwtlIcG59pXwbfgW9c
wgwwCyPIl0kBFXszWTOJTKdamAbFE55Ga9Auouj0C1csx62dIC/ciwuWGmBJxhLE8Fr0ADgU1gD7
sdg71YsvoVSMOzB8pHCRbntFyTvmLboQmSvCwsGgYop6t4rtVrdXu5u0aFiflOYcZKrNRAlqKIE7
pfiNRkSTVqfZWLPBVZI/APIZr+npCmzoW+rXuiqeYCEXXp+k/811qLC8jlsZRtoQvQAjEB0of+Yg
DB/GaJCXqie8rFNNn902iZv61JQ+neAJLDtXYByNI/J5fJAGJsXbInSS/9owqIOdhETyiTZA0z91
cdjmSQBaK9TeOSg8K5lo8N+NYo7tmmEl0Mr8PfwKKUTfAzKcRgs0NYhOCQC0fya7+iEZlME7ge3H
eT9tnd6NYMC7mPPSrF9YdRxSL4Y9B3bHQuYz7BWxet2s6BlRlYLdkP3l1JXG1EsHzw0h2BV6HLIF
MLk7MhmdvTO7t90m5UZhwG9IaNKzrhWS/NC34eZeRZHvkDqPlieWD1jjgZZijIjHyFXKdIUgtEEx
Fmucphw+lgTu1y/arD493/TJee1LMGm6Ohi1oT6Mbr5kp5tIU8C+wG+fB0zhR2WLnQqPdxyhP4uO
M8XNFAf1r8zm7yQ7uSI1DPPkvthlz3o6mS5C4eTuuhek7/i4LS0HW19WfLRqOIFp0jShVx9Bfh5Y
GgCg7OSbXf0fukFn6FJd45SnnHsXaHf4de0F7TSxKjIq5XDCMOQqvRSiuacTEaTIRvUXBM8h8sFk
4SkYGc7onI2glTtd5srQEvtxHZKxBHaR4UQ40tUml6V7txLkcdRzBebkO3Kn0Jl/jheabJEkFjGk
DjhLcXhp8Fp7/Z/+PWb2PA0YD3bdbkC66WeVAVyOUlU4CF4cYKr++AoACR8MzUnVuCQ3Il6CULUh
zCsGRW0PGmA9zIOd48DbOrXt6ploHyfKw+YC4YzanW65XDZ0fwBIWPmQVK5Ii6cXmzT4EG5MIxBb
L2W8VanCX9zE1hhb3STExfzIE9lIdP1C+O2IK6nKlmqBKcEhtXXWUjp+6fE7bH2/+XRapcmPIkok
KUEUCTZZUGstZjq5A8C17fs=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 15 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 15 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 9 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 10;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 10;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "1";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "Estimated Power for IP     :     1.57205 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "ip_peakdetector_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "ip_peakdetector_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1024;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1024;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 16;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 16;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1024;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 1024;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 16;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is 16;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "blk_mem_gen_v8_4_3";
  attribute c_family : string;
  attribute c_family of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 : entity is "artix7";
end design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  doutb(15) <= \<const0>\;
  doutb(14) <= \<const0>\;
  doutb(13) <= \<const0>\;
  doutb(12) <= \<const0>\;
  doutb(11) <= \<const0>\;
  doutb(10) <= \<const0>\;
  doutb(9) <= \<const0>\;
  doutb(8) <= \<const0>\;
  doutb(7) <= \<const0>\;
  doutb(6) <= \<const0>\;
  doutb(5) <= \<const0>\;
  doutb(4) <= \<const0>\;
  doutb(3) <= \<const0>\;
  doutb(2) <= \<const0>\;
  doutb(1) <= \<const0>\;
  doutb(0) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3_synth
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      clka => clka,
      dina(15 downto 0) => dina(15 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 is
  port (
    A : in STD_LOGIC_VECTOR ( 16 downto 0 );
    B : in STD_LOGIC_VECTOR ( 16 downto 0 );
    CLK : in STD_LOGIC;
    ADD : in STD_LOGIC;
    C_IN : in STD_LOGIC;
    CE : in STD_LOGIC;
    BYPASS : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    C_OUT : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 1;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is "c_addsub_v12_0_13";
  attribute c_a_type : integer;
  attribute c_a_type of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute c_a_width : integer;
  attribute c_a_width of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 17;
  attribute c_add_mode : integer;
  attribute c_add_mode of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute c_b_type : integer;
  attribute c_b_type of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute c_b_value : string;
  attribute c_b_value of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is "00000000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 17;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 0;
  attribute c_out_width : integer;
  attribute c_out_width of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 : entity is 17;
end design_1_ip_peakdetector_0_0_c_addsub_v12_0_13;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_xst_addsub_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of xst_addsub : label is "0";
  attribute C_BORROW_LOW of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_BYPASS of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_SCLR of xst_addsub : label is 0;
  attribute C_HAS_CE of xst_addsub : label is 1;
  attribute C_HAS_SCLR of xst_addsub : label is 0;
  attribute C_HAS_SINIT of xst_addsub : label is 0;
  attribute C_HAS_SSET of xst_addsub : label is 0;
  attribute C_IMPLEMENTATION of xst_addsub : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of xst_addsub : label is 1;
  attribute C_SINIT_VAL of xst_addsub : label is "0";
  attribute C_VERBOSITY of xst_addsub : label is 0;
  attribute C_XDEVICEFAMILY of xst_addsub : label is "artix7";
  attribute c_a_type of xst_addsub : label is 0;
  attribute c_a_width of xst_addsub : label is 17;
  attribute c_add_mode of xst_addsub : label is 1;
  attribute c_b_constant of xst_addsub : label is 0;
  attribute c_b_type of xst_addsub : label is 0;
  attribute c_b_value of xst_addsub : label is "00000000000000000";
  attribute c_b_width of xst_addsub : label is 17;
  attribute c_bypass_low of xst_addsub : label is 0;
  attribute c_has_bypass of xst_addsub : label is 0;
  attribute c_has_c_in of xst_addsub : label is 0;
  attribute c_has_c_out of xst_addsub : label is 0;
  attribute c_latency of xst_addsub : label is 1;
  attribute c_out_width of xst_addsub : label is 17;
  attribute downgradeipidentifiedwarnings of xst_addsub : label is "yes";
begin
  C_OUT <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
xst_addsub: entity work.design_1_ip_peakdetector_0_0_c_addsub_v12_0_13_viv
     port map (
      A(16 downto 0) => A(16 downto 0),
      ADD => '0',
      B(16 downto 0) => B(16 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_xst_addsub_C_OUT_UNCONNECTED,
      S(16 downto 0) => S(16 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ is
  port (
    A : in STD_LOGIC_VECTOR ( 16 downto 0 );
    B : in STD_LOGIC_VECTOR ( 16 downto 0 );
    CLK : in STD_LOGIC;
    ADD : in STD_LOGIC;
    C_IN : in STD_LOGIC;
    CE : in STD_LOGIC;
    BYPASS : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    C_OUT : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is "c_addsub_v12_0_13";
  attribute c_a_type : integer;
  attribute c_a_type of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_a_width : integer;
  attribute c_a_width of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 17;
  attribute c_add_mode : integer;
  attribute c_add_mode of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_b_type : integer;
  attribute c_b_type of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_b_value : string;
  attribute c_b_value of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is "00000000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 17;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 0;
  attribute c_out_width : integer;
  attribute c_out_width of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ : entity is 17;
end \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\ is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_xst_addsub_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of xst_addsub : label is "0";
  attribute C_BORROW_LOW of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_BYPASS of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_SCLR of xst_addsub : label is 0;
  attribute C_HAS_CE of xst_addsub : label is 1;
  attribute C_HAS_SCLR of xst_addsub : label is 0;
  attribute C_HAS_SINIT of xst_addsub : label is 0;
  attribute C_HAS_SSET of xst_addsub : label is 0;
  attribute C_IMPLEMENTATION of xst_addsub : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of xst_addsub : label is 1;
  attribute C_SINIT_VAL of xst_addsub : label is "0";
  attribute C_VERBOSITY of xst_addsub : label is 0;
  attribute C_XDEVICEFAMILY of xst_addsub : label is "artix7";
  attribute c_a_type of xst_addsub : label is 0;
  attribute c_a_width of xst_addsub : label is 17;
  attribute c_add_mode of xst_addsub : label is 1;
  attribute c_b_constant of xst_addsub : label is 0;
  attribute c_b_type of xst_addsub : label is 0;
  attribute c_b_value of xst_addsub : label is "00000000000000000";
  attribute c_b_width of xst_addsub : label is 17;
  attribute c_bypass_low of xst_addsub : label is 0;
  attribute c_has_bypass of xst_addsub : label is 0;
  attribute c_has_c_in of xst_addsub : label is 0;
  attribute c_has_c_out of xst_addsub : label is 0;
  attribute c_latency of xst_addsub : label is 1;
  attribute c_out_width of xst_addsub : label is 17;
  attribute downgradeipidentifiedwarnings of xst_addsub : label is "yes";
begin
  C_OUT <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
xst_addsub: entity work.\design_1_ip_peakdetector_0_0_c_addsub_v12_0_13_viv__2\
     port map (
      A(16 downto 0) => A(16 downto 0),
      ADD => '0',
      B(16 downto 0) => B(16 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_xst_addsub_C_OUT_UNCONNECTED,
      S(16 downto 0) => S(16 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
I27g6MYD4MW0QL8wlWDusLpmUQr++VIrJciW4kxAZvD1xu4xHc71nmmtQKn4RXprTItt3VZ4jlYb
W7sDjTzN9g==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZCd7ncP4pCL3bR7D848emv5B1F3GIhdyKdOrR8YDzkxL987Xx05dubKjo4b+YR7j9Ecj4dvvZe1H
6adMwXtSnoRoi30g04SXknicgV7eAYs7TVosxFfBt1CNgOyzN0bQbEUEFceUCCSfx8G96lJC7l6k
w+A3ZWFjWJVg1Vt6/vk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WdPinNhXowL0eItMQjILfN3ZtNRlAFPcgUKQgbQY6PjgOLOq8AYH8MaWMxwJ1Q3XLuR/CpJ3Msf8
9ck26xaGFODD49GyvXEm1m8jiFYiUuki4s8taTUnBUe8UmyAXLQtV80x3pecHd3LuDlAHzqlwHLb
RwNUJ8jmu/WUllodatqqpKOBuaenIRptuK0/OJ4m/EhxfYY9CrhNzvJ2OB+5wKW4GAF64RWui9+x
Bqw+bOAFz/60QdWNUEfYkftioYtQ1bPR23AMgCCwss6e7ZAS1ZDoUsWca/IzfDGAnuGA5fYYttxq
uoEZzpjupgu6Vfe6XH1ShFfcTJds9diYIBxF7w==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
J2PI9pkL6e7AvAtwdDAKsuYeJfRneKgQO+nHNIiAxTtlsTW9qYfsuK9HIkf0Sb62x0qE564ViGrS
3wuHHLwljmlXkNuh3H5s1WSIvBYog56SpodBhW9K9QsQbL95ZzCqaRj4TQCbxUYoSGwFsdPmmdM7
La05z8feuoK9AbXfQhrl8CwxQ/x9xQWu9KqCdtni2/rS3DBIIvXrfDnjMLkEKBjcfotyV3n5YDPS
xsl7G0OdCDCYK2w60G1P6raIszyzlL95Ntk3rgPPX40+b45hMQ7F7lMQBoxIOoJb8u1v/MHG31i3
lgLBBsa7tA8yQ8i1322megfZnBtOkWTPkmGATA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T2bPt/UsVlHeNg5Le1CxaL61udjoxzw96Kj3hgyH1tUYL6gN3XkvsqfcbtTEYOMh8h8ccS0iuxDR
u1AEFZYGDETWy4UIcIXJiMNJW9mKlMxeX0aYJmt2gk5SzH3rwCtKFV4BE7w8PwEho0CpeEjRbnf/
H/ZBsUMPSTkaly3dWhZ8iwOFxUFab/dTKOrq+/zYR0ymNFfbFDioA0t6O1QvpkQKgAmLKtz48cIZ
d8mpcUcwmUu2smVlnmcYowYxe9PRMHJNRJBKGeYM3CGauAWJ//wpfST4UNmOFt1mBZDKyRVEtdmb
fxus/Lw6a1mgHZoYg82xB4qQBrCBG6m9hlSvQA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
O9o1BY8Sbymv0CVlrYskQzxGr+xrqarG7EPRt/pk31lim//eAelugbq/q5/E+p/kQuJs9wJgGlt7
J4EL+RC/2F1gmfEvMCf58rYENifz93c15KzjI9WIzhK9r51ZA8fR9ZyTMeOrkrakuwslx8gz2Nd8
GtHVrcoDTocBpHaQfUc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
B1N78V5eMBUsTRlKv5Ei+E+GTZzSlJSEsn133UlYjp9l9fAT6QvueXKaAUXJstu/mKRIG6pF2HeT
10E1KqusBF+N1vx0mJm+BGQmw8VwnDo/1C0bMq/T7k3skIGTTlpmmE04UhvTFZ5eNWSXF3hpRbnX
7LVz1QBuLq3y5vrznsX3rcSujxWkx6qVpUyrOGyzLDbbTs8ND/8qOUNz+Tsj2+A/fjXiwPURl7Ci
pqZKDVAkBrWTm4hLakBaJZUjicezF1sJAkTUk1hm+9WjXbPsux0DtpZ2Gw6FtD0vv/mLn3UAKK8N
uj2yxGASMChllxrDzGBUQlWEDBX02i/b8OMnHA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
fwyM9gZlpHjR4teHyoGV5bSUq0/oX6w42Ha9kfEq9uEKmoUoM/mBNinn+W/Eo2375gi+AHjitb/9
6PlxX9Mn5h63Bo2BJGAs4/ZdX4LPJIydgWGbPXmUZvoVF7K6awzoU/TafUwZmIuMJUovA5TNmoBr
W9xun2XcPwHn6i1T946QYzudHFxljmDv4TGxKXOHknpJyrqrdbMJZa6zdkawt9rQrCiVveuEk4zc
kmNKcftxFJJZpPoKeojlWrvidh6rbj+1mqjfJKNv3BFVeQ9tPYE/j+tAfkwLF0N5uOjSgmHSDDit
6bl4tNDapS+n6fO2Wa2wo7akzQ5yZQylJqhKLw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
S0zIXRRAxV5G9DykoLZOAeehj9GKq1C7kxMtU8ROSlC86aiLGWVJ/4yjKFk3YYm4032VGT5lgWS/
nWa3bEF05H7WnBg7fOpWVB5OD43+p+KrFooxXewokbxckOiJLHF12IshN0g/d/OQIXVZokIm9hRI
cdAGqgtDRqIPejuwaO1Sq7Bd3NFwB1LD5xxKqfVgnCW7s+QE3yygQ3YULWtTl9nIiRDCUy+SKB3j
HCFd4aBLqFN+IfMCCvUHM8eYxv0/GhF61e+13wWHG+UIQKTX0FxCRXPZ2dywfAS7VA1F7ouNFyjL
5ka8jA5HWcByhsezJBNaHwWTaSfwgjCsmY7xLQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4768)
`protect data_block
DPnXrLJTY4ssG854byo+rsYRPBLUK1LZiQJyR5lsSkFFw/a6fJw515/19j92NENUvpgIWTTAhZLL
WTcd2M0E/bisxS4Nbyv1vfHQ1AzyXlfBGB1hzLWj1lcrBIo/FA4C90w6twJUvlg73qC7K6bbVp4U
qHLyuX1VtH8Uf1mVV7L21Xu5yk8aT80BW2CgW+/qEEoIbK/2YHQ9v4zICOO/GLB5bLzkzy1u9S4c
VKcxfN6ARFnjX8IFeKfL/jbOMk7z8vo8AVu/rbAtOdbSnNMXNn1dLtUHHupUpvQJ0XunneXUu9bB
bfDUneNTP0qQ9acd9GYY4T7qFiGY4q91E0mjFiXSRVmuJtGYFFUU4fz0lLKfpXJdim4JhsPvUGN+
Hyvel+RyxsvRlh/p5PtCmWlnFdgI2VIaqkLTl67hfOlph5nkeG3rSkyypeQsN/j14VgIwU2REQnI
IXsxwhKBW7UCMIVO1BWv/KKwTCa6KrGjU1T0MNMENT+jBT4xDbp7UxU5fYF0nd4bnh7gFN1g20s5
OSkpJXrSikx422tyoZH14N8yQfUPQLqK2tHTOMOZffRv09J+PqIms4MvvPV7kj7E3LwbAhy8AlJK
LffBuODpn8Rnjpbg3/8jqPPJ/Fudq+oR2mg6p4mgqp+f9t49EREY1n3/MnXEN4RXFjyp38FExk4r
s+heBBcDogrQ5ijvA0xqawrnyB1My88K/mo6mnHQuSxmXz+RkNoWsYapA6onpHgSzVrlzJ/F3iHf
FGOlO60icAThkMr1W0xpmdMQvQC1e/SL7I2B4Wo9nIrAcZfrNp1spSwtSHFf1c2TIo9Nf7z4/ZuK
5FUuxyQh3DL6MfYirLpvVJgzZRZUEvLi92W9vi1453YE7LqooVoHSvYym0YqSpCuj/004LFtFcSZ
mLlhYpS61aA/4PsmacB9Onp4cRaFK9dvlD3ReMV17p2I0+i96KFej7jtqJ66MedJEkHpg0vQVDYz
peB4MilkyPlBtIUVTVe4Va/nQyURt2GXC72d5jCbFOeNXj0dzmsvoWbCSJ+mG2FGQcHXHhSabXpL
14/rB0i1DyLi4yvqJpjNFat20eKjAIkofBBz6c076D13Avzh7tcrEmOjqDEssKkglqZJvQ1epfeK
A1dYdVvJeyQMvn8X+1ZD0FCxYQOvPiPBLWgjVuOuen/VWeEaon3tbDewy8pAtTt58e7XtYeQkDO3
eFfgKU4BKZUQApNnWKZ3caq4DWR6ZpXiuviEVImfWJq6w90d37BO2zw4P6uoW3Lf6eGioSk5zLiv
vz3N2f5KrUE/+OdpzZ3GA6C9562HuxMG7i7/PVr3fDs9gYWsT3y5sDoknTS7/Ch5qtf0rn2zONFw
YuQQ7xNykwdQLjpNju3/QNIftZW2/+nXRUQbCZeZJbBCrhgJcjsvMlje5l03Ao77L1lBdvw8FNM0
709CIPpDwxSUsRhSBuTHCvF9IxvRw+d4n/oY06Ws9Hy79YNxpRz38vwxG0pD38jmkaFdK51ZBGLE
+RfW3RwaOlGxbaYtcBtuJ0WzQkmTqawAqx2vglML5nIbSsFdsfO+xipYeMPtzk5qxTtkJEg6KE+T
vRByT5D+X9AwcBTwROzHfiv4sDoqhE5g5lFSEXgIHyUj4VUva7t95GmEoHloyJUxwLlyrbUPtMfi
1+Chs6C7UTZ9G0YP9HBC8K69rms/jgrYibFF82vFMRMlDvXpgR7vUqBRHPaUWTopHAAavUZZSYYN
Lw+bbsHY4qJ3gQIbjSbJf62Id1uvwQJbV90wtUr6RQgIdWMk/1K2nFAoloAm0dMnFYYgLOYWFOxI
t/SnX2Y4WO9kCZs9Hn2evDp1+SfhnQhbO02EjNIeiaefnVuzPRsA6TNNZkK8qdaycMmBqJt3nQAQ
oIMZIGKpqC2VOzA0/DFWYlx3aQY+I4BrtSaa5jZGNPloDfuDhOjt9kIn0iMhBWqnhjvcy20KNW4X
WbYDnwIivBHuAmOUzqgc7A2AIWKcNm4XJ2y+ZX2nQFrOQaSYH1AuTf8wAErb2XNDUSAmgF19bGpE
SGjGHy84tm8LMBinOeSm8TSNKNZidtjjRtl+oo28FvKpCsVvbTPlNWpYiwDShp9Vylk/3pGKG0gi
W/12IcPuKAL9Pr02qSXgur1mmW+No/YQhZk9p7rQkGVWxmXt0mSR/GvqctLr5EOcgK6WfceKuZP2
+FekKIbEhInTyRDGtPyGbJjsXb/sxCFNUq6E0eGGE44spwZzgdqU18vQ2Sk9wUUFpKAHrF3/1CPX
CFRS/pptX0HHvLVBpRX3OM1OozSUtJTuB+FlJ7RAqpcTxow7YxQ6Uixh2stKQq/Pf/FbYTf3tM2J
5bTRkBvGTT3aL0USLCgNVxPGg4loBcujV7rbr+KZ+rxP69ep4SCmkNK14vcc/bXcbbH6Kl15PNCK
7umQv5csIzIuHDOB+ejYu6KGarEt0u/WgIydXTQakuUnzPuCC0xw+PIiXJ0gye7ulIgv7rP/WGZF
vEuDB0bJO8Xb8ALcwnQZJUShq+9BPvREHZBlN6+MzEHiuL64U/7tqndLWnPHIQ/bvcnO7vOoij5G
NS638KKLsUfbtGMynEzbkPo5dd37+3lwaUP4ExKek4sXldeICj7ucYKxy7VKnbXSucu0SDcpAL5k
K81mlH6fiQ/kaw/GFkT78pjrcZ2cduH0g74WZ3VU2rq/RbQ21fMG+zu1HRxqhgVclVHZuy5j6ROA
amK2G28/xjCOOzGRwMmRnrw/oxHn/hFfogl/O6zMStOF5r9uxLpaKI4TlWQ3f6dB+O4CX96FYt1r
0Vz/GrgJ/eksnriBqoZSG1wQleYe5ICYEakfe69V2f1qXnviOS9Oq+4lC+v7wOpj60nEvN1Y+2C5
PQokwV1laHnEI2xmwdsxVbdyuLr23F4rhMsNaXTcq2g2Co8vwmK//D2hpbQ2JusLgmtZAkNvhJmS
RNRO4y9DUzyurfWjGmI3Wt2Q79YwNSBI7tojG+yIjAdtweoMyEzgWsb9MAmHmgsJsxbuaXYvpHvQ
nsHBim7ySZRAvYne+9JkZUmvTuH0uh1weTkx2D6N+hbQbp5BYsv1c244WswQ1pjhsKbwcqLLLd8b
4DnjofTeuAwMu5mSFvXs3OcnS25wAaz/ns4AinCRSKOEufd1BKBvMY4NoZhN7Qdr97t/bkc2PM5X
RIy74aRXEh3KU+iS4+F5WM2y8WreTQ1gX2FuQ9mKJum4w2Rw5MAWW8gj5hv2bkIKXVnzrbtY4uNO
VZZSgMCL1CjIzB8PknloyBwHaOYnUETSEE0F+isvbFb94noV+V+yac8eAJkX7LlVt9EIjN+eLVgk
rpmRaqd5Btb3v8ldarQZpOHLT8e2GFwEv3yWytnewPF0wcPK+Pli+3HI+lRkwALkYH4zwDi+egdk
m3jPbo6UEwzjDxVGoHTNxssCa4KzL3ejZ7jccjIj71RNttydv0Zem4/gy+ajFT6A3s6JEzDPxQ5/
zGgIcdBDj2RrJV7c8yX1jgdpnk0V20PQ5L0353TTuTrNUrzC8TwpacjFWxQvjJKxRAFqrXgK7hKc
Hp3ot/Jtbyz6V9pZDD9jniM3duq5lVaLUvZDG1IqYjYU/G+LL88jr0DesPljV9u4N4LVBIPZUfq0
ONvEJVlC5fzPELMJJQTT44cCZCSh/e3sg49hyG+9r28itlzbMJE/ilY3Ic4WwBgsirjxd2C2fGhK
UKidwXUyfcMN31fMamukhKPIpDm3JLr7j//jalYEF/M4PPruh4FhAS54NFKg1nzGfpxJEIXXyTsz
MmPZ39HxfugBhqGUJw3R20WYCMXXJcPG/9o4wwm6JKPc5cLxMYiTk8fDBHi2EyFYA7f2AMlpny8g
QHpES50B9f8nY5ACyEv1z8HZ/6deMC3xA7mxgQTRRvAuhgAJhwn0P/999dhbeakAfBZGOumnUlq6
B7LSxjPRPp4IL2UpF9XRBzNLrKs6UZOuiPTthAddiXTldhkDwJOTJfgR3RaOcgcsmVyuHh+0Vh17
dJP73vEN8NJY7ywtwpmcpKsYPD8xuNb1qNOI82bEKheiQxgOH4rSYJAv6p/x3mRzgsYV+HLx2veN
2YeRcE+hSgrmeZRQV15f94gfl6SSWG7HzeIlPvqZGtrt+Mv1fpdaeKX/PKGagjLtYb4c+LqMEO1o
mi5mTbyjjxyaTIDmHR4dydTbwB8KEs+Dja9DRf26vVA5u6vfJ8dz/hQoH1HQ6fslTAYK+wR+Twn2
St66LPp2VUgm1VyN5PLGuGeuq3NbciNE/dqU3zWxBufRbmYpXuWEPZ+O0CMcyhDx42A/eQSgYbN+
FZfA6MHdet7J1c+IsTgjgQx4v0aTzx76y2dvvP5Az32T/NeLGxHO/6X7AhgWV7ZMjrY24NyByB+a
kL4BNoYwz05sexXEgn9RxcI4UBeLehgrJi8w5GRODWxYVdeddxLlpcoPMzlB7vbSGQR3NlAlp/7K
5tz26rvMPkBOAydpWynrMDp0G6TA1IcY2OHzFTQ+HJRi4dBcRElwh0AE/TgfiGxB0WxpsQjbYKa2
BtnXszrX1lGkRgxNDPa6JXD1l06P2/jnmjmKwMKbH/ijXzatYLdmyUINOgjWyWvk+eHKRQEs7Ls2
0+KNn//INHQhpXq+V20F7i+h69Pr4NfD8EKPl8kzuxKEZyhK5l5tgAu5/sAUgzBR0eS/uZOjreXg
NFHZwDGhrq/HxlEMYRPAgnJ0xJD0ohZAurHsEjJ2ZrHnqdZfODCejZpr7wpwCww3VDDebO7tB5YX
gCFQpMsJdlBvpy30I64DzMNB4iLPpiJc72Td6c8kin9tx3eeQkwMnTEoKKrdl0kjbrx7e6CeVU72
OqlgOtqQcf60lW0wRyADJzh9gYj3y4x7CNu0fULvDm7WF3ZI0mmeLSbsI5nOwBP7aVEmhCJNNsp4
KwuTHiLWGhm2zXi93qWqGcUeqgAu0HcWm/Nn/4kkNt2kTvsl6fYag3lhl7pkqC2rHPVnyaHwnej3
jxCEbISjVFEM7kqA/r0e0kFc4/a5bRm4Y2Nu8gOYLl562KXHJu9aDYdai5YlV8ozJsbXy4G6n+7x
/zqmpRJtVmJMfW5e0SJdfpDqw6RjVUwpSrLQoYQseh57JlHR96xa7vVG6ZtJWOobPZLp15tHMJkA
JyRa+dJIyvdgUIGBRTyq/leGLzr8FowDcUCELfYNn2Df70uxkHPjHu4Yeq4E8EK+TwIjLfHIZoSz
4uzEbGPbmvX6MP0yj6gkjvUU4zEJ3k7svKGlVWJhHhBWD7kBbaD6MAb6yrYL9SbtnG/hSoSW8UJQ
fJ9M3KAFNAsEMLPMdgRyjlZhqUgDkumzRYXJdbYf7lyawIdrcKNxq1d7DBR+3mG3ld6qnnGwz1Dd
1hunVsO5sAbiepsePPSAuBRa20V1NSVei7o3vsiPiyoCQVvxS2cg/f6aP/nUM2qmaad2sX8xpFf+
Ev/NN2XIAVB5Z2hxvLy2RpyF7Kie1epgxiVQ51jBRFgDfFxWQ4U91yOK97zf/sUELcYZPJ5kgFxq
N2ZE0F8ay9X2IJfegi7u8sZEeveiotztBnrJg/vByvUPmMFo4K5fE4arg9+oRRULxuLb2UkNcZjV
+sJ+KNTrxccgdKMF/m5HAZqWJMYV1D9zwspRfjChCHBuonyCOcZ5ncEx15uzgE917pBGkeoQ1luR
paHnZmjQQlrWQS9A00Xk7UffjzfpIqs777K244BhQ4spvSkJjQExXq0Gw5P1fpr/CEUAd2Epg5yc
RRcwXYzNWx1wa0c+ey5Jl33uhOZnaKD5DkKQt3n8um9g/k4CuaeRNm9XRWJl//pU4DC6p3LpZmEa
69bdz0A+zMpxG5Z/oVvpv/L0TFOctto2xDlPZHgiRwVe7qHcEUDy0Lg5VEb72Kao/H2Tg5a12UFv
Ng8+yFBkfhwLOT7WOHfueKhvlBek30nDJiUnm+lz2hk4bFYJk/OZSvc0KiPRktmrze6ei0fsshif
pdlLEh8x6LLdkDsgPRzcaB6a4k9zzqnrvBrWEgHfJ/sqJ2hwWauwziHdMUiqBJk9Akb6Nye4xQAg
v7ec7Mm/402ITm0AyJOyUViryiAQDnaA6csFTbF3WJUg1oYXeF7sptL45JDg0S8KDQo6heq++XSi
iWaQd2cVml80BSb3Fn5MewoiNROY34fLpFY/md7y8cOBgazTpqRpWvm97/Oij5T5aBlliUsOYeHt
SWHuIcWXssQ+DFv/T4S7QQ2rtT1Omfd0gs+56XjZCuk9RKy1A4EE108zIDVFvKewN+xMC/OgaBFM
3wuwet8Lq6dKbL7PPj1giymC3vrZ46SKJ66aOYP4y+kTgmc/RA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0 : entity is "ip_peakdetector_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0 : entity is "ip_peakdetector_blk_mem_gen_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0 : entity is "blk_mem_gen_v8_4_3,Vivado 2019.1";
end design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_doutb_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 10;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 10;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "1";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "0";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     1.57205 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_peakdetector_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_peakdetector_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 1024;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 1024;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 16;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 16;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 1024;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 1024;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 16;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 16;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
begin
U0: entity work.design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3
     port map (
      addra(9 downto 0) => addra(9 downto 0),
      addrb(9 downto 0) => B"0000000000",
      clka => clka,
      clkb => '0',
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(15 downto 0) => dina(15 downto 0),
      dinb(15 downto 0) => B"0000000000000000",
      douta(15 downto 0) => douta(15 downto 0),
      doutb(15 downto 0) => NLW_U0_doutb_UNCONNECTED(15 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(9 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(9 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(9 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(9 downto 0),
      s_axi_rdata(15 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(15 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(15 downto 0) => B"0000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0 is
  port (
    A : in STD_LOGIC_VECTOR ( 16 downto 0 );
    B : in STD_LOGIC_VECTOR ( 16 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0 : entity is "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0 : entity is "ip_peakdetector_c_addsub_v12_0_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0 : entity is "c_addsub_v12_0_13,Vivado 2019.1";
end design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0 is
  signal NLW_U0_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of U0 : label is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of U0 : label is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_a_type : integer;
  attribute c_a_type of U0 : label is 0;
  attribute c_a_width : integer;
  attribute c_a_width of U0 : label is 17;
  attribute c_add_mode : integer;
  attribute c_add_mode of U0 : label is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of U0 : label is 0;
  attribute c_b_type : integer;
  attribute c_b_type of U0 : label is 0;
  attribute c_b_value : string;
  attribute c_b_value of U0 : label is "00000000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of U0 : label is 17;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of U0 : label is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of U0 : label is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of U0 : label is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_out_width : integer;
  attribute c_out_width of U0 : label is 17;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of A : signal is "xilinx.com:signal:data:1.0 a_intf DATA";
  attribute X_INTERFACE_PARAMETER of A : signal is "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of B : signal is "xilinx.com:signal:data:1.0 b_intf DATA";
  attribute X_INTERFACE_PARAMETER of B : signal is "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of S : signal is "xilinx.com:signal:data:1.0 s_intf DATA";
  attribute X_INTERFACE_PARAMETER of S : signal is "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef";
begin
U0: entity work.design_1_ip_peakdetector_0_0_c_addsub_v12_0_13
     port map (
      A(16 downto 0) => A(16 downto 0),
      ADD => '1',
      B(16 downto 0) => B(16 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_U0_C_OUT_UNCONNECTED,
      S(16 downto 0) => S(16 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2\ is
  port (
    A : in STD_LOGIC_VECTOR ( 16 downto 0 );
    B : in STD_LOGIC_VECTOR ( 16 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2\ : entity is "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of \design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2\ : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2\ : entity is "ip_peakdetector_c_addsub_v12_0_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of \design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2\ : entity is "c_addsub_v12_0_13,Vivado 2019.1";
end \design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2\ is
  signal NLW_U0_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of U0 : label is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of U0 : label is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_a_type : integer;
  attribute c_a_type of U0 : label is 0;
  attribute c_a_width : integer;
  attribute c_a_width of U0 : label is 17;
  attribute c_add_mode : integer;
  attribute c_add_mode of U0 : label is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of U0 : label is 0;
  attribute c_b_type : integer;
  attribute c_b_type of U0 : label is 0;
  attribute c_b_value : string;
  attribute c_b_value of U0 : label is "00000000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of U0 : label is 17;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of U0 : label is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of U0 : label is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of U0 : label is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_out_width : integer;
  attribute c_out_width of U0 : label is 17;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of A : signal is "xilinx.com:signal:data:1.0 a_intf DATA";
  attribute X_INTERFACE_PARAMETER of A : signal is "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of B : signal is "xilinx.com:signal:data:1.0 b_intf DATA";
  attribute X_INTERFACE_PARAMETER of B : signal is "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of S : signal is "xilinx.com:signal:data:1.0 s_intf DATA";
  attribute X_INTERFACE_PARAMETER of S : signal is "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef";
begin
U0: entity work.\design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2\
     port map (
      A(16 downto 0) => A(16 downto 0),
      ADD => '1',
      B(16 downto 0) => B(16 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_U0_C_OUT_UNCONNECTED,
      S(16 downto 0) => S(16 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 9 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is "1";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is 10;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 : entity is "c_counter_binary_v12_0_13";
end design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_i_synth_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 1;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 10;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "1";
  attribute c_has_load of i_synth : label is 1;
  attribute c_has_thresh0 of i_synth : label is 0;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 0;
  attribute c_thresh0_value of i_synth : label is "1";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
  THRESH0 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_synth: entity work.design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13_viv
     port map (
      CE => CE,
      CLK => CLK,
      L(9 downto 0) => L(9 downto 0),
      LOAD => LOAD,
      Q(9 downto 0) => Q(9 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_i_synth_THRESH0_UNCONNECTED,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 9 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0 : entity is "ip_peakdetector_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0 : entity is "ip_peakdetector_c_counter_binary_v12_0_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0 : entity is "c_counter_binary_v12_0_13,Vivado 2019.1";
end design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0 is
  signal NLW_U0_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 10;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "1";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 1;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 0;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "1";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of LOAD : signal is "xilinx.com:signal:data:1.0 load_intf DATA";
  attribute X_INTERFACE_PARAMETER of LOAD : signal is "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of SINIT : signal is "xilinx.com:signal:data:1.0 sinit_intf DATA";
  attribute X_INTERFACE_PARAMETER of SINIT : signal is "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of L : signal is "xilinx.com:signal:data:1.0 l_intf DATA";
  attribute X_INTERFACE_PARAMETER of L : signal is "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute X_INTERFACE_PARAMETER of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13
     port map (
      CE => CE,
      CLK => CLK,
      L(9 downto 0) => L(9 downto 0),
      LOAD => LOAD,
      Q(9 downto 0) => Q(9 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_U0_THRESH0_UNCONNECTED,
      UP => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub is
  port (
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[6]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[6]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    douta : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub : entity is "ip_peakdetector_xladdsub";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub is
  signal core_s : STD_LOGIC_VECTOR ( 16 downto 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_addsub_v12_0_13,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0
     port map (
      A(16) => douta(15),
      A(15 downto 0) => douta(15 downto 0),
      B(16) => o(15),
      B(15 downto 0) => o(15 downto 0),
      CE => '1',
      CLK => signaldomain_clk,
      S(16 downto 0) => core_s(16 downto 0)
    );
\rel_47_16_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4000DDDC"
    )
        port map (
      I0 => x(14),
      I1 => x(15),
      I2 => core_s(14),
      I3 => core_s(15),
      I4 => core_s(16),
      O => \x[14]_0\(3)
    );
\rel_47_16_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(12),
      I1 => x(13),
      I2 => core_s(13),
      I3 => core_s(12),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[14]_0\(2)
    );
\rel_47_16_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(10),
      I1 => x(11),
      I2 => core_s(11),
      I3 => core_s(10),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[14]_0\(1)
    );
\rel_47_16_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(8),
      I1 => x(9),
      I2 => core_s(9),
      I3 => core_s(8),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[14]_0\(0)
    );
\rel_47_16_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82424241"
    )
        port map (
      I0 => x(14),
      I1 => x(15),
      I2 => core_s(16),
      I3 => core_s(15),
      I4 => core_s(14),
      O => \x[14]\(3)
    );
\rel_47_16_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(12),
      I1 => x(13),
      I2 => core_s(13),
      I3 => core_s(12),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[14]\(2)
    );
\rel_47_16_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(10),
      I1 => x(11),
      I2 => core_s(11),
      I3 => core_s(10),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[14]\(1)
    );
\rel_47_16_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(8),
      I1 => x(9),
      I2 => core_s(9),
      I3 => core_s(8),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[14]\(0)
    );
rel_47_16_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(6),
      I1 => x(7),
      I2 => core_s(7),
      I3 => core_s(6),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[6]_0\(3)
    );
rel_47_16_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(4),
      I1 => x(5),
      I2 => core_s(5),
      I3 => core_s(4),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[6]_0\(2)
    );
rel_47_16_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(2),
      I1 => x(3),
      I2 => core_s(3),
      I3 => core_s(2),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[6]_0\(1)
    );
rel_47_16_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => x(0),
      I1 => x(1),
      I2 => core_s(1),
      I3 => core_s(0),
      I4 => core_s(15),
      I5 => core_s(16),
      O => \x[6]_0\(0)
    );
rel_47_16_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(6),
      I1 => x(7),
      I2 => core_s(7),
      I3 => core_s(6),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[6]\(3)
    );
rel_47_16_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(4),
      I1 => x(5),
      I2 => core_s(5),
      I3 => core_s(4),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[6]\(2)
    );
rel_47_16_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(2),
      I1 => x(3),
      I2 => core_s(3),
      I3 => core_s(2),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[6]\(1)
    );
rel_47_16_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => x(0),
      I1 => x(1),
      I2 => core_s(1),
      I3 => core_s(0),
      I4 => core_s(16),
      I5 => core_s(15),
      O => \x[6]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub__xdcDup__1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    douta : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub__xdcDup__1\ : entity is "ip_peakdetector_xladdsub";
end \design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub__xdcDup__1\;

architecture STRUCTURE of \design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub__xdcDup__1\ is
  signal core_s : STD_LOGIC_VECTOR ( 16 downto 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_addsub_v12_0_13,Vivado 2019.1";
begin
\comp0.core_instance0\: entity work.\design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2\
     port map (
      A(16) => x(15),
      A(15 downto 0) => x(15 downto 0),
      B(16) => o(15),
      B(15 downto 0) => o(15 downto 0),
      CE => '1',
      CLK => signaldomain_clk,
      S(16 downto 0) => core_s(16 downto 0)
    );
\rel_63_21_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"73331110"
    )
        port map (
      I0 => douta(14),
      I1 => core_s(16),
      I2 => core_s(14),
      I3 => core_s(15),
      I4 => douta(15),
      O => DI(3)
    );
\rel_63_21_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => douta(12),
      I1 => douta(13),
      I2 => core_s(13),
      I3 => core_s(12),
      I4 => core_s(15),
      I5 => core_s(16),
      O => DI(2)
    );
\rel_63_21_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7130000077777130"
    )
        port map (
      I0 => douta(10),
      I1 => douta(11),
      I2 => core_s(11),
      I3 => core_s(10),
      I4 => core_s(15),
      I5 => core_s(16),
      O => DI(1)
    );
\rel_63_21_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(8),
      I1 => douta(9),
      I2 => core_s(9),
      I3 => core_s(16),
      I4 => core_s(8),
      I5 => core_s(15),
      O => DI(0)
    );
\rel_63_21_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"84442221"
    )
        port map (
      I0 => douta(14),
      I1 => core_s(16),
      I2 => core_s(15),
      I3 => core_s(14),
      I4 => douta(15),
      O => S(3)
    );
\rel_63_21_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => douta(12),
      I1 => douta(13),
      I2 => core_s(13),
      I3 => core_s(12),
      I4 => core_s(16),
      I5 => core_s(15),
      O => S(2)
    );
\rel_63_21_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8241888811118241"
    )
        port map (
      I0 => douta(10),
      I1 => douta(11),
      I2 => core_s(11),
      I3 => core_s(10),
      I4 => core_s(16),
      I5 => core_s(15),
      O => S(1)
    );
\rel_63_21_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(8),
      I1 => douta(9),
      I2 => core_s(9),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(8),
      O => S(0)
    );
rel_63_21_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(6),
      I1 => douta(7),
      I2 => core_s(7),
      I3 => core_s(16),
      I4 => core_s(6),
      I5 => core_s(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3)
    );
rel_63_21_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(4),
      I1 => douta(5),
      I2 => core_s(5),
      I3 => core_s(16),
      I4 => core_s(4),
      I5 => core_s(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(2)
    );
rel_63_21_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(2),
      I1 => douta(3),
      I2 => core_s(3),
      I3 => core_s(16),
      I4 => core_s(2),
      I5 => core_s(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(1)
    );
rel_63_21_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7177307700710030"
    )
        port map (
      I0 => douta(0),
      I1 => douta(1),
      I2 => core_s(1),
      I3 => core_s(16),
      I4 => core_s(0),
      I5 => core_s(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(0)
    );
rel_63_21_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(6),
      I1 => douta(7),
      I2 => core_s(7),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(6),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3)
    );
rel_63_21_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(4),
      I1 => douta(5),
      I2 => core_s(5),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(4),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(2)
    );
rel_63_21_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(2),
      I1 => douta(3),
      I2 => core_s(3),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(2),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(1)
    );
rel_63_21_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8288118241881141"
    )
        port map (
      I0 => douta(0),
      I1 => douta(1),
      I2 => core_s(1),
      I3 => core_s(16),
      I4 => core_s(15),
      I5 => core_s(0),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xlspram is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xlspram : entity is "ip_peakdetector_xlspram";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xlspram;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xlspram is
  signal \^douta\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_peakdetector_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_3,Vivado 2019.1";
begin
  douta(15 downto 0) <= \^douta\(15 downto 0);
\comp0.core_instance0\: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0
     port map (
      addra(9 downto 0) => Q(9 downto 0),
      clka => signaldomain_clk,
      dina(15 downto 0) => x(15 downto 0),
      douta(15 downto 0) => \^douta\(15 downto 0),
      ena => '1',
      wea(0) => '1'
    );
\rel_61_30_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(14),
      I1 => o(14),
      I2 => \^douta\(15),
      I3 => o(15),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3)
    );
\rel_61_30_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(12),
      I1 => o(12),
      I2 => o(13),
      I3 => \^douta\(13),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(2)
    );
\rel_61_30_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(10),
      I1 => o(10),
      I2 => o(11),
      I3 => \^douta\(11),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(1)
    );
\rel_61_30_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(8),
      I1 => o(8),
      I2 => o(9),
      I3 => \^douta\(9),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(0)
    );
\rel_61_30_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(14),
      I1 => \^douta\(15),
      I2 => o(15),
      I3 => o(14),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3)
    );
\rel_61_30_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(12),
      I1 => o(13),
      I2 => \^douta\(13),
      I3 => o(12),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(2)
    );
\rel_61_30_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(10),
      I1 => o(11),
      I2 => \^douta\(11),
      I3 => o(10),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(1)
    );
\rel_61_30_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(8),
      I1 => o(9),
      I2 => \^douta\(9),
      I3 => o(8),
      O => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(0)
    );
rel_61_30_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(6),
      I1 => o(6),
      I2 => o(7),
      I3 => \^douta\(7),
      O => DI(3)
    );
rel_61_30_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(4),
      I1 => o(4),
      I2 => o(5),
      I3 => \^douta\(5),
      O => DI(2)
    );
rel_61_30_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(2),
      I1 => o(2),
      I2 => o(3),
      I3 => \^douta\(3),
      O => DI(1)
    );
rel_61_30_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40F4"
    )
        port map (
      I0 => \^douta\(0),
      I1 => o(0),
      I2 => o(1),
      I3 => \^douta\(1),
      O => DI(0)
    );
rel_61_30_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(6),
      I1 => o(7),
      I2 => \^douta\(7),
      I3 => o(6),
      O => S(3)
    );
rel_61_30_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(4),
      I1 => o(5),
      I2 => \^douta\(5),
      I3 => o(4),
      O => S(2)
    );
rel_61_30_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(2),
      I1 => o(3),
      I2 => \^douta\(3),
      I3 => o(2),
      O => S(1)
    );
rel_61_30_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8241"
    )
        port map (
      I0 => \^douta\(0),
      I1 => o(1),
      I2 => \^douta\(1),
      I3 => o(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_math is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[6]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[14]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x[6]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    douta : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_math : entity is "ip_peakdetector_math";
end design_1_ip_peakdetector_0_0_ip_peakdetector_math;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_math is
begin
addsub: entity work.\design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub__xdcDup__1\
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3 downto 0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3 downto 0),
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3 downto 0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3 downto 0),
      DI(3 downto 0) => DI(3 downto 0),
      S(3 downto 0) => S(3 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
addsub1: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub
     port map (
      douta(15 downto 0) => douta(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3 downto 0) => \x[14]\(3 downto 0),
      \x[14]_0\(3 downto 0) => \x[14]_0\(3 downto 0),
      \x[6]\(3 downto 0) => \x[6]\(3 downto 0),
      \x[6]_0\(3 downto 0) => \x[6]_0\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_xlcounter_free is
  port (
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 );
    S : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    \op_mem_37_22_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_xlcounter_free : entity is "ip_peakdetector_xlcounter_free";
end design_1_ip_peakdetector_0_0_ip_peakdetector_xlcounter_free;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_xlcounter_free is
  signal \^q\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_peakdetector_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_counter_binary_v12_0_13,Vivado 2019.1";
begin
  Q(9 downto 0) <= \^q\(9 downto 0);
\comp0.core_instance0\: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0
     port map (
      CE => '1',
      CLK => signaldomain_clk,
      L(9 downto 0) => B"0000000000",
      LOAD => LOAD,
      Q(9 downto 0) => \^q\(9 downto 0),
      SINIT => '0'
    );
\op_mem_37_22[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(9),
      I1 => \op_mem_37_22_reg[0]\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_delayline is
  port (
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \op_mem_37_22_reg[0]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_delayline : entity is "ip_peakdetector_delayline";
end design_1_ip_peakdetector_0_0_ip_peakdetector_delayline;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_delayline is
  signal counter_n_10 : STD_LOGIC;
  signal counter_op_net : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal relational_op_net : STD_LOGIC;
begin
counter: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xlcounter_free
     port map (
      LOAD => relational_op_net,
      Q(9 downto 0) => counter_op_net(9 downto 0),
      S(0) => counter_n_10,
      \op_mem_37_22_reg[0]\(0) => \op_mem_37_22_reg[0]\(9),
      signaldomain_clk => signaldomain_clk
    );
relational: entity work.design_1_ip_peakdetector_0_0_sysgen_relational_3828661dd3
     port map (
      LOAD => relational_op_net,
      Q(8 downto 0) => counter_op_net(8 downto 0),
      S(0) => counter_n_10,
      \op_mem_37_22_reg[0]_0\(8 downto 0) => \op_mem_37_22_reg[0]\(8 downto 0),
      signaldomain_clk => signaldomain_clk
    );
single_port_ram: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xlspram
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3 downto 0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3 downto 0),
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3 downto 0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3 downto 0),
      DI(3 downto 0) => DI(3 downto 0),
      Q(9 downto 0) => counter_op_net(9 downto 0),
      S(3 downto 0) => S(3 downto 0),
      douta(15 downto 0) => douta(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_peakdetector is
  port (
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    d : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \xmax_i_12_24_reg[2]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \^o\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_35_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \op_mem_37_22_reg[0]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \^q\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_array[12].fde_used.u2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_array[7].fde_used.u2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_array[11].fde_used.u2\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_peakdetector : entity is "ip_peakdetector_peakdetector";
end design_1_ip_peakdetector_0_0_ip_peakdetector_peakdetector;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_peakdetector is
  signal delayline_n_16 : STD_LOGIC;
  signal delayline_n_17 : STD_LOGIC;
  signal delayline_n_18 : STD_LOGIC;
  signal delayline_n_19 : STD_LOGIC;
  signal delayline_n_20 : STD_LOGIC;
  signal delayline_n_21 : STD_LOGIC;
  signal delayline_n_22 : STD_LOGIC;
  signal delayline_n_23 : STD_LOGIC;
  signal delayline_n_24 : STD_LOGIC;
  signal delayline_n_25 : STD_LOGIC;
  signal delayline_n_26 : STD_LOGIC;
  signal delayline_n_27 : STD_LOGIC;
  signal delayline_n_28 : STD_LOGIC;
  signal delayline_n_29 : STD_LOGIC;
  signal delayline_n_30 : STD_LOGIC;
  signal delayline_n_31 : STD_LOGIC;
  signal math_n_0 : STD_LOGIC;
  signal math_n_1 : STD_LOGIC;
  signal math_n_10 : STD_LOGIC;
  signal math_n_11 : STD_LOGIC;
  signal math_n_12 : STD_LOGIC;
  signal math_n_13 : STD_LOGIC;
  signal math_n_14 : STD_LOGIC;
  signal math_n_15 : STD_LOGIC;
  signal math_n_16 : STD_LOGIC;
  signal math_n_17 : STD_LOGIC;
  signal math_n_18 : STD_LOGIC;
  signal math_n_19 : STD_LOGIC;
  signal math_n_2 : STD_LOGIC;
  signal math_n_20 : STD_LOGIC;
  signal math_n_21 : STD_LOGIC;
  signal math_n_22 : STD_LOGIC;
  signal math_n_23 : STD_LOGIC;
  signal math_n_24 : STD_LOGIC;
  signal math_n_25 : STD_LOGIC;
  signal math_n_26 : STD_LOGIC;
  signal math_n_27 : STD_LOGIC;
  signal math_n_28 : STD_LOGIC;
  signal math_n_29 : STD_LOGIC;
  signal math_n_3 : STD_LOGIC;
  signal math_n_30 : STD_LOGIC;
  signal math_n_31 : STD_LOGIC;
  signal math_n_4 : STD_LOGIC;
  signal math_n_5 : STD_LOGIC;
  signal math_n_6 : STD_LOGIC;
  signal math_n_7 : STD_LOGIC;
  signal math_n_8 : STD_LOGIC;
  signal math_n_9 : STD_LOGIC;
  signal mcode_cmp_en_net : STD_LOGIC;
  signal mcode_cmp_rst_net : STD_LOGIC;
  signal mcode_pkd_net : STD_LOGIC;
  signal risingedgetrigger_n_0 : STD_LOGIC;
  signal single_port_ram_data_out_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
delayline: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_delayline
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3) => delayline_n_24,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(2) => delayline_n_25,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(1) => delayline_n_26,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(0) => delayline_n_27,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3) => delayline_n_28,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(2) => delayline_n_29,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(1) => delayline_n_30,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(0) => delayline_n_31,
      DI(3) => delayline_n_16,
      DI(2) => delayline_n_17,
      DI(1) => delayline_n_18,
      DI(0) => delayline_n_19,
      S(3) => delayline_n_20,
      S(2) => delayline_n_21,
      S(1) => delayline_n_22,
      S(0) => delayline_n_23,
      douta(15 downto 0) => single_port_ram_data_out_net(15 downto 0),
      o(15 downto 0) => \^o\(15 downto 0),
      \op_mem_37_22_reg[0]\(9 downto 0) => \op_mem_37_22_reg[0]\(9 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
math: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_math
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(3) => math_n_4,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(2) => math_n_5,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(1) => math_n_6,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram\(0) => math_n_7,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(3) => math_n_12,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(2) => math_n_13,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(1) => math_n_14,
      \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0\(0) => math_n_15,
      DI(3) => math_n_8,
      DI(2) => math_n_9,
      DI(1) => math_n_10,
      DI(0) => math_n_11,
      S(3) => math_n_0,
      S(2) => math_n_1,
      S(1) => math_n_2,
      S(0) => math_n_3,
      douta(15 downto 0) => single_port_ram_data_out_net(15 downto 0),
      o(15 downto 0) => \^o\(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3) => math_n_16,
      \x[14]\(2) => math_n_17,
      \x[14]\(1) => math_n_18,
      \x[14]\(0) => math_n_19,
      \x[14]_0\(3) => math_n_24,
      \x[14]_0\(2) => math_n_25,
      \x[14]_0\(1) => math_n_26,
      \x[14]_0\(0) => math_n_27,
      \x[6]\(3) => math_n_20,
      \x[6]\(2) => math_n_21,
      \x[6]\(1) => math_n_22,
      \x[6]\(0) => math_n_23,
      \x[6]_0\(3) => math_n_28,
      \x[6]_0\(2) => math_n_29,
      \x[6]_0\(1) => math_n_30,
      \x[6]_0\(0) => math_n_31
    );
mcode: entity work.design_1_ip_peakdetector_0_0_sysgen_mcode_block_a80b0cfdd5
     port map (
      DI(3) => math_n_8,
      DI(2) => math_n_9,
      DI(1) => math_n_10,
      DI(0) => math_n_11,
      S(3) => math_n_0,
      S(2) => math_n_1,
      S(1) => math_n_2,
      S(0) => math_n_3,
      SR(0) => mcode_cmp_rst_net,
      mcode_cmp_en_net => mcode_cmp_en_net,
      mcode_pkd_net => mcode_pkd_net,
      peak_amp_rdy(0) => peak_amp_rdy(0),
      peak_amp_rdy_0_sp_1 => risingedgetrigger_n_0,
      pkd_i_16_23_reg_0(3 downto 0) => pkd_i_16_23_reg(3 downto 0),
      pkd_i_16_23_reg_1(3 downto 0) => pkd_i_16_23_reg_0(3 downto 0),
      pkd_i_16_23_reg_2(3 downto 0) => pkd_i_16_23_reg_1(3 downto 0),
      pkd_i_16_23_reg_3(3 downto 0) => pkd_i_16_23_reg_2(3 downto 0),
      q(0) => \^q\(0),
      \rel_35_16_carry__0_0\(3 downto 0) => \rel_35_16_carry__0\(3 downto 0),
      \rel_35_16_carry__0_1\(3 downto 0) => S(3 downto 0),
      \rel_47_16_carry__0_0\(3) => math_n_28,
      \rel_47_16_carry__0_0\(2) => math_n_29,
      \rel_47_16_carry__0_0\(1) => math_n_30,
      \rel_47_16_carry__0_0\(0) => math_n_31,
      \rel_47_16_carry__0_1\(3) => math_n_20,
      \rel_47_16_carry__0_1\(2) => math_n_21,
      \rel_47_16_carry__0_1\(1) => math_n_22,
      \rel_47_16_carry__0_1\(0) => math_n_23,
      \rel_55_16_carry__0_0\(3 downto 0) => \rel_55_16_carry__0\(3 downto 0),
      \rel_55_16_carry__0_1\(3 downto 0) => \rel_55_16_carry__0_0\(3 downto 0),
      \rel_55_30_carry__0_0\(3 downto 0) => \rel_55_30_carry__0\(3 downto 0),
      \rel_55_30_carry__0_1\(3 downto 0) => \rel_55_30_carry__0_0\(3 downto 0),
      \rel_61_16_carry__0_0\(3 downto 0) => DI(3 downto 0),
      \rel_61_16_carry__0_1\(3 downto 0) => \rel_61_16_carry__0\(3 downto 0),
      \rel_61_30_carry__0_0\(3) => delayline_n_16,
      \rel_61_30_carry__0_0\(2) => delayline_n_17,
      \rel_61_30_carry__0_0\(1) => delayline_n_18,
      \rel_61_30_carry__0_0\(0) => delayline_n_19,
      \rel_61_30_carry__0_1\(3) => delayline_n_20,
      \rel_61_30_carry__0_1\(2) => delayline_n_21,
      \rel_61_30_carry__0_1\(1) => delayline_n_22,
      \rel_61_30_carry__0_1\(0) => delayline_n_23,
      \rel_63_21_carry__0_0\(3) => math_n_12,
      \rel_63_21_carry__0_0\(2) => math_n_13,
      \rel_63_21_carry__0_0\(1) => math_n_14,
      \rel_63_21_carry__0_0\(0) => math_n_15,
      \rel_63_21_carry__0_1\(3) => math_n_4,
      \rel_63_21_carry__0_1\(2) => math_n_5,
      \rel_63_21_carry__0_1\(1) => math_n_6,
      \rel_63_21_carry__0_1\(0) => math_n_7,
      signaldomain_clk => signaldomain_clk,
      \state_15_23[0]_i_3_0\(3) => delayline_n_24,
      \state_15_23[0]_i_3_0\(2) => delayline_n_25,
      \state_15_23[0]_i_3_0\(1) => delayline_n_26,
      \state_15_23[0]_i_3_0\(0) => delayline_n_27,
      \state_15_23[0]_i_3_1\(3) => delayline_n_28,
      \state_15_23[0]_i_3_1\(2) => delayline_n_29,
      \state_15_23[0]_i_3_1\(1) => delayline_n_30,
      \state_15_23[0]_i_3_1\(0) => delayline_n_31,
      \state_15_23[0]_i_3_2\(3 downto 0) => \state_15_23[0]_i_3\(3 downto 0),
      \state_15_23[0]_i_3_3\(3 downto 0) => \state_15_23[0]_i_3_0\(3 downto 0),
      \state_15_23[1]_i_2_0\(3 downto 0) => \state_15_23[1]_i_2\(3 downto 0),
      \state_15_23[1]_i_2_1\(3 downto 0) => \state_15_23[1]_i_2_0\(3 downto 0),
      \state_15_23[2]_i_2_0\(3) => math_n_24,
      \state_15_23[2]_i_2_0\(2) => math_n_25,
      \state_15_23[2]_i_2_0\(1) => math_n_26,
      \state_15_23[2]_i_2_0\(0) => math_n_27,
      \state_15_23[2]_i_2_1\(3) => math_n_16,
      \state_15_23[2]_i_2_1\(2) => math_n_17,
      \state_15_23[2]_i_2_1\(1) => math_n_18,
      \state_15_23[2]_i_2_1\(0) => math_n_19
    );
mcode1: entity work.design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c3d4c1851
     port map (
      CO(0) => CO(0),
      O(3 downto 0) => O(3 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      SR(0) => mcode_cmp_rst_net,
      d(13 downto 0) => d(13 downto 0),
      mcode_cmp_en_net => mcode_cmp_en_net,
      \reg_array[11].fde_used.u2\(3 downto 0) => \reg_array[11].fde_used.u2\(3 downto 0),
      \reg_array[12].fde_used.u2\(1 downto 0) => \reg_array[12].fde_used.u2\(1 downto 0),
      \reg_array[7].fde_used.u2\(3 downto 0) => \reg_array[7].fde_used.u2\(3 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \xmax_i_12_24_reg[2]_0\(0) => \xmax_i_12_24_reg[2]\(0)
    );
risingedgetrigger: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_risingedgetrigger
     port map (
      \ff_2_17_reg[0]\ => risingedgetrigger_n_0,
      mcode_pkd_net => mcode_pkd_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_signaldomain is
  port (
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_61_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[0]_i_3_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_35_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_15_23[1]_i_2_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_30_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \rel_55_16_carry__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pkd_i_16_23_reg_2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \op_mem_37_22_reg[0]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \^q\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_signaldomain : entity is "ip_peakdetector_signaldomain";
end design_1_ip_peakdetector_0_0_ip_peakdetector_signaldomain;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_signaldomain is
  signal \^q_1\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \convert4/result\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal format_dac_n_0 : STD_LOGIC;
  signal format_dac_n_1 : STD_LOGIC;
  signal format_dac_n_10 : STD_LOGIC;
  signal format_dac_n_11 : STD_LOGIC;
  signal format_dac_n_12 : STD_LOGIC;
  signal format_dac_n_13 : STD_LOGIC;
  signal format_dac_n_14 : STD_LOGIC;
  signal format_dac_n_2 : STD_LOGIC;
  signal format_dac_n_3 : STD_LOGIC;
  signal format_dac_n_4 : STD_LOGIC;
  signal format_dac_n_5 : STD_LOGIC;
  signal format_dac_n_6 : STD_LOGIC;
  signal format_dac_n_7 : STD_LOGIC;
  signal format_dac_n_8 : STD_LOGIC;
  signal format_dac_n_9 : STD_LOGIC;
  signal peakdetector_n_30 : STD_LOGIC;
begin
  Q(15 downto 0) <= \^q_1\(15 downto 0);
format_dac: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_format_dac
     port map (
      CO(0) => format_dac_n_12,
      O(3) => format_dac_n_0,
      O(2) => format_dac_n_1,
      O(1) => format_dac_n_2,
      O(0) => format_dac_n_3,
      Q(13 downto 0) => \^q_1\(15 downto 2),
      S(0) => peakdetector_n_30,
      d(13 downto 0) => \convert4/result\(13 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      signaldomain_clk => signaldomain_clk,
      \xmax_i_12_24_reg[13]\(3) => format_dac_n_8,
      \xmax_i_12_24_reg[13]\(2) => format_dac_n_9,
      \xmax_i_12_24_reg[13]\(1) => format_dac_n_10,
      \xmax_i_12_24_reg[13]\(0) => format_dac_n_11,
      \xmax_i_12_24_reg[15]\(1) => format_dac_n_13,
      \xmax_i_12_24_reg[15]\(0) => format_dac_n_14,
      \xmax_i_12_24_reg[9]\(3) => format_dac_n_4,
      \xmax_i_12_24_reg[9]\(2) => format_dac_n_5,
      \xmax_i_12_24_reg[9]\(1) => format_dac_n_6,
      \xmax_i_12_24_reg[9]\(0) => format_dac_n_7
    );
peakdetector: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_peakdetector
     port map (
      CO(0) => format_dac_n_12,
      DI(3 downto 0) => DI(3 downto 0),
      O(3) => format_dac_n_0,
      O(2) => format_dac_n_1,
      O(1) => format_dac_n_2,
      O(0) => format_dac_n_3,
      Q(15 downto 0) => \^q_1\(15 downto 0),
      S(3 downto 0) => S(3 downto 0),
      d(13 downto 0) => \convert4/result\(13 downto 0),
      \^o\(15 downto 0) => o(15 downto 0),
      \op_mem_37_22_reg[0]\(9 downto 0) => \op_mem_37_22_reg[0]\(9 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      pkd_i_16_23_reg(3 downto 0) => pkd_i_16_23_reg(3 downto 0),
      pkd_i_16_23_reg_0(3 downto 0) => pkd_i_16_23_reg_0(3 downto 0),
      pkd_i_16_23_reg_1(3 downto 0) => pkd_i_16_23_reg_1(3 downto 0),
      pkd_i_16_23_reg_2(3 downto 0) => pkd_i_16_23_reg_2(3 downto 0),
      \^q\(0) => \^q\(0),
      \reg_array[11].fde_used.u2\(3) => format_dac_n_8,
      \reg_array[11].fde_used.u2\(2) => format_dac_n_9,
      \reg_array[11].fde_used.u2\(1) => format_dac_n_10,
      \reg_array[11].fde_used.u2\(0) => format_dac_n_11,
      \reg_array[12].fde_used.u2\(1) => format_dac_n_13,
      \reg_array[12].fde_used.u2\(0) => format_dac_n_14,
      \reg_array[7].fde_used.u2\(3) => format_dac_n_4,
      \reg_array[7].fde_used.u2\(2) => format_dac_n_5,
      \reg_array[7].fde_used.u2\(1) => format_dac_n_6,
      \reg_array[7].fde_used.u2\(0) => format_dac_n_7,
      \rel_35_16_carry__0\(3 downto 0) => \rel_35_16_carry__0\(3 downto 0),
      \rel_55_16_carry__0\(3 downto 0) => \rel_55_16_carry__0\(3 downto 0),
      \rel_55_16_carry__0_0\(3 downto 0) => \rel_55_16_carry__0_0\(3 downto 0),
      \rel_55_30_carry__0\(3 downto 0) => \rel_55_30_carry__0\(3 downto 0),
      \rel_55_30_carry__0_0\(3 downto 0) => \rel_55_30_carry__0_0\(3 downto 0),
      \rel_61_16_carry__0\(3 downto 0) => \rel_61_16_carry__0\(3 downto 0),
      signaldomain_clk => signaldomain_clk,
      \state_15_23[0]_i_3\(3 downto 0) => \state_15_23[0]_i_3\(3 downto 0),
      \state_15_23[0]_i_3_0\(3 downto 0) => \state_15_23[0]_i_3_0\(3 downto 0),
      \state_15_23[1]_i_2\(3 downto 0) => \state_15_23[1]_i_2\(3 downto 0),
      \state_15_23[1]_i_2_0\(3 downto 0) => \state_15_23[1]_i_2_0\(3 downto 0),
      x(15 downto 0) => x(15 downto 0),
      \xmax_i_12_24_reg[2]\(0) => peakdetector_n_30
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector_struct is
  port (
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_x_delay : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r5_x_noise : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r6_x_min : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r7_x_max : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r8_en_pkd : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector_struct : entity is "ip_peakdetector_struct";
end design_1_ip_peakdetector_0_0_ip_peakdetector_struct;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector_struct is
  signal register1_n_0 : STD_LOGIC;
  signal register1_n_1 : STD_LOGIC;
  signal register1_n_2 : STD_LOGIC;
  signal register1_n_20 : STD_LOGIC;
  signal register1_n_21 : STD_LOGIC;
  signal register1_n_22 : STD_LOGIC;
  signal register1_n_23 : STD_LOGIC;
  signal register1_n_24 : STD_LOGIC;
  signal register1_n_25 : STD_LOGIC;
  signal register1_n_26 : STD_LOGIC;
  signal register1_n_27 : STD_LOGIC;
  signal register1_n_28 : STD_LOGIC;
  signal register1_n_29 : STD_LOGIC;
  signal register1_n_3 : STD_LOGIC;
  signal register1_n_30 : STD_LOGIC;
  signal register1_n_31 : STD_LOGIC;
  signal register1_n_32 : STD_LOGIC;
  signal register1_n_33 : STD_LOGIC;
  signal register1_n_34 : STD_LOGIC;
  signal register1_n_35 : STD_LOGIC;
  signal register1_n_36 : STD_LOGIC;
  signal register1_n_37 : STD_LOGIC;
  signal register1_n_38 : STD_LOGIC;
  signal register1_n_39 : STD_LOGIC;
  signal register1_n_40 : STD_LOGIC;
  signal register1_n_41 : STD_LOGIC;
  signal register1_n_42 : STD_LOGIC;
  signal register1_n_43 : STD_LOGIC;
  signal register1_n_44 : STD_LOGIC;
  signal register1_n_45 : STD_LOGIC;
  signal register1_n_46 : STD_LOGIC;
  signal register1_n_47 : STD_LOGIC;
  signal register1_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register2_n_0 : STD_LOGIC;
  signal register2_n_1 : STD_LOGIC;
  signal register2_n_10 : STD_LOGIC;
  signal register2_n_11 : STD_LOGIC;
  signal register2_n_12 : STD_LOGIC;
  signal register2_n_13 : STD_LOGIC;
  signal register2_n_14 : STD_LOGIC;
  signal register2_n_15 : STD_LOGIC;
  signal register2_n_2 : STD_LOGIC;
  signal register2_n_3 : STD_LOGIC;
  signal register2_n_4 : STD_LOGIC;
  signal register2_n_5 : STD_LOGIC;
  signal register2_n_6 : STD_LOGIC;
  signal register2_n_7 : STD_LOGIC;
  signal register2_n_8 : STD_LOGIC;
  signal register2_n_9 : STD_LOGIC;
  signal register3_n_0 : STD_LOGIC;
  signal register3_n_1 : STD_LOGIC;
  signal register3_n_10 : STD_LOGIC;
  signal register3_n_11 : STD_LOGIC;
  signal register3_n_12 : STD_LOGIC;
  signal register3_n_13 : STD_LOGIC;
  signal register3_n_14 : STD_LOGIC;
  signal register3_n_15 : STD_LOGIC;
  signal register3_n_2 : STD_LOGIC;
  signal register3_n_3 : STD_LOGIC;
  signal register3_n_4 : STD_LOGIC;
  signal register3_n_5 : STD_LOGIC;
  signal register3_n_6 : STD_LOGIC;
  signal register3_n_7 : STD_LOGIC;
  signal register3_n_8 : STD_LOGIC;
  signal register3_n_9 : STD_LOGIC;
  signal register4_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register_q_net : STD_LOGIC_VECTOR ( 9 downto 0 );
begin
register1: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister
     port map (
      DI(3) => register1_n_20,
      DI(2) => register1_n_21,
      DI(1) => register1_n_22,
      DI(0) => register1_n_23,
      S(3) => register1_n_0,
      S(2) => register1_n_1,
      S(1) => register1_n_2,
      S(0) => register1_n_3,
      axibusdomain_clk => axibusdomain_clk,
      \fd_prim_array[14].bit_is_0.fdre_comp\(3) => register1_n_24,
      \fd_prim_array[14].bit_is_0.fdre_comp\(2) => register1_n_25,
      \fd_prim_array[14].bit_is_0.fdre_comp\(1) => register1_n_26,
      \fd_prim_array[14].bit_is_0.fdre_comp\(0) => register1_n_27,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3) => register1_n_32,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(2) => register1_n_33,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(1) => register1_n_34,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(0) => register1_n_35,
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(3) => register1_n_44,
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(2) => register1_n_45,
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(1) => register1_n_46,
      \fd_prim_array[14].bit_is_0.fdre_comp_1\(0) => register1_n_47,
      \fd_prim_array[6].bit_is_0.fdre_comp\(3) => register1_n_36,
      \fd_prim_array[6].bit_is_0.fdre_comp\(2) => register1_n_37,
      \fd_prim_array[6].bit_is_0.fdre_comp\(1) => register1_n_38,
      \fd_prim_array[6].bit_is_0.fdre_comp\(0) => register1_n_39,
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(3) => register1_n_40,
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(2) => register1_n_41,
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(1) => register1_n_42,
      \fd_prim_array[6].bit_is_0.fdre_comp_0\(0) => register1_n_43,
      o(15 downto 0) => register1_q_net(15 downto 0),
      r5_x_noise(15 downto 0) => r5_x_noise(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3) => register1_n_28,
      \x[14]\(2) => register1_n_29,
      \x[14]\(1) => register1_n_30,
      \x[14]\(0) => register1_n_31
    );
register2: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_0
     port map (
      DI(3) => register2_n_8,
      DI(2) => register2_n_9,
      DI(1) => register2_n_10,
      DI(0) => register2_n_11,
      S(3) => register2_n_0,
      S(2) => register2_n_1,
      S(1) => register2_n_2,
      S(0) => register2_n_3,
      axibusdomain_clk => axibusdomain_clk,
      \fd_prim_array[14].bit_is_0.fdre_comp\(3) => register2_n_4,
      \fd_prim_array[14].bit_is_0.fdre_comp\(2) => register2_n_5,
      \fd_prim_array[14].bit_is_0.fdre_comp\(1) => register2_n_6,
      \fd_prim_array[14].bit_is_0.fdre_comp\(0) => register2_n_7,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(3) => register2_n_12,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(2) => register2_n_13,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(1) => register2_n_14,
      \fd_prim_array[14].bit_is_0.fdre_comp_0\(0) => register2_n_15,
      r6_x_min(15 downto 0) => r6_x_min(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
register3: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_1
     port map (
      DI(3) => register3_n_8,
      DI(2) => register3_n_9,
      DI(1) => register3_n_10,
      DI(0) => register3_n_11,
      S(3) => register3_n_0,
      S(2) => register3_n_1,
      S(1) => register3_n_2,
      S(0) => register3_n_3,
      axibusdomain_clk => axibusdomain_clk,
      \fd_prim_array[14].bit_is_0.fdre_comp\(3) => register3_n_4,
      \fd_prim_array[14].bit_is_0.fdre_comp\(2) => register3_n_5,
      \fd_prim_array[14].bit_is_0.fdre_comp\(1) => register3_n_6,
      \fd_prim_array[14].bit_is_0.fdre_comp\(0) => register3_n_7,
      r7_x_max(15 downto 0) => r7_x_max(15 downto 0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0),
      \x[14]\(3) => register3_n_12,
      \x[14]\(2) => register3_n_13,
      \x[14]\(1) => register3_n_14,
      \x[14]\(0) => register3_n_15
    );
register4: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_2
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register4_q_net(0),
      r8_en_pkd(0) => r8_en_pkd(0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_3
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(9 downto 0) => register_q_net(9 downto 0),
      r4_x_delay(9 downto 0) => r4_x_delay(9 downto 0),
      signaldomain_clk => signaldomain_clk
    );
signaldomain: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_signaldomain
     port map (
      DI(3) => register1_n_20,
      DI(2) => register1_n_21,
      DI(1) => register1_n_22,
      DI(0) => register1_n_23,
      Q(15 downto 0) => Q(15 downto 0),
      S(3) => register1_n_0,
      S(2) => register1_n_1,
      S(1) => register1_n_2,
      S(0) => register1_n_3,
      o(15 downto 0) => register1_q_net(15 downto 0),
      \op_mem_37_22_reg[0]\(9 downto 0) => register_q_net(9 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      pkd_i_16_23_reg(3) => register3_n_12,
      pkd_i_16_23_reg(2) => register3_n_13,
      pkd_i_16_23_reg(1) => register3_n_14,
      pkd_i_16_23_reg(0) => register3_n_15,
      pkd_i_16_23_reg_0(3) => register3_n_4,
      pkd_i_16_23_reg_0(2) => register3_n_5,
      pkd_i_16_23_reg_0(1) => register3_n_6,
      pkd_i_16_23_reg_0(0) => register3_n_7,
      pkd_i_16_23_reg_1(3) => register2_n_12,
      pkd_i_16_23_reg_1(2) => register2_n_13,
      pkd_i_16_23_reg_1(1) => register2_n_14,
      pkd_i_16_23_reg_1(0) => register2_n_15,
      pkd_i_16_23_reg_2(3) => register2_n_4,
      pkd_i_16_23_reg_2(2) => register2_n_5,
      pkd_i_16_23_reg_2(1) => register2_n_6,
      pkd_i_16_23_reg_2(0) => register2_n_7,
      \^q\(0) => register4_q_net(0),
      \rel_35_16_carry__0\(3) => register1_n_36,
      \rel_35_16_carry__0\(2) => register1_n_37,
      \rel_35_16_carry__0\(1) => register1_n_38,
      \rel_35_16_carry__0\(0) => register1_n_39,
      \rel_55_16_carry__0\(3) => register2_n_8,
      \rel_55_16_carry__0\(2) => register2_n_9,
      \rel_55_16_carry__0\(1) => register2_n_10,
      \rel_55_16_carry__0\(0) => register2_n_11,
      \rel_55_16_carry__0_0\(3) => register2_n_0,
      \rel_55_16_carry__0_0\(2) => register2_n_1,
      \rel_55_16_carry__0_0\(1) => register2_n_2,
      \rel_55_16_carry__0_0\(0) => register2_n_3,
      \rel_55_30_carry__0\(3) => register3_n_8,
      \rel_55_30_carry__0\(2) => register3_n_9,
      \rel_55_30_carry__0\(1) => register3_n_10,
      \rel_55_30_carry__0\(0) => register3_n_11,
      \rel_55_30_carry__0_0\(3) => register3_n_0,
      \rel_55_30_carry__0_0\(2) => register3_n_1,
      \rel_55_30_carry__0_0\(1) => register3_n_2,
      \rel_55_30_carry__0_0\(0) => register3_n_3,
      \rel_61_16_carry__0\(3) => register1_n_40,
      \rel_61_16_carry__0\(2) => register1_n_41,
      \rel_61_16_carry__0\(1) => register1_n_42,
      \rel_61_16_carry__0\(0) => register1_n_43,
      signaldomain_clk => signaldomain_clk,
      \state_15_23[0]_i_3\(3) => register1_n_28,
      \state_15_23[0]_i_3\(2) => register1_n_29,
      \state_15_23[0]_i_3\(1) => register1_n_30,
      \state_15_23[0]_i_3\(0) => register1_n_31,
      \state_15_23[0]_i_3_0\(3) => register1_n_44,
      \state_15_23[0]_i_3_0\(2) => register1_n_45,
      \state_15_23[0]_i_3_0\(1) => register1_n_46,
      \state_15_23[0]_i_3_0\(0) => register1_n_47,
      \state_15_23[1]_i_2\(3) => register1_n_32,
      \state_15_23[1]_i_2\(2) => register1_n_33,
      \state_15_23[1]_i_2\(1) => register1_n_34,
      \state_15_23[1]_i_2\(0) => register1_n_35,
      \state_15_23[1]_i_2_0\(3) => register1_n_24,
      \state_15_23[1]_i_2_0\(2) => register1_n_25,
      \state_15_23[1]_i_2_0\(1) => register1_n_26,
      \state_15_23[1]_i_2_0\(0) => register1_n_27,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0_ip_peakdetector is
  port (
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    peak_amp : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_peakdetector_0_0_ip_peakdetector : entity is "ip_peakdetector";
end design_1_ip_peakdetector_0_0_ip_peakdetector;

architecture STRUCTURE of design_1_ip_peakdetector_0_0_ip_peakdetector is
  signal \<const0>\ : STD_LOGIC;
  signal r4_x_delay : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal r5_x_noise : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r6_x_min : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r7_x_max : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r8_en_pkd : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  axibusdomain_s_axi_bresp(1) <= \<const0>\;
  axibusdomain_s_axi_bresp(0) <= \<const0>\;
  axibusdomain_s_axi_rresp(1) <= \<const0>\;
  axibusdomain_s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
axibusdomain_axi_lite_interface: entity work.design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(4 downto 0) => axibusdomain_s_axi_araddr(4 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(4 downto 0) => axibusdomain_s_axi_awaddr(4 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => r8_en_pkd(0),
      i(15 downto 0) => r7_x_max(15 downto 0),
      \slv_reg_array_reg[2][15]\(15 downto 0) => r6_x_min(15 downto 0),
      \slv_reg_array_reg[3][15]\(15 downto 0) => r5_x_noise(15 downto 0),
      \slv_reg_array_reg[4][9]\(9 downto 0) => r4_x_delay(9 downto 0)
    );
ip_peakdetector_struct: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector_struct
     port map (
      Q(15 downto 0) => peak_amp(15 downto 0),
      axibusdomain_clk => axibusdomain_clk,
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      r4_x_delay(9 downto 0) => r4_x_delay(9 downto 0),
      r5_x_noise(15 downto 0) => r5_x_noise(15 downto 0),
      r6_x_min(15 downto 0) => r6_x_min(15 downto 0),
      r7_x_max(15 downto 0) => r7_x_max(15 downto 0),
      r8_en_pkd(0) => r8_en_pkd(0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_peakdetector_0_0 is
  port (
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    peak_amp : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_peakdetector_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_peakdetector_0_0 : entity is "design_1_ip_peakdetector_0_0,ip_peakdetector,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_peakdetector_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_1_ip_peakdetector_0_0 : entity is "sysgen";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_peakdetector_0_0 : entity is "ip_peakdetector,Vivado 2019.1";
end design_1_ip_peakdetector_0_0;

architecture STRUCTURE of design_1_ip_peakdetector_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of axibusdomain_aresetn : signal is "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of axibusdomain_aresetn : signal is "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_clk : signal is "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of axibusdomain_clk : signal is "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID";
  attribute X_INTERFACE_PARAMETER of axibusdomain_s_axi_rvalid : signal is "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID";
  attribute X_INTERFACE_INFO of signaldomain_clk : signal is "xilinx.com:signal:clock:1.0 signaldomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of signaldomain_clk : signal is "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB";
  attribute X_INTERFACE_INFO of peak_amp : signal is "xilinx.com:signal:data:1.0 peak_amp DATA";
  attribute X_INTERFACE_PARAMETER of peak_amp : signal is "XIL_INTERFACENAME peak_amp, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}";
  attribute X_INTERFACE_INFO of peak_amp_dac : signal is "xilinx.com:signal:data:1.0 peak_amp_dac DATA";
  attribute X_INTERFACE_PARAMETER of peak_amp_dac : signal is "XIL_INTERFACENAME peak_amp_dac, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}";
  attribute X_INTERFACE_INFO of peak_amp_rdy : signal is "xilinx.com:signal:data:1.0 peak_amp_rdy DATA";
  attribute X_INTERFACE_PARAMETER of peak_amp_rdy : signal is "XIL_INTERFACENAME peak_amp_rdy, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}, PortType data, PortType.PROP_SRC false";
  attribute X_INTERFACE_INFO of x : signal is "xilinx.com:signal:data:1.0 x DATA";
  attribute X_INTERFACE_PARAMETER of x : signal is "XIL_INTERFACENAME x, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
begin
inst: entity work.design_1_ip_peakdetector_0_0_ip_peakdetector
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(4 downto 0) => axibusdomain_s_axi_araddr(4 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(4 downto 0) => axibusdomain_s_axi_awaddr(4 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bresp(1 downto 0) => axibusdomain_s_axi_bresp(1 downto 0),
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rresp(1 downto 0) => axibusdomain_s_axi_rresp(1 downto 0),
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      peak_amp(15 downto 0) => peak_amp(15 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
