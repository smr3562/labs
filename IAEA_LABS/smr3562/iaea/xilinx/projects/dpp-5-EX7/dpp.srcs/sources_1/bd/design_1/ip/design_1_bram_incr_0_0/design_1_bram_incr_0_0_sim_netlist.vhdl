-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 17:57:54 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_bram_incr_0_0/design_1_bram_incr_0_0_sim_netlist.vhdl
-- Design      : design_1_bram_incr_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_bram_incr_0_0_bram_incr is
  port (
    bram_we : out STD_LOGIC;
    bram_rst : out STD_LOGIC;
    bram_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk : in STD_LOGIC;
    peak_amp_rdy : in STD_LOGIC;
    bram_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rstn : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_bram_incr_0_0_bram_incr : entity is "bram_incr";
end design_1_bram_incr_0_0_bram_incr;

architecture STRUCTURE of design_1_bram_incr_0_0_bram_incr is
  signal \FSM_onehot_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \^bram_rst\ : STD_LOGIC;
  signal \^bram_we\ : STD_LOGIC;
  signal inc : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \uSum_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \uSum_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \uSum_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \uSum_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \uSum_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \uSum_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \uSum_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \uSum_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \uSum_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \uSum_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \uSum_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \uSum_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \uSum_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \uSum_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \uSum_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \uSum_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \uSum_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \uSum_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \uSum_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \uSum_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \uSum_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \uSum_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \uSum_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \uSum_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \uSum_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \uSum_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \uSum_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \uSum_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \uSum_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \uSum_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \NLW_uSum_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_uSum_reg[31]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "st3_increment:0100,st4_store:1000,st1_idle:0001,st2_read:0010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "st3_increment:0100,st4_store:1000,st1_idle:0001,st2_read:0010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "st3_increment:0100,st4_store:1000,st1_idle:0001,st2_read:0010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[3]\ : label is "st3_increment:0100,st4_store:1000,st1_idle:0001,st2_read:0010";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \uSum_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[10]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[11]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[12]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[13]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[14]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[15]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[16]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[17]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[18]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[19]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[20]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[21]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[22]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[23]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[24]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[25]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[26]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[27]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[28]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[29]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[2]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[30]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[31]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[3]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[5]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[6]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[7]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[8]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \uSum_reg[9]\ : label is "LD";
begin
  bram_rst <= \^bram_rst\;
  bram_we <= \^bram_we\;
\FSM_onehot_state[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^bram_we\,
      I1 => peak_amp_rdy,
      I2 => \FSM_onehot_state_reg_n_0_[0]\,
      O => \FSM_onehot_state[0]_i_1_n_0\
    );
\FSM_onehot_state[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[0]\,
      I1 => peak_amp_rdy,
      O => \FSM_onehot_state[1]_i_1_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[0]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[0]\,
      S => \^bram_rst\
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[1]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[1]\,
      R => \^bram_rst\
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg_n_0_[1]\,
      Q => inc,
      R => \^bram_rst\
    );
\FSM_onehot_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => inc,
      Q => \^bram_we\,
      R => \^bram_rst\
    );
bram_rst_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rstn,
      O => \^bram_rst\
    );
\uSum_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(0),
      G => inc,
      GE => '1',
      Q => bram_din(0)
    );
\uSum_reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => bram_dout(0),
      O => plusOp(0)
    );
\uSum_reg[10]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(10),
      G => inc,
      GE => '1',
      Q => bram_din(10)
    );
\uSum_reg[11]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(11),
      G => inc,
      GE => '1',
      Q => bram_din(11)
    );
\uSum_reg[12]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(12),
      G => inc,
      GE => '1',
      Q => bram_din(12)
    );
\uSum_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \uSum_reg[8]_i_1_n_0\,
      CO(3) => \uSum_reg[12]_i_1_n_0\,
      CO(2) => \uSum_reg[12]_i_1_n_1\,
      CO(1) => \uSum_reg[12]_i_1_n_2\,
      CO(0) => \uSum_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => plusOp(12 downto 9),
      S(3 downto 0) => bram_dout(12 downto 9)
    );
\uSum_reg[13]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(13),
      G => inc,
      GE => '1',
      Q => bram_din(13)
    );
\uSum_reg[14]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(14),
      G => inc,
      GE => '1',
      Q => bram_din(14)
    );
\uSum_reg[15]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(15),
      G => inc,
      GE => '1',
      Q => bram_din(15)
    );
\uSum_reg[16]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(16),
      G => inc,
      GE => '1',
      Q => bram_din(16)
    );
\uSum_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \uSum_reg[12]_i_1_n_0\,
      CO(3) => \uSum_reg[16]_i_1_n_0\,
      CO(2) => \uSum_reg[16]_i_1_n_1\,
      CO(1) => \uSum_reg[16]_i_1_n_2\,
      CO(0) => \uSum_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => plusOp(16 downto 13),
      S(3 downto 0) => bram_dout(16 downto 13)
    );
\uSum_reg[17]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(17),
      G => inc,
      GE => '1',
      Q => bram_din(17)
    );
\uSum_reg[18]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(18),
      G => inc,
      GE => '1',
      Q => bram_din(18)
    );
\uSum_reg[19]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(19),
      G => inc,
      GE => '1',
      Q => bram_din(19)
    );
\uSum_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(1),
      G => inc,
      GE => '1',
      Q => bram_din(1)
    );
\uSum_reg[20]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(20),
      G => inc,
      GE => '1',
      Q => bram_din(20)
    );
\uSum_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \uSum_reg[16]_i_1_n_0\,
      CO(3) => \uSum_reg[20]_i_1_n_0\,
      CO(2) => \uSum_reg[20]_i_1_n_1\,
      CO(1) => \uSum_reg[20]_i_1_n_2\,
      CO(0) => \uSum_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => plusOp(20 downto 17),
      S(3 downto 0) => bram_dout(20 downto 17)
    );
\uSum_reg[21]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(21),
      G => inc,
      GE => '1',
      Q => bram_din(21)
    );
\uSum_reg[22]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(22),
      G => inc,
      GE => '1',
      Q => bram_din(22)
    );
\uSum_reg[23]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(23),
      G => inc,
      GE => '1',
      Q => bram_din(23)
    );
\uSum_reg[24]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(24),
      G => inc,
      GE => '1',
      Q => bram_din(24)
    );
\uSum_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \uSum_reg[20]_i_1_n_0\,
      CO(3) => \uSum_reg[24]_i_1_n_0\,
      CO(2) => \uSum_reg[24]_i_1_n_1\,
      CO(1) => \uSum_reg[24]_i_1_n_2\,
      CO(0) => \uSum_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => plusOp(24 downto 21),
      S(3 downto 0) => bram_dout(24 downto 21)
    );
\uSum_reg[25]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(25),
      G => inc,
      GE => '1',
      Q => bram_din(25)
    );
\uSum_reg[26]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(26),
      G => inc,
      GE => '1',
      Q => bram_din(26)
    );
\uSum_reg[27]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(27),
      G => inc,
      GE => '1',
      Q => bram_din(27)
    );
\uSum_reg[28]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(28),
      G => inc,
      GE => '1',
      Q => bram_din(28)
    );
\uSum_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \uSum_reg[24]_i_1_n_0\,
      CO(3) => \uSum_reg[28]_i_1_n_0\,
      CO(2) => \uSum_reg[28]_i_1_n_1\,
      CO(1) => \uSum_reg[28]_i_1_n_2\,
      CO(0) => \uSum_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => plusOp(28 downto 25),
      S(3 downto 0) => bram_dout(28 downto 25)
    );
\uSum_reg[29]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(29),
      G => inc,
      GE => '1',
      Q => bram_din(29)
    );
\uSum_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(2),
      G => inc,
      GE => '1',
      Q => bram_din(2)
    );
\uSum_reg[30]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(30),
      G => inc,
      GE => '1',
      Q => bram_din(30)
    );
\uSum_reg[31]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(31),
      G => inc,
      GE => '1',
      Q => bram_din(31)
    );
\uSum_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \uSum_reg[28]_i_1_n_0\,
      CO(3 downto 2) => \NLW_uSum_reg[31]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \uSum_reg[31]_i_1_n_2\,
      CO(0) => \uSum_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_uSum_reg[31]_i_1_O_UNCONNECTED\(3),
      O(2 downto 0) => plusOp(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => bram_dout(31 downto 29)
    );
\uSum_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(3),
      G => inc,
      GE => '1',
      Q => bram_din(3)
    );
\uSum_reg[4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(4),
      G => inc,
      GE => '1',
      Q => bram_din(4)
    );
\uSum_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \uSum_reg[4]_i_1_n_0\,
      CO(2) => \uSum_reg[4]_i_1_n_1\,
      CO(1) => \uSum_reg[4]_i_1_n_2\,
      CO(0) => \uSum_reg[4]_i_1_n_3\,
      CYINIT => bram_dout(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => plusOp(4 downto 1),
      S(3 downto 0) => bram_dout(4 downto 1)
    );
\uSum_reg[5]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(5),
      G => inc,
      GE => '1',
      Q => bram_din(5)
    );
\uSum_reg[6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(6),
      G => inc,
      GE => '1',
      Q => bram_din(6)
    );
\uSum_reg[7]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(7),
      G => inc,
      GE => '1',
      Q => bram_din(7)
    );
\uSum_reg[8]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(8),
      G => inc,
      GE => '1',
      Q => bram_din(8)
    );
\uSum_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \uSum_reg[4]_i_1_n_0\,
      CO(3) => \uSum_reg[8]_i_1_n_0\,
      CO(2) => \uSum_reg[8]_i_1_n_1\,
      CO(1) => \uSum_reg[8]_i_1_n_2\,
      CO(0) => \uSum_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => plusOp(8 downto 5),
      S(3 downto 0) => bram_dout(8 downto 5)
    );
\uSum_reg[9]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => plusOp(9),
      G => inc,
      GE => '1',
      Q => bram_din(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_bram_incr_0_0 is
  port (
    bram_addr : out STD_LOGIC_VECTOR ( 13 downto 0 );
    bram_clk : out STD_LOGIC;
    bram_en : out STD_LOGIC;
    bram_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_rst : out STD_LOGIC;
    bram_we : out STD_LOGIC;
    bram_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    peak_amp : in STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy : in STD_LOGIC;
    rstn : in STD_LOGIC;
    clk : in STD_LOGIC;
    segment : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_bram_incr_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_bram_incr_0_0 : entity is "design_1_bram_incr_0_0,bram_incr,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_bram_incr_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_bram_incr_0_0 : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of design_1_bram_incr_0_0 : entity is "bram_incr,Vivado 2019.1";
end design_1_bram_incr_0_0;

architecture STRUCTURE of design_1_bram_incr_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^clk\ : STD_LOGIC;
  signal \^peak_amp\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^segment\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of bram_clk : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK";
  attribute x_interface_info of bram_en : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB EN";
  attribute x_interface_info of bram_rst : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB RST";
  attribute x_interface_info of bram_we : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB WE";
  attribute x_interface_info of rstn : signal is "xilinx.com:signal:reset:1.0 rstn RST";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of rstn : signal is "XIL_INTERFACENAME rstn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of bram_addr : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR";
  attribute x_interface_parameter of bram_addr : signal is "XIL_INTERFACENAME BRAM_PORTB, MASTER_TYPE BRAM_CTRL, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, READ_LATENCY 1";
  attribute x_interface_info of bram_din : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN";
  attribute x_interface_info of bram_dout : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT";
begin
  \^clk\ <= clk;
  \^peak_amp\(14 downto 4) <= peak_amp(14 downto 4);
  \^segment\ <= segment;
  bram_addr(13) <= \^segment\;
  bram_addr(12 downto 2) <= \^peak_amp\(14 downto 4);
  bram_addr(1) <= \<const0>\;
  bram_addr(0) <= \<const0>\;
  bram_clk <= \^clk\;
  bram_en <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.design_1_bram_incr_0_0_bram_incr
     port map (
      bram_din(31 downto 0) => bram_din(31 downto 0),
      bram_dout(31 downto 0) => bram_dout(31 downto 0),
      bram_rst => bram_rst,
      bram_we => bram_we,
      clk => \^clk\,
      peak_amp_rdy => peak_amp_rdy,
      rstn => rstn
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
end STRUCTURE;
