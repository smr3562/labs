// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 17:58:00 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_pha_0_0/design_1_ip_dbg_pha_0_0_stub.v
// Design      : design_1_ip_dbg_pha_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "top,Vivado 2019.1" *)
module design_1_ip_dbg_pha_0_0(peak_amp_rdy_fast, rejectn, 
  peak_amp_rdy_slow, peak_det_signal, peak_amp_rdy_fast_out, rejectn_out, 
  peak_amp_rdy_slow_out, peak_det_signal_out)
/* synthesis syn_black_box black_box_pad_pin="peak_amp_rdy_fast,rejectn,peak_amp_rdy_slow,peak_det_signal[15:0],peak_amp_rdy_fast_out[15:0],rejectn_out[15:0],peak_amp_rdy_slow_out[15:0],peak_det_signal_out[15:0]" */;
  input peak_amp_rdy_fast;
  input rejectn;
  input peak_amp_rdy_slow;
  input [15:0]peak_det_signal;
  output [15:0]peak_amp_rdy_fast_out;
  output [15:0]rejectn_out;
  output [15:0]peak_amp_rdy_slow_out;
  output [15:0]peak_det_signal_out;
endmodule
