-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 17:46:15 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_invert_and_of_0_0/design_1_ip_dbg_invert_and_of_0_0_stub.vhdl
-- Design      : design_1_ip_dbg_invert_and_of_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_ip_dbg_invert_and_of_0_0 is
  Port ( 
    adc_data : in STD_LOGIC_VECTOR ( 13 downto 0 );
    outp : in STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    outp_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end design_1_ip_dbg_invert_and_of_0_0;

architecture stub of design_1_ip_dbg_invert_and_of_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "adc_data[13:0],outp[15:0],adc_data_out[15:0],outp_out[15:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "top,Vivado 2019.1";
begin
end;
