// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 18:31:22 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_peakdetector_0_0/design_1_ip_peakdetector_0_0_sim_netlist.v
// Design      : design_1_ip_peakdetector_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ip_peakdetector_0_0,ip_peakdetector,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "sysgen" *) 
(* X_CORE_INFO = "ip_peakdetector,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_ip_peakdetector_0_0
   (x,
    signaldomain_clk,
    axibusdomain_clk,
    axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready,
    peak_amp,
    peak_amp_dac,
    peak_amp_rdy,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 x DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME x, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}" *) input [15:0]x;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signaldomain_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input signaldomain_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input axibusdomain_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input axibusdomain_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR" *) input [4:0]axibusdomain_s_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID" *) input axibusdomain_s_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA" *) input [31:0]axibusdomain_s_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB" *) input [3:0]axibusdomain_s_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID" *) input axibusdomain_s_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY" *) input axibusdomain_s_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR" *) input [4:0]axibusdomain_s_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID" *) input axibusdomain_s_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY" *) input axibusdomain_s_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 peak_amp DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME peak_amp, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}" *) output [15:0]peak_amp;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 peak_amp_dac DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME peak_amp_dac, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}" *) output [13:0]peak_amp_dac;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 peak_amp_rdy DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME peak_amp_rdy, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}, PortType data, PortType.PROP_SRC false" *) output [0:0]peak_amp_rdy;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY" *) output axibusdomain_s_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY" *) output axibusdomain_s_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP" *) output [1:0]axibusdomain_s_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID" *) output axibusdomain_s_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY" *) output axibusdomain_s_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA" *) output [31:0]axibusdomain_s_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP" *) output [1:0]axibusdomain_s_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output axibusdomain_s_axi_rvalid;

  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [4:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [4:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire [1:0]axibusdomain_s_axi_bresp;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire [1:0]axibusdomain_s_axi_rresp;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [15:0]peak_amp;
  wire [13:0]peak_amp_dac;
  wire [0:0]peak_amp_rdy;
  wire signaldomain_clk;
  wire [15:0]x;

  design_1_ip_peakdetector_0_0_ip_peakdetector inst
       (.axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_arready),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_awready),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_bready),
        .axibusdomain_s_axi_bresp(axibusdomain_s_axi_bresp),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_rready),
        .axibusdomain_s_axi_rresp(axibusdomain_s_axi_rresp),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_wready),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .peak_amp(peak_amp),
        .peak_amp_dac(peak_amp_dac),
        .peak_amp_rdy(peak_amp_rdy),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

(* ORIG_REF_NAME = "axibusdomain_axi_lite_interface" *) 
module design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface
   (i,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    d,
    \slv_reg_array_reg[2][15] ,
    \slv_reg_array_reg[3][15] ,
    \slv_reg_array_reg[4][9] ,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rvalid,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wdata,
    axibusdomain_clk,
    axibusdomain_s_axi_awaddr,
    axibusdomain_aresetn,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_rready);
  output [15:0]i;
  output axibusdomain_s_axi_awready;
  output axibusdomain_s_axi_wready;
  output [0:0]d;
  output [15:0]\slv_reg_array_reg[2][15] ;
  output [15:0]\slv_reg_array_reg[3][15] ;
  output [9:0]\slv_reg_array_reg[4][9] ;
  output axibusdomain_s_axi_arready;
  output [31:0]axibusdomain_s_axi_rdata;
  output axibusdomain_s_axi_rvalid;
  output axibusdomain_s_axi_bvalid;
  input [3:0]axibusdomain_s_axi_wstrb;
  input [31:0]axibusdomain_s_axi_wdata;
  input axibusdomain_clk;
  input [4:0]axibusdomain_s_axi_awaddr;
  input axibusdomain_aresetn;
  input [4:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_awvalid;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_bready;
  input axibusdomain_s_axi_rready;

  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [4:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [4:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [0:0]d;
  wire [15:0]i;
  wire [15:0]\slv_reg_array_reg[2][15] ;
  wire [15:0]\slv_reg_array_reg[3][15] ;
  wire [9:0]\slv_reg_array_reg[4][9] ;

  LUT6 #(
    .INIT(64'h55555555C0000000)) 
    axi_bvalid_i_1
       (.I0(axibusdomain_s_axi_bready),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_awready),
        .I4(axibusdomain_s_axi_wready),
        .I5(axibusdomain_s_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h0F88)) 
    axi_rvalid_i_1
       (.I0(axibusdomain_s_axi_arready),
        .I1(axibusdomain_s_axi_arvalid),
        .I2(axibusdomain_s_axi_rready),
        .I3(axibusdomain_s_axi_rvalid),
        .O(axi_rvalid_i_1_n_0));
  design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface_verilog inst
       (.axi_arready_reg_0(axibusdomain_s_axi_arready),
        .axi_awready_reg_0(axibusdomain_s_axi_awready),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .axi_rvalid_reg_0(axi_rvalid_i_1_n_0),
        .axi_wready_reg_0(axibusdomain_s_axi_wready),
        .axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .d(d),
        .i(i),
        .\slv_reg_array_reg[2][15]_0 (\slv_reg_array_reg[2][15] ),
        .\slv_reg_array_reg[3][15]_0 (\slv_reg_array_reg[3][15] ),
        .\slv_reg_array_reg[4][9]_0 (\slv_reg_array_reg[4][9] ));
endmodule

(* ORIG_REF_NAME = "axibusdomain_axi_lite_interface_verilog" *) 
module design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface_verilog
   (axi_awready_reg_0,
    axi_wready_reg_0,
    axi_arready_reg_0,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_rvalid,
    i,
    d,
    \slv_reg_array_reg[2][15]_0 ,
    \slv_reg_array_reg[3][15]_0 ,
    \slv_reg_array_reg[4][9]_0 ,
    axibusdomain_s_axi_rdata,
    axibusdomain_clk,
    axi_bvalid_reg_0,
    axi_rvalid_reg_0,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_awaddr,
    axibusdomain_aresetn,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_awvalid);
  output axi_awready_reg_0;
  output axi_wready_reg_0;
  output axi_arready_reg_0;
  output axibusdomain_s_axi_bvalid;
  output axibusdomain_s_axi_rvalid;
  output [15:0]i;
  output [0:0]d;
  output [15:0]\slv_reg_array_reg[2][15]_0 ;
  output [15:0]\slv_reg_array_reg[3][15]_0 ;
  output [9:0]\slv_reg_array_reg[4][9]_0 ;
  output [31:0]axibusdomain_s_axi_rdata;
  input axibusdomain_clk;
  input axi_bvalid_reg_0;
  input axi_rvalid_reg_0;
  input [3:0]axibusdomain_s_axi_wstrb;
  input [31:0]axibusdomain_s_axi_wdata;
  input [4:0]axibusdomain_s_axi_awaddr;
  input axibusdomain_aresetn;
  input [4:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_awvalid;

  wire [4:0]axi_araddr;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire [4:0]axi_awaddr;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [4:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arvalid;
  wire [4:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [0:0]d;
  wire [2:0]dec_r__10;
  wire [2:0]dec_w__10;
  wire [15:0]i;
  wire p_0_in;
  wire [31:10]r4_x_delay;
  wire [31:16]r5_x_noise;
  wire [31:16]r6_x_min;
  wire [31:16]r7_x_max;
  wire [31:1]r8_en_pkd;
  wire \slv_reg_array[0][0]_i_1_n_0 ;
  wire \slv_reg_array[0][0]_i_2_n_0 ;
  wire \slv_reg_array[0][15]_i_1_n_0 ;
  wire \slv_reg_array[0][15]_i_2_n_0 ;
  wire \slv_reg_array[0][23]_i_1_n_0 ;
  wire \slv_reg_array[0][23]_i_2_n_0 ;
  wire \slv_reg_array[0][31]_i_1_n_0 ;
  wire \slv_reg_array[0][31]_i_2_n_0 ;
  wire \slv_reg_array[0][31]_i_3_n_0 ;
  wire \slv_reg_array[0][31]_i_5_n_0 ;
  wire \slv_reg_array[0][7]_i_1_n_0 ;
  wire \slv_reg_array[0][7]_i_2_n_0 ;
  wire \slv_reg_array[0][7]_i_3_n_0 ;
  wire \slv_reg_array[1][0]_i_1_n_0 ;
  wire \slv_reg_array[1][0]_i_2_n_0 ;
  wire \slv_reg_array[1][14]_i_1_n_0 ;
  wire \slv_reg_array[1][14]_i_2_n_0 ;
  wire \slv_reg_array[1][15]_i_1_n_0 ;
  wire \slv_reg_array[1][1]_i_1_n_0 ;
  wire \slv_reg_array[1][23]_i_1_n_0 ;
  wire \slv_reg_array[1][23]_i_2_n_0 ;
  wire \slv_reg_array[1][31]_i_1_n_0 ;
  wire \slv_reg_array[1][31]_i_2_n_0 ;
  wire \slv_reg_array[1][31]_i_4_n_0 ;
  wire \slv_reg_array[1][5]_i_1_n_0 ;
  wire \slv_reg_array[1][6]_i_1_n_0 ;
  wire \slv_reg_array[1][6]_i_2_n_0 ;
  wire \slv_reg_array[1][7]_i_1_n_0 ;
  wire \slv_reg_array[2][0]_i_1_n_0 ;
  wire \slv_reg_array[2][0]_i_2_n_0 ;
  wire \slv_reg_array[2][15]_i_1_n_0 ;
  wire \slv_reg_array[2][15]_i_2_n_0 ;
  wire \slv_reg_array[2][1]_i_1_n_0 ;
  wire \slv_reg_array[2][23]_i_1_n_0 ;
  wire \slv_reg_array[2][23]_i_2_n_0 ;
  wire \slv_reg_array[2][31]_i_1_n_0 ;
  wire \slv_reg_array[2][31]_i_2_n_0 ;
  wire \slv_reg_array[2][31]_i_4_n_0 ;
  wire \slv_reg_array[2][5]_i_1_n_0 ;
  wire \slv_reg_array[2][6]_i_1_n_0 ;
  wire \slv_reg_array[2][6]_i_2_n_0 ;
  wire \slv_reg_array[2][7]_i_1_n_0 ;
  wire \slv_reg_array[2][7]_i_2_n_0 ;
  wire \slv_reg_array[3][0]_i_1_n_0 ;
  wire \slv_reg_array[3][0]_i_2_n_0 ;
  wire \slv_reg_array[3][15]_i_1_n_0 ;
  wire \slv_reg_array[3][15]_i_2_n_0 ;
  wire \slv_reg_array[3][23]_i_1_n_0 ;
  wire \slv_reg_array[3][23]_i_2_n_0 ;
  wire \slv_reg_array[3][31]_i_1_n_0 ;
  wire \slv_reg_array[3][31]_i_2_n_0 ;
  wire \slv_reg_array[3][31]_i_4_n_0 ;
  wire \slv_reg_array[3][4]_i_1_n_0 ;
  wire \slv_reg_array[3][6]_i_1_n_0 ;
  wire \slv_reg_array[3][6]_i_2_n_0 ;
  wire \slv_reg_array[3][7]_i_1_n_0 ;
  wire \slv_reg_array[3][7]_i_2_n_0 ;
  wire \slv_reg_array[4][0]_i_1_n_0 ;
  wire \slv_reg_array[4][0]_i_2_n_0 ;
  wire \slv_reg_array[4][15]_i_1_n_0 ;
  wire \slv_reg_array[4][15]_i_2_n_0 ;
  wire \slv_reg_array[4][1]_i_1_n_0 ;
  wire \slv_reg_array[4][23]_i_1_n_0 ;
  wire \slv_reg_array[4][23]_i_2_n_0 ;
  wire \slv_reg_array[4][2]_i_1_n_0 ;
  wire \slv_reg_array[4][31]_i_1_n_0 ;
  wire \slv_reg_array[4][31]_i_2_n_0 ;
  wire \slv_reg_array[4][31]_i_4_n_0 ;
  wire \slv_reg_array[4][3]_i_1_n_0 ;
  wire \slv_reg_array[4][3]_i_2_n_0 ;
  wire \slv_reg_array[4][7]_i_1_n_0 ;
  wire \slv_reg_array[4][7]_i_2_n_0 ;
  wire [7:7]\slv_reg_array_reg[0] ;
  wire [7:7]\slv_reg_array_reg[1] ;
  wire [7:7]\slv_reg_array_reg[2] ;
  wire [15:0]\slv_reg_array_reg[2][15]_0 ;
  wire [7:7]\slv_reg_array_reg[3] ;
  wire [15:0]\slv_reg_array_reg[3][15]_0 ;
  wire [7:7]\slv_reg_array_reg[4] ;
  wire [9:0]\slv_reg_array_reg[4][9]_0 ;
  wire slv_reg_rden__0;
  wire [31:0]slv_wire_array;

  FDRE \axi_araddr_reg[0] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[0]),
        .Q(axi_araddr[0]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[1] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[1]),
        .Q(axi_araddr[1]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[2] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[2]),
        .Q(axi_araddr[2]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[3] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[3]),
        .Q(axi_araddr[3]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[4] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[4]),
        .Q(axi_araddr[4]),
        .R(p_0_in));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(axibusdomain_s_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[0] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[0]),
        .Q(axi_awaddr[0]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[1] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[1]),
        .Q(axi_awaddr[1]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[2] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[2]),
        .Q(axi_awaddr[2]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[3]),
        .Q(axi_awaddr[3]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[4]),
        .Q(axi_awaddr[4]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(axibusdomain_aresetn),
        .O(p_0_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(axibusdomain_s_axi_awvalid),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axi_awready_reg_0),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(p_0_in));
  FDRE axi_bvalid_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(axibusdomain_s_axi_bvalid),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[0]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [0]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[0]_i_2_n_0 ),
        .O(slv_wire_array[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [0]),
        .I1(\slv_reg_array_reg[2][15]_0 [0]),
        .I2(dec_r__10[1]),
        .I3(i[0]),
        .I4(dec_r__10[0]),
        .I5(d),
        .O(\axi_rdata[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[10]_i_1 
       (.I0(r4_x_delay[10]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[10]_i_2_n_0 ),
        .O(slv_wire_array[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [10]),
        .I1(\slv_reg_array_reg[2][15]_0 [10]),
        .I2(dec_r__10[1]),
        .I3(i[10]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[11]_i_1 
       (.I0(r4_x_delay[11]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[11]_i_2_n_0 ),
        .O(slv_wire_array[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [11]),
        .I1(\slv_reg_array_reg[2][15]_0 [11]),
        .I2(dec_r__10[1]),
        .I3(i[11]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[12]_i_1 
       (.I0(r4_x_delay[12]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[12]_i_2_n_0 ),
        .O(slv_wire_array[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [12]),
        .I1(\slv_reg_array_reg[2][15]_0 [12]),
        .I2(dec_r__10[1]),
        .I3(i[12]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[13]_i_1 
       (.I0(r4_x_delay[13]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[13]_i_2_n_0 ),
        .O(slv_wire_array[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [13]),
        .I1(\slv_reg_array_reg[2][15]_0 [13]),
        .I2(dec_r__10[1]),
        .I3(i[13]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[14]_i_1 
       (.I0(r4_x_delay[14]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[14]_i_2_n_0 ),
        .O(slv_wire_array[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [14]),
        .I1(\slv_reg_array_reg[2][15]_0 [14]),
        .I2(dec_r__10[1]),
        .I3(i[14]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[15]_i_1 
       (.I0(r4_x_delay[15]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[15]_i_2_n_0 ),
        .O(slv_wire_array[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [15]),
        .I1(\slv_reg_array_reg[2][15]_0 [15]),
        .I2(dec_r__10[1]),
        .I3(i[15]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[16]_i_1 
       (.I0(r4_x_delay[16]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[16]_i_2_n_0 ),
        .O(slv_wire_array[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(r5_x_noise[16]),
        .I1(r6_x_min[16]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[16]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[17]_i_1 
       (.I0(r4_x_delay[17]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[17]_i_2_n_0 ),
        .O(slv_wire_array[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(r5_x_noise[17]),
        .I1(r6_x_min[17]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[17]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[18]_i_1 
       (.I0(r4_x_delay[18]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[18]_i_2_n_0 ),
        .O(slv_wire_array[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(r5_x_noise[18]),
        .I1(r6_x_min[18]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[18]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[19]_i_1 
       (.I0(r4_x_delay[19]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[19]_i_2_n_0 ),
        .O(slv_wire_array[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_2 
       (.I0(r5_x_noise[19]),
        .I1(r6_x_min[19]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[19]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[1]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [1]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[1]_i_2_n_0 ),
        .O(slv_wire_array[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [1]),
        .I1(\slv_reg_array_reg[2][15]_0 [1]),
        .I2(dec_r__10[1]),
        .I3(i[1]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[20]_i_1 
       (.I0(r4_x_delay[20]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[20]_i_2_n_0 ),
        .O(slv_wire_array[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(r5_x_noise[20]),
        .I1(r6_x_min[20]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[20]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[21]_i_1 
       (.I0(r4_x_delay[21]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[21]_i_2_n_0 ),
        .O(slv_wire_array[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(r5_x_noise[21]),
        .I1(r6_x_min[21]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[21]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[22]_i_1 
       (.I0(r4_x_delay[22]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[22]_i_2_n_0 ),
        .O(slv_wire_array[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(r5_x_noise[22]),
        .I1(r6_x_min[22]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[22]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[23]_i_1 
       (.I0(r4_x_delay[23]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[23]_i_2_n_0 ),
        .O(slv_wire_array[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_2 
       (.I0(r5_x_noise[23]),
        .I1(r6_x_min[23]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[23]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[24]_i_1 
       (.I0(r4_x_delay[24]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[24]_i_2_n_0 ),
        .O(slv_wire_array[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(r5_x_noise[24]),
        .I1(r6_x_min[24]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[24]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[25]_i_1 
       (.I0(r4_x_delay[25]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[25]_i_2_n_0 ),
        .O(slv_wire_array[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(r5_x_noise[25]),
        .I1(r6_x_min[25]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[25]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[26]_i_1 
       (.I0(r4_x_delay[26]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[26]_i_2_n_0 ),
        .O(slv_wire_array[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(r5_x_noise[26]),
        .I1(r6_x_min[26]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[26]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[27]_i_1 
       (.I0(r4_x_delay[27]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[27]_i_2_n_0 ),
        .O(slv_wire_array[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(r5_x_noise[27]),
        .I1(r6_x_min[27]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[27]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[28]_i_1 
       (.I0(r4_x_delay[28]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[28]_i_2_n_0 ),
        .O(slv_wire_array[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(r5_x_noise[28]),
        .I1(r6_x_min[28]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[28]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[29]_i_1 
       (.I0(r4_x_delay[29]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[29]_i_2_n_0 ),
        .O(slv_wire_array[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(r5_x_noise[29]),
        .I1(r6_x_min[29]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[29]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[2]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [2]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[2]_i_2_n_0 ),
        .O(slv_wire_array[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [2]),
        .I1(\slv_reg_array_reg[2][15]_0 [2]),
        .I2(dec_r__10[1]),
        .I3(i[2]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[30]_i_1 
       (.I0(r4_x_delay[30]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[30]_i_2_n_0 ),
        .O(slv_wire_array[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(r5_x_noise[30]),
        .I1(r6_x_min[30]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[30]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[31]_i_1 
       (.I0(r4_x_delay[31]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[31]_i_3_n_0 ),
        .O(slv_wire_array[31]));
  LUT5 #(
    .INIT(32'h00000002)) 
    \axi_rdata[31]_i_2 
       (.I0(axi_araddr[4]),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[1]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[0]),
        .O(dec_r__10[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_3 
       (.I0(r5_x_noise[31]),
        .I1(r6_x_min[31]),
        .I2(dec_r__10[1]),
        .I3(r7_x_max[31]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_4 
       (.I0(axi_araddr[4]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[1]),
        .O(dec_r__10[1]));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_5 
       (.I0(axi_araddr[4]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[1]),
        .O(dec_r__10[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[3]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [3]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[3]_i_2_n_0 ),
        .O(slv_wire_array[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [3]),
        .I1(\slv_reg_array_reg[2][15]_0 [3]),
        .I2(dec_r__10[1]),
        .I3(i[3]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[4]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [4]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[4]_i_2_n_0 ),
        .O(slv_wire_array[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [4]),
        .I1(\slv_reg_array_reg[2][15]_0 [4]),
        .I2(dec_r__10[1]),
        .I3(i[4]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[5]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [5]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[5]_i_2_n_0 ),
        .O(slv_wire_array[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [5]),
        .I1(\slv_reg_array_reg[2][15]_0 [5]),
        .I2(dec_r__10[1]),
        .I3(i[5]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[6]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [6]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[6]_i_2_n_0 ),
        .O(slv_wire_array[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [6]),
        .I1(\slv_reg_array_reg[2][15]_0 [6]),
        .I2(dec_r__10[1]),
        .I3(i[6]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[7]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [7]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[7]_i_2_n_0 ),
        .O(slv_wire_array[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [7]),
        .I1(\slv_reg_array_reg[2][15]_0 [7]),
        .I2(dec_r__10[1]),
        .I3(i[7]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[8]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [8]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[8]_i_2_n_0 ),
        .O(slv_wire_array[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [8]),
        .I1(\slv_reg_array_reg[2][15]_0 [8]),
        .I2(dec_r__10[1]),
        .I3(i[8]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[9]_i_1 
       (.I0(\slv_reg_array_reg[4][9]_0 [9]),
        .I1(dec_r__10[2]),
        .I2(\axi_rdata[9]_i_2_n_0 ),
        .O(slv_wire_array[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(\slv_reg_array_reg[3][15]_0 [9]),
        .I1(\slv_reg_array_reg[2][15]_0 [9]),
        .I2(dec_r__10[1]),
        .I3(i[9]),
        .I4(dec_r__10[0]),
        .I5(r8_en_pkd[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[0]),
        .Q(axibusdomain_s_axi_rdata[0]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[10] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[10]),
        .Q(axibusdomain_s_axi_rdata[10]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[11] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[11]),
        .Q(axibusdomain_s_axi_rdata[11]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[12] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[12]),
        .Q(axibusdomain_s_axi_rdata[12]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[13] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[13]),
        .Q(axibusdomain_s_axi_rdata[13]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[14] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[14]),
        .Q(axibusdomain_s_axi_rdata[14]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[15] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[15]),
        .Q(axibusdomain_s_axi_rdata[15]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[16] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[16]),
        .Q(axibusdomain_s_axi_rdata[16]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[17] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[17]),
        .Q(axibusdomain_s_axi_rdata[17]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[18] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[18]),
        .Q(axibusdomain_s_axi_rdata[18]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[19] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[19]),
        .Q(axibusdomain_s_axi_rdata[19]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[1] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[1]),
        .Q(axibusdomain_s_axi_rdata[1]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[20] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[20]),
        .Q(axibusdomain_s_axi_rdata[20]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[21] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[21]),
        .Q(axibusdomain_s_axi_rdata[21]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[22] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[22]),
        .Q(axibusdomain_s_axi_rdata[22]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[23] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[23]),
        .Q(axibusdomain_s_axi_rdata[23]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[24] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[24]),
        .Q(axibusdomain_s_axi_rdata[24]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[25] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[25]),
        .Q(axibusdomain_s_axi_rdata[25]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[26] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[26]),
        .Q(axibusdomain_s_axi_rdata[26]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[27] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[27]),
        .Q(axibusdomain_s_axi_rdata[27]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[28] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[28]),
        .Q(axibusdomain_s_axi_rdata[28]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[29] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[29]),
        .Q(axibusdomain_s_axi_rdata[29]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[2] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[2]),
        .Q(axibusdomain_s_axi_rdata[2]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[30] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[30]),
        .Q(axibusdomain_s_axi_rdata[30]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[31] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[31]),
        .Q(axibusdomain_s_axi_rdata[31]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[3] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[3]),
        .Q(axibusdomain_s_axi_rdata[3]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[4] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[4]),
        .Q(axibusdomain_s_axi_rdata[4]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[5] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[5]),
        .Q(axibusdomain_s_axi_rdata[5]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[6] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[6]),
        .Q(axibusdomain_s_axi_rdata[6]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[7] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[7]),
        .Q(axibusdomain_s_axi_rdata[7]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[8] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[8]),
        .Q(axibusdomain_s_axi_rdata[8]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[9] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[9]),
        .Q(axibusdomain_s_axi_rdata[9]),
        .R(p_0_in));
  FDRE axi_rvalid_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_rvalid_reg_0),
        .Q(axibusdomain_s_axi_rvalid),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(axibusdomain_s_axi_awvalid),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axi_wready_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hFDFFFDFDFD00FDFD)) 
    \slv_reg_array[0][0]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[0][31]_i_5_n_0 ),
        .I2(axibusdomain_s_axi_wdata[0]),
        .I3(\slv_reg_array[0][0]_i_2_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(d),
        .O(\slv_reg_array[0][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[0][0]_i_2 
       (.I0(\slv_reg_array_reg[0] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[0][0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \slv_reg_array[0][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[0] ),
        .I3(\slv_reg_array[0][31]_i_5_n_0 ),
        .O(\slv_reg_array[0][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[0][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[0] ),
        .O(\slv_reg_array[0][15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \slv_reg_array[0][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[0] ),
        .I3(\slv_reg_array[0][31]_i_5_n_0 ),
        .O(\slv_reg_array[0][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[0][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[0] ),
        .O(\slv_reg_array[0][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \slv_reg_array[0][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[0] ),
        .I3(\slv_reg_array[0][31]_i_5_n_0 ),
        .O(\slv_reg_array[0][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[0][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[0] ),
        .O(\slv_reg_array[0][31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \slv_reg_array[0][31]_i_3 
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .I2(axi_wready_reg_0),
        .I3(axi_awready_reg_0),
        .I4(axibusdomain_aresetn),
        .O(\slv_reg_array[0][31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFE1FFFFFFFF)) 
    \slv_reg_array[0][31]_i_4 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[0] ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAABBBE)) 
    \slv_reg_array[0][31]_i_5 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[1]),
        .I5(axi_awaddr[0]),
        .O(\slv_reg_array[0][31]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD0)) 
    \slv_reg_array[0][7]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[0][31]_i_5_n_0 ),
        .I2(\slv_reg_array[0][7]_i_2_n_0 ),
        .O(\slv_reg_array[0][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[0][7]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[0] ),
        .O(\slv_reg_array[0][7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[0][7]_i_3 
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .O(\slv_reg_array[0][7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h80FF808080008080)) 
    \slv_reg_array[1][0]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[0]),
        .I3(\slv_reg_array[1][0]_i_2_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(i[0]),
        .O(\slv_reg_array[1][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[1][0]_i_2 
       (.I0(\slv_reg_array_reg[1] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[1][0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000FB0000)) 
    \slv_reg_array[1][14]_i_1 
       (.I0(dec_w__10[2]),
        .I1(dec_w__10[0]),
        .I2(dec_w__10[1]),
        .I3(axibusdomain_aresetn),
        .I4(axibusdomain_s_axi_wstrb[1]),
        .I5(\slv_reg_array[0][31]_i_3_n_0 ),
        .O(\slv_reg_array[1][14]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[1][14]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[1] ),
        .O(\slv_reg_array[1][14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \slv_reg_array[1][14]_i_3 
       (.I0(axi_awaddr[0]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[3]),
        .I4(axi_awaddr[4]),
        .O(dec_w__10[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    \slv_reg_array[1][14]_i_4 
       (.I0(axi_awaddr[1]),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[2]),
        .O(dec_w__10[0]));
  LUT4 #(
    .INIT(16'h0100)) 
    \slv_reg_array[1][14]_i_5 
       (.I0(axi_awaddr[1]),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[3]),
        .O(dec_w__10[1]));
  LUT6 #(
    .INIT(64'hFFBFBFBF40000000)) 
    \slv_reg_array[1][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[1] ),
        .I3(axibusdomain_s_axi_wdata[15]),
        .I4(\slv_reg_array[1][31]_i_4_n_0 ),
        .I5(i[15]),
        .O(\slv_reg_array[1][15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80FF8000)) 
    \slv_reg_array[1][1]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[1]),
        .I3(\slv_reg_array[1][6]_i_2_n_0 ),
        .I4(i[1]),
        .O(\slv_reg_array[1][1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[1][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[1] ),
        .I3(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[1][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[1] ),
        .O(\slv_reg_array[1][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[1][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[1] ),
        .I3(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[1][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[1] ),
        .O(\slv_reg_array[1][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000010FFFFFFFF)) 
    \slv_reg_array[1][31]_i_3 
       (.I0(axi_awaddr[0]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[3]),
        .I4(axi_awaddr[4]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[1] ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \slv_reg_array[1][31]_i_4 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[1]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[1][31]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h80FF8000)) 
    \slv_reg_array[1][5]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[5]),
        .I3(\slv_reg_array[1][6]_i_2_n_0 ),
        .I4(i[5]),
        .O(\slv_reg_array[1][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \slv_reg_array[1][6]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(dec_w__10[2]),
        .I2(\slv_reg_array[0][31]_i_3_n_0 ),
        .I3(dec_w__10[1]),
        .I4(dec_w__10[0]),
        .I5(\slv_reg_array[1][6]_i_2_n_0 ),
        .O(\slv_reg_array[1][6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[1][6]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[1] ),
        .O(\slv_reg_array[1][6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h80FF8000)) 
    \slv_reg_array[1][7]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[7]),
        .I3(\slv_reg_array[1][6]_i_2_n_0 ),
        .I4(i[7]),
        .O(\slv_reg_array[1][7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEFEEE0EE)) 
    \slv_reg_array[2][0]_i_1 
       (.I0(\slv_reg_array[2][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[0]),
        .I2(\slv_reg_array[2][0]_i_2_n_0 ),
        .I3(axibusdomain_aresetn),
        .I4(\slv_reg_array_reg[2][15]_0 [0]),
        .O(\slv_reg_array[2][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[2][0]_i_2 
       (.I0(\slv_reg_array_reg[2] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[2][0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[2][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[2] ),
        .I3(\slv_reg_array[2][31]_i_4_n_0 ),
        .O(\slv_reg_array[2][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[2][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[2] ),
        .O(\slv_reg_array[2][15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[2][1]_i_1 
       (.I0(\slv_reg_array[2][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[1]),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[2][15]_0 [1]),
        .O(\slv_reg_array[2][1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[2][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[2] ),
        .I3(\slv_reg_array[2][31]_i_4_n_0 ),
        .O(\slv_reg_array[2][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[2][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[2] ),
        .O(\slv_reg_array[2][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[2][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[2] ),
        .I3(\slv_reg_array[2][31]_i_4_n_0 ),
        .O(\slv_reg_array[2][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[2][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[2] ),
        .O(\slv_reg_array[2][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000002FFFFFFFF)) 
    \slv_reg_array[2][31]_i_3 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[2] ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \slv_reg_array[2][31]_i_4 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[2]),
        .I5(axi_awaddr[3]),
        .O(\slv_reg_array[2][31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[2][5]_i_1 
       (.I0(\slv_reg_array[2][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[5]),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[2][15]_0 [5]),
        .O(\slv_reg_array[2][5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \slv_reg_array[2][6]_i_1 
       (.I0(\slv_reg_array[2][6]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[2][31]_i_4_n_0 ),
        .O(\slv_reg_array[2][6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[2][6]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[2] ),
        .O(\slv_reg_array[2][6]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[2][7]_i_1 
       (.I0(\slv_reg_array[2][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[7]),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[2][15]_0 [7]),
        .O(\slv_reg_array[2][7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFDFF)) 
    \slv_reg_array[2][7]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(dec_w__10[0]),
        .I2(dec_w__10[2]),
        .I3(dec_w__10[1]),
        .I4(\slv_reg_array[0][31]_i_3_n_0 ),
        .O(\slv_reg_array[2][7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFEEE0EE)) 
    \slv_reg_array[3][0]_i_1 
       (.I0(\slv_reg_array[3][6]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[0]),
        .I2(\slv_reg_array[3][0]_i_2_n_0 ),
        .I3(axibusdomain_aresetn),
        .I4(\slv_reg_array_reg[3][15]_0 [0]),
        .O(\slv_reg_array[3][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[3][0]_i_2 
       (.I0(\slv_reg_array_reg[3] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[3][0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[3][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[3] ),
        .I3(\slv_reg_array[3][31]_i_4_n_0 ),
        .O(\slv_reg_array[3][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[3] ),
        .O(\slv_reg_array[3][15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[3][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[3] ),
        .I3(\slv_reg_array[3][31]_i_4_n_0 ),
        .O(\slv_reg_array[3][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[3] ),
        .O(\slv_reg_array[3][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[3][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[3] ),
        .I3(\slv_reg_array[3][31]_i_4_n_0 ),
        .O(\slv_reg_array[3][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[3] ),
        .O(\slv_reg_array[3][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000008FFFFFFFF)) 
    \slv_reg_array[3][31]_i_3 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[3] ));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \slv_reg_array[3][31]_i_4 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[2]),
        .O(\slv_reg_array[3][31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[3][4]_i_1 
       (.I0(\slv_reg_array[3][6]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[4]),
        .I2(\slv_reg_array[3][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[3][15]_0 [4]),
        .O(\slv_reg_array[3][4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[3][6]_i_1 
       (.I0(\slv_reg_array[3][6]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[6]),
        .I2(\slv_reg_array[3][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[3][15]_0 [6]),
        .O(\slv_reg_array[3][6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFF7F)) 
    \slv_reg_array[3][6]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(dec_w__10[1]),
        .I2(dec_w__10[0]),
        .I3(dec_w__10[2]),
        .I4(\slv_reg_array[0][31]_i_3_n_0 ),
        .O(\slv_reg_array[3][6]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \slv_reg_array[3][7]_i_1 
       (.I0(\slv_reg_array[3][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[3][31]_i_4_n_0 ),
        .O(\slv_reg_array[3][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[3][7]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[3] ),
        .O(\slv_reg_array[3][7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFEEE0EE)) 
    \slv_reg_array[4][0]_i_1 
       (.I0(\slv_reg_array[4][3]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[0]),
        .I2(\slv_reg_array[4][0]_i_2_n_0 ),
        .I3(axibusdomain_aresetn),
        .I4(\slv_reg_array_reg[4][9]_0 [0]),
        .O(\slv_reg_array[4][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA000000030000000)) 
    \slv_reg_array[4][0]_i_2 
       (.I0(\slv_reg_array_reg[4] ),
        .I1(axibusdomain_aresetn),
        .I2(\slv_reg_array[0][7]_i_3_n_0 ),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_s_axi_wstrb[0]),
        .O(\slv_reg_array[4][0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[4][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[4] ),
        .I3(\slv_reg_array[4][31]_i_4_n_0 ),
        .O(\slv_reg_array[4][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[4][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array_reg[4] ),
        .O(\slv_reg_array[4][15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[4][1]_i_1 
       (.I0(\slv_reg_array[4][3]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[1]),
        .I2(\slv_reg_array[4][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[4][9]_0 [1]),
        .O(\slv_reg_array[4][1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[4][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[4] ),
        .I3(\slv_reg_array[4][31]_i_4_n_0 ),
        .O(\slv_reg_array[4][23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[4][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array_reg[4] ),
        .O(\slv_reg_array[4][23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[4][2]_i_1 
       (.I0(\slv_reg_array[4][3]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[2]),
        .I2(\slv_reg_array[4][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[4][9]_0 [2]),
        .O(\slv_reg_array[4][2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    \slv_reg_array[4][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[4] ),
        .I3(\slv_reg_array[4][31]_i_4_n_0 ),
        .O(\slv_reg_array[4][31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[4][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array_reg[4] ),
        .O(\slv_reg_array[4][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000010FFFFFFFF)) 
    \slv_reg_array[4][31]_i_3 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array_reg[4] ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \slv_reg_array[4][31]_i_4 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[2]),
        .I2(axi_awaddr[4]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(\slv_reg_array[0][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hEFE0)) 
    \slv_reg_array[4][3]_i_1 
       (.I0(\slv_reg_array[4][3]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wdata[3]),
        .I2(\slv_reg_array[4][7]_i_2_n_0 ),
        .I3(\slv_reg_array_reg[4][9]_0 [3]),
        .O(\slv_reg_array[4][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFBFF)) 
    \slv_reg_array[4][3]_i_2 
       (.I0(dec_w__10[1]),
        .I1(dec_w__10[2]),
        .I2(\slv_reg_array[0][31]_i_3_n_0 ),
        .I3(axibusdomain_s_axi_wstrb[0]),
        .I4(dec_w__10[0]),
        .O(\slv_reg_array[4][3]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \slv_reg_array[4][7]_i_1 
       (.I0(\slv_reg_array[4][7]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[4][31]_i_4_n_0 ),
        .O(\slv_reg_array[4][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[4][7]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(\slv_reg_array[0][7]_i_3_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[4] ),
        .O(\slv_reg_array[4][7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[0][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[0][0]_i_1_n_0 ),
        .Q(d),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(r8_en_pkd[10]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(r8_en_pkd[11]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(r8_en_pkd[12]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(r8_en_pkd[13]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(r8_en_pkd[14]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[15]),
        .Q(r8_en_pkd[15]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r8_en_pkd[16]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r8_en_pkd[17]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r8_en_pkd[18]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r8_en_pkd[19]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[1]),
        .Q(r8_en_pkd[1]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r8_en_pkd[20]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r8_en_pkd[21]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r8_en_pkd[22]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r8_en_pkd[23]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r8_en_pkd[24]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r8_en_pkd[25]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r8_en_pkd[26]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r8_en_pkd[27]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r8_en_pkd[28]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r8_en_pkd[29]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[2]),
        .Q(r8_en_pkd[2]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r8_en_pkd[30]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r8_en_pkd[31]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[3]),
        .Q(r8_en_pkd[3]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[4]),
        .Q(r8_en_pkd[4]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[5]),
        .Q(r8_en_pkd[5]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[6]),
        .Q(r8_en_pkd[6]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[7]),
        .Q(r8_en_pkd[7]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(r8_en_pkd[8]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(r8_en_pkd[9]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][0]_i_1_n_0 ),
        .Q(i[0]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(i[10]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(i[11]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(i[12]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(i[13]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(i[14]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][15] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][15]_i_1_n_0 ),
        .Q(i[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r7_x_max[16]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r7_x_max[17]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r7_x_max[18]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r7_x_max[19]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][1] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][1]_i_1_n_0 ),
        .Q(i[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r7_x_max[20]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r7_x_max[21]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r7_x_max[22]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r7_x_max[23]),
        .R(\slv_reg_array[1][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r7_x_max[24]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r7_x_max[25]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r7_x_max[26]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r7_x_max[27]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r7_x_max[28]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r7_x_max[29]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[2]),
        .Q(i[2]),
        .S(\slv_reg_array[1][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r7_x_max[30]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r7_x_max[31]),
        .R(\slv_reg_array[1][31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[3]),
        .Q(i[3]),
        .S(\slv_reg_array[1][6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[4]),
        .Q(i[4]),
        .S(\slv_reg_array[1][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][5] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][5]_i_1_n_0 ),
        .Q(i[5]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[6]),
        .Q(i[6]),
        .S(\slv_reg_array[1][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][7] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][7]_i_1_n_0 ),
        .Q(i[7]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(i[8]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][14]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(i[9]),
        .S(\slv_reg_array[1][14]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[2][15]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(\slv_reg_array_reg[2][15]_0 [10]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(\slv_reg_array_reg[2][15]_0 [11]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(\slv_reg_array_reg[2][15]_0 [12]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(\slv_reg_array_reg[2][15]_0 [13]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(\slv_reg_array_reg[2][15]_0 [14]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[15]),
        .Q(\slv_reg_array_reg[2][15]_0 [15]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r6_x_min[16]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r6_x_min[17]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r6_x_min[18]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r6_x_min[19]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][1] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][1]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[2][15]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r6_x_min[20]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r6_x_min[21]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r6_x_min[22]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r6_x_min[23]),
        .R(\slv_reg_array[2][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r6_x_min[24]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r6_x_min[25]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r6_x_min[26]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r6_x_min[27]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r6_x_min[28]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r6_x_min[29]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[2]),
        .Q(\slv_reg_array_reg[2][15]_0 [2]),
        .R(\slv_reg_array[2][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r6_x_min[30]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r6_x_min[31]),
        .R(\slv_reg_array[2][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[3]),
        .Q(\slv_reg_array_reg[2][15]_0 [3]),
        .R(\slv_reg_array[2][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[4]),
        .Q(\slv_reg_array_reg[2][15]_0 [4]),
        .R(\slv_reg_array[2][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][5] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][5]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[2][15]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][6]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[6]),
        .Q(\slv_reg_array_reg[2][15]_0 [6]),
        .R(\slv_reg_array[2][6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][7] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][7]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[2][15]_0 [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(\slv_reg_array_reg[2][15]_0 [8]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(\slv_reg_array_reg[2][15]_0 [9]),
        .R(\slv_reg_array[2][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(\slv_reg_array_reg[3][15]_0 [10]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(\slv_reg_array_reg[3][15]_0 [11]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(\slv_reg_array_reg[3][15]_0 [12]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(\slv_reg_array_reg[3][15]_0 [13]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(\slv_reg_array_reg[3][15]_0 [14]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[15]),
        .Q(\slv_reg_array_reg[3][15]_0 [15]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r5_x_noise[16]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r5_x_noise[17]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r5_x_noise[18]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r5_x_noise[19]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[1]),
        .Q(\slv_reg_array_reg[3][15]_0 [1]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r5_x_noise[20]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r5_x_noise[21]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r5_x_noise[22]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r5_x_noise[23]),
        .R(\slv_reg_array[3][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r5_x_noise[24]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r5_x_noise[25]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r5_x_noise[26]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r5_x_noise[27]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r5_x_noise[28]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r5_x_noise[29]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[2]),
        .Q(\slv_reg_array_reg[3][15]_0 [2]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r5_x_noise[30]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r5_x_noise[31]),
        .R(\slv_reg_array[3][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[3]),
        .Q(\slv_reg_array_reg[3][15]_0 [3]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][4] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][4]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[5]),
        .Q(\slv_reg_array_reg[3][15]_0 [5]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][6] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][6]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[7]),
        .Q(\slv_reg_array_reg[3][15]_0 [7]),
        .R(\slv_reg_array[3][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(\slv_reg_array_reg[3][15]_0 [8]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(\slv_reg_array_reg[3][15]_0 [9]),
        .R(\slv_reg_array[3][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[4][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[4][9]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[10]),
        .Q(r4_x_delay[10]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[11]),
        .Q(r4_x_delay[11]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[12]),
        .Q(r4_x_delay[12]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[13]),
        .Q(r4_x_delay[13]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[14]),
        .Q(r4_x_delay[14]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[15]),
        .Q(r4_x_delay[15]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[16]),
        .Q(r4_x_delay[16]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[17]),
        .Q(r4_x_delay[17]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[18]),
        .Q(r4_x_delay[18]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[19]),
        .Q(r4_x_delay[19]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[4][1] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][1]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[4][9]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[20]),
        .Q(r4_x_delay[20]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[21]),
        .Q(r4_x_delay[21]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[22]),
        .Q(r4_x_delay[22]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[23]),
        .Q(r4_x_delay[23]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[24]),
        .Q(r4_x_delay[24]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[25]),
        .Q(r4_x_delay[25]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[26]),
        .Q(r4_x_delay[26]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[27]),
        .Q(r4_x_delay[27]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[28]),
        .Q(r4_x_delay[28]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[29]),
        .Q(r4_x_delay[29]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[4][2] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][2]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[4][9]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[30]),
        .Q(r4_x_delay[30]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[31]),
        .Q(r4_x_delay[31]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[4][3] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][3]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[4][9]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[4]),
        .Q(\slv_reg_array_reg[4][9]_0 [4]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[5]),
        .Q(\slv_reg_array_reg[4][9]_0 [5]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[6]),
        .Q(\slv_reg_array_reg[4][9]_0 [6]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[7]),
        .Q(\slv_reg_array_reg[4][9]_0 [7]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[8]),
        .Q(\slv_reg_array_reg[4][9]_0 [8]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(axibusdomain_s_axi_wdata[9]),
        .Q(\slv_reg_array_reg[4][9]_0 [9]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(axibusdomain_s_axi_arvalid),
        .I1(axibusdomain_s_axi_rvalid),
        .I2(axi_arready_reg_0),
        .O(slv_reg_rden__0));
endmodule

(* ORIG_REF_NAME = "convert_func_call_ip_peakdetector_xlconvert" *) 
module design_1_ip_peakdetector_0_0_convert_func_call_ip_peakdetector_xlconvert
   (O,
    \xmax_i_12_24_reg[9] ,
    \xmax_i_12_24_reg[13] ,
    CO,
    \xmax_i_12_24_reg[15] ,
    Q,
    S);
  output [3:0]O;
  output [3:0]\xmax_i_12_24_reg[9] ;
  output [3:0]\xmax_i_12_24_reg[13] ;
  output [0:0]CO;
  output [1:0]\xmax_i_12_24_reg[15] ;
  input [13:0]Q;
  input [0:0]S;

  wire [0:0]CO;
  wire [3:0]O;
  wire [13:0]Q;
  wire [0:0]S;
  wire inp_carry__0_n_0;
  wire inp_carry__0_n_1;
  wire inp_carry__0_n_2;
  wire inp_carry__0_n_3;
  wire inp_carry__1_n_0;
  wire inp_carry__1_n_1;
  wire inp_carry__1_n_2;
  wire inp_carry__1_n_3;
  wire inp_carry__2_n_3;
  wire inp_carry_n_0;
  wire inp_carry_n_1;
  wire inp_carry_n_2;
  wire inp_carry_n_3;
  wire [3:0]\xmax_i_12_24_reg[13] ;
  wire [1:0]\xmax_i_12_24_reg[15] ;
  wire [3:0]\xmax_i_12_24_reg[9] ;
  wire [3:1]NLW_inp_carry__2_CO_UNCONNECTED;
  wire [3:2]NLW_inp_carry__2_O_UNCONNECTED;

  CARRY4 inp_carry
       (.CI(1'b0),
        .CO({inp_carry_n_0,inp_carry_n_1,inp_carry_n_2,inp_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Q[0]}),
        .O(O),
        .S({Q[3:1],S}));
  CARRY4 inp_carry__0
       (.CI(inp_carry_n_0),
        .CO({inp_carry__0_n_0,inp_carry__0_n_1,inp_carry__0_n_2,inp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\xmax_i_12_24_reg[9] ),
        .S(Q[7:4]));
  CARRY4 inp_carry__1
       (.CI(inp_carry__0_n_0),
        .CO({inp_carry__1_n_0,inp_carry__1_n_1,inp_carry__1_n_2,inp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\xmax_i_12_24_reg[13] ),
        .S(Q[11:8]));
  CARRY4 inp_carry__2
       (.CI(inp_carry__1_n_0),
        .CO({NLW_inp_carry__2_CO_UNCONNECTED[3],CO,NLW_inp_carry__2_CO_UNCONNECTED[1],inp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({NLW_inp_carry__2_O_UNCONNECTED[3:2],\xmax_i_12_24_reg[15] }),
        .S({1'b0,1'b1,Q[13:12]}));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector
   (x,
    signaldomain_clk,
    axibusdomain_clk,
    axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready,
    peak_amp,
    peak_amp_dac,
    peak_amp_rdy,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid);
  input [15:0]x;
  input signaldomain_clk;
  input axibusdomain_clk;
  input axibusdomain_aresetn;
  input [4:0]axibusdomain_s_axi_awaddr;
  input axibusdomain_s_axi_awvalid;
  input [31:0]axibusdomain_s_axi_wdata;
  input [3:0]axibusdomain_s_axi_wstrb;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_bready;
  input [4:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_rready;
  output [15:0]peak_amp;
  output [13:0]peak_amp_dac;
  output [0:0]peak_amp_rdy;
  output axibusdomain_s_axi_awready;
  output axibusdomain_s_axi_wready;
  output [1:0]axibusdomain_s_axi_bresp;
  output axibusdomain_s_axi_bvalid;
  output axibusdomain_s_axi_arready;
  output [31:0]axibusdomain_s_axi_rdata;
  output [1:0]axibusdomain_s_axi_rresp;
  output axibusdomain_s_axi_rvalid;

  wire \<const0> ;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [4:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [4:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [15:0]peak_amp;
  wire [13:0]peak_amp_dac;
  wire [0:0]peak_amp_rdy;
  wire [9:0]r4_x_delay;
  wire [15:0]r5_x_noise;
  wire [15:0]r6_x_min;
  wire [15:0]r7_x_max;
  wire [0:0]r8_en_pkd;
  wire signaldomain_clk;
  wire [15:0]x;

  assign axibusdomain_s_axi_bresp[1] = \<const0> ;
  assign axibusdomain_s_axi_bresp[0] = \<const0> ;
  assign axibusdomain_s_axi_rresp[1] = \<const0> ;
  assign axibusdomain_s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_ip_peakdetector_0_0_axibusdomain_axi_lite_interface axibusdomain_axi_lite_interface
       (.axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_arready),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_awready),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_bready),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_rready),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_wready),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .d(r8_en_pkd),
        .i(r7_x_max),
        .\slv_reg_array_reg[2][15] (r6_x_min),
        .\slv_reg_array_reg[3][15] (r5_x_noise),
        .\slv_reg_array_reg[4][9] (r4_x_delay));
  design_1_ip_peakdetector_0_0_ip_peakdetector_struct ip_peakdetector_struct
       (.Q(peak_amp),
        .axibusdomain_clk(axibusdomain_clk),
        .peak_amp_dac(peak_amp_dac),
        .peak_amp_rdy(peak_amp_rdy),
        .r4_x_delay(r4_x_delay),
        .r5_x_noise(r5_x_noise),
        .r6_x_min(r6_x_min),
        .r7_x_max(r7_x_max),
        .r8_en_pkd(r8_en_pkd),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

(* CHECK_LICENSE_TYPE = "ip_peakdetector_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "ip_peakdetector_blk_mem_gen_i0" *) 
(* X_CORE_INFO = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0
   (clka,
    ena,
    wea,
    addra,
    dina,
    douta);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [15:0]dina;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [15:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [15:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [15:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.57205 mW" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ip_peakdetector_blk_mem_gen_i0.mem" *) 
  (* C_INIT_FILE_NAME = "ip_peakdetector_blk_mem_gen_i0.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "16" *) 
  (* C_READ_WIDTH_B = "16" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "READ_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "16" *) 
  (* C_WRITE_WIDTH_B = "16" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_family = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[15:0]),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[15:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "ip_peakdetector_c_addsub_v12_0_i0" *) 
(* X_CORE_INFO = "c_addsub_v12_0_13,Vivado 2019.1" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0
   (A,
    B,
    CLK,
    CE,
    S);
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [16:0]A;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [16:0]B;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [16:0]S;

  wire [16:0]A;
  wire [16:0]B;
  wire CE;
  wire CLK;
  wire [16:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "17" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "00000000000000000" *) 
  (* c_b_width = "17" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "17" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_peakdetector_0_0_c_addsub_v12_0_13 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "ip_peakdetector_c_addsub_v12_0_i0" *) 
(* X_CORE_INFO = "c_addsub_v12_0_13,Vivado 2019.1" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2
   (A,
    B,
    CLK,
    CE,
    S);
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [16:0]A;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [16:0]B;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [16:0]S;

  wire [16:0]A;
  wire [16:0]B;
  wire CE;
  wire CLK;
  wire [16:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "17" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "00000000000000000" *) 
  (* c_b_width = "17" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "17" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ip_peakdetector_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "ip_peakdetector_c_counter_binary_v12_0_i0" *) 
(* X_CORE_INFO = "c_counter_binary_v12_0_13,Vivado 2019.1" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0
   (CLK,
    CE,
    SINIT,
    LOAD,
    L,
    Q);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 sinit_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef" *) input SINIT;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [9:0]L;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [9:0]Q;

  wire CE;
  wire CLK;
  wire [9:0]L;
  wire LOAD;
  wire [9:0]Q;
  wire SINIT;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_delayline" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_delayline
   (douta,
    DI,
    S,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    signaldomain_clk,
    x,
    \op_mem_37_22_reg[0] ,
    o);
  output [15:0]douta;
  output [3:0]DI;
  output [3:0]S;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  input signaldomain_clk;
  input [15:0]x;
  input [9:0]\op_mem_37_22_reg[0] ;
  input [15:0]o;

  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [3:0]DI;
  wire [3:0]S;
  wire counter_n_10;
  wire [9:0]counter_op_net;
  wire [15:0]douta;
  wire [15:0]o;
  wire [9:0]\op_mem_37_22_reg[0] ;
  wire relational_op_net;
  wire signaldomain_clk;
  wire [15:0]x;

  design_1_ip_peakdetector_0_0_ip_peakdetector_xlcounter_free counter
       (.LOAD(relational_op_net),
        .Q(counter_op_net),
        .S(counter_n_10),
        .\op_mem_37_22_reg[0] (\op_mem_37_22_reg[0] [9]),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_sysgen_relational_3828661dd3 relational
       (.LOAD(relational_op_net),
        .Q(counter_op_net[8:0]),
        .S(counter_n_10),
        .\op_mem_37_22_reg[0]_0 (\op_mem_37_22_reg[0] [8:0]),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_ip_peakdetector_xlspram single_port_ram
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ),
        .DI(DI),
        .Q(counter_op_net),
        .S(S),
        .douta(douta),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_format_dac" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_format_dac
   (O,
    \xmax_i_12_24_reg[9] ,
    \xmax_i_12_24_reg[13] ,
    CO,
    \xmax_i_12_24_reg[15] ,
    peak_amp_dac,
    Q,
    S,
    d,
    signaldomain_clk);
  output [3:0]O;
  output [3:0]\xmax_i_12_24_reg[9] ;
  output [3:0]\xmax_i_12_24_reg[13] ;
  output [0:0]CO;
  output [1:0]\xmax_i_12_24_reg[15] ;
  output [13:0]peak_amp_dac;
  input [13:0]Q;
  input [0:0]S;
  input [13:0]d;
  input signaldomain_clk;

  wire [0:0]CO;
  wire [3:0]O;
  wire [13:0]Q;
  wire [0:0]S;
  wire [12:0]convert4_dout_net;
  wire [13:0]d;
  wire [13:13]fully_2_1_bit;
  wire [13:0]peak_amp_dac;
  wire signaldomain_clk;
  wire [3:0]\xmax_i_12_24_reg[13] ;
  wire [1:0]\xmax_i_12_24_reg[15] ;
  wire [3:0]\xmax_i_12_24_reg[9] ;

  design_1_ip_peakdetector_0_0_ip_peakdetector_xlconvert convert4
       (.CO(CO),
        .D({fully_2_1_bit,convert4_dout_net}),
        .O(O),
        .Q(Q),
        .S(S),
        .d(d),
        .signaldomain_clk(signaldomain_clk),
        .\xmax_i_12_24_reg[13] (\xmax_i_12_24_reg[13] ),
        .\xmax_i_12_24_reg[15] (\xmax_i_12_24_reg[15] ),
        .\xmax_i_12_24_reg[9] (\xmax_i_12_24_reg[9] ));
  design_1_ip_peakdetector_0_0_sysgen_logical_80d79f42b8 logical
       (.D({fully_2_1_bit,convert4_dout_net}),
        .peak_amp_dac(peak_amp_dac),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_math" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_math
   (S,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    DI,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    \x[14] ,
    \x[6] ,
    \x[14]_0 ,
    \x[6]_0 ,
    x,
    o,
    signaldomain_clk,
    douta);
  output [3:0]S;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  output [3:0]DI;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  output [3:0]\x[14] ;
  output [3:0]\x[6] ;
  output [3:0]\x[14]_0 ;
  output [3:0]\x[6]_0 ;
  input [15:0]x;
  input [15:0]o;
  input signaldomain_clk;
  input [15:0]douta;

  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [3:0]DI;
  wire [3:0]S;
  wire [15:0]douta;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;
  wire [3:0]\x[14]_0 ;
  wire [3:0]\x[6] ;
  wire [3:0]\x[6]_0 ;

  design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub__xdcDup__1 addsub
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ),
        .DI(DI),
        .S(S),
        .douta(douta),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
  design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub addsub1
       (.douta(douta),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ),
        .\x[14]_0 (\x[14]_0 ),
        .\x[6] (\x[6] ),
        .\x[6]_0 (\x[6]_0 ));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_peakdetector" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_peakdetector
   (Q,
    d,
    \xmax_i_12_24_reg[2] ,
    peak_amp_rdy,
    signaldomain_clk,
    x,
    o,
    DI,
    rel_61_16_carry__0,
    \state_15_23[0]_i_3 ,
    \state_15_23[0]_i_3_0 ,
    rel_35_16_carry__0,
    S,
    \state_15_23[1]_i_2 ,
    \state_15_23[1]_i_2_0 ,
    rel_55_30_carry__0,
    rel_55_30_carry__0_0,
    pkd_i_16_23_reg,
    pkd_i_16_23_reg_0,
    rel_55_16_carry__0,
    rel_55_16_carry__0_0,
    pkd_i_16_23_reg_1,
    pkd_i_16_23_reg_2,
    \op_mem_37_22_reg[0] ,
    q,
    CO,
    O,
    \reg_array[12].fde_used.u2 ,
    \reg_array[7].fde_used.u2 ,
    \reg_array[11].fde_used.u2 );
  output [15:0]Q;
  output [13:0]d;
  output [0:0]\xmax_i_12_24_reg[2] ;
  output [0:0]peak_amp_rdy;
  input signaldomain_clk;
  input [15:0]x;
  input [15:0]o;
  input [3:0]DI;
  input [3:0]rel_61_16_carry__0;
  input [3:0]\state_15_23[0]_i_3 ;
  input [3:0]\state_15_23[0]_i_3_0 ;
  input [3:0]rel_35_16_carry__0;
  input [3:0]S;
  input [3:0]\state_15_23[1]_i_2 ;
  input [3:0]\state_15_23[1]_i_2_0 ;
  input [3:0]rel_55_30_carry__0;
  input [3:0]rel_55_30_carry__0_0;
  input [3:0]pkd_i_16_23_reg;
  input [3:0]pkd_i_16_23_reg_0;
  input [3:0]rel_55_16_carry__0;
  input [3:0]rel_55_16_carry__0_0;
  input [3:0]pkd_i_16_23_reg_1;
  input [3:0]pkd_i_16_23_reg_2;
  input [9:0]\op_mem_37_22_reg[0] ;
  input [0:0]q;
  input [0:0]CO;
  input [3:0]O;
  input [1:0]\reg_array[12].fde_used.u2 ;
  input [3:0]\reg_array[7].fde_used.u2 ;
  input [3:0]\reg_array[11].fde_used.u2 ;

  wire [0:0]CO;
  wire [3:0]DI;
  wire [3:0]O;
  wire [15:0]Q;
  wire [3:0]S;
  wire [13:0]d;
  wire delayline_n_16;
  wire delayline_n_17;
  wire delayline_n_18;
  wire delayline_n_19;
  wire delayline_n_20;
  wire delayline_n_21;
  wire delayline_n_22;
  wire delayline_n_23;
  wire delayline_n_24;
  wire delayline_n_25;
  wire delayline_n_26;
  wire delayline_n_27;
  wire delayline_n_28;
  wire delayline_n_29;
  wire delayline_n_30;
  wire delayline_n_31;
  wire math_n_0;
  wire math_n_1;
  wire math_n_10;
  wire math_n_11;
  wire math_n_12;
  wire math_n_13;
  wire math_n_14;
  wire math_n_15;
  wire math_n_16;
  wire math_n_17;
  wire math_n_18;
  wire math_n_19;
  wire math_n_2;
  wire math_n_20;
  wire math_n_21;
  wire math_n_22;
  wire math_n_23;
  wire math_n_24;
  wire math_n_25;
  wire math_n_26;
  wire math_n_27;
  wire math_n_28;
  wire math_n_29;
  wire math_n_3;
  wire math_n_30;
  wire math_n_31;
  wire math_n_4;
  wire math_n_5;
  wire math_n_6;
  wire math_n_7;
  wire math_n_8;
  wire math_n_9;
  wire mcode_cmp_en_net;
  wire mcode_cmp_rst_net;
  wire mcode_pkd_net;
  wire [15:0]o;
  wire [9:0]\op_mem_37_22_reg[0] ;
  wire [0:0]peak_amp_rdy;
  wire [3:0]pkd_i_16_23_reg;
  wire [3:0]pkd_i_16_23_reg_0;
  wire [3:0]pkd_i_16_23_reg_1;
  wire [3:0]pkd_i_16_23_reg_2;
  wire [0:0]q;
  wire [3:0]\reg_array[11].fde_used.u2 ;
  wire [1:0]\reg_array[12].fde_used.u2 ;
  wire [3:0]\reg_array[7].fde_used.u2 ;
  wire [3:0]rel_35_16_carry__0;
  wire [3:0]rel_55_16_carry__0;
  wire [3:0]rel_55_16_carry__0_0;
  wire [3:0]rel_55_30_carry__0;
  wire [3:0]rel_55_30_carry__0_0;
  wire [3:0]rel_61_16_carry__0;
  wire risingedgetrigger_n_0;
  wire signaldomain_clk;
  wire [15:0]single_port_ram_data_out_net;
  wire [3:0]\state_15_23[0]_i_3 ;
  wire [3:0]\state_15_23[0]_i_3_0 ;
  wire [3:0]\state_15_23[1]_i_2 ;
  wire [3:0]\state_15_23[1]_i_2_0 ;
  wire [15:0]x;
  wire [0:0]\xmax_i_12_24_reg[2] ;

  design_1_ip_peakdetector_0_0_ip_peakdetector_delayline delayline
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ({delayline_n_24,delayline_n_25,delayline_n_26,delayline_n_27}),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ({delayline_n_28,delayline_n_29,delayline_n_30,delayline_n_31}),
        .DI({delayline_n_16,delayline_n_17,delayline_n_18,delayline_n_19}),
        .S({delayline_n_20,delayline_n_21,delayline_n_22,delayline_n_23}),
        .douta(single_port_ram_data_out_net),
        .o(o),
        .\op_mem_37_22_reg[0] (\op_mem_37_22_reg[0] ),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
  design_1_ip_peakdetector_0_0_ip_peakdetector_math math
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ({math_n_4,math_n_5,math_n_6,math_n_7}),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ({math_n_12,math_n_13,math_n_14,math_n_15}),
        .DI({math_n_8,math_n_9,math_n_10,math_n_11}),
        .S({math_n_0,math_n_1,math_n_2,math_n_3}),
        .douta(single_port_ram_data_out_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] ({math_n_16,math_n_17,math_n_18,math_n_19}),
        .\x[14]_0 ({math_n_24,math_n_25,math_n_26,math_n_27}),
        .\x[6] ({math_n_20,math_n_21,math_n_22,math_n_23}),
        .\x[6]_0 ({math_n_28,math_n_29,math_n_30,math_n_31}));
  design_1_ip_peakdetector_0_0_sysgen_mcode_block_a80b0cfdd5 mcode
       (.DI({math_n_8,math_n_9,math_n_10,math_n_11}),
        .S({math_n_0,math_n_1,math_n_2,math_n_3}),
        .SR(mcode_cmp_rst_net),
        .mcode_cmp_en_net(mcode_cmp_en_net),
        .mcode_pkd_net(mcode_pkd_net),
        .peak_amp_rdy(peak_amp_rdy),
        .peak_amp_rdy_0_sp_1(risingedgetrigger_n_0),
        .pkd_i_16_23_reg_0(pkd_i_16_23_reg),
        .pkd_i_16_23_reg_1(pkd_i_16_23_reg_0),
        .pkd_i_16_23_reg_2(pkd_i_16_23_reg_1),
        .pkd_i_16_23_reg_3(pkd_i_16_23_reg_2),
        .q(q),
        .rel_35_16_carry__0_0(rel_35_16_carry__0),
        .rel_35_16_carry__0_1(S),
        .rel_47_16_carry__0_0({math_n_28,math_n_29,math_n_30,math_n_31}),
        .rel_47_16_carry__0_1({math_n_20,math_n_21,math_n_22,math_n_23}),
        .rel_55_16_carry__0_0(rel_55_16_carry__0),
        .rel_55_16_carry__0_1(rel_55_16_carry__0_0),
        .rel_55_30_carry__0_0(rel_55_30_carry__0),
        .rel_55_30_carry__0_1(rel_55_30_carry__0_0),
        .rel_61_16_carry__0_0(DI),
        .rel_61_16_carry__0_1(rel_61_16_carry__0),
        .rel_61_30_carry__0_0({delayline_n_16,delayline_n_17,delayline_n_18,delayline_n_19}),
        .rel_61_30_carry__0_1({delayline_n_20,delayline_n_21,delayline_n_22,delayline_n_23}),
        .rel_63_21_carry__0_0({math_n_12,math_n_13,math_n_14,math_n_15}),
        .rel_63_21_carry__0_1({math_n_4,math_n_5,math_n_6,math_n_7}),
        .signaldomain_clk(signaldomain_clk),
        .\state_15_23[0]_i_3_0 ({delayline_n_24,delayline_n_25,delayline_n_26,delayline_n_27}),
        .\state_15_23[0]_i_3_1 ({delayline_n_28,delayline_n_29,delayline_n_30,delayline_n_31}),
        .\state_15_23[0]_i_3_2 (\state_15_23[0]_i_3 ),
        .\state_15_23[0]_i_3_3 (\state_15_23[0]_i_3_0 ),
        .\state_15_23[1]_i_2_0 (\state_15_23[1]_i_2 ),
        .\state_15_23[1]_i_2_1 (\state_15_23[1]_i_2_0 ),
        .\state_15_23[2]_i_2_0 ({math_n_24,math_n_25,math_n_26,math_n_27}),
        .\state_15_23[2]_i_2_1 ({math_n_16,math_n_17,math_n_18,math_n_19}));
  design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c3d4c1851 mcode1
       (.CO(CO),
        .O(O),
        .Q(Q),
        .SR(mcode_cmp_rst_net),
        .d(d),
        .mcode_cmp_en_net(mcode_cmp_en_net),
        .\reg_array[11].fde_used.u2 (\reg_array[11].fde_used.u2 ),
        .\reg_array[12].fde_used.u2 (\reg_array[12].fde_used.u2 ),
        .\reg_array[7].fde_used.u2 (\reg_array[7].fde_used.u2 ),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\xmax_i_12_24_reg[2]_0 (\xmax_i_12_24_reg[2] ));
  design_1_ip_peakdetector_0_0_ip_peakdetector_risingedgetrigger risingedgetrigger
       (.\ff_2_17_reg[0] (risingedgetrigger_n_0),
        .mcode_pkd_net(mcode_pkd_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_risingedgetrigger" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_risingedgetrigger
   (\ff_2_17_reg[0] ,
    mcode_pkd_net,
    signaldomain_clk);
  output \ff_2_17_reg[0] ;
  input mcode_pkd_net;
  input signaldomain_clk;

  wire \ff_2_17_reg[0] ;
  wire mcode_pkd_net;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c4ff59dc3 mcode
       (.\ff_2_17_reg[0]_0 (\ff_2_17_reg[0] ),
        .mcode_pkd_net(mcode_pkd_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_signaldomain" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_signaldomain
   (Q,
    peak_amp_rdy,
    peak_amp_dac,
    signaldomain_clk,
    x,
    o,
    DI,
    rel_61_16_carry__0,
    \state_15_23[0]_i_3 ,
    \state_15_23[0]_i_3_0 ,
    rel_35_16_carry__0,
    S,
    \state_15_23[1]_i_2 ,
    \state_15_23[1]_i_2_0 ,
    rel_55_30_carry__0,
    rel_55_30_carry__0_0,
    pkd_i_16_23_reg,
    pkd_i_16_23_reg_0,
    rel_55_16_carry__0,
    rel_55_16_carry__0_0,
    pkd_i_16_23_reg_1,
    pkd_i_16_23_reg_2,
    \op_mem_37_22_reg[0] ,
    q);
  output [15:0]Q;
  output [0:0]peak_amp_rdy;
  output [13:0]peak_amp_dac;
  input signaldomain_clk;
  input [15:0]x;
  input [15:0]o;
  input [3:0]DI;
  input [3:0]rel_61_16_carry__0;
  input [3:0]\state_15_23[0]_i_3 ;
  input [3:0]\state_15_23[0]_i_3_0 ;
  input [3:0]rel_35_16_carry__0;
  input [3:0]S;
  input [3:0]\state_15_23[1]_i_2 ;
  input [3:0]\state_15_23[1]_i_2_0 ;
  input [3:0]rel_55_30_carry__0;
  input [3:0]rel_55_30_carry__0_0;
  input [3:0]pkd_i_16_23_reg;
  input [3:0]pkd_i_16_23_reg_0;
  input [3:0]rel_55_16_carry__0;
  input [3:0]rel_55_16_carry__0_0;
  input [3:0]pkd_i_16_23_reg_1;
  input [3:0]pkd_i_16_23_reg_2;
  input [9:0]\op_mem_37_22_reg[0] ;
  input [0:0]q;

  wire [3:0]DI;
  wire [15:0]Q;
  wire [3:0]S;
  wire [13:0]\convert4/result ;
  wire format_dac_n_0;
  wire format_dac_n_1;
  wire format_dac_n_10;
  wire format_dac_n_11;
  wire format_dac_n_12;
  wire format_dac_n_13;
  wire format_dac_n_14;
  wire format_dac_n_2;
  wire format_dac_n_3;
  wire format_dac_n_4;
  wire format_dac_n_5;
  wire format_dac_n_6;
  wire format_dac_n_7;
  wire format_dac_n_8;
  wire format_dac_n_9;
  wire [15:0]o;
  wire [9:0]\op_mem_37_22_reg[0] ;
  wire [13:0]peak_amp_dac;
  wire [0:0]peak_amp_rdy;
  wire peakdetector_n_30;
  wire [3:0]pkd_i_16_23_reg;
  wire [3:0]pkd_i_16_23_reg_0;
  wire [3:0]pkd_i_16_23_reg_1;
  wire [3:0]pkd_i_16_23_reg_2;
  wire [0:0]q;
  wire [3:0]rel_35_16_carry__0;
  wire [3:0]rel_55_16_carry__0;
  wire [3:0]rel_55_16_carry__0_0;
  wire [3:0]rel_55_30_carry__0;
  wire [3:0]rel_55_30_carry__0_0;
  wire [3:0]rel_61_16_carry__0;
  wire signaldomain_clk;
  wire [3:0]\state_15_23[0]_i_3 ;
  wire [3:0]\state_15_23[0]_i_3_0 ;
  wire [3:0]\state_15_23[1]_i_2 ;
  wire [3:0]\state_15_23[1]_i_2_0 ;
  wire [15:0]x;

  design_1_ip_peakdetector_0_0_ip_peakdetector_format_dac format_dac
       (.CO(format_dac_n_12),
        .O({format_dac_n_0,format_dac_n_1,format_dac_n_2,format_dac_n_3}),
        .Q(Q[15:2]),
        .S(peakdetector_n_30),
        .d(\convert4/result ),
        .peak_amp_dac(peak_amp_dac),
        .signaldomain_clk(signaldomain_clk),
        .\xmax_i_12_24_reg[13] ({format_dac_n_8,format_dac_n_9,format_dac_n_10,format_dac_n_11}),
        .\xmax_i_12_24_reg[15] ({format_dac_n_13,format_dac_n_14}),
        .\xmax_i_12_24_reg[9] ({format_dac_n_4,format_dac_n_5,format_dac_n_6,format_dac_n_7}));
  design_1_ip_peakdetector_0_0_ip_peakdetector_peakdetector peakdetector
       (.CO(format_dac_n_12),
        .DI(DI),
        .O({format_dac_n_0,format_dac_n_1,format_dac_n_2,format_dac_n_3}),
        .Q(Q),
        .S(S),
        .d(\convert4/result ),
        .o(o),
        .\op_mem_37_22_reg[0] (\op_mem_37_22_reg[0] ),
        .peak_amp_rdy(peak_amp_rdy),
        .pkd_i_16_23_reg(pkd_i_16_23_reg),
        .pkd_i_16_23_reg_0(pkd_i_16_23_reg_0),
        .pkd_i_16_23_reg_1(pkd_i_16_23_reg_1),
        .pkd_i_16_23_reg_2(pkd_i_16_23_reg_2),
        .q(q),
        .\reg_array[11].fde_used.u2 ({format_dac_n_8,format_dac_n_9,format_dac_n_10,format_dac_n_11}),
        .\reg_array[12].fde_used.u2 ({format_dac_n_13,format_dac_n_14}),
        .\reg_array[7].fde_used.u2 ({format_dac_n_4,format_dac_n_5,format_dac_n_6,format_dac_n_7}),
        .rel_35_16_carry__0(rel_35_16_carry__0),
        .rel_55_16_carry__0(rel_55_16_carry__0),
        .rel_55_16_carry__0_0(rel_55_16_carry__0_0),
        .rel_55_30_carry__0(rel_55_30_carry__0),
        .rel_55_30_carry__0_0(rel_55_30_carry__0_0),
        .rel_61_16_carry__0(rel_61_16_carry__0),
        .signaldomain_clk(signaldomain_clk),
        .\state_15_23[0]_i_3 (\state_15_23[0]_i_3 ),
        .\state_15_23[0]_i_3_0 (\state_15_23[0]_i_3_0 ),
        .\state_15_23[1]_i_2 (\state_15_23[1]_i_2 ),
        .\state_15_23[1]_i_2_0 (\state_15_23[1]_i_2_0 ),
        .x(x),
        .\xmax_i_12_24_reg[2] (peakdetector_n_30));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_struct" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_struct
   (Q,
    peak_amp_dac,
    peak_amp_rdy,
    signaldomain_clk,
    x,
    r4_x_delay,
    axibusdomain_clk,
    r5_x_noise,
    r6_x_min,
    r7_x_max,
    r8_en_pkd);
  output [15:0]Q;
  output [13:0]peak_amp_dac;
  output [0:0]peak_amp_rdy;
  input signaldomain_clk;
  input [15:0]x;
  input [9:0]r4_x_delay;
  input axibusdomain_clk;
  input [15:0]r5_x_noise;
  input [15:0]r6_x_min;
  input [15:0]r7_x_max;
  input [0:0]r8_en_pkd;

  wire [15:0]Q;
  wire axibusdomain_clk;
  wire [13:0]peak_amp_dac;
  wire [0:0]peak_amp_rdy;
  wire [9:0]r4_x_delay;
  wire [15:0]r5_x_noise;
  wire [15:0]r6_x_min;
  wire [15:0]r7_x_max;
  wire [0:0]r8_en_pkd;
  wire register1_n_0;
  wire register1_n_1;
  wire register1_n_2;
  wire register1_n_20;
  wire register1_n_21;
  wire register1_n_22;
  wire register1_n_23;
  wire register1_n_24;
  wire register1_n_25;
  wire register1_n_26;
  wire register1_n_27;
  wire register1_n_28;
  wire register1_n_29;
  wire register1_n_3;
  wire register1_n_30;
  wire register1_n_31;
  wire register1_n_32;
  wire register1_n_33;
  wire register1_n_34;
  wire register1_n_35;
  wire register1_n_36;
  wire register1_n_37;
  wire register1_n_38;
  wire register1_n_39;
  wire register1_n_40;
  wire register1_n_41;
  wire register1_n_42;
  wire register1_n_43;
  wire register1_n_44;
  wire register1_n_45;
  wire register1_n_46;
  wire register1_n_47;
  wire [15:0]register1_q_net;
  wire register2_n_0;
  wire register2_n_1;
  wire register2_n_10;
  wire register2_n_11;
  wire register2_n_12;
  wire register2_n_13;
  wire register2_n_14;
  wire register2_n_15;
  wire register2_n_2;
  wire register2_n_3;
  wire register2_n_4;
  wire register2_n_5;
  wire register2_n_6;
  wire register2_n_7;
  wire register2_n_8;
  wire register2_n_9;
  wire register3_n_0;
  wire register3_n_1;
  wire register3_n_10;
  wire register3_n_11;
  wire register3_n_12;
  wire register3_n_13;
  wire register3_n_14;
  wire register3_n_15;
  wire register3_n_2;
  wire register3_n_3;
  wire register3_n_4;
  wire register3_n_5;
  wire register3_n_6;
  wire register3_n_7;
  wire register3_n_8;
  wire register3_n_9;
  wire [0:0]register4_q_net;
  wire [9:0]register_q_net;
  wire signaldomain_clk;
  wire [15:0]x;

  design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister register1
       (.DI({register1_n_20,register1_n_21,register1_n_22,register1_n_23}),
        .S({register1_n_0,register1_n_1,register1_n_2,register1_n_3}),
        .axibusdomain_clk(axibusdomain_clk),
        .\fd_prim_array[14].bit_is_0.fdre_comp ({register1_n_24,register1_n_25,register1_n_26,register1_n_27}),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 ({register1_n_32,register1_n_33,register1_n_34,register1_n_35}),
        .\fd_prim_array[14].bit_is_0.fdre_comp_1 ({register1_n_44,register1_n_45,register1_n_46,register1_n_47}),
        .\fd_prim_array[6].bit_is_0.fdre_comp ({register1_n_36,register1_n_37,register1_n_38,register1_n_39}),
        .\fd_prim_array[6].bit_is_0.fdre_comp_0 ({register1_n_40,register1_n_41,register1_n_42,register1_n_43}),
        .o(register1_q_net),
        .r5_x_noise(r5_x_noise),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] ({register1_n_28,register1_n_29,register1_n_30,register1_n_31}));
  design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_0 register2
       (.DI({register2_n_8,register2_n_9,register2_n_10,register2_n_11}),
        .S({register2_n_0,register2_n_1,register2_n_2,register2_n_3}),
        .axibusdomain_clk(axibusdomain_clk),
        .\fd_prim_array[14].bit_is_0.fdre_comp ({register2_n_4,register2_n_5,register2_n_6,register2_n_7}),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 ({register2_n_12,register2_n_13,register2_n_14,register2_n_15}),
        .r6_x_min(r6_x_min),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
  design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_1 register3
       (.DI({register3_n_8,register3_n_9,register3_n_10,register3_n_11}),
        .S({register3_n_0,register3_n_1,register3_n_2,register3_n_3}),
        .axibusdomain_clk(axibusdomain_clk),
        .\fd_prim_array[14].bit_is_0.fdre_comp ({register3_n_4,register3_n_5,register3_n_6,register3_n_7}),
        .r7_x_max(r7_x_max),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] ({register3_n_12,register3_n_13,register3_n_14,register3_n_15}));
  design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_2 register4
       (.axibusdomain_clk(axibusdomain_clk),
        .q(register4_q_net),
        .r8_en_pkd(r8_en_pkd),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_3 register_x0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(register_q_net),
        .r4_x_delay(r4_x_delay),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_ip_peakdetector_signaldomain signaldomain
       (.DI({register1_n_20,register1_n_21,register1_n_22,register1_n_23}),
        .Q(Q),
        .S({register1_n_0,register1_n_1,register1_n_2,register1_n_3}),
        .o(register1_q_net),
        .\op_mem_37_22_reg[0] (register_q_net),
        .peak_amp_dac(peak_amp_dac),
        .peak_amp_rdy(peak_amp_rdy),
        .pkd_i_16_23_reg({register3_n_12,register3_n_13,register3_n_14,register3_n_15}),
        .pkd_i_16_23_reg_0({register3_n_4,register3_n_5,register3_n_6,register3_n_7}),
        .pkd_i_16_23_reg_1({register2_n_12,register2_n_13,register2_n_14,register2_n_15}),
        .pkd_i_16_23_reg_2({register2_n_4,register2_n_5,register2_n_6,register2_n_7}),
        .q(register4_q_net),
        .rel_35_16_carry__0({register1_n_36,register1_n_37,register1_n_38,register1_n_39}),
        .rel_55_16_carry__0({register2_n_8,register2_n_9,register2_n_10,register2_n_11}),
        .rel_55_16_carry__0_0({register2_n_0,register2_n_1,register2_n_2,register2_n_3}),
        .rel_55_30_carry__0({register3_n_8,register3_n_9,register3_n_10,register3_n_11}),
        .rel_55_30_carry__0_0({register3_n_0,register3_n_1,register3_n_2,register3_n_3}),
        .rel_61_16_carry__0({register1_n_40,register1_n_41,register1_n_42,register1_n_43}),
        .signaldomain_clk(signaldomain_clk),
        .\state_15_23[0]_i_3 ({register1_n_28,register1_n_29,register1_n_30,register1_n_31}),
        .\state_15_23[0]_i_3_0 ({register1_n_44,register1_n_45,register1_n_46,register1_n_47}),
        .\state_15_23[1]_i_2 ({register1_n_32,register1_n_33,register1_n_34,register1_n_35}),
        .\state_15_23[1]_i_2_0 ({register1_n_24,register1_n_25,register1_n_26,register1_n_27}),
        .x(x));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister
   (S,
    o,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    \x[14] ,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[6].bit_is_0.fdre_comp ,
    \fd_prim_array[6].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[14].bit_is_0.fdre_comp_1 ,
    x,
    r5_x_noise,
    axibusdomain_clk,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]\x[14] ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  input [15:0]x;
  input [15:0]r5_x_noise;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire axibusdomain_clk;
  wire [15:0]d1_net;
  wire [15:0]d2_net;
  wire [15:0]d3_net;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  wire [15:0]o;
  wire [15:0]r5_x_noise;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_38 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r5_x_noise(r5_x_noise));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_39 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41 synth_reg_inst_3
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp_0 ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_1 (\fd_prim_array[14].bit_is_0.fdre_comp_1 ),
        .\fd_prim_array[6].bit_is_0.fdre_comp (\fd_prim_array[6].bit_is_0.fdre_comp ),
        .\fd_prim_array[6].bit_is_0.fdre_comp_0 (\fd_prim_array[6].bit_is_0.fdre_comp_0 ),
        .i(d3_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_0
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    x,
    r6_x_min,
    axibusdomain_clk,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  input [15:0]x;
  input [15:0]r6_x_min;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire axibusdomain_clk;
  wire [15:0]d1_net;
  wire [15:0]d2_net;
  wire [15:0]d3_net;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [15:0]r6_x_min;
  wire signaldomain_clk;
  wire [15:0]x;

  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_30 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r6_x_min(r6_x_min));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_31 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_32 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_33 synth_reg_inst_3
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp_0 ),
        .i(d3_net),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_1
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    DI,
    \x[14] ,
    x,
    r7_x_max,
    axibusdomain_clk,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]DI;
  output [3:0]\x[14] ;
  input [15:0]x;
  input [15:0]r7_x_max;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire axibusdomain_clk;
  wire [15:0]d1_net;
  wire [15:0]d2_net;
  wire [15:0]d3_net;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [15:0]r7_x_max;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_22 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r7_x_max(r7_x_max));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_23 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_24 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_25 synth_reg_inst_3
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .i(d3_net),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_2
   (q,
    r8_en_pkd,
    axibusdomain_clk,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]r8_en_pkd;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire [0:0]q;
  wire [0:0]r8_en_pkd;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_14 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r8_en_pkd(r8_en_pkd));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_15 synth_reg_inst_1
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_16 synth_reg_inst_2
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_17 synth_reg_inst_3
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlAsynRegister" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xlAsynRegister_3
   (o,
    r4_x_delay,
    axibusdomain_clk,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]r4_x_delay;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [9:0]d1_net;
  wire [9:0]d2_net;
  wire [9:0]d3_net;
  wire [9:0]o;
  wire [9:0]r4_x_delay;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r4_x_delay(r4_x_delay));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_8 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_9 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_10 synth_reg_inst_3
       (.i(d3_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xladdsub" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub
   (\x[14] ,
    \x[6] ,
    \x[14]_0 ,
    \x[6]_0 ,
    douta,
    o,
    signaldomain_clk,
    x);
  output [3:0]\x[14] ;
  output [3:0]\x[6] ;
  output [3:0]\x[14]_0 ;
  output [3:0]\x[6]_0 ;
  input [15:0]douta;
  input [15:0]o;
  input signaldomain_clk;
  input [15:0]x;

  wire [16:0]core_s;
  wire [15:0]douta;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;
  wire [3:0]\x[14]_0 ;
  wire [3:0]\x[6] ;
  wire [3:0]\x[6]_0 ;

  (* CHECK_LICENSE_TYPE = "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "c_addsub_v12_0_13,Vivado 2019.1" *) 
  design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0 \comp0.core_instance0 
       (.A({douta[15],douta}),
        .B({o[15],o}),
        .CE(1'b1),
        .CLK(signaldomain_clk),
        .S(core_s));
  LUT5 #(
    .INIT(32'h4000DDDC)) 
    rel_47_16_carry__0_i_1
       (.I0(x[14]),
        .I1(x[15]),
        .I2(core_s[14]),
        .I3(core_s[15]),
        .I4(core_s[16]),
        .O(\x[14]_0 [3]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry__0_i_2
       (.I0(x[12]),
        .I1(x[13]),
        .I2(core_s[13]),
        .I3(core_s[12]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[14]_0 [2]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry__0_i_3
       (.I0(x[10]),
        .I1(x[11]),
        .I2(core_s[11]),
        .I3(core_s[10]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[14]_0 [1]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry__0_i_4
       (.I0(x[8]),
        .I1(x[9]),
        .I2(core_s[9]),
        .I3(core_s[8]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[14]_0 [0]));
  LUT5 #(
    .INIT(32'h82424241)) 
    rel_47_16_carry__0_i_5
       (.I0(x[14]),
        .I1(x[15]),
        .I2(core_s[16]),
        .I3(core_s[15]),
        .I4(core_s[14]),
        .O(\x[14] [3]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry__0_i_6
       (.I0(x[12]),
        .I1(x[13]),
        .I2(core_s[13]),
        .I3(core_s[12]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[14] [2]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry__0_i_7
       (.I0(x[10]),
        .I1(x[11]),
        .I2(core_s[11]),
        .I3(core_s[10]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[14] [1]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry__0_i_8
       (.I0(x[8]),
        .I1(x[9]),
        .I2(core_s[9]),
        .I3(core_s[8]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[14] [0]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry_i_1
       (.I0(x[6]),
        .I1(x[7]),
        .I2(core_s[7]),
        .I3(core_s[6]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[6]_0 [3]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry_i_2
       (.I0(x[4]),
        .I1(x[5]),
        .I2(core_s[5]),
        .I3(core_s[4]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[6]_0 [2]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry_i_3
       (.I0(x[2]),
        .I1(x[3]),
        .I2(core_s[3]),
        .I3(core_s[2]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[6]_0 [1]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_47_16_carry_i_4
       (.I0(x[0]),
        .I1(x[1]),
        .I2(core_s[1]),
        .I3(core_s[0]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(\x[6]_0 [0]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry_i_5
       (.I0(x[6]),
        .I1(x[7]),
        .I2(core_s[7]),
        .I3(core_s[6]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[6] [3]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry_i_6
       (.I0(x[4]),
        .I1(x[5]),
        .I2(core_s[5]),
        .I3(core_s[4]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[6] [2]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry_i_7
       (.I0(x[2]),
        .I1(x[3]),
        .I2(core_s[3]),
        .I3(core_s[2]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[6] [1]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_47_16_carry_i_8
       (.I0(x[0]),
        .I1(x[1]),
        .I2(core_s[1]),
        .I3(core_s[0]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(\x[6] [0]));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xladdsub" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xladdsub__xdcDup__1
   (S,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    DI,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    x,
    o,
    signaldomain_clk,
    douta);
  output [3:0]S;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  output [3:0]DI;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  input [15:0]x;
  input [15:0]o;
  input signaldomain_clk;
  input [15:0]douta;

  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [3:0]DI;
  wire [3:0]S;
  wire [16:0]core_s;
  wire [15:0]douta;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;

  (* CHECK_LICENSE_TYPE = "ip_peakdetector_c_addsub_v12_0_i0,c_addsub_v12_0_13,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "c_addsub_v12_0_13,Vivado 2019.1" *) 
  design_1_ip_peakdetector_0_0_ip_peakdetector_c_addsub_v12_0_i0__2 \comp0.core_instance0 
       (.A({x[15],x}),
        .B({o[15],o}),
        .CE(1'b1),
        .CLK(signaldomain_clk),
        .S(core_s));
  LUT5 #(
    .INIT(32'h73331110)) 
    rel_63_21_carry__0_i_1
       (.I0(douta[14]),
        .I1(core_s[16]),
        .I2(core_s[14]),
        .I3(core_s[15]),
        .I4(douta[15]),
        .O(DI[3]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_63_21_carry__0_i_2
       (.I0(douta[12]),
        .I1(douta[13]),
        .I2(core_s[13]),
        .I3(core_s[12]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(DI[2]));
  LUT6 #(
    .INIT(64'h7130000077777130)) 
    rel_63_21_carry__0_i_3
       (.I0(douta[10]),
        .I1(douta[11]),
        .I2(core_s[11]),
        .I3(core_s[10]),
        .I4(core_s[15]),
        .I5(core_s[16]),
        .O(DI[1]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry__0_i_4
       (.I0(douta[8]),
        .I1(douta[9]),
        .I2(core_s[9]),
        .I3(core_s[16]),
        .I4(core_s[8]),
        .I5(core_s[15]),
        .O(DI[0]));
  LUT5 #(
    .INIT(32'h84442221)) 
    rel_63_21_carry__0_i_5
       (.I0(douta[14]),
        .I1(core_s[16]),
        .I2(core_s[15]),
        .I3(core_s[14]),
        .I4(douta[15]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_63_21_carry__0_i_6
       (.I0(douta[12]),
        .I1(douta[13]),
        .I2(core_s[13]),
        .I3(core_s[12]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h8241888811118241)) 
    rel_63_21_carry__0_i_7
       (.I0(douta[10]),
        .I1(douta[11]),
        .I2(core_s[11]),
        .I3(core_s[10]),
        .I4(core_s[16]),
        .I5(core_s[15]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry__0_i_8
       (.I0(douta[8]),
        .I1(douta[9]),
        .I2(core_s[9]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[8]),
        .O(S[0]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry_i_1
       (.I0(douta[6]),
        .I1(douta[7]),
        .I2(core_s[7]),
        .I3(core_s[16]),
        .I4(core_s[6]),
        .I5(core_s[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [3]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry_i_2
       (.I0(douta[4]),
        .I1(douta[5]),
        .I2(core_s[5]),
        .I3(core_s[16]),
        .I4(core_s[4]),
        .I5(core_s[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [2]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry_i_3
       (.I0(douta[2]),
        .I1(douta[3]),
        .I2(core_s[3]),
        .I3(core_s[16]),
        .I4(core_s[2]),
        .I5(core_s[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [1]));
  LUT6 #(
    .INIT(64'h7177307700710030)) 
    rel_63_21_carry_i_4
       (.I0(douta[0]),
        .I1(douta[1]),
        .I2(core_s[1]),
        .I3(core_s[16]),
        .I4(core_s[0]),
        .I5(core_s[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [0]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry_i_5
       (.I0(douta[6]),
        .I1(douta[7]),
        .I2(core_s[7]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[6]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [3]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry_i_6
       (.I0(douta[4]),
        .I1(douta[5]),
        .I2(core_s[5]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[4]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [2]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry_i_7
       (.I0(douta[2]),
        .I1(douta[3]),
        .I2(core_s[3]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[2]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [1]));
  LUT6 #(
    .INIT(64'h8288118241881141)) 
    rel_63_21_carry_i_8
       (.I0(douta[0]),
        .I1(douta[1]),
        .I2(core_s[1]),
        .I3(core_s[16]),
        .I4(core_s[15]),
        .I5(core_s[0]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [0]));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlconvert" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xlconvert
   (O,
    \xmax_i_12_24_reg[9] ,
    \xmax_i_12_24_reg[13] ,
    CO,
    \xmax_i_12_24_reg[15] ,
    D,
    Q,
    S,
    d,
    signaldomain_clk);
  output [3:0]O;
  output [3:0]\xmax_i_12_24_reg[9] ;
  output [3:0]\xmax_i_12_24_reg[13] ;
  output [0:0]CO;
  output [1:0]\xmax_i_12_24_reg[15] ;
  output [13:0]D;
  input [13:0]Q;
  input [0:0]S;
  input [13:0]d;
  input signaldomain_clk;

  wire [0:0]CO;
  wire [13:0]D;
  wire [3:0]O;
  wire [13:0]Q;
  wire [0:0]S;
  wire [13:0]d;
  wire signaldomain_clk;
  wire [3:0]\xmax_i_12_24_reg[13] ;
  wire [1:0]\xmax_i_12_24_reg[15] ;
  wire [3:0]\xmax_i_12_24_reg[9] ;

  design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg \latency_test.reg 
       (.D(D),
        .d(d),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_peakdetector_0_0_convert_func_call_ip_peakdetector_xlconvert \std_conversion_generate.convert 
       (.CO(CO),
        .O(O),
        .Q(Q),
        .S(S),
        .\xmax_i_12_24_reg[13] (\xmax_i_12_24_reg[13] ),
        .\xmax_i_12_24_reg[15] (\xmax_i_12_24_reg[15] ),
        .\xmax_i_12_24_reg[9] (\xmax_i_12_24_reg[9] ));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlcounter_free" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xlcounter_free
   (Q,
    S,
    signaldomain_clk,
    LOAD,
    \op_mem_37_22_reg[0] );
  output [9:0]Q;
  output [0:0]S;
  input signaldomain_clk;
  input LOAD;
  input [0:0]\op_mem_37_22_reg[0] ;

  wire LOAD;
  wire [9:0]Q;
  wire [0:0]S;
  wire [0:0]\op_mem_37_22_reg[0] ;
  wire signaldomain_clk;

  (* CHECK_LICENSE_TYPE = "ip_peakdetector_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "c_counter_binary_v12_0_13,Vivado 2019.1" *) 
  design_1_ip_peakdetector_0_0_ip_peakdetector_c_counter_binary_v12_0_i0 \comp0.core_instance0 
       (.CE(1'b1),
        .CLK(signaldomain_clk),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(LOAD),
        .Q(Q),
        .SINIT(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    \op_mem_37_22[0]_i_2 
       (.I0(Q[9]),
        .I1(\op_mem_37_22_reg[0] ),
        .O(S));
endmodule

(* ORIG_REF_NAME = "ip_peakdetector_xlspram" *) 
module design_1_ip_peakdetector_0_0_ip_peakdetector_xlspram
   (douta,
    DI,
    S,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    signaldomain_clk,
    Q,
    x,
    o);
  output [15:0]douta;
  output [3:0]DI;
  output [3:0]S;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  output [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  input signaldomain_clk;
  input [9:0]Q;
  input [15:0]x;
  input [15:0]o;

  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [3:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [3:0]DI;
  wire [9:0]Q;
  wire [3:0]S;
  wire [15:0]douta;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;

  (* CHECK_LICENSE_TYPE = "ip_peakdetector_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
  design_1_ip_peakdetector_0_0_ip_peakdetector_blk_mem_gen_i0 \comp0.core_instance0 
       (.addra(Q),
        .clka(signaldomain_clk),
        .dina(x),
        .douta(douta),
        .ena(1'b1),
        .wea(1'b1));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry__0_i_1
       (.I0(douta[14]),
        .I1(o[14]),
        .I2(douta[15]),
        .I3(o[15]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry__0_i_2
       (.I0(douta[12]),
        .I1(o[12]),
        .I2(o[13]),
        .I3(douta[13]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry__0_i_3
       (.I0(douta[10]),
        .I1(o[10]),
        .I2(o[11]),
        .I3(douta[11]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry__0_i_4
       (.I0(douta[8]),
        .I1(o[8]),
        .I2(o[9]),
        .I3(douta[9]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry__0_i_5
       (.I0(douta[14]),
        .I1(douta[15]),
        .I2(o[15]),
        .I3(o[14]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry__0_i_6
       (.I0(douta[12]),
        .I1(o[13]),
        .I2(douta[13]),
        .I3(o[12]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry__0_i_7
       (.I0(douta[10]),
        .I1(o[11]),
        .I2(douta[11]),
        .I3(o[10]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry__0_i_8
       (.I0(douta[8]),
        .I1(o[9]),
        .I2(douta[9]),
        .I3(o[8]),
        .O(\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry_i_1
       (.I0(douta[6]),
        .I1(o[6]),
        .I2(o[7]),
        .I3(douta[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry_i_2
       (.I0(douta[4]),
        .I1(o[4]),
        .I2(o[5]),
        .I3(douta[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry_i_3
       (.I0(douta[2]),
        .I1(o[2]),
        .I2(o[3]),
        .I3(douta[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_30_carry_i_4
       (.I0(douta[0]),
        .I1(o[0]),
        .I2(o[1]),
        .I3(douta[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry_i_5
       (.I0(douta[6]),
        .I1(o[7]),
        .I2(douta[7]),
        .I3(o[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry_i_6
       (.I0(douta[4]),
        .I1(o[5]),
        .I2(douta[5]),
        .I3(o[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry_i_7
       (.I0(douta[2]),
        .I1(o[3]),
        .I2(douta[3]),
        .I3(o[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_30_carry_i_8
       (.I0(douta[0]),
        .I1(o[1]),
        .I2(douta[1]),
        .I3(o[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "sysgen_logical_80d79f42b8" *) 
module design_1_ip_peakdetector_0_0_sysgen_logical_80d79f42b8
   (peak_amp_dac,
    D,
    signaldomain_clk);
  output [13:0]peak_amp_dac;
  input [13:0]D;
  input signaldomain_clk;

  wire [13:0]D;
  wire [13:0]peak_amp_dac;
  wire signaldomain_clk;

  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[0]),
        .Q(peak_amp_dac[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][10] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[10]),
        .Q(peak_amp_dac[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][11] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[11]),
        .Q(peak_amp_dac[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][12] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[12]),
        .Q(peak_amp_dac[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][13] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[13]),
        .Q(peak_amp_dac[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][1] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[1]),
        .Q(peak_amp_dac[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][2] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[2]),
        .Q(peak_amp_dac[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][3] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[3]),
        .Q(peak_amp_dac[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][4] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[4]),
        .Q(peak_amp_dac[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][5] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[5]),
        .Q(peak_amp_dac[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][6] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[6]),
        .Q(peak_amp_dac[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][7] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[7]),
        .Q(peak_amp_dac[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][8] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[8]),
        .Q(peak_amp_dac[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latency_pipe_5_26_reg[0][9] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(D[9]),
        .Q(peak_amp_dac[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "sysgen_mcode_block_0c3d4c1851" *) 
module design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c3d4c1851
   (Q,
    d,
    \xmax_i_12_24_reg[2]_0 ,
    mcode_cmp_en_net,
    x,
    CO,
    O,
    \reg_array[12].fde_used.u2 ,
    \reg_array[7].fde_used.u2 ,
    \reg_array[11].fde_used.u2 ,
    SR,
    signaldomain_clk);
  output [15:0]Q;
  output [13:0]d;
  output [0:0]\xmax_i_12_24_reg[2]_0 ;
  input mcode_cmp_en_net;
  input [15:0]x;
  input [0:0]CO;
  input [3:0]O;
  input [1:0]\reg_array[12].fde_used.u2 ;
  input [3:0]\reg_array[7].fde_used.u2 ;
  input [3:0]\reg_array[11].fde_used.u2 ;
  input [0:0]SR;
  input signaldomain_clk;

  wire [0:0]CO;
  wire [3:0]O;
  wire [15:0]Q;
  wire [0:0]SR;
  wire [13:0]d;
  wire mcode_cmp_en_net;
  wire [3:0]\reg_array[11].fde_used.u2 ;
  wire [1:0]\reg_array[12].fde_used.u2 ;
  wire [3:0]\reg_array[7].fde_used.u2 ;
  wire rel_19_28;
  wire rel_19_28_carry__0_i_1_n_0;
  wire rel_19_28_carry__0_i_2_n_0;
  wire rel_19_28_carry__0_i_3_n_0;
  wire rel_19_28_carry__0_i_4_n_0;
  wire rel_19_28_carry__0_i_5_n_0;
  wire rel_19_28_carry__0_i_6_n_0;
  wire rel_19_28_carry__0_i_7_n_0;
  wire rel_19_28_carry__0_i_8_n_0;
  wire rel_19_28_carry__0_n_1;
  wire rel_19_28_carry__0_n_2;
  wire rel_19_28_carry__0_n_3;
  wire rel_19_28_carry_i_1_n_0;
  wire rel_19_28_carry_i_2_n_0;
  wire rel_19_28_carry_i_3_n_0;
  wire rel_19_28_carry_i_4_n_0;
  wire rel_19_28_carry_i_5_n_0;
  wire rel_19_28_carry_i_6_n_0;
  wire rel_19_28_carry_i_7_n_0;
  wire rel_19_28_carry_i_8_n_0;
  wire rel_19_28_carry_n_0;
  wire rel_19_28_carry_n_1;
  wire rel_19_28_carry_n_2;
  wire rel_19_28_carry_n_3;
  wire signaldomain_clk;
  wire [15:0]x;
  wire xmax_i_12_2400_out;
  wire [0:0]\xmax_i_12_24_reg[2]_0 ;
  wire [3:0]NLW_rel_19_28_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_19_28_carry__0_O_UNCONNECTED;

  LUT4 #(
    .INIT(16'h5A9A)) 
    inp_carry_i_1
       (.I0(Q[2]),
        .I1(Q[15]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\xmax_i_12_24_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[0].fde_used.u2_i_1 
       (.I0(O[0]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[0]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[10].fde_used.u2_i_1 
       (.I0(\reg_array[11].fde_used.u2 [2]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[10]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[11].fde_used.u2_i_1 
       (.I0(\reg_array[11].fde_used.u2 [3]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[11]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[12].fde_used.u2_i_1 
       (.I0(\reg_array[12].fde_used.u2 [0]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[12]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \reg_array[13].fde_used.u2_i_1 
       (.I0(CO),
        .O(d[13]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[1].fde_used.u2_i_1 
       (.I0(O[1]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[1]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[2].fde_used.u2_i_1 
       (.I0(O[2]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[2]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[3].fde_used.u2_i_1 
       (.I0(O[3]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[3]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[4].fde_used.u2_i_1 
       (.I0(\reg_array[7].fde_used.u2 [0]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[4]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[5].fde_used.u2_i_1 
       (.I0(\reg_array[7].fde_used.u2 [1]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[5]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[6].fde_used.u2_i_1 
       (.I0(\reg_array[7].fde_used.u2 [2]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[6]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[7].fde_used.u2_i_1 
       (.I0(\reg_array[7].fde_used.u2 [3]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[7]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[8].fde_used.u2_i_1 
       (.I0(\reg_array[11].fde_used.u2 [0]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[8]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \reg_array[9].fde_used.u2_i_1 
       (.I0(\reg_array[11].fde_used.u2 [1]),
        .I1(CO),
        .I2(\reg_array[12].fde_used.u2 [1]),
        .O(d[9]));
  CARRY4 rel_19_28_carry
       (.CI(1'b0),
        .CO({rel_19_28_carry_n_0,rel_19_28_carry_n_1,rel_19_28_carry_n_2,rel_19_28_carry_n_3}),
        .CYINIT(1'b0),
        .DI({rel_19_28_carry_i_1_n_0,rel_19_28_carry_i_2_n_0,rel_19_28_carry_i_3_n_0,rel_19_28_carry_i_4_n_0}),
        .O(NLW_rel_19_28_carry_O_UNCONNECTED[3:0]),
        .S({rel_19_28_carry_i_5_n_0,rel_19_28_carry_i_6_n_0,rel_19_28_carry_i_7_n_0,rel_19_28_carry_i_8_n_0}));
  CARRY4 rel_19_28_carry__0
       (.CI(rel_19_28_carry_n_0),
        .CO({rel_19_28,rel_19_28_carry__0_n_1,rel_19_28_carry__0_n_2,rel_19_28_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({rel_19_28_carry__0_i_1_n_0,rel_19_28_carry__0_i_2_n_0,rel_19_28_carry__0_i_3_n_0,rel_19_28_carry__0_i_4_n_0}),
        .O(NLW_rel_19_28_carry__0_O_UNCONNECTED[3:0]),
        .S({rel_19_28_carry__0_i_5_n_0,rel_19_28_carry__0_i_6_n_0,rel_19_28_carry__0_i_7_n_0,rel_19_28_carry__0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry__0_i_1
       (.I0(Q[14]),
        .I1(x[14]),
        .I2(Q[15]),
        .I3(x[15]),
        .O(rel_19_28_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry__0_i_2
       (.I0(Q[12]),
        .I1(x[12]),
        .I2(x[13]),
        .I3(Q[13]),
        .O(rel_19_28_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry__0_i_3
       (.I0(Q[10]),
        .I1(x[10]),
        .I2(x[11]),
        .I3(Q[11]),
        .O(rel_19_28_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry__0_i_4
       (.I0(Q[8]),
        .I1(x[8]),
        .I2(x[9]),
        .I3(Q[9]),
        .O(rel_19_28_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h8421)) 
    rel_19_28_carry__0_i_5
       (.I0(x[14]),
        .I1(x[15]),
        .I2(Q[14]),
        .I3(Q[15]),
        .O(rel_19_28_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry__0_i_6
       (.I0(x[12]),
        .I1(x[13]),
        .I2(Q[13]),
        .I3(Q[12]),
        .O(rel_19_28_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry__0_i_7
       (.I0(x[10]),
        .I1(x[11]),
        .I2(Q[11]),
        .I3(Q[10]),
        .O(rel_19_28_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry__0_i_8
       (.I0(x[8]),
        .I1(x[9]),
        .I2(Q[9]),
        .I3(Q[8]),
        .O(rel_19_28_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry_i_1
       (.I0(Q[6]),
        .I1(x[6]),
        .I2(x[7]),
        .I3(Q[7]),
        .O(rel_19_28_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry_i_2
       (.I0(Q[4]),
        .I1(x[4]),
        .I2(x[5]),
        .I3(Q[5]),
        .O(rel_19_28_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry_i_3
       (.I0(Q[2]),
        .I1(x[2]),
        .I2(x[3]),
        .I3(Q[3]),
        .O(rel_19_28_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_19_28_carry_i_4
       (.I0(Q[0]),
        .I1(x[0]),
        .I2(x[1]),
        .I3(Q[1]),
        .O(rel_19_28_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry_i_5
       (.I0(x[6]),
        .I1(x[7]),
        .I2(Q[7]),
        .I3(Q[6]),
        .O(rel_19_28_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry_i_6
       (.I0(x[4]),
        .I1(x[5]),
        .I2(Q[5]),
        .I3(Q[4]),
        .O(rel_19_28_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry_i_7
       (.I0(x[2]),
        .I1(x[3]),
        .I2(Q[3]),
        .I3(Q[2]),
        .O(rel_19_28_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_19_28_carry_i_8
       (.I0(x[0]),
        .I1(Q[1]),
        .I2(x[1]),
        .I3(Q[0]),
        .O(rel_19_28_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    \xmax_i_12_24[15]_i_1 
       (.I0(rel_19_28),
        .I1(mcode_cmp_en_net),
        .O(xmax_i_12_2400_out));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[0] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[10] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[11] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[12] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[13] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[14] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[15] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[1] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[2] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[3] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[4] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[5] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[6] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[7] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[8] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \xmax_i_12_24_reg[9] 
       (.C(signaldomain_clk),
        .CE(xmax_i_12_2400_out),
        .D(x[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "sysgen_mcode_block_0c4ff59dc3" *) 
module design_1_ip_peakdetector_0_0_sysgen_mcode_block_0c4ff59dc3
   (\ff_2_17_reg[0]_0 ,
    mcode_pkd_net,
    signaldomain_clk);
  output \ff_2_17_reg[0]_0 ;
  input mcode_pkd_net;
  input signaldomain_clk;

  wire \ff_2_17_reg[0]_0 ;
  wire mcode_pkd_net;
  wire signaldomain_clk;

  FDRE #(
    .INIT(1'b0)) 
    \ff_2_17_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(mcode_pkd_net),
        .Q(\ff_2_17_reg[0]_0 ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "sysgen_mcode_block_a80b0cfdd5" *) 
module design_1_ip_peakdetector_0_0_sysgen_mcode_block_a80b0cfdd5
   (mcode_pkd_net,
    mcode_cmp_en_net,
    SR,
    peak_amp_rdy,
    rel_63_21_carry__0_0,
    rel_63_21_carry__0_1,
    DI,
    S,
    rel_61_30_carry__0_0,
    rel_61_30_carry__0_1,
    \state_15_23[0]_i_3_0 ,
    \state_15_23[0]_i_3_1 ,
    rel_61_16_carry__0_0,
    rel_61_16_carry__0_1,
    \state_15_23[0]_i_3_2 ,
    \state_15_23[0]_i_3_3 ,
    rel_47_16_carry__0_0,
    rel_47_16_carry__0_1,
    \state_15_23[2]_i_2_0 ,
    \state_15_23[2]_i_2_1 ,
    rel_35_16_carry__0_0,
    rel_35_16_carry__0_1,
    \state_15_23[1]_i_2_0 ,
    \state_15_23[1]_i_2_1 ,
    rel_55_30_carry__0_0,
    rel_55_30_carry__0_1,
    pkd_i_16_23_reg_0,
    pkd_i_16_23_reg_1,
    rel_55_16_carry__0_0,
    rel_55_16_carry__0_1,
    pkd_i_16_23_reg_2,
    pkd_i_16_23_reg_3,
    signaldomain_clk,
    q,
    peak_amp_rdy_0_sp_1);
  output mcode_pkd_net;
  output mcode_cmp_en_net;
  output [0:0]SR;
  output [0:0]peak_amp_rdy;
  input [3:0]rel_63_21_carry__0_0;
  input [3:0]rel_63_21_carry__0_1;
  input [3:0]DI;
  input [3:0]S;
  input [3:0]rel_61_30_carry__0_0;
  input [3:0]rel_61_30_carry__0_1;
  input [3:0]\state_15_23[0]_i_3_0 ;
  input [3:0]\state_15_23[0]_i_3_1 ;
  input [3:0]rel_61_16_carry__0_0;
  input [3:0]rel_61_16_carry__0_1;
  input [3:0]\state_15_23[0]_i_3_2 ;
  input [3:0]\state_15_23[0]_i_3_3 ;
  input [3:0]rel_47_16_carry__0_0;
  input [3:0]rel_47_16_carry__0_1;
  input [3:0]\state_15_23[2]_i_2_0 ;
  input [3:0]\state_15_23[2]_i_2_1 ;
  input [3:0]rel_35_16_carry__0_0;
  input [3:0]rel_35_16_carry__0_1;
  input [3:0]\state_15_23[1]_i_2_0 ;
  input [3:0]\state_15_23[1]_i_2_1 ;
  input [3:0]rel_55_30_carry__0_0;
  input [3:0]rel_55_30_carry__0_1;
  input [3:0]pkd_i_16_23_reg_0;
  input [3:0]pkd_i_16_23_reg_1;
  input [3:0]rel_55_16_carry__0_0;
  input [3:0]rel_55_16_carry__0_1;
  input [3:0]pkd_i_16_23_reg_2;
  input [3:0]pkd_i_16_23_reg_3;
  input signaldomain_clk;
  input [0:0]q;
  input peak_amp_rdy_0_sp_1;

  wire [3:0]DI;
  wire [3:0]S;
  wire [0:0]SR;
  wire cmp_en_i_18_26_i_1_n_0;
  wire cmp_rst_i_17_27_i_1_n_0;
  wire mcode_cmp_en_net;
  wire mcode_pkd_net;
  wire [0:0]peak_amp_rdy;
  wire peak_amp_rdy_0_sn_1;
  wire pkd_i_16_23_i_1_n_0;
  wire [3:0]pkd_i_16_23_reg_0;
  wire [3:0]pkd_i_16_23_reg_1;
  wire [3:0]pkd_i_16_23_reg_2;
  wire [3:0]pkd_i_16_23_reg_3;
  wire [0:0]q;
  wire rel_35_16;
  wire [3:0]rel_35_16_carry__0_0;
  wire [3:0]rel_35_16_carry__0_1;
  wire rel_35_16_carry__0_n_1;
  wire rel_35_16_carry__0_n_2;
  wire rel_35_16_carry__0_n_3;
  wire rel_35_16_carry_n_0;
  wire rel_35_16_carry_n_1;
  wire rel_35_16_carry_n_2;
  wire rel_35_16_carry_n_3;
  wire rel_47_16;
  wire [3:0]rel_47_16_carry__0_0;
  wire [3:0]rel_47_16_carry__0_1;
  wire rel_47_16_carry__0_n_1;
  wire rel_47_16_carry__0_n_2;
  wire rel_47_16_carry__0_n_3;
  wire rel_47_16_carry_n_0;
  wire rel_47_16_carry_n_1;
  wire rel_47_16_carry_n_2;
  wire rel_47_16_carry_n_3;
  wire rel_55_16;
  wire [3:0]rel_55_16_carry__0_0;
  wire [3:0]rel_55_16_carry__0_1;
  wire rel_55_16_carry__0_n_1;
  wire rel_55_16_carry__0_n_2;
  wire rel_55_16_carry__0_n_3;
  wire rel_55_16_carry_n_0;
  wire rel_55_16_carry_n_1;
  wire rel_55_16_carry_n_2;
  wire rel_55_16_carry_n_3;
  wire rel_55_30;
  wire [3:0]rel_55_30_carry__0_0;
  wire [3:0]rel_55_30_carry__0_1;
  wire rel_55_30_carry__0_n_1;
  wire rel_55_30_carry__0_n_2;
  wire rel_55_30_carry__0_n_3;
  wire rel_55_30_carry_n_0;
  wire rel_55_30_carry_n_1;
  wire rel_55_30_carry_n_2;
  wire rel_55_30_carry_n_3;
  wire rel_61_16;
  wire [3:0]rel_61_16_carry__0_0;
  wire [3:0]rel_61_16_carry__0_1;
  wire rel_61_16_carry__0_n_1;
  wire rel_61_16_carry__0_n_2;
  wire rel_61_16_carry__0_n_3;
  wire rel_61_16_carry_n_0;
  wire rel_61_16_carry_n_1;
  wire rel_61_16_carry_n_2;
  wire rel_61_16_carry_n_3;
  wire rel_61_30;
  wire [3:0]rel_61_30_carry__0_0;
  wire [3:0]rel_61_30_carry__0_1;
  wire rel_61_30_carry__0_n_1;
  wire rel_61_30_carry__0_n_2;
  wire rel_61_30_carry__0_n_3;
  wire rel_61_30_carry_n_0;
  wire rel_61_30_carry_n_1;
  wire rel_61_30_carry_n_2;
  wire rel_61_30_carry_n_3;
  wire rel_63_21;
  wire [3:0]rel_63_21_carry__0_0;
  wire [3:0]rel_63_21_carry__0_1;
  wire rel_63_21_carry__0_n_1;
  wire rel_63_21_carry__0_n_2;
  wire rel_63_21_carry__0_n_3;
  wire rel_63_21_carry_n_0;
  wire rel_63_21_carry_n_1;
  wire rel_63_21_carry_n_2;
  wire rel_63_21_carry_n_3;
  wire signaldomain_clk;
  wire [2:0]state_15_23;
  wire \state_15_23[0]_i_2_n_0 ;
  wire [3:0]\state_15_23[0]_i_3_0 ;
  wire [3:0]\state_15_23[0]_i_3_1 ;
  wire [3:0]\state_15_23[0]_i_3_2 ;
  wire [3:0]\state_15_23[0]_i_3_3 ;
  wire \state_15_23[0]_i_3_n_0 ;
  wire [3:0]\state_15_23[1]_i_2_0 ;
  wire [3:0]\state_15_23[1]_i_2_1 ;
  wire \state_15_23[1]_i_2_n_0 ;
  wire \state_15_23[1]_i_3_n_0 ;
  wire [3:0]\state_15_23[2]_i_2_0 ;
  wire [3:0]\state_15_23[2]_i_2_1 ;
  wire \state_15_23[2]_i_2_n_0 ;
  wire \state_15_23[2]_i_3_n_0 ;
  wire [2:0]state_join_28_5;
  wire [3:0]NLW_rel_35_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_35_16_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_47_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_47_16_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_55_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_55_16_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_55_30_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_55_30_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_61_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_61_16_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_61_30_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_61_30_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_rel_63_21_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_63_21_carry__0_O_UNCONNECTED;

  assign peak_amp_rdy_0_sn_1 = peak_amp_rdy_0_sp_1;
  LUT5 #(
    .INIT(32'hC0800080)) 
    cmp_en_i_18_26_i_1
       (.I0(state_15_23[0]),
        .I1(state_15_23[1]),
        .I2(q),
        .I3(state_15_23[2]),
        .I4(mcode_cmp_en_net),
        .O(cmp_en_i_18_26_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    cmp_en_i_18_26_reg
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(cmp_en_i_18_26_i_1_n_0),
        .Q(mcode_cmp_en_net),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hCF7F0F7F)) 
    cmp_rst_i_17_27_i_1
       (.I0(state_15_23[0]),
        .I1(state_15_23[1]),
        .I2(q),
        .I3(state_15_23[2]),
        .I4(SR),
        .O(cmp_rst_i_17_27_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    cmp_rst_i_17_27_reg
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(cmp_rst_i_17_27_i_1_n_0),
        .Q(SR),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \peak_amp_rdy[0]_INST_0 
       (.I0(mcode_pkd_net),
        .I1(peak_amp_rdy_0_sn_1),
        .O(peak_amp_rdy));
  LUT6 #(
    .INIT(64'hAAAA000000800000)) 
    pkd_i_16_23_i_1
       (.I0(q),
        .I1(rel_55_30),
        .I2(rel_55_16),
        .I3(state_15_23[1]),
        .I4(state_15_23[2]),
        .I5(mcode_pkd_net),
        .O(pkd_i_16_23_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pkd_i_16_23_reg
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(pkd_i_16_23_i_1_n_0),
        .Q(mcode_pkd_net),
        .R(1'b0));
  CARRY4 rel_35_16_carry
       (.CI(1'b0),
        .CO({rel_35_16_carry_n_0,rel_35_16_carry_n_1,rel_35_16_carry_n_2,rel_35_16_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_35_16_carry__0_0),
        .O(NLW_rel_35_16_carry_O_UNCONNECTED[3:0]),
        .S(rel_35_16_carry__0_1));
  CARRY4 rel_35_16_carry__0
       (.CI(rel_35_16_carry_n_0),
        .CO({rel_35_16,rel_35_16_carry__0_n_1,rel_35_16_carry__0_n_2,rel_35_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_15_23[1]_i_2_0 ),
        .O(NLW_rel_35_16_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_15_23[1]_i_2_1 ));
  CARRY4 rel_47_16_carry
       (.CI(1'b0),
        .CO({rel_47_16_carry_n_0,rel_47_16_carry_n_1,rel_47_16_carry_n_2,rel_47_16_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_47_16_carry__0_0),
        .O(NLW_rel_47_16_carry_O_UNCONNECTED[3:0]),
        .S(rel_47_16_carry__0_1));
  CARRY4 rel_47_16_carry__0
       (.CI(rel_47_16_carry_n_0),
        .CO({rel_47_16,rel_47_16_carry__0_n_1,rel_47_16_carry__0_n_2,rel_47_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_15_23[2]_i_2_0 ),
        .O(NLW_rel_47_16_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_15_23[2]_i_2_1 ));
  CARRY4 rel_55_16_carry
       (.CI(1'b0),
        .CO({rel_55_16_carry_n_0,rel_55_16_carry_n_1,rel_55_16_carry_n_2,rel_55_16_carry_n_3}),
        .CYINIT(1'b1),
        .DI(rel_55_16_carry__0_0),
        .O(NLW_rel_55_16_carry_O_UNCONNECTED[3:0]),
        .S(rel_55_16_carry__0_1));
  CARRY4 rel_55_16_carry__0
       (.CI(rel_55_16_carry_n_0),
        .CO({rel_55_16,rel_55_16_carry__0_n_1,rel_55_16_carry__0_n_2,rel_55_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(pkd_i_16_23_reg_2),
        .O(NLW_rel_55_16_carry__0_O_UNCONNECTED[3:0]),
        .S(pkd_i_16_23_reg_3));
  CARRY4 rel_55_30_carry
       (.CI(1'b0),
        .CO({rel_55_30_carry_n_0,rel_55_30_carry_n_1,rel_55_30_carry_n_2,rel_55_30_carry_n_3}),
        .CYINIT(1'b1),
        .DI(rel_55_30_carry__0_0),
        .O(NLW_rel_55_30_carry_O_UNCONNECTED[3:0]),
        .S(rel_55_30_carry__0_1));
  CARRY4 rel_55_30_carry__0
       (.CI(rel_55_30_carry_n_0),
        .CO({rel_55_30,rel_55_30_carry__0_n_1,rel_55_30_carry__0_n_2,rel_55_30_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(pkd_i_16_23_reg_0),
        .O(NLW_rel_55_30_carry__0_O_UNCONNECTED[3:0]),
        .S(pkd_i_16_23_reg_1));
  CARRY4 rel_61_16_carry
       (.CI(1'b0),
        .CO({rel_61_16_carry_n_0,rel_61_16_carry_n_1,rel_61_16_carry_n_2,rel_61_16_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_61_16_carry__0_0),
        .O(NLW_rel_61_16_carry_O_UNCONNECTED[3:0]),
        .S(rel_61_16_carry__0_1));
  CARRY4 rel_61_16_carry__0
       (.CI(rel_61_16_carry_n_0),
        .CO({rel_61_16,rel_61_16_carry__0_n_1,rel_61_16_carry__0_n_2,rel_61_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_15_23[0]_i_3_2 ),
        .O(NLW_rel_61_16_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_15_23[0]_i_3_3 ));
  CARRY4 rel_61_30_carry
       (.CI(1'b0),
        .CO({rel_61_30_carry_n_0,rel_61_30_carry_n_1,rel_61_30_carry_n_2,rel_61_30_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_61_30_carry__0_0),
        .O(NLW_rel_61_30_carry_O_UNCONNECTED[3:0]),
        .S(rel_61_30_carry__0_1));
  CARRY4 rel_61_30_carry__0
       (.CI(rel_61_30_carry_n_0),
        .CO({rel_61_30,rel_61_30_carry__0_n_1,rel_61_30_carry__0_n_2,rel_61_30_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_15_23[0]_i_3_0 ),
        .O(NLW_rel_61_30_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_15_23[0]_i_3_1 ));
  CARRY4 rel_63_21_carry
       (.CI(1'b0),
        .CO({rel_63_21_carry_n_0,rel_63_21_carry_n_1,rel_63_21_carry_n_2,rel_63_21_carry_n_3}),
        .CYINIT(1'b0),
        .DI(rel_63_21_carry__0_0),
        .O(NLW_rel_63_21_carry_O_UNCONNECTED[3:0]),
        .S(rel_63_21_carry__0_1));
  CARRY4 rel_63_21_carry__0
       (.CI(rel_63_21_carry_n_0),
        .CO({rel_63_21,rel_63_21_carry__0_n_1,rel_63_21_carry__0_n_2,rel_63_21_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(DI),
        .O(NLW_rel_63_21_carry__0_O_UNCONNECTED[3:0]),
        .S(S));
  LUT5 #(
    .INIT(32'h4FFF7FFF)) 
    \state_15_23[0]_i_2 
       (.I0(rel_47_16),
        .I1(state_15_23[1]),
        .I2(state_15_23[0]),
        .I3(q),
        .I4(rel_35_16),
        .O(\state_15_23[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7333777777777777)) 
    \state_15_23[0]_i_3 
       (.I0(state_15_23[1]),
        .I1(q),
        .I2(rel_61_16),
        .I3(rel_61_30),
        .I4(rel_63_21),
        .I5(state_15_23[0]),
        .O(\state_15_23[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h70C040C0)) 
    \state_15_23[1]_i_2 
       (.I0(rel_47_16),
        .I1(state_15_23[1]),
        .I2(q),
        .I3(state_15_23[0]),
        .I4(rel_35_16),
        .O(\state_15_23[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000008880000)) 
    \state_15_23[1]_i_3 
       (.I0(state_15_23[0]),
        .I1(rel_63_21),
        .I2(rel_61_16),
        .I3(rel_61_30),
        .I4(q),
        .I5(state_15_23[1]),
        .O(\state_15_23[1]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \state_15_23[2]_i_2 
       (.I0(rel_47_16),
        .I1(state_15_23[0]),
        .I2(q),
        .I3(state_15_23[1]),
        .O(\state_15_23[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000022222AAA)) 
    \state_15_23[2]_i_3 
       (.I0(q),
        .I1(state_15_23[0]),
        .I2(rel_61_30),
        .I3(rel_61_16),
        .I4(rel_63_21),
        .I5(state_15_23[1]),
        .O(\state_15_23[2]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_15_23_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(state_join_28_5[0]),
        .Q(state_15_23[0]),
        .R(1'b0));
  MUXF7 \state_15_23_reg[0]_i_1 
       (.I0(\state_15_23[0]_i_2_n_0 ),
        .I1(\state_15_23[0]_i_3_n_0 ),
        .O(state_join_28_5[0]),
        .S(state_15_23[2]));
  FDRE #(
    .INIT(1'b0)) 
    \state_15_23_reg[1] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(state_join_28_5[1]),
        .Q(state_15_23[1]),
        .R(1'b0));
  MUXF7 \state_15_23_reg[1]_i_1 
       (.I0(\state_15_23[1]_i_2_n_0 ),
        .I1(\state_15_23[1]_i_3_n_0 ),
        .O(state_join_28_5[1]),
        .S(state_15_23[2]));
  FDRE #(
    .INIT(1'b0)) 
    \state_15_23_reg[2] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(state_join_28_5[2]),
        .Q(state_15_23[2]),
        .R(1'b0));
  MUXF7 \state_15_23_reg[2]_i_1 
       (.I0(\state_15_23[2]_i_2_n_0 ),
        .I1(\state_15_23[2]_i_3_n_0 ),
        .O(state_join_28_5[2]),
        .S(state_15_23[2]));
endmodule

(* ORIG_REF_NAME = "sysgen_relational_3828661dd3" *) 
module design_1_ip_peakdetector_0_0_sysgen_relational_3828661dd3
   (LOAD,
    signaldomain_clk,
    S,
    \op_mem_37_22_reg[0]_0 ,
    Q);
  output LOAD;
  input signaldomain_clk;
  input [0:0]S;
  input [8:0]\op_mem_37_22_reg[0]_0 ;
  input [8:0]Q;

  wire LOAD;
  wire [8:0]Q;
  wire [0:0]S;
  wire \op_mem_37_22[0]_i_3_n_0 ;
  wire \op_mem_37_22[0]_i_4_n_0 ;
  wire \op_mem_37_22[0]_i_5_n_0 ;
  wire [8:0]\op_mem_37_22_reg[0]_0 ;
  wire \op_mem_37_22_reg[0]_i_1_n_1 ;
  wire \op_mem_37_22_reg[0]_i_1_n_2 ;
  wire \op_mem_37_22_reg[0]_i_1_n_3 ;
  wire result_12_3_rel;
  wire signaldomain_clk;
  wire [3:0]\NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_3 
       (.I0(\op_mem_37_22_reg[0]_0 [6]),
        .I1(Q[6]),
        .I2(Q[8]),
        .I3(\op_mem_37_22_reg[0]_0 [8]),
        .I4(Q[7]),
        .I5(\op_mem_37_22_reg[0]_0 [7]),
        .O(\op_mem_37_22[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_4 
       (.I0(\op_mem_37_22_reg[0]_0 [3]),
        .I1(Q[3]),
        .I2(Q[5]),
        .I3(\op_mem_37_22_reg[0]_0 [5]),
        .I4(Q[4]),
        .I5(\op_mem_37_22_reg[0]_0 [4]),
        .O(\op_mem_37_22[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_5 
       (.I0(\op_mem_37_22_reg[0]_0 [0]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(\op_mem_37_22_reg[0]_0 [2]),
        .I4(Q[1]),
        .I5(\op_mem_37_22_reg[0]_0 [1]),
        .O(\op_mem_37_22[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_37_22_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(result_12_3_rel),
        .Q(LOAD),
        .R(1'b0));
  CARRY4 \op_mem_37_22_reg[0]_i_1 
       (.CI(1'b0),
        .CO({result_12_3_rel,\op_mem_37_22_reg[0]_i_1_n_1 ,\op_mem_37_22_reg[0]_i_1_n_2 ,\op_mem_37_22_reg[0]_i_1_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED [3:0]),
        .S({S,\op_mem_37_22[0]_i_3_n_0 ,\op_mem_37_22[0]_i_4_n_0 ,\op_mem_37_22[0]_i_5_n_0 }));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_11
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_12
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_13
   (o,
    r4_x_delay,
    axibusdomain_clk);
  output [9:0]o;
  input [9:0]r4_x_delay;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [9:0]o;
  wire [9:0]r4_x_delay;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_x_delay[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_18
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]d3_net;
  input signaldomain_clk;

  wire [0:0]d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d3_net),
        .Q(q),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_19
   (d3_net,
    d2_net,
    signaldomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input signaldomain_clk;

  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d2_net),
        .Q(d3_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_20
   (d2_net,
    d1_net,
    signaldomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d1_net),
        .Q(d2_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_21
   (d1_net,
    r8_en_pkd,
    axibusdomain_clk);
  output [0:0]d1_net;
  input [0:0]r8_en_pkd;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]r8_en_pkd;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r8_en_pkd),
        .Q(d1_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_26
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    DI,
    \x[14] ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]DI;
  output [3:0]\x[14] ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire [15:0]register3_q_net;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(register3_q_net[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(register3_q_net[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(register3_q_net[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(register3_q_net[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(register3_q_net[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(register3_q_net[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(register3_q_net[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(register3_q_net[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(register3_q_net[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(register3_q_net[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(register3_q_net[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(register3_q_net[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(register3_q_net[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(register3_q_net[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(register3_q_net[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(register3_q_net[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry__0_i_1
       (.I0(x[14]),
        .I1(register3_q_net[14]),
        .I2(x[15]),
        .I3(register3_q_net[15]),
        .O(\x[14] [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry__0_i_2
       (.I0(x[12]),
        .I1(register3_q_net[12]),
        .I2(register3_q_net[13]),
        .I3(x[13]),
        .O(\x[14] [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry__0_i_3
       (.I0(x[10]),
        .I1(register3_q_net[10]),
        .I2(register3_q_net[11]),
        .I3(x[11]),
        .O(\x[14] [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry__0_i_4
       (.I0(x[8]),
        .I1(register3_q_net[8]),
        .I2(register3_q_net[9]),
        .I3(x[9]),
        .O(\x[14] [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry__0_i_5
       (.I0(register3_q_net[14]),
        .I1(x[15]),
        .I2(register3_q_net[15]),
        .I3(x[14]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry__0_i_6
       (.I0(register3_q_net[12]),
        .I1(x[13]),
        .I2(register3_q_net[13]),
        .I3(x[12]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry__0_i_7
       (.I0(register3_q_net[10]),
        .I1(x[11]),
        .I2(register3_q_net[11]),
        .I3(x[10]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry__0_i_8
       (.I0(register3_q_net[8]),
        .I1(x[9]),
        .I2(register3_q_net[9]),
        .I3(x[8]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry_i_1
       (.I0(x[6]),
        .I1(register3_q_net[6]),
        .I2(register3_q_net[7]),
        .I3(x[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry_i_2
       (.I0(x[4]),
        .I1(register3_q_net[4]),
        .I2(register3_q_net[5]),
        .I3(x[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry_i_3
       (.I0(x[2]),
        .I1(register3_q_net[2]),
        .I2(register3_q_net[3]),
        .I3(x[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_30_carry_i_4
       (.I0(x[0]),
        .I1(register3_q_net[0]),
        .I2(register3_q_net[1]),
        .I3(x[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry_i_5
       (.I0(register3_q_net[6]),
        .I1(x[7]),
        .I2(register3_q_net[7]),
        .I3(x[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry_i_6
       (.I0(register3_q_net[4]),
        .I1(x[5]),
        .I2(register3_q_net[5]),
        .I3(x[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry_i_7
       (.I0(register3_q_net[2]),
        .I1(x[3]),
        .I2(register3_q_net[3]),
        .I3(x[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_30_carry_i_8
       (.I0(register3_q_net[0]),
        .I1(x[1]),
        .I2(register3_q_net[1]),
        .I3(x[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_27
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_28
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_29
   (o,
    r7_x_max,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r7_x_max;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r7_x_max;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_x_max[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_34
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp_1 ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  wire [15:0]i;
  wire [15:0]register2_q_net;
  wire signaldomain_clk;
  wire [15:0]x;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(register2_q_net[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(register2_q_net[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(register2_q_net[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(register2_q_net[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(register2_q_net[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(register2_q_net[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(register2_q_net[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(register2_q_net[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(register2_q_net[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(register2_q_net[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(register2_q_net[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(register2_q_net[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(register2_q_net[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(register2_q_net[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(register2_q_net[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(register2_q_net[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry__0_i_1
       (.I0(register2_q_net[14]),
        .I1(x[14]),
        .I2(register2_q_net[15]),
        .I3(x[15]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry__0_i_2
       (.I0(register2_q_net[12]),
        .I1(x[12]),
        .I2(x[13]),
        .I3(register2_q_net[13]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry__0_i_3
       (.I0(register2_q_net[10]),
        .I1(x[10]),
        .I2(x[11]),
        .I3(register2_q_net[11]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry__0_i_4
       (.I0(register2_q_net[8]),
        .I1(x[8]),
        .I2(x[9]),
        .I3(register2_q_net[9]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry__0_i_5
       (.I0(register2_q_net[14]),
        .I1(x[15]),
        .I2(register2_q_net[15]),
        .I3(x[14]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry__0_i_6
       (.I0(register2_q_net[12]),
        .I1(x[13]),
        .I2(register2_q_net[13]),
        .I3(x[12]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry__0_i_7
       (.I0(register2_q_net[10]),
        .I1(x[11]),
        .I2(register2_q_net[11]),
        .I3(x[10]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry__0_i_8
       (.I0(register2_q_net[8]),
        .I1(x[9]),
        .I2(register2_q_net[9]),
        .I3(x[8]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry_i_1
       (.I0(register2_q_net[6]),
        .I1(x[6]),
        .I2(x[7]),
        .I3(register2_q_net[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry_i_2
       (.I0(register2_q_net[4]),
        .I1(x[4]),
        .I2(x[5]),
        .I3(register2_q_net[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry_i_3
       (.I0(register2_q_net[2]),
        .I1(x[2]),
        .I2(x[3]),
        .I3(register2_q_net[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_55_16_carry_i_4
       (.I0(register2_q_net[0]),
        .I1(x[0]),
        .I2(x[1]),
        .I3(register2_q_net[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry_i_5
       (.I0(register2_q_net[6]),
        .I1(x[7]),
        .I2(register2_q_net[7]),
        .I3(x[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry_i_6
       (.I0(register2_q_net[4]),
        .I1(x[5]),
        .I2(register2_q_net[5]),
        .I3(x[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry_i_7
       (.I0(register2_q_net[2]),
        .I1(x[3]),
        .I2(register2_q_net[3]),
        .I3(x[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_55_16_carry_i_8
       (.I0(register2_q_net[0]),
        .I1(x[1]),
        .I2(register2_q_net[1]),
        .I3(x[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_35
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_36
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_37
   (o,
    r6_x_min,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r6_x_min;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r6_x_min;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_x_min[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_42
   (S,
    o,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    \x[14] ,
    \fd_prim_array[14].bit_is_0.fdre_comp_1 ,
    \fd_prim_array[6].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[6].bit_is_0.fdre_comp_1 ,
    \fd_prim_array[14].bit_is_0.fdre_comp_2 ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]\x[14] ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_1 ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_2 ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_2 ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_1 ;
  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry__0_i_1
       (.I0(o[14]),
        .I1(x[14]),
        .I2(o[15]),
        .I3(x[15]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry__0_i_2
       (.I0(o[12]),
        .I1(x[12]),
        .I2(x[13]),
        .I3(o[13]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry__0_i_3
       (.I0(o[10]),
        .I1(x[10]),
        .I2(x[11]),
        .I3(o[11]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry__0_i_4
       (.I0(o[8]),
        .I1(x[8]),
        .I2(x[9]),
        .I3(o[9]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_1 [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry__0_i_5
       (.I0(o[14]),
        .I1(x[15]),
        .I2(o[15]),
        .I3(x[14]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry__0_i_6
       (.I0(o[12]),
        .I1(x[13]),
        .I2(o[13]),
        .I3(x[12]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry__0_i_7
       (.I0(o[10]),
        .I1(x[11]),
        .I2(o[11]),
        .I3(x[10]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry__0_i_8
       (.I0(o[8]),
        .I1(x[9]),
        .I2(o[9]),
        .I3(x[8]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry_i_1
       (.I0(o[6]),
        .I1(x[6]),
        .I2(x[7]),
        .I3(o[7]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry_i_2
       (.I0(o[4]),
        .I1(x[4]),
        .I2(x[5]),
        .I3(o[5]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry_i_3
       (.I0(o[2]),
        .I1(x[2]),
        .I2(x[3]),
        .I3(o[3]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_35_16_carry_i_4
       (.I0(o[0]),
        .I1(x[0]),
        .I2(x[1]),
        .I3(o[1]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry_i_5
       (.I0(o[6]),
        .I1(x[7]),
        .I2(o[7]),
        .I3(x[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry_i_6
       (.I0(o[4]),
        .I1(x[5]),
        .I2(o[5]),
        .I3(x[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry_i_7
       (.I0(o[2]),
        .I1(x[3]),
        .I2(o[3]),
        .I3(x[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_35_16_carry_i_8
       (.I0(o[0]),
        .I1(x[1]),
        .I2(o[1]),
        .I3(x[0]),
        .O(S[0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry__0_i_1
       (.I0(x[14]),
        .I1(o[14]),
        .I2(x[15]),
        .I3(o[15]),
        .O(\x[14] [3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry__0_i_2
       (.I0(x[12]),
        .I1(o[12]),
        .I2(o[13]),
        .I3(x[13]),
        .O(\x[14] [2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry__0_i_3
       (.I0(x[10]),
        .I1(o[10]),
        .I2(o[11]),
        .I3(x[11]),
        .O(\x[14] [1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry__0_i_4
       (.I0(x[8]),
        .I1(o[8]),
        .I2(o[9]),
        .I3(x[9]),
        .O(\x[14] [0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry__0_i_5
       (.I0(o[14]),
        .I1(x[15]),
        .I2(o[15]),
        .I3(x[14]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_2 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry__0_i_6
       (.I0(o[12]),
        .I1(x[13]),
        .I2(o[13]),
        .I3(x[12]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_2 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry__0_i_7
       (.I0(o[10]),
        .I1(x[11]),
        .I2(o[11]),
        .I3(x[10]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_2 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry__0_i_8
       (.I0(o[8]),
        .I1(x[9]),
        .I2(o[9]),
        .I3(x[8]),
        .O(\fd_prim_array[14].bit_is_0.fdre_comp_2 [0]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry_i_1
       (.I0(x[6]),
        .I1(o[6]),
        .I2(o[7]),
        .I3(x[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry_i_2
       (.I0(x[4]),
        .I1(o[4]),
        .I2(o[5]),
        .I3(x[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry_i_3
       (.I0(x[2]),
        .I1(o[2]),
        .I2(o[3]),
        .I3(x[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h40F4)) 
    rel_61_16_carry_i_4
       (.I0(x[0]),
        .I1(o[0]),
        .I2(o[1]),
        .I3(x[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry_i_5
       (.I0(o[6]),
        .I1(x[7]),
        .I2(o[7]),
        .I3(x[6]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_1 [3]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry_i_6
       (.I0(o[4]),
        .I1(x[5]),
        .I2(o[5]),
        .I3(x[4]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_1 [2]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry_i_7
       (.I0(o[2]),
        .I1(x[3]),
        .I2(o[3]),
        .I3(x[2]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_1 [1]));
  LUT4 #(
    .INIT(16'h8241)) 
    rel_61_16_carry_i_8
       (.I0(o[0]),
        .I1(x[1]),
        .I2(o[1]),
        .I3(x[0]),
        .O(\fd_prim_array[6].bit_is_0.fdre_comp_1 [0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_43
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45
   (o,
    r5_x_noise,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r5_x_noise;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r5_x_noise;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_x_noise[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_srlc33e" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_srlc33e
   (D,
    d,
    signaldomain_clk);
  output [13:0]D;
  input [13:0]d;
  input signaldomain_clk;

  wire [13:0]D;
  wire [13:13]convert4_dout_net;
  wire [13:0]d;
  wire signaldomain_clk;

  LUT1 #(
    .INIT(2'h1)) 
    \latency_pipe_5_26[0][13]_i_1 
       (.I0(convert4_dout_net),
        .O(D[13]));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[0].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[0]),
        .Q(D[0]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[10].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[10]),
        .Q(D[10]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[11].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[11]),
        .Q(D[11]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[12].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[12]),
        .Q(D[12]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[13].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[13]),
        .Q(convert4_dout_net),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[1].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[1]),
        .Q(D[1]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[2].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[2]),
        .Q(D[2]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[3].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[3]),
        .Q(D[3]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[4].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[4]),
        .Q(D[4]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[5].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[5]),
        .Q(D[5]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[6].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[6]),
        .Q(D[6]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[7].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[7]),
        .Q(D[7]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[8].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[8]),
        .Q(D[8]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[9].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d[9]),
        .Q(D[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg
   (D,
    d,
    signaldomain_clk);
  output [13:0]D;
  input [13:0]d;
  input signaldomain_clk;

  wire [13:0]D;
  wire [13:0]d;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_srlc33e \partial_one.last_srlc33e 
       (.D(D),
        .d(d),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0
   (o,
    r4_x_delay,
    axibusdomain_clk);
  output [9:0]o;
  input [9:0]r4_x_delay;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [9:0]o;
  wire [9:0]r4_x_delay;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_13 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r4_x_delay(r4_x_delay));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_10
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_14
   (d1_net,
    r8_en_pkd,
    axibusdomain_clk);
  output [0:0]d1_net;
  input [0:0]r8_en_pkd;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]r8_en_pkd;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_21 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r8_en_pkd(r8_en_pkd));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_15
   (d2_net,
    d1_net,
    signaldomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_20 \latency_gt_0.fd_array[1].reg_comp 
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_16
   (d3_net,
    d2_net,
    signaldomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input signaldomain_clk;

  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_19 \latency_gt_0.fd_array[1].reg_comp 
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_17
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]d3_net;
  input signaldomain_clk;

  wire [0:0]d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_18 \latency_gt_0.fd_array[1].reg_comp 
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_22
   (o,
    r7_x_max,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r7_x_max;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r7_x_max;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_29 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r7_x_max(r7_x_max));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_23
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_28 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_24
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_27 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_25
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    DI,
    \x[14] ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]DI;
  output [3:0]\x[14] ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [15:0]i;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_26 \latency_gt_0.fd_array[1].reg_comp 
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .i(i),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_30
   (o,
    r6_x_min,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r6_x_min;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r6_x_min;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_37 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r6_x_min(r6_x_min));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_31
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_36 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_32
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_35 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_33
   (S,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire signaldomain_clk;
  wire [15:0]x;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_34 \latency_gt_0.fd_array[1].reg_comp 
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_1 (\fd_prim_array[14].bit_is_0.fdre_comp_0 ),
        .i(i),
        .signaldomain_clk(signaldomain_clk),
        .x(x));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_38
   (o,
    r5_x_noise,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r5_x_noise;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r5_x_noise;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r5_x_noise(r5_x_noise));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_39
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_43 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41
   (S,
    o,
    DI,
    \fd_prim_array[14].bit_is_0.fdre_comp ,
    \x[14] ,
    \fd_prim_array[14].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[6].bit_is_0.fdre_comp ,
    \fd_prim_array[6].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[14].bit_is_0.fdre_comp_1 ,
    x,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [3:0]DI;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  output [3:0]\x[14] ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp ;
  output [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  output [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  input [15:0]x;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [3:0]S;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_0 ;
  wire [3:0]\fd_prim_array[14].bit_is_0.fdre_comp_1 ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp ;
  wire [3:0]\fd_prim_array[6].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;
  wire [15:0]x;
  wire [3:0]\x[14] ;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_42 \latency_gt_0.fd_array[1].reg_comp 
       (.DI(DI),
        .S(S),
        .\fd_prim_array[14].bit_is_0.fdre_comp_0 (\fd_prim_array[14].bit_is_0.fdre_comp ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_1 (\fd_prim_array[14].bit_is_0.fdre_comp_0 ),
        .\fd_prim_array[14].bit_is_0.fdre_comp_2 (\fd_prim_array[14].bit_is_0.fdre_comp_1 ),
        .\fd_prim_array[6].bit_is_0.fdre_comp_0 (\fd_prim_array[6].bit_is_0.fdre_comp ),
        .\fd_prim_array[6].bit_is_0.fdre_comp_1 (\fd_prim_array[6].bit_is_0.fdre_comp_0 ),
        .i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk),
        .x(x),
        .\x[14] (\x[14] ));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_8
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_12 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_peakdetector_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_9
   (o,
    i,
    signaldomain_clk);
  output [9:0]o;
  input [9:0]i;
  input signaldomain_clk;

  wire [9:0]i;
  wire [9:0]o;
  wire signaldomain_clk;

  design_1_ip_peakdetector_0_0_xil_defaultlib_single_reg_w_init__parameterized0_11 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_generic_cstr" *) 
module design_1_ip_peakdetector_0_0_blk_mem_gen_generic_cstr
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_peakdetector_0_0_blk_mem_gen_prim_width \ramloop[0].ram.r 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module design_1_ip_peakdetector_0_0_blk_mem_gen_prim_width
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_peakdetector_0_0_blk_mem_gen_prim_wrapper_init \prim_init.ram 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module design_1_ip_peakdetector_0_0_blk_mem_gen_prim_wrapper_init
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_32 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_33 ;
  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;
  wire [15:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DIADI(dina),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO(douta),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP({\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_32 ,\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_n_33 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(ena),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_top" *) 
module design_1_ip_peakdetector_0_0_blk_mem_gen_top
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_peakdetector_0_0_blk_mem_gen_generic_cstr \valid.cstr 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* C_ADDRA_WIDTH = "10" *) (* C_ADDRB_WIDTH = "10" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "0" *) (* C_COUNT_18K_BRAM = "1" *) 
(* C_COUNT_36K_BRAM = "0" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.57205 mW" *) 
(* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "1" *) (* C_HAS_ENB = "0" *) 
(* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
(* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) (* C_HAS_REGCEA = "0" *) 
(* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) (* C_HAS_RSTB = "0" *) 
(* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) (* C_INITA_VAL = "0" *) 
(* C_INITB_VAL = "0" *) (* C_INIT_FILE = "ip_peakdetector_blk_mem_gen_i0.mem" *) (* C_INIT_FILE_NAME = "ip_peakdetector_blk_mem_gen_i0.mif" *) 
(* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "1" *) (* C_MEM_TYPE = "0" *) 
(* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) (* C_READ_DEPTH_A = "1024" *) 
(* C_READ_DEPTH_B = "1024" *) (* C_READ_LATENCY_A = "1" *) (* C_READ_LATENCY_B = "1" *) 
(* C_READ_WIDTH_A = "16" *) (* C_READ_WIDTH_B = "16" *) (* C_RSTRAM_A = "0" *) 
(* C_RSTRAM_B = "0" *) (* C_RST_PRIORITY_A = "CE" *) (* C_RST_PRIORITY_B = "CE" *) 
(* C_SIM_COLLISION_CHECK = "ALL" *) (* C_USE_BRAM_BLOCK = "0" *) (* C_USE_BYTE_WEA = "0" *) 
(* C_USE_BYTE_WEB = "0" *) (* C_USE_DEFAULT_DATA = "0" *) (* C_USE_ECC = "0" *) 
(* C_USE_SOFTECC = "0" *) (* C_USE_URAM = "0" *) (* C_WEA_WIDTH = "1" *) 
(* C_WEB_WIDTH = "1" *) (* C_WRITE_DEPTH_A = "1024" *) (* C_WRITE_DEPTH_B = "1024" *) 
(* C_WRITE_MODE_A = "READ_FIRST" *) (* C_WRITE_MODE_B = "WRITE_FIRST" *) (* C_WRITE_WIDTH_A = "16" *) 
(* C_WRITE_WIDTH_B = "16" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* ORIG_REF_NAME = "blk_mem_gen_v8_4_3" *) (* c_family = "artix7" *) 
module design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [9:0]addra;
  input [15:0]dina;
  output [15:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [9:0]addrb;
  input [15:0]dinb;
  output [15:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [9:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [15:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [15:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [9:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  assign dbiterr = \<const0> ;
  assign doutb[15] = \<const0> ;
  assign doutb[14] = \<const0> ;
  assign doutb[13] = \<const0> ;
  assign doutb[12] = \<const0> ;
  assign doutb[11] = \<const0> ;
  assign doutb[10] = \<const0> ;
  assign doutb[9] = \<const0> ;
  assign doutb[8] = \<const0> ;
  assign doutb[7] = \<const0> ;
  assign doutb[6] = \<const0> ;
  assign doutb[5] = \<const0> ;
  assign doutb[4] = \<const0> ;
  assign doutb[3] = \<const0> ;
  assign doutb[2] = \<const0> ;
  assign doutb[1] = \<const0> ;
  assign doutb[0] = \<const0> ;
  assign rdaddrecc[9] = \<const0> ;
  assign rdaddrecc[8] = \<const0> ;
  assign rdaddrecc[7] = \<const0> ;
  assign rdaddrecc[6] = \<const0> ;
  assign rdaddrecc[5] = \<const0> ;
  assign rdaddrecc[4] = \<const0> ;
  assign rdaddrecc[3] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[9] = \<const0> ;
  assign s_axi_rdaddrecc[8] = \<const0> ;
  assign s_axi_rdaddrecc[7] = \<const0> ;
  assign s_axi_rdaddrecc[6] = \<const0> ;
  assign s_axi_rdaddrecc[5] = \<const0> ;
  assign s_axi_rdaddrecc[4] = \<const0> ;
  assign s_axi_rdaddrecc[3] = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[15] = \<const0> ;
  assign s_axi_rdata[14] = \<const0> ;
  assign s_axi_rdata[13] = \<const0> ;
  assign s_axi_rdata[12] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3_synth inst_blk_mem_gen
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_4_3_synth" *) 
module design_1_ip_peakdetector_0_0_blk_mem_gen_v8_4_3_synth
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [15:0]douta;
  input clka;
  input ena;
  input [9:0]addra;
  input [15:0]dina;
  input [0:0]wea;

  wire [9:0]addra;
  wire clka;
  wire [15:0]dina;
  wire [15:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_peakdetector_0_0_blk_mem_gen_top \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* C_AINIT_VAL = "0" *) (* C_BORROW_LOW = "1" *) (* C_CE_OVERRIDES_BYPASS = "1" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) 
(* C_LATENCY = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* ORIG_REF_NAME = "c_addsub_v12_0_13" *) (* c_a_type = "0" *) (* c_a_width = "17" *) 
(* c_add_mode = "1" *) (* c_b_constant = "0" *) (* c_b_type = "0" *) 
(* c_b_value = "00000000000000000" *) (* c_b_width = "17" *) (* c_bypass_low = "0" *) 
(* c_has_bypass = "0" *) (* c_has_c_in = "0" *) (* c_has_c_out = "0" *) 
(* c_out_width = "17" *) 
module design_1_ip_peakdetector_0_0_c_addsub_v12_0_13
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [16:0]A;
  input [16:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [16:0]S;

  wire \<const0> ;
  wire [16:0]A;
  wire [16:0]B;
  wire CE;
  wire CLK;
  wire [16:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "17" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "00000000000000000" *) 
  (* c_b_width = "17" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "17" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_peakdetector_0_0_c_addsub_v12_0_13_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_AINIT_VAL = "0" *) (* C_BORROW_LOW = "1" *) (* C_CE_OVERRIDES_BYPASS = "1" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) 
(* C_LATENCY = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* ORIG_REF_NAME = "c_addsub_v12_0_13" *) (* c_a_type = "0" *) (* c_a_width = "17" *) 
(* c_add_mode = "1" *) (* c_b_constant = "0" *) (* c_b_type = "0" *) 
(* c_b_value = "00000000000000000" *) (* c_b_width = "17" *) (* c_bypass_low = "0" *) 
(* c_has_bypass = "0" *) (* c_has_c_in = "0" *) (* c_has_c_out = "0" *) 
(* c_out_width = "17" *) 
module design_1_ip_peakdetector_0_0_c_addsub_v12_0_13__2
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [16:0]A;
  input [16:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [16:0]S;

  wire \<const0> ;
  wire [16:0]A;
  wire [16:0]B;
  wire CE;
  wire CLK;
  wire [16:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "17" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "00000000000000000" *) 
  (* c_b_width = "17" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "17" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_peakdetector_0_0_c_addsub_v12_0_13_viv__2 xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_LOAD = "1" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "1" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "0" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "0" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "10" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "c_counter_binary_v12_0_13" *) 
module design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [9:0]L;
  output THRESH0;
  output [9:0]Q;

  wire \<const0> ;
  wire CE;
  wire CLK;
  wire [9:0]L;
  wire LOAD;
  wire [9:0]Q;
  wire SINIT;
  wire NLW_i_synth_THRESH0_UNCONNECTED;

  assign THRESH0 = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_peakdetector_0_0_c_counter_binary_v12_0_13_viv i_synth
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_i_synth_THRESH0_UNCONNECTED),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Atjb6DLj9/RVkZ8WLUSQbOe0zOnSpJM/Yn2SIhTxbj2F0+RUV9QjpA0YlDh6jzVjNBhEu0yAu5AH
zQOBr6JW+tRbRn8B/2a3OYEG+XqRADe6gAU4XXKWVRJacRJn98947T82XixrDZtkBzaTcqmbVZhc
FqK3/W0bVW38N7QI22jk/JXPTDU8Th3p2Gd6AVTG4wsfH/t63SQXU6a5X4r4Z9P7+5xwbOEWghn7
bEuXvvEJ7936SSSAr49Qp0bV0yj5uWrbIhIr0Z7Yp8nC+mZQYC0JHHnUkMVqRD/kZtnUYPtrlZ4v
vStH9NflW2nhwODAnTfAOY1l14P9tqsNYuS39g==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ukmiV72zGW5URKKt8D4ZXo70r096lNbOFUCdp6JuVYC5HdQnhOzbjakMt4ASQVQ9wPchEDyyqBjm
u1h7CRsRxbmUUjzMxM59XoeJfMPZzBE5ltRyvfMSXkmpOx9MwXCEvdG1VbfbXnuiRjcw7AEDYoOP
SxBQiQPFrBJH8uYY3vZeDXjeun7Yaw5YXJczoXeHJmqSdazQOyI78I2QBMTPX97/WLHTDdjjLo9/
p4DUb4dphtHM264aYVgYr6bANbzSYnogg+xlk3Ou7ZTZHsw1MxSVpiO82/wlWuy2dm6MDdMwS//T
VGDusx54V3iPkc7J3ZkjaLbPLNn8rBzJZkaMUA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 40928)
`pragma protect data_block
/cWQT4cZMSliuzYha5GFay4pXrGepgoBuo3wMW7e//jcp6cEZYfot0OFZ8xakCepZxqBR8X4fUwO
y2kb57u9YogY5sMyk39RKkvkaxWYf8Dap+iU0YsLCwbi19uJTq8q8GbPPJUx+RDce9ea7I8g2lOF
eZrVNzpJ4OS2UTChyeo6HopxLtyCl+WiR1bFEuhJTwU0fi0yg7AiMl9qWTBAujPbFye9P3+Kn6zy
yunJ5mtaibR2QXHZ8aKmPa3RTCRszY57hi0zSJ6sEowkMhxu77fxVq1rkvQymumw/c9qoxl172eQ
WUL0AmS1/aBO0uxLJ8PAQk/75CSL9ZClbLJuU7hr67VUlDwdf/UIExPwuOSTtVAkpSmaeOyt65Ys
p9ZAIcYELPDAk5hUZFrBOaaBHOIVE49uY+9bLFS+dNdXDgOcBJS/3JenDISDnKN7MV09Pg1yREQ1
TDc5+F0jiaX7Dmwvc2jA36l0yIK6jnIjlbGVAAV7Uz0DdMzeRjy+le8ANKPbO/dixnxYD80iyK7T
KdRRnG0uZL4Yor1Hg4bIgcH9re797WamFGlkVlwBT0UsXHMnIdA5F6NmdMC/qGdMvI50n4/w02GK
LUhkga7kEvQVRKDLYi/xQV79/1s7ym1N7Qnh8v46udpieb4AyC0NEqF3hqGC79ZaBARME2Rh8Rul
FQ8Duny8XdlCFL+y7iy+sPiCl7WSzlHAsfeLwTi4U435oifimLPdfKGtrPxTtBD5HHpbXdD5INOI
i5cP9X/7CYWdNRGRWsjCTt1bTlvQFxUO4xYtljruumL7pCMWaAz415zPPJgIEtolK28Do3dh00AB
avDN4MJOA2jAziIXliwhsth+vfojOhBlhJoUdZVW7vpqukNX8+gx1+HuXy9qWzf2zBOhcPF9fW4X
MTWqsogrol0ZUgoivnSaBRp84Ntq7KWJ6ddfrs9h0azjPfxrKKr8XIy5ceQCUnx6q5apbq3iFoZC
KPi+yEoGT04M/PPSFdW6YrJk9NwRqWWtzHX4SUlVMB9xhf7sZdXVJCiMgJIIiEXx7D6LvmqL+tlH
bvKIJbvHH3mEhu5JrxJpeGiQqkLRf9Y+KI6CtxbbLyHdDMBbQ1Lj2JZt+i7ERFpQqh1ORSSPDeGv
vIM8BdpTznamev7cp7GFlh6O+ub+cEgyL5YQj8dLiLdPE6tsv1/ITQZpd0pYm8TAGGZ7z4dytMWJ
31I5dmNOfg+UM/2vaHTSJfS3BnZg8HHt+/E+m+I8oqG2EtEAGQi7aCZEvJU1cstbBpBFYGq/reU7
irA5NzVFHE8RA6Woq4o8CYNGcdB555FMXSiv/oraW9hjNdiIMxw2ozDBYhtnZlzVIrWakyCyjuzE
svuptuH9JZ+CVWiyumI/e63j7SxsbK43hS7wzYLzUx3hE6KQrtaGAg8JmjjKbe8r6QsfFmpoclQT
SWBQ40kjJbRc/MiruXIfnBpUu2hsINNI8HymoUmdqcrXi0u/ivIxpUuvO9ySBFHdekxjXMCrVvJJ
cOkJbIUXrjWoR4BMngI6G3qefeUXHxQTicSL2YXemBKJZ5/NANofZoMhme0IeY4gzn93OyTGwqM5
118qrORcILXJlOCNfMS+x/ezp6ttGclaKH/kpm4fpUk8FeBfabFUekCFFbr6uccEuS9xohVWs9H/
Ygrf/qbw6FkaM9hX3hYBLGJf0LlNqGPOcfxvHQLK7EEiIG5RM2iQ0Rz7evBL/Li72/278hJXJ75m
PCfKN6iWjbtUHjWOwdCsQ8IKer4QCXtEFr4QMTPGifj5P8n5bHT/ZEvisYd86HAGeOOnxn6U93N4
zt1sc8LgwGYUj1OqJhbVrhDfyqcefFHJ9YfCZTWkl7fwqV5dlnhhBPutUSIzQS7fXKdszJ+l2eVV
Euy7T0nl5W+nKj0NvV5y+L52h9YK0/uMwrpQ5czUFHdXuUFjK+dV4iR6AxnoIqbyeN6oqetSIL2/
UmEfRAv1e/XpM+lfvrisv2Z7BexqKGHXzLZOEiLM8fdRtfvAFwXrkS31dyX2/DgUcBalrCjxtnQJ
qiNPL00UEvoeX4RxqSYkoMaWcltrUfNH2xVcCHIfz7OB7+39DqsN4yQVfGXVeTY/aF0B537WgIy5
8H5hVPf05Qtl3rKAlUvPs2DtCBcWUuyRm+WjolF61bxc/q2t9wuyGjLd1gMsmscP6ENXYE8FiS/y
ywjHmKPa1HlWCmtAag3WYVNW7RIZOUaMdXSimhKWytDTEC55ixe986ymQ6IRI5rni6xh2u0yfqbM
GVP9aaiFtUfgAYUDUufYzTNZU8Dq8SMXXwOEnCytg+SMIfl0FI+64YoSU4t0OIXWQ3dlSWOzIBYy
9XpaZOTqo80Gsm+h84kemoQqt9/sknZNzgawiRhDNjYkmUcpxL+/nQgTP3XhTvqpaxaGnQ4Ca1H4
N7w+ESzSwrYCok9bfz2wHOesPlR57dElZeUbcu6STLa8kDZkW6pDu+/BmUJFkgCR2WbUk3grPrh9
3qOuaOffOHsCeFTaML8wwwqX3fHiNq0S0+stbCn3RSb/GtKfN+1jxzohVb4Ozk/WfsIlrbhtjxwy
fPLqXMmC+1gHOfHQZ6Lw52uBeL4LCuTkL1VwOXtFW/TwjiBCnVvSlmVyOXGzSempFroR5twDJnxu
ORwlmXcxEjc0VUGUHc2EjsjJdSR74LXcmJiKD4eUNnLxgo/5QvvLyJtETDyOtGrLB+EpJYhWKfup
DHYlvx/3h3UPZy5IYLoDEY5H0t9nQq+qzc0/XswlRIxKgpJxQZnZpT/Ql/rcwbRW93v5nC0XNqm0
pR0k8nbiquvH397Hh1WVOE165Cexs0OgUD8QV59U8XYB2SOCGYZ/yQ1K4gHy7B/AyZOAIA1CWI03
s/y5jqZsLjM2PdWKa/geF6yTLkHK7swhB/6+ioFv6ye9hhI2ChT/GY5Vng0M3ydAO/VUqVXbbQPR
KV+7DqDTls0E9JJXgQLg6cD9Rdq/r8fy0fUxPrvLFZIbf5CFruDNjacEMNpXcIfUoVvyPkZY0wKF
pVW3EHsACb1CiibbmLzCGS1a39UIDTDFCaWdgqH7K2WzF4Tpzk0MjFxvcUMxyd8gFXX+4ccM3AAF
7ozCyUl6Zvzr+v9QATIZ821Ay3kqzBsCXZcytFN+Ki1WEBEetM7D3M6TgscFJjAMiwb/PWkizaKH
h9u30+VzSw1dTOhm0uj/S8OwLuffwWazcPxG6ZNhbB6wnq4JSr4bGpZeIG+L0F6zy84nTfsNSp0p
b/X3ywleBeijH2Bck9k9ypQmIPsGbiTIWzRIYgQ0/B7cSjNHs9SMeNe8lsRXXjS9GoZ2oh6a9s5M
baf6Wagv22QGDAOVYY1dyYdGB8myFWCIR6dsM3QZABJqieR2jPIRBf1FwZ0N2cGsF9i051OA+d2K
7S6ymA9lf1psWNmNAx4or5J0yFMBhLCzqkIA8E/2V6LtgMbxLTWVINv7TloodIKyrsXxzzY0wGtW
yHAbx4AnPTtsROz0pOxhJzxXEL2MnmODVc/9eyuAE3qhBVoZutS+L5JvAac1ZpiQZKOu2wvpPlOx
IzibNSXmtcL9qnUQZn/z+CZh03FHVBttMY3tWkW6Oe8P0T1bKUTYOyXQ9tLeepCUtSN00JNoGWU8
/IoUj1oOak4dsvu+tXY/2bj/tF+mZ7JPhjmfiydmKOotYtlan2lzNcnYtYAjk/LbKGZ1nUM25Pev
G7bc00JqtygNU6zi5rfnZ69OTCBzlJWNflgWJPCSbB7y37ZP0mvstX5rm5vcE2+GH3mLix/48yXv
RMZO6qmMCDWLfXG3iPoO/4NsuyUcpw76Qx3ZeGgSYa9glmxGgMabW2riETEWyvLG/yB16A3mJ2AL
WId0vMLsJUhgtzkUf3boyLGcx1fpsZAAmqaviddQxuC95X9SezgxeE9MWYs+6uODi7Oc7Tg6exxs
yxnO7mfvUnz74Zo+Ko8muji696x0w7ikpbHZxfAmpjk+QOARSNpyV9VNW7YVskGRRxN66uvWbVjh
qFtTpFiKP5MeHXoyt7lyB58IhvcVi9GnULYmhiovRsR9Sodr80kmfUVCoSJm0IsmOUAjqtNXYUKw
JwnAaJu3z6E4qMbkXCt6NQB6Q/IwrTBRZTzjnpUlQ93xs1t1IowHi6Lct9jdS1LwHcmgv5Jn31ht
cUXXUmotPMItpqTrtlc6TrUB89pxEUnOa/ZoeODeP8coeGDp+ilGiYDvX7j+PiwlHVseTpEsRqyc
WIFxwKxrPl7o0onyLDgMhvIdxWazdVKcSa1K8satM4zN9NTajVzXKq2788cPtu9AqTaySoCQG3fk
mI4kG7iNZDMmvRqoF6KJMaPquAT2F+66QnrzSeN1Cr9AV0lAeBM/Lu4Yzqvwwdejh9G4R1Ogrf6i
XZYJntAgODqPaV8Oy4az+WJy8sdqBD5OZJIHVdVhz0oRklutwrqjqySpimIofHutWeUcSHTGr0sO
5KXOM5WMj7HzlE19Drs6kfmpt41TvPuOmzsnSJ/b3dinVbBrARpMuXo48qIh8/x63Pp9ceq2c0T2
Ku/goYJCV5Tjq5oMJdXNbaxQaAB1SVf6k+fAwD6OY9lzacUHrYod3IyRbuNjYoD/WGtB+TC2CFuq
YOE2S6VP5xcd/H0Fxkneas2vjO+nuRzf0WjMHiKxWbuMTozZCUjnAzAZNWWuzZLBrJmxWUwjQP8K
Hx9/MFzFCgQlI0uGiSX2GQQ2A6k9j8eq2MU5mOJfTmmU05IccvNlQO2R1jqcAqKr4AWSe/i6jwh3
E5kyb07DIbpgF/QEAYgZSZqyu+QQFiM+wPZ7ycoNWMRhBMFMd0zEzkRSSWtH7dspILe5meYSRXM2
Iva1k9OV/Uhkp0KRRhDvf94yuaO60LD2jacAN7Q7xXpC03KLxb4rGTYAto78xa/mRcph3ZW0+rcZ
I+J+MSPO62X6h8Em7ez//RekflMnT2TtO7HXABk9DpxR/uwceJDY03tOpx6+VLDf1VUXN60CD5uE
nsWWltftum1zJDx46PNcrffH/3rNvq4l17oHIvl0GToKHQxOx9bYX+6I5XKn0ZyF5vUMfY7LSp0H
P0YJ0KPRnpW3yZdBn+V9/3w7PagN1cEWMpb183fiSa0fhsKo523KLs1cR3kfQiSqW/ruM/3ERyHQ
WbrEZNSZIhkq+w5eW+cLxZj+xxrzk/OKKo65et2jn+zETrIn0xn7vNblnbKZszKJBAyMQJcN+ppT
X5e0T6lCL1Dh/OzbtEogcJO3g3pGZIc8qsIPHZAT8k0HtovMbKmuDZkqNElr7KwwMysAhHGuLZCI
Ptx7HbZku54YzBJ90cDoPCYJjNT8qMoYXurrrO3raZPZGp6ILETznYL0v6QEa4ub8qLp9gdrceZa
Ln3PuNTrm8uM+pzCqj329sVuoThLuBuF07h7Hn6OLyy4rHBAhg0Yl84p21Xp1vHpJnt44QA/7f8T
A2Jf9jj1JWmOBH+YjkCb58uzgi4vaeq0FzmcPmjd8TL0FkIWjnNAla+M1pEXTHulEXSV2iGfNx+h
VU5+Tx5EKy19RQZlZ3XTUbHJrutSdyqEALXC65z6FGjte2r9ZuWdG0bNzsi2ZJyl7evBDzE59I5+
ZBhi+z7R8LIL5lDHqHR5Va+U3+zcKdd5b6Zmn8koA+igmVsQfgf/GlIsgXuuXn9NOEXGMJPFJrgv
lgrirwO9GlApc6Mzf5/Lb9l/46GB5hhwCL24RScPc5U6IXwX8xtz1hjWdZvillnvObrWPKx0e8ru
Wx7Vz7FmByhm56dMuf8ExbCyxVAXbeIl7sU1f3VSPm+J4Rkv5Bgci99kbA51PnYRoJPJTcIvuhiz
1BObqdjIq5NWPqMtJvN89Kdz3ksB+Ax+7YzpYmb3WBp3Z5b8T7h36pQ6IinGKU7buk8XjkDKCblE
lVB6PctdS8QXbsDi+jkbiSX2Eb/oWQK1CsdQaVsHx9bVb6X00OWYM817yxSM8FTftv7J1DrxXR4f
EkT+962OmD7mDBsfAh9S6YDAViaUhdklK7+7/P42/s58JmaLfCh+pamOpvW6aPhgK9ITseaQ62jk
mdAHTL+GzwC0V1EeqZ5tRLyyHg0WUXAWLKBqDaL5RHDcU+0dDWumwyvHSbwHagYolfbmD/jj6rzx
5q5CY4hJOG/rr4LW9tbTJxmmwiy94Jwpt5/RGyWzPZMHP9R6jMzq6aBPIEFxFEAF8SqogT1uEaf+
gqS/tqyb90zhLkBA5ac/4i6K+D3u3NxC43M22bWrH0oQfeW3Z0hClEWbw59rd0yqX8qDow0e9d4B
RyfsNStszuWd7wnGpWr0e+BSbgdOryqb9BOmh8zmhH01PDjeUqcHscXeTNflwgz9NeU1J3B1lUKB
9T7sgWITzr5h//np6rvU1rwFqg8V3quGMbqX+bwupZgSWYkLHMyD6nKImr+mBMpwp2vFGMVtd/Af
5EiLUktjusCF7uKTbQnQNbLHG9aSlpU+BClCt2mmTqYJBC0siu5UMHngBZp6d2PasRxiGYrlD1rb
S6aq4GzSoR/Ki/cWOo4VTCcTJWVfSVY8rwa4jAtedPMKg1x57Xvr713DZO1XRVnbclY/a3xfp2fL
deNUi4JEHWfguxHhgtUHremY2P3Q/OhELXl17tSOP+5h92FX69VkbprMPaTrf2Qy7GKlOX8bQQVS
E9EVDlPoqBbeTUDeH8EVYJThLhWJmOwwFApfWfPBXiCbBjlHLguu2vLYfBEr/1TnoYdJ+0pnBjDh
TivXYWsnC9IFb4UiJA2LLtyZ+NGRL3PIyHplL1QO2uWtcgzT3YZBnVaI3SAPHQ6SF0ZrEzKrmJ9M
akTgposBg2rP2sFTixXdUnRYOBn255VKBfSKKWriGkrvAQ0e+JyH0Blqzt4jpfomQ//UbG5gS4Fj
uKDvAsTIRC70ZhCDEr59uLQO/Ta9qa5uBy9jk6zFxQCyeTqOBrgM6BdqRCtj6YArsMrgOJRXyanL
pVefMhBXv4pPm6d0o7XUYfMSOFYdTL4lvIp1O1VMv9fPQku4z9uOgb8262Bsk6bxLXB7dnbTsS2N
5R5gpITMa0LnifpApi6nnj4JmPdF9o87JC/hKEdsJ+duJGjP4tYtj4UqFMAv9rTLoPHOkL92wRqB
V5tH4BkTEKqvyrkDbVv50E7antFw3EyS1C2SomYiSTahm/6+ws2vhaiaQFIVdskQziGq39GN+y7u
Qj94cIba6cajB46ujtKbDeLYpaMBEyD6Cq1Cnq7AEJoIlb0tsJLOTcj9RSgdc+jRZwntYLCpyGte
XRNSHNOFWsi9C1hhQNq2Pw1EaTtLC0u/qrZf95tQsopijIsriiJlXQSKq1Jhfu1+7JoLEKPXoNra
d4h+OCB5VqA+y/7modzmQfE2L2aLZv9piCbMntS4BnSj+X6MsFKQWBGtV8ZrW69rBc1tz3/dyXeT
RIKDN4Zcytmcfons44u43Zc2BC2emXpjFRFwd+sS4mcZdFsy5qVHWPd+m55r/FqZeMeLa+NFR2NE
/93TjtxJFTzS4ktnriU9s4/4wy0FDN+eCT6hZDecdpSYjJTWb5dCj45NFktaQLTlYoT+ZMe0WgGC
9dcYbyJ/RvV/4DOcRVOr4+yhMiYsL8L31tTghE7EP0lMa+bu6yqSk+VIreZF7vd0KbjlDHxrZnef
W2ensQ5tVjgoswBY0vmzIrLYEHH0duzdxz3QBjiKJpFkp50hLxPbM4s/W3JBmd97jHBhtM/VBXvH
BqEMSzldQCzSGT3o3MZl0nUWoM/uN2Wa6R/TKJ1HS50OEAyeYTkTlNxbNr3y5Cm0ew5cyedSb8QM
vKzOVz1tjg7M7gL2mf0ygbhs884BSLxvadyDIHIoA9Lkse4a/IRmwU5o19R6juN4C/+j8bsEHmAg
C1MwiTZWPshI4KpFIDVkdLKRd5q45pzEbjGCHn7y6GGsqh0hQIdPLJmKhzE4wHmSlzzzFNJwpCGz
UnfcroLt5LF5HTxut/qzPSETsIjJD5tXp4Sg/K82DR8DQxyREqBMxnvjPCciqejcDEBHvlz2y/LN
q9/jTHBEstyk60ZZIzgBki/+2XTBHsVYNqZt9tkFpEtV2+CpZWQH5GvTawLjPfC8BdLIKiIwJEar
+/Q7lFihDpHVqzb2x6oYcE9uIiGRwwjnSfBJ5dD0CWrg3pxz3s+RXs8j2s29BkzQNsuXnuZ7PQuW
Zd4op7lrXAucT/98PWj8ljvJCHyl09PJ0/8COn7Azo1z0QkG773EVtk46J6zWHXxbOBrpWZjLsnM
UWQ0hTEDOY5TCym1qdJSAQLsJDeNtlbdhpkGvSkymUTxBSgU2VQGV5bbmNgT9Uec/3xx8STk+M1O
40SAC1ryBJqbxr4FRzAjbariRE/k8SKgMQ88ZTFjFiG2BzzrAxeBuP3xpQFWM2HycsNCB7y3M+DP
zghwXfjIImkHCIiIZ/ns1h7Z/5z89yAJGmbYLZykoOtb/Y9/YrBkdNtwJptAzdoHEX+4SXuuqMle
z+UzvLjQMJFl64zi1Lq2dpxMTIBGiitAcSVuw1MgUtWKjz4oQ12wjEjiucEUeo3idxmxCoAeqMAw
ARP7zg+Ef+eTP9pe4TCeKWbzEOzHLbXrBLF4xDj54dFeZNHDV/pPzmxDKGNQJtmQDPU66I9P4rYN
swbsb5rhJcGLcCMKl1+ra5qSng/jyZmgIg3Lal+Tf9lCaElazVDVFA/eOfveHNWtos5HU3KSqOzf
uEkQw3kiLwBB+aBvCTCSckn7nJ1ACLeKzUVNXU2KhTBlosaCgnjCpE170pQHb41ks06hpEOWR6So
YoNEzQapbZkvvz0AXq/jIIPAkWx5nyJKEnPtb0KguiHol/8tX3Yp2qBCr7B62RuAlNBLeWxMZNpE
/0RDVQk4iJtO84DrfS5XvJwCEsnw89Y2gZJZfPJBW5QzJ8lSxdk/3W+li9A4E2zkydOrfiFm5U+Y
yFKoODXV0o+82j7o0n8BCYjyRAWLIYxoNZs3xM+7dZS+XvItaj69P21NeQVjfTUdMNS4sW0YPO7h
XqQEu6zoQpDjoPbmMxlVb/sh2ce2R5wN8pwMDu+2zvf9jcT7CsYSa09ODngmbKIoyw1MrxhbI/FE
+eXA4XcWecVo7YPiyrrOxzoAgqR8VKQexplAkZp4YFpLI97qfW/NPEYXN6aQdDC5RSwRXp078ZR6
kjJGMfC9BGsWN91A+V5l9qmolc2tsV1+p4RO4wo6jMhOX7IBqni/aQH8VsJq2Lz+LBu53Ft1fjld
YiOZZphHfKty0XRbGQyUcFrIxBttktRGk44mhCqcUvEWQld4MA+pJMh6z5AH2EqRp5zcdWzTRjWy
12uWDQbFv3jSzbqD8a+u3QVC1H2F/VGVp94HVjAt3C18nUy8F9pnguX5hSktnR3HSymPP4oa61D0
oapT3sTVdye5hJXC+Uw3k+DHpdv0c3out7mYy7HkFNsozjVS45+KPbM/WqZZa47ceIgSYo7ERubS
pOSlAyJFhDydGkAafRaDLrKJ22gzMbzj6TuhwKC57FNDVOsyAbEPdmHxR9TD3jL16Qr2kNMz+yU7
BEO+qLjEpv2VnR75SWMZCeblXeAHYLtVI812oXw1Ni3Fare3LO1eCvS1hpK5mgvLvMTjmd8GinAZ
7YlOD+o00IBb32YZaYqEIH+LXac5JbBG0C2d6cON8F3YLrliDToGCrPahAHnc9chudKJgXzHIbNT
orR9mYdNEtWbYycbD9qpLNO17RsGDDZhn/fgQRXu4wtfV2SHglGXa2R3l9d6hrqg1LKT+vapiZGC
1jpf52xe4m2D5Pe/2xNlcfpcdXbYAP4BIA09MkftG0nYSyh2CR5I8AIO92AAxB0V5O41erd42Qxq
8NJZguXfG0+55unnPI5hQVw2WsPP+/3mH7U0rhfaHiCv5mMHKQ/nL9cUoqHNhvbBd7aewPKOonT6
RTC9b2mwC3Cm/ALjLIrWRgN9BoX6GFqzlfJ2BtUd11P16KFw3UkRlQXGy9GeAJWLPP8RCffZy2UR
Q9GjGIgL0GKZuTJLNWtKBwsrZAHjwPk8+epxjchjh8CxowpHEIQVIceyMbmIBQHN3ihPqNJ6q7en
XeChFKK2vtDxkqMmD1kJlXm2MMW66LhK2b4n/90C4+B9TiBg81p52QwrLsk3j20M4kuZRYLO5utW
LeO2FrEJlvTp6c9I3Lt/k0lGVYyToXTuATxR8O6VoFTLtpd/kuJ0gv/kNvSr6pcLUybaCjgXdlbY
a8vORXRJGkRT0c4lBjZR+e4BhbCxSyNNs/F3T03MDa5fMzbr6AZJkHEGScn7AQl8IhieuUaL//fX
PbFlYuOxVuyPP0VJWoJMbGSec2qS+aK/0hboEyeLPNn33juaBIBUL+LfbsyD+ORTUU7IMeV1h/xD
6GEyL5D4DVdQ3mZ/ssSFlf4xcDGcot/iUHLccD1Ol7A806+4xL/K0uxP8Rid/dERwjEDMxcbICSN
ymGEGIF6WVcHO2iIgSGyLM44IAfe8Sog20BclLkajz158iILLwEEZXt1tmrUsubciX88vJpvw3IG
pGMyFZe+ib6wYFr2Z0LqV7aP2XtomUnfuKZk0lvlCgcckSBTr6IuxzlvwnEtqWHhFerAmZPjaUB+
OAXFI5F4J31wFWNT21nk0RSZD9K1G1sd9rKTr0dZ0uzpKPyFQmMBzcrxIY5zMgYqp0DP9OrLlQfc
Dbs4WN1ef5KaXPDK53EXSPFKvBYOhzoZ42uX8/LzhR90jHc5dg0mtHInwaQiI9xSy67vVqe+OhcE
Isa4/p1i9Nbs1tgBaez4CueAvB4uAcY6iBXCOZSX5bp13pwCUcfYA3OTjzuF0b2EhfAGaeGFuCUE
iotntFOZvT+shqqCQiE6iKD43A6aa5gpRQFyrKsfH0tlM9iuRa2FOiLYq6SvMcakftYp4ZBwpvX4
4mTLIx+mV81Fbi9J50hDOgMGAEIyILdjlMedqXzCR1YAjJH6CtemGpveI8+B9qx8HKTYnvw8VVDh
9njk5N4gs8+gGSftOB77ucnemTkW0xoi+QAy/p5KQytS709CCy7HO3NsRp8rExgbF+NGWQHmG7Ki
8+njprT8Vf/XEa//QyTKCKzwCiYirnTHrM4KONVv/QZPqJOyxYTXc00b6lyO4JCCDkkJy8EPdiwm
g6290GQwcJ0OEwFYKXnRK/QtRtSOWQsN06RGVopFPnlUJu0Y1rFIc6yt/7W0n4jBqxIsVyXgt5tJ
eQHRjcL0k7u45YwdFQc2g3X+s1lMUgLJpuZNtWFs/2hdyYHNW1PV8cBSjodLxIaCLE3I4V6lLtz1
FTwC/Lgzwce/498wlVUIJbDa1IrBD3feDXI8+iaSUXkQQLhR89xY1xjhVAPN/Cgr8HLlqwVSO0n6
zq0Uv8eo8iDo9RjnqxX31e3UbjXf8Waech2cmL3gP7i+uOLMWBOliXS34eXQ9jV/HNMoTlxdOttP
jNN4vFV/NOSI3M3pCHO5d2n4LKPUd7TguTg9HGo3mZ/FY+vKK19CRuLUlQHhAcentIv+yRtXPOkr
+AYjaE72TgNmDyQ0BynOS/QYAMUgQMepb+eP1d76SytZodJufW8SYnq62ZVEcBw61qYGK7ExtlvY
mm7T1y1DuE/8tpm1g80i5fer3Lo269Qb4q4Ebqbro64xVPy+31offgR3zUubxv/Ufzg9Heto/fzd
fGUHmuA9CY10AB1cn+OPPt3uW3sYTweJknFH7Rv/iXHf6JX/NMEehuTc44wIeCTJagVqXe7yc2fJ
SHXVJejsFwZwFuuseaaXR4nOgsxpXkxISYZaP7CjDTiacBxd9ItrW8BXEHV81a1mGvxLYDIc5KPj
zWIJLCm11iUv3QwqbBuj7PrkD7YIXJT9syWHDffMd/g7fceMDuG06mGQI6CGKx+vpx2RKWAHP0R7
CgSRBN1RqkF8BqX7FR2A5Wc5s+hy+kz5LwyrjO33GA6+qf5H9WxhkTjDyp1zWAi/Q6C2UQ42R+ie
e9FGovEiShGs31mg4rwawuKSdGhvkGUB/tKkKDM0Nk7P+bTT46lc0GFnDoYBhrQRoeCREJw0liVS
OCCZZ6eGkMYgDlaRSz4g6YYSnHAoWaTKM6+tMOYZpkfHfgb1+GzsljPTxUMFc7gqgsnu8VdaO2KR
a2q5aWTgRT/3GKflB1lfWWeOuUwn9fVOe83i788ydPA4Q7F4d1j/gJwF+OuOGb0RGM80pieGgQNq
+QQvmBht0pzLTVc5gYQm+WswGA20Vql4oaGo3TNn6o0s9qp7M9VF5dBMdCBs0k/o7dMLKB02JF03
LNabhkQE8SHw3/YFqUNJW/Zq7Bqi5sT/4QgrG8SjspeYbbvJB9L8kym63gtHYg+pa6dhx7MqQGkZ
PWj+mP7kl3iDZZgfNiF/b9PsgKjcPoT7vAUN4ntnqlGdqWTxctZoBy8TFIrBZyDDHDOWwscT2I7W
4TVRNlu2QZNw4oqx2HTPSiKjxFzflVHapODhZifI7F4XwdOicyilETTtEdx29YdjAgaLER4JRcsm
a2yWuyJ5l9g6868MUHW1+CVxRxe8Vj3MkHM42YmNQh1u4r8wQwAM/SLvPI7QPXDR1Z/giAZlvxnt
xz67L2j1F+psGtt0Qk8gmgS1XjLJMAquZ1rE0ZvxGYvJS3GB9ZM5an2V7SO/a1f2pYtmTDP9mp5C
9KUqbljW86jyB0ISktcp+SMjrevdd1H070GS9LgpwJAAgPMp0ecFI+GpoIFvO77Eg5u63BSuUi+v
MPd8NIsjTFqyX6IEB0jUGJi/SYO7wrVW1nr/vjngcoIeRZ0/hNy+Xc6UrioVFGjRfatc9VFdSrK9
yni6W9b/wrHFiWuSWwAYTwtplbXhwGYc2yeuen4+6m3UxmgFEIqO/0KTyqsDql+sXTvBc0BAVs+e
Wct31rNExgQ4ig9b1eVrwTDvu9xwNETQ13x/k7yexUVjLCkIBx06bO7AOyK8VxCZJC03ebSFl3pJ
H5cHxnBn94D3fsqqHfBnkxCs3nxH2+ruRvAYnacq5wqH+6Mv7njSJ5ynpR0WrGH+7t4fikiRtlxT
ErSoIpeJQpxCcxAZT9yopG3kvKp9TZd18LHFtcjLt4kmpQeL1eM/+9T607ZGg1h/mV/n0eHTEdb1
PyxyOf6/iZQ9Mlya11jveNmeb72hbNNkSbQQe9516osiBwW4AbD5xnd7Ioknj/WGV2e9URFJphtP
bm/a7QnhM+ukQvLrndLfLbiXijrWyPaG3ORgKzemAfnx/kX0LcZgvgPIZJlWbkh/EaNja3NOitQK
1UKTXMXVPUHhoA1FUrJSuVX3nzfdJJv7+MAtJfSjXYnjjHRReKfpaZJ6jEVLK2LIjuaIYlnLohN8
r8nLUa/yDs/qGejpDj8cK28oPMqYLUE9k4WGzjYrnsijECfgHp1dAu6XWBQi5J9tyt7sXwsV5Tem
vKXfoD6cQG1if5dPRgDyMLwBEXzjkKa648KGtbS4DXm9M2L1V/XcYndArES+/cOF8lf+DttNeUcR
NytiDJQgG9THQmrwKgQFyxxTUOTcl9PgsXENY+Ar+tvphIhWxQdpTL2hT5HeECHPJBEIKq82XbLW
kp2Z0+M+mSNgyb1nw7E8VTjQUgOdJVC39PfzVPaCcL3JHCPF4+c/bjcjh5XUh8JgoKFGkvWnoTr7
hsH45IQoHqcPJl+6mWQ0vIHN+PiDwdD5D6d/+OssEdovAV+9lRvSNq2UgSryz6BR+IwbXLruBK5V
ETic8ZWanNOCLxVOwhhqk2nYWJ/0ttpdfToDJk6CAGVXqhwIsEA0xeE0HaO/pInfXoHwqSMHA08N
BBrb7GEIdB1XlO75y2InPEMyALjgD5bQ4lcLG1/zWPOXMZwxsdzf/oj7LlOjAA9NE7IkGruY2p0J
JzNwsY3+VyFpJsYk6I0/XHpweOHaiKjpgI9WWAvLO6nXjgi1lFq6gXM2YRC+wrO35PgXFRtW6L9c
zBR4UbhAarBfmBxy7WAkJePMZiokRL89yYPQwzoMkkV0kgbsL/t+PetVrLwhKFTolNjXobgDI+cu
WXqmqOBXTCfv4RbuRqg6ejk7MP/ebMvfCpuwXg7nOJ3obk8k6Ey6zfKumAYnCcTlUx3kikfIoabQ
9dDxeyO4GILOP6aEymGSszW89zyr/4H5sqRbnwVrjKOp281CV7AJbyY3yzyw4TSXkTgYzpLNzi4W
4iBq3UO3FLHtgynJhgE3U6RqBASZ1fxgzhCEY1qqCHnyxwaFjgnamghYkFsq31TkamDCbOC1sVrM
QwaGs4HznGr/uD33lv1fZ6OKB53AYfZv3zgfCvstqUxyhWuhYvvNf3v5wcEgWjAGXHTW4tPbXRwl
dum7+H8NkEGYp4FDSnPnBfK7o7OlIdiVPuP+2ypBsUcGfsik69VCgqMOF1+fmPkiFGPzrbwtyfGV
yQAktAVCaVh67SaCB2AvD2c3PwyRDYpmRVZRbFgRLk0tYIRPN9lfoTvDVBVr8VUfdZK/ebNuCKAr
2x0YFibubhF/IguQChdvHmLgQTqH4kukvqbVz/ktGw/oeWtKfjS9tuXybOAIHgu2GCSJw5sQkr3p
rp6Ljg+55VZGZ/cY89KO1M2oPe0uUuIjX3TbO3c/+zcPUgY88PFIEipYuqj9eG0fCXM7JAvRVxso
gvsViJauVfmKDZsdtcQOndezqH8+cxtpxI+FN+7l3me2T7bMBmhEfQatAHoXcT4CpVDRIYAmhn72
yEO1bVBTw72MuSZfiy1KSmLzquZKxPeFd5lmwe/Y4CrGQyD/uTjvzjhtZuZVUleEHmf+H7naM3z7
tSUZS4b2Bon/2Nm1ncbj3QOBPbVGOmRN/hkBwRAOc2PTkmSeaQs7AaHgy8PYUdctgxI6qF1DeA8a
JyP5sxrvcdYM/lYZDT7WTh+DuyEq3dGOdO6BsA73gK2EGslrr6VkJOz8IZMiGC10Dfrs4uUwiWEN
tkodNuanBu7ECuK8nZQzoNr3pLuLj4CGypgzWUSi5R5IrLrcY5wLOdLoWeg3mpsZK/UJxC5SO1I+
fIxXZhDg0qYwD7UeH5KOu7XUZaoUYJFXC38B7ZupTojtkBoevLYifUSXySZPh6PG0YNBdnocGUNw
mJOmxC3OLoQdj82QGA8SNelX0JN2U1yCeM14vYGDwkvmIqjT1JEr+bunuEHYtz2BBlrpHNzrn28U
+KZjXQSzw9OdGzjDdTdLV4rukq7ES+Cw5YLoZ4Dy2cDYwtDNWRepxmYmyLDsglg5CANtJqaUn//o
dv+WMyHjC40Zlqjc0wCL8aDS1xCT83r444fvzZTvgg77EAJjAttsDOM3/fo+EnZcfudSP92YhFuy
GeOP++DfCAsp49kLTTUSGz04XSK4abdDoLIX3mrOGvh0NtBjnpeQcdg91ws/UY5wLzjaQ9rOSH3u
NNM4s070CYmeXHlhj02siXaEpkFKqpnZ7pTYadVT/MomWA4ZgJRTmaobphncZSjacnEwbuaUGmV5
JJO55eyfmERSDbdNDPYQLGJCshYYU8aODHWK1ssl10HPrudGYBOIMiZI1sAAjB1ooNiNsM1stwa8
E9ZUYT0K74jGy9emZmhlXdx4cdy2iVzpDUhlq8r/DAjlqRwJM/pxXgETqaXzr8t1EDcDz92tkgsK
8UGTq47lbmP7N6aC0Anz2JfoEDhyYnRd50IhRpg/SG7c70XcjFgxCtwepoLk+GDvZA4nykOuNUjH
EolsWQpFNBzJeUi6UT/OMYciKQFyPo9e/TDe9PqaejdISPBKTyCnFQH7QCTABWwMcfxR77ZZ7DoC
4TfLujfACSDuS2X3KCrcm7LJR24wGffp9BRius/bcbBJSbo8ImlDeVJtawUpZgUIUQLsKkpg/dmU
OPrCB4fhBZEj1FP1+i2AVjiZJ9IN8YXsRqueKp9zj4vZCUvOfBE4EV3e4ukwsf/M3g+/MBZKsfsN
9Lto6uAi5NMEMR38mwfG9xXeib3Xhpa4ms6QLfh7l230eTxw0sMl25JU2zlhFrq24UdB4oRgrmX4
YkLoRrf7KjyMoesyvRAE2luMf3DXzV/IH7YRUphptFr47p2wl/GFRZ6CiOpwgF06QdUHvO6YAEcj
gBlm6umQ64zOw13Bw8yxtbARE1gzMS6TraUQj0ymCIGY7mzE1pdgE8nSXu19UE1J/8Dy54cg+BNh
11U8Zn4HD75xMd7Cn01HItKABc2UGhAg4J7S45rOOh51ji1OQJCsiOItGBT67qt3YbrdJdm9Ur0l
LHizsho2aSsiEuysnRLb5NPZm+syPM6ua9Wn2uZsGB6Md0R/yj6dPi+yMPo5yoB7PkW4BQiAtd4G
GFHOjx7rATI+ymX3L/AfspUuh9CAzypYiLcX3utdOR7Kjfnvdjaw/iIO8kf/M56NC3Na82nWk25B
w3M9c1AgXrBVrbUWIzXXNgbjFCtCuYXwvIbcGZJNlPHiytiy8wsjxqI/9yFzMYm8EVl8KPAkyDW1
PWnrYnZA3kfdalwsRRZazYKadiQflt5YGCHV/xgM4vcS5Mz02X2SFLnjMB+f6RSSrf5iFsVtcFW8
Y1+Sep7ZQRsB3mXQlbq+dbkZ9PGXSBV7xvPhPhsWy1xepaSrepepNce1vxtvl6GTKtm36a8GfcY+
5RUSPfZdrQykxKxDbfi1o0DoOk4PuiwaX/jO3ulcsa5C99n7sAxthI0OidjilQ/FqSfYm4fS8nfE
ZWSe74n3xY8RRdBkVm1PmJbGG+gdStiDxPCW0rZbbfYXpntzpwDs6VY6E6AJ17FvuygQMcp6dMjU
/uNS1duslcSsChsgdHjnODnnqW5oiS+TJ+qo1+lee7XJ5TEBtHiHT4T8tLBZjPs/WxdeKA0Vd0+8
GpXe1oSPaGm7G1OlcYOFloV+hxwT6M1ZuJHx+nuQ8qj9Y2I+yetnzRT8z4KV2NUx5NqTYjH037MG
EkhFGwGDVacGHCUIgDBKnyDQ9477EtMjDUsG77qbhVaW5F3QGGFOW8FQN0iLmjT0RSFrcoGes5qa
8sW6U3frW5Kmt4JHvD3ESicvRPluQ8k/qyAiJfzzZyBVOzbcWT12XvYUOp+62QD84ZTQCL7Y9Bwc
eDC0edjWFZCCE0WxDCi6korYuPA5ojfZN59lEQ57wrrtNdjONqDZTbMJTISlLK+/o0UoFb89mnWo
NJ7n3fzK2ruFMcuFuXmIELE6z3MxfA9UZI6YX2VeR8u7yG91MgG4Phvw2abKtyqO8JCvzNdAJyXT
YXfyqEDrF1fZPVhh0CudW819qLOpduGnDvVJ0IxUHd9LyVWBHcTYYt7WOanZABzixJVYYZI9z1xU
MRereJvQQ2PlNVDx5lUD41Ys06fcOlHOgIBXoxuhgCf80d+moTZLQUdyia5zOu2wY1wJ8AROkTfJ
FOz068ROJvrn4N5O4CtliFQiwYLxqgpMdpW8b8oovpaNu9brx8XKiP63QSChf3ODk0napBphlCEE
W6hLq2PiSrWr4MQA/Ar9czxiAKrwOIfW5zjoOHhtbpaEsLkQxlLNd0qzmx557aPKIriMADEph3cv
wu7a3E0lSB7g6yeL96fZ3EQTNIzzNcQ2RDuG8iaRIXNIZMn0ZvRhrhVI86UNirt8H5V23JPTLdWl
VXpFcNAHMWXStzA8Wa9x6qZbfRAj+XFkiRzMJcX0KAswY3dvoYqGOzjSyuaGbIe00ZPafAVoY214
njrddXhxsRaTKKgvDLEwkcZge2pvFOAVCE4LRmYeXl8UYXJptGPps77dOt9QUeshAGapZHuPNUej
8jb2UlYDG/Gbx/hS/Bno+ipjr2IcB/Ca8gHLO6PkAl1Gwr7c4ZaRfzIUxe30ja9CtvcLRpgGcflu
Q2dOYCiwRv9D28N6x35n5VHqd9dE2s0f8Nqm9FDeGC+57Zq8PTQSgLs77egGsHNaiiRwI/p2A6xI
5rNpnp53CGKcISQuNfB+6fDu6JTkAoFI845JDTaB6Z3vaOk+khGRkSyOpvutZ/TBc6uu0kFjgzfB
QZqgTYXoaSUmbE51rHUUCTxqgS8jZsVWd0Nb6ArTl1Ft39EWxc6mOoXA5+pKkry10gBUhdyZH7um
EJ3dY8Dv0u9PImNHELghG249sj4gfJJoRXzxqPWSpKUvS6W+ESM1KNPnSf8uKJP6pijVMSwYOe1t
OfHSKBiKa52Z55FY/BDmLqUzM26Xq6J8o8c/cyJ4Rb6Z+wM/nICCnLfV8ZHERYWKcUcTuN4IIFtS
uO/9x6iA75p5Re2Cb5HAKwQpOVN+AlDcZO5igR/+HYWkiGJZ0SfuRoUmsE77zlvNMUoR0bIusEoZ
RHjXo0PoWOaKp7K//A4EA46wxFvzKXTWcczxAHEX1ZzGxZCJ7E9OLT4aDNjyQh8hMtTcEwCT5z6u
VcsJzoARKI2RBoOQLnKg7XAexCz1KmT9EZl3UQBaY9nxSnMF7FPXpaTnWpIifgzQu8BfxtZLOTDf
E4devYf9H6CSF96lO/mQ6ezFSv1/Ehhuho0JsWz9TXrk3arLJOeYqSyB9CWVSe2vkI7dQjGIHWDj
XJ4UUnpfbnYAQJO56GxhiRs9VcU+ZbiLXN7YjKJMYMF3kA8ee8QZdmhfFFLVtH4LmpHvLCzJyr96
wiD6AZj5YWmn1QnjtNEv5c/9cccQxlXjVINEiS6YjiE0mcUO+rSKaIDrdVzkpHVZSAOSj+nVpO4b
bX3QfFO9diyI6D63RWZ3oraXL9vVaS3esG0tOf3OKpNjBwv+W2H1lRk+FSL8eJ9U/wX0OJSnZSLI
msHc3apts76bVbbBW6ab704JQJgiucZ0wm0DHWcLmNDhCZymQ6HVLFzCHrqEmaVEq+d1kHMfY8lN
PcxOsjOwzfKgrCb3QyvofHyXLPGVXQkANKeVRAIy8O2FptMmhlbUEgtIXAJ8p3FDPAeKtH2qF6+v
8/i67ub2Qvq6FcS3bAy4r6DgkTVwTCopwAwGSXbbdQ34sxCFE4a0nDToDcVbo9Mx6M67ltDK6Ela
YoBMDmAFu9XQdEUTgq3y3N/bNJXdn4YjCp9C+oP+6lljNHiML6e3CspjZk2nVq4GnIUzHcyhZJX2
ZMFKVkEVxmYr1Mw8RoTpFTxamLR+WEPtg6bwYDq3v4Jy4ir84vn0uQspeag+kAGTWhl2dwqVP+e1
FTRCbhaM+qTLWV8ly/rmGqMjNCHYGki4YFMlC3VbhCjCIDOD2yWpgRgAAw47/9Iq5/Wv740kVm1B
TXEABSDGJCpN6bKN26kfXDv0CcVYMauGWAgD/t1wpcemysuNTlrOMkiZ39I7YodNCbAeiAUSfbWw
CwrUnpzc/LlkJxAdPJ9gqLpAQoCHl7qxGPwIicR0XaMyjHG1fJJvxlSgZ48Ykw2EEZJso4PSboJB
lssVTEYAPekUCSL7JcZ6WgBfH0VLHxQVWdqOxY9PtUMcg9cbAVOYR2UoTAHVgRaJfNm7/QgnSAug
r9v1qHWKeyyLCgiBKHIdCSiGDUtkZib6Zat81JlrpGXJCIOzSB98U8vuj3DqbDVqytt8gbTH8cRP
aMLQJT3fApMz/RywfvdpMHAmMu3mLSC+l3/U7bfgawu/2L4FbRPNmcCcEHQjs0nqPSTXCcVJmvWH
A/j5Gmd1SkajVqRr+Y+AeaULCY6knuL8uTyPWs1bIkVbmKSf7fMZz1wZ3bZOwLpEO7GIBdiDImQn
Cb4oeuDuIfMvQo/mJdeLFW3KvcHUvi42j5K5Y2SVgvv2mW42e85G4dEmSBE0F7v0DN9ajr+RPGs9
8QWAfaEGTKIQx7H5e5Iu2wVY+ZHVvs/X24vudGS0l+DlXbC27Vg9moAGTFeKhzT73pUyxrU+1O5q
tU9qCcdXrtAlGBKbgHUTb/EgKOKH5XEsTRHh8LTX3HWKX+qLjYCWz1eJi+w+YNvCFM2zQhVIQY0u
+jf18fKESO0tNlyC+Yz7rWuFgO4YVKBTobnnGGD/fXEIEzQ0P0SvxMSvTXXAtTHCCN8r9TA1X7Es
YYUjwRjyGDOWiwqWBzzL5Wg4njO0/0LytPekBPReWKqM/twXBtMUmWWdRB92V921RVObO3/q3+PP
lMUw+MjwcKM6zfcxErH0bvfDppjQ6ukQlHhztYt7JSM0btTRFHFaU69QPdjhrQ7qp0+sAXJFL8rN
3+tSK5/6F4ocRLc3Pr1e9+9WTYieAJyb+eTPxNb2gl43IByAE5z7+x3rJi5TODQ3wu31kY0NYk1T
kbAspvmSp2bmocAJJKu3hB+7BH93Ebuuwu99vfMWcSsmYIHqAvost7FMn1AR/Z1U0UEH+qe11L7K
gW3ipjM9a7D6gLwGjPBl75Ihb2CTZgkDdJW015r8qnzJm1SqWR6FLOz1Jmaw1MjvrZ2zPTw0ZiCc
o9Rf1DioD/GDQ9v3E8tWf8dtXZD1gNhAku/ozMIjxgMAq8xXis19uKsK8gD7E94ussRjXprow/PR
velBOhW939NOCa3RkdaJJFleqpF1brZGA2p8sekWit6nES2V52uIS4dcnaFSvcORZIZkdtULaoF1
VALDAYMM2w7EuUTuRWlhC3jq5u/hEvYGKAWje89sAR63saepyd0GzcAxi0WFujoByleaxEkpsP0L
aacgZpKTXjiUlXMVLQYTSDkKmqGJOxvpRf7/P7oL1erwSKDYElQf9AUlp8lInsbUHpo3P6o1Z6Rq
KDTDbD+7WLmZbEBbiiRHwPgx3NT2VtkZMq3U9BL/juaJDM2cdi8Wv71qEoRVAuQj4Y0OtAJBRu9J
LUdGTsFnq5JJKPlrfplHpRrPW0wnWc8VcshqIrsVwW8JHRFb/JVhgcMne0BqXMWgm+iTWwewiZsA
Sh4clj33Xsj//L+zjp6EJRTycWWobNSmC3cKilkDB9KoUGnmIErq7yyfgmyl34MyihiPyAp7HgeK
LgAZffJ2rVNKZv2v8MVUcY1+F1z7K0ctza7XY2UxjJGXpjcr0wWSZQkt5mD66U8aB4I83yXg7x8t
j8v4rN2hQIVxLs1NX/rgc1ws4MLfXrrYn/J1ePl7STxT/6HBv1z4kXcM3JB3rE1p8Cm294srHduR
4skYLKmlHrj29NuzpafnnKka6l+go3NM/SIJA/N7IXRYd1371iYEl6gfFJTPHemQtAx5I0Q39SvI
j8R/k4nL8pcKgWxmfsXnUwz6rQmua6n3tsaVOnvgh1jxuqY4+6eNta5x7jqjEirc3L+jqlkVFbWg
wBsiN+8JrUCwb4JYMSIS5gF7dvkrGOQ7c0MO6XYSDUC8pUfX7wX0Iup9jB2L0FUYeXlwXoo9pabs
pApCNUd+xnIAvjOax0uByhS5gnZZ8FsaRwv7IyT8HW0za2rpKH3/+l6q8/Fus6teTvzmhGiotHfa
e9/OhQoUsmYbwzPSs1SLK/7ZXm7V4vHe7ECr2Y2Y2dFP4uudQTia5MVpg3Or5hZwTn1+BtSErwl7
vQYljV7OTi0YmfModFngsIiSSbmv8QGa6HJajFdp2+6IsnXa7MJRDKtU0npTkvc+1MRKcv2n1NZR
4yEH4OgmVJ7FuK4mXeByBA1LC4DUVN8HuRxuSQzEDzal7KN+ufPhqbnjTmixadMYogBf11JxNpao
2A+oYVkjJ+vbMgv97N681/Okgw0NjNup349q/VaY2HkVybToBoDuRuy1fgqTnH3C2IEzaM2saT1E
JEAn+/co/9/EXL85FspWYMnObiZjVS8Lkixwe7mbohGJkVfCQgs8AEPPrtohKY0ctKFYSbO+W7Yr
Bi6ZZxHHdEHYpykWhXHyvF3KW3OwmQbDVgE44Pf4LgB5KRuUzgytzNWsOlFBmYfGGJVlgaAW/gv8
yORl5Leq37ZKzAmeMGzRBMHdUioRRHzJmQokI3MwEM59OjErTCzfSaAyG8dzvzMLpOrPC2Bfvj7j
b9b//e08AnlCmp3rNItUcu28j6xhVZSh4pFDnZhaS3WA7MFwkn3krp7ESIxtnMd/VJsLJ2kA8IZs
A//gsoru+RV3fMPcgx9T2/SMQKsmd+MNwPKW9awS012v54F2fu9P8tidUKiYyZX3AiRW7XiE84oK
2SkWg2l6o3OyhCn3Gm0nHGtrGu1kbPYlkWwyj5MtrV/zkkG2EWUcrfMHK8DuU5lQTMP8vZD26BC3
0z8U2KZsfZdWNyNQ8nWOQN7Eko78Hd1zYRDS215F6S5rMaGzb0QDAS8s8y7kUDyvx2hUrEaFdekr
MLFYPNftGoDd18MbSDc2qy/8CS50Oeo6aK0LX907+3tpElBmnJM+hN4tEIDhnO2fG19Kp6QvKkVJ
bHKt2NZiOSuVq5aTUUxg86jikxvaXTG07ZsKkbWUlZg3O5XWhT+7gqvD1Twfa91K03Wd9i8NS9/v
k5XREf8sVMwmT96up5VPdL6puidpKcbdQ62ufq1zGInohqT8E8xUn2/Svbrw4dxqeT/Irufr1GV0
RbLg+gJFEbAuLmpeQbm1iqv5DSJlb4kI0V98SCp3GSr30fzH/dOleXH8uRds6/znafAONCSpuozq
G4ICbu0wPH8VzYQyHx97YQe1Wv7YJ5E6/aRTzJD6ZIpaW2tQn9pwGSzyInKtghkVR2YhXwPobQv5
0RTxgzVnvT/GrZNfiP39PV0z84WBE5AestM1AqsximdELzlm4LxB4a3Ca7r4uLYl0Uk+VTO9Vmx6
icUulDCxxi/B0ybYDVDg0+vYR9f5rAZoHBaSrHIkNRbXMKh48BDEyt35XM73aGK7eULe6e62Glz+
wR8SOW9BHpf1mQFe2X69xwvHFz4KwzFe9RyLV1iUKmAgNxUKie6su7ahavIO2v4g/ZnoQnZ/XPFq
XG5ubUvX8DOgQEqMZnxS03WCyuf+jx1LiGPkc7Vj70bCsDZf4/OmdI3uhIGKLy0IZJ709tDqFw4Q
qnXQ7H0163xy/MW4S7GZXAgqAVq5mfjmhroBxLr2U3bD161EytTVwnbgXvsjSmnt1oV9cVhNeo9h
QUH2Mbs1oO7ClbzXgmRi+LskWpw3PLv345OKB9e8Yc69S1lZTSmfYe4l7OnF9HBIBRlMqNNvXrN7
ulx6j1AGJsIO6B/ujJMiV6xiifmeoe9RCdbCONKITFitQrkcPkVnkaJt3JnzHTtAleQaf49RTtLQ
juToYNscVpKIS0hxaIudXoUeg1XHd5wcVgfGMHPnmU6JBv8ncrEz5OfMB2MPo8L6b2W/Ug5/5IQ6
MXkXGCdiqWI1nfToEGDBX7zAi8tt3rt8pPlmBfJCtTThh04NpkmpdgcdCsKAvfi1RdHbdkzsHFwS
IpLa4gofZhKf9QQNn/2arCAaxV70uYeY03pJY8wNXFouZeorZcmjhGWTk5hRgf80EusGv6YpeHzC
oTXOFSzaBAdciagd1OZqO5Oloj3NnYS9zF5MIzIEGaO1g66rFKyswkzF3gsnmzqN4tI67oPF10l5
HpyoE6gU6m9EoQABSAatgNQqjsMJHBKwj/C3g0ie3oBl417ZuK1L3m1d80y548m5hH5+CrgLc+rV
qaI8Vad1Lz7F+tJAeIzQiKO2p6GvpHFOpAEeObmEXaa+uDYQHXTkpHEQGSA+2cyPftleMVRb98HP
sPOUctk3tfal6CYz30mgDBVjSflQvK4IrfXuFWTLAcWGlTk1GUY3UwtUq3cLTojTnmZGEQGfNayi
a8bejzJwF7uItGAGNlkCfKkjX1KOiks3eiXJtmp0um4L1T4RW9Q4MmMOdgK2RtqHPZDBhk5A+oCP
vSXroQGwYO9zE6jua1vX/GxCgYH+o8He+9wxf7Gyz2Y5eYbMqkVSOQmMphFlElcXT/AKixl/FEQm
Dj6G1/SGjw2jNhIn6NQeIQF2hHLojmG9/6jEQ93gQk8Z2ifoM+sozUV7akEtJilV/KvljlplYiG/
FrYLo8qdVPWVRbwK6inogf85gAe3Fa9GkL3yCdgMlZAFjG+ZTEc3KVCUBcZSoop6KPMNZWhXRRAt
VvFtQjUtQE5Wo7abmLdIT+aC9C2gKOD1Cinuyjr2z0sEQiy09GShms53WAGEqjhFg5XyKhOxPaSi
T8r0mpJzrDRJ4UbQha2e8HSFSlbrXu39JmLIqKcTiMmDJAd/3AN1XKgSVT1w+er7dXVQAnxshK3B
F9E06+gifQrQHdISH8JG5zk4iwCv+tLX6nf/vgI8ichgRcv+HKGhztzHErb7CHRYNOz2RylvsIcF
ZBDSx3QlurarUabvApiSHWAA12gFmHWdsTJ21xH0i0QPnDLuJ+hVxWJXWstocsUGQKkicvwbehZW
pMI5HWd++MCYf6GsINqEZFvGGdm6V5WBAoe4SnI1Ymb5Tsn4dOVCn2QJUzeOf5Ap54jYbaQ4e+KV
7kXFOi1TW0svDQ7FzOBcvAxUp+8guC6DST4+e3+/ydl/q2n7seG97hr5iJFH2FFV/N+0hI4O6Bl0
a2ysUrimbQpBQST0GFY8AZ3AdFPgJuAFvVVzoV5K3/uqsgv/S5DQkztAudc5EJz/7n5nYtkXKeqx
tSmoKja2Z1uQzwWZVFwR1uexG5FazMhrkscOdkH5VZVYCxtE7kzZutp0sOMJi5oMqdOIGW3cKros
S9ixuj0LroTAoym2rx6v/pWbQhf+oymdQmI1Dpv48r1Q9AveK4aVGLmo+EIjfYn5N4JzpPwFHVh5
YePXD93VtTVrUMriHPZYdMajuuqYNbsKl4sfi6HuteO78qRUKyquVFE71OxWcYqFUUrB+o8NRPZl
u8ZUL/VJuX4uBWWnzkehErbfC+KEuvlIvy7R3i9U3V/dz7TOqq2ccc5LE5x7s3JUmUE5Ct5I8L+O
oDPFwE0FUOtaZ3JSCA7lL1EXWdbxG+1cvB5toAaY1MegnIze9Nk1c13xBekLumHOWtU6bOOmy0u7
xfR2zVr6tlUOW9VEgbCcHr+41s9N2ylG4Ma31kkM6G3QKKunscD5nawNKL2L40j+azkUAAB2KAkK
M1aDhVsMRzUOCVBJ2+UAWAp6JkiNI639N+eyA28+QOHISqUQuIqBKDFMJ7FrtIWCasg9/nDJ1MDW
fJjcd7nV9P5wbM8IIIK9xu6cek6c+YSvhYurC5pYYvvmE82gsnYlgpdTBpoJAFkuxzbzOmZMOjmH
EjLkiA94kpkzYTEv52fORDH0IeVuxupTVvQOY2mRux/Ctl2g/fuaGvTXq5P78MnKAFilTqccArwu
izxm0P5bF4Jd5EvCfP33Mt69VmGuj952pwPoB4dO7u4djMxw5p5HkC1dKIbLXmnAY4avJ4LxTqki
LMatGNbOKd1s8GxN+q8RWdHBGCdtwzD2AZjeZmAeZFNVDts5l9ELV/h7137SVR/d7ISyIV49vaub
K00/LK9Iw/qm77XXwJbZpCmU+NHGSN5tVsRVfQZSTdZb7Hxyr7wJV3jzM0e53ZmWlO2Jv4D4hI2R
4N75h5uGsmZWnUXNX7IiWNFhvT3xdy5bNrxGRFOHPm9WgugvuUdZkm+1L+dXq2+Y7FyP/o/8g5v0
GsBraFDpmfkpwoukje5sLNQ/qP3bCCY9Ji2swnffnROUqJuCHWl53Q92x8GfbJ3fSVFt9mkUQLT3
R+eDV3UzAA/WjtE6tA5wEMM3tU+2CRHW+1zs0mIaa9NUYsBy3vdoicCXAGcjg2i1RQ3bEORGs5VY
c6SnuKubn8HwzDh7RFVyi6xZHFuk/ETDLd0hb5Ke9Bv6jkX85KIs20NZFgpFnAA7KbtyGF5Rpc1y
AMacMsBlb1cuTrfnHYp2Uy+q8GQNkXnLHGEbaCZRo5Up3Yf6/RwerI2fOxIwc+W5a+26iGkVrYGE
KLYZhPTZ/SpbQYDsGP3Mw9FVHXxOPKWiyv2k3AdtMCkD2P7PXJiwoJUcBXFl5lZZuimvqnQGOmBq
WfZk1AOmb3iLuxQCmBvYbyGNQ4qc/pkDYW/KG+b8rAhrSpD8+dk8fbJeCraQgUh8ahDC+/kG7SN2
cxu5vQudHUKA6GF6iuwArIiFU8NXgow+V2UgMjrS757V225/wvvRdTu6SWRWu7m0xvvaOBqeF7zp
m8PuPb8+iUDnLJQmlhwJhN/wUkAAJfgXoKJm7piCccFs9Howweum3nar1qXkXmrhKn69Ap/z1O4x
+EkY5eVKsqaVBKZhySwomub4FS/xApJCG8aplvwRCA7FHohEc0wWnmFcOAdwA5XxOyk7Xefln7P5
lNM300pf7Llid15bwpelONmMaQVM4yXqBb94MCSENV/8hF73lDsU6Ud/wFKpNrLFw7GWONlx5hN9
jvFojFrLso9qoeXvb/UNzfc0aXUiDy0BXKDLz3Z4vADI5WPuNzoBlBIc+hog/hKhFwBnzrr5VtUe
D33mgCGzE7pvECMafdJAG/hrufMfEfnamLBmMQ2chsFmhXbjM4+VpiOYPTtx5kAzYKu+U6188fA8
fdzHFh3s6DfWKIgYHYy2IEhEdJIyquD2IuuyuTb6eagFPmbiiM6+tz5aozfUmhyLWhVDnvYwAo0v
d2FSmHv0v+QVKuujphwl9+v7st7D7/+CZuCcewx34HBeg6w5DlwBTBGXst+t8xjN841bkC1FJ67R
//02B+SBQR4lSIcx4wgJjcEUNWdhd8AK0JWj60uL5RuO3L0DyxYOxpN7D5uTx1Z713Sf0/1Tu+9Y
ocP4+mEUbqZDZ1ewWOALjhCODFEOG5LAAAyRv4jveNE0nR2maWkfrh4S0N/9iMRRmDhWxO4wm4z/
5hFIrUoNRD6YgwjUT0PldXCL09KW6T/6UYRY5Mh1LatGsf7nZag98D/IxQQ7DO/20FAmcI1LEiJt
sUwR2X62tXsMriXDpHbG2FJIF4Z5U7CazRUIps4oLUgsJQGheo/vdOAZRdIo1IYi16TduXyqCp+C
h4W/1JNE/y4AFHV/ulFGUVOjH9zWncYOohKgZJ99+HZG6K2TvKwmLb8Wd91UTynTU9cLSjKYohZ4
so3mhZ0HdAN2XeyEZYfq5P90p6IlH71letGiGmHe6TAULf4hWZ4XVFUl3NQzAa9UDpYfxRxGmxAI
KlKTMGl3cMPtzhjVv0U5LQ64He+MOZK5QMYSNqDUxJwdl1D0zpWtq4YuaVNThp2ZUZq3SiuX+Abx
RLvxdlMcy9ufZ7wRE53jLrOIR3OyBP17deL+poX4TCP9u+Rog+uUdYXh1qN6UZtgwDI5/GivgMq5
cTN/i4+HpD+k4RjM33UgGWPMSMHN5VfNIhwT05sk138ofjKCX6JMuqoo84WP5qduX+ZzMrkd26qj
lNRQuoCGop5ImSHbTZsrCNmCwnUj88GVJDb+DJvdvZyNyNbEVkQrmCq+qjm40C4xLaG6H6cEcYXQ
QtdOiTCcB+y7SfydikB1qiyos6UI4W8/HutvKVk0jevl7OJs32JpP0ii3vm75MBZNvnDb9cDbZQt
nO4k77FJG1CW6/We1KhoX5wrNyYPIfsmVa1tanzl+uhffmy5LifRzI4P+j93l0waLte374Y+hn8G
1pQwdVHvm7wOjH9DO9IpMMdDUOoS1WpPRgIr6BXSP+8toW5lBMaWgp4Pzwa6K0D92LdZwX9FFksX
kOHqNOT0VfaBCFpN7aZ6gnccmXuk8Tb3rnIckaAyKW7eun7VWq98YsfXiMSnxNE9mgS4q1Th68hw
S9BpSRWtossYhNjvDH+FZzZfuul8h1gOdb8vuq7uWIViCE7adWdf5uFTnQVZ7x13U9+jS9Ce+Cq7
A2A1ztWqhZk1uGb1Cmvj0QqOJK4Jfm7RP0ys2qqgw+beuWREJ8ISJzuw0zJivd6f0jdV8K5hV7MG
q/m2WFthjBCQDlIq7+3l/VLi3dPxsVAuGtkAaIG1r9gfjgWoc/qDxPNxyNIwdbr/9DGua9E9A3J+
vA185aPDErfn3laE2gcu/BcOEfLT47jqSD2eZpPZXp/+Xm3QCt5GYMJjZBEvIw8CJoiK6whWabHb
Js+bstamnS2c9YE3bCc7xIj8Q4jctT8AIOM1SyJ3RJu+i80/tI2VfU92n9LuJPu5PrYJLwmBKWyo
/SXvkC/6rTqN9gcO4KoPKsQpgoyuCWbau6PUd/O3nUt/hT6aX9VmUIo6IaLI81RplnXtVlU/+EMc
vLEH7OF6NqVIyCCeYXfwh65EINmrfQPsjl6P8igY0G+5p9hdHxTLYSED/urmQ7agrzbzSFdhPPOy
hJp3wj50OGXSy8GFjpCCBGdvb+zJvSjqM+UnU5cPTfd/ErJsAvHiDefJImrXmbXUvHhsdiQdFHLE
g0tCZAbGw6xxdfsXbwo3Ux4H4JHHEpzHiQPchLCaPAM5W9Nj+coLCAXUm9/2wpW/J43Sh9HYnjj/
1tyC2K7aByXmAd8DDU1K1081PVY+NFUm0bIJ51YbdVpxF1L6af+NUpSWVHhsUy5TFUgRhFOSw3rz
dBb0hMusi9KklGdVdtqwwNLdnE0yPE8e6m1e9+zDl9iPmRDmz2K/rzZ4asLEl4V7ABqJ1t9T1VZ9
An9yxBi1gHB0GcIVg+seCNNYAdGjKcBx2+WP8NTHXyloWo31BApAnJ4PxsA3B3E49IztN3fE5Tn+
gIa9gpZgZd0B8/WgjHtGO3KEVgKOKkVGrTYPdIkJg3rd6sooo/Th2R35KxHbPZZUpgsL1MuX4CG7
3WG023VHrI1GOXn3FXbw5XbJQ9fU8sc+QrkCojul2WQJQbiH4g637ojJ8edk1u5AUkG+kuJM/MxR
TKdJbHIaSVw+MMSKn/nErajgz8eETSEt8fWIswSC4tIwoV4dIwW/v3gUwk1R+WstBU2WlOnOCVlD
o+blW45k3uZuN/2fSoUkZ7s52LCXVNgNWrchI9AQyG9plUnUq0x+BTRBXW7vDlSfzRg6zy2WH3OQ
NWEuAcSAnAtsmUHi8GPHKlhkM/uvPAvsff4WKBGTzh5TeCXuIVKw4oQ3qxlY45HucCdHgL0Kk+Lo
Oov2Ci7HxrIexYY0EqrYxFcOU7YKXNyCmNTrE0C/HJuhmeCFmP+ma0+e0xNxhIA17atg9U4SRJ5e
ZKtVavCHnTFi/9RVM9p7hK14b8TLIYyPl1WjMaKnLLWovvNSxIuiTJ5tBh3CDYKqoVhvK0BBQSD5
PZacfHUJ578Up0vGbPmJzRABjKFDa/1uoEXUvCgAbm9prAo+vQA1sLq4xj2fzLczipnaffiFP0Wz
THH1j4fr5k1rGbqowGZa0wgYIOLQlDzOOPJIoKwZKvR51xEltgCY8iYCRSuGVMjZ0RzbZ2sKd/7O
kmrAsUKww/rK5H5XOMKtGWT4oR8a0tdOMRhxKAJJ060MZiNq5rKShn9lUaCmUmSYy/Tiv0R2Twwj
hv3ZEo3z055mfMfQaUxqpmWv+xpY9hzaaEuH1IdpwGtzpHnw8HEwuanX3o6qnpbbvUXr/5qLYzVK
O02Sxs50lCB2UqNINw7Pt5ABA/UhvieCQnn0RCyo/86qwMDYw+rO8dtxwEiJHSQ89D95W5y8c/af
DMX6L9aTyv/5SegmMDH7393j8Gh2IUAIKTULDyPiXtSkM6gw7nFwB7VO7ZKN1pNHe7gTRpTnYN7P
y2/GwPntj98HaX+19EV4oV4oZnYF5HAWfygti/HGtwPEKs4CYeAEWJzYUm7Uzqtbf+BM4SMhvxIz
n+72RKSWgpCdGW21qidw9g2LUgoKsFyI5GNEBj/yTNGvE+9XIuSbIuw8/5DRELa7UyKMvQcLgWEn
SdxIyF8QEvme6QKxRHlxab/CeDvXuEdmdD6h0u6qo3IoTpIF84fJMt+7GAs5xG2X0u4ek8Na/tq4
bkPeOgHIj8Y68L4uWqvdwgatnaNmmGiJYw05o2a8aRK0jSbsZMkNZypq/mdNDj0TBdVEM14+mGLO
JFRBCLhBnuX6zQl3HclgLzA43ruOVULLmHBU0C/vmNBPnKIsyvLE75PzOzq1mMcLTc29aCaZyT+P
U6JdXCr1xqiVJpU3NDjXJ16opeOKXm5M4/2x17Z/pe5LlbziNdkJwZrJVLBF44lLpjCvfvoNMYzg
7AvnRNkZjqYT2TSkt3SW7p0A3Kiy9T07X9Xki0mv2aS79RmPh8hB7iSnCicqApSzjfBa4C3sZnUU
14GdQEZSt/Y7MEKsAFFl0Q4W873Q5sVWSwxr5lCE8cFi4taBjvdsdHEIkygEIVecDUT9Kzqko1U3
43CUUfBbmGvcBtRl6jmrONoF1bjIKX/ZbIQpKHmeUWQw5waUqAEOqb2FS49iXeSZ2nRN/4ZPbHgj
Zru7z/rLnlS8k1UcRB7B43k4YMCgV7DFTlzmTwMQEsKkB4irczVOd9nC/0G9BkTEZDY46BfmFYLA
7Js1PoaTda/XXx6l1ySB0JHZH56pvmZASDg/XyTEbkcKlK3N1OX27jV9uFuCKZ2fEf4Zc17k0IOx
VrMlck//wn9WFTO2OC1oxCdSw7+Ntm8USZz4fPASGmrrA3B5DWMgSKfN6wl1hZdH5wZcPVzy6cDJ
9R9of8DM+jCOQ4dwTZyx/BbJ/U4G8pYbbAycTK1v2+TLK9rlXMVvK/bXmIdNiXvXqg6igsHG9DSM
7MHL41NX6F3XJj2HwmVR6SLjTDamerm3aoSGnJmjr6i5WacwsKHkpAh1kb13zTzbrECFHvcmJhR5
EMnhXzyYpp/5GuyfeTzHa+x5M7IHVGDbjWSqoB7JISFD0jsSMgqnGPZIBwzbWVO4nyKg1JbaMruo
dV7PPGLYhsvddOMMG6pwsVh356RMlwnVbRH0gDPTjR2v9X83gREdVa6yi2yFiN2AbLMufVM0PBc1
k79m/84lCWd9iMZKtvVC24WQxh1/36bsEzaJ3iflR1A5Pz1A651CrF8LXIKHJoPvBSYXVeZXJSDP
MDJvXhux8Ar50wJQWct5yN/3SxoGwJ/ESIQYONu1LMGJUaYeA5kjrZcYmK+qHo0bFApS7irGuTZ3
y4HfAYqX/SARVA4DecAAr1X3pi8zLoZ2mz6AhzkyyfHCzs/WZwds01hYzPAcTYi0Gd/JU23ewhKB
NjafWBUsjK9BXy9+oEVJtRQKIJQlbmM2+tTRkTH040BBBiM8UtjOA8O5AobVg/iGvdIkMQO1nBrL
JeiXmIr2V+tFLqm51WdIyiW3vj+nPaGeZ1pStzKjGH4PyINE2OvBr6FcHzKKSbLI4aU81f7S3Vf/
K5Q/UdN1CLEoSImXrp/F6zQNy/70W+4fk83f14ofonjiOY1nCCGXi+l28fnxHGmlbZJC/3z/vmtK
Xa9NNZz+p498BdHTr/HV2Mqrp5lZdBQ1EV+tWd1ZhdVCxR/6QbdAEJEkp+Q8vlAfqJLUZDS50eXW
vrTO9xWN1Jlei7H6r3ecQUg5MNlrzBiYP/+4hfiih7DYJguBquLkW+juaS+Cr+FpNOXQoXpsO3Be
h5fX1UfwbeZixzjiYpmGKok/oI4C78U86s810zvo5ZUwp9eZCBSY8eE9uAy7B3tNiAxZS7BQ3WPb
Pv3ZEyZjWAi1tRPllZhsBGQJlxhReb1h1jJ5zyh9t/gof5DQEjIFZM/Rb4MvSGUNQre/MuOInjRu
apFUv81C10CoSmLzt5w1nyue/kFSPydWroYLXk/6fMRcql7Z8xbM0aWWVYORlsjDKs7qKsrs+Aa2
Dy+C9tS+TwqxEkulPSKh3fXc6KpGe00k5WH4b0JdySv89z38hoeTKZg7F8AhmmZk6MvK4AteIzLn
4tKXPf9ICNKOwYGZ54QdtufDgJ634nFVc25Y5+3cauX7ADsyt6OWtC5pzgtPE2j6YRpODKdkz9PS
qQK5L5xSOtxvqxJXtO6V+P+QDC2JM6zuGiQpiIHLyp6FG/6jO0QHy+xpSYa7yvEagEkBp0hH3hQm
VXnM9yRFVm+aibq0EwQP0pWk9NRFARg/DwRQ56O8AtZSiymEw/fqzkO5LQEwGIKPNhE36DDne5dO
3X0/bfBQ6+zPExIJWZUz4ayzVkevXdNgE4Nslo7fu94gZ5504b4azxnvCHg7LEP+nTOgtYU7lwmk
MDR4WqhiodQ/xk35SfQ5W4KK4i9NckL73Fp2UeoTVP7DMWW97uIaGLY7ygPTWnWRnCOHnYTtQkvQ
bUVGznUtOW1C74H+Y1WEaGGR6yY0IVwj83PU2gxzFx4qmFuyOGNVmbmM5U8uqqyE9eBNbK6U05p0
ksDE4EU607Y2/A1AiQoIbkRSPUmlZU6tSJXlRB2sMbkc53UFpo+EwHy8Af/wIvuVe5O2dQGV1bp5
gdX7ibz8uInUFrgCWabTrWqhrkKnXAFpbxfqoWgmrQl2fFp0b6XVKkQRzea52sNtuO9810y7XBhq
qtLX0QjjKareKAOlAr7lpBraP8dEUTFqnAL3HJFTRldrZ5QVnmK2R8i0HWh5foT/CpjmeC9z5Sfx
nPmuqfH7soVnnt2JB+aCMD2DQgaMMyZ8YdV7w4xP0PpA+XCx5DClzhOLXpVk49Q7sQK/JrDZgCOd
25i9S5iZeF+KaKVyKRsKflFBhdblyuSvmEAM5s/EegY/LfORVpmPn/ccGADjn/hE2S+P/0SnZJRY
7dKbhDRbh9WjPDTylU7MaMqH77dYN+gQmXgNYoCA0Cq4RY6ERlLSD9wMLwqWe8yX3yVYpUaomSQm
5KDSw+8T4FA3a1judva6nGe2UBtUvlAA/PCRxR2fHl70nb7I6jxZbaSR/BYMHxCElSxDux5qlyZB
GSVJnxPp8pawXx6sTyKvkwIgQXa+kuMby7oYgHl4rnCJs8Y+qXkPX7CdOl5LHHzXO52EorPP4Q+T
q9XJ5eAzOfm+R/HfC95meigyY/0qyu9RkrvOwfuWt3SNE4xfP/hS54SskN7oopZY7m0orqxmvBO4
0o24hh4eshSEx8BG4Ie0DGhSMDe7MACHiKSYDkti6qVQtPGH0iUDQHCAkr0Z46BRPP23nIT/2b9Y
YBl0FEPnnwKfkFms2rIkNBx11dFeml52ATwK2iKtInvHj32Bxyl3CsNOyBCkAL1goCNsdLxEdPXs
/h9nMR3JO0mLC+v6xWsyNUhCcqOvLaprKXvEXLfw7Me7B5zqt5Jk3SmNkbpvCnXS13Hy6lCd205M
LuMBYsJbaH9/a/cne3gNKKdRcKT2kKmCmpcrzWca2dSnGteUSFmbKTJv3RSm9MAl9AmomhCEQWAC
3NZ9VkQ9TGYsHCIDqcp37EkGshSD18CEMx8Plxshvw9j2uvcDdkxKF8Ayjllcm8K/u8xRrD+jjNA
9hHUhtpf0yvqyRTVZ92KWbS/yedZbUarsBUxwkkf2d4w6kM7WaSV6rMjsDmzbwnJvAJHinrosmr5
zEC9w+gK06Ln5I5EfcM93s71Q2TFTbA1IewTx/P3VxBPQAkm9PwlT5yexzek2zmUDWeutHHdJmHv
i/JxAolFggEPWLKzYsFyXV2GWg7klbt4JRzacrEkHUKzvwSu+av3j9fOTPDVgdegIyeGIRXRvqAv
p1o5ghVVUI8S7tkpgwlhrWLWtwSqu2lYzueBnlGk8p7LHV3bB78A8hMeZ402Y563p8q+7eieCsFN
NwLyMaHuZ3bpRfI7fdw5GzHzMcpKVRMuG926ZRuJ312AF1CQX+4lYkc7q4S3sm4ZM66uSNBHwLP7
0gKo0xljxMLzXjqn+4n52J67nWooPf8PuzQC7rvrPScJYZx4SMMgcu5qSOUJgRqoPsWpcTUGsXr+
U2rumTHfiOR6QTiOC7sDStweAFLYv6ETU6VM+r7txoMcUuxsxn0clz9zIKJiM3v28uUcCihXZSB0
FIWxfnAcwil1Folh2O4jEz6osX2HnbsWOTCo+8i8cLY1bLkPPxUjkaZPsEsLt4InHF2IBvnxeqfS
oI8GaO5Y0iVhwRRD+YJFyyGcgwhZrSQ+syI76oasHMNm3kTQSHxNTtgXPkwz6OWMmk+wgj3q0YAd
H0ScJdJBLBtLUr6W55ux1AvCdMz3nLI32C+jY42kE6XaLU5ij1FpRzztsKAPgdZgKhwKH3y1Nms0
z0HIzyIZM1w++Qkk/hnPsS1TorL/FGyBb4llyYDHC0iqXJSyu+u/FT8moiaw5SsH2n+cryAMsuJf
cePgsQCa7gp/WTtRwHv5fO8lwi+CABoBICZLttrwlW9VkcFOz+vE61q23i9pwrRITXE8jLir25ZS
pd9nc7cfh5mpCadYGfInPgqvd4EUg/T7ndHDqnwXT/DTtD+xKfSj3pEjxXuCvrDVM7VxV5cvic1o
JD4sCvFqrr8Dw6jiZhthLWAwEXkgmwF77/D49LVtua0kL7t1XHgURuq8PYPMbm2JsJiBbQ14jdun
IYtqRoa9+cfAD9fmsDQC5g5s3T1EnRchxlm24OZ829GODRmaNLiuFnksytE2a8BhAoqFavIefZ0+
TeKmEBXlfKsyAhe2OY+AtWTZ9AmlT4iWcdlUa/rGL1tK0arkcb9OI/z8Ea8N8NANAkt3nb3kdweG
qaJ9nlbjxG114M3tzdS/Zj2V71NJsbvs5V/e2NYnK/MWqjP2AUHo1YKF/pO/WuK8mTkL/iCmjMvC
FaDakkbYPeRLYxeX7/7kS8heGg9+bAZ/uIK/Z+5R/oJp7MLMaPvACcnrT+cSmGwCrX7c+rSC91td
4jo4xfb/bHFfV70ZCoj0F5GpqP1tX2Tv1zdHnv5bEPXrFLFC3+g9ZQ39/7tMzBIk1ETEog+CqNG8
pA3tQspNNorkM0E9rCY5/tXLKul0dlx4Qxhxhfob0ntQBOiPvuasiuklkeYcYDgv9zCX1+VFcHOD
V3lrtYarb8Dv7Y+Ed2wIqbv7lXOc98/ICrqF4ZwqS7I7xOXCKcxAUgbxcSaGzfKASaCRNcJ+7Qs4
Ol7lC98MbCdY0GkGEbcHW7Ryc/XuslY2+YrGlqegiUAfLeWRSUgiI4hgZW5oR1WEZwxHfgBhTWAK
ByBCrbQPlA2cNlfCUvb6carzkpkCSCaWPi4DVRD7xWHZ0DWY47LdGDijsedvoKI8onpH8HKL4J8a
n9of4WSc4ogpR2kp1jnUlx4T3l7vhygWev7pEWQtZkNN1VUGXiX2hDVIdP/88aR2eKsbunRWeCin
hq2X6510SGeSA0HI3+Eh3ucekCTO0Jcq3uhfRsiJVwYNVEUxYEmbUxNxOj0C+1++/phoKAwzOhCf
e7mW/3ZpPItBMvb6PrY4j7poA1uoon1v5g321fcLQjVcsuF2TZtMza+DBnJddPrCX0tKfiqBya4r
Ust4qaiFh3T+5TTpw+zMQkGvxc2cGf5W8HKXlMv/+XDIe05Uj6Ikq3HJxzo+/4HtIJH2DDuLeR/o
zTxfQEOLvMTZIuR9F4ZUj9265YZlDhl3gaqTLE+bKy6HkpZ1TyegnjcDcXzve+9ahF1GgyUuaOm8
2vXz3FNMoTemtzOl0/pwjESOdtGvwXoDiEnLgw1vL4E1WqB2+Iwt+pw2vT2JFElA7eH/Yx1XryAe
fmFRU/oAVf+ieBgy7m9wOkHNQG63My3ca+SDb4b3zuxFSBXcleOdT6sdOEQItAKHzlPlqolDt5jR
XVB1YeHzbZVAZyL/25dpyLhE6Rzt8+D0wSi8ANVF3HkCxiZrVRlpo7K+SrLIMoXVnZjcIzKka6IP
UY+hHEiER0iZJR5WKZRygONFBFrDFcBhgBWYIL8MtQouS4PX9PBUfWSr2rKpNpddTkWlKmE5bSe+
Amwcw79y15XDeHVUUD538VsDat+G/QhhQdr+VwlneWJDGw+LRF+RXkM53e00lNr+Z2ZiFIjNBFY4
+bohIdNRtULgUJpi7LIlhWAEKXbAdIQgVOoKkjzjZMfYHdXQcDc0zJoPj7Pk0v96WbTOKlQVkjMA
Pb7VT7Mjhzeuxq4YjrNaflFnixKX5zLRjByPnMD0TFHhfTtulBSVzOVlylHydUi4blfYDf1rToBx
wQQ19waWiSfvMqxDo1tI7hG4/BFDSFABuHHDK++SMwz71U2M3eWbn5fbNLkHmJbcmClUvomcrqBL
B4lwBHC91maGSUq3ONKlzU3CsAjwPTXmcWK533EhDmc4Ezccn1JHiqwgXoGYqpTbY00FYuytpbpt
6V5pKKdjnAX4hEq/MRjo/AhKRbvh5d65HUQQHagUSINcVyBfa/IwoGCIrUBgYNvr/hFCXcupyxmG
fC4cT+HoNSx6+PnixoMiS+QUJ2g1KOgceXwU5PN8/PE44+2TleeIyAO6uaoYbomkPFw3d57zhdFj
5nSRmCynjSrYF562VxW0SJ72eByn+EZ7dFuPANCbNW99NFfi5zJG5goxWvttrKaoPWZEb+/zUwlX
/swpSE6qaCnMPkd6kOyFhfo1QZp2EgP3Z7+GddeIUzBRJajpmcATmsX4ZRaZs/cmk1UtIdgiaav8
KNN3EStmQ8DQhUoRyRXzh7GCTF3G8v7HJi0GHXtXE+v+bMdzzMPs1XNduLnG3hS+Z80vbArIt8Y4
CJqnODH+IosSDdphCVkxKxG9zHrQv4keXQsZoSVnyTtp8Pel7F3b2DIcnJnwvdFXeGFhbKEHIIPq
7P/3aNfbTiOH7J+rv6KU6AIH+IPehk6uDaSDI/Tz7NcRWLCCs9hB3gQc26je11Hc50ogQ8L2TLRH
lg5qNkk9KpD55K2wtvrzGtFhZXGXwFXb7DIz+075zGMud65vhb9ueJb+oLskNvQfpJn5TXNZ9ahA
r31Ul6MA0yK+PiVxHM0nkHUG2zXjhAcyUo+6PDrvBrLg99CZSypq2Jc+KD2Lat2RDrspYTeWeSAC
Wx1fR+IQpWzrI410OHbtUg07UIjlyG7zS4IRs2nBLFTs7zkue52u/Qlb57lD+cwIGzTadLRmxx5c
buwxyAtaeDaoGvXt38lgoIc4MU/XP3oJSazseGEPVYX9sL8eolctpcstqbiVHOn9FMPuA38ypOw6
JbsXnYVfIPF64aHnt5/RheQ8KHw1ZDIkMq0D3Ue+48cR/Uu6I0P2Za4+QxVb7sRNio3KPEobQpSU
WXiKNK0HRbryi9IuyfPb8zQ6VihMCTUhqWAI3BJh2CjdOGN9uKVxtjSFdMvdKIteAARpGDR20PJw
7RTJGTcel+amHyeErWXptkywiFK2jY4EeUIUOoCKq6SS+YDg0SlMicAKHv+eAzKEjAzb3zKhKTTj
BDBmHtr8CHAraWCw4efkeQAiEILNeP+1t9l4Cxj0DBcTp44vIvcd7Qi1vwjzuCtzyObF3j4WbdXF
JdsDp2tp20ePFhZhwJpC0MZhFZL+rSdFj1cc4hYvi29RPuHV4bEmVqh/z+wgP9TBGoLHr0UMDvup
Xcj/yp31BBXyi1UEbeFtGTTQ94ptQzIhfRj223nyO0SMem1iq3XNFMHoHrKW+FdFEbOTA1afWDHW
KNQrB9WO0Ay1S0DT7LU2KYugHGdIDVRTTtjURW+V5hcJf6AzfpMaRxRRx3DLnD/f9cpuKzDwNvdm
evCFz7gDiteq5+aQ2yXqPfPyAGVdi7a7Yz04ZJ+GqaSKWZqtemmYcIuNJ2zMsN5QxFVm7tCre0eH
edOPCbeSswIXUe2N7YHB4Zgi7CP8tutNNcs9w3gDTyJBMD5au5o+DeeqpOdE+0GKbvChtujA/SVX
o4thn+PDIolKRln4GUH7khGwOxb9MbAdooW2CfChDldibrFjMkNnsCaKr1M09cxAHvbtPjUKUDWK
Dx+RvuGcOV3U5I1/y2ObugiX8EEmFUjexdEQqaapI7JqWhZc7bRsMM/s2zbSiQTcOUG1HVOR4iAt
nkj6B7MXoL669e/kWO1bITVMu0vwyYYQxo5cF6s2sCpT/fdMN9EStWP74qrtkWbZJlV/D5vOLHc3
hYkzrtkNPxaYMRtx9yFtQV7l3Ef5PwDeIMmeGzmJg1ZSi/m9SOyJVhpisdfLQz0zJSqln6P22szI
qF8ZFFhncAMBUyc8TG681mKW0SJqgf6v0L4XUOADojvhSTYe9Zoo1B1ne8X5W+BVeBYndhXUTFku
/Uo4nLW13VMkEsUf36eJ+fbh9oA2RUOjj59576JeYTGTTJAn2uf5ntXOeKXAR7xLh7QhMQH6MqST
XKXFkVE1oTCQMlQFNBfGbYkApSP2mnXEzB705swnMQU1LOPqswXqobM0EsIEEVyQYtQnmHIqXe+9
V5fbLgNTL7TtKmKhLs1QMoisztYGtkYbfo4NbeHOl7rlnlzGTSaIvvCOz5M0kUwOBGJgcHSyeyzX
GFZw61EoypPorvf6rVYvuFLO/LjSOg31jdkKarVhTfOHSbDTfxPlVswSuVIeT5Gl1UV9dHZnhgkt
7gAN8JkpGNKnFGkbsMT3uYRJMFLNN99pOkhWLR0Ab1PDltzZz9Kg9j52rDO3e//NAfFeI7XUgTRA
a03DdJ7DK1yJCn4qQfxVHh8YyUkCGFzL0ltOOElyv1aXlkIugxvF9UbpP1SQoDK2tZ4sEYq3CtjO
YnNQxCaMs147Yo6CnfW0eF84SPK6wTgZYVVf9eg5FKk+oFfm6XKKXO2AEepXZf5EOAFiS/AzOPVL
pxIOvAJ9SKeUVy4/qfYWVLcTOGrny2/VfqeItHtcsAlP/Ww6r9wHVh/Xsuaymc0TMPW8n5GWXk6h
Y8cYDB7OWrXLgovSXSqQk8d2ECvEvMzS602d8thWKECieIv3PMiVsuGBONLaPuEvIuhhk04qbW1e
63rkz4BuYKw0hjR+B+/vnFUIWfvN7j4XBh1WcFkEFofWRYcJtnX0xkykWkkEjL4pk2JsSmXVt9Ax
AyIYLTxJkoEbaGwyVPd5XrX1DyXJ7mEkm4XrkLWFey2M3bP2dgfpNfFmBZuTk0+m40rN0TLOTMjd
CU4Srd5HehBZsy7nTYME18AjwgEZOUGQs9UdXaFo62na+ny8O4/XZstTCpaQyfHo9eFKcqFvIYWS
EXFfgXrzCnrb5vz9lExVZwOOMgSIA6/RmCQIHCY8mZtP0uyq9NuMxD+ouG2nOXUO6wmx5cQ0Du8h
kkbwe6qeHYVnXBRMHcRuNnFbDKsGbaHhRMfoR/iY+aTlrjjCg/BfGmQW1Yo9Okr3uWLWm2lKbZEK
hwrMryMCEN4TrflvTE5q3SzQQUP3un9gTw+hfphruUyhXoHx+nOzgY21Kb/MMBFpJfmRFAQveNXd
qbhdMGISMUnnIfYUZxcNXSxt+aG/KJg0BbraMK6PNiGhKoamI/dUWOY4INWNF8+KUtmWBv0TvpU9
VLMpbvm5wc1LzzVIg/LAAQDT+eYxr9AmqpI4iVrHNfqxcVGclLNuQbYbK0dEPIgnpuZAWRgE+jEp
wvRAjKV5ZW0dkUzavKPQLOft0HwsnPtUMwfC5cwGJ1tIRZrxl/2kZCiHobapGM+r9wkSLz6LgkHn
0c1VPO2p6vvrPz58rMZaBc3tar00a5m5nUXcT+0mlf7nUPgaiOhPuXXEULxiRG0Dm0dqMjR/sSdB
TDyOJQXbNzpAJoxKsXSRolz/e/lk/Gg8P2mSiRS4q2RDZ4JemGY1gDP021S3POsTORg5rlc6D0VH
Uj/EQHR2DStmmzfS+TrkulkgHIXYZJbfMcoZq/ywd/s4DEllFBhQe5knHJgFEwqYHd44VF4gHbW8
BWo1WNej+bw2B/Ye5P85UlTwWTcPOsexA6PFpOEgVHG4/BCXyr6e3a1H9dw2gT4kTmcEc25mHvrq
pYQBz2AuUGMH0LfWnkoU8uwEwTIX9ymiEJ8xZuLwFf9zPHfwoRFlqMfwNHh6MQg8GPiD5zl5pEQv
1R/AgIgsrJo1pbKqmbHpzgQxf+7zOKPDR68nTB9e4tz32hEWHD1Nigwmlr+XEnNaYydMQ2wl8bJM
yfNNijYvi0NbRRimLh2iQ/pG3kwVKk7eoX03+vgADso8D3dTdP010YjToOF1Mvtlt2yo4k1/Y/br
kSoXZy+PiufXcdLXUyqDs7c599tPXQakyASUdpW/gbG6pb3KWLghdmtj4a6kdVnjlDJWhm1m9edy
IjnjiwK1WLhfY6e/9MMSD+LCn01Hook9sZ+2PyZ2gyU47+FhrT4TxuuNgjoIMcJN6NEhXZiS3Q/2
NG8v0+GIF2FeCpV5a4INlI2a90TE1RGCPvBHlOzOeqmvOg7ggwLL59POEAIqWDi7w5hhjLkMsr8T
6mOMlK3i8zpxk76frWVs4FPzku7yoz3XW73IkXgowGO2MU5V4wIII23+DxWPzilUMosmE7kqkPnv
CX8N0GoP0cx9gfys3ip7WcCYXk3tEkYcyVq735icMaCJjp+rWCJQ8R64WEVng/okrdOR7z+cOPxV
R0CRIxG1iwoJ/bsQuocIU5vap647eUOCUah27/8xSlhCS0CmITmCFRifl+G92NtAAkj0OdaQdn3P
nphtAVp6CQVsGWKmBINw7VmXu4t9dKFGWE29X6EqrvkQGFMhdnnZnZfh0JnSRhJ0C9rirXrZKId8
jwjuwT+nzHg3T6uQFb8RoS8qOL2YQW+ShnC72UjmZR0VymL7KAeSQSjkzv0ci9cwR88Do3agt398
UCx4142cFCBs000mpj3NLI3Obqn4AR7oLN0UZk7Jax/HiJKt9XeKcHxfdVlafBmmyCJdqxt6z7Lv
VX4WHAmoddDjiThTg9z9BMy6zvCfNq1uqTPHnpyqC2we0qBoEvTMIY/hyaA001EYr/CPpmiUinqG
2ekf4H1TCOTYg52GE30OEDrcBgzkw5vUkNeIph1WVgqFLgMbgG8L6LvtiWrN2Ffl2NrlKbyOtRzm
frvfORWd5uQZdPTL4lCYPIhQBR1zP6ywxaoep9q20XGUhCXfGVShEYGRl6YOjWs8scRCKi4THWSb
gAPSoSyPuJfFneekcrX7NDgGRCRk5b+l9GG9oFvfr1hBZHTvlFMaxar/RbVzb8xDCiAD6iKIn6hJ
Xvrq4m3QRpDdwX+4CVL75vwXfWbsIS5aIpzkH4zwojx7z0w3845lfZa46r/z07vUONUxENHO8dMq
kI4prAEuDZ+Ro7dsP7L6usJ0V25/NpFKWjHif3wdnPLnAxdewAqU/QhXZFgevi7dLUiYXnAb3pgP
JBWu1BRfX942dSd+ep0omx3s/EfGr7Nx9Uptz88jL9zTFRIMjbsu5B65y45lIy3ckcU0Pt/XLAbd
bZ31yfmi7SPd2Nzizv3A5gV/7Q7PnJgksouE4XBJVvIee+Jh2H+ozht6za4DHazAK0+oUB63xpYD
Sb5Nkq1NjHfIVpiuTGFZ28Jgm0lDmMmgWGv9lhSZjMlS8KZmw1CbinMNFTVNk6zv4qQrnjxf3O/s
LLBexFUCozAMek7xJS12ILvA3ki8/dNWbthYWgboK8Rsg2+VdtZ7QQe2TsX+vwiE7iYtULcb39p8
RZjtVT3KwdXPFgG5BEH8fzu64L5104sKdTKFMxP3xClyCMJm5+qOOZHeOfi2IAxBm5XZjzEAk3HN
BDQ5sWz4BGq5d3RRyuEtvJlXroCUvtp859NpUwBqhxdBLPcBrl2jmjuLD9iiBAeHEEinPdZiEnxL
rXKoXATHGIvDLRf1xn8wG3+uDxXq1wlXxx3R80MRcvlNwD3VIT9N27mASb8Q2xKwXyDDIYeAfav3
vvPHC2afjGy/ALPrna8uurxglGxMl4mdkdom62XgAvxCfcHRtUMBAp9Iu4xcIJc2sUlcEgnSbx+q
rwnSN6BCR1qqxjErRlbJu582SVxKzcdDGHM4avrC/y+AbcNothJ/GzoIK9ObCCj1igYPO/PFJ6Gg
Hz8wIC3ef36g07A7pJEewrqrwYm1rjiDA86AToCOpV5Rew4qOmPt9d+rBFUj7T3czWb6ZrgiNngg
epE/miULCk78lRE7HA95cpdI7zyAhlq5Aia7sSAVmnv0VPZ81TibAaVhIBIA0I3n/IR3q76XSB1k
Zg+SqZ11R1VdVw9cozjiqfuY/Pba4zPHLl/cvT7gJhi0ZEx+spQkCBYTgDKyCodgjRTDhwswX8Ow
hzgmFndFvMWlq5KoY/y64z5WAjpa9qihoiRoFGqAhCt5k7bgPcq6G2/SmYAUT+FTOQfPpC8L++4x
AcIB2vAgDmfY29UKGFZ2ILFyM+iyTZRL1DYTF6gxDTL2wZCdr/EcAGNXHRZZOzeX2AXMiynq/VeP
89Zka9r5n5RujC2SHFEj2h9Y057vBvd3pgRnqUt/Mrz2yDq0/zxaGhvEe6Fle6j8iA31pWMNixfA
jTWBgxHXmFqFehWdxm5laOHvGxs4kJWDQUdTrn5difcbVMQ3fga0P7JaGJ6MVp3qfZ48l9eUpZ0b
Ei8MqBND4YSNHx4e+6u3AT3cNVdWsNmiFsAggm787yNJxdiMxPLOOv6+D91VvmjbjK8Y4ievPyZ4
qkD/LiQzwQsdCuHiRDGVYCKmhV2gM9MTl9AGZT55Mv4i2iTR+d38AMCxPDcVHSyuKnDclADxzhRz
tIufo/+wdfpj3iKgvZgo9ZZQyBHA1L2gcfCfIX5HfN2g/dQJhBRI7hhQsY8T4P8IMCOQZurVO63i
NP26TVeRwIdvsMPZSfmR/2JP16t/epzgB52MaRx484E8Cc0haLGWKGd6Kxy2vj1F9zTanLb7axOp
srqG7vGm0421BDfBojV+npkQAtnp37CKdNZnqaUvAwEMCY2QWEH078OUrPHlg2iq9qOWRiW0G0X+
ai08ZC9uYoetdQFFf6f0NBYtMRpgAppbBFj9VoxLMA5yIKAWyQBA1QAGaCRmQoetkVHYwmCb8ixe
h5Y/o+ZVNAZEFeMdcNqci/zQoN3uZXYkB/pfLUHujNDK7ZctqeDCWIOoVmsFPP/BKaDIiuKaiGBt
vGnAZJ5heE2XvEbqI5AR/Lak+j+ChVoyaFHi0A/FFJqRf2BkDTEaFBqH6KD1gbcCHegdmu2IT+8T
7DftxnO+eBbkJ7KTt7wheqZx6hDX3nZU9LqoSPkOsRnpjWWdR2X5H9HCU7Vvr+XQ+0tDaQkHOjr5
ljlDxEbBUhLsFNrR14uGKR0f6KFfh8BilhVyrQr48BQ7pkPQf5EGaITUK1JcKHcXbH05Fsxde/d0
1oAibOyigNDNsegqf51WKoL8ixcjeOFq5n8mbZycuj4InFlS1VI3sshvwSg2ZxND1HE5j4MKaZkg
j3YFV+YFXfcswVhcY6u8ihwY/uWyUAObNvzTaawiHvLOnXBo+k7h3c9SqTryaHmbN9HIVKuadCnk
do+iSNodWtMns7slBt1eGYxOAD2R+l14w+e4o4U1zoRI1eDwJR6G42Ex6qdvzyze+x5QlZwsRO+h
x2phDdbQmYnR90VjEpyIjMR1HKVT8a3jqzYsgIaxm0RGd8uclnGovh4f7U/1jLPzQGJDK7NGo6HL
BbzNdhbE0WOk4orHtQRLYRhZKpPzbKrYPwAwH5g7j4Selux6p5eaiNDTZpPaNc6gVjAf57o0d6j2
reGz0q+IPtAC7PPXnR31NZIjGQAflHwakCy1+ssXZlEDNPXkMOWrz5GkEJndvvshFqj9YPSbSKKd
j0tJXHWzFqeHvAvgu7Epa59H+Pcw1DYmnH3EsquWT6WT4FnKakTD8cqcnv9TaDVJaus0iaIqus1S
7XUn51YK2D5765K7j+SjGNGGUxOj91+fiG9HQ7uxFCP1bGCxKGVGGEXHG7htftDrvduDCDyMQxvZ
PiPD1hyPBg360HZ6NKfi+0Kj53+tWBJw6VpZ+dCV8esHt7lVH9RDoamvBnAgc9D6yqoKIYIkYK/k
AtaDXQwBnGS+MqmJKyr5+CgCKdzdyEsWP2QYnpHr/FDYdWhDNV3LTO05Ao8BF+A9kT77hjgpVQsr
Me8eIHDaKGtF2qKLfHSiismRoGumTHxPaE9o/Xqgq6HeYS5YA25Z/iQc3CP4vq00WLgMIuoO1PKe
1C6yYa9Y0CQE/LkJxvTbVGSactmMeDm/wHnaN7fMgqaoiifNSgDjbR4x+mzm0ozXpkj5Yj7AQ66L
wRt21eIeKrLE1JfFGjo6ybCT9bkK1uffa1JzL5GDF5yuQL4KYD5L1Mlv9MU++P6k3JFglWEzkrWr
0t9kDNB5FPS+hPTLTClFuPxFW5V5/PLTEY6jpwT/sC8+UxveBEXJX7iQndHDO0PABwoE6nG+rG0E
P3ETXFYU1zMTJzPs1DHsF3LJTWDlsOCXl1DuzAANaxvJ5Q1MBi5ecJy5xu45P5QRoRVRmofgUtFf
Dr0O3dWafzL47SkgKpPrwlC6ZN+9+f6/BUaJkJUXxERhHBqO6R15h9ZGnqIoDDWwBxnS9x77VTRt
QThnDL1942E9m+ACEMmdUhVfntFhukGAqIyV8GECTJWqRsTNjXGeLgb2kUVJ6nnidH1W2NII0TGL
exOYIdl1vPYwgYQ/rKW5pFuVkBsJUaJHzN/oO1YB9SjW4Ek9rze0eezRHeoMPFTxaTxTLqVyMRUR
+9XKIeHNf/aHupwsIiiTj0nv+DsDXVHn7QDdCJFTIOeOR5ilGqTyDP4tBOPhD8odnu1MMCcd7NZJ
rL5BU3PWa2YmEeok1uPieAHNDb00HpzHO3CZLNLYZqc1wyR5slEnu9ssALlTFDMgpJ0+VbNaFm1l
Jc2prHxUV1DBl+J1ugbji7+eVn3VtMuGfHf4k7Khf/eoOU6c0A033l7xxt2M4eJvoCewJ6iDnxo0
cVE2FfENMM3Ix8x1I/ddbsjmmDBJ2Fp0tMjznpsCho6RpqSn0B3AdXpuvRqvq+8+cuOMvlEB+WYq
FMYWpcpmyajcIEQlFWGYzf3BRAyrgGUWhom53rAG0MDv2uaGNjJR+ong13pfaJJOJXwA6i0/ldyc
lNAu3QG/+bCRdtF8OxgktNDivN5XV4/bx6IxIMXpodfa0FJMnlEib6YRYKf3JBl3GFJzKLW689KL
QYZKwJiK3fdQCztwYDhSHGRB3L+HvDyS01QCjU0419GC7AmH1FrYePOQw0K4zYSCY7dS+NrHDuF7
Q3cAMcJyPax1V9EI0/debO+7BltZzzSIACxeXrPC1Ydn9d+epoj/iYl+6PIRjVrrX2n8p0BILYSh
ldyjCs3mKuvtSSHCTsFrATrSs9jmHIHadLGWKIYwVOfP4n73qZunrege1bP/SMJxM8OEt75+gVW8
3iMXTUR1x4mSrX7nnQGURvH4LlJwxo/+nA/lwJ00ULZFO3GK3sfOF8gOPP+X2BfzjlAxVgNi02Zd
JaxBRFCnfK4JnE2WK9EdkBdgGqmj1yb7EXoxNHYgF59uEaNaZCRdZTUlGDGtn539wEWuF6K2WWRH
ujgmG7fuZQQofi6ZVGNk7czicn0dSeyQZ+6TyK06tn1/Tn8SpmZM4Oo1AMiaO/h+Hht7ov5LUdQn
fYha7lRwjDJJ7VNnr/2BmrAEqI8ls+cdNAZVz4jy95uYxOZAz1NyzRQJIklt68uR68rmriSxceo+
1u6eZzHB0pGElGYf5YwDRL7ws1Wgr+GRcAurAmp/i4Lz5+FmoZD0E0ie2+2OOqQn8SiUDC9UMaEP
lFJKLmOZYtQ40KobveaSOts+l1vo6LG58mfYidieN/aEXwsVERUoFxeY+UmVicCql/KWkv/heBnG
qs2Ly9uxNL9DVwVKczikU+QcykantHg8issqjAMiXrtRxiI3KMc9S2asFZe4a8It5jAM8BHb5CoL
4lOGcFPj6YzhP/gL2PN1ix903oZpGaHIPpI9QHKSmF/0cGjitp34vWzCkujuHyHP8yLS6h4ywj5T
dxLF9eOQsu51ZLrCK3HwY9fmKMNSn6Ebri42HyBCN8V0D2g89W8qSs5gql8q+Yqs5rDN6AVFV958
8UyE1InfzukwktFW2sXJQdSab2/z/j7OJl/PiF95UO8MPV33fYIY2SklnV3eo3B3SOUpWhNYCIOm
P63pt99Ks0TfY2GCmgPnNy2aT4cEs3qHro1Kr312QRCAkpRHZGkzcnO2NFDBPX/eyDlb5n3cm9II
jxBfy0xh6KN4Iyron2cST7z81WtZ20zwpdd2EcFmasqxa0CTRYKgZDWoA6XEUce853TH6BJ6O6Ny
uGFj8hgHZwTm8uW8ZqMDV66ltUsLqEmH5llwetDgKJuF/wAtbMwTwUR3C04nZsCKivsR9Cu1YmtV
j1oqBFiqQfoCaUuMmoPDGGUAQ7gFqp+avSf2J8+py8gnrn/4Y8TKFhMO1cWgECyuPOKov5AqjS68
L0yl2HJl4QZFbEc3G1Fs3VQuX7dIzRFQ9oK0q6jPLwyhs8AqBR3lx0EFsATUiaOjMmqIVW1+zs38
v5D3WNwp4+Y+4ZCKUe2wzwMpi/8pgClHmpUL5qnXhEJ8flNY5bPyzldCMeDqvPutSXt58OvW/zPw
scRCM/y8lCt3FqhKYe1L9BnvSl633rLZwdvVgzYNh0IDiFLZeaBAJ5dYZOuBGp3V4GSgP+hGIlq3
2Gzp6Ncnm6ZsssbQYVxG1iuHNbD1JF3PIr/BZTdd1hl99eOZC4Xm+CRgfWGjakiYu57RVv1igIUy
5KRd0l2DEDDK83nFcVF/sFcW4n/TpM8D9rehwf/0o0lDaUtjbf1MLGg/Lz0l6jnqVp/L+YZLnl9J
uhvUUWTfPXNzEXXGePjWBGEOkJ4ZHKNSwQvPLk6kKPdKfIanDuL4AeHctbI9WHPM+uK1k/Ysio4w
gpSCIeugaKQFg983gzzpNJCMMgE08M+AzvvfvxyoKknZUwLJreiGYTFhtMEE/2PG20F37AugJia8
weNyImi+2IzcF03FEUybSaf73ald7MlM8YWR4RynfJPl0MmByLZ6FEWW69+PpvF7+519k+sx/nRu
d3Hzrj4XA1uCqV3BX4+lrVrlcDKEGzcd9TVJHUIbku3Vd8KaECEsdo84EqUmi26Ga0MAPyE/bVfw
x0Vyt5RVFSuV9lXGnDDjfGyoOPd89AR1UJeLKuJo+CgBra9SzQmOSGcxYI9xOPcHILLm29vZw39y
nzTVrpwXzhtAQQDox7ed6/xFuCV+ULOebruhLC9wO/VuJ0OW/mJmhK2scDnbD2fNMMVBL6IVFEWs
Xuryefc+YJHwq7rrlTu+ZY7daDUWGf0VDppUp7/D21UMchHj56nIP71gQrSGGKhtItvTeZt5nqR6
zil35HMqEBCia2n6/FXO6YSg/I5ihhrezki4iTuefWMHe/gmheTNtHXO9VPyBk8h/lxT/Qb11lv4
Kl0Dcue2+gEVgC/T5VOKpIMgo65lpsYUtvG0IADIZsi7TADX2DexPDt53aefSMwd3SFhFX+WjFx7
gGMglM1cPAG/F5rJzm5//3wOWEbaruo3zFPAuq27VpKtmw+Pa8Awo0UioeUr8kpFCbxIfEqaHHVy
TGH80mVRl/wLR/XNDJC3JkicKqe2YnQGMhaTGT+R0Bxr/FwoEpHzsJlasqI2CoSW3ZcX6zyQtNqF
k1aw6pqNUurp08YLftL6si+cFtZ8FpMYVK6Hq2FBd1UsknOwqupaHxYH+mN4D0WGmyVariJbAsOl
4eELj3J+ec52eo4cRiinAGMovAaeXkNCns8INlzeNt3Z+6Wt1saQHnIiMnMgKjNuSRviqMYec49W
ABG9nNi8ZAAphLQq1q1A99DxHvOAtiavPifWE+DohE3BYYb4xS3Wv86TlaXcobGEcEycemOgJ+yd
9MQGXSfLqEZ+sAgwwP09q77kVMWoJU5pOrlowbMctGbIBi7x4T6eURV43N4U48RHGjX5soB09pLB
uBuDQKtrh+Njf8n5lSFjxVNPHWxCb6LS3HX7kO3dSduJXc1wYUmdSLOJzrzukWiv5SEazrGerzpu
TjkxZXev1dJGCQInFYNuPI/RLgeX9MIYzSNIj3hcU1azYcVlXfLHaCYX+u6VnuRjPw7pBFs8E3A7
MurRsdHRrheYeXGJUxU8L3if0cbmdbumc2nEAo5EOVZ0oOlBQCumXT/9hk+ZaRPdbLQQeBKTLC1q
BDWirfsjeZn3exwaUp9tQyJXV+9ey4Ks2yV20zeB0MG8OjnnCDeSUIJQEWv9gdnmnc7mZleJ7ZMt
F/p/8YZXTc5DdmL3Kf3R0pxvKfVAWtfy4dAwFtn5q8L26hnTXrs7wWVI4zRYX1sU+FWDj7MEUsbY
YEv0vJxN3Eh0XNITcwPWSY6A+9nO8Tv5E7c4zBx7AFpuJ9KJtC9vqRp6GsPnzS+KzTSx09yyv4yk
wROUqq0A3cv8jujqcCtxsF4nWQs1My1MJEANsW2f0MTu7IM8u2WQkXPPcfPGuB5Sx9a+YOA4I9zs
Y7TwOHDrw/IQIhCXheTx05H1CzGh+sMNOtgu8BUp206KHfos6K1lq+8taB+xQuQ6rd6apXHN8o0g
FVod+76lDe5ybiXUa04AvWMTopP2BM2BmQkDB9tvPgWDrD6ikCinMZsgPEWjr6zYdkGeZppQlF+9
4E74WnQoD4NyhgDrPitPw1huD7Bd56SLRM8DcWj88G+XPOzh2rP33TQ5/lOJzhUMyjg52DO1HLTO
/qPybktd79a9FVk6cW8QLj7gJ0xR47p/Itl3ZhvqhEWMjUeqrKTmZcxH4Auhzo3v6UqeWRhGOak0
/tz9KMhKb1o16anDtNTa8xUh9bwRja9zcSCWCgCKIeX5b5vBgyr92ZMY4mZgwjUtIEiyGMHYnJJC
ynE2UjbB0IBs67yHVb2yt3wPMIYpt9pLV2YS+bCm8DedX6bTy1/vMJeZ7irSJLhKZNeMZvxJkV2o
6pC9/QsCJ90kbvjtrn/ZTh4uF+YoDQdAiWJUMw1oYlPGREoxTrMKn5PRJKclDaFQPsEqQMZECOlY
Wl51/TiWr8EoBYojUjckN+oJ4hiAURSUBoxJygZjAcCO6c5LSEYzIQCKAXMW+tlCASP8CbMRiWiR
sOCXgXBlujvZAK6Adkmh1urg099GQ4dvfUSuniuMNePoODByClh5NiOjlgJAuV4e8vEfwA2HfNqO
GQRqzA3zMFWoA6cXfa8s+r66C7o/qLNEx+oP9u5Xs0PS3bsGluASpVm2melnIo+wYCYDtw6Vqf4y
bJXWvqFb9ZZD5uWp4Jb+pNgztDUMHfKK7tuYO4ttkOCz+mC/fpKs5I57ITZTW8/gWjorw/AC4n7q
CsVJpJGr5ITPKRYE+7OJMnCWujxg88n4nQjAXyZ+vqnO7MFek3gYPtmIU2AdbAsfqP7TlmwCiBGC
dpzEoFeVFpejBaQFksowMnOWtffirIRnj8U4/3YM0jxt5RgeqVOneB1WBMHWz7CSWvcr3aj1abCB
YVmIiSUhLzaqigQ3JatmRdWVS4m9TbXhkLgBZPKy8QC8qzXil0F1ukpDPoi2rZBnFE/uQJZwJ6m0
c2A5UVOU/oNRZCP5KshZBBY19by2pg7Fk3R3TCO6qnsjAi0VvR5xMIWionUTUNbcEcifKwxDXAPa
k/QgbSeFSTh1KE7l0C4KnpATcNPX6d3BZMO/W+XE0ugfzeFgNnmeiuDdHyL4zcrylnVaOuQqsjLY
AMD7a+FVr3nfKda0givdZp/37P93oBgG2Cp17AQ6zpC4kfUhYwQNH/CmvfJzR6DuYmWi1fLgdadZ
6Jqb7JXwtnmxk94PznQCh091VD+BtvAKhDamppDF3JWsGWLFpgZaTOiBkiZqV5GGvkN0CU4FgU6M
K8tKZvLEY3eYRv5jm6wTrGF7ASE3Igew5aNF1pTXofDrQgMCUpNSp9N78Mo1fOcIOn07K929Wtih
ygTk6n5swAIZ90WLUoudLeQT2EsNXFNbocLTIdRBDTVRUk1/O59F7bb/Sm8JuVkPi7TrA9H7/vWJ
aUzRWcwEjVB3Z6NAzyqELr2RWsf1exrdxNT7YscX39sxZTo76WMAe7t1kVaXS3/LVQfi9iI9qBKt
LeIEo23nzmMwz5HuqptOJg8YmSr4WUmb2/uVi3N6XBBnAddp4091FbHWBsVcwsPrEsMJLp/o7qAm
bUAl+L0X/yCpYDIkjzAu1/mNbLgvWeAEL2d7KUCpXuYXgLf1eZfxDLFxEkMp8jemhnlRbLiCjdk6
m3/Gr7G2tbMYGRKxr25WR8Lu5p7x2J82Tcu/Kn7ZyHhHrDmK6Nvqvc11JNY1UCNptCuHkU6/iikK
hwd4ONFFsNsT63/UEBHicL6x8eQr2JCzNsvF8zHY//8XYGIX6/qOHmZw6Aq3DK+sXWcz+ereAfQY
eW/u8YErUTWg1faMjdsJEGzYuhm8QwXDdJ5UxkIfEA7hrsokRNLXyQcu7GIZFDIf9qMjVjz54oUk
9oKOfvXFUWQ4hUctKgrWDljLanfyh/54faCWjrYFpWIbybGYm8zkiI/pcKayDys+caHHnM8lNrTJ
cz/vcmNlfwJ4jy5UHljkRGjCbQOf16aVoVCUawwazfb1qcLawE1k79UH0oUhd8Z6plq0ICwG5e53
y/QyPeQS3VFckSpk8vkYRw8/u0r4NMlDQRj+1Zg4X9/GI7cOh8K0iWUymsTKD2A5nEbcyZ66aAT9
jydyfw7Fp3MHCNiuc+fnlm6URYb4gFXzdk2yg4/hFbWr6eVSdgDVwV6YJ8zVncbH4tCbw8Z/SHal
HWLMlku0PGTcNJ773bkWUSQIceDpv1LcgSwbpDWgOyP3KHQCeQe8JeC9XOqp76+DRSYV7strl3a2
JnFRTptVtZcG804HvbJbE5snoVRoU9BAuUM7+sO1ZHGFc+e7RqnDRF9+m0Zs1MGdZZXuA2fFsvX1
AYKwgkOROfQP/RhVjNbWkXyPPdJJljuUtvAUlUldU1MLvmi99c0SJT+keXlN79sAkOwExKEb7nhg
vqaIvN1ORMkYQBaMBjsKWigoEjy0RlJ/TchRGZVRIWzeO0fH0QSK/JIowcppTXx7kcJWjJo4MXbN
DhmNdzOFfaF17CuG+sa8AxJ+oIKvGAC284MJIngLO3714DssjbbYAjnxoRjZLlaL3u0Kx48wk4Os
JKjC+GzHEQeeP/0S1ZmZMpzhU2ei0pRyVdEAE7oRT+9cTTN7AoMM/mHoEiSqnjagN8nNMV+AHUTr
/oRspTEh96ssgMnNeFNQQwFX8MHClmzYuh8ADZt6zmNS6k3Pn0mlh2Lysioq4VWzqZidMbteHLAX
OiXlVpaQFEE3ZC/IgD+rGsnzLp/6m/sMtc5ZG9yJVm9Vd+xwYD/a38v/b4evIV+O2G64VTzSFnwa
h5JD4QhRnSVr8UUSyWh1UXJaHzvLIa/Qs+eeR4W4TC/MIW0yYivvP7vyEAfGHEm1oVqT/ooNlz9x
LCAV6xxGxNI2ThR6u3KVyd1wiOpRBQURS6pOItDi5O6mBWnqAi9vfNTDIII/Gz1+cMhAlcAlYtv7
xd7fgtD+yeRA2e3pO+Dx84gvwEESDX82jp0zruy6FkdPuTFsPGBFQXRrdGq4mNpjMANFBaFJ17R+
sZf/WhYbP7WwW2W8/EFB+vYZUKPebqjgBIkOM6juiYQViMNPtc6IENiAl7WA8eMYJ3eT4GoswoHb
f0oO3H4LQGZdALzlsxVBxDHzcB8HiiyHNKs4DHIDNvE3NE0DOCv3APXwEsqCeLVw8iy2/AncJG15
UD+9MeGZHcUAp+hnd7JRZ/KF+Yeb92IwSctiTnTgjqu/V7VqYUEPWOstRMxI8iNZbeqMPUpggL4b
Wo5YgF+q0E6f/O9mITLwLydcE/cn1Mdsf5UD6pOdnhin4nK/Is61Uyn/PMWBd+ywjCYKe+OHurC3
GR8jOluMncq1Pp+OvR408wt/2ocrHAWeZs271B82IPfhQjWXrP/1QoWoCwD0QMAf6P1ezc5fhmPt
UA/5iX2C45bZGvYm2s1+GP9i9Cl0OvfPEm7UNXiuVU91AXWx3fjHyM5Va2VOYEXqW2M42C7ixFLg
b0aQzkzkfG/N3G6mR0V9Ut/O1KT64Cl9ZEqnSRdBs77rtQo4KlIvnUgdz7xLmvuoh5yavn+oCVy5
3XbWOUPAI1FqM3UH8QgZlXckYyeUm8KlG1SlbKp012ZXDoNZaITXwQ+Nirexspwa42N+n6EwAL/y
KWxXxzFZAvRz4vlt0h9Nvvo6ZmGepPHX9ObJ5J++s30o3q2L5ix75PEmZgqfca0u1QrBGoW2rRqo
GWHEUbOUe9nUIS6IPA9c15dN+Y9848S4GC43Iuwstd1tU/GXR9ISJsO+wxNC8ZhLvDh6aQkAac0e
cUvbfPMd2k5lJFF2DT/hzX18sF2lNhCGghAUXyec4XlzrPNx9a1ZUNbIiEcL0ocZecwxfLz4gZPF
wO3Nv+NVn4ImF91Bdchwrxy6md4W6jzusxsMkzPewe7A57GIr+Rv6jxFycmfXYqVtws1YGXTgMtv
C3QKBeQ7Hz61wd61wkuZKxrUK91jdMcLJcgfxHryjndXdQ8jkfEohh0znwCLh957Wg1toXxWeRrA
s8KAkFZe9aWHXE8G0b5Ipn1GbV/DwJvbJKLFWaiso5X69qlejLhO2Aydr4sBJaY6ne893TpgkXKH
T8Wk1kbdrMrVkKIGZY72ZuRrDk0CHejAyBGuPl6HknMBYvFhcJ3ptSupUBOFjYr2lOB4wJYOomTJ
nLd95H6To/0++4/rks2cz5AtZBCyyTrBUHjOSltwpdsh8bzIsl2YOdek9NISw3bu+y652NanGlV3
9+C2SF9pZTxyen6jJWUu3JXEvXUDzLKQYAf5jOwZvMg8cDTc1lY4Dke9RblTPodMhI/cF07wUTqX
34W7ohVKl5OW4QujDFYfKNuMeLy2UtzCmOB8IAHYWMAKt1sIc4HmjrAI13yfRkSZmntcY8nX7kwj
XGpeYcKu9wbGlDNmdMQu8v72wMzuRIj/VgZCvHd4I/FTtTz/mzXxJGJqu1igo42sPcUQ2E64ypsG
IYGsm4bm3naTuikQ0XeKUv7vZcAaGjK6rWRFta2gzKLF1m2cWauxL59dndHm8In0Yhbv1UNXdR8F
QZUiNyakXsMBL7B4NJrZrAukVfFumgWAWdngClzybl92Y3w2MLmnplMbwS/PujSBhyiL73AvK8hb
Udy/TbdQYMnOKPVuFvNx4tQ87dYThKnsXNuL+KdMK8aMbK59LXI+5JG/YjVivsVcFj9YGu+ck8ov
UtxK3bbvAZK9Om8bmvEu1M1QyFG3Bi2txW4nszb5FpeyC/n9+rGkoWEIJh3OkvUs7BzofLmRgkQx
Z7yI4hIRSUTyR7BnDV04+jFukQRwTaBYczsh88+67IigxVLDdfqklKCzunIR3KPdcLDAeWpn+Ms8
iECGY5TfrE50Euif2DuHKbHX18/33Fqdh8+jM3gTppBDvPtvvB4wNoIXyMYHbLfnZdPO1mrl7Mpg
LYz63KBxj6E90umAy2+66AQU9sfjS1RFxhKFu8CtHDcj5Fo1xf8PyTbV/lOJA+z2RuafoLA3lIff
GzUAgwFc9GbhYLfSHEPusmPvCPCpv8rg0JoJFbQsIMqbtUIYDCdgZ9BfZDFenIOYZBJ+RT9qJ6ql
x3uPp/9sZYNA3unJHVg3W/Gsa/tq4xxygdgqEnO4ow5vGOWWQmArIJPSVCcNziRIZIKvHkjH+/05
7Xyp4Af0Csm6U+fRkH48rDUkiTPDHhnd63WBEI2LtnBxYPj0iA3HMcYRizhfHgBeduQKUFVqhJHG
87jEesdGwZRxSOOwLxTlpOIdZUtCwEgG97xuS0t+iJqawo22jrBsoeSPaGYwIdGHOYjOta/fwO7a
wHEe6aSygTJ+e3Kt3F5tirTPKF5iquH/0c81h8dFh3HyPRc6BIMDEx3xl8bI2y+1NykLM6aquCMf
5EXVatfuURI6ufakbM4ZX0haejrpBPcCjzs9SkTeZyAfrtAPma8pYa+vNQRVGkMAUp+upWDm1Dvl
JQhpWFo0yo4j5wGKVeUPrLuWi3P5bkfrVQL1TrUeaCurRJ+FVkAjHfxAUl5lClzEWUyY6gpN49Ft
E/r6S3tHnoDbQ1VH7zENd7Vc1xtPh88glgjuqqwKDmEOrW6Y1qDqIJYCTl8+nbcg0NVBh+lavB2g
QexewwUMwVVpY73V8ukh5RkvMlsewOUJebqExgBgsoxX+pDue7bVmQLNAIWc3Am2otCCQzvo0g/H
2VgqOzRqXIiOtX1WYY47QxrWDIZnR3RYsC0Y1nTSrcqdVteloMJ4tvYVZgV2XfD0AGe/kwjrvKhY
qQJB4NL62cYmhCYKKAgNlv/U3unefwhsr6VY1e9UEcwi/vGZzkvyaetgKpoaCIa/Fdcejek7Rs0J
rViO4Cjby1ckyXzXLVOkxco5P3UDDOcB4bsP9drZZZ0tB3i6D0kpjmFMWa/mO4OoM1ii0rOM3U/8
svFa0wxtN95jz35k1lPQxHckEiocZUyGMZqxjmSlpZJA1718ByV20w1japc12qAyvQ+a+wFUzs76
uoIatkKvhT+0A+XK3bMJaEPLseZqmX1NyPqDGOQYejnU1eQwMuOj/xAIZnGiz9hdMjCY6wzxUy9u
rTYW9SatlEPfe4Zkq+00WkxD6tYxY+hlGm2dVWXNAnTXlFeG4LY3jMdNb/LixsjNlaKUlSBP90GI
eTIp6I47f8FKOmF8WcfnPhMgeyH6nE2Gdi1uH5+DinYdLR4V3kutVmoHNkXYp773qTjcR15nDqsk
6wZUSEOPE2JC1MiB87EUEETGDkN6oYZNiiImZ7AEtHutqu+PkJROSDXQKoFGmwZNjOC/dB5YkAgO
B1zoiGh7rCzjPu7Ch677XxxIKLkZoAteXJfpDDLflVV8nvTL78db2dDTDZq3So3oGKp0LznEBaYq
BROsELnXZY/tAlKf1lo9EzNr2huBdaXiLIYjpSJ5mK8b/P3Aq+pM4iq45AtJEZV6LXmmkzpJTZ3A
KcU=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
