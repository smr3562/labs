-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 17:57:54 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_bram_incr_0_0/design_1_bram_incr_0_0_stub.vhdl
-- Design      : design_1_bram_incr_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_bram_incr_0_0 is
  Port ( 
    bram_addr : out STD_LOGIC_VECTOR ( 13 downto 0 );
    bram_clk : out STD_LOGIC;
    bram_en : out STD_LOGIC;
    bram_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_rst : out STD_LOGIC;
    bram_we : out STD_LOGIC;
    bram_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    peak_amp : in STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy : in STD_LOGIC;
    rstn : in STD_LOGIC;
    clk : in STD_LOGIC;
    segment : in STD_LOGIC
  );

end design_1_bram_incr_0_0;

architecture stub of design_1_bram_incr_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "bram_addr[13:0],bram_clk,bram_en,bram_dout[31:0],bram_rst,bram_we,bram_din[31:0],peak_amp[15:0],peak_amp_rdy,rstn,clk,segment";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "bram_incr,Vivado 2019.1";
begin
end;
