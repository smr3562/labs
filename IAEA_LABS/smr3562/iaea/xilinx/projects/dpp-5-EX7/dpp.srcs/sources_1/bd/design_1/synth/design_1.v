//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Tue Feb 16 17:37:37 2021
//Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=58,numReposBlks=33,numNonXlnxBlks=6,numHierBlks=25,maxHierDepth=2,numSysgenBlks=3,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_axi4_cnt=15,da_clkrst_cnt=1,da_mb_cnt=1,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (adc_clk,
    adc_data,
    reset,
    sys_clock,
    uart_rtl_rxd,
    uart_rtl_txd);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.ADC_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.ADC_CLK, CLK_DOMAIN /clk_wiz_0_clk_out1, FREQ_HZ 50000000, INSERT_VIP 0, PHASE 0.0" *) output adc_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 DATA.ADC_DATA DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME DATA.ADC_DATA, LAYERED_METADATA undef" *) input [13:0]adc_data;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.RESET RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.RESET, INSERT_VIP 0, POLARITY ACTIVE_HIGH" *) input reset;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.SYS_CLOCK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.SYS_CLOCK, CLK_DOMAIN design_1_sys_clock, FREQ_HZ 12000000, INSERT_VIP 0, PHASE 0.000" *) input sys_clock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:uart:1.0 uart_rtl RxD" *) input uart_rtl_rxd;
  (* X_INTERFACE_INFO = "xilinx.com:interface:uart:1.0 uart_rtl TxD" *) output uart_rtl_txd;

  wire [13:0]adc_data_1;
  wire axi_uartlite_0_UART_RxD;
  wire axi_uartlite_0_UART_TxD;
  wire clk_wiz_0_clk_adc;
  wire clk_wiz_0_clk_dpp;
  wire clk_wiz_0_locked;
  wire [0:0]dpp_0_full;
  wire microblaze_0_Clk;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_ARADDR;
  wire microblaze_0_axi_periph_M00_AXI_ARREADY;
  wire microblaze_0_axi_periph_M00_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_AWADDR;
  wire microblaze_0_axi_periph_M00_AXI_AWREADY;
  wire microblaze_0_axi_periph_M00_AXI_AWVALID;
  wire microblaze_0_axi_periph_M00_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M00_AXI_BRESP;
  wire microblaze_0_axi_periph_M00_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_RDATA;
  wire microblaze_0_axi_periph_M00_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M00_AXI_RRESP;
  wire microblaze_0_axi_periph_M00_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_WDATA;
  wire microblaze_0_axi_periph_M00_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M00_AXI_WSTRB;
  wire microblaze_0_axi_periph_M00_AXI_WVALID;
  wire [31:0]ps_mb_0_M01_AXI_ARADDR;
  wire [2:0]ps_mb_0_M01_AXI_ARPROT;
  wire ps_mb_0_M01_AXI_ARREADY;
  wire ps_mb_0_M01_AXI_ARVALID;
  wire [31:0]ps_mb_0_M01_AXI_AWADDR;
  wire [2:0]ps_mb_0_M01_AXI_AWPROT;
  wire ps_mb_0_M01_AXI_AWREADY;
  wire ps_mb_0_M01_AXI_AWVALID;
  wire ps_mb_0_M01_AXI_BREADY;
  wire [1:0]ps_mb_0_M01_AXI_BRESP;
  wire ps_mb_0_M01_AXI_BVALID;
  wire [31:0]ps_mb_0_M01_AXI_RDATA;
  wire ps_mb_0_M01_AXI_RREADY;
  wire [1:0]ps_mb_0_M01_AXI_RRESP;
  wire ps_mb_0_M01_AXI_RVALID;
  wire [31:0]ps_mb_0_M01_AXI_WDATA;
  wire ps_mb_0_M01_AXI_WREADY;
  wire [3:0]ps_mb_0_M01_AXI_WSTRB;
  wire ps_mb_0_M01_AXI_WVALID;
  wire [31:0]ps_mb_0_M02_AXI_ARADDR;
  wire [2:0]ps_mb_0_M02_AXI_ARPROT;
  wire ps_mb_0_M02_AXI_ARREADY;
  wire ps_mb_0_M02_AXI_ARVALID;
  wire [31:0]ps_mb_0_M02_AXI_AWADDR;
  wire [2:0]ps_mb_0_M02_AXI_AWPROT;
  wire ps_mb_0_M02_AXI_AWREADY;
  wire ps_mb_0_M02_AXI_AWVALID;
  wire ps_mb_0_M02_AXI_BREADY;
  wire [1:0]ps_mb_0_M02_AXI_BRESP;
  wire ps_mb_0_M02_AXI_BVALID;
  wire [31:0]ps_mb_0_M02_AXI_RDATA;
  wire ps_mb_0_M02_AXI_RREADY;
  wire [1:0]ps_mb_0_M02_AXI_RRESP;
  wire ps_mb_0_M02_AXI_RVALID;
  wire [31:0]ps_mb_0_M02_AXI_WDATA;
  wire ps_mb_0_M02_AXI_WREADY;
  wire [3:0]ps_mb_0_M02_AXI_WSTRB;
  wire ps_mb_0_M02_AXI_WVALID;
  wire [31:0]ps_mb_0_M03_AXI_ARADDR;
  wire ps_mb_0_M03_AXI_ARREADY;
  wire ps_mb_0_M03_AXI_ARVALID;
  wire [31:0]ps_mb_0_M03_AXI_AWADDR;
  wire ps_mb_0_M03_AXI_AWREADY;
  wire ps_mb_0_M03_AXI_AWVALID;
  wire ps_mb_0_M03_AXI_BREADY;
  wire [1:0]ps_mb_0_M03_AXI_BRESP;
  wire ps_mb_0_M03_AXI_BVALID;
  wire [31:0]ps_mb_0_M03_AXI_RDATA;
  wire ps_mb_0_M03_AXI_RREADY;
  wire [1:0]ps_mb_0_M03_AXI_RRESP;
  wire ps_mb_0_M03_AXI_RVALID;
  wire [31:0]ps_mb_0_M03_AXI_WDATA;
  wire ps_mb_0_M03_AXI_WREADY;
  wire [3:0]ps_mb_0_M03_AXI_WSTRB;
  wire ps_mb_0_M03_AXI_WVALID;
  wire [31:0]ps_mb_0_M05_AXI_ARADDR;
  wire ps_mb_0_M05_AXI_ARREADY;
  wire ps_mb_0_M05_AXI_ARVALID;
  wire [31:0]ps_mb_0_M05_AXI_AWADDR;
  wire ps_mb_0_M05_AXI_AWREADY;
  wire ps_mb_0_M05_AXI_AWVALID;
  wire ps_mb_0_M05_AXI_BREADY;
  wire [1:0]ps_mb_0_M05_AXI_BRESP;
  wire ps_mb_0_M05_AXI_BVALID;
  wire [31:0]ps_mb_0_M05_AXI_RDATA;
  wire ps_mb_0_M05_AXI_RREADY;
  wire [1:0]ps_mb_0_M05_AXI_RRESP;
  wire ps_mb_0_M05_AXI_RVALID;
  wire [31:0]ps_mb_0_M05_AXI_WDATA;
  wire ps_mb_0_M05_AXI_WREADY;
  wire [3:0]ps_mb_0_M05_AXI_WSTRB;
  wire ps_mb_0_M05_AXI_WVALID;
  wire [31:0]ps_mb_0_M06_AXI_ARADDR;
  wire [2:0]ps_mb_0_M06_AXI_ARPROT;
  wire ps_mb_0_M06_AXI_ARREADY;
  wire ps_mb_0_M06_AXI_ARVALID;
  wire [31:0]ps_mb_0_M06_AXI_AWADDR;
  wire [2:0]ps_mb_0_M06_AXI_AWPROT;
  wire ps_mb_0_M06_AXI_AWREADY;
  wire ps_mb_0_M06_AXI_AWVALID;
  wire ps_mb_0_M06_AXI_BREADY;
  wire [1:0]ps_mb_0_M06_AXI_BRESP;
  wire ps_mb_0_M06_AXI_BVALID;
  wire [31:0]ps_mb_0_M06_AXI_RDATA;
  wire ps_mb_0_M06_AXI_RREADY;
  wire [1:0]ps_mb_0_M06_AXI_RRESP;
  wire ps_mb_0_M06_AXI_RVALID;
  wire [31:0]ps_mb_0_M06_AXI_WDATA;
  wire ps_mb_0_M06_AXI_WREADY;
  wire [3:0]ps_mb_0_M06_AXI_WSTRB;
  wire ps_mb_0_M06_AXI_WVALID;
  wire [31:0]ps_mb_0_M07_AXI_ARADDR;
  wire [2:0]ps_mb_0_M07_AXI_ARPROT;
  wire ps_mb_0_M07_AXI_ARREADY;
  wire ps_mb_0_M07_AXI_ARVALID;
  wire [31:0]ps_mb_0_M07_AXI_AWADDR;
  wire [2:0]ps_mb_0_M07_AXI_AWPROT;
  wire ps_mb_0_M07_AXI_AWREADY;
  wire ps_mb_0_M07_AXI_AWVALID;
  wire ps_mb_0_M07_AXI_BREADY;
  wire [1:0]ps_mb_0_M07_AXI_BRESP;
  wire ps_mb_0_M07_AXI_BVALID;
  wire [31:0]ps_mb_0_M07_AXI_RDATA;
  wire ps_mb_0_M07_AXI_RREADY;
  wire [1:0]ps_mb_0_M07_AXI_RRESP;
  wire ps_mb_0_M07_AXI_RVALID;
  wire [31:0]ps_mb_0_M07_AXI_WDATA;
  wire ps_mb_0_M07_AXI_WREADY;
  wire [3:0]ps_mb_0_M07_AXI_WSTRB;
  wire ps_mb_0_M07_AXI_WVALID;
  wire [31:0]ps_mb_0_M08_AXI_ARADDR;
  wire [2:0]ps_mb_0_M08_AXI_ARPROT;
  wire ps_mb_0_M08_AXI_ARREADY;
  wire ps_mb_0_M08_AXI_ARVALID;
  wire [31:0]ps_mb_0_M08_AXI_AWADDR;
  wire [2:0]ps_mb_0_M08_AXI_AWPROT;
  wire ps_mb_0_M08_AXI_AWREADY;
  wire ps_mb_0_M08_AXI_AWVALID;
  wire ps_mb_0_M08_AXI_BREADY;
  wire [1:0]ps_mb_0_M08_AXI_BRESP;
  wire ps_mb_0_M08_AXI_BVALID;
  wire [31:0]ps_mb_0_M08_AXI_RDATA;
  wire ps_mb_0_M08_AXI_RREADY;
  wire [1:0]ps_mb_0_M08_AXI_RRESP;
  wire ps_mb_0_M08_AXI_RVALID;
  wire [31:0]ps_mb_0_M08_AXI_WDATA;
  wire ps_mb_0_M08_AXI_WREADY;
  wire [3:0]ps_mb_0_M08_AXI_WSTRB;
  wire ps_mb_0_M08_AXI_WVALID;
  wire [12:0]ps_mb_0_M09_AXI_ARADDR;
  wire [1:0]ps_mb_0_M09_AXI_ARBURST;
  wire [3:0]ps_mb_0_M09_AXI_ARCACHE;
  wire [7:0]ps_mb_0_M09_AXI_ARLEN;
  wire ps_mb_0_M09_AXI_ARLOCK;
  wire [2:0]ps_mb_0_M09_AXI_ARPROT;
  wire ps_mb_0_M09_AXI_ARREADY;
  wire [2:0]ps_mb_0_M09_AXI_ARSIZE;
  wire ps_mb_0_M09_AXI_ARVALID;
  wire [12:0]ps_mb_0_M09_AXI_AWADDR;
  wire [1:0]ps_mb_0_M09_AXI_AWBURST;
  wire [3:0]ps_mb_0_M09_AXI_AWCACHE;
  wire [7:0]ps_mb_0_M09_AXI_AWLEN;
  wire ps_mb_0_M09_AXI_AWLOCK;
  wire [2:0]ps_mb_0_M09_AXI_AWPROT;
  wire ps_mb_0_M09_AXI_AWREADY;
  wire [2:0]ps_mb_0_M09_AXI_AWSIZE;
  wire ps_mb_0_M09_AXI_AWVALID;
  wire ps_mb_0_M09_AXI_BREADY;
  wire [1:0]ps_mb_0_M09_AXI_BRESP;
  wire ps_mb_0_M09_AXI_BVALID;
  wire [31:0]ps_mb_0_M09_AXI_RDATA;
  wire ps_mb_0_M09_AXI_RLAST;
  wire ps_mb_0_M09_AXI_RREADY;
  wire [1:0]ps_mb_0_M09_AXI_RRESP;
  wire ps_mb_0_M09_AXI_RVALID;
  wire [31:0]ps_mb_0_M09_AXI_WDATA;
  wire ps_mb_0_M09_AXI_WLAST;
  wire ps_mb_0_M09_AXI_WREADY;
  wire [3:0]ps_mb_0_M09_AXI_WSTRB;
  wire ps_mb_0_M09_AXI_WVALID;
  wire [31:0]ps_mb_0_M10_AXI_ARADDR;
  wire [2:0]ps_mb_0_M10_AXI_ARPROT;
  wire ps_mb_0_M10_AXI_ARREADY;
  wire ps_mb_0_M10_AXI_ARVALID;
  wire [31:0]ps_mb_0_M10_AXI_AWADDR;
  wire [2:0]ps_mb_0_M10_AXI_AWPROT;
  wire ps_mb_0_M10_AXI_AWREADY;
  wire ps_mb_0_M10_AXI_AWVALID;
  wire ps_mb_0_M10_AXI_BREADY;
  wire [1:0]ps_mb_0_M10_AXI_BRESP;
  wire ps_mb_0_M10_AXI_BVALID;
  wire [31:0]ps_mb_0_M10_AXI_RDATA;
  wire ps_mb_0_M10_AXI_RREADY;
  wire [1:0]ps_mb_0_M10_AXI_RRESP;
  wire ps_mb_0_M10_AXI_RVALID;
  wire [31:0]ps_mb_0_M10_AXI_WDATA;
  wire ps_mb_0_M10_AXI_WREADY;
  wire [3:0]ps_mb_0_M10_AXI_WSTRB;
  wire ps_mb_0_M10_AXI_WVALID;
  wire [31:0]ps_mb_0_M11_AXI_ARADDR;
  wire ps_mb_0_M11_AXI_ARREADY;
  wire ps_mb_0_M11_AXI_ARVALID;
  wire [31:0]ps_mb_0_M11_AXI_AWADDR;
  wire ps_mb_0_M11_AXI_AWREADY;
  wire ps_mb_0_M11_AXI_AWVALID;
  wire ps_mb_0_M11_AXI_BREADY;
  wire [1:0]ps_mb_0_M11_AXI_BRESP;
  wire ps_mb_0_M11_AXI_BVALID;
  wire [31:0]ps_mb_0_M11_AXI_RDATA;
  wire ps_mb_0_M11_AXI_RREADY;
  wire [1:0]ps_mb_0_M11_AXI_RRESP;
  wire ps_mb_0_M11_AXI_RVALID;
  wire [31:0]ps_mb_0_M11_AXI_WDATA;
  wire ps_mb_0_M11_AXI_WREADY;
  wire [3:0]ps_mb_0_M11_AXI_WSTRB;
  wire ps_mb_0_M11_AXI_WVALID;
  wire reset_1;
  wire [0:0]rst_clk_wiz_0_120M_peripheral_aresetn;
  wire sys_clock_1;

  assign adc_clk = clk_wiz_0_clk_adc;
  assign adc_data_1 = adc_data[13:0];
  assign axi_uartlite_0_UART_RxD = uart_rtl_rxd;
  assign reset_1 = reset;
  assign sys_clock_1 = sys_clock;
  assign uart_rtl_txd = axi_uartlite_0_UART_TxD;
  design_1_clk_wiz_0_0 clk_wiz_0
       (.clk_adc(clk_wiz_0_clk_adc),
        .clk_cpu(microblaze_0_Clk),
        .clk_dpp(clk_wiz_0_clk_dpp),
        .clk_in1(sys_clock_1),
        .locked(clk_wiz_0_locked),
        .reset(reset_1));
  dpp_0_imp_A6T9OW dpp_0
       (.AXI_DC_STAB_araddr(ps_mb_0_M07_AXI_ARADDR),
        .AXI_DC_STAB_arprot(ps_mb_0_M07_AXI_ARPROT),
        .AXI_DC_STAB_arready(ps_mb_0_M07_AXI_ARREADY),
        .AXI_DC_STAB_arvalid(ps_mb_0_M07_AXI_ARVALID),
        .AXI_DC_STAB_awaddr(ps_mb_0_M07_AXI_AWADDR),
        .AXI_DC_STAB_awprot(ps_mb_0_M07_AXI_AWPROT),
        .AXI_DC_STAB_awready(ps_mb_0_M07_AXI_AWREADY),
        .AXI_DC_STAB_awvalid(ps_mb_0_M07_AXI_AWVALID),
        .AXI_DC_STAB_bready(ps_mb_0_M07_AXI_BREADY),
        .AXI_DC_STAB_bresp(ps_mb_0_M07_AXI_BRESP),
        .AXI_DC_STAB_bvalid(ps_mb_0_M07_AXI_BVALID),
        .AXI_DC_STAB_rdata(ps_mb_0_M07_AXI_RDATA),
        .AXI_DC_STAB_rready(ps_mb_0_M07_AXI_RREADY),
        .AXI_DC_STAB_rresp(ps_mb_0_M07_AXI_RRESP),
        .AXI_DC_STAB_rvalid(ps_mb_0_M07_AXI_RVALID),
        .AXI_DC_STAB_wdata(ps_mb_0_M07_AXI_WDATA),
        .AXI_DC_STAB_wready(ps_mb_0_M07_AXI_WREADY),
        .AXI_DC_STAB_wstrb(ps_mb_0_M07_AXI_WSTRB),
        .AXI_DC_STAB_wvalid(ps_mb_0_M07_AXI_WVALID),
        .S00_AXI1_araddr(ps_mb_0_M02_AXI_ARADDR),
        .S00_AXI1_arprot(ps_mb_0_M02_AXI_ARPROT),
        .S00_AXI1_arready(ps_mb_0_M02_AXI_ARREADY),
        .S00_AXI1_arvalid(ps_mb_0_M02_AXI_ARVALID),
        .S00_AXI1_awaddr(ps_mb_0_M02_AXI_AWADDR),
        .S00_AXI1_awprot(ps_mb_0_M02_AXI_AWPROT),
        .S00_AXI1_awready(ps_mb_0_M02_AXI_AWREADY),
        .S00_AXI1_awvalid(ps_mb_0_M02_AXI_AWVALID),
        .S00_AXI1_bready(ps_mb_0_M02_AXI_BREADY),
        .S00_AXI1_bresp(ps_mb_0_M02_AXI_BRESP),
        .S00_AXI1_bvalid(ps_mb_0_M02_AXI_BVALID),
        .S00_AXI1_rdata(ps_mb_0_M02_AXI_RDATA),
        .S00_AXI1_rready(ps_mb_0_M02_AXI_RREADY),
        .S00_AXI1_rresp(ps_mb_0_M02_AXI_RRESP),
        .S00_AXI1_rvalid(ps_mb_0_M02_AXI_RVALID),
        .S00_AXI1_wdata(ps_mb_0_M02_AXI_WDATA),
        .S00_AXI1_wready(ps_mb_0_M02_AXI_WREADY),
        .S00_AXI1_wstrb(ps_mb_0_M02_AXI_WSTRB),
        .S00_AXI1_wvalid(ps_mb_0_M02_AXI_WVALID),
        .S00_AXI2_araddr(ps_mb_0_M06_AXI_ARADDR),
        .S00_AXI2_arprot(ps_mb_0_M06_AXI_ARPROT),
        .S00_AXI2_arready(ps_mb_0_M06_AXI_ARREADY),
        .S00_AXI2_arvalid(ps_mb_0_M06_AXI_ARVALID),
        .S00_AXI2_awaddr(ps_mb_0_M06_AXI_AWADDR),
        .S00_AXI2_awprot(ps_mb_0_M06_AXI_AWPROT),
        .S00_AXI2_awready(ps_mb_0_M06_AXI_AWREADY),
        .S00_AXI2_awvalid(ps_mb_0_M06_AXI_AWVALID),
        .S00_AXI2_bready(ps_mb_0_M06_AXI_BREADY),
        .S00_AXI2_bresp(ps_mb_0_M06_AXI_BRESP),
        .S00_AXI2_bvalid(ps_mb_0_M06_AXI_BVALID),
        .S00_AXI2_rdata(ps_mb_0_M06_AXI_RDATA),
        .S00_AXI2_rready(ps_mb_0_M06_AXI_RREADY),
        .S00_AXI2_rresp(ps_mb_0_M06_AXI_RRESP),
        .S00_AXI2_rvalid(ps_mb_0_M06_AXI_RVALID),
        .S00_AXI2_wdata(ps_mb_0_M06_AXI_WDATA),
        .S00_AXI2_wready(ps_mb_0_M06_AXI_WREADY),
        .S00_AXI2_wstrb(ps_mb_0_M06_AXI_WSTRB),
        .S00_AXI2_wvalid(ps_mb_0_M06_AXI_WVALID),
        .S00_AXI3_araddr(ps_mb_0_M08_AXI_ARADDR),
        .S00_AXI3_arprot(ps_mb_0_M08_AXI_ARPROT),
        .S00_AXI3_arready(ps_mb_0_M08_AXI_ARREADY),
        .S00_AXI3_arvalid(ps_mb_0_M08_AXI_ARVALID),
        .S00_AXI3_awaddr(ps_mb_0_M08_AXI_AWADDR),
        .S00_AXI3_awprot(ps_mb_0_M08_AXI_AWPROT),
        .S00_AXI3_awready(ps_mb_0_M08_AXI_AWREADY),
        .S00_AXI3_awvalid(ps_mb_0_M08_AXI_AWVALID),
        .S00_AXI3_bready(ps_mb_0_M08_AXI_BREADY),
        .S00_AXI3_bresp(ps_mb_0_M08_AXI_BRESP),
        .S00_AXI3_bvalid(ps_mb_0_M08_AXI_BVALID),
        .S00_AXI3_rdata(ps_mb_0_M08_AXI_RDATA),
        .S00_AXI3_rready(ps_mb_0_M08_AXI_RREADY),
        .S00_AXI3_rresp(ps_mb_0_M08_AXI_RRESP),
        .S00_AXI3_rvalid(ps_mb_0_M08_AXI_RVALID),
        .S00_AXI3_wdata(ps_mb_0_M08_AXI_WDATA),
        .S00_AXI3_wready(ps_mb_0_M08_AXI_WREADY),
        .S00_AXI3_wstrb(ps_mb_0_M08_AXI_WSTRB),
        .S00_AXI3_wvalid(ps_mb_0_M08_AXI_WVALID),
        .S00_AXI4_araddr(ps_mb_0_M10_AXI_ARADDR),
        .S00_AXI4_arprot(ps_mb_0_M10_AXI_ARPROT),
        .S00_AXI4_arready(ps_mb_0_M10_AXI_ARREADY),
        .S00_AXI4_arvalid(ps_mb_0_M10_AXI_ARVALID),
        .S00_AXI4_awaddr(ps_mb_0_M10_AXI_AWADDR),
        .S00_AXI4_awprot(ps_mb_0_M10_AXI_AWPROT),
        .S00_AXI4_awready(ps_mb_0_M10_AXI_AWREADY),
        .S00_AXI4_awvalid(ps_mb_0_M10_AXI_AWVALID),
        .S00_AXI4_bready(ps_mb_0_M10_AXI_BREADY),
        .S00_AXI4_bresp(ps_mb_0_M10_AXI_BRESP),
        .S00_AXI4_bvalid(ps_mb_0_M10_AXI_BVALID),
        .S00_AXI4_rdata(ps_mb_0_M10_AXI_RDATA),
        .S00_AXI4_rready(ps_mb_0_M10_AXI_RREADY),
        .S00_AXI4_rresp(ps_mb_0_M10_AXI_RRESP),
        .S00_AXI4_rvalid(ps_mb_0_M10_AXI_RVALID),
        .S00_AXI4_wdata(ps_mb_0_M10_AXI_WDATA),
        .S00_AXI4_wready(ps_mb_0_M10_AXI_WREADY),
        .S00_AXI4_wstrb(ps_mb_0_M10_AXI_WSTRB),
        .S00_AXI4_wvalid(ps_mb_0_M10_AXI_WVALID),
        .S00_AXI_araddr(ps_mb_0_M01_AXI_ARADDR),
        .S00_AXI_arprot(ps_mb_0_M01_AXI_ARPROT),
        .S00_AXI_arready(ps_mb_0_M01_AXI_ARREADY),
        .S00_AXI_arvalid(ps_mb_0_M01_AXI_ARVALID),
        .S00_AXI_awaddr(ps_mb_0_M01_AXI_AWADDR),
        .S00_AXI_awprot(ps_mb_0_M01_AXI_AWPROT),
        .S00_AXI_awready(ps_mb_0_M01_AXI_AWREADY),
        .S00_AXI_awvalid(ps_mb_0_M01_AXI_AWVALID),
        .S00_AXI_bready(ps_mb_0_M01_AXI_BREADY),
        .S00_AXI_bresp(ps_mb_0_M01_AXI_BRESP),
        .S00_AXI_bvalid(ps_mb_0_M01_AXI_BVALID),
        .S00_AXI_rdata(ps_mb_0_M01_AXI_RDATA),
        .S00_AXI_rready(ps_mb_0_M01_AXI_RREADY),
        .S00_AXI_rresp(ps_mb_0_M01_AXI_RRESP),
        .S00_AXI_rvalid(ps_mb_0_M01_AXI_RVALID),
        .S00_AXI_wdata(ps_mb_0_M01_AXI_WDATA),
        .S00_AXI_wready(ps_mb_0_M01_AXI_WREADY),
        .S00_AXI_wstrb(ps_mb_0_M01_AXI_WSTRB),
        .S00_AXI_wvalid(ps_mb_0_M01_AXI_WVALID),
        .S_AXI_araddr(ps_mb_0_M09_AXI_ARADDR),
        .S_AXI_arburst(ps_mb_0_M09_AXI_ARBURST),
        .S_AXI_arcache(ps_mb_0_M09_AXI_ARCACHE),
        .S_AXI_arlen(ps_mb_0_M09_AXI_ARLEN),
        .S_AXI_arlock(ps_mb_0_M09_AXI_ARLOCK),
        .S_AXI_arprot(ps_mb_0_M09_AXI_ARPROT),
        .S_AXI_arready(ps_mb_0_M09_AXI_ARREADY),
        .S_AXI_arsize(ps_mb_0_M09_AXI_ARSIZE),
        .S_AXI_arvalid(ps_mb_0_M09_AXI_ARVALID),
        .S_AXI_awaddr(ps_mb_0_M09_AXI_AWADDR),
        .S_AXI_awburst(ps_mb_0_M09_AXI_AWBURST),
        .S_AXI_awcache(ps_mb_0_M09_AXI_AWCACHE),
        .S_AXI_awlen(ps_mb_0_M09_AXI_AWLEN),
        .S_AXI_awlock(ps_mb_0_M09_AXI_AWLOCK),
        .S_AXI_awprot(ps_mb_0_M09_AXI_AWPROT),
        .S_AXI_awready(ps_mb_0_M09_AXI_AWREADY),
        .S_AXI_awsize(ps_mb_0_M09_AXI_AWSIZE),
        .S_AXI_awvalid(ps_mb_0_M09_AXI_AWVALID),
        .S_AXI_bready(ps_mb_0_M09_AXI_BREADY),
        .S_AXI_bresp(ps_mb_0_M09_AXI_BRESP),
        .S_AXI_bvalid(ps_mb_0_M09_AXI_BVALID),
        .S_AXI_rdata(ps_mb_0_M09_AXI_RDATA),
        .S_AXI_rlast(ps_mb_0_M09_AXI_RLAST),
        .S_AXI_rready(ps_mb_0_M09_AXI_RREADY),
        .S_AXI_rresp(ps_mb_0_M09_AXI_RRESP),
        .S_AXI_rvalid(ps_mb_0_M09_AXI_RVALID),
        .S_AXI_wdata(ps_mb_0_M09_AXI_WDATA),
        .S_AXI_wlast(ps_mb_0_M09_AXI_WLAST),
        .S_AXI_wready(ps_mb_0_M09_AXI_WREADY),
        .S_AXI_wstrb(ps_mb_0_M09_AXI_WSTRB),
        .S_AXI_wvalid(ps_mb_0_M09_AXI_WVALID),
        .adc_data(adc_data_1),
        .axi_clk_domain_s_axi_araddr(ps_mb_0_M05_AXI_ARADDR),
        .axi_clk_domain_s_axi_arready(ps_mb_0_M05_AXI_ARREADY),
        .axi_clk_domain_s_axi_arvalid(ps_mb_0_M05_AXI_ARVALID),
        .axi_clk_domain_s_axi_awaddr(ps_mb_0_M05_AXI_AWADDR),
        .axi_clk_domain_s_axi_awready(ps_mb_0_M05_AXI_AWREADY),
        .axi_clk_domain_s_axi_awvalid(ps_mb_0_M05_AXI_AWVALID),
        .axi_clk_domain_s_axi_bready(ps_mb_0_M05_AXI_BREADY),
        .axi_clk_domain_s_axi_bresp(ps_mb_0_M05_AXI_BRESP),
        .axi_clk_domain_s_axi_bvalid(ps_mb_0_M05_AXI_BVALID),
        .axi_clk_domain_s_axi_rdata(ps_mb_0_M05_AXI_RDATA),
        .axi_clk_domain_s_axi_rready(ps_mb_0_M05_AXI_RREADY),
        .axi_clk_domain_s_axi_rresp(ps_mb_0_M05_AXI_RRESP),
        .axi_clk_domain_s_axi_rvalid(ps_mb_0_M05_AXI_RVALID),
        .axi_clk_domain_s_axi_wdata(ps_mb_0_M05_AXI_WDATA),
        .axi_clk_domain_s_axi_wready(ps_mb_0_M05_AXI_WREADY),
        .axi_clk_domain_s_axi_wstrb(ps_mb_0_M05_AXI_WSTRB),
        .axi_clk_domain_s_axi_wvalid(ps_mb_0_M05_AXI_WVALID),
        .axibusdomain_s_axi1_araddr(ps_mb_0_M11_AXI_ARADDR),
        .axibusdomain_s_axi1_arready(ps_mb_0_M11_AXI_ARREADY),
        .axibusdomain_s_axi1_arvalid(ps_mb_0_M11_AXI_ARVALID),
        .axibusdomain_s_axi1_awaddr(ps_mb_0_M11_AXI_AWADDR),
        .axibusdomain_s_axi1_awready(ps_mb_0_M11_AXI_AWREADY),
        .axibusdomain_s_axi1_awvalid(ps_mb_0_M11_AXI_AWVALID),
        .axibusdomain_s_axi1_bready(ps_mb_0_M11_AXI_BREADY),
        .axibusdomain_s_axi1_bresp(ps_mb_0_M11_AXI_BRESP),
        .axibusdomain_s_axi1_bvalid(ps_mb_0_M11_AXI_BVALID),
        .axibusdomain_s_axi1_rdata(ps_mb_0_M11_AXI_RDATA),
        .axibusdomain_s_axi1_rready(ps_mb_0_M11_AXI_RREADY),
        .axibusdomain_s_axi1_rresp(ps_mb_0_M11_AXI_RRESP),
        .axibusdomain_s_axi1_rvalid(ps_mb_0_M11_AXI_RVALID),
        .axibusdomain_s_axi1_wdata(ps_mb_0_M11_AXI_WDATA),
        .axibusdomain_s_axi1_wready(ps_mb_0_M11_AXI_WREADY),
        .axibusdomain_s_axi1_wstrb(ps_mb_0_M11_AXI_WSTRB),
        .axibusdomain_s_axi1_wvalid(ps_mb_0_M11_AXI_WVALID),
        .axibusdomain_s_axi_araddr(ps_mb_0_M03_AXI_ARADDR),
        .axibusdomain_s_axi_arready(ps_mb_0_M03_AXI_ARREADY),
        .axibusdomain_s_axi_arvalid(ps_mb_0_M03_AXI_ARVALID),
        .axibusdomain_s_axi_awaddr(ps_mb_0_M03_AXI_AWADDR),
        .axibusdomain_s_axi_awready(ps_mb_0_M03_AXI_AWREADY),
        .axibusdomain_s_axi_awvalid(ps_mb_0_M03_AXI_AWVALID),
        .axibusdomain_s_axi_bready(ps_mb_0_M03_AXI_BREADY),
        .axibusdomain_s_axi_bresp(ps_mb_0_M03_AXI_BRESP),
        .axibusdomain_s_axi_bvalid(ps_mb_0_M03_AXI_BVALID),
        .axibusdomain_s_axi_rdata(ps_mb_0_M03_AXI_RDATA),
        .axibusdomain_s_axi_rready(ps_mb_0_M03_AXI_RREADY),
        .axibusdomain_s_axi_rresp(ps_mb_0_M03_AXI_RRESP),
        .axibusdomain_s_axi_rvalid(ps_mb_0_M03_AXI_RVALID),
        .axibusdomain_s_axi_wdata(ps_mb_0_M03_AXI_WDATA),
        .axibusdomain_s_axi_wready(ps_mb_0_M03_AXI_WREADY),
        .axibusdomain_s_axi_wstrb(ps_mb_0_M03_AXI_WSTRB),
        .axibusdomain_s_axi_wvalid(ps_mb_0_M03_AXI_WVALID),
        .clk(clk_wiz_0_clk_dpp),
        .full(dpp_0_full),
        .s00_axi_aclk(microblaze_0_Clk),
        .s00_axi_aresetn(rst_clk_wiz_0_120M_peripheral_aresetn));
  mb_periph_0_imp_1IALCZF mb_periph_0
       (.S_AXI_araddr(microblaze_0_axi_periph_M00_AXI_ARADDR),
        .S_AXI_arready(microblaze_0_axi_periph_M00_AXI_ARREADY),
        .S_AXI_arvalid(microblaze_0_axi_periph_M00_AXI_ARVALID),
        .S_AXI_awaddr(microblaze_0_axi_periph_M00_AXI_AWADDR),
        .S_AXI_awready(microblaze_0_axi_periph_M00_AXI_AWREADY),
        .S_AXI_awvalid(microblaze_0_axi_periph_M00_AXI_AWVALID),
        .S_AXI_bready(microblaze_0_axi_periph_M00_AXI_BREADY),
        .S_AXI_bresp(microblaze_0_axi_periph_M00_AXI_BRESP),
        .S_AXI_bvalid(microblaze_0_axi_periph_M00_AXI_BVALID),
        .S_AXI_rdata(microblaze_0_axi_periph_M00_AXI_RDATA),
        .S_AXI_rready(microblaze_0_axi_periph_M00_AXI_RREADY),
        .S_AXI_rresp(microblaze_0_axi_periph_M00_AXI_RRESP),
        .S_AXI_rvalid(microblaze_0_axi_periph_M00_AXI_RVALID),
        .S_AXI_wdata(microblaze_0_axi_periph_M00_AXI_WDATA),
        .S_AXI_wready(microblaze_0_axi_periph_M00_AXI_WREADY),
        .S_AXI_wstrb(microblaze_0_axi_periph_M00_AXI_WSTRB),
        .S_AXI_wvalid(microblaze_0_axi_periph_M00_AXI_WVALID),
        .s_axi_aclk(microblaze_0_Clk),
        .s_axi_aresetn(rst_clk_wiz_0_120M_peripheral_aresetn),
        .uart_rtl_rxd(axi_uartlite_0_UART_RxD),
        .uart_rtl_txd(axi_uartlite_0_UART_TxD));
  ps_mb_0_imp_4932ML ps_mb_0
       (.Clk(microblaze_0_Clk),
        .M00_AXI_araddr(microblaze_0_axi_periph_M00_AXI_ARADDR),
        .M00_AXI_arready(microblaze_0_axi_periph_M00_AXI_ARREADY),
        .M00_AXI_arvalid(microblaze_0_axi_periph_M00_AXI_ARVALID),
        .M00_AXI_awaddr(microblaze_0_axi_periph_M00_AXI_AWADDR),
        .M00_AXI_awready(microblaze_0_axi_periph_M00_AXI_AWREADY),
        .M00_AXI_awvalid(microblaze_0_axi_periph_M00_AXI_AWVALID),
        .M00_AXI_bready(microblaze_0_axi_periph_M00_AXI_BREADY),
        .M00_AXI_bresp(microblaze_0_axi_periph_M00_AXI_BRESP),
        .M00_AXI_bvalid(microblaze_0_axi_periph_M00_AXI_BVALID),
        .M00_AXI_rdata(microblaze_0_axi_periph_M00_AXI_RDATA),
        .M00_AXI_rready(microblaze_0_axi_periph_M00_AXI_RREADY),
        .M00_AXI_rresp(microblaze_0_axi_periph_M00_AXI_RRESP),
        .M00_AXI_rvalid(microblaze_0_axi_periph_M00_AXI_RVALID),
        .M00_AXI_wdata(microblaze_0_axi_periph_M00_AXI_WDATA),
        .M00_AXI_wready(microblaze_0_axi_periph_M00_AXI_WREADY),
        .M00_AXI_wstrb(microblaze_0_axi_periph_M00_AXI_WSTRB),
        .M00_AXI_wvalid(microblaze_0_axi_periph_M00_AXI_WVALID),
        .M01_AXI_araddr(ps_mb_0_M01_AXI_ARADDR),
        .M01_AXI_arprot(ps_mb_0_M01_AXI_ARPROT),
        .M01_AXI_arready(ps_mb_0_M01_AXI_ARREADY),
        .M01_AXI_arvalid(ps_mb_0_M01_AXI_ARVALID),
        .M01_AXI_awaddr(ps_mb_0_M01_AXI_AWADDR),
        .M01_AXI_awprot(ps_mb_0_M01_AXI_AWPROT),
        .M01_AXI_awready(ps_mb_0_M01_AXI_AWREADY),
        .M01_AXI_awvalid(ps_mb_0_M01_AXI_AWVALID),
        .M01_AXI_bready(ps_mb_0_M01_AXI_BREADY),
        .M01_AXI_bresp(ps_mb_0_M01_AXI_BRESP),
        .M01_AXI_bvalid(ps_mb_0_M01_AXI_BVALID),
        .M01_AXI_rdata(ps_mb_0_M01_AXI_RDATA),
        .M01_AXI_rready(ps_mb_0_M01_AXI_RREADY),
        .M01_AXI_rresp(ps_mb_0_M01_AXI_RRESP),
        .M01_AXI_rvalid(ps_mb_0_M01_AXI_RVALID),
        .M01_AXI_wdata(ps_mb_0_M01_AXI_WDATA),
        .M01_AXI_wready(ps_mb_0_M01_AXI_WREADY),
        .M01_AXI_wstrb(ps_mb_0_M01_AXI_WSTRB),
        .M01_AXI_wvalid(ps_mb_0_M01_AXI_WVALID),
        .M02_AXI_araddr(ps_mb_0_M02_AXI_ARADDR),
        .M02_AXI_arprot(ps_mb_0_M02_AXI_ARPROT),
        .M02_AXI_arready(ps_mb_0_M02_AXI_ARREADY),
        .M02_AXI_arvalid(ps_mb_0_M02_AXI_ARVALID),
        .M02_AXI_awaddr(ps_mb_0_M02_AXI_AWADDR),
        .M02_AXI_awprot(ps_mb_0_M02_AXI_AWPROT),
        .M02_AXI_awready(ps_mb_0_M02_AXI_AWREADY),
        .M02_AXI_awvalid(ps_mb_0_M02_AXI_AWVALID),
        .M02_AXI_bready(ps_mb_0_M02_AXI_BREADY),
        .M02_AXI_bresp(ps_mb_0_M02_AXI_BRESP),
        .M02_AXI_bvalid(ps_mb_0_M02_AXI_BVALID),
        .M02_AXI_rdata(ps_mb_0_M02_AXI_RDATA),
        .M02_AXI_rready(ps_mb_0_M02_AXI_RREADY),
        .M02_AXI_rresp(ps_mb_0_M02_AXI_RRESP),
        .M02_AXI_rvalid(ps_mb_0_M02_AXI_RVALID),
        .M02_AXI_wdata(ps_mb_0_M02_AXI_WDATA),
        .M02_AXI_wready(ps_mb_0_M02_AXI_WREADY),
        .M02_AXI_wstrb(ps_mb_0_M02_AXI_WSTRB),
        .M02_AXI_wvalid(ps_mb_0_M02_AXI_WVALID),
        .M03_AXI_araddr(ps_mb_0_M03_AXI_ARADDR),
        .M03_AXI_arready(ps_mb_0_M03_AXI_ARREADY),
        .M03_AXI_arvalid(ps_mb_0_M03_AXI_ARVALID),
        .M03_AXI_awaddr(ps_mb_0_M03_AXI_AWADDR),
        .M03_AXI_awready(ps_mb_0_M03_AXI_AWREADY),
        .M03_AXI_awvalid(ps_mb_0_M03_AXI_AWVALID),
        .M03_AXI_bready(ps_mb_0_M03_AXI_BREADY),
        .M03_AXI_bresp(ps_mb_0_M03_AXI_BRESP),
        .M03_AXI_bvalid(ps_mb_0_M03_AXI_BVALID),
        .M03_AXI_rdata(ps_mb_0_M03_AXI_RDATA),
        .M03_AXI_rready(ps_mb_0_M03_AXI_RREADY),
        .M03_AXI_rresp(ps_mb_0_M03_AXI_RRESP),
        .M03_AXI_rvalid(ps_mb_0_M03_AXI_RVALID),
        .M03_AXI_wdata(ps_mb_0_M03_AXI_WDATA),
        .M03_AXI_wready(ps_mb_0_M03_AXI_WREADY),
        .M03_AXI_wstrb(ps_mb_0_M03_AXI_WSTRB),
        .M03_AXI_wvalid(ps_mb_0_M03_AXI_WVALID),
        .M05_AXI_araddr(ps_mb_0_M05_AXI_ARADDR),
        .M05_AXI_arready(ps_mb_0_M05_AXI_ARREADY),
        .M05_AXI_arvalid(ps_mb_0_M05_AXI_ARVALID),
        .M05_AXI_awaddr(ps_mb_0_M05_AXI_AWADDR),
        .M05_AXI_awready(ps_mb_0_M05_AXI_AWREADY),
        .M05_AXI_awvalid(ps_mb_0_M05_AXI_AWVALID),
        .M05_AXI_bready(ps_mb_0_M05_AXI_BREADY),
        .M05_AXI_bresp(ps_mb_0_M05_AXI_BRESP),
        .M05_AXI_bvalid(ps_mb_0_M05_AXI_BVALID),
        .M05_AXI_rdata(ps_mb_0_M05_AXI_RDATA),
        .M05_AXI_rready(ps_mb_0_M05_AXI_RREADY),
        .M05_AXI_rresp(ps_mb_0_M05_AXI_RRESP),
        .M05_AXI_rvalid(ps_mb_0_M05_AXI_RVALID),
        .M05_AXI_wdata(ps_mb_0_M05_AXI_WDATA),
        .M05_AXI_wready(ps_mb_0_M05_AXI_WREADY),
        .M05_AXI_wstrb(ps_mb_0_M05_AXI_WSTRB),
        .M05_AXI_wvalid(ps_mb_0_M05_AXI_WVALID),
        .M06_AXI_araddr(ps_mb_0_M06_AXI_ARADDR),
        .M06_AXI_arprot(ps_mb_0_M06_AXI_ARPROT),
        .M06_AXI_arready(ps_mb_0_M06_AXI_ARREADY),
        .M06_AXI_arvalid(ps_mb_0_M06_AXI_ARVALID),
        .M06_AXI_awaddr(ps_mb_0_M06_AXI_AWADDR),
        .M06_AXI_awprot(ps_mb_0_M06_AXI_AWPROT),
        .M06_AXI_awready(ps_mb_0_M06_AXI_AWREADY),
        .M06_AXI_awvalid(ps_mb_0_M06_AXI_AWVALID),
        .M06_AXI_bready(ps_mb_0_M06_AXI_BREADY),
        .M06_AXI_bresp(ps_mb_0_M06_AXI_BRESP),
        .M06_AXI_bvalid(ps_mb_0_M06_AXI_BVALID),
        .M06_AXI_rdata(ps_mb_0_M06_AXI_RDATA),
        .M06_AXI_rready(ps_mb_0_M06_AXI_RREADY),
        .M06_AXI_rresp(ps_mb_0_M06_AXI_RRESP),
        .M06_AXI_rvalid(ps_mb_0_M06_AXI_RVALID),
        .M06_AXI_wdata(ps_mb_0_M06_AXI_WDATA),
        .M06_AXI_wready(ps_mb_0_M06_AXI_WREADY),
        .M06_AXI_wstrb(ps_mb_0_M06_AXI_WSTRB),
        .M06_AXI_wvalid(ps_mb_0_M06_AXI_WVALID),
        .M07_AXI_araddr(ps_mb_0_M07_AXI_ARADDR),
        .M07_AXI_arprot(ps_mb_0_M07_AXI_ARPROT),
        .M07_AXI_arready(ps_mb_0_M07_AXI_ARREADY),
        .M07_AXI_arvalid(ps_mb_0_M07_AXI_ARVALID),
        .M07_AXI_awaddr(ps_mb_0_M07_AXI_AWADDR),
        .M07_AXI_awprot(ps_mb_0_M07_AXI_AWPROT),
        .M07_AXI_awready(ps_mb_0_M07_AXI_AWREADY),
        .M07_AXI_awvalid(ps_mb_0_M07_AXI_AWVALID),
        .M07_AXI_bready(ps_mb_0_M07_AXI_BREADY),
        .M07_AXI_bresp(ps_mb_0_M07_AXI_BRESP),
        .M07_AXI_bvalid(ps_mb_0_M07_AXI_BVALID),
        .M07_AXI_rdata(ps_mb_0_M07_AXI_RDATA),
        .M07_AXI_rready(ps_mb_0_M07_AXI_RREADY),
        .M07_AXI_rresp(ps_mb_0_M07_AXI_RRESP),
        .M07_AXI_rvalid(ps_mb_0_M07_AXI_RVALID),
        .M07_AXI_wdata(ps_mb_0_M07_AXI_WDATA),
        .M07_AXI_wready(ps_mb_0_M07_AXI_WREADY),
        .M07_AXI_wstrb(ps_mb_0_M07_AXI_WSTRB),
        .M07_AXI_wvalid(ps_mb_0_M07_AXI_WVALID),
        .M08_AXI_araddr(ps_mb_0_M08_AXI_ARADDR),
        .M08_AXI_arprot(ps_mb_0_M08_AXI_ARPROT),
        .M08_AXI_arready(ps_mb_0_M08_AXI_ARREADY),
        .M08_AXI_arvalid(ps_mb_0_M08_AXI_ARVALID),
        .M08_AXI_awaddr(ps_mb_0_M08_AXI_AWADDR),
        .M08_AXI_awprot(ps_mb_0_M08_AXI_AWPROT),
        .M08_AXI_awready(ps_mb_0_M08_AXI_AWREADY),
        .M08_AXI_awvalid(ps_mb_0_M08_AXI_AWVALID),
        .M08_AXI_bready(ps_mb_0_M08_AXI_BREADY),
        .M08_AXI_bresp(ps_mb_0_M08_AXI_BRESP),
        .M08_AXI_bvalid(ps_mb_0_M08_AXI_BVALID),
        .M08_AXI_rdata(ps_mb_0_M08_AXI_RDATA),
        .M08_AXI_rready(ps_mb_0_M08_AXI_RREADY),
        .M08_AXI_rresp(ps_mb_0_M08_AXI_RRESP),
        .M08_AXI_rvalid(ps_mb_0_M08_AXI_RVALID),
        .M08_AXI_wdata(ps_mb_0_M08_AXI_WDATA),
        .M08_AXI_wready(ps_mb_0_M08_AXI_WREADY),
        .M08_AXI_wstrb(ps_mb_0_M08_AXI_WSTRB),
        .M08_AXI_wvalid(ps_mb_0_M08_AXI_WVALID),
        .M09_AXI_araddr(ps_mb_0_M09_AXI_ARADDR),
        .M09_AXI_arburst(ps_mb_0_M09_AXI_ARBURST),
        .M09_AXI_arcache(ps_mb_0_M09_AXI_ARCACHE),
        .M09_AXI_arlen(ps_mb_0_M09_AXI_ARLEN),
        .M09_AXI_arlock(ps_mb_0_M09_AXI_ARLOCK),
        .M09_AXI_arprot(ps_mb_0_M09_AXI_ARPROT),
        .M09_AXI_arready(ps_mb_0_M09_AXI_ARREADY),
        .M09_AXI_arsize(ps_mb_0_M09_AXI_ARSIZE),
        .M09_AXI_arvalid(ps_mb_0_M09_AXI_ARVALID),
        .M09_AXI_awaddr(ps_mb_0_M09_AXI_AWADDR),
        .M09_AXI_awburst(ps_mb_0_M09_AXI_AWBURST),
        .M09_AXI_awcache(ps_mb_0_M09_AXI_AWCACHE),
        .M09_AXI_awlen(ps_mb_0_M09_AXI_AWLEN),
        .M09_AXI_awlock(ps_mb_0_M09_AXI_AWLOCK),
        .M09_AXI_awprot(ps_mb_0_M09_AXI_AWPROT),
        .M09_AXI_awready(ps_mb_0_M09_AXI_AWREADY),
        .M09_AXI_awsize(ps_mb_0_M09_AXI_AWSIZE),
        .M09_AXI_awvalid(ps_mb_0_M09_AXI_AWVALID),
        .M09_AXI_bready(ps_mb_0_M09_AXI_BREADY),
        .M09_AXI_bresp(ps_mb_0_M09_AXI_BRESP),
        .M09_AXI_bvalid(ps_mb_0_M09_AXI_BVALID),
        .M09_AXI_rdata(ps_mb_0_M09_AXI_RDATA),
        .M09_AXI_rlast(ps_mb_0_M09_AXI_RLAST),
        .M09_AXI_rready(ps_mb_0_M09_AXI_RREADY),
        .M09_AXI_rresp(ps_mb_0_M09_AXI_RRESP),
        .M09_AXI_rvalid(ps_mb_0_M09_AXI_RVALID),
        .M09_AXI_wdata(ps_mb_0_M09_AXI_WDATA),
        .M09_AXI_wlast(ps_mb_0_M09_AXI_WLAST),
        .M09_AXI_wready(ps_mb_0_M09_AXI_WREADY),
        .M09_AXI_wstrb(ps_mb_0_M09_AXI_WSTRB),
        .M09_AXI_wvalid(ps_mb_0_M09_AXI_WVALID),
        .M10_AXI_araddr(ps_mb_0_M10_AXI_ARADDR),
        .M10_AXI_arprot(ps_mb_0_M10_AXI_ARPROT),
        .M10_AXI_arready(ps_mb_0_M10_AXI_ARREADY),
        .M10_AXI_arvalid(ps_mb_0_M10_AXI_ARVALID),
        .M10_AXI_awaddr(ps_mb_0_M10_AXI_AWADDR),
        .M10_AXI_awprot(ps_mb_0_M10_AXI_AWPROT),
        .M10_AXI_awready(ps_mb_0_M10_AXI_AWREADY),
        .M10_AXI_awvalid(ps_mb_0_M10_AXI_AWVALID),
        .M10_AXI_bready(ps_mb_0_M10_AXI_BREADY),
        .M10_AXI_bresp(ps_mb_0_M10_AXI_BRESP),
        .M10_AXI_bvalid(ps_mb_0_M10_AXI_BVALID),
        .M10_AXI_rdata(ps_mb_0_M10_AXI_RDATA),
        .M10_AXI_rready(ps_mb_0_M10_AXI_RREADY),
        .M10_AXI_rresp(ps_mb_0_M10_AXI_RRESP),
        .M10_AXI_rvalid(ps_mb_0_M10_AXI_RVALID),
        .M10_AXI_wdata(ps_mb_0_M10_AXI_WDATA),
        .M10_AXI_wready(ps_mb_0_M10_AXI_WREADY),
        .M10_AXI_wstrb(ps_mb_0_M10_AXI_WSTRB),
        .M10_AXI_wvalid(ps_mb_0_M10_AXI_WVALID),
        .M11_AXI_araddr(ps_mb_0_M11_AXI_ARADDR),
        .M11_AXI_arready(ps_mb_0_M11_AXI_ARREADY),
        .M11_AXI_arvalid(ps_mb_0_M11_AXI_ARVALID),
        .M11_AXI_awaddr(ps_mb_0_M11_AXI_AWADDR),
        .M11_AXI_awready(ps_mb_0_M11_AXI_AWREADY),
        .M11_AXI_awvalid(ps_mb_0_M11_AXI_AWVALID),
        .M11_AXI_bready(ps_mb_0_M11_AXI_BREADY),
        .M11_AXI_bresp(ps_mb_0_M11_AXI_BRESP),
        .M11_AXI_bvalid(ps_mb_0_M11_AXI_BVALID),
        .M11_AXI_rdata(ps_mb_0_M11_AXI_RDATA),
        .M11_AXI_rready(ps_mb_0_M11_AXI_RREADY),
        .M11_AXI_rresp(ps_mb_0_M11_AXI_RRESP),
        .M11_AXI_rvalid(ps_mb_0_M11_AXI_RVALID),
        .M11_AXI_wdata(ps_mb_0_M11_AXI_WDATA),
        .M11_AXI_wready(ps_mb_0_M11_AXI_WREADY),
        .M11_AXI_wstrb(ps_mb_0_M11_AXI_WSTRB),
        .M11_AXI_wvalid(ps_mb_0_M11_AXI_WVALID),
        .S00_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .dcm_locked(clk_wiz_0_locked),
        .intr(dpp_0_full),
        .reset(reset_1));
endmodule

module design_1_microblaze_0_axi_periph_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    M02_ACLK,
    M02_ARESETN,
    M02_AXI_araddr,
    M02_AXI_arprot,
    M02_AXI_arready,
    M02_AXI_arvalid,
    M02_AXI_awaddr,
    M02_AXI_awprot,
    M02_AXI_awready,
    M02_AXI_awvalid,
    M02_AXI_bready,
    M02_AXI_bresp,
    M02_AXI_bvalid,
    M02_AXI_rdata,
    M02_AXI_rready,
    M02_AXI_rresp,
    M02_AXI_rvalid,
    M02_AXI_wdata,
    M02_AXI_wready,
    M02_AXI_wstrb,
    M02_AXI_wvalid,
    M03_ACLK,
    M03_ARESETN,
    M03_AXI_araddr,
    M03_AXI_arready,
    M03_AXI_arvalid,
    M03_AXI_awaddr,
    M03_AXI_awready,
    M03_AXI_awvalid,
    M03_AXI_bready,
    M03_AXI_bresp,
    M03_AXI_bvalid,
    M03_AXI_rdata,
    M03_AXI_rready,
    M03_AXI_rresp,
    M03_AXI_rvalid,
    M03_AXI_wdata,
    M03_AXI_wready,
    M03_AXI_wstrb,
    M03_AXI_wvalid,
    M04_ACLK,
    M04_ARESETN,
    M04_AXI_araddr,
    M04_AXI_arready,
    M04_AXI_arvalid,
    M04_AXI_awaddr,
    M04_AXI_awready,
    M04_AXI_awvalid,
    M04_AXI_bready,
    M04_AXI_bresp,
    M04_AXI_bvalid,
    M04_AXI_rdata,
    M04_AXI_rready,
    M04_AXI_rresp,
    M04_AXI_rvalid,
    M04_AXI_wdata,
    M04_AXI_wready,
    M04_AXI_wstrb,
    M04_AXI_wvalid,
    M05_ACLK,
    M05_ARESETN,
    M05_AXI_araddr,
    M05_AXI_arready,
    M05_AXI_arvalid,
    M05_AXI_awaddr,
    M05_AXI_awready,
    M05_AXI_awvalid,
    M05_AXI_bready,
    M05_AXI_bresp,
    M05_AXI_bvalid,
    M05_AXI_rdata,
    M05_AXI_rready,
    M05_AXI_rresp,
    M05_AXI_rvalid,
    M05_AXI_wdata,
    M05_AXI_wready,
    M05_AXI_wstrb,
    M05_AXI_wvalid,
    M06_ACLK,
    M06_ARESETN,
    M06_AXI_araddr,
    M06_AXI_arprot,
    M06_AXI_arready,
    M06_AXI_arvalid,
    M06_AXI_awaddr,
    M06_AXI_awprot,
    M06_AXI_awready,
    M06_AXI_awvalid,
    M06_AXI_bready,
    M06_AXI_bresp,
    M06_AXI_bvalid,
    M06_AXI_rdata,
    M06_AXI_rready,
    M06_AXI_rresp,
    M06_AXI_rvalid,
    M06_AXI_wdata,
    M06_AXI_wready,
    M06_AXI_wstrb,
    M06_AXI_wvalid,
    M07_ACLK,
    M07_ARESETN,
    M07_AXI_araddr,
    M07_AXI_arprot,
    M07_AXI_arready,
    M07_AXI_arvalid,
    M07_AXI_awaddr,
    M07_AXI_awprot,
    M07_AXI_awready,
    M07_AXI_awvalid,
    M07_AXI_bready,
    M07_AXI_bresp,
    M07_AXI_bvalid,
    M07_AXI_rdata,
    M07_AXI_rready,
    M07_AXI_rresp,
    M07_AXI_rvalid,
    M07_AXI_wdata,
    M07_AXI_wready,
    M07_AXI_wstrb,
    M07_AXI_wvalid,
    M08_ACLK,
    M08_ARESETN,
    M08_AXI_araddr,
    M08_AXI_arprot,
    M08_AXI_arready,
    M08_AXI_arvalid,
    M08_AXI_awaddr,
    M08_AXI_awprot,
    M08_AXI_awready,
    M08_AXI_awvalid,
    M08_AXI_bready,
    M08_AXI_bresp,
    M08_AXI_bvalid,
    M08_AXI_rdata,
    M08_AXI_rready,
    M08_AXI_rresp,
    M08_AXI_rvalid,
    M08_AXI_wdata,
    M08_AXI_wready,
    M08_AXI_wstrb,
    M08_AXI_wvalid,
    M09_ACLK,
    M09_ARESETN,
    M09_AXI_araddr,
    M09_AXI_arburst,
    M09_AXI_arcache,
    M09_AXI_arlen,
    M09_AXI_arlock,
    M09_AXI_arprot,
    M09_AXI_arready,
    M09_AXI_arsize,
    M09_AXI_arvalid,
    M09_AXI_awaddr,
    M09_AXI_awburst,
    M09_AXI_awcache,
    M09_AXI_awlen,
    M09_AXI_awlock,
    M09_AXI_awprot,
    M09_AXI_awready,
    M09_AXI_awsize,
    M09_AXI_awvalid,
    M09_AXI_bready,
    M09_AXI_bresp,
    M09_AXI_bvalid,
    M09_AXI_rdata,
    M09_AXI_rlast,
    M09_AXI_rready,
    M09_AXI_rresp,
    M09_AXI_rvalid,
    M09_AXI_wdata,
    M09_AXI_wlast,
    M09_AXI_wready,
    M09_AXI_wstrb,
    M09_AXI_wvalid,
    M10_ACLK,
    M10_ARESETN,
    M10_AXI_araddr,
    M10_AXI_arprot,
    M10_AXI_arready,
    M10_AXI_arvalid,
    M10_AXI_awaddr,
    M10_AXI_awprot,
    M10_AXI_awready,
    M10_AXI_awvalid,
    M10_AXI_bready,
    M10_AXI_bresp,
    M10_AXI_bvalid,
    M10_AXI_rdata,
    M10_AXI_rready,
    M10_AXI_rresp,
    M10_AXI_rvalid,
    M10_AXI_wdata,
    M10_AXI_wready,
    M10_AXI_wstrb,
    M10_AXI_wvalid,
    M11_ACLK,
    M11_ARESETN,
    M11_AXI_araddr,
    M11_AXI_arready,
    M11_AXI_arvalid,
    M11_AXI_awaddr,
    M11_AXI_awready,
    M11_AXI_awvalid,
    M11_AXI_bready,
    M11_AXI_bresp,
    M11_AXI_bvalid,
    M11_AXI_rdata,
    M11_AXI_rready,
    M11_AXI_rresp,
    M11_AXI_rvalid,
    M11_AXI_wdata,
    M11_AXI_wready,
    M11_AXI_wstrb,
    M11_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [31:0]M00_AXI_araddr;
  input M00_AXI_arready;
  output M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  input M00_AXI_awready;
  output M00_AXI_awvalid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input M01_ACLK;
  input M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input M01_AXI_arready;
  output M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input M01_AXI_awready;
  output M01_AXI_awvalid;
  output M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input M01_AXI_bvalid;
  input [31:0]M01_AXI_rdata;
  output M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input M01_AXI_rvalid;
  output [31:0]M01_AXI_wdata;
  input M01_AXI_wready;
  output [3:0]M01_AXI_wstrb;
  output M01_AXI_wvalid;
  input M02_ACLK;
  input M02_ARESETN;
  output [31:0]M02_AXI_araddr;
  output [2:0]M02_AXI_arprot;
  input M02_AXI_arready;
  output M02_AXI_arvalid;
  output [31:0]M02_AXI_awaddr;
  output [2:0]M02_AXI_awprot;
  input M02_AXI_awready;
  output M02_AXI_awvalid;
  output M02_AXI_bready;
  input [1:0]M02_AXI_bresp;
  input M02_AXI_bvalid;
  input [31:0]M02_AXI_rdata;
  output M02_AXI_rready;
  input [1:0]M02_AXI_rresp;
  input M02_AXI_rvalid;
  output [31:0]M02_AXI_wdata;
  input M02_AXI_wready;
  output [3:0]M02_AXI_wstrb;
  output M02_AXI_wvalid;
  input M03_ACLK;
  input M03_ARESETN;
  output [31:0]M03_AXI_araddr;
  input M03_AXI_arready;
  output M03_AXI_arvalid;
  output [31:0]M03_AXI_awaddr;
  input M03_AXI_awready;
  output M03_AXI_awvalid;
  output M03_AXI_bready;
  input [1:0]M03_AXI_bresp;
  input M03_AXI_bvalid;
  input [31:0]M03_AXI_rdata;
  output M03_AXI_rready;
  input [1:0]M03_AXI_rresp;
  input M03_AXI_rvalid;
  output [31:0]M03_AXI_wdata;
  input M03_AXI_wready;
  output [3:0]M03_AXI_wstrb;
  output M03_AXI_wvalid;
  input M04_ACLK;
  input M04_ARESETN;
  output [31:0]M04_AXI_araddr;
  input [0:0]M04_AXI_arready;
  output [0:0]M04_AXI_arvalid;
  output [31:0]M04_AXI_awaddr;
  input [0:0]M04_AXI_awready;
  output [0:0]M04_AXI_awvalid;
  output [0:0]M04_AXI_bready;
  input [1:0]M04_AXI_bresp;
  input [0:0]M04_AXI_bvalid;
  input [31:0]M04_AXI_rdata;
  output [0:0]M04_AXI_rready;
  input [1:0]M04_AXI_rresp;
  input [0:0]M04_AXI_rvalid;
  output [31:0]M04_AXI_wdata;
  input [0:0]M04_AXI_wready;
  output [3:0]M04_AXI_wstrb;
  output [0:0]M04_AXI_wvalid;
  input M05_ACLK;
  input M05_ARESETN;
  output [31:0]M05_AXI_araddr;
  input M05_AXI_arready;
  output M05_AXI_arvalid;
  output [31:0]M05_AXI_awaddr;
  input M05_AXI_awready;
  output M05_AXI_awvalid;
  output M05_AXI_bready;
  input [1:0]M05_AXI_bresp;
  input M05_AXI_bvalid;
  input [31:0]M05_AXI_rdata;
  output M05_AXI_rready;
  input [1:0]M05_AXI_rresp;
  input M05_AXI_rvalid;
  output [31:0]M05_AXI_wdata;
  input M05_AXI_wready;
  output [3:0]M05_AXI_wstrb;
  output M05_AXI_wvalid;
  input M06_ACLK;
  input M06_ARESETN;
  output [31:0]M06_AXI_araddr;
  output [2:0]M06_AXI_arprot;
  input M06_AXI_arready;
  output M06_AXI_arvalid;
  output [31:0]M06_AXI_awaddr;
  output [2:0]M06_AXI_awprot;
  input M06_AXI_awready;
  output M06_AXI_awvalid;
  output M06_AXI_bready;
  input [1:0]M06_AXI_bresp;
  input M06_AXI_bvalid;
  input [31:0]M06_AXI_rdata;
  output M06_AXI_rready;
  input [1:0]M06_AXI_rresp;
  input M06_AXI_rvalid;
  output [31:0]M06_AXI_wdata;
  input M06_AXI_wready;
  output [3:0]M06_AXI_wstrb;
  output M06_AXI_wvalid;
  input M07_ACLK;
  input M07_ARESETN;
  output [31:0]M07_AXI_araddr;
  output [2:0]M07_AXI_arprot;
  input M07_AXI_arready;
  output M07_AXI_arvalid;
  output [31:0]M07_AXI_awaddr;
  output [2:0]M07_AXI_awprot;
  input M07_AXI_awready;
  output M07_AXI_awvalid;
  output M07_AXI_bready;
  input [1:0]M07_AXI_bresp;
  input M07_AXI_bvalid;
  input [31:0]M07_AXI_rdata;
  output M07_AXI_rready;
  input [1:0]M07_AXI_rresp;
  input M07_AXI_rvalid;
  output [31:0]M07_AXI_wdata;
  input M07_AXI_wready;
  output [3:0]M07_AXI_wstrb;
  output M07_AXI_wvalid;
  input M08_ACLK;
  input M08_ARESETN;
  output [31:0]M08_AXI_araddr;
  output [2:0]M08_AXI_arprot;
  input M08_AXI_arready;
  output M08_AXI_arvalid;
  output [31:0]M08_AXI_awaddr;
  output [2:0]M08_AXI_awprot;
  input M08_AXI_awready;
  output M08_AXI_awvalid;
  output M08_AXI_bready;
  input [1:0]M08_AXI_bresp;
  input M08_AXI_bvalid;
  input [31:0]M08_AXI_rdata;
  output M08_AXI_rready;
  input [1:0]M08_AXI_rresp;
  input M08_AXI_rvalid;
  output [31:0]M08_AXI_wdata;
  input M08_AXI_wready;
  output [3:0]M08_AXI_wstrb;
  output M08_AXI_wvalid;
  input M09_ACLK;
  input M09_ARESETN;
  output [12:0]M09_AXI_araddr;
  output [1:0]M09_AXI_arburst;
  output [3:0]M09_AXI_arcache;
  output [7:0]M09_AXI_arlen;
  output M09_AXI_arlock;
  output [2:0]M09_AXI_arprot;
  input M09_AXI_arready;
  output [2:0]M09_AXI_arsize;
  output M09_AXI_arvalid;
  output [12:0]M09_AXI_awaddr;
  output [1:0]M09_AXI_awburst;
  output [3:0]M09_AXI_awcache;
  output [7:0]M09_AXI_awlen;
  output M09_AXI_awlock;
  output [2:0]M09_AXI_awprot;
  input M09_AXI_awready;
  output [2:0]M09_AXI_awsize;
  output M09_AXI_awvalid;
  output M09_AXI_bready;
  input [1:0]M09_AXI_bresp;
  input M09_AXI_bvalid;
  input [31:0]M09_AXI_rdata;
  input M09_AXI_rlast;
  output M09_AXI_rready;
  input [1:0]M09_AXI_rresp;
  input M09_AXI_rvalid;
  output [31:0]M09_AXI_wdata;
  output M09_AXI_wlast;
  input M09_AXI_wready;
  output [3:0]M09_AXI_wstrb;
  output M09_AXI_wvalid;
  input M10_ACLK;
  input M10_ARESETN;
  output [31:0]M10_AXI_araddr;
  output [2:0]M10_AXI_arprot;
  input M10_AXI_arready;
  output M10_AXI_arvalid;
  output [31:0]M10_AXI_awaddr;
  output [2:0]M10_AXI_awprot;
  input M10_AXI_awready;
  output M10_AXI_awvalid;
  output M10_AXI_bready;
  input [1:0]M10_AXI_bresp;
  input M10_AXI_bvalid;
  input [31:0]M10_AXI_rdata;
  output M10_AXI_rready;
  input [1:0]M10_AXI_rresp;
  input M10_AXI_rvalid;
  output [31:0]M10_AXI_wdata;
  input M10_AXI_wready;
  output [3:0]M10_AXI_wstrb;
  output M10_AXI_wvalid;
  input M11_ACLK;
  input M11_ARESETN;
  output [31:0]M11_AXI_araddr;
  input M11_AXI_arready;
  output M11_AXI_arvalid;
  output [31:0]M11_AXI_awaddr;
  input M11_AXI_awready;
  output M11_AXI_awvalid;
  output M11_AXI_bready;
  input [1:0]M11_AXI_bresp;
  input M11_AXI_bvalid;
  input [31:0]M11_AXI_rdata;
  output M11_AXI_rready;
  input [1:0]M11_AXI_rresp;
  input M11_AXI_rvalid;
  output [31:0]M11_AXI_wdata;
  input M11_AXI_wready;
  output [3:0]M11_AXI_wstrb;
  output M11_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output [0:0]S00_AXI_arready;
  input [0:0]S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output [0:0]S00_AXI_awready;
  input [0:0]S00_AXI_awvalid;
  input [0:0]S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output [0:0]S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input [0:0]S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output [0:0]S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output [0:0]S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input [0:0]S00_AXI_wvalid;

  wire [31:0]m00_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m00_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m00_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m00_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m00_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m00_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m00_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m00_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m00_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m00_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m00_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m00_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m00_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m00_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m00_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m00_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m00_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m01_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [2:0]m01_couplers_to_microblaze_0_axi_periph_ARPROT;
  wire m01_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m01_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m01_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [2:0]m01_couplers_to_microblaze_0_axi_periph_AWPROT;
  wire m01_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m01_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m01_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m01_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m01_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m01_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m01_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m01_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m01_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m01_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m01_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m01_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m01_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m02_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [2:0]m02_couplers_to_microblaze_0_axi_periph_ARPROT;
  wire m02_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m02_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m02_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [2:0]m02_couplers_to_microblaze_0_axi_periph_AWPROT;
  wire m02_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m02_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m02_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m02_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m02_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m02_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m02_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m02_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m02_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m02_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m02_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m02_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m02_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m03_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m03_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m03_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m03_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m03_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m03_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m03_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m03_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m03_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m03_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m03_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m03_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m03_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m03_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m03_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m03_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m03_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m04_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m04_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m04_couplers_to_microblaze_0_axi_periph_BRESP;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m04_couplers_to_microblaze_0_axi_periph_RDATA;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m04_couplers_to_microblaze_0_axi_periph_RRESP;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m04_couplers_to_microblaze_0_axi_periph_WDATA;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m04_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire [0:0]m04_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m05_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m05_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m05_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m05_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m05_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m05_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m05_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m05_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m05_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m05_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m05_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m05_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m05_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m05_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m05_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m05_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m05_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m06_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [2:0]m06_couplers_to_microblaze_0_axi_periph_ARPROT;
  wire m06_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m06_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m06_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [2:0]m06_couplers_to_microblaze_0_axi_periph_AWPROT;
  wire m06_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m06_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m06_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m06_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m06_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m06_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m06_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m06_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m06_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m06_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m06_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m06_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m06_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m07_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [2:0]m07_couplers_to_microblaze_0_axi_periph_ARPROT;
  wire m07_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m07_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m07_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [2:0]m07_couplers_to_microblaze_0_axi_periph_AWPROT;
  wire m07_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m07_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m07_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m07_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m07_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m07_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m07_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m07_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m07_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m07_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m07_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m07_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m07_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m08_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [2:0]m08_couplers_to_microblaze_0_axi_periph_ARPROT;
  wire m08_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m08_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m08_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [2:0]m08_couplers_to_microblaze_0_axi_periph_AWPROT;
  wire m08_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m08_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m08_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m08_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m08_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m08_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m08_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m08_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m08_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m08_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m08_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m08_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m08_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [12:0]m09_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [1:0]m09_couplers_to_microblaze_0_axi_periph_ARBURST;
  wire [3:0]m09_couplers_to_microblaze_0_axi_periph_ARCACHE;
  wire [7:0]m09_couplers_to_microblaze_0_axi_periph_ARLEN;
  wire m09_couplers_to_microblaze_0_axi_periph_ARLOCK;
  wire [2:0]m09_couplers_to_microblaze_0_axi_periph_ARPROT;
  wire m09_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire [2:0]m09_couplers_to_microblaze_0_axi_periph_ARSIZE;
  wire m09_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [12:0]m09_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [1:0]m09_couplers_to_microblaze_0_axi_periph_AWBURST;
  wire [3:0]m09_couplers_to_microblaze_0_axi_periph_AWCACHE;
  wire [7:0]m09_couplers_to_microblaze_0_axi_periph_AWLEN;
  wire m09_couplers_to_microblaze_0_axi_periph_AWLOCK;
  wire [2:0]m09_couplers_to_microblaze_0_axi_periph_AWPROT;
  wire m09_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire [2:0]m09_couplers_to_microblaze_0_axi_periph_AWSIZE;
  wire m09_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m09_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m09_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m09_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m09_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m09_couplers_to_microblaze_0_axi_periph_RLAST;
  wire m09_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m09_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m09_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m09_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m09_couplers_to_microblaze_0_axi_periph_WLAST;
  wire m09_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m09_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m09_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m10_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [2:0]m10_couplers_to_microblaze_0_axi_periph_ARPROT;
  wire m10_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m10_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m10_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [2:0]m10_couplers_to_microblaze_0_axi_periph_AWPROT;
  wire m10_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m10_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m10_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m10_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m10_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m10_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m10_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m10_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m10_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m10_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m10_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m10_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m10_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m11_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m11_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m11_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m11_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m11_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m11_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m11_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m11_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m11_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m11_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m11_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m11_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m11_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m11_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m11_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m11_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m11_couplers_to_microblaze_0_axi_periph_WVALID;
  wire microblaze_0_axi_periph_ACLK_net;
  wire microblaze_0_axi_periph_ARESETN_net;
  wire [31:0]microblaze_0_axi_periph_to_s00_couplers_ARADDR;
  wire [2:0]microblaze_0_axi_periph_to_s00_couplers_ARPROT;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_ARREADY;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_ARVALID;
  wire [31:0]microblaze_0_axi_periph_to_s00_couplers_AWADDR;
  wire [2:0]microblaze_0_axi_periph_to_s00_couplers_AWPROT;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_AWREADY;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_AWVALID;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_BREADY;
  wire [1:0]microblaze_0_axi_periph_to_s00_couplers_BRESP;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_BVALID;
  wire [31:0]microblaze_0_axi_periph_to_s00_couplers_RDATA;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_RREADY;
  wire [1:0]microblaze_0_axi_periph_to_s00_couplers_RRESP;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_RVALID;
  wire [31:0]microblaze_0_axi_periph_to_s00_couplers_WDATA;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_WREADY;
  wire [3:0]microblaze_0_axi_periph_to_s00_couplers_WSTRB;
  wire [0:0]microblaze_0_axi_periph_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire [0:0]s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire [0:0]s00_couplers_to_xbar_AWVALID;
  wire [0:0]s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [31:0]s00_couplers_to_xbar_RDATA;
  wire [0:0]s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [31:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [3:0]s00_couplers_to_xbar_WSTRB;
  wire [0:0]s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire xbar_to_m00_couplers_BVALID;
  wire [31:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire xbar_to_m00_couplers_RVALID;
  wire [31:0]xbar_to_m00_couplers_WDATA;
  wire xbar_to_m00_couplers_WREADY;
  wire [3:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [5:3]xbar_to_m01_couplers_ARPROT;
  wire xbar_to_m01_couplers_ARREADY;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [5:3]xbar_to_m01_couplers_AWPROT;
  wire xbar_to_m01_couplers_AWREADY;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire xbar_to_m01_couplers_BVALID;
  wire [31:0]xbar_to_m01_couplers_RDATA;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire xbar_to_m01_couplers_RVALID;
  wire [63:32]xbar_to_m01_couplers_WDATA;
  wire xbar_to_m01_couplers_WREADY;
  wire [7:4]xbar_to_m01_couplers_WSTRB;
  wire [1:1]xbar_to_m01_couplers_WVALID;
  wire [95:64]xbar_to_m02_couplers_ARADDR;
  wire [8:6]xbar_to_m02_couplers_ARPROT;
  wire xbar_to_m02_couplers_ARREADY;
  wire [2:2]xbar_to_m02_couplers_ARVALID;
  wire [95:64]xbar_to_m02_couplers_AWADDR;
  wire [8:6]xbar_to_m02_couplers_AWPROT;
  wire xbar_to_m02_couplers_AWREADY;
  wire [2:2]xbar_to_m02_couplers_AWVALID;
  wire [2:2]xbar_to_m02_couplers_BREADY;
  wire [1:0]xbar_to_m02_couplers_BRESP;
  wire xbar_to_m02_couplers_BVALID;
  wire [31:0]xbar_to_m02_couplers_RDATA;
  wire [2:2]xbar_to_m02_couplers_RREADY;
  wire [1:0]xbar_to_m02_couplers_RRESP;
  wire xbar_to_m02_couplers_RVALID;
  wire [95:64]xbar_to_m02_couplers_WDATA;
  wire xbar_to_m02_couplers_WREADY;
  wire [11:8]xbar_to_m02_couplers_WSTRB;
  wire [2:2]xbar_to_m02_couplers_WVALID;
  wire [127:96]xbar_to_m03_couplers_ARADDR;
  wire xbar_to_m03_couplers_ARREADY;
  wire [3:3]xbar_to_m03_couplers_ARVALID;
  wire [127:96]xbar_to_m03_couplers_AWADDR;
  wire xbar_to_m03_couplers_AWREADY;
  wire [3:3]xbar_to_m03_couplers_AWVALID;
  wire [3:3]xbar_to_m03_couplers_BREADY;
  wire [1:0]xbar_to_m03_couplers_BRESP;
  wire xbar_to_m03_couplers_BVALID;
  wire [31:0]xbar_to_m03_couplers_RDATA;
  wire [3:3]xbar_to_m03_couplers_RREADY;
  wire [1:0]xbar_to_m03_couplers_RRESP;
  wire xbar_to_m03_couplers_RVALID;
  wire [127:96]xbar_to_m03_couplers_WDATA;
  wire xbar_to_m03_couplers_WREADY;
  wire [15:12]xbar_to_m03_couplers_WSTRB;
  wire [3:3]xbar_to_m03_couplers_WVALID;
  wire [159:128]xbar_to_m04_couplers_ARADDR;
  wire [0:0]xbar_to_m04_couplers_ARREADY;
  wire [4:4]xbar_to_m04_couplers_ARVALID;
  wire [159:128]xbar_to_m04_couplers_AWADDR;
  wire [0:0]xbar_to_m04_couplers_AWREADY;
  wire [4:4]xbar_to_m04_couplers_AWVALID;
  wire [4:4]xbar_to_m04_couplers_BREADY;
  wire [1:0]xbar_to_m04_couplers_BRESP;
  wire [0:0]xbar_to_m04_couplers_BVALID;
  wire [31:0]xbar_to_m04_couplers_RDATA;
  wire [4:4]xbar_to_m04_couplers_RREADY;
  wire [1:0]xbar_to_m04_couplers_RRESP;
  wire [0:0]xbar_to_m04_couplers_RVALID;
  wire [159:128]xbar_to_m04_couplers_WDATA;
  wire [0:0]xbar_to_m04_couplers_WREADY;
  wire [19:16]xbar_to_m04_couplers_WSTRB;
  wire [4:4]xbar_to_m04_couplers_WVALID;
  wire [191:160]xbar_to_m05_couplers_ARADDR;
  wire xbar_to_m05_couplers_ARREADY;
  wire [5:5]xbar_to_m05_couplers_ARVALID;
  wire [191:160]xbar_to_m05_couplers_AWADDR;
  wire xbar_to_m05_couplers_AWREADY;
  wire [5:5]xbar_to_m05_couplers_AWVALID;
  wire [5:5]xbar_to_m05_couplers_BREADY;
  wire [1:0]xbar_to_m05_couplers_BRESP;
  wire xbar_to_m05_couplers_BVALID;
  wire [31:0]xbar_to_m05_couplers_RDATA;
  wire [5:5]xbar_to_m05_couplers_RREADY;
  wire [1:0]xbar_to_m05_couplers_RRESP;
  wire xbar_to_m05_couplers_RVALID;
  wire [191:160]xbar_to_m05_couplers_WDATA;
  wire xbar_to_m05_couplers_WREADY;
  wire [23:20]xbar_to_m05_couplers_WSTRB;
  wire [5:5]xbar_to_m05_couplers_WVALID;
  wire [223:192]xbar_to_m06_couplers_ARADDR;
  wire [20:18]xbar_to_m06_couplers_ARPROT;
  wire xbar_to_m06_couplers_ARREADY;
  wire [6:6]xbar_to_m06_couplers_ARVALID;
  wire [223:192]xbar_to_m06_couplers_AWADDR;
  wire [20:18]xbar_to_m06_couplers_AWPROT;
  wire xbar_to_m06_couplers_AWREADY;
  wire [6:6]xbar_to_m06_couplers_AWVALID;
  wire [6:6]xbar_to_m06_couplers_BREADY;
  wire [1:0]xbar_to_m06_couplers_BRESP;
  wire xbar_to_m06_couplers_BVALID;
  wire [31:0]xbar_to_m06_couplers_RDATA;
  wire [6:6]xbar_to_m06_couplers_RREADY;
  wire [1:0]xbar_to_m06_couplers_RRESP;
  wire xbar_to_m06_couplers_RVALID;
  wire [223:192]xbar_to_m06_couplers_WDATA;
  wire xbar_to_m06_couplers_WREADY;
  wire [27:24]xbar_to_m06_couplers_WSTRB;
  wire [6:6]xbar_to_m06_couplers_WVALID;
  wire [255:224]xbar_to_m07_couplers_ARADDR;
  wire [23:21]xbar_to_m07_couplers_ARPROT;
  wire xbar_to_m07_couplers_ARREADY;
  wire [7:7]xbar_to_m07_couplers_ARVALID;
  wire [255:224]xbar_to_m07_couplers_AWADDR;
  wire [23:21]xbar_to_m07_couplers_AWPROT;
  wire xbar_to_m07_couplers_AWREADY;
  wire [7:7]xbar_to_m07_couplers_AWVALID;
  wire [7:7]xbar_to_m07_couplers_BREADY;
  wire [1:0]xbar_to_m07_couplers_BRESP;
  wire xbar_to_m07_couplers_BVALID;
  wire [31:0]xbar_to_m07_couplers_RDATA;
  wire [7:7]xbar_to_m07_couplers_RREADY;
  wire [1:0]xbar_to_m07_couplers_RRESP;
  wire xbar_to_m07_couplers_RVALID;
  wire [255:224]xbar_to_m07_couplers_WDATA;
  wire xbar_to_m07_couplers_WREADY;
  wire [31:28]xbar_to_m07_couplers_WSTRB;
  wire [7:7]xbar_to_m07_couplers_WVALID;
  wire [287:256]xbar_to_m08_couplers_ARADDR;
  wire [26:24]xbar_to_m08_couplers_ARPROT;
  wire xbar_to_m08_couplers_ARREADY;
  wire [8:8]xbar_to_m08_couplers_ARVALID;
  wire [287:256]xbar_to_m08_couplers_AWADDR;
  wire [26:24]xbar_to_m08_couplers_AWPROT;
  wire xbar_to_m08_couplers_AWREADY;
  wire [8:8]xbar_to_m08_couplers_AWVALID;
  wire [8:8]xbar_to_m08_couplers_BREADY;
  wire [1:0]xbar_to_m08_couplers_BRESP;
  wire xbar_to_m08_couplers_BVALID;
  wire [31:0]xbar_to_m08_couplers_RDATA;
  wire [8:8]xbar_to_m08_couplers_RREADY;
  wire [1:0]xbar_to_m08_couplers_RRESP;
  wire xbar_to_m08_couplers_RVALID;
  wire [287:256]xbar_to_m08_couplers_WDATA;
  wire xbar_to_m08_couplers_WREADY;
  wire [35:32]xbar_to_m08_couplers_WSTRB;
  wire [8:8]xbar_to_m08_couplers_WVALID;
  wire [319:288]xbar_to_m09_couplers_ARADDR;
  wire [29:27]xbar_to_m09_couplers_ARPROT;
  wire xbar_to_m09_couplers_ARREADY;
  wire [9:9]xbar_to_m09_couplers_ARVALID;
  wire [319:288]xbar_to_m09_couplers_AWADDR;
  wire [29:27]xbar_to_m09_couplers_AWPROT;
  wire xbar_to_m09_couplers_AWREADY;
  wire [9:9]xbar_to_m09_couplers_AWVALID;
  wire [9:9]xbar_to_m09_couplers_BREADY;
  wire [1:0]xbar_to_m09_couplers_BRESP;
  wire xbar_to_m09_couplers_BVALID;
  wire [31:0]xbar_to_m09_couplers_RDATA;
  wire [9:9]xbar_to_m09_couplers_RREADY;
  wire [1:0]xbar_to_m09_couplers_RRESP;
  wire xbar_to_m09_couplers_RVALID;
  wire [319:288]xbar_to_m09_couplers_WDATA;
  wire xbar_to_m09_couplers_WREADY;
  wire [39:36]xbar_to_m09_couplers_WSTRB;
  wire [9:9]xbar_to_m09_couplers_WVALID;
  wire [351:320]xbar_to_m10_couplers_ARADDR;
  wire [32:30]xbar_to_m10_couplers_ARPROT;
  wire xbar_to_m10_couplers_ARREADY;
  wire [10:10]xbar_to_m10_couplers_ARVALID;
  wire [351:320]xbar_to_m10_couplers_AWADDR;
  wire [32:30]xbar_to_m10_couplers_AWPROT;
  wire xbar_to_m10_couplers_AWREADY;
  wire [10:10]xbar_to_m10_couplers_AWVALID;
  wire [10:10]xbar_to_m10_couplers_BREADY;
  wire [1:0]xbar_to_m10_couplers_BRESP;
  wire xbar_to_m10_couplers_BVALID;
  wire [31:0]xbar_to_m10_couplers_RDATA;
  wire [10:10]xbar_to_m10_couplers_RREADY;
  wire [1:0]xbar_to_m10_couplers_RRESP;
  wire xbar_to_m10_couplers_RVALID;
  wire [351:320]xbar_to_m10_couplers_WDATA;
  wire xbar_to_m10_couplers_WREADY;
  wire [43:40]xbar_to_m10_couplers_WSTRB;
  wire [10:10]xbar_to_m10_couplers_WVALID;
  wire [383:352]xbar_to_m11_couplers_ARADDR;
  wire xbar_to_m11_couplers_ARREADY;
  wire [11:11]xbar_to_m11_couplers_ARVALID;
  wire [383:352]xbar_to_m11_couplers_AWADDR;
  wire xbar_to_m11_couplers_AWREADY;
  wire [11:11]xbar_to_m11_couplers_AWVALID;
  wire [11:11]xbar_to_m11_couplers_BREADY;
  wire [1:0]xbar_to_m11_couplers_BRESP;
  wire xbar_to_m11_couplers_BVALID;
  wire [31:0]xbar_to_m11_couplers_RDATA;
  wire [11:11]xbar_to_m11_couplers_RREADY;
  wire [1:0]xbar_to_m11_couplers_RRESP;
  wire xbar_to_m11_couplers_RVALID;
  wire [383:352]xbar_to_m11_couplers_WDATA;
  wire xbar_to_m11_couplers_WREADY;
  wire [47:44]xbar_to_m11_couplers_WSTRB;
  wire [11:11]xbar_to_m11_couplers_WVALID;
  wire [35:0]NLW_xbar_m_axi_arprot_UNCONNECTED;
  wire [35:0]NLW_xbar_m_axi_awprot_UNCONNECTED;

  assign M00_AXI_araddr[31:0] = m00_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M00_AXI_arvalid = m00_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M00_AXI_awaddr[31:0] = m00_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M00_AXI_awvalid = m00_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M00_AXI_bready = m00_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M00_AXI_rready = m00_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M00_AXI_wvalid = m00_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M01_AXI_araddr[31:0] = m01_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M01_AXI_arprot[2:0] = m01_couplers_to_microblaze_0_axi_periph_ARPROT;
  assign M01_AXI_arvalid = m01_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M01_AXI_awprot[2:0] = m01_couplers_to_microblaze_0_axi_periph_AWPROT;
  assign M01_AXI_awvalid = m01_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M01_AXI_bready = m01_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M01_AXI_rready = m01_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M01_AXI_wdata[31:0] = m01_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M01_AXI_wstrb[3:0] = m01_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M01_AXI_wvalid = m01_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M02_AXI_araddr[31:0] = m02_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M02_AXI_arprot[2:0] = m02_couplers_to_microblaze_0_axi_periph_ARPROT;
  assign M02_AXI_arvalid = m02_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M02_AXI_awaddr[31:0] = m02_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M02_AXI_awprot[2:0] = m02_couplers_to_microblaze_0_axi_periph_AWPROT;
  assign M02_AXI_awvalid = m02_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M02_AXI_bready = m02_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M02_AXI_rready = m02_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M02_AXI_wdata[31:0] = m02_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M02_AXI_wstrb[3:0] = m02_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M02_AXI_wvalid = m02_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M03_AXI_araddr[31:0] = m03_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M03_AXI_arvalid = m03_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M03_AXI_awaddr[31:0] = m03_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M03_AXI_awvalid = m03_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M03_AXI_bready = m03_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M03_AXI_rready = m03_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M03_AXI_wdata[31:0] = m03_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M03_AXI_wstrb[3:0] = m03_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M03_AXI_wvalid = m03_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M04_AXI_araddr[31:0] = m04_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M04_AXI_arvalid[0] = m04_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M04_AXI_awaddr[31:0] = m04_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M04_AXI_awvalid[0] = m04_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M04_AXI_bready[0] = m04_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M04_AXI_rready[0] = m04_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M04_AXI_wdata[31:0] = m04_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M04_AXI_wstrb[3:0] = m04_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M04_AXI_wvalid[0] = m04_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M05_AXI_araddr[31:0] = m05_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M05_AXI_arvalid = m05_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M05_AXI_awaddr[31:0] = m05_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M05_AXI_awvalid = m05_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M05_AXI_bready = m05_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M05_AXI_rready = m05_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M05_AXI_wdata[31:0] = m05_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M05_AXI_wstrb[3:0] = m05_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M05_AXI_wvalid = m05_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M06_AXI_araddr[31:0] = m06_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M06_AXI_arprot[2:0] = m06_couplers_to_microblaze_0_axi_periph_ARPROT;
  assign M06_AXI_arvalid = m06_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M06_AXI_awaddr[31:0] = m06_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M06_AXI_awprot[2:0] = m06_couplers_to_microblaze_0_axi_periph_AWPROT;
  assign M06_AXI_awvalid = m06_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M06_AXI_bready = m06_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M06_AXI_rready = m06_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M06_AXI_wdata[31:0] = m06_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M06_AXI_wstrb[3:0] = m06_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M06_AXI_wvalid = m06_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M07_AXI_araddr[31:0] = m07_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M07_AXI_arprot[2:0] = m07_couplers_to_microblaze_0_axi_periph_ARPROT;
  assign M07_AXI_arvalid = m07_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M07_AXI_awaddr[31:0] = m07_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M07_AXI_awprot[2:0] = m07_couplers_to_microblaze_0_axi_periph_AWPROT;
  assign M07_AXI_awvalid = m07_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M07_AXI_bready = m07_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M07_AXI_rready = m07_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M07_AXI_wdata[31:0] = m07_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M07_AXI_wstrb[3:0] = m07_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M07_AXI_wvalid = m07_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M08_AXI_araddr[31:0] = m08_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M08_AXI_arprot[2:0] = m08_couplers_to_microblaze_0_axi_periph_ARPROT;
  assign M08_AXI_arvalid = m08_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M08_AXI_awaddr[31:0] = m08_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M08_AXI_awprot[2:0] = m08_couplers_to_microblaze_0_axi_periph_AWPROT;
  assign M08_AXI_awvalid = m08_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M08_AXI_bready = m08_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M08_AXI_rready = m08_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M08_AXI_wdata[31:0] = m08_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M08_AXI_wstrb[3:0] = m08_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M08_AXI_wvalid = m08_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M09_AXI_araddr[12:0] = m09_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M09_AXI_arburst[1:0] = m09_couplers_to_microblaze_0_axi_periph_ARBURST;
  assign M09_AXI_arcache[3:0] = m09_couplers_to_microblaze_0_axi_periph_ARCACHE;
  assign M09_AXI_arlen[7:0] = m09_couplers_to_microblaze_0_axi_periph_ARLEN;
  assign M09_AXI_arlock = m09_couplers_to_microblaze_0_axi_periph_ARLOCK;
  assign M09_AXI_arprot[2:0] = m09_couplers_to_microblaze_0_axi_periph_ARPROT;
  assign M09_AXI_arsize[2:0] = m09_couplers_to_microblaze_0_axi_periph_ARSIZE;
  assign M09_AXI_arvalid = m09_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M09_AXI_awaddr[12:0] = m09_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M09_AXI_awburst[1:0] = m09_couplers_to_microblaze_0_axi_periph_AWBURST;
  assign M09_AXI_awcache[3:0] = m09_couplers_to_microblaze_0_axi_periph_AWCACHE;
  assign M09_AXI_awlen[7:0] = m09_couplers_to_microblaze_0_axi_periph_AWLEN;
  assign M09_AXI_awlock = m09_couplers_to_microblaze_0_axi_periph_AWLOCK;
  assign M09_AXI_awprot[2:0] = m09_couplers_to_microblaze_0_axi_periph_AWPROT;
  assign M09_AXI_awsize[2:0] = m09_couplers_to_microblaze_0_axi_periph_AWSIZE;
  assign M09_AXI_awvalid = m09_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M09_AXI_bready = m09_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M09_AXI_rready = m09_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M09_AXI_wdata[31:0] = m09_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M09_AXI_wlast = m09_couplers_to_microblaze_0_axi_periph_WLAST;
  assign M09_AXI_wstrb[3:0] = m09_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M09_AXI_wvalid = m09_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M10_AXI_araddr[31:0] = m10_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M10_AXI_arprot[2:0] = m10_couplers_to_microblaze_0_axi_periph_ARPROT;
  assign M10_AXI_arvalid = m10_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M10_AXI_awaddr[31:0] = m10_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M10_AXI_awprot[2:0] = m10_couplers_to_microblaze_0_axi_periph_AWPROT;
  assign M10_AXI_awvalid = m10_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M10_AXI_bready = m10_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M10_AXI_rready = m10_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M10_AXI_wdata[31:0] = m10_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M10_AXI_wstrb[3:0] = m10_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M10_AXI_wvalid = m10_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M11_AXI_araddr[31:0] = m11_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M11_AXI_arvalid = m11_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M11_AXI_awaddr[31:0] = m11_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M11_AXI_awvalid = m11_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M11_AXI_bready = m11_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M11_AXI_rready = m11_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M11_AXI_wdata[31:0] = m11_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M11_AXI_wstrb[3:0] = m11_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M11_AXI_wvalid = m11_couplers_to_microblaze_0_axi_periph_WVALID;
  assign S00_AXI_arready[0] = microblaze_0_axi_periph_to_s00_couplers_ARREADY;
  assign S00_AXI_awready[0] = microblaze_0_axi_periph_to_s00_couplers_AWREADY;
  assign S00_AXI_bresp[1:0] = microblaze_0_axi_periph_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid[0] = microblaze_0_axi_periph_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = microblaze_0_axi_periph_to_s00_couplers_RDATA;
  assign S00_AXI_rresp[1:0] = microblaze_0_axi_periph_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid[0] = microblaze_0_axi_periph_to_s00_couplers_RVALID;
  assign S00_AXI_wready[0] = microblaze_0_axi_periph_to_s00_couplers_WREADY;
  assign m00_couplers_to_microblaze_0_axi_periph_ARREADY = M00_AXI_arready;
  assign m00_couplers_to_microblaze_0_axi_periph_AWREADY = M00_AXI_awready;
  assign m00_couplers_to_microblaze_0_axi_periph_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_microblaze_0_axi_periph_BVALID = M00_AXI_bvalid;
  assign m00_couplers_to_microblaze_0_axi_periph_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_microblaze_0_axi_periph_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_microblaze_0_axi_periph_RVALID = M00_AXI_rvalid;
  assign m00_couplers_to_microblaze_0_axi_periph_WREADY = M00_AXI_wready;
  assign m01_couplers_to_microblaze_0_axi_periph_ARREADY = M01_AXI_arready;
  assign m01_couplers_to_microblaze_0_axi_periph_AWREADY = M01_AXI_awready;
  assign m01_couplers_to_microblaze_0_axi_periph_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_microblaze_0_axi_periph_BVALID = M01_AXI_bvalid;
  assign m01_couplers_to_microblaze_0_axi_periph_RDATA = M01_AXI_rdata[31:0];
  assign m01_couplers_to_microblaze_0_axi_periph_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_microblaze_0_axi_periph_RVALID = M01_AXI_rvalid;
  assign m01_couplers_to_microblaze_0_axi_periph_WREADY = M01_AXI_wready;
  assign m02_couplers_to_microblaze_0_axi_periph_ARREADY = M02_AXI_arready;
  assign m02_couplers_to_microblaze_0_axi_periph_AWREADY = M02_AXI_awready;
  assign m02_couplers_to_microblaze_0_axi_periph_BRESP = M02_AXI_bresp[1:0];
  assign m02_couplers_to_microblaze_0_axi_periph_BVALID = M02_AXI_bvalid;
  assign m02_couplers_to_microblaze_0_axi_periph_RDATA = M02_AXI_rdata[31:0];
  assign m02_couplers_to_microblaze_0_axi_periph_RRESP = M02_AXI_rresp[1:0];
  assign m02_couplers_to_microblaze_0_axi_periph_RVALID = M02_AXI_rvalid;
  assign m02_couplers_to_microblaze_0_axi_periph_WREADY = M02_AXI_wready;
  assign m03_couplers_to_microblaze_0_axi_periph_ARREADY = M03_AXI_arready;
  assign m03_couplers_to_microblaze_0_axi_periph_AWREADY = M03_AXI_awready;
  assign m03_couplers_to_microblaze_0_axi_periph_BRESP = M03_AXI_bresp[1:0];
  assign m03_couplers_to_microblaze_0_axi_periph_BVALID = M03_AXI_bvalid;
  assign m03_couplers_to_microblaze_0_axi_periph_RDATA = M03_AXI_rdata[31:0];
  assign m03_couplers_to_microblaze_0_axi_periph_RRESP = M03_AXI_rresp[1:0];
  assign m03_couplers_to_microblaze_0_axi_periph_RVALID = M03_AXI_rvalid;
  assign m03_couplers_to_microblaze_0_axi_periph_WREADY = M03_AXI_wready;
  assign m04_couplers_to_microblaze_0_axi_periph_ARREADY = M04_AXI_arready[0];
  assign m04_couplers_to_microblaze_0_axi_periph_AWREADY = M04_AXI_awready[0];
  assign m04_couplers_to_microblaze_0_axi_periph_BRESP = M04_AXI_bresp[1:0];
  assign m04_couplers_to_microblaze_0_axi_periph_BVALID = M04_AXI_bvalid[0];
  assign m04_couplers_to_microblaze_0_axi_periph_RDATA = M04_AXI_rdata[31:0];
  assign m04_couplers_to_microblaze_0_axi_periph_RRESP = M04_AXI_rresp[1:0];
  assign m04_couplers_to_microblaze_0_axi_periph_RVALID = M04_AXI_rvalid[0];
  assign m04_couplers_to_microblaze_0_axi_periph_WREADY = M04_AXI_wready[0];
  assign m05_couplers_to_microblaze_0_axi_periph_ARREADY = M05_AXI_arready;
  assign m05_couplers_to_microblaze_0_axi_periph_AWREADY = M05_AXI_awready;
  assign m05_couplers_to_microblaze_0_axi_periph_BRESP = M05_AXI_bresp[1:0];
  assign m05_couplers_to_microblaze_0_axi_periph_BVALID = M05_AXI_bvalid;
  assign m05_couplers_to_microblaze_0_axi_periph_RDATA = M05_AXI_rdata[31:0];
  assign m05_couplers_to_microblaze_0_axi_periph_RRESP = M05_AXI_rresp[1:0];
  assign m05_couplers_to_microblaze_0_axi_periph_RVALID = M05_AXI_rvalid;
  assign m05_couplers_to_microblaze_0_axi_periph_WREADY = M05_AXI_wready;
  assign m06_couplers_to_microblaze_0_axi_periph_ARREADY = M06_AXI_arready;
  assign m06_couplers_to_microblaze_0_axi_periph_AWREADY = M06_AXI_awready;
  assign m06_couplers_to_microblaze_0_axi_periph_BRESP = M06_AXI_bresp[1:0];
  assign m06_couplers_to_microblaze_0_axi_periph_BVALID = M06_AXI_bvalid;
  assign m06_couplers_to_microblaze_0_axi_periph_RDATA = M06_AXI_rdata[31:0];
  assign m06_couplers_to_microblaze_0_axi_periph_RRESP = M06_AXI_rresp[1:0];
  assign m06_couplers_to_microblaze_0_axi_periph_RVALID = M06_AXI_rvalid;
  assign m06_couplers_to_microblaze_0_axi_periph_WREADY = M06_AXI_wready;
  assign m07_couplers_to_microblaze_0_axi_periph_ARREADY = M07_AXI_arready;
  assign m07_couplers_to_microblaze_0_axi_periph_AWREADY = M07_AXI_awready;
  assign m07_couplers_to_microblaze_0_axi_periph_BRESP = M07_AXI_bresp[1:0];
  assign m07_couplers_to_microblaze_0_axi_periph_BVALID = M07_AXI_bvalid;
  assign m07_couplers_to_microblaze_0_axi_periph_RDATA = M07_AXI_rdata[31:0];
  assign m07_couplers_to_microblaze_0_axi_periph_RRESP = M07_AXI_rresp[1:0];
  assign m07_couplers_to_microblaze_0_axi_periph_RVALID = M07_AXI_rvalid;
  assign m07_couplers_to_microblaze_0_axi_periph_WREADY = M07_AXI_wready;
  assign m08_couplers_to_microblaze_0_axi_periph_ARREADY = M08_AXI_arready;
  assign m08_couplers_to_microblaze_0_axi_periph_AWREADY = M08_AXI_awready;
  assign m08_couplers_to_microblaze_0_axi_periph_BRESP = M08_AXI_bresp[1:0];
  assign m08_couplers_to_microblaze_0_axi_periph_BVALID = M08_AXI_bvalid;
  assign m08_couplers_to_microblaze_0_axi_periph_RDATA = M08_AXI_rdata[31:0];
  assign m08_couplers_to_microblaze_0_axi_periph_RRESP = M08_AXI_rresp[1:0];
  assign m08_couplers_to_microblaze_0_axi_periph_RVALID = M08_AXI_rvalid;
  assign m08_couplers_to_microblaze_0_axi_periph_WREADY = M08_AXI_wready;
  assign m09_couplers_to_microblaze_0_axi_periph_ARREADY = M09_AXI_arready;
  assign m09_couplers_to_microblaze_0_axi_periph_AWREADY = M09_AXI_awready;
  assign m09_couplers_to_microblaze_0_axi_periph_BRESP = M09_AXI_bresp[1:0];
  assign m09_couplers_to_microblaze_0_axi_periph_BVALID = M09_AXI_bvalid;
  assign m09_couplers_to_microblaze_0_axi_periph_RDATA = M09_AXI_rdata[31:0];
  assign m09_couplers_to_microblaze_0_axi_periph_RLAST = M09_AXI_rlast;
  assign m09_couplers_to_microblaze_0_axi_periph_RRESP = M09_AXI_rresp[1:0];
  assign m09_couplers_to_microblaze_0_axi_periph_RVALID = M09_AXI_rvalid;
  assign m09_couplers_to_microblaze_0_axi_periph_WREADY = M09_AXI_wready;
  assign m10_couplers_to_microblaze_0_axi_periph_ARREADY = M10_AXI_arready;
  assign m10_couplers_to_microblaze_0_axi_periph_AWREADY = M10_AXI_awready;
  assign m10_couplers_to_microblaze_0_axi_periph_BRESP = M10_AXI_bresp[1:0];
  assign m10_couplers_to_microblaze_0_axi_periph_BVALID = M10_AXI_bvalid;
  assign m10_couplers_to_microblaze_0_axi_periph_RDATA = M10_AXI_rdata[31:0];
  assign m10_couplers_to_microblaze_0_axi_periph_RRESP = M10_AXI_rresp[1:0];
  assign m10_couplers_to_microblaze_0_axi_periph_RVALID = M10_AXI_rvalid;
  assign m10_couplers_to_microblaze_0_axi_periph_WREADY = M10_AXI_wready;
  assign m11_couplers_to_microblaze_0_axi_periph_ARREADY = M11_AXI_arready;
  assign m11_couplers_to_microblaze_0_axi_periph_AWREADY = M11_AXI_awready;
  assign m11_couplers_to_microblaze_0_axi_periph_BRESP = M11_AXI_bresp[1:0];
  assign m11_couplers_to_microblaze_0_axi_periph_BVALID = M11_AXI_bvalid;
  assign m11_couplers_to_microblaze_0_axi_periph_RDATA = M11_AXI_rdata[31:0];
  assign m11_couplers_to_microblaze_0_axi_periph_RRESP = M11_AXI_rresp[1:0];
  assign m11_couplers_to_microblaze_0_axi_periph_RVALID = M11_AXI_rvalid;
  assign m11_couplers_to_microblaze_0_axi_periph_WREADY = M11_AXI_wready;
  assign microblaze_0_axi_periph_ACLK_net = ACLK;
  assign microblaze_0_axi_periph_ARESETN_net = ARESETN;
  assign microblaze_0_axi_periph_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign microblaze_0_axi_periph_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign microblaze_0_axi_periph_to_s00_couplers_ARVALID = S00_AXI_arvalid[0];
  assign microblaze_0_axi_periph_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign microblaze_0_axi_periph_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign microblaze_0_axi_periph_to_s00_couplers_AWVALID = S00_AXI_awvalid[0];
  assign microblaze_0_axi_periph_to_s00_couplers_BREADY = S00_AXI_bready[0];
  assign microblaze_0_axi_periph_to_s00_couplers_RREADY = S00_AXI_rready[0];
  assign microblaze_0_axi_periph_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign microblaze_0_axi_periph_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign microblaze_0_axi_periph_to_s00_couplers_WVALID = S00_AXI_wvalid[0];
  m00_couplers_imp_B3DJVS m00_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m00_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m00_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m00_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m00_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m00_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m00_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m00_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m00_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m00_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m00_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m00_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m00_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m00_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  m01_couplers_imp_1JOCD8P m01_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m01_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arprot(m01_couplers_to_microblaze_0_axi_periph_ARPROT),
        .M_AXI_arready(m01_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awprot(m01_couplers_to_microblaze_0_axi_periph_AWPROT),
        .M_AXI_awready(m01_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m01_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m01_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m01_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m01_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m01_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m01_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m01_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m01_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m01_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m01_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m01_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m01_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m01_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m01_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
  m02_couplers_imp_9HHM5N m02_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m02_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arprot(m02_couplers_to_microblaze_0_axi_periph_ARPROT),
        .M_AXI_arready(m02_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m02_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m02_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awprot(m02_couplers_to_microblaze_0_axi_periph_AWPROT),
        .M_AXI_awready(m02_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m02_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m02_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m02_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m02_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m02_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m02_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m02_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m02_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m02_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m02_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m02_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m02_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m02_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m02_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m02_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m02_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m02_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m02_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m02_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m02_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m02_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m02_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m02_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m02_couplers_RDATA),
        .S_AXI_rready(xbar_to_m02_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m02_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m02_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m02_couplers_WDATA),
        .S_AXI_wready(xbar_to_m02_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m02_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m02_couplers_WVALID));
  m03_couplers_imp_1L0KQYY m03_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m03_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m03_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m03_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m03_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m03_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m03_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m03_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m03_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m03_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m03_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m03_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m03_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m03_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m03_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m03_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m03_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m03_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m03_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m03_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m03_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m03_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m03_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m03_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m03_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m03_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m03_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m03_couplers_RDATA),
        .S_AXI_rready(xbar_to_m03_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m03_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m03_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m03_couplers_WDATA),
        .S_AXI_wready(xbar_to_m03_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m03_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m03_couplers_WVALID));
  m04_couplers_imp_CAR3E6 m04_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m04_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m04_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m04_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m04_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m04_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m04_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m04_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m04_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m04_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m04_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m04_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m04_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m04_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m04_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m04_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m04_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m04_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m04_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m04_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m04_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m04_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m04_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m04_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m04_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m04_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m04_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m04_couplers_RDATA),
        .S_AXI_rready(xbar_to_m04_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m04_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m04_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m04_couplers_WDATA),
        .S_AXI_wready(xbar_to_m04_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m04_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m04_couplers_WVALID));
  m05_couplers_imp_1I3VKLR m05_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m05_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m05_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m05_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m05_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m05_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m05_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m05_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m05_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m05_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m05_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m05_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m05_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m05_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m05_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m05_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m05_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m05_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m05_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m05_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m05_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m05_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m05_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m05_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m05_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m05_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m05_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m05_couplers_RDATA),
        .S_AXI_rready(xbar_to_m05_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m05_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m05_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m05_couplers_WDATA),
        .S_AXI_wready(xbar_to_m05_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m05_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m05_couplers_WVALID));
  m06_couplers_imp_BIT7CD m06_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m06_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arprot(m06_couplers_to_microblaze_0_axi_periph_ARPROT),
        .M_AXI_arready(m06_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m06_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m06_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awprot(m06_couplers_to_microblaze_0_axi_periph_AWPROT),
        .M_AXI_awready(m06_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m06_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m06_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m06_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m06_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m06_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m06_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m06_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m06_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m06_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m06_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m06_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m06_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m06_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m06_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m06_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m06_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m06_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m06_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m06_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m06_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m06_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m06_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m06_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m06_couplers_RDATA),
        .S_AXI_rready(xbar_to_m06_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m06_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m06_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m06_couplers_WDATA),
        .S_AXI_wready(xbar_to_m06_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m06_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m06_couplers_WVALID));
  m07_couplers_imp_1J63O24 m07_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m07_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arprot(m07_couplers_to_microblaze_0_axi_periph_ARPROT),
        .M_AXI_arready(m07_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m07_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m07_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awprot(m07_couplers_to_microblaze_0_axi_periph_AWPROT),
        .M_AXI_awready(m07_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m07_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m07_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m07_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m07_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m07_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m07_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m07_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m07_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m07_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m07_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m07_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m07_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m07_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m07_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m07_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m07_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m07_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m07_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m07_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m07_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m07_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m07_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m07_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m07_couplers_RDATA),
        .S_AXI_rready(xbar_to_m07_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m07_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m07_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m07_couplers_WDATA),
        .S_AXI_wready(xbar_to_m07_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m07_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m07_couplers_WVALID));
  m08_couplers_imp_DI3MJO m08_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m08_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arprot(m08_couplers_to_microblaze_0_axi_periph_ARPROT),
        .M_AXI_arready(m08_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m08_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m08_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awprot(m08_couplers_to_microblaze_0_axi_periph_AWPROT),
        .M_AXI_awready(m08_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m08_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m08_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m08_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m08_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m08_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m08_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m08_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m08_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m08_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m08_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m08_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m08_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m08_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m08_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m08_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m08_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m08_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m08_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m08_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m08_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m08_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m08_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m08_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m08_couplers_RDATA),
        .S_AXI_rready(xbar_to_m08_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m08_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m08_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m08_couplers_WDATA),
        .S_AXI_wready(xbar_to_m08_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m08_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m08_couplers_WVALID));
  m09_couplers_imp_1Q56KUD m09_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m09_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arburst(m09_couplers_to_microblaze_0_axi_periph_ARBURST),
        .M_AXI_arcache(m09_couplers_to_microblaze_0_axi_periph_ARCACHE),
        .M_AXI_arlen(m09_couplers_to_microblaze_0_axi_periph_ARLEN),
        .M_AXI_arlock(m09_couplers_to_microblaze_0_axi_periph_ARLOCK),
        .M_AXI_arprot(m09_couplers_to_microblaze_0_axi_periph_ARPROT),
        .M_AXI_arready(m09_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arsize(m09_couplers_to_microblaze_0_axi_periph_ARSIZE),
        .M_AXI_arvalid(m09_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m09_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awburst(m09_couplers_to_microblaze_0_axi_periph_AWBURST),
        .M_AXI_awcache(m09_couplers_to_microblaze_0_axi_periph_AWCACHE),
        .M_AXI_awlen(m09_couplers_to_microblaze_0_axi_periph_AWLEN),
        .M_AXI_awlock(m09_couplers_to_microblaze_0_axi_periph_AWLOCK),
        .M_AXI_awprot(m09_couplers_to_microblaze_0_axi_periph_AWPROT),
        .M_AXI_awready(m09_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awsize(m09_couplers_to_microblaze_0_axi_periph_AWSIZE),
        .M_AXI_awvalid(m09_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m09_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m09_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m09_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m09_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rlast(m09_couplers_to_microblaze_0_axi_periph_RLAST),
        .M_AXI_rready(m09_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m09_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m09_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m09_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wlast(m09_couplers_to_microblaze_0_axi_periph_WLAST),
        .M_AXI_wready(m09_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m09_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m09_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m09_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m09_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m09_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m09_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m09_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m09_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m09_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m09_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m09_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m09_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m09_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m09_couplers_RDATA),
        .S_AXI_rready(xbar_to_m09_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m09_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m09_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m09_couplers_WDATA),
        .S_AXI_wready(xbar_to_m09_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m09_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m09_couplers_WVALID));
  m10_couplers_imp_1UM9S4W m10_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m10_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arprot(m10_couplers_to_microblaze_0_axi_periph_ARPROT),
        .M_AXI_arready(m10_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m10_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m10_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awprot(m10_couplers_to_microblaze_0_axi_periph_AWPROT),
        .M_AXI_awready(m10_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m10_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m10_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m10_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m10_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m10_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m10_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m10_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m10_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m10_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m10_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m10_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m10_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m10_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m10_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m10_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m10_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m10_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m10_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m10_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m10_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m10_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m10_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m10_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m10_couplers_RDATA),
        .S_AXI_rready(xbar_to_m10_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m10_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m10_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m10_couplers_WDATA),
        .S_AXI_wready(xbar_to_m10_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m10_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m10_couplers_WVALID));
  m11_couplers_imp_8OLNIP m11_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(m11_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m11_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m11_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m11_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m11_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m11_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m11_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m11_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m11_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m11_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m11_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m11_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m11_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m11_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m11_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m11_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m11_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m11_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m11_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m11_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m11_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m11_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m11_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m11_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m11_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m11_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m11_couplers_RDATA),
        .S_AXI_rready(xbar_to_m11_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m11_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m11_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m11_couplers_WDATA),
        .S_AXI_wready(xbar_to_m11_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m11_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m11_couplers_WVALID));
  s00_couplers_imp_1PT77VU s00_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(microblaze_0_axi_periph_to_s00_couplers_ARADDR),
        .S_AXI_arprot(microblaze_0_axi_periph_to_s00_couplers_ARPROT),
        .S_AXI_arready(microblaze_0_axi_periph_to_s00_couplers_ARREADY),
        .S_AXI_arvalid(microblaze_0_axi_periph_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(microblaze_0_axi_periph_to_s00_couplers_AWADDR),
        .S_AXI_awprot(microblaze_0_axi_periph_to_s00_couplers_AWPROT),
        .S_AXI_awready(microblaze_0_axi_periph_to_s00_couplers_AWREADY),
        .S_AXI_awvalid(microblaze_0_axi_periph_to_s00_couplers_AWVALID),
        .S_AXI_bready(microblaze_0_axi_periph_to_s00_couplers_BREADY),
        .S_AXI_bresp(microblaze_0_axi_periph_to_s00_couplers_BRESP),
        .S_AXI_bvalid(microblaze_0_axi_periph_to_s00_couplers_BVALID),
        .S_AXI_rdata(microblaze_0_axi_periph_to_s00_couplers_RDATA),
        .S_AXI_rready(microblaze_0_axi_periph_to_s00_couplers_RREADY),
        .S_AXI_rresp(microblaze_0_axi_periph_to_s00_couplers_RRESP),
        .S_AXI_rvalid(microblaze_0_axi_periph_to_s00_couplers_RVALID),
        .S_AXI_wdata(microblaze_0_axi_periph_to_s00_couplers_WDATA),
        .S_AXI_wready(microblaze_0_axi_periph_to_s00_couplers_WREADY),
        .S_AXI_wstrb(microblaze_0_axi_periph_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(microblaze_0_axi_periph_to_s00_couplers_WVALID));
  design_1_xbar_0 xbar
       (.aclk(microblaze_0_axi_periph_ACLK_net),
        .aresetn(microblaze_0_axi_periph_ARESETN_net),
        .m_axi_araddr({xbar_to_m11_couplers_ARADDR,xbar_to_m10_couplers_ARADDR,xbar_to_m09_couplers_ARADDR,xbar_to_m08_couplers_ARADDR,xbar_to_m07_couplers_ARADDR,xbar_to_m06_couplers_ARADDR,xbar_to_m05_couplers_ARADDR,xbar_to_m04_couplers_ARADDR,xbar_to_m03_couplers_ARADDR,xbar_to_m02_couplers_ARADDR,xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arprot({xbar_to_m10_couplers_ARPROT,xbar_to_m09_couplers_ARPROT,xbar_to_m08_couplers_ARPROT,xbar_to_m07_couplers_ARPROT,xbar_to_m06_couplers_ARPROT,NLW_xbar_m_axi_arprot_UNCONNECTED[17:9],xbar_to_m02_couplers_ARPROT,xbar_to_m01_couplers_ARPROT,NLW_xbar_m_axi_arprot_UNCONNECTED[2:0]}),
        .m_axi_arready({xbar_to_m11_couplers_ARREADY,xbar_to_m10_couplers_ARREADY,xbar_to_m09_couplers_ARREADY,xbar_to_m08_couplers_ARREADY,xbar_to_m07_couplers_ARREADY,xbar_to_m06_couplers_ARREADY,xbar_to_m05_couplers_ARREADY,xbar_to_m04_couplers_ARREADY,xbar_to_m03_couplers_ARREADY,xbar_to_m02_couplers_ARREADY,xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arvalid({xbar_to_m11_couplers_ARVALID,xbar_to_m10_couplers_ARVALID,xbar_to_m09_couplers_ARVALID,xbar_to_m08_couplers_ARVALID,xbar_to_m07_couplers_ARVALID,xbar_to_m06_couplers_ARVALID,xbar_to_m05_couplers_ARVALID,xbar_to_m04_couplers_ARVALID,xbar_to_m03_couplers_ARVALID,xbar_to_m02_couplers_ARVALID,xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m11_couplers_AWADDR,xbar_to_m10_couplers_AWADDR,xbar_to_m09_couplers_AWADDR,xbar_to_m08_couplers_AWADDR,xbar_to_m07_couplers_AWADDR,xbar_to_m06_couplers_AWADDR,xbar_to_m05_couplers_AWADDR,xbar_to_m04_couplers_AWADDR,xbar_to_m03_couplers_AWADDR,xbar_to_m02_couplers_AWADDR,xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awprot({xbar_to_m10_couplers_AWPROT,xbar_to_m09_couplers_AWPROT,xbar_to_m08_couplers_AWPROT,xbar_to_m07_couplers_AWPROT,xbar_to_m06_couplers_AWPROT,NLW_xbar_m_axi_awprot_UNCONNECTED[17:9],xbar_to_m02_couplers_AWPROT,xbar_to_m01_couplers_AWPROT,NLW_xbar_m_axi_awprot_UNCONNECTED[2:0]}),
        .m_axi_awready({xbar_to_m11_couplers_AWREADY,xbar_to_m10_couplers_AWREADY,xbar_to_m09_couplers_AWREADY,xbar_to_m08_couplers_AWREADY,xbar_to_m07_couplers_AWREADY,xbar_to_m06_couplers_AWREADY,xbar_to_m05_couplers_AWREADY,xbar_to_m04_couplers_AWREADY,xbar_to_m03_couplers_AWREADY,xbar_to_m02_couplers_AWREADY,xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awvalid({xbar_to_m11_couplers_AWVALID,xbar_to_m10_couplers_AWVALID,xbar_to_m09_couplers_AWVALID,xbar_to_m08_couplers_AWVALID,xbar_to_m07_couplers_AWVALID,xbar_to_m06_couplers_AWVALID,xbar_to_m05_couplers_AWVALID,xbar_to_m04_couplers_AWVALID,xbar_to_m03_couplers_AWVALID,xbar_to_m02_couplers_AWVALID,xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bready({xbar_to_m11_couplers_BREADY,xbar_to_m10_couplers_BREADY,xbar_to_m09_couplers_BREADY,xbar_to_m08_couplers_BREADY,xbar_to_m07_couplers_BREADY,xbar_to_m06_couplers_BREADY,xbar_to_m05_couplers_BREADY,xbar_to_m04_couplers_BREADY,xbar_to_m03_couplers_BREADY,xbar_to_m02_couplers_BREADY,xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m11_couplers_BRESP,xbar_to_m10_couplers_BRESP,xbar_to_m09_couplers_BRESP,xbar_to_m08_couplers_BRESP,xbar_to_m07_couplers_BRESP,xbar_to_m06_couplers_BRESP,xbar_to_m05_couplers_BRESP,xbar_to_m04_couplers_BRESP,xbar_to_m03_couplers_BRESP,xbar_to_m02_couplers_BRESP,xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m11_couplers_BVALID,xbar_to_m10_couplers_BVALID,xbar_to_m09_couplers_BVALID,xbar_to_m08_couplers_BVALID,xbar_to_m07_couplers_BVALID,xbar_to_m06_couplers_BVALID,xbar_to_m05_couplers_BVALID,xbar_to_m04_couplers_BVALID,xbar_to_m03_couplers_BVALID,xbar_to_m02_couplers_BVALID,xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m11_couplers_RDATA,xbar_to_m10_couplers_RDATA,xbar_to_m09_couplers_RDATA,xbar_to_m08_couplers_RDATA,xbar_to_m07_couplers_RDATA,xbar_to_m06_couplers_RDATA,xbar_to_m05_couplers_RDATA,xbar_to_m04_couplers_RDATA,xbar_to_m03_couplers_RDATA,xbar_to_m02_couplers_RDATA,xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rready({xbar_to_m11_couplers_RREADY,xbar_to_m10_couplers_RREADY,xbar_to_m09_couplers_RREADY,xbar_to_m08_couplers_RREADY,xbar_to_m07_couplers_RREADY,xbar_to_m06_couplers_RREADY,xbar_to_m05_couplers_RREADY,xbar_to_m04_couplers_RREADY,xbar_to_m03_couplers_RREADY,xbar_to_m02_couplers_RREADY,xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m11_couplers_RRESP,xbar_to_m10_couplers_RRESP,xbar_to_m09_couplers_RRESP,xbar_to_m08_couplers_RRESP,xbar_to_m07_couplers_RRESP,xbar_to_m06_couplers_RRESP,xbar_to_m05_couplers_RRESP,xbar_to_m04_couplers_RRESP,xbar_to_m03_couplers_RRESP,xbar_to_m02_couplers_RRESP,xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m11_couplers_RVALID,xbar_to_m10_couplers_RVALID,xbar_to_m09_couplers_RVALID,xbar_to_m08_couplers_RVALID,xbar_to_m07_couplers_RVALID,xbar_to_m06_couplers_RVALID,xbar_to_m05_couplers_RVALID,xbar_to_m04_couplers_RVALID,xbar_to_m03_couplers_RVALID,xbar_to_m02_couplers_RVALID,xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m11_couplers_WDATA,xbar_to_m10_couplers_WDATA,xbar_to_m09_couplers_WDATA,xbar_to_m08_couplers_WDATA,xbar_to_m07_couplers_WDATA,xbar_to_m06_couplers_WDATA,xbar_to_m05_couplers_WDATA,xbar_to_m04_couplers_WDATA,xbar_to_m03_couplers_WDATA,xbar_to_m02_couplers_WDATA,xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wready({xbar_to_m11_couplers_WREADY,xbar_to_m10_couplers_WREADY,xbar_to_m09_couplers_WREADY,xbar_to_m08_couplers_WREADY,xbar_to_m07_couplers_WREADY,xbar_to_m06_couplers_WREADY,xbar_to_m05_couplers_WREADY,xbar_to_m04_couplers_WREADY,xbar_to_m03_couplers_WREADY,xbar_to_m02_couplers_WREADY,xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m11_couplers_WSTRB,xbar_to_m10_couplers_WSTRB,xbar_to_m09_couplers_WSTRB,xbar_to_m08_couplers_WSTRB,xbar_to_m07_couplers_WSTRB,xbar_to_m06_couplers_WSTRB,xbar_to_m05_couplers_WSTRB,xbar_to_m04_couplers_WSTRB,xbar_to_m03_couplers_WSTRB,xbar_to_m02_couplers_WSTRB,xbar_to_m01_couplers_WSTRB,xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m11_couplers_WVALID,xbar_to_m10_couplers_WVALID,xbar_to_m09_couplers_WVALID,xbar_to_m08_couplers_WVALID,xbar_to_m07_couplers_WVALID,xbar_to_m06_couplers_WVALID,xbar_to_m05_couplers_WVALID,xbar_to_m04_couplers_WVALID,xbar_to_m03_couplers_WVALID,xbar_to_m02_couplers_WVALID,xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module dpp_0_imp_A6T9OW
   (AXI_DC_STAB_araddr,
    AXI_DC_STAB_arprot,
    AXI_DC_STAB_arready,
    AXI_DC_STAB_arvalid,
    AXI_DC_STAB_awaddr,
    AXI_DC_STAB_awprot,
    AXI_DC_STAB_awready,
    AXI_DC_STAB_awvalid,
    AXI_DC_STAB_bready,
    AXI_DC_STAB_bresp,
    AXI_DC_STAB_bvalid,
    AXI_DC_STAB_rdata,
    AXI_DC_STAB_rready,
    AXI_DC_STAB_rresp,
    AXI_DC_STAB_rvalid,
    AXI_DC_STAB_wdata,
    AXI_DC_STAB_wready,
    AXI_DC_STAB_wstrb,
    AXI_DC_STAB_wvalid,
    S00_AXI1_araddr,
    S00_AXI1_arprot,
    S00_AXI1_arready,
    S00_AXI1_arvalid,
    S00_AXI1_awaddr,
    S00_AXI1_awprot,
    S00_AXI1_awready,
    S00_AXI1_awvalid,
    S00_AXI1_bready,
    S00_AXI1_bresp,
    S00_AXI1_bvalid,
    S00_AXI1_rdata,
    S00_AXI1_rready,
    S00_AXI1_rresp,
    S00_AXI1_rvalid,
    S00_AXI1_wdata,
    S00_AXI1_wready,
    S00_AXI1_wstrb,
    S00_AXI1_wvalid,
    S00_AXI2_araddr,
    S00_AXI2_arprot,
    S00_AXI2_arready,
    S00_AXI2_arvalid,
    S00_AXI2_awaddr,
    S00_AXI2_awprot,
    S00_AXI2_awready,
    S00_AXI2_awvalid,
    S00_AXI2_bready,
    S00_AXI2_bresp,
    S00_AXI2_bvalid,
    S00_AXI2_rdata,
    S00_AXI2_rready,
    S00_AXI2_rresp,
    S00_AXI2_rvalid,
    S00_AXI2_wdata,
    S00_AXI2_wready,
    S00_AXI2_wstrb,
    S00_AXI2_wvalid,
    S00_AXI3_araddr,
    S00_AXI3_arprot,
    S00_AXI3_arready,
    S00_AXI3_arvalid,
    S00_AXI3_awaddr,
    S00_AXI3_awprot,
    S00_AXI3_awready,
    S00_AXI3_awvalid,
    S00_AXI3_bready,
    S00_AXI3_bresp,
    S00_AXI3_bvalid,
    S00_AXI3_rdata,
    S00_AXI3_rready,
    S00_AXI3_rresp,
    S00_AXI3_rvalid,
    S00_AXI3_wdata,
    S00_AXI3_wready,
    S00_AXI3_wstrb,
    S00_AXI3_wvalid,
    S00_AXI4_araddr,
    S00_AXI4_arprot,
    S00_AXI4_arready,
    S00_AXI4_arvalid,
    S00_AXI4_awaddr,
    S00_AXI4_awprot,
    S00_AXI4_awready,
    S00_AXI4_awvalid,
    S00_AXI4_bready,
    S00_AXI4_bresp,
    S00_AXI4_bvalid,
    S00_AXI4_rdata,
    S00_AXI4_rready,
    S00_AXI4_rresp,
    S00_AXI4_rvalid,
    S00_AXI4_wdata,
    S00_AXI4_wready,
    S00_AXI4_wstrb,
    S00_AXI4_wvalid,
    S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid,
    adc_data,
    axi_clk_domain_s_axi_araddr,
    axi_clk_domain_s_axi_arready,
    axi_clk_domain_s_axi_arvalid,
    axi_clk_domain_s_axi_awaddr,
    axi_clk_domain_s_axi_awready,
    axi_clk_domain_s_axi_awvalid,
    axi_clk_domain_s_axi_bready,
    axi_clk_domain_s_axi_bresp,
    axi_clk_domain_s_axi_bvalid,
    axi_clk_domain_s_axi_rdata,
    axi_clk_domain_s_axi_rready,
    axi_clk_domain_s_axi_rresp,
    axi_clk_domain_s_axi_rvalid,
    axi_clk_domain_s_axi_wdata,
    axi_clk_domain_s_axi_wready,
    axi_clk_domain_s_axi_wstrb,
    axi_clk_domain_s_axi_wvalid,
    axibusdomain_s_axi1_araddr,
    axibusdomain_s_axi1_arready,
    axibusdomain_s_axi1_arvalid,
    axibusdomain_s_axi1_awaddr,
    axibusdomain_s_axi1_awready,
    axibusdomain_s_axi1_awvalid,
    axibusdomain_s_axi1_bready,
    axibusdomain_s_axi1_bresp,
    axibusdomain_s_axi1_bvalid,
    axibusdomain_s_axi1_rdata,
    axibusdomain_s_axi1_rready,
    axibusdomain_s_axi1_rresp,
    axibusdomain_s_axi1_rvalid,
    axibusdomain_s_axi1_wdata,
    axibusdomain_s_axi1_wready,
    axibusdomain_s_axi1_wstrb,
    axibusdomain_s_axi1_wvalid,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rready,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    clk,
    full,
    s00_axi_aclk,
    s00_axi_aresetn);
  input [31:0]AXI_DC_STAB_araddr;
  input [2:0]AXI_DC_STAB_arprot;
  output AXI_DC_STAB_arready;
  input AXI_DC_STAB_arvalid;
  input [31:0]AXI_DC_STAB_awaddr;
  input [2:0]AXI_DC_STAB_awprot;
  output AXI_DC_STAB_awready;
  input AXI_DC_STAB_awvalid;
  input AXI_DC_STAB_bready;
  output [1:0]AXI_DC_STAB_bresp;
  output AXI_DC_STAB_bvalid;
  output [31:0]AXI_DC_STAB_rdata;
  input AXI_DC_STAB_rready;
  output [1:0]AXI_DC_STAB_rresp;
  output AXI_DC_STAB_rvalid;
  input [31:0]AXI_DC_STAB_wdata;
  output AXI_DC_STAB_wready;
  input [3:0]AXI_DC_STAB_wstrb;
  input AXI_DC_STAB_wvalid;
  input [31:0]S00_AXI1_araddr;
  input [2:0]S00_AXI1_arprot;
  output S00_AXI1_arready;
  input S00_AXI1_arvalid;
  input [31:0]S00_AXI1_awaddr;
  input [2:0]S00_AXI1_awprot;
  output S00_AXI1_awready;
  input S00_AXI1_awvalid;
  input S00_AXI1_bready;
  output [1:0]S00_AXI1_bresp;
  output S00_AXI1_bvalid;
  output [31:0]S00_AXI1_rdata;
  input S00_AXI1_rready;
  output [1:0]S00_AXI1_rresp;
  output S00_AXI1_rvalid;
  input [31:0]S00_AXI1_wdata;
  output S00_AXI1_wready;
  input [3:0]S00_AXI1_wstrb;
  input S00_AXI1_wvalid;
  input [31:0]S00_AXI2_araddr;
  input [2:0]S00_AXI2_arprot;
  output S00_AXI2_arready;
  input S00_AXI2_arvalid;
  input [31:0]S00_AXI2_awaddr;
  input [2:0]S00_AXI2_awprot;
  output S00_AXI2_awready;
  input S00_AXI2_awvalid;
  input S00_AXI2_bready;
  output [1:0]S00_AXI2_bresp;
  output S00_AXI2_bvalid;
  output [31:0]S00_AXI2_rdata;
  input S00_AXI2_rready;
  output [1:0]S00_AXI2_rresp;
  output S00_AXI2_rvalid;
  input [31:0]S00_AXI2_wdata;
  output S00_AXI2_wready;
  input [3:0]S00_AXI2_wstrb;
  input S00_AXI2_wvalid;
  input [31:0]S00_AXI3_araddr;
  input [2:0]S00_AXI3_arprot;
  output S00_AXI3_arready;
  input S00_AXI3_arvalid;
  input [31:0]S00_AXI3_awaddr;
  input [2:0]S00_AXI3_awprot;
  output S00_AXI3_awready;
  input S00_AXI3_awvalid;
  input S00_AXI3_bready;
  output [1:0]S00_AXI3_bresp;
  output S00_AXI3_bvalid;
  output [31:0]S00_AXI3_rdata;
  input S00_AXI3_rready;
  output [1:0]S00_AXI3_rresp;
  output S00_AXI3_rvalid;
  input [31:0]S00_AXI3_wdata;
  output S00_AXI3_wready;
  input [3:0]S00_AXI3_wstrb;
  input S00_AXI3_wvalid;
  input [31:0]S00_AXI4_araddr;
  input [2:0]S00_AXI4_arprot;
  output S00_AXI4_arready;
  input S00_AXI4_arvalid;
  input [31:0]S00_AXI4_awaddr;
  input [2:0]S00_AXI4_awprot;
  output S00_AXI4_awready;
  input S00_AXI4_awvalid;
  input S00_AXI4_bready;
  output [1:0]S00_AXI4_bresp;
  output S00_AXI4_bvalid;
  output [31:0]S00_AXI4_rdata;
  input S00_AXI4_rready;
  output [1:0]S00_AXI4_rresp;
  output S00_AXI4_rvalid;
  input [31:0]S00_AXI4_wdata;
  output S00_AXI4_wready;
  input [3:0]S00_AXI4_wstrb;
  input S00_AXI4_wvalid;
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output S00_AXI_arready;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output S00_AXI_awready;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;
  input [12:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [12:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;
  input [13:0]adc_data;
  input [31:0]axi_clk_domain_s_axi_araddr;
  output axi_clk_domain_s_axi_arready;
  input axi_clk_domain_s_axi_arvalid;
  input [31:0]axi_clk_domain_s_axi_awaddr;
  output axi_clk_domain_s_axi_awready;
  input axi_clk_domain_s_axi_awvalid;
  input axi_clk_domain_s_axi_bready;
  output [1:0]axi_clk_domain_s_axi_bresp;
  output axi_clk_domain_s_axi_bvalid;
  output [31:0]axi_clk_domain_s_axi_rdata;
  input axi_clk_domain_s_axi_rready;
  output [1:0]axi_clk_domain_s_axi_rresp;
  output axi_clk_domain_s_axi_rvalid;
  input [31:0]axi_clk_domain_s_axi_wdata;
  output axi_clk_domain_s_axi_wready;
  input [3:0]axi_clk_domain_s_axi_wstrb;
  input axi_clk_domain_s_axi_wvalid;
  input [31:0]axibusdomain_s_axi1_araddr;
  output axibusdomain_s_axi1_arready;
  input axibusdomain_s_axi1_arvalid;
  input [31:0]axibusdomain_s_axi1_awaddr;
  output axibusdomain_s_axi1_awready;
  input axibusdomain_s_axi1_awvalid;
  input axibusdomain_s_axi1_bready;
  output [1:0]axibusdomain_s_axi1_bresp;
  output axibusdomain_s_axi1_bvalid;
  output [31:0]axibusdomain_s_axi1_rdata;
  input axibusdomain_s_axi1_rready;
  output [1:0]axibusdomain_s_axi1_rresp;
  output axibusdomain_s_axi1_rvalid;
  input [31:0]axibusdomain_s_axi1_wdata;
  output axibusdomain_s_axi1_wready;
  input [3:0]axibusdomain_s_axi1_wstrb;
  input axibusdomain_s_axi1_wvalid;
  input [31:0]axibusdomain_s_axi_araddr;
  output axibusdomain_s_axi_arready;
  input axibusdomain_s_axi_arvalid;
  input [31:0]axibusdomain_s_axi_awaddr;
  output axibusdomain_s_axi_awready;
  input axibusdomain_s_axi_awvalid;
  input axibusdomain_s_axi_bready;
  output [1:0]axibusdomain_s_axi_bresp;
  output axibusdomain_s_axi_bvalid;
  output [31:0]axibusdomain_s_axi_rdata;
  input axibusdomain_s_axi_rready;
  output [1:0]axibusdomain_s_axi_rresp;
  output axibusdomain_s_axi_rvalid;
  input [31:0]axibusdomain_s_axi_wdata;
  output axibusdomain_s_axi_wready;
  input [3:0]axibusdomain_s_axi_wstrb;
  input axibusdomain_s_axi_wvalid;
  input clk;
  output [0:0]full;
  input s00_axi_aclk;
  input s00_axi_aresetn;

  wire [31:0]AXI_DC_STAB_1_ARADDR;
  wire [2:0]AXI_DC_STAB_1_ARPROT;
  wire AXI_DC_STAB_1_ARREADY;
  wire AXI_DC_STAB_1_ARVALID;
  wire [31:0]AXI_DC_STAB_1_AWADDR;
  wire [2:0]AXI_DC_STAB_1_AWPROT;
  wire AXI_DC_STAB_1_AWREADY;
  wire AXI_DC_STAB_1_AWVALID;
  wire AXI_DC_STAB_1_BREADY;
  wire [1:0]AXI_DC_STAB_1_BRESP;
  wire AXI_DC_STAB_1_BVALID;
  wire [31:0]AXI_DC_STAB_1_RDATA;
  wire AXI_DC_STAB_1_RREADY;
  wire [1:0]AXI_DC_STAB_1_RRESP;
  wire AXI_DC_STAB_1_RVALID;
  wire [31:0]AXI_DC_STAB_1_WDATA;
  wire AXI_DC_STAB_1_WREADY;
  wire [3:0]AXI_DC_STAB_1_WSTRB;
  wire AXI_DC_STAB_1_WVALID;
  wire [15:0]D_1;
  wire [31:0]S00_AXI1_1_ARADDR;
  wire [2:0]S00_AXI1_1_ARPROT;
  wire S00_AXI1_1_ARREADY;
  wire S00_AXI1_1_ARVALID;
  wire [31:0]S00_AXI1_1_AWADDR;
  wire [2:0]S00_AXI1_1_AWPROT;
  wire S00_AXI1_1_AWREADY;
  wire S00_AXI1_1_AWVALID;
  wire S00_AXI1_1_BREADY;
  wire [1:0]S00_AXI1_1_BRESP;
  wire S00_AXI1_1_BVALID;
  wire [31:0]S00_AXI1_1_RDATA;
  wire S00_AXI1_1_RREADY;
  wire [1:0]S00_AXI1_1_RRESP;
  wire S00_AXI1_1_RVALID;
  wire [31:0]S00_AXI1_1_WDATA;
  wire S00_AXI1_1_WREADY;
  wire [3:0]S00_AXI1_1_WSTRB;
  wire S00_AXI1_1_WVALID;
  wire [31:0]S00_AXI2_1_ARADDR;
  wire [2:0]S00_AXI2_1_ARPROT;
  wire S00_AXI2_1_ARREADY;
  wire S00_AXI2_1_ARVALID;
  wire [31:0]S00_AXI2_1_AWADDR;
  wire [2:0]S00_AXI2_1_AWPROT;
  wire S00_AXI2_1_AWREADY;
  wire S00_AXI2_1_AWVALID;
  wire S00_AXI2_1_BREADY;
  wire [1:0]S00_AXI2_1_BRESP;
  wire S00_AXI2_1_BVALID;
  wire [31:0]S00_AXI2_1_RDATA;
  wire S00_AXI2_1_RREADY;
  wire [1:0]S00_AXI2_1_RRESP;
  wire S00_AXI2_1_RVALID;
  wire [31:0]S00_AXI2_1_WDATA;
  wire S00_AXI2_1_WREADY;
  wire [3:0]S00_AXI2_1_WSTRB;
  wire S00_AXI2_1_WVALID;
  wire [31:0]S00_AXI3_1_ARADDR;
  wire [2:0]S00_AXI3_1_ARPROT;
  wire S00_AXI3_1_ARREADY;
  wire S00_AXI3_1_ARVALID;
  wire [31:0]S00_AXI3_1_AWADDR;
  wire [2:0]S00_AXI3_1_AWPROT;
  wire S00_AXI3_1_AWREADY;
  wire S00_AXI3_1_AWVALID;
  wire S00_AXI3_1_BREADY;
  wire [1:0]S00_AXI3_1_BRESP;
  wire S00_AXI3_1_BVALID;
  wire [31:0]S00_AXI3_1_RDATA;
  wire S00_AXI3_1_RREADY;
  wire [1:0]S00_AXI3_1_RRESP;
  wire S00_AXI3_1_RVALID;
  wire [31:0]S00_AXI3_1_WDATA;
  wire S00_AXI3_1_WREADY;
  wire [3:0]S00_AXI3_1_WSTRB;
  wire S00_AXI3_1_WVALID;
  wire [31:0]S00_AXI4_1_ARADDR;
  wire [2:0]S00_AXI4_1_ARPROT;
  wire S00_AXI4_1_ARREADY;
  wire S00_AXI4_1_ARVALID;
  wire [31:0]S00_AXI4_1_AWADDR;
  wire [2:0]S00_AXI4_1_AWPROT;
  wire S00_AXI4_1_AWREADY;
  wire S00_AXI4_1_AWVALID;
  wire S00_AXI4_1_BREADY;
  wire [1:0]S00_AXI4_1_BRESP;
  wire S00_AXI4_1_BVALID;
  wire [31:0]S00_AXI4_1_RDATA;
  wire S00_AXI4_1_RREADY;
  wire [1:0]S00_AXI4_1_RRESP;
  wire S00_AXI4_1_RVALID;
  wire [31:0]S00_AXI4_1_WDATA;
  wire S00_AXI4_1_WREADY;
  wire [3:0]S00_AXI4_1_WSTRB;
  wire S00_AXI4_1_WVALID;
  wire [31:0]S00_AXI_1_ARADDR;
  wire [2:0]S00_AXI_1_ARPROT;
  wire S00_AXI_1_ARREADY;
  wire S00_AXI_1_ARVALID;
  wire [31:0]S00_AXI_1_AWADDR;
  wire [2:0]S00_AXI_1_AWPROT;
  wire S00_AXI_1_AWREADY;
  wire S00_AXI_1_AWVALID;
  wire S00_AXI_1_BREADY;
  wire [1:0]S00_AXI_1_BRESP;
  wire S00_AXI_1_BVALID;
  wire [31:0]S00_AXI_1_RDATA;
  wire S00_AXI_1_RREADY;
  wire [1:0]S00_AXI_1_RRESP;
  wire S00_AXI_1_RVALID;
  wire [31:0]S00_AXI_1_WDATA;
  wire S00_AXI_1_WREADY;
  wire [3:0]S00_AXI_1_WSTRB;
  wire S00_AXI_1_WVALID;
  wire [12:0]S_AXI_1_ARADDR;
  wire [1:0]S_AXI_1_ARBURST;
  wire [3:0]S_AXI_1_ARCACHE;
  wire [7:0]S_AXI_1_ARLEN;
  wire S_AXI_1_ARLOCK;
  wire [2:0]S_AXI_1_ARPROT;
  wire S_AXI_1_ARREADY;
  wire [2:0]S_AXI_1_ARSIZE;
  wire S_AXI_1_ARVALID;
  wire [12:0]S_AXI_1_AWADDR;
  wire [1:0]S_AXI_1_AWBURST;
  wire [3:0]S_AXI_1_AWCACHE;
  wire [7:0]S_AXI_1_AWLEN;
  wire S_AXI_1_AWLOCK;
  wire [2:0]S_AXI_1_AWPROT;
  wire S_AXI_1_AWREADY;
  wire [2:0]S_AXI_1_AWSIZE;
  wire S_AXI_1_AWVALID;
  wire S_AXI_1_BREADY;
  wire [1:0]S_AXI_1_BRESP;
  wire S_AXI_1_BVALID;
  wire [31:0]S_AXI_1_RDATA;
  wire S_AXI_1_RLAST;
  wire S_AXI_1_RREADY;
  wire [1:0]S_AXI_1_RRESP;
  wire S_AXI_1_RVALID;
  wire [31:0]S_AXI_1_WDATA;
  wire S_AXI_1_WLAST;
  wire S_AXI_1_WREADY;
  wire [3:0]S_AXI_1_WSTRB;
  wire S_AXI_1_WVALID;
  wire [13:0]adc_data_1;
  wire [31:0]axi_clk_domain_s_axi_1_ARADDR;
  wire axi_clk_domain_s_axi_1_ARREADY;
  wire axi_clk_domain_s_axi_1_ARVALID;
  wire [31:0]axi_clk_domain_s_axi_1_AWADDR;
  wire axi_clk_domain_s_axi_1_AWREADY;
  wire axi_clk_domain_s_axi_1_AWVALID;
  wire axi_clk_domain_s_axi_1_BREADY;
  wire [1:0]axi_clk_domain_s_axi_1_BRESP;
  wire axi_clk_domain_s_axi_1_BVALID;
  wire [31:0]axi_clk_domain_s_axi_1_RDATA;
  wire axi_clk_domain_s_axi_1_RREADY;
  wire [1:0]axi_clk_domain_s_axi_1_RRESP;
  wire axi_clk_domain_s_axi_1_RVALID;
  wire [31:0]axi_clk_domain_s_axi_1_WDATA;
  wire axi_clk_domain_s_axi_1_WREADY;
  wire [3:0]axi_clk_domain_s_axi_1_WSTRB;
  wire axi_clk_domain_s_axi_1_WVALID;
  wire [31:0]axibusdomain_s_axi1_1_ARADDR;
  wire axibusdomain_s_axi1_1_ARREADY;
  wire axibusdomain_s_axi1_1_ARVALID;
  wire [31:0]axibusdomain_s_axi1_1_AWADDR;
  wire axibusdomain_s_axi1_1_AWREADY;
  wire axibusdomain_s_axi1_1_AWVALID;
  wire axibusdomain_s_axi1_1_BREADY;
  wire [1:0]axibusdomain_s_axi1_1_BRESP;
  wire axibusdomain_s_axi1_1_BVALID;
  wire [31:0]axibusdomain_s_axi1_1_RDATA;
  wire axibusdomain_s_axi1_1_RREADY;
  wire [1:0]axibusdomain_s_axi1_1_RRESP;
  wire axibusdomain_s_axi1_1_RVALID;
  wire [31:0]axibusdomain_s_axi1_1_WDATA;
  wire axibusdomain_s_axi1_1_WREADY;
  wire [3:0]axibusdomain_s_axi1_1_WSTRB;
  wire axibusdomain_s_axi1_1_WVALID;
  wire [31:0]axibusdomain_s_axi_1_ARADDR;
  wire axibusdomain_s_axi_1_ARREADY;
  wire axibusdomain_s_axi_1_ARVALID;
  wire [31:0]axibusdomain_s_axi_1_AWADDR;
  wire axibusdomain_s_axi_1_AWREADY;
  wire axibusdomain_s_axi_1_AWVALID;
  wire axibusdomain_s_axi_1_BREADY;
  wire [1:0]axibusdomain_s_axi_1_BRESP;
  wire axibusdomain_s_axi_1_BVALID;
  wire [31:0]axibusdomain_s_axi_1_RDATA;
  wire axibusdomain_s_axi_1_RREADY;
  wire [1:0]axibusdomain_s_axi_1_RRESP;
  wire axibusdomain_s_axi_1_RVALID;
  wire [31:0]axibusdomain_s_axi_1_WDATA;
  wire axibusdomain_s_axi_1_WREADY;
  wire [3:0]axibusdomain_s_axi_1_WSTRB;
  wire axibusdomain_s_axi_1_WVALID;
  wire clk_1;
  wire [15:0]dbg_pha_s0_1_peak_amp_rdy_fast_out;
  wire [15:0]dbg_pha_s0_1_peak_amp_rdy_slow_out;
  wire [15:0]dbg_pha_s0_1_peak_det_signal_out;
  wire [15:0]dbg_pha_s0_1_rejectn_out;
  wire [15:0]ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data;
  wire [15:0]ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp;
  wire [15:0]pulse_conditioning_slow_dbg_pulse_cond_slow_m0_blr;
  wire [15:0]pulse_conditioning_slow_dbg_pulse_cond_slow_m0_dc_stab;
  wire [15:0]pulse_conditioning_slow_dbg_pulse_cond_slow_m0_impulse;
  wire [15:0]pulse_conditioning_slow_dbg_pulse_cond_slow_m0_rect;
  wire [15:0]pulse_conditioning_slow_dbg_pulse_cond_slow_m0_shaper;
  wire [15:0]pulse_offseting_outp;
  wire s00_axi_aclk_1;
  wire s00_axi_aresetn_1;
  wire [0:0]scope_full;

  assign AXI_DC_STAB_1_ARADDR = AXI_DC_STAB_araddr[31:0];
  assign AXI_DC_STAB_1_ARPROT = AXI_DC_STAB_arprot[2:0];
  assign AXI_DC_STAB_1_ARVALID = AXI_DC_STAB_arvalid;
  assign AXI_DC_STAB_1_AWADDR = AXI_DC_STAB_awaddr[31:0];
  assign AXI_DC_STAB_1_AWPROT = AXI_DC_STAB_awprot[2:0];
  assign AXI_DC_STAB_1_AWVALID = AXI_DC_STAB_awvalid;
  assign AXI_DC_STAB_1_BREADY = AXI_DC_STAB_bready;
  assign AXI_DC_STAB_1_RREADY = AXI_DC_STAB_rready;
  assign AXI_DC_STAB_1_WDATA = AXI_DC_STAB_wdata[31:0];
  assign AXI_DC_STAB_1_WSTRB = AXI_DC_STAB_wstrb[3:0];
  assign AXI_DC_STAB_1_WVALID = AXI_DC_STAB_wvalid;
  assign AXI_DC_STAB_arready = AXI_DC_STAB_1_ARREADY;
  assign AXI_DC_STAB_awready = AXI_DC_STAB_1_AWREADY;
  assign AXI_DC_STAB_bresp[1:0] = AXI_DC_STAB_1_BRESP;
  assign AXI_DC_STAB_bvalid = AXI_DC_STAB_1_BVALID;
  assign AXI_DC_STAB_rdata[31:0] = AXI_DC_STAB_1_RDATA;
  assign AXI_DC_STAB_rresp[1:0] = AXI_DC_STAB_1_RRESP;
  assign AXI_DC_STAB_rvalid = AXI_DC_STAB_1_RVALID;
  assign AXI_DC_STAB_wready = AXI_DC_STAB_1_WREADY;
  assign S00_AXI1_1_ARADDR = S00_AXI1_araddr[31:0];
  assign S00_AXI1_1_ARPROT = S00_AXI1_arprot[2:0];
  assign S00_AXI1_1_ARVALID = S00_AXI1_arvalid;
  assign S00_AXI1_1_AWADDR = S00_AXI1_awaddr[31:0];
  assign S00_AXI1_1_AWPROT = S00_AXI1_awprot[2:0];
  assign S00_AXI1_1_AWVALID = S00_AXI1_awvalid;
  assign S00_AXI1_1_BREADY = S00_AXI1_bready;
  assign S00_AXI1_1_RREADY = S00_AXI1_rready;
  assign S00_AXI1_1_WDATA = S00_AXI1_wdata[31:0];
  assign S00_AXI1_1_WSTRB = S00_AXI1_wstrb[3:0];
  assign S00_AXI1_1_WVALID = S00_AXI1_wvalid;
  assign S00_AXI1_arready = S00_AXI1_1_ARREADY;
  assign S00_AXI1_awready = S00_AXI1_1_AWREADY;
  assign S00_AXI1_bresp[1:0] = S00_AXI1_1_BRESP;
  assign S00_AXI1_bvalid = S00_AXI1_1_BVALID;
  assign S00_AXI1_rdata[31:0] = S00_AXI1_1_RDATA;
  assign S00_AXI1_rresp[1:0] = S00_AXI1_1_RRESP;
  assign S00_AXI1_rvalid = S00_AXI1_1_RVALID;
  assign S00_AXI1_wready = S00_AXI1_1_WREADY;
  assign S00_AXI2_1_ARADDR = S00_AXI2_araddr[31:0];
  assign S00_AXI2_1_ARPROT = S00_AXI2_arprot[2:0];
  assign S00_AXI2_1_ARVALID = S00_AXI2_arvalid;
  assign S00_AXI2_1_AWADDR = S00_AXI2_awaddr[31:0];
  assign S00_AXI2_1_AWPROT = S00_AXI2_awprot[2:0];
  assign S00_AXI2_1_AWVALID = S00_AXI2_awvalid;
  assign S00_AXI2_1_BREADY = S00_AXI2_bready;
  assign S00_AXI2_1_RREADY = S00_AXI2_rready;
  assign S00_AXI2_1_WDATA = S00_AXI2_wdata[31:0];
  assign S00_AXI2_1_WSTRB = S00_AXI2_wstrb[3:0];
  assign S00_AXI2_1_WVALID = S00_AXI2_wvalid;
  assign S00_AXI2_arready = S00_AXI2_1_ARREADY;
  assign S00_AXI2_awready = S00_AXI2_1_AWREADY;
  assign S00_AXI2_bresp[1:0] = S00_AXI2_1_BRESP;
  assign S00_AXI2_bvalid = S00_AXI2_1_BVALID;
  assign S00_AXI2_rdata[31:0] = S00_AXI2_1_RDATA;
  assign S00_AXI2_rresp[1:0] = S00_AXI2_1_RRESP;
  assign S00_AXI2_rvalid = S00_AXI2_1_RVALID;
  assign S00_AXI2_wready = S00_AXI2_1_WREADY;
  assign S00_AXI3_1_ARADDR = S00_AXI3_araddr[31:0];
  assign S00_AXI3_1_ARPROT = S00_AXI3_arprot[2:0];
  assign S00_AXI3_1_ARVALID = S00_AXI3_arvalid;
  assign S00_AXI3_1_AWADDR = S00_AXI3_awaddr[31:0];
  assign S00_AXI3_1_AWPROT = S00_AXI3_awprot[2:0];
  assign S00_AXI3_1_AWVALID = S00_AXI3_awvalid;
  assign S00_AXI3_1_BREADY = S00_AXI3_bready;
  assign S00_AXI3_1_RREADY = S00_AXI3_rready;
  assign S00_AXI3_1_WDATA = S00_AXI3_wdata[31:0];
  assign S00_AXI3_1_WSTRB = S00_AXI3_wstrb[3:0];
  assign S00_AXI3_1_WVALID = S00_AXI3_wvalid;
  assign S00_AXI3_arready = S00_AXI3_1_ARREADY;
  assign S00_AXI3_awready = S00_AXI3_1_AWREADY;
  assign S00_AXI3_bresp[1:0] = S00_AXI3_1_BRESP;
  assign S00_AXI3_bvalid = S00_AXI3_1_BVALID;
  assign S00_AXI3_rdata[31:0] = S00_AXI3_1_RDATA;
  assign S00_AXI3_rresp[1:0] = S00_AXI3_1_RRESP;
  assign S00_AXI3_rvalid = S00_AXI3_1_RVALID;
  assign S00_AXI3_wready = S00_AXI3_1_WREADY;
  assign S00_AXI4_1_ARADDR = S00_AXI4_araddr[31:0];
  assign S00_AXI4_1_ARPROT = S00_AXI4_arprot[2:0];
  assign S00_AXI4_1_ARVALID = S00_AXI4_arvalid;
  assign S00_AXI4_1_AWADDR = S00_AXI4_awaddr[31:0];
  assign S00_AXI4_1_AWPROT = S00_AXI4_awprot[2:0];
  assign S00_AXI4_1_AWVALID = S00_AXI4_awvalid;
  assign S00_AXI4_1_BREADY = S00_AXI4_bready;
  assign S00_AXI4_1_RREADY = S00_AXI4_rready;
  assign S00_AXI4_1_WDATA = S00_AXI4_wdata[31:0];
  assign S00_AXI4_1_WSTRB = S00_AXI4_wstrb[3:0];
  assign S00_AXI4_1_WVALID = S00_AXI4_wvalid;
  assign S00_AXI4_arready = S00_AXI4_1_ARREADY;
  assign S00_AXI4_awready = S00_AXI4_1_AWREADY;
  assign S00_AXI4_bresp[1:0] = S00_AXI4_1_BRESP;
  assign S00_AXI4_bvalid = S00_AXI4_1_BVALID;
  assign S00_AXI4_rdata[31:0] = S00_AXI4_1_RDATA;
  assign S00_AXI4_rresp[1:0] = S00_AXI4_1_RRESP;
  assign S00_AXI4_rvalid = S00_AXI4_1_RVALID;
  assign S00_AXI4_wready = S00_AXI4_1_WREADY;
  assign S00_AXI_1_ARADDR = S00_AXI_araddr[31:0];
  assign S00_AXI_1_ARPROT = S00_AXI_arprot[2:0];
  assign S00_AXI_1_ARVALID = S00_AXI_arvalid;
  assign S00_AXI_1_AWADDR = S00_AXI_awaddr[31:0];
  assign S00_AXI_1_AWPROT = S00_AXI_awprot[2:0];
  assign S00_AXI_1_AWVALID = S00_AXI_awvalid;
  assign S00_AXI_1_BREADY = S00_AXI_bready;
  assign S00_AXI_1_RREADY = S00_AXI_rready;
  assign S00_AXI_1_WDATA = S00_AXI_wdata[31:0];
  assign S00_AXI_1_WSTRB = S00_AXI_wstrb[3:0];
  assign S00_AXI_1_WVALID = S00_AXI_wvalid;
  assign S00_AXI_arready = S00_AXI_1_ARREADY;
  assign S00_AXI_awready = S00_AXI_1_AWREADY;
  assign S00_AXI_bresp[1:0] = S00_AXI_1_BRESP;
  assign S00_AXI_bvalid = S00_AXI_1_BVALID;
  assign S00_AXI_rdata[31:0] = S00_AXI_1_RDATA;
  assign S00_AXI_rresp[1:0] = S00_AXI_1_RRESP;
  assign S00_AXI_rvalid = S00_AXI_1_RVALID;
  assign S00_AXI_wready = S00_AXI_1_WREADY;
  assign S_AXI_1_ARADDR = S_AXI_araddr[12:0];
  assign S_AXI_1_ARBURST = S_AXI_arburst[1:0];
  assign S_AXI_1_ARCACHE = S_AXI_arcache[3:0];
  assign S_AXI_1_ARLEN = S_AXI_arlen[7:0];
  assign S_AXI_1_ARLOCK = S_AXI_arlock;
  assign S_AXI_1_ARPROT = S_AXI_arprot[2:0];
  assign S_AXI_1_ARSIZE = S_AXI_arsize[2:0];
  assign S_AXI_1_ARVALID = S_AXI_arvalid;
  assign S_AXI_1_AWADDR = S_AXI_awaddr[12:0];
  assign S_AXI_1_AWBURST = S_AXI_awburst[1:0];
  assign S_AXI_1_AWCACHE = S_AXI_awcache[3:0];
  assign S_AXI_1_AWLEN = S_AXI_awlen[7:0];
  assign S_AXI_1_AWLOCK = S_AXI_awlock;
  assign S_AXI_1_AWPROT = S_AXI_awprot[2:0];
  assign S_AXI_1_AWSIZE = S_AXI_awsize[2:0];
  assign S_AXI_1_AWVALID = S_AXI_awvalid;
  assign S_AXI_1_BREADY = S_AXI_bready;
  assign S_AXI_1_RREADY = S_AXI_rready;
  assign S_AXI_1_WDATA = S_AXI_wdata[31:0];
  assign S_AXI_1_WLAST = S_AXI_wlast;
  assign S_AXI_1_WSTRB = S_AXI_wstrb[3:0];
  assign S_AXI_1_WVALID = S_AXI_wvalid;
  assign S_AXI_arready = S_AXI_1_ARREADY;
  assign S_AXI_awready = S_AXI_1_AWREADY;
  assign S_AXI_bresp[1:0] = S_AXI_1_BRESP;
  assign S_AXI_bvalid = S_AXI_1_BVALID;
  assign S_AXI_rdata[31:0] = S_AXI_1_RDATA;
  assign S_AXI_rlast = S_AXI_1_RLAST;
  assign S_AXI_rresp[1:0] = S_AXI_1_RRESP;
  assign S_AXI_rvalid = S_AXI_1_RVALID;
  assign S_AXI_wready = S_AXI_1_WREADY;
  assign adc_data_1 = adc_data[13:0];
  assign axi_clk_domain_s_axi_1_ARADDR = axi_clk_domain_s_axi_araddr[31:0];
  assign axi_clk_domain_s_axi_1_ARVALID = axi_clk_domain_s_axi_arvalid;
  assign axi_clk_domain_s_axi_1_AWADDR = axi_clk_domain_s_axi_awaddr[31:0];
  assign axi_clk_domain_s_axi_1_AWVALID = axi_clk_domain_s_axi_awvalid;
  assign axi_clk_domain_s_axi_1_BREADY = axi_clk_domain_s_axi_bready;
  assign axi_clk_domain_s_axi_1_RREADY = axi_clk_domain_s_axi_rready;
  assign axi_clk_domain_s_axi_1_WDATA = axi_clk_domain_s_axi_wdata[31:0];
  assign axi_clk_domain_s_axi_1_WSTRB = axi_clk_domain_s_axi_wstrb[3:0];
  assign axi_clk_domain_s_axi_1_WVALID = axi_clk_domain_s_axi_wvalid;
  assign axi_clk_domain_s_axi_arready = axi_clk_domain_s_axi_1_ARREADY;
  assign axi_clk_domain_s_axi_awready = axi_clk_domain_s_axi_1_AWREADY;
  assign axi_clk_domain_s_axi_bresp[1:0] = axi_clk_domain_s_axi_1_BRESP;
  assign axi_clk_domain_s_axi_bvalid = axi_clk_domain_s_axi_1_BVALID;
  assign axi_clk_domain_s_axi_rdata[31:0] = axi_clk_domain_s_axi_1_RDATA;
  assign axi_clk_domain_s_axi_rresp[1:0] = axi_clk_domain_s_axi_1_RRESP;
  assign axi_clk_domain_s_axi_rvalid = axi_clk_domain_s_axi_1_RVALID;
  assign axi_clk_domain_s_axi_wready = axi_clk_domain_s_axi_1_WREADY;
  assign axibusdomain_s_axi1_1_ARADDR = axibusdomain_s_axi1_araddr[31:0];
  assign axibusdomain_s_axi1_1_ARVALID = axibusdomain_s_axi1_arvalid;
  assign axibusdomain_s_axi1_1_AWADDR = axibusdomain_s_axi1_awaddr[31:0];
  assign axibusdomain_s_axi1_1_AWVALID = axibusdomain_s_axi1_awvalid;
  assign axibusdomain_s_axi1_1_BREADY = axibusdomain_s_axi1_bready;
  assign axibusdomain_s_axi1_1_RREADY = axibusdomain_s_axi1_rready;
  assign axibusdomain_s_axi1_1_WDATA = axibusdomain_s_axi1_wdata[31:0];
  assign axibusdomain_s_axi1_1_WSTRB = axibusdomain_s_axi1_wstrb[3:0];
  assign axibusdomain_s_axi1_1_WVALID = axibusdomain_s_axi1_wvalid;
  assign axibusdomain_s_axi1_arready = axibusdomain_s_axi1_1_ARREADY;
  assign axibusdomain_s_axi1_awready = axibusdomain_s_axi1_1_AWREADY;
  assign axibusdomain_s_axi1_bresp[1:0] = axibusdomain_s_axi1_1_BRESP;
  assign axibusdomain_s_axi1_bvalid = axibusdomain_s_axi1_1_BVALID;
  assign axibusdomain_s_axi1_rdata[31:0] = axibusdomain_s_axi1_1_RDATA;
  assign axibusdomain_s_axi1_rresp[1:0] = axibusdomain_s_axi1_1_RRESP;
  assign axibusdomain_s_axi1_rvalid = axibusdomain_s_axi1_1_RVALID;
  assign axibusdomain_s_axi1_wready = axibusdomain_s_axi1_1_WREADY;
  assign axibusdomain_s_axi_1_ARADDR = axibusdomain_s_axi_araddr[31:0];
  assign axibusdomain_s_axi_1_ARVALID = axibusdomain_s_axi_arvalid;
  assign axibusdomain_s_axi_1_AWADDR = axibusdomain_s_axi_awaddr[31:0];
  assign axibusdomain_s_axi_1_AWVALID = axibusdomain_s_axi_awvalid;
  assign axibusdomain_s_axi_1_BREADY = axibusdomain_s_axi_bready;
  assign axibusdomain_s_axi_1_RREADY = axibusdomain_s_axi_rready;
  assign axibusdomain_s_axi_1_WDATA = axibusdomain_s_axi_wdata[31:0];
  assign axibusdomain_s_axi_1_WSTRB = axibusdomain_s_axi_wstrb[3:0];
  assign axibusdomain_s_axi_1_WVALID = axibusdomain_s_axi_wvalid;
  assign axibusdomain_s_axi_arready = axibusdomain_s_axi_1_ARREADY;
  assign axibusdomain_s_axi_awready = axibusdomain_s_axi_1_AWREADY;
  assign axibusdomain_s_axi_bresp[1:0] = axibusdomain_s_axi_1_BRESP;
  assign axibusdomain_s_axi_bvalid = axibusdomain_s_axi_1_BVALID;
  assign axibusdomain_s_axi_rdata[31:0] = axibusdomain_s_axi_1_RDATA;
  assign axibusdomain_s_axi_rresp[1:0] = axibusdomain_s_axi_1_RRESP;
  assign axibusdomain_s_axi_rvalid = axibusdomain_s_axi_1_RVALID;
  assign axibusdomain_s_axi_wready = axibusdomain_s_axi_1_WREADY;
  assign clk_1 = clk;
  assign full[0] = scope_full;
  assign s00_axi_aclk_1 = s00_axi_aclk;
  assign s00_axi_aresetn_1 = s00_axi_aresetn;
  pha_imp_R9HCV1 pha
       (.D(D_1),
        .S00_ARESETN(s00_axi_aresetn_1),
        .S00_AXI3_araddr(S00_AXI3_1_ARADDR),
        .S00_AXI3_arprot(S00_AXI3_1_ARPROT),
        .S00_AXI3_arready(S00_AXI3_1_ARREADY),
        .S00_AXI3_arvalid(S00_AXI3_1_ARVALID),
        .S00_AXI3_awaddr(S00_AXI3_1_AWADDR),
        .S00_AXI3_awprot(S00_AXI3_1_AWPROT),
        .S00_AXI3_awready(S00_AXI3_1_AWREADY),
        .S00_AXI3_awvalid(S00_AXI3_1_AWVALID),
        .S00_AXI3_bready(S00_AXI3_1_BREADY),
        .S00_AXI3_bresp(S00_AXI3_1_BRESP),
        .S00_AXI3_bvalid(S00_AXI3_1_BVALID),
        .S00_AXI3_rdata(S00_AXI3_1_RDATA),
        .S00_AXI3_rready(S00_AXI3_1_RREADY),
        .S00_AXI3_rresp(S00_AXI3_1_RRESP),
        .S00_AXI3_rvalid(S00_AXI3_1_RVALID),
        .S00_AXI3_wdata(S00_AXI3_1_WDATA),
        .S00_AXI3_wready(S00_AXI3_1_WREADY),
        .S00_AXI3_wstrb(S00_AXI3_1_WSTRB),
        .S00_AXI3_wvalid(S00_AXI3_1_WVALID),
        .S00_AXI4_araddr(S00_AXI4_1_ARADDR),
        .S00_AXI4_arprot(S00_AXI4_1_ARPROT),
        .S00_AXI4_arready(S00_AXI4_1_ARREADY),
        .S00_AXI4_arvalid(S00_AXI4_1_ARVALID),
        .S00_AXI4_awaddr(S00_AXI4_1_AWADDR),
        .S00_AXI4_awprot(S00_AXI4_1_AWPROT),
        .S00_AXI4_awready(S00_AXI4_1_AWREADY),
        .S00_AXI4_awvalid(S00_AXI4_1_AWVALID),
        .S00_AXI4_bready(S00_AXI4_1_BREADY),
        .S00_AXI4_bresp(S00_AXI4_1_BRESP),
        .S00_AXI4_bvalid(S00_AXI4_1_BVALID),
        .S00_AXI4_rdata(S00_AXI4_1_RDATA),
        .S00_AXI4_rready(S00_AXI4_1_RREADY),
        .S00_AXI4_rresp(S00_AXI4_1_RRESP),
        .S00_AXI4_rvalid(S00_AXI4_1_RVALID),
        .S00_AXI4_wdata(S00_AXI4_1_WDATA),
        .S00_AXI4_wready(S00_AXI4_1_WREADY),
        .S00_AXI4_wstrb(S00_AXI4_1_WSTRB),
        .S00_AXI4_wvalid(S00_AXI4_1_WVALID),
        .S_AXI_araddr(S_AXI_1_ARADDR),
        .S_AXI_arburst(S_AXI_1_ARBURST),
        .S_AXI_arcache(S_AXI_1_ARCACHE),
        .S_AXI_arlen(S_AXI_1_ARLEN),
        .S_AXI_arlock(S_AXI_1_ARLOCK),
        .S_AXI_arprot(S_AXI_1_ARPROT),
        .S_AXI_arready(S_AXI_1_ARREADY),
        .S_AXI_arsize(S_AXI_1_ARSIZE),
        .S_AXI_arvalid(S_AXI_1_ARVALID),
        .S_AXI_awaddr(S_AXI_1_AWADDR),
        .S_AXI_awburst(S_AXI_1_AWBURST),
        .S_AXI_awcache(S_AXI_1_AWCACHE),
        .S_AXI_awlen(S_AXI_1_AWLEN),
        .S_AXI_awlock(S_AXI_1_AWLOCK),
        .S_AXI_awprot(S_AXI_1_AWPROT),
        .S_AXI_awready(S_AXI_1_AWREADY),
        .S_AXI_awsize(S_AXI_1_AWSIZE),
        .S_AXI_awvalid(S_AXI_1_AWVALID),
        .S_AXI_bready(S_AXI_1_BREADY),
        .S_AXI_bresp(S_AXI_1_BRESP),
        .S_AXI_bvalid(S_AXI_1_BVALID),
        .S_AXI_rdata(S_AXI_1_RDATA),
        .S_AXI_rlast(S_AXI_1_RLAST),
        .S_AXI_rready(S_AXI_1_RREADY),
        .S_AXI_rresp(S_AXI_1_RRESP),
        .S_AXI_rvalid(S_AXI_1_RVALID),
        .S_AXI_wdata(S_AXI_1_WDATA),
        .S_AXI_wlast(S_AXI_1_WLAST),
        .S_AXI_wready(S_AXI_1_WREADY),
        .S_AXI_wstrb(S_AXI_1_WSTRB),
        .S_AXI_wvalid(S_AXI_1_WVALID),
        .axibusdomain_s_axi1_araddr(axibusdomain_s_axi1_1_ARADDR),
        .axibusdomain_s_axi1_arready(axibusdomain_s_axi1_1_ARREADY),
        .axibusdomain_s_axi1_arvalid(axibusdomain_s_axi1_1_ARVALID),
        .axibusdomain_s_axi1_awaddr(axibusdomain_s_axi1_1_AWADDR),
        .axibusdomain_s_axi1_awready(axibusdomain_s_axi1_1_AWREADY),
        .axibusdomain_s_axi1_awvalid(axibusdomain_s_axi1_1_AWVALID),
        .axibusdomain_s_axi1_bready(axibusdomain_s_axi1_1_BREADY),
        .axibusdomain_s_axi1_bresp(axibusdomain_s_axi1_1_BRESP),
        .axibusdomain_s_axi1_bvalid(axibusdomain_s_axi1_1_BVALID),
        .axibusdomain_s_axi1_rdata(axibusdomain_s_axi1_1_RDATA),
        .axibusdomain_s_axi1_rready(axibusdomain_s_axi1_1_RREADY),
        .axibusdomain_s_axi1_rresp(axibusdomain_s_axi1_1_RRESP),
        .axibusdomain_s_axi1_rvalid(axibusdomain_s_axi1_1_RVALID),
        .axibusdomain_s_axi1_wdata(axibusdomain_s_axi1_1_WDATA),
        .axibusdomain_s_axi1_wready(axibusdomain_s_axi1_1_WREADY),
        .axibusdomain_s_axi1_wstrb(axibusdomain_s_axi1_1_WSTRB),
        .axibusdomain_s_axi1_wvalid(axibusdomain_s_axi1_1_WVALID),
        .clk(clk_1),
        .clk_cpu(s00_axi_aclk_1),
        .dbg_pha_m0_peak_amp_rdy_fast_out(dbg_pha_s0_1_peak_amp_rdy_fast_out),
        .dbg_pha_m0_peak_amp_rdy_slow_out(dbg_pha_s0_1_peak_amp_rdy_slow_out),
        .dbg_pha_m0_peak_det_signal_out(dbg_pha_s0_1_peak_det_signal_out),
        .dbg_pha_m0_rejectn_out(dbg_pha_s0_1_rejectn_out));
  pulse_conditioning_slow_imp_1ED40DV pulse_conditioning_slow
       (.AXI_DC_STAB_araddr(AXI_DC_STAB_1_ARADDR),
        .AXI_DC_STAB_arprot(AXI_DC_STAB_1_ARPROT),
        .AXI_DC_STAB_arready(AXI_DC_STAB_1_ARREADY),
        .AXI_DC_STAB_arvalid(AXI_DC_STAB_1_ARVALID),
        .AXI_DC_STAB_awaddr(AXI_DC_STAB_1_AWADDR),
        .AXI_DC_STAB_awprot(AXI_DC_STAB_1_AWPROT),
        .AXI_DC_STAB_awready(AXI_DC_STAB_1_AWREADY),
        .AXI_DC_STAB_awvalid(AXI_DC_STAB_1_AWVALID),
        .AXI_DC_STAB_bready(AXI_DC_STAB_1_BREADY),
        .AXI_DC_STAB_bresp(AXI_DC_STAB_1_BRESP),
        .AXI_DC_STAB_bvalid(AXI_DC_STAB_1_BVALID),
        .AXI_DC_STAB_rdata(AXI_DC_STAB_1_RDATA),
        .AXI_DC_STAB_rready(AXI_DC_STAB_1_RREADY),
        .AXI_DC_STAB_rresp(AXI_DC_STAB_1_RRESP),
        .AXI_DC_STAB_rvalid(AXI_DC_STAB_1_RVALID),
        .AXI_DC_STAB_wdata(AXI_DC_STAB_1_WDATA),
        .AXI_DC_STAB_wready(AXI_DC_STAB_1_WREADY),
        .AXI_DC_STAB_wstrb(AXI_DC_STAB_1_WSTRB),
        .AXI_DC_STAB_wvalid(AXI_DC_STAB_1_WVALID),
        .S00_ARESETN(s00_axi_aresetn_1),
        .S00_AXI2_araddr(S00_AXI2_1_ARADDR),
        .S00_AXI2_arprot(S00_AXI2_1_ARPROT),
        .S00_AXI2_arready(S00_AXI2_1_ARREADY),
        .S00_AXI2_arvalid(S00_AXI2_1_ARVALID),
        .S00_AXI2_awaddr(S00_AXI2_1_AWADDR),
        .S00_AXI2_awprot(S00_AXI2_1_AWPROT),
        .S00_AXI2_awready(S00_AXI2_1_AWREADY),
        .S00_AXI2_awvalid(S00_AXI2_1_AWVALID),
        .S00_AXI2_bready(S00_AXI2_1_BREADY),
        .S00_AXI2_bresp(S00_AXI2_1_BRESP),
        .S00_AXI2_bvalid(S00_AXI2_1_BVALID),
        .S00_AXI2_rdata(S00_AXI2_1_RDATA),
        .S00_AXI2_rready(S00_AXI2_1_RREADY),
        .S00_AXI2_rresp(S00_AXI2_1_RRESP),
        .S00_AXI2_rvalid(S00_AXI2_1_RVALID),
        .S00_AXI2_wdata(S00_AXI2_1_WDATA),
        .S00_AXI2_wready(S00_AXI2_1_WREADY),
        .S00_AXI2_wstrb(S00_AXI2_1_WSTRB),
        .S00_AXI2_wvalid(S00_AXI2_1_WVALID),
        .axi_clk_domain_s_axi_araddr(axi_clk_domain_s_axi_1_ARADDR),
        .axi_clk_domain_s_axi_arready(axi_clk_domain_s_axi_1_ARREADY),
        .axi_clk_domain_s_axi_arvalid(axi_clk_domain_s_axi_1_ARVALID),
        .axi_clk_domain_s_axi_awaddr(axi_clk_domain_s_axi_1_AWADDR),
        .axi_clk_domain_s_axi_awready(axi_clk_domain_s_axi_1_AWREADY),
        .axi_clk_domain_s_axi_awvalid(axi_clk_domain_s_axi_1_AWVALID),
        .axi_clk_domain_s_axi_bready(axi_clk_domain_s_axi_1_BREADY),
        .axi_clk_domain_s_axi_bresp(axi_clk_domain_s_axi_1_BRESP),
        .axi_clk_domain_s_axi_bvalid(axi_clk_domain_s_axi_1_BVALID),
        .axi_clk_domain_s_axi_rdata(axi_clk_domain_s_axi_1_RDATA),
        .axi_clk_domain_s_axi_rready(axi_clk_domain_s_axi_1_RREADY),
        .axi_clk_domain_s_axi_rresp(axi_clk_domain_s_axi_1_RRESP),
        .axi_clk_domain_s_axi_rvalid(axi_clk_domain_s_axi_1_RVALID),
        .axi_clk_domain_s_axi_wdata(axi_clk_domain_s_axi_1_WDATA),
        .axi_clk_domain_s_axi_wready(axi_clk_domain_s_axi_1_WREADY),
        .axi_clk_domain_s_axi_wstrb(axi_clk_domain_s_axi_1_WSTRB),
        .axi_clk_domain_s_axi_wvalid(axi_clk_domain_s_axi_1_WVALID),
        .clk(clk_1),
        .clk_cpu(s00_axi_aclk_1),
        .data_in(pulse_offseting_outp),
        .dbg_pulse_cond_slow_m0_blr(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_blr),
        .dbg_pulse_cond_slow_m0_dc_stab(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_dc_stab),
        .dbg_pulse_cond_slow_m0_impulse(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_impulse),
        .dbg_pulse_cond_slow_m0_rect(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_rect),
        .dbg_pulse_cond_slow_m0_shaper(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_shaper),
        .y(D_1));
  pulse_offseting_imp_1W6WVCT pulse_offseting
       (.S00_AXI_araddr(S00_AXI_1_ARADDR),
        .S00_AXI_arprot(S00_AXI_1_ARPROT),
        .S00_AXI_arready(S00_AXI_1_ARREADY),
        .S00_AXI_arvalid(S00_AXI_1_ARVALID),
        .S00_AXI_awaddr(S00_AXI_1_AWADDR),
        .S00_AXI_awprot(S00_AXI_1_AWPROT),
        .S00_AXI_awready(S00_AXI_1_AWREADY),
        .S00_AXI_awvalid(S00_AXI_1_AWVALID),
        .S00_AXI_bready(S00_AXI_1_BREADY),
        .S00_AXI_bresp(S00_AXI_1_BRESP),
        .S00_AXI_bvalid(S00_AXI_1_BVALID),
        .S00_AXI_rdata(S00_AXI_1_RDATA),
        .S00_AXI_rready(S00_AXI_1_RREADY),
        .S00_AXI_rresp(S00_AXI_1_RRESP),
        .S00_AXI_rvalid(S00_AXI_1_RVALID),
        .S00_AXI_wdata(S00_AXI_1_WDATA),
        .S00_AXI_wready(S00_AXI_1_WREADY),
        .S00_AXI_wstrb(S00_AXI_1_WSTRB),
        .S00_AXI_wvalid(S00_AXI_1_WVALID),
        .adc_data(adc_data_1),
        .clk(clk_1),
        .dbg_invert_and_offset_m0_adc_data(ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data),
        .dbg_invert_and_offset_m0_outp(ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp),
        .outp(pulse_offseting_outp),
        .s00_axi_aclk(s00_axi_aclk_1),
        .s00_axi_aresetn(s00_axi_aresetn_1));
  scope_imp_DL4TDP scope
       (.S00_AXI_araddr(S00_AXI1_1_ARADDR),
        .S00_AXI_arprot(S00_AXI1_1_ARPROT),
        .S00_AXI_arready(S00_AXI1_1_ARREADY),
        .S00_AXI_arvalid(S00_AXI1_1_ARVALID),
        .S00_AXI_awaddr(S00_AXI1_1_AWADDR),
        .S00_AXI_awprot(S00_AXI1_1_AWPROT),
        .S00_AXI_awready(S00_AXI1_1_AWREADY),
        .S00_AXI_awvalid(S00_AXI1_1_AWVALID),
        .S00_AXI_bready(S00_AXI1_1_BREADY),
        .S00_AXI_bresp(S00_AXI1_1_BRESP),
        .S00_AXI_bvalid(S00_AXI1_1_BVALID),
        .S00_AXI_rdata(S00_AXI1_1_RDATA),
        .S00_AXI_rready(S00_AXI1_1_RREADY),
        .S00_AXI_rresp(S00_AXI1_1_RRESP),
        .S00_AXI_rvalid(S00_AXI1_1_RVALID),
        .S00_AXI_wdata(S00_AXI1_1_WDATA),
        .S00_AXI_wready(S00_AXI1_1_WREADY),
        .S00_AXI_wstrb(S00_AXI1_1_WSTRB),
        .S00_AXI_wvalid(S00_AXI1_1_WVALID),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_1_ARADDR),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_1_ARREADY),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_1_ARVALID),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_1_AWADDR),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_1_AWREADY),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_1_AWVALID),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_1_BREADY),
        .axibusdomain_s_axi_bresp(axibusdomain_s_axi_1_BRESP),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_1_BVALID),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_1_RDATA),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_1_RREADY),
        .axibusdomain_s_axi_rresp(axibusdomain_s_axi_1_RRESP),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_1_RVALID),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_1_WDATA),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_1_WREADY),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_1_WSTRB),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_1_WVALID),
        .clk(clk_1),
        .dbg_invert_and_offset_s0_adc_data(ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data),
        .dbg_invert_and_offset_s0_outp(ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp),
        .dbg_pha_s0_peak_amp_rdy_fast_out(dbg_pha_s0_1_peak_amp_rdy_fast_out),
        .dbg_pha_s0_peak_amp_rdy_slow_out(dbg_pha_s0_1_peak_amp_rdy_slow_out),
        .dbg_pha_s0_peak_det_signal_out(dbg_pha_s0_1_peak_det_signal_out),
        .dbg_pha_s0_rejectn_out(dbg_pha_s0_1_rejectn_out),
        .dbg_pulse_cond_slow_s0_blr(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_blr),
        .dbg_pulse_cond_slow_s0_dc_stab(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_dc_stab),
        .dbg_pulse_cond_slow_s0_impulse(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_impulse),
        .dbg_pulse_cond_slow_s0_rect(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_rect),
        .dbg_pulse_cond_slow_s0_shaper(pulse_conditioning_slow_dbg_pulse_cond_slow_m0_shaper),
        .full(scope_full),
        .s00_axi_aclk(s00_axi_aclk_1),
        .s00_axi_aresetn(s00_axi_aresetn_1));
endmodule

module glue_logic_imp_1C2RNKO
   (In0,
    In1,
    Res);
  input [0:0]In0;
  input [0:0]In1;
  output Res;

  wire [0:0]ip_timers_0_timers_enabled;
  wire [0:0]pkd_peak_amp_rdy;
  wire util_reduced_logic_0_Res;
  wire [1:0]xlconcat_0_dout;

  assign Res = util_reduced_logic_0_Res;
  assign ip_timers_0_timers_enabled = In0[0];
  assign pkd_peak_amp_rdy = In1[0];
  design_1_util_reduced_logic_0_0 util_reduced_logic_0
       (.Op1(xlconcat_0_dout),
        .Res(util_reduced_logic_0_Res));
  design_1_xlconcat_0_0 xlconcat_0
       (.In0(ip_timers_0_timers_enabled),
        .In1(pkd_peak_amp_rdy),
        .dout(xlconcat_0_dout));
endmodule

module m00_couplers_imp_B3DJVS
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m00_couplers_to_m00_couplers_ARADDR;
  wire m00_couplers_to_m00_couplers_ARREADY;
  wire m00_couplers_to_m00_couplers_ARVALID;
  wire [31:0]m00_couplers_to_m00_couplers_AWADDR;
  wire m00_couplers_to_m00_couplers_AWREADY;
  wire m00_couplers_to_m00_couplers_AWVALID;
  wire m00_couplers_to_m00_couplers_BREADY;
  wire [1:0]m00_couplers_to_m00_couplers_BRESP;
  wire m00_couplers_to_m00_couplers_BVALID;
  wire [31:0]m00_couplers_to_m00_couplers_RDATA;
  wire m00_couplers_to_m00_couplers_RREADY;
  wire [1:0]m00_couplers_to_m00_couplers_RRESP;
  wire m00_couplers_to_m00_couplers_RVALID;
  wire [31:0]m00_couplers_to_m00_couplers_WDATA;
  wire m00_couplers_to_m00_couplers_WREADY;
  wire [3:0]m00_couplers_to_m00_couplers_WSTRB;
  wire m00_couplers_to_m00_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m00_couplers_to_m00_couplers_ARADDR;
  assign M_AXI_arvalid = m00_couplers_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m00_couplers_to_m00_couplers_AWADDR;
  assign M_AXI_awvalid = m00_couplers_to_m00_couplers_AWVALID;
  assign M_AXI_bready = m00_couplers_to_m00_couplers_BREADY;
  assign M_AXI_rready = m00_couplers_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m00_couplers_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m00_couplers_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid = m00_couplers_to_m00_couplers_WVALID;
  assign S_AXI_arready = m00_couplers_to_m00_couplers_ARREADY;
  assign S_AXI_awready = m00_couplers_to_m00_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_m00_couplers_BRESP;
  assign S_AXI_bvalid = m00_couplers_to_m00_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m00_couplers_to_m00_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_m00_couplers_RRESP;
  assign S_AXI_rvalid = m00_couplers_to_m00_couplers_RVALID;
  assign S_AXI_wready = m00_couplers_to_m00_couplers_WREADY;
  assign m00_couplers_to_m00_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_m00_couplers_ARREADY = M_AXI_arready;
  assign m00_couplers_to_m00_couplers_ARVALID = S_AXI_arvalid;
  assign m00_couplers_to_m00_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_m00_couplers_AWREADY = M_AXI_awready;
  assign m00_couplers_to_m00_couplers_AWVALID = S_AXI_awvalid;
  assign m00_couplers_to_m00_couplers_BREADY = S_AXI_bready;
  assign m00_couplers_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign m00_couplers_to_m00_couplers_BVALID = M_AXI_bvalid;
  assign m00_couplers_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign m00_couplers_to_m00_couplers_RREADY = S_AXI_rready;
  assign m00_couplers_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign m00_couplers_to_m00_couplers_RVALID = M_AXI_rvalid;
  assign m00_couplers_to_m00_couplers_WDATA = S_AXI_wdata[31:0];
  assign m00_couplers_to_m00_couplers_WREADY = M_AXI_wready;
  assign m00_couplers_to_m00_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m00_couplers_to_m00_couplers_WVALID = S_AXI_wvalid;
endmodule

module m01_couplers_imp_1JOCD8P
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m01_couplers_to_m01_couplers_ARADDR;
  wire [2:0]m01_couplers_to_m01_couplers_ARPROT;
  wire m01_couplers_to_m01_couplers_ARREADY;
  wire m01_couplers_to_m01_couplers_ARVALID;
  wire [31:0]m01_couplers_to_m01_couplers_AWADDR;
  wire [2:0]m01_couplers_to_m01_couplers_AWPROT;
  wire m01_couplers_to_m01_couplers_AWREADY;
  wire m01_couplers_to_m01_couplers_AWVALID;
  wire m01_couplers_to_m01_couplers_BREADY;
  wire [1:0]m01_couplers_to_m01_couplers_BRESP;
  wire m01_couplers_to_m01_couplers_BVALID;
  wire [31:0]m01_couplers_to_m01_couplers_RDATA;
  wire m01_couplers_to_m01_couplers_RREADY;
  wire [1:0]m01_couplers_to_m01_couplers_RRESP;
  wire m01_couplers_to_m01_couplers_RVALID;
  wire [31:0]m01_couplers_to_m01_couplers_WDATA;
  wire m01_couplers_to_m01_couplers_WREADY;
  wire [3:0]m01_couplers_to_m01_couplers_WSTRB;
  wire m01_couplers_to_m01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m01_couplers_to_m01_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m01_couplers_to_m01_couplers_ARPROT;
  assign M_AXI_arvalid = m01_couplers_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m01_couplers_to_m01_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m01_couplers_to_m01_couplers_AWPROT;
  assign M_AXI_awvalid = m01_couplers_to_m01_couplers_AWVALID;
  assign M_AXI_bready = m01_couplers_to_m01_couplers_BREADY;
  assign M_AXI_rready = m01_couplers_to_m01_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m01_couplers_to_m01_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m01_couplers_to_m01_couplers_WSTRB;
  assign M_AXI_wvalid = m01_couplers_to_m01_couplers_WVALID;
  assign S_AXI_arready = m01_couplers_to_m01_couplers_ARREADY;
  assign S_AXI_awready = m01_couplers_to_m01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m01_couplers_to_m01_couplers_BRESP;
  assign S_AXI_bvalid = m01_couplers_to_m01_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m01_couplers_to_m01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m01_couplers_to_m01_couplers_RRESP;
  assign S_AXI_rvalid = m01_couplers_to_m01_couplers_RVALID;
  assign S_AXI_wready = m01_couplers_to_m01_couplers_WREADY;
  assign m01_couplers_to_m01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_m01_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m01_couplers_to_m01_couplers_ARREADY = M_AXI_arready;
  assign m01_couplers_to_m01_couplers_ARVALID = S_AXI_arvalid;
  assign m01_couplers_to_m01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_m01_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m01_couplers_to_m01_couplers_AWREADY = M_AXI_awready;
  assign m01_couplers_to_m01_couplers_AWVALID = S_AXI_awvalid;
  assign m01_couplers_to_m01_couplers_BREADY = S_AXI_bready;
  assign m01_couplers_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign m01_couplers_to_m01_couplers_BVALID = M_AXI_bvalid;
  assign m01_couplers_to_m01_couplers_RDATA = M_AXI_rdata[31:0];
  assign m01_couplers_to_m01_couplers_RREADY = S_AXI_rready;
  assign m01_couplers_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign m01_couplers_to_m01_couplers_RVALID = M_AXI_rvalid;
  assign m01_couplers_to_m01_couplers_WDATA = S_AXI_wdata[31:0];
  assign m01_couplers_to_m01_couplers_WREADY = M_AXI_wready;
  assign m01_couplers_to_m01_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m01_couplers_to_m01_couplers_WVALID = S_AXI_wvalid;
endmodule

module m02_couplers_imp_9HHM5N
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m02_couplers_to_m02_couplers_ARADDR;
  wire [2:0]m02_couplers_to_m02_couplers_ARPROT;
  wire m02_couplers_to_m02_couplers_ARREADY;
  wire m02_couplers_to_m02_couplers_ARVALID;
  wire [31:0]m02_couplers_to_m02_couplers_AWADDR;
  wire [2:0]m02_couplers_to_m02_couplers_AWPROT;
  wire m02_couplers_to_m02_couplers_AWREADY;
  wire m02_couplers_to_m02_couplers_AWVALID;
  wire m02_couplers_to_m02_couplers_BREADY;
  wire [1:0]m02_couplers_to_m02_couplers_BRESP;
  wire m02_couplers_to_m02_couplers_BVALID;
  wire [31:0]m02_couplers_to_m02_couplers_RDATA;
  wire m02_couplers_to_m02_couplers_RREADY;
  wire [1:0]m02_couplers_to_m02_couplers_RRESP;
  wire m02_couplers_to_m02_couplers_RVALID;
  wire [31:0]m02_couplers_to_m02_couplers_WDATA;
  wire m02_couplers_to_m02_couplers_WREADY;
  wire [3:0]m02_couplers_to_m02_couplers_WSTRB;
  wire m02_couplers_to_m02_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m02_couplers_to_m02_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m02_couplers_to_m02_couplers_ARPROT;
  assign M_AXI_arvalid = m02_couplers_to_m02_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m02_couplers_to_m02_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m02_couplers_to_m02_couplers_AWPROT;
  assign M_AXI_awvalid = m02_couplers_to_m02_couplers_AWVALID;
  assign M_AXI_bready = m02_couplers_to_m02_couplers_BREADY;
  assign M_AXI_rready = m02_couplers_to_m02_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m02_couplers_to_m02_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m02_couplers_to_m02_couplers_WSTRB;
  assign M_AXI_wvalid = m02_couplers_to_m02_couplers_WVALID;
  assign S_AXI_arready = m02_couplers_to_m02_couplers_ARREADY;
  assign S_AXI_awready = m02_couplers_to_m02_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m02_couplers_to_m02_couplers_BRESP;
  assign S_AXI_bvalid = m02_couplers_to_m02_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m02_couplers_to_m02_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m02_couplers_to_m02_couplers_RRESP;
  assign S_AXI_rvalid = m02_couplers_to_m02_couplers_RVALID;
  assign S_AXI_wready = m02_couplers_to_m02_couplers_WREADY;
  assign m02_couplers_to_m02_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m02_couplers_to_m02_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m02_couplers_to_m02_couplers_ARREADY = M_AXI_arready;
  assign m02_couplers_to_m02_couplers_ARVALID = S_AXI_arvalid;
  assign m02_couplers_to_m02_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m02_couplers_to_m02_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m02_couplers_to_m02_couplers_AWREADY = M_AXI_awready;
  assign m02_couplers_to_m02_couplers_AWVALID = S_AXI_awvalid;
  assign m02_couplers_to_m02_couplers_BREADY = S_AXI_bready;
  assign m02_couplers_to_m02_couplers_BRESP = M_AXI_bresp[1:0];
  assign m02_couplers_to_m02_couplers_BVALID = M_AXI_bvalid;
  assign m02_couplers_to_m02_couplers_RDATA = M_AXI_rdata[31:0];
  assign m02_couplers_to_m02_couplers_RREADY = S_AXI_rready;
  assign m02_couplers_to_m02_couplers_RRESP = M_AXI_rresp[1:0];
  assign m02_couplers_to_m02_couplers_RVALID = M_AXI_rvalid;
  assign m02_couplers_to_m02_couplers_WDATA = S_AXI_wdata[31:0];
  assign m02_couplers_to_m02_couplers_WREADY = M_AXI_wready;
  assign m02_couplers_to_m02_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m02_couplers_to_m02_couplers_WVALID = S_AXI_wvalid;
endmodule

module m03_couplers_imp_1L0KQYY
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m03_couplers_to_m03_couplers_ARADDR;
  wire m03_couplers_to_m03_couplers_ARREADY;
  wire m03_couplers_to_m03_couplers_ARVALID;
  wire [31:0]m03_couplers_to_m03_couplers_AWADDR;
  wire m03_couplers_to_m03_couplers_AWREADY;
  wire m03_couplers_to_m03_couplers_AWVALID;
  wire m03_couplers_to_m03_couplers_BREADY;
  wire [1:0]m03_couplers_to_m03_couplers_BRESP;
  wire m03_couplers_to_m03_couplers_BVALID;
  wire [31:0]m03_couplers_to_m03_couplers_RDATA;
  wire m03_couplers_to_m03_couplers_RREADY;
  wire [1:0]m03_couplers_to_m03_couplers_RRESP;
  wire m03_couplers_to_m03_couplers_RVALID;
  wire [31:0]m03_couplers_to_m03_couplers_WDATA;
  wire m03_couplers_to_m03_couplers_WREADY;
  wire [3:0]m03_couplers_to_m03_couplers_WSTRB;
  wire m03_couplers_to_m03_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m03_couplers_to_m03_couplers_ARADDR;
  assign M_AXI_arvalid = m03_couplers_to_m03_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m03_couplers_to_m03_couplers_AWADDR;
  assign M_AXI_awvalid = m03_couplers_to_m03_couplers_AWVALID;
  assign M_AXI_bready = m03_couplers_to_m03_couplers_BREADY;
  assign M_AXI_rready = m03_couplers_to_m03_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m03_couplers_to_m03_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m03_couplers_to_m03_couplers_WSTRB;
  assign M_AXI_wvalid = m03_couplers_to_m03_couplers_WVALID;
  assign S_AXI_arready = m03_couplers_to_m03_couplers_ARREADY;
  assign S_AXI_awready = m03_couplers_to_m03_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m03_couplers_to_m03_couplers_BRESP;
  assign S_AXI_bvalid = m03_couplers_to_m03_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m03_couplers_to_m03_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m03_couplers_to_m03_couplers_RRESP;
  assign S_AXI_rvalid = m03_couplers_to_m03_couplers_RVALID;
  assign S_AXI_wready = m03_couplers_to_m03_couplers_WREADY;
  assign m03_couplers_to_m03_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m03_couplers_to_m03_couplers_ARREADY = M_AXI_arready;
  assign m03_couplers_to_m03_couplers_ARVALID = S_AXI_arvalid;
  assign m03_couplers_to_m03_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m03_couplers_to_m03_couplers_AWREADY = M_AXI_awready;
  assign m03_couplers_to_m03_couplers_AWVALID = S_AXI_awvalid;
  assign m03_couplers_to_m03_couplers_BREADY = S_AXI_bready;
  assign m03_couplers_to_m03_couplers_BRESP = M_AXI_bresp[1:0];
  assign m03_couplers_to_m03_couplers_BVALID = M_AXI_bvalid;
  assign m03_couplers_to_m03_couplers_RDATA = M_AXI_rdata[31:0];
  assign m03_couplers_to_m03_couplers_RREADY = S_AXI_rready;
  assign m03_couplers_to_m03_couplers_RRESP = M_AXI_rresp[1:0];
  assign m03_couplers_to_m03_couplers_RVALID = M_AXI_rvalid;
  assign m03_couplers_to_m03_couplers_WDATA = S_AXI_wdata[31:0];
  assign m03_couplers_to_m03_couplers_WREADY = M_AXI_wready;
  assign m03_couplers_to_m03_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m03_couplers_to_m03_couplers_WVALID = S_AXI_wvalid;
endmodule

module m04_couplers_imp_CAR3E6
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m04_couplers_to_m04_couplers_ARADDR;
  wire [0:0]m04_couplers_to_m04_couplers_ARREADY;
  wire [0:0]m04_couplers_to_m04_couplers_ARVALID;
  wire [31:0]m04_couplers_to_m04_couplers_AWADDR;
  wire [0:0]m04_couplers_to_m04_couplers_AWREADY;
  wire [0:0]m04_couplers_to_m04_couplers_AWVALID;
  wire [0:0]m04_couplers_to_m04_couplers_BREADY;
  wire [1:0]m04_couplers_to_m04_couplers_BRESP;
  wire [0:0]m04_couplers_to_m04_couplers_BVALID;
  wire [31:0]m04_couplers_to_m04_couplers_RDATA;
  wire [0:0]m04_couplers_to_m04_couplers_RREADY;
  wire [1:0]m04_couplers_to_m04_couplers_RRESP;
  wire [0:0]m04_couplers_to_m04_couplers_RVALID;
  wire [31:0]m04_couplers_to_m04_couplers_WDATA;
  wire [0:0]m04_couplers_to_m04_couplers_WREADY;
  wire [3:0]m04_couplers_to_m04_couplers_WSTRB;
  wire [0:0]m04_couplers_to_m04_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m04_couplers_to_m04_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m04_couplers_to_m04_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m04_couplers_to_m04_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m04_couplers_to_m04_couplers_AWVALID;
  assign M_AXI_bready[0] = m04_couplers_to_m04_couplers_BREADY;
  assign M_AXI_rready[0] = m04_couplers_to_m04_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m04_couplers_to_m04_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m04_couplers_to_m04_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m04_couplers_to_m04_couplers_WVALID;
  assign S_AXI_arready[0] = m04_couplers_to_m04_couplers_ARREADY;
  assign S_AXI_awready[0] = m04_couplers_to_m04_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m04_couplers_to_m04_couplers_BRESP;
  assign S_AXI_bvalid[0] = m04_couplers_to_m04_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m04_couplers_to_m04_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m04_couplers_to_m04_couplers_RRESP;
  assign S_AXI_rvalid[0] = m04_couplers_to_m04_couplers_RVALID;
  assign S_AXI_wready[0] = m04_couplers_to_m04_couplers_WREADY;
  assign m04_couplers_to_m04_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m04_couplers_to_m04_couplers_ARREADY = M_AXI_arready[0];
  assign m04_couplers_to_m04_couplers_ARVALID = S_AXI_arvalid[0];
  assign m04_couplers_to_m04_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m04_couplers_to_m04_couplers_AWREADY = M_AXI_awready[0];
  assign m04_couplers_to_m04_couplers_AWVALID = S_AXI_awvalid[0];
  assign m04_couplers_to_m04_couplers_BREADY = S_AXI_bready[0];
  assign m04_couplers_to_m04_couplers_BRESP = M_AXI_bresp[1:0];
  assign m04_couplers_to_m04_couplers_BVALID = M_AXI_bvalid[0];
  assign m04_couplers_to_m04_couplers_RDATA = M_AXI_rdata[31:0];
  assign m04_couplers_to_m04_couplers_RREADY = S_AXI_rready[0];
  assign m04_couplers_to_m04_couplers_RRESP = M_AXI_rresp[1:0];
  assign m04_couplers_to_m04_couplers_RVALID = M_AXI_rvalid[0];
  assign m04_couplers_to_m04_couplers_WDATA = S_AXI_wdata[31:0];
  assign m04_couplers_to_m04_couplers_WREADY = M_AXI_wready[0];
  assign m04_couplers_to_m04_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m04_couplers_to_m04_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m05_couplers_imp_1I3VKLR
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m05_couplers_to_m05_couplers_ARADDR;
  wire m05_couplers_to_m05_couplers_ARREADY;
  wire m05_couplers_to_m05_couplers_ARVALID;
  wire [31:0]m05_couplers_to_m05_couplers_AWADDR;
  wire m05_couplers_to_m05_couplers_AWREADY;
  wire m05_couplers_to_m05_couplers_AWVALID;
  wire m05_couplers_to_m05_couplers_BREADY;
  wire [1:0]m05_couplers_to_m05_couplers_BRESP;
  wire m05_couplers_to_m05_couplers_BVALID;
  wire [31:0]m05_couplers_to_m05_couplers_RDATA;
  wire m05_couplers_to_m05_couplers_RREADY;
  wire [1:0]m05_couplers_to_m05_couplers_RRESP;
  wire m05_couplers_to_m05_couplers_RVALID;
  wire [31:0]m05_couplers_to_m05_couplers_WDATA;
  wire m05_couplers_to_m05_couplers_WREADY;
  wire [3:0]m05_couplers_to_m05_couplers_WSTRB;
  wire m05_couplers_to_m05_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m05_couplers_to_m05_couplers_ARADDR;
  assign M_AXI_arvalid = m05_couplers_to_m05_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m05_couplers_to_m05_couplers_AWADDR;
  assign M_AXI_awvalid = m05_couplers_to_m05_couplers_AWVALID;
  assign M_AXI_bready = m05_couplers_to_m05_couplers_BREADY;
  assign M_AXI_rready = m05_couplers_to_m05_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m05_couplers_to_m05_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m05_couplers_to_m05_couplers_WSTRB;
  assign M_AXI_wvalid = m05_couplers_to_m05_couplers_WVALID;
  assign S_AXI_arready = m05_couplers_to_m05_couplers_ARREADY;
  assign S_AXI_awready = m05_couplers_to_m05_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m05_couplers_to_m05_couplers_BRESP;
  assign S_AXI_bvalid = m05_couplers_to_m05_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m05_couplers_to_m05_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m05_couplers_to_m05_couplers_RRESP;
  assign S_AXI_rvalid = m05_couplers_to_m05_couplers_RVALID;
  assign S_AXI_wready = m05_couplers_to_m05_couplers_WREADY;
  assign m05_couplers_to_m05_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m05_couplers_to_m05_couplers_ARREADY = M_AXI_arready;
  assign m05_couplers_to_m05_couplers_ARVALID = S_AXI_arvalid;
  assign m05_couplers_to_m05_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m05_couplers_to_m05_couplers_AWREADY = M_AXI_awready;
  assign m05_couplers_to_m05_couplers_AWVALID = S_AXI_awvalid;
  assign m05_couplers_to_m05_couplers_BREADY = S_AXI_bready;
  assign m05_couplers_to_m05_couplers_BRESP = M_AXI_bresp[1:0];
  assign m05_couplers_to_m05_couplers_BVALID = M_AXI_bvalid;
  assign m05_couplers_to_m05_couplers_RDATA = M_AXI_rdata[31:0];
  assign m05_couplers_to_m05_couplers_RREADY = S_AXI_rready;
  assign m05_couplers_to_m05_couplers_RRESP = M_AXI_rresp[1:0];
  assign m05_couplers_to_m05_couplers_RVALID = M_AXI_rvalid;
  assign m05_couplers_to_m05_couplers_WDATA = S_AXI_wdata[31:0];
  assign m05_couplers_to_m05_couplers_WREADY = M_AXI_wready;
  assign m05_couplers_to_m05_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m05_couplers_to_m05_couplers_WVALID = S_AXI_wvalid;
endmodule

module m06_couplers_imp_BIT7CD
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m06_couplers_to_m06_couplers_ARADDR;
  wire [2:0]m06_couplers_to_m06_couplers_ARPROT;
  wire m06_couplers_to_m06_couplers_ARREADY;
  wire m06_couplers_to_m06_couplers_ARVALID;
  wire [31:0]m06_couplers_to_m06_couplers_AWADDR;
  wire [2:0]m06_couplers_to_m06_couplers_AWPROT;
  wire m06_couplers_to_m06_couplers_AWREADY;
  wire m06_couplers_to_m06_couplers_AWVALID;
  wire m06_couplers_to_m06_couplers_BREADY;
  wire [1:0]m06_couplers_to_m06_couplers_BRESP;
  wire m06_couplers_to_m06_couplers_BVALID;
  wire [31:0]m06_couplers_to_m06_couplers_RDATA;
  wire m06_couplers_to_m06_couplers_RREADY;
  wire [1:0]m06_couplers_to_m06_couplers_RRESP;
  wire m06_couplers_to_m06_couplers_RVALID;
  wire [31:0]m06_couplers_to_m06_couplers_WDATA;
  wire m06_couplers_to_m06_couplers_WREADY;
  wire [3:0]m06_couplers_to_m06_couplers_WSTRB;
  wire m06_couplers_to_m06_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m06_couplers_to_m06_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m06_couplers_to_m06_couplers_ARPROT;
  assign M_AXI_arvalid = m06_couplers_to_m06_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m06_couplers_to_m06_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m06_couplers_to_m06_couplers_AWPROT;
  assign M_AXI_awvalid = m06_couplers_to_m06_couplers_AWVALID;
  assign M_AXI_bready = m06_couplers_to_m06_couplers_BREADY;
  assign M_AXI_rready = m06_couplers_to_m06_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m06_couplers_to_m06_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m06_couplers_to_m06_couplers_WSTRB;
  assign M_AXI_wvalid = m06_couplers_to_m06_couplers_WVALID;
  assign S_AXI_arready = m06_couplers_to_m06_couplers_ARREADY;
  assign S_AXI_awready = m06_couplers_to_m06_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m06_couplers_to_m06_couplers_BRESP;
  assign S_AXI_bvalid = m06_couplers_to_m06_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m06_couplers_to_m06_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m06_couplers_to_m06_couplers_RRESP;
  assign S_AXI_rvalid = m06_couplers_to_m06_couplers_RVALID;
  assign S_AXI_wready = m06_couplers_to_m06_couplers_WREADY;
  assign m06_couplers_to_m06_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m06_couplers_to_m06_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m06_couplers_to_m06_couplers_ARREADY = M_AXI_arready;
  assign m06_couplers_to_m06_couplers_ARVALID = S_AXI_arvalid;
  assign m06_couplers_to_m06_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m06_couplers_to_m06_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m06_couplers_to_m06_couplers_AWREADY = M_AXI_awready;
  assign m06_couplers_to_m06_couplers_AWVALID = S_AXI_awvalid;
  assign m06_couplers_to_m06_couplers_BREADY = S_AXI_bready;
  assign m06_couplers_to_m06_couplers_BRESP = M_AXI_bresp[1:0];
  assign m06_couplers_to_m06_couplers_BVALID = M_AXI_bvalid;
  assign m06_couplers_to_m06_couplers_RDATA = M_AXI_rdata[31:0];
  assign m06_couplers_to_m06_couplers_RREADY = S_AXI_rready;
  assign m06_couplers_to_m06_couplers_RRESP = M_AXI_rresp[1:0];
  assign m06_couplers_to_m06_couplers_RVALID = M_AXI_rvalid;
  assign m06_couplers_to_m06_couplers_WDATA = S_AXI_wdata[31:0];
  assign m06_couplers_to_m06_couplers_WREADY = M_AXI_wready;
  assign m06_couplers_to_m06_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m06_couplers_to_m06_couplers_WVALID = S_AXI_wvalid;
endmodule

module m07_couplers_imp_1J63O24
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m07_couplers_to_m07_couplers_ARADDR;
  wire [2:0]m07_couplers_to_m07_couplers_ARPROT;
  wire m07_couplers_to_m07_couplers_ARREADY;
  wire m07_couplers_to_m07_couplers_ARVALID;
  wire [31:0]m07_couplers_to_m07_couplers_AWADDR;
  wire [2:0]m07_couplers_to_m07_couplers_AWPROT;
  wire m07_couplers_to_m07_couplers_AWREADY;
  wire m07_couplers_to_m07_couplers_AWVALID;
  wire m07_couplers_to_m07_couplers_BREADY;
  wire [1:0]m07_couplers_to_m07_couplers_BRESP;
  wire m07_couplers_to_m07_couplers_BVALID;
  wire [31:0]m07_couplers_to_m07_couplers_RDATA;
  wire m07_couplers_to_m07_couplers_RREADY;
  wire [1:0]m07_couplers_to_m07_couplers_RRESP;
  wire m07_couplers_to_m07_couplers_RVALID;
  wire [31:0]m07_couplers_to_m07_couplers_WDATA;
  wire m07_couplers_to_m07_couplers_WREADY;
  wire [3:0]m07_couplers_to_m07_couplers_WSTRB;
  wire m07_couplers_to_m07_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m07_couplers_to_m07_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m07_couplers_to_m07_couplers_ARPROT;
  assign M_AXI_arvalid = m07_couplers_to_m07_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m07_couplers_to_m07_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m07_couplers_to_m07_couplers_AWPROT;
  assign M_AXI_awvalid = m07_couplers_to_m07_couplers_AWVALID;
  assign M_AXI_bready = m07_couplers_to_m07_couplers_BREADY;
  assign M_AXI_rready = m07_couplers_to_m07_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m07_couplers_to_m07_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m07_couplers_to_m07_couplers_WSTRB;
  assign M_AXI_wvalid = m07_couplers_to_m07_couplers_WVALID;
  assign S_AXI_arready = m07_couplers_to_m07_couplers_ARREADY;
  assign S_AXI_awready = m07_couplers_to_m07_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m07_couplers_to_m07_couplers_BRESP;
  assign S_AXI_bvalid = m07_couplers_to_m07_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m07_couplers_to_m07_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m07_couplers_to_m07_couplers_RRESP;
  assign S_AXI_rvalid = m07_couplers_to_m07_couplers_RVALID;
  assign S_AXI_wready = m07_couplers_to_m07_couplers_WREADY;
  assign m07_couplers_to_m07_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m07_couplers_to_m07_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m07_couplers_to_m07_couplers_ARREADY = M_AXI_arready;
  assign m07_couplers_to_m07_couplers_ARVALID = S_AXI_arvalid;
  assign m07_couplers_to_m07_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m07_couplers_to_m07_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m07_couplers_to_m07_couplers_AWREADY = M_AXI_awready;
  assign m07_couplers_to_m07_couplers_AWVALID = S_AXI_awvalid;
  assign m07_couplers_to_m07_couplers_BREADY = S_AXI_bready;
  assign m07_couplers_to_m07_couplers_BRESP = M_AXI_bresp[1:0];
  assign m07_couplers_to_m07_couplers_BVALID = M_AXI_bvalid;
  assign m07_couplers_to_m07_couplers_RDATA = M_AXI_rdata[31:0];
  assign m07_couplers_to_m07_couplers_RREADY = S_AXI_rready;
  assign m07_couplers_to_m07_couplers_RRESP = M_AXI_rresp[1:0];
  assign m07_couplers_to_m07_couplers_RVALID = M_AXI_rvalid;
  assign m07_couplers_to_m07_couplers_WDATA = S_AXI_wdata[31:0];
  assign m07_couplers_to_m07_couplers_WREADY = M_AXI_wready;
  assign m07_couplers_to_m07_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m07_couplers_to_m07_couplers_WVALID = S_AXI_wvalid;
endmodule

module m08_couplers_imp_DI3MJO
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m08_couplers_to_m08_couplers_ARADDR;
  wire [2:0]m08_couplers_to_m08_couplers_ARPROT;
  wire m08_couplers_to_m08_couplers_ARREADY;
  wire m08_couplers_to_m08_couplers_ARVALID;
  wire [31:0]m08_couplers_to_m08_couplers_AWADDR;
  wire [2:0]m08_couplers_to_m08_couplers_AWPROT;
  wire m08_couplers_to_m08_couplers_AWREADY;
  wire m08_couplers_to_m08_couplers_AWVALID;
  wire m08_couplers_to_m08_couplers_BREADY;
  wire [1:0]m08_couplers_to_m08_couplers_BRESP;
  wire m08_couplers_to_m08_couplers_BVALID;
  wire [31:0]m08_couplers_to_m08_couplers_RDATA;
  wire m08_couplers_to_m08_couplers_RREADY;
  wire [1:0]m08_couplers_to_m08_couplers_RRESP;
  wire m08_couplers_to_m08_couplers_RVALID;
  wire [31:0]m08_couplers_to_m08_couplers_WDATA;
  wire m08_couplers_to_m08_couplers_WREADY;
  wire [3:0]m08_couplers_to_m08_couplers_WSTRB;
  wire m08_couplers_to_m08_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m08_couplers_to_m08_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m08_couplers_to_m08_couplers_ARPROT;
  assign M_AXI_arvalid = m08_couplers_to_m08_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m08_couplers_to_m08_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m08_couplers_to_m08_couplers_AWPROT;
  assign M_AXI_awvalid = m08_couplers_to_m08_couplers_AWVALID;
  assign M_AXI_bready = m08_couplers_to_m08_couplers_BREADY;
  assign M_AXI_rready = m08_couplers_to_m08_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m08_couplers_to_m08_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m08_couplers_to_m08_couplers_WSTRB;
  assign M_AXI_wvalid = m08_couplers_to_m08_couplers_WVALID;
  assign S_AXI_arready = m08_couplers_to_m08_couplers_ARREADY;
  assign S_AXI_awready = m08_couplers_to_m08_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m08_couplers_to_m08_couplers_BRESP;
  assign S_AXI_bvalid = m08_couplers_to_m08_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m08_couplers_to_m08_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m08_couplers_to_m08_couplers_RRESP;
  assign S_AXI_rvalid = m08_couplers_to_m08_couplers_RVALID;
  assign S_AXI_wready = m08_couplers_to_m08_couplers_WREADY;
  assign m08_couplers_to_m08_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m08_couplers_to_m08_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m08_couplers_to_m08_couplers_ARREADY = M_AXI_arready;
  assign m08_couplers_to_m08_couplers_ARVALID = S_AXI_arvalid;
  assign m08_couplers_to_m08_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m08_couplers_to_m08_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m08_couplers_to_m08_couplers_AWREADY = M_AXI_awready;
  assign m08_couplers_to_m08_couplers_AWVALID = S_AXI_awvalid;
  assign m08_couplers_to_m08_couplers_BREADY = S_AXI_bready;
  assign m08_couplers_to_m08_couplers_BRESP = M_AXI_bresp[1:0];
  assign m08_couplers_to_m08_couplers_BVALID = M_AXI_bvalid;
  assign m08_couplers_to_m08_couplers_RDATA = M_AXI_rdata[31:0];
  assign m08_couplers_to_m08_couplers_RREADY = S_AXI_rready;
  assign m08_couplers_to_m08_couplers_RRESP = M_AXI_rresp[1:0];
  assign m08_couplers_to_m08_couplers_RVALID = M_AXI_rvalid;
  assign m08_couplers_to_m08_couplers_WDATA = S_AXI_wdata[31:0];
  assign m08_couplers_to_m08_couplers_WREADY = M_AXI_wready;
  assign m08_couplers_to_m08_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m08_couplers_to_m08_couplers_WVALID = S_AXI_wvalid;
endmodule

module m09_couplers_imp_1Q56KUD
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [12:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [12:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [12:0]auto_pc_to_m09_couplers_ARADDR;
  wire [1:0]auto_pc_to_m09_couplers_ARBURST;
  wire [3:0]auto_pc_to_m09_couplers_ARCACHE;
  wire [7:0]auto_pc_to_m09_couplers_ARLEN;
  wire [0:0]auto_pc_to_m09_couplers_ARLOCK;
  wire [2:0]auto_pc_to_m09_couplers_ARPROT;
  wire auto_pc_to_m09_couplers_ARREADY;
  wire [2:0]auto_pc_to_m09_couplers_ARSIZE;
  wire auto_pc_to_m09_couplers_ARVALID;
  wire [12:0]auto_pc_to_m09_couplers_AWADDR;
  wire [1:0]auto_pc_to_m09_couplers_AWBURST;
  wire [3:0]auto_pc_to_m09_couplers_AWCACHE;
  wire [7:0]auto_pc_to_m09_couplers_AWLEN;
  wire [0:0]auto_pc_to_m09_couplers_AWLOCK;
  wire [2:0]auto_pc_to_m09_couplers_AWPROT;
  wire auto_pc_to_m09_couplers_AWREADY;
  wire [2:0]auto_pc_to_m09_couplers_AWSIZE;
  wire auto_pc_to_m09_couplers_AWVALID;
  wire auto_pc_to_m09_couplers_BREADY;
  wire [1:0]auto_pc_to_m09_couplers_BRESP;
  wire auto_pc_to_m09_couplers_BVALID;
  wire [31:0]auto_pc_to_m09_couplers_RDATA;
  wire auto_pc_to_m09_couplers_RLAST;
  wire auto_pc_to_m09_couplers_RREADY;
  wire [1:0]auto_pc_to_m09_couplers_RRESP;
  wire auto_pc_to_m09_couplers_RVALID;
  wire [31:0]auto_pc_to_m09_couplers_WDATA;
  wire auto_pc_to_m09_couplers_WLAST;
  wire auto_pc_to_m09_couplers_WREADY;
  wire [3:0]auto_pc_to_m09_couplers_WSTRB;
  wire auto_pc_to_m09_couplers_WVALID;
  wire [31:0]m09_couplers_to_auto_pc_ARADDR;
  wire [2:0]m09_couplers_to_auto_pc_ARPROT;
  wire m09_couplers_to_auto_pc_ARREADY;
  wire m09_couplers_to_auto_pc_ARVALID;
  wire [31:0]m09_couplers_to_auto_pc_AWADDR;
  wire [2:0]m09_couplers_to_auto_pc_AWPROT;
  wire m09_couplers_to_auto_pc_AWREADY;
  wire m09_couplers_to_auto_pc_AWVALID;
  wire m09_couplers_to_auto_pc_BREADY;
  wire [1:0]m09_couplers_to_auto_pc_BRESP;
  wire m09_couplers_to_auto_pc_BVALID;
  wire [31:0]m09_couplers_to_auto_pc_RDATA;
  wire m09_couplers_to_auto_pc_RREADY;
  wire [1:0]m09_couplers_to_auto_pc_RRESP;
  wire m09_couplers_to_auto_pc_RVALID;
  wire [31:0]m09_couplers_to_auto_pc_WDATA;
  wire m09_couplers_to_auto_pc_WREADY;
  wire [3:0]m09_couplers_to_auto_pc_WSTRB;
  wire m09_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[12:0] = auto_pc_to_m09_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_pc_to_m09_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_m09_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_pc_to_m09_couplers_ARLEN;
  assign M_AXI_arlock = auto_pc_to_m09_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_m09_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = auto_pc_to_m09_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_m09_couplers_ARVALID;
  assign M_AXI_awaddr[12:0] = auto_pc_to_m09_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_pc_to_m09_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_m09_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_pc_to_m09_couplers_AWLEN;
  assign M_AXI_awlock = auto_pc_to_m09_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_m09_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = auto_pc_to_m09_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_m09_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m09_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m09_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_m09_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_m09_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_pc_to_m09_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m09_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m09_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m09_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bresp[1:0] = m09_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m09_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = m09_couplers_to_auto_pc_RDATA;
  assign S_AXI_rresp[1:0] = m09_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m09_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m09_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m09_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m09_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m09_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m09_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m09_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_m09_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_m09_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m09_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m09_couplers_WREADY = M_AXI_wready;
  assign m09_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m09_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m09_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m09_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m09_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m09_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m09_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m09_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m09_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign m09_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign m09_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  design_1_auto_pc_0 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m09_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_m09_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_m09_couplers_ARCACHE),
        .m_axi_arlen(auto_pc_to_m09_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_m09_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_m09_couplers_ARPROT),
        .m_axi_arready(auto_pc_to_m09_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_m09_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_m09_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m09_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_m09_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_m09_couplers_AWCACHE),
        .m_axi_awlen(auto_pc_to_m09_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_m09_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_m09_couplers_AWPROT),
        .m_axi_awready(auto_pc_to_m09_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_m09_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_m09_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m09_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m09_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m09_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m09_couplers_RDATA),
        .m_axi_rlast(auto_pc_to_m09_couplers_RLAST),
        .m_axi_rready(auto_pc_to_m09_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m09_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m09_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m09_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_m09_couplers_WLAST),
        .m_axi_wready(auto_pc_to_m09_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m09_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m09_couplers_WVALID),
        .s_axi_araddr(m09_couplers_to_auto_pc_ARADDR[12:0]),
        .s_axi_arprot(m09_couplers_to_auto_pc_ARPROT),
        .s_axi_arready(m09_couplers_to_auto_pc_ARREADY),
        .s_axi_arvalid(m09_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m09_couplers_to_auto_pc_AWADDR[12:0]),
        .s_axi_awprot(m09_couplers_to_auto_pc_AWPROT),
        .s_axi_awready(m09_couplers_to_auto_pc_AWREADY),
        .s_axi_awvalid(m09_couplers_to_auto_pc_AWVALID),
        .s_axi_bready(m09_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m09_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m09_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m09_couplers_to_auto_pc_RDATA),
        .s_axi_rready(m09_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m09_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m09_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m09_couplers_to_auto_pc_WDATA),
        .s_axi_wready(m09_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m09_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m09_couplers_to_auto_pc_WVALID));
endmodule

module m10_couplers_imp_1UM9S4W
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m10_couplers_to_m10_couplers_ARADDR;
  wire [2:0]m10_couplers_to_m10_couplers_ARPROT;
  wire m10_couplers_to_m10_couplers_ARREADY;
  wire m10_couplers_to_m10_couplers_ARVALID;
  wire [31:0]m10_couplers_to_m10_couplers_AWADDR;
  wire [2:0]m10_couplers_to_m10_couplers_AWPROT;
  wire m10_couplers_to_m10_couplers_AWREADY;
  wire m10_couplers_to_m10_couplers_AWVALID;
  wire m10_couplers_to_m10_couplers_BREADY;
  wire [1:0]m10_couplers_to_m10_couplers_BRESP;
  wire m10_couplers_to_m10_couplers_BVALID;
  wire [31:0]m10_couplers_to_m10_couplers_RDATA;
  wire m10_couplers_to_m10_couplers_RREADY;
  wire [1:0]m10_couplers_to_m10_couplers_RRESP;
  wire m10_couplers_to_m10_couplers_RVALID;
  wire [31:0]m10_couplers_to_m10_couplers_WDATA;
  wire m10_couplers_to_m10_couplers_WREADY;
  wire [3:0]m10_couplers_to_m10_couplers_WSTRB;
  wire m10_couplers_to_m10_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m10_couplers_to_m10_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m10_couplers_to_m10_couplers_ARPROT;
  assign M_AXI_arvalid = m10_couplers_to_m10_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m10_couplers_to_m10_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m10_couplers_to_m10_couplers_AWPROT;
  assign M_AXI_awvalid = m10_couplers_to_m10_couplers_AWVALID;
  assign M_AXI_bready = m10_couplers_to_m10_couplers_BREADY;
  assign M_AXI_rready = m10_couplers_to_m10_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m10_couplers_to_m10_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m10_couplers_to_m10_couplers_WSTRB;
  assign M_AXI_wvalid = m10_couplers_to_m10_couplers_WVALID;
  assign S_AXI_arready = m10_couplers_to_m10_couplers_ARREADY;
  assign S_AXI_awready = m10_couplers_to_m10_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m10_couplers_to_m10_couplers_BRESP;
  assign S_AXI_bvalid = m10_couplers_to_m10_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m10_couplers_to_m10_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m10_couplers_to_m10_couplers_RRESP;
  assign S_AXI_rvalid = m10_couplers_to_m10_couplers_RVALID;
  assign S_AXI_wready = m10_couplers_to_m10_couplers_WREADY;
  assign m10_couplers_to_m10_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m10_couplers_to_m10_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m10_couplers_to_m10_couplers_ARREADY = M_AXI_arready;
  assign m10_couplers_to_m10_couplers_ARVALID = S_AXI_arvalid;
  assign m10_couplers_to_m10_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m10_couplers_to_m10_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m10_couplers_to_m10_couplers_AWREADY = M_AXI_awready;
  assign m10_couplers_to_m10_couplers_AWVALID = S_AXI_awvalid;
  assign m10_couplers_to_m10_couplers_BREADY = S_AXI_bready;
  assign m10_couplers_to_m10_couplers_BRESP = M_AXI_bresp[1:0];
  assign m10_couplers_to_m10_couplers_BVALID = M_AXI_bvalid;
  assign m10_couplers_to_m10_couplers_RDATA = M_AXI_rdata[31:0];
  assign m10_couplers_to_m10_couplers_RREADY = S_AXI_rready;
  assign m10_couplers_to_m10_couplers_RRESP = M_AXI_rresp[1:0];
  assign m10_couplers_to_m10_couplers_RVALID = M_AXI_rvalid;
  assign m10_couplers_to_m10_couplers_WDATA = S_AXI_wdata[31:0];
  assign m10_couplers_to_m10_couplers_WREADY = M_AXI_wready;
  assign m10_couplers_to_m10_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m10_couplers_to_m10_couplers_WVALID = S_AXI_wvalid;
endmodule

module m11_couplers_imp_8OLNIP
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m11_couplers_to_m11_couplers_ARADDR;
  wire m11_couplers_to_m11_couplers_ARREADY;
  wire m11_couplers_to_m11_couplers_ARVALID;
  wire [31:0]m11_couplers_to_m11_couplers_AWADDR;
  wire m11_couplers_to_m11_couplers_AWREADY;
  wire m11_couplers_to_m11_couplers_AWVALID;
  wire m11_couplers_to_m11_couplers_BREADY;
  wire [1:0]m11_couplers_to_m11_couplers_BRESP;
  wire m11_couplers_to_m11_couplers_BVALID;
  wire [31:0]m11_couplers_to_m11_couplers_RDATA;
  wire m11_couplers_to_m11_couplers_RREADY;
  wire [1:0]m11_couplers_to_m11_couplers_RRESP;
  wire m11_couplers_to_m11_couplers_RVALID;
  wire [31:0]m11_couplers_to_m11_couplers_WDATA;
  wire m11_couplers_to_m11_couplers_WREADY;
  wire [3:0]m11_couplers_to_m11_couplers_WSTRB;
  wire m11_couplers_to_m11_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m11_couplers_to_m11_couplers_ARADDR;
  assign M_AXI_arvalid = m11_couplers_to_m11_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m11_couplers_to_m11_couplers_AWADDR;
  assign M_AXI_awvalid = m11_couplers_to_m11_couplers_AWVALID;
  assign M_AXI_bready = m11_couplers_to_m11_couplers_BREADY;
  assign M_AXI_rready = m11_couplers_to_m11_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m11_couplers_to_m11_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m11_couplers_to_m11_couplers_WSTRB;
  assign M_AXI_wvalid = m11_couplers_to_m11_couplers_WVALID;
  assign S_AXI_arready = m11_couplers_to_m11_couplers_ARREADY;
  assign S_AXI_awready = m11_couplers_to_m11_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m11_couplers_to_m11_couplers_BRESP;
  assign S_AXI_bvalid = m11_couplers_to_m11_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m11_couplers_to_m11_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m11_couplers_to_m11_couplers_RRESP;
  assign S_AXI_rvalid = m11_couplers_to_m11_couplers_RVALID;
  assign S_AXI_wready = m11_couplers_to_m11_couplers_WREADY;
  assign m11_couplers_to_m11_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m11_couplers_to_m11_couplers_ARREADY = M_AXI_arready;
  assign m11_couplers_to_m11_couplers_ARVALID = S_AXI_arvalid;
  assign m11_couplers_to_m11_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m11_couplers_to_m11_couplers_AWREADY = M_AXI_awready;
  assign m11_couplers_to_m11_couplers_AWVALID = S_AXI_awvalid;
  assign m11_couplers_to_m11_couplers_BREADY = S_AXI_bready;
  assign m11_couplers_to_m11_couplers_BRESP = M_AXI_bresp[1:0];
  assign m11_couplers_to_m11_couplers_BVALID = M_AXI_bvalid;
  assign m11_couplers_to_m11_couplers_RDATA = M_AXI_rdata[31:0];
  assign m11_couplers_to_m11_couplers_RREADY = S_AXI_rready;
  assign m11_couplers_to_m11_couplers_RRESP = M_AXI_rresp[1:0];
  assign m11_couplers_to_m11_couplers_RVALID = M_AXI_rvalid;
  assign m11_couplers_to_m11_couplers_WDATA = S_AXI_wdata[31:0];
  assign m11_couplers_to_m11_couplers_WREADY = M_AXI_wready;
  assign m11_couplers_to_m11_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m11_couplers_to_m11_couplers_WVALID = S_AXI_wvalid;
endmodule

module mb_periph_0_imp_1IALCZF
   (S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid,
    s_axi_aclk,
    s_axi_aresetn,
    uart_rtl_rxd,
    uart_rtl_txd);
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;
  input s_axi_aclk;
  input s_axi_aresetn;
  input uart_rtl_rxd;
  output uart_rtl_txd;

  wire axi_uartlite_0_UART_RxD;
  wire axi_uartlite_0_UART_TxD;
  wire microblaze_0_Clk;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_ARADDR;
  wire microblaze_0_axi_periph_M00_AXI_ARREADY;
  wire microblaze_0_axi_periph_M00_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_AWADDR;
  wire microblaze_0_axi_periph_M00_AXI_AWREADY;
  wire microblaze_0_axi_periph_M00_AXI_AWVALID;
  wire microblaze_0_axi_periph_M00_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M00_AXI_BRESP;
  wire microblaze_0_axi_periph_M00_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_RDATA;
  wire microblaze_0_axi_periph_M00_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M00_AXI_RRESP;
  wire microblaze_0_axi_periph_M00_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_WDATA;
  wire microblaze_0_axi_periph_M00_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M00_AXI_WSTRB;
  wire microblaze_0_axi_periph_M00_AXI_WVALID;
  wire rst_clk_wiz_0_120M_peripheral_aresetn;

  assign S_AXI_arready = microblaze_0_axi_periph_M00_AXI_ARREADY;
  assign S_AXI_awready = microblaze_0_axi_periph_M00_AXI_AWREADY;
  assign S_AXI_bresp[1:0] = microblaze_0_axi_periph_M00_AXI_BRESP;
  assign S_AXI_bvalid = microblaze_0_axi_periph_M00_AXI_BVALID;
  assign S_AXI_rdata[31:0] = microblaze_0_axi_periph_M00_AXI_RDATA;
  assign S_AXI_rresp[1:0] = microblaze_0_axi_periph_M00_AXI_RRESP;
  assign S_AXI_rvalid = microblaze_0_axi_periph_M00_AXI_RVALID;
  assign S_AXI_wready = microblaze_0_axi_periph_M00_AXI_WREADY;
  assign axi_uartlite_0_UART_RxD = uart_rtl_rxd;
  assign microblaze_0_Clk = s_axi_aclk;
  assign microblaze_0_axi_periph_M00_AXI_ARADDR = S_AXI_araddr[31:0];
  assign microblaze_0_axi_periph_M00_AXI_ARVALID = S_AXI_arvalid;
  assign microblaze_0_axi_periph_M00_AXI_AWADDR = S_AXI_awaddr[31:0];
  assign microblaze_0_axi_periph_M00_AXI_AWVALID = S_AXI_awvalid;
  assign microblaze_0_axi_periph_M00_AXI_BREADY = S_AXI_bready;
  assign microblaze_0_axi_periph_M00_AXI_RREADY = S_AXI_rready;
  assign microblaze_0_axi_periph_M00_AXI_WDATA = S_AXI_wdata[31:0];
  assign microblaze_0_axi_periph_M00_AXI_WSTRB = S_AXI_wstrb[3:0];
  assign microblaze_0_axi_periph_M00_AXI_WVALID = S_AXI_wvalid;
  assign rst_clk_wiz_0_120M_peripheral_aresetn = s_axi_aresetn;
  assign uart_rtl_txd = axi_uartlite_0_UART_TxD;
  design_1_axi_uartlite_0_0 axi_uartlite_0
       (.rx(axi_uartlite_0_UART_RxD),
        .s_axi_aclk(microblaze_0_Clk),
        .s_axi_araddr(microblaze_0_axi_periph_M00_AXI_ARADDR[3:0]),
        .s_axi_aresetn(rst_clk_wiz_0_120M_peripheral_aresetn),
        .s_axi_arready(microblaze_0_axi_periph_M00_AXI_ARREADY),
        .s_axi_arvalid(microblaze_0_axi_periph_M00_AXI_ARVALID),
        .s_axi_awaddr(microblaze_0_axi_periph_M00_AXI_AWADDR[3:0]),
        .s_axi_awready(microblaze_0_axi_periph_M00_AXI_AWREADY),
        .s_axi_awvalid(microblaze_0_axi_periph_M00_AXI_AWVALID),
        .s_axi_bready(microblaze_0_axi_periph_M00_AXI_BREADY),
        .s_axi_bresp(microblaze_0_axi_periph_M00_AXI_BRESP),
        .s_axi_bvalid(microblaze_0_axi_periph_M00_AXI_BVALID),
        .s_axi_rdata(microblaze_0_axi_periph_M00_AXI_RDATA),
        .s_axi_rready(microblaze_0_axi_periph_M00_AXI_RREADY),
        .s_axi_rresp(microblaze_0_axi_periph_M00_AXI_RRESP),
        .s_axi_rvalid(microblaze_0_axi_periph_M00_AXI_RVALID),
        .s_axi_wdata(microblaze_0_axi_periph_M00_AXI_WDATA),
        .s_axi_wready(microblaze_0_axi_periph_M00_AXI_WREADY),
        .s_axi_wstrb(microblaze_0_axi_periph_M00_AXI_WSTRB),
        .s_axi_wvalid(microblaze_0_axi_periph_M00_AXI_WVALID),
        .tx(axi_uartlite_0_UART_TxD));
endmodule

module mca_imp_44X2PW
   (S00_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid,
    clk,
    clk_cpu,
    peak_amp,
    peak_amp_rdy,
    segment);
  input S00_ARESETN;
  input [12:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [12:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;
  input clk;
  input clk_cpu;
  input [15:0]peak_amp;
  input peak_amp_rdy;
  input segment;

  wire [12:0]Conn1_ARADDR;
  wire [1:0]Conn1_ARBURST;
  wire [3:0]Conn1_ARCACHE;
  wire [7:0]Conn1_ARLEN;
  wire Conn1_ARLOCK;
  wire [2:0]Conn1_ARPROT;
  wire Conn1_ARREADY;
  wire [2:0]Conn1_ARSIZE;
  wire Conn1_ARVALID;
  wire [12:0]Conn1_AWADDR;
  wire [1:0]Conn1_AWBURST;
  wire [3:0]Conn1_AWCACHE;
  wire [7:0]Conn1_AWLEN;
  wire Conn1_AWLOCK;
  wire [2:0]Conn1_AWPROT;
  wire Conn1_AWREADY;
  wire [2:0]Conn1_AWSIZE;
  wire Conn1_AWVALID;
  wire Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire Conn1_BVALID;
  wire [31:0]Conn1_RDATA;
  wire Conn1_RLAST;
  wire Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire Conn1_RVALID;
  wire [31:0]Conn1_WDATA;
  wire Conn1_WLAST;
  wire Conn1_WREADY;
  wire [3:0]Conn1_WSTRB;
  wire Conn1_WVALID;
  wire S00_ARESETN_1;
  wire [12:0]axi_bram_ctrl_0_BRAM_PORTA_ADDR;
  wire axi_bram_ctrl_0_BRAM_PORTA_CLK;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTA_DIN;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTA_DOUT;
  wire axi_bram_ctrl_0_BRAM_PORTA_EN;
  wire axi_bram_ctrl_0_BRAM_PORTA_RST;
  wire [3:0]axi_bram_ctrl_0_BRAM_PORTA_WE;
  wire [13:0]bram_incr_0_BRAM_PORTB_ADDR;
  wire bram_incr_0_BRAM_PORTB_CLK;
  wire [31:0]bram_incr_0_BRAM_PORTB_DIN;
  wire [31:0]bram_incr_0_BRAM_PORTB_DOUT;
  wire bram_incr_0_BRAM_PORTB_EN;
  wire bram_incr_0_BRAM_PORTB_RST;
  wire bram_incr_0_BRAM_PORTB_WE;
  wire clk_1;
  wire clk_cpu_1;
  wire [15:0]peak_amp_1;
  wire peak_amp_rdy_1;
  wire segment_1;

  assign Conn1_ARADDR = S_AXI_araddr[12:0];
  assign Conn1_ARBURST = S_AXI_arburst[1:0];
  assign Conn1_ARCACHE = S_AXI_arcache[3:0];
  assign Conn1_ARLEN = S_AXI_arlen[7:0];
  assign Conn1_ARLOCK = S_AXI_arlock;
  assign Conn1_ARPROT = S_AXI_arprot[2:0];
  assign Conn1_ARSIZE = S_AXI_arsize[2:0];
  assign Conn1_ARVALID = S_AXI_arvalid;
  assign Conn1_AWADDR = S_AXI_awaddr[12:0];
  assign Conn1_AWBURST = S_AXI_awburst[1:0];
  assign Conn1_AWCACHE = S_AXI_awcache[3:0];
  assign Conn1_AWLEN = S_AXI_awlen[7:0];
  assign Conn1_AWLOCK = S_AXI_awlock;
  assign Conn1_AWPROT = S_AXI_awprot[2:0];
  assign Conn1_AWSIZE = S_AXI_awsize[2:0];
  assign Conn1_AWVALID = S_AXI_awvalid;
  assign Conn1_BREADY = S_AXI_bready;
  assign Conn1_RREADY = S_AXI_rready;
  assign Conn1_WDATA = S_AXI_wdata[31:0];
  assign Conn1_WLAST = S_AXI_wlast;
  assign Conn1_WSTRB = S_AXI_wstrb[3:0];
  assign Conn1_WVALID = S_AXI_wvalid;
  assign S00_ARESETN_1 = S00_ARESETN;
  assign S_AXI_arready = Conn1_ARREADY;
  assign S_AXI_awready = Conn1_AWREADY;
  assign S_AXI_bresp[1:0] = Conn1_BRESP;
  assign S_AXI_bvalid = Conn1_BVALID;
  assign S_AXI_rdata[31:0] = Conn1_RDATA;
  assign S_AXI_rlast = Conn1_RLAST;
  assign S_AXI_rresp[1:0] = Conn1_RRESP;
  assign S_AXI_rvalid = Conn1_RVALID;
  assign S_AXI_wready = Conn1_WREADY;
  assign clk_1 = clk;
  assign clk_cpu_1 = clk_cpu;
  assign peak_amp_1 = peak_amp[15:0];
  assign peak_amp_rdy_1 = peak_amp_rdy;
  assign segment_1 = segment;
  (* BMM_INFO_ADDRESS_SPACE = "byte  0xC0000000 32 > design_1 dpp_0/pha/mca/blk_mem_gen_0" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  design_1_axi_bram_ctrl_0_0 axi_bram_ctrl_0
       (.bram_addr_a(axi_bram_ctrl_0_BRAM_PORTA_ADDR),
        .bram_clk_a(axi_bram_ctrl_0_BRAM_PORTA_CLK),
        .bram_en_a(axi_bram_ctrl_0_BRAM_PORTA_EN),
        .bram_rddata_a(axi_bram_ctrl_0_BRAM_PORTA_DOUT),
        .bram_rst_a(axi_bram_ctrl_0_BRAM_PORTA_RST),
        .bram_we_a(axi_bram_ctrl_0_BRAM_PORTA_WE),
        .bram_wrdata_a(axi_bram_ctrl_0_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_cpu_1),
        .s_axi_araddr(Conn1_ARADDR),
        .s_axi_arburst(Conn1_ARBURST),
        .s_axi_arcache(Conn1_ARCACHE),
        .s_axi_aresetn(S00_ARESETN_1),
        .s_axi_arlen(Conn1_ARLEN),
        .s_axi_arlock(Conn1_ARLOCK),
        .s_axi_arprot(Conn1_ARPROT),
        .s_axi_arready(Conn1_ARREADY),
        .s_axi_arsize(Conn1_ARSIZE),
        .s_axi_arvalid(Conn1_ARVALID),
        .s_axi_awaddr(Conn1_AWADDR),
        .s_axi_awburst(Conn1_AWBURST),
        .s_axi_awcache(Conn1_AWCACHE),
        .s_axi_awlen(Conn1_AWLEN),
        .s_axi_awlock(Conn1_AWLOCK),
        .s_axi_awprot(Conn1_AWPROT),
        .s_axi_awready(Conn1_AWREADY),
        .s_axi_awsize(Conn1_AWSIZE),
        .s_axi_awvalid(Conn1_AWVALID),
        .s_axi_bready(Conn1_BREADY),
        .s_axi_bresp(Conn1_BRESP),
        .s_axi_bvalid(Conn1_BVALID),
        .s_axi_rdata(Conn1_RDATA),
        .s_axi_rlast(Conn1_RLAST),
        .s_axi_rready(Conn1_RREADY),
        .s_axi_rresp(Conn1_RRESP),
        .s_axi_rvalid(Conn1_RVALID),
        .s_axi_wdata(Conn1_WDATA),
        .s_axi_wlast(Conn1_WLAST),
        .s_axi_wready(Conn1_WREADY),
        .s_axi_wstrb(Conn1_WSTRB),
        .s_axi_wvalid(Conn1_WVALID));
  design_1_blk_mem_gen_0_0 blk_mem_gen_0
       (.addra({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,axi_bram_ctrl_0_BRAM_PORTA_ADDR}),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,bram_incr_0_BRAM_PORTB_ADDR}),
        .clka(axi_bram_ctrl_0_BRAM_PORTA_CLK),
        .clkb(bram_incr_0_BRAM_PORTB_CLK),
        .dina(axi_bram_ctrl_0_BRAM_PORTA_DIN),
        .dinb(bram_incr_0_BRAM_PORTB_DIN),
        .douta(axi_bram_ctrl_0_BRAM_PORTA_DOUT),
        .doutb(bram_incr_0_BRAM_PORTB_DOUT),
        .ena(axi_bram_ctrl_0_BRAM_PORTA_EN),
        .enb(bram_incr_0_BRAM_PORTB_EN),
        .rsta(axi_bram_ctrl_0_BRAM_PORTA_RST),
        .rstb(bram_incr_0_BRAM_PORTB_RST),
        .wea(axi_bram_ctrl_0_BRAM_PORTA_WE),
        .web({bram_incr_0_BRAM_PORTB_WE,bram_incr_0_BRAM_PORTB_WE,bram_incr_0_BRAM_PORTB_WE,bram_incr_0_BRAM_PORTB_WE}));
  design_1_bram_incr_0_0 bram_incr_0
       (.bram_addr(bram_incr_0_BRAM_PORTB_ADDR),
        .bram_clk(bram_incr_0_BRAM_PORTB_CLK),
        .bram_din(bram_incr_0_BRAM_PORTB_DIN),
        .bram_dout(bram_incr_0_BRAM_PORTB_DOUT),
        .bram_en(bram_incr_0_BRAM_PORTB_EN),
        .bram_rst(bram_incr_0_BRAM_PORTB_RST),
        .bram_we(bram_incr_0_BRAM_PORTB_WE),
        .clk(clk_1),
        .peak_amp(peak_amp_1),
        .peak_amp_rdy(peak_amp_rdy_1),
        .rstn(S00_ARESETN_1),
        .segment(segment_1));
endmodule

module microblaze_0_local_memory_imp_1788IOU
   (DLMB_abus,
    DLMB_addrstrobe,
    DLMB_be,
    DLMB_ce,
    DLMB_readdbus,
    DLMB_readstrobe,
    DLMB_ready,
    DLMB_ue,
    DLMB_wait,
    DLMB_writedbus,
    DLMB_writestrobe,
    ILMB_abus,
    ILMB_addrstrobe,
    ILMB_ce,
    ILMB_readdbus,
    ILMB_readstrobe,
    ILMB_ready,
    ILMB_ue,
    ILMB_wait,
    LMB_Clk,
    SYS_Rst);
  input [0:31]DLMB_abus;
  input DLMB_addrstrobe;
  input [0:3]DLMB_be;
  output DLMB_ce;
  output [0:31]DLMB_readdbus;
  input DLMB_readstrobe;
  output DLMB_ready;
  output DLMB_ue;
  output DLMB_wait;
  input [0:31]DLMB_writedbus;
  input DLMB_writestrobe;
  input [0:31]ILMB_abus;
  input ILMB_addrstrobe;
  output ILMB_ce;
  output [0:31]ILMB_readdbus;
  input ILMB_readstrobe;
  output ILMB_ready;
  output ILMB_ue;
  output ILMB_wait;
  input LMB_Clk;
  input SYS_Rst;

  wire SYS_Rst_1;
  wire microblaze_0_Clk;
  wire [0:31]microblaze_0_dlmb_ABUS;
  wire microblaze_0_dlmb_ADDRSTROBE;
  wire [0:3]microblaze_0_dlmb_BE;
  wire microblaze_0_dlmb_CE;
  wire [0:31]microblaze_0_dlmb_READDBUS;
  wire microblaze_0_dlmb_READSTROBE;
  wire microblaze_0_dlmb_READY;
  wire microblaze_0_dlmb_UE;
  wire microblaze_0_dlmb_WAIT;
  wire [0:31]microblaze_0_dlmb_WRITEDBUS;
  wire microblaze_0_dlmb_WRITESTROBE;
  wire [0:31]microblaze_0_dlmb_bus_ABUS;
  wire microblaze_0_dlmb_bus_ADDRSTROBE;
  wire [0:3]microblaze_0_dlmb_bus_BE;
  wire microblaze_0_dlmb_bus_CE;
  wire [0:31]microblaze_0_dlmb_bus_READDBUS;
  wire microblaze_0_dlmb_bus_READSTROBE;
  wire microblaze_0_dlmb_bus_READY;
  wire microblaze_0_dlmb_bus_UE;
  wire microblaze_0_dlmb_bus_WAIT;
  wire [0:31]microblaze_0_dlmb_bus_WRITEDBUS;
  wire microblaze_0_dlmb_bus_WRITESTROBE;
  wire [0:31]microblaze_0_dlmb_cntlr_ADDR;
  wire microblaze_0_dlmb_cntlr_CLK;
  wire [0:31]microblaze_0_dlmb_cntlr_DIN;
  wire [31:0]microblaze_0_dlmb_cntlr_DOUT;
  wire microblaze_0_dlmb_cntlr_EN;
  wire microblaze_0_dlmb_cntlr_RST;
  wire [0:3]microblaze_0_dlmb_cntlr_WE;
  wire [0:31]microblaze_0_ilmb_ABUS;
  wire microblaze_0_ilmb_ADDRSTROBE;
  wire microblaze_0_ilmb_CE;
  wire [0:31]microblaze_0_ilmb_READDBUS;
  wire microblaze_0_ilmb_READSTROBE;
  wire microblaze_0_ilmb_READY;
  wire microblaze_0_ilmb_UE;
  wire microblaze_0_ilmb_WAIT;
  wire [0:31]microblaze_0_ilmb_bus_ABUS;
  wire microblaze_0_ilmb_bus_ADDRSTROBE;
  wire [0:3]microblaze_0_ilmb_bus_BE;
  wire microblaze_0_ilmb_bus_CE;
  wire [0:31]microblaze_0_ilmb_bus_READDBUS;
  wire microblaze_0_ilmb_bus_READSTROBE;
  wire microblaze_0_ilmb_bus_READY;
  wire microblaze_0_ilmb_bus_UE;
  wire microblaze_0_ilmb_bus_WAIT;
  wire [0:31]microblaze_0_ilmb_bus_WRITEDBUS;
  wire microblaze_0_ilmb_bus_WRITESTROBE;
  wire [0:31]microblaze_0_ilmb_cntlr_ADDR;
  wire microblaze_0_ilmb_cntlr_CLK;
  wire [0:31]microblaze_0_ilmb_cntlr_DIN;
  wire [31:0]microblaze_0_ilmb_cntlr_DOUT;
  wire microblaze_0_ilmb_cntlr_EN;
  wire microblaze_0_ilmb_cntlr_RST;
  wire [0:3]microblaze_0_ilmb_cntlr_WE;

  assign DLMB_ce = microblaze_0_dlmb_CE;
  assign DLMB_readdbus[0:31] = microblaze_0_dlmb_READDBUS;
  assign DLMB_ready = microblaze_0_dlmb_READY;
  assign DLMB_ue = microblaze_0_dlmb_UE;
  assign DLMB_wait = microblaze_0_dlmb_WAIT;
  assign ILMB_ce = microblaze_0_ilmb_CE;
  assign ILMB_readdbus[0:31] = microblaze_0_ilmb_READDBUS;
  assign ILMB_ready = microblaze_0_ilmb_READY;
  assign ILMB_ue = microblaze_0_ilmb_UE;
  assign ILMB_wait = microblaze_0_ilmb_WAIT;
  assign SYS_Rst_1 = SYS_Rst;
  assign microblaze_0_Clk = LMB_Clk;
  assign microblaze_0_dlmb_ABUS = DLMB_abus[0:31];
  assign microblaze_0_dlmb_ADDRSTROBE = DLMB_addrstrobe;
  assign microblaze_0_dlmb_BE = DLMB_be[0:3];
  assign microblaze_0_dlmb_READSTROBE = DLMB_readstrobe;
  assign microblaze_0_dlmb_WRITEDBUS = DLMB_writedbus[0:31];
  assign microblaze_0_dlmb_WRITESTROBE = DLMB_writestrobe;
  assign microblaze_0_ilmb_ABUS = ILMB_abus[0:31];
  assign microblaze_0_ilmb_ADDRSTROBE = ILMB_addrstrobe;
  assign microblaze_0_ilmb_READSTROBE = ILMB_readstrobe;
  (* BMM_INFO_ADDRESS_SPACE = "byte  0x00000000 32 > design_1 ps_mb_0/microblaze_0_local_memory/lmb_bram" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  design_1_dlmb_bram_if_cntlr_0 dlmb_bram_if_cntlr
       (.BRAM_Addr_A(microblaze_0_dlmb_cntlr_ADDR),
        .BRAM_Clk_A(microblaze_0_dlmb_cntlr_CLK),
        .BRAM_Din_A({microblaze_0_dlmb_cntlr_DOUT[31],microblaze_0_dlmb_cntlr_DOUT[30],microblaze_0_dlmb_cntlr_DOUT[29],microblaze_0_dlmb_cntlr_DOUT[28],microblaze_0_dlmb_cntlr_DOUT[27],microblaze_0_dlmb_cntlr_DOUT[26],microblaze_0_dlmb_cntlr_DOUT[25],microblaze_0_dlmb_cntlr_DOUT[24],microblaze_0_dlmb_cntlr_DOUT[23],microblaze_0_dlmb_cntlr_DOUT[22],microblaze_0_dlmb_cntlr_DOUT[21],microblaze_0_dlmb_cntlr_DOUT[20],microblaze_0_dlmb_cntlr_DOUT[19],microblaze_0_dlmb_cntlr_DOUT[18],microblaze_0_dlmb_cntlr_DOUT[17],microblaze_0_dlmb_cntlr_DOUT[16],microblaze_0_dlmb_cntlr_DOUT[15],microblaze_0_dlmb_cntlr_DOUT[14],microblaze_0_dlmb_cntlr_DOUT[13],microblaze_0_dlmb_cntlr_DOUT[12],microblaze_0_dlmb_cntlr_DOUT[11],microblaze_0_dlmb_cntlr_DOUT[10],microblaze_0_dlmb_cntlr_DOUT[9],microblaze_0_dlmb_cntlr_DOUT[8],microblaze_0_dlmb_cntlr_DOUT[7],microblaze_0_dlmb_cntlr_DOUT[6],microblaze_0_dlmb_cntlr_DOUT[5],microblaze_0_dlmb_cntlr_DOUT[4],microblaze_0_dlmb_cntlr_DOUT[3],microblaze_0_dlmb_cntlr_DOUT[2],microblaze_0_dlmb_cntlr_DOUT[1],microblaze_0_dlmb_cntlr_DOUT[0]}),
        .BRAM_Dout_A(microblaze_0_dlmb_cntlr_DIN),
        .BRAM_EN_A(microblaze_0_dlmb_cntlr_EN),
        .BRAM_Rst_A(microblaze_0_dlmb_cntlr_RST),
        .BRAM_WEN_A(microblaze_0_dlmb_cntlr_WE),
        .LMB_ABus(microblaze_0_dlmb_bus_ABUS),
        .LMB_AddrStrobe(microblaze_0_dlmb_bus_ADDRSTROBE),
        .LMB_BE(microblaze_0_dlmb_bus_BE),
        .LMB_Clk(microblaze_0_Clk),
        .LMB_ReadStrobe(microblaze_0_dlmb_bus_READSTROBE),
        .LMB_Rst(SYS_Rst_1),
        .LMB_WriteDBus(microblaze_0_dlmb_bus_WRITEDBUS),
        .LMB_WriteStrobe(microblaze_0_dlmb_bus_WRITESTROBE),
        .Sl_CE(microblaze_0_dlmb_bus_CE),
        .Sl_DBus(microblaze_0_dlmb_bus_READDBUS),
        .Sl_Ready(microblaze_0_dlmb_bus_READY),
        .Sl_UE(microblaze_0_dlmb_bus_UE),
        .Sl_Wait(microblaze_0_dlmb_bus_WAIT));
  design_1_dlmb_v10_0 dlmb_v10
       (.LMB_ABus(microblaze_0_dlmb_bus_ABUS),
        .LMB_AddrStrobe(microblaze_0_dlmb_bus_ADDRSTROBE),
        .LMB_BE(microblaze_0_dlmb_bus_BE),
        .LMB_CE(microblaze_0_dlmb_CE),
        .LMB_Clk(microblaze_0_Clk),
        .LMB_ReadDBus(microblaze_0_dlmb_READDBUS),
        .LMB_ReadStrobe(microblaze_0_dlmb_bus_READSTROBE),
        .LMB_Ready(microblaze_0_dlmb_READY),
        .LMB_UE(microblaze_0_dlmb_UE),
        .LMB_Wait(microblaze_0_dlmb_WAIT),
        .LMB_WriteDBus(microblaze_0_dlmb_bus_WRITEDBUS),
        .LMB_WriteStrobe(microblaze_0_dlmb_bus_WRITESTROBE),
        .M_ABus(microblaze_0_dlmb_ABUS),
        .M_AddrStrobe(microblaze_0_dlmb_ADDRSTROBE),
        .M_BE(microblaze_0_dlmb_BE),
        .M_DBus(microblaze_0_dlmb_WRITEDBUS),
        .M_ReadStrobe(microblaze_0_dlmb_READSTROBE),
        .M_WriteStrobe(microblaze_0_dlmb_WRITESTROBE),
        .SYS_Rst(SYS_Rst_1),
        .Sl_CE(microblaze_0_dlmb_bus_CE),
        .Sl_DBus(microblaze_0_dlmb_bus_READDBUS),
        .Sl_Ready(microblaze_0_dlmb_bus_READY),
        .Sl_UE(microblaze_0_dlmb_bus_UE),
        .Sl_Wait(microblaze_0_dlmb_bus_WAIT));
  design_1_ilmb_bram_if_cntlr_0 ilmb_bram_if_cntlr
       (.BRAM_Addr_A(microblaze_0_ilmb_cntlr_ADDR),
        .BRAM_Clk_A(microblaze_0_ilmb_cntlr_CLK),
        .BRAM_Din_A({microblaze_0_ilmb_cntlr_DOUT[31],microblaze_0_ilmb_cntlr_DOUT[30],microblaze_0_ilmb_cntlr_DOUT[29],microblaze_0_ilmb_cntlr_DOUT[28],microblaze_0_ilmb_cntlr_DOUT[27],microblaze_0_ilmb_cntlr_DOUT[26],microblaze_0_ilmb_cntlr_DOUT[25],microblaze_0_ilmb_cntlr_DOUT[24],microblaze_0_ilmb_cntlr_DOUT[23],microblaze_0_ilmb_cntlr_DOUT[22],microblaze_0_ilmb_cntlr_DOUT[21],microblaze_0_ilmb_cntlr_DOUT[20],microblaze_0_ilmb_cntlr_DOUT[19],microblaze_0_ilmb_cntlr_DOUT[18],microblaze_0_ilmb_cntlr_DOUT[17],microblaze_0_ilmb_cntlr_DOUT[16],microblaze_0_ilmb_cntlr_DOUT[15],microblaze_0_ilmb_cntlr_DOUT[14],microblaze_0_ilmb_cntlr_DOUT[13],microblaze_0_ilmb_cntlr_DOUT[12],microblaze_0_ilmb_cntlr_DOUT[11],microblaze_0_ilmb_cntlr_DOUT[10],microblaze_0_ilmb_cntlr_DOUT[9],microblaze_0_ilmb_cntlr_DOUT[8],microblaze_0_ilmb_cntlr_DOUT[7],microblaze_0_ilmb_cntlr_DOUT[6],microblaze_0_ilmb_cntlr_DOUT[5],microblaze_0_ilmb_cntlr_DOUT[4],microblaze_0_ilmb_cntlr_DOUT[3],microblaze_0_ilmb_cntlr_DOUT[2],microblaze_0_ilmb_cntlr_DOUT[1],microblaze_0_ilmb_cntlr_DOUT[0]}),
        .BRAM_Dout_A(microblaze_0_ilmb_cntlr_DIN),
        .BRAM_EN_A(microblaze_0_ilmb_cntlr_EN),
        .BRAM_Rst_A(microblaze_0_ilmb_cntlr_RST),
        .BRAM_WEN_A(microblaze_0_ilmb_cntlr_WE),
        .LMB_ABus(microblaze_0_ilmb_bus_ABUS),
        .LMB_AddrStrobe(microblaze_0_ilmb_bus_ADDRSTROBE),
        .LMB_BE(microblaze_0_ilmb_bus_BE),
        .LMB_Clk(microblaze_0_Clk),
        .LMB_ReadStrobe(microblaze_0_ilmb_bus_READSTROBE),
        .LMB_Rst(SYS_Rst_1),
        .LMB_WriteDBus(microblaze_0_ilmb_bus_WRITEDBUS),
        .LMB_WriteStrobe(microblaze_0_ilmb_bus_WRITESTROBE),
        .Sl_CE(microblaze_0_ilmb_bus_CE),
        .Sl_DBus(microblaze_0_ilmb_bus_READDBUS),
        .Sl_Ready(microblaze_0_ilmb_bus_READY),
        .Sl_UE(microblaze_0_ilmb_bus_UE),
        .Sl_Wait(microblaze_0_ilmb_bus_WAIT));
  design_1_ilmb_v10_0 ilmb_v10
       (.LMB_ABus(microblaze_0_ilmb_bus_ABUS),
        .LMB_AddrStrobe(microblaze_0_ilmb_bus_ADDRSTROBE),
        .LMB_BE(microblaze_0_ilmb_bus_BE),
        .LMB_CE(microblaze_0_ilmb_CE),
        .LMB_Clk(microblaze_0_Clk),
        .LMB_ReadDBus(microblaze_0_ilmb_READDBUS),
        .LMB_ReadStrobe(microblaze_0_ilmb_bus_READSTROBE),
        .LMB_Ready(microblaze_0_ilmb_READY),
        .LMB_UE(microblaze_0_ilmb_UE),
        .LMB_Wait(microblaze_0_ilmb_WAIT),
        .LMB_WriteDBus(microblaze_0_ilmb_bus_WRITEDBUS),
        .LMB_WriteStrobe(microblaze_0_ilmb_bus_WRITESTROBE),
        .M_ABus(microblaze_0_ilmb_ABUS),
        .M_AddrStrobe(microblaze_0_ilmb_ADDRSTROBE),
        .M_BE({1'b0,1'b0,1'b0,1'b0}),
        .M_DBus({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .M_ReadStrobe(microblaze_0_ilmb_READSTROBE),
        .M_WriteStrobe(1'b0),
        .SYS_Rst(SYS_Rst_1),
        .Sl_CE(microblaze_0_ilmb_bus_CE),
        .Sl_DBus(microblaze_0_ilmb_bus_READDBUS),
        .Sl_Ready(microblaze_0_ilmb_bus_READY),
        .Sl_UE(microblaze_0_ilmb_bus_UE),
        .Sl_Wait(microblaze_0_ilmb_bus_WAIT));
  design_1_lmb_bram_0 lmb_bram
       (.addra({microblaze_0_dlmb_cntlr_ADDR[0],microblaze_0_dlmb_cntlr_ADDR[1],microblaze_0_dlmb_cntlr_ADDR[2],microblaze_0_dlmb_cntlr_ADDR[3],microblaze_0_dlmb_cntlr_ADDR[4],microblaze_0_dlmb_cntlr_ADDR[5],microblaze_0_dlmb_cntlr_ADDR[6],microblaze_0_dlmb_cntlr_ADDR[7],microblaze_0_dlmb_cntlr_ADDR[8],microblaze_0_dlmb_cntlr_ADDR[9],microblaze_0_dlmb_cntlr_ADDR[10],microblaze_0_dlmb_cntlr_ADDR[11],microblaze_0_dlmb_cntlr_ADDR[12],microblaze_0_dlmb_cntlr_ADDR[13],microblaze_0_dlmb_cntlr_ADDR[14],microblaze_0_dlmb_cntlr_ADDR[15],microblaze_0_dlmb_cntlr_ADDR[16],microblaze_0_dlmb_cntlr_ADDR[17],microblaze_0_dlmb_cntlr_ADDR[18],microblaze_0_dlmb_cntlr_ADDR[19],microblaze_0_dlmb_cntlr_ADDR[20],microblaze_0_dlmb_cntlr_ADDR[21],microblaze_0_dlmb_cntlr_ADDR[22],microblaze_0_dlmb_cntlr_ADDR[23],microblaze_0_dlmb_cntlr_ADDR[24],microblaze_0_dlmb_cntlr_ADDR[25],microblaze_0_dlmb_cntlr_ADDR[26],microblaze_0_dlmb_cntlr_ADDR[27],microblaze_0_dlmb_cntlr_ADDR[28],microblaze_0_dlmb_cntlr_ADDR[29],microblaze_0_dlmb_cntlr_ADDR[30],microblaze_0_dlmb_cntlr_ADDR[31]}),
        .addrb({microblaze_0_ilmb_cntlr_ADDR[0],microblaze_0_ilmb_cntlr_ADDR[1],microblaze_0_ilmb_cntlr_ADDR[2],microblaze_0_ilmb_cntlr_ADDR[3],microblaze_0_ilmb_cntlr_ADDR[4],microblaze_0_ilmb_cntlr_ADDR[5],microblaze_0_ilmb_cntlr_ADDR[6],microblaze_0_ilmb_cntlr_ADDR[7],microblaze_0_ilmb_cntlr_ADDR[8],microblaze_0_ilmb_cntlr_ADDR[9],microblaze_0_ilmb_cntlr_ADDR[10],microblaze_0_ilmb_cntlr_ADDR[11],microblaze_0_ilmb_cntlr_ADDR[12],microblaze_0_ilmb_cntlr_ADDR[13],microblaze_0_ilmb_cntlr_ADDR[14],microblaze_0_ilmb_cntlr_ADDR[15],microblaze_0_ilmb_cntlr_ADDR[16],microblaze_0_ilmb_cntlr_ADDR[17],microblaze_0_ilmb_cntlr_ADDR[18],microblaze_0_ilmb_cntlr_ADDR[19],microblaze_0_ilmb_cntlr_ADDR[20],microblaze_0_ilmb_cntlr_ADDR[21],microblaze_0_ilmb_cntlr_ADDR[22],microblaze_0_ilmb_cntlr_ADDR[23],microblaze_0_ilmb_cntlr_ADDR[24],microblaze_0_ilmb_cntlr_ADDR[25],microblaze_0_ilmb_cntlr_ADDR[26],microblaze_0_ilmb_cntlr_ADDR[27],microblaze_0_ilmb_cntlr_ADDR[28],microblaze_0_ilmb_cntlr_ADDR[29],microblaze_0_ilmb_cntlr_ADDR[30],microblaze_0_ilmb_cntlr_ADDR[31]}),
        .clka(microblaze_0_dlmb_cntlr_CLK),
        .clkb(microblaze_0_ilmb_cntlr_CLK),
        .dina({microblaze_0_dlmb_cntlr_DIN[0],microblaze_0_dlmb_cntlr_DIN[1],microblaze_0_dlmb_cntlr_DIN[2],microblaze_0_dlmb_cntlr_DIN[3],microblaze_0_dlmb_cntlr_DIN[4],microblaze_0_dlmb_cntlr_DIN[5],microblaze_0_dlmb_cntlr_DIN[6],microblaze_0_dlmb_cntlr_DIN[7],microblaze_0_dlmb_cntlr_DIN[8],microblaze_0_dlmb_cntlr_DIN[9],microblaze_0_dlmb_cntlr_DIN[10],microblaze_0_dlmb_cntlr_DIN[11],microblaze_0_dlmb_cntlr_DIN[12],microblaze_0_dlmb_cntlr_DIN[13],microblaze_0_dlmb_cntlr_DIN[14],microblaze_0_dlmb_cntlr_DIN[15],microblaze_0_dlmb_cntlr_DIN[16],microblaze_0_dlmb_cntlr_DIN[17],microblaze_0_dlmb_cntlr_DIN[18],microblaze_0_dlmb_cntlr_DIN[19],microblaze_0_dlmb_cntlr_DIN[20],microblaze_0_dlmb_cntlr_DIN[21],microblaze_0_dlmb_cntlr_DIN[22],microblaze_0_dlmb_cntlr_DIN[23],microblaze_0_dlmb_cntlr_DIN[24],microblaze_0_dlmb_cntlr_DIN[25],microblaze_0_dlmb_cntlr_DIN[26],microblaze_0_dlmb_cntlr_DIN[27],microblaze_0_dlmb_cntlr_DIN[28],microblaze_0_dlmb_cntlr_DIN[29],microblaze_0_dlmb_cntlr_DIN[30],microblaze_0_dlmb_cntlr_DIN[31]}),
        .dinb({microblaze_0_ilmb_cntlr_DIN[0],microblaze_0_ilmb_cntlr_DIN[1],microblaze_0_ilmb_cntlr_DIN[2],microblaze_0_ilmb_cntlr_DIN[3],microblaze_0_ilmb_cntlr_DIN[4],microblaze_0_ilmb_cntlr_DIN[5],microblaze_0_ilmb_cntlr_DIN[6],microblaze_0_ilmb_cntlr_DIN[7],microblaze_0_ilmb_cntlr_DIN[8],microblaze_0_ilmb_cntlr_DIN[9],microblaze_0_ilmb_cntlr_DIN[10],microblaze_0_ilmb_cntlr_DIN[11],microblaze_0_ilmb_cntlr_DIN[12],microblaze_0_ilmb_cntlr_DIN[13],microblaze_0_ilmb_cntlr_DIN[14],microblaze_0_ilmb_cntlr_DIN[15],microblaze_0_ilmb_cntlr_DIN[16],microblaze_0_ilmb_cntlr_DIN[17],microblaze_0_ilmb_cntlr_DIN[18],microblaze_0_ilmb_cntlr_DIN[19],microblaze_0_ilmb_cntlr_DIN[20],microblaze_0_ilmb_cntlr_DIN[21],microblaze_0_ilmb_cntlr_DIN[22],microblaze_0_ilmb_cntlr_DIN[23],microblaze_0_ilmb_cntlr_DIN[24],microblaze_0_ilmb_cntlr_DIN[25],microblaze_0_ilmb_cntlr_DIN[26],microblaze_0_ilmb_cntlr_DIN[27],microblaze_0_ilmb_cntlr_DIN[28],microblaze_0_ilmb_cntlr_DIN[29],microblaze_0_ilmb_cntlr_DIN[30],microblaze_0_ilmb_cntlr_DIN[31]}),
        .douta(microblaze_0_dlmb_cntlr_DOUT),
        .doutb(microblaze_0_ilmb_cntlr_DOUT),
        .ena(microblaze_0_dlmb_cntlr_EN),
        .enb(microblaze_0_ilmb_cntlr_EN),
        .rsta(microblaze_0_dlmb_cntlr_RST),
        .rstb(microblaze_0_ilmb_cntlr_RST),
        .wea({microblaze_0_dlmb_cntlr_WE[0],microblaze_0_dlmb_cntlr_WE[1],microblaze_0_dlmb_cntlr_WE[2],microblaze_0_dlmb_cntlr_WE[3]}),
        .web({microblaze_0_ilmb_cntlr_WE[0],microblaze_0_ilmb_cntlr_WE[1],microblaze_0_ilmb_cntlr_WE[2],microblaze_0_ilmb_cntlr_WE[3]}));
endmodule

module pha_imp_R9HCV1
   (D,
    S00_ARESETN,
    S00_AXI3_araddr,
    S00_AXI3_arprot,
    S00_AXI3_arready,
    S00_AXI3_arvalid,
    S00_AXI3_awaddr,
    S00_AXI3_awprot,
    S00_AXI3_awready,
    S00_AXI3_awvalid,
    S00_AXI3_bready,
    S00_AXI3_bresp,
    S00_AXI3_bvalid,
    S00_AXI3_rdata,
    S00_AXI3_rready,
    S00_AXI3_rresp,
    S00_AXI3_rvalid,
    S00_AXI3_wdata,
    S00_AXI3_wready,
    S00_AXI3_wstrb,
    S00_AXI3_wvalid,
    S00_AXI4_araddr,
    S00_AXI4_arprot,
    S00_AXI4_arready,
    S00_AXI4_arvalid,
    S00_AXI4_awaddr,
    S00_AXI4_awprot,
    S00_AXI4_awready,
    S00_AXI4_awvalid,
    S00_AXI4_bready,
    S00_AXI4_bresp,
    S00_AXI4_bvalid,
    S00_AXI4_rdata,
    S00_AXI4_rready,
    S00_AXI4_rresp,
    S00_AXI4_rvalid,
    S00_AXI4_wdata,
    S00_AXI4_wready,
    S00_AXI4_wstrb,
    S00_AXI4_wvalid,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid,
    axibusdomain_s_axi1_araddr,
    axibusdomain_s_axi1_arready,
    axibusdomain_s_axi1_arvalid,
    axibusdomain_s_axi1_awaddr,
    axibusdomain_s_axi1_awready,
    axibusdomain_s_axi1_awvalid,
    axibusdomain_s_axi1_bready,
    axibusdomain_s_axi1_bresp,
    axibusdomain_s_axi1_bvalid,
    axibusdomain_s_axi1_rdata,
    axibusdomain_s_axi1_rready,
    axibusdomain_s_axi1_rresp,
    axibusdomain_s_axi1_rvalid,
    axibusdomain_s_axi1_wdata,
    axibusdomain_s_axi1_wready,
    axibusdomain_s_axi1_wstrb,
    axibusdomain_s_axi1_wvalid,
    clk,
    clk_cpu,
    dbg_pha_m0_peak_amp_rdy_fast_out,
    dbg_pha_m0_peak_amp_rdy_slow_out,
    dbg_pha_m0_peak_det_signal_out,
    dbg_pha_m0_rejectn_out);
  input [15:0]D;
  input S00_ARESETN;
  input [31:0]S00_AXI3_araddr;
  input [2:0]S00_AXI3_arprot;
  output S00_AXI3_arready;
  input S00_AXI3_arvalid;
  input [31:0]S00_AXI3_awaddr;
  input [2:0]S00_AXI3_awprot;
  output S00_AXI3_awready;
  input S00_AXI3_awvalid;
  input S00_AXI3_bready;
  output [1:0]S00_AXI3_bresp;
  output S00_AXI3_bvalid;
  output [31:0]S00_AXI3_rdata;
  input S00_AXI3_rready;
  output [1:0]S00_AXI3_rresp;
  output S00_AXI3_rvalid;
  input [31:0]S00_AXI3_wdata;
  output S00_AXI3_wready;
  input [3:0]S00_AXI3_wstrb;
  input S00_AXI3_wvalid;
  input [31:0]S00_AXI4_araddr;
  input [2:0]S00_AXI4_arprot;
  output S00_AXI4_arready;
  input S00_AXI4_arvalid;
  input [31:0]S00_AXI4_awaddr;
  input [2:0]S00_AXI4_awprot;
  output S00_AXI4_awready;
  input S00_AXI4_awvalid;
  input S00_AXI4_bready;
  output [1:0]S00_AXI4_bresp;
  output S00_AXI4_bvalid;
  output [31:0]S00_AXI4_rdata;
  input S00_AXI4_rready;
  output [1:0]S00_AXI4_rresp;
  output S00_AXI4_rvalid;
  input [31:0]S00_AXI4_wdata;
  output S00_AXI4_wready;
  input [3:0]S00_AXI4_wstrb;
  input S00_AXI4_wvalid;
  input [12:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [12:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;
  input [31:0]axibusdomain_s_axi1_araddr;
  output axibusdomain_s_axi1_arready;
  input axibusdomain_s_axi1_arvalid;
  input [31:0]axibusdomain_s_axi1_awaddr;
  output axibusdomain_s_axi1_awready;
  input axibusdomain_s_axi1_awvalid;
  input axibusdomain_s_axi1_bready;
  output [1:0]axibusdomain_s_axi1_bresp;
  output axibusdomain_s_axi1_bvalid;
  output [31:0]axibusdomain_s_axi1_rdata;
  input axibusdomain_s_axi1_rready;
  output [1:0]axibusdomain_s_axi1_rresp;
  output axibusdomain_s_axi1_rvalid;
  input [31:0]axibusdomain_s_axi1_wdata;
  output axibusdomain_s_axi1_wready;
  input [3:0]axibusdomain_s_axi1_wstrb;
  input axibusdomain_s_axi1_wvalid;
  input clk;
  input clk_cpu;
  output [15:0]dbg_pha_m0_peak_amp_rdy_fast_out;
  output [15:0]dbg_pha_m0_peak_amp_rdy_slow_out;
  output [15:0]dbg_pha_m0_peak_det_signal_out;
  output [15:0]dbg_pha_m0_rejectn_out;

  wire [31:0]Conn1_ARADDR;
  wire [2:0]Conn1_ARPROT;
  wire Conn1_ARREADY;
  wire Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire [2:0]Conn1_AWPROT;
  wire Conn1_AWREADY;
  wire Conn1_AWVALID;
  wire Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire Conn1_BVALID;
  wire [31:0]Conn1_RDATA;
  wire Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire Conn1_RVALID;
  wire [31:0]Conn1_WDATA;
  wire Conn1_WREADY;
  wire [3:0]Conn1_WSTRB;
  wire Conn1_WVALID;
  wire [12:0]Conn2_ARADDR;
  wire [1:0]Conn2_ARBURST;
  wire [3:0]Conn2_ARCACHE;
  wire [7:0]Conn2_ARLEN;
  wire Conn2_ARLOCK;
  wire [2:0]Conn2_ARPROT;
  wire Conn2_ARREADY;
  wire [2:0]Conn2_ARSIZE;
  wire Conn2_ARVALID;
  wire [12:0]Conn2_AWADDR;
  wire [1:0]Conn2_AWBURST;
  wire [3:0]Conn2_AWCACHE;
  wire [7:0]Conn2_AWLEN;
  wire Conn2_AWLOCK;
  wire [2:0]Conn2_AWPROT;
  wire Conn2_AWREADY;
  wire [2:0]Conn2_AWSIZE;
  wire Conn2_AWVALID;
  wire Conn2_BREADY;
  wire [1:0]Conn2_BRESP;
  wire Conn2_BVALID;
  wire [31:0]Conn2_RDATA;
  wire Conn2_RLAST;
  wire Conn2_RREADY;
  wire [1:0]Conn2_RRESP;
  wire Conn2_RVALID;
  wire [31:0]Conn2_WDATA;
  wire Conn2_WLAST;
  wire Conn2_WREADY;
  wire [3:0]Conn2_WSTRB;
  wire Conn2_WVALID;
  wire [31:0]Conn3_ARADDR;
  wire [2:0]Conn3_ARPROT;
  wire Conn3_ARREADY;
  wire Conn3_ARVALID;
  wire [31:0]Conn3_AWADDR;
  wire [2:0]Conn3_AWPROT;
  wire Conn3_AWREADY;
  wire Conn3_AWVALID;
  wire Conn3_BREADY;
  wire [1:0]Conn3_BRESP;
  wire Conn3_BVALID;
  wire [31:0]Conn3_RDATA;
  wire Conn3_RREADY;
  wire [1:0]Conn3_RRESP;
  wire Conn3_RVALID;
  wire [31:0]Conn3_WDATA;
  wire Conn3_WREADY;
  wire [3:0]Conn3_WSTRB;
  wire Conn3_WVALID;
  wire [31:0]Conn4_ARADDR;
  wire Conn4_ARREADY;
  wire Conn4_ARVALID;
  wire [31:0]Conn4_AWADDR;
  wire Conn4_AWREADY;
  wire Conn4_AWVALID;
  wire Conn4_BREADY;
  wire [1:0]Conn4_BRESP;
  wire Conn4_BVALID;
  wire [31:0]Conn4_RDATA;
  wire Conn4_RREADY;
  wire [1:0]Conn4_RRESP;
  wire Conn4_RVALID;
  wire [31:0]Conn4_WDATA;
  wire Conn4_WREADY;
  wire [3:0]Conn4_WSTRB;
  wire Conn4_WVALID;
  wire [15:0]D_1;
  wire S00_ARESETN_1;
  wire clk_1;
  wire clk_cpu_1;
  wire [15:0]ip_dbg_pha_0_dbg_pha_m0_peak_amp_rdy_fast_out;
  wire [15:0]ip_dbg_pha_0_dbg_pha_m0_peak_amp_rdy_slow_out;
  wire [15:0]ip_dbg_pha_0_dbg_pha_m0_peak_det_signal_out;
  wire [15:0]ip_dbg_pha_0_dbg_pha_m0_rejectn_out;
  wire ip_timers_0_bram_seg;
  wire ip_timers_0_timers_enabled;
  wire [15:0]pkd_peak_amp;
  wire [0:0]pkd_peak_amp_rdy;
  wire util_reduced_logic_0_Res;
  wire [1:0]xlconstant_0_dout;

  assign Conn1_ARADDR = S00_AXI3_araddr[31:0];
  assign Conn1_ARPROT = S00_AXI3_arprot[2:0];
  assign Conn1_ARVALID = S00_AXI3_arvalid;
  assign Conn1_AWADDR = S00_AXI3_awaddr[31:0];
  assign Conn1_AWPROT = S00_AXI3_awprot[2:0];
  assign Conn1_AWVALID = S00_AXI3_awvalid;
  assign Conn1_BREADY = S00_AXI3_bready;
  assign Conn1_RREADY = S00_AXI3_rready;
  assign Conn1_WDATA = S00_AXI3_wdata[31:0];
  assign Conn1_WSTRB = S00_AXI3_wstrb[3:0];
  assign Conn1_WVALID = S00_AXI3_wvalid;
  assign Conn2_ARADDR = S_AXI_araddr[12:0];
  assign Conn2_ARBURST = S_AXI_arburst[1:0];
  assign Conn2_ARCACHE = S_AXI_arcache[3:0];
  assign Conn2_ARLEN = S_AXI_arlen[7:0];
  assign Conn2_ARLOCK = S_AXI_arlock;
  assign Conn2_ARPROT = S_AXI_arprot[2:0];
  assign Conn2_ARSIZE = S_AXI_arsize[2:0];
  assign Conn2_ARVALID = S_AXI_arvalid;
  assign Conn2_AWADDR = S_AXI_awaddr[12:0];
  assign Conn2_AWBURST = S_AXI_awburst[1:0];
  assign Conn2_AWCACHE = S_AXI_awcache[3:0];
  assign Conn2_AWLEN = S_AXI_awlen[7:0];
  assign Conn2_AWLOCK = S_AXI_awlock;
  assign Conn2_AWPROT = S_AXI_awprot[2:0];
  assign Conn2_AWSIZE = S_AXI_awsize[2:0];
  assign Conn2_AWVALID = S_AXI_awvalid;
  assign Conn2_BREADY = S_AXI_bready;
  assign Conn2_RREADY = S_AXI_rready;
  assign Conn2_WDATA = S_AXI_wdata[31:0];
  assign Conn2_WLAST = S_AXI_wlast;
  assign Conn2_WSTRB = S_AXI_wstrb[3:0];
  assign Conn2_WVALID = S_AXI_wvalid;
  assign Conn3_ARADDR = S00_AXI4_araddr[31:0];
  assign Conn3_ARPROT = S00_AXI4_arprot[2:0];
  assign Conn3_ARVALID = S00_AXI4_arvalid;
  assign Conn3_AWADDR = S00_AXI4_awaddr[31:0];
  assign Conn3_AWPROT = S00_AXI4_awprot[2:0];
  assign Conn3_AWVALID = S00_AXI4_awvalid;
  assign Conn3_BREADY = S00_AXI4_bready;
  assign Conn3_RREADY = S00_AXI4_rready;
  assign Conn3_WDATA = S00_AXI4_wdata[31:0];
  assign Conn3_WSTRB = S00_AXI4_wstrb[3:0];
  assign Conn3_WVALID = S00_AXI4_wvalid;
  assign Conn4_ARADDR = axibusdomain_s_axi1_araddr[31:0];
  assign Conn4_ARVALID = axibusdomain_s_axi1_arvalid;
  assign Conn4_AWADDR = axibusdomain_s_axi1_awaddr[31:0];
  assign Conn4_AWVALID = axibusdomain_s_axi1_awvalid;
  assign Conn4_BREADY = axibusdomain_s_axi1_bready;
  assign Conn4_RREADY = axibusdomain_s_axi1_rready;
  assign Conn4_WDATA = axibusdomain_s_axi1_wdata[31:0];
  assign Conn4_WSTRB = axibusdomain_s_axi1_wstrb[3:0];
  assign Conn4_WVALID = axibusdomain_s_axi1_wvalid;
  assign D_1 = D[15:0];
  assign S00_ARESETN_1 = S00_ARESETN;
  assign S00_AXI3_arready = Conn1_ARREADY;
  assign S00_AXI3_awready = Conn1_AWREADY;
  assign S00_AXI3_bresp[1:0] = Conn1_BRESP;
  assign S00_AXI3_bvalid = Conn1_BVALID;
  assign S00_AXI3_rdata[31:0] = Conn1_RDATA;
  assign S00_AXI3_rresp[1:0] = Conn1_RRESP;
  assign S00_AXI3_rvalid = Conn1_RVALID;
  assign S00_AXI3_wready = Conn1_WREADY;
  assign S00_AXI4_arready = Conn3_ARREADY;
  assign S00_AXI4_awready = Conn3_AWREADY;
  assign S00_AXI4_bresp[1:0] = Conn3_BRESP;
  assign S00_AXI4_bvalid = Conn3_BVALID;
  assign S00_AXI4_rdata[31:0] = Conn3_RDATA;
  assign S00_AXI4_rresp[1:0] = Conn3_RRESP;
  assign S00_AXI4_rvalid = Conn3_RVALID;
  assign S00_AXI4_wready = Conn3_WREADY;
  assign S_AXI_arready = Conn2_ARREADY;
  assign S_AXI_awready = Conn2_AWREADY;
  assign S_AXI_bresp[1:0] = Conn2_BRESP;
  assign S_AXI_bvalid = Conn2_BVALID;
  assign S_AXI_rdata[31:0] = Conn2_RDATA;
  assign S_AXI_rlast = Conn2_RLAST;
  assign S_AXI_rresp[1:0] = Conn2_RRESP;
  assign S_AXI_rvalid = Conn2_RVALID;
  assign S_AXI_wready = Conn2_WREADY;
  assign axibusdomain_s_axi1_arready = Conn4_ARREADY;
  assign axibusdomain_s_axi1_awready = Conn4_AWREADY;
  assign axibusdomain_s_axi1_bresp[1:0] = Conn4_BRESP;
  assign axibusdomain_s_axi1_bvalid = Conn4_BVALID;
  assign axibusdomain_s_axi1_rdata[31:0] = Conn4_RDATA;
  assign axibusdomain_s_axi1_rresp[1:0] = Conn4_RRESP;
  assign axibusdomain_s_axi1_rvalid = Conn4_RVALID;
  assign axibusdomain_s_axi1_wready = Conn4_WREADY;
  assign clk_1 = clk;
  assign clk_cpu_1 = clk_cpu;
  assign dbg_pha_m0_peak_amp_rdy_fast_out[15:0] = ip_dbg_pha_0_dbg_pha_m0_peak_amp_rdy_fast_out;
  assign dbg_pha_m0_peak_amp_rdy_slow_out[15:0] = ip_dbg_pha_0_dbg_pha_m0_peak_amp_rdy_slow_out;
  assign dbg_pha_m0_peak_det_signal_out[15:0] = ip_dbg_pha_0_dbg_pha_m0_peak_det_signal_out;
  assign dbg_pha_m0_rejectn_out[15:0] = ip_dbg_pha_0_dbg_pha_m0_rejectn_out;
  glue_logic_imp_1C2RNKO glue_logic
       (.In0(ip_timers_0_timers_enabled),
        .In1(pkd_peak_amp_rdy),
        .Res(util_reduced_logic_0_Res));
  design_1_ip_dbg_pha_0_0 ip_dbg_pha_0
       (.peak_amp_rdy_fast(pkd_peak_amp_rdy),
        .peak_amp_rdy_fast_out(ip_dbg_pha_0_dbg_pha_m0_peak_amp_rdy_fast_out),
        .peak_amp_rdy_slow(pkd_peak_amp_rdy),
        .peak_amp_rdy_slow_out(ip_dbg_pha_0_dbg_pha_m0_peak_amp_rdy_slow_out),
        .peak_det_signal(pkd_peak_amp),
        .peak_det_signal_out(ip_dbg_pha_0_dbg_pha_m0_peak_det_signal_out),
        .rejectn(pkd_peak_amp_rdy),
        .rejectn_out(ip_dbg_pha_0_dbg_pha_m0_rejectn_out));
  design_1_ip_timers_0_0 ip_timers_0
       (.bram_seg(ip_timers_0_bram_seg),
        .clk(clk_1),
        .lt_dir(xlconstant_0_dout),
        .s00_axi_aclk(clk_cpu_1),
        .s00_axi_araddr(Conn1_ARADDR[4:0]),
        .s00_axi_aresetn(S00_ARESETN_1),
        .s00_axi_arprot(Conn1_ARPROT),
        .s00_axi_arready(Conn1_ARREADY),
        .s00_axi_arvalid(Conn1_ARVALID),
        .s00_axi_awaddr(Conn1_AWADDR[4:0]),
        .s00_axi_awprot(Conn1_AWPROT),
        .s00_axi_awready(Conn1_AWREADY),
        .s00_axi_awvalid(Conn1_AWVALID),
        .s00_axi_bready(Conn1_BREADY),
        .s00_axi_bresp(Conn1_BRESP),
        .s00_axi_bvalid(Conn1_BVALID),
        .s00_axi_rdata(Conn1_RDATA),
        .s00_axi_rready(Conn1_RREADY),
        .s00_axi_rresp(Conn1_RRESP),
        .s00_axi_rvalid(Conn1_RVALID),
        .s00_axi_wdata(Conn1_WDATA),
        .s00_axi_wready(Conn1_WREADY),
        .s00_axi_wstrb(Conn1_WSTRB),
        .s00_axi_wvalid(Conn1_WVALID),
        .timers_enabled(ip_timers_0_timers_enabled));
  mca_imp_44X2PW mca
       (.S00_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(Conn2_ARADDR),
        .S_AXI_arburst(Conn2_ARBURST),
        .S_AXI_arcache(Conn2_ARCACHE),
        .S_AXI_arlen(Conn2_ARLEN),
        .S_AXI_arlock(Conn2_ARLOCK),
        .S_AXI_arprot(Conn2_ARPROT),
        .S_AXI_arready(Conn2_ARREADY),
        .S_AXI_arsize(Conn2_ARSIZE),
        .S_AXI_arvalid(Conn2_ARVALID),
        .S_AXI_awaddr(Conn2_AWADDR),
        .S_AXI_awburst(Conn2_AWBURST),
        .S_AXI_awcache(Conn2_AWCACHE),
        .S_AXI_awlen(Conn2_AWLEN),
        .S_AXI_awlock(Conn2_AWLOCK),
        .S_AXI_awprot(Conn2_AWPROT),
        .S_AXI_awready(Conn2_AWREADY),
        .S_AXI_awsize(Conn2_AWSIZE),
        .S_AXI_awvalid(Conn2_AWVALID),
        .S_AXI_bready(Conn2_BREADY),
        .S_AXI_bresp(Conn2_BRESP),
        .S_AXI_bvalid(Conn2_BVALID),
        .S_AXI_rdata(Conn2_RDATA),
        .S_AXI_rlast(Conn2_RLAST),
        .S_AXI_rready(Conn2_RREADY),
        .S_AXI_rresp(Conn2_RRESP),
        .S_AXI_rvalid(Conn2_RVALID),
        .S_AXI_wdata(Conn2_WDATA),
        .S_AXI_wlast(Conn2_WLAST),
        .S_AXI_wready(Conn2_WREADY),
        .S_AXI_wstrb(Conn2_WSTRB),
        .S_AXI_wvalid(Conn2_WVALID),
        .clk(clk_1),
        .clk_cpu(clk_cpu_1),
        .peak_amp(pkd_peak_amp),
        .peak_amp_rdy(util_reduced_logic_0_Res),
        .segment(ip_timers_0_bram_seg));
  pkd_imp_E3BOMR pkd
       (.D(D_1),
        .S00_ARESETN(S00_ARESETN_1),
        .S00_AXI4_araddr(Conn3_ARADDR),
        .S00_AXI4_arprot(Conn3_ARPROT),
        .S00_AXI4_arready(Conn3_ARREADY),
        .S00_AXI4_arvalid(Conn3_ARVALID),
        .S00_AXI4_awaddr(Conn3_AWADDR),
        .S00_AXI4_awprot(Conn3_AWPROT),
        .S00_AXI4_awready(Conn3_AWREADY),
        .S00_AXI4_awvalid(Conn3_AWVALID),
        .S00_AXI4_bready(Conn3_BREADY),
        .S00_AXI4_bresp(Conn3_BRESP),
        .S00_AXI4_bvalid(Conn3_BVALID),
        .S00_AXI4_rdata(Conn3_RDATA),
        .S00_AXI4_rready(Conn3_RREADY),
        .S00_AXI4_rresp(Conn3_RRESP),
        .S00_AXI4_rvalid(Conn3_RVALID),
        .S00_AXI4_wdata(Conn3_WDATA),
        .S00_AXI4_wready(Conn3_WREADY),
        .S00_AXI4_wstrb(Conn3_WSTRB),
        .S00_AXI4_wvalid(Conn3_WVALID),
        .axibusdomain_s_axi1_araddr(Conn4_ARADDR),
        .axibusdomain_s_axi1_arready(Conn4_ARREADY),
        .axibusdomain_s_axi1_arvalid(Conn4_ARVALID),
        .axibusdomain_s_axi1_awaddr(Conn4_AWADDR),
        .axibusdomain_s_axi1_awready(Conn4_AWREADY),
        .axibusdomain_s_axi1_awvalid(Conn4_AWVALID),
        .axibusdomain_s_axi1_bready(Conn4_BREADY),
        .axibusdomain_s_axi1_bresp(Conn4_BRESP),
        .axibusdomain_s_axi1_bvalid(Conn4_BVALID),
        .axibusdomain_s_axi1_rdata(Conn4_RDATA),
        .axibusdomain_s_axi1_rready(Conn4_RREADY),
        .axibusdomain_s_axi1_rresp(Conn4_RRESP),
        .axibusdomain_s_axi1_rvalid(Conn4_RVALID),
        .axibusdomain_s_axi1_wdata(Conn4_WDATA),
        .axibusdomain_s_axi1_wready(Conn4_WREADY),
        .axibusdomain_s_axi1_wstrb(Conn4_WSTRB),
        .axibusdomain_s_axi1_wvalid(Conn4_WVALID),
        .clk(clk_1),
        .clk_cpu(clk_cpu_1),
        .peak_amp(pkd_peak_amp),
        .peak_amp_rdy(pkd_peak_amp_rdy));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule

module pkd_imp_E3BOMR
   (D,
    S00_ARESETN,
    S00_AXI4_araddr,
    S00_AXI4_arprot,
    S00_AXI4_arready,
    S00_AXI4_arvalid,
    S00_AXI4_awaddr,
    S00_AXI4_awprot,
    S00_AXI4_awready,
    S00_AXI4_awvalid,
    S00_AXI4_bready,
    S00_AXI4_bresp,
    S00_AXI4_bvalid,
    S00_AXI4_rdata,
    S00_AXI4_rready,
    S00_AXI4_rresp,
    S00_AXI4_rvalid,
    S00_AXI4_wdata,
    S00_AXI4_wready,
    S00_AXI4_wstrb,
    S00_AXI4_wvalid,
    axibusdomain_s_axi1_araddr,
    axibusdomain_s_axi1_arready,
    axibusdomain_s_axi1_arvalid,
    axibusdomain_s_axi1_awaddr,
    axibusdomain_s_axi1_awready,
    axibusdomain_s_axi1_awvalid,
    axibusdomain_s_axi1_bready,
    axibusdomain_s_axi1_bresp,
    axibusdomain_s_axi1_bvalid,
    axibusdomain_s_axi1_rdata,
    axibusdomain_s_axi1_rready,
    axibusdomain_s_axi1_rresp,
    axibusdomain_s_axi1_rvalid,
    axibusdomain_s_axi1_wdata,
    axibusdomain_s_axi1_wready,
    axibusdomain_s_axi1_wstrb,
    axibusdomain_s_axi1_wvalid,
    clk,
    clk_cpu,
    peak_amp,
    peak_amp_rdy);
  input [15:0]D;
  input S00_ARESETN;
  input [31:0]S00_AXI4_araddr;
  input [2:0]S00_AXI4_arprot;
  output S00_AXI4_arready;
  input S00_AXI4_arvalid;
  input [31:0]S00_AXI4_awaddr;
  input [2:0]S00_AXI4_awprot;
  output S00_AXI4_awready;
  input S00_AXI4_awvalid;
  input S00_AXI4_bready;
  output [1:0]S00_AXI4_bresp;
  output S00_AXI4_bvalid;
  output [31:0]S00_AXI4_rdata;
  input S00_AXI4_rready;
  output [1:0]S00_AXI4_rresp;
  output S00_AXI4_rvalid;
  input [31:0]S00_AXI4_wdata;
  output S00_AXI4_wready;
  input [3:0]S00_AXI4_wstrb;
  input S00_AXI4_wvalid;
  input [31:0]axibusdomain_s_axi1_araddr;
  output axibusdomain_s_axi1_arready;
  input axibusdomain_s_axi1_arvalid;
  input [31:0]axibusdomain_s_axi1_awaddr;
  output axibusdomain_s_axi1_awready;
  input axibusdomain_s_axi1_awvalid;
  input axibusdomain_s_axi1_bready;
  output [1:0]axibusdomain_s_axi1_bresp;
  output axibusdomain_s_axi1_bvalid;
  output [31:0]axibusdomain_s_axi1_rdata;
  input axibusdomain_s_axi1_rready;
  output [1:0]axibusdomain_s_axi1_rresp;
  output axibusdomain_s_axi1_rvalid;
  input [31:0]axibusdomain_s_axi1_wdata;
  output axibusdomain_s_axi1_wready;
  input [3:0]axibusdomain_s_axi1_wstrb;
  input axibusdomain_s_axi1_wvalid;
  input clk;
  input clk_cpu;
  output [15:0]peak_amp;
  output [0:0]peak_amp_rdy;

  wire [31:0]Conn1_ARADDR;
  wire [2:0]Conn1_ARPROT;
  wire Conn1_ARREADY;
  wire Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire [2:0]Conn1_AWPROT;
  wire Conn1_AWREADY;
  wire Conn1_AWVALID;
  wire Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire Conn1_BVALID;
  wire [31:0]Conn1_RDATA;
  wire Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire Conn1_RVALID;
  wire [31:0]Conn1_WDATA;
  wire Conn1_WREADY;
  wire [3:0]Conn1_WSTRB;
  wire Conn1_WVALID;
  wire [31:0]Conn2_ARADDR;
  wire Conn2_ARREADY;
  wire Conn2_ARVALID;
  wire [31:0]Conn2_AWADDR;
  wire Conn2_AWREADY;
  wire Conn2_AWVALID;
  wire Conn2_BREADY;
  wire [1:0]Conn2_BRESP;
  wire Conn2_BVALID;
  wire [31:0]Conn2_RDATA;
  wire Conn2_RREADY;
  wire [1:0]Conn2_RRESP;
  wire Conn2_RVALID;
  wire [31:0]Conn2_WDATA;
  wire Conn2_WREADY;
  wire [3:0]Conn2_WSTRB;
  wire Conn2_WVALID;
  wire [15:0]D_1;
  wire S00_ARESETN_1;
  wire [9:0]ShiftRegisterControl_0_shift_value;
  wire [15:0]c_shift_ram_0_Q;
  wire clk_1;
  wire clk_cpu_1;
  wire [15:0]ip_peakdetector_0_peak_amp;
  wire [0:0]ip_peakdetector_0_peak_amp_rdy;

  assign Conn1_ARADDR = S00_AXI4_araddr[31:0];
  assign Conn1_ARPROT = S00_AXI4_arprot[2:0];
  assign Conn1_ARVALID = S00_AXI4_arvalid;
  assign Conn1_AWADDR = S00_AXI4_awaddr[31:0];
  assign Conn1_AWPROT = S00_AXI4_awprot[2:0];
  assign Conn1_AWVALID = S00_AXI4_awvalid;
  assign Conn1_BREADY = S00_AXI4_bready;
  assign Conn1_RREADY = S00_AXI4_rready;
  assign Conn1_WDATA = S00_AXI4_wdata[31:0];
  assign Conn1_WSTRB = S00_AXI4_wstrb[3:0];
  assign Conn1_WVALID = S00_AXI4_wvalid;
  assign Conn2_ARADDR = axibusdomain_s_axi1_araddr[31:0];
  assign Conn2_ARVALID = axibusdomain_s_axi1_arvalid;
  assign Conn2_AWADDR = axibusdomain_s_axi1_awaddr[31:0];
  assign Conn2_AWVALID = axibusdomain_s_axi1_awvalid;
  assign Conn2_BREADY = axibusdomain_s_axi1_bready;
  assign Conn2_RREADY = axibusdomain_s_axi1_rready;
  assign Conn2_WDATA = axibusdomain_s_axi1_wdata[31:0];
  assign Conn2_WSTRB = axibusdomain_s_axi1_wstrb[3:0];
  assign Conn2_WVALID = axibusdomain_s_axi1_wvalid;
  assign D_1 = D[15:0];
  assign S00_ARESETN_1 = S00_ARESETN;
  assign S00_AXI4_arready = Conn1_ARREADY;
  assign S00_AXI4_awready = Conn1_AWREADY;
  assign S00_AXI4_bresp[1:0] = Conn1_BRESP;
  assign S00_AXI4_bvalid = Conn1_BVALID;
  assign S00_AXI4_rdata[31:0] = Conn1_RDATA;
  assign S00_AXI4_rresp[1:0] = Conn1_RRESP;
  assign S00_AXI4_rvalid = Conn1_RVALID;
  assign S00_AXI4_wready = Conn1_WREADY;
  assign axibusdomain_s_axi1_arready = Conn2_ARREADY;
  assign axibusdomain_s_axi1_awready = Conn2_AWREADY;
  assign axibusdomain_s_axi1_bresp[1:0] = Conn2_BRESP;
  assign axibusdomain_s_axi1_bvalid = Conn2_BVALID;
  assign axibusdomain_s_axi1_rdata[31:0] = Conn2_RDATA;
  assign axibusdomain_s_axi1_rresp[1:0] = Conn2_RRESP;
  assign axibusdomain_s_axi1_rvalid = Conn2_RVALID;
  assign axibusdomain_s_axi1_wready = Conn2_WREADY;
  assign clk_1 = clk;
  assign clk_cpu_1 = clk_cpu;
  assign peak_amp[15:0] = ip_peakdetector_0_peak_amp;
  assign peak_amp_rdy[0] = ip_peakdetector_0_peak_amp_rdy;
  design_1_ShiftRegisterControl_0_0 ShiftRegisterControl_0
       (.s00_axi_aclk(clk_cpu_1),
        .s00_axi_araddr(Conn1_ARADDR[3:0]),
        .s00_axi_aresetn(S00_ARESETN_1),
        .s00_axi_arprot(Conn1_ARPROT),
        .s00_axi_arready(Conn1_ARREADY),
        .s00_axi_arvalid(Conn1_ARVALID),
        .s00_axi_awaddr(Conn1_AWADDR[3:0]),
        .s00_axi_awprot(Conn1_AWPROT),
        .s00_axi_awready(Conn1_AWREADY),
        .s00_axi_awvalid(Conn1_AWVALID),
        .s00_axi_bready(Conn1_BREADY),
        .s00_axi_bresp(Conn1_BRESP),
        .s00_axi_bvalid(Conn1_BVALID),
        .s00_axi_rdata(Conn1_RDATA),
        .s00_axi_rready(Conn1_RREADY),
        .s00_axi_rresp(Conn1_RRESP),
        .s00_axi_rvalid(Conn1_RVALID),
        .s00_axi_wdata(Conn1_WDATA),
        .s00_axi_wready(Conn1_WREADY),
        .s00_axi_wstrb(Conn1_WSTRB),
        .s00_axi_wvalid(Conn1_WVALID),
        .shift_value(ShiftRegisterControl_0_shift_value));
  design_1_c_shift_ram_0_0 c_shift_ram_0
       (.A(ShiftRegisterControl_0_shift_value),
        .CLK(clk_1),
        .D(D_1),
        .Q(c_shift_ram_0_Q));
  design_1_ip_peakdetector_0_0 ip_peakdetector_0
       (.axibusdomain_aresetn(S00_ARESETN_1),
        .axibusdomain_clk(clk_cpu_1),
        .axibusdomain_s_axi_araddr(Conn2_ARADDR[4:0]),
        .axibusdomain_s_axi_arready(Conn2_ARREADY),
        .axibusdomain_s_axi_arvalid(Conn2_ARVALID),
        .axibusdomain_s_axi_awaddr(Conn2_AWADDR[4:0]),
        .axibusdomain_s_axi_awready(Conn2_AWREADY),
        .axibusdomain_s_axi_awvalid(Conn2_AWVALID),
        .axibusdomain_s_axi_bready(Conn2_BREADY),
        .axibusdomain_s_axi_bresp(Conn2_BRESP),
        .axibusdomain_s_axi_bvalid(Conn2_BVALID),
        .axibusdomain_s_axi_rdata(Conn2_RDATA),
        .axibusdomain_s_axi_rready(Conn2_RREADY),
        .axibusdomain_s_axi_rresp(Conn2_RRESP),
        .axibusdomain_s_axi_rvalid(Conn2_RVALID),
        .axibusdomain_s_axi_wdata(Conn2_WDATA),
        .axibusdomain_s_axi_wready(Conn2_WREADY),
        .axibusdomain_s_axi_wstrb(Conn2_WSTRB),
        .axibusdomain_s_axi_wvalid(Conn2_WVALID),
        .peak_amp(ip_peakdetector_0_peak_amp),
        .peak_amp_rdy(ip_peakdetector_0_peak_amp_rdy),
        .signaldomain_clk(clk_1),
        .x(c_shift_ram_0_Q));
endmodule

module ps_mb_0_imp_4932ML
   (Clk,
    M00_AXI_araddr,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    M02_AXI_araddr,
    M02_AXI_arprot,
    M02_AXI_arready,
    M02_AXI_arvalid,
    M02_AXI_awaddr,
    M02_AXI_awprot,
    M02_AXI_awready,
    M02_AXI_awvalid,
    M02_AXI_bready,
    M02_AXI_bresp,
    M02_AXI_bvalid,
    M02_AXI_rdata,
    M02_AXI_rready,
    M02_AXI_rresp,
    M02_AXI_rvalid,
    M02_AXI_wdata,
    M02_AXI_wready,
    M02_AXI_wstrb,
    M02_AXI_wvalid,
    M03_AXI_araddr,
    M03_AXI_arready,
    M03_AXI_arvalid,
    M03_AXI_awaddr,
    M03_AXI_awready,
    M03_AXI_awvalid,
    M03_AXI_bready,
    M03_AXI_bresp,
    M03_AXI_bvalid,
    M03_AXI_rdata,
    M03_AXI_rready,
    M03_AXI_rresp,
    M03_AXI_rvalid,
    M03_AXI_wdata,
    M03_AXI_wready,
    M03_AXI_wstrb,
    M03_AXI_wvalid,
    M05_AXI_araddr,
    M05_AXI_arready,
    M05_AXI_arvalid,
    M05_AXI_awaddr,
    M05_AXI_awready,
    M05_AXI_awvalid,
    M05_AXI_bready,
    M05_AXI_bresp,
    M05_AXI_bvalid,
    M05_AXI_rdata,
    M05_AXI_rready,
    M05_AXI_rresp,
    M05_AXI_rvalid,
    M05_AXI_wdata,
    M05_AXI_wready,
    M05_AXI_wstrb,
    M05_AXI_wvalid,
    M06_AXI_araddr,
    M06_AXI_arprot,
    M06_AXI_arready,
    M06_AXI_arvalid,
    M06_AXI_awaddr,
    M06_AXI_awprot,
    M06_AXI_awready,
    M06_AXI_awvalid,
    M06_AXI_bready,
    M06_AXI_bresp,
    M06_AXI_bvalid,
    M06_AXI_rdata,
    M06_AXI_rready,
    M06_AXI_rresp,
    M06_AXI_rvalid,
    M06_AXI_wdata,
    M06_AXI_wready,
    M06_AXI_wstrb,
    M06_AXI_wvalid,
    M07_AXI_araddr,
    M07_AXI_arprot,
    M07_AXI_arready,
    M07_AXI_arvalid,
    M07_AXI_awaddr,
    M07_AXI_awprot,
    M07_AXI_awready,
    M07_AXI_awvalid,
    M07_AXI_bready,
    M07_AXI_bresp,
    M07_AXI_bvalid,
    M07_AXI_rdata,
    M07_AXI_rready,
    M07_AXI_rresp,
    M07_AXI_rvalid,
    M07_AXI_wdata,
    M07_AXI_wready,
    M07_AXI_wstrb,
    M07_AXI_wvalid,
    M08_AXI_araddr,
    M08_AXI_arprot,
    M08_AXI_arready,
    M08_AXI_arvalid,
    M08_AXI_awaddr,
    M08_AXI_awprot,
    M08_AXI_awready,
    M08_AXI_awvalid,
    M08_AXI_bready,
    M08_AXI_bresp,
    M08_AXI_bvalid,
    M08_AXI_rdata,
    M08_AXI_rready,
    M08_AXI_rresp,
    M08_AXI_rvalid,
    M08_AXI_wdata,
    M08_AXI_wready,
    M08_AXI_wstrb,
    M08_AXI_wvalid,
    M09_AXI_araddr,
    M09_AXI_arburst,
    M09_AXI_arcache,
    M09_AXI_arlen,
    M09_AXI_arlock,
    M09_AXI_arprot,
    M09_AXI_arready,
    M09_AXI_arsize,
    M09_AXI_arvalid,
    M09_AXI_awaddr,
    M09_AXI_awburst,
    M09_AXI_awcache,
    M09_AXI_awlen,
    M09_AXI_awlock,
    M09_AXI_awprot,
    M09_AXI_awready,
    M09_AXI_awsize,
    M09_AXI_awvalid,
    M09_AXI_bready,
    M09_AXI_bresp,
    M09_AXI_bvalid,
    M09_AXI_rdata,
    M09_AXI_rlast,
    M09_AXI_rready,
    M09_AXI_rresp,
    M09_AXI_rvalid,
    M09_AXI_wdata,
    M09_AXI_wlast,
    M09_AXI_wready,
    M09_AXI_wstrb,
    M09_AXI_wvalid,
    M10_AXI_araddr,
    M10_AXI_arprot,
    M10_AXI_arready,
    M10_AXI_arvalid,
    M10_AXI_awaddr,
    M10_AXI_awprot,
    M10_AXI_awready,
    M10_AXI_awvalid,
    M10_AXI_bready,
    M10_AXI_bresp,
    M10_AXI_bvalid,
    M10_AXI_rdata,
    M10_AXI_rready,
    M10_AXI_rresp,
    M10_AXI_rvalid,
    M10_AXI_wdata,
    M10_AXI_wready,
    M10_AXI_wstrb,
    M10_AXI_wvalid,
    M11_AXI_araddr,
    M11_AXI_arready,
    M11_AXI_arvalid,
    M11_AXI_awaddr,
    M11_AXI_awready,
    M11_AXI_awvalid,
    M11_AXI_bready,
    M11_AXI_bresp,
    M11_AXI_bvalid,
    M11_AXI_rdata,
    M11_AXI_rready,
    M11_AXI_rresp,
    M11_AXI_rvalid,
    M11_AXI_wdata,
    M11_AXI_wready,
    M11_AXI_wstrb,
    M11_AXI_wvalid,
    S00_ARESETN,
    dcm_locked,
    intr,
    reset);
  input Clk;
  output [31:0]M00_AXI_araddr;
  input M00_AXI_arready;
  output M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  input M00_AXI_awready;
  output M00_AXI_awvalid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input M01_AXI_arready;
  output M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input M01_AXI_awready;
  output M01_AXI_awvalid;
  output M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input M01_AXI_bvalid;
  input [31:0]M01_AXI_rdata;
  output M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input M01_AXI_rvalid;
  output [31:0]M01_AXI_wdata;
  input M01_AXI_wready;
  output [3:0]M01_AXI_wstrb;
  output M01_AXI_wvalid;
  output [31:0]M02_AXI_araddr;
  output [2:0]M02_AXI_arprot;
  input M02_AXI_arready;
  output M02_AXI_arvalid;
  output [31:0]M02_AXI_awaddr;
  output [2:0]M02_AXI_awprot;
  input M02_AXI_awready;
  output M02_AXI_awvalid;
  output M02_AXI_bready;
  input [1:0]M02_AXI_bresp;
  input M02_AXI_bvalid;
  input [31:0]M02_AXI_rdata;
  output M02_AXI_rready;
  input [1:0]M02_AXI_rresp;
  input M02_AXI_rvalid;
  output [31:0]M02_AXI_wdata;
  input M02_AXI_wready;
  output [3:0]M02_AXI_wstrb;
  output M02_AXI_wvalid;
  output [31:0]M03_AXI_araddr;
  input M03_AXI_arready;
  output M03_AXI_arvalid;
  output [31:0]M03_AXI_awaddr;
  input M03_AXI_awready;
  output M03_AXI_awvalid;
  output M03_AXI_bready;
  input [1:0]M03_AXI_bresp;
  input M03_AXI_bvalid;
  input [31:0]M03_AXI_rdata;
  output M03_AXI_rready;
  input [1:0]M03_AXI_rresp;
  input M03_AXI_rvalid;
  output [31:0]M03_AXI_wdata;
  input M03_AXI_wready;
  output [3:0]M03_AXI_wstrb;
  output M03_AXI_wvalid;
  output [31:0]M05_AXI_araddr;
  input M05_AXI_arready;
  output M05_AXI_arvalid;
  output [31:0]M05_AXI_awaddr;
  input M05_AXI_awready;
  output M05_AXI_awvalid;
  output M05_AXI_bready;
  input [1:0]M05_AXI_bresp;
  input M05_AXI_bvalid;
  input [31:0]M05_AXI_rdata;
  output M05_AXI_rready;
  input [1:0]M05_AXI_rresp;
  input M05_AXI_rvalid;
  output [31:0]M05_AXI_wdata;
  input M05_AXI_wready;
  output [3:0]M05_AXI_wstrb;
  output M05_AXI_wvalid;
  output [31:0]M06_AXI_araddr;
  output [2:0]M06_AXI_arprot;
  input M06_AXI_arready;
  output M06_AXI_arvalid;
  output [31:0]M06_AXI_awaddr;
  output [2:0]M06_AXI_awprot;
  input M06_AXI_awready;
  output M06_AXI_awvalid;
  output M06_AXI_bready;
  input [1:0]M06_AXI_bresp;
  input M06_AXI_bvalid;
  input [31:0]M06_AXI_rdata;
  output M06_AXI_rready;
  input [1:0]M06_AXI_rresp;
  input M06_AXI_rvalid;
  output [31:0]M06_AXI_wdata;
  input M06_AXI_wready;
  output [3:0]M06_AXI_wstrb;
  output M06_AXI_wvalid;
  output [31:0]M07_AXI_araddr;
  output [2:0]M07_AXI_arprot;
  input M07_AXI_arready;
  output M07_AXI_arvalid;
  output [31:0]M07_AXI_awaddr;
  output [2:0]M07_AXI_awprot;
  input M07_AXI_awready;
  output M07_AXI_awvalid;
  output M07_AXI_bready;
  input [1:0]M07_AXI_bresp;
  input M07_AXI_bvalid;
  input [31:0]M07_AXI_rdata;
  output M07_AXI_rready;
  input [1:0]M07_AXI_rresp;
  input M07_AXI_rvalid;
  output [31:0]M07_AXI_wdata;
  input M07_AXI_wready;
  output [3:0]M07_AXI_wstrb;
  output M07_AXI_wvalid;
  output [31:0]M08_AXI_araddr;
  output [2:0]M08_AXI_arprot;
  input M08_AXI_arready;
  output M08_AXI_arvalid;
  output [31:0]M08_AXI_awaddr;
  output [2:0]M08_AXI_awprot;
  input M08_AXI_awready;
  output M08_AXI_awvalid;
  output M08_AXI_bready;
  input [1:0]M08_AXI_bresp;
  input M08_AXI_bvalid;
  input [31:0]M08_AXI_rdata;
  output M08_AXI_rready;
  input [1:0]M08_AXI_rresp;
  input M08_AXI_rvalid;
  output [31:0]M08_AXI_wdata;
  input M08_AXI_wready;
  output [3:0]M08_AXI_wstrb;
  output M08_AXI_wvalid;
  output [12:0]M09_AXI_araddr;
  output [1:0]M09_AXI_arburst;
  output [3:0]M09_AXI_arcache;
  output [7:0]M09_AXI_arlen;
  output M09_AXI_arlock;
  output [2:0]M09_AXI_arprot;
  input M09_AXI_arready;
  output [2:0]M09_AXI_arsize;
  output M09_AXI_arvalid;
  output [12:0]M09_AXI_awaddr;
  output [1:0]M09_AXI_awburst;
  output [3:0]M09_AXI_awcache;
  output [7:0]M09_AXI_awlen;
  output M09_AXI_awlock;
  output [2:0]M09_AXI_awprot;
  input M09_AXI_awready;
  output [2:0]M09_AXI_awsize;
  output M09_AXI_awvalid;
  output M09_AXI_bready;
  input [1:0]M09_AXI_bresp;
  input M09_AXI_bvalid;
  input [31:0]M09_AXI_rdata;
  input M09_AXI_rlast;
  output M09_AXI_rready;
  input [1:0]M09_AXI_rresp;
  input M09_AXI_rvalid;
  output [31:0]M09_AXI_wdata;
  output M09_AXI_wlast;
  input M09_AXI_wready;
  output [3:0]M09_AXI_wstrb;
  output M09_AXI_wvalid;
  output [31:0]M10_AXI_araddr;
  output [2:0]M10_AXI_arprot;
  input M10_AXI_arready;
  output M10_AXI_arvalid;
  output [31:0]M10_AXI_awaddr;
  output [2:0]M10_AXI_awprot;
  input M10_AXI_awready;
  output M10_AXI_awvalid;
  output M10_AXI_bready;
  input [1:0]M10_AXI_bresp;
  input M10_AXI_bvalid;
  input [31:0]M10_AXI_rdata;
  output M10_AXI_rready;
  input [1:0]M10_AXI_rresp;
  input M10_AXI_rvalid;
  output [31:0]M10_AXI_wdata;
  input M10_AXI_wready;
  output [3:0]M10_AXI_wstrb;
  output M10_AXI_wvalid;
  output [31:0]M11_AXI_araddr;
  input M11_AXI_arready;
  output M11_AXI_arvalid;
  output [31:0]M11_AXI_awaddr;
  input M11_AXI_awready;
  output M11_AXI_awvalid;
  output M11_AXI_bready;
  input [1:0]M11_AXI_bresp;
  input M11_AXI_bvalid;
  input [31:0]M11_AXI_rdata;
  output M11_AXI_rready;
  input [1:0]M11_AXI_rresp;
  input M11_AXI_rvalid;
  output [31:0]M11_AXI_wdata;
  input M11_AXI_wready;
  output [3:0]M11_AXI_wstrb;
  output M11_AXI_wvalid;
  output [0:0]S00_ARESETN;
  input dcm_locked;
  input [0:0]intr;
  input reset;

  wire axi_intc_0_interrupt_INTERRUPT;
  wire clk_wiz_0_locked;
  wire [0:0]intr_1;
  wire mdm_1_debug_sys_rst;
  wire microblaze_0_Clk;
  wire [31:0]microblaze_0_M_AXI_DP_ARADDR;
  wire [2:0]microblaze_0_M_AXI_DP_ARPROT;
  wire [0:0]microblaze_0_M_AXI_DP_ARREADY;
  wire microblaze_0_M_AXI_DP_ARVALID;
  wire [31:0]microblaze_0_M_AXI_DP_AWADDR;
  wire [2:0]microblaze_0_M_AXI_DP_AWPROT;
  wire [0:0]microblaze_0_M_AXI_DP_AWREADY;
  wire microblaze_0_M_AXI_DP_AWVALID;
  wire microblaze_0_M_AXI_DP_BREADY;
  wire [1:0]microblaze_0_M_AXI_DP_BRESP;
  wire [0:0]microblaze_0_M_AXI_DP_BVALID;
  wire [31:0]microblaze_0_M_AXI_DP_RDATA;
  wire microblaze_0_M_AXI_DP_RREADY;
  wire [1:0]microblaze_0_M_AXI_DP_RRESP;
  wire [0:0]microblaze_0_M_AXI_DP_RVALID;
  wire [31:0]microblaze_0_M_AXI_DP_WDATA;
  wire [0:0]microblaze_0_M_AXI_DP_WREADY;
  wire [3:0]microblaze_0_M_AXI_DP_WSTRB;
  wire microblaze_0_M_AXI_DP_WVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_ARADDR;
  wire microblaze_0_axi_periph_M00_AXI_ARREADY;
  wire microblaze_0_axi_periph_M00_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_AWADDR;
  wire microblaze_0_axi_periph_M00_AXI_AWREADY;
  wire microblaze_0_axi_periph_M00_AXI_AWVALID;
  wire microblaze_0_axi_periph_M00_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M00_AXI_BRESP;
  wire microblaze_0_axi_periph_M00_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_RDATA;
  wire microblaze_0_axi_periph_M00_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M00_AXI_RRESP;
  wire microblaze_0_axi_periph_M00_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_WDATA;
  wire microblaze_0_axi_periph_M00_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M00_AXI_WSTRB;
  wire microblaze_0_axi_periph_M00_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M01_AXI_ARADDR;
  wire [2:0]microblaze_0_axi_periph_M01_AXI_ARPROT;
  wire microblaze_0_axi_periph_M01_AXI_ARREADY;
  wire microblaze_0_axi_periph_M01_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M01_AXI_AWADDR;
  wire [2:0]microblaze_0_axi_periph_M01_AXI_AWPROT;
  wire microblaze_0_axi_periph_M01_AXI_AWREADY;
  wire microblaze_0_axi_periph_M01_AXI_AWVALID;
  wire microblaze_0_axi_periph_M01_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M01_AXI_BRESP;
  wire microblaze_0_axi_periph_M01_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M01_AXI_RDATA;
  wire microblaze_0_axi_periph_M01_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M01_AXI_RRESP;
  wire microblaze_0_axi_periph_M01_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M01_AXI_WDATA;
  wire microblaze_0_axi_periph_M01_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M01_AXI_WSTRB;
  wire microblaze_0_axi_periph_M01_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M02_AXI_ARADDR;
  wire [2:0]microblaze_0_axi_periph_M02_AXI_ARPROT;
  wire microblaze_0_axi_periph_M02_AXI_ARREADY;
  wire microblaze_0_axi_periph_M02_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M02_AXI_AWADDR;
  wire [2:0]microblaze_0_axi_periph_M02_AXI_AWPROT;
  wire microblaze_0_axi_periph_M02_AXI_AWREADY;
  wire microblaze_0_axi_periph_M02_AXI_AWVALID;
  wire microblaze_0_axi_periph_M02_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M02_AXI_BRESP;
  wire microblaze_0_axi_periph_M02_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M02_AXI_RDATA;
  wire microblaze_0_axi_periph_M02_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M02_AXI_RRESP;
  wire microblaze_0_axi_periph_M02_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M02_AXI_WDATA;
  wire microblaze_0_axi_periph_M02_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M02_AXI_WSTRB;
  wire microblaze_0_axi_periph_M02_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M03_AXI_ARADDR;
  wire microblaze_0_axi_periph_M03_AXI_ARREADY;
  wire microblaze_0_axi_periph_M03_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M03_AXI_AWADDR;
  wire microblaze_0_axi_periph_M03_AXI_AWREADY;
  wire microblaze_0_axi_periph_M03_AXI_AWVALID;
  wire microblaze_0_axi_periph_M03_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M03_AXI_BRESP;
  wire microblaze_0_axi_periph_M03_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M03_AXI_RDATA;
  wire microblaze_0_axi_periph_M03_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M03_AXI_RRESP;
  wire microblaze_0_axi_periph_M03_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M03_AXI_WDATA;
  wire microblaze_0_axi_periph_M03_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M03_AXI_WSTRB;
  wire microblaze_0_axi_periph_M03_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M04_AXI_ARADDR;
  wire microblaze_0_axi_periph_M04_AXI_ARREADY;
  wire [0:0]microblaze_0_axi_periph_M04_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M04_AXI_AWADDR;
  wire microblaze_0_axi_periph_M04_AXI_AWREADY;
  wire [0:0]microblaze_0_axi_periph_M04_AXI_AWVALID;
  wire [0:0]microblaze_0_axi_periph_M04_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M04_AXI_BRESP;
  wire microblaze_0_axi_periph_M04_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M04_AXI_RDATA;
  wire [0:0]microblaze_0_axi_periph_M04_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M04_AXI_RRESP;
  wire microblaze_0_axi_periph_M04_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M04_AXI_WDATA;
  wire microblaze_0_axi_periph_M04_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M04_AXI_WSTRB;
  wire [0:0]microblaze_0_axi_periph_M04_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M05_AXI_ARADDR;
  wire microblaze_0_axi_periph_M05_AXI_ARREADY;
  wire microblaze_0_axi_periph_M05_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M05_AXI_AWADDR;
  wire microblaze_0_axi_periph_M05_AXI_AWREADY;
  wire microblaze_0_axi_periph_M05_AXI_AWVALID;
  wire microblaze_0_axi_periph_M05_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M05_AXI_BRESP;
  wire microblaze_0_axi_periph_M05_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M05_AXI_RDATA;
  wire microblaze_0_axi_periph_M05_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M05_AXI_RRESP;
  wire microblaze_0_axi_periph_M05_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M05_AXI_WDATA;
  wire microblaze_0_axi_periph_M05_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M05_AXI_WSTRB;
  wire microblaze_0_axi_periph_M05_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M06_AXI_ARADDR;
  wire [2:0]microblaze_0_axi_periph_M06_AXI_ARPROT;
  wire microblaze_0_axi_periph_M06_AXI_ARREADY;
  wire microblaze_0_axi_periph_M06_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M06_AXI_AWADDR;
  wire [2:0]microblaze_0_axi_periph_M06_AXI_AWPROT;
  wire microblaze_0_axi_periph_M06_AXI_AWREADY;
  wire microblaze_0_axi_periph_M06_AXI_AWVALID;
  wire microblaze_0_axi_periph_M06_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M06_AXI_BRESP;
  wire microblaze_0_axi_periph_M06_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M06_AXI_RDATA;
  wire microblaze_0_axi_periph_M06_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M06_AXI_RRESP;
  wire microblaze_0_axi_periph_M06_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M06_AXI_WDATA;
  wire microblaze_0_axi_periph_M06_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M06_AXI_WSTRB;
  wire microblaze_0_axi_periph_M06_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M07_AXI_ARADDR;
  wire [2:0]microblaze_0_axi_periph_M07_AXI_ARPROT;
  wire microblaze_0_axi_periph_M07_AXI_ARREADY;
  wire microblaze_0_axi_periph_M07_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M07_AXI_AWADDR;
  wire [2:0]microblaze_0_axi_periph_M07_AXI_AWPROT;
  wire microblaze_0_axi_periph_M07_AXI_AWREADY;
  wire microblaze_0_axi_periph_M07_AXI_AWVALID;
  wire microblaze_0_axi_periph_M07_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M07_AXI_BRESP;
  wire microblaze_0_axi_periph_M07_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M07_AXI_RDATA;
  wire microblaze_0_axi_periph_M07_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M07_AXI_RRESP;
  wire microblaze_0_axi_periph_M07_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M07_AXI_WDATA;
  wire microblaze_0_axi_periph_M07_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M07_AXI_WSTRB;
  wire microblaze_0_axi_periph_M07_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M08_AXI_ARADDR;
  wire [2:0]microblaze_0_axi_periph_M08_AXI_ARPROT;
  wire microblaze_0_axi_periph_M08_AXI_ARREADY;
  wire microblaze_0_axi_periph_M08_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M08_AXI_AWADDR;
  wire [2:0]microblaze_0_axi_periph_M08_AXI_AWPROT;
  wire microblaze_0_axi_periph_M08_AXI_AWREADY;
  wire microblaze_0_axi_periph_M08_AXI_AWVALID;
  wire microblaze_0_axi_periph_M08_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M08_AXI_BRESP;
  wire microblaze_0_axi_periph_M08_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M08_AXI_RDATA;
  wire microblaze_0_axi_periph_M08_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M08_AXI_RRESP;
  wire microblaze_0_axi_periph_M08_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M08_AXI_WDATA;
  wire microblaze_0_axi_periph_M08_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M08_AXI_WSTRB;
  wire microblaze_0_axi_periph_M08_AXI_WVALID;
  wire [12:0]microblaze_0_axi_periph_M09_AXI_ARADDR;
  wire [1:0]microblaze_0_axi_periph_M09_AXI_ARBURST;
  wire [3:0]microblaze_0_axi_periph_M09_AXI_ARCACHE;
  wire [7:0]microblaze_0_axi_periph_M09_AXI_ARLEN;
  wire microblaze_0_axi_periph_M09_AXI_ARLOCK;
  wire [2:0]microblaze_0_axi_periph_M09_AXI_ARPROT;
  wire microblaze_0_axi_periph_M09_AXI_ARREADY;
  wire [2:0]microblaze_0_axi_periph_M09_AXI_ARSIZE;
  wire microblaze_0_axi_periph_M09_AXI_ARVALID;
  wire [12:0]microblaze_0_axi_periph_M09_AXI_AWADDR;
  wire [1:0]microblaze_0_axi_periph_M09_AXI_AWBURST;
  wire [3:0]microblaze_0_axi_periph_M09_AXI_AWCACHE;
  wire [7:0]microblaze_0_axi_periph_M09_AXI_AWLEN;
  wire microblaze_0_axi_periph_M09_AXI_AWLOCK;
  wire [2:0]microblaze_0_axi_periph_M09_AXI_AWPROT;
  wire microblaze_0_axi_periph_M09_AXI_AWREADY;
  wire [2:0]microblaze_0_axi_periph_M09_AXI_AWSIZE;
  wire microblaze_0_axi_periph_M09_AXI_AWVALID;
  wire microblaze_0_axi_periph_M09_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M09_AXI_BRESP;
  wire microblaze_0_axi_periph_M09_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M09_AXI_RDATA;
  wire microblaze_0_axi_periph_M09_AXI_RLAST;
  wire microblaze_0_axi_periph_M09_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M09_AXI_RRESP;
  wire microblaze_0_axi_periph_M09_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M09_AXI_WDATA;
  wire microblaze_0_axi_periph_M09_AXI_WLAST;
  wire microblaze_0_axi_periph_M09_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M09_AXI_WSTRB;
  wire microblaze_0_axi_periph_M09_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M10_AXI_ARADDR;
  wire [2:0]microblaze_0_axi_periph_M10_AXI_ARPROT;
  wire microblaze_0_axi_periph_M10_AXI_ARREADY;
  wire microblaze_0_axi_periph_M10_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M10_AXI_AWADDR;
  wire [2:0]microblaze_0_axi_periph_M10_AXI_AWPROT;
  wire microblaze_0_axi_periph_M10_AXI_AWREADY;
  wire microblaze_0_axi_periph_M10_AXI_AWVALID;
  wire microblaze_0_axi_periph_M10_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M10_AXI_BRESP;
  wire microblaze_0_axi_periph_M10_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M10_AXI_RDATA;
  wire microblaze_0_axi_periph_M10_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M10_AXI_RRESP;
  wire microblaze_0_axi_periph_M10_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M10_AXI_WDATA;
  wire microblaze_0_axi_periph_M10_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M10_AXI_WSTRB;
  wire microblaze_0_axi_periph_M10_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M11_AXI_ARADDR;
  wire microblaze_0_axi_periph_M11_AXI_ARREADY;
  wire microblaze_0_axi_periph_M11_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M11_AXI_AWADDR;
  wire microblaze_0_axi_periph_M11_AXI_AWREADY;
  wire microblaze_0_axi_periph_M11_AXI_AWVALID;
  wire microblaze_0_axi_periph_M11_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M11_AXI_BRESP;
  wire microblaze_0_axi_periph_M11_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M11_AXI_RDATA;
  wire microblaze_0_axi_periph_M11_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M11_AXI_RRESP;
  wire microblaze_0_axi_periph_M11_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M11_AXI_WDATA;
  wire microblaze_0_axi_periph_M11_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M11_AXI_WSTRB;
  wire microblaze_0_axi_periph_M11_AXI_WVALID;
  wire microblaze_0_debug_CAPTURE;
  wire microblaze_0_debug_CLK;
  wire microblaze_0_debug_DISABLE;
  wire [0:7]microblaze_0_debug_REG_EN;
  wire microblaze_0_debug_RST;
  wire microblaze_0_debug_SHIFT;
  wire microblaze_0_debug_TDI;
  wire microblaze_0_debug_TDO;
  wire microblaze_0_debug_UPDATE;
  wire [0:31]microblaze_0_dlmb_1_ABUS;
  wire microblaze_0_dlmb_1_ADDRSTROBE;
  wire [0:3]microblaze_0_dlmb_1_BE;
  wire microblaze_0_dlmb_1_CE;
  wire [0:31]microblaze_0_dlmb_1_READDBUS;
  wire microblaze_0_dlmb_1_READSTROBE;
  wire microblaze_0_dlmb_1_READY;
  wire microblaze_0_dlmb_1_UE;
  wire microblaze_0_dlmb_1_WAIT;
  wire [0:31]microblaze_0_dlmb_1_WRITEDBUS;
  wire microblaze_0_dlmb_1_WRITESTROBE;
  wire [0:31]microblaze_0_ilmb_1_ABUS;
  wire microblaze_0_ilmb_1_ADDRSTROBE;
  wire microblaze_0_ilmb_1_CE;
  wire [0:31]microblaze_0_ilmb_1_READDBUS;
  wire microblaze_0_ilmb_1_READSTROBE;
  wire microblaze_0_ilmb_1_READY;
  wire microblaze_0_ilmb_1_UE;
  wire microblaze_0_ilmb_1_WAIT;
  wire reset_1;
  wire [0:0]rst_clk_wiz_0_120M_bus_struct_reset;
  wire rst_clk_wiz_0_120M_mb_reset;
  wire [0:0]rst_clk_wiz_0_120M_peripheral_aresetn;

  assign M00_AXI_araddr[31:0] = microblaze_0_axi_periph_M00_AXI_ARADDR;
  assign M00_AXI_arvalid = microblaze_0_axi_periph_M00_AXI_ARVALID;
  assign M00_AXI_awaddr[31:0] = microblaze_0_axi_periph_M00_AXI_AWADDR;
  assign M00_AXI_awvalid = microblaze_0_axi_periph_M00_AXI_AWVALID;
  assign M00_AXI_bready = microblaze_0_axi_periph_M00_AXI_BREADY;
  assign M00_AXI_rready = microblaze_0_axi_periph_M00_AXI_RREADY;
  assign M00_AXI_wdata[31:0] = microblaze_0_axi_periph_M00_AXI_WDATA;
  assign M00_AXI_wstrb[3:0] = microblaze_0_axi_periph_M00_AXI_WSTRB;
  assign M00_AXI_wvalid = microblaze_0_axi_periph_M00_AXI_WVALID;
  assign M01_AXI_araddr[31:0] = microblaze_0_axi_periph_M01_AXI_ARADDR;
  assign M01_AXI_arprot[2:0] = microblaze_0_axi_periph_M01_AXI_ARPROT;
  assign M01_AXI_arvalid = microblaze_0_axi_periph_M01_AXI_ARVALID;
  assign M01_AXI_awaddr[31:0] = microblaze_0_axi_periph_M01_AXI_AWADDR;
  assign M01_AXI_awprot[2:0] = microblaze_0_axi_periph_M01_AXI_AWPROT;
  assign M01_AXI_awvalid = microblaze_0_axi_periph_M01_AXI_AWVALID;
  assign M01_AXI_bready = microblaze_0_axi_periph_M01_AXI_BREADY;
  assign M01_AXI_rready = microblaze_0_axi_periph_M01_AXI_RREADY;
  assign M01_AXI_wdata[31:0] = microblaze_0_axi_periph_M01_AXI_WDATA;
  assign M01_AXI_wstrb[3:0] = microblaze_0_axi_periph_M01_AXI_WSTRB;
  assign M01_AXI_wvalid = microblaze_0_axi_periph_M01_AXI_WVALID;
  assign M02_AXI_araddr[31:0] = microblaze_0_axi_periph_M02_AXI_ARADDR;
  assign M02_AXI_arprot[2:0] = microblaze_0_axi_periph_M02_AXI_ARPROT;
  assign M02_AXI_arvalid = microblaze_0_axi_periph_M02_AXI_ARVALID;
  assign M02_AXI_awaddr[31:0] = microblaze_0_axi_periph_M02_AXI_AWADDR;
  assign M02_AXI_awprot[2:0] = microblaze_0_axi_periph_M02_AXI_AWPROT;
  assign M02_AXI_awvalid = microblaze_0_axi_periph_M02_AXI_AWVALID;
  assign M02_AXI_bready = microblaze_0_axi_periph_M02_AXI_BREADY;
  assign M02_AXI_rready = microblaze_0_axi_periph_M02_AXI_RREADY;
  assign M02_AXI_wdata[31:0] = microblaze_0_axi_periph_M02_AXI_WDATA;
  assign M02_AXI_wstrb[3:0] = microblaze_0_axi_periph_M02_AXI_WSTRB;
  assign M02_AXI_wvalid = microblaze_0_axi_periph_M02_AXI_WVALID;
  assign M03_AXI_araddr[31:0] = microblaze_0_axi_periph_M03_AXI_ARADDR;
  assign M03_AXI_arvalid = microblaze_0_axi_periph_M03_AXI_ARVALID;
  assign M03_AXI_awaddr[31:0] = microblaze_0_axi_periph_M03_AXI_AWADDR;
  assign M03_AXI_awvalid = microblaze_0_axi_periph_M03_AXI_AWVALID;
  assign M03_AXI_bready = microblaze_0_axi_periph_M03_AXI_BREADY;
  assign M03_AXI_rready = microblaze_0_axi_periph_M03_AXI_RREADY;
  assign M03_AXI_wdata[31:0] = microblaze_0_axi_periph_M03_AXI_WDATA;
  assign M03_AXI_wstrb[3:0] = microblaze_0_axi_periph_M03_AXI_WSTRB;
  assign M03_AXI_wvalid = microblaze_0_axi_periph_M03_AXI_WVALID;
  assign M05_AXI_araddr[31:0] = microblaze_0_axi_periph_M05_AXI_ARADDR;
  assign M05_AXI_arvalid = microblaze_0_axi_periph_M05_AXI_ARVALID;
  assign M05_AXI_awaddr[31:0] = microblaze_0_axi_periph_M05_AXI_AWADDR;
  assign M05_AXI_awvalid = microblaze_0_axi_periph_M05_AXI_AWVALID;
  assign M05_AXI_bready = microblaze_0_axi_periph_M05_AXI_BREADY;
  assign M05_AXI_rready = microblaze_0_axi_periph_M05_AXI_RREADY;
  assign M05_AXI_wdata[31:0] = microblaze_0_axi_periph_M05_AXI_WDATA;
  assign M05_AXI_wstrb[3:0] = microblaze_0_axi_periph_M05_AXI_WSTRB;
  assign M05_AXI_wvalid = microblaze_0_axi_periph_M05_AXI_WVALID;
  assign M06_AXI_araddr[31:0] = microblaze_0_axi_periph_M06_AXI_ARADDR;
  assign M06_AXI_arprot[2:0] = microblaze_0_axi_periph_M06_AXI_ARPROT;
  assign M06_AXI_arvalid = microblaze_0_axi_periph_M06_AXI_ARVALID;
  assign M06_AXI_awaddr[31:0] = microblaze_0_axi_periph_M06_AXI_AWADDR;
  assign M06_AXI_awprot[2:0] = microblaze_0_axi_periph_M06_AXI_AWPROT;
  assign M06_AXI_awvalid = microblaze_0_axi_periph_M06_AXI_AWVALID;
  assign M06_AXI_bready = microblaze_0_axi_periph_M06_AXI_BREADY;
  assign M06_AXI_rready = microblaze_0_axi_periph_M06_AXI_RREADY;
  assign M06_AXI_wdata[31:0] = microblaze_0_axi_periph_M06_AXI_WDATA;
  assign M06_AXI_wstrb[3:0] = microblaze_0_axi_periph_M06_AXI_WSTRB;
  assign M06_AXI_wvalid = microblaze_0_axi_periph_M06_AXI_WVALID;
  assign M07_AXI_araddr[31:0] = microblaze_0_axi_periph_M07_AXI_ARADDR;
  assign M07_AXI_arprot[2:0] = microblaze_0_axi_periph_M07_AXI_ARPROT;
  assign M07_AXI_arvalid = microblaze_0_axi_periph_M07_AXI_ARVALID;
  assign M07_AXI_awaddr[31:0] = microblaze_0_axi_periph_M07_AXI_AWADDR;
  assign M07_AXI_awprot[2:0] = microblaze_0_axi_periph_M07_AXI_AWPROT;
  assign M07_AXI_awvalid = microblaze_0_axi_periph_M07_AXI_AWVALID;
  assign M07_AXI_bready = microblaze_0_axi_periph_M07_AXI_BREADY;
  assign M07_AXI_rready = microblaze_0_axi_periph_M07_AXI_RREADY;
  assign M07_AXI_wdata[31:0] = microblaze_0_axi_periph_M07_AXI_WDATA;
  assign M07_AXI_wstrb[3:0] = microblaze_0_axi_periph_M07_AXI_WSTRB;
  assign M07_AXI_wvalid = microblaze_0_axi_periph_M07_AXI_WVALID;
  assign M08_AXI_araddr[31:0] = microblaze_0_axi_periph_M08_AXI_ARADDR;
  assign M08_AXI_arprot[2:0] = microblaze_0_axi_periph_M08_AXI_ARPROT;
  assign M08_AXI_arvalid = microblaze_0_axi_periph_M08_AXI_ARVALID;
  assign M08_AXI_awaddr[31:0] = microblaze_0_axi_periph_M08_AXI_AWADDR;
  assign M08_AXI_awprot[2:0] = microblaze_0_axi_periph_M08_AXI_AWPROT;
  assign M08_AXI_awvalid = microblaze_0_axi_periph_M08_AXI_AWVALID;
  assign M08_AXI_bready = microblaze_0_axi_periph_M08_AXI_BREADY;
  assign M08_AXI_rready = microblaze_0_axi_periph_M08_AXI_RREADY;
  assign M08_AXI_wdata[31:0] = microblaze_0_axi_periph_M08_AXI_WDATA;
  assign M08_AXI_wstrb[3:0] = microblaze_0_axi_periph_M08_AXI_WSTRB;
  assign M08_AXI_wvalid = microblaze_0_axi_periph_M08_AXI_WVALID;
  assign M09_AXI_araddr[12:0] = microblaze_0_axi_periph_M09_AXI_ARADDR;
  assign M09_AXI_arburst[1:0] = microblaze_0_axi_periph_M09_AXI_ARBURST;
  assign M09_AXI_arcache[3:0] = microblaze_0_axi_periph_M09_AXI_ARCACHE;
  assign M09_AXI_arlen[7:0] = microblaze_0_axi_periph_M09_AXI_ARLEN;
  assign M09_AXI_arlock = microblaze_0_axi_periph_M09_AXI_ARLOCK;
  assign M09_AXI_arprot[2:0] = microblaze_0_axi_periph_M09_AXI_ARPROT;
  assign M09_AXI_arsize[2:0] = microblaze_0_axi_periph_M09_AXI_ARSIZE;
  assign M09_AXI_arvalid = microblaze_0_axi_periph_M09_AXI_ARVALID;
  assign M09_AXI_awaddr[12:0] = microblaze_0_axi_periph_M09_AXI_AWADDR;
  assign M09_AXI_awburst[1:0] = microblaze_0_axi_periph_M09_AXI_AWBURST;
  assign M09_AXI_awcache[3:0] = microblaze_0_axi_periph_M09_AXI_AWCACHE;
  assign M09_AXI_awlen[7:0] = microblaze_0_axi_periph_M09_AXI_AWLEN;
  assign M09_AXI_awlock = microblaze_0_axi_periph_M09_AXI_AWLOCK;
  assign M09_AXI_awprot[2:0] = microblaze_0_axi_periph_M09_AXI_AWPROT;
  assign M09_AXI_awsize[2:0] = microblaze_0_axi_periph_M09_AXI_AWSIZE;
  assign M09_AXI_awvalid = microblaze_0_axi_periph_M09_AXI_AWVALID;
  assign M09_AXI_bready = microblaze_0_axi_periph_M09_AXI_BREADY;
  assign M09_AXI_rready = microblaze_0_axi_periph_M09_AXI_RREADY;
  assign M09_AXI_wdata[31:0] = microblaze_0_axi_periph_M09_AXI_WDATA;
  assign M09_AXI_wlast = microblaze_0_axi_periph_M09_AXI_WLAST;
  assign M09_AXI_wstrb[3:0] = microblaze_0_axi_periph_M09_AXI_WSTRB;
  assign M09_AXI_wvalid = microblaze_0_axi_periph_M09_AXI_WVALID;
  assign M10_AXI_araddr[31:0] = microblaze_0_axi_periph_M10_AXI_ARADDR;
  assign M10_AXI_arprot[2:0] = microblaze_0_axi_periph_M10_AXI_ARPROT;
  assign M10_AXI_arvalid = microblaze_0_axi_periph_M10_AXI_ARVALID;
  assign M10_AXI_awaddr[31:0] = microblaze_0_axi_periph_M10_AXI_AWADDR;
  assign M10_AXI_awprot[2:0] = microblaze_0_axi_periph_M10_AXI_AWPROT;
  assign M10_AXI_awvalid = microblaze_0_axi_periph_M10_AXI_AWVALID;
  assign M10_AXI_bready = microblaze_0_axi_periph_M10_AXI_BREADY;
  assign M10_AXI_rready = microblaze_0_axi_periph_M10_AXI_RREADY;
  assign M10_AXI_wdata[31:0] = microblaze_0_axi_periph_M10_AXI_WDATA;
  assign M10_AXI_wstrb[3:0] = microblaze_0_axi_periph_M10_AXI_WSTRB;
  assign M10_AXI_wvalid = microblaze_0_axi_periph_M10_AXI_WVALID;
  assign M11_AXI_araddr[31:0] = microblaze_0_axi_periph_M11_AXI_ARADDR;
  assign M11_AXI_arvalid = microblaze_0_axi_periph_M11_AXI_ARVALID;
  assign M11_AXI_awaddr[31:0] = microblaze_0_axi_periph_M11_AXI_AWADDR;
  assign M11_AXI_awvalid = microblaze_0_axi_periph_M11_AXI_AWVALID;
  assign M11_AXI_bready = microblaze_0_axi_periph_M11_AXI_BREADY;
  assign M11_AXI_rready = microblaze_0_axi_periph_M11_AXI_RREADY;
  assign M11_AXI_wdata[31:0] = microblaze_0_axi_periph_M11_AXI_WDATA;
  assign M11_AXI_wstrb[3:0] = microblaze_0_axi_periph_M11_AXI_WSTRB;
  assign M11_AXI_wvalid = microblaze_0_axi_periph_M11_AXI_WVALID;
  assign S00_ARESETN[0] = rst_clk_wiz_0_120M_peripheral_aresetn;
  assign clk_wiz_0_locked = dcm_locked;
  assign intr_1 = intr[0];
  assign microblaze_0_Clk = Clk;
  assign microblaze_0_axi_periph_M00_AXI_ARREADY = M00_AXI_arready;
  assign microblaze_0_axi_periph_M00_AXI_AWREADY = M00_AXI_awready;
  assign microblaze_0_axi_periph_M00_AXI_BRESP = M00_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M00_AXI_BVALID = M00_AXI_bvalid;
  assign microblaze_0_axi_periph_M00_AXI_RDATA = M00_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M00_AXI_RRESP = M00_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M00_AXI_RVALID = M00_AXI_rvalid;
  assign microblaze_0_axi_periph_M00_AXI_WREADY = M00_AXI_wready;
  assign microblaze_0_axi_periph_M01_AXI_ARREADY = M01_AXI_arready;
  assign microblaze_0_axi_periph_M01_AXI_AWREADY = M01_AXI_awready;
  assign microblaze_0_axi_periph_M01_AXI_BRESP = M01_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M01_AXI_BVALID = M01_AXI_bvalid;
  assign microblaze_0_axi_periph_M01_AXI_RDATA = M01_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M01_AXI_RRESP = M01_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M01_AXI_RVALID = M01_AXI_rvalid;
  assign microblaze_0_axi_periph_M01_AXI_WREADY = M01_AXI_wready;
  assign microblaze_0_axi_periph_M02_AXI_ARREADY = M02_AXI_arready;
  assign microblaze_0_axi_periph_M02_AXI_AWREADY = M02_AXI_awready;
  assign microblaze_0_axi_periph_M02_AXI_BRESP = M02_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M02_AXI_BVALID = M02_AXI_bvalid;
  assign microblaze_0_axi_periph_M02_AXI_RDATA = M02_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M02_AXI_RRESP = M02_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M02_AXI_RVALID = M02_AXI_rvalid;
  assign microblaze_0_axi_periph_M02_AXI_WREADY = M02_AXI_wready;
  assign microblaze_0_axi_periph_M03_AXI_ARREADY = M03_AXI_arready;
  assign microblaze_0_axi_periph_M03_AXI_AWREADY = M03_AXI_awready;
  assign microblaze_0_axi_periph_M03_AXI_BRESP = M03_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M03_AXI_BVALID = M03_AXI_bvalid;
  assign microblaze_0_axi_periph_M03_AXI_RDATA = M03_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M03_AXI_RRESP = M03_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M03_AXI_RVALID = M03_AXI_rvalid;
  assign microblaze_0_axi_periph_M03_AXI_WREADY = M03_AXI_wready;
  assign microblaze_0_axi_periph_M05_AXI_ARREADY = M05_AXI_arready;
  assign microblaze_0_axi_periph_M05_AXI_AWREADY = M05_AXI_awready;
  assign microblaze_0_axi_periph_M05_AXI_BRESP = M05_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M05_AXI_BVALID = M05_AXI_bvalid;
  assign microblaze_0_axi_periph_M05_AXI_RDATA = M05_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M05_AXI_RRESP = M05_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M05_AXI_RVALID = M05_AXI_rvalid;
  assign microblaze_0_axi_periph_M05_AXI_WREADY = M05_AXI_wready;
  assign microblaze_0_axi_periph_M06_AXI_ARREADY = M06_AXI_arready;
  assign microblaze_0_axi_periph_M06_AXI_AWREADY = M06_AXI_awready;
  assign microblaze_0_axi_periph_M06_AXI_BRESP = M06_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M06_AXI_BVALID = M06_AXI_bvalid;
  assign microblaze_0_axi_periph_M06_AXI_RDATA = M06_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M06_AXI_RRESP = M06_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M06_AXI_RVALID = M06_AXI_rvalid;
  assign microblaze_0_axi_periph_M06_AXI_WREADY = M06_AXI_wready;
  assign microblaze_0_axi_periph_M07_AXI_ARREADY = M07_AXI_arready;
  assign microblaze_0_axi_periph_M07_AXI_AWREADY = M07_AXI_awready;
  assign microblaze_0_axi_periph_M07_AXI_BRESP = M07_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M07_AXI_BVALID = M07_AXI_bvalid;
  assign microblaze_0_axi_periph_M07_AXI_RDATA = M07_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M07_AXI_RRESP = M07_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M07_AXI_RVALID = M07_AXI_rvalid;
  assign microblaze_0_axi_periph_M07_AXI_WREADY = M07_AXI_wready;
  assign microblaze_0_axi_periph_M08_AXI_ARREADY = M08_AXI_arready;
  assign microblaze_0_axi_periph_M08_AXI_AWREADY = M08_AXI_awready;
  assign microblaze_0_axi_periph_M08_AXI_BRESP = M08_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M08_AXI_BVALID = M08_AXI_bvalid;
  assign microblaze_0_axi_periph_M08_AXI_RDATA = M08_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M08_AXI_RRESP = M08_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M08_AXI_RVALID = M08_AXI_rvalid;
  assign microblaze_0_axi_periph_M08_AXI_WREADY = M08_AXI_wready;
  assign microblaze_0_axi_periph_M09_AXI_ARREADY = M09_AXI_arready;
  assign microblaze_0_axi_periph_M09_AXI_AWREADY = M09_AXI_awready;
  assign microblaze_0_axi_periph_M09_AXI_BRESP = M09_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M09_AXI_BVALID = M09_AXI_bvalid;
  assign microblaze_0_axi_periph_M09_AXI_RDATA = M09_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M09_AXI_RLAST = M09_AXI_rlast;
  assign microblaze_0_axi_periph_M09_AXI_RRESP = M09_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M09_AXI_RVALID = M09_AXI_rvalid;
  assign microblaze_0_axi_periph_M09_AXI_WREADY = M09_AXI_wready;
  assign microblaze_0_axi_periph_M10_AXI_ARREADY = M10_AXI_arready;
  assign microblaze_0_axi_periph_M10_AXI_AWREADY = M10_AXI_awready;
  assign microblaze_0_axi_periph_M10_AXI_BRESP = M10_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M10_AXI_BVALID = M10_AXI_bvalid;
  assign microblaze_0_axi_periph_M10_AXI_RDATA = M10_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M10_AXI_RRESP = M10_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M10_AXI_RVALID = M10_AXI_rvalid;
  assign microblaze_0_axi_periph_M10_AXI_WREADY = M10_AXI_wready;
  assign microblaze_0_axi_periph_M11_AXI_ARREADY = M11_AXI_arready;
  assign microblaze_0_axi_periph_M11_AXI_AWREADY = M11_AXI_awready;
  assign microblaze_0_axi_periph_M11_AXI_BRESP = M11_AXI_bresp[1:0];
  assign microblaze_0_axi_periph_M11_AXI_BVALID = M11_AXI_bvalid;
  assign microblaze_0_axi_periph_M11_AXI_RDATA = M11_AXI_rdata[31:0];
  assign microblaze_0_axi_periph_M11_AXI_RRESP = M11_AXI_rresp[1:0];
  assign microblaze_0_axi_periph_M11_AXI_RVALID = M11_AXI_rvalid;
  assign microblaze_0_axi_periph_M11_AXI_WREADY = M11_AXI_wready;
  assign reset_1 = reset;
  design_1_axi_intc_0_0 axi_intc_0
       (.intr(intr_1),
        .irq(axi_intc_0_interrupt_INTERRUPT),
        .s_axi_aclk(microblaze_0_Clk),
        .s_axi_araddr(microblaze_0_axi_periph_M04_AXI_ARADDR[8:0]),
        .s_axi_aresetn(rst_clk_wiz_0_120M_peripheral_aresetn),
        .s_axi_arready(microblaze_0_axi_periph_M04_AXI_ARREADY),
        .s_axi_arvalid(microblaze_0_axi_periph_M04_AXI_ARVALID),
        .s_axi_awaddr(microblaze_0_axi_periph_M04_AXI_AWADDR[8:0]),
        .s_axi_awready(microblaze_0_axi_periph_M04_AXI_AWREADY),
        .s_axi_awvalid(microblaze_0_axi_periph_M04_AXI_AWVALID),
        .s_axi_bready(microblaze_0_axi_periph_M04_AXI_BREADY),
        .s_axi_bresp(microblaze_0_axi_periph_M04_AXI_BRESP),
        .s_axi_bvalid(microblaze_0_axi_periph_M04_AXI_BVALID),
        .s_axi_rdata(microblaze_0_axi_periph_M04_AXI_RDATA),
        .s_axi_rready(microblaze_0_axi_periph_M04_AXI_RREADY),
        .s_axi_rresp(microblaze_0_axi_periph_M04_AXI_RRESP),
        .s_axi_rvalid(microblaze_0_axi_periph_M04_AXI_RVALID),
        .s_axi_wdata(microblaze_0_axi_periph_M04_AXI_WDATA),
        .s_axi_wready(microblaze_0_axi_periph_M04_AXI_WREADY),
        .s_axi_wstrb(microblaze_0_axi_periph_M04_AXI_WSTRB),
        .s_axi_wvalid(microblaze_0_axi_periph_M04_AXI_WVALID));
  design_1_mdm_1_0 mdm_1
       (.Dbg_Capture_0(microblaze_0_debug_CAPTURE),
        .Dbg_Clk_0(microblaze_0_debug_CLK),
        .Dbg_Disable_0(microblaze_0_debug_DISABLE),
        .Dbg_Reg_En_0(microblaze_0_debug_REG_EN),
        .Dbg_Rst_0(microblaze_0_debug_RST),
        .Dbg_Shift_0(microblaze_0_debug_SHIFT),
        .Dbg_TDI_0(microblaze_0_debug_TDI),
        .Dbg_TDO_0(microblaze_0_debug_TDO),
        .Dbg_Update_0(microblaze_0_debug_UPDATE),
        .Debug_SYS_Rst(mdm_1_debug_sys_rst));
  (* BMM_INFO_PROCESSOR = "microblaze-le > design_1 ps_mb_0/microblaze_0_local_memory/dlmb_bram_if_cntlr design_1 dpp_0/pha/mca/axi_bram_ctrl_0" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  design_1_microblaze_0_0 microblaze_0
       (.Byte_Enable(microblaze_0_dlmb_1_BE),
        .Clk(microblaze_0_Clk),
        .DCE(microblaze_0_dlmb_1_CE),
        .DReady(microblaze_0_dlmb_1_READY),
        .DUE(microblaze_0_dlmb_1_UE),
        .DWait(microblaze_0_dlmb_1_WAIT),
        .D_AS(microblaze_0_dlmb_1_ADDRSTROBE),
        .Data_Addr(microblaze_0_dlmb_1_ABUS),
        .Data_Read(microblaze_0_dlmb_1_READDBUS),
        .Data_Write(microblaze_0_dlmb_1_WRITEDBUS),
        .Dbg_Capture(microblaze_0_debug_CAPTURE),
        .Dbg_Clk(microblaze_0_debug_CLK),
        .Dbg_Disable(microblaze_0_debug_DISABLE),
        .Dbg_Reg_En(microblaze_0_debug_REG_EN),
        .Dbg_Shift(microblaze_0_debug_SHIFT),
        .Dbg_TDI(microblaze_0_debug_TDI),
        .Dbg_TDO(microblaze_0_debug_TDO),
        .Dbg_Update(microblaze_0_debug_UPDATE),
        .Debug_Rst(microblaze_0_debug_RST),
        .ICE(microblaze_0_ilmb_1_CE),
        .IFetch(microblaze_0_ilmb_1_READSTROBE),
        .IReady(microblaze_0_ilmb_1_READY),
        .IUE(microblaze_0_ilmb_1_UE),
        .IWAIT(microblaze_0_ilmb_1_WAIT),
        .I_AS(microblaze_0_ilmb_1_ADDRSTROBE),
        .Instr(microblaze_0_ilmb_1_READDBUS),
        .Instr_Addr(microblaze_0_ilmb_1_ABUS),
        .Interrupt(axi_intc_0_interrupt_INTERRUPT),
        .Interrupt_Address({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .M_AXI_DP_ARADDR(microblaze_0_M_AXI_DP_ARADDR),
        .M_AXI_DP_ARPROT(microblaze_0_M_AXI_DP_ARPROT),
        .M_AXI_DP_ARREADY(microblaze_0_M_AXI_DP_ARREADY),
        .M_AXI_DP_ARVALID(microblaze_0_M_AXI_DP_ARVALID),
        .M_AXI_DP_AWADDR(microblaze_0_M_AXI_DP_AWADDR),
        .M_AXI_DP_AWPROT(microblaze_0_M_AXI_DP_AWPROT),
        .M_AXI_DP_AWREADY(microblaze_0_M_AXI_DP_AWREADY),
        .M_AXI_DP_AWVALID(microblaze_0_M_AXI_DP_AWVALID),
        .M_AXI_DP_BREADY(microblaze_0_M_AXI_DP_BREADY),
        .M_AXI_DP_BRESP(microblaze_0_M_AXI_DP_BRESP),
        .M_AXI_DP_BVALID(microblaze_0_M_AXI_DP_BVALID),
        .M_AXI_DP_RDATA(microblaze_0_M_AXI_DP_RDATA),
        .M_AXI_DP_RREADY(microblaze_0_M_AXI_DP_RREADY),
        .M_AXI_DP_RRESP(microblaze_0_M_AXI_DP_RRESP),
        .M_AXI_DP_RVALID(microblaze_0_M_AXI_DP_RVALID),
        .M_AXI_DP_WDATA(microblaze_0_M_AXI_DP_WDATA),
        .M_AXI_DP_WREADY(microblaze_0_M_AXI_DP_WREADY),
        .M_AXI_DP_WSTRB(microblaze_0_M_AXI_DP_WSTRB),
        .M_AXI_DP_WVALID(microblaze_0_M_AXI_DP_WVALID),
        .Read_Strobe(microblaze_0_dlmb_1_READSTROBE),
        .Reset(rst_clk_wiz_0_120M_mb_reset),
        .Write_Strobe(microblaze_0_dlmb_1_WRITESTROBE));
  design_1_microblaze_0_axi_periph_0 microblaze_0_axi_periph
       (.ACLK(microblaze_0_Clk),
        .ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M00_ACLK(microblaze_0_Clk),
        .M00_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M00_AXI_araddr(microblaze_0_axi_periph_M00_AXI_ARADDR),
        .M00_AXI_arready(microblaze_0_axi_periph_M00_AXI_ARREADY),
        .M00_AXI_arvalid(microblaze_0_axi_periph_M00_AXI_ARVALID),
        .M00_AXI_awaddr(microblaze_0_axi_periph_M00_AXI_AWADDR),
        .M00_AXI_awready(microblaze_0_axi_periph_M00_AXI_AWREADY),
        .M00_AXI_awvalid(microblaze_0_axi_periph_M00_AXI_AWVALID),
        .M00_AXI_bready(microblaze_0_axi_periph_M00_AXI_BREADY),
        .M00_AXI_bresp(microblaze_0_axi_periph_M00_AXI_BRESP),
        .M00_AXI_bvalid(microblaze_0_axi_periph_M00_AXI_BVALID),
        .M00_AXI_rdata(microblaze_0_axi_periph_M00_AXI_RDATA),
        .M00_AXI_rready(microblaze_0_axi_periph_M00_AXI_RREADY),
        .M00_AXI_rresp(microblaze_0_axi_periph_M00_AXI_RRESP),
        .M00_AXI_rvalid(microblaze_0_axi_periph_M00_AXI_RVALID),
        .M00_AXI_wdata(microblaze_0_axi_periph_M00_AXI_WDATA),
        .M00_AXI_wready(microblaze_0_axi_periph_M00_AXI_WREADY),
        .M00_AXI_wstrb(microblaze_0_axi_periph_M00_AXI_WSTRB),
        .M00_AXI_wvalid(microblaze_0_axi_periph_M00_AXI_WVALID),
        .M01_ACLK(microblaze_0_Clk),
        .M01_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M01_AXI_araddr(microblaze_0_axi_periph_M01_AXI_ARADDR),
        .M01_AXI_arprot(microblaze_0_axi_periph_M01_AXI_ARPROT),
        .M01_AXI_arready(microblaze_0_axi_periph_M01_AXI_ARREADY),
        .M01_AXI_arvalid(microblaze_0_axi_periph_M01_AXI_ARVALID),
        .M01_AXI_awaddr(microblaze_0_axi_periph_M01_AXI_AWADDR),
        .M01_AXI_awprot(microblaze_0_axi_periph_M01_AXI_AWPROT),
        .M01_AXI_awready(microblaze_0_axi_periph_M01_AXI_AWREADY),
        .M01_AXI_awvalid(microblaze_0_axi_periph_M01_AXI_AWVALID),
        .M01_AXI_bready(microblaze_0_axi_periph_M01_AXI_BREADY),
        .M01_AXI_bresp(microblaze_0_axi_periph_M01_AXI_BRESP),
        .M01_AXI_bvalid(microblaze_0_axi_periph_M01_AXI_BVALID),
        .M01_AXI_rdata(microblaze_0_axi_periph_M01_AXI_RDATA),
        .M01_AXI_rready(microblaze_0_axi_periph_M01_AXI_RREADY),
        .M01_AXI_rresp(microblaze_0_axi_periph_M01_AXI_RRESP),
        .M01_AXI_rvalid(microblaze_0_axi_periph_M01_AXI_RVALID),
        .M01_AXI_wdata(microblaze_0_axi_periph_M01_AXI_WDATA),
        .M01_AXI_wready(microblaze_0_axi_periph_M01_AXI_WREADY),
        .M01_AXI_wstrb(microblaze_0_axi_periph_M01_AXI_WSTRB),
        .M01_AXI_wvalid(microblaze_0_axi_periph_M01_AXI_WVALID),
        .M02_ACLK(microblaze_0_Clk),
        .M02_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M02_AXI_araddr(microblaze_0_axi_periph_M02_AXI_ARADDR),
        .M02_AXI_arprot(microblaze_0_axi_periph_M02_AXI_ARPROT),
        .M02_AXI_arready(microblaze_0_axi_periph_M02_AXI_ARREADY),
        .M02_AXI_arvalid(microblaze_0_axi_periph_M02_AXI_ARVALID),
        .M02_AXI_awaddr(microblaze_0_axi_periph_M02_AXI_AWADDR),
        .M02_AXI_awprot(microblaze_0_axi_periph_M02_AXI_AWPROT),
        .M02_AXI_awready(microblaze_0_axi_periph_M02_AXI_AWREADY),
        .M02_AXI_awvalid(microblaze_0_axi_periph_M02_AXI_AWVALID),
        .M02_AXI_bready(microblaze_0_axi_periph_M02_AXI_BREADY),
        .M02_AXI_bresp(microblaze_0_axi_periph_M02_AXI_BRESP),
        .M02_AXI_bvalid(microblaze_0_axi_periph_M02_AXI_BVALID),
        .M02_AXI_rdata(microblaze_0_axi_periph_M02_AXI_RDATA),
        .M02_AXI_rready(microblaze_0_axi_periph_M02_AXI_RREADY),
        .M02_AXI_rresp(microblaze_0_axi_periph_M02_AXI_RRESP),
        .M02_AXI_rvalid(microblaze_0_axi_periph_M02_AXI_RVALID),
        .M02_AXI_wdata(microblaze_0_axi_periph_M02_AXI_WDATA),
        .M02_AXI_wready(microblaze_0_axi_periph_M02_AXI_WREADY),
        .M02_AXI_wstrb(microblaze_0_axi_periph_M02_AXI_WSTRB),
        .M02_AXI_wvalid(microblaze_0_axi_periph_M02_AXI_WVALID),
        .M03_ACLK(microblaze_0_Clk),
        .M03_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M03_AXI_araddr(microblaze_0_axi_periph_M03_AXI_ARADDR),
        .M03_AXI_arready(microblaze_0_axi_periph_M03_AXI_ARREADY),
        .M03_AXI_arvalid(microblaze_0_axi_periph_M03_AXI_ARVALID),
        .M03_AXI_awaddr(microblaze_0_axi_periph_M03_AXI_AWADDR),
        .M03_AXI_awready(microblaze_0_axi_periph_M03_AXI_AWREADY),
        .M03_AXI_awvalid(microblaze_0_axi_periph_M03_AXI_AWVALID),
        .M03_AXI_bready(microblaze_0_axi_periph_M03_AXI_BREADY),
        .M03_AXI_bresp(microblaze_0_axi_periph_M03_AXI_BRESP),
        .M03_AXI_bvalid(microblaze_0_axi_periph_M03_AXI_BVALID),
        .M03_AXI_rdata(microblaze_0_axi_periph_M03_AXI_RDATA),
        .M03_AXI_rready(microblaze_0_axi_periph_M03_AXI_RREADY),
        .M03_AXI_rresp(microblaze_0_axi_periph_M03_AXI_RRESP),
        .M03_AXI_rvalid(microblaze_0_axi_periph_M03_AXI_RVALID),
        .M03_AXI_wdata(microblaze_0_axi_periph_M03_AXI_WDATA),
        .M03_AXI_wready(microblaze_0_axi_periph_M03_AXI_WREADY),
        .M03_AXI_wstrb(microblaze_0_axi_periph_M03_AXI_WSTRB),
        .M03_AXI_wvalid(microblaze_0_axi_periph_M03_AXI_WVALID),
        .M04_ACLK(microblaze_0_Clk),
        .M04_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M04_AXI_araddr(microblaze_0_axi_periph_M04_AXI_ARADDR),
        .M04_AXI_arready(microblaze_0_axi_periph_M04_AXI_ARREADY),
        .M04_AXI_arvalid(microblaze_0_axi_periph_M04_AXI_ARVALID),
        .M04_AXI_awaddr(microblaze_0_axi_periph_M04_AXI_AWADDR),
        .M04_AXI_awready(microblaze_0_axi_periph_M04_AXI_AWREADY),
        .M04_AXI_awvalid(microblaze_0_axi_periph_M04_AXI_AWVALID),
        .M04_AXI_bready(microblaze_0_axi_periph_M04_AXI_BREADY),
        .M04_AXI_bresp(microblaze_0_axi_periph_M04_AXI_BRESP),
        .M04_AXI_bvalid(microblaze_0_axi_periph_M04_AXI_BVALID),
        .M04_AXI_rdata(microblaze_0_axi_periph_M04_AXI_RDATA),
        .M04_AXI_rready(microblaze_0_axi_periph_M04_AXI_RREADY),
        .M04_AXI_rresp(microblaze_0_axi_periph_M04_AXI_RRESP),
        .M04_AXI_rvalid(microblaze_0_axi_periph_M04_AXI_RVALID),
        .M04_AXI_wdata(microblaze_0_axi_periph_M04_AXI_WDATA),
        .M04_AXI_wready(microblaze_0_axi_periph_M04_AXI_WREADY),
        .M04_AXI_wstrb(microblaze_0_axi_periph_M04_AXI_WSTRB),
        .M04_AXI_wvalid(microblaze_0_axi_periph_M04_AXI_WVALID),
        .M05_ACLK(microblaze_0_Clk),
        .M05_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M05_AXI_araddr(microblaze_0_axi_periph_M05_AXI_ARADDR),
        .M05_AXI_arready(microblaze_0_axi_periph_M05_AXI_ARREADY),
        .M05_AXI_arvalid(microblaze_0_axi_periph_M05_AXI_ARVALID),
        .M05_AXI_awaddr(microblaze_0_axi_periph_M05_AXI_AWADDR),
        .M05_AXI_awready(microblaze_0_axi_periph_M05_AXI_AWREADY),
        .M05_AXI_awvalid(microblaze_0_axi_periph_M05_AXI_AWVALID),
        .M05_AXI_bready(microblaze_0_axi_periph_M05_AXI_BREADY),
        .M05_AXI_bresp(microblaze_0_axi_periph_M05_AXI_BRESP),
        .M05_AXI_bvalid(microblaze_0_axi_periph_M05_AXI_BVALID),
        .M05_AXI_rdata(microblaze_0_axi_periph_M05_AXI_RDATA),
        .M05_AXI_rready(microblaze_0_axi_periph_M05_AXI_RREADY),
        .M05_AXI_rresp(microblaze_0_axi_periph_M05_AXI_RRESP),
        .M05_AXI_rvalid(microblaze_0_axi_periph_M05_AXI_RVALID),
        .M05_AXI_wdata(microblaze_0_axi_periph_M05_AXI_WDATA),
        .M05_AXI_wready(microblaze_0_axi_periph_M05_AXI_WREADY),
        .M05_AXI_wstrb(microblaze_0_axi_periph_M05_AXI_WSTRB),
        .M05_AXI_wvalid(microblaze_0_axi_periph_M05_AXI_WVALID),
        .M06_ACLK(microblaze_0_Clk),
        .M06_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M06_AXI_araddr(microblaze_0_axi_periph_M06_AXI_ARADDR),
        .M06_AXI_arprot(microblaze_0_axi_periph_M06_AXI_ARPROT),
        .M06_AXI_arready(microblaze_0_axi_periph_M06_AXI_ARREADY),
        .M06_AXI_arvalid(microblaze_0_axi_periph_M06_AXI_ARVALID),
        .M06_AXI_awaddr(microblaze_0_axi_periph_M06_AXI_AWADDR),
        .M06_AXI_awprot(microblaze_0_axi_periph_M06_AXI_AWPROT),
        .M06_AXI_awready(microblaze_0_axi_periph_M06_AXI_AWREADY),
        .M06_AXI_awvalid(microblaze_0_axi_periph_M06_AXI_AWVALID),
        .M06_AXI_bready(microblaze_0_axi_periph_M06_AXI_BREADY),
        .M06_AXI_bresp(microblaze_0_axi_periph_M06_AXI_BRESP),
        .M06_AXI_bvalid(microblaze_0_axi_periph_M06_AXI_BVALID),
        .M06_AXI_rdata(microblaze_0_axi_periph_M06_AXI_RDATA),
        .M06_AXI_rready(microblaze_0_axi_periph_M06_AXI_RREADY),
        .M06_AXI_rresp(microblaze_0_axi_periph_M06_AXI_RRESP),
        .M06_AXI_rvalid(microblaze_0_axi_periph_M06_AXI_RVALID),
        .M06_AXI_wdata(microblaze_0_axi_periph_M06_AXI_WDATA),
        .M06_AXI_wready(microblaze_0_axi_periph_M06_AXI_WREADY),
        .M06_AXI_wstrb(microblaze_0_axi_periph_M06_AXI_WSTRB),
        .M06_AXI_wvalid(microblaze_0_axi_periph_M06_AXI_WVALID),
        .M07_ACLK(microblaze_0_Clk),
        .M07_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M07_AXI_araddr(microblaze_0_axi_periph_M07_AXI_ARADDR),
        .M07_AXI_arprot(microblaze_0_axi_periph_M07_AXI_ARPROT),
        .M07_AXI_arready(microblaze_0_axi_periph_M07_AXI_ARREADY),
        .M07_AXI_arvalid(microblaze_0_axi_periph_M07_AXI_ARVALID),
        .M07_AXI_awaddr(microblaze_0_axi_periph_M07_AXI_AWADDR),
        .M07_AXI_awprot(microblaze_0_axi_periph_M07_AXI_AWPROT),
        .M07_AXI_awready(microblaze_0_axi_periph_M07_AXI_AWREADY),
        .M07_AXI_awvalid(microblaze_0_axi_periph_M07_AXI_AWVALID),
        .M07_AXI_bready(microblaze_0_axi_periph_M07_AXI_BREADY),
        .M07_AXI_bresp(microblaze_0_axi_periph_M07_AXI_BRESP),
        .M07_AXI_bvalid(microblaze_0_axi_periph_M07_AXI_BVALID),
        .M07_AXI_rdata(microblaze_0_axi_periph_M07_AXI_RDATA),
        .M07_AXI_rready(microblaze_0_axi_periph_M07_AXI_RREADY),
        .M07_AXI_rresp(microblaze_0_axi_periph_M07_AXI_RRESP),
        .M07_AXI_rvalid(microblaze_0_axi_periph_M07_AXI_RVALID),
        .M07_AXI_wdata(microblaze_0_axi_periph_M07_AXI_WDATA),
        .M07_AXI_wready(microblaze_0_axi_periph_M07_AXI_WREADY),
        .M07_AXI_wstrb(microblaze_0_axi_periph_M07_AXI_WSTRB),
        .M07_AXI_wvalid(microblaze_0_axi_periph_M07_AXI_WVALID),
        .M08_ACLK(microblaze_0_Clk),
        .M08_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M08_AXI_araddr(microblaze_0_axi_periph_M08_AXI_ARADDR),
        .M08_AXI_arprot(microblaze_0_axi_periph_M08_AXI_ARPROT),
        .M08_AXI_arready(microblaze_0_axi_periph_M08_AXI_ARREADY),
        .M08_AXI_arvalid(microblaze_0_axi_periph_M08_AXI_ARVALID),
        .M08_AXI_awaddr(microblaze_0_axi_periph_M08_AXI_AWADDR),
        .M08_AXI_awprot(microblaze_0_axi_periph_M08_AXI_AWPROT),
        .M08_AXI_awready(microblaze_0_axi_periph_M08_AXI_AWREADY),
        .M08_AXI_awvalid(microblaze_0_axi_periph_M08_AXI_AWVALID),
        .M08_AXI_bready(microblaze_0_axi_periph_M08_AXI_BREADY),
        .M08_AXI_bresp(microblaze_0_axi_periph_M08_AXI_BRESP),
        .M08_AXI_bvalid(microblaze_0_axi_periph_M08_AXI_BVALID),
        .M08_AXI_rdata(microblaze_0_axi_periph_M08_AXI_RDATA),
        .M08_AXI_rready(microblaze_0_axi_periph_M08_AXI_RREADY),
        .M08_AXI_rresp(microblaze_0_axi_periph_M08_AXI_RRESP),
        .M08_AXI_rvalid(microblaze_0_axi_periph_M08_AXI_RVALID),
        .M08_AXI_wdata(microblaze_0_axi_periph_M08_AXI_WDATA),
        .M08_AXI_wready(microblaze_0_axi_periph_M08_AXI_WREADY),
        .M08_AXI_wstrb(microblaze_0_axi_periph_M08_AXI_WSTRB),
        .M08_AXI_wvalid(microblaze_0_axi_periph_M08_AXI_WVALID),
        .M09_ACLK(microblaze_0_Clk),
        .M09_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M09_AXI_araddr(microblaze_0_axi_periph_M09_AXI_ARADDR),
        .M09_AXI_arburst(microblaze_0_axi_periph_M09_AXI_ARBURST),
        .M09_AXI_arcache(microblaze_0_axi_periph_M09_AXI_ARCACHE),
        .M09_AXI_arlen(microblaze_0_axi_periph_M09_AXI_ARLEN),
        .M09_AXI_arlock(microblaze_0_axi_periph_M09_AXI_ARLOCK),
        .M09_AXI_arprot(microblaze_0_axi_periph_M09_AXI_ARPROT),
        .M09_AXI_arready(microblaze_0_axi_periph_M09_AXI_ARREADY),
        .M09_AXI_arsize(microblaze_0_axi_periph_M09_AXI_ARSIZE),
        .M09_AXI_arvalid(microblaze_0_axi_periph_M09_AXI_ARVALID),
        .M09_AXI_awaddr(microblaze_0_axi_periph_M09_AXI_AWADDR),
        .M09_AXI_awburst(microblaze_0_axi_periph_M09_AXI_AWBURST),
        .M09_AXI_awcache(microblaze_0_axi_periph_M09_AXI_AWCACHE),
        .M09_AXI_awlen(microblaze_0_axi_periph_M09_AXI_AWLEN),
        .M09_AXI_awlock(microblaze_0_axi_periph_M09_AXI_AWLOCK),
        .M09_AXI_awprot(microblaze_0_axi_periph_M09_AXI_AWPROT),
        .M09_AXI_awready(microblaze_0_axi_periph_M09_AXI_AWREADY),
        .M09_AXI_awsize(microblaze_0_axi_periph_M09_AXI_AWSIZE),
        .M09_AXI_awvalid(microblaze_0_axi_periph_M09_AXI_AWVALID),
        .M09_AXI_bready(microblaze_0_axi_periph_M09_AXI_BREADY),
        .M09_AXI_bresp(microblaze_0_axi_periph_M09_AXI_BRESP),
        .M09_AXI_bvalid(microblaze_0_axi_periph_M09_AXI_BVALID),
        .M09_AXI_rdata(microblaze_0_axi_periph_M09_AXI_RDATA),
        .M09_AXI_rlast(microblaze_0_axi_periph_M09_AXI_RLAST),
        .M09_AXI_rready(microblaze_0_axi_periph_M09_AXI_RREADY),
        .M09_AXI_rresp(microblaze_0_axi_periph_M09_AXI_RRESP),
        .M09_AXI_rvalid(microblaze_0_axi_periph_M09_AXI_RVALID),
        .M09_AXI_wdata(microblaze_0_axi_periph_M09_AXI_WDATA),
        .M09_AXI_wlast(microblaze_0_axi_periph_M09_AXI_WLAST),
        .M09_AXI_wready(microblaze_0_axi_periph_M09_AXI_WREADY),
        .M09_AXI_wstrb(microblaze_0_axi_periph_M09_AXI_WSTRB),
        .M09_AXI_wvalid(microblaze_0_axi_periph_M09_AXI_WVALID),
        .M10_ACLK(microblaze_0_Clk),
        .M10_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M10_AXI_araddr(microblaze_0_axi_periph_M10_AXI_ARADDR),
        .M10_AXI_arprot(microblaze_0_axi_periph_M10_AXI_ARPROT),
        .M10_AXI_arready(microblaze_0_axi_periph_M10_AXI_ARREADY),
        .M10_AXI_arvalid(microblaze_0_axi_periph_M10_AXI_ARVALID),
        .M10_AXI_awaddr(microblaze_0_axi_periph_M10_AXI_AWADDR),
        .M10_AXI_awprot(microblaze_0_axi_periph_M10_AXI_AWPROT),
        .M10_AXI_awready(microblaze_0_axi_periph_M10_AXI_AWREADY),
        .M10_AXI_awvalid(microblaze_0_axi_periph_M10_AXI_AWVALID),
        .M10_AXI_bready(microblaze_0_axi_periph_M10_AXI_BREADY),
        .M10_AXI_bresp(microblaze_0_axi_periph_M10_AXI_BRESP),
        .M10_AXI_bvalid(microblaze_0_axi_periph_M10_AXI_BVALID),
        .M10_AXI_rdata(microblaze_0_axi_periph_M10_AXI_RDATA),
        .M10_AXI_rready(microblaze_0_axi_periph_M10_AXI_RREADY),
        .M10_AXI_rresp(microblaze_0_axi_periph_M10_AXI_RRESP),
        .M10_AXI_rvalid(microblaze_0_axi_periph_M10_AXI_RVALID),
        .M10_AXI_wdata(microblaze_0_axi_periph_M10_AXI_WDATA),
        .M10_AXI_wready(microblaze_0_axi_periph_M10_AXI_WREADY),
        .M10_AXI_wstrb(microblaze_0_axi_periph_M10_AXI_WSTRB),
        .M10_AXI_wvalid(microblaze_0_axi_periph_M10_AXI_WVALID),
        .M11_ACLK(microblaze_0_Clk),
        .M11_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .M11_AXI_araddr(microblaze_0_axi_periph_M11_AXI_ARADDR),
        .M11_AXI_arready(microblaze_0_axi_periph_M11_AXI_ARREADY),
        .M11_AXI_arvalid(microblaze_0_axi_periph_M11_AXI_ARVALID),
        .M11_AXI_awaddr(microblaze_0_axi_periph_M11_AXI_AWADDR),
        .M11_AXI_awready(microblaze_0_axi_periph_M11_AXI_AWREADY),
        .M11_AXI_awvalid(microblaze_0_axi_periph_M11_AXI_AWVALID),
        .M11_AXI_bready(microblaze_0_axi_periph_M11_AXI_BREADY),
        .M11_AXI_bresp(microblaze_0_axi_periph_M11_AXI_BRESP),
        .M11_AXI_bvalid(microblaze_0_axi_periph_M11_AXI_BVALID),
        .M11_AXI_rdata(microblaze_0_axi_periph_M11_AXI_RDATA),
        .M11_AXI_rready(microblaze_0_axi_periph_M11_AXI_RREADY),
        .M11_AXI_rresp(microblaze_0_axi_periph_M11_AXI_RRESP),
        .M11_AXI_rvalid(microblaze_0_axi_periph_M11_AXI_RVALID),
        .M11_AXI_wdata(microblaze_0_axi_periph_M11_AXI_WDATA),
        .M11_AXI_wready(microblaze_0_axi_periph_M11_AXI_WREADY),
        .M11_AXI_wstrb(microblaze_0_axi_periph_M11_AXI_WSTRB),
        .M11_AXI_wvalid(microblaze_0_axi_periph_M11_AXI_WVALID),
        .S00_ACLK(microblaze_0_Clk),
        .S00_ARESETN(rst_clk_wiz_0_120M_peripheral_aresetn),
        .S00_AXI_araddr(microblaze_0_M_AXI_DP_ARADDR),
        .S00_AXI_arprot(microblaze_0_M_AXI_DP_ARPROT),
        .S00_AXI_arready(microblaze_0_M_AXI_DP_ARREADY),
        .S00_AXI_arvalid(microblaze_0_M_AXI_DP_ARVALID),
        .S00_AXI_awaddr(microblaze_0_M_AXI_DP_AWADDR),
        .S00_AXI_awprot(microblaze_0_M_AXI_DP_AWPROT),
        .S00_AXI_awready(microblaze_0_M_AXI_DP_AWREADY),
        .S00_AXI_awvalid(microblaze_0_M_AXI_DP_AWVALID),
        .S00_AXI_bready(microblaze_0_M_AXI_DP_BREADY),
        .S00_AXI_bresp(microblaze_0_M_AXI_DP_BRESP),
        .S00_AXI_bvalid(microblaze_0_M_AXI_DP_BVALID),
        .S00_AXI_rdata(microblaze_0_M_AXI_DP_RDATA),
        .S00_AXI_rready(microblaze_0_M_AXI_DP_RREADY),
        .S00_AXI_rresp(microblaze_0_M_AXI_DP_RRESP),
        .S00_AXI_rvalid(microblaze_0_M_AXI_DP_RVALID),
        .S00_AXI_wdata(microblaze_0_M_AXI_DP_WDATA),
        .S00_AXI_wready(microblaze_0_M_AXI_DP_WREADY),
        .S00_AXI_wstrb(microblaze_0_M_AXI_DP_WSTRB),
        .S00_AXI_wvalid(microblaze_0_M_AXI_DP_WVALID));
  microblaze_0_local_memory_imp_1788IOU microblaze_0_local_memory
       (.DLMB_abus(microblaze_0_dlmb_1_ABUS),
        .DLMB_addrstrobe(microblaze_0_dlmb_1_ADDRSTROBE),
        .DLMB_be(microblaze_0_dlmb_1_BE),
        .DLMB_ce(microblaze_0_dlmb_1_CE),
        .DLMB_readdbus(microblaze_0_dlmb_1_READDBUS),
        .DLMB_readstrobe(microblaze_0_dlmb_1_READSTROBE),
        .DLMB_ready(microblaze_0_dlmb_1_READY),
        .DLMB_ue(microblaze_0_dlmb_1_UE),
        .DLMB_wait(microblaze_0_dlmb_1_WAIT),
        .DLMB_writedbus(microblaze_0_dlmb_1_WRITEDBUS),
        .DLMB_writestrobe(microblaze_0_dlmb_1_WRITESTROBE),
        .ILMB_abus(microblaze_0_ilmb_1_ABUS),
        .ILMB_addrstrobe(microblaze_0_ilmb_1_ADDRSTROBE),
        .ILMB_ce(microblaze_0_ilmb_1_CE),
        .ILMB_readdbus(microblaze_0_ilmb_1_READDBUS),
        .ILMB_readstrobe(microblaze_0_ilmb_1_READSTROBE),
        .ILMB_ready(microblaze_0_ilmb_1_READY),
        .ILMB_ue(microblaze_0_ilmb_1_UE),
        .ILMB_wait(microblaze_0_ilmb_1_WAIT),
        .LMB_Clk(microblaze_0_Clk),
        .SYS_Rst(rst_clk_wiz_0_120M_bus_struct_reset));
  design_1_rst_clk_wiz_0_120M_0 rst_clk_wiz_0_120M
       (.aux_reset_in(1'b1),
        .bus_struct_reset(rst_clk_wiz_0_120M_bus_struct_reset),
        .dcm_locked(clk_wiz_0_locked),
        .ext_reset_in(reset_1),
        .mb_debug_sys_rst(mdm_1_debug_sys_rst),
        .mb_reset(rst_clk_wiz_0_120M_mb_reset),
        .peripheral_aresetn(rst_clk_wiz_0_120M_peripheral_aresetn),
        .slowest_sync_clk(microblaze_0_Clk));
endmodule

module pulse_conditioning_slow_imp_1ED40DV
   (AXI_DC_STAB_araddr,
    AXI_DC_STAB_arprot,
    AXI_DC_STAB_arready,
    AXI_DC_STAB_arvalid,
    AXI_DC_STAB_awaddr,
    AXI_DC_STAB_awprot,
    AXI_DC_STAB_awready,
    AXI_DC_STAB_awvalid,
    AXI_DC_STAB_bready,
    AXI_DC_STAB_bresp,
    AXI_DC_STAB_bvalid,
    AXI_DC_STAB_rdata,
    AXI_DC_STAB_rready,
    AXI_DC_STAB_rresp,
    AXI_DC_STAB_rvalid,
    AXI_DC_STAB_wdata,
    AXI_DC_STAB_wready,
    AXI_DC_STAB_wstrb,
    AXI_DC_STAB_wvalid,
    S00_ARESETN,
    S00_AXI2_araddr,
    S00_AXI2_arprot,
    S00_AXI2_arready,
    S00_AXI2_arvalid,
    S00_AXI2_awaddr,
    S00_AXI2_awprot,
    S00_AXI2_awready,
    S00_AXI2_awvalid,
    S00_AXI2_bready,
    S00_AXI2_bresp,
    S00_AXI2_bvalid,
    S00_AXI2_rdata,
    S00_AXI2_rready,
    S00_AXI2_rresp,
    S00_AXI2_rvalid,
    S00_AXI2_wdata,
    S00_AXI2_wready,
    S00_AXI2_wstrb,
    S00_AXI2_wvalid,
    axi_clk_domain_s_axi_araddr,
    axi_clk_domain_s_axi_arready,
    axi_clk_domain_s_axi_arvalid,
    axi_clk_domain_s_axi_awaddr,
    axi_clk_domain_s_axi_awready,
    axi_clk_domain_s_axi_awvalid,
    axi_clk_domain_s_axi_bready,
    axi_clk_domain_s_axi_bresp,
    axi_clk_domain_s_axi_bvalid,
    axi_clk_domain_s_axi_rdata,
    axi_clk_domain_s_axi_rready,
    axi_clk_domain_s_axi_rresp,
    axi_clk_domain_s_axi_rvalid,
    axi_clk_domain_s_axi_wdata,
    axi_clk_domain_s_axi_wready,
    axi_clk_domain_s_axi_wstrb,
    axi_clk_domain_s_axi_wvalid,
    clk,
    clk_cpu,
    data_in,
    dbg_pulse_cond_slow_m0_blr,
    dbg_pulse_cond_slow_m0_dc_stab,
    dbg_pulse_cond_slow_m0_impulse,
    dbg_pulse_cond_slow_m0_rect,
    dbg_pulse_cond_slow_m0_shaper,
    y);
  input [31:0]AXI_DC_STAB_araddr;
  input [2:0]AXI_DC_STAB_arprot;
  output AXI_DC_STAB_arready;
  input AXI_DC_STAB_arvalid;
  input [31:0]AXI_DC_STAB_awaddr;
  input [2:0]AXI_DC_STAB_awprot;
  output AXI_DC_STAB_awready;
  input AXI_DC_STAB_awvalid;
  input AXI_DC_STAB_bready;
  output [1:0]AXI_DC_STAB_bresp;
  output AXI_DC_STAB_bvalid;
  output [31:0]AXI_DC_STAB_rdata;
  input AXI_DC_STAB_rready;
  output [1:0]AXI_DC_STAB_rresp;
  output AXI_DC_STAB_rvalid;
  input [31:0]AXI_DC_STAB_wdata;
  output AXI_DC_STAB_wready;
  input [3:0]AXI_DC_STAB_wstrb;
  input AXI_DC_STAB_wvalid;
  input S00_ARESETN;
  input [31:0]S00_AXI2_araddr;
  input [2:0]S00_AXI2_arprot;
  output S00_AXI2_arready;
  input S00_AXI2_arvalid;
  input [31:0]S00_AXI2_awaddr;
  input [2:0]S00_AXI2_awprot;
  output S00_AXI2_awready;
  input S00_AXI2_awvalid;
  input S00_AXI2_bready;
  output [1:0]S00_AXI2_bresp;
  output S00_AXI2_bvalid;
  output [31:0]S00_AXI2_rdata;
  input S00_AXI2_rready;
  output [1:0]S00_AXI2_rresp;
  output S00_AXI2_rvalid;
  input [31:0]S00_AXI2_wdata;
  output S00_AXI2_wready;
  input [3:0]S00_AXI2_wstrb;
  input S00_AXI2_wvalid;
  input [31:0]axi_clk_domain_s_axi_araddr;
  output axi_clk_domain_s_axi_arready;
  input axi_clk_domain_s_axi_arvalid;
  input [31:0]axi_clk_domain_s_axi_awaddr;
  output axi_clk_domain_s_axi_awready;
  input axi_clk_domain_s_axi_awvalid;
  input axi_clk_domain_s_axi_bready;
  output [1:0]axi_clk_domain_s_axi_bresp;
  output axi_clk_domain_s_axi_bvalid;
  output [31:0]axi_clk_domain_s_axi_rdata;
  input axi_clk_domain_s_axi_rready;
  output [1:0]axi_clk_domain_s_axi_rresp;
  output axi_clk_domain_s_axi_rvalid;
  input [31:0]axi_clk_domain_s_axi_wdata;
  output axi_clk_domain_s_axi_wready;
  input [3:0]axi_clk_domain_s_axi_wstrb;
  input axi_clk_domain_s_axi_wvalid;
  input clk;
  input clk_cpu;
  input [15:0]data_in;
  output [15:0]dbg_pulse_cond_slow_m0_blr;
  output [15:0]dbg_pulse_cond_slow_m0_dc_stab;
  output [15:0]dbg_pulse_cond_slow_m0_impulse;
  output [15:0]dbg_pulse_cond_slow_m0_rect;
  output [15:0]dbg_pulse_cond_slow_m0_shaper;
  output [15:0]y;

  wire [31:0]Conn1_ARADDR;
  wire Conn1_ARREADY;
  wire Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire Conn1_AWREADY;
  wire Conn1_AWVALID;
  wire Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire Conn1_BVALID;
  wire [31:0]Conn1_RDATA;
  wire Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire Conn1_RVALID;
  wire [31:0]Conn1_WDATA;
  wire Conn1_WREADY;
  wire [3:0]Conn1_WSTRB;
  wire Conn1_WVALID;
  wire [31:0]Conn2_ARADDR;
  wire [2:0]Conn2_ARPROT;
  wire Conn2_ARREADY;
  wire Conn2_ARVALID;
  wire [31:0]Conn2_AWADDR;
  wire [2:0]Conn2_AWPROT;
  wire Conn2_AWREADY;
  wire Conn2_AWVALID;
  wire Conn2_BREADY;
  wire [1:0]Conn2_BRESP;
  wire Conn2_BVALID;
  wire [31:0]Conn2_RDATA;
  wire Conn2_RREADY;
  wire [1:0]Conn2_RRESP;
  wire Conn2_RVALID;
  wire [31:0]Conn2_WDATA;
  wire Conn2_WREADY;
  wire [3:0]Conn2_WSTRB;
  wire Conn2_WVALID;
  wire [31:0]Conn3_ARADDR;
  wire [2:0]Conn3_ARPROT;
  wire Conn3_ARREADY;
  wire Conn3_ARVALID;
  wire [31:0]Conn3_AWADDR;
  wire [2:0]Conn3_AWPROT;
  wire Conn3_AWREADY;
  wire Conn3_AWVALID;
  wire Conn3_BREADY;
  wire [1:0]Conn3_BRESP;
  wire Conn3_BVALID;
  wire [31:0]Conn3_RDATA;
  wire Conn3_RREADY;
  wire [1:0]Conn3_RRESP;
  wire Conn3_RVALID;
  wire [31:0]Conn3_WDATA;
  wire Conn3_WREADY;
  wire [3:0]Conn3_WSTRB;
  wire Conn3_WVALID;
  wire S00_ARESETN_1;
  wire clk_1;
  wire clk_cpu_1;
  wire [15:0]data_in_1;
  wire [15:0]ip_blr_0_y;
  wire [15:0]ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_blr;
  wire [15:0]ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_dc_stab;
  wire [15:0]ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_impulse;
  wire [15:0]ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_rect;
  wire [15:0]ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_shaper;
  wire [15:0]ip_dc_stabilizer_0_dbg_acc;
  wire [15:0]ip_dc_stabilizer_0_y;
  wire [15:0]ip_shaper_0_impulse_out;
  wire [15:0]ip_shaper_0_rect_out;
  wire [15:0]ip_shaper_0_shaper_out;

  assign AXI_DC_STAB_arready = Conn3_ARREADY;
  assign AXI_DC_STAB_awready = Conn3_AWREADY;
  assign AXI_DC_STAB_bresp[1:0] = Conn3_BRESP;
  assign AXI_DC_STAB_bvalid = Conn3_BVALID;
  assign AXI_DC_STAB_rdata[31:0] = Conn3_RDATA;
  assign AXI_DC_STAB_rresp[1:0] = Conn3_RRESP;
  assign AXI_DC_STAB_rvalid = Conn3_RVALID;
  assign AXI_DC_STAB_wready = Conn3_WREADY;
  assign Conn1_ARADDR = axi_clk_domain_s_axi_araddr[31:0];
  assign Conn1_ARVALID = axi_clk_domain_s_axi_arvalid;
  assign Conn1_AWADDR = axi_clk_domain_s_axi_awaddr[31:0];
  assign Conn1_AWVALID = axi_clk_domain_s_axi_awvalid;
  assign Conn1_BREADY = axi_clk_domain_s_axi_bready;
  assign Conn1_RREADY = axi_clk_domain_s_axi_rready;
  assign Conn1_WDATA = axi_clk_domain_s_axi_wdata[31:0];
  assign Conn1_WSTRB = axi_clk_domain_s_axi_wstrb[3:0];
  assign Conn1_WVALID = axi_clk_domain_s_axi_wvalid;
  assign Conn2_ARADDR = S00_AXI2_araddr[31:0];
  assign Conn2_ARPROT = S00_AXI2_arprot[2:0];
  assign Conn2_ARVALID = S00_AXI2_arvalid;
  assign Conn2_AWADDR = S00_AXI2_awaddr[31:0];
  assign Conn2_AWPROT = S00_AXI2_awprot[2:0];
  assign Conn2_AWVALID = S00_AXI2_awvalid;
  assign Conn2_BREADY = S00_AXI2_bready;
  assign Conn2_RREADY = S00_AXI2_rready;
  assign Conn2_WDATA = S00_AXI2_wdata[31:0];
  assign Conn2_WSTRB = S00_AXI2_wstrb[3:0];
  assign Conn2_WVALID = S00_AXI2_wvalid;
  assign Conn3_ARADDR = AXI_DC_STAB_araddr[31:0];
  assign Conn3_ARPROT = AXI_DC_STAB_arprot[2:0];
  assign Conn3_ARVALID = AXI_DC_STAB_arvalid;
  assign Conn3_AWADDR = AXI_DC_STAB_awaddr[31:0];
  assign Conn3_AWPROT = AXI_DC_STAB_awprot[2:0];
  assign Conn3_AWVALID = AXI_DC_STAB_awvalid;
  assign Conn3_BREADY = AXI_DC_STAB_bready;
  assign Conn3_RREADY = AXI_DC_STAB_rready;
  assign Conn3_WDATA = AXI_DC_STAB_wdata[31:0];
  assign Conn3_WSTRB = AXI_DC_STAB_wstrb[3:0];
  assign Conn3_WVALID = AXI_DC_STAB_wvalid;
  assign S00_ARESETN_1 = S00_ARESETN;
  assign S00_AXI2_arready = Conn2_ARREADY;
  assign S00_AXI2_awready = Conn2_AWREADY;
  assign S00_AXI2_bresp[1:0] = Conn2_BRESP;
  assign S00_AXI2_bvalid = Conn2_BVALID;
  assign S00_AXI2_rdata[31:0] = Conn2_RDATA;
  assign S00_AXI2_rresp[1:0] = Conn2_RRESP;
  assign S00_AXI2_rvalid = Conn2_RVALID;
  assign S00_AXI2_wready = Conn2_WREADY;
  assign axi_clk_domain_s_axi_arready = Conn1_ARREADY;
  assign axi_clk_domain_s_axi_awready = Conn1_AWREADY;
  assign axi_clk_domain_s_axi_bresp[1:0] = Conn1_BRESP;
  assign axi_clk_domain_s_axi_bvalid = Conn1_BVALID;
  assign axi_clk_domain_s_axi_rdata[31:0] = Conn1_RDATA;
  assign axi_clk_domain_s_axi_rresp[1:0] = Conn1_RRESP;
  assign axi_clk_domain_s_axi_rvalid = Conn1_RVALID;
  assign axi_clk_domain_s_axi_wready = Conn1_WREADY;
  assign clk_1 = clk;
  assign clk_cpu_1 = clk_cpu;
  assign data_in_1 = data_in[15:0];
  assign dbg_pulse_cond_slow_m0_blr[15:0] = ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_blr;
  assign dbg_pulse_cond_slow_m0_dc_stab[15:0] = ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_dc_stab;
  assign dbg_pulse_cond_slow_m0_impulse[15:0] = ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_impulse;
  assign dbg_pulse_cond_slow_m0_rect[15:0] = ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_rect;
  assign dbg_pulse_cond_slow_m0_shaper[15:0] = ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_shaper;
  assign y[15:0] = ip_blr_0_y;
  design_1_ip_blr_0_0 ip_blr_0
       (.clk(clk_1),
        .resetn(S00_ARESETN_1),
        .s00_axi_aclk(clk_cpu_1),
        .s00_axi_araddr(Conn2_ARADDR[3:0]),
        .s00_axi_aresetn(S00_ARESETN_1),
        .s00_axi_arprot(Conn2_ARPROT),
        .s00_axi_arready(Conn2_ARREADY),
        .s00_axi_arvalid(Conn2_ARVALID),
        .s00_axi_awaddr(Conn2_AWADDR[3:0]),
        .s00_axi_awprot(Conn2_AWPROT),
        .s00_axi_awready(Conn2_AWREADY),
        .s00_axi_awvalid(Conn2_AWVALID),
        .s00_axi_bready(Conn2_BREADY),
        .s00_axi_bresp(Conn2_BRESP),
        .s00_axi_bvalid(Conn2_BVALID),
        .s00_axi_rdata(Conn2_RDATA),
        .s00_axi_rready(Conn2_RREADY),
        .s00_axi_rresp(Conn2_RRESP),
        .s00_axi_rvalid(Conn2_RVALID),
        .s00_axi_wdata(Conn2_WDATA),
        .s00_axi_wready(Conn2_WREADY),
        .s00_axi_wstrb(Conn2_WSTRB),
        .s00_axi_wvalid(Conn2_WVALID),
        .x(ip_shaper_0_shaper_out),
        .y(ip_blr_0_y));
  design_1_ip_dbg_pulse_cond_sl_0_0 ip_dbg_pulse_cond_sl_0
       (.blr(ip_blr_0_y),
        .blr_out(ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_blr),
        .dc_stab(ip_dc_stabilizer_0_y),
        .dc_stab_acc(ip_dc_stabilizer_0_dbg_acc),
        .dc_stab_out(ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_dc_stab),
        .impulse(ip_shaper_0_impulse_out),
        .impulse_out(ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_impulse),
        .rect(ip_shaper_0_rect_out),
        .rect_out(ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_rect),
        .shaper(ip_shaper_0_shaper_out),
        .shaper_out(ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0_shaper));
  design_1_ip_dc_stabilizer_0_0 ip_dc_stabilizer_0
       (.axi_dc_stab_aclk(clk_cpu_1),
        .axi_dc_stab_araddr(Conn3_ARADDR[3:0]),
        .axi_dc_stab_aresetn(S00_ARESETN_1),
        .axi_dc_stab_arprot(Conn3_ARPROT),
        .axi_dc_stab_arready(Conn3_ARREADY),
        .axi_dc_stab_arvalid(Conn3_ARVALID),
        .axi_dc_stab_awaddr(Conn3_AWADDR[3:0]),
        .axi_dc_stab_awprot(Conn3_AWPROT),
        .axi_dc_stab_awready(Conn3_AWREADY),
        .axi_dc_stab_awvalid(Conn3_AWVALID),
        .axi_dc_stab_bready(Conn3_BREADY),
        .axi_dc_stab_bresp(Conn3_BRESP),
        .axi_dc_stab_bvalid(Conn3_BVALID),
        .axi_dc_stab_rdata(Conn3_RDATA),
        .axi_dc_stab_rready(Conn3_RREADY),
        .axi_dc_stab_rresp(Conn3_RRESP),
        .axi_dc_stab_rvalid(Conn3_RVALID),
        .axi_dc_stab_wdata(Conn3_WDATA),
        .axi_dc_stab_wready(Conn3_WREADY),
        .axi_dc_stab_wstrb(Conn3_WSTRB),
        .axi_dc_stab_wvalid(Conn3_WVALID),
        .clk(clk_1),
        .dbg_acc(ip_dc_stabilizer_0_dbg_acc),
        .feedback(ip_shaper_0_shaper_out),
        .resetn(S00_ARESETN_1),
        .x(data_in_1),
        .y(ip_dc_stabilizer_0_y));
  design_1_ip_shaper_0_0 ip_shaper_0
       (.axi_clk_domain_aresetn(S00_ARESETN_1),
        .axi_clk_domain_clk(clk_cpu_1),
        .axi_clk_domain_s_axi_araddr(Conn1_ARADDR[4:0]),
        .axi_clk_domain_s_axi_arready(Conn1_ARREADY),
        .axi_clk_domain_s_axi_arvalid(Conn1_ARVALID),
        .axi_clk_domain_s_axi_awaddr(Conn1_AWADDR[4:0]),
        .axi_clk_domain_s_axi_awready(Conn1_AWREADY),
        .axi_clk_domain_s_axi_awvalid(Conn1_AWVALID),
        .axi_clk_domain_s_axi_bready(Conn1_BREADY),
        .axi_clk_domain_s_axi_bresp(Conn1_BRESP),
        .axi_clk_domain_s_axi_bvalid(Conn1_BVALID),
        .axi_clk_domain_s_axi_rdata(Conn1_RDATA),
        .axi_clk_domain_s_axi_rready(Conn1_RREADY),
        .axi_clk_domain_s_axi_rresp(Conn1_RRESP),
        .axi_clk_domain_s_axi_rvalid(Conn1_RVALID),
        .axi_clk_domain_s_axi_wdata(Conn1_WDATA),
        .axi_clk_domain_s_axi_wready(Conn1_WREADY),
        .axi_clk_domain_s_axi_wstrb(Conn1_WSTRB),
        .axi_clk_domain_s_axi_wvalid(Conn1_WVALID),
        .data_in(ip_dc_stabilizer_0_y),
        .filter_clk_domain_clk(clk_1),
        .impulse_out(ip_shaper_0_impulse_out),
        .rect_out(ip_shaper_0_rect_out),
        .shaper_out(ip_shaper_0_shaper_out));
endmodule

module pulse_offseting_imp_1W6WVCT
   (S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid,
    adc_data,
    clk,
    dbg_invert_and_offset_m0_adc_data,
    dbg_invert_and_offset_m0_outp,
    outp,
    s00_axi_aclk,
    s00_axi_aresetn);
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output S00_AXI_arready;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output S00_AXI_awready;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;
  input [13:0]adc_data;
  input clk;
  output [15:0]dbg_invert_and_offset_m0_adc_data;
  output [15:0]dbg_invert_and_offset_m0_outp;
  output [15:0]outp;
  input s00_axi_aclk;
  input s00_axi_aresetn;

  wire [31:0]S00_AXI_1_ARADDR;
  wire [2:0]S00_AXI_1_ARPROT;
  wire S00_AXI_1_ARREADY;
  wire S00_AXI_1_ARVALID;
  wire [31:0]S00_AXI_1_AWADDR;
  wire [2:0]S00_AXI_1_AWPROT;
  wire S00_AXI_1_AWREADY;
  wire S00_AXI_1_AWVALID;
  wire S00_AXI_1_BREADY;
  wire [1:0]S00_AXI_1_BRESP;
  wire S00_AXI_1_BVALID;
  wire [31:0]S00_AXI_1_RDATA;
  wire S00_AXI_1_RREADY;
  wire [1:0]S00_AXI_1_RRESP;
  wire S00_AXI_1_RVALID;
  wire [31:0]S00_AXI_1_WDATA;
  wire S00_AXI_1_WREADY;
  wire [3:0]S00_AXI_1_WSTRB;
  wire S00_AXI_1_WVALID;
  wire [13:0]adc_data_1;
  wire clk_1;
  wire [15:0]invert_and_offset_0_outp;
  wire [15:0]ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data;
  wire [15:0]ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp;
  wire s00_axi_aclk_1;
  wire s00_axi_aresetn_1;

  assign S00_AXI_1_ARADDR = S00_AXI_araddr[31:0];
  assign S00_AXI_1_ARPROT = S00_AXI_arprot[2:0];
  assign S00_AXI_1_ARVALID = S00_AXI_arvalid;
  assign S00_AXI_1_AWADDR = S00_AXI_awaddr[31:0];
  assign S00_AXI_1_AWPROT = S00_AXI_awprot[2:0];
  assign S00_AXI_1_AWVALID = S00_AXI_awvalid;
  assign S00_AXI_1_BREADY = S00_AXI_bready;
  assign S00_AXI_1_RREADY = S00_AXI_rready;
  assign S00_AXI_1_WDATA = S00_AXI_wdata[31:0];
  assign S00_AXI_1_WSTRB = S00_AXI_wstrb[3:0];
  assign S00_AXI_1_WVALID = S00_AXI_wvalid;
  assign S00_AXI_arready = S00_AXI_1_ARREADY;
  assign S00_AXI_awready = S00_AXI_1_AWREADY;
  assign S00_AXI_bresp[1:0] = S00_AXI_1_BRESP;
  assign S00_AXI_bvalid = S00_AXI_1_BVALID;
  assign S00_AXI_rdata[31:0] = S00_AXI_1_RDATA;
  assign S00_AXI_rresp[1:0] = S00_AXI_1_RRESP;
  assign S00_AXI_rvalid = S00_AXI_1_RVALID;
  assign S00_AXI_wready = S00_AXI_1_WREADY;
  assign adc_data_1 = adc_data[13:0];
  assign clk_1 = clk;
  assign dbg_invert_and_offset_m0_adc_data[15:0] = ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data;
  assign dbg_invert_and_offset_m0_outp[15:0] = ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp;
  assign outp[15:0] = invert_and_offset_0_outp;
  assign s00_axi_aclk_1 = s00_axi_aclk;
  assign s00_axi_aresetn_1 = s00_axi_aresetn;
  design_1_invert_and_offset_0_0 invert_and_offset_0
       (.clk(clk_1),
        .inp(adc_data_1),
        .outp(invert_and_offset_0_outp),
        .s00_axi_aclk(s00_axi_aclk_1),
        .s00_axi_araddr(S00_AXI_1_ARADDR[3:0]),
        .s00_axi_aresetn(s00_axi_aresetn_1),
        .s00_axi_arprot(S00_AXI_1_ARPROT),
        .s00_axi_arready(S00_AXI_1_ARREADY),
        .s00_axi_arvalid(S00_AXI_1_ARVALID),
        .s00_axi_awaddr(S00_AXI_1_AWADDR[3:0]),
        .s00_axi_awprot(S00_AXI_1_AWPROT),
        .s00_axi_awready(S00_AXI_1_AWREADY),
        .s00_axi_awvalid(S00_AXI_1_AWVALID),
        .s00_axi_bready(S00_AXI_1_BREADY),
        .s00_axi_bresp(S00_AXI_1_BRESP),
        .s00_axi_bvalid(S00_AXI_1_BVALID),
        .s00_axi_rdata(S00_AXI_1_RDATA),
        .s00_axi_rready(S00_AXI_1_RREADY),
        .s00_axi_rresp(S00_AXI_1_RRESP),
        .s00_axi_rvalid(S00_AXI_1_RVALID),
        .s00_axi_wdata(S00_AXI_1_WDATA),
        .s00_axi_wready(S00_AXI_1_WREADY),
        .s00_axi_wstrb(S00_AXI_1_WSTRB),
        .s00_axi_wvalid(S00_AXI_1_WVALID));
  design_1_ip_dbg_invert_and_of_0_0 ip_dbg_invert_and_of_0
       (.adc_data(adc_data_1),
        .adc_data_out(ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data),
        .outp(invert_and_offset_0_outp),
        .outp_out(ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp));
endmodule

module s00_couplers_imp_1PT77VU
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]s00_couplers_to_s00_couplers_ARADDR;
  wire [2:0]s00_couplers_to_s00_couplers_ARPROT;
  wire [0:0]s00_couplers_to_s00_couplers_ARREADY;
  wire [0:0]s00_couplers_to_s00_couplers_ARVALID;
  wire [31:0]s00_couplers_to_s00_couplers_AWADDR;
  wire [2:0]s00_couplers_to_s00_couplers_AWPROT;
  wire [0:0]s00_couplers_to_s00_couplers_AWREADY;
  wire [0:0]s00_couplers_to_s00_couplers_AWVALID;
  wire [0:0]s00_couplers_to_s00_couplers_BREADY;
  wire [1:0]s00_couplers_to_s00_couplers_BRESP;
  wire [0:0]s00_couplers_to_s00_couplers_BVALID;
  wire [31:0]s00_couplers_to_s00_couplers_RDATA;
  wire [0:0]s00_couplers_to_s00_couplers_RREADY;
  wire [1:0]s00_couplers_to_s00_couplers_RRESP;
  wire [0:0]s00_couplers_to_s00_couplers_RVALID;
  wire [31:0]s00_couplers_to_s00_couplers_WDATA;
  wire [0:0]s00_couplers_to_s00_couplers_WREADY;
  wire [3:0]s00_couplers_to_s00_couplers_WSTRB;
  wire [0:0]s00_couplers_to_s00_couplers_WVALID;

  assign M_AXI_araddr[31:0] = s00_couplers_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = s00_couplers_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid[0] = s00_couplers_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = s00_couplers_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = s00_couplers_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid[0] = s00_couplers_to_s00_couplers_AWVALID;
  assign M_AXI_bready[0] = s00_couplers_to_s00_couplers_BREADY;
  assign M_AXI_rready[0] = s00_couplers_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = s00_couplers_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = s00_couplers_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid[0] = s00_couplers_to_s00_couplers_WVALID;
  assign S_AXI_arready[0] = s00_couplers_to_s00_couplers_ARREADY;
  assign S_AXI_awready[0] = s00_couplers_to_s00_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = s00_couplers_to_s00_couplers_BRESP;
  assign S_AXI_bvalid[0] = s00_couplers_to_s00_couplers_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_s00_couplers_RDATA;
  assign S_AXI_rresp[1:0] = s00_couplers_to_s00_couplers_RRESP;
  assign S_AXI_rvalid[0] = s00_couplers_to_s00_couplers_RVALID;
  assign S_AXI_wready[0] = s00_couplers_to_s00_couplers_WREADY;
  assign s00_couplers_to_s00_couplers_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_s00_couplers_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_s00_couplers_ARREADY = M_AXI_arready[0];
  assign s00_couplers_to_s00_couplers_ARVALID = S_AXI_arvalid[0];
  assign s00_couplers_to_s00_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_s00_couplers_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_s00_couplers_AWREADY = M_AXI_awready[0];
  assign s00_couplers_to_s00_couplers_AWVALID = S_AXI_awvalid[0];
  assign s00_couplers_to_s00_couplers_BREADY = S_AXI_bready[0];
  assign s00_couplers_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign s00_couplers_to_s00_couplers_BVALID = M_AXI_bvalid[0];
  assign s00_couplers_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign s00_couplers_to_s00_couplers_RREADY = S_AXI_rready[0];
  assign s00_couplers_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign s00_couplers_to_s00_couplers_RVALID = M_AXI_rvalid[0];
  assign s00_couplers_to_s00_couplers_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_s00_couplers_WREADY = M_AXI_wready[0];
  assign s00_couplers_to_s00_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_s00_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module scope_imp_DL4TDP
   (S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rready,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    clk,
    dbg_invert_and_offset_s0_adc_data,
    dbg_invert_and_offset_s0_outp,
    dbg_pha_s0_peak_amp_rdy_fast_out,
    dbg_pha_s0_peak_amp_rdy_slow_out,
    dbg_pha_s0_peak_det_signal_out,
    dbg_pha_s0_rejectn_out,
    dbg_pulse_cond_slow_s0_blr,
    dbg_pulse_cond_slow_s0_dc_stab,
    dbg_pulse_cond_slow_s0_impulse,
    dbg_pulse_cond_slow_s0_rect,
    dbg_pulse_cond_slow_s0_shaper,
    full,
    s00_axi_aclk,
    s00_axi_aresetn);
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output S00_AXI_arready;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output S00_AXI_awready;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;
  input [31:0]axibusdomain_s_axi_araddr;
  output axibusdomain_s_axi_arready;
  input axibusdomain_s_axi_arvalid;
  input [31:0]axibusdomain_s_axi_awaddr;
  output axibusdomain_s_axi_awready;
  input axibusdomain_s_axi_awvalid;
  input axibusdomain_s_axi_bready;
  output [1:0]axibusdomain_s_axi_bresp;
  output axibusdomain_s_axi_bvalid;
  output [31:0]axibusdomain_s_axi_rdata;
  input axibusdomain_s_axi_rready;
  output [1:0]axibusdomain_s_axi_rresp;
  output axibusdomain_s_axi_rvalid;
  input [31:0]axibusdomain_s_axi_wdata;
  output axibusdomain_s_axi_wready;
  input [3:0]axibusdomain_s_axi_wstrb;
  input axibusdomain_s_axi_wvalid;
  input clk;
  input [15:0]dbg_invert_and_offset_s0_adc_data;
  input [15:0]dbg_invert_and_offset_s0_outp;
  input [15:0]dbg_pha_s0_peak_amp_rdy_fast_out;
  input [15:0]dbg_pha_s0_peak_amp_rdy_slow_out;
  input [15:0]dbg_pha_s0_peak_det_signal_out;
  input [15:0]dbg_pha_s0_rejectn_out;
  input [15:0]dbg_pulse_cond_slow_s0_blr;
  input [15:0]dbg_pulse_cond_slow_s0_dc_stab;
  input [15:0]dbg_pulse_cond_slow_s0_impulse;
  input [15:0]dbg_pulse_cond_slow_s0_rect;
  input [15:0]dbg_pulse_cond_slow_s0_shaper;
  output [0:0]full;
  input s00_axi_aclk;
  input s00_axi_aresetn;

  wire [31:0]S00_AXI1_1_ARADDR;
  wire [2:0]S00_AXI1_1_ARPROT;
  wire S00_AXI1_1_ARREADY;
  wire S00_AXI1_1_ARVALID;
  wire [31:0]S00_AXI1_1_AWADDR;
  wire [2:0]S00_AXI1_1_AWPROT;
  wire S00_AXI1_1_AWREADY;
  wire S00_AXI1_1_AWVALID;
  wire S00_AXI1_1_BREADY;
  wire [1:0]S00_AXI1_1_BRESP;
  wire S00_AXI1_1_BVALID;
  wire [31:0]S00_AXI1_1_RDATA;
  wire S00_AXI1_1_RREADY;
  wire [1:0]S00_AXI1_1_RRESP;
  wire S00_AXI1_1_RVALID;
  wire [31:0]S00_AXI1_1_WDATA;
  wire S00_AXI1_1_WREADY;
  wire [3:0]S00_AXI1_1_WSTRB;
  wire S00_AXI1_1_WVALID;
  wire [31:0]axibusdomain_s_axi_1_ARADDR;
  wire axibusdomain_s_axi_1_ARREADY;
  wire axibusdomain_s_axi_1_ARVALID;
  wire [31:0]axibusdomain_s_axi_1_AWADDR;
  wire axibusdomain_s_axi_1_AWREADY;
  wire axibusdomain_s_axi_1_AWVALID;
  wire axibusdomain_s_axi_1_BREADY;
  wire [1:0]axibusdomain_s_axi_1_BRESP;
  wire axibusdomain_s_axi_1_BVALID;
  wire [31:0]axibusdomain_s_axi_1_RDATA;
  wire axibusdomain_s_axi_1_RREADY;
  wire [1:0]axibusdomain_s_axi_1_RRESP;
  wire axibusdomain_s_axi_1_RVALID;
  wire [31:0]axibusdomain_s_axi_1_WDATA;
  wire axibusdomain_s_axi_1_WREADY;
  wire [3:0]axibusdomain_s_axi_1_WSTRB;
  wire axibusdomain_s_axi_1_WVALID;
  wire clk_1;
  wire [15:0]dbg_pha_s0_1_peak_amp_rdy_fast_out;
  wire [15:0]dbg_pha_s0_1_peak_amp_rdy_slow_out;
  wire [15:0]dbg_pha_s0_1_peak_det_signal_out;
  wire [15:0]dbg_pha_s0_1_rejectn_out;
  wire [15:0]dbg_pulse_cond_slow_s0_1_blr;
  wire [15:0]dbg_pulse_cond_slow_s0_1_dc_stab;
  wire [15:0]dbg_pulse_cond_slow_s0_1_impulse;
  wire [15:0]dbg_pulse_cond_slow_s0_1_rect;
  wire [15:0]dbg_pulse_cond_slow_s0_1_shaper;
  wire [15:0]ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data;
  wire [15:0]ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp;
  wire [15:0]ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_blr;
  wire [15:0]ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_dc_stab;
  wire [15:0]ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_impulse;
  wire [15:0]ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_rect;
  wire [15:0]ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_shaper;
  wire [15:0]ip_mux16_2_if_0_outp1;
  wire [15:0]ip_mux16_2_if_0_outp2;
  wire [15:0]ip_mux16_2_if_0_trig;
  wire s00_axi_aclk_1;
  wire s00_axi_aresetn_1;
  wire [0:0]scope_full;

  assign S00_AXI1_1_ARADDR = S00_AXI_araddr[31:0];
  assign S00_AXI1_1_ARPROT = S00_AXI_arprot[2:0];
  assign S00_AXI1_1_ARVALID = S00_AXI_arvalid;
  assign S00_AXI1_1_AWADDR = S00_AXI_awaddr[31:0];
  assign S00_AXI1_1_AWPROT = S00_AXI_awprot[2:0];
  assign S00_AXI1_1_AWVALID = S00_AXI_awvalid;
  assign S00_AXI1_1_BREADY = S00_AXI_bready;
  assign S00_AXI1_1_RREADY = S00_AXI_rready;
  assign S00_AXI1_1_WDATA = S00_AXI_wdata[31:0];
  assign S00_AXI1_1_WSTRB = S00_AXI_wstrb[3:0];
  assign S00_AXI1_1_WVALID = S00_AXI_wvalid;
  assign S00_AXI_arready = S00_AXI1_1_ARREADY;
  assign S00_AXI_awready = S00_AXI1_1_AWREADY;
  assign S00_AXI_bresp[1:0] = S00_AXI1_1_BRESP;
  assign S00_AXI_bvalid = S00_AXI1_1_BVALID;
  assign S00_AXI_rdata[31:0] = S00_AXI1_1_RDATA;
  assign S00_AXI_rresp[1:0] = S00_AXI1_1_RRESP;
  assign S00_AXI_rvalid = S00_AXI1_1_RVALID;
  assign S00_AXI_wready = S00_AXI1_1_WREADY;
  assign axibusdomain_s_axi_1_ARADDR = axibusdomain_s_axi_araddr[31:0];
  assign axibusdomain_s_axi_1_ARVALID = axibusdomain_s_axi_arvalid;
  assign axibusdomain_s_axi_1_AWADDR = axibusdomain_s_axi_awaddr[31:0];
  assign axibusdomain_s_axi_1_AWVALID = axibusdomain_s_axi_awvalid;
  assign axibusdomain_s_axi_1_BREADY = axibusdomain_s_axi_bready;
  assign axibusdomain_s_axi_1_RREADY = axibusdomain_s_axi_rready;
  assign axibusdomain_s_axi_1_WDATA = axibusdomain_s_axi_wdata[31:0];
  assign axibusdomain_s_axi_1_WSTRB = axibusdomain_s_axi_wstrb[3:0];
  assign axibusdomain_s_axi_1_WVALID = axibusdomain_s_axi_wvalid;
  assign axibusdomain_s_axi_arready = axibusdomain_s_axi_1_ARREADY;
  assign axibusdomain_s_axi_awready = axibusdomain_s_axi_1_AWREADY;
  assign axibusdomain_s_axi_bresp[1:0] = axibusdomain_s_axi_1_BRESP;
  assign axibusdomain_s_axi_bvalid = axibusdomain_s_axi_1_BVALID;
  assign axibusdomain_s_axi_rdata[31:0] = axibusdomain_s_axi_1_RDATA;
  assign axibusdomain_s_axi_rresp[1:0] = axibusdomain_s_axi_1_RRESP;
  assign axibusdomain_s_axi_rvalid = axibusdomain_s_axi_1_RVALID;
  assign axibusdomain_s_axi_wready = axibusdomain_s_axi_1_WREADY;
  assign clk_1 = clk;
  assign dbg_pha_s0_1_peak_amp_rdy_fast_out = dbg_pha_s0_peak_amp_rdy_fast_out[15:0];
  assign dbg_pha_s0_1_peak_amp_rdy_slow_out = dbg_pha_s0_peak_amp_rdy_slow_out[15:0];
  assign dbg_pha_s0_1_peak_det_signal_out = dbg_pha_s0_peak_det_signal_out[15:0];
  assign dbg_pha_s0_1_rejectn_out = dbg_pha_s0_rejectn_out[15:0];
  assign dbg_pulse_cond_slow_s0_1_blr = dbg_pulse_cond_slow_s0_blr[15:0];
  assign dbg_pulse_cond_slow_s0_1_dc_stab = dbg_pulse_cond_slow_s0_dc_stab[15:0];
  assign dbg_pulse_cond_slow_s0_1_impulse = dbg_pulse_cond_slow_s0_impulse[15:0];
  assign dbg_pulse_cond_slow_s0_1_rect = dbg_pulse_cond_slow_s0_rect[15:0];
  assign dbg_pulse_cond_slow_s0_1_shaper = dbg_pulse_cond_slow_s0_shaper[15:0];
  assign full[0] = scope_full;
  assign ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data = dbg_invert_and_offset_s0_adc_data[15:0];
  assign ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp = dbg_invert_and_offset_s0_outp[15:0];
  assign s00_axi_aclk_1 = s00_axi_aclk;
  assign s00_axi_aresetn_1 = s00_axi_aresetn;
  design_1_ip_dbg_term_pulse_co_0_3 ip_dbg_term_pulse_co_0
       (.blr_out(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_blr),
        .dc_stab_out(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_dc_stab),
        .impulse_out(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_impulse),
        .rect_out(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_rect),
        .shaper_out(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_shaper));
  design_1_ip_mux16_2_if_0_0 ip_mux16_2_if_0
       (.inp1(ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_adc_data),
        .inp10(dbg_pha_s0_1_peak_amp_rdy_slow_out),
        .inp11(dbg_pha_s0_1_peak_amp_rdy_fast_out),
        .inp12(dbg_pulse_cond_slow_s0_1_impulse),
        .inp13(dbg_pulse_cond_slow_s0_1_rect),
        .inp14(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_impulse),
        .inp15(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_rect),
        .inp16(ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0_outp),
        .inp2(dbg_pulse_cond_slow_s0_1_dc_stab),
        .inp3(dbg_pulse_cond_slow_s0_1_shaper),
        .inp4(dbg_pulse_cond_slow_s0_1_blr),
        .inp5(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_dc_stab),
        .inp6(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_shaper),
        .inp7(ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast_blr),
        .inp8(dbg_pha_s0_1_peak_det_signal_out),
        .inp9(dbg_pha_s0_1_rejectn_out),
        .outp1(ip_mux16_2_if_0_outp1),
        .outp2(ip_mux16_2_if_0_outp2),
        .s00_axi_aclk(s00_axi_aclk_1),
        .s00_axi_araddr(S00_AXI1_1_ARADDR[3:0]),
        .s00_axi_aresetn(s00_axi_aresetn_1),
        .s00_axi_arprot(S00_AXI1_1_ARPROT),
        .s00_axi_arready(S00_AXI1_1_ARREADY),
        .s00_axi_arvalid(S00_AXI1_1_ARVALID),
        .s00_axi_awaddr(S00_AXI1_1_AWADDR[3:0]),
        .s00_axi_awprot(S00_AXI1_1_AWPROT),
        .s00_axi_awready(S00_AXI1_1_AWREADY),
        .s00_axi_awvalid(S00_AXI1_1_AWVALID),
        .s00_axi_bready(S00_AXI1_1_BREADY),
        .s00_axi_bresp(S00_AXI1_1_BRESP),
        .s00_axi_bvalid(S00_AXI1_1_BVALID),
        .s00_axi_rdata(S00_AXI1_1_RDATA),
        .s00_axi_rready(S00_AXI1_1_RREADY),
        .s00_axi_rresp(S00_AXI1_1_RRESP),
        .s00_axi_rvalid(S00_AXI1_1_RVALID),
        .s00_axi_wdata(S00_AXI1_1_WDATA),
        .s00_axi_wready(S00_AXI1_1_WREADY),
        .s00_axi_wstrb(S00_AXI1_1_WSTRB),
        .s00_axi_wvalid(S00_AXI1_1_WVALID),
        .trig(ip_mux16_2_if_0_trig));
  design_1_ip_scope_0_0 ip_scope_0
       (.axibusdomain_aresetn(s00_axi_aresetn_1),
        .axibusdomain_clk(s00_axi_aclk_1),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_1_ARADDR[5:0]),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_1_ARREADY),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_1_ARVALID),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_1_AWADDR[5:0]),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_1_AWREADY),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_1_AWVALID),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_1_BREADY),
        .axibusdomain_s_axi_bresp(axibusdomain_s_axi_1_BRESP),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_1_BVALID),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_1_RDATA),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_1_RREADY),
        .axibusdomain_s_axi_rresp(axibusdomain_s_axi_1_RRESP),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_1_RVALID),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_1_WDATA),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_1_WREADY),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_1_WSTRB),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_1_WVALID),
        .ch1(ip_mux16_2_if_0_outp1),
        .ch2(ip_mux16_2_if_0_outp2),
        .ch_trigger(ip_mux16_2_if_0_trig),
        .full(scope_full),
        .signaldomain_clk(clk_1));
endmodule
