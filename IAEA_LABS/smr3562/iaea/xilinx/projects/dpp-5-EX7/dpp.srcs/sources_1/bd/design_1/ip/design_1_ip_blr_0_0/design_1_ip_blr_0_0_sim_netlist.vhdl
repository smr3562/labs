-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 17:49:23 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_blr_0_0/design_1_ip_blr_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_blr_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_blr_0_0_comparator is
  port (
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_blr_0_0_comparator : entity is "comparator";
end design_1_ip_blr_0_0_comparator;

architecture STRUCTURE of design_1_ip_blr_0_0_comparator is
  signal cmp0_carry_n_1 : STD_LOGIC;
  signal cmp0_carry_n_2 : STD_LOGIC;
  signal cmp0_carry_n_3 : STD_LOGIC;
  signal NLW_cmp0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
cmp0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => CO(0),
      CO(2) => cmp0_carry_n_1,
      CO(1) => cmp0_carry_n_2,
      CO(0) => cmp0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_cmp0_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_blr_0_0_counter is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 );
    cmp0_carry : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    resetn : in STD_LOGIC;
    en : in STD_LOGIC;
    clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_blr_0_0_counter : entity is "counter";
end design_1_ip_blr_0_0_counter;

architecture STRUCTURE of design_1_ip_blr_0_0_counter is
  signal \^q\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal cmp0_carry_i_5_n_0 : STD_LOGIC;
  signal cmp0_carry_i_6_n_0 : STD_LOGIC;
  signal cmp0_carry_i_7_n_0 : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \tmp[9]_i_2_n_0\ : STD_LOGIC;
  signal \tmp[9]_i_3_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \tmp[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \tmp[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \tmp[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tmp[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tmp[6]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tmp[7]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tmp[8]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \tmp[9]_i_1\ : label is "soft_lutpair1";
begin
  Q(9 downto 0) <= \^q\(9 downto 0);
cmp0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => \^q\(9),
      I1 => cmp0_carry(3),
      I2 => cmp0_carry(1),
      I3 => cmp0_carry(2),
      O => S(3)
    );
cmp0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80007F00000000"
    )
        port map (
      I0 => cmp0_carry(1),
      I1 => cmp0_carry(0),
      I2 => cmp0_carry(2),
      I3 => cmp0_carry(3),
      I4 => \^q\(6),
      I5 => cmp0_carry_i_5_n_0,
      O => S(2)
    );
cmp0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E100"
    )
        port map (
      I0 => cmp0_carry(2),
      I1 => cmp0_carry(3),
      I2 => \^q\(3),
      I3 => cmp0_carry_i_6_n_0,
      O => S(1)
    );
cmp0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE000100000000"
    )
        port map (
      I0 => cmp0_carry(0),
      I1 => cmp0_carry(1),
      I2 => cmp0_carry(2),
      I3 => cmp0_carry(3),
      I4 => \^q\(0),
      I5 => cmp0_carry_i_7_n_0,
      O => S(0)
    );
cmp0_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8000000025555"
    )
        port map (
      I0 => \^q\(7),
      I1 => cmp0_carry(2),
      I2 => cmp0_carry(1),
      I3 => cmp0_carry(0),
      I4 => cmp0_carry(3),
      I5 => \^q\(8),
      O => cmp0_carry_i_5_n_0
    );
cmp0_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCC00000000933"
    )
        port map (
      I0 => cmp0_carry(0),
      I1 => \^q\(4),
      I2 => cmp0_carry(1),
      I3 => cmp0_carry(2),
      I4 => cmp0_carry(3),
      I5 => \^q\(5),
      O => cmp0_carry_i_6_n_0
    );
cmp0_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAA8000000025"
    )
        port map (
      I0 => \^q\(1),
      I1 => cmp0_carry(0),
      I2 => cmp0_carry(1),
      I3 => cmp0_carry(2),
      I4 => cmp0_carry(3),
      I5 => \^q\(2),
      O => cmp0_carry_i_7_n_0
    );
\tmp[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => plusOp(0)
    );
\tmp[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => plusOp(1)
    );
\tmp[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      O => plusOp(2)
    );
\tmp[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      O => plusOp(3)
    );
\tmp[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(3),
      I4 => \^q\(4),
      O => plusOp(4)
    );
\tmp[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^q\(4),
      I5 => \^q\(5),
      O => plusOp(5)
    );
\tmp[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tmp[9]_i_3_n_0\,
      I1 => \^q\(6),
      O => plusOp(6)
    );
\tmp[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \tmp[9]_i_3_n_0\,
      I1 => \^q\(6),
      I2 => \^q\(7),
      O => plusOp(7)
    );
\tmp[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^q\(6),
      I1 => \tmp[9]_i_3_n_0\,
      I2 => \^q\(7),
      I3 => \^q\(8),
      O => plusOp(8)
    );
\tmp[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \^q\(7),
      I1 => \tmp[9]_i_3_n_0\,
      I2 => \^q\(6),
      I3 => \^q\(8),
      I4 => \^q\(9),
      O => plusOp(9)
    );
\tmp[9]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => CO(0),
      I1 => resetn,
      O => \tmp[9]_i_2_n_0\
    );
\tmp[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(3),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \^q\(2),
      I5 => \^q\(4),
      O => \tmp[9]_i_3_n_0\
    );
\tmp_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(0),
      Q => \^q\(0)
    );
\tmp_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(1),
      Q => \^q\(1)
    );
\tmp_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(2),
      Q => \^q\(2)
    );
\tmp_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(3),
      Q => \^q\(3)
    );
\tmp_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(4),
      Q => \^q\(4)
    );
\tmp_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(5),
      Q => \^q\(5)
    );
\tmp_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(6),
      Q => \^q\(6)
    );
\tmp_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(7),
      Q => \^q\(7)
    );
\tmp_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(8),
      Q => \^q\(8)
    );
\tmp_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => \tmp[9]_i_2_n_0\,
      D => plusOp(9),
      Q => \^q\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_blr_0_0_sdp_ram is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[23]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[27]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[31]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    clk : in STD_LOGIC;
    en : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 );
    DIADI : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_blr_0_0_sdp_ram : entity is "sdp_ram";
end design_1_ip_blr_0_0_sdp_ram;

architecture STRUCTURE of design_1_ip_blr_0_0_sdp_ram is
  signal dob : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_RAM_reg_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_RAM_reg_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_RAM_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_RAM_reg_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_RAM_reg_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of RAM_reg : label is "p0_d32";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of RAM_reg : label is "p0_d32";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of RAM_reg : label is 32768;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of RAM_reg : label is "RAM";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of RAM_reg : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of RAM_reg : label is 1023;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of RAM_reg : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of RAM_reg : label is 31;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of RAM_reg : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of RAM_reg : label is 1023;
  attribute ram_offset : integer;
  attribute ram_offset of RAM_reg : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of RAM_reg : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of RAM_reg : label is 31;
begin
RAM_reg: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 36,
      READ_WIDTH_B => 36,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 36,
      WRITE_WIDTH_B => 36
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 5) => Q(9 downto 0),
      ADDRARDADDR(4 downto 0) => B"11111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 5) => Q(9 downto 0),
      ADDRBWRADDR(4 downto 0) => B"11111",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_RAM_reg_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_RAM_reg_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DBITERR => NLW_RAM_reg_DBITERR_UNCONNECTED,
      DIADI(31 downto 0) => DIADI(31 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_RAM_reg_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 0) => dob(31 downto 0),
      DOPADOP(3 downto 0) => NLW_RAM_reg_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_RAM_reg_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_RAM_reg_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '1',
      INJECTDBITERR => NLW_RAM_reg_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_RAM_reg_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_RAM_reg_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_RAM_reg_SBITERR_UNCONNECTED,
      WEA(3 downto 0) => B"1111",
      WEBWE(7 downto 0) => B"00000000"
    );
\u320_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(7),
      I1 => dob(7),
      O => \acc32_reg[7]\(3)
    );
\u320_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(6),
      I1 => dob(6),
      O => \acc32_reg[7]\(2)
    );
\u320_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(5),
      I1 => dob(5),
      O => \acc32_reg[7]\(1)
    );
\u320_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(4),
      I1 => dob(4),
      O => \acc32_reg[7]\(0)
    );
\u320_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(11),
      I1 => dob(11),
      O => \acc32_reg[11]\(3)
    );
\u320_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(10),
      I1 => dob(10),
      O => \acc32_reg[11]\(2)
    );
\u320_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(9),
      I1 => dob(9),
      O => \acc32_reg[11]\(1)
    );
\u320_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(8),
      I1 => dob(8),
      O => \acc32_reg[11]\(0)
    );
\u320_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(15),
      I1 => dob(15),
      O => \acc32_reg[15]\(3)
    );
\u320_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(14),
      I1 => dob(14),
      O => \acc32_reg[15]\(2)
    );
\u320_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(13),
      I1 => dob(13),
      O => \acc32_reg[15]\(1)
    );
\u320_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(12),
      I1 => dob(12),
      O => \acc32_reg[15]\(0)
    );
\u320_carry__3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(19),
      I1 => dob(19),
      O => \acc32_reg[19]\(3)
    );
\u320_carry__3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(18),
      I1 => dob(18),
      O => \acc32_reg[19]\(2)
    );
\u320_carry__3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(17),
      I1 => dob(17),
      O => \acc32_reg[19]\(1)
    );
\u320_carry__3_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(16),
      I1 => dob(16),
      O => \acc32_reg[19]\(0)
    );
\u320_carry__4_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(23),
      I1 => dob(23),
      O => \acc32_reg[23]\(3)
    );
\u320_carry__4_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(22),
      I1 => dob(22),
      O => \acc32_reg[23]\(2)
    );
\u320_carry__4_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(21),
      I1 => dob(21),
      O => \acc32_reg[23]\(1)
    );
\u320_carry__4_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(20),
      I1 => dob(20),
      O => \acc32_reg[23]\(0)
    );
\u320_carry__5_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(27),
      I1 => dob(27),
      O => \acc32_reg[27]\(3)
    );
\u320_carry__5_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(26),
      I1 => dob(26),
      O => \acc32_reg[27]\(2)
    );
\u320_carry__5_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(25),
      I1 => dob(25),
      O => \acc32_reg[27]\(1)
    );
\u320_carry__5_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(24),
      I1 => dob(24),
      O => \acc32_reg[27]\(0)
    );
\u320_carry__6_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(31),
      I1 => dob(31),
      O => \acc32_reg[31]\(3)
    );
\u320_carry__6_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(30),
      I1 => dob(30),
      O => \acc32_reg[31]\(2)
    );
\u320_carry__6_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(29),
      I1 => dob(29),
      O => \acc32_reg[31]\(1)
    );
\u320_carry__6_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(28),
      I1 => dob(28),
      O => \acc32_reg[31]\(0)
    );
u320_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(3),
      I1 => dob(3),
      O => S(3)
    );
u320_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(2),
      I1 => dob(2),
      O => S(2)
    );
u320_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(1),
      I1 => dob(1),
      O => S(1)
    );
u320_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => DIADI(0),
      I1 => dob(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_blr_0_0_delay_line is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[23]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[27]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \acc32_reg[31]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    clk : in STD_LOGIC;
    en : in STD_LOGIC;
    DIADI : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    resetn : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_blr_0_0_delay_line : entity is "delay_line";
end design_1_ip_blr_0_0_delay_line;

architecture STRUCTURE of design_1_ip_blr_0_0_delay_line is
  signal counter_clr : STD_LOGIC;
  signal counter_inst_n_0 : STD_LOGIC;
  signal counter_inst_n_1 : STD_LOGIC;
  signal counter_inst_n_2 : STD_LOGIC;
  signal counter_inst_n_3 : STD_LOGIC;
  signal counter_out : STD_LOGIC_VECTOR ( 9 downto 0 );
begin
comparator_inst: entity work.design_1_ip_blr_0_0_comparator
     port map (
      CO(0) => counter_clr,
      S(3) => counter_inst_n_0,
      S(2) => counter_inst_n_1,
      S(1) => counter_inst_n_2,
      S(0) => counter_inst_n_3
    );
counter_inst: entity work.design_1_ip_blr_0_0_counter
     port map (
      CO(0) => counter_clr,
      Q(9 downto 0) => counter_out(9 downto 0),
      S(3) => counter_inst_n_0,
      S(2) => counter_inst_n_1,
      S(1) => counter_inst_n_2,
      S(0) => counter_inst_n_3,
      clk => clk,
      cmp0_carry(3 downto 0) => Q(3 downto 0),
      en => en,
      resetn => resetn
    );
sdp_ram_inst: entity work.design_1_ip_blr_0_0_sdp_ram
     port map (
      DIADI(31 downto 0) => DIADI(31 downto 0),
      Q(9 downto 0) => counter_out(9 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \acc32_reg[11]\(3 downto 0) => \acc32_reg[11]\(3 downto 0),
      \acc32_reg[15]\(3 downto 0) => \acc32_reg[15]\(3 downto 0),
      \acc32_reg[19]\(3 downto 0) => \acc32_reg[19]\(3 downto 0),
      \acc32_reg[23]\(3 downto 0) => \acc32_reg[23]\(3 downto 0),
      \acc32_reg[27]\(3 downto 0) => \acc32_reg[27]\(3 downto 0),
      \acc32_reg[31]\(3 downto 0) => \acc32_reg[31]\(3 downto 0),
      \acc32_reg[7]\(3 downto 0) => \acc32_reg[7]\(3 downto 0),
      clk => clk,
      en => en
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_blr_0_0_blr is
  port (
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    resetn : in STD_LOGIC;
    \en1_carry__0_0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \en1_inferred__0/i__carry__0_0\ : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_blr_0_0_blr : entity is "blr";
end design_1_ip_blr_0_0_blr;

architecture STRUCTURE of design_1_ip_blr_0_0_blr is
  signal \acc32[0]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[0]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[0]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[0]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[12]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[12]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[12]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[12]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[16]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[16]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[16]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[16]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[20]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[20]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[20]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[20]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[24]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[24]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[24]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[24]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[28]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[28]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[28]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[28]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[4]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[4]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[4]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[4]_i_5_n_0\ : STD_LOGIC;
  signal \acc32[8]_i_2_n_0\ : STD_LOGIC;
  signal \acc32[8]_i_3_n_0\ : STD_LOGIC;
  signal \acc32[8]_i_4_n_0\ : STD_LOGIC;
  signal \acc32[8]_i_5_n_0\ : STD_LOGIC;
  signal acc32_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \acc32_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \acc32_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \acc32_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \acc32_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \acc32_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \acc32_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \acc32_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \acc32_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \acc32_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal delay_line_inst_n_0 : STD_LOGIC;
  signal delay_line_inst_n_1 : STD_LOGIC;
  signal delay_line_inst_n_10 : STD_LOGIC;
  signal delay_line_inst_n_11 : STD_LOGIC;
  signal delay_line_inst_n_12 : STD_LOGIC;
  signal delay_line_inst_n_13 : STD_LOGIC;
  signal delay_line_inst_n_14 : STD_LOGIC;
  signal delay_line_inst_n_15 : STD_LOGIC;
  signal delay_line_inst_n_16 : STD_LOGIC;
  signal delay_line_inst_n_17 : STD_LOGIC;
  signal delay_line_inst_n_18 : STD_LOGIC;
  signal delay_line_inst_n_19 : STD_LOGIC;
  signal delay_line_inst_n_2 : STD_LOGIC;
  signal delay_line_inst_n_20 : STD_LOGIC;
  signal delay_line_inst_n_21 : STD_LOGIC;
  signal delay_line_inst_n_22 : STD_LOGIC;
  signal delay_line_inst_n_23 : STD_LOGIC;
  signal delay_line_inst_n_24 : STD_LOGIC;
  signal delay_line_inst_n_25 : STD_LOGIC;
  signal delay_line_inst_n_26 : STD_LOGIC;
  signal delay_line_inst_n_27 : STD_LOGIC;
  signal delay_line_inst_n_28 : STD_LOGIC;
  signal delay_line_inst_n_29 : STD_LOGIC;
  signal delay_line_inst_n_3 : STD_LOGIC;
  signal delay_line_inst_n_30 : STD_LOGIC;
  signal delay_line_inst_n_31 : STD_LOGIC;
  signal delay_line_inst_n_4 : STD_LOGIC;
  signal delay_line_inst_n_5 : STD_LOGIC;
  signal delay_line_inst_n_6 : STD_LOGIC;
  signal delay_line_inst_n_7 : STD_LOGIC;
  signal delay_line_inst_n_8 : STD_LOGIC;
  signal delay_line_inst_n_9 : STD_LOGIC;
  signal en : STD_LOGIC;
  signal en0 : STD_LOGIC;
  signal en1 : STD_LOGIC;
  signal en10_in : STD_LOGIC;
  signal \en1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \en1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \en1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \en1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \en1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \en1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \en1_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \en1_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \en1_carry__0_n_1\ : STD_LOGIC;
  signal \en1_carry__0_n_2\ : STD_LOGIC;
  signal \en1_carry__0_n_3\ : STD_LOGIC;
  signal en1_carry_i_1_n_0 : STD_LOGIC;
  signal en1_carry_i_2_n_0 : STD_LOGIC;
  signal en1_carry_i_3_n_0 : STD_LOGIC;
  signal en1_carry_i_4_n_0 : STD_LOGIC;
  signal en1_carry_i_5_n_0 : STD_LOGIC;
  signal en1_carry_i_6_n_0 : STD_LOGIC;
  signal en1_carry_i_7_n_0 : STD_LOGIC;
  signal en1_carry_i_8_n_0 : STD_LOGIC;
  signal en1_carry_n_0 : STD_LOGIC;
  signal en1_carry_n_1 : STD_LOGIC;
  signal en1_carry_n_2 : STD_LOGIC;
  signal en1_carry_n_3 : STD_LOGIC;
  signal \en1_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \en1_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \en1_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \en1_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \en1_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \en1_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \en1_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal en_i_2_n_0 : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal p_2_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \result3_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__0_n_0\ : STD_LOGIC;
  signal \result3_carry__0_n_1\ : STD_LOGIC;
  signal \result3_carry__0_n_2\ : STD_LOGIC;
  signal \result3_carry__0_n_3\ : STD_LOGIC;
  signal \result3_carry__0_n_4\ : STD_LOGIC;
  signal \result3_carry__0_n_5\ : STD_LOGIC;
  signal \result3_carry__0_n_6\ : STD_LOGIC;
  signal \result3_carry__0_n_7\ : STD_LOGIC;
  signal \result3_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__1_n_0\ : STD_LOGIC;
  signal \result3_carry__1_n_1\ : STD_LOGIC;
  signal \result3_carry__1_n_2\ : STD_LOGIC;
  signal \result3_carry__1_n_3\ : STD_LOGIC;
  signal \result3_carry__1_n_4\ : STD_LOGIC;
  signal \result3_carry__1_n_5\ : STD_LOGIC;
  signal \result3_carry__1_n_6\ : STD_LOGIC;
  signal \result3_carry__1_n_7\ : STD_LOGIC;
  signal \result3_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__2_n_0\ : STD_LOGIC;
  signal \result3_carry__2_n_1\ : STD_LOGIC;
  signal \result3_carry__2_n_2\ : STD_LOGIC;
  signal \result3_carry__2_n_3\ : STD_LOGIC;
  signal \result3_carry__2_n_5\ : STD_LOGIC;
  signal \result3_carry__2_n_6\ : STD_LOGIC;
  signal \result3_carry__2_n_7\ : STD_LOGIC;
  signal \result3_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_14_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_15_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_16_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__3_n_0\ : STD_LOGIC;
  signal \result3_carry__3_n_1\ : STD_LOGIC;
  signal \result3_carry__3_n_2\ : STD_LOGIC;
  signal \result3_carry__3_n_3\ : STD_LOGIC;
  signal \result3_carry__4_i_13_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_14_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_15_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__4_n_0\ : STD_LOGIC;
  signal \result3_carry__4_n_1\ : STD_LOGIC;
  signal \result3_carry__4_n_2\ : STD_LOGIC;
  signal \result3_carry__4_n_3\ : STD_LOGIC;
  signal \result3_carry__5_i_10_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_11_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_14_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_15_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__5_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__5_n_0\ : STD_LOGIC;
  signal \result3_carry__5_n_1\ : STD_LOGIC;
  signal \result3_carry__5_n_2\ : STD_LOGIC;
  signal \result3_carry__5_n_3\ : STD_LOGIC;
  signal \result3_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_4_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_5_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_6_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_7_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_8_n_0\ : STD_LOGIC;
  signal \result3_carry__6_i_9_n_0\ : STD_LOGIC;
  signal \result3_carry__6_n_1\ : STD_LOGIC;
  signal \result3_carry__6_n_2\ : STD_LOGIC;
  signal \result3_carry__6_n_3\ : STD_LOGIC;
  signal result3_carry_i_10_n_0 : STD_LOGIC;
  signal result3_carry_i_11_n_0 : STD_LOGIC;
  signal result3_carry_i_12_n_0 : STD_LOGIC;
  signal result3_carry_i_13_n_0 : STD_LOGIC;
  signal result3_carry_i_1_n_0 : STD_LOGIC;
  signal result3_carry_i_2_n_0 : STD_LOGIC;
  signal result3_carry_i_3_n_0 : STD_LOGIC;
  signal result3_carry_i_4_n_0 : STD_LOGIC;
  signal result3_carry_i_5_n_0 : STD_LOGIC;
  signal result3_carry_i_6_n_0 : STD_LOGIC;
  signal result3_carry_i_7_n_0 : STD_LOGIC;
  signal result3_carry_i_8_n_0 : STD_LOGIC;
  signal result3_carry_i_9_n_0 : STD_LOGIC;
  signal result3_carry_n_0 : STD_LOGIC;
  signal result3_carry_n_1 : STD_LOGIC;
  signal result3_carry_n_2 : STD_LOGIC;
  signal result3_carry_n_3 : STD_LOGIC;
  signal result3_carry_n_4 : STD_LOGIC;
  signal result3_carry_n_5 : STD_LOGIC;
  signal result3_carry_n_6 : STD_LOGIC;
  signal result3_carry_n_7 : STD_LOGIC;
  signal \result_reg[0]_C_n_0\ : STD_LOGIC;
  signal \result_reg[0]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[0]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[0]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[0]_P_n_0\ : STD_LOGIC;
  signal \result_reg[10]_C_n_0\ : STD_LOGIC;
  signal \result_reg[10]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[10]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[10]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[10]_P_n_0\ : STD_LOGIC;
  signal \result_reg[11]_C_n_0\ : STD_LOGIC;
  signal \result_reg[11]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[11]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[11]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[11]_P_n_0\ : STD_LOGIC;
  signal \result_reg[12]_C_n_0\ : STD_LOGIC;
  signal \result_reg[12]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[12]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[12]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[12]_P_n_0\ : STD_LOGIC;
  signal \result_reg[13]_C_n_0\ : STD_LOGIC;
  signal \result_reg[13]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[13]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[13]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[13]_P_n_0\ : STD_LOGIC;
  signal \result_reg[14]_C_n_0\ : STD_LOGIC;
  signal \result_reg[14]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[14]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[14]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[14]_P_n_0\ : STD_LOGIC;
  signal \result_reg[1]_C_n_0\ : STD_LOGIC;
  signal \result_reg[1]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[1]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[1]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[1]_P_n_0\ : STD_LOGIC;
  signal \result_reg[2]_C_n_0\ : STD_LOGIC;
  signal \result_reg[2]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[2]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[2]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[2]_P_n_0\ : STD_LOGIC;
  signal \result_reg[31]_C_n_0\ : STD_LOGIC;
  signal \result_reg[31]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[31]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[31]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[31]_P_n_0\ : STD_LOGIC;
  signal \result_reg[3]_C_n_0\ : STD_LOGIC;
  signal \result_reg[3]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[3]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[3]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[3]_P_n_0\ : STD_LOGIC;
  signal \result_reg[4]_C_n_0\ : STD_LOGIC;
  signal \result_reg[4]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[4]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[4]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[4]_P_n_0\ : STD_LOGIC;
  signal \result_reg[5]_C_n_0\ : STD_LOGIC;
  signal \result_reg[5]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[5]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[5]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[5]_P_n_0\ : STD_LOGIC;
  signal \result_reg[6]_C_n_0\ : STD_LOGIC;
  signal \result_reg[6]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[6]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[6]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[6]_P_n_0\ : STD_LOGIC;
  signal \result_reg[7]_C_n_0\ : STD_LOGIC;
  signal \result_reg[7]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[7]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[7]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[7]_P_n_0\ : STD_LOGIC;
  signal \result_reg[8]_C_n_0\ : STD_LOGIC;
  signal \result_reg[8]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[8]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[8]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[8]_P_n_0\ : STD_LOGIC;
  signal \result_reg[9]_C_n_0\ : STD_LOGIC;
  signal \result_reg[9]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[9]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \result_reg[9]_LDC_n_0\ : STD_LOGIC;
  signal \result_reg[9]_P_n_0\ : STD_LOGIC;
  signal u32 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal u320 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \u320_carry__0_n_0\ : STD_LOGIC;
  signal \u320_carry__0_n_1\ : STD_LOGIC;
  signal \u320_carry__0_n_2\ : STD_LOGIC;
  signal \u320_carry__0_n_3\ : STD_LOGIC;
  signal \u320_carry__1_n_0\ : STD_LOGIC;
  signal \u320_carry__1_n_1\ : STD_LOGIC;
  signal \u320_carry__1_n_2\ : STD_LOGIC;
  signal \u320_carry__1_n_3\ : STD_LOGIC;
  signal \u320_carry__2_n_0\ : STD_LOGIC;
  signal \u320_carry__2_n_1\ : STD_LOGIC;
  signal \u320_carry__2_n_2\ : STD_LOGIC;
  signal \u320_carry__2_n_3\ : STD_LOGIC;
  signal \u320_carry__3_n_0\ : STD_LOGIC;
  signal \u320_carry__3_n_1\ : STD_LOGIC;
  signal \u320_carry__3_n_2\ : STD_LOGIC;
  signal \u320_carry__3_n_3\ : STD_LOGIC;
  signal \u320_carry__4_n_0\ : STD_LOGIC;
  signal \u320_carry__4_n_1\ : STD_LOGIC;
  signal \u320_carry__4_n_2\ : STD_LOGIC;
  signal \u320_carry__4_n_3\ : STD_LOGIC;
  signal \u320_carry__5_n_0\ : STD_LOGIC;
  signal \u320_carry__5_n_1\ : STD_LOGIC;
  signal \u320_carry__5_n_2\ : STD_LOGIC;
  signal \u320_carry__5_n_3\ : STD_LOGIC;
  signal \u320_carry__6_n_1\ : STD_LOGIC;
  signal \u320_carry__6_n_2\ : STD_LOGIC;
  signal \u320_carry__6_n_3\ : STD_LOGIC;
  signal u320_carry_n_0 : STD_LOGIC;
  signal u320_carry_n_1 : STD_LOGIC;
  signal u320_carry_n_2 : STD_LOGIC;
  signal u320_carry_n_3 : STD_LOGIC;
  signal v32 : STD_LOGIC_VECTOR ( 25 downto 15 );
  signal \NLW_acc32_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_en1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_en1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_en1_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_en1_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_result3_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_result3_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_result3_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_result3_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_result3_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_u320_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \result3_carry__0_i_7\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \result3_carry__0_i_8\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \result3_carry__1_i_7\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \result3_carry__1_i_8\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \result3_carry__2_i_10\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \result3_carry__2_i_11\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \result3_carry__4_i_14\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \result3_carry__4_i_15\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \result3_carry__5_i_10\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \result3_carry__5_i_15\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of result3_carry_i_7 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of result3_carry_i_9 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \result[0]_C_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \result[10]_C_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \result[11]_C_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \result[12]_C_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \result[13]_C_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \result[14]_C_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \result[1]_C_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \result[2]_C_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \result[31]_C_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \result[3]_C_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \result[4]_C_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \result[5]_C_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \result[6]_C_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \result[7]_C_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \result[8]_C_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \result[9]_C_i_1\ : label is "soft_lutpair15";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \result_reg[0]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[10]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[11]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[12]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[13]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[14]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[1]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[2]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[31]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[3]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[4]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[5]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[6]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[7]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[8]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \result_reg[9]_LDC\ : label is "LDC";
begin
\acc32[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(3),
      I1 => acc32_reg(3),
      O => \acc32[0]_i_2_n_0\
    );
\acc32[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(2),
      I1 => acc32_reg(2),
      O => \acc32[0]_i_3_n_0\
    );
\acc32[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(1),
      I1 => acc32_reg(1),
      O => \acc32[0]_i_4_n_0\
    );
\acc32[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(0),
      I1 => acc32_reg(0),
      O => \acc32[0]_i_5_n_0\
    );
\acc32[12]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(15),
      O => \acc32[12]_i_2_n_0\
    );
\acc32[12]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(14),
      I1 => acc32_reg(14),
      O => \acc32[12]_i_3_n_0\
    );
\acc32[12]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(13),
      I1 => acc32_reg(13),
      O => \acc32[12]_i_4_n_0\
    );
\acc32[12]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(12),
      I1 => acc32_reg(12),
      O => \acc32[12]_i_5_n_0\
    );
\acc32[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(19),
      O => \acc32[16]_i_2_n_0\
    );
\acc32[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(18),
      O => \acc32[16]_i_3_n_0\
    );
\acc32[16]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(17),
      O => \acc32[16]_i_4_n_0\
    );
\acc32[16]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(16),
      O => \acc32[16]_i_5_n_0\
    );
\acc32[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(23),
      O => \acc32[20]_i_2_n_0\
    );
\acc32[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(22),
      O => \acc32[20]_i_3_n_0\
    );
\acc32[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(21),
      O => \acc32[20]_i_4_n_0\
    );
\acc32[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(20),
      O => \acc32[20]_i_5_n_0\
    );
\acc32[24]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(27),
      O => \acc32[24]_i_2_n_0\
    );
\acc32[24]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(26),
      O => \acc32[24]_i_3_n_0\
    );
\acc32[24]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(25),
      O => \acc32[24]_i_4_n_0\
    );
\acc32[24]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(24),
      O => \acc32[24]_i_5_n_0\
    );
\acc32[28]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(31),
      O => \acc32[28]_i_2_n_0\
    );
\acc32[28]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(30),
      O => \acc32[28]_i_3_n_0\
    );
\acc32[28]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(29),
      O => \acc32[28]_i_4_n_0\
    );
\acc32[28]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(15),
      I1 => acc32_reg(28),
      O => \acc32[28]_i_5_n_0\
    );
\acc32[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(7),
      I1 => acc32_reg(7),
      O => \acc32[4]_i_2_n_0\
    );
\acc32[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(6),
      I1 => acc32_reg(6),
      O => \acc32[4]_i_3_n_0\
    );
\acc32[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(5),
      I1 => acc32_reg(5),
      O => \acc32[4]_i_4_n_0\
    );
\acc32[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(4),
      I1 => acc32_reg(4),
      O => \acc32[4]_i_5_n_0\
    );
\acc32[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(11),
      I1 => acc32_reg(11),
      O => \acc32[8]_i_2_n_0\
    );
\acc32[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(10),
      I1 => acc32_reg(10),
      O => \acc32[8]_i_3_n_0\
    );
\acc32[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(9),
      I1 => acc32_reg(9),
      O => \acc32[8]_i_4_n_0\
    );
\acc32[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => x(8),
      I1 => acc32_reg(8),
      O => \acc32[8]_i_5_n_0\
    );
\acc32_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[0]_i_1_n_7\,
      Q => acc32_reg(0)
    );
\acc32_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \acc32_reg[0]_i_1_n_0\,
      CO(2) => \acc32_reg[0]_i_1_n_1\,
      CO(1) => \acc32_reg[0]_i_1_n_2\,
      CO(0) => \acc32_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => x(3 downto 0),
      O(3) => \acc32_reg[0]_i_1_n_4\,
      O(2) => \acc32_reg[0]_i_1_n_5\,
      O(1) => \acc32_reg[0]_i_1_n_6\,
      O(0) => \acc32_reg[0]_i_1_n_7\,
      S(3) => \acc32[0]_i_2_n_0\,
      S(2) => \acc32[0]_i_3_n_0\,
      S(1) => \acc32[0]_i_4_n_0\,
      S(0) => \acc32[0]_i_5_n_0\
    );
\acc32_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[8]_i_1_n_5\,
      Q => acc32_reg(10)
    );
\acc32_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[8]_i_1_n_4\,
      Q => acc32_reg(11)
    );
\acc32_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[12]_i_1_n_7\,
      Q => acc32_reg(12)
    );
\acc32_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[8]_i_1_n_0\,
      CO(3) => \acc32_reg[12]_i_1_n_0\,
      CO(2) => \acc32_reg[12]_i_1_n_1\,
      CO(1) => \acc32_reg[12]_i_1_n_2\,
      CO(0) => \acc32_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => x(15 downto 12),
      O(3) => \acc32_reg[12]_i_1_n_4\,
      O(2) => \acc32_reg[12]_i_1_n_5\,
      O(1) => \acc32_reg[12]_i_1_n_6\,
      O(0) => \acc32_reg[12]_i_1_n_7\,
      S(3) => \acc32[12]_i_2_n_0\,
      S(2) => \acc32[12]_i_3_n_0\,
      S(1) => \acc32[12]_i_4_n_0\,
      S(0) => \acc32[12]_i_5_n_0\
    );
\acc32_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[12]_i_1_n_6\,
      Q => acc32_reg(13)
    );
\acc32_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[12]_i_1_n_5\,
      Q => acc32_reg(14)
    );
\acc32_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[12]_i_1_n_4\,
      Q => acc32_reg(15)
    );
\acc32_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[16]_i_1_n_7\,
      Q => acc32_reg(16)
    );
\acc32_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[12]_i_1_n_0\,
      CO(3) => \acc32_reg[16]_i_1_n_0\,
      CO(2) => \acc32_reg[16]_i_1_n_1\,
      CO(1) => \acc32_reg[16]_i_1_n_2\,
      CO(0) => \acc32_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => x(15),
      DI(2) => x(15),
      DI(1) => x(15),
      DI(0) => x(15),
      O(3) => \acc32_reg[16]_i_1_n_4\,
      O(2) => \acc32_reg[16]_i_1_n_5\,
      O(1) => \acc32_reg[16]_i_1_n_6\,
      O(0) => \acc32_reg[16]_i_1_n_7\,
      S(3) => \acc32[16]_i_2_n_0\,
      S(2) => \acc32[16]_i_3_n_0\,
      S(1) => \acc32[16]_i_4_n_0\,
      S(0) => \acc32[16]_i_5_n_0\
    );
\acc32_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[16]_i_1_n_6\,
      Q => acc32_reg(17)
    );
\acc32_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[16]_i_1_n_5\,
      Q => acc32_reg(18)
    );
\acc32_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[16]_i_1_n_4\,
      Q => acc32_reg(19)
    );
\acc32_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[0]_i_1_n_6\,
      Q => acc32_reg(1)
    );
\acc32_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[20]_i_1_n_7\,
      Q => acc32_reg(20)
    );
\acc32_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[16]_i_1_n_0\,
      CO(3) => \acc32_reg[20]_i_1_n_0\,
      CO(2) => \acc32_reg[20]_i_1_n_1\,
      CO(1) => \acc32_reg[20]_i_1_n_2\,
      CO(0) => \acc32_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => x(15),
      DI(2) => x(15),
      DI(1) => x(15),
      DI(0) => x(15),
      O(3) => \acc32_reg[20]_i_1_n_4\,
      O(2) => \acc32_reg[20]_i_1_n_5\,
      O(1) => \acc32_reg[20]_i_1_n_6\,
      O(0) => \acc32_reg[20]_i_1_n_7\,
      S(3) => \acc32[20]_i_2_n_0\,
      S(2) => \acc32[20]_i_3_n_0\,
      S(1) => \acc32[20]_i_4_n_0\,
      S(0) => \acc32[20]_i_5_n_0\
    );
\acc32_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[20]_i_1_n_6\,
      Q => acc32_reg(21)
    );
\acc32_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[20]_i_1_n_5\,
      Q => acc32_reg(22)
    );
\acc32_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[20]_i_1_n_4\,
      Q => acc32_reg(23)
    );
\acc32_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[24]_i_1_n_7\,
      Q => acc32_reg(24)
    );
\acc32_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[20]_i_1_n_0\,
      CO(3) => \acc32_reg[24]_i_1_n_0\,
      CO(2) => \acc32_reg[24]_i_1_n_1\,
      CO(1) => \acc32_reg[24]_i_1_n_2\,
      CO(0) => \acc32_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => x(15),
      DI(2) => x(15),
      DI(1) => x(15),
      DI(0) => x(15),
      O(3) => \acc32_reg[24]_i_1_n_4\,
      O(2) => \acc32_reg[24]_i_1_n_5\,
      O(1) => \acc32_reg[24]_i_1_n_6\,
      O(0) => \acc32_reg[24]_i_1_n_7\,
      S(3) => \acc32[24]_i_2_n_0\,
      S(2) => \acc32[24]_i_3_n_0\,
      S(1) => \acc32[24]_i_4_n_0\,
      S(0) => \acc32[24]_i_5_n_0\
    );
\acc32_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[24]_i_1_n_6\,
      Q => acc32_reg(25)
    );
\acc32_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[24]_i_1_n_5\,
      Q => acc32_reg(26)
    );
\acc32_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[24]_i_1_n_4\,
      Q => acc32_reg(27)
    );
\acc32_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[28]_i_1_n_7\,
      Q => acc32_reg(28)
    );
\acc32_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[24]_i_1_n_0\,
      CO(3) => \NLW_acc32_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \acc32_reg[28]_i_1_n_1\,
      CO(1) => \acc32_reg[28]_i_1_n_2\,
      CO(0) => \acc32_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => x(15),
      DI(1) => x(15),
      DI(0) => x(15),
      O(3) => \acc32_reg[28]_i_1_n_4\,
      O(2) => \acc32_reg[28]_i_1_n_5\,
      O(1) => \acc32_reg[28]_i_1_n_6\,
      O(0) => \acc32_reg[28]_i_1_n_7\,
      S(3) => \acc32[28]_i_2_n_0\,
      S(2) => \acc32[28]_i_3_n_0\,
      S(1) => \acc32[28]_i_4_n_0\,
      S(0) => \acc32[28]_i_5_n_0\
    );
\acc32_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[28]_i_1_n_6\,
      Q => acc32_reg(29)
    );
\acc32_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[0]_i_1_n_5\,
      Q => acc32_reg(2)
    );
\acc32_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[28]_i_1_n_5\,
      Q => acc32_reg(30)
    );
\acc32_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[28]_i_1_n_4\,
      Q => acc32_reg(31)
    );
\acc32_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[0]_i_1_n_4\,
      Q => acc32_reg(3)
    );
\acc32_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[4]_i_1_n_7\,
      Q => acc32_reg(4)
    );
\acc32_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[0]_i_1_n_0\,
      CO(3) => \acc32_reg[4]_i_1_n_0\,
      CO(2) => \acc32_reg[4]_i_1_n_1\,
      CO(1) => \acc32_reg[4]_i_1_n_2\,
      CO(0) => \acc32_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => x(7 downto 4),
      O(3) => \acc32_reg[4]_i_1_n_4\,
      O(2) => \acc32_reg[4]_i_1_n_5\,
      O(1) => \acc32_reg[4]_i_1_n_6\,
      O(0) => \acc32_reg[4]_i_1_n_7\,
      S(3) => \acc32[4]_i_2_n_0\,
      S(2) => \acc32[4]_i_3_n_0\,
      S(1) => \acc32[4]_i_4_n_0\,
      S(0) => \acc32[4]_i_5_n_0\
    );
\acc32_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[4]_i_1_n_6\,
      Q => acc32_reg(5)
    );
\acc32_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[4]_i_1_n_5\,
      Q => acc32_reg(6)
    );
\acc32_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[4]_i_1_n_4\,
      Q => acc32_reg(7)
    );
\acc32_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[8]_i_1_n_7\,
      Q => acc32_reg(8)
    );
\acc32_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc32_reg[4]_i_1_n_0\,
      CO(3) => \acc32_reg[8]_i_1_n_0\,
      CO(2) => \acc32_reg[8]_i_1_n_1\,
      CO(1) => \acc32_reg[8]_i_1_n_2\,
      CO(0) => \acc32_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => x(11 downto 8),
      O(3) => \acc32_reg[8]_i_1_n_4\,
      O(2) => \acc32_reg[8]_i_1_n_5\,
      O(1) => \acc32_reg[8]_i_1_n_6\,
      O(0) => \acc32_reg[8]_i_1_n_7\,
      S(3) => \acc32[8]_i_2_n_0\,
      S(2) => \acc32[8]_i_3_n_0\,
      S(1) => \acc32[8]_i_4_n_0\,
      S(0) => \acc32[8]_i_5_n_0\
    );
\acc32_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => \acc32_reg[8]_i_1_n_6\,
      Q => acc32_reg(9)
    );
delay_line_inst: entity work.design_1_ip_blr_0_0_delay_line
     port map (
      DIADI(31 downto 0) => acc32_reg(31 downto 0),
      Q(3 downto 0) => Q(3 downto 0),
      S(3) => delay_line_inst_n_0,
      S(2) => delay_line_inst_n_1,
      S(1) => delay_line_inst_n_2,
      S(0) => delay_line_inst_n_3,
      \acc32_reg[11]\(3) => delay_line_inst_n_8,
      \acc32_reg[11]\(2) => delay_line_inst_n_9,
      \acc32_reg[11]\(1) => delay_line_inst_n_10,
      \acc32_reg[11]\(0) => delay_line_inst_n_11,
      \acc32_reg[15]\(3) => delay_line_inst_n_12,
      \acc32_reg[15]\(2) => delay_line_inst_n_13,
      \acc32_reg[15]\(1) => delay_line_inst_n_14,
      \acc32_reg[15]\(0) => delay_line_inst_n_15,
      \acc32_reg[19]\(3) => delay_line_inst_n_16,
      \acc32_reg[19]\(2) => delay_line_inst_n_17,
      \acc32_reg[19]\(1) => delay_line_inst_n_18,
      \acc32_reg[19]\(0) => delay_line_inst_n_19,
      \acc32_reg[23]\(3) => delay_line_inst_n_20,
      \acc32_reg[23]\(2) => delay_line_inst_n_21,
      \acc32_reg[23]\(1) => delay_line_inst_n_22,
      \acc32_reg[23]\(0) => delay_line_inst_n_23,
      \acc32_reg[27]\(3) => delay_line_inst_n_24,
      \acc32_reg[27]\(2) => delay_line_inst_n_25,
      \acc32_reg[27]\(1) => delay_line_inst_n_26,
      \acc32_reg[27]\(0) => delay_line_inst_n_27,
      \acc32_reg[31]\(3) => delay_line_inst_n_28,
      \acc32_reg[31]\(2) => delay_line_inst_n_29,
      \acc32_reg[31]\(1) => delay_line_inst_n_30,
      \acc32_reg[31]\(0) => delay_line_inst_n_31,
      \acc32_reg[7]\(3) => delay_line_inst_n_4,
      \acc32_reg[7]\(2) => delay_line_inst_n_5,
      \acc32_reg[7]\(1) => delay_line_inst_n_6,
      \acc32_reg[7]\(0) => delay_line_inst_n_7,
      clk => clk,
      en => en,
      resetn => resetn
    );
en1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => en1_carry_n_0,
      CO(2) => en1_carry_n_1,
      CO(1) => en1_carry_n_2,
      CO(0) => en1_carry_n_3,
      CYINIT => '0',
      DI(3) => en1_carry_i_1_n_0,
      DI(2) => en1_carry_i_2_n_0,
      DI(1) => en1_carry_i_3_n_0,
      DI(0) => en1_carry_i_4_n_0,
      O(3 downto 0) => NLW_en1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => en1_carry_i_5_n_0,
      S(2) => en1_carry_i_6_n_0,
      S(1) => en1_carry_i_7_n_0,
      S(0) => en1_carry_i_8_n_0
    );
\en1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => en1_carry_n_0,
      CO(3) => en1,
      CO(2) => \en1_carry__0_n_1\,
      CO(1) => \en1_carry__0_n_2\,
      CO(0) => \en1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \en1_carry__0_i_1_n_0\,
      DI(2) => \en1_carry__0_i_2_n_0\,
      DI(1) => \en1_carry__0_i_3_n_0\,
      DI(0) => \en1_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_en1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \en1_carry__0_i_5_n_0\,
      S(2) => \en1_carry__0_i_6_n_0\,
      S(1) => \en1_carry__0_i_7_n_0\,
      S(0) => \en1_carry__0_i_8_n_0\
    );
\en1_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => x(14),
      I1 => \en1_carry__0_0\(14),
      I2 => x(15),
      I3 => \en1_carry__0_0\(15),
      O => \en1_carry__0_i_1_n_0\
    );
\en1_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => x(12),
      I1 => \en1_carry__0_0\(12),
      I2 => \en1_carry__0_0\(13),
      I3 => x(13),
      O => \en1_carry__0_i_2_n_0\
    );
\en1_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => x(10),
      I1 => \en1_carry__0_0\(10),
      I2 => \en1_carry__0_0\(11),
      I3 => x(11),
      O => \en1_carry__0_i_3_n_0\
    );
\en1_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => x(8),
      I1 => \en1_carry__0_0\(8),
      I2 => \en1_carry__0_0\(9),
      I3 => x(9),
      O => \en1_carry__0_i_4_n_0\
    );
\en1_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => x(14),
      I1 => \en1_carry__0_0\(14),
      I2 => \en1_carry__0_0\(15),
      I3 => x(15),
      O => \en1_carry__0_i_5_n_0\
    );
\en1_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => x(12),
      I1 => \en1_carry__0_0\(12),
      I2 => x(13),
      I3 => \en1_carry__0_0\(13),
      O => \en1_carry__0_i_6_n_0\
    );
\en1_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => x(10),
      I1 => \en1_carry__0_0\(10),
      I2 => x(11),
      I3 => \en1_carry__0_0\(11),
      O => \en1_carry__0_i_7_n_0\
    );
\en1_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => x(8),
      I1 => \en1_carry__0_0\(8),
      I2 => x(9),
      I3 => \en1_carry__0_0\(9),
      O => \en1_carry__0_i_8_n_0\
    );
en1_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => x(6),
      I1 => \en1_carry__0_0\(6),
      I2 => \en1_carry__0_0\(7),
      I3 => x(7),
      O => en1_carry_i_1_n_0
    );
en1_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => x(4),
      I1 => \en1_carry__0_0\(4),
      I2 => \en1_carry__0_0\(5),
      I3 => x(5),
      O => en1_carry_i_2_n_0
    );
en1_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => x(2),
      I1 => \en1_carry__0_0\(2),
      I2 => \en1_carry__0_0\(3),
      I3 => x(3),
      O => en1_carry_i_3_n_0
    );
en1_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => x(0),
      I1 => \en1_carry__0_0\(0),
      I2 => \en1_carry__0_0\(1),
      I3 => x(1),
      O => en1_carry_i_4_n_0
    );
en1_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => x(6),
      I1 => \en1_carry__0_0\(6),
      I2 => x(7),
      I3 => \en1_carry__0_0\(7),
      O => en1_carry_i_5_n_0
    );
en1_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => x(4),
      I1 => \en1_carry__0_0\(4),
      I2 => x(5),
      I3 => \en1_carry__0_0\(5),
      O => en1_carry_i_6_n_0
    );
en1_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => x(2),
      I1 => \en1_carry__0_0\(2),
      I2 => x(3),
      I3 => \en1_carry__0_0\(3),
      O => en1_carry_i_7_n_0
    );
en1_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => x(0),
      I1 => \en1_carry__0_0\(0),
      I2 => x(1),
      I3 => \en1_carry__0_0\(1),
      O => en1_carry_i_8_n_0
    );
\en1_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \en1_inferred__0/i__carry_n_0\,
      CO(2) => \en1_inferred__0/i__carry_n_1\,
      CO(1) => \en1_inferred__0/i__carry_n_2\,
      CO(0) => \en1_inferred__0/i__carry_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry_i_1_n_0\,
      DI(2) => \i__carry_i_2_n_0\,
      DI(1) => \i__carry_i_3_n_0\,
      DI(0) => \i__carry_i_4_n_0\,
      O(3 downto 0) => \NLW_en1_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_5_n_0\,
      S(2) => \i__carry_i_6_n_0\,
      S(1) => \i__carry_i_7_n_0\,
      S(0) => \i__carry_i_8_n_0\
    );
\en1_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \en1_inferred__0/i__carry_n_0\,
      CO(3) => en10_in,
      CO(2) => \en1_inferred__0/i__carry__0_n_1\,
      CO(1) => \en1_inferred__0/i__carry__0_n_2\,
      CO(0) => \en1_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry__0_i_1_n_0\,
      DI(2) => \i__carry__0_i_2_n_0\,
      DI(1) => \i__carry__0_i_3_n_0\,
      DI(0) => \i__carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_en1_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__0_i_5_n_0\,
      S(2) => \i__carry__0_i_6_n_0\,
      S(1) => \i__carry__0_i_7_n_0\,
      S(0) => \i__carry__0_i_8_n_0\
    );
en_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => en10_in,
      I1 => en1,
      O => en0
    );
en_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => resetn,
      I1 => Q(4),
      O => en_i_2_n_0
    );
en_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => en_i_2_n_0,
      D => en0,
      Q => en
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(14),
      I1 => x(14),
      I2 => \en1_inferred__0/i__carry__0_0\(15),
      I3 => x(15),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(12),
      I1 => x(12),
      I2 => x(13),
      I3 => \en1_inferred__0/i__carry__0_0\(13),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(10),
      I1 => x(10),
      I2 => x(11),
      I3 => \en1_inferred__0/i__carry__0_0\(11),
      O => \i__carry__0_i_3_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(8),
      I1 => x(8),
      I2 => x(9),
      I3 => \en1_inferred__0/i__carry__0_0\(9),
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(14),
      I1 => x(14),
      I2 => x(15),
      I3 => \en1_inferred__0/i__carry__0_0\(15),
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(12),
      I1 => x(12),
      I2 => \en1_inferred__0/i__carry__0_0\(13),
      I3 => x(13),
      O => \i__carry__0_i_6_n_0\
    );
\i__carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(10),
      I1 => x(10),
      I2 => \en1_inferred__0/i__carry__0_0\(11),
      I3 => x(11),
      O => \i__carry__0_i_7_n_0\
    );
\i__carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(8),
      I1 => x(8),
      I2 => \en1_inferred__0/i__carry__0_0\(9),
      I3 => x(9),
      O => \i__carry__0_i_8_n_0\
    );
\i__carry_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(6),
      I1 => x(6),
      I2 => x(7),
      I3 => \en1_inferred__0/i__carry__0_0\(7),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(4),
      I1 => x(4),
      I2 => x(5),
      I3 => \en1_inferred__0/i__carry__0_0\(5),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(2),
      I1 => x(2),
      I2 => x(3),
      I3 => \en1_inferred__0/i__carry__0_0\(3),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(0),
      I1 => x(0),
      I2 => x(1),
      I3 => \en1_inferred__0/i__carry__0_0\(1),
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(6),
      I1 => x(6),
      I2 => \en1_inferred__0/i__carry__0_0\(7),
      I3 => x(7),
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(4),
      I1 => x(4),
      I2 => \en1_inferred__0/i__carry__0_0\(5),
      I3 => x(5),
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(2),
      I1 => x(2),
      I2 => \en1_inferred__0/i__carry__0_0\(3),
      I3 => x(3),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \en1_inferred__0/i__carry__0_0\(0),
      I1 => x(0),
      I2 => \en1_inferred__0/i__carry__0_0\(1),
      I3 => x(1),
      O => \i__carry_i_8_n_0\
    );
result3_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result3_carry_n_0,
      CO(2) => result3_carry_n_1,
      CO(1) => result3_carry_n_2,
      CO(0) => result3_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => x(3 downto 0),
      O(3) => result3_carry_n_4,
      O(2) => result3_carry_n_5,
      O(1) => result3_carry_n_6,
      O(0) => result3_carry_n_7,
      S(3) => result3_carry_i_1_n_0,
      S(2) => result3_carry_i_2_n_0,
      S(1) => result3_carry_i_3_n_0,
      S(0) => result3_carry_i_4_n_0
    );
\result3_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => result3_carry_n_0,
      CO(3) => \result3_carry__0_n_0\,
      CO(2) => \result3_carry__0_n_1\,
      CO(1) => \result3_carry__0_n_2\,
      CO(0) => \result3_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => x(7 downto 4),
      O(3) => \result3_carry__0_n_4\,
      O(2) => \result3_carry__0_n_5\,
      O(1) => \result3_carry__0_n_6\,
      O(0) => \result3_carry__0_n_7\,
      S(3) => \result3_carry__0_i_1_n_0\,
      S(2) => \result3_carry__0_i_2_n_0\,
      S(1) => \result3_carry__0_i_3_n_0\,
      S(0) => \result3_carry__0_i_4_n_0\
    );
\result3_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B847B847FF0000FF"
    )
        port map (
      I0 => \result3_carry__0_i_5_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__0_i_6_n_0\,
      I3 => x(7),
      I4 => \result3_carry__0_i_7_n_0\,
      I5 => Q(0),
      O => \result3_carry__0_i_1_n_0\
    );
\result3_carry__0_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(19),
      I1 => u32(11),
      I2 => Q(2),
      I3 => u32(15),
      I4 => Q(3),
      I5 => u32(7),
      O => \result3_carry__0_i_10_n_0\
    );
\result3_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00B84700FFB847"
    )
        port map (
      I0 => \result3_carry__0_i_6_n_0\,
      I1 => Q(1),
      I2 => result3_carry_i_5_n_0,
      I3 => x(6),
      I4 => Q(0),
      I5 => \result3_carry__0_i_7_n_0\,
      O => \result3_carry__0_i_2_n_0\
    );
\result3_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B847B847FF0000FF"
    )
        port map (
      I0 => \result3_carry__0_i_6_n_0\,
      I1 => Q(1),
      I2 => result3_carry_i_5_n_0,
      I3 => x(5),
      I4 => \result3_carry__0_i_8_n_0\,
      I5 => Q(0),
      O => \result3_carry__0_i_3_n_0\
    );
\result3_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00B84700FFB847"
    )
        port map (
      I0 => result3_carry_i_5_n_0,
      I1 => Q(1),
      I2 => result3_carry_i_6_n_0,
      I3 => x(4),
      I4 => Q(0),
      I5 => \result3_carry__0_i_8_n_0\,
      O => \result3_carry__0_i_4_n_0\
    );
\result3_carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(22),
      I1 => u32(14),
      I2 => Q(2),
      I3 => u32(18),
      I4 => Q(3),
      I5 => u32(10),
      O => \result3_carry__0_i_5_n_0\
    );
\result3_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(20),
      I1 => u32(12),
      I2 => Q(2),
      I3 => u32(16),
      I4 => Q(3),
      I5 => u32(8),
      O => \result3_carry__0_i_6_n_0\
    );
\result3_carry__0_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__0_i_9_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__0_i_10_n_0\,
      O => \result3_carry__0_i_7_n_0\
    );
\result3_carry__0_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__0_i_10_n_0\,
      I1 => Q(1),
      I2 => result3_carry_i_11_n_0,
      O => \result3_carry__0_i_8_n_0\
    );
\result3_carry__0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(21),
      I1 => u32(13),
      I2 => Q(2),
      I3 => u32(17),
      I4 => Q(3),
      I5 => u32(9),
      O => \result3_carry__0_i_9_n_0\
    );
\result3_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__0_n_0\,
      CO(3) => \result3_carry__1_n_0\,
      CO(2) => \result3_carry__1_n_1\,
      CO(1) => \result3_carry__1_n_2\,
      CO(0) => \result3_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => x(11 downto 8),
      O(3) => \result3_carry__1_n_4\,
      O(2) => \result3_carry__1_n_5\,
      O(1) => \result3_carry__1_n_6\,
      O(0) => \result3_carry__1_n_7\,
      S(3) => \result3_carry__1_i_1_n_0\,
      S(2) => \result3_carry__1_i_2_n_0\,
      S(1) => \result3_carry__1_i_3_n_0\,
      S(0) => \result3_carry__1_i_4_n_0\
    );
\result3_carry__1_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B847B847FF0000FF"
    )
        port map (
      I0 => \result3_carry__1_i_5_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__1_i_6_n_0\,
      I3 => x(11),
      I4 => \result3_carry__1_i_7_n_0\,
      I5 => Q(0),
      O => \result3_carry__1_i_1_n_0\
    );
\result3_carry__1_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(23),
      I1 => u32(15),
      I2 => Q(2),
      I3 => u32(19),
      I4 => Q(3),
      I5 => u32(11),
      O => \result3_carry__1_i_10_n_0\
    );
\result3_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00B84700FFB847"
    )
        port map (
      I0 => \result3_carry__1_i_6_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__0_i_5_n_0\,
      I3 => x(10),
      I4 => Q(0),
      I5 => \result3_carry__1_i_7_n_0\,
      O => \result3_carry__1_i_2_n_0\
    );
\result3_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B847B847FF0000FF"
    )
        port map (
      I0 => \result3_carry__1_i_6_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__0_i_5_n_0\,
      I3 => x(9),
      I4 => \result3_carry__1_i_8_n_0\,
      I5 => Q(0),
      O => \result3_carry__1_i_3_n_0\
    );
\result3_carry__1_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00B84700FFB847"
    )
        port map (
      I0 => \result3_carry__0_i_5_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__0_i_6_n_0\,
      I3 => x(8),
      I4 => Q(0),
      I5 => \result3_carry__1_i_8_n_0\,
      O => \result3_carry__1_i_4_n_0\
    );
\result3_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(26),
      I1 => u32(18),
      I2 => Q(2),
      I3 => u32(22),
      I4 => Q(3),
      I5 => u32(14),
      O => \result3_carry__1_i_5_n_0\
    );
\result3_carry__1_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(24),
      I1 => u32(16),
      I2 => Q(2),
      I3 => u32(20),
      I4 => Q(3),
      I5 => u32(12),
      O => \result3_carry__1_i_6_n_0\
    );
\result3_carry__1_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__1_i_9_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__1_i_10_n_0\,
      O => \result3_carry__1_i_7_n_0\
    );
\result3_carry__1_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__1_i_10_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__0_i_9_n_0\,
      O => \result3_carry__1_i_8_n_0\
    );
\result3_carry__1_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(25),
      I1 => u32(17),
      I2 => Q(2),
      I3 => u32(21),
      I4 => Q(3),
      I5 => u32(13),
      O => \result3_carry__1_i_9_n_0\
    );
\result3_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__1_n_0\,
      CO(3) => \result3_carry__2_n_0\,
      CO(2) => \result3_carry__2_n_1\,
      CO(1) => \result3_carry__2_n_2\,
      CO(0) => \result3_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => v32(15),
      DI(2 downto 0) => x(14 downto 12),
      O(3) => p_1_in,
      O(2) => \result3_carry__2_n_5\,
      O(1) => \result3_carry__2_n_6\,
      O(0) => \result3_carry__2_n_7\,
      S(3) => \result3_carry__2_i_2_n_0\,
      S(2) => \result3_carry__2_i_3_n_0\,
      S(1) => \result3_carry__2_i_4_n_0\,
      S(0) => \result3_carry__2_i_5_n_0\
    );
\result3_carry__2_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__2_i_6_n_0\,
      I1 => \result3_carry__2_i_7_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__2_i_8_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__2_i_9_n_0\,
      O => v32(15)
    );
\result3_carry__2_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__2_i_8_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__2_i_9_n_0\,
      O => \result3_carry__2_i_10_n_0\
    );
\result3_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result3_carry__2_i_9_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__1_i_9_n_0\,
      O => \result3_carry__2_i_11_n_0\
    );
\result3_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(15),
      I1 => x(15),
      O => \result3_carry__2_i_2_n_0\
    );
\result3_carry__2_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00B84700FFB847"
    )
        port map (
      I0 => \result3_carry__2_i_7_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__1_i_5_n_0\,
      I3 => x(14),
      I4 => Q(0),
      I5 => \result3_carry__2_i_10_n_0\,
      O => \result3_carry__2_i_3_n_0\
    );
\result3_carry__2_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B847B847FF0000FF"
    )
        port map (
      I0 => \result3_carry__2_i_7_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__1_i_5_n_0\,
      I3 => x(13),
      I4 => \result3_carry__2_i_11_n_0\,
      I5 => Q(0),
      O => \result3_carry__2_i_4_n_0\
    );
\result3_carry__2_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00B84700FFB847"
    )
        port map (
      I0 => \result3_carry__1_i_5_n_0\,
      I1 => Q(1),
      I2 => \result3_carry__1_i_6_n_0\,
      I3 => x(12),
      I4 => Q(0),
      I5 => \result3_carry__2_i_11_n_0\,
      O => \result3_carry__2_i_5_n_0\
    );
\result3_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(30),
      I1 => u32(22),
      I2 => Q(2),
      I3 => u32(26),
      I4 => Q(3),
      I5 => u32(18),
      O => \result3_carry__2_i_6_n_0\
    );
\result3_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(28),
      I1 => u32(20),
      I2 => Q(2),
      I3 => u32(24),
      I4 => Q(3),
      I5 => u32(16),
      O => \result3_carry__2_i_7_n_0\
    );
\result3_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(29),
      I1 => u32(21),
      I2 => Q(2),
      I3 => u32(25),
      I4 => Q(3),
      I5 => u32(17),
      O => \result3_carry__2_i_8_n_0\
    );
\result3_carry__2_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(27),
      I1 => u32(19),
      I2 => Q(2),
      I3 => u32(23),
      I4 => Q(3),
      I5 => u32(15),
      O => \result3_carry__2_i_9_n_0\
    );
\result3_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__2_n_0\,
      CO(3) => \result3_carry__3_n_0\,
      CO(2) => \result3_carry__3_n_1\,
      CO(1) => \result3_carry__3_n_2\,
      CO(0) => \result3_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \result3_carry__3_i_1_n_0\,
      DI(2) => \result3_carry__3_i_2_n_0\,
      DI(1) => \result3_carry__3_i_3_n_0\,
      DI(0) => \result3_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_result3_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \result3_carry__3_i_5_n_0\,
      S(2) => \result3_carry__3_i_6_n_0\,
      S(1) => \result3_carry__3_i_7_n_0\,
      S(0) => \result3_carry__3_i_8_n_0\
    );
\result3_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(18),
      O => \result3_carry__3_i_1_n_0\
    );
\result3_carry__3_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__3_i_15_n_0\,
      I1 => \result3_carry__2_i_6_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_14_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__2_i_8_n_0\,
      O => v32(17)
    );
\result3_carry__3_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__3_i_14_n_0\,
      I1 => \result3_carry__2_i_8_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__2_i_6_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__2_i_7_n_0\,
      O => v32(16)
    );
\result3_carry__3_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__3_i_16_n_0\,
      I1 => \result3_carry__3_i_15_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_13_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__3_i_14_n_0\,
      O => v32(19)
    );
\result3_carry__3_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(31),
      I1 => u32(25),
      I2 => Q(2),
      I3 => u32(29),
      I4 => Q(3),
      I5 => u32(21),
      O => \result3_carry__3_i_13_n_0\
    );
\result3_carry__3_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(31),
      I1 => u32(23),
      I2 => Q(2),
      I3 => u32(27),
      I4 => Q(3),
      I5 => u32(19),
      O => \result3_carry__3_i_14_n_0\
    );
\result3_carry__3_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(31),
      I1 => u32(24),
      I2 => Q(2),
      I3 => u32(28),
      I4 => Q(3),
      I5 => u32(20),
      O => \result3_carry__3_i_15_n_0\
    );
\result3_carry__3_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(31),
      I1 => u32(26),
      I2 => Q(2),
      I3 => u32(30),
      I4 => Q(3),
      I5 => u32(22),
      O => \result3_carry__3_i_16_n_0\
    );
\result3_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(17),
      O => \result3_carry__3_i_2_n_0\
    );
\result3_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(16),
      O => \result3_carry__3_i_3_n_0\
    );
\result3_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(15),
      O => \result3_carry__3_i_4_n_0\
    );
\result3_carry__3_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(18),
      I1 => v32(19),
      O => \result3_carry__3_i_5_n_0\
    );
\result3_carry__3_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(17),
      I1 => v32(18),
      O => \result3_carry__3_i_6_n_0\
    );
\result3_carry__3_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(16),
      I1 => v32(17),
      O => \result3_carry__3_i_7_n_0\
    );
\result3_carry__3_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(15),
      I1 => v32(16),
      O => \result3_carry__3_i_8_n_0\
    );
\result3_carry__3_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__3_i_13_n_0\,
      I1 => \result3_carry__3_i_14_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_15_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__2_i_6_n_0\,
      O => v32(18)
    );
\result3_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__3_n_0\,
      CO(3) => \result3_carry__4_n_0\,
      CO(2) => \result3_carry__4_n_1\,
      CO(1) => \result3_carry__4_n_2\,
      CO(0) => \result3_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \result3_carry__4_i_1_n_0\,
      DI(2) => \result3_carry__4_i_2_n_0\,
      DI(1) => \result3_carry__4_i_3_n_0\,
      DI(0) => \result3_carry__4_i_4_n_0\,
      O(3 downto 0) => \NLW_result3_carry__4_O_UNCONNECTED\(3 downto 0),
      S(3) => \result3_carry__4_i_5_n_0\,
      S(2) => \result3_carry__4_i_6_n_0\,
      S(1) => \result3_carry__4_i_7_n_0\,
      S(0) => \result3_carry__4_i_8_n_0\
    );
\result3_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(22),
      O => \result3_carry__4_i_1_n_0\
    );
\result3_carry__4_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__4_i_15_n_0\,
      I1 => \result3_carry__3_i_16_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__4_i_14_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__3_i_13_n_0\,
      O => v32(21)
    );
\result3_carry__4_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__4_i_14_n_0\,
      I1 => \result3_carry__3_i_13_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__3_i_16_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__3_i_15_n_0\,
      O => v32(20)
    );
\result3_carry__4_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__5_i_11_n_0\,
      I1 => \result3_carry__4_i_15_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__4_i_13_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_14_n_0\,
      O => v32(23)
    );
\result3_carry__4_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => u32(29),
      I1 => Q(2),
      I2 => u32(31),
      I3 => Q(3),
      I4 => u32(25),
      O => \result3_carry__4_i_13_n_0\
    );
\result3_carry__4_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => u32(27),
      I1 => Q(2),
      I2 => u32(31),
      I3 => Q(3),
      I4 => u32(23),
      O => \result3_carry__4_i_14_n_0\
    );
\result3_carry__4_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => u32(28),
      I1 => Q(2),
      I2 => u32(31),
      I3 => Q(3),
      I4 => u32(24),
      O => \result3_carry__4_i_15_n_0\
    );
\result3_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(21),
      O => \result3_carry__4_i_2_n_0\
    );
\result3_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(20),
      O => \result3_carry__4_i_3_n_0\
    );
\result3_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(19),
      O => \result3_carry__4_i_4_n_0\
    );
\result3_carry__4_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(22),
      I1 => v32(23),
      O => \result3_carry__4_i_5_n_0\
    );
\result3_carry__4_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(21),
      I1 => v32(22),
      O => \result3_carry__4_i_6_n_0\
    );
\result3_carry__4_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(20),
      I1 => v32(21),
      O => \result3_carry__4_i_7_n_0\
    );
\result3_carry__4_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(19),
      I1 => v32(20),
      O => \result3_carry__4_i_8_n_0\
    );
\result3_carry__4_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__4_i_13_n_0\,
      I1 => \result3_carry__4_i_14_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__4_i_15_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__3_i_16_n_0\,
      O => v32(22)
    );
\result3_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__4_n_0\,
      CO(3) => \result3_carry__5_n_0\,
      CO(2) => \result3_carry__5_n_1\,
      CO(1) => \result3_carry__5_n_2\,
      CO(0) => \result3_carry__5_n_3\,
      CYINIT => '0',
      DI(3) => \result3_carry__5_i_1_n_0\,
      DI(2) => \result3_carry__5_i_2_n_0\,
      DI(1) => \result3_carry__5_i_3_n_0\,
      DI(0) => \result3_carry__5_i_4_n_0\,
      O(3 downto 0) => \NLW_result3_carry__5_O_UNCONNECTED\(3 downto 0),
      S(3) => \result3_carry__5_i_5_n_0\,
      S(2) => \result3_carry__5_i_6_n_0\,
      S(1) => \result3_carry__5_i_7_n_0\,
      S(0) => \result3_carry__5_i_8_n_0\
    );
\result3_carry__5_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"47444777"
    )
        port map (
      I0 => \result3_carry__5_i_9_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__5_i_10_n_0\,
      I3 => Q(1),
      I4 => \result3_carry__5_i_11_n_0\,
      O => \result3_carry__5_i_1_n_0\
    );
\result3_carry__5_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => Q(2),
      I1 => u32(31),
      I2 => Q(3),
      I3 => u32(28),
      O => \result3_carry__5_i_10_n_0\
    );
\result3_carry__5_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => u32(30),
      I1 => Q(2),
      I2 => u32(31),
      I3 => Q(3),
      I4 => u32(26),
      O => \result3_carry__5_i_11_n_0\
    );
\result3_carry__5_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__5_i_10_n_0\,
      I1 => \result3_carry__5_i_11_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__5_i_15_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_13_n_0\,
      O => v32(25)
    );
\result3_carry__5_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \result3_carry__5_i_15_n_0\,
      I1 => \result3_carry__4_i_13_n_0\,
      I2 => Q(0),
      I3 => \result3_carry__5_i_11_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__4_i_15_n_0\,
      O => v32(24)
    );
\result3_carry__5_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => u32(30),
      I1 => Q(1),
      I2 => Q(2),
      I3 => u32(31),
      I4 => Q(3),
      I5 => u32(28),
      O => \result3_carry__5_i_14_n_0\
    );
\result3_carry__5_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => Q(2),
      I1 => u32(31),
      I2 => Q(3),
      I3 => u32(27),
      O => \result3_carry__5_i_15_n_0\
    );
\result3_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(25),
      O => \result3_carry__5_i_2_n_0\
    );
\result3_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(24),
      O => \result3_carry__5_i_3_n_0\
    );
\result3_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => v32(23),
      O => \result3_carry__5_i_4_n_0\
    );
\result3_carry__5_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B487B4B4B4878787"
    )
        port map (
      I0 => \result3_carry__5_i_14_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__5_i_9_n_0\,
      I3 => \result3_carry__5_i_10_n_0\,
      I4 => Q(1),
      I5 => \result3_carry__5_i_11_n_0\,
      O => \result3_carry__5_i_5_n_0\
    );
\result3_carry__5_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBB88847444777"
    )
        port map (
      I0 => \result3_carry__5_i_9_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__5_i_10_n_0\,
      I3 => Q(1),
      I4 => \result3_carry__5_i_11_n_0\,
      I5 => v32(25),
      O => \result3_carry__5_i_6_n_0\
    );
\result3_carry__5_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(24),
      I1 => v32(25),
      O => \result3_carry__5_i_7_n_0\
    );
\result3_carry__5_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => v32(23),
      I1 => v32(24),
      O => \result3_carry__5_i_8_n_0\
    );
\result3_carry__5_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => u32(29),
      I1 => Q(1),
      I2 => Q(2),
      I3 => u32(31),
      I4 => Q(3),
      I5 => u32(27),
      O => \result3_carry__5_i_9_n_0\
    );
\result3_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \result3_carry__5_n_0\,
      CO(3) => \NLW_result3_carry__6_CO_UNCONNECTED\(3),
      CO(2) => \result3_carry__6_n_1\,
      CO(1) => \result3_carry__6_n_2\,
      CO(0) => \result3_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \result3_carry__6_i_1_n_0\,
      DI(1) => \result3_carry__6_i_2_n_0\,
      DI(0) => \result3_carry__6_i_3_n_0\,
      O(3) => p_0_in,
      O(2 downto 0) => \NLW_result3_carry__6_O_UNCONNECTED\(2 downto 0),
      S(3) => \result3_carry__6_i_4_n_0\,
      S(2) => \result3_carry__6_i_5_n_0\,
      S(1) => \result3_carry__6_i_6_n_0\,
      S(0) => \result3_carry__6_i_7_n_0\
    );
\result3_carry__6_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \result3_carry__6_i_8_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__6_i_9_n_0\,
      O => \result3_carry__6_i_1_n_0\
    );
\result3_carry__6_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \result3_carry__6_i_9_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__5_i_14_n_0\,
      O => \result3_carry__6_i_2_n_0\
    );
\result3_carry__6_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \result3_carry__5_i_14_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__5_i_9_n_0\,
      O => \result3_carry__6_i_3_n_0\
    );
\result3_carry__6_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFD"
    )
        port map (
      I0 => u32(30),
      I1 => Q(3),
      I2 => Q(2),
      I3 => Q(1),
      I4 => Q(0),
      I5 => u32(31),
      O => \result3_carry__6_i_4_n_0\
    );
\result3_carry__6_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C939"
    )
        port map (
      I0 => \result3_carry__6_i_9_n_0\,
      I1 => \result3_carry__6_i_8_n_0\,
      I2 => Q(0),
      I3 => u32(31),
      O => \result3_carry__6_i_5_n_0\
    );
\result3_carry__6_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B487"
    )
        port map (
      I0 => \result3_carry__6_i_8_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__6_i_9_n_0\,
      I3 => \result3_carry__5_i_14_n_0\,
      O => \result3_carry__6_i_6_n_0\
    );
\result3_carry__6_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B487"
    )
        port map (
      I0 => \result3_carry__6_i_9_n_0\,
      I1 => Q(0),
      I2 => \result3_carry__5_i_14_n_0\,
      I3 => \result3_carry__5_i_9_n_0\,
      O => \result3_carry__6_i_7_n_0\
    );
\result3_carry__6_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => Q(1),
      I1 => Q(2),
      I2 => u32(31),
      I3 => Q(3),
      I4 => u32(30),
      O => \result3_carry__6_i_8_n_0\
    );
\result3_carry__6_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => Q(1),
      I1 => Q(2),
      I2 => u32(31),
      I3 => Q(3),
      I4 => u32(29),
      O => \result3_carry__6_i_9_n_0\
    );
result3_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B847B847FF0000FF"
    )
        port map (
      I0 => result3_carry_i_5_n_0,
      I1 => Q(1),
      I2 => result3_carry_i_6_n_0,
      I3 => x(3),
      I4 => result3_carry_i_7_n_0,
      I5 => Q(0),
      O => result3_carry_i_1_n_0
    );
result3_carry_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(12),
      I1 => u32(4),
      I2 => Q(2),
      I3 => u32(8),
      I4 => Q(3),
      I5 => u32(0),
      O => result3_carry_i_10_n_0
    );
result3_carry_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(17),
      I1 => u32(9),
      I2 => Q(2),
      I3 => u32(13),
      I4 => Q(3),
      I5 => u32(5),
      O => result3_carry_i_11_n_0
    );
result3_carry_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(15),
      I1 => u32(7),
      I2 => Q(2),
      I3 => u32(11),
      I4 => Q(3),
      I5 => u32(3),
      O => result3_carry_i_12_n_0
    );
result3_carry_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(13),
      I1 => u32(5),
      I2 => Q(2),
      I3 => u32(9),
      I4 => Q(3),
      I5 => u32(1),
      O => result3_carry_i_13_n_0
    );
result3_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00B84700FFB847"
    )
        port map (
      I0 => result3_carry_i_6_n_0,
      I1 => Q(1),
      I2 => result3_carry_i_8_n_0,
      I3 => x(2),
      I4 => Q(0),
      I5 => result3_carry_i_7_n_0,
      O => result3_carry_i_2_n_0
    );
result3_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B847B847FF0000FF"
    )
        port map (
      I0 => result3_carry_i_6_n_0,
      I1 => Q(1),
      I2 => result3_carry_i_8_n_0,
      I3 => x(1),
      I4 => result3_carry_i_9_n_0,
      I5 => Q(0),
      O => result3_carry_i_3_n_0
    );
result3_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAA9595555A959"
    )
        port map (
      I0 => x(0),
      I1 => result3_carry_i_10_n_0,
      I2 => Q(1),
      I3 => result3_carry_i_8_n_0,
      I4 => Q(0),
      I5 => result3_carry_i_9_n_0,
      O => result3_carry_i_4_n_0
    );
result3_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(18),
      I1 => u32(10),
      I2 => Q(2),
      I3 => u32(14),
      I4 => Q(3),
      I5 => u32(6),
      O => result3_carry_i_5_n_0
    );
result3_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(16),
      I1 => u32(8),
      I2 => Q(2),
      I3 => u32(12),
      I4 => Q(3),
      I5 => u32(4),
      O => result3_carry_i_6_n_0
    );
result3_carry_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => result3_carry_i_11_n_0,
      I1 => Q(1),
      I2 => result3_carry_i_12_n_0,
      O => result3_carry_i_7_n_0
    );
result3_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => u32(14),
      I1 => u32(6),
      I2 => Q(2),
      I3 => u32(10),
      I4 => Q(3),
      I5 => u32(2),
      O => result3_carry_i_8_n_0
    );
result3_carry_i_9: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => result3_carry_i_12_n_0,
      I1 => Q(1),
      I2 => result3_carry_i_13_n_0,
      O => result3_carry_i_9_n_0
    );
\result[0]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => result3_carry_n_7,
      O => p_2_in(0)
    );
\result[10]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__1_n_5\,
      O => p_2_in(10)
    );
\result[11]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__1_n_4\,
      O => p_2_in(11)
    );
\result[12]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__2_n_7\,
      O => p_2_in(12)
    );
\result[13]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__2_n_6\,
      O => p_2_in(13)
    );
\result[14]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__2_n_5\,
      O => p_2_in(14)
    );
\result[1]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => result3_carry_n_6,
      O => p_2_in(1)
    );
\result[2]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => result3_carry_n_5,
      O => p_2_in(2)
    );
\result[31]_C_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_0_in,
      O => p_2_in(31)
    );
\result[3]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => result3_carry_n_4,
      O => p_2_in(3)
    );
\result[4]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__0_n_7\,
      O => p_2_in(4)
    );
\result[5]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__0_n_6\,
      O => p_2_in(5)
    );
\result[6]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__0_n_5\,
      O => p_2_in(6)
    );
\result[7]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__0_n_4\,
      O => p_2_in(7)
    );
\result[8]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__1_n_7\,
      O => p_2_in(8)
    );
\result[9]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => p_0_in,
      I1 => p_1_in,
      I2 => \result3_carry__1_n_6\,
      O => p_2_in(9)
    );
\result_reg[0]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[0]_LDC_i_2_n_0\,
      D => p_2_in(0),
      Q => \result_reg[0]_C_n_0\
    );
\result_reg[0]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[0]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[0]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[0]_LDC_n_0\
    );
\result_reg[0]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(0),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[0]_LDC_i_1_n_0\
    );
\result_reg[0]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(0),
      O => \result_reg[0]_LDC_i_2_n_0\
    );
\result_reg[0]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(0),
      PRE => \result_reg[0]_LDC_i_1_n_0\,
      Q => \result_reg[0]_P_n_0\
    );
\result_reg[10]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[10]_LDC_i_2_n_0\,
      D => p_2_in(10),
      Q => \result_reg[10]_C_n_0\
    );
\result_reg[10]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[10]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[10]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[10]_LDC_n_0\
    );
\result_reg[10]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(10),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[10]_LDC_i_1_n_0\
    );
\result_reg[10]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(10),
      O => \result_reg[10]_LDC_i_2_n_0\
    );
\result_reg[10]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(10),
      PRE => \result_reg[10]_LDC_i_1_n_0\,
      Q => \result_reg[10]_P_n_0\
    );
\result_reg[11]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[11]_LDC_i_2_n_0\,
      D => p_2_in(11),
      Q => \result_reg[11]_C_n_0\
    );
\result_reg[11]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[11]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[11]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[11]_LDC_n_0\
    );
\result_reg[11]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(11),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[11]_LDC_i_1_n_0\
    );
\result_reg[11]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(11),
      O => \result_reg[11]_LDC_i_2_n_0\
    );
\result_reg[11]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(11),
      PRE => \result_reg[11]_LDC_i_1_n_0\,
      Q => \result_reg[11]_P_n_0\
    );
\result_reg[12]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[12]_LDC_i_2_n_0\,
      D => p_2_in(12),
      Q => \result_reg[12]_C_n_0\
    );
\result_reg[12]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[12]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[12]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[12]_LDC_n_0\
    );
\result_reg[12]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(12),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[12]_LDC_i_1_n_0\
    );
\result_reg[12]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(12),
      O => \result_reg[12]_LDC_i_2_n_0\
    );
\result_reg[12]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(12),
      PRE => \result_reg[12]_LDC_i_1_n_0\,
      Q => \result_reg[12]_P_n_0\
    );
\result_reg[13]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[13]_LDC_i_2_n_0\,
      D => p_2_in(13),
      Q => \result_reg[13]_C_n_0\
    );
\result_reg[13]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[13]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[13]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[13]_LDC_n_0\
    );
\result_reg[13]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(13),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[13]_LDC_i_1_n_0\
    );
\result_reg[13]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(13),
      O => \result_reg[13]_LDC_i_2_n_0\
    );
\result_reg[13]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(13),
      PRE => \result_reg[13]_LDC_i_1_n_0\,
      Q => \result_reg[13]_P_n_0\
    );
\result_reg[14]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[14]_LDC_i_2_n_0\,
      D => p_2_in(14),
      Q => \result_reg[14]_C_n_0\
    );
\result_reg[14]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[14]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[14]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[14]_LDC_n_0\
    );
\result_reg[14]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(14),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[14]_LDC_i_1_n_0\
    );
\result_reg[14]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(14),
      O => \result_reg[14]_LDC_i_2_n_0\
    );
\result_reg[14]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(14),
      PRE => \result_reg[14]_LDC_i_1_n_0\,
      Q => \result_reg[14]_P_n_0\
    );
\result_reg[1]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[1]_LDC_i_2_n_0\,
      D => p_2_in(1),
      Q => \result_reg[1]_C_n_0\
    );
\result_reg[1]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[1]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[1]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[1]_LDC_n_0\
    );
\result_reg[1]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(1),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[1]_LDC_i_1_n_0\
    );
\result_reg[1]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(1),
      O => \result_reg[1]_LDC_i_2_n_0\
    );
\result_reg[1]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(1),
      PRE => \result_reg[1]_LDC_i_1_n_0\,
      Q => \result_reg[1]_P_n_0\
    );
\result_reg[2]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[2]_LDC_i_2_n_0\,
      D => p_2_in(2),
      Q => \result_reg[2]_C_n_0\
    );
\result_reg[2]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[2]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[2]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[2]_LDC_n_0\
    );
\result_reg[2]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(2),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[2]_LDC_i_1_n_0\
    );
\result_reg[2]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(2),
      O => \result_reg[2]_LDC_i_2_n_0\
    );
\result_reg[2]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(2),
      PRE => \result_reg[2]_LDC_i_1_n_0\,
      Q => \result_reg[2]_P_n_0\
    );
\result_reg[31]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[31]_LDC_i_2_n_0\,
      D => p_2_in(31),
      Q => \result_reg[31]_C_n_0\
    );
\result_reg[31]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[31]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[31]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[31]_LDC_n_0\
    );
\result_reg[31]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(15),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[31]_LDC_i_1_n_0\
    );
\result_reg[31]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(15),
      O => \result_reg[31]_LDC_i_2_n_0\
    );
\result_reg[31]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(31),
      PRE => \result_reg[31]_LDC_i_1_n_0\,
      Q => \result_reg[31]_P_n_0\
    );
\result_reg[3]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[3]_LDC_i_2_n_0\,
      D => p_2_in(3),
      Q => \result_reg[3]_C_n_0\
    );
\result_reg[3]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[3]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[3]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[3]_LDC_n_0\
    );
\result_reg[3]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(3),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[3]_LDC_i_1_n_0\
    );
\result_reg[3]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(3),
      O => \result_reg[3]_LDC_i_2_n_0\
    );
\result_reg[3]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(3),
      PRE => \result_reg[3]_LDC_i_1_n_0\,
      Q => \result_reg[3]_P_n_0\
    );
\result_reg[4]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[4]_LDC_i_2_n_0\,
      D => p_2_in(4),
      Q => \result_reg[4]_C_n_0\
    );
\result_reg[4]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[4]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[4]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[4]_LDC_n_0\
    );
\result_reg[4]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(4),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[4]_LDC_i_1_n_0\
    );
\result_reg[4]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(4),
      O => \result_reg[4]_LDC_i_2_n_0\
    );
\result_reg[4]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(4),
      PRE => \result_reg[4]_LDC_i_1_n_0\,
      Q => \result_reg[4]_P_n_0\
    );
\result_reg[5]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[5]_LDC_i_2_n_0\,
      D => p_2_in(5),
      Q => \result_reg[5]_C_n_0\
    );
\result_reg[5]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[5]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[5]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[5]_LDC_n_0\
    );
\result_reg[5]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(5),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[5]_LDC_i_1_n_0\
    );
\result_reg[5]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(5),
      O => \result_reg[5]_LDC_i_2_n_0\
    );
\result_reg[5]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(5),
      PRE => \result_reg[5]_LDC_i_1_n_0\,
      Q => \result_reg[5]_P_n_0\
    );
\result_reg[6]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[6]_LDC_i_2_n_0\,
      D => p_2_in(6),
      Q => \result_reg[6]_C_n_0\
    );
\result_reg[6]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[6]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[6]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[6]_LDC_n_0\
    );
\result_reg[6]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(6),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[6]_LDC_i_1_n_0\
    );
\result_reg[6]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(6),
      O => \result_reg[6]_LDC_i_2_n_0\
    );
\result_reg[6]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(6),
      PRE => \result_reg[6]_LDC_i_1_n_0\,
      Q => \result_reg[6]_P_n_0\
    );
\result_reg[7]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[7]_LDC_i_2_n_0\,
      D => p_2_in(7),
      Q => \result_reg[7]_C_n_0\
    );
\result_reg[7]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[7]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[7]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[7]_LDC_n_0\
    );
\result_reg[7]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(7),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[7]_LDC_i_1_n_0\
    );
\result_reg[7]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(7),
      O => \result_reg[7]_LDC_i_2_n_0\
    );
\result_reg[7]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(7),
      PRE => \result_reg[7]_LDC_i_1_n_0\,
      Q => \result_reg[7]_P_n_0\
    );
\result_reg[8]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[8]_LDC_i_2_n_0\,
      D => p_2_in(8),
      Q => \result_reg[8]_C_n_0\
    );
\result_reg[8]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[8]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[8]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[8]_LDC_n_0\
    );
\result_reg[8]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(8),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[8]_LDC_i_1_n_0\
    );
\result_reg[8]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(8),
      O => \result_reg[8]_LDC_i_2_n_0\
    );
\result_reg[8]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(8),
      PRE => \result_reg[8]_LDC_i_1_n_0\,
      Q => \result_reg[8]_P_n_0\
    );
\result_reg[9]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \result_reg[9]_LDC_i_2_n_0\,
      D => p_2_in(9),
      Q => \result_reg[9]_C_n_0\
    );
\result_reg[9]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \result_reg[9]_LDC_i_2_n_0\,
      D => '1',
      G => \result_reg[9]_LDC_i_1_n_0\,
      GE => '1',
      Q => \result_reg[9]_LDC_n_0\
    );
\result_reg[9]_LDC_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => x(9),
      I1 => Q(4),
      I2 => resetn,
      O => \result_reg[9]_LDC_i_1_n_0\
    );
\result_reg[9]_LDC_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => Q(4),
      I1 => resetn,
      I2 => x(9),
      O => \result_reg[9]_LDC_i_2_n_0\
    );
\result_reg[9]_P\: unisim.vcomponents.FDPE
     port map (
      C => clk,
      CE => '1',
      D => p_2_in(9),
      PRE => \result_reg[9]_LDC_i_1_n_0\,
      Q => \result_reg[9]_P_n_0\
    );
u320_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => u320_carry_n_0,
      CO(2) => u320_carry_n_1,
      CO(1) => u320_carry_n_2,
      CO(0) => u320_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => acc32_reg(3 downto 0),
      O(3 downto 0) => u320(3 downto 0),
      S(3) => delay_line_inst_n_0,
      S(2) => delay_line_inst_n_1,
      S(1) => delay_line_inst_n_2,
      S(0) => delay_line_inst_n_3
    );
\u320_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => u320_carry_n_0,
      CO(3) => \u320_carry__0_n_0\,
      CO(2) => \u320_carry__0_n_1\,
      CO(1) => \u320_carry__0_n_2\,
      CO(0) => \u320_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => acc32_reg(7 downto 4),
      O(3 downto 0) => u320(7 downto 4),
      S(3) => delay_line_inst_n_4,
      S(2) => delay_line_inst_n_5,
      S(1) => delay_line_inst_n_6,
      S(0) => delay_line_inst_n_7
    );
\u320_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \u320_carry__0_n_0\,
      CO(3) => \u320_carry__1_n_0\,
      CO(2) => \u320_carry__1_n_1\,
      CO(1) => \u320_carry__1_n_2\,
      CO(0) => \u320_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => acc32_reg(11 downto 8),
      O(3 downto 0) => u320(11 downto 8),
      S(3) => delay_line_inst_n_8,
      S(2) => delay_line_inst_n_9,
      S(1) => delay_line_inst_n_10,
      S(0) => delay_line_inst_n_11
    );
\u320_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \u320_carry__1_n_0\,
      CO(3) => \u320_carry__2_n_0\,
      CO(2) => \u320_carry__2_n_1\,
      CO(1) => \u320_carry__2_n_2\,
      CO(0) => \u320_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => acc32_reg(15 downto 12),
      O(3 downto 0) => u320(15 downto 12),
      S(3) => delay_line_inst_n_12,
      S(2) => delay_line_inst_n_13,
      S(1) => delay_line_inst_n_14,
      S(0) => delay_line_inst_n_15
    );
\u320_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \u320_carry__2_n_0\,
      CO(3) => \u320_carry__3_n_0\,
      CO(2) => \u320_carry__3_n_1\,
      CO(1) => \u320_carry__3_n_2\,
      CO(0) => \u320_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => acc32_reg(19 downto 16),
      O(3 downto 0) => u320(19 downto 16),
      S(3) => delay_line_inst_n_16,
      S(2) => delay_line_inst_n_17,
      S(1) => delay_line_inst_n_18,
      S(0) => delay_line_inst_n_19
    );
\u320_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \u320_carry__3_n_0\,
      CO(3) => \u320_carry__4_n_0\,
      CO(2) => \u320_carry__4_n_1\,
      CO(1) => \u320_carry__4_n_2\,
      CO(0) => \u320_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => acc32_reg(23 downto 20),
      O(3 downto 0) => u320(23 downto 20),
      S(3) => delay_line_inst_n_20,
      S(2) => delay_line_inst_n_21,
      S(1) => delay_line_inst_n_22,
      S(0) => delay_line_inst_n_23
    );
\u320_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \u320_carry__4_n_0\,
      CO(3) => \u320_carry__5_n_0\,
      CO(2) => \u320_carry__5_n_1\,
      CO(1) => \u320_carry__5_n_2\,
      CO(0) => \u320_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => acc32_reg(27 downto 24),
      O(3 downto 0) => u320(27 downto 24),
      S(3) => delay_line_inst_n_24,
      S(2) => delay_line_inst_n_25,
      S(1) => delay_line_inst_n_26,
      S(0) => delay_line_inst_n_27
    );
\u320_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \u320_carry__5_n_0\,
      CO(3) => \NLW_u320_carry__6_CO_UNCONNECTED\(3),
      CO(2) => \u320_carry__6_n_1\,
      CO(1) => \u320_carry__6_n_2\,
      CO(0) => \u320_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => acc32_reg(30 downto 28),
      O(3 downto 0) => u320(31 downto 28),
      S(3) => delay_line_inst_n_28,
      S(2) => delay_line_inst_n_29,
      S(1) => delay_line_inst_n_30,
      S(0) => delay_line_inst_n_31
    );
\u32_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(0),
      Q => u32(0)
    );
\u32_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(10),
      Q => u32(10)
    );
\u32_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(11),
      Q => u32(11)
    );
\u32_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(12),
      Q => u32(12)
    );
\u32_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(13),
      Q => u32(13)
    );
\u32_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(14),
      Q => u32(14)
    );
\u32_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(15),
      Q => u32(15)
    );
\u32_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(16),
      Q => u32(16)
    );
\u32_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(17),
      Q => u32(17)
    );
\u32_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(18),
      Q => u32(18)
    );
\u32_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(19),
      Q => u32(19)
    );
\u32_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(1),
      Q => u32(1)
    );
\u32_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(20),
      Q => u32(20)
    );
\u32_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(21),
      Q => u32(21)
    );
\u32_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(22),
      Q => u32(22)
    );
\u32_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(23),
      Q => u32(23)
    );
\u32_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(24),
      Q => u32(24)
    );
\u32_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(25),
      Q => u32(25)
    );
\u32_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(26),
      Q => u32(26)
    );
\u32_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(27),
      Q => u32(27)
    );
\u32_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(28),
      Q => u32(28)
    );
\u32_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(29),
      Q => u32(29)
    );
\u32_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(2),
      Q => u32(2)
    );
\u32_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(30),
      Q => u32(30)
    );
\u32_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(31),
      Q => u32(31)
    );
\u32_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(3),
      Q => u32(3)
    );
\u32_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(4),
      Q => u32(4)
    );
\u32_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(5),
      Q => u32(5)
    );
\u32_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(6),
      Q => u32(6)
    );
\u32_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(7),
      Q => u32(7)
    );
\u32_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(8),
      Q => u32(8)
    );
\u32_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => en,
      CLR => en_i_2_n_0,
      D => u320(9),
      Q => u32(9)
    );
\y[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[0]_P_n_0\,
      I1 => \result_reg[0]_LDC_n_0\,
      I2 => \result_reg[0]_C_n_0\,
      O => y(0)
    );
\y[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[10]_P_n_0\,
      I1 => \result_reg[10]_LDC_n_0\,
      I2 => \result_reg[10]_C_n_0\,
      O => y(10)
    );
\y[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[11]_P_n_0\,
      I1 => \result_reg[11]_LDC_n_0\,
      I2 => \result_reg[11]_C_n_0\,
      O => y(11)
    );
\y[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[12]_P_n_0\,
      I1 => \result_reg[12]_LDC_n_0\,
      I2 => \result_reg[12]_C_n_0\,
      O => y(12)
    );
\y[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[13]_P_n_0\,
      I1 => \result_reg[13]_LDC_n_0\,
      I2 => \result_reg[13]_C_n_0\,
      O => y(13)
    );
\y[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[14]_P_n_0\,
      I1 => \result_reg[14]_LDC_n_0\,
      I2 => \result_reg[14]_C_n_0\,
      O => y(14)
    );
\y[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[31]_P_n_0\,
      I1 => \result_reg[31]_LDC_n_0\,
      I2 => \result_reg[31]_C_n_0\,
      O => y(15)
    );
\y[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[1]_P_n_0\,
      I1 => \result_reg[1]_LDC_n_0\,
      I2 => \result_reg[1]_C_n_0\,
      O => y(1)
    );
\y[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[2]_P_n_0\,
      I1 => \result_reg[2]_LDC_n_0\,
      I2 => \result_reg[2]_C_n_0\,
      O => y(2)
    );
\y[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[3]_P_n_0\,
      I1 => \result_reg[3]_LDC_n_0\,
      I2 => \result_reg[3]_C_n_0\,
      O => y(3)
    );
\y[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[4]_P_n_0\,
      I1 => \result_reg[4]_LDC_n_0\,
      I2 => \result_reg[4]_C_n_0\,
      O => y(4)
    );
\y[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[5]_P_n_0\,
      I1 => \result_reg[5]_LDC_n_0\,
      I2 => \result_reg[5]_C_n_0\,
      O => y(5)
    );
\y[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[6]_P_n_0\,
      I1 => \result_reg[6]_LDC_n_0\,
      I2 => \result_reg[6]_C_n_0\,
      O => y(6)
    );
\y[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[7]_P_n_0\,
      I1 => \result_reg[7]_LDC_n_0\,
      I2 => \result_reg[7]_C_n_0\,
      O => y(7)
    );
\y[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[8]_P_n_0\,
      I1 => \result_reg[8]_LDC_n_0\,
      I2 => \result_reg[8]_C_n_0\,
      O => y(8)
    );
\y[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \result_reg[9]_P_n_0\,
      I1 => \result_reg[9]_LDC_n_0\,
      I2 => \result_reg[9]_C_n_0\,
      O => y(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_blr_0_0_ip_blr_v3_0_S00_AXI is
  port (
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    resetn : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_blr_0_0_ip_blr_v3_0_S00_AXI : entity is "ip_blr_v3_0_S00_AXI";
end design_1_ip_blr_0_0_ip_blr_v3_0_S00_AXI;

architecture STRUCTURE of design_1_ip_blr_0_0_ip_blr_v3_0_S00_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_araddr : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_awready_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal blr_enable : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal \slv_reg0[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg0_D : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg0_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[9]\ : STD_LOGIC;
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg1_D : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \slv_reg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg1_reg_n_0_[9]\ : STD_LOGIC;
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2_D[4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg2_D_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[9]\ : STD_LOGIC;
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair18";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBF00BF00BF00"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => s00_axi_awvalid,
      I2 => s00_axi_wvalid,
      I3 => aw_en_reg_n_0,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => axi_awready_i_1_n_0
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(2),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(3),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => axi_araddr(2),
      S => axi_awready_i_1_n_0
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => axi_araddr(3),
      S => axi_awready_i_1_n_0
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => axi_awready_i_1_n_0
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_0_in(0),
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_0_in(1),
      R => axi_awready_i_1_n_0
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => axi_awready_i_1_n_0
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => axi_awready_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => axi_awready_i_1_n_0
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[0]\,
      I1 => \slv_reg0_reg_n_0_[0]\,
      I2 => slv_reg3(0),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[0]\,
      O => reg_data_out(0)
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[10]\,
      I1 => \slv_reg0_reg_n_0_[10]\,
      I2 => slv_reg3(10),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[10]\,
      O => reg_data_out(10)
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[11]\,
      I1 => \slv_reg0_reg_n_0_[11]\,
      I2 => slv_reg3(11),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[11]\,
      O => reg_data_out(11)
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[12]\,
      I1 => \slv_reg0_reg_n_0_[12]\,
      I2 => slv_reg3(12),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[12]\,
      O => reg_data_out(12)
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[13]\,
      I1 => \slv_reg0_reg_n_0_[13]\,
      I2 => slv_reg3(13),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[13]\,
      O => reg_data_out(13)
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[14]\,
      I1 => \slv_reg0_reg_n_0_[14]\,
      I2 => slv_reg3(14),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[14]\,
      O => reg_data_out(14)
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[15]\,
      I1 => \slv_reg0_reg_n_0_[15]\,
      I2 => slv_reg3(15),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[15]\,
      O => reg_data_out(15)
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[16]\,
      I1 => \slv_reg0_reg_n_0_[16]\,
      I2 => slv_reg3(16),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[16]\,
      O => reg_data_out(16)
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[17]\,
      I1 => \slv_reg0_reg_n_0_[17]\,
      I2 => slv_reg3(17),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[17]\,
      O => reg_data_out(17)
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[18]\,
      I1 => \slv_reg0_reg_n_0_[18]\,
      I2 => slv_reg3(18),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[18]\,
      O => reg_data_out(18)
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[19]\,
      I1 => \slv_reg0_reg_n_0_[19]\,
      I2 => slv_reg3(19),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[19]\,
      O => reg_data_out(19)
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[1]\,
      I1 => \slv_reg0_reg_n_0_[1]\,
      I2 => slv_reg3(1),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[1]\,
      O => reg_data_out(1)
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[20]\,
      I1 => \slv_reg0_reg_n_0_[20]\,
      I2 => slv_reg3(20),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[20]\,
      O => reg_data_out(20)
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[21]\,
      I1 => \slv_reg0_reg_n_0_[21]\,
      I2 => slv_reg3(21),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[21]\,
      O => reg_data_out(21)
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[22]\,
      I1 => \slv_reg0_reg_n_0_[22]\,
      I2 => slv_reg3(22),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[22]\,
      O => reg_data_out(22)
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[23]\,
      I1 => \slv_reg0_reg_n_0_[23]\,
      I2 => slv_reg3(23),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[23]\,
      O => reg_data_out(23)
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[24]\,
      I1 => \slv_reg0_reg_n_0_[24]\,
      I2 => slv_reg3(24),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[24]\,
      O => reg_data_out(24)
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[25]\,
      I1 => \slv_reg0_reg_n_0_[25]\,
      I2 => slv_reg3(25),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[25]\,
      O => reg_data_out(25)
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[26]\,
      I1 => \slv_reg0_reg_n_0_[26]\,
      I2 => slv_reg3(26),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[26]\,
      O => reg_data_out(26)
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[27]\,
      I1 => \slv_reg0_reg_n_0_[27]\,
      I2 => slv_reg3(27),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[27]\,
      O => reg_data_out(27)
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[28]\,
      I1 => \slv_reg0_reg_n_0_[28]\,
      I2 => slv_reg3(28),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[28]\,
      O => reg_data_out(28)
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[29]\,
      I1 => \slv_reg0_reg_n_0_[29]\,
      I2 => slv_reg3(29),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[29]\,
      O => reg_data_out(29)
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[2]\,
      I1 => \slv_reg0_reg_n_0_[2]\,
      I2 => slv_reg3(2),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[2]\,
      O => reg_data_out(2)
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[30]\,
      I1 => \slv_reg0_reg_n_0_[30]\,
      I2 => slv_reg3(30),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[30]\,
      O => reg_data_out(30)
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[31]\,
      I1 => \slv_reg0_reg_n_0_[31]\,
      I2 => slv_reg3(31),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[31]\,
      O => reg_data_out(31)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[3]\,
      I1 => \slv_reg0_reg_n_0_[3]\,
      I2 => slv_reg3(3),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[3]\,
      O => reg_data_out(3)
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[4]\,
      I1 => \slv_reg0_reg_n_0_[4]\,
      I2 => slv_reg3(4),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[4]\,
      O => reg_data_out(4)
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[5]\,
      I1 => \slv_reg0_reg_n_0_[5]\,
      I2 => slv_reg3(5),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[5]\,
      O => reg_data_out(5)
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[6]\,
      I1 => \slv_reg0_reg_n_0_[6]\,
      I2 => slv_reg3(6),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[6]\,
      O => reg_data_out(6)
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[7]\,
      I1 => \slv_reg0_reg_n_0_[7]\,
      I2 => slv_reg3(7),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[7]\,
      O => reg_data_out(7)
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[8]\,
      I1 => \slv_reg0_reg_n_0_[8]\,
      I2 => slv_reg3(8),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[8]\,
      O => reg_data_out(8)
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => \slv_reg1_reg_n_0_[9]\,
      I1 => \slv_reg0_reg_n_0_[9]\,
      I2 => slv_reg3(9),
      I3 => axi_araddr(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg2_reg_n_0_[9]\,
      O => reg_data_out(9)
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => axi_awready_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => axi_awready_i_1_n_0
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => axi_awready_i_1_n_0
    );
blr_int: entity work.design_1_ip_blr_0_0_blr
     port map (
      Q(4) => blr_enable,
      Q(3) => \slv_reg2_D_reg_n_0_[3]\,
      Q(2) => \slv_reg2_D_reg_n_0_[2]\,
      Q(1) => \slv_reg2_D_reg_n_0_[1]\,
      Q(0) => \slv_reg2_D_reg_n_0_[0]\,
      clk => clk,
      \en1_carry__0_0\(15 downto 0) => slv_reg1_D(15 downto 0),
      \en1_inferred__0/i__carry__0_0\(15 downto 0) => slv_reg0_D(15 downto 0),
      resetn => resetn,
      x(15 downto 0) => x(15 downto 0),
      y(15 downto 0) => y(15 downto 0)
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(1),
      O => \slv_reg0[15]_i_1_n_0\
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(2),
      O => \slv_reg0[23]_i_1_n_0\
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(3),
      O => \slv_reg0[31]_i_1_n_0\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(0),
      O => \slv_reg0[7]_i_1_n_0\
    );
\slv_reg0_D_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[0]\,
      Q => slv_reg0_D(0),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[10]\,
      Q => slv_reg0_D(10),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[11]\,
      Q => slv_reg0_D(11),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[12]\,
      Q => slv_reg0_D(12),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[13]\,
      Q => slv_reg0_D(13),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[14]\,
      Q => slv_reg0_D(14),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[15]\,
      Q => slv_reg0_D(15),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[1]\,
      Q => slv_reg0_D(1),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[2]\,
      Q => slv_reg0_D(2),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[3]\,
      Q => slv_reg0_D(3),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[4]\,
      Q => slv_reg0_D(4),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[5]\,
      Q => slv_reg0_D(5),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[6]\,
      Q => slv_reg0_D(6),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[7]\,
      Q => slv_reg0_D(7),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[8]\,
      Q => slv_reg0_D(8),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_D_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[9]\,
      Q => slv_reg0_D(9),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \slv_reg0_reg_n_0_[0]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg0_reg_n_0_[10]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg0_reg_n_0_[11]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg0_reg_n_0_[12]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg0_reg_n_0_[13]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg0_reg_n_0_[14]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg0_reg_n_0_[15]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg0_reg_n_0_[16]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg0_reg_n_0_[17]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg0_reg_n_0_[18]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg0_reg_n_0_[19]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \slv_reg0_reg_n_0_[1]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg0_reg_n_0_[20]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg0_reg_n_0_[21]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg0_reg_n_0_[22]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg0_reg_n_0_[23]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg0_reg_n_0_[24]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg0_reg_n_0_[25]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg0_reg_n_0_[26]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg0_reg_n_0_[27]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg0_reg_n_0_[28]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg0_reg_n_0_[29]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \slv_reg0_reg_n_0_[2]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg0_reg_n_0_[30]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg0_reg_n_0_[31]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \slv_reg0_reg_n_0_[3]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \slv_reg0_reg_n_0_[4]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \slv_reg0_reg_n_0_[5]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \slv_reg0_reg_n_0_[6]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \slv_reg0_reg_n_0_[7]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg0_reg_n_0_[8]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg0_reg_n_0_[9]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(1),
      I3 => p_0_in(0),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(2),
      I3 => p_0_in(0),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(3),
      I3 => p_0_in(0),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(0),
      I3 => p_0_in(0),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_D_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[0]\,
      Q => slv_reg1_D(0),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[10]\,
      Q => slv_reg1_D(10),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[11]\,
      Q => slv_reg1_D(11),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[12]\,
      Q => slv_reg1_D(12),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[13]\,
      Q => slv_reg1_D(13),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[14]\,
      Q => slv_reg1_D(14),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[15]\,
      Q => slv_reg1_D(15),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[1]\,
      Q => slv_reg1_D(1),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[2]\,
      Q => slv_reg1_D(2),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[3]\,
      Q => slv_reg1_D(3),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[4]\,
      Q => slv_reg1_D(4),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[5]\,
      Q => slv_reg1_D(5),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[6]\,
      Q => slv_reg1_D(6),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[7]\,
      Q => slv_reg1_D(7),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[8]\,
      Q => slv_reg1_D(8),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_D_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg1_reg_n_0_[9]\,
      Q => slv_reg1_D(9),
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \slv_reg1_reg_n_0_[0]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg1_reg_n_0_[10]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg1_reg_n_0_[11]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg1_reg_n_0_[12]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg1_reg_n_0_[13]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg1_reg_n_0_[14]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg1_reg_n_0_[15]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg1_reg_n_0_[16]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg1_reg_n_0_[17]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg1_reg_n_0_[18]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg1_reg_n_0_[19]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \slv_reg1_reg_n_0_[1]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg1_reg_n_0_[20]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg1_reg_n_0_[21]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg1_reg_n_0_[22]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg1_reg_n_0_[23]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg1_reg_n_0_[24]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg1_reg_n_0_[25]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg1_reg_n_0_[26]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg1_reg_n_0_[27]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg1_reg_n_0_[28]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg1_reg_n_0_[29]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \slv_reg1_reg_n_0_[2]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg1_reg_n_0_[30]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg1_reg_n_0_[31]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \slv_reg1_reg_n_0_[3]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \slv_reg1_reg_n_0_[4]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \slv_reg1_reg_n_0_[5]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \slv_reg1_reg_n_0_[6]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \slv_reg1_reg_n_0_[7]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg1_reg_n_0_[8]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg1_reg_n_0_[9]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(1),
      I3 => p_0_in(0),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(2),
      I3 => p_0_in(0),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(3),
      I3 => p_0_in(0),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(0),
      I3 => p_0_in(0),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_D[4]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg2_D_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[0]\,
      Q => \slv_reg2_D_reg_n_0_[0]\,
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg2_D_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[1]\,
      Q => \slv_reg2_D_reg_n_0_[1]\,
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg2_D_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[2]\,
      Q => \slv_reg2_D_reg_n_0_[2]\,
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg2_D_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[3]\,
      Q => \slv_reg2_D_reg_n_0_[3]\,
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg2_D_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \slv_reg2_reg_n_0_[4]\,
      Q => blr_enable,
      R => \slv_reg2_D[4]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \slv_reg2_reg_n_0_[0]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg2_reg_n_0_[10]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg2_reg_n_0_[11]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg2_reg_n_0_[12]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg2_reg_n_0_[13]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg2_reg_n_0_[14]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg2_reg_n_0_[15]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg2_reg_n_0_[16]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg2_reg_n_0_[17]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg2_reg_n_0_[18]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg2_reg_n_0_[19]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \slv_reg2_reg_n_0_[1]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg2_reg_n_0_[20]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg2_reg_n_0_[21]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg2_reg_n_0_[22]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg2_reg_n_0_[23]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg2_reg_n_0_[24]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg2_reg_n_0_[25]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg2_reg_n_0_[26]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg2_reg_n_0_[27]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg2_reg_n_0_[28]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg2_reg_n_0_[29]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \slv_reg2_reg_n_0_[2]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg2_reg_n_0_[30]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg2_reg_n_0_[31]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \slv_reg2_reg_n_0_[3]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \slv_reg2_reg_n_0_[4]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \slv_reg2_reg_n_0_[5]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \slv_reg2_reg_n_0_[6]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \slv_reg2_reg_n_0_[7]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg2_reg_n_0_[8]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg2_reg_n_0_[9]\,
      R => axi_awready_i_1_n_0
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg3(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg3(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg3(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg3(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg3(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg3(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg3(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg3(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg3(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg3(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg3(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg3(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg3(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg3(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg3(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg3(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg3(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg3(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg3(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg3(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg3(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg3(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg3(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg3(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg3(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg3(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg3(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg3(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg3(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg3(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg3(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg3(9),
      R => axi_awready_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_blr_0_0_ip_blr_v3_0 is
  port (
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    resetn : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_blr_0_0_ip_blr_v3_0 : entity is "ip_blr_v3_0";
end design_1_ip_blr_0_0_ip_blr_v3_0;

architecture STRUCTURE of design_1_ip_blr_0_0_ip_blr_v3_0 is
begin
ip_blr_v3_0_S00_AXI_inst: entity work.design_1_ip_blr_0_0_ip_blr_v3_0_S00_AXI
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      clk => clk,
      resetn => resetn,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(1 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(1 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      x(15 downto 0) => x(15 downto 0),
      y(15 downto 0) => y(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_blr_0_0 is
  port (
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    y : out STD_LOGIC_VECTOR ( 15 downto 0 );
    resetn : in STD_LOGIC;
    clk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_blr_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_blr_0_0 : entity is "design_1_ip_blr_0_0,ip_blr_v3_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_ip_blr_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_1_ip_blr_0_0 : entity is "ip_blr_v3_0,Vivado 2019.1";
end design_1_ip_blr_0_0;

architecture STRUCTURE of design_1_ip_blr_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.design_1_ip_blr_0_0_ip_blr_v3_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      clk => clk,
      resetn => resetn,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(3 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(3 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      x(15 downto 0) => x(15 downto 0),
      y(15 downto 0) => y(15 downto 0)
    );
end STRUCTURE;
