-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 17:55:20 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_timers_0_0/design_1_ip_timers_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_timers_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_clk_lt_enable is
  port (
    geqOp : out STD_LOGIC;
    \slv_reg5_reg[3]\ : out STD_LOGIC;
    lt_clk_en_i_reg_0 : out STD_LOGIC;
    lt_clk_en_i_reg_1 : out STD_LOGIC;
    ce : out STD_LOGIC;
    \tmp_reg[16]_0\ : in STD_LOGIC;
    clk : in STD_LOGIC;
    \tmp_reg[16]_1\ : in STD_LOGIC;
    lt_dir : in STD_LOGIC_VECTOR ( 1 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 );
    clk_1kHz_en : in STD_LOGIC;
    clk_1kHz_enb : in STD_LOGIC;
    clk_1kHz_enc : in STD_LOGIC;
    lt_clk_en_i_reg_2 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_clk_lt_enable : entity is "clk_lt_enable";
end design_1_ip_timers_0_0_clk_lt_enable;

architecture STRUCTURE of design_1_ip_timers_0_0_clk_lt_enable is
  signal clk_lt_en : STD_LOGIC;
  signal \^geqop\ : STD_LOGIC;
  signal lt_clk_en_i_i_1_n_0 : STD_LOGIC;
  signal \^slv_reg5_reg[3]\ : STD_LOGIC;
  signal \tmp[0]_i_10_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_11_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_12_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_13_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_14_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_15_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_6_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_7_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_8_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_9_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_2_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_3_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_4_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_5_n_0\ : STD_LOGIC;
  signal \tmp[16]_i_2_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_2_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_3_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_4_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_5_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_2_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_3_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_4_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_5_n_0\ : STD_LOGIC;
  signal tmp_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \tmp_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_tmp_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tmp_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \tmp[0]_i_1__3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tmp[0]_i_1__4\ : label is "soft_lutpair0";
begin
  geqOp <= \^geqop\;
  \slv_reg5_reg[3]\ <= \^slv_reg5_reg[3]\;
lt_clk_en_i_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFBFBFBFAAAAAAAA"
    )
        port map (
      I0 => \^geqop\,
      I1 => \^slv_reg5_reg[3]\,
      I2 => lt_clk_en_i_reg_2,
      I3 => Q(5),
      I4 => Q(2),
      I5 => clk_lt_en,
      O => lt_clk_en_i_i_1_n_0
    );
lt_clk_en_i_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000D888"
    )
        port map (
      I0 => Q(1),
      I1 => Q(4),
      I2 => Q(3),
      I3 => Q(0),
      I4 => Q(2),
      O => \^slv_reg5_reg[3]\
    );
lt_clk_en_i_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \tmp_reg[16]_1\,
      D => lt_clk_en_i_i_1_n_0,
      Q => clk_lt_en
    );
\tmp[0]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E01"
    )
        port map (
      I0 => lt_dir(1),
      I1 => lt_dir(0),
      I2 => \^geqop\,
      I3 => tmp_reg(1),
      O => \tmp[0]_i_10_n_0\
    );
\tmp[0]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"010E"
    )
        port map (
      I0 => lt_dir(1),
      I1 => lt_dir(0),
      I2 => \^geqop\,
      I3 => tmp_reg(0),
      O => \tmp[0]_i_11_n_0\
    );
\tmp[0]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => tmp_reg(14),
      O => \tmp[0]_i_12_n_0\
    );
\tmp[0]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8880888088808080"
    )
        port map (
      I0 => tmp_reg(10),
      I1 => tmp_reg(9),
      I2 => tmp_reg(8),
      I3 => tmp_reg(7),
      I4 => \tmp[0]_i_14_n_0\,
      I5 => \tmp[0]_i_15_n_0\,
      O => \tmp[0]_i_13_n_0\
    );
\tmp[0]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => tmp_reg(0),
      I1 => tmp_reg(1),
      I2 => tmp_reg(2),
      I3 => tmp_reg(4),
      I4 => tmp_reg(3),
      O => \tmp[0]_i_14_n_0\
    );
\tmp[0]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tmp_reg(5),
      I1 => tmp_reg(6),
      O => \tmp[0]_i_15_n_0\
    );
\tmp[0]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => clk_lt_en,
      I1 => Q(2),
      I2 => clk_1kHz_en,
      O => lt_clk_en_i_reg_0
    );
\tmp[0]_i_1__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => clk_lt_en,
      I1 => Q(1),
      I2 => clk_1kHz_enb,
      O => lt_clk_en_i_reg_1
    );
\tmp[0]_i_1__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => clk_lt_en,
      I1 => Q(0),
      I2 => clk_1kHz_enc,
      O => ce
    );
\tmp[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888880"
    )
        port map (
      I0 => tmp_reg(16),
      I1 => tmp_reg(15),
      I2 => \tmp[0]_i_12_n_0\,
      I3 => tmp_reg(13),
      I4 => tmp_reg(12),
      I5 => \tmp[0]_i_13_n_0\,
      O => \^geqop\
    );
\tmp[0]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => lt_dir(0),
      I1 => lt_dir(1),
      I2 => \^geqop\,
      O => \tmp[0]_i_6_n_0\
    );
\tmp[0]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"54"
    )
        port map (
      I0 => \^geqop\,
      I1 => lt_dir(0),
      I2 => lt_dir(1),
      O => \tmp[0]_i_7_n_0\
    );
\tmp[0]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(3),
      I1 => \^geqop\,
      O => \tmp[0]_i_8_n_0\
    );
\tmp[0]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(2),
      I1 => \^geqop\,
      O => \tmp[0]_i_9_n_0\
    );
\tmp[12]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(15),
      I1 => \^geqop\,
      O => \tmp[12]_i_2_n_0\
    );
\tmp[12]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(14),
      I1 => \^geqop\,
      O => \tmp[12]_i_3_n_0\
    );
\tmp[12]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(13),
      I1 => \^geqop\,
      O => \tmp[12]_i_4_n_0\
    );
\tmp[12]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(12),
      I1 => \^geqop\,
      O => \tmp[12]_i_5_n_0\
    );
\tmp[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(16),
      I1 => \^geqop\,
      O => \tmp[16]_i_2_n_0\
    );
\tmp[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(7),
      I1 => \^geqop\,
      O => \tmp[4]_i_2_n_0\
    );
\tmp[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(6),
      I1 => \^geqop\,
      O => \tmp[4]_i_3_n_0\
    );
\tmp[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(5),
      I1 => \^geqop\,
      O => \tmp[4]_i_4_n_0\
    );
\tmp[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(4),
      I1 => \^geqop\,
      O => \tmp[4]_i_5_n_0\
    );
\tmp[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => \^geqop\,
      O => \tmp[8]_i_2_n_0\
    );
\tmp[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(10),
      I1 => \^geqop\,
      O => \tmp[8]_i_3_n_0\
    );
\tmp[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(9),
      I1 => \^geqop\,
      O => \tmp[8]_i_4_n_0\
    );
\tmp[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(8),
      I1 => \^geqop\,
      O => \tmp[8]_i_5_n_0\
    );
\tmp_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[0]_i_2_n_7\,
      Q => tmp_reg(0)
    );
\tmp_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tmp_reg[0]_i_2_n_0\,
      CO(2) => \tmp_reg[0]_i_2_n_1\,
      CO(1) => \tmp_reg[0]_i_2_n_2\,
      CO(0) => \tmp_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \tmp[0]_i_6_n_0\,
      DI(0) => \tmp[0]_i_7_n_0\,
      O(3) => \tmp_reg[0]_i_2_n_4\,
      O(2) => \tmp_reg[0]_i_2_n_5\,
      O(1) => \tmp_reg[0]_i_2_n_6\,
      O(0) => \tmp_reg[0]_i_2_n_7\,
      S(3) => \tmp[0]_i_8_n_0\,
      S(2) => \tmp[0]_i_9_n_0\,
      S(1) => \tmp[0]_i_10_n_0\,
      S(0) => \tmp[0]_i_11_n_0\
    );
\tmp_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[8]_i_1_n_5\,
      Q => tmp_reg(10)
    );
\tmp_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[8]_i_1_n_4\,
      Q => tmp_reg(11)
    );
\tmp_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[12]_i_1_n_7\,
      Q => tmp_reg(12)
    );
\tmp_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[8]_i_1_n_0\,
      CO(3) => \tmp_reg[12]_i_1_n_0\,
      CO(2) => \tmp_reg[12]_i_1_n_1\,
      CO(1) => \tmp_reg[12]_i_1_n_2\,
      CO(0) => \tmp_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[12]_i_1_n_4\,
      O(2) => \tmp_reg[12]_i_1_n_5\,
      O(1) => \tmp_reg[12]_i_1_n_6\,
      O(0) => \tmp_reg[12]_i_1_n_7\,
      S(3) => \tmp[12]_i_2_n_0\,
      S(2) => \tmp[12]_i_3_n_0\,
      S(1) => \tmp[12]_i_4_n_0\,
      S(0) => \tmp[12]_i_5_n_0\
    );
\tmp_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[12]_i_1_n_6\,
      Q => tmp_reg(13)
    );
\tmp_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[12]_i_1_n_5\,
      Q => tmp_reg(14)
    );
\tmp_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[12]_i_1_n_4\,
      Q => tmp_reg(15)
    );
\tmp_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[16]_i_1_n_7\,
      Q => tmp_reg(16)
    );
\tmp_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_tmp_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_tmp_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \tmp_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \tmp[16]_i_2_n_0\
    );
\tmp_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[0]_i_2_n_6\,
      Q => tmp_reg(1)
    );
\tmp_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[0]_i_2_n_5\,
      Q => tmp_reg(2)
    );
\tmp_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[0]_i_2_n_4\,
      Q => tmp_reg(3)
    );
\tmp_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[4]_i_1_n_7\,
      Q => tmp_reg(4)
    );
\tmp_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[0]_i_2_n_0\,
      CO(3) => \tmp_reg[4]_i_1_n_0\,
      CO(2) => \tmp_reg[4]_i_1_n_1\,
      CO(1) => \tmp_reg[4]_i_1_n_2\,
      CO(0) => \tmp_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[4]_i_1_n_4\,
      O(2) => \tmp_reg[4]_i_1_n_5\,
      O(1) => \tmp_reg[4]_i_1_n_6\,
      O(0) => \tmp_reg[4]_i_1_n_7\,
      S(3) => \tmp[4]_i_2_n_0\,
      S(2) => \tmp[4]_i_3_n_0\,
      S(1) => \tmp[4]_i_4_n_0\,
      S(0) => \tmp[4]_i_5_n_0\
    );
\tmp_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[4]_i_1_n_6\,
      Q => tmp_reg(5)
    );
\tmp_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[4]_i_1_n_5\,
      Q => tmp_reg(6)
    );
\tmp_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[4]_i_1_n_4\,
      Q => tmp_reg(7)
    );
\tmp_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[8]_i_1_n_7\,
      Q => tmp_reg(8)
    );
\tmp_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[4]_i_1_n_0\,
      CO(3) => \tmp_reg[8]_i_1_n_0\,
      CO(2) => \tmp_reg[8]_i_1_n_1\,
      CO(1) => \tmp_reg[8]_i_1_n_2\,
      CO(0) => \tmp_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[8]_i_1_n_4\,
      O(2) => \tmp_reg[8]_i_1_n_5\,
      O(1) => \tmp_reg[8]_i_1_n_6\,
      O(0) => \tmp_reg[8]_i_1_n_7\,
      S(3) => \tmp[8]_i_2_n_0\,
      S(2) => \tmp[8]_i_3_n_0\,
      S(1) => \tmp[8]_i_4_n_0\,
      S(0) => \tmp[8]_i_5_n_0\
    );
\tmp_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[16]_0\,
      CLR => \tmp_reg[16]_1\,
      D => \tmp_reg[8]_i_1_n_6\,
      Q => tmp_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_clock_1KHz_enable is
  port (
    clk_1kHz_en : out STD_LOGIC;
    eqOp : out STD_LOGIC;
    clk : in STD_LOGIC;
    clr1_out : in STD_LOGIC;
    \tmp_reg[15]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_clock_1KHz_enable : entity is "clock_1KHz_enable";
end design_1_ip_timers_0_0_clock_1KHz_enable;

architecture STRUCTURE of design_1_ip_timers_0_0_clock_1KHz_enable is
  signal clk_1kHz_en_i_i_3_n_0 : STD_LOGIC;
  signal clk_1kHz_en_i_i_4_n_0 : STD_LOGIC;
  signal clk_1kHz_en_i_i_5_n_0 : STD_LOGIC;
  signal \^eqop\ : STD_LOGIC;
  signal \tmp[0]_i_3_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_4__0_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_5__0_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_6__0_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_7__0_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_2__0_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_3__0_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_4__0_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_5__0_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_2__0_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_3__0_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_4__0_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_5__0_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_2__0_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_3__0_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_4__0_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_5__0_n_0\ : STD_LOGIC;
  signal tmp_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \tmp_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \NLW_tmp_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  eqOp <= \^eqop\;
clk_1kHz_en_i_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => clk_1kHz_en_i_i_3_n_0,
      I1 => tmp_reg(1),
      I2 => tmp_reg(0),
      I3 => tmp_reg(3),
      I4 => tmp_reg(2),
      I5 => clk_1kHz_en_i_i_4_n_0,
      O => \^eqop\
    );
clk_1kHz_en_i_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => tmp_reg(7),
      I1 => tmp_reg(6),
      I2 => tmp_reg(5),
      I3 => tmp_reg(4),
      O => clk_1kHz_en_i_i_3_n_0
    );
clk_1kHz_en_i_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => tmp_reg(12),
      I1 => tmp_reg(13),
      I2 => tmp_reg(14),
      I3 => tmp_reg(15),
      I4 => clk_1kHz_en_i_i_5_n_0,
      O => clk_1kHz_en_i_i_4_n_0
    );
clk_1kHz_en_i_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => tmp_reg(10),
      I2 => tmp_reg(9),
      I3 => tmp_reg(8),
      O => clk_1kHz_en_i_i_5_n_0
    );
clk_1kHz_en_i_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => clr1_out,
      D => \^eqop\,
      Q => clk_1kHz_en
    );
\tmp[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(0),
      I1 => \^eqop\,
      O => \tmp[0]_i_3_n_0\
    );
\tmp[0]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(3),
      I1 => \^eqop\,
      O => \tmp[0]_i_4__0_n_0\
    );
\tmp[0]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(2),
      I1 => \^eqop\,
      O => \tmp[0]_i_5__0_n_0\
    );
\tmp[0]_i_6__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(1),
      I1 => \^eqop\,
      O => \tmp[0]_i_6__0_n_0\
    );
\tmp[0]_i_7__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tmp_reg(0),
      I1 => \^eqop\,
      O => \tmp[0]_i_7__0_n_0\
    );
\tmp[12]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(15),
      I1 => \^eqop\,
      O => \tmp[12]_i_2__0_n_0\
    );
\tmp[12]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(14),
      I1 => \^eqop\,
      O => \tmp[12]_i_3__0_n_0\
    );
\tmp[12]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(13),
      I1 => \^eqop\,
      O => \tmp[12]_i_4__0_n_0\
    );
\tmp[12]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(12),
      I1 => \^eqop\,
      O => \tmp[12]_i_5__0_n_0\
    );
\tmp[4]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(7),
      I1 => \^eqop\,
      O => \tmp[4]_i_2__0_n_0\
    );
\tmp[4]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(6),
      I1 => \^eqop\,
      O => \tmp[4]_i_3__0_n_0\
    );
\tmp[4]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(5),
      I1 => \^eqop\,
      O => \tmp[4]_i_4__0_n_0\
    );
\tmp[4]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(4),
      I1 => \^eqop\,
      O => \tmp[4]_i_5__0_n_0\
    );
\tmp[8]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => \^eqop\,
      O => \tmp[8]_i_2__0_n_0\
    );
\tmp[8]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(10),
      I1 => \^eqop\,
      O => \tmp[8]_i_3__0_n_0\
    );
\tmp[8]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(9),
      I1 => \^eqop\,
      O => \tmp[8]_i_4__0_n_0\
    );
\tmp[8]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(8),
      I1 => \^eqop\,
      O => \tmp[8]_i_5__0_n_0\
    );
\tmp_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[0]_i_2__0_n_7\,
      Q => tmp_reg(0)
    );
\tmp_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tmp_reg[0]_i_2__0_n_0\,
      CO(2) => \tmp_reg[0]_i_2__0_n_1\,
      CO(1) => \tmp_reg[0]_i_2__0_n_2\,
      CO(0) => \tmp_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \tmp[0]_i_3_n_0\,
      O(3) => \tmp_reg[0]_i_2__0_n_4\,
      O(2) => \tmp_reg[0]_i_2__0_n_5\,
      O(1) => \tmp_reg[0]_i_2__0_n_6\,
      O(0) => \tmp_reg[0]_i_2__0_n_7\,
      S(3) => \tmp[0]_i_4__0_n_0\,
      S(2) => \tmp[0]_i_5__0_n_0\,
      S(1) => \tmp[0]_i_6__0_n_0\,
      S(0) => \tmp[0]_i_7__0_n_0\
    );
\tmp_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[8]_i_1__0_n_5\,
      Q => tmp_reg(10)
    );
\tmp_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[8]_i_1__0_n_4\,
      Q => tmp_reg(11)
    );
\tmp_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[12]_i_1__0_n_7\,
      Q => tmp_reg(12)
    );
\tmp_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[8]_i_1__0_n_0\,
      CO(3) => \NLW_tmp_reg[12]_i_1__0_CO_UNCONNECTED\(3),
      CO(2) => \tmp_reg[12]_i_1__0_n_1\,
      CO(1) => \tmp_reg[12]_i_1__0_n_2\,
      CO(0) => \tmp_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[12]_i_1__0_n_4\,
      O(2) => \tmp_reg[12]_i_1__0_n_5\,
      O(1) => \tmp_reg[12]_i_1__0_n_6\,
      O(0) => \tmp_reg[12]_i_1__0_n_7\,
      S(3) => \tmp[12]_i_2__0_n_0\,
      S(2) => \tmp[12]_i_3__0_n_0\,
      S(1) => \tmp[12]_i_4__0_n_0\,
      S(0) => \tmp[12]_i_5__0_n_0\
    );
\tmp_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[12]_i_1__0_n_6\,
      Q => tmp_reg(13)
    );
\tmp_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[12]_i_1__0_n_5\,
      Q => tmp_reg(14)
    );
\tmp_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[12]_i_1__0_n_4\,
      Q => tmp_reg(15)
    );
\tmp_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[0]_i_2__0_n_6\,
      Q => tmp_reg(1)
    );
\tmp_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[0]_i_2__0_n_5\,
      Q => tmp_reg(2)
    );
\tmp_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[0]_i_2__0_n_4\,
      Q => tmp_reg(3)
    );
\tmp_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[4]_i_1__0_n_7\,
      Q => tmp_reg(4)
    );
\tmp_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[0]_i_2__0_n_0\,
      CO(3) => \tmp_reg[4]_i_1__0_n_0\,
      CO(2) => \tmp_reg[4]_i_1__0_n_1\,
      CO(1) => \tmp_reg[4]_i_1__0_n_2\,
      CO(0) => \tmp_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[4]_i_1__0_n_4\,
      O(2) => \tmp_reg[4]_i_1__0_n_5\,
      O(1) => \tmp_reg[4]_i_1__0_n_6\,
      O(0) => \tmp_reg[4]_i_1__0_n_7\,
      S(3) => \tmp[4]_i_2__0_n_0\,
      S(2) => \tmp[4]_i_3__0_n_0\,
      S(1) => \tmp[4]_i_4__0_n_0\,
      S(0) => \tmp[4]_i_5__0_n_0\
    );
\tmp_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[4]_i_1__0_n_6\,
      Q => tmp_reg(5)
    );
\tmp_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[4]_i_1__0_n_5\,
      Q => tmp_reg(6)
    );
\tmp_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[4]_i_1__0_n_4\,
      Q => tmp_reg(7)
    );
\tmp_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[8]_i_1__0_n_7\,
      Q => tmp_reg(8)
    );
\tmp_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[4]_i_1__0_n_0\,
      CO(3) => \tmp_reg[8]_i_1__0_n_0\,
      CO(2) => \tmp_reg[8]_i_1__0_n_1\,
      CO(1) => \tmp_reg[8]_i_1__0_n_2\,
      CO(0) => \tmp_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[8]_i_1__0_n_4\,
      O(2) => \tmp_reg[8]_i_1__0_n_5\,
      O(1) => \tmp_reg[8]_i_1__0_n_6\,
      O(0) => \tmp_reg[8]_i_1__0_n_7\,
      S(3) => \tmp[8]_i_2__0_n_0\,
      S(2) => \tmp[8]_i_3__0_n_0\,
      S(1) => \tmp[8]_i_4__0_n_0\,
      S(0) => \tmp[8]_i_5__0_n_0\
    );
\tmp_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr1_out,
      D => \tmp_reg[8]_i_1__0_n_6\,
      Q => tmp_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_clock_1KHz_enable_0 is
  port (
    clk_1kHz_enb : out STD_LOGIC;
    eqOp : out STD_LOGIC;
    clk : in STD_LOGIC;
    \tmp_reg[0]_0\ : in STD_LOGIC;
    \tmp_reg[15]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_clock_1KHz_enable_0 : entity is "clock_1KHz_enable";
end design_1_ip_timers_0_0_clock_1KHz_enable_0;

architecture STRUCTURE of design_1_ip_timers_0_0_clock_1KHz_enable_0 is
  signal \clk_1kHz_en_i_i_3__0_n_0\ : STD_LOGIC;
  signal \clk_1kHz_en_i_i_4__0_n_0\ : STD_LOGIC;
  signal \clk_1kHz_en_i_i_5__0_n_0\ : STD_LOGIC;
  signal \^eqop\ : STD_LOGIC;
  signal \tmp[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_4__1_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_5__1_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_6__1_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_7__1_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_2__1_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_3__1_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_4__1_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_5__1_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_2__1_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_3__1_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_4__1_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_5__1_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_2__1_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_3__1_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_4__1_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_5__1_n_0\ : STD_LOGIC;
  signal tmp_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \tmp_reg[0]_i_2__1_n_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__1_n_1\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__1_n_2\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__1_n_3\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__1_n_4\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__1_n_5\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__1_n_6\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__1_n_7\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__1_n_1\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__1_n_2\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__1_n_3\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__1_n_4\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__1_n_5\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__1_n_6\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__1_n_7\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__1_n_1\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__1_n_2\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__1_n_3\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__1_n_4\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__1_n_5\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__1_n_6\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__1_n_7\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__1_n_1\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__1_n_2\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__1_n_3\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__1_n_4\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__1_n_5\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__1_n_6\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__1_n_7\ : STD_LOGIC;
  signal \NLW_tmp_reg[12]_i_1__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  eqOp <= \^eqop\;
\clk_1kHz_en_i_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \clk_1kHz_en_i_i_3__0_n_0\,
      I1 => tmp_reg(1),
      I2 => tmp_reg(0),
      I3 => tmp_reg(3),
      I4 => tmp_reg(2),
      I5 => \clk_1kHz_en_i_i_4__0_n_0\,
      O => \^eqop\
    );
\clk_1kHz_en_i_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => tmp_reg(7),
      I1 => tmp_reg(6),
      I2 => tmp_reg(5),
      I3 => tmp_reg(4),
      O => \clk_1kHz_en_i_i_3__0_n_0\
    );
\clk_1kHz_en_i_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => tmp_reg(12),
      I1 => tmp_reg(13),
      I2 => tmp_reg(14),
      I3 => tmp_reg(15),
      I4 => \clk_1kHz_en_i_i_5__0_n_0\,
      O => \clk_1kHz_en_i_i_4__0_n_0\
    );
\clk_1kHz_en_i_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => tmp_reg(10),
      I2 => tmp_reg(9),
      I3 => tmp_reg(8),
      O => \clk_1kHz_en_i_i_5__0_n_0\
    );
clk_1kHz_en_i_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => \tmp_reg[0]_0\,
      D => \^eqop\,
      Q => clk_1kHz_enb
    );
\tmp[0]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(0),
      I1 => \^eqop\,
      O => \tmp[0]_i_3__0_n_0\
    );
\tmp[0]_i_4__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(3),
      I1 => \^eqop\,
      O => \tmp[0]_i_4__1_n_0\
    );
\tmp[0]_i_5__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(2),
      I1 => \^eqop\,
      O => \tmp[0]_i_5__1_n_0\
    );
\tmp[0]_i_6__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(1),
      I1 => \^eqop\,
      O => \tmp[0]_i_6__1_n_0\
    );
\tmp[0]_i_7__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tmp_reg(0),
      I1 => \^eqop\,
      O => \tmp[0]_i_7__1_n_0\
    );
\tmp[12]_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(15),
      I1 => \^eqop\,
      O => \tmp[12]_i_2__1_n_0\
    );
\tmp[12]_i_3__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(14),
      I1 => \^eqop\,
      O => \tmp[12]_i_3__1_n_0\
    );
\tmp[12]_i_4__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(13),
      I1 => \^eqop\,
      O => \tmp[12]_i_4__1_n_0\
    );
\tmp[12]_i_5__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(12),
      I1 => \^eqop\,
      O => \tmp[12]_i_5__1_n_0\
    );
\tmp[4]_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(7),
      I1 => \^eqop\,
      O => \tmp[4]_i_2__1_n_0\
    );
\tmp[4]_i_3__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(6),
      I1 => \^eqop\,
      O => \tmp[4]_i_3__1_n_0\
    );
\tmp[4]_i_4__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(5),
      I1 => \^eqop\,
      O => \tmp[4]_i_4__1_n_0\
    );
\tmp[4]_i_5__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(4),
      I1 => \^eqop\,
      O => \tmp[4]_i_5__1_n_0\
    );
\tmp[8]_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => \^eqop\,
      O => \tmp[8]_i_2__1_n_0\
    );
\tmp[8]_i_3__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(10),
      I1 => \^eqop\,
      O => \tmp[8]_i_3__1_n_0\
    );
\tmp[8]_i_4__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(9),
      I1 => \^eqop\,
      O => \tmp[8]_i_4__1_n_0\
    );
\tmp[8]_i_5__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(8),
      I1 => \^eqop\,
      O => \tmp[8]_i_5__1_n_0\
    );
\tmp_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[0]_i_2__1_n_7\,
      Q => tmp_reg(0)
    );
\tmp_reg[0]_i_2__1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tmp_reg[0]_i_2__1_n_0\,
      CO(2) => \tmp_reg[0]_i_2__1_n_1\,
      CO(1) => \tmp_reg[0]_i_2__1_n_2\,
      CO(0) => \tmp_reg[0]_i_2__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \tmp[0]_i_3__0_n_0\,
      O(3) => \tmp_reg[0]_i_2__1_n_4\,
      O(2) => \tmp_reg[0]_i_2__1_n_5\,
      O(1) => \tmp_reg[0]_i_2__1_n_6\,
      O(0) => \tmp_reg[0]_i_2__1_n_7\,
      S(3) => \tmp[0]_i_4__1_n_0\,
      S(2) => \tmp[0]_i_5__1_n_0\,
      S(1) => \tmp[0]_i_6__1_n_0\,
      S(0) => \tmp[0]_i_7__1_n_0\
    );
\tmp_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[8]_i_1__1_n_5\,
      Q => tmp_reg(10)
    );
\tmp_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[8]_i_1__1_n_4\,
      Q => tmp_reg(11)
    );
\tmp_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[12]_i_1__1_n_7\,
      Q => tmp_reg(12)
    );
\tmp_reg[12]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[8]_i_1__1_n_0\,
      CO(3) => \NLW_tmp_reg[12]_i_1__1_CO_UNCONNECTED\(3),
      CO(2) => \tmp_reg[12]_i_1__1_n_1\,
      CO(1) => \tmp_reg[12]_i_1__1_n_2\,
      CO(0) => \tmp_reg[12]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[12]_i_1__1_n_4\,
      O(2) => \tmp_reg[12]_i_1__1_n_5\,
      O(1) => \tmp_reg[12]_i_1__1_n_6\,
      O(0) => \tmp_reg[12]_i_1__1_n_7\,
      S(3) => \tmp[12]_i_2__1_n_0\,
      S(2) => \tmp[12]_i_3__1_n_0\,
      S(1) => \tmp[12]_i_4__1_n_0\,
      S(0) => \tmp[12]_i_5__1_n_0\
    );
\tmp_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[12]_i_1__1_n_6\,
      Q => tmp_reg(13)
    );
\tmp_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[12]_i_1__1_n_5\,
      Q => tmp_reg(14)
    );
\tmp_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[12]_i_1__1_n_4\,
      Q => tmp_reg(15)
    );
\tmp_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[0]_i_2__1_n_6\,
      Q => tmp_reg(1)
    );
\tmp_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[0]_i_2__1_n_5\,
      Q => tmp_reg(2)
    );
\tmp_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[0]_i_2__1_n_4\,
      Q => tmp_reg(3)
    );
\tmp_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[4]_i_1__1_n_7\,
      Q => tmp_reg(4)
    );
\tmp_reg[4]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[0]_i_2__1_n_0\,
      CO(3) => \tmp_reg[4]_i_1__1_n_0\,
      CO(2) => \tmp_reg[4]_i_1__1_n_1\,
      CO(1) => \tmp_reg[4]_i_1__1_n_2\,
      CO(0) => \tmp_reg[4]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[4]_i_1__1_n_4\,
      O(2) => \tmp_reg[4]_i_1__1_n_5\,
      O(1) => \tmp_reg[4]_i_1__1_n_6\,
      O(0) => \tmp_reg[4]_i_1__1_n_7\,
      S(3) => \tmp[4]_i_2__1_n_0\,
      S(2) => \tmp[4]_i_3__1_n_0\,
      S(1) => \tmp[4]_i_4__1_n_0\,
      S(0) => \tmp[4]_i_5__1_n_0\
    );
\tmp_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[4]_i_1__1_n_6\,
      Q => tmp_reg(5)
    );
\tmp_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[4]_i_1__1_n_5\,
      Q => tmp_reg(6)
    );
\tmp_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[4]_i_1__1_n_4\,
      Q => tmp_reg(7)
    );
\tmp_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[8]_i_1__1_n_7\,
      Q => tmp_reg(8)
    );
\tmp_reg[8]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[4]_i_1__1_n_0\,
      CO(3) => \tmp_reg[8]_i_1__1_n_0\,
      CO(2) => \tmp_reg[8]_i_1__1_n_1\,
      CO(1) => \tmp_reg[8]_i_1__1_n_2\,
      CO(0) => \tmp_reg[8]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[8]_i_1__1_n_4\,
      O(2) => \tmp_reg[8]_i_1__1_n_5\,
      O(1) => \tmp_reg[8]_i_1__1_n_6\,
      O(0) => \tmp_reg[8]_i_1__1_n_7\,
      S(3) => \tmp[8]_i_2__1_n_0\,
      S(2) => \tmp[8]_i_3__1_n_0\,
      S(1) => \tmp[8]_i_4__1_n_0\,
      S(0) => \tmp[8]_i_5__1_n_0\
    );
\tmp_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => \tmp_reg[0]_0\,
      D => \tmp_reg[8]_i_1__1_n_6\,
      Q => tmp_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_clock_1KHz_enable_1 is
  port (
    clk_1kHz_enc : out STD_LOGIC;
    eqOp : out STD_LOGIC;
    clk : in STD_LOGIC;
    clr0_out : in STD_LOGIC;
    \tmp_reg[15]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_clock_1KHz_enable_1 : entity is "clock_1KHz_enable";
end design_1_ip_timers_0_0_clock_1KHz_enable_1;

architecture STRUCTURE of design_1_ip_timers_0_0_clock_1KHz_enable_1 is
  signal \clk_1kHz_en_i_i_3__1_n_0\ : STD_LOGIC;
  signal \clk_1kHz_en_i_i_4__1_n_0\ : STD_LOGIC;
  signal \clk_1kHz_en_i_i_5__1_n_0\ : STD_LOGIC;
  signal \^eqop\ : STD_LOGIC;
  signal \tmp[0]_i_3__1_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_4__2_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_5__2_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_6__2_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_7__2_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_2__2_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_3__2_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_4__2_n_0\ : STD_LOGIC;
  signal \tmp[12]_i_5__2_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_2__2_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_3__2_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_4__2_n_0\ : STD_LOGIC;
  signal \tmp[4]_i_5__2_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_2__2_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_3__2_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_4__2_n_0\ : STD_LOGIC;
  signal \tmp[8]_i_5__2_n_0\ : STD_LOGIC;
  signal tmp_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \tmp_reg[0]_i_2__2_n_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__2_n_1\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__2_n_2\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__2_n_3\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__2_n_4\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__2_n_5\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__2_n_6\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__2_n_7\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__2_n_1\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__2_n_2\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__2_n_3\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__2_n_4\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__2_n_5\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__2_n_6\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__2_n_7\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__2_n_1\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__2_n_2\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__2_n_3\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__2_n_4\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__2_n_5\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__2_n_6\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__2_n_7\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__2_n_1\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__2_n_2\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__2_n_3\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__2_n_4\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__2_n_5\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__2_n_6\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__2_n_7\ : STD_LOGIC;
  signal \NLW_tmp_reg[12]_i_1__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  eqOp <= \^eqop\;
\clk_1kHz_en_i_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \clk_1kHz_en_i_i_3__1_n_0\,
      I1 => tmp_reg(1),
      I2 => tmp_reg(0),
      I3 => tmp_reg(3),
      I4 => tmp_reg(2),
      I5 => \clk_1kHz_en_i_i_4__1_n_0\,
      O => \^eqop\
    );
\clk_1kHz_en_i_i_3__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => tmp_reg(7),
      I1 => tmp_reg(6),
      I2 => tmp_reg(5),
      I3 => tmp_reg(4),
      O => \clk_1kHz_en_i_i_3__1_n_0\
    );
\clk_1kHz_en_i_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => tmp_reg(12),
      I1 => tmp_reg(13),
      I2 => tmp_reg(14),
      I3 => tmp_reg(15),
      I4 => \clk_1kHz_en_i_i_5__1_n_0\,
      O => \clk_1kHz_en_i_i_4__1_n_0\
    );
\clk_1kHz_en_i_i_5__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => tmp_reg(10),
      I2 => tmp_reg(9),
      I3 => tmp_reg(8),
      O => \clk_1kHz_en_i_i_5__1_n_0\
    );
clk_1kHz_en_i_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => clr0_out,
      D => \^eqop\,
      Q => clk_1kHz_enc
    );
\tmp[0]_i_3__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(0),
      I1 => \^eqop\,
      O => \tmp[0]_i_3__1_n_0\
    );
\tmp[0]_i_4__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(3),
      I1 => \^eqop\,
      O => \tmp[0]_i_4__2_n_0\
    );
\tmp[0]_i_5__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(2),
      I1 => \^eqop\,
      O => \tmp[0]_i_5__2_n_0\
    );
\tmp[0]_i_6__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(1),
      I1 => \^eqop\,
      O => \tmp[0]_i_6__2_n_0\
    );
\tmp[0]_i_7__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tmp_reg(0),
      I1 => \^eqop\,
      O => \tmp[0]_i_7__2_n_0\
    );
\tmp[12]_i_2__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(15),
      I1 => \^eqop\,
      O => \tmp[12]_i_2__2_n_0\
    );
\tmp[12]_i_3__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(14),
      I1 => \^eqop\,
      O => \tmp[12]_i_3__2_n_0\
    );
\tmp[12]_i_4__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(13),
      I1 => \^eqop\,
      O => \tmp[12]_i_4__2_n_0\
    );
\tmp[12]_i_5__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(12),
      I1 => \^eqop\,
      O => \tmp[12]_i_5__2_n_0\
    );
\tmp[4]_i_2__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(7),
      I1 => \^eqop\,
      O => \tmp[4]_i_2__2_n_0\
    );
\tmp[4]_i_3__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(6),
      I1 => \^eqop\,
      O => \tmp[4]_i_3__2_n_0\
    );
\tmp[4]_i_4__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(5),
      I1 => \^eqop\,
      O => \tmp[4]_i_4__2_n_0\
    );
\tmp[4]_i_5__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(4),
      I1 => \^eqop\,
      O => \tmp[4]_i_5__2_n_0\
    );
\tmp[8]_i_2__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => \^eqop\,
      O => \tmp[8]_i_2__2_n_0\
    );
\tmp[8]_i_3__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(10),
      I1 => \^eqop\,
      O => \tmp[8]_i_3__2_n_0\
    );
\tmp[8]_i_4__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(9),
      I1 => \^eqop\,
      O => \tmp[8]_i_4__2_n_0\
    );
\tmp[8]_i_5__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_reg(8),
      I1 => \^eqop\,
      O => \tmp[8]_i_5__2_n_0\
    );
\tmp_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[0]_i_2__2_n_7\,
      Q => tmp_reg(0)
    );
\tmp_reg[0]_i_2__2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tmp_reg[0]_i_2__2_n_0\,
      CO(2) => \tmp_reg[0]_i_2__2_n_1\,
      CO(1) => \tmp_reg[0]_i_2__2_n_2\,
      CO(0) => \tmp_reg[0]_i_2__2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \tmp[0]_i_3__1_n_0\,
      O(3) => \tmp_reg[0]_i_2__2_n_4\,
      O(2) => \tmp_reg[0]_i_2__2_n_5\,
      O(1) => \tmp_reg[0]_i_2__2_n_6\,
      O(0) => \tmp_reg[0]_i_2__2_n_7\,
      S(3) => \tmp[0]_i_4__2_n_0\,
      S(2) => \tmp[0]_i_5__2_n_0\,
      S(1) => \tmp[0]_i_6__2_n_0\,
      S(0) => \tmp[0]_i_7__2_n_0\
    );
\tmp_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[8]_i_1__2_n_5\,
      Q => tmp_reg(10)
    );
\tmp_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[8]_i_1__2_n_4\,
      Q => tmp_reg(11)
    );
\tmp_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[12]_i_1__2_n_7\,
      Q => tmp_reg(12)
    );
\tmp_reg[12]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[8]_i_1__2_n_0\,
      CO(3) => \NLW_tmp_reg[12]_i_1__2_CO_UNCONNECTED\(3),
      CO(2) => \tmp_reg[12]_i_1__2_n_1\,
      CO(1) => \tmp_reg[12]_i_1__2_n_2\,
      CO(0) => \tmp_reg[12]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[12]_i_1__2_n_4\,
      O(2) => \tmp_reg[12]_i_1__2_n_5\,
      O(1) => \tmp_reg[12]_i_1__2_n_6\,
      O(0) => \tmp_reg[12]_i_1__2_n_7\,
      S(3) => \tmp[12]_i_2__2_n_0\,
      S(2) => \tmp[12]_i_3__2_n_0\,
      S(1) => \tmp[12]_i_4__2_n_0\,
      S(0) => \tmp[12]_i_5__2_n_0\
    );
\tmp_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[12]_i_1__2_n_6\,
      Q => tmp_reg(13)
    );
\tmp_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[12]_i_1__2_n_5\,
      Q => tmp_reg(14)
    );
\tmp_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[12]_i_1__2_n_4\,
      Q => tmp_reg(15)
    );
\tmp_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[0]_i_2__2_n_6\,
      Q => tmp_reg(1)
    );
\tmp_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[0]_i_2__2_n_5\,
      Q => tmp_reg(2)
    );
\tmp_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[0]_i_2__2_n_4\,
      Q => tmp_reg(3)
    );
\tmp_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[4]_i_1__2_n_7\,
      Q => tmp_reg(4)
    );
\tmp_reg[4]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[0]_i_2__2_n_0\,
      CO(3) => \tmp_reg[4]_i_1__2_n_0\,
      CO(2) => \tmp_reg[4]_i_1__2_n_1\,
      CO(1) => \tmp_reg[4]_i_1__2_n_2\,
      CO(0) => \tmp_reg[4]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[4]_i_1__2_n_4\,
      O(2) => \tmp_reg[4]_i_1__2_n_5\,
      O(1) => \tmp_reg[4]_i_1__2_n_6\,
      O(0) => \tmp_reg[4]_i_1__2_n_7\,
      S(3) => \tmp[4]_i_2__2_n_0\,
      S(2) => \tmp[4]_i_3__2_n_0\,
      S(1) => \tmp[4]_i_4__2_n_0\,
      S(0) => \tmp[4]_i_5__2_n_0\
    );
\tmp_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[4]_i_1__2_n_6\,
      Q => tmp_reg(5)
    );
\tmp_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[4]_i_1__2_n_5\,
      Q => tmp_reg(6)
    );
\tmp_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[4]_i_1__2_n_4\,
      Q => tmp_reg(7)
    );
\tmp_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[8]_i_1__2_n_7\,
      Q => tmp_reg(8)
    );
\tmp_reg[8]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[4]_i_1__2_n_0\,
      CO(3) => \tmp_reg[8]_i_1__2_n_0\,
      CO(2) => \tmp_reg[8]_i_1__2_n_1\,
      CO(1) => \tmp_reg[8]_i_1__2_n_2\,
      CO(0) => \tmp_reg[8]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[8]_i_1__2_n_4\,
      O(2) => \tmp_reg[8]_i_1__2_n_5\,
      O(1) => \tmp_reg[8]_i_1__2_n_6\,
      O(0) => \tmp_reg[8]_i_1__2_n_7\,
      S(3) => \tmp[8]_i_2__2_n_0\,
      S(2) => \tmp[8]_i_3__2_n_0\,
      S(1) => \tmp[8]_i_4__2_n_0\,
      S(0) => \tmp[8]_i_5__2_n_0\
    );
\tmp_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \tmp_reg[15]_0\,
      CLR => clr0_out,
      D => \tmp_reg[8]_i_1__2_n_6\,
      Q => tmp_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_comparator is
  port (
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \cmp0_carry__1_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \cmp0_carry__1_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg7_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg7_reg[0]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg7_reg[0]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg7_reg[0]_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_comparator : entity is "comparator";
end design_1_ip_timers_0_0_comparator;

architecture STRUCTURE of design_1_ip_timers_0_0_comparator is
  signal \cmp0_carry__0_n_0\ : STD_LOGIC;
  signal \cmp0_carry__0_n_1\ : STD_LOGIC;
  signal \cmp0_carry__0_n_2\ : STD_LOGIC;
  signal \cmp0_carry__0_n_3\ : STD_LOGIC;
  signal \cmp0_carry__1_n_0\ : STD_LOGIC;
  signal \cmp0_carry__1_n_1\ : STD_LOGIC;
  signal \cmp0_carry__1_n_2\ : STD_LOGIC;
  signal \cmp0_carry__1_n_3\ : STD_LOGIC;
  signal \cmp0_carry__2_n_1\ : STD_LOGIC;
  signal \cmp0_carry__2_n_2\ : STD_LOGIC;
  signal \cmp0_carry__2_n_3\ : STD_LOGIC;
  signal cmp0_carry_n_0 : STD_LOGIC;
  signal cmp0_carry_n_1 : STD_LOGIC;
  signal cmp0_carry_n_2 : STD_LOGIC;
  signal cmp0_carry_n_3 : STD_LOGIC;
  signal NLW_cmp0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cmp0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cmp0_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cmp0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
cmp0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cmp0_carry_n_0,
      CO(2) => cmp0_carry_n_1,
      CO(1) => cmp0_carry_n_2,
      CO(0) => cmp0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => NLW_cmp0_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\cmp0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cmp0_carry_n_0,
      CO(3) => \cmp0_carry__0_n_0\,
      CO(2) => \cmp0_carry__0_n_1\,
      CO(1) => \cmp0_carry__0_n_2\,
      CO(0) => \cmp0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \cmp0_carry__1_0\(3 downto 0),
      O(3 downto 0) => \NLW_cmp0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \cmp0_carry__1_1\(3 downto 0)
    );
\cmp0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cmp0_carry__0_n_0\,
      CO(3) => \cmp0_carry__1_n_0\,
      CO(2) => \cmp0_carry__1_n_1\,
      CO(1) => \cmp0_carry__1_n_2\,
      CO(0) => \cmp0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \slv_reg7_reg[0]\(3 downto 0),
      O(3 downto 0) => \NLW_cmp0_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \slv_reg7_reg[0]_0\(3 downto 0)
    );
\cmp0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cmp0_carry__1_n_0\,
      CO(3) => CO(0),
      CO(2) => \cmp0_carry__2_n_1\,
      CO(1) => \cmp0_carry__2_n_2\,
      CO(0) => \cmp0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \slv_reg7_reg[0]_1\(3 downto 0),
      O(3 downto 0) => \NLW_cmp0_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \slv_reg7_reg[0]_2\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_counter is
  port (
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_araddr_reg[4]\ : out STD_LOGIC_VECTOR ( 29 downto 0 );
    \tmp_reg[31]_0\ : in STD_LOGIC;
    clk : in STD_LOGIC;
    clr1_out : in STD_LOGIC;
    \axi_rdata_reg[0]\ : in STD_LOGIC;
    sel0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[0]_0\ : in STD_LOGIC;
    \axi_rdata_reg[0]_1\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 29 downto 0 );
    \axi_rdata_reg[3]\ : in STD_LOGIC;
    \axi_rdata_reg[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata_reg[4]\ : in STD_LOGIC;
    \axi_rdata_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[5]\ : in STD_LOGIC;
    \axi_rdata_reg[6]\ : in STD_LOGIC;
    \axi_rdata_reg[7]_0\ : in STD_LOGIC;
    \axi_rdata_reg[8]\ : in STD_LOGIC;
    \axi_rdata_reg[11]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[9]\ : in STD_LOGIC;
    \axi_rdata_reg[10]\ : in STD_LOGIC;
    \axi_rdata_reg[11]_0\ : in STD_LOGIC;
    \axi_rdata_reg[12]\ : in STD_LOGIC;
    \axi_rdata_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[13]\ : in STD_LOGIC;
    \axi_rdata_reg[14]\ : in STD_LOGIC;
    \axi_rdata_reg[15]_0\ : in STD_LOGIC;
    \axi_rdata_reg[16]\ : in STD_LOGIC;
    \axi_rdata_reg[19]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[17]\ : in STD_LOGIC;
    \axi_rdata_reg[18]\ : in STD_LOGIC;
    \axi_rdata_reg[19]_0\ : in STD_LOGIC;
    \axi_rdata_reg[20]\ : in STD_LOGIC;
    \axi_rdata_reg[23]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[21]\ : in STD_LOGIC;
    \axi_rdata_reg[22]\ : in STD_LOGIC;
    \axi_rdata_reg[23]_0\ : in STD_LOGIC;
    \axi_rdata_reg[24]\ : in STD_LOGIC;
    \axi_rdata_reg[27]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[25]\ : in STD_LOGIC;
    \axi_rdata_reg[26]\ : in STD_LOGIC;
    \axi_rdata_reg[27]_0\ : in STD_LOGIC;
    \axi_rdata_reg[28]\ : in STD_LOGIC;
    \axi_rdata_reg[31]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[29]\ : in STD_LOGIC;
    \axi_rdata_reg[30]\ : in STD_LOGIC;
    \axi_rdata_reg[31]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_counter : entity is "counter";
end design_1_ip_timers_0_0_counter;

architecture STRUCTURE of design_1_ip_timers_0_0_counter is
  signal \^s\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_3__4_n_0\ : STD_LOGIC;
  signal tmp_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \tmp_reg[0]_i_2__4_n_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__4_n_1\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__4_n_2\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__4_n_3\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__4_n_4\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__4_n_5\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__4_n_6\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__4_n_7\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__4_n_0\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__4_n_1\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__4_n_2\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__4_n_3\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__4_n_4\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__4_n_5\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__4_n_6\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__4_n_7\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__1_n_0\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__1_n_1\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__1_n_2\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__1_n_3\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__1_n_4\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__1_n_5\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__1_n_6\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__1_n_7\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__0_n_0\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__0_n_1\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__0_n_2\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__0_n_3\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__0_n_4\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__0_n_5\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__0_n_6\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__0_n_7\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__0_n_0\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__0_n_1\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__0_n_2\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__0_n_3\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__0_n_4\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__0_n_5\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__0_n_6\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__0_n_7\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__0_n_1\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__0_n_2\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__0_n_3\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__0_n_4\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__0_n_5\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__0_n_6\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__0_n_7\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__4_n_0\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__4_n_1\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__4_n_2\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__4_n_3\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__4_n_4\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__4_n_5\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__4_n_6\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__4_n_7\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__4_n_0\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__4_n_1\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__4_n_2\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__4_n_3\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__4_n_4\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__4_n_5\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__4_n_6\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__4_n_7\ : STD_LOGIC;
  signal \NLW_tmp_reg[28]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  S(1 downto 0) <= \^s\(1 downto 0);
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8B8B8B8B8B8B8"
    )
        port map (
      I0 => \axi_rdata_reg[0]\,
      I1 => sel0(2),
      I2 => \axi_rdata[0]_i_3_n_0\,
      I3 => \axi_rdata_reg[0]_0\,
      I4 => sel0(1),
      I5 => sel0(0),
      O => \axi_araddr_reg[4]\(0)
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(0),
      I1 => \axi_rdata_reg[0]_1\,
      I2 => D(0),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(10),
      I1 => \axi_rdata_reg[11]\(2),
      I2 => D(8),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(11),
      I1 => \axi_rdata_reg[11]\(3),
      I2 => D(9),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(12),
      I1 => \axi_rdata_reg[15]\(0),
      I2 => D(10),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(13),
      I1 => \axi_rdata_reg[15]\(1),
      I2 => D(11),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(14),
      I1 => \axi_rdata_reg[15]\(2),
      I2 => D(12),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(15),
      I1 => \axi_rdata_reg[15]\(3),
      I2 => D(13),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(16),
      I1 => \axi_rdata_reg[19]\(0),
      I2 => D(14),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(17),
      I1 => \axi_rdata_reg[19]\(1),
      I2 => D(15),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(18),
      I1 => \axi_rdata_reg[19]\(2),
      I2 => D(16),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(19),
      I1 => \axi_rdata_reg[19]\(3),
      I2 => D(17),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(20),
      I1 => \axi_rdata_reg[23]\(0),
      I2 => D(18),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(21),
      I1 => \axi_rdata_reg[23]\(1),
      I2 => D(19),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(22),
      I1 => \axi_rdata_reg[23]\(2),
      I2 => D(20),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(23),
      I1 => \axi_rdata_reg[23]\(3),
      I2 => D(21),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(24),
      I1 => \axi_rdata_reg[27]\(0),
      I2 => D(22),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(25),
      I1 => \axi_rdata_reg[27]\(1),
      I2 => D(23),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(26),
      I1 => \axi_rdata_reg[27]\(2),
      I2 => D(24),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(27),
      I1 => \axi_rdata_reg[27]\(3),
      I2 => D(25),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(28),
      I1 => \axi_rdata_reg[31]\(0),
      I2 => D(26),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(29),
      I1 => \axi_rdata_reg[31]\(1),
      I2 => D(27),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(30),
      I1 => \axi_rdata_reg[31]\(2),
      I2 => D(28),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(31),
      I1 => \axi_rdata_reg[31]\(3),
      I2 => D(29),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(3),
      I1 => \axi_rdata_reg[3]_0\(0),
      I2 => D(1),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(4),
      I1 => \axi_rdata_reg[7]\(0),
      I2 => D(2),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(5),
      I1 => \axi_rdata_reg[7]\(1),
      I2 => D(3),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(6),
      I1 => \axi_rdata_reg[7]\(2),
      I2 => D(4),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(7),
      I1 => \axi_rdata_reg[7]\(3),
      I2 => D(5),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(8),
      I1 => \axi_rdata_reg[11]\(0),
      I2 => D(6),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => tmp_reg(9),
      I1 => \axi_rdata_reg[11]\(1),
      I2 => D(7),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata_reg[10]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => \axi_rdata_reg[10]\,
      O => \axi_araddr_reg[4]\(8),
      S => sel0(2)
    );
\axi_rdata_reg[11]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => \axi_rdata_reg[11]_0\,
      O => \axi_araddr_reg[4]\(9),
      S => sel0(2)
    );
\axi_rdata_reg[12]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => \axi_rdata_reg[12]\,
      O => \axi_araddr_reg[4]\(10),
      S => sel0(2)
    );
\axi_rdata_reg[13]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => \axi_rdata_reg[13]\,
      O => \axi_araddr_reg[4]\(11),
      S => sel0(2)
    );
\axi_rdata_reg[14]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => \axi_rdata_reg[14]\,
      O => \axi_araddr_reg[4]\(12),
      S => sel0(2)
    );
\axi_rdata_reg[15]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata_reg[15]_0\,
      O => \axi_araddr_reg[4]\(13),
      S => sel0(2)
    );
\axi_rdata_reg[16]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_2_n_0\,
      I1 => \axi_rdata_reg[16]\,
      O => \axi_araddr_reg[4]\(14),
      S => sel0(2)
    );
\axi_rdata_reg[17]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_2_n_0\,
      I1 => \axi_rdata_reg[17]\,
      O => \axi_araddr_reg[4]\(15),
      S => sel0(2)
    );
\axi_rdata_reg[18]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => \axi_rdata_reg[18]\,
      O => \axi_araddr_reg[4]\(16),
      S => sel0(2)
    );
\axi_rdata_reg[19]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_2_n_0\,
      I1 => \axi_rdata_reg[19]_0\,
      O => \axi_araddr_reg[4]\(17),
      S => sel0(2)
    );
\axi_rdata_reg[20]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_2_n_0\,
      I1 => \axi_rdata_reg[20]\,
      O => \axi_araddr_reg[4]\(18),
      S => sel0(2)
    );
\axi_rdata_reg[21]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_2_n_0\,
      I1 => \axi_rdata_reg[21]\,
      O => \axi_araddr_reg[4]\(19),
      S => sel0(2)
    );
\axi_rdata_reg[22]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_2_n_0\,
      I1 => \axi_rdata_reg[22]\,
      O => \axi_araddr_reg[4]\(20),
      S => sel0(2)
    );
\axi_rdata_reg[23]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_2_n_0\,
      I1 => \axi_rdata_reg[23]_0\,
      O => \axi_araddr_reg[4]\(21),
      S => sel0(2)
    );
\axi_rdata_reg[24]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => \axi_rdata_reg[24]\,
      O => \axi_araddr_reg[4]\(22),
      S => sel0(2)
    );
\axi_rdata_reg[25]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => \axi_rdata_reg[25]\,
      O => \axi_araddr_reg[4]\(23),
      S => sel0(2)
    );
\axi_rdata_reg[26]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_2_n_0\,
      I1 => \axi_rdata_reg[26]\,
      O => \axi_araddr_reg[4]\(24),
      S => sel0(2)
    );
\axi_rdata_reg[27]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_2_n_0\,
      I1 => \axi_rdata_reg[27]_0\,
      O => \axi_araddr_reg[4]\(25),
      S => sel0(2)
    );
\axi_rdata_reg[28]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => \axi_rdata_reg[28]\,
      O => \axi_araddr_reg[4]\(26),
      S => sel0(2)
    );
\axi_rdata_reg[29]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_2_n_0\,
      I1 => \axi_rdata_reg[29]\,
      O => \axi_araddr_reg[4]\(27),
      S => sel0(2)
    );
\axi_rdata_reg[30]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => \axi_rdata_reg[30]\,
      O => \axi_araddr_reg[4]\(28),
      S => sel0(2)
    );
\axi_rdata_reg[31]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_3_n_0\,
      I1 => \axi_rdata_reg[31]_0\,
      O => \axi_araddr_reg[4]\(29),
      S => sel0(2)
    );
\axi_rdata_reg[3]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => \axi_rdata_reg[3]\,
      O => \axi_araddr_reg[4]\(1),
      S => sel0(2)
    );
\axi_rdata_reg[4]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => \axi_rdata_reg[4]\,
      O => \axi_araddr_reg[4]\(2),
      S => sel0(2)
    );
\axi_rdata_reg[5]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_2_n_0\,
      I1 => \axi_rdata_reg[5]\,
      O => \axi_araddr_reg[4]\(3),
      S => sel0(2)
    );
\axi_rdata_reg[6]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_2_n_0\,
      I1 => \axi_rdata_reg[6]\,
      O => \axi_araddr_reg[4]\(4),
      S => sel0(2)
    );
\axi_rdata_reg[7]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_2_n_0\,
      I1 => \axi_rdata_reg[7]_0\,
      O => \axi_araddr_reg[4]\(5),
      S => sel0(2)
    );
\axi_rdata_reg[8]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => \axi_rdata_reg[8]\,
      O => \axi_araddr_reg[4]\(6),
      S => sel0(2)
    );
\axi_rdata_reg[9]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => \axi_rdata_reg[9]\,
      O => \axi_araddr_reg[4]\(7),
      S => sel0(2)
    );
\tmp[0]_i_3__4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tmp_reg(0),
      O => \tmp[0]_i_3__4_n_0\
    );
\tmp_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[0]_i_2__4_n_7\,
      Q => tmp_reg(0)
    );
\tmp_reg[0]_i_2__4\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tmp_reg[0]_i_2__4_n_0\,
      CO(2) => \tmp_reg[0]_i_2__4_n_1\,
      CO(1) => \tmp_reg[0]_i_2__4_n_2\,
      CO(0) => \tmp_reg[0]_i_2__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \tmp_reg[0]_i_2__4_n_4\,
      O(2) => \tmp_reg[0]_i_2__4_n_5\,
      O(1) => \tmp_reg[0]_i_2__4_n_6\,
      O(0) => \tmp_reg[0]_i_2__4_n_7\,
      S(3) => tmp_reg(3),
      S(2 downto 1) => \^s\(1 downto 0),
      S(0) => \tmp[0]_i_3__4_n_0\
    );
\tmp_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[8]_i_1__4_n_5\,
      Q => tmp_reg(10)
    );
\tmp_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[8]_i_1__4_n_4\,
      Q => tmp_reg(11)
    );
\tmp_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[12]_i_1__4_n_7\,
      Q => tmp_reg(12)
    );
\tmp_reg[12]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[8]_i_1__4_n_0\,
      CO(3) => \tmp_reg[12]_i_1__4_n_0\,
      CO(2) => \tmp_reg[12]_i_1__4_n_1\,
      CO(1) => \tmp_reg[12]_i_1__4_n_2\,
      CO(0) => \tmp_reg[12]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[12]_i_1__4_n_4\,
      O(2) => \tmp_reg[12]_i_1__4_n_5\,
      O(1) => \tmp_reg[12]_i_1__4_n_6\,
      O(0) => \tmp_reg[12]_i_1__4_n_7\,
      S(3 downto 0) => tmp_reg(15 downto 12)
    );
\tmp_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[12]_i_1__4_n_6\,
      Q => tmp_reg(13)
    );
\tmp_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[12]_i_1__4_n_5\,
      Q => tmp_reg(14)
    );
\tmp_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[12]_i_1__4_n_4\,
      Q => tmp_reg(15)
    );
\tmp_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[16]_i_1__1_n_7\,
      Q => tmp_reg(16)
    );
\tmp_reg[16]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[12]_i_1__4_n_0\,
      CO(3) => \tmp_reg[16]_i_1__1_n_0\,
      CO(2) => \tmp_reg[16]_i_1__1_n_1\,
      CO(1) => \tmp_reg[16]_i_1__1_n_2\,
      CO(0) => \tmp_reg[16]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[16]_i_1__1_n_4\,
      O(2) => \tmp_reg[16]_i_1__1_n_5\,
      O(1) => \tmp_reg[16]_i_1__1_n_6\,
      O(0) => \tmp_reg[16]_i_1__1_n_7\,
      S(3 downto 0) => tmp_reg(19 downto 16)
    );
\tmp_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[16]_i_1__1_n_6\,
      Q => tmp_reg(17)
    );
\tmp_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[16]_i_1__1_n_5\,
      Q => tmp_reg(18)
    );
\tmp_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[16]_i_1__1_n_4\,
      Q => tmp_reg(19)
    );
\tmp_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[0]_i_2__4_n_6\,
      Q => \^s\(0)
    );
\tmp_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[20]_i_1__0_n_7\,
      Q => tmp_reg(20)
    );
\tmp_reg[20]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[16]_i_1__1_n_0\,
      CO(3) => \tmp_reg[20]_i_1__0_n_0\,
      CO(2) => \tmp_reg[20]_i_1__0_n_1\,
      CO(1) => \tmp_reg[20]_i_1__0_n_2\,
      CO(0) => \tmp_reg[20]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[20]_i_1__0_n_4\,
      O(2) => \tmp_reg[20]_i_1__0_n_5\,
      O(1) => \tmp_reg[20]_i_1__0_n_6\,
      O(0) => \tmp_reg[20]_i_1__0_n_7\,
      S(3 downto 0) => tmp_reg(23 downto 20)
    );
\tmp_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[20]_i_1__0_n_6\,
      Q => tmp_reg(21)
    );
\tmp_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[20]_i_1__0_n_5\,
      Q => tmp_reg(22)
    );
\tmp_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[20]_i_1__0_n_4\,
      Q => tmp_reg(23)
    );
\tmp_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[24]_i_1__0_n_7\,
      Q => tmp_reg(24)
    );
\tmp_reg[24]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[20]_i_1__0_n_0\,
      CO(3) => \tmp_reg[24]_i_1__0_n_0\,
      CO(2) => \tmp_reg[24]_i_1__0_n_1\,
      CO(1) => \tmp_reg[24]_i_1__0_n_2\,
      CO(0) => \tmp_reg[24]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[24]_i_1__0_n_4\,
      O(2) => \tmp_reg[24]_i_1__0_n_5\,
      O(1) => \tmp_reg[24]_i_1__0_n_6\,
      O(0) => \tmp_reg[24]_i_1__0_n_7\,
      S(3 downto 0) => tmp_reg(27 downto 24)
    );
\tmp_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[24]_i_1__0_n_6\,
      Q => tmp_reg(25)
    );
\tmp_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[24]_i_1__0_n_5\,
      Q => tmp_reg(26)
    );
\tmp_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[24]_i_1__0_n_4\,
      Q => tmp_reg(27)
    );
\tmp_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[28]_i_1__0_n_7\,
      Q => tmp_reg(28)
    );
\tmp_reg[28]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[24]_i_1__0_n_0\,
      CO(3) => \NLW_tmp_reg[28]_i_1__0_CO_UNCONNECTED\(3),
      CO(2) => \tmp_reg[28]_i_1__0_n_1\,
      CO(1) => \tmp_reg[28]_i_1__0_n_2\,
      CO(0) => \tmp_reg[28]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[28]_i_1__0_n_4\,
      O(2) => \tmp_reg[28]_i_1__0_n_5\,
      O(1) => \tmp_reg[28]_i_1__0_n_6\,
      O(0) => \tmp_reg[28]_i_1__0_n_7\,
      S(3 downto 0) => tmp_reg(31 downto 28)
    );
\tmp_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[28]_i_1__0_n_6\,
      Q => tmp_reg(29)
    );
\tmp_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[0]_i_2__4_n_5\,
      Q => \^s\(1)
    );
\tmp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[28]_i_1__0_n_5\,
      Q => tmp_reg(30)
    );
\tmp_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[28]_i_1__0_n_4\,
      Q => tmp_reg(31)
    );
\tmp_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[0]_i_2__4_n_4\,
      Q => tmp_reg(3)
    );
\tmp_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[4]_i_1__4_n_7\,
      Q => tmp_reg(4)
    );
\tmp_reg[4]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[0]_i_2__4_n_0\,
      CO(3) => \tmp_reg[4]_i_1__4_n_0\,
      CO(2) => \tmp_reg[4]_i_1__4_n_1\,
      CO(1) => \tmp_reg[4]_i_1__4_n_2\,
      CO(0) => \tmp_reg[4]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[4]_i_1__4_n_4\,
      O(2) => \tmp_reg[4]_i_1__4_n_5\,
      O(1) => \tmp_reg[4]_i_1__4_n_6\,
      O(0) => \tmp_reg[4]_i_1__4_n_7\,
      S(3 downto 0) => tmp_reg(7 downto 4)
    );
\tmp_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[4]_i_1__4_n_6\,
      Q => tmp_reg(5)
    );
\tmp_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[4]_i_1__4_n_5\,
      Q => tmp_reg(6)
    );
\tmp_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[4]_i_1__4_n_4\,
      Q => tmp_reg(7)
    );
\tmp_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[8]_i_1__4_n_7\,
      Q => tmp_reg(8)
    );
\tmp_reg[8]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[4]_i_1__4_n_0\,
      CO(3) => \tmp_reg[8]_i_1__4_n_0\,
      CO(2) => \tmp_reg[8]_i_1__4_n_1\,
      CO(1) => \tmp_reg[8]_i_1__4_n_2\,
      CO(0) => \tmp_reg[8]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[8]_i_1__4_n_4\,
      O(2) => \tmp_reg[8]_i_1__4_n_5\,
      O(1) => \tmp_reg[8]_i_1__4_n_6\,
      O(0) => \tmp_reg[8]_i_1__4_n_7\,
      S(3 downto 0) => tmp_reg(11 downto 8)
    );
\tmp_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => clr1_out,
      D => \tmp_reg[8]_i_1__4_n_6\,
      Q => tmp_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_counter_2 is
  port (
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_araddr_reg[4]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \tmp_reg[31]_0\ : in STD_LOGIC;
    clk : in STD_LOGIC;
    \tmp_reg[31]_1\ : in STD_LOGIC;
    sel0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[2]\ : in STD_LOGIC;
    bram_seg : in STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata_reg[2]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_counter_2 : entity is "counter";
end design_1_ip_timers_0_0_counter_2;

architecture STRUCTURE of design_1_ip_timers_0_0_counter_2 is
  signal \^d\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_3__5_n_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__5_n_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__5_n_1\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__5_n_2\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__5_n_3\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__5_n_4\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__5_n_5\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__5_n_6\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__5_n_7\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__5_n_0\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__5_n_1\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__5_n_2\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__5_n_3\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__5_n_4\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__5_n_5\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__5_n_6\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__5_n_7\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__2_n_0\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__2_n_1\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__2_n_2\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__2_n_3\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__2_n_4\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__2_n_5\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__2_n_6\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__2_n_7\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__1_n_0\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__1_n_1\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__1_n_2\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__1_n_3\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__1_n_4\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__1_n_5\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__1_n_6\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1__1_n_7\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__1_n_0\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__1_n_1\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__1_n_2\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__1_n_3\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__1_n_4\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__1_n_5\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__1_n_6\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1__1_n_7\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__1_n_1\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__1_n_2\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__1_n_3\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__1_n_4\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__1_n_5\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__1_n_6\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1__1_n_7\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__5_n_0\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__5_n_1\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__5_n_2\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__5_n_3\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__5_n_4\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__5_n_5\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__5_n_6\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__5_n_7\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__5_n_0\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__5_n_1\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__5_n_2\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__5_n_3\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__5_n_4\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__5_n_5\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__5_n_6\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__5_n_7\ : STD_LOGIC;
  signal \NLW_tmp_reg[28]_i_1__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  D(31 downto 0) <= \^d\(31 downto 0);
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \^d\(2),
      I1 => bram_seg,
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => S(0),
      I5 => \axi_rdata_reg[2]_0\(0),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => \axi_rdata_reg[2]\,
      O => \axi_araddr_reg[4]\(0),
      S => sel0(2)
    );
\tmp[0]_i_3__5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^d\(0),
      O => \tmp[0]_i_3__5_n_0\
    );
\tmp_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[0]_i_2__5_n_7\,
      Q => \^d\(0)
    );
\tmp_reg[0]_i_2__5\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tmp_reg[0]_i_2__5_n_0\,
      CO(2) => \tmp_reg[0]_i_2__5_n_1\,
      CO(1) => \tmp_reg[0]_i_2__5_n_2\,
      CO(0) => \tmp_reg[0]_i_2__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \tmp_reg[0]_i_2__5_n_4\,
      O(2) => \tmp_reg[0]_i_2__5_n_5\,
      O(1) => \tmp_reg[0]_i_2__5_n_6\,
      O(0) => \tmp_reg[0]_i_2__5_n_7\,
      S(3 downto 1) => \^d\(3 downto 1),
      S(0) => \tmp[0]_i_3__5_n_0\
    );
\tmp_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[8]_i_1__5_n_5\,
      Q => \^d\(10)
    );
\tmp_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[8]_i_1__5_n_4\,
      Q => \^d\(11)
    );
\tmp_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[12]_i_1__5_n_7\,
      Q => \^d\(12)
    );
\tmp_reg[12]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[8]_i_1__5_n_0\,
      CO(3) => \tmp_reg[12]_i_1__5_n_0\,
      CO(2) => \tmp_reg[12]_i_1__5_n_1\,
      CO(1) => \tmp_reg[12]_i_1__5_n_2\,
      CO(0) => \tmp_reg[12]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[12]_i_1__5_n_4\,
      O(2) => \tmp_reg[12]_i_1__5_n_5\,
      O(1) => \tmp_reg[12]_i_1__5_n_6\,
      O(0) => \tmp_reg[12]_i_1__5_n_7\,
      S(3 downto 0) => \^d\(15 downto 12)
    );
\tmp_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[12]_i_1__5_n_6\,
      Q => \^d\(13)
    );
\tmp_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[12]_i_1__5_n_5\,
      Q => \^d\(14)
    );
\tmp_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[12]_i_1__5_n_4\,
      Q => \^d\(15)
    );
\tmp_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[16]_i_1__2_n_7\,
      Q => \^d\(16)
    );
\tmp_reg[16]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[12]_i_1__5_n_0\,
      CO(3) => \tmp_reg[16]_i_1__2_n_0\,
      CO(2) => \tmp_reg[16]_i_1__2_n_1\,
      CO(1) => \tmp_reg[16]_i_1__2_n_2\,
      CO(0) => \tmp_reg[16]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[16]_i_1__2_n_4\,
      O(2) => \tmp_reg[16]_i_1__2_n_5\,
      O(1) => \tmp_reg[16]_i_1__2_n_6\,
      O(0) => \tmp_reg[16]_i_1__2_n_7\,
      S(3 downto 0) => \^d\(19 downto 16)
    );
\tmp_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[16]_i_1__2_n_6\,
      Q => \^d\(17)
    );
\tmp_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[16]_i_1__2_n_5\,
      Q => \^d\(18)
    );
\tmp_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[16]_i_1__2_n_4\,
      Q => \^d\(19)
    );
\tmp_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[0]_i_2__5_n_6\,
      Q => \^d\(1)
    );
\tmp_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[20]_i_1__1_n_7\,
      Q => \^d\(20)
    );
\tmp_reg[20]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[16]_i_1__2_n_0\,
      CO(3) => \tmp_reg[20]_i_1__1_n_0\,
      CO(2) => \tmp_reg[20]_i_1__1_n_1\,
      CO(1) => \tmp_reg[20]_i_1__1_n_2\,
      CO(0) => \tmp_reg[20]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[20]_i_1__1_n_4\,
      O(2) => \tmp_reg[20]_i_1__1_n_5\,
      O(1) => \tmp_reg[20]_i_1__1_n_6\,
      O(0) => \tmp_reg[20]_i_1__1_n_7\,
      S(3 downto 0) => \^d\(23 downto 20)
    );
\tmp_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[20]_i_1__1_n_6\,
      Q => \^d\(21)
    );
\tmp_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[20]_i_1__1_n_5\,
      Q => \^d\(22)
    );
\tmp_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[20]_i_1__1_n_4\,
      Q => \^d\(23)
    );
\tmp_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[24]_i_1__1_n_7\,
      Q => \^d\(24)
    );
\tmp_reg[24]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[20]_i_1__1_n_0\,
      CO(3) => \tmp_reg[24]_i_1__1_n_0\,
      CO(2) => \tmp_reg[24]_i_1__1_n_1\,
      CO(1) => \tmp_reg[24]_i_1__1_n_2\,
      CO(0) => \tmp_reg[24]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[24]_i_1__1_n_4\,
      O(2) => \tmp_reg[24]_i_1__1_n_5\,
      O(1) => \tmp_reg[24]_i_1__1_n_6\,
      O(0) => \tmp_reg[24]_i_1__1_n_7\,
      S(3 downto 0) => \^d\(27 downto 24)
    );
\tmp_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[24]_i_1__1_n_6\,
      Q => \^d\(25)
    );
\tmp_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[24]_i_1__1_n_5\,
      Q => \^d\(26)
    );
\tmp_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[24]_i_1__1_n_4\,
      Q => \^d\(27)
    );
\tmp_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[28]_i_1__1_n_7\,
      Q => \^d\(28)
    );
\tmp_reg[28]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[24]_i_1__1_n_0\,
      CO(3) => \NLW_tmp_reg[28]_i_1__1_CO_UNCONNECTED\(3),
      CO(2) => \tmp_reg[28]_i_1__1_n_1\,
      CO(1) => \tmp_reg[28]_i_1__1_n_2\,
      CO(0) => \tmp_reg[28]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[28]_i_1__1_n_4\,
      O(2) => \tmp_reg[28]_i_1__1_n_5\,
      O(1) => \tmp_reg[28]_i_1__1_n_6\,
      O(0) => \tmp_reg[28]_i_1__1_n_7\,
      S(3 downto 0) => \^d\(31 downto 28)
    );
\tmp_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[28]_i_1__1_n_6\,
      Q => \^d\(29)
    );
\tmp_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[0]_i_2__5_n_5\,
      Q => \^d\(2)
    );
\tmp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[28]_i_1__1_n_5\,
      Q => \^d\(30)
    );
\tmp_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[28]_i_1__1_n_4\,
      Q => \^d\(31)
    );
\tmp_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[0]_i_2__5_n_4\,
      Q => \^d\(3)
    );
\tmp_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[4]_i_1__5_n_7\,
      Q => \^d\(4)
    );
\tmp_reg[4]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[0]_i_2__5_n_0\,
      CO(3) => \tmp_reg[4]_i_1__5_n_0\,
      CO(2) => \tmp_reg[4]_i_1__5_n_1\,
      CO(1) => \tmp_reg[4]_i_1__5_n_2\,
      CO(0) => \tmp_reg[4]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[4]_i_1__5_n_4\,
      O(2) => \tmp_reg[4]_i_1__5_n_5\,
      O(1) => \tmp_reg[4]_i_1__5_n_6\,
      O(0) => \tmp_reg[4]_i_1__5_n_7\,
      S(3 downto 0) => \^d\(7 downto 4)
    );
\tmp_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[4]_i_1__5_n_6\,
      Q => \^d\(5)
    );
\tmp_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[4]_i_1__5_n_5\,
      Q => \^d\(6)
    );
\tmp_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[4]_i_1__5_n_4\,
      Q => \^d\(7)
    );
\tmp_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[8]_i_1__5_n_7\,
      Q => \^d\(8)
    );
\tmp_reg[8]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[4]_i_1__5_n_0\,
      CO(3) => \tmp_reg[8]_i_1__5_n_0\,
      CO(2) => \tmp_reg[8]_i_1__5_n_1\,
      CO(1) => \tmp_reg[8]_i_1__5_n_2\,
      CO(0) => \tmp_reg[8]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[8]_i_1__5_n_4\,
      O(2) => \tmp_reg[8]_i_1__5_n_5\,
      O(1) => \tmp_reg[8]_i_1__5_n_6\,
      O(0) => \tmp_reg[8]_i_1__5_n_7\,
      S(3 downto 0) => \^d\(11 downto 8)
    );
\tmp_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => \tmp_reg[31]_0\,
      CLR => \tmp_reg[31]_1\,
      D => \tmp_reg[8]_i_1__5_n_6\,
      Q => \^d\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_counter_3 is
  port (
    \tmp_reg[0]_0\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \tmp_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[11]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[15]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[19]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[23]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[27]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[31]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[14]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[22]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[23]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tmp_reg[30]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[31]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_araddr_reg[4]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    ce : in STD_LOGIC;
    clk : in STD_LOGIC;
    clr0_out : in STD_LOGIC;
    \slv_reg7_reg[0]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    sel0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[1]\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata_reg[1]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_counter_3 : entity is "counter";
end design_1_ip_timers_0_0_counter_3;

architecture STRUCTURE of design_1_ip_timers_0_0_counter_3 is
  signal \^s\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \tmp[0]_i_3__3_n_0\ : STD_LOGIC;
  signal \^tmp_reg[0]_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__3_n_0\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__3_n_1\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__3_n_2\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__3_n_3\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__3_n_4\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__3_n_5\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__3_n_6\ : STD_LOGIC;
  signal \tmp_reg[0]_i_2__3_n_7\ : STD_LOGIC;
  signal \^tmp_reg[11]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tmp_reg[12]_i_1__3_n_0\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__3_n_1\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__3_n_2\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__3_n_3\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__3_n_4\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__3_n_5\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__3_n_6\ : STD_LOGIC;
  signal \tmp_reg[12]_i_1__3_n_7\ : STD_LOGIC;
  signal \^tmp_reg[15]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tmp_reg[16]_i_1__0_n_0\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__0_n_1\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__0_n_4\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \tmp_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \^tmp_reg[19]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tmp_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \^tmp_reg[23]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tmp_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \^tmp_reg[27]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tmp_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \^tmp_reg[31]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tmp_reg[4]_i_1__3_n_0\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__3_n_1\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__3_n_2\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__3_n_3\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__3_n_4\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__3_n_5\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__3_n_6\ : STD_LOGIC;
  signal \tmp_reg[4]_i_1__3_n_7\ : STD_LOGIC;
  signal \^tmp_reg[7]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tmp_reg[8]_i_1__3_n_0\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__3_n_1\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__3_n_2\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__3_n_3\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__3_n_4\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__3_n_5\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__3_n_6\ : STD_LOGIC;
  signal \tmp_reg[8]_i_1__3_n_7\ : STD_LOGIC;
  signal \NLW_tmp_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  S(2 downto 0) <= \^s\(2 downto 0);
  \tmp_reg[0]_0\ <= \^tmp_reg[0]_0\;
  \tmp_reg[11]_0\(3 downto 0) <= \^tmp_reg[11]_0\(3 downto 0);
  \tmp_reg[15]_0\(3 downto 0) <= \^tmp_reg[15]_0\(3 downto 0);
  \tmp_reg[19]_0\(3 downto 0) <= \^tmp_reg[19]_0\(3 downto 0);
  \tmp_reg[23]_0\(3 downto 0) <= \^tmp_reg[23]_0\(3 downto 0);
  \tmp_reg[27]_0\(3 downto 0) <= \^tmp_reg[27]_0\(3 downto 0);
  \tmp_reg[31]_0\(3 downto 0) <= \^tmp_reg[31]_0\(3 downto 0);
  \tmp_reg[7]_0\(3 downto 0) <= \^tmp_reg[7]_0\(3 downto 0);
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => D(0),
      I1 => CO(0),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => \axi_rdata_reg[1]_0\(0),
      I5 => \^s\(0),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata_reg[1]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => \axi_rdata_reg[1]\,
      O => \axi_araddr_reg[4]\(0),
      S => sel0(2)
    );
\cmp0_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[15]_0\(2),
      I1 => \slv_reg7_reg[0]\(14),
      I2 => \slv_reg7_reg[0]\(15),
      I3 => \^tmp_reg[15]_0\(3),
      O => \tmp_reg[14]_0\(3)
    );
\cmp0_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[15]_0\(0),
      I1 => \slv_reg7_reg[0]\(12),
      I2 => \slv_reg7_reg[0]\(13),
      I3 => \^tmp_reg[15]_0\(1),
      O => \tmp_reg[14]_0\(2)
    );
\cmp0_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[11]_0\(2),
      I1 => \slv_reg7_reg[0]\(10),
      I2 => \slv_reg7_reg[0]\(11),
      I3 => \^tmp_reg[11]_0\(3),
      O => \tmp_reg[14]_0\(1)
    );
\cmp0_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[11]_0\(0),
      I1 => \slv_reg7_reg[0]\(8),
      I2 => \slv_reg7_reg[0]\(9),
      I3 => \^tmp_reg[11]_0\(1),
      O => \tmp_reg[14]_0\(0)
    );
\cmp0_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(15),
      I1 => \^tmp_reg[15]_0\(3),
      I2 => \slv_reg7_reg[0]\(14),
      I3 => \^tmp_reg[15]_0\(2),
      O => \slv_reg4_reg[15]\(3)
    );
\cmp0_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(13),
      I1 => \^tmp_reg[15]_0\(1),
      I2 => \slv_reg7_reg[0]\(12),
      I3 => \^tmp_reg[15]_0\(0),
      O => \slv_reg4_reg[15]\(2)
    );
\cmp0_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(11),
      I1 => \^tmp_reg[11]_0\(3),
      I2 => \slv_reg7_reg[0]\(10),
      I3 => \^tmp_reg[11]_0\(2),
      O => \slv_reg4_reg[15]\(1)
    );
\cmp0_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(9),
      I1 => \^tmp_reg[11]_0\(1),
      I2 => \slv_reg7_reg[0]\(8),
      I3 => \^tmp_reg[11]_0\(0),
      O => \slv_reg4_reg[15]\(0)
    );
\cmp0_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[23]_0\(2),
      I1 => \slv_reg7_reg[0]\(22),
      I2 => \slv_reg7_reg[0]\(23),
      I3 => \^tmp_reg[23]_0\(3),
      O => \tmp_reg[22]_0\(3)
    );
\cmp0_carry__1_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[23]_0\(0),
      I1 => \slv_reg7_reg[0]\(20),
      I2 => \slv_reg7_reg[0]\(21),
      I3 => \^tmp_reg[23]_0\(1),
      O => \tmp_reg[22]_0\(2)
    );
\cmp0_carry__1_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[19]_0\(2),
      I1 => \slv_reg7_reg[0]\(18),
      I2 => \slv_reg7_reg[0]\(19),
      I3 => \^tmp_reg[19]_0\(3),
      O => \tmp_reg[22]_0\(1)
    );
\cmp0_carry__1_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[19]_0\(0),
      I1 => \slv_reg7_reg[0]\(16),
      I2 => \slv_reg7_reg[0]\(17),
      I3 => \^tmp_reg[19]_0\(1),
      O => \tmp_reg[22]_0\(0)
    );
\cmp0_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(23),
      I1 => \^tmp_reg[23]_0\(3),
      I2 => \slv_reg7_reg[0]\(22),
      I3 => \^tmp_reg[23]_0\(2),
      O => \slv_reg4_reg[23]\(3)
    );
\cmp0_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(21),
      I1 => \^tmp_reg[23]_0\(1),
      I2 => \slv_reg7_reg[0]\(20),
      I3 => \^tmp_reg[23]_0\(0),
      O => \slv_reg4_reg[23]\(2)
    );
\cmp0_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(19),
      I1 => \^tmp_reg[19]_0\(3),
      I2 => \slv_reg7_reg[0]\(18),
      I3 => \^tmp_reg[19]_0\(2),
      O => \slv_reg4_reg[23]\(1)
    );
\cmp0_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(17),
      I1 => \^tmp_reg[19]_0\(1),
      I2 => \slv_reg7_reg[0]\(16),
      I3 => \^tmp_reg[19]_0\(0),
      O => \slv_reg4_reg[23]\(0)
    );
\cmp0_carry__2_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[31]_0\(2),
      I1 => \slv_reg7_reg[0]\(30),
      I2 => \slv_reg7_reg[0]\(31),
      I3 => \^tmp_reg[31]_0\(3),
      O => \tmp_reg[30]_0\(3)
    );
\cmp0_carry__2_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[31]_0\(0),
      I1 => \slv_reg7_reg[0]\(28),
      I2 => \slv_reg7_reg[0]\(29),
      I3 => \^tmp_reg[31]_0\(1),
      O => \tmp_reg[30]_0\(2)
    );
\cmp0_carry__2_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[27]_0\(2),
      I1 => \slv_reg7_reg[0]\(26),
      I2 => \slv_reg7_reg[0]\(27),
      I3 => \^tmp_reg[27]_0\(3),
      O => \tmp_reg[30]_0\(1)
    );
\cmp0_carry__2_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[27]_0\(0),
      I1 => \slv_reg7_reg[0]\(24),
      I2 => \slv_reg7_reg[0]\(25),
      I3 => \^tmp_reg[27]_0\(1),
      O => \tmp_reg[30]_0\(0)
    );
\cmp0_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(31),
      I1 => \^tmp_reg[31]_0\(3),
      I2 => \slv_reg7_reg[0]\(30),
      I3 => \^tmp_reg[31]_0\(2),
      O => \slv_reg4_reg[31]\(3)
    );
\cmp0_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(29),
      I1 => \^tmp_reg[31]_0\(1),
      I2 => \slv_reg7_reg[0]\(28),
      I3 => \^tmp_reg[31]_0\(0),
      O => \slv_reg4_reg[31]\(2)
    );
\cmp0_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(27),
      I1 => \^tmp_reg[27]_0\(3),
      I2 => \slv_reg7_reg[0]\(26),
      I3 => \^tmp_reg[27]_0\(2),
      O => \slv_reg4_reg[31]\(1)
    );
\cmp0_carry__2_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(25),
      I1 => \^tmp_reg[27]_0\(1),
      I2 => \slv_reg7_reg[0]\(24),
      I3 => \^tmp_reg[27]_0\(0),
      O => \slv_reg4_reg[31]\(0)
    );
cmp0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[7]_0\(2),
      I1 => \slv_reg7_reg[0]\(6),
      I2 => \slv_reg7_reg[0]\(7),
      I3 => \^tmp_reg[7]_0\(3),
      O => DI(3)
    );
cmp0_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[7]_0\(0),
      I1 => \slv_reg7_reg[0]\(4),
      I2 => \slv_reg7_reg[0]\(5),
      I3 => \^tmp_reg[7]_0\(1),
      O => DI(2)
    );
cmp0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^s\(1),
      I1 => \slv_reg7_reg[0]\(2),
      I2 => \slv_reg7_reg[0]\(3),
      I3 => \^s\(2),
      O => DI(1)
    );
cmp0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \^tmp_reg[0]_0\,
      I1 => \slv_reg7_reg[0]\(0),
      I2 => \slv_reg7_reg[0]\(1),
      I3 => \^s\(0),
      O => DI(0)
    );
cmp0_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(7),
      I1 => \^tmp_reg[7]_0\(3),
      I2 => \slv_reg7_reg[0]\(6),
      I3 => \^tmp_reg[7]_0\(2),
      O => \slv_reg4_reg[7]\(3)
    );
cmp0_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(5),
      I1 => \^tmp_reg[7]_0\(1),
      I2 => \slv_reg7_reg[0]\(4),
      I3 => \^tmp_reg[7]_0\(0),
      O => \slv_reg4_reg[7]\(2)
    );
cmp0_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(3),
      I1 => \^s\(2),
      I2 => \slv_reg7_reg[0]\(2),
      I3 => \^s\(1),
      O => \slv_reg4_reg[7]\(1)
    );
cmp0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \slv_reg7_reg[0]\(1),
      I1 => \^s\(0),
      I2 => \slv_reg7_reg[0]\(0),
      I3 => \^tmp_reg[0]_0\,
      O => \slv_reg4_reg[7]\(0)
    );
\tmp[0]_i_3__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tmp_reg[0]_0\,
      O => \tmp[0]_i_3__3_n_0\
    );
\tmp_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[0]_i_2__3_n_7\,
      Q => \^tmp_reg[0]_0\
    );
\tmp_reg[0]_i_2__3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tmp_reg[0]_i_2__3_n_0\,
      CO(2) => \tmp_reg[0]_i_2__3_n_1\,
      CO(1) => \tmp_reg[0]_i_2__3_n_2\,
      CO(0) => \tmp_reg[0]_i_2__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \tmp_reg[0]_i_2__3_n_4\,
      O(2) => \tmp_reg[0]_i_2__3_n_5\,
      O(1) => \tmp_reg[0]_i_2__3_n_6\,
      O(0) => \tmp_reg[0]_i_2__3_n_7\,
      S(3 downto 1) => \^s\(2 downto 0),
      S(0) => \tmp[0]_i_3__3_n_0\
    );
\tmp_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[8]_i_1__3_n_5\,
      Q => \^tmp_reg[11]_0\(2)
    );
\tmp_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[8]_i_1__3_n_4\,
      Q => \^tmp_reg[11]_0\(3)
    );
\tmp_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[12]_i_1__3_n_7\,
      Q => \^tmp_reg[15]_0\(0)
    );
\tmp_reg[12]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[8]_i_1__3_n_0\,
      CO(3) => \tmp_reg[12]_i_1__3_n_0\,
      CO(2) => \tmp_reg[12]_i_1__3_n_1\,
      CO(1) => \tmp_reg[12]_i_1__3_n_2\,
      CO(0) => \tmp_reg[12]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[12]_i_1__3_n_4\,
      O(2) => \tmp_reg[12]_i_1__3_n_5\,
      O(1) => \tmp_reg[12]_i_1__3_n_6\,
      O(0) => \tmp_reg[12]_i_1__3_n_7\,
      S(3 downto 0) => \^tmp_reg[15]_0\(3 downto 0)
    );
\tmp_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[12]_i_1__3_n_6\,
      Q => \^tmp_reg[15]_0\(1)
    );
\tmp_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[12]_i_1__3_n_5\,
      Q => \^tmp_reg[15]_0\(2)
    );
\tmp_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[12]_i_1__3_n_4\,
      Q => \^tmp_reg[15]_0\(3)
    );
\tmp_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[16]_i_1__0_n_7\,
      Q => \^tmp_reg[19]_0\(0)
    );
\tmp_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[12]_i_1__3_n_0\,
      CO(3) => \tmp_reg[16]_i_1__0_n_0\,
      CO(2) => \tmp_reg[16]_i_1__0_n_1\,
      CO(1) => \tmp_reg[16]_i_1__0_n_2\,
      CO(0) => \tmp_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[16]_i_1__0_n_4\,
      O(2) => \tmp_reg[16]_i_1__0_n_5\,
      O(1) => \tmp_reg[16]_i_1__0_n_6\,
      O(0) => \tmp_reg[16]_i_1__0_n_7\,
      S(3 downto 0) => \^tmp_reg[19]_0\(3 downto 0)
    );
\tmp_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[16]_i_1__0_n_6\,
      Q => \^tmp_reg[19]_0\(1)
    );
\tmp_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[16]_i_1__0_n_5\,
      Q => \^tmp_reg[19]_0\(2)
    );
\tmp_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[16]_i_1__0_n_4\,
      Q => \^tmp_reg[19]_0\(3)
    );
\tmp_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[0]_i_2__3_n_6\,
      Q => \^s\(0)
    );
\tmp_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[20]_i_1_n_7\,
      Q => \^tmp_reg[23]_0\(0)
    );
\tmp_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[16]_i_1__0_n_0\,
      CO(3) => \tmp_reg[20]_i_1_n_0\,
      CO(2) => \tmp_reg[20]_i_1_n_1\,
      CO(1) => \tmp_reg[20]_i_1_n_2\,
      CO(0) => \tmp_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[20]_i_1_n_4\,
      O(2) => \tmp_reg[20]_i_1_n_5\,
      O(1) => \tmp_reg[20]_i_1_n_6\,
      O(0) => \tmp_reg[20]_i_1_n_7\,
      S(3 downto 0) => \^tmp_reg[23]_0\(3 downto 0)
    );
\tmp_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[20]_i_1_n_6\,
      Q => \^tmp_reg[23]_0\(1)
    );
\tmp_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[20]_i_1_n_5\,
      Q => \^tmp_reg[23]_0\(2)
    );
\tmp_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[20]_i_1_n_4\,
      Q => \^tmp_reg[23]_0\(3)
    );
\tmp_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[24]_i_1_n_7\,
      Q => \^tmp_reg[27]_0\(0)
    );
\tmp_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[20]_i_1_n_0\,
      CO(3) => \tmp_reg[24]_i_1_n_0\,
      CO(2) => \tmp_reg[24]_i_1_n_1\,
      CO(1) => \tmp_reg[24]_i_1_n_2\,
      CO(0) => \tmp_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[24]_i_1_n_4\,
      O(2) => \tmp_reg[24]_i_1_n_5\,
      O(1) => \tmp_reg[24]_i_1_n_6\,
      O(0) => \tmp_reg[24]_i_1_n_7\,
      S(3 downto 0) => \^tmp_reg[27]_0\(3 downto 0)
    );
\tmp_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[24]_i_1_n_6\,
      Q => \^tmp_reg[27]_0\(1)
    );
\tmp_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[24]_i_1_n_5\,
      Q => \^tmp_reg[27]_0\(2)
    );
\tmp_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[24]_i_1_n_4\,
      Q => \^tmp_reg[27]_0\(3)
    );
\tmp_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[28]_i_1_n_7\,
      Q => \^tmp_reg[31]_0\(0)
    );
\tmp_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[24]_i_1_n_0\,
      CO(3) => \NLW_tmp_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \tmp_reg[28]_i_1_n_1\,
      CO(1) => \tmp_reg[28]_i_1_n_2\,
      CO(0) => \tmp_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[28]_i_1_n_4\,
      O(2) => \tmp_reg[28]_i_1_n_5\,
      O(1) => \tmp_reg[28]_i_1_n_6\,
      O(0) => \tmp_reg[28]_i_1_n_7\,
      S(3 downto 0) => \^tmp_reg[31]_0\(3 downto 0)
    );
\tmp_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[28]_i_1_n_6\,
      Q => \^tmp_reg[31]_0\(1)
    );
\tmp_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[0]_i_2__3_n_5\,
      Q => \^s\(1)
    );
\tmp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[28]_i_1_n_5\,
      Q => \^tmp_reg[31]_0\(2)
    );
\tmp_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[28]_i_1_n_4\,
      Q => \^tmp_reg[31]_0\(3)
    );
\tmp_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[0]_i_2__3_n_4\,
      Q => \^s\(2)
    );
\tmp_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[4]_i_1__3_n_7\,
      Q => \^tmp_reg[7]_0\(0)
    );
\tmp_reg[4]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[0]_i_2__3_n_0\,
      CO(3) => \tmp_reg[4]_i_1__3_n_0\,
      CO(2) => \tmp_reg[4]_i_1__3_n_1\,
      CO(1) => \tmp_reg[4]_i_1__3_n_2\,
      CO(0) => \tmp_reg[4]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[4]_i_1__3_n_4\,
      O(2) => \tmp_reg[4]_i_1__3_n_5\,
      O(1) => \tmp_reg[4]_i_1__3_n_6\,
      O(0) => \tmp_reg[4]_i_1__3_n_7\,
      S(3 downto 0) => \^tmp_reg[7]_0\(3 downto 0)
    );
\tmp_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[4]_i_1__3_n_6\,
      Q => \^tmp_reg[7]_0\(1)
    );
\tmp_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[4]_i_1__3_n_5\,
      Q => \^tmp_reg[7]_0\(2)
    );
\tmp_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[4]_i_1__3_n_4\,
      Q => \^tmp_reg[7]_0\(3)
    );
\tmp_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[8]_i_1__3_n_7\,
      Q => \^tmp_reg[11]_0\(0)
    );
\tmp_reg[8]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_reg[4]_i_1__3_n_0\,
      CO(3) => \tmp_reg[8]_i_1__3_n_0\,
      CO(2) => \tmp_reg[8]_i_1__3_n_1\,
      CO(1) => \tmp_reg[8]_i_1__3_n_2\,
      CO(0) => \tmp_reg[8]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_reg[8]_i_1__3_n_4\,
      O(2) => \tmp_reg[8]_i_1__3_n_5\,
      O(1) => \tmp_reg[8]_i_1__3_n_6\,
      O(0) => \tmp_reg[8]_i_1__3_n_7\,
      S(3 downto 0) => \^tmp_reg[11]_0\(3 downto 0)
    );
\tmp_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => ce,
      CLR => clr0_out,
      D => \tmp_reg[8]_i_1__3_n_6\,
      Q => \^tmp_reg[11]_0\(1)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_t_ff is
  port (
    bram_seg : out STD_LOGIC;
    en : in STD_LOGIC;
    temp_reg_0 : in STD_LOGIC;
    temp_reg_1 : in STD_LOGIC;
    auto_clr : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_t_ff : entity is "t_ff";
end design_1_ip_timers_0_0_t_ff;

architecture STRUCTURE of design_1_ip_timers_0_0_t_ff is
  signal \^bram_seg\ : STD_LOGIC;
  signal temp_i_1_n_0 : STD_LOGIC;
begin
  bram_seg <= \^bram_seg\;
temp_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0BF40000"
    )
        port map (
      I0 => temp_reg_0,
      I1 => temp_reg_1,
      I2 => auto_clr,
      I3 => \^bram_seg\,
      I4 => Q(0),
      O => temp_i_1_n_0
    );
temp_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => en,
      CE => '1',
      D => temp_i_1_n_0,
      Q => \^bram_seg\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_fsm_auto_mode is
  port (
    bram_seg : out STD_LOGIC;
    lt_dir_0_sp_1 : out STD_LOGIC;
    \slv_reg5_reg[7]\ : out STD_LOGIC;
    \FSM_onehot_state_reg[1]_0\ : out STD_LOGIC;
    \slv_reg5_reg[6]\ : out STD_LOGIC;
    \slv_reg5_reg[5]\ : out STD_LOGIC;
    \slv_reg5_reg[3]\ : out STD_LOGIC;
    clr0_out : out STD_LOGIC;
    \slv_reg5_reg[9]\ : out STD_LOGIC;
    clr1_out : out STD_LOGIC;
    lt_dir : in STD_LOGIC_VECTOR ( 1 downto 0 );
    geqOp : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \tmp_reg[16]\ : in STD_LOGIC;
    eqOp : in STD_LOGIC;
    eqOp_0 : in STD_LOGIC;
    eqOp_1 : in STD_LOGIC;
    clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_fsm_auto_mode : entity is "fsm_auto_mode";
end design_1_ip_timers_0_0_fsm_auto_mode;

architecture STRUCTURE of design_1_ip_timers_0_0_fsm_auto_mode is
  signal \FSM_onehot_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_state_reg[1]_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[2]\ : STD_LOGIC;
  signal auto_clr : STD_LOGIC;
  signal \^clr0_out\ : STD_LOGIC;
  signal \^clr1_out\ : STD_LOGIC;
  signal en : STD_LOGIC;
  signal lt_dir_0_sn_1 : STD_LOGIC;
  signal \^slv_reg5_reg[9]\ : STD_LOGIC;
  signal \tmp[0]_i_4_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_state[2]_i_1\ : label is "soft_lutpair1";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "st1_idle:001,st2_clear:010,st3_count:100,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "st1_idle:001,st2_clear:010,st3_count:100,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "st1_idle:001,st2_clear:010,st3_count:100,";
  attribute SOFT_HLUTNM of clk_1kHz_en_i_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \clk_1kHz_en_i_i_2__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of temp_i_2 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of timers_enabled_INST_0 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \tmp[0]_i_1__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tmp[0]_i_1__2\ : label is "soft_lutpair3";
begin
  \FSM_onehot_state_reg[1]_0\ <= \^fsm_onehot_state_reg[1]_0\;
  clr0_out <= \^clr0_out\;
  clr1_out <= \^clr1_out\;
  lt_dir_0_sp_1 <= lt_dir_0_sn_1;
  \slv_reg5_reg[9]\ <= \^slv_reg5_reg[9]\;
\FSM_onehot_state[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(1),
      O => \FSM_onehot_state[0]_i_1_n_0\
    );
\FSM_onehot_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FAF80000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => CO(0),
      I2 => \FSM_onehot_state_reg_n_0_[0]\,
      I3 => auto_clr,
      I4 => Q(1),
      O => \FSM_onehot_state[1]_i_1_n_0\
    );
\FSM_onehot_state[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF020000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => CO(0),
      I2 => \FSM_onehot_state_reg_n_0_[0]\,
      I3 => auto_clr,
      I4 => Q(1),
      O => \FSM_onehot_state[2]_i_1_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[0]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[0]\,
      R => '0'
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[1]_i_1_n_0\,
      Q => auto_clr,
      R => '0'
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[2]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[2]\,
      R => '0'
    );
clk_1kHz_en_i_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => Q(10),
      I1 => auto_clr,
      O => \^clr1_out\
    );
\clk_1kHz_en_i_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => Q(9),
      I1 => auto_clr,
      O => \^slv_reg5_reg[9]\
    );
\clk_1kHz_en_i_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => Q(8),
      I1 => auto_clr,
      O => \^clr0_out\
    );
t_ff_inst: entity work.design_1_ip_timers_0_0_t_ff
     port map (
      Q(0) => Q(1),
      auto_clr => auto_clr,
      bram_seg => bram_seg,
      en => en,
      temp_reg_0 => \FSM_onehot_state_reg_n_0_[0]\,
      temp_reg_1 => \FSM_onehot_state_reg_n_0_[2]\
    );
temp_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => auto_clr,
      I1 => \FSM_onehot_state_reg_n_0_[2]\,
      I2 => \FSM_onehot_state_reg_n_0_[0]\,
      O => en
    );
timers_enabled_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00FF0004"
    )
        port map (
      I0 => auto_clr,
      I1 => \FSM_onehot_state_reg_n_0_[2]\,
      I2 => \FSM_onehot_state_reg_n_0_[0]\,
      I3 => CO(0),
      I4 => Q(0),
      O => \^fsm_onehot_state_reg[1]_0\
    );
\tmp[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFD0"
    )
        port map (
      I0 => lt_dir(0),
      I1 => lt_dir(1),
      I2 => \tmp[0]_i_4_n_0\,
      I3 => geqOp,
      O => lt_dir_0_sn_1
    );
\tmp[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => \^fsm_onehot_state_reg[1]_0\,
      I1 => Q(7),
      I2 => eqOp,
      O => \slv_reg5_reg[7]\
    );
\tmp[0]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => \^fsm_onehot_state_reg[1]_0\,
      I1 => Q(6),
      I2 => eqOp_0,
      O => \slv_reg5_reg[6]\
    );
\tmp[0]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => \^fsm_onehot_state_reg[1]_0\,
      I1 => Q(5),
      I2 => eqOp_1,
      O => \slv_reg5_reg[5]\
    );
\tmp[0]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFB510000FB51"
    )
        port map (
      I0 => Q(3),
      I1 => Q(2),
      I2 => \^clr0_out\,
      I3 => \^slv_reg5_reg[9]\,
      I4 => Q(4),
      I5 => \^clr1_out\,
      O => \slv_reg5_reg[3]\
    );
\tmp[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FF00F000880080"
    )
        port map (
      I0 => Q(4),
      I1 => Q(7),
      I2 => en,
      I3 => CO(0),
      I4 => Q(0),
      I5 => \tmp_reg[16]\,
      O => \tmp[0]_i_4_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_timers is
  port (
    tmp_reg : out STD_LOGIC_VECTOR ( 31 downto 0 );
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    bram_seg : out STD_LOGIC;
    \FSM_onehot_state_reg[1]\ : out STD_LOGIC;
    \axi_araddr_reg[4]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk : in STD_LOGIC;
    lt_dir : in STD_LOGIC_VECTOR ( 1 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    \axi_rdata_reg[0]\ : in STD_LOGIC;
    sel0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \slv_reg7_reg[0]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_rdata_reg[1]\ : in STD_LOGIC;
    \axi_rdata_reg[2]\ : in STD_LOGIC;
    \axi_rdata_reg[3]\ : in STD_LOGIC;
    \axi_rdata_reg[4]\ : in STD_LOGIC;
    \axi_rdata_reg[5]\ : in STD_LOGIC;
    \axi_rdata_reg[6]\ : in STD_LOGIC;
    \axi_rdata_reg[7]\ : in STD_LOGIC;
    \axi_rdata_reg[8]\ : in STD_LOGIC;
    \axi_rdata_reg[9]\ : in STD_LOGIC;
    \axi_rdata_reg[10]\ : in STD_LOGIC;
    \axi_rdata_reg[11]\ : in STD_LOGIC;
    \axi_rdata_reg[12]\ : in STD_LOGIC;
    \axi_rdata_reg[13]\ : in STD_LOGIC;
    \axi_rdata_reg[14]\ : in STD_LOGIC;
    \axi_rdata_reg[15]\ : in STD_LOGIC;
    \axi_rdata_reg[16]\ : in STD_LOGIC;
    \axi_rdata_reg[17]\ : in STD_LOGIC;
    \axi_rdata_reg[18]\ : in STD_LOGIC;
    \axi_rdata_reg[19]\ : in STD_LOGIC;
    \axi_rdata_reg[20]\ : in STD_LOGIC;
    \axi_rdata_reg[21]\ : in STD_LOGIC;
    \axi_rdata_reg[22]\ : in STD_LOGIC;
    \axi_rdata_reg[23]\ : in STD_LOGIC;
    \axi_rdata_reg[24]\ : in STD_LOGIC;
    \axi_rdata_reg[25]\ : in STD_LOGIC;
    \axi_rdata_reg[26]\ : in STD_LOGIC;
    \axi_rdata_reg[27]\ : in STD_LOGIC;
    \axi_rdata_reg[28]\ : in STD_LOGIC;
    \axi_rdata_reg[29]\ : in STD_LOGIC;
    \axi_rdata_reg[30]\ : in STD_LOGIC;
    \axi_rdata_reg[31]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_timers : entity is "timers";
end design_1_ip_timers_0_0_timers;

architecture STRUCTURE of design_1_ip_timers_0_0_timers is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^d\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^fsm_onehot_state_reg[1]\ : STD_LOGIC;
  signal \^bram_seg\ : STD_LOGIC;
  signal ce : STD_LOGIC;
  signal clk_1kHz_en : STD_LOGIC;
  signal clk_1kHz_enb : STD_LOGIC;
  signal clk_1kHz_enc : STD_LOGIC;
  signal clk_lt_enable_inst_n_1 : STD_LOGIC;
  signal clk_lt_enable_inst_n_2 : STD_LOGIC;
  signal clk_lt_enable_inst_n_3 : STD_LOGIC;
  signal clr0_out : STD_LOGIC;
  signal clr1_out : STD_LOGIC;
  signal counter_inst_C_n_32 : STD_LOGIC;
  signal counter_inst_C_n_33 : STD_LOGIC;
  signal counter_inst_C_n_34 : STD_LOGIC;
  signal counter_inst_C_n_35 : STD_LOGIC;
  signal counter_inst_C_n_36 : STD_LOGIC;
  signal counter_inst_C_n_37 : STD_LOGIC;
  signal counter_inst_C_n_38 : STD_LOGIC;
  signal counter_inst_C_n_39 : STD_LOGIC;
  signal counter_inst_C_n_40 : STD_LOGIC;
  signal counter_inst_C_n_41 : STD_LOGIC;
  signal counter_inst_C_n_42 : STD_LOGIC;
  signal counter_inst_C_n_43 : STD_LOGIC;
  signal counter_inst_C_n_44 : STD_LOGIC;
  signal counter_inst_C_n_45 : STD_LOGIC;
  signal counter_inst_C_n_46 : STD_LOGIC;
  signal counter_inst_C_n_47 : STD_LOGIC;
  signal counter_inst_C_n_48 : STD_LOGIC;
  signal counter_inst_C_n_49 : STD_LOGIC;
  signal counter_inst_C_n_50 : STD_LOGIC;
  signal counter_inst_C_n_51 : STD_LOGIC;
  signal counter_inst_C_n_52 : STD_LOGIC;
  signal counter_inst_C_n_53 : STD_LOGIC;
  signal counter_inst_C_n_54 : STD_LOGIC;
  signal counter_inst_C_n_55 : STD_LOGIC;
  signal counter_inst_C_n_56 : STD_LOGIC;
  signal counter_inst_C_n_57 : STD_LOGIC;
  signal counter_inst_C_n_58 : STD_LOGIC;
  signal counter_inst_C_n_59 : STD_LOGIC;
  signal counter_inst_C_n_60 : STD_LOGIC;
  signal counter_inst_C_n_61 : STD_LOGIC;
  signal counter_inst_C_n_62 : STD_LOGIC;
  signal counter_inst_C_n_63 : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal eqOp_0 : STD_LOGIC;
  signal eqOp_1 : STD_LOGIC;
  signal fsm_auto_mode_inst_n_1 : STD_LOGIC;
  signal fsm_auto_mode_inst_n_2 : STD_LOGIC;
  signal fsm_auto_mode_inst_n_4 : STD_LOGIC;
  signal fsm_auto_mode_inst_n_5 : STD_LOGIC;
  signal fsm_auto_mode_inst_n_6 : STD_LOGIC;
  signal fsm_auto_mode_inst_n_8 : STD_LOGIC;
  signal geqOp : STD_LOGIC;
  signal \^tmp_reg\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal tmp_reg_2 : STD_LOGIC_VECTOR ( 2 downto 1 );
begin
  CO(0) <= \^co\(0);
  D(31 downto 0) <= \^d\(31 downto 0);
  \FSM_onehot_state_reg[1]\ <= \^fsm_onehot_state_reg[1]\;
  bram_seg <= \^bram_seg\;
  tmp_reg(31 downto 0) <= \^tmp_reg\(31 downto 0);
clk_lt_enable_inst: entity work.design_1_ip_timers_0_0_clk_lt_enable
     port map (
      Q(5 downto 0) => Q(7 downto 2),
      ce => ce,
      clk => clk,
      clk_1kHz_en => clk_1kHz_en,
      clk_1kHz_enb => clk_1kHz_enb,
      clk_1kHz_enc => clk_1kHz_enc,
      geqOp => geqOp,
      lt_clk_en_i_reg_0 => clk_lt_enable_inst_n_2,
      lt_clk_en_i_reg_1 => clk_lt_enable_inst_n_3,
      lt_clk_en_i_reg_2 => \^fsm_onehot_state_reg[1]\,
      lt_dir(1 downto 0) => lt_dir(1 downto 0),
      \slv_reg5_reg[3]\ => clk_lt_enable_inst_n_1,
      \tmp_reg[16]_0\ => fsm_auto_mode_inst_n_1,
      \tmp_reg[16]_1\ => fsm_auto_mode_inst_n_6
    );
clock_1KHz_enable_inst_A: entity work.design_1_ip_timers_0_0_clock_1KHz_enable
     port map (
      clk => clk,
      clk_1kHz_en => clk_1kHz_en,
      clr1_out => clr1_out,
      eqOp => eqOp,
      \tmp_reg[15]_0\ => fsm_auto_mode_inst_n_2
    );
clock_1KHz_enable_inst_B: entity work.design_1_ip_timers_0_0_clock_1KHz_enable_0
     port map (
      clk => clk,
      clk_1kHz_enb => clk_1kHz_enb,
      eqOp => eqOp_0,
      \tmp_reg[0]_0\ => fsm_auto_mode_inst_n_8,
      \tmp_reg[15]_0\ => fsm_auto_mode_inst_n_4
    );
clock_1KHz_enable_inst_C: entity work.design_1_ip_timers_0_0_clock_1KHz_enable_1
     port map (
      clk => clk,
      clk_1kHz_enc => clk_1kHz_enc,
      clr0_out => clr0_out,
      eqOp => eqOp_1,
      \tmp_reg[15]_0\ => fsm_auto_mode_inst_n_5
    );
comparator_inst: entity work.design_1_ip_timers_0_0_comparator
     port map (
      CO(0) => \^co\(0),
      DI(3) => counter_inst_C_n_32,
      DI(2) => counter_inst_C_n_33,
      DI(1) => counter_inst_C_n_34,
      DI(0) => counter_inst_C_n_35,
      S(3) => counter_inst_C_n_36,
      S(2) => counter_inst_C_n_37,
      S(1) => counter_inst_C_n_38,
      S(0) => counter_inst_C_n_39,
      \cmp0_carry__1_0\(3) => counter_inst_C_n_40,
      \cmp0_carry__1_0\(2) => counter_inst_C_n_41,
      \cmp0_carry__1_0\(1) => counter_inst_C_n_42,
      \cmp0_carry__1_0\(0) => counter_inst_C_n_43,
      \cmp0_carry__1_1\(3) => counter_inst_C_n_44,
      \cmp0_carry__1_1\(2) => counter_inst_C_n_45,
      \cmp0_carry__1_1\(1) => counter_inst_C_n_46,
      \cmp0_carry__1_1\(0) => counter_inst_C_n_47,
      \slv_reg7_reg[0]\(3) => counter_inst_C_n_48,
      \slv_reg7_reg[0]\(2) => counter_inst_C_n_49,
      \slv_reg7_reg[0]\(1) => counter_inst_C_n_50,
      \slv_reg7_reg[0]\(0) => counter_inst_C_n_51,
      \slv_reg7_reg[0]_0\(3) => counter_inst_C_n_52,
      \slv_reg7_reg[0]_0\(2) => counter_inst_C_n_53,
      \slv_reg7_reg[0]_0\(1) => counter_inst_C_n_54,
      \slv_reg7_reg[0]_0\(0) => counter_inst_C_n_55,
      \slv_reg7_reg[0]_1\(3) => counter_inst_C_n_56,
      \slv_reg7_reg[0]_1\(2) => counter_inst_C_n_57,
      \slv_reg7_reg[0]_1\(1) => counter_inst_C_n_58,
      \slv_reg7_reg[0]_1\(0) => counter_inst_C_n_59,
      \slv_reg7_reg[0]_2\(3) => counter_inst_C_n_60,
      \slv_reg7_reg[0]_2\(2) => counter_inst_C_n_61,
      \slv_reg7_reg[0]_2\(1) => counter_inst_C_n_62,
      \slv_reg7_reg[0]_2\(0) => counter_inst_C_n_63
    );
counter_inst_A: entity work.design_1_ip_timers_0_0_counter
     port map (
      D(29 downto 1) => \^d\(31 downto 3),
      D(0) => \^d\(0),
      S(1 downto 0) => tmp_reg_2(2 downto 1),
      \axi_araddr_reg[4]\(29 downto 1) => \axi_araddr_reg[4]\(31 downto 3),
      \axi_araddr_reg[4]\(0) => \axi_araddr_reg[4]\(0),
      \axi_rdata_reg[0]\ => \axi_rdata_reg[0]\,
      \axi_rdata_reg[0]_0\ => \^fsm_onehot_state_reg[1]\,
      \axi_rdata_reg[0]_1\ => \^tmp_reg\(0),
      \axi_rdata_reg[10]\ => \axi_rdata_reg[10]\,
      \axi_rdata_reg[11]\(3 downto 0) => \^tmp_reg\(11 downto 8),
      \axi_rdata_reg[11]_0\ => \axi_rdata_reg[11]\,
      \axi_rdata_reg[12]\ => \axi_rdata_reg[12]\,
      \axi_rdata_reg[13]\ => \axi_rdata_reg[13]\,
      \axi_rdata_reg[14]\ => \axi_rdata_reg[14]\,
      \axi_rdata_reg[15]\(3 downto 0) => \^tmp_reg\(15 downto 12),
      \axi_rdata_reg[15]_0\ => \axi_rdata_reg[15]\,
      \axi_rdata_reg[16]\ => \axi_rdata_reg[16]\,
      \axi_rdata_reg[17]\ => \axi_rdata_reg[17]\,
      \axi_rdata_reg[18]\ => \axi_rdata_reg[18]\,
      \axi_rdata_reg[19]\(3 downto 0) => \^tmp_reg\(19 downto 16),
      \axi_rdata_reg[19]_0\ => \axi_rdata_reg[19]\,
      \axi_rdata_reg[20]\ => \axi_rdata_reg[20]\,
      \axi_rdata_reg[21]\ => \axi_rdata_reg[21]\,
      \axi_rdata_reg[22]\ => \axi_rdata_reg[22]\,
      \axi_rdata_reg[23]\(3 downto 0) => \^tmp_reg\(23 downto 20),
      \axi_rdata_reg[23]_0\ => \axi_rdata_reg[23]\,
      \axi_rdata_reg[24]\ => \axi_rdata_reg[24]\,
      \axi_rdata_reg[25]\ => \axi_rdata_reg[25]\,
      \axi_rdata_reg[26]\ => \axi_rdata_reg[26]\,
      \axi_rdata_reg[27]\(3 downto 0) => \^tmp_reg\(27 downto 24),
      \axi_rdata_reg[27]_0\ => \axi_rdata_reg[27]\,
      \axi_rdata_reg[28]\ => \axi_rdata_reg[28]\,
      \axi_rdata_reg[29]\ => \axi_rdata_reg[29]\,
      \axi_rdata_reg[30]\ => \axi_rdata_reg[30]\,
      \axi_rdata_reg[31]\(3 downto 0) => \^tmp_reg\(31 downto 28),
      \axi_rdata_reg[31]_0\ => \axi_rdata_reg[31]\,
      \axi_rdata_reg[3]\ => \axi_rdata_reg[3]\,
      \axi_rdata_reg[3]_0\(0) => \^tmp_reg\(3),
      \axi_rdata_reg[4]\ => \axi_rdata_reg[4]\,
      \axi_rdata_reg[5]\ => \axi_rdata_reg[5]\,
      \axi_rdata_reg[6]\ => \axi_rdata_reg[6]\,
      \axi_rdata_reg[7]\(3 downto 0) => \^tmp_reg\(7 downto 4),
      \axi_rdata_reg[7]_0\ => \axi_rdata_reg[7]\,
      \axi_rdata_reg[8]\ => \axi_rdata_reg[8]\,
      \axi_rdata_reg[9]\ => \axi_rdata_reg[9]\,
      clk => clk,
      clr1_out => clr1_out,
      sel0(2 downto 0) => sel0(2 downto 0),
      \tmp_reg[31]_0\ => clk_lt_enable_inst_n_2
    );
counter_inst_B: entity work.design_1_ip_timers_0_0_counter_2
     port map (
      D(31 downto 0) => \^d\(31 downto 0),
      S(0) => tmp_reg_2(2),
      \axi_araddr_reg[4]\(0) => \axi_araddr_reg[4]\(2),
      \axi_rdata_reg[2]\ => \axi_rdata_reg[2]\,
      \axi_rdata_reg[2]_0\(0) => \^tmp_reg\(2),
      bram_seg => \^bram_seg\,
      clk => clk,
      sel0(2 downto 0) => sel0(2 downto 0),
      \tmp_reg[31]_0\ => clk_lt_enable_inst_n_3,
      \tmp_reg[31]_1\ => fsm_auto_mode_inst_n_8
    );
counter_inst_C: entity work.design_1_ip_timers_0_0_counter_3
     port map (
      CO(0) => \^co\(0),
      D(0) => \^d\(1),
      DI(3) => counter_inst_C_n_32,
      DI(2) => counter_inst_C_n_33,
      DI(1) => counter_inst_C_n_34,
      DI(0) => counter_inst_C_n_35,
      S(2 downto 0) => \^tmp_reg\(3 downto 1),
      \axi_araddr_reg[4]\(0) => \axi_araddr_reg[4]\(1),
      \axi_rdata_reg[1]\ => \axi_rdata_reg[1]\,
      \axi_rdata_reg[1]_0\(0) => tmp_reg_2(1),
      ce => ce,
      clk => clk,
      clr0_out => clr0_out,
      sel0(2 downto 0) => sel0(2 downto 0),
      \slv_reg4_reg[15]\(3) => counter_inst_C_n_44,
      \slv_reg4_reg[15]\(2) => counter_inst_C_n_45,
      \slv_reg4_reg[15]\(1) => counter_inst_C_n_46,
      \slv_reg4_reg[15]\(0) => counter_inst_C_n_47,
      \slv_reg4_reg[23]\(3) => counter_inst_C_n_52,
      \slv_reg4_reg[23]\(2) => counter_inst_C_n_53,
      \slv_reg4_reg[23]\(1) => counter_inst_C_n_54,
      \slv_reg4_reg[23]\(0) => counter_inst_C_n_55,
      \slv_reg4_reg[31]\(3) => counter_inst_C_n_60,
      \slv_reg4_reg[31]\(2) => counter_inst_C_n_61,
      \slv_reg4_reg[31]\(1) => counter_inst_C_n_62,
      \slv_reg4_reg[31]\(0) => counter_inst_C_n_63,
      \slv_reg4_reg[7]\(3) => counter_inst_C_n_36,
      \slv_reg4_reg[7]\(2) => counter_inst_C_n_37,
      \slv_reg4_reg[7]\(1) => counter_inst_C_n_38,
      \slv_reg4_reg[7]\(0) => counter_inst_C_n_39,
      \slv_reg7_reg[0]\(31 downto 0) => \slv_reg7_reg[0]\(31 downto 0),
      \tmp_reg[0]_0\ => \^tmp_reg\(0),
      \tmp_reg[11]_0\(3 downto 0) => \^tmp_reg\(11 downto 8),
      \tmp_reg[14]_0\(3) => counter_inst_C_n_40,
      \tmp_reg[14]_0\(2) => counter_inst_C_n_41,
      \tmp_reg[14]_0\(1) => counter_inst_C_n_42,
      \tmp_reg[14]_0\(0) => counter_inst_C_n_43,
      \tmp_reg[15]_0\(3 downto 0) => \^tmp_reg\(15 downto 12),
      \tmp_reg[19]_0\(3 downto 0) => \^tmp_reg\(19 downto 16),
      \tmp_reg[22]_0\(3) => counter_inst_C_n_48,
      \tmp_reg[22]_0\(2) => counter_inst_C_n_49,
      \tmp_reg[22]_0\(1) => counter_inst_C_n_50,
      \tmp_reg[22]_0\(0) => counter_inst_C_n_51,
      \tmp_reg[23]_0\(3 downto 0) => \^tmp_reg\(23 downto 20),
      \tmp_reg[27]_0\(3 downto 0) => \^tmp_reg\(27 downto 24),
      \tmp_reg[30]_0\(3) => counter_inst_C_n_56,
      \tmp_reg[30]_0\(2) => counter_inst_C_n_57,
      \tmp_reg[30]_0\(1) => counter_inst_C_n_58,
      \tmp_reg[30]_0\(0) => counter_inst_C_n_59,
      \tmp_reg[31]_0\(3 downto 0) => \^tmp_reg\(31 downto 28),
      \tmp_reg[7]_0\(3 downto 0) => \^tmp_reg\(7 downto 4)
    );
fsm_auto_mode_inst: entity work.design_1_ip_timers_0_0_fsm_auto_mode
     port map (
      CO(0) => \^co\(0),
      \FSM_onehot_state_reg[1]_0\ => \^fsm_onehot_state_reg[1]\,
      Q(10 downto 0) => Q(10 downto 0),
      bram_seg => \^bram_seg\,
      clk => clk,
      clr0_out => clr0_out,
      clr1_out => clr1_out,
      eqOp => eqOp,
      eqOp_0 => eqOp_0,
      eqOp_1 => eqOp_1,
      geqOp => geqOp,
      lt_dir(1 downto 0) => lt_dir(1 downto 0),
      lt_dir_0_sp_1 => fsm_auto_mode_inst_n_1,
      \slv_reg5_reg[3]\ => fsm_auto_mode_inst_n_6,
      \slv_reg5_reg[5]\ => fsm_auto_mode_inst_n_5,
      \slv_reg5_reg[6]\ => fsm_auto_mode_inst_n_4,
      \slv_reg5_reg[7]\ => fsm_auto_mode_inst_n_2,
      \slv_reg5_reg[9]\ => fsm_auto_mode_inst_n_8,
      \tmp_reg[16]\ => clk_lt_enable_inst_n_1
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_ip_timers_v2_0_S00_AXI is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \FSM_onehot_state_reg[1]\ : out STD_LOGIC;
    bram_seg : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    clk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    lt_dir : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_ip_timers_v2_0_S00_AXI : entity is "ip_timers_v2_0_S00_AXI";
end design_1_ip_timers_0_0_ip_timers_v2_0_S00_AXI;

architecture STRUCTURE of design_1_ip_timers_0_0_ip_timers_v2_0_S00_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal a_mode : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal b_mode : STD_LOGIC;
  signal c_mode : STD_LOGIC;
  signal cea : STD_LOGIC;
  signal ceb : STD_LOGIC;
  signal cec : STD_LOGIC;
  signal clra : STD_LOGIC;
  signal clrb : STD_LOGIC;
  signal clrc : STD_LOGIC;
  signal \counter_inst_B/tmp_reg\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \counter_inst_C/tmp_reg\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal p_0_in_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slv_reg4 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg5[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg5_reg_n_0_[31]\ : STD_LOGIC;
  signal slv_reg6 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg7 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  signal tc : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair5";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBF00BF00BF00"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => s00_axi_awvalid,
      I2 => s00_axi_wvalid,
      I3 => aw_en_reg_n_0,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => p_0_in
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => sel0(0),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => sel0(1),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(2),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => sel0(2),
      O => \axi_araddr[4]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => sel0(0),
      S => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => sel0(1),
      S => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[4]_i_1_n_0\,
      Q => sel0(2),
      S => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => p_0_in
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in_0(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in_0(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(2),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in_0(2),
      O => \axi_awaddr[4]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_0_in_0(0),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_0_in_0(1),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[4]_i_1_n_0\,
      Q => p_0_in_0(2),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => p_0_in
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => p_0_in
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[0]\,
      I1 => slv_reg7(0),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(0),
      I5 => slv_reg6(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => clra,
      I1 => slv_reg7(10),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(10),
      I5 => slv_reg6(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[11]\,
      I1 => slv_reg7(11),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(11),
      I5 => slv_reg6(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[12]\,
      I1 => slv_reg7(12),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(12),
      I5 => slv_reg6(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[13]\,
      I1 => slv_reg7(13),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(13),
      I5 => slv_reg6(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[14]\,
      I1 => slv_reg7(14),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(14),
      I5 => slv_reg6(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[15]\,
      I1 => slv_reg7(15),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(15),
      I5 => slv_reg6(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[16]\,
      I1 => slv_reg7(16),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(16),
      I5 => slv_reg6(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[17]\,
      I1 => slv_reg7(17),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(17),
      I5 => slv_reg6(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[18]\,
      I1 => slv_reg7(18),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(18),
      I5 => slv_reg6(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[19]\,
      I1 => slv_reg7(19),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(19),
      I5 => slv_reg6(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[1]\,
      I1 => slv_reg7(1),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(1),
      I5 => slv_reg6(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[20]\,
      I1 => slv_reg7(20),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(20),
      I5 => slv_reg6(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[21]\,
      I1 => slv_reg7(21),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(21),
      I5 => slv_reg6(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[22]\,
      I1 => slv_reg7(22),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(22),
      I5 => slv_reg6(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[23]\,
      I1 => slv_reg7(23),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(23),
      I5 => slv_reg6(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[24]\,
      I1 => slv_reg7(24),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(24),
      I5 => slv_reg6(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[25]\,
      I1 => slv_reg7(25),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(25),
      I5 => slv_reg6(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[26]\,
      I1 => slv_reg7(26),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(26),
      I5 => slv_reg6(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[27]\,
      I1 => slv_reg7(27),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(27),
      I5 => slv_reg6(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[28]\,
      I1 => slv_reg7(28),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(28),
      I5 => slv_reg6(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[29]\,
      I1 => slv_reg7(29),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(29),
      I5 => slv_reg6(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => c_mode,
      I1 => slv_reg7(2),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(2),
      I5 => slv_reg6(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[30]\,
      I1 => slv_reg7(30),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(30),
      I5 => slv_reg6(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => \slv_reg5_reg_n_0_[31]\,
      I1 => slv_reg7(31),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(31),
      I5 => slv_reg6(31),
      O => \axi_rdata[31]_i_4_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => b_mode,
      I1 => slv_reg7(3),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(3),
      I5 => slv_reg6(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => a_mode,
      I1 => slv_reg7(4),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(4),
      I5 => slv_reg6(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => cec,
      I1 => slv_reg7(5),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(5),
      I5 => slv_reg6(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => ceb,
      I1 => slv_reg7(6),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(6),
      I5 => slv_reg6(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => cea,
      I1 => slv_reg7(7),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(7),
      I5 => slv_reg6(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => clrc,
      I1 => slv_reg7(8),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(8),
      I5 => slv_reg6(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => clrb,
      I1 => slv_reg7(9),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => slv_reg4(9),
      I5 => slv_reg6(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => p_0_in
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in_0(1),
      I2 => p_0_in_0(2),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in_0(0),
      O => p_1_in(15)
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in_0(1),
      I2 => p_0_in_0(2),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in_0(0),
      O => p_1_in(23)
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in_0(1),
      I2 => p_0_in_0(2),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in_0(0),
      O => p_1_in(31)
    );
\slv_reg4[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in_0(1),
      I2 => p_0_in_0(2),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in_0(0),
      O => p_1_in(7)
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => slv_reg4(0),
      R => p_0_in
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => slv_reg4(10),
      R => p_0_in
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => slv_reg4(11),
      R => p_0_in
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => slv_reg4(12),
      R => p_0_in
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => slv_reg4(13),
      R => p_0_in
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => slv_reg4(14),
      R => p_0_in
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => slv_reg4(15),
      R => p_0_in
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => slv_reg4(16),
      R => p_0_in
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => slv_reg4(17),
      R => p_0_in
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => slv_reg4(18),
      R => p_0_in
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => slv_reg4(19),
      R => p_0_in
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => slv_reg4(1),
      R => p_0_in
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => slv_reg4(20),
      R => p_0_in
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => slv_reg4(21),
      R => p_0_in
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => slv_reg4(22),
      R => p_0_in
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => slv_reg4(23),
      R => p_0_in
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => slv_reg4(24),
      R => p_0_in
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => slv_reg4(25),
      R => p_0_in
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => slv_reg4(26),
      R => p_0_in
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => slv_reg4(27),
      R => p_0_in
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => slv_reg4(28),
      R => p_0_in
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => slv_reg4(29),
      R => p_0_in
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => slv_reg4(2),
      R => p_0_in
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => slv_reg4(30),
      R => p_0_in
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => slv_reg4(31),
      R => p_0_in
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => slv_reg4(3),
      R => p_0_in
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => slv_reg4(4),
      R => p_0_in
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => slv_reg4(5),
      R => p_0_in
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => slv_reg4(6),
      R => p_0_in
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => slv_reg4(7),
      R => p_0_in
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => slv_reg4(8),
      R => p_0_in
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => slv_reg4(9),
      R => p_0_in
    );
\slv_reg5[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in_0(1),
      I2 => p_0_in_0(2),
      I3 => p_0_in_0(0),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg5[15]_i_1_n_0\
    );
\slv_reg5[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in_0(1),
      I2 => p_0_in_0(2),
      I3 => p_0_in_0(0),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg5[23]_i_1_n_0\
    );
\slv_reg5[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in_0(1),
      I2 => p_0_in_0(2),
      I3 => p_0_in_0(0),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg5[31]_i_1_n_0\
    );
\slv_reg5[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in_0(1),
      I2 => p_0_in_0(2),
      I3 => p_0_in_0(0),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg5[7]_i_1_n_0\
    );
\slv_reg5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \slv_reg5_reg_n_0_[0]\,
      R => p_0_in
    );
\slv_reg5_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => clra,
      R => p_0_in
    );
\slv_reg5_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg5_reg_n_0_[11]\,
      R => p_0_in
    );
\slv_reg5_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg5_reg_n_0_[12]\,
      R => p_0_in
    );
\slv_reg5_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg5_reg_n_0_[13]\,
      R => p_0_in
    );
\slv_reg5_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg5_reg_n_0_[14]\,
      R => p_0_in
    );
\slv_reg5_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg5_reg_n_0_[15]\,
      R => p_0_in
    );
\slv_reg5_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg5_reg_n_0_[16]\,
      R => p_0_in
    );
\slv_reg5_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg5_reg_n_0_[17]\,
      R => p_0_in
    );
\slv_reg5_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg5_reg_n_0_[18]\,
      R => p_0_in
    );
\slv_reg5_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg5_reg_n_0_[19]\,
      R => p_0_in
    );
\slv_reg5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \slv_reg5_reg_n_0_[1]\,
      R => p_0_in
    );
\slv_reg5_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg5_reg_n_0_[20]\,
      R => p_0_in
    );
\slv_reg5_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg5_reg_n_0_[21]\,
      R => p_0_in
    );
\slv_reg5_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg5_reg_n_0_[22]\,
      R => p_0_in
    );
\slv_reg5_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg5_reg_n_0_[23]\,
      R => p_0_in
    );
\slv_reg5_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg5_reg_n_0_[24]\,
      R => p_0_in
    );
\slv_reg5_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg5_reg_n_0_[25]\,
      R => p_0_in
    );
\slv_reg5_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg5_reg_n_0_[26]\,
      R => p_0_in
    );
\slv_reg5_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg5_reg_n_0_[27]\,
      R => p_0_in
    );
\slv_reg5_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg5_reg_n_0_[28]\,
      R => p_0_in
    );
\slv_reg5_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg5_reg_n_0_[29]\,
      R => p_0_in
    );
\slv_reg5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => c_mode,
      R => p_0_in
    );
\slv_reg5_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg5_reg_n_0_[30]\,
      R => p_0_in
    );
\slv_reg5_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg5_reg_n_0_[31]\,
      R => p_0_in
    );
\slv_reg5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => b_mode,
      R => p_0_in
    );
\slv_reg5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => a_mode,
      R => p_0_in
    );
\slv_reg5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => cec,
      R => p_0_in
    );
\slv_reg5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => ceb,
      R => p_0_in
    );
\slv_reg5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => cea,
      R => p_0_in
    );
\slv_reg5_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => clrc,
      R => p_0_in
    );
\slv_reg5_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => clrb,
      R => p_0_in
    );
\slv_reg6_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(0),
      Q => slv_reg6(0)
    );
\slv_reg6_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(10),
      Q => slv_reg6(10)
    );
\slv_reg6_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(11),
      Q => slv_reg6(11)
    );
\slv_reg6_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(12),
      Q => slv_reg6(12)
    );
\slv_reg6_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(13),
      Q => slv_reg6(13)
    );
\slv_reg6_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(14),
      Q => slv_reg6(14)
    );
\slv_reg6_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(15),
      Q => slv_reg6(15)
    );
\slv_reg6_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(16),
      Q => slv_reg6(16)
    );
\slv_reg6_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(17),
      Q => slv_reg6(17)
    );
\slv_reg6_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(18),
      Q => slv_reg6(18)
    );
\slv_reg6_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(19),
      Q => slv_reg6(19)
    );
\slv_reg6_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(1),
      Q => slv_reg6(1)
    );
\slv_reg6_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(20),
      Q => slv_reg6(20)
    );
\slv_reg6_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(21),
      Q => slv_reg6(21)
    );
\slv_reg6_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(22),
      Q => slv_reg6(22)
    );
\slv_reg6_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(23),
      Q => slv_reg6(23)
    );
\slv_reg6_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(24),
      Q => slv_reg6(24)
    );
\slv_reg6_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(25),
      Q => slv_reg6(25)
    );
\slv_reg6_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(26),
      Q => slv_reg6(26)
    );
\slv_reg6_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(27),
      Q => slv_reg6(27)
    );
\slv_reg6_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(28),
      Q => slv_reg6(28)
    );
\slv_reg6_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(29),
      Q => slv_reg6(29)
    );
\slv_reg6_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(2),
      Q => slv_reg6(2)
    );
\slv_reg6_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(30),
      Q => slv_reg6(30)
    );
\slv_reg6_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(31),
      Q => slv_reg6(31)
    );
\slv_reg6_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(3),
      Q => slv_reg6(3)
    );
\slv_reg6_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(4),
      Q => slv_reg6(4)
    );
\slv_reg6_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(5),
      Q => slv_reg6(5)
    );
\slv_reg6_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(6),
      Q => slv_reg6(6)
    );
\slv_reg6_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(7),
      Q => slv_reg6(7)
    );
\slv_reg6_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(8),
      Q => slv_reg6(8)
    );
\slv_reg6_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_B/tmp_reg\(9),
      Q => slv_reg6(9)
    );
\slv_reg7_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(0),
      Q => slv_reg7(0)
    );
\slv_reg7_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(10),
      Q => slv_reg7(10)
    );
\slv_reg7_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(11),
      Q => slv_reg7(11)
    );
\slv_reg7_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(12),
      Q => slv_reg7(12)
    );
\slv_reg7_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(13),
      Q => slv_reg7(13)
    );
\slv_reg7_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(14),
      Q => slv_reg7(14)
    );
\slv_reg7_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(15),
      Q => slv_reg7(15)
    );
\slv_reg7_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(16),
      Q => slv_reg7(16)
    );
\slv_reg7_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(17),
      Q => slv_reg7(17)
    );
\slv_reg7_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(18),
      Q => slv_reg7(18)
    );
\slv_reg7_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(19),
      Q => slv_reg7(19)
    );
\slv_reg7_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(1),
      Q => slv_reg7(1)
    );
\slv_reg7_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(20),
      Q => slv_reg7(20)
    );
\slv_reg7_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(21),
      Q => slv_reg7(21)
    );
\slv_reg7_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(22),
      Q => slv_reg7(22)
    );
\slv_reg7_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(23),
      Q => slv_reg7(23)
    );
\slv_reg7_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(24),
      Q => slv_reg7(24)
    );
\slv_reg7_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(25),
      Q => slv_reg7(25)
    );
\slv_reg7_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(26),
      Q => slv_reg7(26)
    );
\slv_reg7_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(27),
      Q => slv_reg7(27)
    );
\slv_reg7_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(28),
      Q => slv_reg7(28)
    );
\slv_reg7_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(29),
      Q => slv_reg7(29)
    );
\slv_reg7_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(2),
      Q => slv_reg7(2)
    );
\slv_reg7_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(30),
      Q => slv_reg7(30)
    );
\slv_reg7_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(31),
      Q => slv_reg7(31)
    );
\slv_reg7_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(3),
      Q => slv_reg7(3)
    );
\slv_reg7_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(4),
      Q => slv_reg7(4)
    );
\slv_reg7_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(5),
      Q => slv_reg7(5)
    );
\slv_reg7_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(6),
      Q => slv_reg7(6)
    );
\slv_reg7_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(7),
      Q => slv_reg7(7)
    );
\slv_reg7_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(8),
      Q => slv_reg7(8)
    );
\slv_reg7_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => tc,
      CLR => p_0_in,
      D => \counter_inst_C/tmp_reg\(9),
      Q => slv_reg7(9)
    );
timers_inst: entity work.design_1_ip_timers_0_0_timers
     port map (
      CO(0) => tc,
      D(31 downto 0) => \counter_inst_B/tmp_reg\(31 downto 0),
      \FSM_onehot_state_reg[1]\ => \FSM_onehot_state_reg[1]\,
      Q(10) => clra,
      Q(9) => clrb,
      Q(8) => clrc,
      Q(7) => cea,
      Q(6) => ceb,
      Q(5) => cec,
      Q(4) => a_mode,
      Q(3) => b_mode,
      Q(2) => c_mode,
      Q(1) => \slv_reg5_reg_n_0_[1]\,
      Q(0) => \slv_reg5_reg_n_0_[0]\,
      \axi_araddr_reg[4]\(31 downto 0) => reg_data_out(31 downto 0),
      \axi_rdata_reg[0]\ => \axi_rdata[0]_i_2_n_0\,
      \axi_rdata_reg[10]\ => \axi_rdata[10]_i_3_n_0\,
      \axi_rdata_reg[11]\ => \axi_rdata[11]_i_3_n_0\,
      \axi_rdata_reg[12]\ => \axi_rdata[12]_i_3_n_0\,
      \axi_rdata_reg[13]\ => \axi_rdata[13]_i_3_n_0\,
      \axi_rdata_reg[14]\ => \axi_rdata[14]_i_3_n_0\,
      \axi_rdata_reg[15]\ => \axi_rdata[15]_i_3_n_0\,
      \axi_rdata_reg[16]\ => \axi_rdata[16]_i_3_n_0\,
      \axi_rdata_reg[17]\ => \axi_rdata[17]_i_3_n_0\,
      \axi_rdata_reg[18]\ => \axi_rdata[18]_i_3_n_0\,
      \axi_rdata_reg[19]\ => \axi_rdata[19]_i_3_n_0\,
      \axi_rdata_reg[1]\ => \axi_rdata[1]_i_3_n_0\,
      \axi_rdata_reg[20]\ => \axi_rdata[20]_i_3_n_0\,
      \axi_rdata_reg[21]\ => \axi_rdata[21]_i_3_n_0\,
      \axi_rdata_reg[22]\ => \axi_rdata[22]_i_3_n_0\,
      \axi_rdata_reg[23]\ => \axi_rdata[23]_i_3_n_0\,
      \axi_rdata_reg[24]\ => \axi_rdata[24]_i_3_n_0\,
      \axi_rdata_reg[25]\ => \axi_rdata[25]_i_3_n_0\,
      \axi_rdata_reg[26]\ => \axi_rdata[26]_i_3_n_0\,
      \axi_rdata_reg[27]\ => \axi_rdata[27]_i_3_n_0\,
      \axi_rdata_reg[28]\ => \axi_rdata[28]_i_3_n_0\,
      \axi_rdata_reg[29]\ => \axi_rdata[29]_i_3_n_0\,
      \axi_rdata_reg[2]\ => \axi_rdata[2]_i_3_n_0\,
      \axi_rdata_reg[30]\ => \axi_rdata[30]_i_3_n_0\,
      \axi_rdata_reg[31]\ => \axi_rdata[31]_i_4_n_0\,
      \axi_rdata_reg[3]\ => \axi_rdata[3]_i_3_n_0\,
      \axi_rdata_reg[4]\ => \axi_rdata[4]_i_3_n_0\,
      \axi_rdata_reg[5]\ => \axi_rdata[5]_i_3_n_0\,
      \axi_rdata_reg[6]\ => \axi_rdata[6]_i_3_n_0\,
      \axi_rdata_reg[7]\ => \axi_rdata[7]_i_3_n_0\,
      \axi_rdata_reg[8]\ => \axi_rdata[8]_i_3_n_0\,
      \axi_rdata_reg[9]\ => \axi_rdata[9]_i_3_n_0\,
      bram_seg => bram_seg,
      clk => clk,
      lt_dir(1 downto 0) => lt_dir(1 downto 0),
      sel0(2 downto 0) => sel0(2 downto 0),
      \slv_reg7_reg[0]\(31 downto 0) => slv_reg4(31 downto 0),
      tmp_reg(31 downto 0) => \counter_inst_C/tmp_reg\(31 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0_ip_timers_v2_0 is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \FSM_onehot_state_reg[1]\ : out STD_LOGIC;
    bram_seg : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    clk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    lt_dir : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_timers_0_0_ip_timers_v2_0 : entity is "ip_timers_v2_0";
end design_1_ip_timers_0_0_ip_timers_v2_0;

architecture STRUCTURE of design_1_ip_timers_0_0_ip_timers_v2_0 is
begin
ip_timers_v2_0_S00_AXI_inst: entity work.design_1_ip_timers_0_0_ip_timers_v2_0_S00_AXI
     port map (
      \FSM_onehot_state_reg[1]\ => \FSM_onehot_state_reg[1]\,
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      bram_seg => bram_seg,
      clk => clk,
      lt_dir(1 downto 0) => lt_dir(1 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(2 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(2 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_timers_0_0 is
  port (
    clk : in STD_LOGIC;
    lt_dir : in STD_LOGIC_VECTOR ( 1 downto 0 );
    timers_enabled : out STD_LOGIC;
    bram_seg : out STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_timers_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_timers_0_0 : entity is "design_1_ip_timers_0_0,ip_timers_v2_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_ip_timers_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_1_ip_timers_0_0 : entity is "ip_timers_v2_0,Vivado 2019.1";
end design_1_ip_timers_0_0;

architecture STRUCTURE of design_1_ip_timers_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 8, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.design_1_ip_timers_0_0_ip_timers_v2_0
     port map (
      \FSM_onehot_state_reg[1]\ => timers_enabled,
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      bram_seg => bram_seg,
      clk => clk,
      lt_dir(1 downto 0) => lt_dir(1 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(4 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(4 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
