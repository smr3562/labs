-- (c) Copyright 1995-2021 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:user:ip_dc_stabilizer:1.0
-- IP Revision: 17

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_ip_dc_stabilizer_0_0 IS
  PORT (
    x : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    y : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    feedback : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    resetn : IN STD_LOGIC;
    clk : IN STD_LOGIC;
    dbg_acc : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    axi_dc_stab_aclk : IN STD_LOGIC;
    axi_dc_stab_aresetn : IN STD_LOGIC;
    axi_dc_stab_awaddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    axi_dc_stab_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    axi_dc_stab_awvalid : IN STD_LOGIC;
    axi_dc_stab_awready : OUT STD_LOGIC;
    axi_dc_stab_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    axi_dc_stab_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    axi_dc_stab_wvalid : IN STD_LOGIC;
    axi_dc_stab_wready : OUT STD_LOGIC;
    axi_dc_stab_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    axi_dc_stab_bvalid : OUT STD_LOGIC;
    axi_dc_stab_bready : IN STD_LOGIC;
    axi_dc_stab_araddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    axi_dc_stab_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    axi_dc_stab_arvalid : IN STD_LOGIC;
    axi_dc_stab_arready : OUT STD_LOGIC;
    axi_dc_stab_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    axi_dc_stab_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    axi_dc_stab_rvalid : OUT STD_LOGIC;
    axi_dc_stab_rready : IN STD_LOGIC
  );
END design_1_ip_dc_stabilizer_0_0;

ARCHITECTURE design_1_ip_dc_stabilizer_0_0_arch OF design_1_ip_dc_stabilizer_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_ip_dc_stabilizer_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT ip_dc_stabilizer_v1_0 IS
    GENERIC (
      C_AXI_DC_STAB_DATA_WIDTH : INTEGER; -- Width of S_AXI data bus
      C_AXI_DC_STAB_ADDR_WIDTH : INTEGER -- Width of S_AXI address bus
    );
    PORT (
      x : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      y : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      feedback : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      resetn : IN STD_LOGIC;
      clk : IN STD_LOGIC;
      dbg_acc : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      axi_dc_stab_aclk : IN STD_LOGIC;
      axi_dc_stab_aresetn : IN STD_LOGIC;
      axi_dc_stab_awaddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      axi_dc_stab_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      axi_dc_stab_awvalid : IN STD_LOGIC;
      axi_dc_stab_awready : OUT STD_LOGIC;
      axi_dc_stab_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      axi_dc_stab_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      axi_dc_stab_wvalid : IN STD_LOGIC;
      axi_dc_stab_wready : OUT STD_LOGIC;
      axi_dc_stab_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      axi_dc_stab_bvalid : OUT STD_LOGIC;
      axi_dc_stab_bready : IN STD_LOGIC;
      axi_dc_stab_araddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      axi_dc_stab_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      axi_dc_stab_arvalid : IN STD_LOGIC;
      axi_dc_stab_arready : OUT STD_LOGIC;
      axi_dc_stab_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      axi_dc_stab_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      axi_dc_stab_rvalid : OUT STD_LOGIC;
      axi_dc_stab_rready : IN STD_LOGIC
    );
  END COMPONENT ip_dc_stabilizer_v1_0;
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWPROT";
  ATTRIBUTE X_INTERFACE_PARAMETER OF axi_dc_stab_awaddr: SIGNAL IS "XIL_INTERFACENAME AXI_DC_STAB, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, " & 
"NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWADDR";
  ATTRIBUTE X_INTERFACE_PARAMETER OF axi_dc_stab_aresetn: SIGNAL IS "XIL_INTERFACENAME AXI_DC_STAB_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 AXI_DC_STAB_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF axi_dc_stab_aclk: SIGNAL IS "XIL_INTERFACENAME AXI_DC_STAB_CLK, ASSOCIATED_BUSIF AXI_DC_STAB, ASSOCIATED_RESET axi_dc_stab_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF axi_dc_stab_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 AXI_DC_STAB_CLK CLK";
BEGIN
  U0 : ip_dc_stabilizer_v1_0
    GENERIC MAP (
      C_AXI_DC_STAB_DATA_WIDTH => 32,
      C_AXI_DC_STAB_ADDR_WIDTH => 4
    )
    PORT MAP (
      x => x,
      y => y,
      feedback => feedback,
      resetn => resetn,
      clk => clk,
      dbg_acc => dbg_acc,
      axi_dc_stab_aclk => axi_dc_stab_aclk,
      axi_dc_stab_aresetn => axi_dc_stab_aresetn,
      axi_dc_stab_awaddr => axi_dc_stab_awaddr,
      axi_dc_stab_awprot => axi_dc_stab_awprot,
      axi_dc_stab_awvalid => axi_dc_stab_awvalid,
      axi_dc_stab_awready => axi_dc_stab_awready,
      axi_dc_stab_wdata => axi_dc_stab_wdata,
      axi_dc_stab_wstrb => axi_dc_stab_wstrb,
      axi_dc_stab_wvalid => axi_dc_stab_wvalid,
      axi_dc_stab_wready => axi_dc_stab_wready,
      axi_dc_stab_bresp => axi_dc_stab_bresp,
      axi_dc_stab_bvalid => axi_dc_stab_bvalid,
      axi_dc_stab_bready => axi_dc_stab_bready,
      axi_dc_stab_araddr => axi_dc_stab_araddr,
      axi_dc_stab_arprot => axi_dc_stab_arprot,
      axi_dc_stab_arvalid => axi_dc_stab_arvalid,
      axi_dc_stab_arready => axi_dc_stab_arready,
      axi_dc_stab_rdata => axi_dc_stab_rdata,
      axi_dc_stab_rresp => axi_dc_stab_rresp,
      axi_dc_stab_rvalid => axi_dc_stab_rvalid,
      axi_dc_stab_rready => axi_dc_stab_rready
    );
END design_1_ip_dc_stabilizer_0_0_arch;
