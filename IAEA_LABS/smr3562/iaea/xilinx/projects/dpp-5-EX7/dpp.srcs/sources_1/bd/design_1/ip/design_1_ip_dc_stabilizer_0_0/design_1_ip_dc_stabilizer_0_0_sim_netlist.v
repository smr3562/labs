// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 17:52:15 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dc_stabilizer_0_0/design_1_ip_dc_stabilizer_0_0_sim_netlist.v
// Design      : design_1_ip_dc_stabilizer_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ip_dc_stabilizer_0_0,ip_dc_stabilizer_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "ip_dc_stabilizer_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_ip_dc_stabilizer_0_0
   (x,
    y,
    feedback,
    resetn,
    clk,
    dbg_acc,
    axi_dc_stab_aclk,
    axi_dc_stab_aresetn,
    axi_dc_stab_awaddr,
    axi_dc_stab_awprot,
    axi_dc_stab_awvalid,
    axi_dc_stab_awready,
    axi_dc_stab_wdata,
    axi_dc_stab_wstrb,
    axi_dc_stab_wvalid,
    axi_dc_stab_wready,
    axi_dc_stab_bresp,
    axi_dc_stab_bvalid,
    axi_dc_stab_bready,
    axi_dc_stab_araddr,
    axi_dc_stab_arprot,
    axi_dc_stab_arvalid,
    axi_dc_stab_arready,
    axi_dc_stab_rdata,
    axi_dc_stab_rresp,
    axi_dc_stab_rvalid,
    axi_dc_stab_rready);
  input [15:0]x;
  output [15:0]y;
  input [15:0]feedback;
  input resetn;
  input clk;
  output [15:0]dbg_acc;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 AXI_DC_STAB_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI_DC_STAB_CLK, ASSOCIATED_BUSIF AXI_DC_STAB, ASSOCIATED_RESET axi_dc_stab_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input axi_dc_stab_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 AXI_DC_STAB_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI_DC_STAB_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input axi_dc_stab_aresetn;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI_DC_STAB, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]axi_dc_stab_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWPROT" *) input [2:0]axi_dc_stab_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWVALID" *) input axi_dc_stab_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB AWREADY" *) output axi_dc_stab_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WDATA" *) input [31:0]axi_dc_stab_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WSTRB" *) input [3:0]axi_dc_stab_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WVALID" *) input axi_dc_stab_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB WREADY" *) output axi_dc_stab_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BRESP" *) output [1:0]axi_dc_stab_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BVALID" *) output axi_dc_stab_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB BREADY" *) input axi_dc_stab_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARADDR" *) input [3:0]axi_dc_stab_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARPROT" *) input [2:0]axi_dc_stab_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARVALID" *) input axi_dc_stab_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB ARREADY" *) output axi_dc_stab_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RDATA" *) output [31:0]axi_dc_stab_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RRESP" *) output [1:0]axi_dc_stab_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RVALID" *) output axi_dc_stab_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI_DC_STAB RREADY" *) input axi_dc_stab_rready;

  wire \<const0> ;
  wire axi_dc_stab_aclk;
  wire [3:0]axi_dc_stab_araddr;
  wire axi_dc_stab_aresetn;
  wire axi_dc_stab_arready;
  wire axi_dc_stab_arvalid;
  wire [3:0]axi_dc_stab_awaddr;
  wire axi_dc_stab_awready;
  wire axi_dc_stab_awvalid;
  wire axi_dc_stab_bready;
  wire axi_dc_stab_bvalid;
  wire [31:0]axi_dc_stab_rdata;
  wire axi_dc_stab_rready;
  wire axi_dc_stab_rvalid;
  wire [31:0]axi_dc_stab_wdata;
  wire axi_dc_stab_wready;
  wire [3:0]axi_dc_stab_wstrb;
  wire axi_dc_stab_wvalid;
  wire clk;
  wire [15:0]dbg_acc;
  wire [15:0]feedback;
  wire resetn;
  wire [15:0]x;
  wire [15:0]y;

  assign axi_dc_stab_bresp[1] = \<const0> ;
  assign axi_dc_stab_bresp[0] = \<const0> ;
  assign axi_dc_stab_rresp[1] = \<const0> ;
  assign axi_dc_stab_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_ip_dc_stabilizer_0_0_ip_dc_stabilizer_v1_0 U0
       (.S_AXI_ARREADY(axi_dc_stab_arready),
        .S_AXI_AWREADY(axi_dc_stab_awready),
        .S_AXI_WREADY(axi_dc_stab_wready),
        .axi_dc_stab_aclk(axi_dc_stab_aclk),
        .axi_dc_stab_araddr(axi_dc_stab_araddr[3:2]),
        .axi_dc_stab_aresetn(axi_dc_stab_aresetn),
        .axi_dc_stab_arvalid(axi_dc_stab_arvalid),
        .axi_dc_stab_awaddr(axi_dc_stab_awaddr[3:2]),
        .axi_dc_stab_awvalid(axi_dc_stab_awvalid),
        .axi_dc_stab_bready(axi_dc_stab_bready),
        .axi_dc_stab_bvalid(axi_dc_stab_bvalid),
        .axi_dc_stab_rdata(axi_dc_stab_rdata),
        .axi_dc_stab_rready(axi_dc_stab_rready),
        .axi_dc_stab_rvalid(axi_dc_stab_rvalid),
        .axi_dc_stab_wdata(axi_dc_stab_wdata),
        .axi_dc_stab_wstrb(axi_dc_stab_wstrb),
        .axi_dc_stab_wvalid(axi_dc_stab_wvalid),
        .clk(clk),
        .dbg_acc(dbg_acc),
        .feedback(feedback),
        .resetn(resetn),
        .x(x),
        .y(y));
endmodule

(* ORIG_REF_NAME = "dc_stabilizer" *) 
module design_1_ip_dc_stabilizer_0_0_dc_stabilizer
   (dbg_acc,
    y,
    SR,
    clk,
    x,
    Q,
    resetn,
    feedback,
    acc321_carry__0_0,
    \acc321_inferred__0/i__carry__0_0 );
  output [15:0]dbg_acc;
  output [15:0]y;
  output [0:0]SR;
  input clk;
  input [15:0]x;
  input [5:0]Q;
  input resetn;
  input [15:0]feedback;
  input [15:0]acc321_carry__0_0;
  input [15:0]\acc321_inferred__0/i__carry__0_0 ;

  wire [5:0]Q;
  wire [15:0]RESIZE;
  wire [0:0]SR;
  wire acc321;
  wire acc3210_in;
  wire [15:0]acc321_carry__0_0;
  wire acc321_carry__0_i_1_n_0;
  wire acc321_carry__0_i_2_n_0;
  wire acc321_carry__0_i_3_n_0;
  wire acc321_carry__0_i_4_n_0;
  wire acc321_carry__0_i_5_n_0;
  wire acc321_carry__0_i_6_n_0;
  wire acc321_carry__0_i_7_n_0;
  wire acc321_carry__0_i_8_n_0;
  wire acc321_carry__0_n_1;
  wire acc321_carry__0_n_2;
  wire acc321_carry__0_n_3;
  wire acc321_carry_i_1_n_0;
  wire acc321_carry_i_2_n_0;
  wire acc321_carry_i_3_n_0;
  wire acc321_carry_i_4_n_0;
  wire acc321_carry_i_5_n_0;
  wire acc321_carry_i_6_n_0;
  wire acc321_carry_i_7_n_0;
  wire acc321_carry_i_8_n_0;
  wire acc321_carry_n_0;
  wire acc321_carry_n_1;
  wire acc321_carry_n_2;
  wire acc321_carry_n_3;
  wire [15:0]\acc321_inferred__0/i__carry__0_0 ;
  wire \acc321_inferred__0/i__carry__0_n_1 ;
  wire \acc321_inferred__0/i__carry__0_n_2 ;
  wire \acc321_inferred__0/i__carry__0_n_3 ;
  wire \acc321_inferred__0/i__carry_n_0 ;
  wire \acc321_inferred__0/i__carry_n_1 ;
  wire \acc321_inferred__0/i__carry_n_2 ;
  wire \acc321_inferred__0/i__carry_n_3 ;
  wire \acc32[11]_i_2_n_0 ;
  wire \acc32[11]_i_3_n_0 ;
  wire \acc32[11]_i_4_n_0 ;
  wire \acc32[11]_i_5_n_0 ;
  wire \acc32[14]_i_2_n_0 ;
  wire \acc32[14]_i_3_n_0 ;
  wire \acc32[14]_i_4_n_0 ;
  wire \acc32[14]_i_5_n_0 ;
  wire \acc32[16]_i_2_n_0 ;
  wire \acc32[16]_i_3_n_0 ;
  wire \acc32[16]_i_4_n_0 ;
  wire \acc32[16]_i_5_n_0 ;
  wire \acc32[20]_i_2_n_0 ;
  wire \acc32[20]_i_3_n_0 ;
  wire \acc32[20]_i_4_n_0 ;
  wire \acc32[20]_i_5_n_0 ;
  wire \acc32[24]_i_2_n_0 ;
  wire \acc32[24]_i_3_n_0 ;
  wire \acc32[24]_i_4_n_0 ;
  wire \acc32[24]_i_5_n_0 ;
  wire \acc32[31]_i_1_n_0 ;
  wire \acc32[31]_i_4_n_0 ;
  wire \acc32[31]_i_5_n_0 ;
  wire \acc32[31]_i_6_n_0 ;
  wire \acc32[31]_i_7_n_0 ;
  wire \acc32[3]_i_2_n_0 ;
  wire \acc32[3]_i_3_n_0 ;
  wire \acc32[3]_i_4_n_0 ;
  wire \acc32[3]_i_5_n_0 ;
  wire \acc32[7]_i_2_n_0 ;
  wire \acc32[7]_i_3_n_0 ;
  wire \acc32[7]_i_4_n_0 ;
  wire \acc32[7]_i_5_n_0 ;
  wire [30:15]acc32_reg;
  wire \acc32_reg[11]_i_1_n_0 ;
  wire \acc32_reg[11]_i_1_n_1 ;
  wire \acc32_reg[11]_i_1_n_2 ;
  wire \acc32_reg[11]_i_1_n_3 ;
  wire \acc32_reg[11]_i_1_n_4 ;
  wire \acc32_reg[11]_i_1_n_5 ;
  wire \acc32_reg[11]_i_1_n_6 ;
  wire \acc32_reg[11]_i_1_n_7 ;
  wire \acc32_reg[14]_i_1_n_0 ;
  wire \acc32_reg[14]_i_1_n_1 ;
  wire \acc32_reg[14]_i_1_n_2 ;
  wire \acc32_reg[14]_i_1_n_3 ;
  wire \acc32_reg[14]_i_1_n_4 ;
  wire \acc32_reg[14]_i_1_n_5 ;
  wire \acc32_reg[14]_i_1_n_6 ;
  wire \acc32_reg[14]_i_1_n_7 ;
  wire \acc32_reg[16]_i_1_n_0 ;
  wire \acc32_reg[16]_i_1_n_1 ;
  wire \acc32_reg[16]_i_1_n_2 ;
  wire \acc32_reg[16]_i_1_n_3 ;
  wire \acc32_reg[16]_i_1_n_4 ;
  wire \acc32_reg[16]_i_1_n_5 ;
  wire \acc32_reg[16]_i_1_n_6 ;
  wire \acc32_reg[16]_i_1_n_7 ;
  wire \acc32_reg[20]_i_1_n_0 ;
  wire \acc32_reg[20]_i_1_n_1 ;
  wire \acc32_reg[20]_i_1_n_2 ;
  wire \acc32_reg[20]_i_1_n_3 ;
  wire \acc32_reg[20]_i_1_n_4 ;
  wire \acc32_reg[20]_i_1_n_5 ;
  wire \acc32_reg[20]_i_1_n_6 ;
  wire \acc32_reg[20]_i_1_n_7 ;
  wire \acc32_reg[24]_i_1_n_0 ;
  wire \acc32_reg[24]_i_1_n_1 ;
  wire \acc32_reg[24]_i_1_n_2 ;
  wire \acc32_reg[24]_i_1_n_3 ;
  wire \acc32_reg[24]_i_1_n_4 ;
  wire \acc32_reg[24]_i_1_n_5 ;
  wire \acc32_reg[24]_i_1_n_6 ;
  wire \acc32_reg[24]_i_1_n_7 ;
  wire \acc32_reg[31]_i_3_n_1 ;
  wire \acc32_reg[31]_i_3_n_2 ;
  wire \acc32_reg[31]_i_3_n_3 ;
  wire \acc32_reg[31]_i_3_n_4 ;
  wire \acc32_reg[31]_i_3_n_5 ;
  wire \acc32_reg[31]_i_3_n_6 ;
  wire \acc32_reg[31]_i_3_n_7 ;
  wire \acc32_reg[3]_i_1_n_0 ;
  wire \acc32_reg[3]_i_1_n_1 ;
  wire \acc32_reg[3]_i_1_n_2 ;
  wire \acc32_reg[3]_i_1_n_3 ;
  wire \acc32_reg[3]_i_1_n_4 ;
  wire \acc32_reg[3]_i_1_n_5 ;
  wire \acc32_reg[3]_i_1_n_6 ;
  wire \acc32_reg[3]_i_1_n_7 ;
  wire \acc32_reg[7]_i_1_n_0 ;
  wire \acc32_reg[7]_i_1_n_1 ;
  wire \acc32_reg[7]_i_1_n_2 ;
  wire \acc32_reg[7]_i_1_n_3 ;
  wire \acc32_reg[7]_i_1_n_4 ;
  wire \acc32_reg[7]_i_1_n_5 ;
  wire \acc32_reg[7]_i_1_n_6 ;
  wire \acc32_reg[7]_i_1_n_7 ;
  wire clk;
  wire [15:0]dbg_acc;
  wire en;
  wire [15:0]feedback;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__0_i_6_n_0;
  wire i__carry__0_i_7_n_0;
  wire i__carry__0_i_8_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire p_0_in;
  wire p_1_in;
  wire [31:0]p_2_in;
  wire resetn;
  wire result3_carry__0_i_10_n_0;
  wire result3_carry__0_i_11_n_0;
  wire result3_carry__0_i_12_n_0;
  wire result3_carry__0_i_1_n_0;
  wire result3_carry__0_i_2_n_0;
  wire result3_carry__0_i_3_n_0;
  wire result3_carry__0_i_4_n_0;
  wire result3_carry__0_i_5_n_0;
  wire result3_carry__0_i_6_n_0;
  wire result3_carry__0_i_7_n_0;
  wire result3_carry__0_i_8_n_0;
  wire result3_carry__0_i_9_n_0;
  wire result3_carry__0_n_0;
  wire result3_carry__0_n_1;
  wire result3_carry__0_n_2;
  wire result3_carry__0_n_3;
  wire result3_carry__0_n_4;
  wire result3_carry__0_n_5;
  wire result3_carry__0_n_6;
  wire result3_carry__0_n_7;
  wire result3_carry__1_i_10_n_0;
  wire result3_carry__1_i_11_n_0;
  wire result3_carry__1_i_12_n_0;
  wire result3_carry__1_i_1_n_0;
  wire result3_carry__1_i_2_n_0;
  wire result3_carry__1_i_3_n_0;
  wire result3_carry__1_i_4_n_0;
  wire result3_carry__1_i_5_n_0;
  wire result3_carry__1_i_6_n_0;
  wire result3_carry__1_i_7_n_0;
  wire result3_carry__1_i_8_n_0;
  wire result3_carry__1_i_9_n_0;
  wire result3_carry__1_n_0;
  wire result3_carry__1_n_1;
  wire result3_carry__1_n_2;
  wire result3_carry__1_n_3;
  wire result3_carry__1_n_4;
  wire result3_carry__1_n_5;
  wire result3_carry__1_n_6;
  wire result3_carry__1_n_7;
  wire result3_carry__2_i_10_n_0;
  wire result3_carry__2_i_11_n_0;
  wire result3_carry__2_i_12_n_0;
  wire result3_carry__2_i_13_n_0;
  wire result3_carry__2_i_2_n_0;
  wire result3_carry__2_i_3_n_0;
  wire result3_carry__2_i_4_n_0;
  wire result3_carry__2_i_5_n_0;
  wire result3_carry__2_i_6_n_0;
  wire result3_carry__2_i_7_n_0;
  wire result3_carry__2_i_8_n_0;
  wire result3_carry__2_i_9_n_0;
  wire result3_carry__2_n_0;
  wire result3_carry__2_n_1;
  wire result3_carry__2_n_2;
  wire result3_carry__2_n_3;
  wire result3_carry__2_n_5;
  wire result3_carry__2_n_6;
  wire result3_carry__2_n_7;
  wire result3_carry__3_i_10_n_0;
  wire result3_carry__3_i_11_n_0;
  wire result3_carry__3_i_12_n_0;
  wire result3_carry__3_i_13_n_0;
  wire result3_carry__3_i_14_n_0;
  wire result3_carry__3_i_15_n_0;
  wire result3_carry__3_i_16_n_0;
  wire result3_carry__3_i_17_n_0;
  wire result3_carry__3_i_18_n_0;
  wire result3_carry__3_i_19_n_0;
  wire result3_carry__3_i_1_n_0;
  wire result3_carry__3_i_20_n_0;
  wire result3_carry__3_i_21_n_0;
  wire result3_carry__3_i_22_n_0;
  wire result3_carry__3_i_2_n_0;
  wire result3_carry__3_i_3_n_0;
  wire result3_carry__3_i_4_n_0;
  wire result3_carry__3_i_5_n_0;
  wire result3_carry__3_i_6_n_0;
  wire result3_carry__3_i_7_n_0;
  wire result3_carry__3_i_8_n_0;
  wire result3_carry__3_i_9_n_0;
  wire result3_carry__3_n_0;
  wire result3_carry__3_n_1;
  wire result3_carry__3_n_2;
  wire result3_carry__3_n_3;
  wire result3_carry__4_i_10_n_0;
  wire result3_carry__4_i_11_n_0;
  wire result3_carry__4_i_12_n_0;
  wire result3_carry__4_i_13_n_0;
  wire result3_carry__4_i_14_n_0;
  wire result3_carry__4_i_15_n_0;
  wire result3_carry__4_i_16_n_0;
  wire result3_carry__4_i_17_n_0;
  wire result3_carry__4_i_1_n_0;
  wire result3_carry__4_i_2_n_0;
  wire result3_carry__4_i_3_n_0;
  wire result3_carry__4_i_4_n_0;
  wire result3_carry__4_i_5_n_0;
  wire result3_carry__4_i_6_n_0;
  wire result3_carry__4_i_7_n_0;
  wire result3_carry__4_i_8_n_0;
  wire result3_carry__4_i_9_n_0;
  wire result3_carry__4_n_0;
  wire result3_carry__4_n_1;
  wire result3_carry__4_n_2;
  wire result3_carry__4_n_3;
  wire result3_carry__5_i_14_n_0;
  wire result3_carry__5_i_15_n_0;
  wire result3_carry__5_i_1_n_0;
  wire result3_carry__5_i_2_n_0;
  wire result3_carry__5_i_3_n_0;
  wire result3_carry__5_i_4_n_0;
  wire result3_carry__5_i_5_n_0;
  wire result3_carry__5_i_6_n_0;
  wire result3_carry__5_i_7_n_0;
  wire result3_carry__5_i_8_n_0;
  wire result3_carry__5_n_0;
  wire result3_carry__5_n_1;
  wire result3_carry__5_n_2;
  wire result3_carry__5_n_3;
  wire result3_carry__6_i_10_n_0;
  wire result3_carry__6_i_11_n_0;
  wire result3_carry__6_i_1_n_0;
  wire result3_carry__6_i_2_n_0;
  wire result3_carry__6_i_3_n_0;
  wire result3_carry__6_i_4_n_0;
  wire result3_carry__6_i_5_n_0;
  wire result3_carry__6_i_6_n_0;
  wire result3_carry__6_i_7_n_0;
  wire result3_carry__6_i_8_n_0;
  wire result3_carry__6_i_9_n_0;
  wire result3_carry__6_n_1;
  wire result3_carry__6_n_2;
  wire result3_carry__6_n_3;
  wire result3_carry_i_10_n_0;
  wire result3_carry_i_11_n_0;
  wire result3_carry_i_12_n_0;
  wire result3_carry_i_13_n_0;
  wire result3_carry_i_14_n_0;
  wire result3_carry_i_15_n_0;
  wire result3_carry_i_16_n_0;
  wire result3_carry_i_17_n_0;
  wire result3_carry_i_18_n_0;
  wire result3_carry_i_19_n_0;
  wire result3_carry_i_1_n_0;
  wire result3_carry_i_20_n_0;
  wire result3_carry_i_21_n_0;
  wire result3_carry_i_2_n_0;
  wire result3_carry_i_3_n_0;
  wire result3_carry_i_4_n_0;
  wire result3_carry_i_5_n_0;
  wire result3_carry_i_6_n_0;
  wire result3_carry_i_7_n_0;
  wire result3_carry_i_8_n_0;
  wire result3_carry_i_9_n_0;
  wire result3_carry_n_0;
  wire result3_carry_n_1;
  wire result3_carry_n_2;
  wire result3_carry_n_3;
  wire result3_carry_n_4;
  wire result3_carry_n_5;
  wire result3_carry_n_6;
  wire result3_carry_n_7;
  wire \result_reg[0]_C_n_0 ;
  wire \result_reg[0]_LDC_i_1_n_0 ;
  wire \result_reg[0]_LDC_i_2_n_0 ;
  wire \result_reg[0]_LDC_n_0 ;
  wire \result_reg[0]_P_n_0 ;
  wire \result_reg[10]_C_n_0 ;
  wire \result_reg[10]_LDC_i_1_n_0 ;
  wire \result_reg[10]_LDC_i_2_n_0 ;
  wire \result_reg[10]_LDC_n_0 ;
  wire \result_reg[10]_P_n_0 ;
  wire \result_reg[11]_C_n_0 ;
  wire \result_reg[11]_LDC_i_1_n_0 ;
  wire \result_reg[11]_LDC_i_2_n_0 ;
  wire \result_reg[11]_LDC_n_0 ;
  wire \result_reg[11]_P_n_0 ;
  wire \result_reg[12]_C_n_0 ;
  wire \result_reg[12]_LDC_i_1_n_0 ;
  wire \result_reg[12]_LDC_i_2_n_0 ;
  wire \result_reg[12]_LDC_n_0 ;
  wire \result_reg[12]_P_n_0 ;
  wire \result_reg[13]_C_n_0 ;
  wire \result_reg[13]_LDC_i_1_n_0 ;
  wire \result_reg[13]_LDC_i_2_n_0 ;
  wire \result_reg[13]_LDC_n_0 ;
  wire \result_reg[13]_P_n_0 ;
  wire \result_reg[14]_C_n_0 ;
  wire \result_reg[14]_LDC_i_1_n_0 ;
  wire \result_reg[14]_LDC_i_2_n_0 ;
  wire \result_reg[14]_LDC_n_0 ;
  wire \result_reg[14]_P_n_0 ;
  wire \result_reg[1]_C_n_0 ;
  wire \result_reg[1]_LDC_i_1_n_0 ;
  wire \result_reg[1]_LDC_i_2_n_0 ;
  wire \result_reg[1]_LDC_n_0 ;
  wire \result_reg[1]_P_n_0 ;
  wire \result_reg[2]_C_n_0 ;
  wire \result_reg[2]_LDC_i_1_n_0 ;
  wire \result_reg[2]_LDC_i_2_n_0 ;
  wire \result_reg[2]_LDC_n_0 ;
  wire \result_reg[2]_P_n_0 ;
  wire \result_reg[31]_C_n_0 ;
  wire \result_reg[31]_LDC_i_1_n_0 ;
  wire \result_reg[31]_LDC_i_2_n_0 ;
  wire \result_reg[31]_LDC_n_0 ;
  wire \result_reg[31]_P_n_0 ;
  wire \result_reg[3]_C_n_0 ;
  wire \result_reg[3]_LDC_i_1_n_0 ;
  wire \result_reg[3]_LDC_i_2_n_0 ;
  wire \result_reg[3]_LDC_n_0 ;
  wire \result_reg[3]_P_n_0 ;
  wire \result_reg[4]_C_n_0 ;
  wire \result_reg[4]_LDC_i_1_n_0 ;
  wire \result_reg[4]_LDC_i_2_n_0 ;
  wire \result_reg[4]_LDC_n_0 ;
  wire \result_reg[4]_P_n_0 ;
  wire \result_reg[5]_C_n_0 ;
  wire \result_reg[5]_LDC_i_1_n_0 ;
  wire \result_reg[5]_LDC_i_2_n_0 ;
  wire \result_reg[5]_LDC_n_0 ;
  wire \result_reg[5]_P_n_0 ;
  wire \result_reg[6]_C_n_0 ;
  wire \result_reg[6]_LDC_i_1_n_0 ;
  wire \result_reg[6]_LDC_i_2_n_0 ;
  wire \result_reg[6]_LDC_n_0 ;
  wire \result_reg[6]_P_n_0 ;
  wire \result_reg[7]_C_n_0 ;
  wire \result_reg[7]_LDC_i_1_n_0 ;
  wire \result_reg[7]_LDC_i_2_n_0 ;
  wire \result_reg[7]_LDC_n_0 ;
  wire \result_reg[7]_P_n_0 ;
  wire \result_reg[8]_C_n_0 ;
  wire \result_reg[8]_LDC_i_1_n_0 ;
  wire \result_reg[8]_LDC_i_2_n_0 ;
  wire \result_reg[8]_LDC_n_0 ;
  wire \result_reg[8]_P_n_0 ;
  wire \result_reg[9]_C_n_0 ;
  wire \result_reg[9]_LDC_i_1_n_0 ;
  wire \result_reg[9]_LDC_i_2_n_0 ;
  wire \result_reg[9]_LDC_n_0 ;
  wire \result_reg[9]_P_n_0 ;
  wire [27:15]u32;
  wire [15:0]x;
  wire [15:0]y;
  wire [3:0]NLW_acc321_carry_O_UNCONNECTED;
  wire [3:0]NLW_acc321_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_acc321_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_acc321_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:3]\NLW_acc32_reg[31]_i_3_CO_UNCONNECTED ;
  wire [3:0]NLW_result3_carry__3_O_UNCONNECTED;
  wire [3:0]NLW_result3_carry__4_O_UNCONNECTED;
  wire [3:0]NLW_result3_carry__5_O_UNCONNECTED;
  wire [3:3]NLW_result3_carry__6_CO_UNCONNECTED;
  wire [2:0]NLW_result3_carry__6_O_UNCONNECTED;

  CARRY4 acc321_carry
       (.CI(1'b0),
        .CO({acc321_carry_n_0,acc321_carry_n_1,acc321_carry_n_2,acc321_carry_n_3}),
        .CYINIT(1'b0),
        .DI({acc321_carry_i_1_n_0,acc321_carry_i_2_n_0,acc321_carry_i_3_n_0,acc321_carry_i_4_n_0}),
        .O(NLW_acc321_carry_O_UNCONNECTED[3:0]),
        .S({acc321_carry_i_5_n_0,acc321_carry_i_6_n_0,acc321_carry_i_7_n_0,acc321_carry_i_8_n_0}));
  CARRY4 acc321_carry__0
       (.CI(acc321_carry_n_0),
        .CO({acc321,acc321_carry__0_n_1,acc321_carry__0_n_2,acc321_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({acc321_carry__0_i_1_n_0,acc321_carry__0_i_2_n_0,acc321_carry__0_i_3_n_0,acc321_carry__0_i_4_n_0}),
        .O(NLW_acc321_carry__0_O_UNCONNECTED[3:0]),
        .S({acc321_carry__0_i_5_n_0,acc321_carry__0_i_6_n_0,acc321_carry__0_i_7_n_0,acc321_carry__0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    acc321_carry__0_i_1
       (.I0(feedback[14]),
        .I1(acc321_carry__0_0[14]),
        .I2(feedback[15]),
        .I3(acc321_carry__0_0[15]),
        .O(acc321_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    acc321_carry__0_i_2
       (.I0(feedback[12]),
        .I1(acc321_carry__0_0[12]),
        .I2(acc321_carry__0_0[13]),
        .I3(feedback[13]),
        .O(acc321_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    acc321_carry__0_i_3
       (.I0(feedback[10]),
        .I1(acc321_carry__0_0[10]),
        .I2(acc321_carry__0_0[11]),
        .I3(feedback[11]),
        .O(acc321_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    acc321_carry__0_i_4
       (.I0(feedback[8]),
        .I1(acc321_carry__0_0[8]),
        .I2(acc321_carry__0_0[9]),
        .I3(feedback[9]),
        .O(acc321_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    acc321_carry__0_i_5
       (.I0(feedback[14]),
        .I1(acc321_carry__0_0[14]),
        .I2(acc321_carry__0_0[15]),
        .I3(feedback[15]),
        .O(acc321_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    acc321_carry__0_i_6
       (.I0(feedback[12]),
        .I1(acc321_carry__0_0[12]),
        .I2(feedback[13]),
        .I3(acc321_carry__0_0[13]),
        .O(acc321_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    acc321_carry__0_i_7
       (.I0(feedback[10]),
        .I1(acc321_carry__0_0[10]),
        .I2(feedback[11]),
        .I3(acc321_carry__0_0[11]),
        .O(acc321_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    acc321_carry__0_i_8
       (.I0(feedback[8]),
        .I1(acc321_carry__0_0[8]),
        .I2(feedback[9]),
        .I3(acc321_carry__0_0[9]),
        .O(acc321_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    acc321_carry_i_1
       (.I0(feedback[6]),
        .I1(acc321_carry__0_0[6]),
        .I2(acc321_carry__0_0[7]),
        .I3(feedback[7]),
        .O(acc321_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    acc321_carry_i_2
       (.I0(feedback[4]),
        .I1(acc321_carry__0_0[4]),
        .I2(acc321_carry__0_0[5]),
        .I3(feedback[5]),
        .O(acc321_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    acc321_carry_i_3
       (.I0(feedback[2]),
        .I1(acc321_carry__0_0[2]),
        .I2(acc321_carry__0_0[3]),
        .I3(feedback[3]),
        .O(acc321_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    acc321_carry_i_4
       (.I0(feedback[0]),
        .I1(acc321_carry__0_0[0]),
        .I2(acc321_carry__0_0[1]),
        .I3(feedback[1]),
        .O(acc321_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    acc321_carry_i_5
       (.I0(feedback[6]),
        .I1(acc321_carry__0_0[6]),
        .I2(feedback[7]),
        .I3(acc321_carry__0_0[7]),
        .O(acc321_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    acc321_carry_i_6
       (.I0(feedback[4]),
        .I1(acc321_carry__0_0[4]),
        .I2(feedback[5]),
        .I3(acc321_carry__0_0[5]),
        .O(acc321_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    acc321_carry_i_7
       (.I0(feedback[2]),
        .I1(acc321_carry__0_0[2]),
        .I2(feedback[3]),
        .I3(acc321_carry__0_0[3]),
        .O(acc321_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    acc321_carry_i_8
       (.I0(feedback[0]),
        .I1(acc321_carry__0_0[0]),
        .I2(feedback[1]),
        .I3(acc321_carry__0_0[1]),
        .O(acc321_carry_i_8_n_0));
  CARRY4 \acc321_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\acc321_inferred__0/i__carry_n_0 ,\acc321_inferred__0/i__carry_n_1 ,\acc321_inferred__0/i__carry_n_2 ,\acc321_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_acc321_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0}));
  CARRY4 \acc321_inferred__0/i__carry__0 
       (.CI(\acc321_inferred__0/i__carry_n_0 ),
        .CO({acc3210_in,\acc321_inferred__0/i__carry__0_n_1 ,\acc321_inferred__0/i__carry__0_n_2 ,\acc321_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0}),
        .O(\NLW_acc321_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_5_n_0,i__carry__0_i_6_n_0,i__carry__0_i_7_n_0,i__carry__0_i_8_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[11]_i_2 
       (.I0(feedback[11]),
        .I1(dbg_acc[11]),
        .O(\acc32[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[11]_i_3 
       (.I0(feedback[10]),
        .I1(dbg_acc[10]),
        .O(\acc32[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[11]_i_4 
       (.I0(feedback[9]),
        .I1(dbg_acc[9]),
        .O(\acc32[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[11]_i_5 
       (.I0(feedback[8]),
        .I1(dbg_acc[8]),
        .O(\acc32[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[14]_i_2 
       (.I0(feedback[15]),
        .I1(acc32_reg[15]),
        .O(\acc32[14]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[14]_i_3 
       (.I0(feedback[14]),
        .I1(dbg_acc[14]),
        .O(\acc32[14]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[14]_i_4 
       (.I0(feedback[13]),
        .I1(dbg_acc[13]),
        .O(\acc32[14]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[14]_i_5 
       (.I0(feedback[12]),
        .I1(dbg_acc[12]),
        .O(\acc32[14]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[16]_i_2 
       (.I0(feedback[15]),
        .I1(acc32_reg[19]),
        .O(\acc32[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[16]_i_3 
       (.I0(feedback[15]),
        .I1(acc32_reg[18]),
        .O(\acc32[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[16]_i_4 
       (.I0(feedback[15]),
        .I1(acc32_reg[17]),
        .O(\acc32[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[16]_i_5 
       (.I0(feedback[15]),
        .I1(acc32_reg[16]),
        .O(\acc32[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[20]_i_2 
       (.I0(feedback[15]),
        .I1(acc32_reg[23]),
        .O(\acc32[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[20]_i_3 
       (.I0(feedback[15]),
        .I1(acc32_reg[22]),
        .O(\acc32[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[20]_i_4 
       (.I0(feedback[15]),
        .I1(acc32_reg[21]),
        .O(\acc32[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[20]_i_5 
       (.I0(feedback[15]),
        .I1(acc32_reg[20]),
        .O(\acc32[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[24]_i_2 
       (.I0(feedback[15]),
        .I1(acc32_reg[27]),
        .O(\acc32[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[24]_i_3 
       (.I0(feedback[15]),
        .I1(acc32_reg[26]),
        .O(\acc32[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[24]_i_4 
       (.I0(feedback[15]),
        .I1(acc32_reg[25]),
        .O(\acc32[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[24]_i_5 
       (.I0(feedback[15]),
        .I1(acc32_reg[24]),
        .O(\acc32[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \acc32[31]_i_1 
       (.I0(resetn),
        .I1(Q[5]),
        .O(\acc32[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \acc32[31]_i_2 
       (.I0(acc3210_in),
        .I1(acc321),
        .O(en));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[31]_i_4 
       (.I0(feedback[15]),
        .I1(dbg_acc[15]),
        .O(\acc32[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[31]_i_5 
       (.I0(feedback[15]),
        .I1(acc32_reg[30]),
        .O(\acc32[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[31]_i_6 
       (.I0(feedback[15]),
        .I1(acc32_reg[29]),
        .O(\acc32[31]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[31]_i_7 
       (.I0(feedback[15]),
        .I1(acc32_reg[28]),
        .O(\acc32[31]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[3]_i_2 
       (.I0(feedback[3]),
        .I1(dbg_acc[3]),
        .O(\acc32[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[3]_i_3 
       (.I0(feedback[2]),
        .I1(dbg_acc[2]),
        .O(\acc32[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[3]_i_4 
       (.I0(feedback[1]),
        .I1(dbg_acc[1]),
        .O(\acc32[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[3]_i_5 
       (.I0(feedback[0]),
        .I1(dbg_acc[0]),
        .O(\acc32[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[7]_i_2 
       (.I0(feedback[7]),
        .I1(dbg_acc[7]),
        .O(\acc32[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[7]_i_3 
       (.I0(feedback[6]),
        .I1(dbg_acc[6]),
        .O(\acc32[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[7]_i_4 
       (.I0(feedback[5]),
        .I1(dbg_acc[5]),
        .O(\acc32[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc32[7]_i_5 
       (.I0(feedback[4]),
        .I1(dbg_acc[4]),
        .O(\acc32[7]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[0] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[3]_i_1_n_7 ),
        .Q(dbg_acc[0]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[10] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[11]_i_1_n_5 ),
        .Q(dbg_acc[10]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[11] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[11]_i_1_n_4 ),
        .Q(dbg_acc[11]),
        .R(\acc32[31]_i_1_n_0 ));
  CARRY4 \acc32_reg[11]_i_1 
       (.CI(\acc32_reg[7]_i_1_n_0 ),
        .CO({\acc32_reg[11]_i_1_n_0 ,\acc32_reg[11]_i_1_n_1 ,\acc32_reg[11]_i_1_n_2 ,\acc32_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(feedback[11:8]),
        .O({\acc32_reg[11]_i_1_n_4 ,\acc32_reg[11]_i_1_n_5 ,\acc32_reg[11]_i_1_n_6 ,\acc32_reg[11]_i_1_n_7 }),
        .S({\acc32[11]_i_2_n_0 ,\acc32[11]_i_3_n_0 ,\acc32[11]_i_4_n_0 ,\acc32[11]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[12] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[14]_i_1_n_7 ),
        .Q(dbg_acc[12]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[13] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[14]_i_1_n_6 ),
        .Q(dbg_acc[13]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[14] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[14]_i_1_n_5 ),
        .Q(dbg_acc[14]),
        .R(\acc32[31]_i_1_n_0 ));
  CARRY4 \acc32_reg[14]_i_1 
       (.CI(\acc32_reg[11]_i_1_n_0 ),
        .CO({\acc32_reg[14]_i_1_n_0 ,\acc32_reg[14]_i_1_n_1 ,\acc32_reg[14]_i_1_n_2 ,\acc32_reg[14]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(feedback[15:12]),
        .O({\acc32_reg[14]_i_1_n_4 ,\acc32_reg[14]_i_1_n_5 ,\acc32_reg[14]_i_1_n_6 ,\acc32_reg[14]_i_1_n_7 }),
        .S({\acc32[14]_i_2_n_0 ,\acc32[14]_i_3_n_0 ,\acc32[14]_i_4_n_0 ,\acc32[14]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[15] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[14]_i_1_n_4 ),
        .Q(acc32_reg[15]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[16] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[16]_i_1_n_7 ),
        .Q(acc32_reg[16]),
        .R(\acc32[31]_i_1_n_0 ));
  CARRY4 \acc32_reg[16]_i_1 
       (.CI(\acc32_reg[14]_i_1_n_0 ),
        .CO({\acc32_reg[16]_i_1_n_0 ,\acc32_reg[16]_i_1_n_1 ,\acc32_reg[16]_i_1_n_2 ,\acc32_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({feedback[15],feedback[15],feedback[15],feedback[15]}),
        .O({\acc32_reg[16]_i_1_n_4 ,\acc32_reg[16]_i_1_n_5 ,\acc32_reg[16]_i_1_n_6 ,\acc32_reg[16]_i_1_n_7 }),
        .S({\acc32[16]_i_2_n_0 ,\acc32[16]_i_3_n_0 ,\acc32[16]_i_4_n_0 ,\acc32[16]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[17] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[16]_i_1_n_6 ),
        .Q(acc32_reg[17]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[18] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[16]_i_1_n_5 ),
        .Q(acc32_reg[18]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[19] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[16]_i_1_n_4 ),
        .Q(acc32_reg[19]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[1] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[3]_i_1_n_6 ),
        .Q(dbg_acc[1]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[20] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[20]_i_1_n_7 ),
        .Q(acc32_reg[20]),
        .R(\acc32[31]_i_1_n_0 ));
  CARRY4 \acc32_reg[20]_i_1 
       (.CI(\acc32_reg[16]_i_1_n_0 ),
        .CO({\acc32_reg[20]_i_1_n_0 ,\acc32_reg[20]_i_1_n_1 ,\acc32_reg[20]_i_1_n_2 ,\acc32_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({feedback[15],feedback[15],feedback[15],feedback[15]}),
        .O({\acc32_reg[20]_i_1_n_4 ,\acc32_reg[20]_i_1_n_5 ,\acc32_reg[20]_i_1_n_6 ,\acc32_reg[20]_i_1_n_7 }),
        .S({\acc32[20]_i_2_n_0 ,\acc32[20]_i_3_n_0 ,\acc32[20]_i_4_n_0 ,\acc32[20]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[21] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[20]_i_1_n_6 ),
        .Q(acc32_reg[21]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[22] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[20]_i_1_n_5 ),
        .Q(acc32_reg[22]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[23] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[20]_i_1_n_4 ),
        .Q(acc32_reg[23]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[24] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[24]_i_1_n_7 ),
        .Q(acc32_reg[24]),
        .R(\acc32[31]_i_1_n_0 ));
  CARRY4 \acc32_reg[24]_i_1 
       (.CI(\acc32_reg[20]_i_1_n_0 ),
        .CO({\acc32_reg[24]_i_1_n_0 ,\acc32_reg[24]_i_1_n_1 ,\acc32_reg[24]_i_1_n_2 ,\acc32_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({feedback[15],feedback[15],feedback[15],feedback[15]}),
        .O({\acc32_reg[24]_i_1_n_4 ,\acc32_reg[24]_i_1_n_5 ,\acc32_reg[24]_i_1_n_6 ,\acc32_reg[24]_i_1_n_7 }),
        .S({\acc32[24]_i_2_n_0 ,\acc32[24]_i_3_n_0 ,\acc32[24]_i_4_n_0 ,\acc32[24]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[25] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[24]_i_1_n_6 ),
        .Q(acc32_reg[25]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[26] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[24]_i_1_n_5 ),
        .Q(acc32_reg[26]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[27] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[24]_i_1_n_4 ),
        .Q(acc32_reg[27]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[28] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[31]_i_3_n_7 ),
        .Q(acc32_reg[28]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[29] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[31]_i_3_n_6 ),
        .Q(acc32_reg[29]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[2] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[3]_i_1_n_5 ),
        .Q(dbg_acc[2]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[30] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[31]_i_3_n_5 ),
        .Q(acc32_reg[30]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[31] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[31]_i_3_n_4 ),
        .Q(dbg_acc[15]),
        .R(\acc32[31]_i_1_n_0 ));
  CARRY4 \acc32_reg[31]_i_3 
       (.CI(\acc32_reg[24]_i_1_n_0 ),
        .CO({\NLW_acc32_reg[31]_i_3_CO_UNCONNECTED [3],\acc32_reg[31]_i_3_n_1 ,\acc32_reg[31]_i_3_n_2 ,\acc32_reg[31]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,feedback[15],feedback[15],feedback[15]}),
        .O({\acc32_reg[31]_i_3_n_4 ,\acc32_reg[31]_i_3_n_5 ,\acc32_reg[31]_i_3_n_6 ,\acc32_reg[31]_i_3_n_7 }),
        .S({\acc32[31]_i_4_n_0 ,\acc32[31]_i_5_n_0 ,\acc32[31]_i_6_n_0 ,\acc32[31]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[3] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[3]_i_1_n_4 ),
        .Q(dbg_acc[3]),
        .R(\acc32[31]_i_1_n_0 ));
  CARRY4 \acc32_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\acc32_reg[3]_i_1_n_0 ,\acc32_reg[3]_i_1_n_1 ,\acc32_reg[3]_i_1_n_2 ,\acc32_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(feedback[3:0]),
        .O({\acc32_reg[3]_i_1_n_4 ,\acc32_reg[3]_i_1_n_5 ,\acc32_reg[3]_i_1_n_6 ,\acc32_reg[3]_i_1_n_7 }),
        .S({\acc32[3]_i_2_n_0 ,\acc32[3]_i_3_n_0 ,\acc32[3]_i_4_n_0 ,\acc32[3]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[4] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[7]_i_1_n_7 ),
        .Q(dbg_acc[4]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[5] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[7]_i_1_n_6 ),
        .Q(dbg_acc[5]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[6] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[7]_i_1_n_5 ),
        .Q(dbg_acc[6]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[7] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[7]_i_1_n_4 ),
        .Q(dbg_acc[7]),
        .R(\acc32[31]_i_1_n_0 ));
  CARRY4 \acc32_reg[7]_i_1 
       (.CI(\acc32_reg[3]_i_1_n_0 ),
        .CO({\acc32_reg[7]_i_1_n_0 ,\acc32_reg[7]_i_1_n_1 ,\acc32_reg[7]_i_1_n_2 ,\acc32_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(feedback[7:4]),
        .O({\acc32_reg[7]_i_1_n_4 ,\acc32_reg[7]_i_1_n_5 ,\acc32_reg[7]_i_1_n_6 ,\acc32_reg[7]_i_1_n_7 }),
        .S({\acc32[7]_i_2_n_0 ,\acc32[7]_i_3_n_0 ,\acc32[7]_i_4_n_0 ,\acc32[7]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[8] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[11]_i_1_n_7 ),
        .Q(dbg_acc[8]),
        .R(\acc32[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc32_reg[9] 
       (.C(clk),
        .CE(en),
        .D(\acc32_reg[11]_i_1_n_6 ),
        .Q(dbg_acc[9]),
        .R(\acc32[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_1
       (.I0(\acc321_inferred__0/i__carry__0_0 [14]),
        .I1(feedback[14]),
        .I2(\acc321_inferred__0/i__carry__0_0 [15]),
        .I3(feedback[15]),
        .O(i__carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_2
       (.I0(\acc321_inferred__0/i__carry__0_0 [12]),
        .I1(feedback[12]),
        .I2(feedback[13]),
        .I3(\acc321_inferred__0/i__carry__0_0 [13]),
        .O(i__carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_3
       (.I0(\acc321_inferred__0/i__carry__0_0 [10]),
        .I1(feedback[10]),
        .I2(feedback[11]),
        .I3(\acc321_inferred__0/i__carry__0_0 [11]),
        .O(i__carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_4
       (.I0(\acc321_inferred__0/i__carry__0_0 [8]),
        .I1(feedback[8]),
        .I2(feedback[9]),
        .I3(\acc321_inferred__0/i__carry__0_0 [9]),
        .O(i__carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5
       (.I0(\acc321_inferred__0/i__carry__0_0 [14]),
        .I1(feedback[14]),
        .I2(feedback[15]),
        .I3(\acc321_inferred__0/i__carry__0_0 [15]),
        .O(i__carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6
       (.I0(\acc321_inferred__0/i__carry__0_0 [12]),
        .I1(feedback[12]),
        .I2(\acc321_inferred__0/i__carry__0_0 [13]),
        .I3(feedback[13]),
        .O(i__carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_7
       (.I0(\acc321_inferred__0/i__carry__0_0 [10]),
        .I1(feedback[10]),
        .I2(\acc321_inferred__0/i__carry__0_0 [11]),
        .I3(feedback[11]),
        .O(i__carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_8
       (.I0(\acc321_inferred__0/i__carry__0_0 [8]),
        .I1(feedback[8]),
        .I2(\acc321_inferred__0/i__carry__0_0 [9]),
        .I3(feedback[9]),
        .O(i__carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_1
       (.I0(\acc321_inferred__0/i__carry__0_0 [6]),
        .I1(feedback[6]),
        .I2(feedback[7]),
        .I3(\acc321_inferred__0/i__carry__0_0 [7]),
        .O(i__carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_2
       (.I0(\acc321_inferred__0/i__carry__0_0 [4]),
        .I1(feedback[4]),
        .I2(feedback[5]),
        .I3(\acc321_inferred__0/i__carry__0_0 [5]),
        .O(i__carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_3
       (.I0(\acc321_inferred__0/i__carry__0_0 [2]),
        .I1(feedback[2]),
        .I2(feedback[3]),
        .I3(\acc321_inferred__0/i__carry__0_0 [3]),
        .O(i__carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_4
       (.I0(\acc321_inferred__0/i__carry__0_0 [0]),
        .I1(feedback[0]),
        .I2(feedback[1]),
        .I3(\acc321_inferred__0/i__carry__0_0 [1]),
        .O(i__carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5
       (.I0(\acc321_inferred__0/i__carry__0_0 [6]),
        .I1(feedback[6]),
        .I2(\acc321_inferred__0/i__carry__0_0 [7]),
        .I3(feedback[7]),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(\acc321_inferred__0/i__carry__0_0 [4]),
        .I1(feedback[4]),
        .I2(\acc321_inferred__0/i__carry__0_0 [5]),
        .I3(feedback[5]),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(\acc321_inferred__0/i__carry__0_0 [2]),
        .I1(feedback[2]),
        .I2(\acc321_inferred__0/i__carry__0_0 [3]),
        .I3(feedback[3]),
        .O(i__carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(\acc321_inferred__0/i__carry__0_0 [0]),
        .I1(feedback[0]),
        .I2(\acc321_inferred__0/i__carry__0_0 [1]),
        .I3(feedback[1]),
        .O(i__carry_i_8_n_0));
  CARRY4 result3_carry
       (.CI(1'b0),
        .CO({result3_carry_n_0,result3_carry_n_1,result3_carry_n_2,result3_carry_n_3}),
        .CYINIT(1'b1),
        .DI(x[3:0]),
        .O({result3_carry_n_4,result3_carry_n_5,result3_carry_n_6,result3_carry_n_7}),
        .S({result3_carry_i_1_n_0,result3_carry_i_2_n_0,result3_carry_i_3_n_0,result3_carry_i_4_n_0}));
  CARRY4 result3_carry__0
       (.CI(result3_carry_n_0),
        .CO({result3_carry__0_n_0,result3_carry__0_n_1,result3_carry__0_n_2,result3_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(x[7:4]),
        .O({result3_carry__0_n_4,result3_carry__0_n_5,result3_carry__0_n_6,result3_carry__0_n_7}),
        .S({result3_carry__0_i_1_n_0,result3_carry__0_i_2_n_0,result3_carry__0_i_3_n_0,result3_carry__0_i_4_n_0}));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__0_i_1
       (.I0(x[7]),
        .I1(result3_carry__0_i_5_n_0),
        .I2(Q[0]),
        .I3(result3_carry__0_i_6_n_0),
        .O(result3_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_10
       (.I0(dbg_acc[15]),
        .I1(acc32_reg[19]),
        .I2(Q[3]),
        .I3(acc32_reg[27]),
        .I4(Q[4]),
        .I5(dbg_acc[11]),
        .O(result3_carry__0_i_10_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_11
       (.I0(dbg_acc[15]),
        .I1(acc32_reg[22]),
        .I2(Q[3]),
        .I3(acc32_reg[30]),
        .I4(Q[4]),
        .I5(dbg_acc[14]),
        .O(result3_carry__0_i_11_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_12
       (.I0(dbg_acc[15]),
        .I1(acc32_reg[20]),
        .I2(Q[3]),
        .I3(acc32_reg[28]),
        .I4(Q[4]),
        .I5(dbg_acc[12]),
        .O(result3_carry__0_i_12_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__0_i_2
       (.I0(x[6]),
        .I1(result3_carry__0_i_7_n_0),
        .I2(Q[0]),
        .I3(result3_carry__0_i_5_n_0),
        .O(result3_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__0_i_3
       (.I0(x[5]),
        .I1(result3_carry__0_i_8_n_0),
        .I2(Q[0]),
        .I3(result3_carry__0_i_7_n_0),
        .O(result3_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__0_i_4
       (.I0(x[4]),
        .I1(result3_carry_i_6_n_0),
        .I2(Q[0]),
        .I3(result3_carry__0_i_8_n_0),
        .O(result3_carry__0_i_4_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_5
       (.I0(result3_carry__0_i_9_n_0),
        .I1(result3_carry_i_11_n_0),
        .I2(Q[1]),
        .I3(result3_carry__0_i_10_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_13_n_0),
        .O(result3_carry__0_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_6
       (.I0(result3_carry__0_i_11_n_0),
        .I1(result3_carry_i_15_n_0),
        .I2(Q[1]),
        .I3(result3_carry__0_i_12_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_17_n_0),
        .O(result3_carry__0_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_7
       (.I0(result3_carry__0_i_12_n_0),
        .I1(result3_carry_i_17_n_0),
        .I2(Q[1]),
        .I3(result3_carry_i_15_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_16_n_0),
        .O(result3_carry__0_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_8
       (.I0(result3_carry__0_i_10_n_0),
        .I1(result3_carry_i_13_n_0),
        .I2(Q[1]),
        .I3(result3_carry_i_11_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_12_n_0),
        .O(result3_carry__0_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__0_i_9
       (.I0(dbg_acc[15]),
        .I1(acc32_reg[21]),
        .I2(Q[3]),
        .I3(acc32_reg[29]),
        .I4(Q[4]),
        .I5(dbg_acc[13]),
        .O(result3_carry__0_i_9_n_0));
  CARRY4 result3_carry__1
       (.CI(result3_carry__0_n_0),
        .CO({result3_carry__1_n_0,result3_carry__1_n_1,result3_carry__1_n_2,result3_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(x[11:8]),
        .O({result3_carry__1_n_4,result3_carry__1_n_5,result3_carry__1_n_6,result3_carry__1_n_7}),
        .S({result3_carry__1_i_1_n_0,result3_carry__1_i_2_n_0,result3_carry__1_i_3_n_0,result3_carry__1_i_4_n_0}));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__1_i_1
       (.I0(x[11]),
        .I1(result3_carry__1_i_5_n_0),
        .I2(Q[0]),
        .I3(result3_carry__1_i_6_n_0),
        .O(result3_carry__1_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__1_i_10
       (.I0(acc32_reg[23]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[15]),
        .O(result3_carry__1_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__1_i_11
       (.I0(acc32_reg[26]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[18]),
        .O(result3_carry__1_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__1_i_12
       (.I0(acc32_reg[24]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[16]),
        .O(result3_carry__1_i_12_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__1_i_2
       (.I0(x[10]),
        .I1(result3_carry__1_i_7_n_0),
        .I2(Q[0]),
        .I3(result3_carry__1_i_5_n_0),
        .O(result3_carry__1_i_2_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__1_i_3
       (.I0(x[9]),
        .I1(result3_carry__1_i_8_n_0),
        .I2(Q[0]),
        .I3(result3_carry__1_i_7_n_0),
        .O(result3_carry__1_i_3_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__1_i_4
       (.I0(x[8]),
        .I1(result3_carry__0_i_6_n_0),
        .I2(Q[0]),
        .I3(result3_carry__1_i_8_n_0),
        .O(result3_carry__1_i_4_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__1_i_5
       (.I0(result3_carry__1_i_9_n_0),
        .I1(result3_carry__0_i_9_n_0),
        .I2(Q[1]),
        .I3(result3_carry__1_i_10_n_0),
        .I4(Q[2]),
        .I5(result3_carry__0_i_10_n_0),
        .O(result3_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__1_i_6
       (.I0(result3_carry__1_i_11_n_0),
        .I1(result3_carry__0_i_11_n_0),
        .I2(Q[1]),
        .I3(result3_carry__1_i_12_n_0),
        .I4(Q[2]),
        .I5(result3_carry__0_i_12_n_0),
        .O(result3_carry__1_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__1_i_7
       (.I0(result3_carry__1_i_12_n_0),
        .I1(result3_carry__0_i_12_n_0),
        .I2(Q[1]),
        .I3(result3_carry__0_i_11_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_15_n_0),
        .O(result3_carry__1_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__1_i_8
       (.I0(result3_carry__1_i_10_n_0),
        .I1(result3_carry__0_i_10_n_0),
        .I2(Q[1]),
        .I3(result3_carry__0_i_9_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_11_n_0),
        .O(result3_carry__1_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__1_i_9
       (.I0(acc32_reg[25]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[17]),
        .O(result3_carry__1_i_9_n_0));
  CARRY4 result3_carry__2
       (.CI(result3_carry__1_n_0),
        .CO({result3_carry__2_n_0,result3_carry__2_n_1,result3_carry__2_n_2,result3_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({u32[15],x[14:12]}),
        .O({p_1_in,result3_carry__2_n_5,result3_carry__2_n_6,result3_carry__2_n_7}),
        .S({result3_carry__2_i_2_n_0,result3_carry__2_i_3_n_0,result3_carry__2_i_4_n_0,result3_carry__2_i_5_n_0}));
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__2_i_1
       (.I0(result3_carry__2_i_6_n_0),
        .I1(Q[0]),
        .I2(result3_carry__2_i_7_n_0),
        .O(u32[15]));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__2_i_10
       (.I0(acc32_reg[30]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[22]),
        .O(result3_carry__2_i_10_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__2_i_11
       (.I0(acc32_reg[28]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[20]),
        .O(result3_carry__2_i_11_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__2_i_12
       (.I0(acc32_reg[29]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[21]),
        .O(result3_carry__2_i_12_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    result3_carry__2_i_13
       (.I0(acc32_reg[27]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[19]),
        .O(result3_carry__2_i_13_n_0));
  LUT4 #(
    .INIT(16'hE21D)) 
    result3_carry__2_i_2
       (.I0(result3_carry__2_i_7_n_0),
        .I1(Q[0]),
        .I2(result3_carry__2_i_6_n_0),
        .I3(x[15]),
        .O(result3_carry__2_i_2_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__2_i_3
       (.I0(x[14]),
        .I1(result3_carry__2_i_8_n_0),
        .I2(Q[0]),
        .I3(result3_carry__2_i_7_n_0),
        .O(result3_carry__2_i_3_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__2_i_4
       (.I0(x[13]),
        .I1(result3_carry__2_i_9_n_0),
        .I2(Q[0]),
        .I3(result3_carry__2_i_8_n_0),
        .O(result3_carry__2_i_4_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry__2_i_5
       (.I0(x[12]),
        .I1(result3_carry__1_i_6_n_0),
        .I2(Q[0]),
        .I3(result3_carry__2_i_9_n_0),
        .O(result3_carry__2_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_6
       (.I0(result3_carry__2_i_10_n_0),
        .I1(result3_carry__1_i_11_n_0),
        .I2(Q[1]),
        .I3(result3_carry__2_i_11_n_0),
        .I4(Q[2]),
        .I5(result3_carry__1_i_12_n_0),
        .O(result3_carry__2_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_7
       (.I0(result3_carry__2_i_12_n_0),
        .I1(result3_carry__1_i_9_n_0),
        .I2(Q[1]),
        .I3(result3_carry__2_i_13_n_0),
        .I4(Q[2]),
        .I5(result3_carry__1_i_10_n_0),
        .O(result3_carry__2_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_8
       (.I0(result3_carry__2_i_11_n_0),
        .I1(result3_carry__1_i_12_n_0),
        .I2(Q[1]),
        .I3(result3_carry__1_i_11_n_0),
        .I4(Q[2]),
        .I5(result3_carry__0_i_11_n_0),
        .O(result3_carry__2_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__2_i_9
       (.I0(result3_carry__2_i_13_n_0),
        .I1(result3_carry__1_i_10_n_0),
        .I2(Q[1]),
        .I3(result3_carry__1_i_9_n_0),
        .I4(Q[2]),
        .I5(result3_carry__0_i_9_n_0),
        .O(result3_carry__2_i_9_n_0));
  CARRY4 result3_carry__3
       (.CI(result3_carry__2_n_0),
        .CO({result3_carry__3_n_0,result3_carry__3_n_1,result3_carry__3_n_2,result3_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({result3_carry__3_i_1_n_0,result3_carry__3_i_2_n_0,result3_carry__3_i_3_n_0,result3_carry__3_i_4_n_0}),
        .O(NLW_result3_carry__3_O_UNCONNECTED[3:0]),
        .S({result3_carry__3_i_5_n_0,result3_carry__3_i_6_n_0,result3_carry__3_i_7_n_0,result3_carry__3_i_8_n_0}));
  LUT3 #(
    .INIT(8'h1D)) 
    result3_carry__3_i_1
       (.I0(result3_carry__3_i_9_n_0),
        .I1(Q[0]),
        .I2(result3_carry__3_i_10_n_0),
        .O(result3_carry__3_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_10
       (.I0(result3_carry__3_i_21_n_0),
        .I1(result3_carry__2_i_12_n_0),
        .I2(Q[1]),
        .I3(result3_carry__3_i_22_n_0),
        .I4(Q[2]),
        .I5(result3_carry__2_i_13_n_0),
        .O(result3_carry__3_i_10_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_11
       (.I0(result3_carry__3_i_22_n_0),
        .I1(result3_carry__2_i_13_n_0),
        .I2(Q[1]),
        .I3(result3_carry__2_i_12_n_0),
        .I4(Q[2]),
        .I5(result3_carry__1_i_9_n_0),
        .O(result3_carry__3_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__3_i_12
       (.I0(result3_carry__2_i_10_n_0),
        .I1(Q[2]),
        .I2(result3_carry__1_i_11_n_0),
        .O(result3_carry__3_i_12_n_0));
  LUT6 #(
    .INIT(64'hCDC8FFFFCDC80000)) 
    result3_carry__3_i_13
       (.I0(Q[3]),
        .I1(dbg_acc[15]),
        .I2(Q[4]),
        .I3(acc32_reg[24]),
        .I4(Q[2]),
        .I5(result3_carry__2_i_11_n_0),
        .O(result3_carry__3_i_13_n_0));
  LUT6 #(
    .INIT(64'hCDC8FFFFCDC80000)) 
    result3_carry__3_i_14
       (.I0(Q[3]),
        .I1(dbg_acc[15]),
        .I2(Q[4]),
        .I3(acc32_reg[26]),
        .I4(Q[2]),
        .I5(result3_carry__2_i_10_n_0),
        .O(result3_carry__3_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__3_i_15
       (.I0(result3_carry__2_i_12_n_0),
        .I1(Q[2]),
        .I2(result3_carry__1_i_9_n_0),
        .O(result3_carry__3_i_15_n_0));
  LUT6 #(
    .INIT(64'hCDC8FFFFCDC80000)) 
    result3_carry__3_i_16
       (.I0(Q[3]),
        .I1(dbg_acc[15]),
        .I2(Q[4]),
        .I3(acc32_reg[23]),
        .I4(Q[2]),
        .I5(result3_carry__2_i_13_n_0),
        .O(result3_carry__3_i_16_n_0));
  LUT6 #(
    .INIT(64'hCDC8FFFFCDC80000)) 
    result3_carry__3_i_17
       (.I0(Q[3]),
        .I1(dbg_acc[15]),
        .I2(Q[4]),
        .I3(acc32_reg[25]),
        .I4(Q[2]),
        .I5(result3_carry__2_i_12_n_0),
        .O(result3_carry__3_i_17_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__3_i_18
       (.I0(result3_carry__2_i_11_n_0),
        .I1(Q[2]),
        .I2(result3_carry__1_i_12_n_0),
        .O(result3_carry__3_i_18_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__3_i_19
       (.I0(result3_carry__2_i_13_n_0),
        .I1(Q[2]),
        .I2(result3_carry__1_i_10_n_0),
        .O(result3_carry__3_i_19_n_0));
  LUT3 #(
    .INIT(8'h1D)) 
    result3_carry__3_i_2
       (.I0(result3_carry__3_i_11_n_0),
        .I1(Q[0]),
        .I2(result3_carry__3_i_9_n_0),
        .O(result3_carry__3_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    result3_carry__3_i_20
       (.I0(Q[3]),
        .I1(dbg_acc[15]),
        .I2(Q[4]),
        .I3(acc32_reg[24]),
        .O(result3_carry__3_i_20_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    result3_carry__3_i_21
       (.I0(Q[3]),
        .I1(dbg_acc[15]),
        .I2(Q[4]),
        .I3(acc32_reg[25]),
        .O(result3_carry__3_i_21_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    result3_carry__3_i_22
       (.I0(Q[3]),
        .I1(dbg_acc[15]),
        .I2(Q[4]),
        .I3(acc32_reg[23]),
        .O(result3_carry__3_i_22_n_0));
  LUT3 #(
    .INIT(8'h1D)) 
    result3_carry__3_i_3
       (.I0(result3_carry__2_i_6_n_0),
        .I1(Q[0]),
        .I2(result3_carry__3_i_11_n_0),
        .O(result3_carry__3_i_3_n_0));
  LUT3 #(
    .INIT(8'h1D)) 
    result3_carry__3_i_4
       (.I0(result3_carry__2_i_7_n_0),
        .I1(Q[0]),
        .I2(result3_carry__2_i_6_n_0),
        .O(result3_carry__3_i_4_n_0));
  LUT6 #(
    .INIT(64'hCCC3C9393C33C939)) 
    result3_carry__3_i_5
       (.I0(result3_carry__3_i_12_n_0),
        .I1(result3_carry__3_i_10_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_13_n_0),
        .I4(Q[1]),
        .I5(result3_carry__3_i_14_n_0),
        .O(result3_carry__3_i_5_n_0));
  LUT6 #(
    .INIT(64'hCCC3C9393C33C939)) 
    result3_carry__3_i_6
       (.I0(result3_carry__3_i_15_n_0),
        .I1(result3_carry__3_i_9_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_16_n_0),
        .I4(Q[1]),
        .I5(result3_carry__3_i_17_n_0),
        .O(result3_carry__3_i_6_n_0));
  LUT6 #(
    .INIT(64'hCCC3C9393C33C939)) 
    result3_carry__3_i_7
       (.I0(result3_carry__3_i_18_n_0),
        .I1(result3_carry__3_i_11_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_12_n_0),
        .I4(Q[1]),
        .I5(result3_carry__3_i_13_n_0),
        .O(result3_carry__3_i_7_n_0));
  LUT6 #(
    .INIT(64'hCCC3C9393C33C939)) 
    result3_carry__3_i_8
       (.I0(result3_carry__3_i_19_n_0),
        .I1(result3_carry__2_i_6_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_15_n_0),
        .I4(Q[1]),
        .I5(result3_carry__3_i_16_n_0),
        .O(result3_carry__3_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__3_i_9
       (.I0(result3_carry__3_i_20_n_0),
        .I1(result3_carry__2_i_11_n_0),
        .I2(Q[1]),
        .I3(result3_carry__2_i_10_n_0),
        .I4(Q[2]),
        .I5(result3_carry__1_i_11_n_0),
        .O(result3_carry__3_i_9_n_0));
  CARRY4 result3_carry__4
       (.CI(result3_carry__3_n_0),
        .CO({result3_carry__4_n_0,result3_carry__4_n_1,result3_carry__4_n_2,result3_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({result3_carry__4_i_1_n_0,result3_carry__4_i_2_n_0,result3_carry__4_i_3_n_0,result3_carry__4_i_4_n_0}),
        .O(NLW_result3_carry__4_O_UNCONNECTED[3:0]),
        .S({result3_carry__4_i_5_n_0,result3_carry__4_i_6_n_0,result3_carry__4_i_7_n_0,result3_carry__4_i_8_n_0}));
  LUT3 #(
    .INIT(8'h1D)) 
    result3_carry__4_i_1
       (.I0(result3_carry__4_i_9_n_0),
        .I1(Q[0]),
        .I2(result3_carry__4_i_10_n_0),
        .O(result3_carry__4_i_1_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry__4_i_10
       (.I0(result3_carry__4_i_16_n_0),
        .I1(Q[1]),
        .I2(result3_carry__4_i_15_n_0),
        .O(result3_carry__4_i_10_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    result3_carry__4_i_11
       (.I0(result3_carry__4_i_15_n_0),
        .I1(Q[1]),
        .I2(result3_carry__3_i_21_n_0),
        .I3(Q[2]),
        .I4(result3_carry__2_i_12_n_0),
        .O(result3_carry__4_i_11_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__4_i_12
       (.I0(result3_carry__4_i_17_n_0),
        .I1(result3_carry__2_i_10_n_0),
        .I2(Q[1]),
        .I3(result3_carry__3_i_20_n_0),
        .I4(Q[2]),
        .I5(result3_carry__2_i_11_n_0),
        .O(result3_carry__4_i_12_n_0));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    result3_carry__4_i_13
       (.I0(acc32_reg[28]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(dbg_acc[15]),
        .I4(Q[4]),
        .I5(acc32_reg[24]),
        .O(result3_carry__4_i_13_n_0));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    result3_carry__4_i_14
       (.I0(acc32_reg[30]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(dbg_acc[15]),
        .I4(Q[4]),
        .I5(acc32_reg[26]),
        .O(result3_carry__4_i_14_n_0));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    result3_carry__4_i_15
       (.I0(acc32_reg[27]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(dbg_acc[15]),
        .I4(Q[4]),
        .I5(acc32_reg[23]),
        .O(result3_carry__4_i_15_n_0));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    result3_carry__4_i_16
       (.I0(acc32_reg[29]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(dbg_acc[15]),
        .I4(Q[4]),
        .I5(acc32_reg[25]),
        .O(result3_carry__4_i_16_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    result3_carry__4_i_17
       (.I0(Q[3]),
        .I1(dbg_acc[15]),
        .I2(Q[4]),
        .I3(acc32_reg[26]),
        .O(result3_carry__4_i_17_n_0));
  LUT3 #(
    .INIT(8'h1D)) 
    result3_carry__4_i_2
       (.I0(result3_carry__4_i_11_n_0),
        .I1(Q[0]),
        .I2(result3_carry__4_i_9_n_0),
        .O(result3_carry__4_i_2_n_0));
  LUT3 #(
    .INIT(8'h1D)) 
    result3_carry__4_i_3
       (.I0(result3_carry__4_i_12_n_0),
        .I1(Q[0]),
        .I2(result3_carry__4_i_11_n_0),
        .O(result3_carry__4_i_3_n_0));
  LUT3 #(
    .INIT(8'h1D)) 
    result3_carry__4_i_4
       (.I0(result3_carry__3_i_10_n_0),
        .I1(Q[0]),
        .I2(result3_carry__4_i_12_n_0),
        .O(result3_carry__4_i_4_n_0));
  LUT6 #(
    .INIT(64'hCCC3C9393C33C939)) 
    result3_carry__4_i_5
       (.I0(result3_carry__3_i_14_n_0),
        .I1(result3_carry__4_i_10_n_0),
        .I2(Q[0]),
        .I3(result3_carry__4_i_13_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_14_n_0),
        .O(result3_carry__4_i_5_n_0));
  LUT6 #(
    .INIT(64'hCCC3C9393C33C939)) 
    result3_carry__4_i_6
       (.I0(result3_carry__3_i_17_n_0),
        .I1(result3_carry__4_i_9_n_0),
        .I2(Q[0]),
        .I3(result3_carry__4_i_15_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_16_n_0),
        .O(result3_carry__4_i_6_n_0));
  LUT6 #(
    .INIT(64'hCCC3C9393C33C939)) 
    result3_carry__4_i_7
       (.I0(result3_carry__3_i_13_n_0),
        .I1(result3_carry__4_i_11_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_14_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_13_n_0),
        .O(result3_carry__4_i_7_n_0));
  LUT6 #(
    .INIT(64'hCCC3C9393C33C939)) 
    result3_carry__4_i_8
       (.I0(result3_carry__3_i_16_n_0),
        .I1(result3_carry__4_i_12_n_0),
        .I2(Q[0]),
        .I3(result3_carry__3_i_17_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_15_n_0),
        .O(result3_carry__4_i_8_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    result3_carry__4_i_9
       (.I0(result3_carry__4_i_13_n_0),
        .I1(Q[1]),
        .I2(result3_carry__4_i_17_n_0),
        .I3(Q[2]),
        .I4(result3_carry__2_i_10_n_0),
        .O(result3_carry__4_i_9_n_0));
  CARRY4 result3_carry__5
       (.CI(result3_carry__4_n_0),
        .CO({result3_carry__5_n_0,result3_carry__5_n_1,result3_carry__5_n_2,result3_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({result3_carry__5_i_1_n_0,result3_carry__5_i_2_n_0,result3_carry__5_i_3_n_0,result3_carry__5_i_4_n_0}),
        .O(NLW_result3_carry__5_O_UNCONNECTED[3:0]),
        .S({result3_carry__5_i_5_n_0,result3_carry__5_i_6_n_0,result3_carry__5_i_7_n_0,result3_carry__5_i_8_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__5_i_1
       (.I0(u32[26]),
        .O(result3_carry__5_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__5_i_10
       (.I0(result3_carry__6_i_11_n_0),
        .I1(result3_carry__4_i_14_n_0),
        .I2(Q[0]),
        .I3(result3_carry__5_i_15_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_16_n_0),
        .O(u32[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__5_i_11
       (.I0(result3_carry__5_i_15_n_0),
        .I1(result3_carry__4_i_16_n_0),
        .I2(Q[0]),
        .I3(result3_carry__4_i_14_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_13_n_0),
        .O(u32[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__5_i_12
       (.I0(result3_carry__4_i_14_n_0),
        .I1(result3_carry__4_i_13_n_0),
        .I2(Q[0]),
        .I3(result3_carry__4_i_16_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_15_n_0),
        .O(u32[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__5_i_13
       (.I0(result3_carry__6_i_10_n_0),
        .I1(result3_carry__6_i_11_n_0),
        .I2(Q[0]),
        .I3(result3_carry__5_i_14_n_0),
        .I4(Q[1]),
        .I5(result3_carry__5_i_15_n_0),
        .O(u32[27]));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    result3_carry__5_i_14
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[29]),
        .O(result3_carry__5_i_14_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    result3_carry__5_i_15
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[27]),
        .O(result3_carry__5_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__5_i_2
       (.I0(u32[25]),
        .O(result3_carry__5_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__5_i_3
       (.I0(u32[24]),
        .O(result3_carry__5_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__5_i_4
       (.I0(u32[23]),
        .O(result3_carry__5_i_4_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__5_i_5
       (.I0(u32[26]),
        .I1(u32[27]),
        .O(result3_carry__5_i_5_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__5_i_6
       (.I0(u32[25]),
        .I1(u32[26]),
        .O(result3_carry__5_i_6_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__5_i_7
       (.I0(u32[24]),
        .I1(u32[25]),
        .O(result3_carry__5_i_7_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result3_carry__5_i_8
       (.I0(u32[23]),
        .I1(u32[24]),
        .O(result3_carry__5_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry__5_i_9
       (.I0(result3_carry__5_i_14_n_0),
        .I1(result3_carry__5_i_15_n_0),
        .I2(Q[0]),
        .I3(result3_carry__6_i_11_n_0),
        .I4(Q[1]),
        .I5(result3_carry__4_i_14_n_0),
        .O(u32[26]));
  CARRY4 result3_carry__6
       (.CI(result3_carry__5_n_0),
        .CO({NLW_result3_carry__6_CO_UNCONNECTED[3],result3_carry__6_n_1,result3_carry__6_n_2,result3_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,result3_carry__6_i_1_n_0,result3_carry__6_i_2_n_0,result3_carry__6_i_3_n_0}),
        .O({p_0_in,NLW_result3_carry__6_O_UNCONNECTED[2:0]}),
        .S({result3_carry__6_i_4_n_0,result3_carry__6_i_5_n_0,result3_carry__6_i_6_n_0,result3_carry__6_i_7_n_0}));
  LUT3 #(
    .INIT(8'h47)) 
    result3_carry__6_i_1
       (.I0(result3_carry__6_i_8_n_0),
        .I1(Q[0]),
        .I2(result3_carry__6_i_9_n_0),
        .O(result3_carry__6_i_1_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    result3_carry__6_i_10
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[30]),
        .O(result3_carry__6_i_10_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    result3_carry__6_i_11
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(dbg_acc[15]),
        .I3(Q[4]),
        .I4(acc32_reg[28]),
        .O(result3_carry__6_i_11_n_0));
  LUT5 #(
    .INIT(32'h47444777)) 
    result3_carry__6_i_2
       (.I0(result3_carry__6_i_9_n_0),
        .I1(Q[0]),
        .I2(result3_carry__6_i_10_n_0),
        .I3(Q[1]),
        .I4(result3_carry__6_i_11_n_0),
        .O(result3_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    result3_carry__6_i_3
       (.I0(u32[27]),
        .O(result3_carry__6_i_3_n_0));
  LUT3 #(
    .INIT(8'hED)) 
    result3_carry__6_i_4
       (.I0(result3_carry__6_i_8_n_0),
        .I1(Q[0]),
        .I2(dbg_acc[15]),
        .O(result3_carry__6_i_4_n_0));
  LUT4 #(
    .INIT(16'hC939)) 
    result3_carry__6_i_5
       (.I0(result3_carry__6_i_9_n_0),
        .I1(result3_carry__6_i_8_n_0),
        .I2(Q[0]),
        .I3(dbg_acc[15]),
        .O(result3_carry__6_i_5_n_0));
  LUT6 #(
    .INIT(64'hB487B4B4B4878787)) 
    result3_carry__6_i_6
       (.I0(result3_carry__6_i_8_n_0),
        .I1(Q[0]),
        .I2(result3_carry__6_i_9_n_0),
        .I3(result3_carry__6_i_10_n_0),
        .I4(Q[1]),
        .I5(result3_carry__6_i_11_n_0),
        .O(result3_carry__6_i_6_n_0));
  LUT6 #(
    .INIT(64'hB8BBB88847444777)) 
    result3_carry__6_i_7
       (.I0(result3_carry__6_i_9_n_0),
        .I1(Q[0]),
        .I2(result3_carry__6_i_10_n_0),
        .I3(Q[1]),
        .I4(result3_carry__6_i_11_n_0),
        .I5(u32[27]),
        .O(result3_carry__6_i_7_n_0));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    result3_carry__6_i_8
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(dbg_acc[15]),
        .I4(Q[4]),
        .I5(acc32_reg[30]),
        .O(result3_carry__6_i_8_n_0));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    result3_carry__6_i_9
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(dbg_acc[15]),
        .I4(Q[4]),
        .I5(acc32_reg[29]),
        .O(result3_carry__6_i_9_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry_i_1
       (.I0(x[3]),
        .I1(result3_carry_i_5_n_0),
        .I2(Q[0]),
        .I3(result3_carry_i_6_n_0),
        .O(result3_carry_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry_i_10
       (.I0(result3_carry_i_16_n_0),
        .I1(Q[2]),
        .I2(result3_carry_i_19_n_0),
        .O(result3_carry_i_10_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_11
       (.I0(dbg_acc[15]),
        .I1(acc32_reg[17]),
        .I2(Q[3]),
        .I3(acc32_reg[25]),
        .I4(Q[4]),
        .I5(dbg_acc[9]),
        .O(result3_carry_i_11_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_12
       (.I0(acc32_reg[29]),
        .I1(dbg_acc[13]),
        .I2(Q[3]),
        .I3(acc32_reg[21]),
        .I4(Q[4]),
        .I5(dbg_acc[5]),
        .O(result3_carry_i_12_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_13
       (.I0(dbg_acc[15]),
        .I1(acc32_reg[15]),
        .I2(Q[3]),
        .I3(acc32_reg[23]),
        .I4(Q[4]),
        .I5(dbg_acc[7]),
        .O(result3_carry_i_13_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_14
       (.I0(acc32_reg[27]),
        .I1(dbg_acc[11]),
        .I2(Q[3]),
        .I3(acc32_reg[19]),
        .I4(Q[4]),
        .I5(dbg_acc[3]),
        .O(result3_carry_i_14_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_15
       (.I0(dbg_acc[15]),
        .I1(acc32_reg[18]),
        .I2(Q[3]),
        .I3(acc32_reg[26]),
        .I4(Q[4]),
        .I5(dbg_acc[10]),
        .O(result3_carry_i_15_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_16
       (.I0(acc32_reg[30]),
        .I1(dbg_acc[14]),
        .I2(Q[3]),
        .I3(acc32_reg[22]),
        .I4(Q[4]),
        .I5(dbg_acc[6]),
        .O(result3_carry_i_16_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_17
       (.I0(dbg_acc[15]),
        .I1(acc32_reg[16]),
        .I2(Q[3]),
        .I3(acc32_reg[24]),
        .I4(Q[4]),
        .I5(dbg_acc[8]),
        .O(result3_carry_i_17_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_18
       (.I0(acc32_reg[28]),
        .I1(dbg_acc[12]),
        .I2(Q[3]),
        .I3(acc32_reg[20]),
        .I4(Q[4]),
        .I5(dbg_acc[4]),
        .O(result3_carry_i_18_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_19
       (.I0(acc32_reg[26]),
        .I1(dbg_acc[10]),
        .I2(Q[3]),
        .I3(acc32_reg[18]),
        .I4(Q[4]),
        .I5(dbg_acc[2]),
        .O(result3_carry_i_19_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry_i_2
       (.I0(x[2]),
        .I1(result3_carry_i_7_n_0),
        .I2(Q[0]),
        .I3(result3_carry_i_5_n_0),
        .O(result3_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_20
       (.I0(acc32_reg[25]),
        .I1(dbg_acc[9]),
        .I2(Q[3]),
        .I3(acc32_reg[17]),
        .I4(Q[4]),
        .I5(dbg_acc[1]),
        .O(result3_carry_i_20_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_21
       (.I0(acc32_reg[24]),
        .I1(dbg_acc[8]),
        .I2(Q[3]),
        .I3(acc32_reg[16]),
        .I4(Q[4]),
        .I5(dbg_acc[0]),
        .O(result3_carry_i_21_n_0));
  LUT4 #(
    .INIT(16'hA959)) 
    result3_carry_i_3
       (.I0(x[1]),
        .I1(result3_carry_i_8_n_0),
        .I2(Q[0]),
        .I3(result3_carry_i_7_n_0),
        .O(result3_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'hAAAAA9595555A959)) 
    result3_carry_i_4
       (.I0(x[0]),
        .I1(result3_carry_i_9_n_0),
        .I2(Q[1]),
        .I3(result3_carry_i_10_n_0),
        .I4(Q[0]),
        .I5(result3_carry_i_8_n_0),
        .O(result3_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_5
       (.I0(result3_carry_i_11_n_0),
        .I1(result3_carry_i_12_n_0),
        .I2(Q[1]),
        .I3(result3_carry_i_13_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_14_n_0),
        .O(result3_carry_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_6
       (.I0(result3_carry_i_15_n_0),
        .I1(result3_carry_i_16_n_0),
        .I2(Q[1]),
        .I3(result3_carry_i_17_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_18_n_0),
        .O(result3_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_7
       (.I0(result3_carry_i_17_n_0),
        .I1(result3_carry_i_18_n_0),
        .I2(Q[1]),
        .I3(result3_carry_i_16_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_19_n_0),
        .O(result3_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    result3_carry_i_8
       (.I0(result3_carry_i_13_n_0),
        .I1(result3_carry_i_14_n_0),
        .I2(Q[1]),
        .I3(result3_carry_i_12_n_0),
        .I4(Q[2]),
        .I5(result3_carry_i_20_n_0),
        .O(result3_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    result3_carry_i_9
       (.I0(result3_carry_i_18_n_0),
        .I1(Q[2]),
        .I2(result3_carry_i_21_n_0),
        .O(result3_carry_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[0]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry_n_7),
        .O(p_2_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[10]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__1_n_5),
        .O(p_2_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[11]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__1_n_4),
        .O(p_2_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[12]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__2_n_7),
        .O(p_2_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[13]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__2_n_6),
        .O(p_2_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[14]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__2_n_5),
        .O(p_2_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[1]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry_n_6),
        .O(p_2_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[2]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry_n_5),
        .O(p_2_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT1 #(
    .INIT(2'h2)) 
    \result[31]_C_i_1 
       (.I0(p_0_in),
        .O(p_2_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[3]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry_n_4),
        .O(p_2_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[4]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__0_n_7),
        .O(p_2_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[5]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__0_n_6),
        .O(p_2_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[6]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__0_n_5),
        .O(p_2_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[7]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__0_n_4),
        .O(p_2_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[8]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__1_n_7),
        .O(p_2_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    \result[9]_C_i_1 
       (.I0(p_0_in),
        .I1(p_1_in),
        .I2(result3_carry__1_n_6),
        .O(p_2_in[9]));
  FDCE \result_reg[0]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[0]_LDC_i_2_n_0 ),
        .D(p_2_in[0]),
        .Q(\result_reg[0]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[0]_LDC 
       (.CLR(\result_reg[0]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[0]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[0]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[0]_LDC_i_1 
       (.I0(x[0]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[0]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[0]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[0]),
        .O(\result_reg[0]_LDC_i_2_n_0 ));
  FDPE \result_reg[0]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[0]),
        .PRE(\result_reg[0]_LDC_i_1_n_0 ),
        .Q(\result_reg[0]_P_n_0 ));
  FDCE \result_reg[10]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[10]_LDC_i_2_n_0 ),
        .D(p_2_in[10]),
        .Q(\result_reg[10]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[10]_LDC 
       (.CLR(\result_reg[10]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[10]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[10]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[10]_LDC_i_1 
       (.I0(x[10]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[10]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[10]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[10]),
        .O(\result_reg[10]_LDC_i_2_n_0 ));
  FDPE \result_reg[10]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[10]),
        .PRE(\result_reg[10]_LDC_i_1_n_0 ),
        .Q(\result_reg[10]_P_n_0 ));
  FDCE \result_reg[11]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[11]_LDC_i_2_n_0 ),
        .D(p_2_in[11]),
        .Q(\result_reg[11]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[11]_LDC 
       (.CLR(\result_reg[11]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[11]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[11]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[11]_LDC_i_1 
       (.I0(x[11]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[11]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[11]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[11]),
        .O(\result_reg[11]_LDC_i_2_n_0 ));
  FDPE \result_reg[11]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[11]),
        .PRE(\result_reg[11]_LDC_i_1_n_0 ),
        .Q(\result_reg[11]_P_n_0 ));
  FDCE \result_reg[12]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[12]_LDC_i_2_n_0 ),
        .D(p_2_in[12]),
        .Q(\result_reg[12]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[12]_LDC 
       (.CLR(\result_reg[12]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[12]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[12]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[12]_LDC_i_1 
       (.I0(x[12]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[12]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[12]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[12]),
        .O(\result_reg[12]_LDC_i_2_n_0 ));
  FDPE \result_reg[12]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[12]),
        .PRE(\result_reg[12]_LDC_i_1_n_0 ),
        .Q(\result_reg[12]_P_n_0 ));
  FDCE \result_reg[13]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[13]_LDC_i_2_n_0 ),
        .D(p_2_in[13]),
        .Q(\result_reg[13]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[13]_LDC 
       (.CLR(\result_reg[13]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[13]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[13]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[13]_LDC_i_1 
       (.I0(x[13]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[13]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[13]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[13]),
        .O(\result_reg[13]_LDC_i_2_n_0 ));
  FDPE \result_reg[13]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[13]),
        .PRE(\result_reg[13]_LDC_i_1_n_0 ),
        .Q(\result_reg[13]_P_n_0 ));
  FDCE \result_reg[14]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[14]_LDC_i_2_n_0 ),
        .D(p_2_in[14]),
        .Q(\result_reg[14]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[14]_LDC 
       (.CLR(\result_reg[14]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[14]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[14]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[14]_LDC_i_1 
       (.I0(x[14]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[14]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[14]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[14]),
        .O(\result_reg[14]_LDC_i_2_n_0 ));
  FDPE \result_reg[14]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[14]),
        .PRE(\result_reg[14]_LDC_i_1_n_0 ),
        .Q(\result_reg[14]_P_n_0 ));
  FDCE \result_reg[1]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[1]_LDC_i_2_n_0 ),
        .D(p_2_in[1]),
        .Q(\result_reg[1]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[1]_LDC 
       (.CLR(\result_reg[1]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[1]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[1]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[1]_LDC_i_1 
       (.I0(x[1]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[1]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[1]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[1]),
        .O(\result_reg[1]_LDC_i_2_n_0 ));
  FDPE \result_reg[1]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[1]),
        .PRE(\result_reg[1]_LDC_i_1_n_0 ),
        .Q(\result_reg[1]_P_n_0 ));
  FDCE \result_reg[2]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[2]_LDC_i_2_n_0 ),
        .D(p_2_in[2]),
        .Q(\result_reg[2]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[2]_LDC 
       (.CLR(\result_reg[2]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[2]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[2]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[2]_LDC_i_1 
       (.I0(x[2]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[2]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[2]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[2]),
        .O(\result_reg[2]_LDC_i_2_n_0 ));
  FDPE \result_reg[2]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[2]),
        .PRE(\result_reg[2]_LDC_i_1_n_0 ),
        .Q(\result_reg[2]_P_n_0 ));
  FDCE \result_reg[31]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[31]_LDC_i_2_n_0 ),
        .D(p_2_in[31]),
        .Q(\result_reg[31]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[31]_LDC 
       (.CLR(\result_reg[31]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[31]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[31]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[31]_LDC_i_1 
       (.I0(x[15]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[31]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[31]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[15]),
        .O(\result_reg[31]_LDC_i_2_n_0 ));
  FDPE \result_reg[31]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[31]),
        .PRE(\result_reg[31]_LDC_i_1_n_0 ),
        .Q(\result_reg[31]_P_n_0 ));
  FDCE \result_reg[3]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[3]_LDC_i_2_n_0 ),
        .D(p_2_in[3]),
        .Q(\result_reg[3]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[3]_LDC 
       (.CLR(\result_reg[3]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[3]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[3]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[3]_LDC_i_1 
       (.I0(x[3]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[3]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[3]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[3]),
        .O(\result_reg[3]_LDC_i_2_n_0 ));
  FDPE \result_reg[3]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[3]),
        .PRE(\result_reg[3]_LDC_i_1_n_0 ),
        .Q(\result_reg[3]_P_n_0 ));
  FDCE \result_reg[4]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[4]_LDC_i_2_n_0 ),
        .D(p_2_in[4]),
        .Q(\result_reg[4]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[4]_LDC 
       (.CLR(\result_reg[4]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[4]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[4]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[4]_LDC_i_1 
       (.I0(x[4]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[4]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[4]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[4]),
        .O(\result_reg[4]_LDC_i_2_n_0 ));
  FDPE \result_reg[4]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[4]),
        .PRE(\result_reg[4]_LDC_i_1_n_0 ),
        .Q(\result_reg[4]_P_n_0 ));
  FDCE \result_reg[5]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[5]_LDC_i_2_n_0 ),
        .D(p_2_in[5]),
        .Q(\result_reg[5]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[5]_LDC 
       (.CLR(\result_reg[5]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[5]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[5]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[5]_LDC_i_1 
       (.I0(x[5]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[5]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[5]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[5]),
        .O(\result_reg[5]_LDC_i_2_n_0 ));
  FDPE \result_reg[5]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[5]),
        .PRE(\result_reg[5]_LDC_i_1_n_0 ),
        .Q(\result_reg[5]_P_n_0 ));
  FDCE \result_reg[6]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[6]_LDC_i_2_n_0 ),
        .D(p_2_in[6]),
        .Q(\result_reg[6]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[6]_LDC 
       (.CLR(\result_reg[6]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[6]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[6]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[6]_LDC_i_1 
       (.I0(x[6]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[6]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[6]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[6]),
        .O(\result_reg[6]_LDC_i_2_n_0 ));
  FDPE \result_reg[6]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[6]),
        .PRE(\result_reg[6]_LDC_i_1_n_0 ),
        .Q(\result_reg[6]_P_n_0 ));
  FDCE \result_reg[7]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[7]_LDC_i_2_n_0 ),
        .D(p_2_in[7]),
        .Q(\result_reg[7]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[7]_LDC 
       (.CLR(\result_reg[7]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[7]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[7]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[7]_LDC_i_1 
       (.I0(x[7]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[7]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[7]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[7]),
        .O(\result_reg[7]_LDC_i_2_n_0 ));
  FDPE \result_reg[7]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[7]),
        .PRE(\result_reg[7]_LDC_i_1_n_0 ),
        .Q(\result_reg[7]_P_n_0 ));
  FDCE \result_reg[8]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[8]_LDC_i_2_n_0 ),
        .D(p_2_in[8]),
        .Q(\result_reg[8]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[8]_LDC 
       (.CLR(\result_reg[8]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[8]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[8]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[8]_LDC_i_1 
       (.I0(x[8]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[8]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[8]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[8]),
        .O(\result_reg[8]_LDC_i_2_n_0 ));
  FDPE \result_reg[8]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[8]),
        .PRE(\result_reg[8]_LDC_i_1_n_0 ),
        .Q(\result_reg[8]_P_n_0 ));
  FDCE \result_reg[9]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(\result_reg[9]_LDC_i_2_n_0 ),
        .D(p_2_in[9]),
        .Q(\result_reg[9]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \result_reg[9]_LDC 
       (.CLR(\result_reg[9]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\result_reg[9]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\result_reg[9]_LDC_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \result_reg[9]_LDC_i_1 
       (.I0(x[9]),
        .I1(Q[5]),
        .I2(resetn),
        .O(\result_reg[9]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \result_reg[9]_LDC_i_2 
       (.I0(Q[5]),
        .I1(resetn),
        .I2(x[9]),
        .O(\result_reg[9]_LDC_i_2_n_0 ));
  FDPE \result_reg[9]_P 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_in[9]),
        .PRE(\result_reg[9]_LDC_i_1_n_0 ),
        .Q(\result_reg[9]_P_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[0]_i_1 
       (.I0(\result_reg[0]_P_n_0 ),
        .I1(\result_reg[0]_LDC_n_0 ),
        .I2(\result_reg[0]_C_n_0 ),
        .O(RESIZE[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[10]_i_1 
       (.I0(\result_reg[10]_P_n_0 ),
        .I1(\result_reg[10]_LDC_n_0 ),
        .I2(\result_reg[10]_C_n_0 ),
        .O(RESIZE[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[11]_i_1 
       (.I0(\result_reg[11]_P_n_0 ),
        .I1(\result_reg[11]_LDC_n_0 ),
        .I2(\result_reg[11]_C_n_0 ),
        .O(RESIZE[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[12]_i_1 
       (.I0(\result_reg[12]_P_n_0 ),
        .I1(\result_reg[12]_LDC_n_0 ),
        .I2(\result_reg[12]_C_n_0 ),
        .O(RESIZE[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[13]_i_1 
       (.I0(\result_reg[13]_P_n_0 ),
        .I1(\result_reg[13]_LDC_n_0 ),
        .I2(\result_reg[13]_C_n_0 ),
        .O(RESIZE[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[14]_i_1 
       (.I0(\result_reg[14]_P_n_0 ),
        .I1(\result_reg[14]_LDC_n_0 ),
        .I2(\result_reg[14]_C_n_0 ),
        .O(RESIZE[14]));
  LUT1 #(
    .INIT(2'h1)) 
    \y16_reg[15]_i_1 
       (.I0(resetn),
        .O(SR));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[15]_i_2 
       (.I0(\result_reg[31]_P_n_0 ),
        .I1(\result_reg[31]_LDC_n_0 ),
        .I2(\result_reg[31]_C_n_0 ),
        .O(RESIZE[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[1]_i_1 
       (.I0(\result_reg[1]_P_n_0 ),
        .I1(\result_reg[1]_LDC_n_0 ),
        .I2(\result_reg[1]_C_n_0 ),
        .O(RESIZE[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[2]_i_1 
       (.I0(\result_reg[2]_P_n_0 ),
        .I1(\result_reg[2]_LDC_n_0 ),
        .I2(\result_reg[2]_C_n_0 ),
        .O(RESIZE[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[3]_i_1 
       (.I0(\result_reg[3]_P_n_0 ),
        .I1(\result_reg[3]_LDC_n_0 ),
        .I2(\result_reg[3]_C_n_0 ),
        .O(RESIZE[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[4]_i_1 
       (.I0(\result_reg[4]_P_n_0 ),
        .I1(\result_reg[4]_LDC_n_0 ),
        .I2(\result_reg[4]_C_n_0 ),
        .O(RESIZE[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[5]_i_1 
       (.I0(\result_reg[5]_P_n_0 ),
        .I1(\result_reg[5]_LDC_n_0 ),
        .I2(\result_reg[5]_C_n_0 ),
        .O(RESIZE[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[6]_i_1 
       (.I0(\result_reg[6]_P_n_0 ),
        .I1(\result_reg[6]_LDC_n_0 ),
        .I2(\result_reg[6]_C_n_0 ),
        .O(RESIZE[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[7]_i_1 
       (.I0(\result_reg[7]_P_n_0 ),
        .I1(\result_reg[7]_LDC_n_0 ),
        .I2(\result_reg[7]_C_n_0 ),
        .O(RESIZE[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[8]_i_1 
       (.I0(\result_reg[8]_P_n_0 ),
        .I1(\result_reg[8]_LDC_n_0 ),
        .I2(\result_reg[8]_C_n_0 ),
        .O(RESIZE[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y16_reg[9]_i_1 
       (.I0(\result_reg[9]_P_n_0 ),
        .I1(\result_reg[9]_LDC_n_0 ),
        .I2(\result_reg[9]_C_n_0 ),
        .O(RESIZE[9]));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[0]),
        .Q(y[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[10]),
        .Q(y[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[11]),
        .Q(y[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[12]),
        .Q(y[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[13]),
        .Q(y[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[14]),
        .Q(y[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[15]),
        .Q(y[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[1]),
        .Q(y[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[2]),
        .Q(y[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[3]),
        .Q(y[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[4]),
        .Q(y[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[5]),
        .Q(y[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[6]),
        .Q(y[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[7]),
        .Q(y[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[8]),
        .Q(y[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \y16_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(RESIZE[9]),
        .Q(y[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "ip_dc_stabilizer_v1_0" *) 
module design_1_ip_dc_stabilizer_0_0_ip_dc_stabilizer_v1_0
   (dbg_acc,
    S_AXI_AWREADY,
    y,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    axi_dc_stab_rdata,
    axi_dc_stab_rvalid,
    axi_dc_stab_bvalid,
    clk,
    x,
    resetn,
    axi_dc_stab_awaddr,
    axi_dc_stab_wvalid,
    axi_dc_stab_awvalid,
    axi_dc_stab_aclk,
    axi_dc_stab_wdata,
    axi_dc_stab_araddr,
    axi_dc_stab_arvalid,
    feedback,
    axi_dc_stab_wstrb,
    axi_dc_stab_aresetn,
    axi_dc_stab_bready,
    axi_dc_stab_rready);
  output [15:0]dbg_acc;
  output S_AXI_AWREADY;
  output [15:0]y;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]axi_dc_stab_rdata;
  output axi_dc_stab_rvalid;
  output axi_dc_stab_bvalid;
  input clk;
  input [15:0]x;
  input resetn;
  input [1:0]axi_dc_stab_awaddr;
  input axi_dc_stab_wvalid;
  input axi_dc_stab_awvalid;
  input axi_dc_stab_aclk;
  input [31:0]axi_dc_stab_wdata;
  input [1:0]axi_dc_stab_araddr;
  input axi_dc_stab_arvalid;
  input [15:0]feedback;
  input [3:0]axi_dc_stab_wstrb;
  input axi_dc_stab_aresetn;
  input axi_dc_stab_bready;
  input axi_dc_stab_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire axi_dc_stab_aclk;
  wire [1:0]axi_dc_stab_araddr;
  wire axi_dc_stab_aresetn;
  wire axi_dc_stab_arvalid;
  wire [1:0]axi_dc_stab_awaddr;
  wire axi_dc_stab_awvalid;
  wire axi_dc_stab_bready;
  wire axi_dc_stab_bvalid;
  wire [31:0]axi_dc_stab_rdata;
  wire axi_dc_stab_rready;
  wire axi_dc_stab_rvalid;
  wire [31:0]axi_dc_stab_wdata;
  wire [3:0]axi_dc_stab_wstrb;
  wire axi_dc_stab_wvalid;
  wire clk;
  wire [15:0]dbg_acc;
  wire [15:0]feedback;
  wire resetn;
  wire [15:0]x;
  wire [15:0]y;

  design_1_ip_dc_stabilizer_0_0_ip_dc_stabilizer_v1_0_AXI_DC_STAB ip_dc_stabilizer_v1_0_AXI_DC_STAB_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .axi_dc_stab_aclk(axi_dc_stab_aclk),
        .axi_dc_stab_araddr(axi_dc_stab_araddr),
        .axi_dc_stab_aresetn(axi_dc_stab_aresetn),
        .axi_dc_stab_arvalid(axi_dc_stab_arvalid),
        .axi_dc_stab_awaddr(axi_dc_stab_awaddr),
        .axi_dc_stab_awvalid(axi_dc_stab_awvalid),
        .axi_dc_stab_bready(axi_dc_stab_bready),
        .axi_dc_stab_bvalid(axi_dc_stab_bvalid),
        .axi_dc_stab_rdata(axi_dc_stab_rdata),
        .axi_dc_stab_rready(axi_dc_stab_rready),
        .axi_dc_stab_rvalid(axi_dc_stab_rvalid),
        .axi_dc_stab_wdata(axi_dc_stab_wdata),
        .axi_dc_stab_wstrb(axi_dc_stab_wstrb),
        .axi_dc_stab_wvalid(axi_dc_stab_wvalid),
        .clk(clk),
        .dbg_acc(dbg_acc),
        .feedback(feedback),
        .resetn(resetn),
        .x(x),
        .y(y));
endmodule

(* ORIG_REF_NAME = "ip_dc_stabilizer_v1_0_AXI_DC_STAB" *) 
module design_1_ip_dc_stabilizer_0_0_ip_dc_stabilizer_v1_0_AXI_DC_STAB
   (dbg_acc,
    S_AXI_AWREADY,
    y,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    axi_dc_stab_rdata,
    axi_dc_stab_rvalid,
    axi_dc_stab_bvalid,
    clk,
    x,
    resetn,
    axi_dc_stab_awaddr,
    axi_dc_stab_wvalid,
    axi_dc_stab_awvalid,
    axi_dc_stab_aclk,
    axi_dc_stab_wdata,
    axi_dc_stab_araddr,
    axi_dc_stab_arvalid,
    feedback,
    axi_dc_stab_wstrb,
    axi_dc_stab_aresetn,
    axi_dc_stab_bready,
    axi_dc_stab_rready);
  output [15:0]dbg_acc;
  output S_AXI_AWREADY;
  output [15:0]y;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]axi_dc_stab_rdata;
  output axi_dc_stab_rvalid;
  output axi_dc_stab_bvalid;
  input clk;
  input [15:0]x;
  input resetn;
  input [1:0]axi_dc_stab_awaddr;
  input axi_dc_stab_wvalid;
  input axi_dc_stab_awvalid;
  input axi_dc_stab_aclk;
  input [31:0]axi_dc_stab_wdata;
  input [1:0]axi_dc_stab_araddr;
  input axi_dc_stab_arvalid;
  input [15:0]feedback;
  input [3:0]axi_dc_stab_wstrb;
  input axi_dc_stab_aresetn;
  input axi_dc_stab_bready;
  input axi_dc_stab_rready;

  wire DCS_n_32;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_dc_stab_aclk;
  wire [1:0]axi_dc_stab_araddr;
  wire axi_dc_stab_aresetn;
  wire axi_dc_stab_arvalid;
  wire [1:0]axi_dc_stab_awaddr;
  wire axi_dc_stab_awvalid;
  wire axi_dc_stab_bready;
  wire axi_dc_stab_bvalid;
  wire [31:0]axi_dc_stab_rdata;
  wire axi_dc_stab_rready;
  wire axi_dc_stab_rvalid;
  wire [31:0]axi_dc_stab_wdata;
  wire [3:0]axi_dc_stab_wstrb;
  wire axi_dc_stab_wvalid;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire clk;
  wire [15:0]dbg_acc;
  wire dc_stab_enable;
  wire [15:0]feedback;
  wire [1:0]p_0_in;
  wire [31:0]reg_data_out;
  wire resetn;
  wire \slv_reg0[15]_i_1_n_0 ;
  wire \slv_reg0[23]_i_1_n_0 ;
  wire \slv_reg0[31]_i_1_n_0 ;
  wire \slv_reg0[7]_i_1_n_0 ;
  wire [15:0]slv_reg0_D;
  wire \slv_reg0_reg_n_0_[0] ;
  wire \slv_reg0_reg_n_0_[10] ;
  wire \slv_reg0_reg_n_0_[11] ;
  wire \slv_reg0_reg_n_0_[12] ;
  wire \slv_reg0_reg_n_0_[13] ;
  wire \slv_reg0_reg_n_0_[14] ;
  wire \slv_reg0_reg_n_0_[15] ;
  wire \slv_reg0_reg_n_0_[16] ;
  wire \slv_reg0_reg_n_0_[17] ;
  wire \slv_reg0_reg_n_0_[18] ;
  wire \slv_reg0_reg_n_0_[19] ;
  wire \slv_reg0_reg_n_0_[1] ;
  wire \slv_reg0_reg_n_0_[20] ;
  wire \slv_reg0_reg_n_0_[21] ;
  wire \slv_reg0_reg_n_0_[22] ;
  wire \slv_reg0_reg_n_0_[23] ;
  wire \slv_reg0_reg_n_0_[24] ;
  wire \slv_reg0_reg_n_0_[25] ;
  wire \slv_reg0_reg_n_0_[26] ;
  wire \slv_reg0_reg_n_0_[27] ;
  wire \slv_reg0_reg_n_0_[28] ;
  wire \slv_reg0_reg_n_0_[29] ;
  wire \slv_reg0_reg_n_0_[2] ;
  wire \slv_reg0_reg_n_0_[30] ;
  wire \slv_reg0_reg_n_0_[31] ;
  wire \slv_reg0_reg_n_0_[3] ;
  wire \slv_reg0_reg_n_0_[4] ;
  wire \slv_reg0_reg_n_0_[5] ;
  wire \slv_reg0_reg_n_0_[6] ;
  wire \slv_reg0_reg_n_0_[7] ;
  wire \slv_reg0_reg_n_0_[8] ;
  wire \slv_reg0_reg_n_0_[9] ;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [15:0]slv_reg1_D;
  wire \slv_reg1_reg_n_0_[0] ;
  wire \slv_reg1_reg_n_0_[10] ;
  wire \slv_reg1_reg_n_0_[11] ;
  wire \slv_reg1_reg_n_0_[12] ;
  wire \slv_reg1_reg_n_0_[13] ;
  wire \slv_reg1_reg_n_0_[14] ;
  wire \slv_reg1_reg_n_0_[15] ;
  wire \slv_reg1_reg_n_0_[16] ;
  wire \slv_reg1_reg_n_0_[17] ;
  wire \slv_reg1_reg_n_0_[18] ;
  wire \slv_reg1_reg_n_0_[19] ;
  wire \slv_reg1_reg_n_0_[1] ;
  wire \slv_reg1_reg_n_0_[20] ;
  wire \slv_reg1_reg_n_0_[21] ;
  wire \slv_reg1_reg_n_0_[22] ;
  wire \slv_reg1_reg_n_0_[23] ;
  wire \slv_reg1_reg_n_0_[24] ;
  wire \slv_reg1_reg_n_0_[25] ;
  wire \slv_reg1_reg_n_0_[26] ;
  wire \slv_reg1_reg_n_0_[27] ;
  wire \slv_reg1_reg_n_0_[28] ;
  wire \slv_reg1_reg_n_0_[29] ;
  wire \slv_reg1_reg_n_0_[2] ;
  wire \slv_reg1_reg_n_0_[30] ;
  wire \slv_reg1_reg_n_0_[31] ;
  wire \slv_reg1_reg_n_0_[3] ;
  wire \slv_reg1_reg_n_0_[4] ;
  wire \slv_reg1_reg_n_0_[5] ;
  wire \slv_reg1_reg_n_0_[6] ;
  wire \slv_reg1_reg_n_0_[7] ;
  wire \slv_reg1_reg_n_0_[8] ;
  wire \slv_reg1_reg_n_0_[9] ;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire \slv_reg2_D_reg_n_0_[0] ;
  wire \slv_reg2_D_reg_n_0_[1] ;
  wire \slv_reg2_D_reg_n_0_[2] ;
  wire \slv_reg2_D_reg_n_0_[3] ;
  wire \slv_reg2_D_reg_n_0_[4] ;
  wire \slv_reg2_reg_n_0_[0] ;
  wire \slv_reg2_reg_n_0_[10] ;
  wire \slv_reg2_reg_n_0_[11] ;
  wire \slv_reg2_reg_n_0_[12] ;
  wire \slv_reg2_reg_n_0_[13] ;
  wire \slv_reg2_reg_n_0_[14] ;
  wire \slv_reg2_reg_n_0_[15] ;
  wire \slv_reg2_reg_n_0_[16] ;
  wire \slv_reg2_reg_n_0_[17] ;
  wire \slv_reg2_reg_n_0_[18] ;
  wire \slv_reg2_reg_n_0_[19] ;
  wire \slv_reg2_reg_n_0_[1] ;
  wire \slv_reg2_reg_n_0_[20] ;
  wire \slv_reg2_reg_n_0_[21] ;
  wire \slv_reg2_reg_n_0_[22] ;
  wire \slv_reg2_reg_n_0_[23] ;
  wire \slv_reg2_reg_n_0_[24] ;
  wire \slv_reg2_reg_n_0_[25] ;
  wire \slv_reg2_reg_n_0_[26] ;
  wire \slv_reg2_reg_n_0_[27] ;
  wire \slv_reg2_reg_n_0_[28] ;
  wire \slv_reg2_reg_n_0_[29] ;
  wire \slv_reg2_reg_n_0_[2] ;
  wire \slv_reg2_reg_n_0_[30] ;
  wire \slv_reg2_reg_n_0_[31] ;
  wire \slv_reg2_reg_n_0_[3] ;
  wire \slv_reg2_reg_n_0_[4] ;
  wire \slv_reg2_reg_n_0_[5] ;
  wire \slv_reg2_reg_n_0_[6] ;
  wire \slv_reg2_reg_n_0_[7] ;
  wire \slv_reg2_reg_n_0_[8] ;
  wire \slv_reg2_reg_n_0_[9] ;
  wire slv_reg_rden;
  wire slv_reg_wren__2;
  wire [15:0]x;
  wire [15:0]y;

  design_1_ip_dc_stabilizer_0_0_dc_stabilizer DCS
       (.Q({dc_stab_enable,\slv_reg2_D_reg_n_0_[4] ,\slv_reg2_D_reg_n_0_[3] ,\slv_reg2_D_reg_n_0_[2] ,\slv_reg2_D_reg_n_0_[1] ,\slv_reg2_D_reg_n_0_[0] }),
        .SR(DCS_n_32),
        .acc321_carry__0_0(slv_reg1_D),
        .\acc321_inferred__0/i__carry__0_0 (slv_reg0_D),
        .clk(clk),
        .dbg_acc(dbg_acc),
        .feedback(feedback),
        .resetn(resetn),
        .x(x),
        .y(y));
  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(axi_dc_stab_awvalid),
        .I2(axi_dc_stab_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(axi_dc_stab_bready),
        .I5(axi_dc_stab_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(axi_dc_stab_araddr[0]),
        .I1(axi_dc_stab_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(axi_dc_stab_araddr[1]),
        .I1(axi_dc_stab_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .S(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(axi_dc_stab_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(axi_dc_stab_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(axi_dc_stab_wvalid),
        .I3(axi_dc_stab_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(axi_dc_stab_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(axi_dc_stab_wvalid),
        .I3(axi_dc_stab_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(axi_dc_stab_aresetn),
        .O(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(axi_dc_stab_wvalid),
        .I2(axi_dc_stab_awvalid),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(axi_dc_stab_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(axi_dc_stab_wvalid),
        .I4(axi_dc_stab_bready),
        .I5(axi_dc_stab_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(axi_dc_stab_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[0]_i_1 
       (.I0(\slv_reg1_reg_n_0_[0] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[0] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .O(reg_data_out[0]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_1 
       (.I0(\slv_reg1_reg_n_0_[10] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[10] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[10] ),
        .O(reg_data_out[10]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_1 
       (.I0(\slv_reg1_reg_n_0_[11] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[11] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[11] ),
        .O(reg_data_out[11]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[12]_i_1 
       (.I0(\slv_reg1_reg_n_0_[12] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[12] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[12] ),
        .O(reg_data_out[12]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[13]_i_1 
       (.I0(\slv_reg1_reg_n_0_[13] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[13] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[13] ),
        .O(reg_data_out[13]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[14]_i_1 
       (.I0(\slv_reg1_reg_n_0_[14] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[14] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[14] ),
        .O(reg_data_out[14]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[15]_i_1 
       (.I0(\slv_reg1_reg_n_0_[15] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[15] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[15] ),
        .O(reg_data_out[15]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[16]_i_1 
       (.I0(\slv_reg1_reg_n_0_[16] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[16] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[16] ),
        .O(reg_data_out[16]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[17]_i_1 
       (.I0(\slv_reg1_reg_n_0_[17] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[17] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[17] ),
        .O(reg_data_out[17]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[18]_i_1 
       (.I0(\slv_reg1_reg_n_0_[18] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[18] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[18] ),
        .O(reg_data_out[18]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[19]_i_1 
       (.I0(\slv_reg1_reg_n_0_[19] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[19] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[19] ),
        .O(reg_data_out[19]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_1 
       (.I0(\slv_reg1_reg_n_0_[1] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[1] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[1] ),
        .O(reg_data_out[1]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[20]_i_1 
       (.I0(\slv_reg1_reg_n_0_[20] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[20] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[20] ),
        .O(reg_data_out[20]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[21]_i_1 
       (.I0(\slv_reg1_reg_n_0_[21] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[21] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[21] ),
        .O(reg_data_out[21]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[22]_i_1 
       (.I0(\slv_reg1_reg_n_0_[22] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[22] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[22] ),
        .O(reg_data_out[22]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[23]_i_1 
       (.I0(\slv_reg1_reg_n_0_[23] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[23] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[23] ),
        .O(reg_data_out[23]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[24]_i_1 
       (.I0(\slv_reg1_reg_n_0_[24] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[24] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[24] ),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[25]_i_1 
       (.I0(\slv_reg1_reg_n_0_[25] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[25] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[25] ),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[26]_i_1 
       (.I0(\slv_reg1_reg_n_0_[26] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[26] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[26] ),
        .O(reg_data_out[26]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[27]_i_1 
       (.I0(\slv_reg1_reg_n_0_[27] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[27] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[27] ),
        .O(reg_data_out[27]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[28]_i_1 
       (.I0(\slv_reg1_reg_n_0_[28] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[28] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[28] ),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[29]_i_1 
       (.I0(\slv_reg1_reg_n_0_[29] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[29] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[29] ),
        .O(reg_data_out[29]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_1 
       (.I0(\slv_reg1_reg_n_0_[2] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[2] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[2] ),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[30]_i_1 
       (.I0(\slv_reg1_reg_n_0_[30] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[30] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[30] ),
        .O(reg_data_out[30]));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(axi_dc_stab_arvalid),
        .I2(axi_dc_stab_rvalid),
        .O(slv_reg_rden));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[31]_i_2 
       (.I0(\slv_reg1_reg_n_0_[31] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[31] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[31] ),
        .O(reg_data_out[31]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_1 
       (.I0(\slv_reg1_reg_n_0_[3] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[3] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[3] ),
        .O(reg_data_out[3]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_1 
       (.I0(\slv_reg1_reg_n_0_[4] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[4] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[4] ),
        .O(reg_data_out[4]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_1 
       (.I0(\slv_reg1_reg_n_0_[5] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[5] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[5] ),
        .O(reg_data_out[5]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_1 
       (.I0(\slv_reg1_reg_n_0_[6] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[6] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[6] ),
        .O(reg_data_out[6]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_1 
       (.I0(\slv_reg1_reg_n_0_[7] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[7] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[7] ),
        .O(reg_data_out[7]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_1 
       (.I0(\slv_reg1_reg_n_0_[8] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[8] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[8] ),
        .O(reg_data_out[8]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_1 
       (.I0(\slv_reg1_reg_n_0_[9] ),
        .I1(axi_araddr[2]),
        .I2(\slv_reg2_reg_n_0_[9] ),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[9] ),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(axi_dc_stab_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(axi_dc_stab_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(axi_dc_stab_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(axi_dc_stab_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(axi_dc_stab_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(axi_dc_stab_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(axi_dc_stab_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(axi_dc_stab_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(axi_dc_stab_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(axi_dc_stab_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(axi_dc_stab_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(axi_dc_stab_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(axi_dc_stab_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(axi_dc_stab_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(axi_dc_stab_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(axi_dc_stab_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(axi_dc_stab_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(axi_dc_stab_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(axi_dc_stab_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(axi_dc_stab_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(axi_dc_stab_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(axi_dc_stab_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(axi_dc_stab_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(axi_dc_stab_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(axi_dc_stab_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(axi_dc_stab_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(axi_dc_stab_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(axi_dc_stab_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(axi_dc_stab_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(axi_dc_stab_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(axi_dc_stab_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(axi_dc_stab_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(axi_dc_stab_rdata[9]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(axi_dc_stab_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(axi_dc_stab_rvalid),
        .I3(axi_dc_stab_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(axi_dc_stab_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(axi_dc_stab_wvalid),
        .I2(axi_dc_stab_awvalid),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(axi_dc_stab_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(axi_dc_stab_wstrb[1]),
        .O(\slv_reg0[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(axi_dc_stab_wstrb[2]),
        .O(\slv_reg0[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(axi_dc_stab_wstrb[3]),
        .O(\slv_reg0[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(axi_dc_stab_wstrb[0]),
        .O(\slv_reg0[7]_i_1_n_0 ));
  FDRE \slv_reg0_D_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[0] ),
        .Q(slv_reg0_D[0]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[10] ),
        .Q(slv_reg0_D[10]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[11] ),
        .Q(slv_reg0_D[11]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[12] ),
        .Q(slv_reg0_D[12]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[13] ),
        .Q(slv_reg0_D[13]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[14] ),
        .Q(slv_reg0_D[14]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[15] ),
        .Q(slv_reg0_D[15]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[1] ),
        .Q(slv_reg0_D[1]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[2] ),
        .Q(slv_reg0_D[2]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[3] ),
        .Q(slv_reg0_D[3]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[4] ),
        .Q(slv_reg0_D[4]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[5] ),
        .Q(slv_reg0_D[5]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[6] ),
        .Q(slv_reg0_D[6]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[7] ),
        .Q(slv_reg0_D[7]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[8] ),
        .Q(slv_reg0_D[8]),
        .R(DCS_n_32));
  FDRE \slv_reg0_D_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[9] ),
        .Q(slv_reg0_D[9]),
        .R(DCS_n_32));
  FDRE \slv_reg0_reg[0] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[0]),
        .Q(\slv_reg0_reg_n_0_[0] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[10] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[10]),
        .Q(\slv_reg0_reg_n_0_[10] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[11] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[11]),
        .Q(\slv_reg0_reg_n_0_[11] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[12] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[12]),
        .Q(\slv_reg0_reg_n_0_[12] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[13] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[13]),
        .Q(\slv_reg0_reg_n_0_[13] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[14] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[14]),
        .Q(\slv_reg0_reg_n_0_[14] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[15] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[15]),
        .Q(\slv_reg0_reg_n_0_[15] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[16] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[16]),
        .Q(\slv_reg0_reg_n_0_[16] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[17] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[17]),
        .Q(\slv_reg0_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[18] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[18]),
        .Q(\slv_reg0_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[19] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[19]),
        .Q(\slv_reg0_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[1] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[1]),
        .Q(\slv_reg0_reg_n_0_[1] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[20] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[20]),
        .Q(\slv_reg0_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[21] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[21]),
        .Q(\slv_reg0_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[22] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[22]),
        .Q(\slv_reg0_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[23] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[23]),
        .Q(\slv_reg0_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[24] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[24]),
        .Q(\slv_reg0_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[25] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[25]),
        .Q(\slv_reg0_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[26] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[26]),
        .Q(\slv_reg0_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[27] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[27]),
        .Q(\slv_reg0_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[28] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[28]),
        .Q(\slv_reg0_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[29] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[29]),
        .Q(\slv_reg0_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[2] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[2]),
        .Q(\slv_reg0_reg_n_0_[2] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[30] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[30]),
        .Q(\slv_reg0_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[31] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[31]),
        .Q(\slv_reg0_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[3] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[3]),
        .Q(\slv_reg0_reg_n_0_[3] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[4] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[4]),
        .Q(\slv_reg0_reg_n_0_[4] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[5] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[5]),
        .Q(\slv_reg0_reg_n_0_[5] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[6] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[6]),
        .Q(\slv_reg0_reg_n_0_[6] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[7] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[7]),
        .Q(\slv_reg0_reg_n_0_[7] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[8] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[8]),
        .Q(\slv_reg0_reg_n_0_[8] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[9] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[9]),
        .Q(\slv_reg0_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_dc_stab_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_dc_stab_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_dc_stab_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg1[31]_i_2 
       (.I0(axi_dc_stab_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(axi_dc_stab_wvalid),
        .O(slv_reg_wren__2));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_dc_stab_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_D_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[0] ),
        .Q(slv_reg1_D[0]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[10] ),
        .Q(slv_reg1_D[10]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[11] ),
        .Q(slv_reg1_D[11]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[12] ),
        .Q(slv_reg1_D[12]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[13] ),
        .Q(slv_reg1_D[13]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[14] ),
        .Q(slv_reg1_D[14]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[15] ),
        .Q(slv_reg1_D[15]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[1] ),
        .Q(slv_reg1_D[1]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[2] ),
        .Q(slv_reg1_D[2]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[3] ),
        .Q(slv_reg1_D[3]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[4] ),
        .Q(slv_reg1_D[4]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[5] ),
        .Q(slv_reg1_D[5]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[6] ),
        .Q(slv_reg1_D[6]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[7] ),
        .Q(slv_reg1_D[7]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[8] ),
        .Q(slv_reg1_D[8]),
        .R(DCS_n_32));
  FDRE \slv_reg1_D_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg1_reg_n_0_[9] ),
        .Q(slv_reg1_D[9]),
        .R(DCS_n_32));
  FDRE \slv_reg1_reg[0] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[0]),
        .Q(\slv_reg1_reg_n_0_[0] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[10]),
        .Q(\slv_reg1_reg_n_0_[10] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[11]),
        .Q(\slv_reg1_reg_n_0_[11] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[12]),
        .Q(\slv_reg1_reg_n_0_[12] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[13]),
        .Q(\slv_reg1_reg_n_0_[13] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[14]),
        .Q(\slv_reg1_reg_n_0_[14] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[15]),
        .Q(\slv_reg1_reg_n_0_[15] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[16]),
        .Q(\slv_reg1_reg_n_0_[16] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[17]),
        .Q(\slv_reg1_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[18]),
        .Q(\slv_reg1_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[19]),
        .Q(\slv_reg1_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[1]),
        .Q(\slv_reg1_reg_n_0_[1] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[20]),
        .Q(\slv_reg1_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[21]),
        .Q(\slv_reg1_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[22]),
        .Q(\slv_reg1_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[23]),
        .Q(\slv_reg1_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[24]),
        .Q(\slv_reg1_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[25]),
        .Q(\slv_reg1_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[26]),
        .Q(\slv_reg1_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[27]),
        .Q(\slv_reg1_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[28]),
        .Q(\slv_reg1_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[29]),
        .Q(\slv_reg1_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[2]),
        .Q(\slv_reg1_reg_n_0_[2] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[30]),
        .Q(\slv_reg1_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[31]),
        .Q(\slv_reg1_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[3]),
        .Q(\slv_reg1_reg_n_0_[3] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[4]),
        .Q(\slv_reg1_reg_n_0_[4] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[5]),
        .Q(\slv_reg1_reg_n_0_[5] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[6]),
        .Q(\slv_reg1_reg_n_0_[6] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[7]),
        .Q(\slv_reg1_reg_n_0_[7] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[8]),
        .Q(\slv_reg1_reg_n_0_[8] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[9]),
        .Q(\slv_reg1_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(axi_dc_stab_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(axi_dc_stab_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(axi_dc_stab_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(axi_dc_stab_wstrb[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_D_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[0] ),
        .Q(\slv_reg2_D_reg_n_0_[0] ),
        .R(DCS_n_32));
  FDRE \slv_reg2_D_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[1] ),
        .Q(\slv_reg2_D_reg_n_0_[1] ),
        .R(DCS_n_32));
  FDRE \slv_reg2_D_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[2] ),
        .Q(\slv_reg2_D_reg_n_0_[2] ),
        .R(DCS_n_32));
  FDRE \slv_reg2_D_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[3] ),
        .Q(\slv_reg2_D_reg_n_0_[3] ),
        .R(DCS_n_32));
  FDRE \slv_reg2_D_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[4] ),
        .Q(\slv_reg2_D_reg_n_0_[4] ),
        .R(DCS_n_32));
  FDRE \slv_reg2_D_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\slv_reg2_reg_n_0_[5] ),
        .Q(dc_stab_enable),
        .R(DCS_n_32));
  FDSE \slv_reg2_reg[0] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[0]),
        .Q(\slv_reg2_reg_n_0_[0] ),
        .S(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[10]),
        .Q(\slv_reg2_reg_n_0_[10] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[11]),
        .Q(\slv_reg2_reg_n_0_[11] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[12]),
        .Q(\slv_reg2_reg_n_0_[12] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[13]),
        .Q(\slv_reg2_reg_n_0_[13] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[14]),
        .Q(\slv_reg2_reg_n_0_[14] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[15]),
        .Q(\slv_reg2_reg_n_0_[15] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[16]),
        .Q(\slv_reg2_reg_n_0_[16] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[17]),
        .Q(\slv_reg2_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[18]),
        .Q(\slv_reg2_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[19]),
        .Q(\slv_reg2_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDSE \slv_reg2_reg[1] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[1]),
        .Q(\slv_reg2_reg_n_0_[1] ),
        .S(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[20]),
        .Q(\slv_reg2_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[21]),
        .Q(\slv_reg2_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[22]),
        .Q(\slv_reg2_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[23]),
        .Q(\slv_reg2_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[24]),
        .Q(\slv_reg2_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[25]),
        .Q(\slv_reg2_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[26]),
        .Q(\slv_reg2_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[27]),
        .Q(\slv_reg2_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[28]),
        .Q(\slv_reg2_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[29]),
        .Q(\slv_reg2_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDSE \slv_reg2_reg[2] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[2]),
        .Q(\slv_reg2_reg_n_0_[2] ),
        .S(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[30]),
        .Q(\slv_reg2_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[31]),
        .Q(\slv_reg2_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[3]),
        .Q(\slv_reg2_reg_n_0_[3] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[4]),
        .Q(\slv_reg2_reg_n_0_[4] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[5]),
        .Q(\slv_reg2_reg_n_0_[5] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[6]),
        .Q(\slv_reg2_reg_n_0_[6] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[7]),
        .Q(\slv_reg2_reg_n_0_[7] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[8]),
        .Q(\slv_reg2_reg_n_0_[8] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(axi_dc_stab_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(axi_dc_stab_wdata[9]),
        .Q(\slv_reg2_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
