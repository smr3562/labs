-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 18:31:22 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_peakdetector_0_0/design_1_ip_peakdetector_0_0_stub.vhdl
-- Design      : design_1_ip_peakdetector_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_ip_peakdetector_0_0 is
  Port ( 
    x : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    peak_amp : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );

end design_1_ip_peakdetector_0_0;

architecture stub of design_1_ip_peakdetector_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "x[15:0],signaldomain_clk,axibusdomain_clk,axibusdomain_aresetn,axibusdomain_s_axi_awaddr[4:0],axibusdomain_s_axi_awvalid,axibusdomain_s_axi_wdata[31:0],axibusdomain_s_axi_wstrb[3:0],axibusdomain_s_axi_wvalid,axibusdomain_s_axi_bready,axibusdomain_s_axi_araddr[4:0],axibusdomain_s_axi_arvalid,axibusdomain_s_axi_rready,peak_amp[15:0],peak_amp_dac[13:0],peak_amp_rdy[0:0],axibusdomain_s_axi_awready,axibusdomain_s_axi_wready,axibusdomain_s_axi_bresp[1:0],axibusdomain_s_axi_bvalid,axibusdomain_s_axi_arready,axibusdomain_s_axi_rdata[31:0],axibusdomain_s_axi_rresp[1:0],axibusdomain_s_axi_rvalid";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "ip_peakdetector,Vivado 2019.1";
begin
end;
