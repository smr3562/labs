library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity invert_and_offset is
    port(
      inp      : in  std_logic_vector(13 downto 0);
      offset   : in  std_logic_vector(13 downto 0);
	  inv_sel  : in  std_logic;
	  clk      : in std_logic;
	  
	  
      outp      : out std_logic_vector(15 downto 0)
	  
	);
    end invert_and_offset;
	
    architecture rtl of invert_and_offset is
      -- declarative part: empty
    
	signal inp14 : signed(13 downto 0);
	signal outp16 : signed(15 downto 0);
	signal offset14 : signed(13 downto 0);
	signal inp16 : signed (15 downto 0);
	signal offset16 : signed (15 downto 0);
	
	
	begin
    
	inp14 <= signed(inp);
	outp <= std_logic_vector(outp16);
	offset14 <= signed(offset);
	
	inp16 <= inp14(13)&inp14(13 downto 0) & '0';
	--inp16 <= resize(inp14,16);
	
	--offset16 <= resize(offset14,16);
	offset16 <= offset14(13)&offset14(13 downto 0) & '0';
	
	
	process (clk)
	begin
	  if rising_edge(clk) then 
	     if (inv_sel = '1') then outp16 <= -inp16 - offset16;
		 else outp16 <= inp16 - offset16;
		 end if;
	  end if;
	end process;
	
	
    end rtl;