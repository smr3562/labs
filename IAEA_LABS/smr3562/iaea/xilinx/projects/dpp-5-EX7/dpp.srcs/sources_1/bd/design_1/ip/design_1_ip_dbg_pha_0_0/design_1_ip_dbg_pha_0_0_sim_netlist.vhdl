-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Feb 16 17:58:00 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_pha_0_0/design_1_ip_dbg_pha_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_dbg_pha_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_dbg_pha_0_0 is
  port (
    peak_amp_rdy_fast : in STD_LOGIC;
    rejectn : in STD_LOGIC;
    peak_amp_rdy_slow : in STD_LOGIC;
    peak_det_signal : in STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy_fast_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rejectn_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy_slow_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_det_signal_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_dbg_pha_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_dbg_pha_0_0 : entity is "design_1_ip_dbg_pha_0_0,top,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_ip_dbg_pha_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_ip_dbg_pha_0_0 : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of design_1_ip_dbg_pha_0_0 : entity is "top,Vivado 2019.1";
end design_1_ip_dbg_pha_0_0;

architecture STRUCTURE of design_1_ip_dbg_pha_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^peak_amp_rdy_fast\ : STD_LOGIC;
  signal \^peak_amp_rdy_slow\ : STD_LOGIC;
  signal \^peak_det_signal\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^rejectn\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of peak_amp_rdy_fast_out : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_amp_rdy_fast_out";
  attribute x_interface_info of peak_amp_rdy_slow_out : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_amp_rdy_slow_out";
  attribute x_interface_info of peak_det_signal_out : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_det_signal_out";
  attribute x_interface_info of rejectn_out : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 rejectn_out";
begin
  \^peak_amp_rdy_fast\ <= peak_amp_rdy_fast;
  \^peak_amp_rdy_slow\ <= peak_amp_rdy_slow;
  \^peak_det_signal\(15 downto 0) <= peak_det_signal(15 downto 0);
  \^rejectn\ <= rejectn;
  peak_amp_rdy_fast_out(15) <= \<const0>\;
  peak_amp_rdy_fast_out(14) <= \^peak_amp_rdy_fast\;
  peak_amp_rdy_fast_out(13) <= \<const0>\;
  peak_amp_rdy_fast_out(12) <= \<const0>\;
  peak_amp_rdy_fast_out(11) <= \<const0>\;
  peak_amp_rdy_fast_out(10) <= \<const0>\;
  peak_amp_rdy_fast_out(9) <= \<const0>\;
  peak_amp_rdy_fast_out(8) <= \<const0>\;
  peak_amp_rdy_fast_out(7) <= \<const0>\;
  peak_amp_rdy_fast_out(6) <= \<const0>\;
  peak_amp_rdy_fast_out(5) <= \<const0>\;
  peak_amp_rdy_fast_out(4) <= \<const0>\;
  peak_amp_rdy_fast_out(3) <= \<const0>\;
  peak_amp_rdy_fast_out(2) <= \<const0>\;
  peak_amp_rdy_fast_out(1) <= \<const0>\;
  peak_amp_rdy_fast_out(0) <= \<const0>\;
  peak_amp_rdy_slow_out(15) <= \<const0>\;
  peak_amp_rdy_slow_out(14) <= \^peak_amp_rdy_slow\;
  peak_amp_rdy_slow_out(13) <= \<const0>\;
  peak_amp_rdy_slow_out(12) <= \<const0>\;
  peak_amp_rdy_slow_out(11) <= \<const0>\;
  peak_amp_rdy_slow_out(10) <= \<const0>\;
  peak_amp_rdy_slow_out(9) <= \<const0>\;
  peak_amp_rdy_slow_out(8) <= \<const0>\;
  peak_amp_rdy_slow_out(7) <= \<const0>\;
  peak_amp_rdy_slow_out(6) <= \<const0>\;
  peak_amp_rdy_slow_out(5) <= \<const0>\;
  peak_amp_rdy_slow_out(4) <= \<const0>\;
  peak_amp_rdy_slow_out(3) <= \<const0>\;
  peak_amp_rdy_slow_out(2) <= \<const0>\;
  peak_amp_rdy_slow_out(1) <= \<const0>\;
  peak_amp_rdy_slow_out(0) <= \<const0>\;
  peak_det_signal_out(15 downto 0) <= \^peak_det_signal\(15 downto 0);
  rejectn_out(15) <= \<const0>\;
  rejectn_out(14) <= \^rejectn\;
  rejectn_out(13) <= \<const0>\;
  rejectn_out(12) <= \<const0>\;
  rejectn_out(11) <= \<const0>\;
  rejectn_out(10) <= \<const0>\;
  rejectn_out(9) <= \<const0>\;
  rejectn_out(8) <= \<const0>\;
  rejectn_out(7) <= \<const0>\;
  rejectn_out(6) <= \<const0>\;
  rejectn_out(5) <= \<const0>\;
  rejectn_out(4) <= \<const0>\;
  rejectn_out(3) <= \<const0>\;
  rejectn_out(2) <= \<const0>\;
  rejectn_out(1) <= \<const0>\;
  rejectn_out(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
end STRUCTURE;
