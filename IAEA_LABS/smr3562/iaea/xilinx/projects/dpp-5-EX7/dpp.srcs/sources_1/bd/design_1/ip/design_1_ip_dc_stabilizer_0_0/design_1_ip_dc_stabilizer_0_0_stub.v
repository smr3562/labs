// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Feb 16 17:52:15 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               E:/smr3562/iaea/xilinx/projects/dpp-5/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dc_stabilizer_0_0/design_1_ip_dc_stabilizer_0_0_stub.v
// Design      : design_1_ip_dc_stabilizer_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ip_dc_stabilizer_v1_0,Vivado 2019.1" *)
module design_1_ip_dc_stabilizer_0_0(x, y, feedback, resetn, clk, dbg_acc, 
  axi_dc_stab_aclk, axi_dc_stab_aresetn, axi_dc_stab_awaddr, axi_dc_stab_awprot, 
  axi_dc_stab_awvalid, axi_dc_stab_awready, axi_dc_stab_wdata, axi_dc_stab_wstrb, 
  axi_dc_stab_wvalid, axi_dc_stab_wready, axi_dc_stab_bresp, axi_dc_stab_bvalid, 
  axi_dc_stab_bready, axi_dc_stab_araddr, axi_dc_stab_arprot, axi_dc_stab_arvalid, 
  axi_dc_stab_arready, axi_dc_stab_rdata, axi_dc_stab_rresp, axi_dc_stab_rvalid, 
  axi_dc_stab_rready)
/* synthesis syn_black_box black_box_pad_pin="x[15:0],y[15:0],feedback[15:0],resetn,clk,dbg_acc[15:0],axi_dc_stab_aclk,axi_dc_stab_aresetn,axi_dc_stab_awaddr[3:0],axi_dc_stab_awprot[2:0],axi_dc_stab_awvalid,axi_dc_stab_awready,axi_dc_stab_wdata[31:0],axi_dc_stab_wstrb[3:0],axi_dc_stab_wvalid,axi_dc_stab_wready,axi_dc_stab_bresp[1:0],axi_dc_stab_bvalid,axi_dc_stab_bready,axi_dc_stab_araddr[3:0],axi_dc_stab_arprot[2:0],axi_dc_stab_arvalid,axi_dc_stab_arready,axi_dc_stab_rdata[31:0],axi_dc_stab_rresp[1:0],axi_dc_stab_rvalid,axi_dc_stab_rready" */;
  input [15:0]x;
  output [15:0]y;
  input [15:0]feedback;
  input resetn;
  input clk;
  output [15:0]dbg_acc;
  input axi_dc_stab_aclk;
  input axi_dc_stab_aresetn;
  input [3:0]axi_dc_stab_awaddr;
  input [2:0]axi_dc_stab_awprot;
  input axi_dc_stab_awvalid;
  output axi_dc_stab_awready;
  input [31:0]axi_dc_stab_wdata;
  input [3:0]axi_dc_stab_wstrb;
  input axi_dc_stab_wvalid;
  output axi_dc_stab_wready;
  output [1:0]axi_dc_stab_bresp;
  output axi_dc_stab_bvalid;
  input axi_dc_stab_bready;
  input [3:0]axi_dc_stab_araddr;
  input [2:0]axi_dc_stab_arprot;
  input axi_dc_stab_arvalid;
  output axi_dc_stab_arready;
  output [31:0]axi_dc_stab_rdata;
  output [1:0]axi_dc_stab_rresp;
  output axi_dc_stab_rvalid;
  input axi_dc_stab_rready;
endmodule
