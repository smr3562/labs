library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ip_dc_stabilizer_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface AXI_DC_STAB
		C_AXI_DC_STAB_DATA_WIDTH	: integer	:= 32;
		C_AXI_DC_STAB_ADDR_WIDTH	: integer	:= 4
	);
	port (
		-- Users to add ports here
        x : in std_logic_vector(15 downto 0); 
        y : out std_logic_vector(15 downto 0);  
		feedback : in std_logic_vector(15 downto 0);  		
        resetn : in std_logic;
        clk : in std_logic;
		dbg_acc : out std_logic_vector (15 downto 0);
        
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface AXI_DC_STAB
		axi_dc_stab_aclk	: in std_logic;
		axi_dc_stab_aresetn	: in std_logic;
		axi_dc_stab_awaddr	: in std_logic_vector(C_AXI_DC_STAB_ADDR_WIDTH-1 downto 0);
		axi_dc_stab_awprot	: in std_logic_vector(2 downto 0);
		axi_dc_stab_awvalid	: in std_logic;
		axi_dc_stab_awready	: out std_logic;
		axi_dc_stab_wdata	: in std_logic_vector(C_AXI_DC_STAB_DATA_WIDTH-1 downto 0);
		axi_dc_stab_wstrb	: in std_logic_vector((C_AXI_DC_STAB_DATA_WIDTH/8)-1 downto 0);
		axi_dc_stab_wvalid	: in std_logic;
		axi_dc_stab_wready	: out std_logic;
		axi_dc_stab_bresp	: out std_logic_vector(1 downto 0);
		axi_dc_stab_bvalid	: out std_logic;
		axi_dc_stab_bready	: in std_logic;
		axi_dc_stab_araddr	: in std_logic_vector(C_AXI_DC_STAB_ADDR_WIDTH-1 downto 0);
		axi_dc_stab_arprot	: in std_logic_vector(2 downto 0);
		axi_dc_stab_arvalid	: in std_logic;
		axi_dc_stab_arready	: out std_logic;
		axi_dc_stab_rdata	: out std_logic_vector(C_AXI_DC_STAB_DATA_WIDTH-1 downto 0);
		axi_dc_stab_rresp	: out std_logic_vector(1 downto 0);
		axi_dc_stab_rvalid	: out std_logic;
		axi_dc_stab_rready	: in std_logic
	);
end ip_dc_stabilizer_v1_0;

architecture arch_imp of ip_dc_stabilizer_v1_0 is

	-- component declaration
	component ip_dc_stabilizer_v1_0_AXI_DC_STAB is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
		);
		port (
		x : in std_logic_vector(15 downto 0);   --top modul does not support VHDL 2008   
        y : out std_logic_vector(15 downto 0);         
		feedback : in std_logic_vector(15 downto 0);  
        resetn : in std_logic;
        clk : in std_logic;
		dbg_acc : out std_logic_vector (15 downto 0);
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component ip_dc_stabilizer_v1_0_AXI_DC_STAB;

begin

-- Instantiation of Axi Bus Interface AXI_DC_STAB
ip_dc_stabilizer_v1_0_AXI_DC_STAB_inst : ip_dc_stabilizer_v1_0_AXI_DC_STAB
   generic map 
      (
      C_S_AXI_DATA_WIDTH	=> C_AXI_DC_STAB_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH	=> C_AXI_DC_STAB_ADDR_WIDTH
      )
   port map 
      (
	  x => x,
      y => y,
	  feedback => feedback,
      resetn => resetn,
      clk => clk,
	  dbg_acc => dbg_acc,
	  S_AXI_ACLK	=> axi_dc_stab_aclk,
	  S_AXI_ARESETN	=> axi_dc_stab_aresetn,
	  S_AXI_AWADDR	=> axi_dc_stab_awaddr,
	  S_AXI_AWPROT	=> axi_dc_stab_awprot,
	  S_AXI_AWVALID	=> axi_dc_stab_awvalid,
	  S_AXI_AWREADY	=> axi_dc_stab_awready,
	  S_AXI_WDATA	=> axi_dc_stab_wdata,
	  S_AXI_WSTRB	=> axi_dc_stab_wstrb,
	  S_AXI_WVALID	=> axi_dc_stab_wvalid,
	  S_AXI_WREADY	=> axi_dc_stab_wready,
	  S_AXI_BRESP	=> axi_dc_stab_bresp,
	  S_AXI_BVALID	=> axi_dc_stab_bvalid,
	  S_AXI_BREADY	=> axi_dc_stab_bready,
	  S_AXI_ARADDR	=> axi_dc_stab_araddr,
	  S_AXI_ARPROT	=> axi_dc_stab_arprot,
	  S_AXI_ARVALID	=> axi_dc_stab_arvalid,
	  S_AXI_ARREADY	=> axi_dc_stab_arready,
	  S_AXI_RDATA	=> axi_dc_stab_rdata,
	  S_AXI_RRESP	=> axi_dc_stab_rresp,
	  S_AXI_RVALID	=> axi_dc_stab_rvalid,
	  S_AXI_RREADY	=> axi_dc_stab_rready
	  );

	-- Add user logic here

	-- User logic ends

end arch_imp;
