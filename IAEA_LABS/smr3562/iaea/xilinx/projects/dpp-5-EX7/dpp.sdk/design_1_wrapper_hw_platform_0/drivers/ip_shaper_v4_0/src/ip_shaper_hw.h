/**
*
* @file ip_shaper_hw.h
*
* This header file contains identifiers and driver functions (or
* macros) that can be used to access the device.  The user should refer to the
* hardware device specification for more details of the device operation.
*/ 
#define IP_SHAPER_R6_B20 0x0/**< r6_b20 */
#define IP_SHAPER_R5_B00 0x4/**< r5_b00 */
#define IP_SHAPER_R4_NA_INV 0x8/**< r4_na_inv */
#define IP_SHAPER_R3_NB 0xc/**< r3_nb */
#define IP_SHAPER_R2_NA 0x10/**< r2_na */
#define IP_SHAPER_R1_B10 0x14/**< r1_b10 */
