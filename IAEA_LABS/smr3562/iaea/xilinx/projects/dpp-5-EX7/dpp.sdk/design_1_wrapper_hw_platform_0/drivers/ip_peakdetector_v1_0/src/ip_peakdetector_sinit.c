/**
* @file ip_peakdetector_sinit.c
*
* The implementation of the ip_peakdetector driver's static initialzation
* functionality.
*
* @note
*
* None
*
*/

#include "xstatus.h"
#include "xparameters.h"
#include "ip_peakdetector.h"
extern ip_peakdetector_Config ip_peakdetector_ConfigTable[];
/**
* Lookup the device configuration based on the unique device ID.  The table
* ConfigTable contains the configuration info for each device in the system.
*
* @param DeviceId is the device identifier to lookup.
*
* @return
*     - A pointer of data type ip_peakdetector_Config which
*    points to the device configuration if DeviceID is found.
*    - NULL if DeviceID is not found.
*
* @note    None.
*
*/
ip_peakdetector_Config *ip_peakdetector_LookupConfig(u16 DeviceId) {
    ip_peakdetector_Config *ConfigPtr = NULL;
    int Index;
    for (Index = 0; Index < XPAR_IP_PEAKDETECTOR_NUM_INSTANCES; Index++) {
        if (ip_peakdetector_ConfigTable[Index].DeviceId == DeviceId) {
            ConfigPtr = &ip_peakdetector_ConfigTable[Index];
            break;
        }
    }
    return ConfigPtr;
}
int ip_peakdetector_Initialize(ip_peakdetector *InstancePtr, u16 DeviceId) {
    ip_peakdetector_Config *ConfigPtr;
    Xil_AssertNonvoid(InstancePtr != NULL);
    ConfigPtr = ip_peakdetector_LookupConfig(DeviceId);
    if (ConfigPtr == NULL) {
        InstancePtr->IsReady = 0;
        return (XST_DEVICE_NOT_FOUND);
    }
    return ip_peakdetector_CfgInitialize(InstancePtr, ConfigPtr);
}

