/*
 * int.c
 *
 *  Created on: Oct 21, 2019
 *      Author: boss
 */


/******************************************************************************
*
* Copyright (C) 2002 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/
/******************************************************************************/
/**
*
* @file xintc_example.c
*
* This file contains a design example using the Interrupt Controller driver
* (XIntc) and hardware device. Please reference other device driver examples to
* see more examples of how the intc and interrupts can be used by a software
* application.
*
* This example shows the use of the Interrupt Controller both with a PowerPC
* and MicroBlaze processor.
*
* @note
*		This example can also be used for Cascade mode interrupt
*		controllers by using the interrupt IDs generated in
*		xparameters.h. For Cascade mode, Interrupt IDs are generated
*		in xparameters.h as shown below:
*
*	    Master/Primary INTC
*		 ______
*		|      |-0      Secondary INTC
*		|      |-.         ______
*		|      |-.        |      |-32        Last INTC
*		|      |-.        |      |-.          ______
*		|______|<--31-----|      |-.         |      |-64
*			          |      |-.         |      |-.
*			          |______|<--63------|      |-.
*                                                    |      |-.
*                                                    |______|-95
*
*		All driver functions has to be called using
*		DeviceId/InstancePtr of Primary/Master Controller only. Driver
*		functions takes care of Slave Controllers based on Interrupt
*		ID passed. User must not use Interrupt source/ID  31 of Primary
*		and Secondary controllers to call driver functions.
*
* <pre>
*
* MODIFICATION HISTORY:
* Ver   Who  Date	 Changes
* ----- ---- -------- ----------------------------------------------------
* 1.00b jhl  02/13/02 First release
* 1.00c rpm  11/13/03 Updated to show microblaze and PPC interrupt use and
*		      to use the common L0/L1 interrupt handler with device ID.
* 1.00c sv   06/29/05 Minor changes to comply to Doxygen and coding guidelines
* 2.00a ktn  10/20/09 Updated to use HAL Processor APIs amd minor modifications
*		      as per coding guidelines.
* 3.6   ms   01/23/17 Added xil_printf statement in main function to
*                     ensure that "Successfully ran" and "Failed" strings
*                     are available in all examples. This is a fix for
*                     CR-965028.
* ----- ---- -------- ---------------------------------------------------------
* 4.00  Mladen Bogovac    10/10/19
******************************************************************************/

/***************************** Include Files *********************************/

#include "xparameters.h"
#include "xstatus.h"
#include "xintc.h"
#include "xil_exception.h"
//#include "xil_printf.h"
#include "intc.h"

/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
//below is replaced by Int32 function parameter
//#define INTC_DEVICE_ID		  XPAR_INTC_0_DEVICE_ID

/*
 *  This is the Interrupt Number of the Device whose Interrupt Output is
 *  connected to the Input of the Interrupt Controller
 */

static void ScopeInterruptHandler(void *CallbackRef);
static int SetUpInterruptSystem(XIntc *XIntcInstancePtr);
static XIntc InterruptController; /* Instance of the Interrupt Controller */
static int WaveformReady;

/******************************************************************************
* @param	DeviceId is Device ID of the Interrupt Controller Device,
*		typically XPAR_<INTC_instance>_DEVICE_ID value from
*		xparameters.h.
*
* @return	XST_SUCCESS to indicate success, otherwise XST_FAILURE.
*
* @note		None.
*
******************************************************************************/

int IntcPSInit(u16 DeviceId)
   {
   int Status;

   /*
	* Initialize the interrupt controller driver so that it is ready to
	* use.
	*/
   Status = XIntc_Initialize(&InterruptController, DeviceId);
   if (Status != XST_SUCCESS)
      {
	  return XST_FAILURE;
	  }


   /*
	* Perform a self-test to ensure that the hardware was built
	* correctly.
	*/
   Status = XIntc_SelfTest(&InterruptController);
   if (Status != XST_SUCCESS)
      {
	  return XST_FAILURE;
	  }

   /*
	* Setup the Interrupt System.
	*/
   Status = SetUpInterruptSystem(&InterruptController);
   if (Status != XST_SUCCESS)
      {
	  return XST_FAILURE;
	  }

   return XST_SUCCESS;

   }

/******************************************************************************/
/**
*
* This function connects the interrupt handler of the interrupt controller to
* the processor.  This function is seperate to allow it to be customized for
* each application.  Each processor or RTOS may require unique processing to
* connect the interrupt handler.
*
* @param	None.
*
* @return	None.
*
* @note		None.
*
****************************************************************************/
int SetUpInterruptSystem(XIntc *XIntcInstancePtr)
   {
   int Status;


   /*
	* Connect a device driver handler that will be called when an interrupt
	* for the device occurs, the device driver handler performs the
	* specific interrupt processing for the device.
	*/
   Status = XIntc_Connect(XIntcInstancePtr,  XPAR_INTC_0_IP_SCOPE_0_VEC_ID, (XInterruptHandler) ScopeInterruptHandler, (void *)0);
   if (Status != XST_SUCCESS)
      {
	  return XST_FAILURE;
	  }


   /*
	* Start the interrupt controller such that interrupts are enabled for
	* all devices that cause interrupts, specify simulation mode so that
	* an interrupt can be caused by software rather than a real hardware
	* interrupt.
	*/
   Status = XIntc_Start(XIntcInstancePtr, XIN_REAL_MODE);
   if (Status != XST_SUCCESS)
      {
	  return XST_FAILURE;
	  }


   /*
	* Enable the interrupt for the device and then cause (simulate) an
	* interrupt so the handlers will be called.
	*/
   XIntc_Enable(XIntcInstancePtr, XPAR_INTC_0_IP_SCOPE_0_VEC_ID);

   /*
	* Initialize the exception table.
	*/
   Xil_ExceptionInit();

   /*
	* Register the interrupt controller handler with the exception table.
	*/
   Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				(Xil_ExceptionHandler)XIntc_InterruptHandler,
				XIntcInstancePtr);

   /*
	* Enable exceptions.
	*/
   Xil_ExceptionEnable();

   return XST_SUCCESS;

   }

/******************************************************************************/
/**
*
* This function is designed to look like an interrupt handler in a device
* driver. This is typically a 2nd level handler that is called from the
* interrupt controller interrupt handler.  This handler would typically
* perform device specific processing such as reading and writing the registers
* of the device to clear the interrupt condition and pass any data to an
* application using the device driver.  Many drivers already provide this
* handler and the user is not required to create it.
*
* @param	CallbackRef is passed back to the device driver's interrupt
*		handler by the XIntc driver.  It was given to the XIntc driver
*		in the XIntc_Connect() function call.  It is typically a pointer
*		to the device driver instance variable if using the Xilinx
*		Level 1 device drivers.  In this example, we do not care about
*		the callback reference, so we passed it a 0 when connecting the
*		handler to the XIntc driver and we make no use of it here.
*
* @return	None.
*
* @note		None.
*
****************************************************************************/
void ScopeInterruptHandler(void *pInterruptController)
   {
   //disable interrupt from Scope
   //this interrupt is enabled again when host sends command "$LS"
   XIntc_Disable(&InterruptController,XPAR_INTC_0_IP_SCOPE_0_VEC_ID);
   WaveformReady = 1;
   }

void IntcDisableInterrupt(u32 IntId)
   {
   XIntc_Disable(&InterruptController, IntId);
   }

void IntcEnableInterrupt(u32 IntId)
   {
   WaveformReady = 0;
   XIntc_Enable(&InterruptController, IntId);
   }

int IntcIsWaveformReady()
   {
   return WaveformReady;
   }
