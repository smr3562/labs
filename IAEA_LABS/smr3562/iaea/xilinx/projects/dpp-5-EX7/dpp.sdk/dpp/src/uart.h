/*
 * uart.h
 *
 *  Created on: Nov 13, 2019
 *      Author: boss
 */

#ifndef SRC_UART_H_
#define SRC_UART_H_

//
// methods defined in this modules
//
int UartLiteInit(u16 DeviceId);
int UartLiteSendReply(u16 DeviceId, u8 *szReplay, unsigned int uiReplayCount);
int UartLiteReceiveCommand(u16 DeviceId, u8 *szCommand, unsigned int *uiReceivedCount, unsigned int uiMaxCount);

#endif /* SRC_UART_H_ */
