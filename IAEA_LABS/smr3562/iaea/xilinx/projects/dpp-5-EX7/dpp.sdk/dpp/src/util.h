/*
 * util.h
 *
 *  Created on: Feb 1, 2021
 *      Author: Mladen
 */

#ifndef SRC_UTIL_H_
#define SRC_UTIL_H_

int u32toa(u32 n, char *str);
int u32atoa(u32 *array, int len, char *str);
u32 atou32(char* str);
int atoi32(char* str);

#endif /* SRC_UTIL_H_ */
