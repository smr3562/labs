/**
*
* @file ip_scope_hw.h
*
* This header file contains identifiers and driver functions (or
* macros) that can be used to access the device.  The user should refer to the
* hardware device specification for more details of the device operation.
*/ 
#define IP_SCOPE_R7_CLEAR 0x0/**< r7_clear */
#define IP_SCOPE_R6_DELAY 0x4/**< r6_delay */
#define IP_SCOPE_R5_ENABLE 0x8/**< r5_enable */
#define IP_SCOPE_R4_THRESHOLD 0xc/**< r4_threshold */
#define IP_SCOPE_R3_DINA 0x10/**< r3_dina */
#define IP_SCOPE_R2_WEA 0x14/**< r2_wea */
#define IP_SCOPE_R1_ADDRA 0x18/**< r1_addra */
#define IP_SCOPE_R8_FULL 0x1c/**< r8_full */
#define IP_SCOPE_R9_DOUTA 0x20/**< r9_douta */
