
 PARAMETER VERSION = 2.2.0


BEGIN OS
 PARAMETER OS_NAME = standalone
 PARAMETER OS_VER = 7.0
 PARAMETER PROC_INSTANCE = ps_mb_0_microblaze_0
 PARAMETER stdin = mb_periph_0_axi_uartlite_0
 PARAMETER stdout = mb_periph_0_axi_uartlite_0
END


BEGIN PROCESSOR
 PARAMETER DRIVER_NAME = cpu
 PARAMETER DRIVER_VER = 2.9
 PARAMETER HW_INSTANCE = ps_mb_0_microblaze_0
 PARAMETER compiler_flags =  -mlittle-endian -mxl-soft-mul -mcpu=v11.0
END


BEGIN DRIVER
 PARAMETER DRIVER_NAME = ip_shaper
 PARAMETER DRIVER_VER = 4.0
 PARAMETER HW_INSTANCE = dpp_0_pulse_conditioning_slow_ip_shaper_0
END

BEGIN DRIVER
 PARAMETER DRIVER_NAME = invert_and_offset
 PARAMETER DRIVER_VER = 1.0
 PARAMETER HW_INSTANCE = dpp_0_pulse_offseting_invert_and_offset_0
END

BEGIN DRIVER
 PARAMETER DRIVER_NAME = ip_mux16_2_if
 PARAMETER DRIVER_VER = 1.0
 PARAMETER HW_INSTANCE = dpp_0_scope_ip_mux16_2_if_0
END

BEGIN DRIVER
 PARAMETER DRIVER_NAME = ip_scope
 PARAMETER DRIVER_VER = 1.2
 PARAMETER HW_INSTANCE = dpp_0_scope_ip_scope_0
END

BEGIN DRIVER
 PARAMETER DRIVER_NAME = uartlite
 PARAMETER DRIVER_VER = 3.2
 PARAMETER HW_INSTANCE = mb_periph_0_axi_uartlite_0
END

BEGIN DRIVER
 PARAMETER DRIVER_NAME = intc
 PARAMETER DRIVER_VER = 3.9
 PARAMETER HW_INSTANCE = ps_mb_0_axi_intc_0
END

BEGIN DRIVER
 PARAMETER DRIVER_NAME = bram
 PARAMETER DRIVER_VER = 4.3
 PARAMETER HW_INSTANCE = ps_mb_0_microblaze_0_local_memory_dlmb_bram_if_cntlr
END

BEGIN DRIVER
 PARAMETER DRIVER_NAME = bram
 PARAMETER DRIVER_VER = 4.3
 PARAMETER HW_INSTANCE = ps_mb_0_microblaze_0_local_memory_ilmb_bram_if_cntlr
END


