
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7a35tcpg236-1
   set_property BOARD_PART digilentinc.com:cmod_a7-35t:part0:1.1 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: microblaze_0_local_memory
proc create_hier_cell_microblaze_0_local_memory { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_microblaze_0_local_memory() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB

  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB


  # Create pins
  create_bd_pin -dir I -type clk LMB_Clk
  create_bd_pin -dir I -type rst SYS_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list \
   CONFIG.C_ECC {0} \
 ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list \
   CONFIG.C_ECC {0} \
 ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 lmb_bram ]
  set_property -dict [ list \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Use_RSTB_Pin {true} \
   CONFIG.use_bram_block {BRAM_Controller} \
 ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_0_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_0_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net SYS_Rst_1 [get_bd_pins SYS_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: scope
proc create_hier_cell_scope { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_scope() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 axibusdomain_s_axi

  create_bd_intf_pin -mode Slave -vlnv iaea.org:interface:dbg_invert_and_offset_rtl:1.0 dbg_invert_and_offset_s0

  create_bd_intf_pin -mode Slave -vlnv iaea.org:interface:dbg_pulse_cond_slow_rtl:1.0 dbg_pulse_cond_slow_s0


  # Create pins
  create_bd_pin -dir I -type clk clk
  create_bd_pin -dir O -from 0 -to 0 -type intr full
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn

  # Create instance: ip_dbg_term_pha_0, and set properties
  set ip_dbg_term_pha_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ip_dbg_term_pha:1.0 ip_dbg_term_pha_0 ]

  # Create instance: ip_dbg_term_pulse_co_0, and set properties
  set ip_dbg_term_pulse_co_0 [ create_bd_cell -type ip -vlnv iaea.org:user:ip_dbg_term_pulse_cond_fast:1.0 ip_dbg_term_pulse_co_0 ]

  # Create instance: ip_mux16_2_if_0, and set properties
  set ip_mux16_2_if_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ip_mux16_2_if:1.0 ip_mux16_2_if_0 ]

  # Create instance: ip_scope_0, and set properties
  set ip_scope_0 [ create_bd_cell -type ip -vlnv User_Company:SysGen:ip_scope:1.2 ip_scope_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI1_1 [get_bd_intf_pins S00_AXI] [get_bd_intf_pins ip_mux16_2_if_0/S00_AXI]
  connect_bd_intf_net -intf_net axibusdomain_s_axi_1 [get_bd_intf_pins axibusdomain_s_axi] [get_bd_intf_pins ip_scope_0/axibusdomain_s_axi]
  connect_bd_intf_net -intf_net dbg_pulse_cond_slow_s0_1 [get_bd_intf_pins dbg_pulse_cond_slow_s0] [get_bd_intf_pins ip_mux16_2_if_0/dbg_pulse_cond_slow_s0]
  connect_bd_intf_net -intf_net ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0 [get_bd_intf_pins dbg_invert_and_offset_s0] [get_bd_intf_pins ip_mux16_2_if_0/dbg_invert_and_offset_s0]
  connect_bd_intf_net -intf_net ip_dbg_term_pha_0_dbg_pha_m0 [get_bd_intf_pins ip_dbg_term_pha_0/dbg_pha_m0] [get_bd_intf_pins ip_mux16_2_if_0/dbg_pha_s0]
  connect_bd_intf_net -intf_net ip_dbg_term_pulse_co_0_dbg_pulse_cond_fast [get_bd_intf_pins ip_dbg_term_pulse_co_0/dbg_pulse_cond_fast] [get_bd_intf_pins ip_mux16_2_if_0/dbg_pulse_cond_fast_s0]

  # Create port connections
  connect_bd_net -net clk_1 [get_bd_pins clk] [get_bd_pins ip_scope_0/signaldomain_clk]
  connect_bd_net -net ip_mux16_2_if_0_outp1 [get_bd_pins ip_mux16_2_if_0/outp1] [get_bd_pins ip_scope_0/ch1]
  connect_bd_net -net ip_mux16_2_if_0_outp2 [get_bd_pins ip_mux16_2_if_0/outp2] [get_bd_pins ip_scope_0/ch2]
  connect_bd_net -net ip_mux16_2_if_0_trig [get_bd_pins ip_mux16_2_if_0/trig] [get_bd_pins ip_scope_0/ch_trigger]
  connect_bd_net -net s00_axi_aclk_1 [get_bd_pins s00_axi_aclk] [get_bd_pins ip_mux16_2_if_0/s00_axi_aclk] [get_bd_pins ip_scope_0/axibusdomain_clk]
  connect_bd_net -net s00_axi_aresetn_1 [get_bd_pins s00_axi_aresetn] [get_bd_pins ip_mux16_2_if_0/s00_axi_aresetn] [get_bd_pins ip_scope_0/axibusdomain_aresetn]
  connect_bd_net -net scope_full [get_bd_pins full] [get_bd_pins ip_scope_0/full]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: pulse_offseting
proc create_hier_cell_pulse_offseting { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_pulse_offseting() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI

  create_bd_intf_pin -mode Master -vlnv iaea.org:interface:dbg_invert_and_offset_rtl:1.0 dbg_invert_and_offset_m0


  # Create pins
  create_bd_pin -dir I -from 13 -to 0 adc_data
  create_bd_pin -dir I clk
  create_bd_pin -dir O -from 15 -to 0 -type data outp
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn

  # Create instance: invert_and_offset_0, and set properties
  set invert_and_offset_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:invert_and_offset:1.0 invert_and_offset_0 ]

  # Create instance: ip_dbg_invert_and_of_0, and set properties
  set ip_dbg_invert_and_of_0 [ create_bd_cell -type ip -vlnv iaea.org:user:ip_dbg_invert_and_offset:1.0 ip_dbg_invert_and_of_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins S00_AXI] [get_bd_intf_pins invert_and_offset_0/S00_AXI]
  connect_bd_intf_net -intf_net ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0 [get_bd_intf_pins dbg_invert_and_offset_m0] [get_bd_intf_pins ip_dbg_invert_and_of_0/dbg_invert_and_offset_m0]

  # Create port connections
  connect_bd_net -net adc_data_1 [get_bd_pins adc_data] [get_bd_pins invert_and_offset_0/inp] [get_bd_pins ip_dbg_invert_and_of_0/adc_data]
  connect_bd_net -net clk_1 [get_bd_pins clk] [get_bd_pins invert_and_offset_0/clk]
  connect_bd_net -net invert_and_offset_0_outp [get_bd_pins outp] [get_bd_pins invert_and_offset_0/outp] [get_bd_pins ip_dbg_invert_and_of_0/outp]
  connect_bd_net -net s00_axi_aclk_1 [get_bd_pins s00_axi_aclk] [get_bd_pins invert_and_offset_0/s00_axi_aclk]
  connect_bd_net -net s00_axi_aresetn_1 [get_bd_pins s00_axi_aresetn] [get_bd_pins invert_and_offset_0/s00_axi_aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: pulse_conditioning_slow
proc create_hier_cell_pulse_conditioning_slow { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_pulse_conditioning_slow() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 axi_clk_domain_s_axi

  create_bd_intf_pin -mode Master -vlnv iaea.org:interface:dbg_pulse_cond_slow_rtl:1.0 dbg_pulse_cond_slow_m0


  # Create pins
  create_bd_pin -dir I -type rst S00_ARESETN
  create_bd_pin -dir I -type clk clk
  create_bd_pin -dir I -type clk clk_cpu
  create_bd_pin -dir I -from 15 -to 0 -type data data_in

  # Create instance: ip_dbg_pulse_cond_sl_0, and set properties
  set ip_dbg_pulse_cond_sl_0 [ create_bd_cell -type ip -vlnv iaea.org:interface:ip_dbg_pulse_cond_slow:1.0 ip_dbg_pulse_cond_sl_0 ]

  # Create instance: ip_shaper_0, and set properties
  set ip_shaper_0 [ create_bd_cell -type ip -vlnv IAEA:IP_Shaper:ip_shaper:4.0 ip_shaper_0 ]

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {16} \
 ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins axi_clk_domain_s_axi] [get_bd_intf_pins ip_shaper_0/axi_clk_domain_s_axi]
  connect_bd_intf_net -intf_net ip_dbg_pulse_cond_sl_0_dbg_pulse_cond_slow_m0 [get_bd_intf_pins dbg_pulse_cond_slow_m0] [get_bd_intf_pins ip_dbg_pulse_cond_sl_0/dbg_pulse_cond_slow_m0]

  # Create port connections
  connect_bd_net -net S00_ARESETN_1 [get_bd_pins S00_ARESETN] [get_bd_pins ip_shaper_0/axi_clk_domain_aresetn]
  connect_bd_net -net clk_1 [get_bd_pins clk] [get_bd_pins ip_shaper_0/filter_clk_domain_clk]
  connect_bd_net -net clk_cpu_1 [get_bd_pins clk_cpu] [get_bd_pins ip_shaper_0/axi_clk_domain_clk]
  connect_bd_net -net data_in_1 [get_bd_pins data_in] [get_bd_pins ip_shaper_0/data_in]
  connect_bd_net -net ip_shaper_0_impulse_out [get_bd_pins ip_dbg_pulse_cond_sl_0/impulse] [get_bd_pins ip_shaper_0/impulse_out]
  connect_bd_net -net ip_shaper_0_rect_out [get_bd_pins ip_dbg_pulse_cond_sl_0/rect] [get_bd_pins ip_shaper_0/rect_out]
  connect_bd_net -net ip_shaper_0_shaper_out [get_bd_pins ip_dbg_pulse_cond_sl_0/shaper] [get_bd_pins ip_shaper_0/shaper_out]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins ip_dbg_pulse_cond_sl_0/blr] [get_bd_pins ip_dbg_pulse_cond_sl_0/dc_stab] [get_bd_pins ip_dbg_pulse_cond_sl_0/dc_stab_acc] [get_bd_pins xlconstant_0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ps_mb_0
proc create_hier_cell_ps_mb_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ps_mb_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M00_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M01_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M02_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M03_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M05_AXI


  # Create pins
  create_bd_pin -dir I -type clk Clk
  create_bd_pin -dir O -from 0 -to 0 -type rst S00_ARESETN
  create_bd_pin -dir I dcm_locked
  create_bd_pin -dir I -from 0 -to 0 -type intr intr
  create_bd_pin -dir I -type rst reset

  # Create instance: axi_intc_0, and set properties
  set axi_intc_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 axi_intc_0 ]

  # Create instance: mdm_1, and set properties
  set mdm_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:mdm:3.2 mdm_1 ]

  # Create instance: microblaze_0, and set properties
  set microblaze_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:11.0 microblaze_0 ]
  set_property -dict [ list \
   CONFIG.C_DEBUG_ENABLED {1} \
   CONFIG.C_D_AXI {1} \
   CONFIG.C_D_LMB {1} \
   CONFIG.C_I_LMB {1} \
 ] $microblaze_0

  # Create instance: microblaze_0_axi_periph, and set properties
  set microblaze_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 microblaze_0_axi_periph ]
  set_property -dict [ list \
   CONFIG.NUM_MI {6} \
 ] $microblaze_0_axi_periph

  # Create instance: microblaze_0_local_memory
  create_hier_cell_microblaze_0_local_memory $hier_obj microblaze_0_local_memory

  # Create instance: rst_clk_wiz_0_120M, and set properties
  set rst_clk_wiz_0_120M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_clk_wiz_0_120M ]

  # Create interface connections
  connect_bd_intf_net -intf_net axi_intc_0_interrupt [get_bd_intf_pins axi_intc_0/interrupt] [get_bd_intf_pins microblaze_0/INTERRUPT]
  connect_bd_intf_net -intf_net microblaze_0_M_AXI_DP [get_bd_intf_pins microblaze_0/M_AXI_DP] [get_bd_intf_pins microblaze_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M00_AXI [get_bd_intf_pins M00_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M01_AXI [get_bd_intf_pins M01_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M01_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M02_AXI [get_bd_intf_pins M02_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M02_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M03_AXI [get_bd_intf_pins M03_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M03_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M04_AXI [get_bd_intf_pins axi_intc_0/s_axi] [get_bd_intf_pins microblaze_0_axi_periph/M04_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M05_AXI [get_bd_intf_pins M05_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M05_AXI]
  connect_bd_intf_net -intf_net microblaze_0_debug [get_bd_intf_pins mdm_1/MBDEBUG_0] [get_bd_intf_pins microblaze_0/DEBUG]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_1 [get_bd_intf_pins microblaze_0/DLMB] [get_bd_intf_pins microblaze_0_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_1 [get_bd_intf_pins microblaze_0/ILMB] [get_bd_intf_pins microblaze_0_local_memory/ILMB]

  # Create port connections
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins dcm_locked] [get_bd_pins rst_clk_wiz_0_120M/dcm_locked]
  connect_bd_net -net intr_1 [get_bd_pins intr] [get_bd_pins axi_intc_0/intr]
  connect_bd_net -net mdm_1_debug_sys_rst [get_bd_pins mdm_1/Debug_SYS_Rst] [get_bd_pins rst_clk_wiz_0_120M/mb_debug_sys_rst]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins Clk] [get_bd_pins axi_intc_0/s_axi_aclk] [get_bd_pins microblaze_0/Clk] [get_bd_pins microblaze_0_axi_periph/ACLK] [get_bd_pins microblaze_0_axi_periph/M00_ACLK] [get_bd_pins microblaze_0_axi_periph/M01_ACLK] [get_bd_pins microblaze_0_axi_periph/M02_ACLK] [get_bd_pins microblaze_0_axi_periph/M03_ACLK] [get_bd_pins microblaze_0_axi_periph/M04_ACLK] [get_bd_pins microblaze_0_axi_periph/M05_ACLK] [get_bd_pins microblaze_0_axi_periph/S00_ACLK] [get_bd_pins microblaze_0_local_memory/LMB_Clk] [get_bd_pins rst_clk_wiz_0_120M/slowest_sync_clk]
  connect_bd_net -net reset_1 [get_bd_pins reset] [get_bd_pins rst_clk_wiz_0_120M/ext_reset_in]
  connect_bd_net -net rst_clk_wiz_0_120M_bus_struct_reset [get_bd_pins microblaze_0_local_memory/SYS_Rst] [get_bd_pins rst_clk_wiz_0_120M/bus_struct_reset]
  connect_bd_net -net rst_clk_wiz_0_120M_mb_reset [get_bd_pins microblaze_0/Reset] [get_bd_pins rst_clk_wiz_0_120M/mb_reset]
  connect_bd_net -net rst_clk_wiz_0_120M_peripheral_aresetn [get_bd_pins S00_ARESETN] [get_bd_pins axi_intc_0/s_axi_aresetn] [get_bd_pins microblaze_0_axi_periph/ARESETN] [get_bd_pins microblaze_0_axi_periph/M00_ARESETN] [get_bd_pins microblaze_0_axi_periph/M01_ARESETN] [get_bd_pins microblaze_0_axi_periph/M02_ARESETN] [get_bd_pins microblaze_0_axi_periph/M03_ARESETN] [get_bd_pins microblaze_0_axi_periph/M04_ARESETN] [get_bd_pins microblaze_0_axi_periph/M05_ARESETN] [get_bd_pins microblaze_0_axi_periph/S00_ARESETN] [get_bd_pins rst_clk_wiz_0_120M/peripheral_aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: mb_periph_0
proc create_hier_cell_mb_periph_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_mb_periph_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 uart_rtl


  # Create pins
  create_bd_pin -dir I -type clk s_axi_aclk
  create_bd_pin -dir I -type rst s_axi_aresetn

  # Create instance: axi_uartlite_0, and set properties
  set axi_uartlite_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uartlite:2.0 axi_uartlite_0 ]
  set_property -dict [ list \
   CONFIG.C_BAUDRATE {460800} \
   CONFIG.C_S_AXI_ACLK_FREQ_HZ {120000000} \
   CONFIG.UARTLITE_BOARD_INTERFACE {Custom} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_uartlite_0

  # Create interface connections
  connect_bd_intf_net -intf_net axi_uartlite_0_UART [get_bd_intf_pins uart_rtl] [get_bd_intf_pins axi_uartlite_0/UART]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M00_AXI [get_bd_intf_pins S_AXI] [get_bd_intf_pins axi_uartlite_0/S_AXI]

  # Create port connections
  connect_bd_net -net microblaze_0_Clk [get_bd_pins s_axi_aclk] [get_bd_pins axi_uartlite_0/s_axi_aclk]
  connect_bd_net -net rst_clk_wiz_0_120M_peripheral_aresetn [get_bd_pins s_axi_aresetn] [get_bd_pins axi_uartlite_0/s_axi_aresetn]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: dpp_0
proc create_hier_cell_dpp_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_dpp_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI1

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 axi_clk_domain_s_axi

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 axibusdomain_s_axi


  # Create pins
  create_bd_pin -dir I -from 13 -to 0 adc_data
  create_bd_pin -dir I clk
  create_bd_pin -dir O -from 0 -to 0 -type intr full
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn

  # Create instance: pulse_conditioning_slow
  create_hier_cell_pulse_conditioning_slow $hier_obj pulse_conditioning_slow

  # Create instance: pulse_offseting
  create_hier_cell_pulse_offseting $hier_obj pulse_offseting

  # Create instance: scope
  create_hier_cell_scope $hier_obj scope

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI1_1 [get_bd_intf_pins S00_AXI1] [get_bd_intf_pins scope/S00_AXI]
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins S00_AXI] [get_bd_intf_pins pulse_offseting/S00_AXI]
  connect_bd_intf_net -intf_net axi_clk_domain_s_axi_1 [get_bd_intf_pins axi_clk_domain_s_axi] [get_bd_intf_pins pulse_conditioning_slow/axi_clk_domain_s_axi]
  connect_bd_intf_net -intf_net axibusdomain_s_axi_1 [get_bd_intf_pins axibusdomain_s_axi] [get_bd_intf_pins scope/axibusdomain_s_axi]
  connect_bd_intf_net -intf_net ip_dbg_invert_and_of_0_dbg_invert_and_offset_m0 [get_bd_intf_pins pulse_offseting/dbg_invert_and_offset_m0] [get_bd_intf_pins scope/dbg_invert_and_offset_s0]
  connect_bd_intf_net -intf_net pulse_conditioning_slow_dbg_pulse_cond_slow_m0 [get_bd_intf_pins pulse_conditioning_slow/dbg_pulse_cond_slow_m0] [get_bd_intf_pins scope/dbg_pulse_cond_slow_s0]

  # Create port connections
  connect_bd_net -net adc_data_1 [get_bd_pins adc_data] [get_bd_pins pulse_offseting/adc_data]
  connect_bd_net -net clk_1 [get_bd_pins clk] [get_bd_pins pulse_conditioning_slow/clk] [get_bd_pins pulse_offseting/clk] [get_bd_pins scope/clk]
  connect_bd_net -net pulse_offseting_outp [get_bd_pins pulse_conditioning_slow/data_in] [get_bd_pins pulse_offseting/outp]
  connect_bd_net -net s00_axi_aclk_1 [get_bd_pins s00_axi_aclk] [get_bd_pins pulse_conditioning_slow/clk_cpu] [get_bd_pins pulse_offseting/s00_axi_aclk] [get_bd_pins scope/s00_axi_aclk]
  connect_bd_net -net s00_axi_aresetn_1 [get_bd_pins s00_axi_aresetn] [get_bd_pins pulse_conditioning_slow/S00_ARESETN] [get_bd_pins pulse_offseting/s00_axi_aresetn] [get_bd_pins scope/s00_axi_aresetn]
  connect_bd_net -net scope_full [get_bd_pins full] [get_bd_pins scope/full]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set uart_rtl [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 uart_rtl ]


  # Create ports
  set adc_clk [ create_bd_port -dir O -type clk adc_clk ]
  set adc_data [ create_bd_port -dir I -from 13 -to 0 -type data adc_data ]
  set reset [ create_bd_port -dir I -type rst reset ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $reset
  set sys_clock [ create_bd_port -dir I -type clk sys_clock ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {12000000} \
   CONFIG.PHASE {0.000} \
 ] $sys_clock

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKIN1_JITTER_PS {833.33} \
   CONFIG.CLKOUT1_JITTER {469.462} \
   CONFIG.CLKOUT1_PHASE_ERROR {668.310} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {120} \
   CONFIG.CLKOUT2_JITTER {522.315} \
   CONFIG.CLKOUT2_PHASE_ERROR {668.310} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {50} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLKOUT3_JITTER {522.315} \
   CONFIG.CLKOUT3_PHASE_ERROR {668.310} \
   CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {50} \
   CONFIG.CLKOUT3_USED {true} \
   CONFIG.CLK_IN1_BOARD_INTERFACE {sys_clock} \
   CONFIG.CLK_OUT1_PORT {clk_cpu} \
   CONFIG.CLK_OUT2_PORT {clk_dpp} \
   CONFIG.CLK_OUT3_PORT {clk_adc} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {62.500} \
   CONFIG.MMCM_CLKIN1_PERIOD {83.333} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {6.250} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {15} \
   CONFIG.MMCM_CLKOUT2_DIVIDE {15} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.NUM_OUT_CLKS {3} \
   CONFIG.RESET_BOARD_INTERFACE {reset} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $clk_wiz_0

  # Create instance: dpp_0
  create_hier_cell_dpp_0 [current_bd_instance .] dpp_0

  # Create instance: mb_periph_0
  create_hier_cell_mb_periph_0 [current_bd_instance .] mb_periph_0

  # Create instance: ps_mb_0
  create_hier_cell_ps_mb_0 [current_bd_instance .] ps_mb_0

  # Create interface connections
  connect_bd_intf_net -intf_net axi_uartlite_0_UART [get_bd_intf_ports uart_rtl] [get_bd_intf_pins mb_periph_0/uart_rtl]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M00_AXI [get_bd_intf_pins mb_periph_0/S_AXI] [get_bd_intf_pins ps_mb_0/M00_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M01_AXI [get_bd_intf_pins dpp_0/S00_AXI] [get_bd_intf_pins ps_mb_0/M01_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M02_AXI [get_bd_intf_pins dpp_0/S00_AXI1] [get_bd_intf_pins ps_mb_0/M02_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M03_AXI [get_bd_intf_pins dpp_0/axibusdomain_s_axi] [get_bd_intf_pins ps_mb_0/M03_AXI]
  connect_bd_intf_net -intf_net ps_mb_0_M05_AXI [get_bd_intf_pins dpp_0/axi_clk_domain_s_axi] [get_bd_intf_pins ps_mb_0/M05_AXI]

  # Create port connections
  connect_bd_net -net adc_data_1 [get_bd_ports adc_data] [get_bd_pins dpp_0/adc_data]
  connect_bd_net -net clk_wiz_0_clk_adc [get_bd_ports adc_clk] [get_bd_pins clk_wiz_0/clk_adc]
  connect_bd_net -net clk_wiz_0_clk_dpp [get_bd_pins clk_wiz_0/clk_dpp] [get_bd_pins dpp_0/clk]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins clk_wiz_0/locked] [get_bd_pins ps_mb_0/dcm_locked]
  connect_bd_net -net dpp_0_full [get_bd_pins dpp_0/full] [get_bd_pins ps_mb_0/intr]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins clk_wiz_0/clk_cpu] [get_bd_pins dpp_0/s00_axi_aclk] [get_bd_pins mb_periph_0/s_axi_aclk] [get_bd_pins ps_mb_0/Clk]
  connect_bd_net -net reset_1 [get_bd_ports reset] [get_bd_pins clk_wiz_0/reset] [get_bd_pins ps_mb_0/reset]
  connect_bd_net -net rst_clk_wiz_0_120M_peripheral_aresetn [get_bd_pins dpp_0/s00_axi_aresetn] [get_bd_pins mb_periph_0/s_axi_aresetn] [get_bd_pins ps_mb_0/S00_ARESETN]
  connect_bd_net -net sys_clock_1 [get_bd_ports sys_clock] [get_bd_pins clk_wiz_0/clk_in1]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x41200000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs ps_mb_0/axi_intc_0/S_AXI/Reg] SEG_axi_intc_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x40600000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs mb_periph_0/axi_uartlite_0/S_AXI/Reg] SEG_axi_uartlite_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x00000000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs ps_mb_0/microblaze_0_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x00010000 -offset 0x00000000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Instruction] [get_bd_addr_segs ps_mb_0/microblaze_0_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x00010000 -offset 0x44A00000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pulse_offseting/invert_and_offset_0/S00_AXI/S00_AXI_reg] SEG_invert_and_offset_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A20000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/scope/ip_mux16_2_if_0/S00_AXI/S00_AXI_reg] SEG_ip_mux16_2_if_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A10000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/scope/ip_scope_0/axibusdomain_s_axi/reg0] SEG_ip_scope_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x44A30000 [get_bd_addr_spaces ps_mb_0/microblaze_0/Data] [get_bd_addr_segs dpp_0/pulse_conditioning_slow/ip_shaper_0/axi_clk_domain_s_axi/reg0] SEG_ip_shaper_0_reg0


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


