-- (c) Copyright 1995-2021 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:user:ip_dbg_term_pha:1.0
-- IP Revision: 1

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_ip_dbg_term_pha_0_3 IS
  PORT (
    peak_amp_rdy_fast_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    rejectn_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    peak_amp_rdy_slow_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    peak_det_signal_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END design_1_ip_dbg_term_pha_0_3;

ARCHITECTURE design_1_ip_dbg_term_pha_0_3_arch OF design_1_ip_dbg_term_pha_0_3 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_ip_dbg_term_pha_0_3_arch: ARCHITECTURE IS "yes";
  COMPONENT top IS
    PORT (
      peak_amp_rdy_fast_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      rejectn_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      peak_amp_rdy_slow_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      peak_det_signal_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
    );
  END COMPONENT top;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_ip_dbg_term_pha_0_3_arch: ARCHITECTURE IS "top,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_ip_dbg_term_pha_0_3_arch : ARCHITECTURE IS "design_1_ip_dbg_term_pha_0_3,top,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF design_1_ip_dbg_term_pha_0_3_arch: ARCHITECTURE IS "design_1_ip_dbg_term_pha_0_3,top,{x_ipProduct=Vivado 2019.1,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=ip_dbg_term_pha,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF design_1_ip_dbg_term_pha_0_3_arch: ARCHITECTURE IS "package_project";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF peak_det_signal_out: SIGNAL IS "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_det_signal_out";
  ATTRIBUTE X_INTERFACE_INFO OF peak_amp_rdy_slow_out: SIGNAL IS "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_amp_rdy_slow_out";
  ATTRIBUTE X_INTERFACE_INFO OF rejectn_out: SIGNAL IS "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 rejectn_out";
  ATTRIBUTE X_INTERFACE_INFO OF peak_amp_rdy_fast_out: SIGNAL IS "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_amp_rdy_fast_out";
BEGIN
  U0 : top
    PORT MAP (
      peak_amp_rdy_fast_out => peak_amp_rdy_fast_out,
      rejectn_out => rejectn_out,
      peak_amp_rdy_slow_out => peak_amp_rdy_slow_out,
      peak_det_signal_out => peak_det_signal_out
    );
END design_1_ip_dbg_term_pha_0_3_arch;
