-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Feb 13 21:00:06 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-3/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_invert_and_of_0_0/design_1_ip_dbg_invert_and_of_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_dbg_invert_and_of_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_dbg_invert_and_of_0_0 is
  port (
    adc_data : in STD_LOGIC_VECTOR ( 13 downto 0 );
    outp : in STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    outp_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_dbg_invert_and_of_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_dbg_invert_and_of_0_0 : entity is "design_1_ip_dbg_invert_and_of_0_0,top,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_ip_dbg_invert_and_of_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_ip_dbg_invert_and_of_0_0 : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of design_1_ip_dbg_invert_and_of_0_0 : entity is "top,Vivado 2019.1";
end design_1_ip_dbg_invert_and_of_0_0;

architecture STRUCTURE of design_1_ip_dbg_invert_and_of_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^adc_data\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \^outp\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute x_interface_info : string;
  attribute x_interface_info of adc_data_out : signal is "iaea.org:interface:dbg_invert_and_offset:1.0 dbg_invert_and_offset_m0 adc_data";
  attribute x_interface_info of outp_out : signal is "iaea.org:interface:dbg_invert_and_offset:1.0 dbg_invert_and_offset_m0 outp";
begin
  \^adc_data\(13 downto 0) <= adc_data(13 downto 0);
  \^outp\(15 downto 0) <= outp(15 downto 0);
  adc_data_out(15) <= \^adc_data\(13);
  adc_data_out(14 downto 1) <= \^adc_data\(13 downto 0);
  adc_data_out(0) <= \<const0>\;
  outp_out(15 downto 0) <= \^outp\(15 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
end STRUCTURE;
