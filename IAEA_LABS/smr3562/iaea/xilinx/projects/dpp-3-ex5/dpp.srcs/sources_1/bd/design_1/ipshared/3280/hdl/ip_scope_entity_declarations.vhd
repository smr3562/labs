-------------------------------------------------------------------
-- System Generator version 2018.3 VHDL source file.
--
-- Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity ip_scope_xlAsynRegister is

   generic (d_width          : integer := 5;          -- Width of d input
            init_value       : bit_vector := b"00");  -- Binary init value string

   port (d     : in std_logic_vector (d_width-1 downto 0);
         rst   : in std_logic_vector(0 downto 0) := "0";
         en    : in std_logic_vector(0 downto 0) := "1";
         d_ce  : in std_logic;
         d_clk : in std_logic;
         q_ce  : in std_logic;
         q_clk : in std_logic;
         q     : out std_logic_vector (d_width-1 downto 0));

end ip_scope_xlAsynRegister;

architecture behavior of ip_scope_xlAsynRegister is

   component synth_reg_w_init
      generic (width      : integer;
               init_index : integer;
               init_value : bit_vector;
               latency    : integer);
      port (i   : in std_logic_vector(width-1 downto 0);
            ce  : in std_logic;
            clr : in std_logic;
            clk : in std_logic;
            o   : out std_logic_vector(width-1 downto 0));
   end component; -- end synth_reg_w_init

   signal internal_d_clr      : std_logic;
   signal internal_d_ce       : std_logic;
   signal internal_q_clr      : std_logic;
   signal internal_q_ce       : std_logic;

   signal d1_net              : std_logic_vector (d_width-1 downto 0);
   signal d2_net              : std_logic_vector (d_width-1 downto 0);
   signal d3_net              : std_logic_vector (d_width-1 downto 0);

begin

   internal_d_clr <= rst(0) and d_ce;
   internal_d_ce  <= en(0) and d_ce;
   -- drive default values on enable and clear ports
   internal_q_clr <= '0' and q_ce;
   internal_q_ce  <= '1' and q_ce;

   -- Synthesizable behavioral model
   synth_reg_inst_0 : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d,
                ce  => internal_d_ce,
                clr => internal_d_clr,
                clk => d_clk,
                o   => d1_net);

   synth_reg_inst_1 : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d1_net,
                ce  => internal_q_ce,
                clr => internal_q_clr,
                clk => q_clk,
                o   => d2_net);

   synth_reg_inst_2 : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d2_net,
                ce  => internal_q_ce,
                clr => internal_q_clr,
                clk => q_clk,
                o   => d3_net);

   synth_reg_inst_3 : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d3_net,
                ce  => internal_q_ce,
                clr => internal_q_clr,
                clk => q_clk,
                o   => q);

end architecture behavior;


library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

--$Header: /devl/xcs/repo/env/Jobs/sysgen/src/xbs/blocks/xlconvert/hdl/xlconvert.vhd,v 1.1 2004/11/22 00:17:30 rosty Exp $
---------------------------------------------------------------------
--
--  Filename      : xlconvert.vhd
--
--  Description   : VHDL description of a fixed point converter block that
--                  converts the input to a new output type.

--
---------------------------------------------------------------------


---------------------------------------------------------------------
--
--  Entity        : xlconvert
--
--  Architecture  : behavior
--
--  Description   : Top level VHDL description of fixed point conver block.
--
---------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity convert_func_call_ip_scope_xlconvert is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        result : out std_logic_vector (dout_width-1 downto 0));
end convert_func_call_ip_scope_xlconvert ;

architecture behavior of convert_func_call_ip_scope_xlconvert is
begin
    -- Convert to output type and do saturation arith.
    result <= convert_type(din, din_width, din_bin_pt, din_arith,
                           dout_width, dout_bin_pt, dout_arith,
                           quantization, overflow);
end behavior;


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity ip_scope_xlconvert  is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        en_width     : integer := 1;
        en_bin_pt    : integer := 0;
        en_arith     : integer := xlUnsigned;
        bool_conversion : integer :=0;           -- if one, convert ufix_1_0 to
                                                 -- bool
        latency      : integer := 0;             -- Ouput delay clk cycles
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        en  : in std_logic_vector (en_width-1 downto 0);
        ce  : in std_logic;
        clr : in std_logic;
        clk : in std_logic;
        dout : out std_logic_vector (dout_width-1 downto 0));

end ip_scope_xlconvert ;

architecture behavior of ip_scope_xlconvert  is

    component synth_reg
        generic (width       : integer;
                 latency     : integer);
        port (i       : in std_logic_vector(width-1 downto 0);
              ce      : in std_logic;
              clr     : in std_logic;
              clk     : in std_logic;
              o       : out std_logic_vector(width-1 downto 0));
    end component;

    component convert_func_call_ip_scope_xlconvert 
        generic (
            din_width    : integer := 16;            -- Width of input
            din_bin_pt   : integer := 4;             -- Binary point of input
            din_arith    : integer := xlUnsigned;    -- Type of arith of input
            dout_width   : integer := 8;             -- Width of output
            dout_bin_pt  : integer := 2;             -- Binary point of output
            dout_arith   : integer := xlUnsigned;    -- Type of arith of output
            quantization : integer := xlTruncate;    -- xlRound or xlTruncate
            overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
        port (
            din : in std_logic_vector (din_width-1 downto 0);
            result : out std_logic_vector (dout_width-1 downto 0));
    end component;


    -- synthesis translate_off
--    signal real_din, real_dout : real;    -- For debugging info ports
    -- synthesis translate_on
    signal result : std_logic_vector(dout_width-1 downto 0);
    signal internal_ce : std_logic;

begin

    -- Debugging info for internal full precision variables
    -- synthesis translate_off
--     real_din <= to_real(din, din_bin_pt, din_arith);
--     real_dout <= to_real(dout, dout_bin_pt, dout_arith);
    -- synthesis translate_on

    internal_ce <= ce and en(0);

    bool_conversion_generate : if (bool_conversion = 1)
    generate
      result <= din;
    end generate; --bool_conversion_generate

    std_conversion_generate : if (bool_conversion = 0)
    generate
      -- Workaround for XST bug
      convert : convert_func_call_ip_scope_xlconvert 
        generic map (
          din_width   => din_width,
          din_bin_pt  => din_bin_pt,
          din_arith   => din_arith,
          dout_width  => dout_width,
          dout_bin_pt => dout_bin_pt,
          dout_arith  => dout_arith,
          quantization => quantization,
          overflow     => overflow)
        port map (
          din => din,
          result => result);
    end generate; --std_conversion_generate

    latency_test : if (latency > 0) generate
        reg : synth_reg
            generic map (
              width => dout_width,
              latency => latency
            )
            port map (
              i => result,
              ce => internal_ce,
              clr => clr,
              clk => clk,
              o => dout
            );
    end generate;

    latency0 : if (latency = 0)
    generate
        dout <= result;
    end generate latency0;

end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
--
--  Filename      : xlslice.vhd
--
--  Description   : VHDL description of a block that sets the output to a
--                  specified range of the input bits. The output is always
--                  set to an unsigned type with it's binary point at zero.
--
---------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity ip_scope_xlslice is
    generic (
        new_msb      : integer := 9;           -- position of new msb
        new_lsb      : integer := 1;           -- position of new lsb
        x_width      : integer := 16;          -- Width of x input
        y_width      : integer := 8);          -- Width of y output
    port (
        x : in std_logic_vector (x_width-1 downto 0);
        y : out std_logic_vector (y_width-1 downto 0));
end ip_scope_xlslice;

architecture behavior of ip_scope_xlslice is
begin
    y <= x(new_msb downto new_lsb);
end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_cf6db46343 is
  port (
    input_port : in std_logic_vector((16 - 1) downto 0);
    output_port : out std_logic_vector((16 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_cf6db46343;
architecture behavior of sysgen_reinterpret_cf6db46343
is
  signal input_port_1_40: unsigned((16 - 1) downto 0);
  signal output_port_5_5_force: signed((16 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port_5_5_force <= unsigned_to_signed(input_port_1_40);
  output_port <= signed_to_std_logic_vector(output_port_5_5_force);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_749ec0155a is
  port (
    input_port : in std_logic_vector((11 - 1) downto 0);
    output_port : out std_logic_vector((11 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_749ec0155a;
architecture behavior of sysgen_reinterpret_749ec0155a
is
  signal input_port_1_40: unsigned((11 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port <= unsigned_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_1f1ea39045 is
  port (
    input_port : in std_logic_vector((32 - 1) downto 0);
    output_port : out std_logic_vector((32 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_1f1ea39045;
architecture behavior of sysgen_reinterpret_1f1ea39045
is
  signal input_port_1_40: unsigned((32 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port <= unsigned_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_concat_f45ca58bd3 is
  port (
    in0 : in std_logic_vector((16 - 1) downto 0);
    in1 : in std_logic_vector((16 - 1) downto 0);
    y : out std_logic_vector((32 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_concat_f45ca58bd3;
architecture behavior of sysgen_concat_f45ca58bd3
is
  signal in0_1_23: unsigned((16 - 1) downto 0);
  signal in1_1_27: unsigned((16 - 1) downto 0);
  signal y_2_1_concat: unsigned((32 - 1) downto 0);
begin
  in0_1_23 <= std_logic_vector_to_unsigned(in0);
  in1_1_27 <= std_logic_vector_to_unsigned(in1);
  y_2_1_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(in0_1_23) & unsigned_to_std_logic_vector(in1_1_27));
  y <= unsigned_to_std_logic_vector(y_2_1_concat);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
--
--  Filename      : xlregister.vhd
--
--  Description   : VHDL description of an arbitrary wide register.
--                  Unlike the delay block, an initial value is
--                  specified and is considered valid at the start
--                  of simulation.  The register is only one word
--                  deep.
--
--  Mod. History  : Removed valid bit logic from wrapper.
--                : Changed VHDL to use a bit_vector generic for its
--
---------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity ip_scope_xlregister is

   generic (d_width          : integer := 5;          -- Width of d input
            init_value       : bit_vector := b"00");  -- Binary init value string

   port (d   : in std_logic_vector (d_width-1 downto 0);
         rst : in std_logic_vector(0 downto 0) := "0";
         en  : in std_logic_vector(0 downto 0) := "1";
         ce  : in std_logic;
         clk : in std_logic;
         q   : out std_logic_vector (d_width-1 downto 0));

end ip_scope_xlregister;

architecture behavior of ip_scope_xlregister is

   component synth_reg_w_init
      generic (width      : integer;
               init_index : integer;
               init_value : bit_vector;
               latency    : integer);
      port (i   : in std_logic_vector(width-1 downto 0);
            ce  : in std_logic;
            clr : in std_logic;
            clk : in std_logic;
            o   : out std_logic_vector(width-1 downto 0));
   end component; -- end synth_reg_w_init

   -- synthesis translate_off
   signal real_d, real_q           : real;    -- For debugging info ports
   -- synthesis translate_on
   signal internal_clr             : std_logic;
   signal internal_ce              : std_logic;

begin

   internal_clr <= rst(0) and ce;
   internal_ce  <= en(0) and ce;

   -- Synthesizable behavioral model
   synth_reg_inst : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d,
                ce  => internal_ce,
                clr => internal_clr,
                clk => clk,
                o   => q);

end architecture behavior;


library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_f4d98d0287 is
  port (
    op : out std_logic_vector((11 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_f4d98d0287;
architecture behavior of sysgen_constant_f4d98d0287
is
begin
  op <= "00000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_1c29ff0c58 is
  port (
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_1c29ff0c58;
architecture behavior of sysgen_constant_1c29ff0c58
is
begin
  op <= "1";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_relational_f845914c88 is
  port (
    a : in std_logic_vector((11 - 1) downto 0);
    b : in std_logic_vector((11 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_relational_f845914c88;
architecture behavior of sysgen_relational_f845914c88
is
  signal a_1_31: unsigned((11 - 1) downto 0);
  signal b_1_34: unsigned((11 - 1) downto 0);
  type array_type_op_mem_37_22 is array (0 to (1 - 1)) of boolean;
  signal op_mem_37_22: array_type_op_mem_37_22 := (
    0 => false);
  signal op_mem_37_22_front_din: boolean;
  signal op_mem_37_22_back: boolean;
  signal op_mem_37_22_push_front_pop_back_en: std_logic;
  signal result_12_3_rel: boolean;
begin
  a_1_31 <= std_logic_vector_to_unsigned(a);
  b_1_34 <= std_logic_vector_to_unsigned(b);
  op_mem_37_22_back <= op_mem_37_22(0);
  proc_op_mem_37_22: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_37_22_push_front_pop_back_en = '1')) then
        op_mem_37_22(0) <= op_mem_37_22_front_din;
      end if;
    end if;
  end process proc_op_mem_37_22;
  result_12_3_rel <= a_1_31 = b_1_34;
  op_mem_37_22_front_din <= result_12_3_rel;
  op_mem_37_22_push_front_pop_back_en <= '1';
  op <= boolean_to_vector(op_mem_37_22_back);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_mcode_block_46de72b52d is
  port (
    x : in std_logic_vector((16 - 1) downto 0);
    threshold : in std_logic_vector((16 - 1) downto 0);
    ena : in std_logic_vector((1 - 1) downto 0);
    clear : in std_logic_vector((1 - 1) downto 0);
    addr : out std_logic_vector((11 - 1) downto 0);
    we : out std_logic_vector((1 - 1) downto 0);
    full : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_mcode_block_46de72b52d;
architecture behavior of sysgen_mcode_block_46de72b52d
is
  signal x_1_56: signed((16 - 1) downto 0);
  signal threshold_1_59: signed((16 - 1) downto 0);
  signal ena_1_70: boolean;
  signal clear_1_75: boolean;
  signal state_4_23_next: unsigned((3 - 1) downto 0);
  signal state_4_23: unsigned((3 - 1) downto 0) := "000";
  signal full_i_5_24_next: unsigned((1 - 1) downto 0);
  signal full_i_5_24: unsigned((1 - 1) downto 0) := "0";
  signal addr_i_6_24: unsigned((11 - 1) downto 0) := "00000000000";
  signal addr_i_6_24_rst: std_logic;
  signal we_i_7_22_next: boolean;
  signal we_i_7_22: boolean := false;
  signal we_i_7_22_rst: std_logic;
  signal wm_8_20_next: boolean;
  signal wm_8_20: boolean := false;
  signal rel_15_8: boolean;
  signal addr_i_join_15_5: unsigned((12 - 1) downto 0);
  signal addr_i_join_15_5_rst: std_logic;
  signal rel_22_8: boolean;
  signal we_i_join_22_5: boolean;
  signal we_i_join_22_5_rst: std_logic;
  signal rel_29_8: boolean;
  signal state_join_29_5: unsigned((3 - 1) downto 0);
  signal rel_39_16: boolean;
  signal state_join_39_13: unsigned((3 - 1) downto 0);
  signal rel_45_16: boolean;
  signal state_join_45_13: unsigned((3 - 1) downto 0);
  signal rel_52_16: boolean;
  signal state_join_52_13: unsigned((3 - 1) downto 0);
  signal state_join_33_5: unsigned((3 - 1) downto 0);
  signal wm_join_33_5: boolean;
  signal full_i_join_33_5: unsigned((1 - 1) downto 0);
begin
  x_1_56 <= std_logic_vector_to_signed(x);
  threshold_1_59 <= std_logic_vector_to_signed(threshold);
  ena_1_70 <= ((ena) = "1");
  clear_1_75 <= ((clear) = "1");
  proc_state_4_23: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        state_4_23 <= state_4_23_next;
      end if;
    end if;
  end process proc_state_4_23;
  proc_full_i_5_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        full_i_5_24 <= full_i_5_24_next;
      end if;
    end if;
  end process proc_full_i_5_24;
  proc_addr_i_6_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (addr_i_6_24_rst = '1')) then
        addr_i_6_24 <= "00000000000";
      elsif (ce = '1') then 
        addr_i_6_24 <= addr_i_6_24 + std_logic_vector_to_unsigned("00000000001");
      end if;
    end if;
  end process proc_addr_i_6_24;
  proc_we_i_7_22: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (we_i_7_22_rst = '1')) then
        we_i_7_22 <= false;
      elsif (ce = '1') then 
        we_i_7_22 <= we_i_7_22_next;
      end if;
    end if;
  end process proc_we_i_7_22;
  proc_wm_8_20: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        wm_8_20 <= wm_8_20_next;
      end if;
    end if;
  end process proc_wm_8_20;
  rel_15_8 <= wm_8_20 = false;
  proc_if_15_5: process (addr_i_6_24, rel_15_8)
  is
  begin
    if rel_15_8 then
      addr_i_join_15_5_rst <= '1';
    else 
      addr_i_join_15_5_rst <= '0';
    end if;
  end process proc_if_15_5;
  rel_22_8 <= wm_8_20 = false;
  proc_if_22_5: process (rel_22_8)
  is
  begin
    if rel_22_8 then
      we_i_join_22_5_rst <= '1';
    else 
      we_i_join_22_5_rst <= '0';
    end if;
    we_i_join_22_5 <= true;
  end process proc_if_22_5;
  rel_29_8 <= ena_1_70 = false;
  proc_if_29_5: process (rel_29_8, state_4_23)
  is
  begin
    if rel_29_8 then
      state_join_29_5 <= std_logic_vector_to_unsigned("000");
    else 
      state_join_29_5 <= state_4_23;
    end if;
  end process proc_if_29_5;
  rel_39_16 <= x_1_56 > threshold_1_59;
  proc_if_39_13: process (rel_39_16, state_join_29_5)
  is
  begin
    if rel_39_16 then
      state_join_39_13 <= std_logic_vector_to_unsigned("010");
    else 
      state_join_39_13 <= state_join_29_5;
    end if;
  end process proc_if_39_13;
  rel_45_16 <= addr_i_6_24 = std_logic_vector_to_unsigned("11111111111");
  proc_if_45_13: process (rel_45_16, state_join_29_5)
  is
  begin
    if rel_45_16 then
      state_join_45_13 <= std_logic_vector_to_unsigned("011");
    else 
      state_join_45_13 <= state_join_29_5;
    end if;
  end process proc_if_45_13;
  rel_52_16 <= clear_1_75 = true;
  proc_if_52_13: process (rel_52_16, state_join_29_5)
  is
  begin
    if rel_52_16 then
      state_join_52_13 <= std_logic_vector_to_unsigned("001");
    else 
      state_join_52_13 <= state_join_29_5;
    end if;
  end process proc_if_52_13;
  proc_switch_33_5: process (state_join_29_5, state_join_39_13, state_join_45_13, state_join_52_13)
  is
  begin
    case state_join_29_5 is 
      when "000" =>
        state_join_33_5 <= std_logic_vector_to_unsigned("001");
        wm_join_33_5 <= false;
        full_i_join_33_5 <= std_logic_vector_to_unsigned("0");
      when "001" =>
        state_join_33_5 <= state_join_39_13;
        wm_join_33_5 <= false;
        full_i_join_33_5 <= std_logic_vector_to_unsigned("0");
      when "010" =>
        state_join_33_5 <= state_join_45_13;
        wm_join_33_5 <= true;
        full_i_join_33_5 <= std_logic_vector_to_unsigned("0");
      when "011" =>
        state_join_33_5 <= state_join_52_13;
        wm_join_33_5 <= false;
        full_i_join_33_5 <= std_logic_vector_to_unsigned("1");
      when others =>
        state_join_33_5 <= std_logic_vector_to_unsigned("000");
        wm_join_33_5 <= false;
        full_i_join_33_5 <= std_logic_vector_to_unsigned("0");
    end case;
  end process proc_switch_33_5;
  state_4_23_next <= state_join_33_5;
  full_i_5_24_next <= full_i_join_33_5;
  addr_i_6_24_rst <= addr_i_join_15_5_rst;
  we_i_7_22_next <= true;
  we_i_7_22_rst <= we_i_join_22_5_rst;
  wm_8_20_next <= wm_join_33_5;
  addr <= unsigned_to_std_logic_vector(addr_i_6_24);
  we <= boolean_to_vector(we_i_7_22);
  full <= unsigned_to_std_logic_vector(full_i_5_24);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_mcode_block_2be3aaba4a is
  port (
    x : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((1 - 1) downto 0);
    re : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_mcode_block_2be3aaba4a;
architecture behavior of sysgen_mcode_block_2be3aaba4a
is
  signal x_1_38: unsigned((1 - 1) downto 0);
  signal ff_2_17_next: unsigned((1 - 1) downto 0);
  signal ff_2_17: unsigned((1 - 1) downto 0) := "0";
  signal rel_5_5: boolean;
  signal rel_5_15: boolean;
  signal bool_5_5: boolean;
  signal re_join_5_2: boolean;
begin
  x_1_38 <= std_logic_vector_to_unsigned(x);
  proc_ff_2_17: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if (ce = '1') then
        ff_2_17 <= ff_2_17_next;
      end if;
    end if;
  end process proc_ff_2_17;
  rel_5_5 <= ff_2_17 = std_logic_vector_to_unsigned("0");
  rel_5_15 <= x_1_38 = std_logic_vector_to_unsigned("1");
  bool_5_5 <= rel_5_5 and rel_5_15;
  proc_if_5_2: process (bool_5_5)
  is
  begin
    if bool_5_5 then
      re_join_5_2 <= true;
    else 
      re_join_5_2 <= false;
    end if;
  end process proc_if_5_2;
  ff_2_17_next <= x_1_38;
  y <= unsigned_to_std_logic_vector(ff_2_17);
  re <= boolean_to_vector(re_join_5_2);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_0b927c7559 is
  port (
    input_port : in std_logic_vector((1 - 1) downto 0);
    output_port : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_0b927c7559;
architecture behavior of sysgen_reinterpret_0b927c7559
is
  signal input_port_1_40: unsigned((1 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port <= unsigned_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_30d70cf429 is
  port (
    input_port : in std_logic_vector((16 - 1) downto 0);
    output_port : out std_logic_vector((16 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_30d70cf429;
architecture behavior of sysgen_reinterpret_30d70cf429
is
  signal input_port_1_40: unsigned((16 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port <= unsigned_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity axibusdomain_axi_lite_interface is 
    port(
        r7_clear : out std_logic_vector(31 downto 0);
        r6_delay : out std_logic_vector(31 downto 0);
        r5_enable : out std_logic_vector(31 downto 0);
        r4_threshold : out std_logic_vector(31 downto 0);
        r3_dina : out std_logic_vector(31 downto 0);
        r2_wea : out std_logic_vector(31 downto 0);
        r1_addra : out std_logic_vector(31 downto 0);
        r8_full : in std_logic_vector(31 downto 0);
        r9_douta : in std_logic_vector(31 downto 0);
        axibusdomain_clk : out std_logic;
        axibusdomain_aclk : in std_logic;
        axibusdomain_aresetn : in std_logic;
        axibusdomain_s_axi_awaddr : in std_logic_vector(6-1 downto 0);
        axibusdomain_s_axi_awvalid : in std_logic;
        axibusdomain_s_axi_awready : out std_logic;
        axibusdomain_s_axi_wdata : in std_logic_vector(32-1 downto 0);
        axibusdomain_s_axi_wstrb : in std_logic_vector(32/8-1 downto 0);
        axibusdomain_s_axi_wvalid : in std_logic;
        axibusdomain_s_axi_wready : out std_logic;
        axibusdomain_s_axi_bresp : out std_logic_vector(1 downto 0);
        axibusdomain_s_axi_bvalid : out std_logic;
        axibusdomain_s_axi_bready : in std_logic;
        axibusdomain_s_axi_araddr : in std_logic_vector(6-1 downto 0);
        axibusdomain_s_axi_arvalid : in std_logic;
        axibusdomain_s_axi_arready : out std_logic;
        axibusdomain_s_axi_rdata : out std_logic_vector(32-1 downto 0);
        axibusdomain_s_axi_rresp : out std_logic_vector(1 downto 0);
        axibusdomain_s_axi_rvalid : out std_logic;
        axibusdomain_s_axi_rready : in std_logic
    );
end axibusdomain_axi_lite_interface;
architecture structural of axibusdomain_axi_lite_interface is 
component axibusdomain_axi_lite_interface_verilog is
    port(
        r7_clear : out std_logic_vector(31 downto 0);
        r6_delay : out std_logic_vector(31 downto 0);
        r5_enable : out std_logic_vector(31 downto 0);
        r4_threshold : out std_logic_vector(31 downto 0);
        r3_dina : out std_logic_vector(31 downto 0);
        r2_wea : out std_logic_vector(31 downto 0);
        r1_addra : out std_logic_vector(31 downto 0);
        r8_full : in std_logic_vector(31 downto 0);
        r9_douta : in std_logic_vector(31 downto 0);
        axibusdomain_clk : out std_logic;
        axibusdomain_aclk : in std_logic;
        axibusdomain_aresetn : in std_logic;
        axibusdomain_s_axi_awaddr : in std_logic_vector(6-1 downto 0);
        axibusdomain_s_axi_awvalid : in std_logic;
        axibusdomain_s_axi_awready : out std_logic;
        axibusdomain_s_axi_wdata : in std_logic_vector(32-1 downto 0);
        axibusdomain_s_axi_wstrb : in std_logic_vector(32/8-1 downto 0);
        axibusdomain_s_axi_wvalid : in std_logic;
        axibusdomain_s_axi_wready : out std_logic;
        axibusdomain_s_axi_bresp : out std_logic_vector(1 downto 0);
        axibusdomain_s_axi_bvalid : out std_logic;
        axibusdomain_s_axi_bready : in std_logic;
        axibusdomain_s_axi_araddr : in std_logic_vector(6-1 downto 0);
        axibusdomain_s_axi_arvalid : in std_logic;
        axibusdomain_s_axi_arready : out std_logic;
        axibusdomain_s_axi_rdata : out std_logic_vector(32-1 downto 0);
        axibusdomain_s_axi_rresp : out std_logic_vector(1 downto 0);
        axibusdomain_s_axi_rvalid : out std_logic;
        axibusdomain_s_axi_rready : in std_logic
    );
end component;
begin
inst : axibusdomain_axi_lite_interface_verilog
    port map(
    r7_clear => r7_clear,
    r6_delay => r6_delay,
    r5_enable => r5_enable,
    r4_threshold => r4_threshold,
    r3_dina => r3_dina,
    r2_wea => r2_wea,
    r1_addra => r1_addra,
    r8_full => r8_full,
    r9_douta => r9_douta,
    axibusdomain_clk => axibusdomain_clk,
    axibusdomain_aclk => axibusdomain_aclk,
    axibusdomain_aresetn => axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr => axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wdata => axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb => axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp => axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr => axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata => axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp => axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
    axibusdomain_s_axi_rready => axibusdomain_s_axi_rready
);
end structural;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
 --
 --  Filename      : xlcounter_rst.vhd
 --
 --  Created       : 1/31/01
 --  Modified      :
 --
 --  Description   : VHDL wrapper for a counter. This wrapper
 --                  uses the Binary Counter CoreGenerator core.
 --
 ---------------------------------------------------------------------
 
 
 ---------------------------------------------------------------------
 --
 --  Entity        : xlcounter
 --
 --  Architecture  : behavior
 --
 --  Description   : Top level VHDL description of a counter.
 --
 ---------------------------------------------------------------------
 
 library IEEE;
 use IEEE.std_logic_1164.all;

entity ip_scope_xlcounter_free is 
   generic (
     core_name0: string := "";
     op_width: integer := 5;
     op_arith: integer := xlSigned
   );
   port (
     ce: in std_logic;
     clr: in std_logic;
     clk: in std_logic;
     op: out std_logic_vector(op_width - 1 downto 0);
     up: in std_logic_vector(0 downto 0) := (others => '0');
     load: in std_logic_vector(0 downto 0) := (others => '0');
     din: in std_logic_vector(op_width - 1 downto 0) := (others => '0');
     en: in std_logic_vector(0 downto 0);
     rst: in std_logic_vector(0 downto 0)
   );
 end ip_scope_xlcounter_free;
 
 architecture behavior of ip_scope_xlcounter_free is


 component ip_scope_c_counter_binary_v12_0_i0
    port ( 
      clk: in std_logic;
      ce: in std_logic;
      SINIT: in std_logic;
      load: in std_logic;
      l: in std_logic_vector(op_width - 1 downto 0);
      q: out std_logic_vector(op_width - 1 downto 0) 
 		  ); 
 end component;

-- synthesis translate_off
   constant zeroVec: std_logic_vector(op_width - 1 downto 0) := (others => '0');
   constant oneVec: std_logic_vector(op_width - 1 downto 0) := (others => '1');
   constant zeroStr: string(1 to op_width) :=
     std_logic_vector_to_bin_string(zeroVec);
   constant oneStr: string(1 to op_width) :=
     std_logic_vector_to_bin_string(oneVec);
 -- synthesis translate_on
 
   signal core_sinit: std_logic;
   signal core_ce: std_logic;
   signal op_net: std_logic_vector(op_width - 1 downto 0);
 begin
   core_ce <= ce and en(0);
   core_sinit <= (clr or rst(0)) and ce;
   op <= op_net;


 comp0: if ((core_name0 = "ip_scope_c_counter_binary_v12_0_i0")) generate 
  core_instance0:ip_scope_c_counter_binary_v12_0_i0
   port map ( 
        clk => clk,
        ce => core_ce,
        SINIT => core_sinit,
        load => load(0),
        l => din,
        q => op_net
  ); 
   end generate;

end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
 --
 --  Entity        : xldpram
 --
 --  Architecture  : behavior
 --
 --  Description   : Top level HDL wrapper for block dual port ram
 --
 ---------------------------------------------------------------------
 
 library IEEE;
 use IEEE.std_logic_1164.all;
 use IEEE.std_logic_arith.all;

entity ip_scope_xldpram is 
   generic (
     core_name0: string := "";
     c_width_a: integer := 13;
     c_address_width_a: integer := 4;
     c_width_b: integer := 13;
     c_address_width_b: integer := 4;
     c_has_sinita: integer := 0;
     c_has_sinitb: integer := 0;
     latency: integer := 1
   );
   port (
     dina: in std_logic_vector(c_width_a - 1 downto 0);
     addra: in std_logic_vector(c_address_width_a - 1 downto 0);
     wea: in std_logic_vector(0 downto 0);
     a_ce: in std_logic;
     a_clk: in std_logic;
     rsta: in std_logic_vector(0 downto 0) := (others => '0');
     ena: in std_logic_vector(0 downto 0) := (others => '1');
     douta: out std_logic_vector(c_width_a - 1 downto 0);
     dinb: in std_logic_vector(c_width_b - 1 downto 0);
     addrb: in std_logic_vector(c_address_width_b - 1 downto 0);
     web: in std_logic_vector(0 downto 0);
     b_ce: in std_logic;
     b_clk: in std_logic;
     rstb: in std_logic_vector(0 downto 0) := (others => '0');
     enb: in std_logic_vector(0 downto 0) := (others => '1');
     doutb: out std_logic_vector(c_width_b - 1 downto 0)
   );
 end ip_scope_xldpram;
 
 architecture behavior of ip_scope_xldpram is
 component synth_reg
 generic (
 width: integer;
 latency: integer
 );
 port (
 i: in std_logic_vector(width - 1 downto 0);
 ce: in std_logic;
 clr: in std_logic;
 clk: in std_logic;
 o: out std_logic_vector(width - 1 downto 0)
 );
 end component;
 
 signal core_addra: std_logic_vector(c_address_width_a - 1 downto 0);
 signal core_addrb: std_logic_vector(c_address_width_b - 1 downto 0);
 signal core_dina, core_douta, dly_douta:
 std_logic_vector(c_width_a - 1 downto 0);
 signal core_dinb, core_doutb, dly_doutb:
 std_logic_vector(c_width_b - 1 downto 0);
 signal core_wea, core_web: std_logic;
 signal core_a_ce, core_b_ce: std_logic;
 signal sinita, sinitb: std_logic;


 component ip_scope_blk_mem_gen_i0
    port ( 
addra: in std_logic_vector(c_address_width_a - 1 downto 0);
      addrb: in std_logic_vector(c_address_width_b - 1 downto 0);
      dina: in std_logic_vector(c_width_a - 1 downto 0);
      dinb: in std_logic_vector(c_width_b - 1 downto 0);
      clka: in std_logic;
      clkb: in std_logic;
      wea: in std_logic_vector(0 downto 0);
      web: in std_logic_vector(0 downto 0);
      ena: in std_logic;
      enb: in std_logic;
      douta: out std_logic_vector(c_width_a - 1 downto 0);
      doutb: out std_logic_vector(c_width_b - 1 downto 0) 
 		  ); 
 end component;

begin
 core_addra <= addra;
 core_dina <= dina;
 douta <= dly_douta;
 core_wea <= wea(0);
 core_a_ce <= a_ce and ena(0);
 sinita <= rsta(0) and a_ce;
 
 core_addrb <= addrb;
 core_dinb <= dinb;
 doutb <= dly_doutb;
 core_web <= web(0);
 core_b_ce <= b_ce and enb(0);
 sinitb <= rstb(0) and b_ce;


 comp0: if ((core_name0 = "ip_scope_blk_mem_gen_i0")) generate 
  core_instance0:ip_scope_blk_mem_gen_i0
   port map ( 
addra => core_addra,
        clka => a_clk,
        addrb => core_addrb,
        clkb => b_clk,
        dina => core_dina,
        wea(0) => core_wea,
        dinb => core_dinb,
        web(0) => core_web,
        ena => core_a_ce,
        enb => core_b_ce,
        douta => core_douta,
        doutb => core_doutb
  ); 
   end generate;

latency_test: if (latency > 0) generate
 regA: synth_reg
 generic map (
 width => c_width_a,
 latency => latency
 )
 port map (
 i => core_douta,
 ce => core_a_ce,
 clr => '0',
 clk => a_clk,
 o => dly_douta
 );
 regB: synth_reg
 generic map (
 width => c_width_b,
 latency => latency
 )
 port map (
 i => core_doutb,
 ce => core_b_ce,
 clr => '0',
 clk => b_clk,
 o => dly_doutb
 );
 end generate;
 latency1: if (latency < 1) generate
 dly_douta <= core_douta;
 dly_doutb <= core_doutb;
 end generate;
 end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
 use IEEE.std_logic_1164.all;

entity ip_scope_xlspram is 
   generic (
     core_name0: string := "";
     c_width: integer := 12;
     c_address_width: integer := 4;
     latency: integer := 1
     );
   port (
     data_in: in std_logic_vector(c_width - 1 downto 0);
     addr: in std_logic_vector(c_address_width - 1 downto 0);
     we: in std_logic_vector(0 downto 0);
     en: in std_logic_vector(0 downto 0);
     rst: in std_logic_vector(0 downto 0);
     ce: in std_logic;
     clk: in std_logic;
     data_out: out std_logic_vector(c_width - 1 downto 0)
   );
 end ip_scope_xlspram;
 
 architecture behavior of ip_scope_xlspram is
 component synth_reg
 generic (
 width: integer;
 latency: integer
 );
 port (
 i: in std_logic_vector(width - 1 downto 0);
 ce: in std_logic;
 clr: in std_logic;
 clk: in std_logic;
 o: out std_logic_vector(width - 1 downto 0)
 );
 end component;
 signal core_data_out, dly_data_out: std_logic_vector(c_width - 1 downto 0);
 signal core_we, core_ce, sinit: std_logic;


 component ip_scope_blk_mem_gen_i1
    port ( 
      addra: in std_logic_vector(c_address_width - 1 downto 0);
      clka: in std_logic;
      dina: in std_logic_vector(c_width - 1 downto 0);
      wea: in std_logic_vector(0 downto 0);
      ena: in std_logic;
      douta: out std_logic_vector(c_width - 1 downto 0) 
 		  ); 
 end component;

begin
 data_out <= dly_data_out;
 core_we <= we(0);
 core_ce <= ce and en(0);
 sinit <= rst(0) and ce;


 comp0: if ((core_name0 = "ip_scope_blk_mem_gen_i1")) generate 
  core_instance0:ip_scope_blk_mem_gen_i1
   port map ( 
        addra => addr,
        clka => clk,
        dina => data_in,
        wea(0) => core_we,
        ena => core_ce,
        douta => core_data_out
  ); 
   end generate;

latency_test: if (latency > 1) generate
 reg: synth_reg
 generic map (
 width => c_width,
 latency => latency - 1
 )
 port map (
 i => core_data_out,
 ce => core_ce,
 clr => '0',
 clk => clk,
 o => dly_data_out
 );
 end generate;
 latency_1: if (latency <= 1) generate
 dly_data_out <= core_data_out;
 end generate;
 end behavior;

