// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Thu Feb  4 23:58:07 2021
// Host        : ZBOOK running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               e:/smr3562/iaea/xilinx/projects/dpp/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_scope_0_0/design_1_ip_scope_0_0_sim_netlist.v
// Design      : design_1_ip_scope_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ip_scope_0_0,ip_scope,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "sysgen" *) 
(* X_CORE_INFO = "ip_scope,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module design_1_ip_scope_0_0
   (ch1,
    ch2,
    ch_trigger,
    axibusdomain_clk,
    signaldomain_clk,
    axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready,
    full,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 ch1 DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ch1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}" *) input [15:0]ch1;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 ch2 DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ch2, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}" *) input [15:0]ch2;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 ch_trigger DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ch_trigger, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}" *) input [15:0]ch_trigger;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input axibusdomain_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signaldomain_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input signaldomain_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input axibusdomain_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR" *) input [5:0]axibusdomain_s_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID" *) input axibusdomain_s_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA" *) input [31:0]axibusdomain_s_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB" *) input [3:0]axibusdomain_s_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID" *) input axibusdomain_s_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY" *) input axibusdomain_s_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR" *) input [5:0]axibusdomain_s_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID" *) input axibusdomain_s_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY" *) input axibusdomain_s_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 full INTERRUPT" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME full, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output [0:0]full;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY" *) output axibusdomain_s_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY" *) output axibusdomain_s_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP" *) output [1:0]axibusdomain_s_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID" *) output axibusdomain_s_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY" *) output axibusdomain_s_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA" *) output [31:0]axibusdomain_s_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP" *) output [1:0]axibusdomain_s_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output axibusdomain_s_axi_rvalid;

  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [5:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [5:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire [1:0]axibusdomain_s_axi_bresp;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire [1:0]axibusdomain_s_axi_rresp;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [15:0]ch1;
  wire [15:0]ch2;
  wire [15:0]ch_trigger;
  wire [0:0]full;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_ip_scope inst
       (.axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_arready),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_awready),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_bready),
        .axibusdomain_s_axi_bresp(axibusdomain_s_axi_bresp),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_rready),
        .axibusdomain_s_axi_rresp(axibusdomain_s_axi_rresp),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_wready),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .ch1(ch1),
        .ch2(ch2),
        .ch_trigger(ch_trigger),
        .full(full),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "axibusdomain_axi_lite_interface" *) 
module design_1_ip_scope_0_0_axibusdomain_axi_lite_interface
   (d,
    i,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    \slv_reg_array_reg[0][0] ,
    \slv_reg_array_reg[3][15] ,
    r3_dina,
    \slv_reg_array_reg[5][0] ,
    \slv_reg_array_reg[6][10] ,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rvalid,
    axibusdomain_s_axi_bvalid,
    axibusdomain_aresetn,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_clk,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_arvalid,
    r9_douta,
    q,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_rready);
  output [0:0]d;
  output [10:0]i;
  output axibusdomain_s_axi_awready;
  output axibusdomain_s_axi_wready;
  output [0:0]\slv_reg_array_reg[0][0] ;
  output [15:0]\slv_reg_array_reg[3][15] ;
  output [31:0]r3_dina;
  output [0:0]\slv_reg_array_reg[5][0] ;
  output [10:0]\slv_reg_array_reg[6][10] ;
  output axibusdomain_s_axi_arready;
  output [31:0]axibusdomain_s_axi_rdata;
  output axibusdomain_s_axi_rvalid;
  output axibusdomain_s_axi_bvalid;
  input axibusdomain_aresetn;
  input [31:0]axibusdomain_s_axi_wdata;
  input [3:0]axibusdomain_s_axi_wstrb;
  input axibusdomain_clk;
  input [5:0]axibusdomain_s_axi_awaddr;
  input [5:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_awvalid;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_arvalid;
  input [31:0]r9_douta;
  input [0:0]q;
  input axibusdomain_s_axi_bready;
  input axibusdomain_s_axi_rready;

  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [5:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [5:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [0:0]d;
  wire [10:0]i;
  wire [0:0]q;
  wire [31:0]r3_dina;
  wire [31:0]r9_douta;
  wire [0:0]\slv_reg_array_reg[0][0] ;
  wire [15:0]\slv_reg_array_reg[3][15] ;
  wire [0:0]\slv_reg_array_reg[5][0] ;
  wire [10:0]\slv_reg_array_reg[6][10] ;

  LUT6 #(
    .INIT(64'h7444444444444444)) 
    axi_bvalid_i_1
       (.I0(axibusdomain_s_axi_bready),
        .I1(axibusdomain_s_axi_bvalid),
        .I2(axibusdomain_s_axi_wvalid),
        .I3(axibusdomain_s_axi_awvalid),
        .I4(axibusdomain_s_axi_wready),
        .I5(axibusdomain_s_axi_awready),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h0F88)) 
    axi_rvalid_i_1
       (.I0(axibusdomain_s_axi_arready),
        .I1(axibusdomain_s_axi_arvalid),
        .I2(axibusdomain_s_axi_rready),
        .I3(axibusdomain_s_axi_rvalid),
        .O(axi_rvalid_i_1_n_0));
  design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog inst
       (.axi_arready_reg_0(axibusdomain_s_axi_arready),
        .axi_awready_reg_0(axibusdomain_s_axi_awready),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .axi_rvalid_reg_0(axi_rvalid_i_1_n_0),
        .axi_wready_reg_0(axibusdomain_s_axi_wready),
        .axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .d(d),
        .i(i),
        .q(q),
        .r3_dina(r3_dina),
        .r9_douta(r9_douta),
        .\slv_reg_array_reg[0][0]_0 (\slv_reg_array_reg[0][0] ),
        .\slv_reg_array_reg[3][15]_0 (\slv_reg_array_reg[3][15] ),
        .\slv_reg_array_reg[5][0]_0 (\slv_reg_array_reg[5][0] ),
        .\slv_reg_array_reg[6][10]_0 (\slv_reg_array_reg[6][10] ));
endmodule

(* ORIG_REF_NAME = "axibusdomain_axi_lite_interface_verilog" *) 
module design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog
   (axi_awready_reg_0,
    axi_wready_reg_0,
    axi_arready_reg_0,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_rvalid,
    i,
    \slv_reg_array_reg[0][0]_0 ,
    d,
    \slv_reg_array_reg[3][15]_0 ,
    r3_dina,
    \slv_reg_array_reg[5][0]_0 ,
    \slv_reg_array_reg[6][10]_0 ,
    axibusdomain_s_axi_rdata,
    axibusdomain_clk,
    axi_bvalid_reg_0,
    axi_rvalid_reg_0,
    axibusdomain_aresetn,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wvalid,
    r9_douta,
    q);
  output axi_awready_reg_0;
  output axi_wready_reg_0;
  output axi_arready_reg_0;
  output axibusdomain_s_axi_bvalid;
  output axibusdomain_s_axi_rvalid;
  output [10:0]i;
  output [0:0]\slv_reg_array_reg[0][0]_0 ;
  output [0:0]d;
  output [15:0]\slv_reg_array_reg[3][15]_0 ;
  output [31:0]r3_dina;
  output [0:0]\slv_reg_array_reg[5][0]_0 ;
  output [10:0]\slv_reg_array_reg[6][10]_0 ;
  output [31:0]axibusdomain_s_axi_rdata;
  input axibusdomain_clk;
  input axi_bvalid_reg_0;
  input axi_rvalid_reg_0;
  input axibusdomain_aresetn;
  input [31:0]axibusdomain_s_axi_wdata;
  input [3:0]axibusdomain_s_axi_wstrb;
  input [5:0]axibusdomain_s_axi_awaddr;
  input [5:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_awvalid;
  input axibusdomain_s_axi_wvalid;
  input [31:0]r9_douta;
  input [0:0]q;

  wire [5:0]axi_araddr;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire [5:0]axi_awaddr;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [5:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arvalid;
  wire [5:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [0:0]d;
  wire [3:0]dec_r__15;
  wire [10:0]i;
  wire p_0_in;
  wire [0:0]q;
  wire [31:11]r1_addra;
  wire [31:1]r2_wea;
  wire [31:0]r3_dina;
  wire [31:16]r4_threshold;
  wire [31:1]r5_enable;
  wire [31:11]r6_delay;
  wire [31:1]r7_clear;
  wire [31:0]r9_douta;
  wire \slv_reg_array[0][0]_i_1_n_0 ;
  wire \slv_reg_array[0][10]_i_1_n_0 ;
  wire \slv_reg_array[0][11]_i_1_n_0 ;
  wire \slv_reg_array[0][12]_i_1_n_0 ;
  wire \slv_reg_array[0][13]_i_1_n_0 ;
  wire \slv_reg_array[0][14]_i_1_n_0 ;
  wire \slv_reg_array[0][15]_i_1_n_0 ;
  wire \slv_reg_array[0][15]_i_2_n_0 ;
  wire \slv_reg_array[0][15]_i_3_n_0 ;
  wire \slv_reg_array[0][15]_i_4_n_0 ;
  wire \slv_reg_array[0][15]_i_5_n_0 ;
  wire \slv_reg_array[0][16]_i_1_n_0 ;
  wire \slv_reg_array[0][17]_i_1_n_0 ;
  wire \slv_reg_array[0][18]_i_1_n_0 ;
  wire \slv_reg_array[0][19]_i_1_n_0 ;
  wire \slv_reg_array[0][1]_i_1_n_0 ;
  wire \slv_reg_array[0][20]_i_1_n_0 ;
  wire \slv_reg_array[0][21]_i_1_n_0 ;
  wire \slv_reg_array[0][22]_i_1_n_0 ;
  wire \slv_reg_array[0][23]_i_1_n_0 ;
  wire \slv_reg_array[0][23]_i_2_n_0 ;
  wire \slv_reg_array[0][23]_i_3_n_0 ;
  wire \slv_reg_array[0][23]_i_4_n_0 ;
  wire \slv_reg_array[0][23]_i_5_n_0 ;
  wire \slv_reg_array[0][24]_i_1_n_0 ;
  wire \slv_reg_array[0][25]_i_1_n_0 ;
  wire \slv_reg_array[0][26]_i_1_n_0 ;
  wire \slv_reg_array[0][27]_i_1_n_0 ;
  wire \slv_reg_array[0][28]_i_1_n_0 ;
  wire \slv_reg_array[0][29]_i_1_n_0 ;
  wire \slv_reg_array[0][2]_i_1_n_0 ;
  wire \slv_reg_array[0][30]_i_1_n_0 ;
  wire \slv_reg_array[0][31]_i_1_n_0 ;
  wire \slv_reg_array[0][31]_i_2_n_0 ;
  wire \slv_reg_array[0][31]_i_3_n_0 ;
  wire \slv_reg_array[0][31]_i_4_n_0 ;
  wire \slv_reg_array[0][31]_i_5_n_0 ;
  wire \slv_reg_array[0][31]_i_6_n_0 ;
  wire \slv_reg_array[0][3]_i_1_n_0 ;
  wire \slv_reg_array[0][4]_i_1_n_0 ;
  wire \slv_reg_array[0][5]_i_1_n_0 ;
  wire \slv_reg_array[0][6]_i_1_n_0 ;
  wire \slv_reg_array[0][7]_i_1_n_0 ;
  wire \slv_reg_array[0][7]_i_2_n_0 ;
  wire \slv_reg_array[0][7]_i_3_n_0 ;
  wire \slv_reg_array[0][7]_i_4_n_0 ;
  wire \slv_reg_array[0][7]_i_5_n_0 ;
  wire \slv_reg_array[0][8]_i_1_n_0 ;
  wire \slv_reg_array[0][9]_i_1_n_0 ;
  wire \slv_reg_array[1][0]_i_1_n_0 ;
  wire \slv_reg_array[1][0]_i_2_n_0 ;
  wire \slv_reg_array[1][10]_i_1_n_0 ;
  wire \slv_reg_array[1][10]_i_2_n_0 ;
  wire \slv_reg_array[1][11]_i_1_n_0 ;
  wire \slv_reg_array[1][12]_i_1_n_0 ;
  wire \slv_reg_array[1][13]_i_1_n_0 ;
  wire \slv_reg_array[1][14]_i_1_n_0 ;
  wire \slv_reg_array[1][15]_i_1_n_0 ;
  wire \slv_reg_array[1][15]_i_2_n_0 ;
  wire \slv_reg_array[1][16]_i_1_n_0 ;
  wire \slv_reg_array[1][17]_i_1_n_0 ;
  wire \slv_reg_array[1][18]_i_1_n_0 ;
  wire \slv_reg_array[1][19]_i_1_n_0 ;
  wire \slv_reg_array[1][1]_i_1_n_0 ;
  wire \slv_reg_array[1][20]_i_1_n_0 ;
  wire \slv_reg_array[1][21]_i_1_n_0 ;
  wire \slv_reg_array[1][22]_i_1_n_0 ;
  wire \slv_reg_array[1][23]_i_1_n_0 ;
  wire \slv_reg_array[1][23]_i_2_n_0 ;
  wire \slv_reg_array[1][24]_i_1_n_0 ;
  wire \slv_reg_array[1][25]_i_1_n_0 ;
  wire \slv_reg_array[1][26]_i_1_n_0 ;
  wire \slv_reg_array[1][27]_i_1_n_0 ;
  wire \slv_reg_array[1][28]_i_1_n_0 ;
  wire \slv_reg_array[1][29]_i_1_n_0 ;
  wire \slv_reg_array[1][2]_i_1_n_0 ;
  wire \slv_reg_array[1][30]_i_1_n_0 ;
  wire \slv_reg_array[1][31]_i_1_n_0 ;
  wire \slv_reg_array[1][31]_i_2_n_0 ;
  wire \slv_reg_array[1][31]_i_3_n_0 ;
  wire \slv_reg_array[1][31]_i_4_n_0 ;
  wire \slv_reg_array[1][3]_i_1_n_0 ;
  wire \slv_reg_array[1][4]_i_1_n_0 ;
  wire \slv_reg_array[1][5]_i_1_n_0 ;
  wire \slv_reg_array[1][6]_i_1_n_0 ;
  wire \slv_reg_array[1][7]_i_1_n_0 ;
  wire \slv_reg_array[1][7]_i_2_n_0 ;
  wire \slv_reg_array[1][8]_i_1_n_0 ;
  wire \slv_reg_array[1][9]_i_1_n_0 ;
  wire \slv_reg_array[2][0]_i_1_n_0 ;
  wire \slv_reg_array[2][0]_i_2_n_0 ;
  wire \slv_reg_array[2][10]_i_1_n_0 ;
  wire \slv_reg_array[2][15]_i_1_n_0 ;
  wire \slv_reg_array[2][23]_i_1_n_0 ;
  wire \slv_reg_array[2][2]_i_1_n_0 ;
  wire \slv_reg_array[2][31]_i_1_n_0 ;
  wire \slv_reg_array[2][31]_i_2_n_0 ;
  wire \slv_reg_array[2][5]_i_1_n_0 ;
  wire \slv_reg_array[2][6]_i_1_n_0 ;
  wire \slv_reg_array[2][6]_i_2_n_0 ;
  wire \slv_reg_array[2][6]_i_3_n_0 ;
  wire \slv_reg_array[2][7]_i_1_n_0 ;
  wire \slv_reg_array[2][9]_i_1_n_0 ;
  wire \slv_reg_array[3][0]_i_1_n_0 ;
  wire \slv_reg_array[3][10]_i_1_n_0 ;
  wire \slv_reg_array[3][15]_i_1_n_0 ;
  wire \slv_reg_array[3][1]_i_1_n_0 ;
  wire \slv_reg_array[3][23]_i_1_n_0 ;
  wire \slv_reg_array[3][2]_i_1_n_0 ;
  wire \slv_reg_array[3][31]_i_1_n_0 ;
  wire \slv_reg_array[3][3]_i_1_n_0 ;
  wire \slv_reg_array[3][4]_i_1_n_0 ;
  wire \slv_reg_array[3][5]_i_1_n_0 ;
  wire \slv_reg_array[3][6]_i_1_n_0 ;
  wire \slv_reg_array[3][6]_i_2_n_0 ;
  wire \slv_reg_array[3][7]_i_1_n_0 ;
  wire \slv_reg_array[3][7]_i_2_n_0 ;
  wire \slv_reg_array[3][7]_i_3_n_0 ;
  wire \slv_reg_array[3][9]_i_1_n_0 ;
  wire \slv_reg_array[4][0]_i_1_n_0 ;
  wire \slv_reg_array[4][15]_i_1_n_0 ;
  wire \slv_reg_array[4][15]_i_2_n_0 ;
  wire \slv_reg_array[4][23]_i_1_n_0 ;
  wire \slv_reg_array[4][23]_i_2_n_0 ;
  wire \slv_reg_array[4][31]_i_1_n_0 ;
  wire \slv_reg_array[4][31]_i_2_n_0 ;
  wire \slv_reg_array[4][31]_i_3_n_0 ;
  wire \slv_reg_array[4][7]_i_1_n_0 ;
  wire \slv_reg_array[4][7]_i_2_n_0 ;
  wire \slv_reg_array[5][0]_i_1_n_0 ;
  wire \slv_reg_array[5][15]_i_1_n_0 ;
  wire \slv_reg_array[5][15]_i_2_n_0 ;
  wire \slv_reg_array[5][23]_i_1_n_0 ;
  wire \slv_reg_array[5][23]_i_2_n_0 ;
  wire \slv_reg_array[5][31]_i_1_n_0 ;
  wire \slv_reg_array[5][31]_i_2_n_0 ;
  wire \slv_reg_array[5][31]_i_3_n_0 ;
  wire \slv_reg_array[5][7]_i_1_n_0 ;
  wire \slv_reg_array[5][7]_i_2_n_0 ;
  wire \slv_reg_array[6][0]_i_1_n_0 ;
  wire \slv_reg_array[6][15]_i_1_n_0 ;
  wire \slv_reg_array[6][15]_i_2_n_0 ;
  wire \slv_reg_array[6][23]_i_1_n_0 ;
  wire \slv_reg_array[6][23]_i_2_n_0 ;
  wire \slv_reg_array[6][31]_i_1_n_0 ;
  wire \slv_reg_array[6][31]_i_2_n_0 ;
  wire \slv_reg_array[6][31]_i_3_n_0 ;
  wire \slv_reg_array[6][7]_i_1_n_0 ;
  wire \slv_reg_array[6][7]_i_2_n_0 ;
  wire [0:0]\slv_reg_array_reg[0][0]_0 ;
  wire [15:0]\slv_reg_array_reg[3][15]_0 ;
  wire [0:0]\slv_reg_array_reg[5][0]_0 ;
  wire [10:0]\slv_reg_array_reg[6][10]_0 ;
  wire slv_reg_rden__0;
  wire [31:0]slv_wire_array;

  FDRE \axi_araddr_reg[0] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[0]),
        .Q(axi_araddr[0]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[1] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[1]),
        .Q(axi_araddr[1]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[2] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[2]),
        .Q(axi_araddr[2]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[3] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[3]),
        .Q(axi_araddr[3]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[4] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[4]),
        .Q(axi_araddr[4]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[5] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[5]),
        .Q(axi_araddr[5]),
        .R(p_0_in));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(axibusdomain_s_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[0] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[0]),
        .Q(axi_awaddr[0]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[1] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[1]),
        .Q(axi_awaddr[1]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[2] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[2]),
        .Q(axi_awaddr[2]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[3]),
        .Q(axi_awaddr[3]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[4]),
        .Q(axi_awaddr[4]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[5] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[5]),
        .Q(axi_awaddr[5]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(axibusdomain_aresetn),
        .O(p_0_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .I2(axi_awready_reg_0),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(p_0_in));
  FDRE axi_bvalid_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(axibusdomain_s_axi_bvalid),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[0]_i_1 
       (.I0(r9_douta[0]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[0]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[0]_i_3_n_0 ),
        .O(slv_wire_array[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(q),
        .I1(\slv_reg_array_reg[6][10]_0 [0]),
        .I2(dec_r__15[1]),
        .I3(\slv_reg_array_reg[5][0]_0 ),
        .I4(dec_r__15[0]),
        .I5(r3_dina[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [0]),
        .I1(d),
        .I2(dec_r__15[1]),
        .I3(i[0]),
        .I4(dec_r__15[0]),
        .I5(\slv_reg_array_reg[0][0]_0 ),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[10]_i_1 
       (.I0(r9_douta[10]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[10]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[10]_i_3_n_0 ),
        .O(slv_wire_array[10]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [10]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[10]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [10]),
        .I1(r5_enable[10]),
        .I2(dec_r__15[1]),
        .I3(i[10]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[11]_i_1 
       (.I0(r9_douta[11]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[11]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[11]_i_3_n_0 ),
        .O(slv_wire_array[11]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_2 
       (.I0(r1_addra[11]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[11]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [11]),
        .I1(r5_enable[11]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[11]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[12]_i_1 
       (.I0(r9_douta[12]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[12]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[12]_i_3_n_0 ),
        .O(slv_wire_array[12]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[12]_i_2 
       (.I0(r1_addra[12]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[12]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [12]),
        .I1(r5_enable[12]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[12]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[13]_i_1 
       (.I0(r9_douta[13]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[13]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[13]_i_3_n_0 ),
        .O(slv_wire_array[13]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[13]_i_2 
       (.I0(r1_addra[13]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[13]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [13]),
        .I1(r5_enable[13]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[13]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[13]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[14]_i_1 
       (.I0(r9_douta[14]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[14]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[14]_i_3_n_0 ),
        .O(slv_wire_array[14]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[14]_i_2 
       (.I0(r1_addra[14]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[14]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [14]),
        .I1(r5_enable[14]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[14]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[14]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[15]_i_1 
       (.I0(r9_douta[15]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[15]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[15]_i_3_n_0 ),
        .O(slv_wire_array[15]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[15]_i_2 
       (.I0(r1_addra[15]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[15]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [15]),
        .I1(r5_enable[15]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[15]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[16]_i_1 
       (.I0(r9_douta[16]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[16]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[16]_i_3_n_0 ),
        .O(slv_wire_array[16]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[16]_i_2 
       (.I0(r1_addra[16]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[16]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_3 
       (.I0(r4_threshold[16]),
        .I1(r5_enable[16]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[16]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[17]_i_1 
       (.I0(r9_douta[17]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[17]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[17]_i_3_n_0 ),
        .O(slv_wire_array[17]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[17]_i_2 
       (.I0(r1_addra[17]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[17]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_3 
       (.I0(r4_threshold[17]),
        .I1(r5_enable[17]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[17]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[18]_i_1 
       (.I0(r9_douta[18]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[18]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[18]_i_3_n_0 ),
        .O(slv_wire_array[18]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[18]_i_2 
       (.I0(r1_addra[18]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[18]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_3 
       (.I0(r4_threshold[18]),
        .I1(r5_enable[18]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[18]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[18]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[19]_i_1 
       (.I0(r9_douta[19]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[19]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[19]_i_3_n_0 ),
        .O(slv_wire_array[19]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[19]_i_2 
       (.I0(r1_addra[19]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[19]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_3 
       (.I0(r4_threshold[19]),
        .I1(r5_enable[19]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[19]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[1]_i_1 
       (.I0(r9_douta[1]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[1]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[1]_i_3_n_0 ),
        .O(slv_wire_array[1]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [1]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[1]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [1]),
        .I1(r5_enable[1]),
        .I2(dec_r__15[1]),
        .I3(i[1]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[20]_i_1 
       (.I0(r9_douta[20]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[20]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[20]_i_3_n_0 ),
        .O(slv_wire_array[20]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[20]_i_2 
       (.I0(r1_addra[20]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[20]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_3 
       (.I0(r4_threshold[20]),
        .I1(r5_enable[20]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[20]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[20]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[21]_i_1 
       (.I0(r9_douta[21]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[21]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[21]_i_3_n_0 ),
        .O(slv_wire_array[21]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[21]_i_2 
       (.I0(r1_addra[21]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[21]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_3 
       (.I0(r4_threshold[21]),
        .I1(r5_enable[21]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[21]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[22]_i_1 
       (.I0(r9_douta[22]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[22]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[22]_i_3_n_0 ),
        .O(slv_wire_array[22]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[22]_i_2 
       (.I0(r1_addra[22]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[22]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_3 
       (.I0(r4_threshold[22]),
        .I1(r5_enable[22]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[22]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[22]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[23]_i_1 
       (.I0(r9_douta[23]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[23]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[23]_i_3_n_0 ),
        .O(slv_wire_array[23]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[23]_i_2 
       (.I0(r1_addra[23]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[23]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_3 
       (.I0(r4_threshold[23]),
        .I1(r5_enable[23]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[23]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[24]_i_1 
       (.I0(r9_douta[24]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[24]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[24]_i_3_n_0 ),
        .O(slv_wire_array[24]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[24]_i_2 
       (.I0(r1_addra[24]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[24]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_3 
       (.I0(r4_threshold[24]),
        .I1(r5_enable[24]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[24]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[25]_i_1 
       (.I0(r9_douta[25]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[25]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[25]_i_3_n_0 ),
        .O(slv_wire_array[25]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[25]_i_2 
       (.I0(r1_addra[25]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[25]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_3 
       (.I0(r4_threshold[25]),
        .I1(r5_enable[25]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[25]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[26]_i_1 
       (.I0(r9_douta[26]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[26]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[26]_i_3_n_0 ),
        .O(slv_wire_array[26]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[26]_i_2 
       (.I0(r1_addra[26]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[26]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_3 
       (.I0(r4_threshold[26]),
        .I1(r5_enable[26]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[26]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[26]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[27]_i_1 
       (.I0(r9_douta[27]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[27]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[27]_i_3_n_0 ),
        .O(slv_wire_array[27]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[27]_i_2 
       (.I0(r1_addra[27]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[27]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_3 
       (.I0(r4_threshold[27]),
        .I1(r5_enable[27]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[27]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[28]_i_1 
       (.I0(r9_douta[28]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[28]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[28]_i_3_n_0 ),
        .O(slv_wire_array[28]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[28]_i_2 
       (.I0(r1_addra[28]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[28]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_3 
       (.I0(r4_threshold[28]),
        .I1(r5_enable[28]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[28]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[29]_i_1 
       (.I0(r9_douta[29]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[29]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[29]_i_3_n_0 ),
        .O(slv_wire_array[29]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[29]_i_2 
       (.I0(r1_addra[29]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[29]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_3 
       (.I0(r4_threshold[29]),
        .I1(r5_enable[29]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[29]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[29]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[2]_i_1 
       (.I0(r9_douta[2]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[2]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[2]_i_3_n_0 ),
        .O(slv_wire_array[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [2]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[2]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [2]),
        .I1(r5_enable[2]),
        .I2(dec_r__15[1]),
        .I3(i[2]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[30]_i_1 
       (.I0(r9_douta[30]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[30]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[30]_i_3_n_0 ),
        .O(slv_wire_array[30]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[30]_i_2 
       (.I0(r1_addra[30]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[30]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_3 
       (.I0(r4_threshold[30]),
        .I1(r5_enable[30]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[30]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[31]_i_1 
       (.I0(r9_douta[31]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[31]_i_3_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[31]_i_5_n_0 ),
        .O(slv_wire_array[31]));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \axi_rdata[31]_i_2 
       (.I0(axi_araddr[0]),
        .I1(axi_araddr[4]),
        .I2(axi_araddr[1]),
        .I3(axi_araddr[5]),
        .I4(axi_araddr[3]),
        .I5(axi_araddr[2]),
        .O(dec_r__15[3]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[31]_i_3 
       (.I0(r1_addra[31]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[31]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_4 
       (.I0(axi_araddr[5]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[4]),
        .I3(axi_araddr[1]),
        .O(dec_r__15[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_5 
       (.I0(r4_threshold[31]),
        .I1(r5_enable[31]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[31]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[31]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_6 
       (.I0(axi_araddr[5]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[1]),
        .O(dec_r__15[1]));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_7 
       (.I0(axi_araddr[5]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[1]),
        .O(dec_r__15[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[3]_i_1 
       (.I0(r9_douta[3]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[3]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[3]_i_3_n_0 ),
        .O(slv_wire_array[3]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [3]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[3]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [3]),
        .I1(r5_enable[3]),
        .I2(dec_r__15[1]),
        .I3(i[3]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[4]_i_1 
       (.I0(r9_douta[4]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[4]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[4]_i_3_n_0 ),
        .O(slv_wire_array[4]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [4]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[4]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [4]),
        .I1(r5_enable[4]),
        .I2(dec_r__15[1]),
        .I3(i[4]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[5]_i_1 
       (.I0(r9_douta[5]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[5]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[5]_i_3_n_0 ),
        .O(slv_wire_array[5]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [5]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[5]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [5]),
        .I1(r5_enable[5]),
        .I2(dec_r__15[1]),
        .I3(i[5]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[6]_i_1 
       (.I0(r9_douta[6]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[6]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[6]_i_3_n_0 ),
        .O(slv_wire_array[6]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [6]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[6]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [6]),
        .I1(r5_enable[6]),
        .I2(dec_r__15[1]),
        .I3(i[6]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[7]_i_1 
       (.I0(r9_douta[7]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[7]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[7]_i_3_n_0 ),
        .O(slv_wire_array[7]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [7]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[7]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [7]),
        .I1(r5_enable[7]),
        .I2(dec_r__15[1]),
        .I3(i[7]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[8]_i_1 
       (.I0(r9_douta[8]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[8]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[8]_i_3_n_0 ),
        .O(slv_wire_array[8]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [8]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[8]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [8]),
        .I1(r5_enable[8]),
        .I2(dec_r__15[1]),
        .I3(i[8]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[9]_i_1 
       (.I0(r9_douta[9]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[9]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[9]_i_3_n_0 ),
        .O(slv_wire_array[9]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [9]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[9]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [9]),
        .I1(r5_enable[9]),
        .I2(dec_r__15[1]),
        .I3(i[9]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[0]),
        .Q(axibusdomain_s_axi_rdata[0]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[10] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[10]),
        .Q(axibusdomain_s_axi_rdata[10]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[11] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[11]),
        .Q(axibusdomain_s_axi_rdata[11]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[12] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[12]),
        .Q(axibusdomain_s_axi_rdata[12]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[13] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[13]),
        .Q(axibusdomain_s_axi_rdata[13]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[14] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[14]),
        .Q(axibusdomain_s_axi_rdata[14]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[15] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[15]),
        .Q(axibusdomain_s_axi_rdata[15]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[16] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[16]),
        .Q(axibusdomain_s_axi_rdata[16]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[17] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[17]),
        .Q(axibusdomain_s_axi_rdata[17]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[18] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[18]),
        .Q(axibusdomain_s_axi_rdata[18]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[19] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[19]),
        .Q(axibusdomain_s_axi_rdata[19]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[1] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[1]),
        .Q(axibusdomain_s_axi_rdata[1]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[20] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[20]),
        .Q(axibusdomain_s_axi_rdata[20]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[21] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[21]),
        .Q(axibusdomain_s_axi_rdata[21]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[22] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[22]),
        .Q(axibusdomain_s_axi_rdata[22]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[23] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[23]),
        .Q(axibusdomain_s_axi_rdata[23]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[24] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[24]),
        .Q(axibusdomain_s_axi_rdata[24]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[25] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[25]),
        .Q(axibusdomain_s_axi_rdata[25]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[26] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[26]),
        .Q(axibusdomain_s_axi_rdata[26]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[27] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[27]),
        .Q(axibusdomain_s_axi_rdata[27]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[28] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[28]),
        .Q(axibusdomain_s_axi_rdata[28]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[29] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[29]),
        .Q(axibusdomain_s_axi_rdata[29]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[2] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[2]),
        .Q(axibusdomain_s_axi_rdata[2]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[30] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[30]),
        .Q(axibusdomain_s_axi_rdata[30]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[31] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[31]),
        .Q(axibusdomain_s_axi_rdata[31]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[3] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[3]),
        .Q(axibusdomain_s_axi_rdata[3]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[4] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[4]),
        .Q(axibusdomain_s_axi_rdata[4]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[5] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[5]),
        .Q(axibusdomain_s_axi_rdata[5]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[6] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[6]),
        .Q(axibusdomain_s_axi_rdata[6]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[7] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[7]),
        .Q(axibusdomain_s_axi_rdata[7]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[8] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[8]),
        .Q(axibusdomain_s_axi_rdata[8]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[9] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[9]),
        .Q(axibusdomain_s_axi_rdata[9]),
        .R(p_0_in));
  FDRE axi_rvalid_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_rvalid_reg_0),
        .Q(axibusdomain_s_axi_rvalid),
        .R(p_0_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .I2(axi_wready_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h8FFF000080000000)) 
    \slv_reg_array[0][0]_i_1 
       (.I0(axibusdomain_s_axi_wdata[0]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .I3(\slv_reg_array[0][31]_i_4_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[0][0]_0 ),
        .O(\slv_reg_array[0][0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][10]_i_1 
       (.I0(axibusdomain_s_axi_wdata[10]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][11]_i_1 
       (.I0(axibusdomain_s_axi_wdata[11]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][12]_i_1 
       (.I0(axibusdomain_s_axi_wdata[12]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][13]_i_1 
       (.I0(axibusdomain_s_axi_wdata[13]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][14]_i_1 
       (.I0(axibusdomain_s_axi_wdata[14]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][14]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[0][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[0][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[0][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .O(\slv_reg_array[0][15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][15]_i_3 
       (.I0(axibusdomain_s_axi_wdata[15]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \slv_reg_array[0][15]_i_4 
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .I2(axi_wready_reg_0),
        .I3(axi_awready_reg_0),
        .I4(axibusdomain_aresetn),
        .I5(axibusdomain_s_axi_wstrb[1]),
        .O(\slv_reg_array[0][15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000080008000)) 
    \slv_reg_array[0][15]_i_5 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_wvalid),
        .I4(axibusdomain_s_axi_wstrb[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][15]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][16]_i_1 
       (.I0(axibusdomain_s_axi_wdata[16]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][17]_i_1 
       (.I0(axibusdomain_s_axi_wdata[17]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][18]_i_1 
       (.I0(axibusdomain_s_axi_wdata[18]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][19]_i_1 
       (.I0(axibusdomain_s_axi_wdata[19]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][1]_i_1 
       (.I0(axibusdomain_s_axi_wdata[1]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][20]_i_1 
       (.I0(axibusdomain_s_axi_wdata[20]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][21]_i_1 
       (.I0(axibusdomain_s_axi_wdata[21]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][22]_i_1 
       (.I0(axibusdomain_s_axi_wdata[22]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][22]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[0][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][23]_i_4_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[0][23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[0][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][23]_i_4_n_0 ),
        .O(\slv_reg_array[0][23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][23]_i_3 
       (.I0(axibusdomain_s_axi_wdata[23]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[0][23]_i_4 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000080008000)) 
    \slv_reg_array[0][23]_i_5 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_wvalid),
        .I4(axibusdomain_s_axi_wstrb[2]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][23]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][24]_i_1 
       (.I0(axibusdomain_s_axi_wdata[24]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][25]_i_1 
       (.I0(axibusdomain_s_axi_wdata[25]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][26]_i_1 
       (.I0(axibusdomain_s_axi_wdata[26]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][27]_i_1 
       (.I0(axibusdomain_s_axi_wdata[27]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][28]_i_1 
       (.I0(axibusdomain_s_axi_wdata[28]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][29]_i_1 
       (.I0(axibusdomain_s_axi_wdata[29]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][2]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wdata[2]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][30]_i_1 
       (.I0(axibusdomain_s_axi_wdata[30]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][30]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[0][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][31]_i_5_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[0][31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[0][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][31]_i_5_n_0 ),
        .O(\slv_reg_array[0][31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][31]_i_3 
       (.I0(axibusdomain_s_axi_wdata[31]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFCFFFCFFFCFEFD)) 
    \slv_reg_array[0][31]_i_4 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[5]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[0][31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[0][31]_i_5 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8000000080008000)) 
    \slv_reg_array[0][31]_i_6 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_wvalid),
        .I4(axibusdomain_s_axi_wstrb[3]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][31]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][3]_i_1 
       (.I0(axibusdomain_s_axi_wdata[3]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][4]_i_1 
       (.I0(axibusdomain_s_axi_wdata[4]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][5]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wdata[5]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][6]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wdata[6]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[0][7]_i_1 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][7]_i_4_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[0][7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[0][7]_i_2 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][7]_i_4_n_0 ),
        .O(\slv_reg_array[0][7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][7]_i_3 
       (.I0(axibusdomain_s_axi_wdata[7]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[0][7]_i_4 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000080008000)) 
    \slv_reg_array[0][7]_i_5 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_wvalid),
        .I4(axibusdomain_s_axi_wstrb[0]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][7]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][8]_i_1 
       (.I0(axibusdomain_s_axi_wdata[8]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][9]_i_1 
       (.I0(axibusdomain_s_axi_wdata[9]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4FFF404040004040)) 
    \slv_reg_array[1][0]_i_1 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(\slv_reg_array[1][0]_i_2_n_0 ),
        .I2(\slv_reg_array[1][31]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(i[0]),
        .O(\slv_reg_array[1][0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[1][0]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wdata[0]),
        .O(\slv_reg_array[1][0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFDD0200)) 
    \slv_reg_array[1][10]_i_1 
       (.I0(\slv_reg_array[1][31]_i_3_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(\slv_reg_array[1][10]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[10]),
        .I4(i[10]),
        .O(\slv_reg_array[1][10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \slv_reg_array[1][10]_i_2 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[5]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[1][10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][11]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[11]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][12]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[12]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][13]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[13]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][14]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[14]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][14]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[1][15]_i_1 
       (.I0(\slv_reg_array[1][31]_i_3_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .O(\slv_reg_array[1][15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][15]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[15]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][16]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[16]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][17]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[17]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][18]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[18]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][19]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[19]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][1]_i_1 
       (.I0(axibusdomain_s_axi_wdata[1]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][20]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[20]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][21]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[21]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][22]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[22]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][22]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[1][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .O(\slv_reg_array[1][23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][23]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[23]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][24]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[24]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][25]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[25]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][26]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[26]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][27]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[27]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][28]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[28]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][29]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[29]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][29]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \slv_reg_array[1][2]_i_1 
       (.I0(axibusdomain_s_axi_wdata[2]),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .I2(\slv_reg_array[0][7]_i_4_n_0 ),
        .I3(i[2]),
        .O(\slv_reg_array[1][2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][30]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[30]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][30]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[1][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .O(\slv_reg_array[1][31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][31]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[31]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \slv_reg_array[1][31]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[5]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[1][31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFFFFFFFFFF)) 
    \slv_reg_array[1][31]_i_4 
       (.I0(\slv_reg_array[1][10]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[1][31]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][3]_i_1 
       (.I0(axibusdomain_s_axi_wdata[3]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][4]_i_1 
       (.I0(axibusdomain_s_axi_wdata[4]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \slv_reg_array[1][5]_i_1 
       (.I0(axibusdomain_s_axi_wdata[5]),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .I2(\slv_reg_array[0][7]_i_4_n_0 ),
        .I3(i[5]),
        .O(\slv_reg_array[1][5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \slv_reg_array[1][6]_i_1 
       (.I0(axibusdomain_s_axi_wdata[6]),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .I2(\slv_reg_array[0][7]_i_4_n_0 ),
        .I3(i[6]),
        .O(\slv_reg_array[1][6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[1][7]_i_1 
       (.I0(\slv_reg_array[1][31]_i_3_n_0 ),
        .I1(\slv_reg_array[0][7]_i_4_n_0 ),
        .O(\slv_reg_array[1][7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][7]_i_2 
       (.I0(axibusdomain_s_axi_wdata[7]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][8]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[8]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hDFDD0200)) 
    \slv_reg_array[1][9]_i_1 
       (.I0(\slv_reg_array[1][31]_i_3_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(\slv_reg_array[1][10]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[9]),
        .I4(i[9]),
        .O(\slv_reg_array[1][9]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEFFFEF00)) 
    \slv_reg_array[2][0]_i_1 
       (.I0(\slv_reg_array[2][6]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wdata[0]),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(\slv_reg_array[2][0]_i_2_n_0 ),
        .I4(d),
        .O(\slv_reg_array[2][0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h4F)) 
    \slv_reg_array[2][0]_i_2 
       (.I0(\slv_reg_array[2][31]_i_2_n_0 ),
        .I1(\slv_reg_array[0][7]_i_5_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[2][0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFEE0100)) 
    \slv_reg_array[2][10]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[1][10]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[10]),
        .I4(r5_enable[10]),
        .O(\slv_reg_array[2][10]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[2][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .O(\slv_reg_array[2][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[2][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .O(\slv_reg_array[2][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDDDDFDDD00002000)) 
    \slv_reg_array[2][2]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[2]),
        .I4(\slv_reg_array[2][6]_i_3_n_0 ),
        .I5(r5_enable[2]),
        .O(\slv_reg_array[2][2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[2][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .O(\slv_reg_array[2][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    \slv_reg_array[2][31]_i_2 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[5]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axi_awaddr[2]),
        .O(\slv_reg_array[2][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hDDDDFDDD00002000)) 
    \slv_reg_array[2][5]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[5]),
        .I4(\slv_reg_array[2][6]_i_3_n_0 ),
        .I5(r5_enable[5]),
        .O(\slv_reg_array[2][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDDDDFDDD00002000)) 
    \slv_reg_array[2][6]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[6]),
        .I4(\slv_reg_array[2][6]_i_3_n_0 ),
        .I5(r5_enable[6]),
        .O(\slv_reg_array[2][6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \slv_reg_array[2][6]_i_2 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[1]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[5]),
        .O(\slv_reg_array[2][6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000F0001000A)) 
    \slv_reg_array[2][6]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[1]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[5]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[2][6]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[2][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .O(\slv_reg_array[2][7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEFEE0100)) 
    \slv_reg_array[2][9]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[1][10]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[9]),
        .I4(r5_enable[9]),
        .O(\slv_reg_array[2][9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF4FF040404000404)) 
    \slv_reg_array[3][0]_i_1 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(\slv_reg_array[1][0]_i_2_n_0 ),
        .I2(\slv_reg_array[3][7]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[3][15]_0 [0]),
        .O(\slv_reg_array[3][0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFE02)) 
    \slv_reg_array[3][10]_i_1 
       (.I0(axibusdomain_s_axi_wdata[10]),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(\slv_reg_array[3][7]_i_3_n_0 ),
        .I3(\slv_reg_array_reg[3][15]_0 [10]),
        .O(\slv_reg_array[3][10]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[3][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[3][7]_i_3_n_0 ),
        .O(\slv_reg_array[3][15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \slv_reg_array[3][1]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[1]),
        .O(\slv_reg_array[3][1]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[3][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[3][7]_i_3_n_0 ),
        .O(\slv_reg_array[3][23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    \slv_reg_array[3][2]_i_1 
       (.I0(axibusdomain_s_axi_wdata[2]),
        .I1(axi_awaddr[3]),
        .I2(\slv_reg_array[3][6]_i_2_n_0 ),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg_array[3][2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[3][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[3][7]_i_3_n_0 ),
        .O(\slv_reg_array[3][31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][3]_i_1 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(axibusdomain_s_axi_wdata[3]),
        .O(\slv_reg_array[3][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][4]_i_1 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(axibusdomain_s_axi_wdata[4]),
        .O(\slv_reg_array[3][4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    \slv_reg_array[3][5]_i_1 
       (.I0(axibusdomain_s_axi_wdata[5]),
        .I1(axi_awaddr[3]),
        .I2(\slv_reg_array[3][6]_i_2_n_0 ),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg_array[3][5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    \slv_reg_array[3][6]_i_1 
       (.I0(axibusdomain_s_axi_wdata[6]),
        .I1(axi_awaddr[3]),
        .I2(\slv_reg_array[3][6]_i_2_n_0 ),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg_array[3][6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \slv_reg_array[3][6]_i_2 
       (.I0(axi_awaddr[1]),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[5]),
        .O(\slv_reg_array[3][6]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[3][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[3][7]_i_3_n_0 ),
        .O(\slv_reg_array[3][7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][7]_i_2 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(axibusdomain_s_axi_wdata[7]),
        .O(\slv_reg_array[3][7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFDFFFFFFFF)) 
    \slv_reg_array[3][7]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[5]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[1]),
        .I4(axi_awaddr[4]),
        .I5(axi_awaddr[3]),
        .O(\slv_reg_array[3][7]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFE02)) 
    \slv_reg_array[3][9]_i_1 
       (.I0(axibusdomain_s_axi_wdata[9]),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(\slv_reg_array[3][7]_i_3_n_0 ),
        .I3(\slv_reg_array_reg[3][15]_0 [9]),
        .O(\slv_reg_array[3][9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF8FF000008000000)) 
    \slv_reg_array[4][0]_i_1 
       (.I0(axibusdomain_s_axi_wdata[0]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[4][31]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(r3_dina[0]),
        .O(\slv_reg_array[4][0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \slv_reg_array[4][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[4][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[4][15]_i_2 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][15]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[4][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[4][23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[4][23]_i_2 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][23]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[4][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[4][31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[4][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \slv_reg_array[4][31]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[5]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[4][31]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[4][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[4][7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[4][7]_i_2 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF8FF000008000000)) 
    \slv_reg_array[5][0]_i_1 
       (.I0(axibusdomain_s_axi_wdata[0]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[5][31]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[5][0]_0 ),
        .O(\slv_reg_array[5][0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \slv_reg_array[5][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[5][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[5][15]_i_2 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .O(\slv_reg_array[5][15]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[5][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[5][23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[5][23]_i_2 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .O(\slv_reg_array[5][23]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[5][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[5][31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[5][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .O(\slv_reg_array[5][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFFFFFFFFFF)) 
    \slv_reg_array[5][31]_i_3 
       (.I0(axi_awaddr[1]),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[5]),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[2]),
        .O(\slv_reg_array[5][31]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[5][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[5][7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[5][7]_i_2 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .O(\slv_reg_array[5][7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF8FF000008000000)) 
    \slv_reg_array[6][0]_i_1 
       (.I0(axibusdomain_s_axi_wdata[0]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[6][31]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[6][10]_0 [0]),
        .O(\slv_reg_array[6][0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \slv_reg_array[6][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[6][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[6][15]_i_2 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .O(\slv_reg_array[6][15]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[6][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[6][23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[6][23]_i_2 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .O(\slv_reg_array[6][23]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[6][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[6][31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[6][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .O(\slv_reg_array[6][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFBFFFFFFFF)) 
    \slv_reg_array[6][31]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[1]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[5]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[6][31]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[6][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[6][7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[6][7]_i_2 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .O(\slv_reg_array[6][7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[0][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[0][0]_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][10]_i_1_n_0 ),
        .Q(r7_clear[10]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][11]_i_1_n_0 ),
        .Q(r7_clear[11]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][12]_i_1_n_0 ),
        .Q(r7_clear[12]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][13]_i_1_n_0 ),
        .Q(r7_clear[13]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][14]_i_1_n_0 ),
        .Q(r7_clear[14]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][15]_i_3_n_0 ),
        .Q(r7_clear[15]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][16]_i_1_n_0 ),
        .Q(r7_clear[16]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][17]_i_1_n_0 ),
        .Q(r7_clear[17]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][18]_i_1_n_0 ),
        .Q(r7_clear[18]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][19]_i_1_n_0 ),
        .Q(r7_clear[19]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][1]_i_1_n_0 ),
        .Q(r7_clear[1]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][20]_i_1_n_0 ),
        .Q(r7_clear[20]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][21]_i_1_n_0 ),
        .Q(r7_clear[21]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][22]_i_1_n_0 ),
        .Q(r7_clear[22]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][23]_i_3_n_0 ),
        .Q(r7_clear[23]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][24]_i_1_n_0 ),
        .Q(r7_clear[24]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][25]_i_1_n_0 ),
        .Q(r7_clear[25]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][26]_i_1_n_0 ),
        .Q(r7_clear[26]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][27]_i_1_n_0 ),
        .Q(r7_clear[27]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][28]_i_1_n_0 ),
        .Q(r7_clear[28]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][29]_i_1_n_0 ),
        .Q(r7_clear[29]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][2]_i_1_n_0 ),
        .Q(r7_clear[2]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][30]_i_1_n_0 ),
        .Q(r7_clear[30]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][31]_i_3_n_0 ),
        .Q(r7_clear[31]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][3]_i_1_n_0 ),
        .Q(r7_clear[3]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][4]_i_1_n_0 ),
        .Q(r7_clear[4]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][5]_i_1_n_0 ),
        .Q(r7_clear[5]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][6]_i_1_n_0 ),
        .Q(r7_clear[6]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][7]_i_3_n_0 ),
        .Q(r7_clear[7]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][8]_i_1_n_0 ),
        .Q(r7_clear[8]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][9]_i_1_n_0 ),
        .Q(r7_clear[9]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][0]_i_1_n_0 ),
        .Q(i[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][10] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][10]_i_1_n_0 ),
        .Q(i[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][11]_i_1_n_0 ),
        .Q(r6_delay[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][12]_i_1_n_0 ),
        .Q(r6_delay[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][13]_i_1_n_0 ),
        .Q(r6_delay[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][14]_i_1_n_0 ),
        .Q(r6_delay[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][15]_i_2_n_0 ),
        .Q(r6_delay[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][16]_i_1_n_0 ),
        .Q(r6_delay[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][17]_i_1_n_0 ),
        .Q(r6_delay[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][18]_i_1_n_0 ),
        .Q(r6_delay[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][19]_i_1_n_0 ),
        .Q(r6_delay[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][1]_i_1_n_0 ),
        .Q(i[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][20]_i_1_n_0 ),
        .Q(r6_delay[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][21]_i_1_n_0 ),
        .Q(r6_delay[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][22]_i_1_n_0 ),
        .Q(r6_delay[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][23]_i_2_n_0 ),
        .Q(r6_delay[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][24]_i_1_n_0 ),
        .Q(r6_delay[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][25]_i_1_n_0 ),
        .Q(r6_delay[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][26]_i_1_n_0 ),
        .Q(r6_delay[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][27]_i_1_n_0 ),
        .Q(r6_delay[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][28]_i_1_n_0 ),
        .Q(r6_delay[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][29]_i_1_n_0 ),
        .Q(r6_delay[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][2] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][2]_i_1_n_0 ),
        .Q(i[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][30]_i_1_n_0 ),
        .Q(r6_delay[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][31]_i_2_n_0 ),
        .Q(r6_delay[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][3]_i_1_n_0 ),
        .Q(i[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][4]_i_1_n_0 ),
        .Q(i[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][5] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][5]_i_1_n_0 ),
        .Q(i[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][6] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][6]_i_1_n_0 ),
        .Q(i[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][7]_i_2_n_0 ),
        .Q(i[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][8]_i_1_n_0 ),
        .Q(i[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][9] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][9]_i_1_n_0 ),
        .Q(i[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][0]_i_1_n_0 ),
        .Q(d),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][10] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][10]_i_1_n_0 ),
        .Q(r5_enable[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][11]_i_1_n_0 ),
        .Q(r5_enable[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][12]_i_1_n_0 ),
        .Q(r5_enable[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][13]_i_1_n_0 ),
        .Q(r5_enable[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][14]_i_1_n_0 ),
        .Q(r5_enable[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][15]_i_2_n_0 ),
        .Q(r5_enable[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][16]_i_1_n_0 ),
        .Q(r5_enable[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][17]_i_1_n_0 ),
        .Q(r5_enable[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][18]_i_1_n_0 ),
        .Q(r5_enable[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][19]_i_1_n_0 ),
        .Q(r5_enable[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][1]_i_1_n_0 ),
        .Q(r5_enable[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][20]_i_1_n_0 ),
        .Q(r5_enable[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][21]_i_1_n_0 ),
        .Q(r5_enable[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][22]_i_1_n_0 ),
        .Q(r5_enable[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][23]_i_2_n_0 ),
        .Q(r5_enable[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][24]_i_1_n_0 ),
        .Q(r5_enable[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][25]_i_1_n_0 ),
        .Q(r5_enable[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][26]_i_1_n_0 ),
        .Q(r5_enable[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][27]_i_1_n_0 ),
        .Q(r5_enable[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][28]_i_1_n_0 ),
        .Q(r5_enable[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][29]_i_1_n_0 ),
        .Q(r5_enable[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][2] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][2]_i_1_n_0 ),
        .Q(r5_enable[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][30]_i_1_n_0 ),
        .Q(r5_enable[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][31]_i_2_n_0 ),
        .Q(r5_enable[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][3]_i_1_n_0 ),
        .Q(r5_enable[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][4]_i_1_n_0 ),
        .Q(r5_enable[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][5] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][5]_i_1_n_0 ),
        .Q(r5_enable[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][6] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][6]_i_1_n_0 ),
        .Q(r5_enable[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][7]_i_2_n_0 ),
        .Q(r5_enable[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][8]_i_1_n_0 ),
        .Q(r5_enable[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][9] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][9]_i_1_n_0 ),
        .Q(r5_enable[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][10] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][10]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][11]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][12]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][13]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][14]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][15]_i_2_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][16]_i_1_n_0 ),
        .Q(r4_threshold[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][17]_i_1_n_0 ),
        .Q(r4_threshold[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][18]_i_1_n_0 ),
        .Q(r4_threshold[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][19]_i_1_n_0 ),
        .Q(r4_threshold[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][1]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][20]_i_1_n_0 ),
        .Q(r4_threshold[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][21]_i_1_n_0 ),
        .Q(r4_threshold[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][22]_i_1_n_0 ),
        .Q(r4_threshold[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][23]_i_2_n_0 ),
        .Q(r4_threshold[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][24]_i_1_n_0 ),
        .Q(r4_threshold[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][25]_i_1_n_0 ),
        .Q(r4_threshold[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][26]_i_1_n_0 ),
        .Q(r4_threshold[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][27]_i_1_n_0 ),
        .Q(r4_threshold[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][28]_i_1_n_0 ),
        .Q(r4_threshold[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][29]_i_1_n_0 ),
        .Q(r4_threshold[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][2]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][30]_i_1_n_0 ),
        .Q(r4_threshold[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][31]_i_2_n_0 ),
        .Q(r4_threshold[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][3]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][4]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][5]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][6]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][7]_i_2_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][8]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][9] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][9]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][0]_i_1_n_0 ),
        .Q(r3_dina[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][10]_i_1_n_0 ),
        .Q(r3_dina[10]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][11]_i_1_n_0 ),
        .Q(r3_dina[11]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][12]_i_1_n_0 ),
        .Q(r3_dina[12]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][13]_i_1_n_0 ),
        .Q(r3_dina[13]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][14]_i_1_n_0 ),
        .Q(r3_dina[14]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][15]_i_3_n_0 ),
        .Q(r3_dina[15]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][16]_i_1_n_0 ),
        .Q(r3_dina[16]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][17]_i_1_n_0 ),
        .Q(r3_dina[17]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][18]_i_1_n_0 ),
        .Q(r3_dina[18]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][19]_i_1_n_0 ),
        .Q(r3_dina[19]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][1]_i_1_n_0 ),
        .Q(r3_dina[1]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][20]_i_1_n_0 ),
        .Q(r3_dina[20]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][21]_i_1_n_0 ),
        .Q(r3_dina[21]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][22]_i_1_n_0 ),
        .Q(r3_dina[22]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][23]_i_3_n_0 ),
        .Q(r3_dina[23]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][24]_i_1_n_0 ),
        .Q(r3_dina[24]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][25]_i_1_n_0 ),
        .Q(r3_dina[25]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][26]_i_1_n_0 ),
        .Q(r3_dina[26]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][27]_i_1_n_0 ),
        .Q(r3_dina[27]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][28]_i_1_n_0 ),
        .Q(r3_dina[28]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][29]_i_1_n_0 ),
        .Q(r3_dina[29]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][2]_i_1_n_0 ),
        .Q(r3_dina[2]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][30]_i_1_n_0 ),
        .Q(r3_dina[30]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][31]_i_3_n_0 ),
        .Q(r3_dina[31]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][3]_i_1_n_0 ),
        .Q(r3_dina[3]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][4]_i_1_n_0 ),
        .Q(r3_dina[4]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][5]_i_1_n_0 ),
        .Q(r3_dina[5]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][6]_i_1_n_0 ),
        .Q(r3_dina[6]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][7]_i_3_n_0 ),
        .Q(r3_dina[7]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][8]_i_1_n_0 ),
        .Q(r3_dina[8]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][9]_i_1_n_0 ),
        .Q(r3_dina[9]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[5][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[5][0]_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][10]_i_1_n_0 ),
        .Q(r2_wea[10]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][11]_i_1_n_0 ),
        .Q(r2_wea[11]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][12]_i_1_n_0 ),
        .Q(r2_wea[12]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][13]_i_1_n_0 ),
        .Q(r2_wea[13]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][14]_i_1_n_0 ),
        .Q(r2_wea[14]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][15]_i_3_n_0 ),
        .Q(r2_wea[15]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][16]_i_1_n_0 ),
        .Q(r2_wea[16]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][17]_i_1_n_0 ),
        .Q(r2_wea[17]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][18]_i_1_n_0 ),
        .Q(r2_wea[18]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][19]_i_1_n_0 ),
        .Q(r2_wea[19]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][1]_i_1_n_0 ),
        .Q(r2_wea[1]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][20]_i_1_n_0 ),
        .Q(r2_wea[20]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][21]_i_1_n_0 ),
        .Q(r2_wea[21]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][22]_i_1_n_0 ),
        .Q(r2_wea[22]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][23]_i_3_n_0 ),
        .Q(r2_wea[23]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][24]_i_1_n_0 ),
        .Q(r2_wea[24]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][25]_i_1_n_0 ),
        .Q(r2_wea[25]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][26]_i_1_n_0 ),
        .Q(r2_wea[26]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][27]_i_1_n_0 ),
        .Q(r2_wea[27]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][28]_i_1_n_0 ),
        .Q(r2_wea[28]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][29]_i_1_n_0 ),
        .Q(r2_wea[29]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][2]_i_1_n_0 ),
        .Q(r2_wea[2]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][30]_i_1_n_0 ),
        .Q(r2_wea[30]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][31]_i_3_n_0 ),
        .Q(r2_wea[31]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][3]_i_1_n_0 ),
        .Q(r2_wea[3]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][4]_i_1_n_0 ),
        .Q(r2_wea[4]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][5]_i_1_n_0 ),
        .Q(r2_wea[5]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][6]_i_1_n_0 ),
        .Q(r2_wea[6]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][7]_i_3_n_0 ),
        .Q(r2_wea[7]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][8]_i_1_n_0 ),
        .Q(r2_wea[8]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][9]_i_1_n_0 ),
        .Q(r2_wea[9]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[6][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][10]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [10]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][11]_i_1_n_0 ),
        .Q(r1_addra[11]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][12]_i_1_n_0 ),
        .Q(r1_addra[12]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][13]_i_1_n_0 ),
        .Q(r1_addra[13]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][14]_i_1_n_0 ),
        .Q(r1_addra[14]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][15]_i_3_n_0 ),
        .Q(r1_addra[15]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][16]_i_1_n_0 ),
        .Q(r1_addra[16]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][17]_i_1_n_0 ),
        .Q(r1_addra[17]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][18]_i_1_n_0 ),
        .Q(r1_addra[18]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][19]_i_1_n_0 ),
        .Q(r1_addra[19]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][1]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [1]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][20]_i_1_n_0 ),
        .Q(r1_addra[20]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][21]_i_1_n_0 ),
        .Q(r1_addra[21]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][22]_i_1_n_0 ),
        .Q(r1_addra[22]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][23]_i_3_n_0 ),
        .Q(r1_addra[23]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][24]_i_1_n_0 ),
        .Q(r1_addra[24]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][25]_i_1_n_0 ),
        .Q(r1_addra[25]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][26]_i_1_n_0 ),
        .Q(r1_addra[26]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][27]_i_1_n_0 ),
        .Q(r1_addra[27]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][28]_i_1_n_0 ),
        .Q(r1_addra[28]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][29]_i_1_n_0 ),
        .Q(r1_addra[29]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][2]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [2]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][30]_i_1_n_0 ),
        .Q(r1_addra[30]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][31]_i_3_n_0 ),
        .Q(r1_addra[31]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][3]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [3]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][4]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [4]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][5]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [5]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][6]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [6]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][7]_i_3_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [7]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][8]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [8]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][9]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [9]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(axibusdomain_s_axi_arvalid),
        .I1(axibusdomain_s_axi_rvalid),
        .I2(axi_arready_reg_0),
        .O(slv_reg_rden__0));
endmodule

(* ORIG_REF_NAME = "ip_scope" *) 
module design_1_ip_scope_0_0_ip_scope
   (ch1,
    ch2,
    ch_trigger,
    axibusdomain_clk,
    signaldomain_clk,
    axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready,
    full,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid);
  input [15:0]ch1;
  input [15:0]ch2;
  input [15:0]ch_trigger;
  input axibusdomain_clk;
  input signaldomain_clk;
  input axibusdomain_aresetn;
  input [5:0]axibusdomain_s_axi_awaddr;
  input axibusdomain_s_axi_awvalid;
  input [31:0]axibusdomain_s_axi_wdata;
  input [3:0]axibusdomain_s_axi_wstrb;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_bready;
  input [5:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_rready;
  output [0:0]full;
  output axibusdomain_s_axi_awready;
  output axibusdomain_s_axi_wready;
  output [1:0]axibusdomain_s_axi_bresp;
  output axibusdomain_s_axi_bvalid;
  output axibusdomain_s_axi_arready;
  output [31:0]axibusdomain_s_axi_rdata;
  output [1:0]axibusdomain_s_axi_rresp;
  output axibusdomain_s_axi_rvalid;

  wire \<const0> ;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [5:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [5:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [15:0]ch1;
  wire [15:0]ch2;
  wire [15:0]ch_trigger;
  wire [0:0]full;
  wire [10:0]r1_addra;
  wire [0:0]r2_wea;
  wire [31:0]r3_dina;
  wire [15:0]r4_threshold;
  wire [0:0]r5_enable;
  wire [10:0]r6_delay;
  wire [0:0]r7_clear;
  wire [0:0]r8_full;
  wire [31:0]r9_douta;
  wire signaldomain_clk;

  assign axibusdomain_s_axi_bresp[1] = \<const0> ;
  assign axibusdomain_s_axi_bresp[0] = \<const0> ;
  assign axibusdomain_s_axi_rresp[1] = \<const0> ;
  assign axibusdomain_s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_ip_scope_0_0_axibusdomain_axi_lite_interface axibusdomain_axi_lite_interface
       (.axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_arready),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_awready),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_bready),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_rready),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_wready),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .d(r5_enable),
        .i(r6_delay),
        .q(r8_full),
        .r3_dina(r3_dina),
        .r9_douta(r9_douta),
        .\slv_reg_array_reg[0][0] (r7_clear),
        .\slv_reg_array_reg[3][15] (r4_threshold),
        .\slv_reg_array_reg[5][0] (r2_wea),
        .\slv_reg_array_reg[6][10] (r1_addra));
  design_1_ip_scope_0_0_ip_scope_struct ip_scope_struct
       (.axibusdomain_clk(axibusdomain_clk),
        .ch1(ch1),
        .ch2(ch2),
        .ch_trigger(ch_trigger),
        .full(full),
        .r1_addra(r1_addra),
        .r2_wea(r2_wea),
        .r3_dina(r3_dina),
        .r4_threshold(r4_threshold),
        .r5_enable(r5_enable),
        .r6_delay(r6_delay),
        .r7_clear(r7_clear),
        .r8_full(r8_full),
        .r9_douta(r9_douta),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* CHECK_LICENSE_TYPE = "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_2,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "ip_scope_blk_mem_gen_i0" *) 
(* X_CORE_INFO = "blk_mem_gen_v8_4_2,Vivado 2018.3" *) 
module design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0
   (clka,
    ena,
    wea,
    addra,
    dina,
    douta,
    clkb,
    enb,
    web,
    addrb,
    dinb,
    doutb);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB EN" *) input enb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) input [0:0]web;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [10:0]addrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) input [31:0]dinb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [31:0]doutb;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "2" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     10.698 mW" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "1" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ip_scope_blk_mem_gen_i0.mem" *) 
  (* C_INIT_FILE_NAME = "ip_scope_blk_mem_gen_i0.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "2" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "2048" *) 
  (* C_READ_DEPTH_B = "2048" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "2048" *) 
  (* C_WRITE_DEPTH_B = "2048" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_family = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(enb),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(web));
endmodule

(* CHECK_LICENSE_TYPE = "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_2,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "ip_scope_blk_mem_gen_i1" *) 
(* X_CORE_INFO = "blk_mem_gen_v8_4_2,Vivado 2018.3" *) 
module design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1
   (clka,
    ena,
    wea,
    addra,
    dina,
    douta);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "2" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     5.6824 mW" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ip_scope_blk_mem_gen_i1.mem" *) 
  (* C_INIT_FILE_NAME = "ip_scope_blk_mem_gen_i1.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "2048" *) 
  (* C_READ_DEPTH_B = "2048" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "2048" *) 
  (* C_WRITE_DEPTH_B = "2048" *) 
  (* C_WRITE_MODE_A = "READ_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_family = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_12,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "ip_scope_c_counter_binary_v12_0_i0" *) 
(* X_CORE_INFO = "c_counter_binary_v12_0_12,Vivado 2018.3" *) 
module design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0
   (CLK,
    CE,
    SINIT,
    LOAD,
    L,
    Q);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 sinit_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef" *) input SINIT;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [10:0]L;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [10:0]Q;

  wire CE;
  wire CLK;
  wire [10:0]L;
  wire LOAD;
  wire [10:0]Q;
  wire SINIT;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "11" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_scope_0_0_c_counter_binary_v12_0_12 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule

(* ORIG_REF_NAME = "ip_scope_delayline" *) 
module design_1_ip_scope_0_0_ip_scope_delayline
   (dinb,
    signaldomain_clk,
    dina,
    o);
  output [31:0]dinb;
  input signaldomain_clk;
  input [31:0]dina;
  input [10:0]o;

  wire [10:0]counter_op_net;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [10:0]o;
  wire relational_op_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_ip_scope_xlcounter_free counter
       (.LOAD(relational_op_net),
        .Q(counter_op_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_sysgen_relational_f845914c88 relational
       (.LOAD(relational_op_net),
        .Q(counter_op_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlspram single_port_ram
       (.Q(counter_op_net),
        .dina(dina),
        .dinb(dinb),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_leveltriggerfifocontroller" *) 
module design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller
   (\full_i_5_24_reg[0] ,
    web,
    Q,
    signaldomain_clk,
    DI,
    S,
    \state_4_23_reg[1] ,
    \state_4_23_reg[1]_0 ,
    register_q_net,
    \state_4_23_reg[1]_1 ,
    register4_q_net);
  output [0:0]\full_i_5_24_reg[0] ;
  output [0:0]web;
  output [10:0]Q;
  input signaldomain_clk;
  input [3:0]DI;
  input [3:0]S;
  input [3:0]\state_4_23_reg[1] ;
  input [3:0]\state_4_23_reg[1]_0 ;
  input register_q_net;
  input \state_4_23_reg[1]_1 ;
  input [0:0]register4_q_net;

  wire [3:0]DI;
  wire [10:0]Q;
  wire [3:0]S;
  wire [0:0]\full_i_5_24_reg[0] ;
  wire [0:0]register4_q_net;
  wire register_q_net;
  wire signaldomain_clk;
  wire [3:0]\state_4_23_reg[1] ;
  wire [3:0]\state_4_23_reg[1]_0 ;
  wire \state_4_23_reg[1]_1 ;
  wire [0:0]web;

  design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d mcode
       (.DI(DI),
        .Q(Q),
        .S(S),
        .\full_i_5_24_reg[0]_0 (\full_i_5_24_reg[0] ),
        .register4_q_net(register4_q_net),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk),
        .\state_4_23_reg[1]_0 (\state_4_23_reg[1] ),
        .\state_4_23_reg[1]_1 (\state_4_23_reg[1]_0 ),
        .\state_4_23_reg[1]_2 (\state_4_23_reg[1]_1 ),
        .web(web));
endmodule

(* ORIG_REF_NAME = "ip_scope_risingedgetrigger" *) 
module design_1_ip_scope_0_0_ip_scope_risingedgetrigger
   (\ff_2_17_reg[0] ,
    register4_q_net,
    signaldomain_clk);
  output \ff_2_17_reg[0] ;
  input [0:0]register4_q_net;
  input signaldomain_clk;

  wire \ff_2_17_reg[0] ;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a mcode
       (.\ff_2_17_reg[0]_0 (\ff_2_17_reg[0] ),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_signaldomain" *) 
module design_1_ip_scope_0_0_ip_scope_signaldomain
   (dinb,
    full,
    \full_i_5_24_reg[0] ,
    addrb,
    web,
    signaldomain_clk,
    enable,
    ch_trigger,
    ch1,
    ch2,
    clear,
    i,
    o);
  output [31:0]dinb;
  output [0:0]full;
  output [0:0]\full_i_5_24_reg[0] ;
  output [10:0]addrb;
  output [0:0]web;
  input signaldomain_clk;
  input [0:0]enable;
  input [15:0]ch_trigger;
  input [15:0]ch1;
  input [15:0]ch2;
  input [0:0]clear;
  input [15:0]i;
  input [10:0]o;

  wire [10:0]addrb;
  wire [15:0]ch1;
  wire [15:0]ch2;
  wire [15:0]ch_trigger;
  wire [0:0]clear;
  wire [31:0]dinb;
  wire [0:0]enable;
  wire [0:0]full;
  wire [0:0]\full_i_5_24_reg[0] ;
  wire [15:0]i;
  wire [10:0]o;
  wire register1_n_0;
  wire register1_n_1;
  wire register1_n_2;
  wire register1_n_20;
  wire register1_n_21;
  wire register1_n_22;
  wire register1_n_3;
  wire [15:0]register1_q_net_x0;
  wire [15:0]register2_q_net_x0;
  wire [15:0]register3_q_net_x0;
  wire [0:0]register4_q_net;
  wire register5_n_0;
  wire register5_n_1;
  wire register5_n_18;
  wire register5_n_19;
  wire register5_n_2;
  wire register5_n_20;
  wire register5_n_21;
  wire register5_n_22;
  wire register5_n_3;
  wire [13:0]register5_q_net;
  wire register_q_net;
  wire risingedgetrigger_n_0;
  wire signaldomain_clk;
  wire [0:0]web;

  design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0 convert
       (.full(full),
        .\reg_array[0].fde_used.u2 (\full_i_5_24_reg[0] ),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_delayline delayline
       (.dina({register2_q_net_x0,register3_q_net_x0}),
        .dinb(dinb),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller leveltriggerfifocontroller
       (.DI({register1_n_0,register1_n_1,register1_n_2,register1_n_3}),
        .Q(addrb),
        .S({register5_n_0,register5_n_1,register5_n_2,register5_n_3}),
        .\full_i_5_24_reg[0] (\full_i_5_24_reg[0] ),
        .register4_q_net(register4_q_net),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk),
        .\state_4_23_reg[1] ({register5_n_22,register1_n_20,register1_n_21,register1_n_22}),
        .\state_4_23_reg[1]_0 ({register5_n_18,register5_n_19,register5_n_20,register5_n_21}),
        .\state_4_23_reg[1]_1 (risingedgetrigger_n_0),
        .web(web));
  design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0 register1
       (.DI({register1_n_0,register1_n_1,register1_n_2,register1_n_3}),
        .ch_trigger(ch_trigger),
        .\fd_prim_array[13].bit_is_0.fdre_comp ({register1_n_20,register1_n_21,register1_n_22}),
        .o(register1_q_net_x0),
        .rel_39_16_carry__0(register5_q_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1 register2
       (.ch1(ch1),
        .o(register2_q_net_x0),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2 register3
       (.ch2(ch2),
        .o(register3_q_net_x0),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1 register4
       (.clear(clear),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3 register5
       (.S({register5_n_0,register5_n_1,register5_n_2,register5_n_3}),
        .\fd_prim_array[15].bit_is_0.fdre_comp ({register5_n_18,register5_n_19,register5_n_20,register5_n_21}),
        .\fd_prim_array[15].bit_is_0.fdre_comp_0 (register5_n_22),
        .i(i),
        .o(register5_q_net),
        .rel_39_16_carry__0(register1_q_net_x0),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlregister register_x0
       (.enable(enable),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_risingedgetrigger risingedgetrigger
       (.\ff_2_17_reg[0] (risingedgetrigger_n_0),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_struct" *) 
module design_1_ip_scope_0_0_ip_scope_struct
   (full,
    r9_douta,
    r8_full,
    signaldomain_clk,
    ch_trigger,
    ch1,
    ch2,
    axibusdomain_clk,
    r2_wea,
    r1_addra,
    r3_dina,
    r4_threshold,
    r5_enable,
    r6_delay,
    r7_clear);
  output [0:0]full;
  output [31:0]r9_douta;
  output [0:0]r8_full;
  input signaldomain_clk;
  input [15:0]ch_trigger;
  input [15:0]ch1;
  input [15:0]ch2;
  input axibusdomain_clk;
  input [0:0]r2_wea;
  input [10:0]r1_addra;
  input [31:0]r3_dina;
  input [15:0]r4_threshold;
  input [0:0]r5_enable;
  input [10:0]r6_delay;
  input [0:0]r7_clear;

  wire axibusdomain_clk;
  wire [15:0]ch1;
  wire [15:0]ch2;
  wire [15:0]ch_trigger;
  wire [0:0]convert_dout_net_x1;
  wire [0:0]full;
  wire mcode_we_net;
  wire [10:0]r1_addra;
  wire [0:0]r2_wea;
  wire [31:0]r3_dina;
  wire [15:0]r4_threshold;
  wire [0:0]r5_enable;
  wire [10:0]r6_delay;
  wire [0:0]r7_clear;
  wire [0:0]r8_full;
  wire [31:0]r9_douta;
  wire register1_q_net;
  wire [10:0]register2_q_net;
  wire [0:0]register3_q_net;
  wire [15:0]register_q_net;
  wire signaldomain_clk;
  wire [31:0]single_port_ram_data_out_net;
  wire [10:0]slice1_y_net;

  design_1_ip_scope_0_0_ip_scope_xldpram dual_port_ram
       (.addrb(slice1_y_net),
        .axibusdomain_clk(axibusdomain_clk),
        .dinb(single_port_ram_data_out_net),
        .r1_addra(r1_addra),
        .r2_wea(r2_wea),
        .r3_dina(r3_dina),
        .r9_douta(r9_douta),
        .signaldomain_clk(signaldomain_clk),
        .web(mcode_we_net));
  design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0 register1
       (.axibusdomain_clk(axibusdomain_clk),
        .q(register1_q_net),
        .r5_enable(r5_enable),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1 register2
       (.axibusdomain_clk(axibusdomain_clk),
        .o(register2_q_net),
        .r6_delay(r6_delay),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2 register3
       (.axibusdomain_clk(axibusdomain_clk),
        .q(register3_q_net),
        .r7_clear(r7_clear),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0 register4
       (.axibusdomain_clk(axibusdomain_clk),
        .full(convert_dout_net_x1),
        .r8_full(r8_full),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_xlAsynRegister register_x0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(register_q_net),
        .r4_threshold(r4_threshold),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_ip_scope_signaldomain signaldomain
       (.addrb(slice1_y_net),
        .ch1(ch1),
        .ch2(ch2),
        .ch_trigger(ch_trigger),
        .clear(register3_q_net),
        .dinb(single_port_ram_data_out_net),
        .enable(register1_q_net),
        .full(convert_dout_net_x1),
        .\full_i_5_24_reg[0] (full),
        .i(register_q_net),
        .o(register2_q_net),
        .signaldomain_clk(signaldomain_clk),
        .web(mcode_we_net));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module design_1_ip_scope_0_0_ip_scope_xlAsynRegister
   (o,
    r4_threshold,
    axibusdomain_clk,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]r4_threshold;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [15:0]d1_net;
  wire [15:0]d2_net;
  wire [15:0]d3_net;
  wire [15:0]o;
  wire [15:0]r4_threshold;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r4_threshold(r4_threshold));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13 synth_reg_inst_3
       (.i(d3_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0
   (q,
    r5_enable,
    axibusdomain_clk,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]r5_enable;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire d1_net;
  wire d2_net;
  wire d3_net;
  wire [0:0]q;
  wire [0:0]r5_enable;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r5_enable(r5_enable));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41 synth_reg_inst_1
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42 synth_reg_inst_2
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43 synth_reg_inst_3
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1
   (o,
    r6_delay,
    axibusdomain_clk,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]r6_delay;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [10:0]d1_net;
  wire [10:0]d2_net;
  wire [10:0]d3_net;
  wire [10:0]o;
  wire [10:0]r6_delay;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r6_delay(r6_delay));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36 synth_reg_inst_3
       (.i(d3_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2
   (q,
    r7_clear,
    axibusdomain_clk,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]r7_clear;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire [0:0]q;
  wire [0:0]r7_clear;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r7_clear(r7_clear));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27 synth_reg_inst_1
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28 synth_reg_inst_2
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29 synth_reg_inst_3
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0
   (r8_full,
    full,
    signaldomain_clk,
    axibusdomain_clk);
  output [0:0]r8_full;
  input [0:0]full;
  input signaldomain_clk;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire [0:0]full;
  wire [0:0]r8_full;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18 synth_reg_inst_0
       (.d1_net(d1_net),
        .full(full),
        .signaldomain_clk(signaldomain_clk));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19 synth_reg_inst_1
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .d2_net(d2_net));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20 synth_reg_inst_2
       (.axibusdomain_clk(axibusdomain_clk),
        .d2_net(d2_net),
        .d3_net(d3_net));
  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21 synth_reg_inst_3
       (.axibusdomain_clk(axibusdomain_clk),
        .d3_net(d3_net),
        .r8_full(r8_full));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlconvert" *) 
module design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0
   (full,
    \reg_array[0].fde_used.u2 ,
    signaldomain_clk);
  output [0:0]full;
  input [0:0]\reg_array[0].fde_used.u2 ;
  input signaldomain_clk;

  wire [0:0]full;
  wire [0:0]\reg_array[0].fde_used.u2 ;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg \latency_test.reg 
       (.full(full),
        .\reg_array[0].fde_used.u2 (\reg_array[0].fde_used.u2 ),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlcounter_free" *) 
module design_1_ip_scope_0_0_ip_scope_xlcounter_free
   (Q,
    signaldomain_clk,
    LOAD);
  output [10:0]Q;
  input signaldomain_clk;
  input LOAD;

  wire LOAD;
  wire [10:0]Q;
  wire signaldomain_clk;

  (* CHECK_LICENSE_TYPE = "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_12,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "c_counter_binary_v12_0_12,Vivado 2018.3" *) 
  design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 \comp0.core_instance0 
       (.CE(1'b1),
        .CLK(signaldomain_clk),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(LOAD),
        .Q(Q),
        .SINIT(1'b0));
endmodule

(* ORIG_REF_NAME = "ip_scope_xldpram" *) 
module design_1_ip_scope_0_0_ip_scope_xldpram
   (r9_douta,
    axibusdomain_clk,
    r2_wea,
    r1_addra,
    r3_dina,
    signaldomain_clk,
    web,
    addrb,
    dinb);
  output [31:0]r9_douta;
  input axibusdomain_clk;
  input [0:0]r2_wea;
  input [10:0]r1_addra;
  input [31:0]r3_dina;
  input signaldomain_clk;
  input [0:0]web;
  input [10:0]addrb;
  input [31:0]dinb;

  wire [10:0]addrb;
  wire axibusdomain_clk;
  wire \comp0.core_instance0_n_32 ;
  wire \comp0.core_instance0_n_33 ;
  wire \comp0.core_instance0_n_34 ;
  wire \comp0.core_instance0_n_35 ;
  wire \comp0.core_instance0_n_36 ;
  wire \comp0.core_instance0_n_37 ;
  wire \comp0.core_instance0_n_38 ;
  wire \comp0.core_instance0_n_39 ;
  wire \comp0.core_instance0_n_40 ;
  wire \comp0.core_instance0_n_41 ;
  wire \comp0.core_instance0_n_42 ;
  wire \comp0.core_instance0_n_43 ;
  wire \comp0.core_instance0_n_44 ;
  wire \comp0.core_instance0_n_45 ;
  wire \comp0.core_instance0_n_46 ;
  wire \comp0.core_instance0_n_47 ;
  wire \comp0.core_instance0_n_48 ;
  wire \comp0.core_instance0_n_49 ;
  wire \comp0.core_instance0_n_50 ;
  wire \comp0.core_instance0_n_51 ;
  wire \comp0.core_instance0_n_52 ;
  wire \comp0.core_instance0_n_53 ;
  wire \comp0.core_instance0_n_54 ;
  wire \comp0.core_instance0_n_55 ;
  wire \comp0.core_instance0_n_56 ;
  wire \comp0.core_instance0_n_57 ;
  wire \comp0.core_instance0_n_58 ;
  wire \comp0.core_instance0_n_59 ;
  wire \comp0.core_instance0_n_60 ;
  wire \comp0.core_instance0_n_61 ;
  wire \comp0.core_instance0_n_62 ;
  wire \comp0.core_instance0_n_63 ;
  wire [31:0]dinb;
  wire [10:0]r1_addra;
  wire [0:0]r2_wea;
  wire [31:0]r3_dina;
  wire [31:0]r9_douta;
  wire signaldomain_clk;
  wire [0:0]web;

  (* CHECK_LICENSE_TYPE = "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_2,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "blk_mem_gen_v8_4_2,Vivado 2018.3" *) 
  design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 \comp0.core_instance0 
       (.addra(r1_addra),
        .addrb(addrb),
        .clka(axibusdomain_clk),
        .clkb(signaldomain_clk),
        .dina(r3_dina),
        .dinb(dinb),
        .douta(r9_douta),
        .doutb({\comp0.core_instance0_n_32 ,\comp0.core_instance0_n_33 ,\comp0.core_instance0_n_34 ,\comp0.core_instance0_n_35 ,\comp0.core_instance0_n_36 ,\comp0.core_instance0_n_37 ,\comp0.core_instance0_n_38 ,\comp0.core_instance0_n_39 ,\comp0.core_instance0_n_40 ,\comp0.core_instance0_n_41 ,\comp0.core_instance0_n_42 ,\comp0.core_instance0_n_43 ,\comp0.core_instance0_n_44 ,\comp0.core_instance0_n_45 ,\comp0.core_instance0_n_46 ,\comp0.core_instance0_n_47 ,\comp0.core_instance0_n_48 ,\comp0.core_instance0_n_49 ,\comp0.core_instance0_n_50 ,\comp0.core_instance0_n_51 ,\comp0.core_instance0_n_52 ,\comp0.core_instance0_n_53 ,\comp0.core_instance0_n_54 ,\comp0.core_instance0_n_55 ,\comp0.core_instance0_n_56 ,\comp0.core_instance0_n_57 ,\comp0.core_instance0_n_58 ,\comp0.core_instance0_n_59 ,\comp0.core_instance0_n_60 ,\comp0.core_instance0_n_61 ,\comp0.core_instance0_n_62 ,\comp0.core_instance0_n_63 }),
        .ena(1'b1),
        .enb(1'b1),
        .wea(r2_wea),
        .web(web));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module design_1_ip_scope_0_0_ip_scope_xlregister
   (register_q_net,
    enable,
    signaldomain_clk);
  output register_q_net;
  input [0:0]enable;
  input signaldomain_clk;

  wire [0:0]enable;
  wire register_q_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0 synth_reg_inst
       (.enable(enable),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0
   (DI,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp ,
    rel_39_16_carry__0,
    ch_trigger,
    signaldomain_clk);
  output [3:0]DI;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  input [13:0]rel_39_16_carry__0;
  input [15:0]ch_trigger;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [15:0]ch_trigger;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  wire [15:0]o;
  wire [13:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8 synth_reg_inst
       (.DI(DI),
        .ch_trigger(ch_trigger),
        .\fd_prim_array[13].bit_is_0.fdre_comp (\fd_prim_array[13].bit_is_0.fdre_comp ),
        .o(o),
        .rel_39_16_carry__0(rel_39_16_carry__0),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1
   (o,
    ch1,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch1;
  input signaldomain_clk;

  wire [15:0]ch1;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6 synth_reg_inst
       (.ch1(ch1),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2
   (o,
    ch2,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch2;
  input signaldomain_clk;

  wire [15:0]ch2;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4 synth_reg_inst
       (.ch2(ch2),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3
   (S,
    o,
    \fd_prim_array[15].bit_is_0.fdre_comp ,
    \fd_prim_array[15].bit_is_0.fdre_comp_0 ,
    rel_39_16_carry__0,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [13:0]o;
  output [3:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]S;
  wire [3:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire [13:0]o;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1 synth_reg_inst
       (.S(S),
        .\fd_prim_array[15].bit_is_0.fdre_comp (\fd_prim_array[15].bit_is_0.fdre_comp ),
        .\fd_prim_array[15].bit_is_0.fdre_comp_0 (\fd_prim_array[15].bit_is_0.fdre_comp_0 ),
        .i(i),
        .o(o),
        .rel_39_16_carry__0(rel_39_16_carry__0),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1
   (register4_q_net,
    clear,
    signaldomain_clk);
  output [0:0]register4_q_net;
  input [0:0]clear;
  input signaldomain_clk;

  wire [0:0]clear;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2 synth_reg_inst
       (.clear(clear),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlspram" *) 
module design_1_ip_scope_0_0_ip_scope_xlspram
   (dinb,
    signaldomain_clk,
    Q,
    dina);
  output [31:0]dinb;
  input signaldomain_clk;
  input [10:0]Q;
  input [31:0]dina;

  wire [10:0]Q;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire signaldomain_clk;

  (* CHECK_LICENSE_TYPE = "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_2,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "blk_mem_gen_v8_4_2,Vivado 2018.3" *) 
  design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 \comp0.core_instance0 
       (.addra(Q),
        .clka(signaldomain_clk),
        .dina(dina),
        .douta(dinb),
        .ena(1'b1),
        .wea(1'b1));
endmodule

(* ORIG_REF_NAME = "sysgen_mcode_block_2be3aaba4a" *) 
module design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a
   (\ff_2_17_reg[0]_0 ,
    register4_q_net,
    signaldomain_clk);
  output \ff_2_17_reg[0]_0 ;
  input [0:0]register4_q_net;
  input signaldomain_clk;

  wire \ff_2_17_reg[0]_0 ;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  FDRE #(
    .INIT(1'b0)) 
    \ff_2_17_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(register4_q_net),
        .Q(\ff_2_17_reg[0]_0 ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "sysgen_mcode_block_46de72b52d" *) 
module design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d
   (\full_i_5_24_reg[0]_0 ,
    web,
    Q,
    signaldomain_clk,
    DI,
    S,
    \state_4_23_reg[1]_0 ,
    \state_4_23_reg[1]_1 ,
    register_q_net,
    \state_4_23_reg[1]_2 ,
    register4_q_net);
  output [0:0]\full_i_5_24_reg[0]_0 ;
  output [0:0]web;
  output [10:0]Q;
  input signaldomain_clk;
  input [3:0]DI;
  input [3:0]S;
  input [3:0]\state_4_23_reg[1]_0 ;
  input [3:0]\state_4_23_reg[1]_1 ;
  input register_q_net;
  input \state_4_23_reg[1]_2 ;
  input [0:0]register4_q_net;

  wire [3:0]DI;
  wire [10:0]Q;
  wire [3:0]S;
  wire \addr_i_6_24[7]_i_2_n_0 ;
  wire \full_i_5_24[0]_i_1_n_0 ;
  wire [0:0]\full_i_5_24_reg[0]_0 ;
  wire [10:0]p_0_in;
  wire [0:0]register4_q_net;
  wire register_q_net;
  wire rel_39_16;
  wire rel_39_16_carry__0_n_1;
  wire rel_39_16_carry__0_n_2;
  wire rel_39_16_carry__0_n_3;
  wire rel_39_16_carry_n_0;
  wire rel_39_16_carry_n_1;
  wire rel_39_16_carry_n_2;
  wire rel_39_16_carry_n_3;
  wire signaldomain_clk;
  wire [1:0]state_4_23;
  wire \state_4_23[0]_i_1_n_0 ;
  wire \state_4_23[0]_i_2_n_0 ;
  wire \state_4_23[0]_i_3_n_0 ;
  wire \state_4_23[1]_i_1_n_0 ;
  wire [3:0]\state_4_23_reg[1]_0 ;
  wire [3:0]\state_4_23_reg[1]_1 ;
  wire \state_4_23_reg[1]_2 ;
  wire [0:0]web;
  wire wm_8_20_inv_i_1_n_0;
  wire wm_8_20_reg_inv_n_0;
  wire [3:0]NLW_rel_39_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_39_16_carry__0_O_UNCONNECTED;

  LUT1 #(
    .INIT(2'h1)) 
    \addr_i_6_24[0]_i_1 
       (.I0(Q[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \addr_i_6_24[10]_i_1 
       (.I0(\state_4_23[0]_i_3_n_0 ),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(Q[10]),
        .O(p_0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \addr_i_6_24[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \addr_i_6_24[2]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \addr_i_6_24[3]_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \addr_i_6_24[4]_i_1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \addr_i_6_24[5]_i_1 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[5]),
        .O(p_0_in[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \addr_i_6_24[6]_i_1 
       (.I0(\addr_i_6_24[7]_i_2_n_0 ),
        .I1(Q[6]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \addr_i_6_24[7]_i_1 
       (.I0(Q[6]),
        .I1(\addr_i_6_24[7]_i_2_n_0 ),
        .I2(Q[7]),
        .O(p_0_in[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \addr_i_6_24[7]_i_2 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[5]),
        .O(\addr_i_6_24[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \addr_i_6_24[8]_i_1 
       (.I0(\state_4_23[0]_i_3_n_0 ),
        .I1(Q[8]),
        .O(p_0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \addr_i_6_24[9]_i_1 
       (.I0(Q[8]),
        .I1(\state_4_23[0]_i_3_n_0 ),
        .I2(Q[9]),
        .O(p_0_in[9]));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(Q[0]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[10] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(Q[10]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[1] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(Q[1]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[2] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(Q[2]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[3] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(Q[3]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[4] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(Q[4]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[5] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(Q[5]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[6] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(Q[6]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[7] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(Q[7]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[8] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(Q[8]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[9] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(Q[9]),
        .R(wm_8_20_reg_inv_n_0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \full_i_5_24[0]_i_1 
       (.I0(register_q_net),
        .I1(state_4_23[1]),
        .I2(state_4_23[0]),
        .O(\full_i_5_24[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \full_i_5_24_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\full_i_5_24[0]_i_1_n_0 ),
        .Q(\full_i_5_24_reg[0]_0 ),
        .R(1'b0));
  CARRY4 rel_39_16_carry
       (.CI(1'b0),
        .CO({rel_39_16_carry_n_0,rel_39_16_carry_n_1,rel_39_16_carry_n_2,rel_39_16_carry_n_3}),
        .CYINIT(1'b0),
        .DI(DI),
        .O(NLW_rel_39_16_carry_O_UNCONNECTED[3:0]),
        .S(S));
  CARRY4 rel_39_16_carry__0
       (.CI(rel_39_16_carry_n_0),
        .CO({rel_39_16,rel_39_16_carry__0_n_1,rel_39_16_carry__0_n_2,rel_39_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_4_23_reg[1]_0 ),
        .O(NLW_rel_39_16_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_4_23_reg[1]_1 ));
  LUT6 #(
    .INIT(64'hFFFFF555F777FFFF)) 
    \state_4_23[0]_i_1 
       (.I0(register_q_net),
        .I1(rel_39_16),
        .I2(\state_4_23[0]_i_2_n_0 ),
        .I3(\state_4_23[0]_i_3_n_0 ),
        .I4(state_4_23[0]),
        .I5(state_4_23[1]),
        .O(\state_4_23[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \state_4_23[0]_i_2 
       (.I0(Q[8]),
        .I1(Q[9]),
        .I2(Q[10]),
        .I3(state_4_23[1]),
        .O(\state_4_23[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \state_4_23[0]_i_3 
       (.I0(Q[6]),
        .I1(\addr_i_6_24[7]_i_2_n_0 ),
        .I2(Q[7]),
        .O(\state_4_23[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFFFCC0000000000)) 
    \state_4_23[1]_i_1 
       (.I0(\state_4_23_reg[1]_2 ),
        .I1(rel_39_16),
        .I2(register4_q_net),
        .I3(state_4_23[0]),
        .I4(state_4_23[1]),
        .I5(register_q_net),
        .O(\state_4_23[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_4_23_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\state_4_23[0]_i_1_n_0 ),
        .Q(state_4_23[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \state_4_23_reg[1] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\state_4_23[1]_i_1_n_0 ),
        .Q(state_4_23[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    we_i_7_22_reg
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(1'b1),
        .Q(web),
        .R(wm_8_20_reg_inv_n_0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hF7)) 
    wm_8_20_inv_i_1
       (.I0(register_q_net),
        .I1(state_4_23[1]),
        .I2(state_4_23[0]),
        .O(wm_8_20_inv_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    wm_8_20_reg_inv
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(wm_8_20_inv_i_1_n_0),
        .Q(wm_8_20_reg_inv_n_0),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "sysgen_relational_f845914c88" *) 
module design_1_ip_scope_0_0_sysgen_relational_f845914c88
   (LOAD,
    signaldomain_clk,
    o,
    Q);
  output LOAD;
  input signaldomain_clk;
  input [10:0]o;
  input [10:0]Q;

  wire LOAD;
  wire [10:0]Q;
  wire [10:0]o;
  wire \op_mem_37_22[0]_i_2_n_0 ;
  wire \op_mem_37_22[0]_i_3_n_0 ;
  wire \op_mem_37_22[0]_i_4_n_0 ;
  wire \op_mem_37_22[0]_i_5_n_0 ;
  wire \op_mem_37_22_reg[0]_i_1_n_1 ;
  wire \op_mem_37_22_reg[0]_i_1_n_2 ;
  wire \op_mem_37_22_reg[0]_i_1_n_3 ;
  wire result_12_3_rel;
  wire signaldomain_clk;
  wire [3:0]\NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h9009)) 
    \op_mem_37_22[0]_i_2 
       (.I0(o[9]),
        .I1(Q[9]),
        .I2(o[10]),
        .I3(Q[10]),
        .O(\op_mem_37_22[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_3 
       (.I0(o[6]),
        .I1(Q[6]),
        .I2(Q[8]),
        .I3(o[8]),
        .I4(Q[7]),
        .I5(o[7]),
        .O(\op_mem_37_22[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_4 
       (.I0(o[3]),
        .I1(Q[3]),
        .I2(Q[5]),
        .I3(o[5]),
        .I4(Q[4]),
        .I5(o[4]),
        .O(\op_mem_37_22[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_5 
       (.I0(o[0]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(o[2]),
        .I4(Q[1]),
        .I5(o[1]),
        .O(\op_mem_37_22[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_37_22_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(result_12_3_rel),
        .Q(LOAD),
        .R(1'b0));
  CARRY4 \op_mem_37_22_reg[0]_i_1 
       (.CI(1'b0),
        .CO({result_12_3_rel,\op_mem_37_22_reg[0]_i_1_n_1 ,\op_mem_37_22_reg[0]_i_1_n_2 ,\op_mem_37_22_reg[0]_i_1_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED [3:0]),
        .S({\op_mem_37_22[0]_i_2_n_0 ,\op_mem_37_22[0]_i_3_n_0 ,\op_mem_37_22[0]_i_4_n_0 ,\op_mem_37_22[0]_i_5_n_0 }));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0
   (register_q_net,
    enable,
    signaldomain_clk);
  output register_q_net;
  input [0:0]enable;
  input signaldomain_clk;

  wire [0:0]enable;
  wire register_q_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(enable),
        .Q(register_q_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input d3_net;
  input signaldomain_clk;

  wire d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d3_net),
        .Q(q),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45
   (d3_net,
    d2_net,
    signaldomain_clk);
  output d3_net;
  input d2_net;
  input signaldomain_clk;

  wire d2_net;
  wire d3_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d2_net),
        .Q(d3_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46
   (d2_net,
    d1_net,
    signaldomain_clk);
  output d2_net;
  input d1_net;
  input signaldomain_clk;

  wire d1_net;
  wire d2_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d1_net),
        .Q(d2_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47
   (d1_net,
    r5_enable,
    axibusdomain_clk);
  output d1_net;
  input [0:0]r5_enable;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire d1_net;
  wire [0:0]r5_enable;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_enable),
        .Q(d1_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1
   (S,
    o,
    \fd_prim_array[15].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[15].bit_is_0.fdre_comp_1 ,
    rel_39_16_carry__0,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [13:0]o;
  output [3:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_1 ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]S;
  wire [3:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_1 ;
  wire [15:0]i;
  wire [13:0]o;
  wire [15:14]register5_q_net;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(register5_q_net[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(register5_q_net[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry__0_i_1
       (.I0(register5_q_net[15]),
        .I1(rel_39_16_carry__0[15]),
        .I2(rel_39_16_carry__0[14]),
        .I3(register5_q_net[14]),
        .O(\fd_prim_array[15].bit_is_0.fdre_comp_1 ));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry__0_i_5
       (.I0(register5_q_net[15]),
        .I1(rel_39_16_carry__0[15]),
        .I2(register5_q_net[14]),
        .I3(rel_39_16_carry__0[14]),
        .O(\fd_prim_array[15].bit_is_0.fdre_comp_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry__0_i_6
       (.I0(o[13]),
        .I1(rel_39_16_carry__0[13]),
        .I2(o[12]),
        .I3(rel_39_16_carry__0[12]),
        .O(\fd_prim_array[15].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry__0_i_7
       (.I0(o[11]),
        .I1(rel_39_16_carry__0[11]),
        .I2(o[10]),
        .I3(rel_39_16_carry__0[10]),
        .O(\fd_prim_array[15].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry__0_i_8
       (.I0(o[9]),
        .I1(rel_39_16_carry__0[9]),
        .I2(o[8]),
        .I3(rel_39_16_carry__0[8]),
        .O(\fd_prim_array[15].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry_i_5
       (.I0(o[7]),
        .I1(rel_39_16_carry__0[7]),
        .I2(o[6]),
        .I3(rel_39_16_carry__0[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry_i_6
       (.I0(o[5]),
        .I1(rel_39_16_carry__0[5]),
        .I2(o[4]),
        .I3(rel_39_16_carry__0[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry_i_7
       (.I0(o[3]),
        .I1(rel_39_16_carry__0[3]),
        .I2(o[2]),
        .I3(rel_39_16_carry__0[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry_i_8
       (.I0(o[1]),
        .I1(rel_39_16_carry__0[1]),
        .I2(o[0]),
        .I3(rel_39_16_carry__0[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17
   (o,
    r4_threshold,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r4_threshold;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r4_threshold;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5
   (o,
    ch2,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch2;
  input signaldomain_clk;

  wire [15:0]ch2;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7
   (o,
    ch1,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch1;
  input signaldomain_clk;

  wire [15:0]ch1;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9
   (DI,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp_0 ,
    rel_39_16_carry__0,
    ch_trigger,
    signaldomain_clk);
  output [3:0]DI;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp_0 ;
  input [13:0]rel_39_16_carry__0;
  input [15:0]ch_trigger;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [15:0]ch_trigger;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp_0 ;
  wire [15:0]o;
  wire [13:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[9]),
        .Q(o[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry__0_i_2
       (.I0(o[13]),
        .I1(rel_39_16_carry__0[13]),
        .I2(o[12]),
        .I3(rel_39_16_carry__0[12]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry__0_i_3
       (.I0(o[11]),
        .I1(rel_39_16_carry__0[11]),
        .I2(o[10]),
        .I3(rel_39_16_carry__0[10]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry__0_i_4
       (.I0(o[9]),
        .I1(rel_39_16_carry__0[9]),
        .I2(o[8]),
        .I3(rel_39_16_carry__0[8]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry_i_1
       (.I0(o[7]),
        .I1(rel_39_16_carry__0[7]),
        .I2(o[6]),
        .I3(rel_39_16_carry__0[6]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry_i_2
       (.I0(o[5]),
        .I1(rel_39_16_carry__0[5]),
        .I2(o[4]),
        .I3(rel_39_16_carry__0[4]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry_i_3
       (.I0(o[3]),
        .I1(rel_39_16_carry__0[3]),
        .I2(o[2]),
        .I3(rel_39_16_carry__0[2]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry_i_4
       (.I0(o[1]),
        .I1(rel_39_16_carry__0[1]),
        .I2(o[0]),
        .I3(rel_39_16_carry__0[0]),
        .O(DI[0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2
   (register4_q_net,
    clear,
    signaldomain_clk);
  output [0:0]register4_q_net;
  input [0:0]clear;
  input signaldomain_clk;

  wire [0:0]clear;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(clear),
        .Q(register4_q_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22
   (r8_full,
    d3_net,
    axibusdomain_clk);
  output [0:0]r8_full;
  input [0:0]d3_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d3_net;
  wire [0:0]r8_full;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(d3_net),
        .Q(r8_full),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23
   (d3_net,
    d2_net,
    axibusdomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d2_net;
  wire [0:0]d3_net;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(d2_net),
        .Q(d3_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24
   (d2_net,
    d1_net,
    axibusdomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(d1_net),
        .Q(d2_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25
   (d1_net,
    full,
    signaldomain_clk);
  output [0:0]d1_net;
  input [0:0]full;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]full;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(full),
        .Q(d1_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]d3_net;
  input signaldomain_clk;

  wire [0:0]d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d3_net),
        .Q(q),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31
   (d3_net,
    d2_net,
    signaldomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input signaldomain_clk;

  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d2_net),
        .Q(d3_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32
   (d2_net,
    d1_net,
    signaldomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d1_net),
        .Q(d2_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33
   (d1_net,
    r7_clear,
    axibusdomain_clk);
  output [0:0]d1_net;
  input [0:0]r7_clear;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]r7_clear;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_clear),
        .Q(d1_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39
   (o,
    r6_delay,
    axibusdomain_clk);
  output [10:0]o;
  input [10:0]r6_delay;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [10:0]o;
  wire [10:0]r6_delay;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_srlc33e" *) 
module design_1_ip_scope_0_0_xil_defaultlib_srlc33e
   (full,
    \reg_array[0].fde_used.u2_0 ,
    signaldomain_clk);
  output [0:0]full;
  input [0:0]\reg_array[0].fde_used.u2_0 ;
  input signaldomain_clk;

  wire [0:0]full;
  wire [0:0]\reg_array[0].fde_used.u2_0 ;
  wire signaldomain_clk;

  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[0].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\reg_array[0].fde_used.u2_0 ),
        .Q(full),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg
   (full,
    \reg_array[0].fde_used.u2 ,
    signaldomain_clk);
  output [0:0]full;
  input [0:0]\reg_array[0].fde_used.u2 ;
  input signaldomain_clk;

  wire [0:0]full;
  wire [0:0]\reg_array[0].fde_used.u2 ;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_srlc33e \partial_one.last_srlc33e 
       (.full(full),
        .\reg_array[0].fde_used.u2_0 (\reg_array[0].fde_used.u2 ),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0
   (register_q_net,
    enable,
    signaldomain_clk);
  output register_q_net;
  input [0:0]enable;
  input signaldomain_clk;

  wire [0:0]enable;
  wire register_q_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0 \latency_gt_0.fd_array[1].reg_comp 
       (.enable(enable),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40
   (d1_net,
    r5_enable,
    axibusdomain_clk);
  output d1_net;
  input [0:0]r5_enable;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire d1_net;
  wire [0:0]r5_enable;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r5_enable(r5_enable));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41
   (d2_net,
    d1_net,
    signaldomain_clk);
  output d2_net;
  input d1_net;
  input signaldomain_clk;

  wire d1_net;
  wire d2_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46 \latency_gt_0.fd_array[1].reg_comp 
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42
   (d3_net,
    d2_net,
    signaldomain_clk);
  output d3_net;
  input d2_net;
  input signaldomain_clk;

  wire d2_net;
  wire d3_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45 \latency_gt_0.fd_array[1].reg_comp 
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input d3_net;
  input signaldomain_clk;

  wire d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44 \latency_gt_0.fd_array[1].reg_comp 
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1
   (S,
    o,
    \fd_prim_array[15].bit_is_0.fdre_comp ,
    \fd_prim_array[15].bit_is_0.fdre_comp_0 ,
    rel_39_16_carry__0,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [13:0]o;
  output [3:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]S;
  wire [3:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire [13:0]o;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1 \latency_gt_0.fd_array[1].reg_comp 
       (.S(S),
        .\fd_prim_array[15].bit_is_0.fdre_comp_0 (\fd_prim_array[15].bit_is_0.fdre_comp ),
        .\fd_prim_array[15].bit_is_0.fdre_comp_1 (\fd_prim_array[15].bit_is_0.fdre_comp_0 ),
        .i(i),
        .o(o),
        .rel_39_16_carry__0(rel_39_16_carry__0),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10
   (o,
    r4_threshold,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r4_threshold;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r4_threshold;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r4_threshold(r4_threshold));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4
   (o,
    ch2,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch2;
  input signaldomain_clk;

  wire [15:0]ch2;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5 \latency_gt_0.fd_array[1].reg_comp 
       (.ch2(ch2),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6
   (o,
    ch1,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch1;
  input signaldomain_clk;

  wire [15:0]ch1;
  wire [15:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7 \latency_gt_0.fd_array[1].reg_comp 
       (.ch1(ch1),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8
   (DI,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp ,
    rel_39_16_carry__0,
    ch_trigger,
    signaldomain_clk);
  output [3:0]DI;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  input [13:0]rel_39_16_carry__0;
  input [15:0]ch_trigger;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [15:0]ch_trigger;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  wire [15:0]o;
  wire [13:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9 \latency_gt_0.fd_array[1].reg_comp 
       (.DI(DI),
        .ch_trigger(ch_trigger),
        .\fd_prim_array[13].bit_is_0.fdre_comp_0 (\fd_prim_array[13].bit_is_0.fdre_comp ),
        .o(o),
        .rel_39_16_carry__0(rel_39_16_carry__0),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2
   (register4_q_net,
    clear,
    signaldomain_clk);
  output [0:0]register4_q_net;
  input [0:0]clear;
  input signaldomain_clk;

  wire [0:0]clear;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2 \latency_gt_0.fd_array[1].reg_comp 
       (.clear(clear),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18
   (d1_net,
    full,
    signaldomain_clk);
  output [0:0]d1_net;
  input [0:0]full;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]full;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25 \latency_gt_0.fd_array[1].reg_comp 
       (.d1_net(d1_net),
        .full(full),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19
   (d2_net,
    d1_net,
    axibusdomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .d2_net(d2_net));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20
   (d3_net,
    d2_net,
    axibusdomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d2_net;
  wire [0:0]d3_net;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d2_net(d2_net),
        .d3_net(d3_net));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21
   (r8_full,
    d3_net,
    axibusdomain_clk);
  output [0:0]r8_full;
  input [0:0]d3_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d3_net;
  wire [0:0]r8_full;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d3_net(d3_net),
        .r8_full(r8_full));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26
   (d1_net,
    r7_clear,
    axibusdomain_clk);
  output [0:0]d1_net;
  input [0:0]r7_clear;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]r7_clear;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r7_clear(r7_clear));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27
   (d2_net,
    d1_net,
    signaldomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32 \latency_gt_0.fd_array[1].reg_comp 
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28
   (d3_net,
    d2_net,
    signaldomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input signaldomain_clk;

  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31 \latency_gt_0.fd_array[1].reg_comp 
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]d3_net;
  input signaldomain_clk;

  wire [0:0]d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30 \latency_gt_0.fd_array[1].reg_comp 
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3
   (o,
    r6_delay,
    axibusdomain_clk);
  output [10:0]o;
  input [10:0]r6_delay;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [10:0]o;
  wire [10:0]r6_delay;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r6_delay(r6_delay));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_generic_cstr" *) 
module design_1_ip_scope_0_0_blk_mem_gen_generic_cstr
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [31:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [31:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_scope_0_0_blk_mem_gen_prim_width \ramloop[0].ram.r 
       (.addra(addra),
        .clka(clka),
        .dina(dina[17:0]),
        .douta(douta[17:0]),
        .ena(ena),
        .wea(wea));
  design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0 \ramloop[1].ram.r 
       (.addra(addra),
        .clka(clka),
        .dina(dina[31:18]),
        .douta(douta[31:18]),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_generic_cstr" *) 
module design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [31:0]douta;
  output [31:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [31:0]dina;
  input [31:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1 \ramloop[0].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina[17:0]),
        .dinb(dinb[17:0]),
        .douta(douta[17:0]),
        .doutb(doutb[17:0]),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
  design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2 \ramloop[1].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina[31:18]),
        .dinb(dinb[31:18]),
        .douta(douta[31:18]),
        .doutb(doutb[31:18]),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module design_1_ip_scope_0_0_blk_mem_gen_prim_width
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [17:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [17:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [17:0]dina;
  wire [17:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init \prim_init.ram 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [13:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [13:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [13:0]dina;
  wire [13:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0 \prim_init.ram 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [17:0]douta;
  output [17:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [17:0]dina;
  input [17:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [17:0]dina;
  wire [17:0]dinb;
  wire [17:0]douta;
  wire [17:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [13:0]douta;
  output [13:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [13:0]dina;
  input [13:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [13:0]dina;
  wire [13:0]dinb;
  wire [13:0]douta;
  wire [13:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [17:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [17:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [17:0]dina;
  wire [17:0]douta;
  wire ena;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[16:9],dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,dina[17],dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:16],douta[16:9],douta[7:0]}),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:2],douta[17],douta[8]}),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [13:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [13:0]dina;
  input [0:0]wea;

  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71 ;
  wire [10:0]addra;
  wire clka;
  wire [13:0]dina;
  wire [13:0]douta;
  wire ena;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[13:7],1'b0,dina[6:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:16],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20 ,douta[13:7],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28 ,douta[6:0]}),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:2],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70 ,\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [17:0]douta;
  output [17:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [17:0]dina;
  input [17:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [17:0]dina;
  wire [17:0]dinb;
  wire [17:0]douta;
  wire [17:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,addrb,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[16:9],dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dinb[16:9],dinb[7:0]}),
        .DIPADIP({1'b0,1'b0,dina[17],dina[8]}),
        .DIPBDIP({1'b0,1'b0,dinb[17],dinb[8]}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:16],douta[16:9],douta[7:0]}),
        .DOBDO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:16],doutb[16:9],doutb[7:0]}),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:2],douta[17],douta[8]}),
        .DOPBDOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:2],doutb[17],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena),
        .ENBWREN(enb),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,web,web,web,web}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [13:0]douta;
  output [13:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [13:0]dina;
  input [13:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75 ;
  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [13:0]dina;
  wire [13:0]dinb;
  wire [13:0]douta;
  wire [13:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,addrb,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[13:7],1'b0,dina[6:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dinb[13:7],1'b0,dinb[6:0]}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:16],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20 ,douta[13:7],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28 ,douta[6:0]}),
        .DOBDO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:16],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52 ,doutb[13:7],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60 ,doutb[6:0]}),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:2],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70 ,\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71 }),
        .DOPBDOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:2],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74 ,\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75 }),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena),
        .ENBWREN(enb),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,web,web,web,web}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_top" *) 
module design_1_ip_scope_0_0_blk_mem_gen_top
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [31:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [31:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_scope_0_0_blk_mem_gen_generic_cstr \valid.cstr 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_top" *) 
module design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [31:0]douta;
  output [31:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [31:0]dina;
  input [31:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0 \valid.cstr 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* C_ADDRA_WIDTH = "11" *) (* C_ADDRB_WIDTH = "11" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "0" *) (* C_COUNT_18K_BRAM = "0" *) 
(* C_COUNT_36K_BRAM = "2" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     5.6824 mW" *) 
(* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "1" *) (* C_HAS_ENB = "0" *) 
(* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
(* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) (* C_HAS_REGCEA = "0" *) 
(* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) (* C_HAS_RSTB = "0" *) 
(* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) (* C_INITA_VAL = "0" *) 
(* C_INITB_VAL = "0" *) (* C_INIT_FILE = "ip_scope_blk_mem_gen_i1.mem" *) (* C_INIT_FILE_NAME = "ip_scope_blk_mem_gen_i1.mif" *) 
(* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "1" *) (* C_MEM_TYPE = "0" *) 
(* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) (* C_READ_DEPTH_A = "2048" *) 
(* C_READ_DEPTH_B = "2048" *) (* C_READ_LATENCY_A = "1" *) (* C_READ_LATENCY_B = "1" *) 
(* C_READ_WIDTH_A = "32" *) (* C_READ_WIDTH_B = "32" *) (* C_RSTRAM_A = "0" *) 
(* C_RSTRAM_B = "0" *) (* C_RST_PRIORITY_A = "CE" *) (* C_RST_PRIORITY_B = "CE" *) 
(* C_SIM_COLLISION_CHECK = "ALL" *) (* C_USE_BRAM_BLOCK = "0" *) (* C_USE_BYTE_WEA = "0" *) 
(* C_USE_BYTE_WEB = "0" *) (* C_USE_DEFAULT_DATA = "0" *) (* C_USE_ECC = "0" *) 
(* C_USE_SOFTECC = "0" *) (* C_USE_URAM = "0" *) (* C_WEA_WIDTH = "1" *) 
(* C_WEB_WIDTH = "1" *) (* C_WRITE_DEPTH_A = "2048" *) (* C_WRITE_DEPTH_B = "2048" *) 
(* C_WRITE_MODE_A = "READ_FIRST" *) (* C_WRITE_MODE_B = "WRITE_FIRST" *) (* C_WRITE_WIDTH_A = "32" *) 
(* C_WRITE_WIDTH_B = "32" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* ORIG_REF_NAME = "blk_mem_gen_v8_4_2" *) (* c_family = "artix7" *) 
module design_1_ip_scope_0_0_blk_mem_gen_v8_4_2
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [10:0]addra;
  input [31:0]dina;
  output [31:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [10:0]addrb;
  input [31:0]dinb;
  output [31:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [10:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [10:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;

  assign dbiterr = \<const0> ;
  assign doutb[31] = \<const0> ;
  assign doutb[30] = \<const0> ;
  assign doutb[29] = \<const0> ;
  assign doutb[28] = \<const0> ;
  assign doutb[27] = \<const0> ;
  assign doutb[26] = \<const0> ;
  assign doutb[25] = \<const0> ;
  assign doutb[24] = \<const0> ;
  assign doutb[23] = \<const0> ;
  assign doutb[22] = \<const0> ;
  assign doutb[21] = \<const0> ;
  assign doutb[20] = \<const0> ;
  assign doutb[19] = \<const0> ;
  assign doutb[18] = \<const0> ;
  assign doutb[17] = \<const0> ;
  assign doutb[16] = \<const0> ;
  assign doutb[15] = \<const0> ;
  assign doutb[14] = \<const0> ;
  assign doutb[13] = \<const0> ;
  assign doutb[12] = \<const0> ;
  assign doutb[11] = \<const0> ;
  assign doutb[10] = \<const0> ;
  assign doutb[9] = \<const0> ;
  assign doutb[8] = \<const0> ;
  assign doutb[7] = \<const0> ;
  assign doutb[6] = \<const0> ;
  assign doutb[5] = \<const0> ;
  assign doutb[4] = \<const0> ;
  assign doutb[3] = \<const0> ;
  assign doutb[2] = \<const0> ;
  assign doutb[1] = \<const0> ;
  assign doutb[0] = \<const0> ;
  assign rdaddrecc[10] = \<const0> ;
  assign rdaddrecc[9] = \<const0> ;
  assign rdaddrecc[8] = \<const0> ;
  assign rdaddrecc[7] = \<const0> ;
  assign rdaddrecc[6] = \<const0> ;
  assign rdaddrecc[5] = \<const0> ;
  assign rdaddrecc[4] = \<const0> ;
  assign rdaddrecc[3] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[10] = \<const0> ;
  assign s_axi_rdaddrecc[9] = \<const0> ;
  assign s_axi_rdaddrecc[8] = \<const0> ;
  assign s_axi_rdaddrecc[7] = \<const0> ;
  assign s_axi_rdaddrecc[6] = \<const0> ;
  assign s_axi_rdaddrecc[5] = \<const0> ;
  assign s_axi_rdaddrecc[4] = \<const0> ;
  assign s_axi_rdaddrecc[3] = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[31] = \<const0> ;
  assign s_axi_rdata[30] = \<const0> ;
  assign s_axi_rdata[29] = \<const0> ;
  assign s_axi_rdata[28] = \<const0> ;
  assign s_axi_rdata[27] = \<const0> ;
  assign s_axi_rdata[26] = \<const0> ;
  assign s_axi_rdata[25] = \<const0> ;
  assign s_axi_rdata[24] = \<const0> ;
  assign s_axi_rdata[23] = \<const0> ;
  assign s_axi_rdata[22] = \<const0> ;
  assign s_axi_rdata[21] = \<const0> ;
  assign s_axi_rdata[20] = \<const0> ;
  assign s_axi_rdata[19] = \<const0> ;
  assign s_axi_rdata[18] = \<const0> ;
  assign s_axi_rdata[17] = \<const0> ;
  assign s_axi_rdata[16] = \<const0> ;
  assign s_axi_rdata[15] = \<const0> ;
  assign s_axi_rdata[14] = \<const0> ;
  assign s_axi_rdata[13] = \<const0> ;
  assign s_axi_rdata[12] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth inst_blk_mem_gen
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* C_ADDRA_WIDTH = "11" *) (* C_ADDRB_WIDTH = "11" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "0" *) (* C_COUNT_18K_BRAM = "0" *) 
(* C_COUNT_36K_BRAM = "2" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     10.698 mW" *) 
(* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "1" *) (* C_HAS_ENB = "1" *) 
(* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
(* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) (* C_HAS_REGCEA = "0" *) 
(* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) (* C_HAS_RSTB = "0" *) 
(* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) (* C_INITA_VAL = "0" *) 
(* C_INITB_VAL = "0" *) (* C_INIT_FILE = "ip_scope_blk_mem_gen_i0.mem" *) (* C_INIT_FILE_NAME = "ip_scope_blk_mem_gen_i0.mif" *) 
(* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "1" *) (* C_MEM_TYPE = "2" *) 
(* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) (* C_READ_DEPTH_A = "2048" *) 
(* C_READ_DEPTH_B = "2048" *) (* C_READ_LATENCY_A = "1" *) (* C_READ_LATENCY_B = "1" *) 
(* C_READ_WIDTH_A = "32" *) (* C_READ_WIDTH_B = "32" *) (* C_RSTRAM_A = "0" *) 
(* C_RSTRAM_B = "0" *) (* C_RST_PRIORITY_A = "CE" *) (* C_RST_PRIORITY_B = "CE" *) 
(* C_SIM_COLLISION_CHECK = "ALL" *) (* C_USE_BRAM_BLOCK = "0" *) (* C_USE_BYTE_WEA = "0" *) 
(* C_USE_BYTE_WEB = "0" *) (* C_USE_DEFAULT_DATA = "0" *) (* C_USE_ECC = "0" *) 
(* C_USE_SOFTECC = "0" *) (* C_USE_URAM = "0" *) (* C_WEA_WIDTH = "1" *) 
(* C_WEB_WIDTH = "1" *) (* C_WRITE_DEPTH_A = "2048" *) (* C_WRITE_DEPTH_B = "2048" *) 
(* C_WRITE_MODE_A = "WRITE_FIRST" *) (* C_WRITE_MODE_B = "WRITE_FIRST" *) (* C_WRITE_WIDTH_A = "32" *) 
(* C_WRITE_WIDTH_B = "32" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* ORIG_REF_NAME = "blk_mem_gen_v8_4_2" *) (* c_family = "artix7" *) 
module design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [10:0]addra;
  input [31:0]dina;
  output [31:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [10:0]addrb;
  input [31:0]dinb;
  output [31:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [10:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [10:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  assign dbiterr = \<const0> ;
  assign rdaddrecc[10] = \<const0> ;
  assign rdaddrecc[9] = \<const0> ;
  assign rdaddrecc[8] = \<const0> ;
  assign rdaddrecc[7] = \<const0> ;
  assign rdaddrecc[6] = \<const0> ;
  assign rdaddrecc[5] = \<const0> ;
  assign rdaddrecc[4] = \<const0> ;
  assign rdaddrecc[3] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[10] = \<const0> ;
  assign s_axi_rdaddrecc[9] = \<const0> ;
  assign s_axi_rdaddrecc[8] = \<const0> ;
  assign s_axi_rdaddrecc[7] = \<const0> ;
  assign s_axi_rdaddrecc[6] = \<const0> ;
  assign s_axi_rdaddrecc[5] = \<const0> ;
  assign s_axi_rdaddrecc[4] = \<const0> ;
  assign s_axi_rdaddrecc[3] = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[31] = \<const0> ;
  assign s_axi_rdata[30] = \<const0> ;
  assign s_axi_rdata[29] = \<const0> ;
  assign s_axi_rdata[28] = \<const0> ;
  assign s_axi_rdata[27] = \<const0> ;
  assign s_axi_rdata[26] = \<const0> ;
  assign s_axi_rdata[25] = \<const0> ;
  assign s_axi_rdata[24] = \<const0> ;
  assign s_axi_rdata[23] = \<const0> ;
  assign s_axi_rdata[22] = \<const0> ;
  assign s_axi_rdata[21] = \<const0> ;
  assign s_axi_rdata[20] = \<const0> ;
  assign s_axi_rdata[19] = \<const0> ;
  assign s_axi_rdata[18] = \<const0> ;
  assign s_axi_rdata[17] = \<const0> ;
  assign s_axi_rdata[16] = \<const0> ;
  assign s_axi_rdata[15] = \<const0> ;
  assign s_axi_rdata[14] = \<const0> ;
  assign s_axi_rdata[13] = \<const0> ;
  assign s_axi_rdata[12] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth__parameterized0 inst_blk_mem_gen
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_4_2_synth" *) 
module design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [31:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [31:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;

  design_1_ip_scope_0_0_blk_mem_gen_top \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_4_2_synth" *) 
module design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth__parameterized0
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [31:0]douta;
  output [31:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [31:0]dina;
  input [31:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0 \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_LOAD = "1" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "1" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "0" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "0" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "11" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* ORIG_REF_NAME = "c_counter_binary_v12_0_12" *) 
module design_1_ip_scope_0_0_c_counter_binary_v12_0_12
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [10:0]L;
  output THRESH0;
  output [10:0]Q;

  wire \<const0> ;
  wire CE;
  wire CLK;
  wire [10:0]L;
  wire LOAD;
  wire [10:0]Q;
  wire SINIT;
  wire NLW_i_synth_THRESH0_UNCONNECTED;

  assign THRESH0 = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "11" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_ip_scope_0_0_c_counter_binary_v12_0_12_viv i_synth
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_i_synth_THRESH0_UNCONNECTED),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
INaBf8vh5mCmDzf2yp77pxZAxQdyEQiT/vG2dEgvrFjseUnGc6ldwH4JvdnpZSpdf/ihioPyMNjl
u6ooyzv5TA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S5XIZZtuFR/MZffuhwdnvE3H9oRWM4uXoaGZTa/Dyk62O+Wa0v41pjmZELCiR7uodZPFQfykZ6K9
2ZDMu8dB3afQRMs5lnd/53M1b9ke+MNEeZ/wzjUcsJghubnEAwzdWeW/0tlqST1WD9B/KCxYqwH5
Gj6IZTTFHAXcaVhnCT8=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CM6IcdzP0PbD6yMSqylmi4JE2qpmxiNeI+prjGwJiD8e3Xsynu3PbGKJAOpOxtR1hT/3mpBcx1Rz
Fkz0QBh4wtE8fiziv1i+xi8T6cqC8ClamjrpZ//sn6dh7NvwSYik14MlwVuei4DZoZJZF63aoPUn
RXkQ13wtK+MkYKBcPVSZMFZmaCU6jMMBYclXzvRG1JqqZoa7mWFTeFZePUTXG7Wo12QaZ8GUi0AV
UIshoN25yn5e2Xr3FyuEtm5AvsZb+iLsgLeHBtKBnsVaHQphicgqwgwv6MQQF6ZNBgU/aACfibDS
3+n/mMMm8k1cj2bW6VCi7a+c8LmCf81NlJuLww==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ehl0CusS7+JNGq6HfhyaBMy68nccIdIGqixoEztEZfkCpXuUYsdqw6G9MIJdWdu0Ck2acV7K6IVg
rzb8/bNaDDVWp48kupToegTkOdwDkCejEqppido4BkJ+iEkjPniz+aJHlOlOwmauETy2hCMuuC57
oWDprzGWlsqbCjqzKrXmPYm6fNdcOa2DiOYstQaMFNbPU2ccrbLJAiYMHNDqtPNqWxKBsD67kiGf
2eOneDOmdmy7YkNsL+cx8MJc3BVUsYBrpAEsGyFMkmX8a8nYz8R/wlFQFGQAd/t5XrfxFNI58mj1
AHXbcAMhGKVq9YdKeU/vSXY/NwMqp12xJ1nUaw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
h/qRAwiPuqY/Zg/QWqbaYm8xWTi9SshYuPzyL0UME9ZDDF+C2CyGAugh9HzMdD0kZmT94TKmBgLR
dKP28nlE8VCCU5rvbjKxfn/wNtNKHCvZ1hns8CF7+pGuelhxGvXNmYKFw5co8+4grYFaDXeoZZR6
S5sjvhqtSVD3+qq4vYWRjT2Y/yes7L9dRsLq2D3iZ4xjgVHuIbOQLT/EUKW+9iYudT9Uy6YTwB+5
mSb0QK3YfZdGwZyXB4S3mdF9vNQHdW/rnACq3yngF+lprNkh3ooQKdGqtxtz8KSQxNZOAFE+koOw
h00o7AKpvDAp3uNguLvnNJH3rugOhh95b8Jatw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
TsA04vIYHDZne2CBj5bWCBFH4MtNoFDCn/3DNEi0BwutuUf+X+lD9kAO3kl352WHjQbF79Ssm+PT
fCYpODgWdxSVbzaHFpITxCQ4HcIJhUeW5PC5tw09Tand68D6eg84qRguH+llbb5jdGJkJeTCf+Mx
pupkkLiDvNyTYWe+nqw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rx9hgQkvaJJTJJcTjGFW1DrrWiT+xanrcMvFn0Z3KRXlZvf+SE7IQgGCiP7ZDA6T5z1Zv5nzS4h5
cVi+CvwC9UMZRWmLDAjzASJ2nx1g9BjbYe2vHAUmyurIiR6LSigTeM/9TlMv+fFwJbqwuH6FJ3/z
Vl4tIMk4NrqkMn/riOG87SjhesepM6kcQOBgDGzLTG14z3qeZG8OPzxgApfyubmX4qdD1oTgGm2u
Q4mQfFxEye6Jqkn4Rzjhifs/ieNYomHlK7R2/72QJj5j0WyYBIhvO+09izz299Z54ZP2ZXaRMfDT
lU4lQNqQU14PX9Yk9p7sy2PnK4vTwwF0CFIgSQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FWWT15Ka+zWLaFVRMVNHPHa2neSlir30qAy3d3/WNXd8KUJygOgfHSoIEe0/1yCw+uEqGbZVwA9C
5TsoVFewBg1/eIMq4OJfnIyitV+Nk3fxm2JyvTPCro7JqU3fIM2bLPnsNknaTtQJ0fWB23X80wFY
WaNkx0Ykkr4vqww7XOhbBulmKUW/jnFhB3+d+y7Q3Rb8wB1eAFi4hoACZePuyE7vxUUeA1gy7msB
+s/XD57kmoFHWpM0N6RZgH2XYSdk2jr6GNBoprAz/bJhsQ9V067BGCoPHWIzePLAPDGP3PHWT0Qh
Bdh1GZEZ+uRIAmQWhyXg9tx5ncPia5gL+szi6Q==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ZaZdRs8xPakPNLale/7OkWwYIZdbP3hmxQlLzTQ7oyk5ZlEi/HKZoMYTEbWaO6ETn7uV/o4mNtJ2
jdks1fiYaGTtLhPT6oyNqaza+9BplUTICCrYoYr2Y/TfTtEnkGJ9Pvwcx9K9JTwTZG8DXMT+lgK/
kI7psxyq6G9d6dkdLtV0d38grTHxg/E4DuznRl4J8gia95TwUUvLkOR2S0wbrbkO2TpOTsA7fAwE
g0LHgEacbcppTxTyzO61C6dUkA4hOalD/XZ0wYtqduyn1fFZtpG7za/85HAEIIiuHgt5GYJV6wLX
PSKgDhqLgqGmdqXglBoXwv36pf9JVoMQ8Q69Ag==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12624)
`pragma protect data_block
Kxy4TRQj2+FHjZYbxrsXmNxZM5tRHPT5Zb9TFQn4Tn8rptLaC53d3BcWf3KZ6yJ/VcyKgWu6G+dQ
txp7pRz4ceh4d49YXwbmmSiaDHrmfsogrmOeHNB8tAfESufHcLVot1bb2pVtmnvobxTpLJx3a23c
ysyMyWIYSxK/Bg/W7IckafR4fQ1QrKFTDASJeCpbVnFmFPOT2HR6enR1WN4tCHoeTFJqHO7ZePXg
OtHgtNppKGvCdwdjQo8mKs8n5B1tIiVXlUXAVqnnbpSVaSRlVsGOyv/BWjyFVH7a2aXkCn+MEVsP
GhjlUx9TXG4iW04uEptZEg/5aWp2d2HHjVP47JWVU1pzgPO9EWI7XrX3tdWLCKMIyVoBxAK8+QCB
SZRFdTJmiFTuAKmX8Uf4qpHmj84xbaoVPIK8mdy1pXy786evOL+1JGCIQMLtSmtDT7QPWXNpjvQ4
I/QePcapV2txneo6QhNImrvaaCeEbFA4BahoD+FkUi9b/obsXzrwXD3X80Ss/2L5ARQqG/Dta4jv
mYTE+ooZzKXSznT89UNXbh1M4TPLKcjkF8VMhLe3RWySmAA6ILmJf+4/AYY++ktGXC8P+U1W04Pm
EIBjDFUyMPbtJ1+nP6pjzMh/td7QBJJBzljrUx69MgMjkXINhY/l2F7/IvJVzKenOFiyiQwzMFva
haNwJpajHfm/n2o4M01k058Mi8VyKxVQnuBUASdBi3bJa2gWuTI5H7TV4yctf9FKuXtsn0V+jYW9
VFJ4DI2S/1hWgOhfBSxTWEx3ySq2o3diZJI3hWtZY+6XmgBzsS7nht1ig46EJiIMCfxhH8JA+Gph
2IAYQcARj9jIrIqrfMi7QokPjFmd4Qw7tcGZQiok89yjlfWqEN0lWuwr+m9kQLCpAT5o/qEoITf3
z3sRxntE/LDqpWcgG7rjQ9dt3Kk0hxGg1p5cgE/IrIrq0InPgT7puQTlTdqtVhmUhZYaAK3qortk
MuBlJ0JqnnoK0578nAkMyTnMe5N8mPLpoj2TNyXUascYTqRmAv6dI87GuKEKQGeSeMt1xnQWCpx6
5Rf4De6wak+jxzeuULtf+UQgGrBuxypkVZ2kbaEIkK6F45UEMZI1By3LV4BIbi8EUSjMac/la0PQ
cPEBMRht6x/wxuLJZH0N23t9NoX4qp+chEj3SzQKp/HSyZMB8gkhAMFE6A4v6o1KdGxLgraQLeqn
LK6XqtPAGCHsESnCMamChBCjn+pTpErZKvZbb4iQ/CXqL0MfZ3UqxFnQffnV8cA7ip1QVsSsl6Bx
+gjt2bPMdmkPmNRoNs8KXYWvWv7eQ6KLrHRHUzJRQpcuYzRzlGhWeFPwSI1K9pEW5QBJkhV2Ny7U
J37RJVOqNQR5YdOqtx0SpqW9ADAZ9/0Qs1AM8ECbP28CFCwdewaR849HYASaHR1UWIFA/jPLPlaB
xoeeH9b7bV9HOBjYp3/DvAAKSEs6epPsI8sRexe6BVOHf820AV6CyPpTZ8Ijc7qwqNbcveUm10GH
sdJ516ghEZQ3ig+CD/Ddid3WHROmCj/0x8JOgPGXB6AhKtkhJXv5UmPcCD0OhZtlipCIXXIQXn9/
K9Hoy3HMtCMo7MLov+lWpjebiqT9DBceAQ3KYwPJ2gWB14tj5VmGxHCFPtUu4vLoV9iKtLmY0VUk
HNzwSNVfvv28/6L2la8Sg+D5taGiIyRtg0EZjBKO8JUsTaqHMvbvv/BgI4UvHjg6MLoIpn+6bQwU
gDaUlWrWNDgoL+rmXUGXLS2Z6kAl64G3qWPJqfqgtIpbVbAbez0zGPwewzwCmwGqOrg0qG+6LMKR
1fyGfHLwKDgNLeqWbiIdfISCSXhClIbUKWDhti4vSpVL/YWsd57DHHJ4x3NZ0mrPEQnOQfumlNxb
0n6ndG3trR3e5cRablfHFnpCZqhmHWuAU1Ho9d/OucFcCeeP1GUUz0FvtNXYyHxEynp4b9dnG5KJ
EoAxongFdlHRgoMqrbezdHkISI3sUXcofVWLvfIGSBVorLbNk2E//BdNkGP1AXY03vsew3lGNpAQ
M8EMbHsgs/5mF2jOqMdR/24ifHpM10HAYr5HdC0RbXMnWWovZ6sIDKUxLj1dPOAVIxjQrdAp4p9n
m8PCiFNMEvFRrxKfptSc9PZqOFf1CcCAR/XrH1M7KxcCWvAcwhsRp1ZaCbWTncuxWYYS4ir0NzxZ
UwqjfFcI9YM/jkzs/1Q1hWTG+Ma+uatpHOu6VEIOqVwRInMRCe5+9u5sOQs8949i3Ne2vEOPnngS
HGtx/Pgu+f6wQ2SyDxkqH4RnhtRKBOwDjSKTKXtf4+2EygFbtZo5DP7wzisSQVJ4Z2SXj5fT3++U
xPP2SxWDewfNbVsMscI+twdDvxb5bvwNya/WnDLkPjzt/+YCtVd+srpujxVUaiT4a0mhj1V779FG
LQggQqR16CpJlfYamRy/TxAEZiS0RbEU+H2ieEnGHNEMJgL2fWpyBz5tgq2g7ksfeOmTC0hEMQb2
PctzLMeUNe49kFhR0cBuUkMIsXl5A/bxOyF44ER0RP5zxISZhIg0MGjj0BJQX1jToJR4fmRN4KWQ
ODyni7AcbEXr2je1aK+AHhu+Y6V0WkZbFD0/lK80xD5fzIcdueY9z8ej5/4D4gBCtD2f0nv2+Szt
T+XaYm6HE+bHnsygu5hldBoK3mfR++w+KewWmHkb0GR0W7f1AwRK2RrTWdckEqu4Y0xFTzDhJgkz
VMhCPI+AdH5McwMRBUxZs+96ja3qa2HdWtW2HbE8gVl3wu2tKqLR31rjHtn/5PgAJ6bC1lc8ETKZ
uH54IHLgCXp9KwW7Jim7ZQV+40H6golvMG4QP5GCXf6mux+WGof9+FOEmeU920p1h+PS6HCpGB6G
7sk3WS9X8TP2yZ9/mwbBodDKHZuLc+HtRUElJB9MnBe2jeSCjyORZd56L4C/ly5w+sI+wAyR7slh
S72S7MXC0xfxAlSP9ZV8kZcSR58Cc90XgV//iBifqS1uZOyB+VxWDml6R5cfuQajLr1NSYPkWx16
/LK2NU7mUki4WukjPSf9OQ3dxJxOdNrhrkvZf4T4E/6/dgN39xAN2aLwdT1Z3M7bHOF0TeXM06h9
qEhcQ9UQ0UhioK+aPNDghUububyDN1yh7445bR0sfLVmnZ08V/CnVKbub2aCrBG9NKd4HNxzlJQI
WJ58liNjxLkA9AHtzs9zdLtNFgddflKnmhsaGg5eH4o1dnLxEe613jEljTC7YVzgEe9cgStys5cq
IIiAy0be654gdVfq7H9EsuBqZcjPrK4wYpiFI+av7cgTC+5yQlDc0Pb9uvZrlj0yZCW756g99766
2+fYCWaZOrIL3Fwyb76kXxdR6d4ZTddDBNbC6t447Kmzj7MQYhoApNZJu4VkrJ8AgiRq4g1rHR2U
TXP14xzlAtKlGmaghTQhQbQqzZFYf0f0iDw4waLAxHODSbt0MCIS9LvcI8/VNmQwwpnwH13AZpWW
DkC8exz7zsX/KpnVBBSaU0dulz6zMWytmCbQnNBFVyiiXMFnENNZddNOfGbELh7h7vIRnI6KqUVZ
Xc89jJ0k8kSUxX9yKYpRgShlc85g1P5lPFA173AGHi5Qd9ezgfgtyButD/q2FK514MK1e0/Jrqf2
8x+w63if1CmoY9n55rGMRIVOkKhfVAKkyw96QztfF28vmtcOateX1ymgd0vvi3m7oooUB1u9gLED
jMMaALf7ZtLekHsTUklYWGwqEG0AXQNIov8sDF3woqPyEF/8+Ds+r8JIHpyO3DSWMHuzCO8Gddk7
MdwE2zZBvn/iaxvkv6hbzH+ehM32Ey3qtL9PhysYXh0h9p+qNDkjlY3PGto8Vnrt/hBZaqJRa5Ka
CVmrtZseE9kP2NSvWNTo7c7xYw88bf5dOH/ktPm0vGyBaKhGeTP2QkLOk2i2H5kUzsNGAEwyoMvR
TjPxgiZ+gQjo7JfvatzacoRFsEFG3m7+Y3qgev+1LXBpqD7b7CdfAbd3bc3JSTex8KXUSh6nv9BA
1+XxmNkg5J7b2nZfNtxQFHwUQJavDwa147dwJcxQlB8D405PWPdHDZlwOmZ2n2P17yY7L4ZM8PEl
nShS3pCrGw1NAXS0m3X1+PAjUsAg3FmyDcRgX+7KJbr+ufongS0F93scUZnosEb7ZRVV33+W6X9m
a0LLXJOa1sBwpRCs2qEAQk8LaZ4pebrlsQ1xrYjTuj1ir0gVDO4UK/+mNoARw+bQa5xoz9guZ75X
fx/D6MuXEvL7amIwEeI2nrT4HtXrGq7jYK7o8kbMv+DRSZ78MdJ5GqJNvwLWlBMDc6qjGLZ3Mnaf
CcUXgD+7CceH83UitvXP2C0Ii8Cg+ESchJczXfxQ4OJgXSuyKFnbk3wAPvrxVOS4WdEiZbFREG7N
3AAqt9wqHQ8H7wnKX8z+uUHKh0nibsKiNjX8avbAfyu49BG22bQtCDbpVou3c+pnjz9gzvRAgPfV
rO2qld8am94hb+go8nltnby4rXWAbHfzxp3noelFxF/kPyB1few1fumSznoqHiB66/863ocOtMeC
bIE/ddjPHpT3OgWsTxPpb19WGqXxDIZ1zMubAXeAOUW4otkwpXyFh395WKCEJLjdICgVG/v4qM9Y
6S6V0js78Q1RUYfieDrz5ZSwGKvZ2Cv3b1+o35rZEZyv+kkI9a9zLtaLQjz/NBIQEWdoRCHgggQ2
7CS+ys7yBCioeHuVcEwN422nzYVlwERX04LQrvQvltUxnUxlV9Mx+sgqW4Tdm8n6oRIm0iW3y4Sk
/6yHGAXynfOY0x4ojzPJQ5px3CrlBKuXlThZZ8Hu3KQ0QeziTVF8ZgjGEntwzvm9upIejEO4gXD1
X98etPzhydiUkJXe23wNM6CqK8ZSNjDPVyEXgv06nfjBEoZ3tL/4SVgO6PiprjtaErTi1FPEsbkg
0VLs3L12N6KU8aMpo+INbuJx8diPSc42x+nZk0Ed/qZl53+DZ9UFohVeKGvhHB2vq9hUurtKJQcq
IDilhChDEfhAJchFsxS0BVzG2nzfdVcm5/W0Yuohip2KYB8Lvn/w3Q43PaJh09Q+75KybtJGUH9j
Ql/AOsz87N7v/zAnvGJ4kjpPJG878t1esXsplpUdlTI9aWilYhn0IQgUmljDQni9FR7lSXaN+PGk
bhcNNMV2D0FYH53EK8yX3ROCMHBP9Qzq2RhwUtnyPgs22v1o7f8Xb5fSN2P3D5t2GL36bfByGUEI
1CAPgpG/KtJCVS4bPLDdPypJOGjJbPCEVubidYGCaiU4b4zBkLD8+1qlgpQjXigYbmZub6AzSGnH
KtDBKplysHYQ0Svax4Kicr3vclqwE3m12nMzorUUmppQDidAF++9Yjd6Qowk/TJ01++QQLHmk0kn
cGEAmpf/W1X2b0MyZhEbu9p5GIagrIF/B4ELiMZzT2FvPCEw90ehUGRVu1YXXBjcBnRJBy+o7QZV
zKdvMuqdR4kI3E+Gl8UA0XYCydZ8vhiU7s0ECdzFkUI1F5YrnBoC1k1ZY054/McWldD9if4418jP
vLlbydJWgLsTj4FJj0HOPRDGA0Rho70Z6U/CBohJGUO7Tzqn33dLo6qpfU6toLctR6ftYDU0VEIM
SPQE0FdgVNKOkNrPb7xz60ktvvh1MsuXIUvZei3sOsQNeyRjdbr52iiuTFaZHQxoa803bWGN8tKE
fzHIlHjh9swhqJZYzf384eHns00R/SNXLRJTyJsCZ9w65HDezvlhSUD9uRKwaOYNPmew3lmYTtU9
DCzB4l4ae+6k1h5Gj2mnRlwhh4zxGrDF2GWGmWXzgwAsrnKUFJUlbWxaMtQnv96O3RRJxGjuuMTa
CQC17xWH94hL02eN2yclk9JDbGxarcgq082HjtNSt2yHi6jYvHG5xPGvovREJ7+LND3D+0P1b5q9
bn9RQxyxa6vGXCNFXKgXIh4iaMIJRd98ST+njjCkaLnudx/aGq4ZeDq1vnVLecuPqohDXoYbmrMj
J1PZOxmK35+aI8XHHaKawgp0zU0xb4aj3zTWVc5c45cpJ4E9i6Ko1bWixNPpqGgn5H9Tdp7oHl+B
/Y/HfsLV1N2eWM/K1/MsFXx22La0Z/LT6Rg6eeYIqzlfgT97NaBiJnf2iezO8hwd1rYXt0AzVTU9
G6vwPKFQzUnGVD/yVq42G45T26THl2Am93v3eIl+/GPotl1/riQLVvS4liVBGfnjBpkqpDAtvwKw
0shmGMBc8d6aJeW1cvYnO/69r8b9tDj6sZek7nEZhQ8Xq28XG5e9OT+uwQZli8jnxWjTAgO8U1vU
vzSV79aQfvBWl/YKgHxGFp65a9bHPGNNicoxVcqkxJNdTCXe4U9EOe8a1kzyypkcFTPLzXkh34F5
+ncnZQUDHZ7/wwfnRLgayAvW3X4l4jAoHXV2G96Cyitv+QTqqJHoZG0W+bXk6bR2Sc1dkt2pBQE7
8zQhsWMdCq+5oRqKSEI212TxukqSCpE6csKZQy4P8k0LLgLTUTuM87EnV5WX4rX31ArspyMoGnl/
Nu9tNnOrlKVDdvpX54iHcGaUKXSvmuznEFkSzHiPEYpl11+mhbhxnjKfYWUfK1iHsOUmkBldHH0Q
JldnpjNgEx0J+8BQHECeoG67yv3S48ChyZE3Mz8846qLuHkVj7GeY/jP1l92JBR6QLc1eywIctAX
RYcXcbadEUFanzG8VgLidsl2r8mgTbCN1AXUer3TSVEnGZgjyAHLO5qmV3jhAWwKgtcP5TWaceBx
pukyPpnQyh3s0iO4cbYGOyhR+7VYWvxpI6By2Bn7+t6V8OB2YK8Lp0dDXaDPWNAvrEAUpJ0Z9M/5
4wI10mTb22Ql6FAzeUlbkmMCzj6dDAye6YYmQ7EznNODcaGpS6E6WPlXA42RtSaNK0p+xpXx6qeR
0SB8lGVS1ZHD7XS3kOpUKOxPv+SXcb46KQ7xHEl/EZbO6ojAmkJRy8c9umWCTSbhz/0VZHBxlUlC
k/ozj/3+7Y7WN6beyMaWVpxNhcYJaZ996A3XPSCEYiimUTXaR+5eNzYMuZ2Mm4gN+VxPjWC6CmZs
AY2B8a/3yi7uaCtGYtrD93xiIB659hHYhFv/EWd2+KXGZAbZOOmpFJ1uI0q9s4K8oBTQJ6xwdWRp
yxYmhtcKOR592xf1sVwaFlQQjOY14HifSPp6wWEdFqcIaYcKFV0Mp4poh43LBQc0TnnWDW+8+gEB
dT2wZ3Cg6h6I2VlmvjV6T/tneHBdrEdSWwUyXT0bdXHnY9nohvvXgCS3iBOE7EBcLV7D7FGctAiL
56UVAhedFoGIsF748YBNzIOmXEoGmEp9akHZbVmY6tci5Xsh/mdi+08kgC+LsvJDOA5xyguVYcCB
ixGFV6PpFk2MQDiA6sUJdQWCi3gDZcX3B8yiQJLwwfhOubrL2nbQ1gL28rrabPf3ZHjAE1SHT4lq
G+tvC4vcONa9X4a6XHSN6R7YGY//XxcfT/Lv/UOKMncau0sLbsz0tpl3nKWPUzcOGBnH+7Eor+Ts
N98OtZzJ7fY5vKT+Urnjmv5kiF2qBEUJzFzjjw7Rcoj3kJdKp+WEZafRyeB0Q3mgS2DzZgkrrcQx
KnEA81KZrL/31Mgm0AKANBFbq0kGrwd4tTUOuDAABZbznNqzeLJM4FMb0Qyfe8Xo0VGrbXyoyvwn
7z9TwjAgf5evT+/pdy3KYD8UKc6Puk85WtsTK9B47Qe7/e94c00DSFcx8aLwUUEWadwSTMBWSbPm
iaEoJ6fzREnIxH97OcTrctY+H2jddNRe6bzZq1ciH7qQO3Sm3MoX+JRBRlnIREFBKPvD/DvHEtfm
sJFj5xpiv3jQjwK1FSNKvBM0zlH7jZ9CqL7WxYYDD1tJZB4CTA7kFAOOa4YALd843H6hGnot0iBj
Q/rDogtVJexF/FzcVlh/ktLr1wBb/qJsHFrf0Lu3HtruMpJ3FYGBdhlp/AIj6tN65kHl5w8JD6RQ
QoiENVX+9c0yirg+oyf20E6+lm11VyV2vKLqXJW/cQCF3n17peAttO9sYBri69uvYAn/RxL1a3hu
wFdhTGvtavCsio0AdTvUVmpWCz7g69ec2jLgRxw1Kh9bREZwAdSZ0GXnZXN/0hq3GRMgu2Wz9v5D
TMLYg5Q5n8YbP4yqMLRfLpkUKAE9weEbVKdnIBRNpFZbze8GjQVwetRygPvahhSJfydFN+cf+qXX
vd4J1rP/oe/M+1S3ypP9x7BKZbTfsufqU0uH/n8iL9B6b3a/JKLxjKPnHB6nMhMen3RHFQXeiBi9
pg3sxIguG8P0haFhN0lc8h2gZgVRAn2ZIo02vzwvR0mWi/KDFqSuuFEiOK4q3vemYBFhnFNbmSLu
4QpBX+2IqEws7OHVsu37vALYI524pk9mf5W1nv8Azi+87ol9toHDHjIsGUm24dpwuBV7f8Davd6H
Ru2itzaRtnMD/HOeSqvo3/mW/wAcNwENZspYcsDtqBUY0AhyJOMzj1BdX5qhswe0L1VLsl4Q9oBi
dddLDtAyFRz9lOopHWKyd89oCatA04oac25lYwWixnegzAm3ShjKi+59dY4vdOqVSU4DEYiA3kD2
dfG3FAjv4ScB1sl5uP1r5jy4O+Ywodq8jFygXlUxFiYozdywIqKLShRjldFdxatlIrPt8In62k1S
6fqoqs9nMrCBgPmgV3iM1S2v3V0hrnz0sPbUOYhdXGzhW6EQN41rKqMCg36J50boFTD2wjZ0hX8L
r38NpN8M7jL2dvbWUfVhgh4MZUjd0tfC2HH3x8K6lqFEo2RBVYjoGAIHH1kv5Xf7zOTROllPHGKa
UWwdvXTXMaVtpLLkiO7CImCBVJo3vNQOHg3MMXSe6O8xODSbXQH6JLc8ahdz4RXWtRq7RT79iJQR
/1K11XrqR6vuT4NQeUTAQ8G5r2HToaFHsGvC300v2wUoPdcTg4qpHdKZBVwHlZoucIXje3cIu7p6
RVDez8z5KFMue6u8QmBRjQYC4iVxz/coO56g7tSssiVR8mbPgHBUPtgvfHKk2yd0f/EuCP1plXs9
iQ3ZosZevz9yN9TIInuyaVtk35raf/POHisJksYKagvVfusz6kyF69W1E29TCjd0BIKc6EI5/3/6
5QIoyLSI4amLWnlcGmD1FZKBX60/m2ZxO+g6XFORYu7MJmRZd40tLaRjOOjcs4PsjBkK5xRZm3Bt
OVxrRFuvoifq69QIj9G4Mcbqs0SwpcJeIYqrZoHUg9rYsFCMi4xhYFKnQvWCJtTGZYCm51G2dQXj
w8Y4g3jYyeQp6YeWX6Jxro7RXGSO/SuQp6gsElX8uiFCpq1vNnGRh5igK2xhOiEpyQb3VRImzVug
IoSwPcTYBsHXpTgNVpZirGKNfEq5h8NJORpm4q9yrudNTIkGS3rW3f6bmUY0AHJ3qm1Xp4sQwItO
QcbT5gu7O0jqQu9eBStcwr/DKlQT4cjIX5Sr/Lk1kuecGH/VdFybYiqum80WzR5kir5+uTwEA2tf
dJ1UhQpJR/96cbIWU/+1F5exg3NiJ9PrXo/E3k8Hx/Ozj9+CJqwmKNJN6v8BSYjfONdgHwB10aBZ
i8r6OT0zFcy1T5A66QBe8ktapxNWqTUR3rGKFomzCkGp+PWd0cLpwEvfQ48hSYMQJVBLytzieWBL
umhVS0HQfp0X20pPUSz6DlCJ7BUvoCVKtG9MVRIFLTZQjqYfHXHzJWjVBuFEBLFqLspobXGKJrXW
lOY96CSQuQ7+WP2oCrtEzsQJ4X96wcgo9XnpeHUCiQbc/lAc8hrC0fUoLaL82SqbM1SfwLU7sJvZ
SQdAGd2s8QBlYwXa8w+G7+fsECBsgO/Nr79wHN8ZBfpbHYddRP+gw7y4HqRUSSV6TA/KqtE5S1qA
orvWK9uWGCrLzPr7gk0eU6oh5FSpSPl109bCLltDeq6PvZzoJRHli4KrNPYat4hE6f3+lVSkKJ/c
Xn1f1IJansbRu5PEZLOnmSjQbCh5Ui95JguGvG+rIM7dn8jZt1k6EEctsXzDNOhqmZd++XLiAX/V
zvHVD/Q1oDjt+SmVTPG1Ja3JoYbNF0lAjWcKSeoR9tQHkDhZ6rpkDnLWgkfTjhe7SHpEFFh+I0Vs
DejMOXYzflNhf+N1sZ4UaYN3++oIQPZQeecVqRPt/4r4BdsekaBuHJ7ER9otaDqAiNyf0PIiQDKC
lwc0AO2R1gfvFxkB5yg6x+QCBBwRe/180Oo79Tgpk/dm3W/WObr3wQTVdF4aqOFCQgmudMrCfsOj
M9cpC74wr0eXgjbgcnW8dPxIKMcTK7NgjZa71KqxTsCRWQubR414Yn9KPZ7S07o7zNTQyyDLFSIN
xnRcVq24WaDtvMpSsVZtW4WrKqVJ08AZtqanjQO8Ojb5iJnbBzuDBVWrgL2BmyHG8kMWEW8n/0JX
ABXvsKXnygrEsUEr6PZm8VK+LqaCmVNp/wZWMK4LBH2sSzxbn/+X1j/kBdDwK5u1D3cnSJFNvEFK
uSFvBtGt9vrCTjKhoyWWelwo1+bRMSQGcHcUP64fi7WcA4J0u2aIBnaIshbAL3fmEJqA8n3u6Rf9
RDcrBs+OGGuPmCsE4ITD2WUR5+ady4akdj17YOKmIxSjLYqzBo16RF7kRuBKttlF2Sop6Xvojrn/
Tf6KzCYce1UDDYgvem+54E87kXiQA3moWxfWWIWWUJvBMVZ/I4z/wK3xZUXwDVQy04+SomiiJVmk
9Z/EfWtDXYosXnp68Z/wPF5g9jiR0nOV9qKUGXVfk5xSUfkmHwTRw8XPMlvGxTYbEvAbVuB6ol1t
bKU1Q8x34H7Ga2rQt3xTrukbf3aJsBEYL/AuWxBNq2MLOCo9v68ReMkhe+8Pg9YgWARSz0O7oM+s
Q7k6pylEVFVsuacOFI5l+tg0bC02P+Q0pqV7ULTAYGOXbzgXtjovhVbmUqpZ2b4GObIYHi97Hvt4
+TlWdAiCaQQJeNMfVkCC3/aVP7ZOpCXHR7LuIGDiLo1PiYfqBH2MP8QSaG1n9S86rByX2aLyHek3
LUW+8bxpdmRGI/yfFFM0MiFW8AjdxZ3mJzrSHcXh9cf5dDocb4PMwod0t1adAfLY+0PUT9sClsb5
B31ZbwhaV/l4sYBcITbVYUm9hrt1q/76ElFQILd7ZQ9sVHyFIZ1qTCsf3Y2+VGJqHS4ZptwZn2e5
LEh6zYTinVo+7P+QZADJPl5jUuI036lvGP8AdBp8z8bKWMnXt483P6cqkHynKRCi2rpdxF/jpO92
ViZsQqTrHhAEsCep4EyvTRNlIBIrk/Uuw5lQymKy0L8DNmU1Old5Va5wH6+OheV+amnjkTr+j4tK
LTtHvEYSCMwAUutoKc9joyyAKBI7b6ofDVyhx5MEwV9f7dB/c7L9JhD5kjwpOOSv9H/x9/+1vi+T
qDcgzW67gP64TAqNeZEaTfwyu47llmhcUsEjQHdIj/MPIXOkoJn6LEFGarXW8g3OkHK22Q1BoM3G
8ZaFUgNOoLQKOjEIm2slK0nhQ4gHqSwqL1qb7d9tptgsZfgwGpke5upo/tsaKD6sw8zGJhen0BTi
haMgW85NjieCP0vXOlDl7T5WM/NSGFPcn6eLAY5Yw1wbP2lEa73qiphX4qoqR4kC1e2ZnSD1P5db
g4Kx0GVH7z3hbb+z4KqW9IkHRjF2CqQt+97vrK/gWglkNXgRqdtWVviMSz9FDYRFoDPTVfthKgEY
3CykVhubMlyO49xNEufDI+a9vhjYHmB89Gt9HCrIQ10XO1euiX8pIVArVdqYuMV+N3pszAdJaFst
EtLyI/I/Q+61zeF7ZXt4hvQoAI/M27AC44uD4Uv4mdV/VNj1DF30BDTmN4B9eLi0xH4d8iW08SbM
/PE0wMGru9Z/RIBuouiIH5F/KyoTZGmrBlCAMFz/Fecij8KJFDbCHklQZpqhyVlPc4V2JYP2n4Mh
qjtdND1hNqIALoHUUMmd01VuHSRJN9CIx0XMk86eDbNPo8FTZccizqak6MPuIW0l7GOqPYOE8G0/
YxYUKTzqc8QJQ2xEuselReyvZmrXmALlBVjGoW6vAhPznmoSz8t1PlxU+ZE8VdT3c+C+qbUf3XjD
gimnaZK7OMTs4yjmBQz3/OXCFdcb0lmqbUc6avjL7E1v74z9I4hk1HSuqQSaYnxvaHK/phLreLG1
V+PGo3klA3q/JOoTtJKmqX/vFytJyzyFM9T3D0ber8lz572vfSaj/gXFIUxwVMNlFc099HVkBSU5
LkBd9P4RfgmMl6mx1V5Nufn6nNU57UtqLac8GWJUKT1K6YIBFFixJx3XI3OJ9Jtr9XngitJmoDhk
IWPk4E/8VPmUCe/pKMetglhfmCWlRzsrBzmXkRtunuMqe0ZYMwBWDdRG8XRa4hLPbf+blbqqUgl6
BZ/ogT9lD6RIK8sG8MiPDyiOale5kwPvEJVN670XLrVfxdAFj7s/6GArj2OKc9oG3CdWAdNVfbrA
kcwipyJTrK6EMRHK6xK9JsIfh5rsVAu9guF7dbsxoCcJ+ghOteKQ/3c1GdzrH9BQG8t+prqPYAVx
PYm/igdCpEy3E9jPiHpv30EqvwT3+UODqcKjTZiVhNcrk32Riq6GEICMvXf6I7+neoYeAXaOMv9H
lpilW7+RqOeSBtEGXvvggouhJlhCm9mtXLK7KpIPwUQKFLtIk1C4+Mkh5bc6aXBwPk50dgcVQNrY
s8b2zU8C9nWvcH+Sr25jULv3qlVgaGCbaHB5kIHvQzAdn6ilf+y6sEWiZgUFH1qZVuL/tImo733i
YTm6PBJI/mWNuOewWt0e0+HLPe9+RSJJdIn/io9ZkSRYDPlxwu1OwlMauGgUlHyLcr/I1ElQZOGR
/za8Hk7h2LZQbcOGwD8KavQj2yCXF3NZ3sJMkFMxe5VlGbruydiGSzrCTQfi8rqtNu59XO8Jn1Ps
kR54aiaO7Hva1BfDhJn5YdS4NoeIH83n5m98BSEMFQbY5oizcDzHK4HR9DaHktSxQkyL0/Db664a
AUX3p9fKGWv/Ew/NXExZQi55WyI3g79vNQbai/NmQnrxSZWzxs09900jizKDPtncZGQIStW+rvxX
ou5AQJ7VZ2XBRqo1/2PcpiGYRERGl8AkMzzKXRu4e9VO9B9SqHc+g5ozpSpr8rnEpLm5o/ONEihZ
urM5AWm5ROPX5UqJWGLSMkE7l0R1efyj078eehX6CW+NYwt/niImQlY9xcUfo/Yeg3FIAXcALDCP
W4Uy2otxo76TGXgimUQ9lSERgCNuhrPeo5W4er61XrCcdaf/mDubMfZEbsAXZlqQCY/06gz8t6Yl
SVaSgWDEmkQN+pT2v4BNcbaiUkQffKvF3m5vJ6rr7Fl6WSNFuBuRXSKOG5u9+Hb9j6yc2M5T/7l8
afsDkl9dcLkFyhsaH9m3s3xzUC5oFN261H1Da2yjcdh1Nbxqs3ExKsiJMGaQGOvQJ++1vF+aOY5Z
8DiSt1dnRtIkUuDX/Q9IUojM8gI0IPfZg7kd9KEevoYBv97FDEP5886e2PK79GcNSDZk2i0V9h+4
D5ydeFb29QRF/2mr33PWR69egjiEbvY8cGzF+99FLEFcjBUzqmWu7bjTmToGx1sYKEO6KAcD2U7T
3WdE9fXaZRzJ7FjkAh6wniit3y/5Yxd8/iSBzQ58XmLPq5DPQe2pstpHZ3+QJ33lKpeKQ6QLhCp0
HSz+i/pWtWvKyIoD6iBKeF2U95NxOe6MSjTGBjZqW9+r+kVkaEKRvrnBCae969i1Mi11LYRRjeCX
z0xoVzWZ74buWCd0Em05ZhRXUiWAWMOUUf4eUZIeOOVVebbWF+/KHPuEvqi+hy0ETl+pEqxsjknc
HO4asFHZdnLNer9G3bcVbTKS7EEypn90YXmzOgHNQPbJbyfe1XZfBUAammwFNgjFIbJRO6CVPMR0
qzmBDX5UhOQ+H+DaQ9UE8/WseiK4ZJ0SkZ42+kvi7gYAbuFyLCukgrLUex7jENYbpAoRhZAaIGn0
ldhGtzUUPLAiCnJm9UZCRkqx1Ae5LYbNAXgtnFOYMT7qAkZFk0+wbQ+/oWbOcqrNCKAJd2AOKgr/
nFyQ6mlDcovhdozkv4g8B6IRdA2aL0F6+PHA/BhUxexv1s+n/lKyi+cXfq4XP2SNqH3wb6Rg42qa
wL9XMURXu/AMPTLvLKcLmg3TB0zRmsmtjr75epgWNTconQ47CZL4Ea1Axl849Z7VINKUQQo41Nhm
YdziCHOlRbCdK+8R/9s7604KEuZsObweFeqKuV0y6iXFlP+ZqWQY9xixgrQ0r7JqjKAEusFwd73D
bc88NHM6eYJMaHdtUkVMrrPc60uoBznIkpoLH3BQhNT4oAxk6AzyEHVATfVCD1Gt8GQIVQZ7/R7X
dncz7z05xeI6QM8audix5gnZVHc0o0V3m/CgfwBQOuTOa0YPYj3IFm/LZBdXOzHURyFgUrBmcFZh
KF2D06zs+YtGNjbpzEVQ7JADvX/Fzld9AwV4tAHuQMhCztZf9veETQoADyoiClBuXpcCF08fqVpK
MOMnPxZjiNh9ZlzYj4CVEqVFEPKVexyfA8oggxHhnlO44Up5FK025VfbEdT2oANKpH0Bk+R5IPdZ
vQ2Ionxi74an2EwOjOXCcSgMUBRnZQ4Jo7qET76whDCUwZPnqlNg3YslvXjDOs9/MKusE7eeJObF
2OF4btQPVSehf6YTWgRv6e5diiYYy6nbVOs4q3vtqhzT05enV1npIEgVy7VKv7iHbeDX07Zxaq+C
xMNg/t70NQ7tDe0z49+C8lY4QcTGxFtVZCS3kZCGibftE3q+ITWokjgMvNfB+W22UI1YtFMiam1g
u3xLODYn7kLXUrTWpBhm99Bth031sriTAMMEtGkhO6BpsMWJ4i6VtqWPQFKwnFINA27D78uxGBYh
+jbXEItSiipxKFtL+qPk8IFyPM0k2xAs1TeNYMNhOQ0xxJwC//xsVoW2h8SaVEPEEq2CbwkDPCWX
FfgthrMwoDx0f3Ozm0BSPg4Z1XLXl38kkmyymAMnN+Rbahp9H8NSXUI5kHbliJ/zw4ZK+f5z25kq
VbeHyn161DhxpbgqcK1MpekV9JYYEKN+5n3QNYQdTwpjZpL7n7PH2xicjPMbfWXGwTv+bmzYCvo1
PaocbBf4wpHmjGLzpr0ii0XZOF/cEgu+8+ZWi7kFLVgYM6/2nPigLAd0u+r59ezYbFsHcB42FLHi
+BmhoMJqAiJ8P8Rh4remC5dJOTfJxF+tGY4cLK6Xfx2A7r8/yBzfj0lNBqy1TBJA07iHpRTSJrN0
VhlLjZIXAyhgKaIROE6JZMU327SC7/te0ghUjVAoHruTFlo29e+Tq7FZwagmVUSn3e4jPhHTHuaP
gN+5r5pX6nhWFYostpIjR2GWv0yJT9C81yitUyMp1dC6igX2BNuhJya9DCk+5B3VyY+5MOg2oXev
2dA+x2PC2ds8bVqUZ29oHnEoWLnySJM2XNJyal40JR6G4Lj0pkzZLVaKjuq4CLEWFWcyc72+22Rf
lvF9wJ5P1T3b+LSIfgK76WcNeKgka33SgkRmGrEHPHEhg1rer9RZAmaJUD1G1OTMR9yLMXbjEIEZ
YjjfTPfB7/ShOYnQD7U0dKI5m6JdC0OWYHYbug2X9HBbtJ8y1WfTLm9cnwAXMf18ErsRjX1PPByV
tg339ogldRiIM4GHOtvzDyd43mOxkQHxA2XdSqG3GGkHQEMhE7SZNg4CHs6JlzDcz2JV2+kv/1Ob
QEUdNhrgQqF7Azex4Fa7JpxiNog2/+t5He5DpRu/420bke3vPltZuxhn9U41kx+nsYK90DwFiD+6
2RBwwV5NjSkiLK1opz8GiinP4NVXajI+fxXadR8GxWS9jpvPCtj5CHxH/whWuhxME8Xia8xLX4bl
x/z8CS84GKYLzDUMSK7B62XxxvZaLsWPohqsESMoS/Vr0s8+xUl8/lGMRCeKVIwLT/N3n/ujINiO
j2FB/EA2TloWc8Wd/3c041r6NOA533jUcGAosJ/Dz3CDSOQdrIoqQVdeG+fOJA4sR1LjU1CdPlQu
SH5j5C8Fh4KAYjl7DUjwHwUEciyZsXqo2z7XK44b5Y9yqYGdotsCchWZxqSDF0md4N8yFzVM+81w
Dy7r2LysR1hpopYSAaBEhpHXAfWdM25nP2gCqcKZ2q7VaincmsUF9SrMzEaUaoOZaBdBOZfxru1E
3ny+aLJIB5Pp4QWEGMZ52sxm/xOqEO5pq7+DDBLuPdP+0/CsE8tbs4N51nWHq69qpnUZZUWUqkp3
4WCeEpI7mhZVyLhTay6q+cAeap7+di3aHYHgj83RRPVQPQKo7vCa6ThtuAF5CANAI/xq9j4ujz0L
q1swS8n+5aRxL3yVF+qzKyzSjtFyxuEax8Uf57dSrn8qwUPXy/P6ZaSzIpX35ktwqvfdGgDABGTq
ZPcF+9FGvQjEls6+bXukgFOVqJI1Ei/YSnkjxJYYqdMbCrAV6E/lN+1DcYZvnCqI2vW/8X2rezLe
U87wSbWDN9J0rleaQmgfiwt48UPmQd70+kW4n8ythkAxBd0HTLgGoYueIMj46ceKZmOW8sZDMoSZ
saFEUSmQ24OAYlbRvF52yt9o9XJZfnG/gXbz0ComVt9ZGtMfrKkmxVoTsSiOpCnek7qgQi5aaqQS
nMnQWR0vW83DnRLhF3O5kPk/9jtYCVXAuJ4oTk85yVKqKRlzSGpoEkUfykwmeYoZapt4iqE9NQG0
ibJmWU7ncEWVYbBCHTZqz+ulLG2WEo2vkRLKxMCjjI7S07aeCAbAL28criGqluj7s+4PqZLBax6r
7urWD7ASsKsTWT88cTV+2a7eWUU32zb85x/1
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
