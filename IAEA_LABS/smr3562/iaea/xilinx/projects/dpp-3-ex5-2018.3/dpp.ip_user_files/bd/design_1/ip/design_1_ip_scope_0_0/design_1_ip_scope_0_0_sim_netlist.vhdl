-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Thu Feb  4 23:58:07 2021
-- Host        : ZBOOK running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               e:/smr3562/iaea/xilinx/projects/dpp/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_scope_0_0/design_1_ip_scope_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_scope_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog is
  port (
    axi_awready_reg_0 : out STD_LOGIC;
    axi_wready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    i : out STD_LOGIC_VECTOR ( 10 downto 0 );
    \slv_reg_array_reg[0][0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[3][15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r3_dina : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg_array_reg[5][0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[6][10]_0\ : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    r9_douta : in STD_LOGIC_VECTOR ( 31 downto 0 );
    q : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog : entity is "axibusdomain_axi_lite_interface_verilog";
end design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog;

architecture STRUCTURE of design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog is
  signal axi_araddr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^d\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dec_r__15\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^i\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal r1_addra : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal r2_wea : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \^r3_dina\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r4_threshold : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r5_enable : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal r6_delay : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal r7_clear : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg_array[0][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][12]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][13]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][16]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][17]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][18]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][20]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][21]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][22]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][24]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][25]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][26]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][27]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][28]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][29]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][30]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_6_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][8]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][10]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][12]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][13]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][16]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][17]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][18]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][20]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][21]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][22]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][24]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][25]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][26]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][27]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][28]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][29]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][30]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][8]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][7]_i_2_n_0\ : STD_LOGIC;
  signal \^slv_reg_array_reg[0][0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg_array_reg[3][15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^slv_reg_array_reg[5][0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg_array_reg[6][10]_0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal slv_wire_array : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \slv_reg_array[0][10]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \slv_reg_array[0][11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \slv_reg_array[0][12]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \slv_reg_array[0][13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \slv_reg_array[0][14]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \slv_reg_array[0][15]_i_3\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \slv_reg_array[0][16]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg_array[0][17]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[0][18]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \slv_reg_array[0][19]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \slv_reg_array[0][1]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \slv_reg_array[0][20]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \slv_reg_array[0][21]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \slv_reg_array[0][22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \slv_reg_array[0][23]_i_3\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \slv_reg_array[0][24]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \slv_reg_array[0][25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \slv_reg_array[0][26]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \slv_reg_array[0][27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \slv_reg_array[0][28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \slv_reg_array[0][29]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \slv_reg_array[0][2]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \slv_reg_array[0][30]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \slv_reg_array[0][31]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[0][3]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \slv_reg_array[0][4]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \slv_reg_array[0][5]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \slv_reg_array[0][6]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \slv_reg_array[0][7]_i_3\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \slv_reg_array[0][8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \slv_reg_array[0][9]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \slv_reg_array[1][0]_i_2\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \slv_reg_array[1][11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \slv_reg_array[1][12]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \slv_reg_array[1][13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \slv_reg_array[1][14]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \slv_reg_array[1][15]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \slv_reg_array[1][16]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg_array[1][17]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[1][18]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \slv_reg_array[1][19]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \slv_reg_array[1][1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg_array[1][20]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \slv_reg_array[1][21]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \slv_reg_array[1][22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \slv_reg_array[1][23]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \slv_reg_array[1][24]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \slv_reg_array[1][25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \slv_reg_array[1][26]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \slv_reg_array[1][27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \slv_reg_array[1][28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \slv_reg_array[1][29]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \slv_reg_array[1][30]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \slv_reg_array[1][31]_i_2\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[1][3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \slv_reg_array[1][4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \slv_reg_array[1][7]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \slv_reg_array[1][8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \slv_reg_array[2][6]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[3][1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg_array[3][3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \slv_reg_array[3][4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \slv_reg_array[3][6]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[3][7]_i_2\ : label is "soft_lutpair6";
begin
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  d(0) <= \^d\(0);
  i(10 downto 0) <= \^i\(10 downto 0);
  r3_dina(31 downto 0) <= \^r3_dina\(31 downto 0);
  \slv_reg_array_reg[0][0]_0\(0) <= \^slv_reg_array_reg[0][0]_0\(0);
  \slv_reg_array_reg[3][15]_0\(15 downto 0) <= \^slv_reg_array_reg[3][15]_0\(15 downto 0);
  \slv_reg_array_reg[5][0]_0\(0) <= \^slv_reg_array_reg[5][0]_0\(0);
  \slv_reg_array_reg[6][10]_0\(10 downto 0) <= \^slv_reg_array_reg[6][10]_0\(10 downto 0);
\axi_araddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(0),
      Q => axi_araddr(0),
      R => p_0_in
    );
\axi_araddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(1),
      Q => axi_araddr(1),
      R => p_0_in
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(2),
      Q => axi_araddr(2),
      R => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(3),
      Q => axi_araddr(3),
      R => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(4),
      Q => axi_araddr(4),
      R => p_0_in
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(5),
      Q => axi_araddr(5),
      R => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => p_0_in
    );
\axi_awaddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(0),
      Q => axi_awaddr(0),
      R => p_0_in
    );
\axi_awaddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(1),
      Q => axi_awaddr(1),
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(2),
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(3),
      Q => axi_awaddr(3),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(4),
      Q => axi_awaddr(4),
      R => p_0_in
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(5),
      Q => axi_awaddr(5),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axibusdomain_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => p_0_in
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => axibusdomain_s_axi_bvalid,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(0),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[0]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[0]_i_3_n_0\,
      O => slv_wire_array(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => q(0),
      I1 => \^slv_reg_array_reg[6][10]_0\(0),
      I2 => \dec_r__15\(1),
      I3 => \^slv_reg_array_reg[5][0]_0\(0),
      I4 => \dec_r__15\(0),
      I5 => \^r3_dina\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(0),
      I1 => \^d\(0),
      I2 => \dec_r__15\(1),
      I3 => \^i\(0),
      I4 => \dec_r__15\(0),
      I5 => \^slv_reg_array_reg[0][0]_0\(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(10),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[10]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[10]_i_3_n_0\,
      O => slv_wire_array(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(10),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(10),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(10),
      I1 => r5_enable(10),
      I2 => \dec_r__15\(1),
      I3 => \^i\(10),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(11),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[11]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[11]_i_3_n_0\,
      O => slv_wire_array(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(11),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(11),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(11),
      I1 => r5_enable(11),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(11),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(12),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[12]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[12]_i_3_n_0\,
      O => slv_wire_array(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(12),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(12),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(12),
      I1 => r5_enable(12),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(12),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(13),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[13]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[13]_i_3_n_0\,
      O => slv_wire_array(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(13),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(13),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(13),
      I1 => r5_enable(13),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(13),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(14),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[14]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[14]_i_3_n_0\,
      O => slv_wire_array(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(14),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(14),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(14),
      I1 => r5_enable(14),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(14),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(15),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[15]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[15]_i_3_n_0\,
      O => slv_wire_array(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(15),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(15),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(15),
      I1 => r5_enable(15),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(15),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(16),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[16]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[16]_i_3_n_0\,
      O => slv_wire_array(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(16),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(16),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(16),
      I1 => r5_enable(16),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(16),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(17),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[17]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[17]_i_3_n_0\,
      O => slv_wire_array(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(17),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(17),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(17),
      I1 => r5_enable(17),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(17),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(18),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[18]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[18]_i_3_n_0\,
      O => slv_wire_array(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(18),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(18),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(18),
      I1 => r5_enable(18),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(18),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(19),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[19]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[19]_i_3_n_0\,
      O => slv_wire_array(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(19),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(19),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(19),
      I1 => r5_enable(19),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(19),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(1),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[1]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[1]_i_3_n_0\,
      O => slv_wire_array(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(1),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(1),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(1),
      I1 => r5_enable(1),
      I2 => \dec_r__15\(1),
      I3 => \^i\(1),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(20),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[20]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[20]_i_3_n_0\,
      O => slv_wire_array(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(20),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(20),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(20),
      I1 => r5_enable(20),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(20),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(21),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[21]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[21]_i_3_n_0\,
      O => slv_wire_array(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(21),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(21),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(21),
      I1 => r5_enable(21),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(21),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(22),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[22]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[22]_i_3_n_0\,
      O => slv_wire_array(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(22),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(22),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(22),
      I1 => r5_enable(22),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(22),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(23),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[23]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[23]_i_3_n_0\,
      O => slv_wire_array(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(23),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(23),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(23),
      I1 => r5_enable(23),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(23),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(24),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[24]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[24]_i_3_n_0\,
      O => slv_wire_array(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(24),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(24),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(24),
      I1 => r5_enable(24),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(24),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(25),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[25]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[25]_i_3_n_0\,
      O => slv_wire_array(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(25),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(25),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(25),
      I1 => r5_enable(25),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(25),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(26),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[26]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[26]_i_3_n_0\,
      O => slv_wire_array(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(26),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(26),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(26),
      I1 => r5_enable(26),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(26),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(27),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[27]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[27]_i_3_n_0\,
      O => slv_wire_array(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(27),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(27),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(27),
      I1 => r5_enable(27),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(27),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(28),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[28]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[28]_i_3_n_0\,
      O => slv_wire_array(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(28),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(28),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(28),
      I1 => r5_enable(28),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(28),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(29),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[29]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[29]_i_3_n_0\,
      O => slv_wire_array(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(29),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(29),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(29),
      I1 => r5_enable(29),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(29),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(2),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[2]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[2]_i_3_n_0\,
      O => slv_wire_array(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(2),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(2),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(2),
      I1 => r5_enable(2),
      I2 => \dec_r__15\(1),
      I3 => \^i\(2),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(30),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[30]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[30]_i_3_n_0\,
      O => slv_wire_array(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(30),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(30),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(30),
      I1 => r5_enable(30),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(30),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(31),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[31]_i_3_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[31]_i_5_n_0\,
      O => slv_wire_array(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => axi_araddr(0),
      I1 => axi_araddr(4),
      I2 => axi_araddr(1),
      I3 => axi_araddr(5),
      I4 => axi_araddr(3),
      I5 => axi_araddr(2),
      O => \dec_r__15\(3)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(31),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(31),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(4),
      I3 => axi_araddr(1),
      O => \dec_r__15\(2)
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(31),
      I1 => r5_enable(31),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(31),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(31),
      O => \axi_rdata[31]_i_5_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(3),
      I3 => axi_araddr(1),
      O => \dec_r__15\(1)
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(2),
      I3 => axi_araddr(1),
      O => \dec_r__15\(0)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(3),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[3]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[3]_i_3_n_0\,
      O => slv_wire_array(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(3),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(3),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(3),
      I1 => r5_enable(3),
      I2 => \dec_r__15\(1),
      I3 => \^i\(3),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(4),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[4]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[4]_i_3_n_0\,
      O => slv_wire_array(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(4),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(4),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(4),
      I1 => r5_enable(4),
      I2 => \dec_r__15\(1),
      I3 => \^i\(4),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(5),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[5]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[5]_i_3_n_0\,
      O => slv_wire_array(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(5),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(5),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(5),
      I1 => r5_enable(5),
      I2 => \dec_r__15\(1),
      I3 => \^i\(5),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(6),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[6]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[6]_i_3_n_0\,
      O => slv_wire_array(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(6),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(6),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(6),
      I1 => r5_enable(6),
      I2 => \dec_r__15\(1),
      I3 => \^i\(6),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(7),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[7]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[7]_i_3_n_0\,
      O => slv_wire_array(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(7),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(7),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(7),
      I1 => r5_enable(7),
      I2 => \dec_r__15\(1),
      I3 => \^i\(7),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(8),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[8]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[8]_i_3_n_0\,
      O => slv_wire_array(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(8),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(8),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(8),
      I1 => r5_enable(8),
      I2 => \dec_r__15\(1),
      I3 => \^i\(8),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(9),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[9]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[9]_i_3_n_0\,
      O => slv_wire_array(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(9),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(9),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(9),
      I1 => r5_enable(9),
      I2 => \dec_r__15\(1),
      I3 => \^i\(9),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(0),
      Q => axibusdomain_s_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(10),
      Q => axibusdomain_s_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(11),
      Q => axibusdomain_s_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(12),
      Q => axibusdomain_s_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(13),
      Q => axibusdomain_s_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(14),
      Q => axibusdomain_s_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(15),
      Q => axibusdomain_s_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(16),
      Q => axibusdomain_s_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(17),
      Q => axibusdomain_s_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(18),
      Q => axibusdomain_s_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(19),
      Q => axibusdomain_s_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(1),
      Q => axibusdomain_s_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(20),
      Q => axibusdomain_s_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(21),
      Q => axibusdomain_s_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(22),
      Q => axibusdomain_s_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(23),
      Q => axibusdomain_s_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(24),
      Q => axibusdomain_s_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(25),
      Q => axibusdomain_s_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(26),
      Q => axibusdomain_s_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(27),
      Q => axibusdomain_s_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(28),
      Q => axibusdomain_s_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(29),
      Q => axibusdomain_s_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(2),
      Q => axibusdomain_s_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(30),
      Q => axibusdomain_s_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(31),
      Q => axibusdomain_s_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(3),
      Q => axibusdomain_s_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(4),
      Q => axibusdomain_s_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(5),
      Q => axibusdomain_s_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(6),
      Q => axibusdomain_s_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(7),
      Q => axibusdomain_s_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(8),
      Q => axibusdomain_s_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(9),
      Q => axibusdomain_s_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^axibusdomain_s_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => p_0_in
    );
\slv_reg_array[0][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8FFF000080000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      I3 => \slv_reg_array[0][31]_i_4_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[0][0]_0\(0),
      O => \slv_reg_array[0][0]_i_1_n_0\
    );
\slv_reg_array[0][10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(10),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][10]_i_1_n_0\
    );
\slv_reg_array[0][11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(11),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][11]_i_1_n_0\
    );
\slv_reg_array[0][12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(12),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][12]_i_1_n_0\
    );
\slv_reg_array[0][13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(13),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][13]_i_1_n_0\
    );
\slv_reg_array[0][14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(14),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][14]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      O => \slv_reg_array[0][15]_i_2_n_0\
    );
\slv_reg_array[0][15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(15),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][15]_i_3_n_0\
    );
\slv_reg_array[0][15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => axibusdomain_aresetn,
      I5 => axibusdomain_s_axi_wstrb(1),
      O => \slv_reg_array[0][15]_i_4_n_0\
    );
\slv_reg_array[0][15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][15]_i_5_n_0\
    );
\slv_reg_array[0][16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(16),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][16]_i_1_n_0\
    );
\slv_reg_array[0][17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(17),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][17]_i_1_n_0\
    );
\slv_reg_array[0][18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(18),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][18]_i_1_n_0\
    );
\slv_reg_array[0][19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(19),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][19]_i_1_n_0\
    );
\slv_reg_array[0][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(1),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][1]_i_1_n_0\
    );
\slv_reg_array[0][20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(20),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][20]_i_1_n_0\
    );
\slv_reg_array[0][21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(21),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][21]_i_1_n_0\
    );
\slv_reg_array[0][22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(22),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][22]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][23]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][23]_i_4_n_0\,
      O => \slv_reg_array[0][23]_i_2_n_0\
    );
\slv_reg_array[0][23]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(23),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][23]_i_3_n_0\
    );
\slv_reg_array[0][23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_4_n_0\
    );
\slv_reg_array[0][23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(2),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_5_n_0\
    );
\slv_reg_array[0][24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(24),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][24]_i_1_n_0\
    );
\slv_reg_array[0][25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(25),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][25]_i_1_n_0\
    );
\slv_reg_array[0][26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(26),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][26]_i_1_n_0\
    );
\slv_reg_array[0][27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(27),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][27]_i_1_n_0\
    );
\slv_reg_array[0][28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(28),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][28]_i_1_n_0\
    );
\slv_reg_array[0][29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(29),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][29]_i_1_n_0\
    );
\slv_reg_array[0][2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(2),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][2]_i_1_n_0\
    );
\slv_reg_array[0][30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(30),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][30]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][31]_i_2_n_0\
    );
\slv_reg_array[0][31]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(31),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][31]_i_3_n_0\
    );
\slv_reg_array[0][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFCFFFCFFFCFEFD"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[0][31]_i_4_n_0\
    );
\slv_reg_array[0][31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_5_n_0\
    );
\slv_reg_array[0][31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(3),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_6_n_0\
    );
\slv_reg_array[0][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(3),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][3]_i_1_n_0\
    );
\slv_reg_array[0][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(4),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][4]_i_1_n_0\
    );
\slv_reg_array[0][5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(5),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][5]_i_1_n_0\
    );
\slv_reg_array[0][6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(6),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][6]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      O => \slv_reg_array[0][7]_i_2_n_0\
    );
\slv_reg_array[0][7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(7),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][7]_i_3_n_0\
    );
\slv_reg_array[0][7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_4_n_0\
    );
\slv_reg_array[0][7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(0),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_5_n_0\
    );
\slv_reg_array[0][8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(8),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][8]_i_1_n_0\
    );
\slv_reg_array[0][9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(9),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][9]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4FFF404040004040"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => \slv_reg_array[1][0]_i_2_n_0\,
      I2 => \slv_reg_array[1][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^i\(0),
      O => \slv_reg_array[1][0]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(0),
      O => \slv_reg_array[1][0]_i_2_n_0\
    );
\slv_reg_array[1][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDD0200"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(10),
      I4 => \^i\(10),
      O => \slv_reg_array[1][10]_i_1_n_0\
    );
\slv_reg_array[1][10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][10]_i_2_n_0\
    );
\slv_reg_array[1][11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(11),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][11]_i_1_n_0\
    );
\slv_reg_array[1][12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(12),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][12]_i_1_n_0\
    );
\slv_reg_array[1][13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(13),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][13]_i_1_n_0\
    );
\slv_reg_array[1][14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(14),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array[1][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      O => \slv_reg_array[1][15]_i_1_n_0\
    );
\slv_reg_array[1][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(15),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][15]_i_2_n_0\
    );
\slv_reg_array[1][16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(16),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][16]_i_1_n_0\
    );
\slv_reg_array[1][17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(17),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][17]_i_1_n_0\
    );
\slv_reg_array[1][18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(18),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][18]_i_1_n_0\
    );
\slv_reg_array[1][19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(19),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][19]_i_1_n_0\
    );
\slv_reg_array[1][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(1),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][1]_i_1_n_0\
    );
\slv_reg_array[1][20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(20),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][20]_i_1_n_0\
    );
\slv_reg_array[1][21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(21),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][21]_i_1_n_0\
    );
\slv_reg_array[1][22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(22),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][22]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      O => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(23),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][23]_i_2_n_0\
    );
\slv_reg_array[1][24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(24),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][24]_i_1_n_0\
    );
\slv_reg_array[1][25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(25),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][25]_i_1_n_0\
    );
\slv_reg_array[1][26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(26),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][26]_i_1_n_0\
    );
\slv_reg_array[1][27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(27),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][27]_i_1_n_0\
    );
\slv_reg_array[1][28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(28),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][28]_i_1_n_0\
    );
\slv_reg_array[1][29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(29),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][29]_i_1_n_0\
    );
\slv_reg_array[1][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(2),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(2),
      O => \slv_reg_array[1][2]_i_1_n_0\
    );
\slv_reg_array[1][30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(30),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][30]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      O => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(31),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][31]_i_2_n_0\
    );
\slv_reg_array[1][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][31]_i_3_n_0\
    );
\slv_reg_array[1][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \slv_reg_array[1][10]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[1][31]_i_4_n_0\
    );
\slv_reg_array[1][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(3),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][3]_i_1_n_0\
    );
\slv_reg_array[1][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(4),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][4]_i_1_n_0\
    );
\slv_reg_array[1][5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(5),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(5),
      O => \slv_reg_array[1][5]_i_1_n_0\
    );
\slv_reg_array[1][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(6),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(6),
      O => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array[1][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      O => \slv_reg_array[1][7]_i_1_n_0\
    );
\slv_reg_array[1][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(7),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][7]_i_2_n_0\
    );
\slv_reg_array[1][8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(8),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][8]_i_1_n_0\
    );
\slv_reg_array[1][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDD0200"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(9),
      I4 => \^i\(9),
      O => \slv_reg_array[1][9]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFEF00"
    )
        port map (
      I0 => \slv_reg_array[2][6]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \slv_reg_array[2][0]_i_2_n_0\,
      I4 => \^d\(0),
      O => \slv_reg_array[2][0]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \slv_reg_array[2][31]_i_2_n_0\,
      I1 => \slv_reg_array[0][7]_i_5_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[2][0]_i_2_n_0\
    );
\slv_reg_array[2][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE0100"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(10),
      I4 => r5_enable(10),
      O => \slv_reg_array[2][10]_i_1_n_0\
    );
\slv_reg_array[2][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array[2][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array[2][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(2),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(2),
      O => \slv_reg_array[2][2]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(5),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[2][31]_i_2_n_0\
    );
\slv_reg_array[2][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(5),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(5),
      O => \slv_reg_array[2][5]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(6),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(6),
      O => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      O => \slv_reg_array[2][6]_i_2_n_0\
    );
\slv_reg_array[2][6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000F0001000A"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[2][6]_i_3_n_0\
    );
\slv_reg_array[2][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][7]_i_1_n_0\
    );
\slv_reg_array[2][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE0100"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(9),
      I4 => r5_enable(9),
      O => \slv_reg_array[2][9]_i_1_n_0\
    );
\slv_reg_array[3][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4FF040404000404"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => \slv_reg_array[1][0]_i_2_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[3][15]_0\(0),
      O => \slv_reg_array[3][0]_i_1_n_0\
    );
\slv_reg_array[3][10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(10),
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(10),
      O => \slv_reg_array[3][10]_i_1_n_0\
    );
\slv_reg_array[3][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array[3][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(1),
      O => \slv_reg_array[3][1]_i_1_n_0\
    );
\slv_reg_array[3][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array[3][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(2),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][2]_i_1_n_0\
    );
\slv_reg_array[3][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array[3][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(3),
      O => \slv_reg_array[3][3]_i_1_n_0\
    );
\slv_reg_array[3][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(4),
      O => \slv_reg_array[3][4]_i_1_n_0\
    );
\slv_reg_array[3][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(5),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][5]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(6),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][6]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(5),
      O => \slv_reg_array[3][6]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array[3][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(7),
      O => \slv_reg_array[3][7]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFDFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(5),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(1),
      I4 => axi_awaddr(4),
      I5 => axi_awaddr(3),
      O => \slv_reg_array[3][7]_i_3_n_0\
    );
\slv_reg_array[3][9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(9),
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(9),
      O => \slv_reg_array[3][9]_i_1_n_0\
    );
\slv_reg_array[4][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[4][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^r3_dina\(0),
      O => \slv_reg_array[4][0]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][15]_i_2_n_0\
    );
\slv_reg_array[4][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array[4][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][23]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array[4][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][31]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[4][31]_i_3_n_0\
    );
\slv_reg_array[4][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array[4][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][7]_i_2_n_0\
    );
\slv_reg_array[5][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[5][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[5][0]_0\(0),
      O => \slv_reg_array[5][0]_i_1_n_0\
    );
\slv_reg_array[5][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array[5][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][15]_i_2_n_0\
    );
\slv_reg_array[5][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array[5][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][23]_i_2_n_0\
    );
\slv_reg_array[5][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array[5][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][31]_i_2_n_0\
    );
\slv_reg_array[5][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFFFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(5),
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[5][31]_i_3_n_0\
    );
\slv_reg_array[5][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array[5][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][7]_i_2_n_0\
    );
\slv_reg_array[6][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[6][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[6][10]_0\(0),
      O => \slv_reg_array[6][0]_i_1_n_0\
    );
\slv_reg_array[6][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array[6][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][15]_i_2_n_0\
    );
\slv_reg_array[6][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array[6][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][23]_i_2_n_0\
    );
\slv_reg_array[6][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array[6][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][31]_i_2_n_0\
    );
\slv_reg_array[6][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[6][31]_i_3_n_0\
    );
\slv_reg_array[6][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array[6][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][7]_i_2_n_0\
    );
\slv_reg_array_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[0][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[0][0]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[0][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => r7_clear(10),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r7_clear(11),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r7_clear(12),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r7_clear(13),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r7_clear(14),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r7_clear(15),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r7_clear(16),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r7_clear(17),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r7_clear(18),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r7_clear(19),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => r7_clear(1),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r7_clear(20),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r7_clear(21),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r7_clear(22),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r7_clear(23),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r7_clear(24),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r7_clear(25),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r7_clear(26),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r7_clear(27),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r7_clear(28),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r7_clear(29),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => r7_clear(2),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r7_clear(30),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r7_clear(31),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => r7_clear(3),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => r7_clear(4),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => r7_clear(5),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => r7_clear(6),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => r7_clear(7),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => r7_clear(8),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => r7_clear(9),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[1][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][0]_i_1_n_0\,
      Q => \^i\(0),
      R => '0'
    );
\slv_reg_array_reg[1][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][10]_i_1_n_0\,
      Q => \^i\(10),
      R => '0'
    );
\slv_reg_array_reg[1][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => r6_delay(11),
      R => '0'
    );
\slv_reg_array_reg[1][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => r6_delay(12),
      R => '0'
    );
\slv_reg_array_reg[1][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => r6_delay(13),
      R => '0'
    );
\slv_reg_array_reg[1][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => r6_delay(14),
      R => '0'
    );
\slv_reg_array_reg[1][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => r6_delay(15),
      R => '0'
    );
\slv_reg_array_reg[1][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r6_delay(16),
      R => '0'
    );
\slv_reg_array_reg[1][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r6_delay(17),
      R => '0'
    );
\slv_reg_array_reg[1][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r6_delay(18),
      R => '0'
    );
\slv_reg_array_reg[1][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r6_delay(19),
      R => '0'
    );
\slv_reg_array_reg[1][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => \^i\(1),
      R => '0'
    );
\slv_reg_array_reg[1][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r6_delay(20),
      R => '0'
    );
\slv_reg_array_reg[1][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r6_delay(21),
      R => '0'
    );
\slv_reg_array_reg[1][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r6_delay(22),
      R => '0'
    );
\slv_reg_array_reg[1][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r6_delay(23),
      R => '0'
    );
\slv_reg_array_reg[1][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r6_delay(24),
      R => '0'
    );
\slv_reg_array_reg[1][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r6_delay(25),
      R => '0'
    );
\slv_reg_array_reg[1][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r6_delay(26),
      R => '0'
    );
\slv_reg_array_reg[1][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r6_delay(27),
      R => '0'
    );
\slv_reg_array_reg[1][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r6_delay(28),
      R => '0'
    );
\slv_reg_array_reg[1][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r6_delay(29),
      R => '0'
    );
\slv_reg_array_reg[1][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][2]_i_1_n_0\,
      Q => \^i\(2),
      R => '0'
    );
\slv_reg_array_reg[1][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r6_delay(30),
      R => '0'
    );
\slv_reg_array_reg[1][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r6_delay(31),
      R => '0'
    );
\slv_reg_array_reg[1][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][3]_i_1_n_0\,
      Q => \^i\(3),
      R => '0'
    );
\slv_reg_array_reg[1][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][4]_i_1_n_0\,
      Q => \^i\(4),
      R => '0'
    );
\slv_reg_array_reg[1][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][5]_i_1_n_0\,
      Q => \^i\(5),
      R => '0'
    );
\slv_reg_array_reg[1][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][6]_i_1_n_0\,
      Q => \^i\(6),
      R => '0'
    );
\slv_reg_array_reg[1][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][7]_i_2_n_0\,
      Q => \^i\(7),
      R => '0'
    );
\slv_reg_array_reg[1][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => \^i\(8),
      R => '0'
    );
\slv_reg_array_reg[1][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][9]_i_1_n_0\,
      Q => \^i\(9),
      R => '0'
    );
\slv_reg_array_reg[2][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][0]_i_1_n_0\,
      Q => \^d\(0),
      R => '0'
    );
\slv_reg_array_reg[2][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][10]_i_1_n_0\,
      Q => r5_enable(10),
      R => '0'
    );
\slv_reg_array_reg[2][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => r5_enable(11),
      R => '0'
    );
\slv_reg_array_reg[2][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => r5_enable(12),
      R => '0'
    );
\slv_reg_array_reg[2][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => r5_enable(13),
      R => '0'
    );
\slv_reg_array_reg[2][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => r5_enable(14),
      R => '0'
    );
\slv_reg_array_reg[2][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => r5_enable(15),
      R => '0'
    );
\slv_reg_array_reg[2][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r5_enable(16),
      R => '0'
    );
\slv_reg_array_reg[2][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r5_enable(17),
      R => '0'
    );
\slv_reg_array_reg[2][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r5_enable(18),
      R => '0'
    );
\slv_reg_array_reg[2][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r5_enable(19),
      R => '0'
    );
\slv_reg_array_reg[2][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => r5_enable(1),
      R => '0'
    );
\slv_reg_array_reg[2][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r5_enable(20),
      R => '0'
    );
\slv_reg_array_reg[2][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r5_enable(21),
      R => '0'
    );
\slv_reg_array_reg[2][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r5_enable(22),
      R => '0'
    );
\slv_reg_array_reg[2][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r5_enable(23),
      R => '0'
    );
\slv_reg_array_reg[2][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r5_enable(24),
      R => '0'
    );
\slv_reg_array_reg[2][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r5_enable(25),
      R => '0'
    );
\slv_reg_array_reg[2][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r5_enable(26),
      R => '0'
    );
\slv_reg_array_reg[2][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r5_enable(27),
      R => '0'
    );
\slv_reg_array_reg[2][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r5_enable(28),
      R => '0'
    );
\slv_reg_array_reg[2][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r5_enable(29),
      R => '0'
    );
\slv_reg_array_reg[2][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][2]_i_1_n_0\,
      Q => r5_enable(2),
      R => '0'
    );
\slv_reg_array_reg[2][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r5_enable(30),
      R => '0'
    );
\slv_reg_array_reg[2][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r5_enable(31),
      R => '0'
    );
\slv_reg_array_reg[2][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][3]_i_1_n_0\,
      Q => r5_enable(3),
      R => '0'
    );
\slv_reg_array_reg[2][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][4]_i_1_n_0\,
      Q => r5_enable(4),
      R => '0'
    );
\slv_reg_array_reg[2][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][5]_i_1_n_0\,
      Q => r5_enable(5),
      R => '0'
    );
\slv_reg_array_reg[2][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][6]_i_1_n_0\,
      Q => r5_enable(6),
      R => '0'
    );
\slv_reg_array_reg[2][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][7]_i_2_n_0\,
      Q => r5_enable(7),
      R => '0'
    );
\slv_reg_array_reg[2][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => r5_enable(8),
      R => '0'
    );
\slv_reg_array_reg[2][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][9]_i_1_n_0\,
      Q => r5_enable(9),
      R => '0'
    );
\slv_reg_array_reg[3][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[3][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][10]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(10),
      R => '0'
    );
\slv_reg_array_reg[3][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(11),
      R => '0'
    );
\slv_reg_array_reg[3][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(12),
      R => '0'
    );
\slv_reg_array_reg[3][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(13),
      R => '0'
    );
\slv_reg_array_reg[3][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(14),
      R => '0'
    );
\slv_reg_array_reg[3][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(15),
      R => '0'
    );
\slv_reg_array_reg[3][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r4_threshold(16),
      R => '0'
    );
\slv_reg_array_reg[3][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r4_threshold(17),
      R => '0'
    );
\slv_reg_array_reg[3][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r4_threshold(18),
      R => '0'
    );
\slv_reg_array_reg[3][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r4_threshold(19),
      R => '0'
    );
\slv_reg_array_reg[3][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(1),
      R => '0'
    );
\slv_reg_array_reg[3][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r4_threshold(20),
      R => '0'
    );
\slv_reg_array_reg[3][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r4_threshold(21),
      R => '0'
    );
\slv_reg_array_reg[3][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r4_threshold(22),
      R => '0'
    );
\slv_reg_array_reg[3][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r4_threshold(23),
      R => '0'
    );
\slv_reg_array_reg[3][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r4_threshold(24),
      R => '0'
    );
\slv_reg_array_reg[3][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r4_threshold(25),
      R => '0'
    );
\slv_reg_array_reg[3][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r4_threshold(26),
      R => '0'
    );
\slv_reg_array_reg[3][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r4_threshold(27),
      R => '0'
    );
\slv_reg_array_reg[3][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r4_threshold(28),
      R => '0'
    );
\slv_reg_array_reg[3][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r4_threshold(29),
      R => '0'
    );
\slv_reg_array_reg[3][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(2),
      R => '0'
    );
\slv_reg_array_reg[3][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r4_threshold(30),
      R => '0'
    );
\slv_reg_array_reg[3][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r4_threshold(31),
      R => '0'
    );
\slv_reg_array_reg[3][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(3),
      R => '0'
    );
\slv_reg_array_reg[3][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(4),
      R => '0'
    );
\slv_reg_array_reg[3][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(5),
      R => '0'
    );
\slv_reg_array_reg[3][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(6),
      R => '0'
    );
\slv_reg_array_reg[3][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][7]_i_2_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(7),
      R => '0'
    );
\slv_reg_array_reg[3][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(8),
      R => '0'
    );
\slv_reg_array_reg[3][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][9]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(9),
      R => '0'
    );
\slv_reg_array_reg[4][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][0]_i_1_n_0\,
      Q => \^r3_dina\(0),
      R => '0'
    );
\slv_reg_array_reg[4][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => \^r3_dina\(10),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => \^r3_dina\(11),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => \^r3_dina\(12),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => \^r3_dina\(13),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => \^r3_dina\(14),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => \^r3_dina\(15),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => \^r3_dina\(16),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => \^r3_dina\(17),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => \^r3_dina\(18),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => \^r3_dina\(19),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => \^r3_dina\(1),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => \^r3_dina\(20),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => \^r3_dina\(21),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => \^r3_dina\(22),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => \^r3_dina\(23),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => \^r3_dina\(24),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => \^r3_dina\(25),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => \^r3_dina\(26),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => \^r3_dina\(27),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => \^r3_dina\(28),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => \^r3_dina\(29),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => \^r3_dina\(2),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => \^r3_dina\(30),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => \^r3_dina\(31),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => \^r3_dina\(3),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => \^r3_dina\(4),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => \^r3_dina\(5),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => \^r3_dina\(6),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => \^r3_dina\(7),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => \^r3_dina\(8),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => \^r3_dina\(9),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[5][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[5][0]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[5][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => r2_wea(10),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r2_wea(11),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r2_wea(12),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r2_wea(13),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r2_wea(14),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r2_wea(15),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r2_wea(16),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r2_wea(17),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r2_wea(18),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r2_wea(19),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => r2_wea(1),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r2_wea(20),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r2_wea(21),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r2_wea(22),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r2_wea(23),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r2_wea(24),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r2_wea(25),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r2_wea(26),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r2_wea(27),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r2_wea(28),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r2_wea(29),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => r2_wea(2),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r2_wea(30),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r2_wea(31),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => r2_wea(3),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => r2_wea(4),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => r2_wea(5),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => r2_wea(6),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => r2_wea(7),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => r2_wea(8),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => r2_wea(9),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[6][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[6][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(10),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r1_addra(11),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r1_addra(12),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r1_addra(13),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r1_addra(14),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r1_addra(15),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r1_addra(16),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r1_addra(17),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r1_addra(18),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r1_addra(19),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(1),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r1_addra(20),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r1_addra(21),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r1_addra(22),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r1_addra(23),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r1_addra(24),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r1_addra(25),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r1_addra(26),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r1_addra(27),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r1_addra(28),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r1_addra(29),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(2),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r1_addra(30),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r1_addra(31),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(3),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(4),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(5),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(6),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(7),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(8),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(9),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axibusdomain_s_axi_rvalid\,
      I2 => \^axi_arready_reg_0\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a is
  port (
    \ff_2_17_reg[0]_0\ : out STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a : entity is "sysgen_mcode_block_2be3aaba4a";
end design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a;

architecture STRUCTURE of design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a is
begin
\ff_2_17_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => register4_q_net(0),
      Q => \ff_2_17_reg[0]_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d is
  port (
    \full_i_5_24_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    register_q_net : in STD_LOGIC;
    \state_4_23_reg[1]_2\ : in STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d : entity is "sysgen_mcode_block_46de72b52d";
end design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d;

architecture STRUCTURE of design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d is
  signal \^q\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \addr_i_6_24[7]_i_2_n_0\ : STD_LOGIC;
  signal \full_i_5_24[0]_i_1_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal rel_39_16 : STD_LOGIC;
  signal \rel_39_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_39_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_39_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_39_16_carry_n_0 : STD_LOGIC;
  signal rel_39_16_carry_n_1 : STD_LOGIC;
  signal rel_39_16_carry_n_2 : STD_LOGIC;
  signal rel_39_16_carry_n_3 : STD_LOGIC;
  signal state_4_23 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \state_4_23[0]_i_1_n_0\ : STD_LOGIC;
  signal \state_4_23[0]_i_2_n_0\ : STD_LOGIC;
  signal \state_4_23[0]_i_3_n_0\ : STD_LOGIC;
  signal \state_4_23[1]_i_1_n_0\ : STD_LOGIC;
  signal wm_8_20_inv_i_1_n_0 : STD_LOGIC;
  signal wm_8_20_reg_inv_n_0 : STD_LOGIC;
  signal NLW_rel_39_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_39_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \addr_i_6_24[10]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \addr_i_6_24[1]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addr_i_6_24[2]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addr_i_6_24[3]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \addr_i_6_24[4]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \addr_i_6_24[7]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \addr_i_6_24[8]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \addr_i_6_24[9]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \full_i_5_24[0]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \state_4_23[0]_i_2\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \state_4_23[0]_i_3\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of wm_8_20_inv_i_1 : label is "soft_lutpair37";
begin
  Q(10 downto 0) <= \^q\(10 downto 0);
\addr_i_6_24[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => p_0_in(0)
    );
\addr_i_6_24[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \state_4_23[0]_i_3_n_0\,
      I1 => \^q\(8),
      I2 => \^q\(9),
      I3 => \^q\(10),
      O => p_0_in(10)
    );
\addr_i_6_24[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => p_0_in(1)
    );
\addr_i_6_24[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => p_0_in(2)
    );
\addr_i_6_24[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(3),
      O => p_0_in(3)
    );
\addr_i_6_24[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^q\(4),
      O => p_0_in(4)
    );
\addr_i_6_24[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(3),
      I5 => \^q\(5),
      O => p_0_in(5)
    );
\addr_i_6_24[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \addr_i_6_24[7]_i_2_n_0\,
      I1 => \^q\(6),
      O => p_0_in(6)
    );
\addr_i_6_24[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(6),
      I1 => \addr_i_6_24[7]_i_2_n_0\,
      I2 => \^q\(7),
      O => p_0_in(7)
    );
\addr_i_6_24[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(3),
      I5 => \^q\(5),
      O => \addr_i_6_24[7]_i_2_n_0\
    );
\addr_i_6_24[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \state_4_23[0]_i_3_n_0\,
      I1 => \^q\(8),
      O => p_0_in(8)
    );
\addr_i_6_24[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(8),
      I1 => \state_4_23[0]_i_3_n_0\,
      I2 => \^q\(9),
      O => p_0_in(9)
    );
\addr_i_6_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(0),
      Q => \^q\(0),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(10),
      Q => \^q\(10),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(1),
      Q => \^q\(1),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(2),
      Q => \^q\(2),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(3),
      Q => \^q\(3),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(4),
      Q => \^q\(4),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(5),
      Q => \^q\(5),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(6),
      Q => \^q\(6),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(7),
      Q => \^q\(7),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(8),
      Q => \^q\(8),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(9),
      Q => \^q\(9),
      R => wm_8_20_reg_inv_n_0
    );
\full_i_5_24[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => register_q_net,
      I1 => state_4_23(1),
      I2 => state_4_23(0),
      O => \full_i_5_24[0]_i_1_n_0\
    );
\full_i_5_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \full_i_5_24[0]_i_1_n_0\,
      Q => \full_i_5_24_reg[0]_0\(0),
      R => '0'
    );
rel_39_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_39_16_carry_n_0,
      CO(2) => rel_39_16_carry_n_1,
      CO(1) => rel_39_16_carry_n_2,
      CO(0) => rel_39_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => NLW_rel_39_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\rel_39_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_39_16_carry_n_0,
      CO(3) => rel_39_16,
      CO(2) => \rel_39_16_carry__0_n_1\,
      CO(1) => \rel_39_16_carry__0_n_2\,
      CO(0) => \rel_39_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_4_23_reg[1]_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_39_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_4_23_reg[1]_1\(3 downto 0)
    );
\state_4_23[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF555F777FFFF"
    )
        port map (
      I0 => register_q_net,
      I1 => rel_39_16,
      I2 => \state_4_23[0]_i_2_n_0\,
      I3 => \state_4_23[0]_i_3_n_0\,
      I4 => state_4_23(0),
      I5 => state_4_23(1),
      O => \state_4_23[0]_i_1_n_0\
    );
\state_4_23[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^q\(9),
      I2 => \^q\(10),
      I3 => state_4_23(1),
      O => \state_4_23[0]_i_2_n_0\
    );
\state_4_23[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^q\(6),
      I1 => \addr_i_6_24[7]_i_2_n_0\,
      I2 => \^q\(7),
      O => \state_4_23[0]_i_3_n_0\
    );
\state_4_23[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFFFCC0000000000"
    )
        port map (
      I0 => \state_4_23_reg[1]_2\,
      I1 => rel_39_16,
      I2 => register4_q_net(0),
      I3 => state_4_23(0),
      I4 => state_4_23(1),
      I5 => register_q_net,
      O => \state_4_23[1]_i_1_n_0\
    );
\state_4_23_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \state_4_23[0]_i_1_n_0\,
      Q => state_4_23(0),
      R => '0'
    );
\state_4_23_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \state_4_23[1]_i_1_n_0\,
      Q => state_4_23(1),
      R => '0'
    );
we_i_7_22_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => '1',
      Q => web(0),
      R => wm_8_20_reg_inv_n_0
    );
wm_8_20_inv_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F7"
    )
        port map (
      I0 => register_q_net,
      I1 => state_4_23(1),
      I2 => state_4_23(0),
      O => wm_8_20_inv_i_1_n_0
    );
wm_8_20_reg_inv: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => wm_8_20_inv_i_1_n_0,
      Q => wm_8_20_reg_inv_n_0,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_sysgen_relational_f845914c88 is
  port (
    LOAD : out STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    o : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_sysgen_relational_f845914c88 : entity is "sysgen_relational_f845914c88";
end design_1_ip_scope_0_0_sysgen_relational_f845914c88;

architecture STRUCTURE of design_1_ip_scope_0_0_sysgen_relational_f845914c88 is
  signal \op_mem_37_22[0]_i_2_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_3_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_4_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_5_n_0\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal result_12_3_rel : STD_LOGIC;
  signal \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\op_mem_37_22[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => o(9),
      I1 => Q(9),
      I2 => o(10),
      I3 => Q(10),
      O => \op_mem_37_22[0]_i_2_n_0\
    );
\op_mem_37_22[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(6),
      I1 => Q(6),
      I2 => Q(8),
      I3 => o(8),
      I4 => Q(7),
      I5 => o(7),
      O => \op_mem_37_22[0]_i_3_n_0\
    );
\op_mem_37_22[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(3),
      I1 => Q(3),
      I2 => Q(5),
      I3 => o(5),
      I4 => Q(4),
      I5 => o(4),
      O => \op_mem_37_22[0]_i_4_n_0\
    );
\op_mem_37_22[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(0),
      I1 => Q(0),
      I2 => Q(2),
      I3 => o(2),
      I4 => Q(1),
      I5 => o(1),
      O => \op_mem_37_22[0]_i_5_n_0\
    );
\op_mem_37_22_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => result_12_3_rel,
      Q => LOAD,
      R => '0'
    );
\op_mem_37_22_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result_12_3_rel,
      CO(2) => \op_mem_37_22_reg[0]_i_1_n_1\,
      CO(1) => \op_mem_37_22_reg[0]_i_1_n_2\,
      CO(0) => \op_mem_37_22_reg[0]_i_1_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\(3 downto 0),
      S(3) => \op_mem_37_22[0]_i_2_n_0\,
      S(2) => \op_mem_37_22[0]_i_3_n_0\,
      S(1) => \op_mem_37_22[0]_i_4_n_0\,
      S(0) => \op_mem_37_22[0]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => enable(0),
      Q => register_q_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net,
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  port (
    d3_net : out STD_LOGIC;
    d2_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net,
      Q => d3_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\ is
  port (
    d2_net : out STD_LOGIC;
    d1_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net,
      Q => d2_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\ is
  port (
    d1_net : out STD_LOGIC;
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_enable(0),
      Q => d1_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal register5_q_net : STD_LOGIC_VECTOR ( 15 downto 14 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(13 downto 0) <= \^o\(13 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => register5_q_net(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => register5_q_net(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_39_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => register5_q_net(15),
      I1 => \rel_39_16_carry__0\(15),
      I2 => \rel_39_16_carry__0\(14),
      I3 => register5_q_net(14),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_1\(0)
    );
\rel_39_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => register5_q_net(15),
      I1 => \rel_39_16_carry__0\(15),
      I2 => register5_q_net(14),
      I3 => \rel_39_16_carry__0\(14),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(3)
    );
\rel_39_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(13),
      I1 => \rel_39_16_carry__0\(13),
      I2 => \^o\(12),
      I3 => \rel_39_16_carry__0\(12),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(2)
    );
\rel_39_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(11),
      I1 => \rel_39_16_carry__0\(11),
      I2 => \^o\(10),
      I3 => \rel_39_16_carry__0\(10),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(1)
    );
\rel_39_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(9),
      I1 => \rel_39_16_carry__0\(9),
      I2 => \^o\(8),
      I3 => \rel_39_16_carry__0\(8),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0)
    );
rel_39_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(7),
      I1 => \rel_39_16_carry__0\(7),
      I2 => \^o\(6),
      I3 => \rel_39_16_carry__0\(6),
      O => S(3)
    );
rel_39_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(5),
      I1 => \rel_39_16_carry__0\(5),
      I2 => \^o\(4),
      I3 => \rel_39_16_carry__0\(4),
      O => S(2)
    );
rel_39_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(3),
      I1 => \rel_39_16_carry__0\(3),
      I2 => \^o\(2),
      I3 => \rel_39_16_carry__0\(2),
      O => S(1)
    );
rel_39_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(1),
      I1 => \rel_39_16_carry__0\(1),
      I2 => \^o\(0),
      I3 => \rel_39_16_carry__0\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(15 downto 0) <= \^o\(15 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(14),
      Q => \^o\(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(15),
      Q => \^o\(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_39_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(13),
      I1 => \rel_39_16_carry__0\(13),
      I2 => \^o\(12),
      I3 => \rel_39_16_carry__0\(12),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(2)
    );
\rel_39_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(11),
      I1 => \rel_39_16_carry__0\(11),
      I2 => \^o\(10),
      I3 => \rel_39_16_carry__0\(10),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(1)
    );
\rel_39_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(9),
      I1 => \rel_39_16_carry__0\(9),
      I2 => \^o\(8),
      I3 => \rel_39_16_carry__0\(8),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(0)
    );
rel_39_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(7),
      I1 => \rel_39_16_carry__0\(7),
      I2 => \^o\(6),
      I3 => \rel_39_16_carry__0\(6),
      O => DI(3)
    );
rel_39_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(5),
      I1 => \rel_39_16_carry__0\(5),
      I2 => \^o\(4),
      I3 => \rel_39_16_carry__0\(4),
      O => DI(2)
    );
rel_39_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(3),
      I1 => \rel_39_16_carry__0\(3),
      I2 => \^o\(2),
      I3 => \rel_39_16_carry__0\(2),
      O => DI(1)
    );
rel_39_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(1),
      I1 => \rel_39_16_carry__0\(1),
      I2 => \^o\(0),
      I3 => \rel_39_16_carry__0\(0),
      O => DI(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => clear(0),
      Q => register4_q_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => r8_full(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => full(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_clear(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\ : entity is "xil_defaultlib_single_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_xil_defaultlib_srlc33e is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_xil_defaultlib_srlc33e : entity is "xil_defaultlib_srlc33e";
end design_1_ip_scope_0_0_xil_defaultlib_srlc33e;

architecture STRUCTURE of design_1_ip_scope_0_0_xil_defaultlib_srlc33e is
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \reg_array[0].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \reg_array[0].fde_used.u2\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \reg_array[0].fde_used.u2\ : label is "TRUE";
begin
\reg_array[0].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \reg_array[0].fde_used.u2_0\(0),
      Q => full(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init : entity is "blk_mem_gen_prim_wrapper_init";
end design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 8) => dina(16 downto 9),
      DIADI(7 downto 0) => dina(7 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1) => dina(17),
      DIPADIP(0) => dina(8),
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15 downto 8) => douta(16 downto 9),
      DOADO(7 downto 0) => douta(7 downto 0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => douta(17),
      DOPADOP(0) => douta(8),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\ : entity is "blk_mem_gen_prim_wrapper_init";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 15) => B"00000000000000000",
      DIADI(14 downto 8) => dina(13 downto 7),
      DIADI(7) => '0',
      DIADI(6 downto 0) => dina(6 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20\,
      DOADO(14 downto 8) => douta(13 downto 7),
      DOADO(7) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28\,
      DOADO(6 downto 0) => douta(6 downto 0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71\,
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\ : entity is "blk_mem_gen_prim_wrapper_init";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 4) => addrb(10 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 8) => dina(16 downto 9),
      DIADI(7 downto 0) => dina(7 downto 0),
      DIBDI(31 downto 16) => B"0000000000000000",
      DIBDI(15 downto 8) => dinb(16 downto 9),
      DIBDI(7 downto 0) => dinb(7 downto 0),
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1) => dina(17),
      DIPADIP(0) => dina(8),
      DIPBDIP(3 downto 2) => B"00",
      DIPBDIP(1) => dinb(17),
      DIPBDIP(0) => dinb(8),
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15 downto 8) => douta(16 downto 9),
      DOADO(7 downto 0) => douta(7 downto 0),
      DOBDO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 16),
      DOBDO(15 downto 8) => doutb(16 downto 9),
      DOBDO(7 downto 0) => doutb(7 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => douta(17),
      DOPADOP(0) => douta(8),
      DOPBDOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 2),
      DOPBDOP(1) => doutb(17),
      DOPBDOP(0) => doutb(8),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => enb,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => web(0),
      WEBWE(2) => web(0),
      WEBWE(1) => web(0),
      WEBWE(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\ : entity is "blk_mem_gen_prim_wrapper_init";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 4) => addrb(10 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 15) => B"00000000000000000",
      DIADI(14 downto 8) => dina(13 downto 7),
      DIADI(7) => '0',
      DIADI(6 downto 0) => dina(6 downto 0),
      DIBDI(31 downto 15) => B"00000000000000000",
      DIBDI(14 downto 8) => dinb(13 downto 7),
      DIBDI(7) => '0',
      DIBDI(6 downto 0) => dinb(6 downto 0),
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20\,
      DOADO(14 downto 8) => douta(13 downto 7),
      DOADO(7) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28\,
      DOADO(6 downto 0) => douta(6 downto 0),
      DOBDO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 16),
      DOBDO(15) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52\,
      DOBDO(14 downto 8) => doutb(13 downto 7),
      DOBDO(7) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60\,
      DOBDO(6 downto 0) => doutb(6 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71\,
      DOPBDOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 2),
      DOPBDOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74\,
      DOPBDOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75\,
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => enb,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => web(0),
      WEBWE(2) => web(0),
      WEBWE(1) => web(0),
      WEBWE(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
PkyhyBb59EPgq8kANKUgNUvJSxwVgcYTKLlfXroHeM6zPnPHm+ATuJPY2OmCojZnDY2A6SHiMUmx
ylnsx6jVAA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
XgKClVpS+h3z22aTgNZepCZW5Yffl4m6nNLRjY88G0b6Og6dF7wA3of30X3Vr2BKX5GVSe+jeu6a
q3D7Qa0T3sEnO1qnWdbom/P31G6nS7/pQCPaLh+suxznQX2imRfhfTkmY1B9wExxZtZBbss2GPfs
EFGX8a+efiUiZLAKaSE=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
LYMHL9qwz9VPPAbHAyLFK1YM6t0YBJUbhdak6y3IQta7KscLfLakFo9QXv7rXKj3R5WEjx6Vg+9K
QUgoa/uCYy+n2t004DDpVeDamNuGIrJU3WXV9mo6tEi21Rm+kIG+CFgVuqLY9JSjwI3dhmEqYYtS
wC2GIO6hKaV0keq1ldvsRFBu71kLY+jczboTe6EddpUktWp3UM/RqnrSfHPMlZWhHp1k3YC0SDq9
gvcPn9DB3vIjXgn+xRbyzZOt/j+s8RfjF446i2RalkF5p/den9o/OMG5jmv4rZKHj9S1V3Z2UuL1
c2fxe26sNIvZ7tpz8RHVWRMloPfcPVakam2zhg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
BACIRg239ZSAZHpsLobWk7IZyWSAM1rsaZq5LesIgnba07iijhvT5s8WIOIIgHZs1XEDKelSnU1J
+5cyEbU9WgPZsja6FQEw6J0GuN3L/1QyrvmNIJKsNXINx7R+xaY/n0uby2eFsFE9luplvdOyrCEw
eK82BghXwPdasTT1ZUgKiycyGYtNsp5ZaPIWXI9ezN9oHowcWp7Mn6v2jrdDl4lzJuoHgqRtkZvG
7GqevJFheGfXkRPuQGkNK2Pk6XN9woSB1a9C+FUsQBM5MlIE7zrBQAjONIQj/nd82Hlp1H4PRxBW
1mmFP7PskMeNR2hH5xwkvg4Q3IfYBlw8gdzneg==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
vUWbACu3JL9XeVH21XChN1bLnACIM0U/dLRQNf2LGaDFNW9CL0o3SY9pOtV226o71+9Eal6i7P4l
ht62RU2AHTweJsgWkXtQBI0/jHIw4/gxbBebNbqZM6m3qjEE5blPsuzJ1njoX2JWCJElO3p9FfRu
uHpC+4hYoccdFayGku3vk1gwz9lLJ4FcYG9mi1vLIY+tzs0o83THQ8dLrg50Rr/r2n0Xf4hxWe4U
tJ6iUOYBQUYjeOwNQOOxfjv5PKfLIgGA2WC8sJb2GFe9MkTDoMAo40nBLK0Y8+klDIJTyx079Bx0
wdRg2JxUF3+TGlXW98+2/iWy94H1CPEVRm18FQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
VX8rVAT0l4oniSvb1X0sblwaqcWh2XE0oCAZbC0SVv8fCy8dLmmtqBzFq3w2V/7nyMmJzWKNP/yV
0GW7ICEfrGaBejU3VpwaHA69xE56Y/8NSHGlZOhr390/5/UqELcFOknZEPJXMLpeKjUn2ijACn/u
O0myDIvGFiUyRGWWYKM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
dlKAt52rb1rebbUvCxUw/pmWR03F+be3vApC1VuekYTvk7BFt7xopdHrqsvoU8rgaCBc2wuCudx5
nUcu7bKEyHKFc6bcbp6J84c2uG0ZckyqBn/OHRMbmq4Vbar8C3ERI2YmcbL0Q0fBLzMosVarF9eM
+c6VfE9hA5lx9qpwFJhgk5v/yx6kjgu+kEnG+xsdWrpKrj8LIxxh6gkrPOn+jQtKQSX3o7q35Rcv
W3vWLRYdH+pHsfJqCdT0wL4oBTLa7ozdsufX9l6UDgT4ECxLf7R1TtNj7XA1jaaefThL0F1AUCjF
5WuhMqBOotpDZUmvB91yVtbXLMm0r85tK9b/iA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
JGeAczlQOSEml1qT/RNo33g/drVXys09zS31Q0AwHbF7XYi7IecC/baEiY3Bs5XyBJbde2EWCLRA
fy5gKNhc972S1zoHtVapMF4TEeIeNZ6eEZOrUxxwryh9d017Kb0qfv0unkc2gc4H5O1/FC3543bY
dxB9fMCJQwyHJvAVzFXj3NSm6s74s80BsQarBjUFqCHguZQUgt/cHEdD9pg8VhbCfBVQDG+TdDxs
V8o8Zim2eloivm9sk8oXkCLCjiMB2iIFZS63kAUy5erZJmpUrLwGc+P6h4E4Lvx3D9tB14eQlN4G
lKVWjWHWpa2TnIkF96jLfSCIwdQ54cB9SsDy5w==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
c+zhdWcuJts1VbudbPIHV55hHjZHCNFxYtLnvXGzKHZNymIxXfAdoanqeKdQILljm4DmHHoYhlQN
Dflj1Bm/J3mG4xqrBD7iXuQNibbfg2I452gmvvGqxQrQV72HDmEIQEiudJKQHeIH8/al4PYHUQlr
Ppx+QJJQqQ43d7AZitoQbHLZbRPHb2Zv36F2DoV5a8SCLT9x0QO9UcFkweOV8Br8I13bEd5ggSgj
Ao39T0xZ2gAFOQNQmYbCZZGMKb4wfb3ykCTJwXVQXaSfeyg4lv9ufGjYH4mmdHq8gepj92dc3v7K
/N8LRndTQHqT+gOjsJRe3CvgyG98gJPtqzcV0A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2960)
`protect data_block
zcqbY58vYyOida2erQWg+5TLBF7a5GAAbMOEC8UZOmv3+u7v4SLZGpLVssM2gh9uQIgMEaa9/WJ7
PzfBqmBQ22tfWrWGFuPRKxcrET+uqkwTOI6yHviMStqZRgggtatBvu09QvJWhwNJdF16tbftpl5U
T7Vn4lNgtkRDsQkNDwKsqApO3n1O/h1PmGyYQyPOFnke+HSoLU1nzLGZ24tuw6L7thfJrOujtL1I
TxzywTldw3B4I6UrcrKSml+ESZkZU4BjhOWBQvl8wL7TzaniBSOd+zeYqt2T8u0FxQZFHrdOtLoO
C+7jZZrtoNbUFzId6motsN3X+5KqiwXS2heuO31K//WK3rnSCDRni8/6iEThvMPnH7gebDWkW6jk
WidPhmgyyrPUnZb39Gwbbzy9VXuubW1mUaZ/3+HJyUYvzXpTd4Q1sP4yKxfC8P7x8ILUanV8ZnTC
R4mHs99cwYhsc2Zu0SMQEYcUkmKtS5M2nsQQs62BzlXUf4n7ev+Wnm2xVwvshrA8TfrmEfyHFp88
4f8VjAML9iWNxVG1AW+Y9Te4h2ZWpUFdIcVNfmy2/S8crAANpFsks7PLw54dFpGU1vzPcHtDKCoB
wjbZ6lK8XeaOWBo++ffk8aRvXdq7hUXpYSAWR7+idg8Hd9ZNDtAl7OMkOa2l3zetLk+gQZdLJ+nW
e9gMSdWk2CB+sBoJz12Ylkbs2nyRjKLgQieHKUJMwEzPwXqOwcGFQwAE025PC0Sq5Z3DT88gPckM
zFkGK8nINRs3GAqePh57LBB/b5KVlrHIQfutCnXWgF4iz+JVlKNdN6Gn0+Ak3xvZ3SO+pMIIfnp8
UyVNp6x/qhFGHLmiOYqNeGRvZ9MiXu3SLr9x9UHHRBsgVADmUSpIi2lUwlj66zVbEwx0HrT4wi1Q
3liYLepNgqu02w4bRsMb+tEUUcxLoOjCgOds9H52xW/ssyuF2UHP8Cv7ufj4avLkI2zXU5qVm/16
dN/3NKczowUALDLYTeRQMI2SwxQu+3gywv15kjKBW52Z9e/sLNb7AVYD8Dhv0v9a/r+ImbHVTG3D
jlRD+EIbp8IW29d+aczEdq8A4JQE7Fa1wvqvYGZ7iUeID+E/WdEft82yEvdJjSePBK4CO4IAHSIT
DmS8GT7Fd+wkBrvcWp8MkewQ+LPXkgwzm7+EB6i4uQfkm/WpFQB/kuN7xIfpnZpbvb48aNkA5EsV
3Qyb17koBLO0t3+5zMSukp6nGZm71+Zwg9I7e0TLR+Lo4Q1Dyd55/g0sRQk1Y5M3MvPd0tSH0NUg
u59QjjDmpMN1aQ5lJDRGdBl72QWubiVJacfiUan8QiGaYknfcVc4jdhSR2BMeKgCILTllgidaYPP
Qbg0uLzeo0/3vyhqwmptGFxYIWRyClThH2Do9fUbzKq3rdpsRNAdzXyNADtVfR6xei911v8BHoK0
KFtJ1/OCPnttrbGCBYyryXE/UwpYCJuRmnXy3y+CNkBVUCkk1eG0lLOCKx7dsGUWLX4JT3xXP1PY
u5eldX64ScGOD3UOYBDiBvB3IiykJbwVRdxo3uE1fvyR/K33YwPjNzudjcePJTY17OcZhk3xhPPi
JO/zj13MXYGwAMnIcOLy/7kpzU0E23/xXcihAwGRqUu1KIbFCpVruLeEAaOkAfJklK9uq5z+3YlD
TNl5jCyV/8BSoVCbpIgfs0hrPHx9RN+dDLLQJTnErb2zNmBjAEa/hmSeJ+UzdvyHjPHWny1GvyLa
eLvTbYsVityfjN/q20g8l6WnteiX6hvK2HGiOJ4m5gkQINbpJe92ol/kV41cnwIDHeTo9eE+3Oye
TH2xPGTqdp8E5BWM8OrgeXG6sUnaTUvgaYc/XmnFTDOVnkBeMrPeO/Xdg8MRWH0aPwVYtPak+d7+
1HUE7nyMMUQgrbLfh8LCbVjt9FJx6eIPvri7CK0I7k3F9J1wDsLJ01RFsly2NyIYgromRSrCsjdI
6inFbazpdpyT8RTkziIwLvfJ6OJFRspOu8Xa8Q8IsvOFJn8yVv0/tqS7TLmwdfy3t84u45tffa6e
ZrIF81SRPPCY/kIG3qaoHDCqbiwdet78XQV0ops07a9642r0GC25MOWJZEmNw8IGUI1EVB18fse8
CFbvzv+FLqSY/3MkdQaMLX6fKpFa/apRjBjgjVpBrwdP+QUeqseR4zeUiJ8KWldlWkVJ4B//w8zF
gCpocFr/JVQ+BGSZqzKtpbKi8kP0Ud1RPV8OBQ67CBaYR79ddBluuu/EqSL7W8xzTrSSBKjGUmds
hp7d7xRAU0gVj4J3WgTo+7kdAZP0Iuxm9N4KF28WUWC3ldJXVXAxXI0gs5i4jajeF2GHqpwvnMg0
eT7giQUwB7Zp0zy2Jkj+FkJIJHGePIyW6qwbbv+kJfu+ep2YEDs50a1fu+THErY3e6B9z3AmyZnb
ksazCZoPAtn/3iyxDwRyQsFg0QL15jFqqn6sq0WgowbiEKuSt4g4EMUVbfHl6+hu3YXb5/NL+wTW
liE2IrLrOfn4rdkj4KIsshvXtk0JBbCls3PTd56B22pfvz4wm/B/GOIoCm/XB0dN2yLFobDMryGw
VNn9ft0Dd0PO17VDKzJgEkUn5eK1VjMVF58iR9zHPggt1hLkgzynAlhizKD590kbrRbw3C8MtoA0
j0YbUlM2vzE/SsuCE3Bnf04jNsgfP7oodW/IN5Mui0jWW6f5m4PSKQZDEGd7t0YzZObXHXsYloa0
ZswJJaxjJemkMIZoOUcyHTqt0xE/cCJQf/otN+wSjuGHsbOvVM78z6uX9DCxu8pUAC4bGUdc3NSD
BOX6wPBnsPDTWU3UDnjCEZpqcbyJ777KuEfLXB7Ad5fva5Imm06gWB6pLhar7/8DYWVKRWpPG9aZ
6zl0aP5/wUuqZiwk/H148VrI2jVFjp3HjW4nRVTDQHReV8GCmF5f40HaFdTgVZ8GgtKHDVCTZfXr
yTFbO8j3cy/YbQQzBk1Gt9FWQIYPIPTxbVo1jnHTXSCj9nLSiei55PyljdKzDPB+wK5/wNDxrZ7U
PS8Cf6xxDTMZoY7eyydH58cmVtc8vU0qMfAg+zED1NjdfpvjO/6+05wBYULLeHc+1CH8dddJVf2C
yQh9AoTBE/yU6iV2rk72Un7kJ2YuSO5pdSGL0ngMZTAGaeeTCQcNQRHkCEwrT7InKT4CrLddvGKI
T79pNd9zBg6fJ4/sj+bOmTIOlfAejnwgkXBVyb3BspQYSnfFVq9tfuPgG9jGH7gdMyyszRs36P1F
B1SJ/2HKIIqIh3O9IuLyWyjF4ZLu9zAbWr/85wRm891lmYvpzjEj42ztLTiHIGLp+Z4oLA2urYIo
rqgObhO4CapkUjKOBj/Q9QqKVTiTShEHmDVs/5zbGrePQ/Wbfg5D+L17SJ/gSKliW+SuTLxE8h72
QzJIxCLpO0Xi5YwgDGaSR+VNJdidOB/ChHIOzEAFDQmW37i/+WI9obZf34Ycp9wfUFy6flgKX9D3
WYrZzfvEQ8993SN2jzXIYhLIehSOFIUxX9qGKQDnD4lQJEbtK6ANqnb/5nJd6wWQhc7P92CnZqpx
TJD5b03JREDG3KVb9kDrVEYhdoZ9mQ4ME7+I5zORmnAZqnn3fzUs68et064litLm1TElEWNP+AfD
F/Yq8HO0GoV/+qbcHsltfArptGTKTPykQtIWJ0MbJqDYBVQA076OTd5S6WJZ2b7T8chGGpXaC0Mf
itaLRHehX7p6ukXwqR0oqlbLtv54ZDLxwCJwaq6AJw1lr60NG1WARlKbvIFrd7idsqGPiTXkuQkn
Me0Qek0MY2ecxEIRj4MLWJWFFKonQd/p8D2/XNXl8BGvhmnGMydhl99SqZAPut9YIxwc9z1oBPCm
FqvY74rwyLMq4RjN95qdkXqQPNRjXcjtBwekU6PYUD/bx5o3MipRxi/54vn7Yj1+3ORObj8=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_axibusdomain_axi_lite_interface is
  port (
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    i : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    \slv_reg_array_reg[0][0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[3][15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r3_dina : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg_array_reg[5][0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[6][10]\ : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    r9_douta : in STD_LOGIC_VECTOR ( 31 downto 0 );
    q : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_axibusdomain_axi_lite_interface : entity is "axibusdomain_axi_lite_interface";
end design_1_ip_scope_0_0_axibusdomain_axi_lite_interface;

architecture STRUCTURE of design_1_ip_scope_0_0_axibusdomain_axi_lite_interface is
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^axibusdomain_s_axi_arready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_awready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_bvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_wready\ : STD_LOGIC;
begin
  axibusdomain_s_axi_arready <= \^axibusdomain_s_axi_arready\;
  axibusdomain_s_axi_awready <= \^axibusdomain_s_axi_awready\;
  axibusdomain_s_axi_bvalid <= \^axibusdomain_s_axi_bvalid\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  axibusdomain_s_axi_wready <= \^axibusdomain_s_axi_wready\;
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7444444444444444"
    )
        port map (
      I0 => axibusdomain_s_axi_bready,
      I1 => \^axibusdomain_s_axi_bvalid\,
      I2 => axibusdomain_s_axi_wvalid,
      I3 => axibusdomain_s_axi_awvalid,
      I4 => \^axibusdomain_s_axi_wready\,
      I5 => \^axibusdomain_s_axi_awready\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0F88"
    )
        port map (
      I0 => \^axibusdomain_s_axi_arready\,
      I1 => axibusdomain_s_axi_arvalid,
      I2 => axibusdomain_s_axi_rready,
      I3 => \^axibusdomain_s_axi_rvalid\,
      O => axi_rvalid_i_1_n_0
    );
inst: entity work.design_1_ip_scope_0_0_axibusdomain_axi_lite_interface_verilog
     port map (
      axi_arready_reg_0 => \^axibusdomain_s_axi_arready\,
      axi_awready_reg_0 => \^axibusdomain_s_axi_awready\,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      axi_wready_reg_0 => \^axibusdomain_s_axi_wready\,
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bvalid => \^axibusdomain_s_axi_bvalid\,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rvalid => \^axibusdomain_s_axi_rvalid\,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => d(0),
      i(10 downto 0) => i(10 downto 0),
      q(0) => q(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      \slv_reg_array_reg[0][0]_0\(0) => \slv_reg_array_reg[0][0]\(0),
      \slv_reg_array_reg[3][15]_0\(15 downto 0) => \slv_reg_array_reg[3][15]\(15 downto 0),
      \slv_reg_array_reg[5][0]_0\(0) => \slv_reg_array_reg[5][0]\(0),
      \slv_reg_array_reg[6][10]_0\(10 downto 0) => \slv_reg_array_reg[6][10]\(10 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller is
  port (
    \full_i_5_24_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    register_q_net : in STD_LOGIC;
    \state_4_23_reg[1]_1\ : in STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller : entity is "ip_scope_leveltriggerfifocontroller";
end design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller is
begin
mcode: entity work.design_1_ip_scope_0_0_sysgen_mcode_block_46de72b52d
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      Q(10 downto 0) => Q(10 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \full_i_5_24_reg[0]_0\(0) => \full_i_5_24_reg[0]\(0),
      register4_q_net(0) => register4_q_net(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk,
      \state_4_23_reg[1]_0\(3 downto 0) => \state_4_23_reg[1]\(3 downto 0),
      \state_4_23_reg[1]_1\(3 downto 0) => \state_4_23_reg[1]_0\(3 downto 0),
      \state_4_23_reg[1]_2\ => \state_4_23_reg[1]_1\,
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_risingedgetrigger is
  port (
    \ff_2_17_reg[0]\ : out STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_risingedgetrigger : entity is "ip_scope_risingedgetrigger";
end design_1_ip_scope_0_0_ip_scope_risingedgetrigger;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_risingedgetrigger is
begin
mcode: entity work.design_1_ip_scope_0_0_sysgen_mcode_block_2be3aaba4a
     port map (
      \ff_2_17_reg[0]_0\ => \ff_2_17_reg[0]\,
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_xil_defaultlib_synth_reg is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_xil_defaultlib_synth_reg : entity is "xil_defaultlib_synth_reg";
end design_1_ip_scope_0_0_xil_defaultlib_synth_reg;

architecture STRUCTURE of design_1_ip_scope_0_0_xil_defaultlib_synth_reg is
begin
\partial_one.last_srlc33e\: entity work.design_1_ip_scope_0_0_xil_defaultlib_srlc33e
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2_0\(0) => \reg_array[0].fde_used.u2\(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0\
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
  port (
    d1_net : out STD_LOGIC;
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_47\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net => d1_net,
      r5_enable(0) => r5_enable(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
  port (
    d2_net : out STD_LOGIC;
    d1_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_46\
     port map (
      d1_net => d1_net,
      d2_net => d2_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\ is
  port (
    d3_net : out STD_LOGIC;
    d2_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_45\
     port map (
      d2_net => d2_net,
      d3_net => d3_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized0_44\
     port map (
      d3_net => d3_net,
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1\
     port map (
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[15].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_1\(0) => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0),
      i(15 downto 0) => i(15 downto 0),
      o(13 downto 0) => o(13 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_17\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_16\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_15\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_14\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_5\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_7\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized1_9\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp_0\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(13 downto 0) => \rel_39_16_carry__0\(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_25\
     port map (
      d1_net(0) => d1_net(0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_24\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_23\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_22\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d3_net(0) => d3_net(0),
      r8_full(0) => r8_full(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_33\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r7_clear(0) => r7_clear(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_32\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_31\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized2_30\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_39\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => o(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_38\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3_37\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\ : entity is "xil_defaultlib_synth_reg_w_init";
end \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\;

architecture STRUCTURE of \design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\design_1_ip_scope_0_0_xil_defaultlib_single_reg_w_init__parameterized3\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_prim_width is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_prim_width : entity is "blk_mem_gen_prim_width";
end design_1_ip_scope_0_0_blk_mem_gen_prim_width;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_prim_width is
begin
\prim_init.ram\: entity work.design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(17 downto 0) => dina(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\ : entity is "blk_mem_gen_prim_width";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\ is
begin
\prim_init.ram\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(13 downto 0) => dina(13 downto 0),
      douta(13 downto 0) => douta(13 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\ : entity is "blk_mem_gen_prim_width";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\ is
begin
\prim_init.ram\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(17 downto 0) => dina(17 downto 0),
      dinb(17 downto 0) => dinb(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      doutb(17 downto 0) => doutb(17 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\ : entity is "blk_mem_gen_prim_width";
end \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\ is
begin
\prim_init.ram\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_wrapper_init__parameterized2\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(13 downto 0) => dina(13 downto 0),
      dinb(13 downto 0) => dinb(13 downto 0),
      douta(13 downto 0) => douta(13 downto 0),
      doutb(13 downto 0) => doutb(13 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
INaBf8vh5mCmDzf2yp77pxZAxQdyEQiT/vG2dEgvrFjseUnGc6ldwH4JvdnpZSpdf/ihioPyMNjl
u6ooyzv5TA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S5XIZZtuFR/MZffuhwdnvE3H9oRWM4uXoaGZTa/Dyk62O+Wa0v41pjmZELCiR7uodZPFQfykZ6K9
2ZDMu8dB3afQRMs5lnd/53M1b9ke+MNEeZ/wzjUcsJghubnEAwzdWeW/0tlqST1WD9B/KCxYqwH5
Gj6IZTTFHAXcaVhnCT8=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
CM6IcdzP0PbD6yMSqylmi4JE2qpmxiNeI+prjGwJiD8e3Xsynu3PbGKJAOpOxtR1hT/3mpBcx1Rz
Fkz0QBh4wtE8fiziv1i+xi8T6cqC8ClamjrpZ//sn6dh7NvwSYik14MlwVuei4DZoZJZF63aoPUn
RXkQ13wtK+MkYKBcPVSZMFZmaCU6jMMBYclXzvRG1JqqZoa7mWFTeFZePUTXG7Wo12QaZ8GUi0AV
UIshoN25yn5e2Xr3FyuEtm5AvsZb+iLsgLeHBtKBnsVaHQphicgqwgwv6MQQF6ZNBgU/aACfibDS
3+n/mMMm8k1cj2bW6VCi7a+c8LmCf81NlJuLww==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ehl0CusS7+JNGq6HfhyaBMy68nccIdIGqixoEztEZfkCpXuUYsdqw6G9MIJdWdu0Ck2acV7K6IVg
rzb8/bNaDDVWp48kupToegTkOdwDkCejEqppido4BkJ+iEkjPniz+aJHlOlOwmauETy2hCMuuC57
oWDprzGWlsqbCjqzKrXmPYm6fNdcOa2DiOYstQaMFNbPU2ccrbLJAiYMHNDqtPNqWxKBsD67kiGf
2eOneDOmdmy7YkNsL+cx8MJc3BVUsYBrpAEsGyFMkmX8a8nYz8R/wlFQFGQAd/t5XrfxFNI58mj1
AHXbcAMhGKVq9YdKeU/vSXY/NwMqp12xJ1nUaw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
h/qRAwiPuqY/Zg/QWqbaYm8xWTi9SshYuPzyL0UME9ZDDF+C2CyGAugh9HzMdD0kZmT94TKmBgLR
dKP28nlE8VCCU5rvbjKxfn/wNtNKHCvZ1hns8CF7+pGuelhxGvXNmYKFw5co8+4grYFaDXeoZZR6
S5sjvhqtSVD3+qq4vYWRjT2Y/yes7L9dRsLq2D3iZ4xjgVHuIbOQLT/EUKW+9iYudT9Uy6YTwB+5
mSb0QK3YfZdGwZyXB4S3mdF9vNQHdW/rnACq3yngF+lprNkh3ooQKdGqtxtz8KSQxNZOAFE+koOw
h00o7AKpvDAp3uNguLvnNJH3rugOhh95b8Jatw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TsA04vIYHDZne2CBj5bWCBFH4MtNoFDCn/3DNEi0BwutuUf+X+lD9kAO3kl352WHjQbF79Ssm+PT
fCYpODgWdxSVbzaHFpITxCQ4HcIJhUeW5PC5tw09Tand68D6eg84qRguH+llbb5jdGJkJeTCf+Mx
pupkkLiDvNyTYWe+nqw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rx9hgQkvaJJTJJcTjGFW1DrrWiT+xanrcMvFn0Z3KRXlZvf+SE7IQgGCiP7ZDA6T5z1Zv5nzS4h5
cVi+CvwC9UMZRWmLDAjzASJ2nx1g9BjbYe2vHAUmyurIiR6LSigTeM/9TlMv+fFwJbqwuH6FJ3/z
Vl4tIMk4NrqkMn/riOG87SjhesepM6kcQOBgDGzLTG14z3qeZG8OPzxgApfyubmX4qdD1oTgGm2u
Q4mQfFxEye6Jqkn4Rzjhifs/ieNYomHlK7R2/72QJj5j0WyYBIhvO+09izz299Z54ZP2ZXaRMfDT
lU4lQNqQU14PX9Yk9p7sy2PnK4vTwwF0CFIgSQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
e4pBbT429WBnBAaES0v6UTRPBH0PLI4C3mX4zI65p/b725o3EdSyAQInL8adAZSSYoyPq3CKPuh7
DTMNr3nMuEjOokJxyPxqqsKs2J4gMTkUXAPWZ9sLiaouPV55RUNUEpJAp4RO753TsMcj4RRT27oQ
9T6Q/7P+0n0x4yNiNxZ2b4QFiWkIJA+6nfSMS2PpjZmdP6MKCl4Q0gfg284MvX+PzCIs7NNBk+qU
GU+GscedBBeHi4S+nTAPZ4gtBN68XYiI5bcWBxKv+uzKHKNUPF3bKh6FK9sVdMqfRdEpcPqjfcUe
vw+JvND9R/iqumGvNm+fsMFPiUz8oh4HI+Qmyg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Krm631/eIRbiIPo/EYBp03YuIbo53+qkNLvLyyf10YNoe1nY/PM4VfmaLPBZfWVeSsyyvcCyaVmO
32j9c9jOsGKP1rx2aErCB8sxtHnY8p/685EkJjoV1jbjgS7IlDPq5DfgoYmDsmFoZK0rWxJZiDkx
2UzLpvtPnlw3SIVQ2Acp147kn8XlCpbScY5K+uJwonYY9i2ylF/NfacvncqGLHvgqVpmSfRiHTjL
/S7F6Rmm9Y+por2D3YX66rcDKA+AMAta+lXEQIcGYv+NKJDPrW0WwI80C7agZKpholk8zmJl+ZJK
8VhDAl7vpvjH3AsLyvn6UrLiJdhd+6ND8Zuu/A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10752)
`protect data_block
8m67guETwdOfqfktYbrkcQQITnVVA7y64LVqPUaDn0lxcmIF41przDGJ73z/OynzgzvCqMT8ylFA
rPzkYeeuKlKj46M9pcukBwm3oVZTyZ/wG+ZGD5164f8NJF6DP3apesQpInAwUcoD4D93iLXC0OOZ
1wtp27Sk4zF6Zb9MGG9OYZu0aXcm8O1LtZmEo05PMxqoLjPgLtrlUOB5MRgQ+cqNWGf+2iUg66WI
TeyVTXzAOtxl4ythOB041yDzuy8eEwYs03/WzxxZ7VRvAKK2wE5wiaEzrdAyieh/rWS9oudik3xH
JKDL8ZRHbzLlJbxmnjsGXlPyCtUR8oxzpWqEbAvGcaGfs9Ouy/syqWpiZ/xGgC1q41jVU+/+0rCX
LNSpKOEuIfzdqweFsnuRAvVwf7lSzk0bueKn5ls4d1YmDB0DjMEGXWL80GO/+P5xU2xhC/Fbo3pS
ujZF4YTcufW0O7yDajfLWTZdxHjpTGET6A/Lq1I9v2T3mp789ITjatNbbvzYJsBFutdPB5+rOI6m
Cd3JzBjBS7MdGdvRA/XLfL0Zul9Y8hIXpDJkd51Qw+S5/bvII1daazsyQgpGVMwHaDjVrfjXOTTh
yRys6AlgFCt9qLky9A2wmCmzaqYaudB1e/JAzpfCGdL9erKLMmYQF/XCE4zfmvaf7B7Z1PNj9CLV
aAQxmz5ann2Hf2Glf18bTyA+CoawAbPjvF+cy4VdU79t08+2XSQfBmKyKFsMVuOseMsQE34d94ja
RPOwU8OzrthyVGkoIooTKTpn/ISLuojPBFavCSDBOW2nN8FnQBWKBa8Lfa5EfqXotzkeGeGw56qX
t536A6i/mu26SEXJoycLVEZLPDvbQNQ8QxYEfVS2bswdTyVKHHJC6iGmU2qLeF7jkFSD0/+29WiV
iMU2L7SnoMYH1ivPJLv4KbGPvSbdLVMVmjbgQUyl0FE8H22F+qiLQEFRjyar13evxzzpLN4Dh4ou
RfTodh+Khkgux3pMisdaP0Wuv3TU3nG8h/2z6Lf9W1W5rOR74nDAC5ugCG+gpCPwKAiBTc4H3AgO
X1rUlKMMcW1UAs28AnAdyoCHW66pZMgeYZgkY97fP5Y/hkd3IIhl109fNKtzkNfteVLpsbwD8YcQ
sWV0rQ/h4a9rUYZ85VP7eTyGgHxxVhTJPHtaUteR1uX6BaN3pcFIQGTSZ7O0qkDu3Otn2eafUxSO
88NM29j0LkDKIlPOghssQPzaiMyYLiew7WIecRlTArHRNURhAIXKIiaYQPnjMOF5A4oWbKYqy0Pd
FWjjfzXRK3c1A0O7yNx5P/FjcQcZARv8ukSQA3eVAI7FOULeAuwK0Y0xcnF6RcALHeHBTT6bfIJX
itIlelyTID74lkqFYc4zaxnP4+HOJXnGq7GjQQmEpcin/Tdz2fgh3za2nN33uxefkS98PD1Fq/Cy
V8ZwuEJZHEHmRFOelLhkgWOjEJIpF/yHx/dBRrEY3CU/GgtQsJ6jtFnVxWQmaSWx8Mr1hKCqGDIe
1oWi28GNYQpInvXvUgmUcwv8ZIfKRHhkdAF4luljeXAA05w+kwV8DSnOD+wbVA7N/x6BK6rf9RET
WVWx4ygBYqcRuOOXuZKoBpimKK4ke8AnUnmdlMlX+I1twpTM2wgsNF6PJS4EuggFZQtaw/W7/nAJ
VHux5tq3kKtIxSxfSfjCIp9Bw0dGcqawtBMCEDvTsYfNft0revPVEC3dqPk4wVm+9ZLah1B8YIpC
4JF1qfWJ++7kJSaaX++ILRrr9xdiu4hmPZgMCrDKSiaiz4Bmgly/lWbXkb+xcXzjBPltMo5an+fX
SPT8J50LqEs0O3o0oRVl66FH+xnlOSpsaxgn0/hJ850iUyGkg5xTL8Z4e4tBIUJFwLjeQin7rxTN
jhihWkMvDQ8mh/P38z312wXo/4wAW/tY7b9lkyvvRLODjLQ/O6wSuzmOlm/kyT5+RZGb+tP7sDL2
6bREAcFcTNwurBEOyxtNwYOJkPU2C3ogfZKMvzF5KHieFb4xc8h7dXLTE91/MPNZM672bFVfNgJj
dlHSw9FoCeycy0eRXEy727WXZb3TqEMcr5RsYTbMTudgl2eG+gDRmO6IlZmXTQvttgQhQTsgjB61
3C9X8hSJg523xbbBqtqta+mGrk1zVFeDqssJp24uhBqMwgKKoWMdjhhd7EzyxswRRumgGGFYiaCP
a6am9CKQEDMEOTAze6PlfPYH8RPaF17PafJUPFNoqg3ikjDXGyF6IIyVwXAChu1BMaW24mNLwNk3
AWrSQclXxwArIJMSfpK2OuU/53F/5nnjW15aCAH2Ev/61iHIKwxzKHpCAAverOUzFednnnhwenuD
qY2XE3HQb366gaQkTRmvzmZIViMSTZmYT7fzLfp5EBrO5PfVSKX+F6CyZVEU1lSQwDElgnXFyZ4L
XpAosFbiYk5PEewnGfxu2Ccx2cF2CM3CDgUh+B7atPbF7rQdvjhZijbHRHpJm3/jD0dw/vqhG2lF
/nQxPEYImirkzprZLVtNG5apTEx+8jFUqIxBdR7qFLanL6EvTS4m6UcvUS1lxChAY0y8RxTKTOxw
6eW0AKk6bfeEs1WC34M4xbOMlMjSrzxVXkBy28r1A967XOLCRuicb1Me+mQsw8Kafh3fVPWcc9Ki
vthUQb77h6uj5LMmfuICh1Oq7DXgMOap57vdICTCeqg9INBLyGFX3N839ZJvPtIAEIeufg+/v+5S
PQvYFr2pYCUBkN4jlrwbsasOL8RBC/cA2vhuNs8sCi5u3WK4LKm8+jdcJSr6HyPecc0h74CzIsgi
p/c6j+8IplLyZcoYndk53IWJYbJ3N+kJs86wTxMyZ0AojCVgBFq1dxXSOct6cEwH4jg2c52hlfth
c9cCXzRknj9mG8NowOJX6xZ5wQVKZiAzK032I8oNbTOxbHp2MzoXCy9cEj9l9EtPmdU2q7OVdHA5
N1vzb1LIwPiKrsAtOaUohZeQ4UFCOClpPcX6V2IjkAeu43jcYF01TVeamcNafHQM9dcANv+z7tEY
a65tP7nL5c8GIsZLxFIj33w+eImoqRUJXDDBZ/EbDKBkJ2BNOPGYT+TcYYbl7sjoFbaydQjtyYWP
vJ0BYaOMDAzyh1CvIFXUSL2Wued6rYpB9RrekwFI443RvnfP6Hg3GAYnz3UYmLmD2OAlpmMlILOZ
lh8a5ZewaIF9Xj7MTaGbbdjWy2jHMEUOEWbMNZz1hK1vnurbSodXs0tlj1oPUiePJ5fqZiwQsSQy
M59JF/6CslDqoSQh5Ioox60ldakn8dsrOvUTNRaG4evIyJw3/oo3Br2Kom3fCr4zGPocwGiuwEjr
IbR4jqWSEpKevYS2uZNx2+IJJuX98oKshBgT8le/lC8cUw1dNt4jwfMhPqrnjOoP09bIwkqaoCmj
Z2+0fhkUve5YusBiTlSZrPihWXF+vJov7ox7o3Eh9GSpoXWepBDmZUlGzqfCEukZuok9qu+8C1Gn
tH6FIx0tR1dcUgzaw73rdBQOyf/Bvunn3Rf+nMxilGBfGMZCralEjGK5KyJZ2AMnltXPYdlPN96v
gC8mHdP7zH2u7m1TaX5/L/oEI2j6S0JK82VGsWCV1RBP+6mqxtf761H+1zG5S8mSGuZqLSAlT7hL
jvlJW3dPGW+szJEZXoH01C30vYv0OK3Yucwdqb6wD8R/i2S02zqXPOU7nmIOjly6ktPBz11cTNPv
IY4qfNp6K5qsASz3Zm24Qf5O1m4n61W/5fmthRllNqIVba/qztEy6V0/p5jrRzyslB+Ou+b4tSSi
xJG41PqqdnQxdblMsuAedVfUQVdcgtQKQAOzb/AMQIaZ/5jK5Xfu741GF/e/wtXxqETw6vXtOls6
w6wy2xy3xiV4uLbcmBOkTFPGqVKMAXKlwPPT/FPbT+XRivZMWYg8Lv3pm0p3YZ6AFT/eMuRZUgyk
zxon6GQznsehwYPYJd6WOHaPaV+G8xm3zlq7jS7NtOyEhW7U1xJV3Ga28fT+lqGyy2+8wGzD1aQ4
n/Zw9wFED/UxlfpItC6DycdEos7ZXDflq9BQNIwS1aaBJsG0DJbpInwDs/KnkKYFAj7cNiLKR3Dt
vfw5pYnx1WVHOyw+K16vHpFbVE+LBWpny/99FPDfAUNEXpX9N8gd+sb/w5JwkkIiA5JBV5Mae+Fp
O/eN0AlCy/wm3TB+V7AuPxQXgvw+HN1GZqLGFTpotbJ+AR2MY/G+EQOa/3MC9Yptxp1uI+2+Ee1d
V0JFMaiM2ux40wsnbI6X/l35yAt3y9Ch9fQQ4419TSJPfbIvp7/Uylg2Jgu8apWIPWRsPk0JOStR
sxBWGsKZk5lfU7oguLI0bpr9nq5lHwEUSVYn7mc+79jWchH/m4h3RhcTvauyvLOntQKtDs19sQhK
KvFNLpxoqD5FaWF85thpfHV7Lk48EXawyaAOQRKPQ02L5vYJGvZWRmex8/stzsTwd9GX1jt9KG9n
XsMFBnAMrUerCwvTZriOOKyRzkXEb9MW6RSX8QQygQxjW2IdWYowpkWi6HWfOZg0NZR/OoCgcfIN
NvR3SRom+S48ezFF0HFZtdv0UjblUblChVwafnxQSZlhzkt66MdsTpRzESV5Z+9NHl5i1pcJNYLQ
P5tb3VNif8YhACPAK5lqCJOShDInH5P6A657JPg5HyXk1qVoeZmStrKUcZELVba1O4srmWDoaLk4
+O/TIqN6E7W8A4DGrXtxRLMNhiXFOXg2k5hKmblIQWJnfOUfe+LEGobtVeCL2rrwWx5Rwkej1Tlt
phMv2SlJhBWAot0fJ3jx2G2/54vIoUowkOLLZQ96KXzXOaSDcypukTnaSevIOvaXxdU6IaRjR1Hd
wNZxukBU/BZQqWHNIRsdavl4Q+GpFxUPE8T/zTJd7IINNipjQiHrPIcNh78N5PMclfCYG9kXN52e
tJ0yfPi7eDiyCNmRiOcrN4rbcqipOv6QEnFBdzAUZx1ysreStmK0OWxK/6ia/zkAta66eGsPL9kn
ECOfD1OlBrOHXMkrVtjSVFE85Tmj67sNVnKfcp80bU1MyvgWGJObYrl+VKu6xVyiu9uWUP3ztWvu
pSlUqdvq2Z5UyM4oJVVedlV8oe43LNiHUF1hkFXw/AqZ06s5ji4IhJYtM2gMzeLbAxPoJzAHtZ83
cnUMR3mU1mqG2UXgziQf6aMm1SNlOiq10lfVxhEvI3Pa9dFsLf0sRk2euclDFTctb5VxUrmlB2dz
eBdWBdFSD83rtAqIxV6wBf7MOlfiAHNa38yzsGUH0N6SO7oFTv5vgedR0dR/rcIczYPtRzXSL0r1
BZyprlYzmZws6Cuz061lU5+0kwGNzlp76wE014She3J6gnY0YFF7OP3WEqjooEMzM4MGsLLicWgw
u0oIpz+SIUE+WtdSEU7DaY0cklDGmEN100QBFb1pAtYrMCfVcQbWVrh00n2lPb+/sQ1CuF0VcKCr
miAE7uektVPRiNswqXG6AKzSCciODJc/rlddTSJeoDC+tB8os6paj6x0UmEFVI8v+9HK0vTnmls9
By+sufijaC+8wEObVIR1VyRUDLWyAQbeRPqWhFhKqdgN8u8Q9lZC1LYZU6Kanw8XvA7b7CeBI61q
yayOLHJ6M95AuKGNy3vfQWXbMMBkOqlteHfw/UttE6PO5BMh5hxfXLRPJQgW5wZ8z8a0038630zw
ibKFWE0ezA4lu9nPhrJs85E8CV3LULc0OIcat1UBN0shTx6a8nE8mCyirmf2Kdb+XFUbfyHRlTIr
LWz7glvj10cLjDeMIsiVoDk6Y5wISef+7EbItGft7ram6sGCFU40BddK8spZz1yf6RYeEteppWPp
bhrGz/n0ahrcEikaDRiPVkuOyoSOUH5uftd0frqEVnd0pFfiMX+0Bv78hzNbbKyRP7gJkVqoJZ6Q
fz9C/AOH2w6rA6hMz1OHWVw2A01C7UDMXXxlJfoG3I/15W4Le79u1mfFWq1Gl165j1XSa0Xdem1z
GgzFVXsGT0xhusgi9R8Z73hdN00VdFzPIKxyS90kvZEPcoqXqt/P6a6n+CLLD/3PTOiH4NqkIHNP
YfLhVyJsbjm343iGms+XzvXxHr7FMOOjdaWEG0oj6ITidy6NfbNpGIvegDw2vcXP/KX7/ulXayAt
2nEXlrvUiVaOwdJYdHAtpmWnoLL86ziyqSYNkJCfW0SAEChP4Wm/+fSpKhQWv6+9GdF9kM3o81KG
U1Is7yX9ngLbCljr/qhu2yiUd4/zehb3oKSto/oPxH6NPtDNOT66aI+Ttk3/CgSoQ7UhjsPMJRc1
jyub+zNLUYSve0I4S+vAjEK7b52GJdaWx49TbXcnN3ejWtbf6fTDNx+xVhaT+7AhV+mPHYVSlm4S
aDhcLwcpzEe2+hzq7y9716cCsDMZqL2K+AvoKFmRFStJajoX6Id6qLeQy4pO7JfwwXVaW0JVY5de
6OIiA0z+O7ZsjZ6TwDRe7UCV/p/3XFM0I3s6SpaZErVRy3qas7cuEzbrXeGVB3ci4aor+UaWRrbl
DkWLCeu2bTVlESE3ad7rHcvA0dseIvnlf7yjpHFsGT7b450N+wqkJBHYis1kbK8v2KY9fvZIXIJY
tdD2H5bPksUfX484H06EqoV7v9CZqVr24mtBwzZ2d5HhdqOIeP3Fa0+r5cnGrJRKMfyFm/rZlbFX
+uIkyBscu9p4YTtGSyPtQbovZmkkwtoS/auZJJEFpWrKAn3pRymYlPYdaDm4tB7sHfFKLASwzAbH
jE+LV3K+Kag6LiQy1BtXSIOmGu8QJUMOlvER2f8r+fBwRm4pAgrPaDgxpigp9kOWUQwCvo+KSf7X
OSYeIem5IELTyzq7JWIDEKb5gWlet2NgCbXv86itGr/l4yladEfkvvoIp14zsO9DCc1FhmSrGJIR
rb0ZGSCr3caBq2EP1n5FSCeH/WnTI2r8KiTaEQ5V9dBT5+mqrRqCNNY+Mrph5M1wXw6LPtbYlbwg
z0fJ8d90MkJE7Pb4hdfR85vFX1hPIDQdQyftd8r2XM0EtUeDE/H6AZYfL4WwdYDYvNns8nhCbW5V
FZqSr+Dx0mGm5znenDwErw4Gv+A6Qw2R51u8oMXIL85P0hK4fohxD/wvjs9Pdfq/su5fm2IYyGgn
sDztr1i04UvewovPiFd41gZ4Q+Aw2lkaaN9f/qnU32xOgSDbAcXNZqcbRy8WirvkTaes+Xw3+LRK
rMyf0Ph23KYDuCHhsOFvNCu4t1ZCH+WpPJhXwP1nfFNRTCwshEen5CfgQrqj6S+5z+8r59gL9zIj
WRvoBuaPDz48GurSuyh8DjqqE68xq911ZmJsNKX9WDUafKRhnSNCOepIixERIXE56jQb2Oa0CquH
KdQxqH0td3SAqBb4uyH2va47wBUmB4KT+vbYC6rXmWtrDH63ZxZaIkg+3nVjRti9IdHt3bAey1it
K0UB7hB/AghfRHJ/kps4ZC16jBhYBnOLqTCnTHdaLKsf/Gag9Q9P+Ijw0pMOh5zYweBmeVksN2kI
PoJLbD4Ud7qqQNHm6R1geRN9rRiQGDvxPt9eNzY+uM8Z7NERCNHFy+VWM7c4qeedhY9Iu1BbrnJU
0WOmGZ1dUYf35wR0nxgS7IV8ETTpQjH41XN0QvFVEpaSlS+8WML6ZolE7E48+d46HDPpRjk+a+2k
+BgULBn+exGRoMH+6Hme8vEt/J+8JXZjZLGLuiygs+1C08/LF/wW40asXXe97HwdnQrVSzq2CGRP
PSJLFKm8LRJpp/h80jO0M/8vN3jKDXNbAI7CMl6htCsJbQ8fQMbCkSRvD9oi/5yGHoliTOf94jPt
2kwX8Ah94ov6WcEAIcOX+DbEPffIW4847y/mQ9wfTmJFrkfQsq+nyVD/c3lqQvFnpBuI1SHfFyzA
LG5N8MIeOL5HKYcn08UFE91cez0TmBKsijPT2mrl+j3Nfgh1Yf7gHuymLSsQT5IwKphisAZMTiz1
ppuu1zOksAVO+9UhU0g/yH3tXdo4OEoV1uBPd9fnwTZqn82RmkM8MPjTgs25I7Zl7+DVpVSyRoSK
ZzqVFFQfdnv2IA/Tdz1njIQorKL7BTOrVHlg8iwcMnAYs4AQDqMNIKitjTBQ2zv7cG/nkOBT/QeX
XfKkBxNwwsAYl/qFnz+FqovGu1Nz8aRCPip4RZkyP5zpiUqaEqgZwN7eRyR149bxFeINURfvAlBY
oOXImw0O90boZ1so2Iip34LctxFMkmNjM0zCbEn0k0Tj7XPuDK/b3KFV+wt8WfEPuPYAPKd8DKZJ
l9j+YCBsIIg1zcBJRoETVg6LoBfs73sad2Fg5eCguZHAMOaSasRFpA96mCoCg92wk389tNHCykLd
pGaXWo9UHMHoPT0ZL/riOWBx8SieMtCJQIKY+mA2Dji73vaBm4JAzOIo9EN32ZoUoJpA/dtCE5hH
72jioqLvoYCsgdhjk2J381pAXz/f28aPzBZPdZbD6Tt+Dv6LfJz4+ERwD5g7syDKAlWp4v/UXX2J
xpIB9SM2JwPxpjx88zEzdabYOr29CYHeVBWem2WWaoyU/avmk9I5GvzTUcM5P82nb+5VSRBU9GUj
c7kOPrzABtYQlIfbNHtWJOmmBMtevWYVmBZRe3FZ2BZ6r1iYJkBRgyxHX7Z0UYB4WrMDkKEIomSL
ok/MRSzmUu+dnzznF1ioalGrJdiBgVE9wn7T5fvsm2g6HVG0FmfWU20mCzez7LvOLOsOzn1PZWMf
ETD+MSx+hnoOQZcnB73Q8sReCVSLbg8I60NfIRNLaGNtSVr/MJIoMW7XUzpcB5nns+Hh9kieVEoW
Uave8QnKjt2qA00GbMt5hUj2oOrs/h3+oRWOmrOCyGeMXcQYgjN3RlTY/ua1etXzGQc7ILp1vZmB
B9pTHG0YC6W5dXJ+gTZMtPdCvX8Irdj3QcCVwtbhfUeH6LnDDINmvMPQIgeoq2l2lXBAWRM1raP3
C5WI0nho4YWsHCCqq8tMvGojRC7FRK0qaEFSyGzlg9Fk5cf9sKIQMANlQpD+3z4fBvBwrCp2ZHJP
tCLIL/VCZ1iToNvJAQUw5ukUeI6qKUWDW3ENm++Qoe63k9JxZB7IZXoqbqLcOasmUIjiTUPhBrgh
oW4I75XL/06X13reChB1oGGGL1VJQwd5lUciO0vdO7YBFMjUWHeXTTrqqAkCJWqnNWpp5e7APDZU
401EKk2oLyztQ4Xv2//C9A1ETMKdY9E2I2aJ37xGci+JN+sx0ZBNAPN37/UZEwyEAH5pjKbWYOrc
XBM1HFeBuD1K+1pq1zgYgtX7T42HkRm3XCqXJ4LG4U5W9wRWVC3nc3Si8udMuhV7nBeokAwJU6Ap
pWix4k8+OYeKPzzqCYvCKRhV/kj6pZIvshrTrYz9Mmlh2wHdYAoM6H9bd++hhmf8DD3dAqp/omlK
TB1kOWBju0/ThRzfFuWZhXWNjHYUVs4r1vM6vYv2mXQDhU6WYoSCIQ7VGK6Gvp6MpdwiKbS2kqEI
Z4flBoCMlIWGAOjCB3Jpm3rCCXSuEilT/K6qRUs8TICmnjtwlapcqp08t5wnjjn5Mr3SjzxFmtdF
YuEh2e3b8jBcKbTZuMd91fkP2v6UW/8U4N2ndLskaOqAGiv96uldcD0rFgTP2495JkHnWO3UOvqQ
jmiKFn2V3BCKJAlynoyenTOlDwliAW8Oh6I/MHBVkVqtTedS+OXluOkCGB2KW+2vr3qPM73MC2sq
Q/yUPREBi21L8y7HpgjbxzQLjkS2WrxxwKJy27p+fDF1VVKYIdl40uOwVt+DZJ/tCiXy91mgP4Cx
OpGNzSTcb+oLXHmMnDbA3JhwCJQaK1zoZFc7orECqJjDxb6hD1n0ljRdc5XkI9Jj9XV1nLHUIfEX
8+2TC7tMKPWaK/UuUVjth+Q3z1HdVCVcnSxRchFfVkmG1eFqz4VHOpyY3JlV/ZPG/3hS4sVYyIjI
UKKz4HotuyDSGMzN+8L1SEBx1tB+ylODyoug9npYk4yEwF90DRn5vehmmuFTHUwiAPv/IU0vgeZu
OF5kKogEA3q2op6ur/zWZZrXfiJzBXMTWvbpyZjATbf3xoqh8rYeAdBp0agRNv7UEBGyE0wGk6wf
h+D8wgLLVPzbn+cqrHgmTus7ivgOFl/9vJmrNOmuq6Pk05Hn6CDRwBzNngK49qHbuadyeP+9k8s9
3fEqqnIfJguVKCjz7C95Oe1Crf4gRx140ioUu26li2QrjBK/1jgkXUF3FBYB1G+KOtYVr50Doz/O
7BHtcQceqLQVrF29wp4xnIGnfjL4pmr8aTl+PGg4Ke6NZxUgkiJgd9Kjev9nkDpROtdidlRvBHFe
Cg3L6h/va8RPr3zKyMu64I3fX62A08fd/CKvKhqNVRaXDkGs2XExnTONx1wsrL8oAl90Yseyyh6E
GHEpEwVj7N7JrgLlRBqPW9ENNAVuxZvDaCo9eIm2j1G5RCUSogMi9yUKO5sOTKFInjldATlBX10P
+gl2wppbVnNUlpdehJwEkiV8nqmE3JjCK9kKPupzG0KR0PQ1cg6W+7LT5mj+tstST61+MLF9QnRW
2rh++coSIyp1uTOTsbdoHoYxsWw4L0SQcAD8Jz/RWPe8pt921hMy71f3HutU5rG95lLfv7rhEJZO
UL6MpnEqngU39mwPKtilw71Al29ukrxrf/PlIamCQy+VlYqyNBngJbJ7rguXpBvTJ1yLsfd12/uV
iHpd3XHHRRDvv6hhYeeiepA8jKa5avb74KZCWOPff/Q6KKoYgSM31kao659CsZjbW4nnuFl1RM6W
UhtyiGXSeSA1Jw0w+p5QhGUCpe4sRKgRpKltQGJChMh/573fN+oIlo+wSh9dLHuEGWL6A0rGdVf7
d3n8fjD7QWGP56XuYcPyZq3sqwfDdmwHdDqeJL/NG+H/k/3Lcdl14AYPN/v1nH9tVTvkZHZHxfWo
I+HvE9uoLpTNcnJIiwai1/SOVT/Myd3MlArMHMHgJIS7Z109SQOIjtvXvL/+S06rDlvaon3of+Ot
gAyKhYKIuu4dPMxMo/a2CTxdTaCXy48Hz2dLFwf7109rfNyefM3Dym1qApk7cfNihQy8wL2R3m8H
QAiH7T2OeaFOIF+WEYc0ki6dLo3842cF7/PYqwsRYg8d/UKHByBm6SFSoXMCNMjdoCkeHtknTTy/
NInlB8H4UoBhyB/ZC3KBYBnzajNi61JCoEu9ZiAEVaCMcLfFY/pexs+kaeK0yYcoDtbNptItEzW8
rETfSqbszfn6/6m9SNBL+3T9fzVJ9NzYe0AHFpKC4T+2h0GOxBTSxM9ZhiYA8pPoXDWS4TqDwQXY
23AlWh4Mzqy14ZvF4kH6QV4x5uG1l1quqOPQEcZvvHjibTeqerICl4FOuZYlJSGuidyIHnHsPqOv
6SpwwLGe6g2y4auEIZOqSJNmIn3Nu7sPY+cat04kIkQA1gwHEZ7Vs/fcA5yGLlCKjZPEH6rXTeJ3
pxLwxdSBVz3YB0jOOb6Tix6d18gZXAiRiUmWuxCUWtA5T7UCZeAd9wayYQFkZ1dnsH8aDS+HPP20
J5ge4RpeYVITzcPy+biQcGj+Xj8pX/TiFS3RmBpwjR1rEpm2LMX/6xi643Gm+kt2vWAROKT/5Kay
Q5ZVDKTMEjQ9A361GE3uTkJKaslrsyQsNzt2cItvdOyZIXzRo3VtZa/TNsU1Lx9dceR71LUz7/rP
0xOwvYBTfsPMt2Rfyg3gm8j31l6A4hi4ACwhtisRyQtTTL1SMe39awIYwvhAh7QL7Tm/ATh7iSUB
mG10JTUNpi3EXGQgd/4GyWyGvEQEybfuxkY86iarP9R6SMbxG8Lb6ccx13g86WXYqWrRNzuvN66m
CZ+s2ME5KBXp2GLTdMdsnD+x70AAZZs1CQPozR/fY5mclOduXAuYqskG1Jm9mDMW4C9JzevbPYWK
ygGSD8CPGNZN3p1xQYxSTubo70+wxNwMxgIjD81/peAzlOR2ESRXd3LN+Nto9bHMDZR405JRV7TV
kQRst6X8BrIPsx5HoNNbc2ldV7X6L1K8SpjwuzKr/4WVu9wt414Dc7Rxd1s6gzaUZm3pkKaJ7Y01
Wh2Uu6GKxiJioCjwS6a0MGvMV+sgsv7JW/8iuM367QqjfJ0sleIdzoUuWdtkuR2RTqLNuKXxLQKK
KbGp9enGWwyHI6FJ6BAck4KQup94iL5v6KZOKS/bbbu//vSOnHBYV0eUwAEW9zFDyYaSqZ4eI6E5
VyfwD8kq1ZBpxLol76h6Rb7sbhwLaiqi2xdcqZZ15DgePl8pgJH6suwSk8CCCzymYNmOhJwixHAJ
Pp55IItAdNg1pP606LWuiTm1eNw+XGnJAha2PlcLTwHwqJaPt2EFhaiu7VUwdzMNwF+NjwAWnfar
8I09q3CZswe/JIds41THW1KCVkgz4IyNXt3OJrUcNyf7kF035t7YVnHxa652Z1NDXYajVV4+ezcL
MLnOzCv6Z772CrJMURQq11mzNHMjOF6rno5zrgLWVjnngu/thXTBt9ZvKg7UMzpOrhN+ZeYhVUU9
ZG3O8ZUUPFwSDrJr+abveywAPR53E9cTr1/bxDBFcCTDg3qMqQ7qsT7A7XIlJshNhEnpYJfz7Iss
iw4FhpsXGtmRPjaOkLyBNv9AfzM8ZmNLlzOSiKAv2skl3dwElt+bryywbj+QBXZe8FMntK5vv6CE
mtgmh354C3sGLarlm/5DD6OnDK/fovVOW6UXiXy2jMHBT6UEeQpHHQI94ZjBwVv88wvHZxXG7eR8
Ic5foOyRx5n/+1EbXPyvd/hjxqdz5TOmW3FP+SoauFAvkiB1c2v6h8PVVRRDY844vfYzjAHZTgh1
yQd46AjOcyI5OZhc47p5UYMCT7yLYOCQI98AlYo9OoXIVmQtqSG8XlN03YZU13dL54Y/JYuKb+yt
B+aeWpTXtBHZnVbJnt12R1o2lsyxIUgVrIkoe/NF2FRt70pzNXgBfiy4TC0leojbJ9g92wLogYwD
v9U6DDUlcHcpkPx/pTEHdfXRuPsmvsd+cO6hHEUOSJMtwbHj4PSQOsW8/vJcOjzpVW+OvF4EvOWb
cUOMmh7vIih7QJ9AwqP9SHAGER2CcMM+Zl2bdjRaQ1nWgSoGN0yKu/K9wLjL6mW4i9nS5LqE7m8h
pcphbL1xjsDmkJrAgizdGd7YN+IBAG6jua+BnLB3u5VQzelUCIK3aBK/fpE1w/Bt7hC2YrjKuEDH
MgZGv2W+RHhFXgocyaJHTHFwrVcAY6Qfmt70Htk6wE9w9aUm7o2DSUl8lYDQmwdNv5nueBZA/VHf
1MQmdCLrKzpGmODYwe+OYGTprGt1BMKGn4OHzX+3U3/CH+k679bK/fvzZe7Ce/euLgRM6/4HAY3M
6XhSl/0uLkGHLU8S9etuA8ibZh1b91pjTxfDlKNdrMc4rbE5O09Y6IGvNGAzEq+ByEE+vTUc0wCv
OKlJaX84EJzmKSVXc2iM1wyIeYJtR1vsNNz2GabwcWloCKXWWOFHtpnf11uJBqQPaJP1JjTw5FTU
gTKpN6pcLdle7aijePLL0ZVkw1ahheEzVDT9C9GwrFLjcxLLIM9LiuCsttJZAN/Hmbmc7yD/7Om4
V/0cq0uVfVgl/PlsQimEPmSbq/szFMt8szwgItk3UQndoENjLscAtUzcEFth0C1jO9YN7hdPTBJ3
xCdDpVc2ShjFr5f6KBlaKp7CZrIQlMGEqZueCQwjtuEk07bpLoAQntzDiev2nj2JQzHvU4sJnzSS
sEi020r5lfzpnNEKMz6rZACED9WVU/Uo6GTXnve1a+KwauNkbZJdSgE2RMa8ms2INMA4R7PLnBPe
1DjtUnrDN1CYyE6ckkbfcHl0w4L1IaS2GU/bYuvHlnV/zJJUpad5ov+pWYDi4LHEZeP60SH08IZE
HZfkIugQEdz2u1sJ5pFMYqv4b2UjV7WHYi92pQAnO5fyRyF7Q+kF0tajTC64C136/Gr9wldlgWva
83PhsqxpDuEdsxnPz4sBC5G45s0/I7ZpwHQ0RbdHWAba46ELPQn8AXvX7m6iqA9gEfB9rqSytg5q
6ilvMw62QOXDdrsTboxWVSK+FtNfNgHUW7kgt0G31hUMLMh6dR9+IWeLtaTOR165cyV46ij1sYRM
nIV1/eQugOH2yKvnNssuB7rF3uP9SzYmBMDPs1iSTIPaQcKTIN5H2hGlnh+uJbUllGssFuDMEAbA
bhmbvRmS4N7sGkCD9g4lzy0EyUi3eSWbu1ut4Upwbw1M4TOr0aQhw+EHclWluQdnOCdjjED7RUR6
mHbLxchvu4gtPRLJHrbGlj+2sRBBIMIDGPEOnunnFR3H4d2N
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xlAsynRegister is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xlAsynRegister : entity is "ip_scope_xlAsynRegister";
end design_1_ip_scope_0_0_ip_scope_xlAsynRegister;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xlAsynRegister is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_10\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0)
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_11\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_12\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_13\
     port map (
      i(15 downto 0) => d3_net(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\ : entity is "ip_scope_xlAsynRegister";
end \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\ is
  signal d1_net : STD_LOGIC;
  signal d2_net : STD_LOGIC;
  signal d3_net : STD_LOGIC;
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_40\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net => d1_net,
      r5_enable(0) => r5_enable(0)
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_41\
     port map (
      d1_net => d1_net,
      d2_net => d2_net,
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_42\
     port map (
      d2_net => d2_net,
      d3_net => d3_net,
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0_43\
     port map (
      d3_net => d3_net,
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\ : entity is "ip_scope_xlAsynRegister";
end \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\ is
  signal d1_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 10 downto 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => d1_net(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0)
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_34\
     port map (
      i(10 downto 0) => d1_net(10 downto 0),
      o(10 downto 0) => d2_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_35\
     port map (
      i(10 downto 0) => d2_net(10 downto 0),
      o(10 downto 0) => d3_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized3_36\
     port map (
      i(10 downto 0) => d3_net(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\ : entity is "ip_scope_xlAsynRegister";
end \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\ is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_26\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r7_clear(0) => r7_clear(0)
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_27\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_28\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_29\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\ : entity is "ip_scope_xlAsynRegister";
end \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\ is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_18\
     port map (
      d1_net(0) => d1_net(0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_1: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_19\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0)
    );
synth_reg_inst_2: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_20\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0)
    );
synth_reg_inst_3: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2_21\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d3_net(0) => d3_net(0),
      r8_full(0) => r8_full(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\ is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\ : entity is "ip_scope_xlconvert";
end \design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\ is
begin
\latency_test.reg\: entity work.design_1_ip_scope_0_0_xil_defaultlib_synth_reg
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2\(0) => \reg_array[0].fde_used.u2\(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xlregister is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xlregister : entity is "ip_scope_xlregister";
end design_1_ip_scope_0_0_ip_scope_xlregister;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xlregister is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized0\
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_8\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(13 downto 0) => \rel_39_16_carry__0\(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_6\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1_4\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized1\
     port map (
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[15].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(0) => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0),
      i(15 downto 0) => i(15 downto 0),
      o(13 downto 0) => o(13 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\ : entity is "ip_scope_xlregister";
end \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\ is
begin
synth_reg_inst: entity work.\design_1_ip_scope_0_0_xil_defaultlib_synth_reg_w_init__parameterized2\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_generic_cstr is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_generic_cstr : entity is "blk_mem_gen_generic_cstr";
end design_1_ip_scope_0_0_blk_mem_gen_generic_cstr;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_generic_cstr is
begin
\ramloop[0].ram.r\: entity work.design_1_ip_scope_0_0_blk_mem_gen_prim_width
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(17 downto 0) => dina(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
\ramloop[1].ram.r\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(13 downto 0) => dina(31 downto 18),
      douta(13 downto 0) => douta(31 downto 18),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\ : entity is "blk_mem_gen_generic_cstr";
end \design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\ is
begin
\ramloop[0].ram.r\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(17 downto 0) => dina(17 downto 0),
      dinb(17 downto 0) => dinb(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      doutb(17 downto 0) => doutb(17 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
\ramloop[1].ram.r\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_prim_width__parameterized2\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(13 downto 0) => dina(31 downto 18),
      dinb(13 downto 0) => dinb(31 downto 18),
      douta(13 downto 0) => douta(31 downto 18),
      doutb(13 downto 0) => doutb(31 downto 18),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
INaBf8vh5mCmDzf2yp77pxZAxQdyEQiT/vG2dEgvrFjseUnGc6ldwH4JvdnpZSpdf/ihioPyMNjl
u6ooyzv5TA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S5XIZZtuFR/MZffuhwdnvE3H9oRWM4uXoaGZTa/Dyk62O+Wa0v41pjmZELCiR7uodZPFQfykZ6K9
2ZDMu8dB3afQRMs5lnd/53M1b9ke+MNEeZ/wzjUcsJghubnEAwzdWeW/0tlqST1WD9B/KCxYqwH5
Gj6IZTTFHAXcaVhnCT8=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
CM6IcdzP0PbD6yMSqylmi4JE2qpmxiNeI+prjGwJiD8e3Xsynu3PbGKJAOpOxtR1hT/3mpBcx1Rz
Fkz0QBh4wtE8fiziv1i+xi8T6cqC8ClamjrpZ//sn6dh7NvwSYik14MlwVuei4DZoZJZF63aoPUn
RXkQ13wtK+MkYKBcPVSZMFZmaCU6jMMBYclXzvRG1JqqZoa7mWFTeFZePUTXG7Wo12QaZ8GUi0AV
UIshoN25yn5e2Xr3FyuEtm5AvsZb+iLsgLeHBtKBnsVaHQphicgqwgwv6MQQF6ZNBgU/aACfibDS
3+n/mMMm8k1cj2bW6VCi7a+c8LmCf81NlJuLww==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ehl0CusS7+JNGq6HfhyaBMy68nccIdIGqixoEztEZfkCpXuUYsdqw6G9MIJdWdu0Ck2acV7K6IVg
rzb8/bNaDDVWp48kupToegTkOdwDkCejEqppido4BkJ+iEkjPniz+aJHlOlOwmauETy2hCMuuC57
oWDprzGWlsqbCjqzKrXmPYm6fNdcOa2DiOYstQaMFNbPU2ccrbLJAiYMHNDqtPNqWxKBsD67kiGf
2eOneDOmdmy7YkNsL+cx8MJc3BVUsYBrpAEsGyFMkmX8a8nYz8R/wlFQFGQAd/t5XrfxFNI58mj1
AHXbcAMhGKVq9YdKeU/vSXY/NwMqp12xJ1nUaw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
h/qRAwiPuqY/Zg/QWqbaYm8xWTi9SshYuPzyL0UME9ZDDF+C2CyGAugh9HzMdD0kZmT94TKmBgLR
dKP28nlE8VCCU5rvbjKxfn/wNtNKHCvZ1hns8CF7+pGuelhxGvXNmYKFw5co8+4grYFaDXeoZZR6
S5sjvhqtSVD3+qq4vYWRjT2Y/yes7L9dRsLq2D3iZ4xjgVHuIbOQLT/EUKW+9iYudT9Uy6YTwB+5
mSb0QK3YfZdGwZyXB4S3mdF9vNQHdW/rnACq3yngF+lprNkh3ooQKdGqtxtz8KSQxNZOAFE+koOw
h00o7AKpvDAp3uNguLvnNJH3rugOhh95b8Jatw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TsA04vIYHDZne2CBj5bWCBFH4MtNoFDCn/3DNEi0BwutuUf+X+lD9kAO3kl352WHjQbF79Ssm+PT
fCYpODgWdxSVbzaHFpITxCQ4HcIJhUeW5PC5tw09Tand68D6eg84qRguH+llbb5jdGJkJeTCf+Mx
pupkkLiDvNyTYWe+nqw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rx9hgQkvaJJTJJcTjGFW1DrrWiT+xanrcMvFn0Z3KRXlZvf+SE7IQgGCiP7ZDA6T5z1Zv5nzS4h5
cVi+CvwC9UMZRWmLDAjzASJ2nx1g9BjbYe2vHAUmyurIiR6LSigTeM/9TlMv+fFwJbqwuH6FJ3/z
Vl4tIMk4NrqkMn/riOG87SjhesepM6kcQOBgDGzLTG14z3qeZG8OPzxgApfyubmX4qdD1oTgGm2u
Q4mQfFxEye6Jqkn4Rzjhifs/ieNYomHlK7R2/72QJj5j0WyYBIhvO+09izz299Z54ZP2ZXaRMfDT
lU4lQNqQU14PX9Yk9p7sy2PnK4vTwwF0CFIgSQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Bboq29ZV44mGpUXLfwn/8M3dmwkhxXxEfUovNKjwTl0wdPokyPS7/Elzy1eTJ8+htrKCGK4dj1uI
Du/4F7ewpvZubkck9uhoksKSEbI+60ZRHzaOVx2wappmzXFmsatFLoETKyJKp+qnGYAsZmSVPIhe
5eLzDyUt796uJcUsKTmbHSQNHiRvVFcWcirZv3UYQLvvAlsFilOz5RZkHbD7+1acuSrCaAFOBwNC
7yi6f9tIA41P6PbyehRDXH+j/eWDj8RLFVx6S0R/JkmIjPetjE75mxYHzWWuSxAOnciUiYIyK5x3
J8OGyoLMAAYQkHvwbTmsc6B04Ao0UPoDjPiE+A==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Ou16U9V2lcLqvmLUs1EJoCpD8iScMJPewSRvtXHmDWDWLVc0WE6AszZE+W/SiyJkaDoql5sf8rlW
2rbmcImOmMsP9yfBcqgjxk2d4AcEfzCzmFsHw4ViScXfCY+KlFaIPiQo/RFMLx2k/aCEiFWCH752
jfAWrdIlWNRs4Dgu8BTLafBWmvst/3o9ohwm+HuustgueZp4l580w9vylcZ4xLQfKbDpj3Xv464Z
ZtlrrD22AERQNpvrmQcqe2GuQnYiWCvwYmzj7P/XS51i6yd7HcvDkdxi/qLAeOhF43nrgypljZOi
Z1IgAqazT/dje5bCpHvjhKUReeZXSqnBe8DzQg==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 976)
`protect data_block
8m67guETwdOfqfktYbrkcQQITnVVA7y64LVqPUaDn0lxcmIF41przDGJ73z/OynzgzvCqMT8ylFA
rPzkYeeuKlKj46M9pcukBwm3oVZTyZ/wG+ZGD5164f8NJF6DP3apesQpInAwUcoD4D93iLXC0OOZ
1wtp27Sk4zF6Zb9MGG9OYZu0aXcm8O1LtZmEo05PmqzIIOmyLek3ZRdOo86FJlLhQY2pgkuzJepz
34txDN52p8fdg8M09FP4hXwHCnTs6RJ5/b+gkiocu9zBagw/OSN/ptSYVXaxnvso9LWiLsHuVRvk
RErBeoX/NbTQcOIh7OgRHdP6X+wuY+c36jnWStsgpSem5kHEI2H+PzXw0apQ5o5NdUylKu4xWD+A
XhpAgBUv9bDctq0iQjycM06875ujeo3LQvsNQ+BRUnb/n87tqcSZIRKVrrhAoTE49AvEUAAYc3P4
k+ZGj3XX41mYptihLWf/EzuBa2UonHBPE84gapESSDMSFBP2nx8jSN4daGSQpFknrLDzjStKf9wV
5EcCnrn9UvCkh8lmmcxSzVdxy1EQCqVAgE3dk43oCw21I76Fz7qoAvP0c6TUjy+MfbRou9fkADdy
+1rThKCxaI6/0KesHNXSFQokmL+ZIX8/Lt3952R27GnUVh7qInArRhx6INgxQIQudp2JpmD7KFXE
gf0ucmGGKXKnQ4Tp7pWd/UWuTZzOgSmXZqRs0axmBQRx/2wiFgCagHICgSxYhAWIdtNyh77ruGFG
vSzREbkP+NoFWCQarE5SAVQGjebWcwcPB32SYJe6Nhz238jvbfe+JwHHvYRgvHLiJGPa+WRA+Gfk
zzqnrt+fXt1tQhElB2tUfn2I0On6SdcpAFU5gEHOvQRCQAEGD93OosUGORP6av0pjEnTaQzW2koO
ZwYDbylMnUt+x12kAfVfQNdsaP5xvv8nq4z38L2+9GlnPKXbRilALGMOS9NnVQuyCRtVCk2Mt3rb
QIvx5GQD3Z09lkNAZdQFVRP+QVl6sAofmaalGAh+kfMSXAXgVbWABfQuLNR8vzXnRoxioElYuXap
qRkw7kHgjYvyrEKwXVt8wXxWGlHpi6XEdPT+Fcj6ye6mTdjQoDdtWt2CJYcMgB6m+DIdh9z524gq
0cfGrDyiz61bUgYR4wg6teMQ4+3cbqUXVSigX5TYBRuoLlUcLJriBZNKj7ncDl2ZEvbMsZgLYwgZ
n9Yz7PrcMW2/FlP0Ag4+jcwZyROQBvMHbKO4JYJe+pwFVdaDqQk32Y3xZCiu33Gy4WbcubEEeo/t
/k3+vJ14GA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_top is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_top : entity is "blk_mem_gen_top";
end design_1_ip_scope_0_0_blk_mem_gen_top;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_top is
begin
\valid.cstr\: entity work.design_1_ip_scope_0_0_blk_mem_gen_generic_cstr
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\ : entity is "blk_mem_gen_top";
end \design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\ is
begin
\valid.cstr\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_generic_cstr__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
INaBf8vh5mCmDzf2yp77pxZAxQdyEQiT/vG2dEgvrFjseUnGc6ldwH4JvdnpZSpdf/ihioPyMNjl
u6ooyzv5TA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S5XIZZtuFR/MZffuhwdnvE3H9oRWM4uXoaGZTa/Dyk62O+Wa0v41pjmZELCiR7uodZPFQfykZ6K9
2ZDMu8dB3afQRMs5lnd/53M1b9ke+MNEeZ/wzjUcsJghubnEAwzdWeW/0tlqST1WD9B/KCxYqwH5
Gj6IZTTFHAXcaVhnCT8=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
CM6IcdzP0PbD6yMSqylmi4JE2qpmxiNeI+prjGwJiD8e3Xsynu3PbGKJAOpOxtR1hT/3mpBcx1Rz
Fkz0QBh4wtE8fiziv1i+xi8T6cqC8ClamjrpZ//sn6dh7NvwSYik14MlwVuei4DZoZJZF63aoPUn
RXkQ13wtK+MkYKBcPVSZMFZmaCU6jMMBYclXzvRG1JqqZoa7mWFTeFZePUTXG7Wo12QaZ8GUi0AV
UIshoN25yn5e2Xr3FyuEtm5AvsZb+iLsgLeHBtKBnsVaHQphicgqwgwv6MQQF6ZNBgU/aACfibDS
3+n/mMMm8k1cj2bW6VCi7a+c8LmCf81NlJuLww==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ehl0CusS7+JNGq6HfhyaBMy68nccIdIGqixoEztEZfkCpXuUYsdqw6G9MIJdWdu0Ck2acV7K6IVg
rzb8/bNaDDVWp48kupToegTkOdwDkCejEqppido4BkJ+iEkjPniz+aJHlOlOwmauETy2hCMuuC57
oWDprzGWlsqbCjqzKrXmPYm6fNdcOa2DiOYstQaMFNbPU2ccrbLJAiYMHNDqtPNqWxKBsD67kiGf
2eOneDOmdmy7YkNsL+cx8MJc3BVUsYBrpAEsGyFMkmX8a8nYz8R/wlFQFGQAd/t5XrfxFNI58mj1
AHXbcAMhGKVq9YdKeU/vSXY/NwMqp12xJ1nUaw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
h/qRAwiPuqY/Zg/QWqbaYm8xWTi9SshYuPzyL0UME9ZDDF+C2CyGAugh9HzMdD0kZmT94TKmBgLR
dKP28nlE8VCCU5rvbjKxfn/wNtNKHCvZ1hns8CF7+pGuelhxGvXNmYKFw5co8+4grYFaDXeoZZR6
S5sjvhqtSVD3+qq4vYWRjT2Y/yes7L9dRsLq2D3iZ4xjgVHuIbOQLT/EUKW+9iYudT9Uy6YTwB+5
mSb0QK3YfZdGwZyXB4S3mdF9vNQHdW/rnACq3yngF+lprNkh3ooQKdGqtxtz8KSQxNZOAFE+koOw
h00o7AKpvDAp3uNguLvnNJH3rugOhh95b8Jatw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TsA04vIYHDZne2CBj5bWCBFH4MtNoFDCn/3DNEi0BwutuUf+X+lD9kAO3kl352WHjQbF79Ssm+PT
fCYpODgWdxSVbzaHFpITxCQ4HcIJhUeW5PC5tw09Tand68D6eg84qRguH+llbb5jdGJkJeTCf+Mx
pupkkLiDvNyTYWe+nqw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rx9hgQkvaJJTJJcTjGFW1DrrWiT+xanrcMvFn0Z3KRXlZvf+SE7IQgGCiP7ZDA6T5z1Zv5nzS4h5
cVi+CvwC9UMZRWmLDAjzASJ2nx1g9BjbYe2vHAUmyurIiR6LSigTeM/9TlMv+fFwJbqwuH6FJ3/z
Vl4tIMk4NrqkMn/riOG87SjhesepM6kcQOBgDGzLTG14z3qeZG8OPzxgApfyubmX4qdD1oTgGm2u
Q4mQfFxEye6Jqkn4Rzjhifs/ieNYomHlK7R2/72QJj5j0WyYBIhvO+09izz299Z54ZP2ZXaRMfDT
lU4lQNqQU14PX9Yk9p7sy2PnK4vTwwF0CFIgSQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ZvYgpbeGI0pCHaq9qkwvldBcCa5FKLglWT4POxo0grY6LQjlyGr1n+fhg1uM+eng28r4bCkQHDDa
DieaddN/kZpLkIkw7slPr5aHR8kpbC/SnBDqJYhZDS82F59K4ZeLMUhgXFtU+qQ7NuU04px826+F
ltwOBZQaI+maOuAFm36xFKMizerada0y+dLcYcl0tG55/iGu7kiSVK0JppIY6YgQZeR/R9VM/xL3
mb99V40G00aBizeNTF4AoQCZP+ewcC4SqSzfbzXOJKkvD4nVGfTBZjk8TUawGnklfzIItI6m3Kqn
erR/hjDzLAsW/COGHYbxc8B6hqJg/mrWjzeXhg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mD73yfDaHbW3Xv2ienaK+gLE5+P6pPLwwTD+Vlb+Cg6Ni5GYw8Wf9Daw5OTsSKGEQ9IucEMpjBKc
WjkHUcBdzO45Jr2w8F2QzZctoWa3Ome0u4h4FXUMJCcz9HBSVWzfcnaGENTIUBj3+lXzUJX7mcRv
qfJhitdYOnGRq3jCGHc2A2mntAVEpjIzfWCVN2go3t4ZMlaVwUFYiVk8gsx6x4zXUkeXUTmOuGL1
nquGlC0Amk+LamASYzoG1BYRXqPKudZ/EU4IB2LmXraKswrUUnZrHejNXpCjQ6xD1U7Gc6f3ipFl
VHL0ei6agYXMpIQHSvgZMMqOR2zeT3fEMKXJMA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 944)
`protect data_block
8m67guETwdOfqfktYbrkcQQITnVVA7y64LVqPUaDn0lxcmIF41przDGJ73z/OynzgzvCqMT8ylFA
rPzkYeeuKlKj46M9pcukBwm3oVZTyZ/wG+ZGD5164f8NJF6DP3apesQpInAwUcoD4D93iLXC0OOZ
1wtp27Sk4zF6Zb9MGG9OYZu0aXcm8O1LtZmEo05PGbb1qqdTJulPWZhvLbokADWSmBspcBoAdt2Q
e/wrUm8ffeqxMcH0uhiIau1YeMmeWi+ynKXh2GbF3YmHL+FnX6xYYIQ0XODXG1z6wstkTi+wuSuJ
TBSjp6SyHptfnjc9cV5SDtAxEpodVFC6Sj4qMdEfktFSeMUE7bI43q0MVR9YgvMmtL8rl2l7ySyC
QG8oxzHudkINyAcA/bxczE64P69M9+yd2LLyZrKmcf+RK9P3uAjwKuIXNq0YgjVHqNU33WM09iAx
D6/j8rPkpofthyqyjtAMlDXO5xKN2Z5wq+RmQLKlqifyCxF5iSJHuz/TaN7cvMXicANrxrvMXwrL
p1MTA2SnRsZ5RUGK/7o+UjEZrteY78312BHgPcOIYkOLy3fT0sB9SEIFb5RXl0B5YFQwpjnnoc7J
5A1ojT1RsBf+mkT7Hp+C5ThNSR1Z0YaHoZHTgGnP1IBYnIteK/IU+oGJoQucgpRSX20oR1PFrpA+
grdMGk7s37lYqei/5CR4BMiZFfm6pcdTsg6tuyTtO0T/QEAfHyHJzDdiLVJzGgzZxn822z9aovOs
V7J3BRdpt69NGXSgyWDFFlmsL64PnVEZE65Nm70jANM9WhVCXzTzI2J4kA71y+faO6m4S3RyoC6a
fTk8m1ggiF9J/Dwzc536WFw9MjLORY2qH+8nLifN+RCog/MX+93lPi+FU7TntMARGecAJupnbGPC
xncbgWL862U8C77ZbXEn/DOMcwBC6BBkHmMBFuuL7ntsY1++8S8RSOgPMAcFRab+DFzWx7Sf1JU4
B3CPhnS/vmcdLyMfVezWCE+0ga3knNEBnXSRvqwiLyFS9CtM7XGk5Yr6sC75BWHarKcZshEAvjO7
FZQ29ddYINTjig66aZ5KNUAexZSgrD9QxiavYf/Lx7EOyVQXwhUd3B8PW2b4df2pcO8JfEmPuFHl
W6dAOOC8/IeoQC0gSUIxAR56texOIUkJbX+yCj0FjhPPc5VE23yhQK6z2kgiuGvYGG1x3oAVyRJ/
RDlSNi/DYHQv0a4CW+G9Ki5NgV7bNsh9PaZOV3hUqao=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth : entity is "blk_mem_gen_v8_4_2_synth";
end design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.design_1_ip_scope_0_0_blk_mem_gen_top
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth__parameterized0\ : entity is "blk_mem_gen_v8_4_2_synth";
end \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth__parameterized0\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth__parameterized0\ is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.\design_1_ip_scope_0_0_blk_mem_gen_top__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
ZBY06y5BSEA3vwLtCYy6nxOZv3rYFFgZv5ABjBaqtaItkwdtQfFvZBIMhBOgu0+1i4DhnUz7pdYr
Y88DaxXmyw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Q91nMYZhjxb8KT0ODrW+miquus8bIV0xJDXXyQLu4mbE2ZGK0HYqPk6xE96lKrNSpNViHea0rEyX
J3Qsb1QJLBM/4rnfg8PNzn8acqAN22JgnqyTntYQVpk0fARej5ldkyKbsCPgkFDFJQnDbUHBIcF2
clV1QCjE7A3SvN91cV0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
fpeDNxCbq4trL0iAEhu+gbl4Rix2OTBKp+3DlpwRVRrJB8M79X6xv2dY4g29GTJWY/qcPCM3xauG
RxLbIsN70w9DSrpdJ31jxXSOp/N0b21smrkPYOGR9al1eBkfjYMFWbiVzWEKHK/6z705awwEunRN
qhtuKyDzs9JphrMi08O8ld4FYuGNYbtDOUXkizCIgaOdAfQTq0yCDea9z6uJ5sQUPwqrjRIroSnJ
mW8XvC4+hFTtIH4kcsR/hWe9eHVCVq7yIdgTrHznDz5I4c7+A0ZUoahnR5dHirQC2z7KKzrCldej
93tdxPQksB7VjPElshg8WP1MGrwn+7hvSijdSw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
No6agU7QCIBdcP4teTJDlwXV+g3qBzu8V5gqFUsql+qUP2ZRyYvAPscmGZyPnHh9xvIYYFmXqCE7
RRM/BcEtyrJ9GJvahRcE/doL0n1EHIOASw/MZnFHkf6gtqWvN+SIv29/H/UyUfhuDXqJBGjBGBRs
+/RValRovCLF1SU7AdbCQbWKJbpj9JDmu7gpnhPbkiKkLcd0L7j/KcvlPBvHLG2JvHXct9Oyye9y
FJ190Nne/diMvLsfTBKIzRzQiV/kj3aSYxw4yzuKLbdVZ9eZYqFHwhjBXrVIvIAq9zy3Z0JajEGH
8Eg7Z1uVL2BNbnB2qP4/6a3wYkq6RDa/mFw99g==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Au9tuW8YCiySVmtwoSZ2LqBsVMwu9uzGBs0i03rtA+ohnDzpS7/saWzSdnxtvJsmHKLPTnuG8etw
O+1iKknogGQAhYN8j4DK0/PmelqEJy8N5vwkQ/o6l1cfVFLfqvAMRbZ7lkPzco2SCT7/KjEJHW7i
5gy7tqPxnW7QwYv2vH65EVqe0p2tQ2kCHVUvvPaAZbeDzA1LHleCahBpWEI3g5wztTT869s7a4yn
1IeWyD5NV38NHHcwqubPZ09C1Vm5NLAHW7sEnM3is9mRkFnCh/x4Fb6Ecuu4bJYFhgmNzCCKgYK9
PEdkW2OgY7EzDM7ocQQuoE0+aHQvw9lRdJm00Q==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
n8+Js6UruWrTa5ioc59l4AeAloQ6ZDwzPNPXUOknQWFRecrzd2eOQ2KSf6tv5Oxix315yAoI88kJ
L1R7xZeU1dj4QCJCinzjHZXGEfUurXJVEcq84ofioKIpCyBd7YnxOq469vjhUCYiTJvMARwPVvDY
U+jspt29lk+k5/XFur0=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HvScITgcbiG4YgkXwlLAPuMki7p9oPIAapsMuPCpK/tVnY9llE0MvUk/POKYiMFRuKgzht1jfNyM
pX8Qwv3/+iDiBgwTwibzi053ET+OglbpoF/MDrRErGx8VRvmBKwxnlefbxg6dCEzjNwYuFpDkHVT
YZySWRuz7hA0uzRJwLLkvg9LoVoAsjHpp+GqlpSqfuVaV3IJzpIboKGmFv2qLj7Z3k2aE4HhZfXc
HclRJsWxw/CA2DK86EGTnPC71xJNT7pgY1DSHCglqFwF35L0FfZes57Wpz5Ka6YR9dKPNCocMfXO
DZKOoy0+Zz/G4HOrhtHGxgzfEtHjRq0ZthhxDQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Gc3QJzPw46KENy36Ou91YNl1q/OVj6O1gDCqTlR0sFSE0U/X1fAisRgvNrlgZeD+8+nhfzcnY45K
gAaUqv5lmKUiPAnezFIzv3wbXsb6RSDCN+3Nl4PVW9FsuwbD0x94yPLJi2aEK+8q+poiqhY78OHW
bnTd3zuX8GI7j0G+qu585z5vCQEWYnFwEVbg/CXHJjRantaGr/sfN2fiPxFHS3QadpFEJTaygOak
Y//y5lGV0NEvzYLzfaG0pMYKBiWyhx3qsfqV60pE1hS7f7tk+3yx5pIPD6H4jl90TSwj768V1qCW
OGI4FSI83/+txsLjKt/HQRVsEdNJjtD9hETe9A==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
m6cQ1yZYJbPmJGVxxpCEuyxJdI/48UVOdpkAsrqbJckgTRrdqyOJm5HYHNeprV6xeXDCqshQiV1t
3QlM3YzfxpgfQR0YzrVza3kqctX55exqEBpqrsG2LgYeks2/5KkvqJxh9R7ux/D4uspTcV2JHY6F
nBTkUDeNplvnqukJh80rUiniV9vWaWJo5iKI1c3OxyM1iRNC4ir+dFBJbBiwpaFqWpdToobJEVML
Twenf3h2oskUdlBFZIKDoLF8Gz9OZYd8IlMqGHfDFXpZSCyqG6RDkqNBzdEcNBIibRwKaB7qAzSa
gIUJlLyK/zTVd5aYtaWAu0J5wJsz7sikQL/3iw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 960)
`protect data_block
BkXdSzPdkRBrWIJ7eV7TJVnSLBtkM0up6jVWwFPwepFZQPR7sSvZavN7hXqlKRjhKwmOg7tFjoxg
3r03hAYVNm/o59oXEwfWf3UXIdhsUJLOWto5bbHIr7WFFNKWj3XW18meIPxign6OiT6ls/YKlkfB
6rnURT4+EVzA6fVCgq7+6YU3KYFJoOD5wU2EsyDnhVaLzHtpehaGpTlK1Q8+F8UCt83iH19fA0pq
TOWYtf2SA2F1XAEzrC1PxOeIiGlZrwAjZfAuuWzgvjFQhzXBRfgp1adPlKRPHc3uv2lmYSdBQHuj
hc371H0Z19ab/onmzraLfaNGMr1+g4HalRR3aQ23p9zD2dsSTDKtSgUJjEI1jGdWRVaUHQfqgODp
xeiyZUspLvdJzR9v8nvpUNeP6zc+ZPGbf0mBX2OHesZoOvcz7k5JCCBtQSl0DdaWwFIfYHXOIHFP
O6I3SCzayRg3Zn9XfwVdvaFmj3McGpyN59+AMe8y/3qtY1/SxdUVM6Dnd/tBIGJnoIfccWSy1Tpj
a6dTB4d9UBQZlYhlXPmzkJK33om6XVkPTSmWA9E1JNi8lzvFV2QqwDUDiM2TjKLgqegFGSsoXgzA
+YU9RDjtjthXQbOB9q1gLpXYcPI0vE6EXKxyTeMw/kCEWENB5CpYex7ehztot+ofJhPNn/fs4DxZ
55+2LG+74nQ2u1GW9OsM3tzdA011cbf8aaUR6YUp8ovEButBOHiXrNBpZ4QDiCLlwFZqi7mFTB79
6Eq1cu0ePhH4IkTtrLqk31E2MIiINqw09eyhaEgiaNHhvRrLEPV4VOPgTL6zUly0upx/q/OBStsE
LX0a/r9Fme6OE5CKogA8uwJZbV69L5avRQAChm28K0aQDe89OvbZe+KJyGs/1NqQTcLpEmNsjHZw
rE+EA4ee0n+U1GWaCRqV4E1ETVXdxgyDUCs08LxQJHECbm98hd1H6Lom7J83a5LynrjIRh/ebuN+
pv91dfmKuyAM63z3bkXexnF7WJnHdRC/Ga0nubQlPGrhNWlDDjitAGIu58g+f8k3lxoLQp3EsMew
PZuI6aBF2PujSpD7QNWY2U7JR3eznpVj1e0j7DGHs2xnzJixykP8gcfYAVyIdKhqNZwfd+n7V4aE
5p1AvGYOP/y6HYWEaZbX1PjOAJ69yz5RZzLbupRSry4Gq2CS+bgdZhjZ8F/8vIDd0PC7THfA/f93
mWCh31p563GltwdIgFL8vdHTXRJDmQQmYEfRQv0PXL7SB6SmjDBN9nO/k6sAKhEC
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "Estimated Power for IP     :     5.6824 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "ip_scope_blk_mem_gen_i1.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "ip_scope_blk_mem_gen_i1.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "blk_mem_gen_v8_4_2";
  attribute c_family : string;
  attribute c_family of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 : entity is "artix7";
end design_1_ip_scope_0_0_blk_mem_gen_v8_4_2;

architecture STRUCTURE of design_1_ip_scope_0_0_blk_mem_gen_v8_4_2 is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  doutb(31) <= \<const0>\;
  doutb(30) <= \<const0>\;
  doutb(29) <= \<const0>\;
  doutb(28) <= \<const0>\;
  doutb(27) <= \<const0>\;
  doutb(26) <= \<const0>\;
  doutb(25) <= \<const0>\;
  doutb(24) <= \<const0>\;
  doutb(23) <= \<const0>\;
  doutb(22) <= \<const0>\;
  doutb(21) <= \<const0>\;
  doutb(20) <= \<const0>\;
  doutb(19) <= \<const0>\;
  doutb(18) <= \<const0>\;
  doutb(17) <= \<const0>\;
  doutb(16) <= \<const0>\;
  doutb(15) <= \<const0>\;
  doutb(14) <= \<const0>\;
  doutb(13) <= \<const0>\;
  doutb(12) <= \<const0>\;
  doutb(11) <= \<const0>\;
  doutb(10) <= \<const0>\;
  doutb(9) <= \<const0>\;
  doutb(8) <= \<const0>\;
  doutb(7) <= \<const0>\;
  doutb(6) <= \<const0>\;
  doutb(5) <= \<const0>\;
  doutb(4) <= \<const0>\;
  doutb(3) <= \<const0>\;
  doutb(2) <= \<const0>\;
  doutb(1) <= \<const0>\;
  doutb(0) <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "Estimated Power for IP     :     10.698 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "ip_scope_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "ip_scope_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "blk_mem_gen_v8_4_2";
  attribute c_family : string;
  attribute c_family of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ : entity is "artix7";
end \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\;

architecture STRUCTURE of \design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\ is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.\design_1_ip_scope_0_0_blk_mem_gen_v8_4_2_synth__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
ZBY06y5BSEA3vwLtCYy6nxOZv3rYFFgZv5ABjBaqtaItkwdtQfFvZBIMhBOgu0+1i4DhnUz7pdYr
Y88DaxXmyw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Q91nMYZhjxb8KT0ODrW+miquus8bIV0xJDXXyQLu4mbE2ZGK0HYqPk6xE96lKrNSpNViHea0rEyX
J3Qsb1QJLBM/4rnfg8PNzn8acqAN22JgnqyTntYQVpk0fARej5ldkyKbsCPgkFDFJQnDbUHBIcF2
clV1QCjE7A3SvN91cV0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
fpeDNxCbq4trL0iAEhu+gbl4Rix2OTBKp+3DlpwRVRrJB8M79X6xv2dY4g29GTJWY/qcPCM3xauG
RxLbIsN70w9DSrpdJ31jxXSOp/N0b21smrkPYOGR9al1eBkfjYMFWbiVzWEKHK/6z705awwEunRN
qhtuKyDzs9JphrMi08O8ld4FYuGNYbtDOUXkizCIgaOdAfQTq0yCDea9z6uJ5sQUPwqrjRIroSnJ
mW8XvC4+hFTtIH4kcsR/hWe9eHVCVq7yIdgTrHznDz5I4c7+A0ZUoahnR5dHirQC2z7KKzrCldej
93tdxPQksB7VjPElshg8WP1MGrwn+7hvSijdSw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
No6agU7QCIBdcP4teTJDlwXV+g3qBzu8V5gqFUsql+qUP2ZRyYvAPscmGZyPnHh9xvIYYFmXqCE7
RRM/BcEtyrJ9GJvahRcE/doL0n1EHIOASw/MZnFHkf6gtqWvN+SIv29/H/UyUfhuDXqJBGjBGBRs
+/RValRovCLF1SU7AdbCQbWKJbpj9JDmu7gpnhPbkiKkLcd0L7j/KcvlPBvHLG2JvHXct9Oyye9y
FJ190Nne/diMvLsfTBKIzRzQiV/kj3aSYxw4yzuKLbdVZ9eZYqFHwhjBXrVIvIAq9zy3Z0JajEGH
8Eg7Z1uVL2BNbnB2qP4/6a3wYkq6RDa/mFw99g==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Au9tuW8YCiySVmtwoSZ2LqBsVMwu9uzGBs0i03rtA+ohnDzpS7/saWzSdnxtvJsmHKLPTnuG8etw
O+1iKknogGQAhYN8j4DK0/PmelqEJy8N5vwkQ/o6l1cfVFLfqvAMRbZ7lkPzco2SCT7/KjEJHW7i
5gy7tqPxnW7QwYv2vH65EVqe0p2tQ2kCHVUvvPaAZbeDzA1LHleCahBpWEI3g5wztTT869s7a4yn
1IeWyD5NV38NHHcwqubPZ09C1Vm5NLAHW7sEnM3is9mRkFnCh/x4Fb6Ecuu4bJYFhgmNzCCKgYK9
PEdkW2OgY7EzDM7ocQQuoE0+aHQvw9lRdJm00Q==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
n8+Js6UruWrTa5ioc59l4AeAloQ6ZDwzPNPXUOknQWFRecrzd2eOQ2KSf6tv5Oxix315yAoI88kJ
L1R7xZeU1dj4QCJCinzjHZXGEfUurXJVEcq84ofioKIpCyBd7YnxOq469vjhUCYiTJvMARwPVvDY
U+jspt29lk+k5/XFur0=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HvScITgcbiG4YgkXwlLAPuMki7p9oPIAapsMuPCpK/tVnY9llE0MvUk/POKYiMFRuKgzht1jfNyM
pX8Qwv3/+iDiBgwTwibzi053ET+OglbpoF/MDrRErGx8VRvmBKwxnlefbxg6dCEzjNwYuFpDkHVT
YZySWRuz7hA0uzRJwLLkvg9LoVoAsjHpp+GqlpSqfuVaV3IJzpIboKGmFv2qLj7Z3k2aE4HhZfXc
HclRJsWxw/CA2DK86EGTnPC71xJNT7pgY1DSHCglqFwF35L0FfZes57Wpz5Ka6YR9dKPNCocMfXO
DZKOoy0+Zz/G4HOrhtHGxgzfEtHjRq0ZthhxDQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mepJO7XC4s9KFbfcgpcvJ/ydxprhhL1tIr6/9l3QeJWcfwguFy6rmCbE05zTtr/yQx2GfEh0qf7z
e/NPD0OIirTO06vf8nNHAB8Y4xCS6ZCp4SWkyiMJL3aFyDKg7FpRDHQhjKtrLxkuq/gKegeSzSwl
+DgVECIFJNw9LBv9r/ZLVu47jAZTa7296SeeXC7a2uZHxL9oRkJzj2uidK0MK/J6yBNLsmOGKlL9
nkKCBbYMstRkkwQThhJd0IQI+10xZTGnvJs7biBJDK+fZ/ZARYWhiVWL+U78zhubBFZ/3MxJL8ZA
oC6DXPkpr9AsPwy2xscmidQfyHe+hhLgd/4u1A==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TUnwCjaw5rKd7LtjoeSXKi75j92HocKiAPFOil/PjStAh07SW8OrOUog98RxueCC0bvgXWsI5uYm
kB9p9Dkxd9fruBzATzd40+UOZSql0jc7V6rOCKFtom+9+Bud7FqGrUfPF9JEVMjXbWTEiDc4rfX3
bRUAW337lTZ0ODwXs0IQEVjnMar0/n7qjI8P/RZg6Z3mUt5j7fXLiVHKHJjKQZ9ktUjpKJ4hHSZ4
woIaIgWPz4xZeRBWlNWwEmpAWUAY1D6h4nYCzeE0fbdnXKqDvZmGKOXgenaQOUTklrEOBt6m5bo+
NYkas+ALKOMfzARl/3UiNwSJVOnAHFjm4J5esQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4560)
`protect data_block
BkXdSzPdkRBrWIJ7eV7TJVnSLBtkM0up6jVWwFPwepFZQPR7sSvZavN7hXqlKRjhKwmOg7tFjoxg
3r03hAYVNm/o59oXEwfWf3UXIdhsUJLOWto5bbHIr7WFFNKWj3XW18meIPxign6OiT6ls/YKlkfB
6rnURT4+EVzA6fVCgq7+6YU3KYFJoOD5wU2EsyDnjmIa74bLDsJo/HcnB0HvGps2gdqA5HSmG0yi
mkbzfPIvhSYtXrvsQWwv3w+9q+tDJmJ65qrbmxUTNKtc4ILmqSGSnInFXL5m/eJXCwaJYXvsFLrf
i70/dxjJZUO3r1E0d6sIDTzlXfRDpHix3SqwuqH60u+cwxu7RO3UGL9ozdxSDVmNUQRUvctwptAD
5bMHs2JCqo+kR9fNGIyY7tXRYEMs7WiNjoK1zQjHr5P3CnMQIKcYEvMHxTSs8WZ/wiftPCrIevV/
RLsoUIJS1PGkEA2pgRriZo0ir0Jm4ThkArNbcI9TnBt/0yNmFhc365q0z0aQpW6Di7eAjYT8l/bN
48D+vmsudlrA7nOWVFeDyGsK1ELowertvy/yzLQqAdhm5jMSnrIJdv7w7vR+HorgcuIA8mAfK5Wo
y+O0V50LE3rkao/iMRlgIwbxUFkEv/gX7vi/nFsR0DRdB/hcJaLLSwa6wTdhOOb1bThOvSAefBOp
ktyabOWOwe/LdgUotUWzpQnTL0MrYEU/tWxTqLQImd7XX33xEZ5lBRan4gn1AiBuQVef91Zsm8on
doxbqHdT+fkYGBBtfKFx4MnJM056bg0bd6xQH2+MiVrTLUw0TyiGMhXZJCbNDTOllPS6vuRSlUtU
nEj1V+fGP3R51AT/o0Br7vqol+DpIFrK7Q9eeosH5guZ0mHl2lnbbnPwgsfSBW1ulWgXNMkYgTWw
1hn2KkU9y3T1zuLECiNxpDOgRFePRybpsVL0fM34Zqfc8EhN5934H09RWYBEHdLWsX2e6xVh4imW
xh96WB3MGDrUkSMSD9w6W4XG9Q50Qegp5NW4rBbOGzWvNVTN1w4rS4OEJoY/gZWOsZLvEa0wffPb
IMfaLBWwUGG8cR9nprt9da3OAETbnva5GdbHdlDXnCHhh+yGCzzUClSoTW1zv3uFOSmMlJ8zZP2F
ndYgeavBA6tezTII1RK5RnwSXLbEvsy7nWgUhi3TKFbz3JogcSnWJWCTs/k0rHEot34o4IrnfxFE
cmchymfGufxsUxPEL99laonWjzLg8NXr9biP+VKVG9fv4BQrue/xTnjj1UUniWm5juuZH6qkzo1U
+CFfcjBzM16lXSPPWMUDjKKNwx4SzP+ewFKcbpNlXYeJTjdyQVKOhZ5co0L51g4ygoqc/35YfwkA
lZp4XdTyEPy+tF7MQ424GdCbPTvVJ0QDeU6Eogf2QTSN2eUYk6paZUREHfVBs/Y33ncDCwfqwjnz
W/lVYYuVRgDp99FcxnKoCIXK9XQEaNqjngYJD9D6mV7aj46c8nV3xlqh6Cqeut7V22NxQ4Al64Jg
PNB5umKNcvpYrf/gWAmmN3o7OdMx6jTvROYhxTBkhCg2/qGfFugvWjAFlhcszSKv6SeDGR5mdYlh
8WOJ+v+EvycOIpTHzGrQCi2/plyeeDcblvfYJYSdLD2dbmJNWDDSY6ul+fcOnwpdZq7D9x/0ztSs
8PuZ1OrkPk4ULt43bFKBo0jnC8Rdo2ALvYIQ8nhnZmesqQevEVKiErZJ3tpddO7QM/uApPh+q1Lc
+sDP7KFYkGCxK6hoGgxaq2Pc6hmByyZ0tWMRYZmY98Z9HDjQysblWoZyAYHcQ5BdS4SD1hUk7pPn
qoyEo8u6/feI8zMe2x+TRQDKmuNlhCH/FhMCsdbZPBoGtZS+3UYjpsiNewQblLGMOIlyC4FHz+5S
qRNvrTLrTtolrODtLC2IFP2XlbVv5Tr+zUT5g4p94t4RgPoWM/K13ITTCIzJ2snFRf24AltFqSRB
5LAZZAqyuZMKCZWpACk/tjrYgWlwsVZm8d0Jb0hgNeA83X8+79uatrC9JW97V2qaZFNhBYEWvT4P
Ox9qpWN4Us79U8UMLoA8pnss8esN1huXVqi5xsPn8C4jV658DHQpONRcdd2+VoPuG0y3DU/eDcjK
d6YmF90hIABi/Uh4YpdkGQUGXWUtlZgy1L82wIMyeDVX4xAeX/SaA4gwaZuE3Pkb9onQgkfvNiz+
kBOvOZ0KB0mAevePihw0WrDL/SFsjTvIPvgzb8emAEZU1AsjfzEGsHtoxdERaQi2M8mJIoLFvz32
zLd4vl+zE2j2fW1SZ+TtVaEo8cLX45GapnG6EqPvRqNJSkkq1p2FGp1NLcMO4+QmJc1fxJ14UmaC
14HUJ+t05rcQ7hXhi+fSs08pnX35ZzF0BM9zSXdosiwNDt1Y1+6UgOEjrjLYAV+zMHQ5vfSJ0BtM
0uNgYq+RFSjIpYsosxSBWUdMzAg0aD8GDrE/ssG9WIkn72HtW98mq9Ew0M3T4B8FxhicvDxprHi5
Lt4QKPF+5CwFR14P39DRhNycaAYfBVVb7WuH1shDj3Mfx7T2YMCfej98qr5T7AlHoEJGrhPB0rei
2DxShh4NIegIqbsdQnZ0gWM8+eU8AurwHs04ifcVCOlQn+HcRoA4Wn6O5OwspVRNLAWgSR49TZdt
vAXiPKR4R+4cJZHThT+QhumPkaLlnGnPwOk3kePEwXdPzlPfBxgi0r+P7rtTgNtPb3rqAWCaDNBN
JViRTH5aiErfDK17eDkanQZXJpxLF7iB6j8axZsJq2EXFvSpGiG9wBJx4219cPdYG8GJzWYud5U1
XDi78khABAQ/BWPbrP98hX2Z+zkW+3oM94YYqZHB928ZtE4pv+TRmXCxhYZqLytwndlyiXgvW+Z2
uYYL+mFQxjER+IOrAyKodD6bjZtB8ywbz9apDR1dPV+qScpNvx+DqHmRQ1o4YAhbycNDW4FjauTK
Fbv4C4KrY6xyzxPodXzSiwvpigMahceKBUCTpPq6wAKAD73l2m7/V0u6q/qMRXyveSMzvo+pWWNl
d2hw+cb8L56Nn+fM8t2sWn1MXMz8Kv4Nt/OAeEPgtGQqkc85O9wFrUzdopBFF5gSt/h0tarHQyWQ
iMh6jT6U0S7I08D2G9BBJocD0CsMFz+NR9T3s14dwQhB+w6BaWEloUTC9GCNPXiKixP3h9bBr/Br
lYZMtqgRvpIDOKCdwJzf4ZVD9vHV52ypWxAnPTNzFoM1PiYyReefEgvBzTU5vvSw2b/xntlkTsV5
pMc7hXjnmLCqcGi409rdbwnZGqR4+VaL+dbckvyABHTZF6Tnf7ZYeYPS94QzEJBHB3xg2B7OaF9x
mn9aASBJcsryivGSLdPZT+rnOAwa0P1CXbtnE3OciseZtZOhlftBVLTBE4kW+ho+JX+D8vSQYwIk
adShtVCorPW65cWWvZ3urm3tA+jVRnj3O7gAl7KDN0xbbytDvvZQkC4rKJayAuVIpsv1UsdbbCh2
ZQ1nW9fWHUlsKg8GaaejnIb08UxqkQvm1vdemwk5jz1V4Hw5UZ/v/KifFmT1nwNTuIjbpjvuZ1RP
jzafMkMpQQujkmU6SsOk0jBhMfkqCi5It2V29YpOa3ej3zUMh8UsZ0Cl/CMxqh63wEysBH13hMUo
bz3vpInp37xenJKCnuJElyycbqf5BG/HIxroRpLKIDrpIQnT+7ah1jNorR0So2W+WJnFif6d+cz+
Td+chWYlykJc0sCJmXQqpM9QzkuL99nIWDTBPZnZ9GXlhOX0xrXDwvqZ0ZHqhXMKKq8LXpmadi/m
iw7K56zbhyNuHqnk6YjhRZx4m8qPtNlSqrEOuyq0vGOhXCgXgrP3ZRywHjWHXYjl7Wpd8+rEmAsX
hbsEOVJ4xL4XtFTR3LQcRid4PuficnUPZwa9KmJTWukpCr8q0NyC/nXdfe0II6yC+mgJyd4tNLMO
uAAbDNpuihZTqJh54etgC6n4SEATsvTa0CWM4ptw8U+QcDcwZLyZ+pZqEnvxINbNx5rND/Nkjus8
jrh+rPz0s49Pl6wFsqABp1/HgOQaBiK1o670OE+jcT5lDE8LXiV4a7RCOAv+MSmt+cDcGfVhAShN
g90E+dgxFnaEqx7TGDRBLIp6avQS5oScQZ8TysZdYK4u7JDFVON9ZzxQrH1jhkIZoDeevLjHYCbB
uMoAYd8Ytgr99/nIDihDFqdJEj5aI0WXoGpytmyxbUaDyvmp0mVvjZbhdx88xLR9ZRNQjYgfM7kM
LFgVO8kfzRn8p4xzE2y9wpfzXl3tq2rpduylkKrQKtL9YL1S76paxSYNo0mLloI0h0reaZi63M28
0X+jV1QaSuTXaV39D2IpOqaMk6EXKmZ/urFsgIUCe6f64ZQXOrN0tet5vVrzDNs4OPT3i+tGhbrR
ChYvXylfSTKvBnhvI79JC8M51CHYmEwMBOExUHJGNo30Ytc9Yh5fZNJoSAIW+sRarz1mypgMjSl5
0aU/kuGBAY9CXsq0SgKDSDdHInWuEgK2VxpKvcvQAvuLHa0VBbgj+x+eM5G0/4rFD7E3Y0xlozyv
DXLUaBkOmK053pJipWEaxc+Mt0ewImn2t+KNAQB4RaHDwFwWqt8B20bANXUVp7mlidntCuCitbgk
M/msVsEAVTswMZOxAK9jJQapEVVodRHF0vApWap7zdZXljgMEX4o1bw8/LQG8JAc1LC2Wsj8gBxw
BTsp5H7Ot99UFKcouRfbjarbb4UfJ6KEVkOlz+A2Ug1ywXaN5kjpjlrOzm0bRAKevw3QSh6PU30C
D83YMRaMCKohXgvML418Er8ff1oV0wVoO4171xm5Cww14e5yDsQX8bRqR2SxjuwAablJeFjGmkYa
iqbf6ngYm8fGN9bkpU4/xvsR40A3HBldXGVBXPdp1BoWMrsww0Sy8oUC9OYRn2s0Uv5c4itQlI5e
1DyP5goKBic/0C73+CR3+WW7Gy7yO6HduIo6AalHja4lBQY0JsCGSSUPh9oKTH9zsyBuEHTp4tpi
ZULw/BkSVOrtXRJiJTRkquchw4WuoUT+BfM41bJLU/qOSXtCXkDZKSrzwFWZU73S4gL7/KY7HzaZ
LKJMv5OknMW1HCTmA5iRCVixyfNdc2zPnkkWwhVrUuVXY9a5ToSJAe9z1sU+LeNPSn/loAJ4mrdW
s8D9N7jpS3KtGOEERAisuf1V0ShKheCjnn0xbKX/2Qmh3FRPvljT3Yu8mvmujSjNeAc/SQM/SDhK
QazFY09o32nFPSbdbIz4aeyo2KebXkYt6z80dALc/WSdEbz9qw+/98kyZ/wAeDpTWdJ+HhrSQmLE
TdCcA0rq6NE5wz1Oj/PPeHcTRPAHOFreuUeoRX8QgCq7yEKNCwJf4aCPyQTG0thiXq5jMFx5YK+9
IGTdy59+Sr7du4jT3FYkMTuFheIUX23xORxe8ppK+h0vAKBuz3KFBwgNBZQdkMOekzGTL33w+d8X
LMOM0a+BPWEW+xo5A41QVDKDCfq0QZoH7CdtHOOD7TXG24vzqsTmlQ7KT67Jpu0gHNWOnxRWSSKd
GAY3T8WL7pp7uHqkXV6a1c4CTyqK8wCN+AV9PvKmZARBk2mWMGfIMOdd55dEhPva2saPxjIvV6ey
VjWXCCbNIgAyXIvrAJ1gYnDMdlGaGzfSE37WMVDVIZ+zr1lmlDJqWmY+Vqfv4XXG8N0kUdd1Rt/j
VYGHyBOkLcTeQ2fsXCsrJpCkKdVBd3pYcPXolgnxiWRt8z4CciR2EfDa06O6v7ggEnBuAjseVMGW
bDbbvVHei01M9zC0rTY/kIpUsRctv0Nk+akO2ZFNxXNA/q1zoYAaHMOZ/Cp6ER7+UFk9UCD7mGXW
DL6I/bw7vaoxqnOf6I6CkD18RFKjgIL4YzV2pM2p/2Ayc3YM84UTVDJVe1D51ZFpsgPzl8OYkVP1
tWPtmAB5ejwKeB7xf5SSY+zL6WgUMPZBfzORKxzByubTVrTSVgvco12SVSCqxiDVAjw9IQG1Vl1i
TaO8xq1vaF/4iV0HdPzZVkyk06TjtaP5u/lhyCfRzgdlJgYEVLU2xoRfbNHRpftTgzpZ/kg5Fbxh
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 : entity is "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_2,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 : entity is "ip_scope_blk_mem_gen_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 : entity is "blk_mem_gen_v8_4_2,Vivado 2018.3";
end design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     10.698 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 1;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_scope_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_scope_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 2;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of clkb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK";
  attribute X_INTERFACE_PARAMETER of clkb : signal is "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of enb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of addrb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of dinb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of doutb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
  attribute X_INTERFACE_INFO of web : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB WE";
begin
U0: entity work.\design_1_ip_scope_0_0_blk_mem_gen_v8_4_2__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => enb,
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(10 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(10 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(10 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(10 downto 0),
      s_axi_rdata(31 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 : entity is "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_2,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 : entity is "ip_scope_blk_mem_gen_i1";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 : entity is "blk_mem_gen_v8_4_2,Vivado 2018.3";
end design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_doutb_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     5.6824 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_scope_blk_mem_gen_i1.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_scope_blk_mem_gen_i1.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
begin
U0: entity work.design_1_ip_scope_0_0_blk_mem_gen_v8_4_2
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => B"00000000000",
      clka => clka,
      clkb => '0',
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => B"00000000000000000000000000000000",
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => NLW_U0_doutb_UNCONNECTED(31 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(10 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(10 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(10 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(10 downto 0),
      s_axi_rdata(31 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_c_counter_binary_v12_0_12 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 10 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is "1";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is "1";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is 11;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 : entity is "c_counter_binary_v12_0_12";
end design_1_ip_scope_0_0_c_counter_binary_v12_0_12;

architecture STRUCTURE of design_1_ip_scope_0_0_c_counter_binary_v12_0_12 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_i_synth_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 1;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 11;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "1";
  attribute c_has_load of i_synth : label is 1;
  attribute c_has_thresh0 of i_synth : label is 0;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 0;
  attribute c_thresh0_value of i_synth : label is "1";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
  THRESH0 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_synth: entity work.design_1_ip_scope_0_0_c_counter_binary_v12_0_12_viv
     port map (
      CE => CE,
      CLK => CLK,
      L(10 downto 0) => L(10 downto 0),
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_i_synth_THRESH0_UNCONNECTED,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 : entity is "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_12,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 : entity is "ip_scope_c_counter_binary_v12_0_i0";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 : entity is "c_counter_binary_v12_0_12,Vivado 2018.3";
end design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0 is
  signal NLW_U0_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 11;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "1";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 1;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 0;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "1";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of LOAD : signal is "xilinx.com:signal:data:1.0 load_intf DATA";
  attribute X_INTERFACE_PARAMETER of LOAD : signal is "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of SINIT : signal is "xilinx.com:signal:data:1.0 sinit_intf DATA";
  attribute X_INTERFACE_PARAMETER of SINIT : signal is "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of L : signal is "xilinx.com:signal:data:1.0 l_intf DATA";
  attribute X_INTERFACE_PARAMETER of L : signal is "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute X_INTERFACE_PARAMETER of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.design_1_ip_scope_0_0_c_counter_binary_v12_0_12
     port map (
      CE => CE,
      CLK => CLK,
      L(10 downto 0) => L(10 downto 0),
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_U0_THRESH0_UNCONNECTED,
      UP => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xldpram is
  port (
    r9_douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r2_wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    r1_addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r3_dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xldpram : entity is "ip_scope_xldpram";
end design_1_ip_scope_0_0_ip_scope_xldpram;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xldpram is
  signal \comp0.core_instance0_n_32\ : STD_LOGIC;
  signal \comp0.core_instance0_n_33\ : STD_LOGIC;
  signal \comp0.core_instance0_n_34\ : STD_LOGIC;
  signal \comp0.core_instance0_n_35\ : STD_LOGIC;
  signal \comp0.core_instance0_n_36\ : STD_LOGIC;
  signal \comp0.core_instance0_n_37\ : STD_LOGIC;
  signal \comp0.core_instance0_n_38\ : STD_LOGIC;
  signal \comp0.core_instance0_n_39\ : STD_LOGIC;
  signal \comp0.core_instance0_n_40\ : STD_LOGIC;
  signal \comp0.core_instance0_n_41\ : STD_LOGIC;
  signal \comp0.core_instance0_n_42\ : STD_LOGIC;
  signal \comp0.core_instance0_n_43\ : STD_LOGIC;
  signal \comp0.core_instance0_n_44\ : STD_LOGIC;
  signal \comp0.core_instance0_n_45\ : STD_LOGIC;
  signal \comp0.core_instance0_n_46\ : STD_LOGIC;
  signal \comp0.core_instance0_n_47\ : STD_LOGIC;
  signal \comp0.core_instance0_n_48\ : STD_LOGIC;
  signal \comp0.core_instance0_n_49\ : STD_LOGIC;
  signal \comp0.core_instance0_n_50\ : STD_LOGIC;
  signal \comp0.core_instance0_n_51\ : STD_LOGIC;
  signal \comp0.core_instance0_n_52\ : STD_LOGIC;
  signal \comp0.core_instance0_n_53\ : STD_LOGIC;
  signal \comp0.core_instance0_n_54\ : STD_LOGIC;
  signal \comp0.core_instance0_n_55\ : STD_LOGIC;
  signal \comp0.core_instance0_n_56\ : STD_LOGIC;
  signal \comp0.core_instance0_n_57\ : STD_LOGIC;
  signal \comp0.core_instance0_n_58\ : STD_LOGIC;
  signal \comp0.core_instance0_n_59\ : STD_LOGIC;
  signal \comp0.core_instance0_n_60\ : STD_LOGIC;
  signal \comp0.core_instance0_n_61\ : STD_LOGIC;
  signal \comp0.core_instance0_n_62\ : STD_LOGIC;
  signal \comp0.core_instance0_n_63\ : STD_LOGIC;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_2,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_2,Vivado 2018.3";
begin
\comp0.core_instance0\: entity work.design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i0
     port map (
      addra(10 downto 0) => r1_addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => axibusdomain_clk,
      clkb => signaldomain_clk,
      dina(31 downto 0) => r3_dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => r9_douta(31 downto 0),
      doutb(31) => \comp0.core_instance0_n_32\,
      doutb(30) => \comp0.core_instance0_n_33\,
      doutb(29) => \comp0.core_instance0_n_34\,
      doutb(28) => \comp0.core_instance0_n_35\,
      doutb(27) => \comp0.core_instance0_n_36\,
      doutb(26) => \comp0.core_instance0_n_37\,
      doutb(25) => \comp0.core_instance0_n_38\,
      doutb(24) => \comp0.core_instance0_n_39\,
      doutb(23) => \comp0.core_instance0_n_40\,
      doutb(22) => \comp0.core_instance0_n_41\,
      doutb(21) => \comp0.core_instance0_n_42\,
      doutb(20) => \comp0.core_instance0_n_43\,
      doutb(19) => \comp0.core_instance0_n_44\,
      doutb(18) => \comp0.core_instance0_n_45\,
      doutb(17) => \comp0.core_instance0_n_46\,
      doutb(16) => \comp0.core_instance0_n_47\,
      doutb(15) => \comp0.core_instance0_n_48\,
      doutb(14) => \comp0.core_instance0_n_49\,
      doutb(13) => \comp0.core_instance0_n_50\,
      doutb(12) => \comp0.core_instance0_n_51\,
      doutb(11) => \comp0.core_instance0_n_52\,
      doutb(10) => \comp0.core_instance0_n_53\,
      doutb(9) => \comp0.core_instance0_n_54\,
      doutb(8) => \comp0.core_instance0_n_55\,
      doutb(7) => \comp0.core_instance0_n_56\,
      doutb(6) => \comp0.core_instance0_n_57\,
      doutb(5) => \comp0.core_instance0_n_58\,
      doutb(4) => \comp0.core_instance0_n_59\,
      doutb(3) => \comp0.core_instance0_n_60\,
      doutb(2) => \comp0.core_instance0_n_61\,
      doutb(1) => \comp0.core_instance0_n_62\,
      doutb(0) => \comp0.core_instance0_n_63\,
      ena => '1',
      enb => '1',
      wea(0) => r2_wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xlspram is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xlspram : entity is "ip_scope_xlspram";
end design_1_ip_scope_0_0_ip_scope_xlspram;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xlspram is
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_2,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_2,Vivado 2018.3";
begin
\comp0.core_instance0\: entity work.design_1_ip_scope_0_0_ip_scope_blk_mem_gen_i1
     port map (
      addra(10 downto 0) => Q(10 downto 0),
      clka => signaldomain_clk,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => dinb(31 downto 0),
      ena => '1',
      wea(0) => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_xlcounter_free is
  port (
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    LOAD : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_xlcounter_free : entity is "ip_scope_xlcounter_free";
end design_1_ip_scope_0_0_ip_scope_xlcounter_free;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_xlcounter_free is
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_12,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_counter_binary_v12_0_12,Vivado 2018.3";
begin
\comp0.core_instance0\: entity work.design_1_ip_scope_0_0_ip_scope_c_counter_binary_v12_0_i0
     port map (
      CE => '1',
      CLK => signaldomain_clk,
      L(10 downto 0) => B"00000000000",
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SINIT => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_delayline is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    o : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_delayline : entity is "ip_scope_delayline";
end design_1_ip_scope_0_0_ip_scope_delayline;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_delayline is
  signal counter_op_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal relational_op_net : STD_LOGIC;
begin
counter: entity work.design_1_ip_scope_0_0_ip_scope_xlcounter_free
     port map (
      LOAD => relational_op_net,
      Q(10 downto 0) => counter_op_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
relational: entity work.design_1_ip_scope_0_0_sysgen_relational_f845914c88
     port map (
      LOAD => relational_op_net,
      Q(10 downto 0) => counter_op_net(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
single_port_ram: entity work.design_1_ip_scope_0_0_ip_scope_xlspram
     port map (
      Q(10 downto 0) => counter_op_net(10 downto 0),
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_signaldomain is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \full_i_5_24_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_signaldomain : entity is "ip_scope_signaldomain";
end design_1_ip_scope_0_0_ip_scope_signaldomain;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_signaldomain is
  signal \^full_i_5_24_reg[0]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register1_n_0 : STD_LOGIC;
  signal register1_n_1 : STD_LOGIC;
  signal register1_n_2 : STD_LOGIC;
  signal register1_n_20 : STD_LOGIC;
  signal register1_n_21 : STD_LOGIC;
  signal register1_n_22 : STD_LOGIC;
  signal register1_n_3 : STD_LOGIC;
  signal register1_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register2_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register3_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register4_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register5_n_0 : STD_LOGIC;
  signal register5_n_1 : STD_LOGIC;
  signal register5_n_18 : STD_LOGIC;
  signal register5_n_19 : STD_LOGIC;
  signal register5_n_2 : STD_LOGIC;
  signal register5_n_20 : STD_LOGIC;
  signal register5_n_21 : STD_LOGIC;
  signal register5_n_22 : STD_LOGIC;
  signal register5_n_3 : STD_LOGIC;
  signal register5_q_net : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal register_q_net : STD_LOGIC;
  signal risingedgetrigger_n_0 : STD_LOGIC;
begin
  \full_i_5_24_reg[0]\(0) <= \^full_i_5_24_reg[0]\(0);
convert: entity work.\design_1_ip_scope_0_0_ip_scope_xlconvert__parameterized0\
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2\(0) => \^full_i_5_24_reg[0]\(0),
      signaldomain_clk => signaldomain_clk
    );
delayline: entity work.design_1_ip_scope_0_0_ip_scope_delayline
     port map (
      dina(31 downto 16) => register2_q_net_x0(15 downto 0),
      dina(15 downto 0) => register3_q_net_x0(15 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
leveltriggerfifocontroller: entity work.design_1_ip_scope_0_0_ip_scope_leveltriggerfifocontroller
     port map (
      DI(3) => register1_n_0,
      DI(2) => register1_n_1,
      DI(1) => register1_n_2,
      DI(0) => register1_n_3,
      Q(10 downto 0) => addrb(10 downto 0),
      S(3) => register5_n_0,
      S(2) => register5_n_1,
      S(1) => register5_n_2,
      S(0) => register5_n_3,
      \full_i_5_24_reg[0]\(0) => \^full_i_5_24_reg[0]\(0),
      register4_q_net(0) => register4_q_net(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk,
      \state_4_23_reg[1]\(3) => register5_n_22,
      \state_4_23_reg[1]\(2) => register1_n_20,
      \state_4_23_reg[1]\(1) => register1_n_21,
      \state_4_23_reg[1]\(0) => register1_n_22,
      \state_4_23_reg[1]_0\(3) => register5_n_18,
      \state_4_23_reg[1]_0\(2) => register5_n_19,
      \state_4_23_reg[1]_0\(1) => register5_n_20,
      \state_4_23_reg[1]_0\(0) => register5_n_21,
      \state_4_23_reg[1]_1\ => risingedgetrigger_n_0,
      web(0) => web(0)
    );
register1: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0\
     port map (
      DI(3) => register1_n_0,
      DI(2) => register1_n_1,
      DI(1) => register1_n_2,
      DI(0) => register1_n_3,
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2) => register1_n_20,
      \fd_prim_array[13].bit_is_0.fdre_comp\(1) => register1_n_21,
      \fd_prim_array[13].bit_is_0.fdre_comp\(0) => register1_n_22,
      o(15 downto 0) => register1_q_net_x0(15 downto 0),
      \rel_39_16_carry__0\(13 downto 0) => register5_q_net(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register2: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_1\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => register2_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register3: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_2\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => register3_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register4: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized1\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
register5: entity work.\design_1_ip_scope_0_0_ip_scope_xlregister__parameterized0_3\
     port map (
      S(3) => register5_n_0,
      S(2) => register5_n_1,
      S(1) => register5_n_2,
      S(0) => register5_n_3,
      \fd_prim_array[15].bit_is_0.fdre_comp\(3) => register5_n_18,
      \fd_prim_array[15].bit_is_0.fdre_comp\(2) => register5_n_19,
      \fd_prim_array[15].bit_is_0.fdre_comp\(1) => register5_n_20,
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => register5_n_21,
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(0) => register5_n_22,
      i(15 downto 0) => i(15 downto 0),
      o(13 downto 0) => register5_q_net(13 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => register1_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.design_1_ip_scope_0_0_ip_scope_xlregister
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
risingedgetrigger: entity work.design_1_ip_scope_0_0_ip_scope_risingedgetrigger
     port map (
      \ff_2_17_reg[0]\ => risingedgetrigger_n_0,
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope_struct is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    r9_douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r2_wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    r1_addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r3_dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope_struct : entity is "ip_scope_struct";
end design_1_ip_scope_0_0_ip_scope_struct;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope_struct is
  signal convert_dout_net_x1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal mcode_we_net : STD_LOGIC;
  signal register1_q_net : STD_LOGIC;
  signal register2_q_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal register3_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal single_port_ram_data_out_net : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slice1_y_net : STD_LOGIC_VECTOR ( 10 downto 0 );
begin
dual_port_ram: entity work.design_1_ip_scope_0_0_ip_scope_xldpram
     port map (
      addrb(10 downto 0) => slice1_y_net(10 downto 0),
      axibusdomain_clk => axibusdomain_clk,
      dinb(31 downto 0) => single_port_ram_data_out_net(31 downto 0),
      r1_addra(10 downto 0) => r1_addra(10 downto 0),
      r2_wea(0) => r2_wea(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      signaldomain_clk => signaldomain_clk,
      web(0) => mcode_we_net
    );
register1: entity work.\design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register1_q_net,
      r5_enable(0) => r5_enable(0),
      signaldomain_clk => signaldomain_clk
    );
register2: entity work.\design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized1\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => register2_q_net(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register3: entity work.\design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register3_q_net(0),
      r7_clear(0) => r7_clear(0),
      signaldomain_clk => signaldomain_clk
    );
register4: entity work.\design_1_ip_scope_0_0_ip_scope_xlAsynRegister__parameterized2_0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      full(0) => convert_dout_net_x1(0),
      r8_full(0) => r8_full(0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.design_1_ip_scope_0_0_ip_scope_xlAsynRegister
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => register_q_net(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
signaldomain: entity work.design_1_ip_scope_0_0_ip_scope_signaldomain
     port map (
      addrb(10 downto 0) => slice1_y_net(10 downto 0),
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      clear(0) => register3_q_net(0),
      dinb(31 downto 0) => single_port_ram_data_out_net(31 downto 0),
      enable(0) => register1_q_net,
      full(0) => convert_dout_net_x1(0),
      \full_i_5_24_reg[0]\(0) => full(0),
      i(15 downto 0) => register_q_net(15 downto 0),
      o(10 downto 0) => register2_q_net(10 downto 0),
      signaldomain_clk => signaldomain_clk,
      web(0) => mcode_we_net
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0_ip_scope is
  port (
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_scope_0_0_ip_scope : entity is "ip_scope";
end design_1_ip_scope_0_0_ip_scope;

architecture STRUCTURE of design_1_ip_scope_0_0_ip_scope is
  signal \<const0>\ : STD_LOGIC;
  signal r1_addra : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal r2_wea : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r3_dina : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r4_threshold : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r5_enable : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r6_delay : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal r7_clear : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r8_full : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r9_douta : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  axibusdomain_s_axi_bresp(1) <= \<const0>\;
  axibusdomain_s_axi_bresp(0) <= \<const0>\;
  axibusdomain_s_axi_rresp(1) <= \<const0>\;
  axibusdomain_s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
axibusdomain_axi_lite_interface: entity work.design_1_ip_scope_0_0_axibusdomain_axi_lite_interface
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => r5_enable(0),
      i(10 downto 0) => r6_delay(10 downto 0),
      q(0) => r8_full(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      \slv_reg_array_reg[0][0]\(0) => r7_clear(0),
      \slv_reg_array_reg[3][15]\(15 downto 0) => r4_threshold(15 downto 0),
      \slv_reg_array_reg[5][0]\(0) => r2_wea(0),
      \slv_reg_array_reg[6][10]\(10 downto 0) => r1_addra(10 downto 0)
    );
ip_scope_struct: entity work.design_1_ip_scope_0_0_ip_scope_struct
     port map (
      axibusdomain_clk => axibusdomain_clk,
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      full(0) => full(0),
      r1_addra(10 downto 0) => r1_addra(10 downto 0),
      r2_wea(0) => r2_wea(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0),
      r5_enable(0) => r5_enable(0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0),
      r7_clear(0) => r7_clear(0),
      r8_full(0) => r8_full(0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_scope_0_0 is
  port (
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_scope_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_scope_0_0 : entity is "design_1_ip_scope_0_0,ip_scope,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ip_scope_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_1_ip_scope_0_0 : entity is "sysgen";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ip_scope_0_0 : entity is "ip_scope,Vivado 2018.3";
end design_1_ip_scope_0_0;

architecture STRUCTURE of design_1_ip_scope_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of axibusdomain_aresetn : signal is "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of axibusdomain_aresetn : signal is "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_clk : signal is "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of axibusdomain_clk : signal is "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID";
  attribute X_INTERFACE_PARAMETER of axibusdomain_s_axi_rvalid : signal is "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID";
  attribute X_INTERFACE_INFO of signaldomain_clk : signal is "xilinx.com:signal:clock:1.0 signaldomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of signaldomain_clk : signal is "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB";
  attribute X_INTERFACE_INFO of ch1 : signal is "xilinx.com:signal:data:1.0 ch1 DATA";
  attribute X_INTERFACE_PARAMETER of ch1 : signal is "XIL_INTERFACENAME ch1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of ch2 : signal is "xilinx.com:signal:data:1.0 ch2 DATA";
  attribute X_INTERFACE_PARAMETER of ch2 : signal is "XIL_INTERFACENAME ch2, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of ch_trigger : signal is "xilinx.com:signal:data:1.0 ch_trigger DATA";
  attribute X_INTERFACE_PARAMETER of ch_trigger : signal is "XIL_INTERFACENAME ch_trigger, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of full : signal is "xilinx.com:signal:interrupt:1.0 full INTERRUPT";
  attribute X_INTERFACE_PARAMETER of full : signal is "XIL_INTERFACENAME full, SENSITIVITY LEVEL_HIGH, PortWidth 1";
begin
inst: entity work.design_1_ip_scope_0_0_ip_scope
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bresp(1 downto 0) => axibusdomain_s_axi_bresp(1 downto 0),
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rresp(1 downto 0) => axibusdomain_s_axi_rresp(1 downto 0),
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
