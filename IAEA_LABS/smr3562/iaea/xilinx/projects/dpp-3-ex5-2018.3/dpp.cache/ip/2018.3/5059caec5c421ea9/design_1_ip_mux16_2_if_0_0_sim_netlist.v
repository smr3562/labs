// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Thu Feb  4 23:54:48 2021
// Host        : ZBOOK running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_mux16_2_if_0_0_sim_netlist.v
// Design      : design_1_ip_mux16_2_if_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ip_mux16_2_if_0_0,ip_mux16_2_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "ip_mux16_2_v1_0,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (inp1,
    inp2,
    inp3,
    inp4,
    inp5,
    inp6,
    inp7,
    inp8,
    inp9,
    inp10,
    inp11,
    inp12,
    inp13,
    inp14,
    inp15,
    inp16,
    outp1,
    outp2,
    trig,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* x_interface_info = "iaea.org:interface:dbg_invert_and_offset:1.0 dbg_invert_and_offset_s0 adc_data" *) input [15:0]inp1;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 dc_stab" *) input [15:0]inp2;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 shaper" *) input [15:0]inp3;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 blr" *) input [15:0]inp4;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 dc_stab" *) input [15:0]inp5;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 shaper" *) input [15:0]inp6;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 blr" *) input [15:0]inp7;
  (* x_interface_info = "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_det_signal_out" *) input [15:0]inp8;
  (* x_interface_info = "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 rejectn_out" *) input [15:0]inp9;
  (* x_interface_info = "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_amp_rdy_slow_out" *) input [15:0]inp10;
  (* x_interface_info = "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_amp_rdy_fast_out" *) input [15:0]inp11;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 impulse" *) input [15:0]inp12;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 rect" *) input [15:0]inp13;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 impulse" *) input [15:0]inp14;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 rect" *) input [15:0]inp15;
  (* x_interface_info = "iaea.org:interface:dbg_invert_and_offset:1.0 dbg_invert_and_offset_s0 outp" *) input [15:0]inp16;
  output [15:0]outp1;
  output [15:0]outp2;
  output [15:0]trig;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [3:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire [15:0]inp1;
  wire [15:0]inp10;
  wire [15:0]inp11;
  wire [15:0]inp12;
  wire [15:0]inp13;
  wire [15:0]inp14;
  wire [15:0]inp15;
  wire [15:0]inp16;
  wire [15:0]inp2;
  wire [15:0]inp3;
  wire [15:0]inp4;
  wire [15:0]inp5;
  wire [15:0]inp6;
  wire [15:0]inp7;
  wire [15:0]inp8;
  wire [15:0]inp9;
  wire [15:0]outp2;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [15:0]trig;

  assign outp1[15:0] = trig;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_mux16_2_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .axi_rvalid_reg(s00_axi_rvalid),
        .inp1(inp1),
        .inp10(inp10),
        .inp11(inp11),
        .inp12(inp12),
        .inp13(inp13),
        .inp14(inp14),
        .inp15(inp15),
        .inp16(inp16),
        .inp2(inp2),
        .inp3(inp3),
        .inp4(inp4),
        .inp5(inp5),
        .inp6(inp6),
        .inp7(inp7),
        .inp8(inp8),
        .inp9(inp9),
        .outp2(outp2),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[3:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[3:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .trig(trig));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_mux16_2_v1_0
   (S_AXI_ARREADY,
    axi_rvalid_reg,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    s00_axi_rdata,
    trig,
    outp2,
    s00_axi_bvalid,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_aclk,
    s00_axi_araddr,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_wstrb,
    inp4,
    inp3,
    inp2,
    inp1,
    inp8,
    inp7,
    inp6,
    inp5,
    inp12,
    inp11,
    inp10,
    inp9,
    inp16,
    inp15,
    inp14,
    inp13,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_ARREADY;
  output axi_rvalid_reg;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output [31:0]s00_axi_rdata;
  output [15:0]trig;
  output [15:0]outp2;
  output s00_axi_bvalid;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_aclk;
  input [1:0]s00_axi_araddr;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input [15:0]inp4;
  input [15:0]inp3;
  input [15:0]inp2;
  input [15:0]inp1;
  input [15:0]inp8;
  input [15:0]inp7;
  input [15:0]inp6;
  input [15:0]inp5;
  input [15:0]inp12;
  input [15:0]inp11;
  input [15:0]inp10;
  input [15:0]inp9;
  input [15:0]inp16;
  input [15:0]inp15;
  input [15:0]inp14;
  input [15:0]inp13;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire axi_rvalid_reg;
  wire [15:0]inp1;
  wire [15:0]inp10;
  wire [15:0]inp11;
  wire [15:0]inp12;
  wire [15:0]inp13;
  wire [15:0]inp14;
  wire [15:0]inp15;
  wire [15:0]inp16;
  wire [15:0]inp2;
  wire [15:0]inp3;
  wire [15:0]inp4;
  wire [15:0]inp5;
  wire [15:0]inp6;
  wire [15:0]inp7;
  wire [15:0]inp8;
  wire [15:0]inp9;
  wire [15:0]outp2;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [15:0]trig;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_mux16_2_v1_0_S00_AXI ip_mux16_2_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .axi_rvalid_reg_0(axi_rvalid_reg),
        .inp1(inp1),
        .inp10(inp10),
        .inp11(inp11),
        .inp12(inp12),
        .inp13(inp13),
        .inp14(inp14),
        .inp15(inp15),
        .inp16(inp16),
        .inp2(inp2),
        .inp3(inp3),
        .inp4(inp4),
        .inp5(inp5),
        .inp6(inp6),
        .inp7(inp7),
        .inp8(inp8),
        .inp9(inp9),
        .outp2(outp2),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .trig(trig));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_mux16_2_v1_0_S00_AXI
   (S_AXI_ARREADY,
    axi_rvalid_reg_0,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    s00_axi_rdata,
    trig,
    outp2,
    s00_axi_bvalid,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_aclk,
    s00_axi_araddr,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_wstrb,
    inp4,
    inp3,
    inp2,
    inp1,
    inp8,
    inp7,
    inp6,
    inp5,
    inp12,
    inp11,
    inp10,
    inp9,
    inp16,
    inp15,
    inp14,
    inp13,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_ARREADY;
  output axi_rvalid_reg_0;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output [31:0]s00_axi_rdata;
  output [15:0]trig;
  output [15:0]outp2;
  output s00_axi_bvalid;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_aclk;
  input [1:0]s00_axi_araddr;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input [15:0]inp4;
  input [15:0]inp3;
  input [15:0]inp2;
  input [15:0]inp1;
  input [15:0]inp8;
  input [15:0]inp7;
  input [15:0]inp6;
  input [15:0]inp5;
  input [15:0]inp12;
  input [15:0]inp11;
  input [15:0]inp10;
  input [15:0]inp9;
  input [15:0]inp16;
  input [15:0]inp15;
  input [15:0]inp14;
  input [15:0]inp13;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire [3:2]axi_awaddr;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire [15:0]inp1;
  wire [15:0]inp10;
  wire [15:0]inp11;
  wire [15:0]inp12;
  wire [15:0]inp13;
  wire [15:0]inp14;
  wire [15:0]inp15;
  wire [15:0]inp16;
  wire [15:0]inp2;
  wire [15:0]inp3;
  wire [15:0]inp4;
  wire [15:0]inp5;
  wire [15:0]inp6;
  wire [15:0]inp7;
  wire [15:0]inp8;
  wire [15:0]inp9;
  wire [15:0]outp2;
  wire \outp2[0]_INST_0_i_1_n_0 ;
  wire \outp2[0]_INST_0_i_2_n_0 ;
  wire \outp2[0]_INST_0_i_3_n_0 ;
  wire \outp2[0]_INST_0_i_4_n_0 ;
  wire \outp2[0]_INST_0_i_5_n_0 ;
  wire \outp2[0]_INST_0_i_6_n_0 ;
  wire \outp2[10]_INST_0_i_1_n_0 ;
  wire \outp2[10]_INST_0_i_2_n_0 ;
  wire \outp2[10]_INST_0_i_3_n_0 ;
  wire \outp2[10]_INST_0_i_4_n_0 ;
  wire \outp2[10]_INST_0_i_5_n_0 ;
  wire \outp2[10]_INST_0_i_6_n_0 ;
  wire \outp2[11]_INST_0_i_1_n_0 ;
  wire \outp2[11]_INST_0_i_2_n_0 ;
  wire \outp2[11]_INST_0_i_3_n_0 ;
  wire \outp2[11]_INST_0_i_4_n_0 ;
  wire \outp2[11]_INST_0_i_5_n_0 ;
  wire \outp2[11]_INST_0_i_6_n_0 ;
  wire \outp2[12]_INST_0_i_1_n_0 ;
  wire \outp2[12]_INST_0_i_2_n_0 ;
  wire \outp2[12]_INST_0_i_3_n_0 ;
  wire \outp2[12]_INST_0_i_4_n_0 ;
  wire \outp2[12]_INST_0_i_5_n_0 ;
  wire \outp2[12]_INST_0_i_6_n_0 ;
  wire \outp2[13]_INST_0_i_1_n_0 ;
  wire \outp2[13]_INST_0_i_2_n_0 ;
  wire \outp2[13]_INST_0_i_3_n_0 ;
  wire \outp2[13]_INST_0_i_4_n_0 ;
  wire \outp2[13]_INST_0_i_5_n_0 ;
  wire \outp2[13]_INST_0_i_6_n_0 ;
  wire \outp2[14]_INST_0_i_1_n_0 ;
  wire \outp2[14]_INST_0_i_2_n_0 ;
  wire \outp2[14]_INST_0_i_3_n_0 ;
  wire \outp2[14]_INST_0_i_4_n_0 ;
  wire \outp2[14]_INST_0_i_5_n_0 ;
  wire \outp2[14]_INST_0_i_6_n_0 ;
  wire \outp2[15]_INST_0_i_1_n_0 ;
  wire \outp2[15]_INST_0_i_2_n_0 ;
  wire \outp2[15]_INST_0_i_3_n_0 ;
  wire \outp2[15]_INST_0_i_4_n_0 ;
  wire \outp2[15]_INST_0_i_5_n_0 ;
  wire \outp2[15]_INST_0_i_6_n_0 ;
  wire \outp2[1]_INST_0_i_1_n_0 ;
  wire \outp2[1]_INST_0_i_2_n_0 ;
  wire \outp2[1]_INST_0_i_3_n_0 ;
  wire \outp2[1]_INST_0_i_4_n_0 ;
  wire \outp2[1]_INST_0_i_5_n_0 ;
  wire \outp2[1]_INST_0_i_6_n_0 ;
  wire \outp2[2]_INST_0_i_1_n_0 ;
  wire \outp2[2]_INST_0_i_2_n_0 ;
  wire \outp2[2]_INST_0_i_3_n_0 ;
  wire \outp2[2]_INST_0_i_4_n_0 ;
  wire \outp2[2]_INST_0_i_5_n_0 ;
  wire \outp2[2]_INST_0_i_6_n_0 ;
  wire \outp2[3]_INST_0_i_1_n_0 ;
  wire \outp2[3]_INST_0_i_2_n_0 ;
  wire \outp2[3]_INST_0_i_3_n_0 ;
  wire \outp2[3]_INST_0_i_4_n_0 ;
  wire \outp2[3]_INST_0_i_5_n_0 ;
  wire \outp2[3]_INST_0_i_6_n_0 ;
  wire \outp2[4]_INST_0_i_1_n_0 ;
  wire \outp2[4]_INST_0_i_2_n_0 ;
  wire \outp2[4]_INST_0_i_3_n_0 ;
  wire \outp2[4]_INST_0_i_4_n_0 ;
  wire \outp2[4]_INST_0_i_5_n_0 ;
  wire \outp2[4]_INST_0_i_6_n_0 ;
  wire \outp2[5]_INST_0_i_1_n_0 ;
  wire \outp2[5]_INST_0_i_2_n_0 ;
  wire \outp2[5]_INST_0_i_3_n_0 ;
  wire \outp2[5]_INST_0_i_4_n_0 ;
  wire \outp2[5]_INST_0_i_5_n_0 ;
  wire \outp2[5]_INST_0_i_6_n_0 ;
  wire \outp2[6]_INST_0_i_1_n_0 ;
  wire \outp2[6]_INST_0_i_2_n_0 ;
  wire \outp2[6]_INST_0_i_3_n_0 ;
  wire \outp2[6]_INST_0_i_4_n_0 ;
  wire \outp2[6]_INST_0_i_5_n_0 ;
  wire \outp2[6]_INST_0_i_6_n_0 ;
  wire \outp2[7]_INST_0_i_1_n_0 ;
  wire \outp2[7]_INST_0_i_2_n_0 ;
  wire \outp2[7]_INST_0_i_3_n_0 ;
  wire \outp2[7]_INST_0_i_4_n_0 ;
  wire \outp2[7]_INST_0_i_5_n_0 ;
  wire \outp2[7]_INST_0_i_6_n_0 ;
  wire \outp2[8]_INST_0_i_1_n_0 ;
  wire \outp2[8]_INST_0_i_2_n_0 ;
  wire \outp2[8]_INST_0_i_3_n_0 ;
  wire \outp2[8]_INST_0_i_4_n_0 ;
  wire \outp2[8]_INST_0_i_5_n_0 ;
  wire \outp2[8]_INST_0_i_6_n_0 ;
  wire \outp2[9]_INST_0_i_1_n_0 ;
  wire \outp2[9]_INST_0_i_2_n_0 ;
  wire \outp2[9]_INST_0_i_3_n_0 ;
  wire \outp2[9]_INST_0_i_4_n_0 ;
  wire \outp2[9]_INST_0_i_5_n_0 ;
  wire \outp2[9]_INST_0_i_6_n_0 ;
  wire p_0_in;
  wire [1:0]p_0_in_0;
  wire [31:3]p_1_in;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [3:0]sel0;
  wire \slv_reg0_reg_n_0_[0] ;
  wire \slv_reg0_reg_n_0_[10] ;
  wire \slv_reg0_reg_n_0_[11] ;
  wire \slv_reg0_reg_n_0_[12] ;
  wire \slv_reg0_reg_n_0_[13] ;
  wire \slv_reg0_reg_n_0_[14] ;
  wire \slv_reg0_reg_n_0_[15] ;
  wire \slv_reg0_reg_n_0_[16] ;
  wire \slv_reg0_reg_n_0_[17] ;
  wire \slv_reg0_reg_n_0_[18] ;
  wire \slv_reg0_reg_n_0_[19] ;
  wire \slv_reg0_reg_n_0_[1] ;
  wire \slv_reg0_reg_n_0_[20] ;
  wire \slv_reg0_reg_n_0_[21] ;
  wire \slv_reg0_reg_n_0_[22] ;
  wire \slv_reg0_reg_n_0_[23] ;
  wire \slv_reg0_reg_n_0_[24] ;
  wire \slv_reg0_reg_n_0_[25] ;
  wire \slv_reg0_reg_n_0_[26] ;
  wire \slv_reg0_reg_n_0_[27] ;
  wire \slv_reg0_reg_n_0_[28] ;
  wire \slv_reg0_reg_n_0_[29] ;
  wire \slv_reg0_reg_n_0_[2] ;
  wire \slv_reg0_reg_n_0_[30] ;
  wire \slv_reg0_reg_n_0_[31] ;
  wire \slv_reg0_reg_n_0_[3] ;
  wire \slv_reg0_reg_n_0_[8] ;
  wire \slv_reg0_reg_n_0_[9] ;
  wire slv_reg_rden;
  wire slv_reg_wren__2;
  wire [15:0]trig;
  wire \trig[0]_INST_0_i_1_n_0 ;
  wire \trig[0]_INST_0_i_2_n_0 ;
  wire \trig[0]_INST_0_i_3_n_0 ;
  wire \trig[0]_INST_0_i_4_n_0 ;
  wire \trig[0]_INST_0_i_5_n_0 ;
  wire \trig[0]_INST_0_i_6_n_0 ;
  wire \trig[10]_INST_0_i_1_n_0 ;
  wire \trig[10]_INST_0_i_2_n_0 ;
  wire \trig[10]_INST_0_i_3_n_0 ;
  wire \trig[10]_INST_0_i_4_n_0 ;
  wire \trig[10]_INST_0_i_5_n_0 ;
  wire \trig[10]_INST_0_i_6_n_0 ;
  wire \trig[11]_INST_0_i_1_n_0 ;
  wire \trig[11]_INST_0_i_2_n_0 ;
  wire \trig[11]_INST_0_i_3_n_0 ;
  wire \trig[11]_INST_0_i_4_n_0 ;
  wire \trig[11]_INST_0_i_5_n_0 ;
  wire \trig[11]_INST_0_i_6_n_0 ;
  wire \trig[12]_INST_0_i_1_n_0 ;
  wire \trig[12]_INST_0_i_2_n_0 ;
  wire \trig[12]_INST_0_i_3_n_0 ;
  wire \trig[12]_INST_0_i_4_n_0 ;
  wire \trig[12]_INST_0_i_5_n_0 ;
  wire \trig[12]_INST_0_i_6_n_0 ;
  wire \trig[13]_INST_0_i_1_n_0 ;
  wire \trig[13]_INST_0_i_2_n_0 ;
  wire \trig[13]_INST_0_i_3_n_0 ;
  wire \trig[13]_INST_0_i_4_n_0 ;
  wire \trig[13]_INST_0_i_5_n_0 ;
  wire \trig[13]_INST_0_i_6_n_0 ;
  wire \trig[14]_INST_0_i_1_n_0 ;
  wire \trig[14]_INST_0_i_2_n_0 ;
  wire \trig[14]_INST_0_i_3_n_0 ;
  wire \trig[14]_INST_0_i_4_n_0 ;
  wire \trig[14]_INST_0_i_5_n_0 ;
  wire \trig[14]_INST_0_i_6_n_0 ;
  wire \trig[15]_INST_0_i_1_n_0 ;
  wire \trig[15]_INST_0_i_2_n_0 ;
  wire \trig[15]_INST_0_i_3_n_0 ;
  wire \trig[15]_INST_0_i_4_n_0 ;
  wire \trig[15]_INST_0_i_5_n_0 ;
  wire \trig[15]_INST_0_i_6_n_0 ;
  wire \trig[1]_INST_0_i_1_n_0 ;
  wire \trig[1]_INST_0_i_2_n_0 ;
  wire \trig[1]_INST_0_i_3_n_0 ;
  wire \trig[1]_INST_0_i_4_n_0 ;
  wire \trig[1]_INST_0_i_5_n_0 ;
  wire \trig[1]_INST_0_i_6_n_0 ;
  wire \trig[2]_INST_0_i_1_n_0 ;
  wire \trig[2]_INST_0_i_2_n_0 ;
  wire \trig[2]_INST_0_i_3_n_0 ;
  wire \trig[2]_INST_0_i_4_n_0 ;
  wire \trig[2]_INST_0_i_5_n_0 ;
  wire \trig[2]_INST_0_i_6_n_0 ;
  wire \trig[3]_INST_0_i_1_n_0 ;
  wire \trig[3]_INST_0_i_2_n_0 ;
  wire \trig[3]_INST_0_i_3_n_0 ;
  wire \trig[3]_INST_0_i_4_n_0 ;
  wire \trig[3]_INST_0_i_5_n_0 ;
  wire \trig[3]_INST_0_i_6_n_0 ;
  wire \trig[4]_INST_0_i_1_n_0 ;
  wire \trig[4]_INST_0_i_2_n_0 ;
  wire \trig[4]_INST_0_i_3_n_0 ;
  wire \trig[4]_INST_0_i_4_n_0 ;
  wire \trig[4]_INST_0_i_5_n_0 ;
  wire \trig[4]_INST_0_i_6_n_0 ;
  wire \trig[5]_INST_0_i_1_n_0 ;
  wire \trig[5]_INST_0_i_2_n_0 ;
  wire \trig[5]_INST_0_i_3_n_0 ;
  wire \trig[5]_INST_0_i_4_n_0 ;
  wire \trig[5]_INST_0_i_5_n_0 ;
  wire \trig[5]_INST_0_i_6_n_0 ;
  wire \trig[6]_INST_0_i_1_n_0 ;
  wire \trig[6]_INST_0_i_2_n_0 ;
  wire \trig[6]_INST_0_i_3_n_0 ;
  wire \trig[6]_INST_0_i_4_n_0 ;
  wire \trig[6]_INST_0_i_5_n_0 ;
  wire \trig[6]_INST_0_i_6_n_0 ;
  wire \trig[7]_INST_0_i_1_n_0 ;
  wire \trig[7]_INST_0_i_2_n_0 ;
  wire \trig[7]_INST_0_i_3_n_0 ;
  wire \trig[7]_INST_0_i_4_n_0 ;
  wire \trig[7]_INST_0_i_5_n_0 ;
  wire \trig[7]_INST_0_i_6_n_0 ;
  wire \trig[8]_INST_0_i_1_n_0 ;
  wire \trig[8]_INST_0_i_2_n_0 ;
  wire \trig[8]_INST_0_i_3_n_0 ;
  wire \trig[8]_INST_0_i_4_n_0 ;
  wire \trig[8]_INST_0_i_5_n_0 ;
  wire \trig[8]_INST_0_i_6_n_0 ;
  wire \trig[9]_INST_0_i_1_n_0 ;
  wire \trig[9]_INST_0_i_2_n_0 ;
  wire \trig[9]_INST_0_i_3_n_0 ;
  wire \trig[9]_INST_0_i_4_n_0 ;
  wire \trig[9]_INST_0_i_5_n_0 ;
  wire \trig[9]_INST_0_i_6_n_0 ;

  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(p_0_in));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(p_0_in_0[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(p_0_in_0[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(p_0_in_0[0]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(p_0_in_0[1]),
        .S(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(axi_awaddr[2]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(axi_awaddr[3]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(axi_awaddr[2]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(axi_awaddr[3]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000E000FFFFFFFF)) 
    \axi_rdata[31]_i_1 
       (.I0(p_0_in_0[0]),
        .I1(p_0_in_0[1]),
        .I2(S_AXI_ARREADY),
        .I3(s00_axi_arvalid),
        .I4(axi_rvalid_reg_0),
        .I5(s00_axi_aresetn),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_2 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(axi_rvalid_reg_0),
        .O(slv_reg_rden));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[0] ),
        .Q(s00_axi_rdata[0]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[10] ),
        .Q(s00_axi_rdata[10]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[11] ),
        .Q(s00_axi_rdata[11]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[12] ),
        .Q(s00_axi_rdata[12]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[13] ),
        .Q(s00_axi_rdata[13]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[14] ),
        .Q(s00_axi_rdata[14]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[15] ),
        .Q(s00_axi_rdata[15]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[16] ),
        .Q(s00_axi_rdata[16]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[17] ),
        .Q(s00_axi_rdata[17]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[18] ),
        .Q(s00_axi_rdata[18]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[19] ),
        .Q(s00_axi_rdata[19]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[1] ),
        .Q(s00_axi_rdata[1]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[20] ),
        .Q(s00_axi_rdata[20]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[21] ),
        .Q(s00_axi_rdata[21]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[22] ),
        .Q(s00_axi_rdata[22]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[23] ),
        .Q(s00_axi_rdata[23]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[24] ),
        .Q(s00_axi_rdata[24]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[25] ),
        .Q(s00_axi_rdata[25]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[26] ),
        .Q(s00_axi_rdata[26]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[27] ),
        .Q(s00_axi_rdata[27]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[28] ),
        .Q(s00_axi_rdata[28]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[29] ),
        .Q(s00_axi_rdata[29]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[2] ),
        .Q(s00_axi_rdata[2]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[30] ),
        .Q(s00_axi_rdata[30]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[31] ),
        .Q(s00_axi_rdata[31]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[3] ),
        .Q(s00_axi_rdata[3]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(sel0[0]),
        .Q(s00_axi_rdata[4]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(sel0[1]),
        .Q(s00_axi_rdata[5]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(sel0[2]),
        .Q(s00_axi_rdata[6]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(sel0[3]),
        .Q(s00_axi_rdata[7]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[8] ),
        .Q(s00_axi_rdata[8]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\slv_reg0_reg_n_0_[9] ),
        .Q(s00_axi_rdata[9]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(axi_rvalid_reg_0),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(axi_rvalid_reg_0),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(p_0_in));
  MUXF8 \outp2[0]_INST_0 
       (.I0(\outp2[0]_INST_0_i_1_n_0 ),
        .I1(\outp2[0]_INST_0_i_2_n_0 ),
        .O(outp2[0]),
        .S(sel0[3]));
  MUXF7 \outp2[0]_INST_0_i_1 
       (.I0(\outp2[0]_INST_0_i_3_n_0 ),
        .I1(\outp2[0]_INST_0_i_4_n_0 ),
        .O(\outp2[0]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[0]_INST_0_i_2 
       (.I0(\outp2[0]_INST_0_i_5_n_0 ),
        .I1(\outp2[0]_INST_0_i_6_n_0 ),
        .O(\outp2[0]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[0]_INST_0_i_3 
       (.I0(inp4[0]),
        .I1(inp3[0]),
        .I2(sel0[1]),
        .I3(inp2[0]),
        .I4(sel0[0]),
        .I5(inp1[0]),
        .O(\outp2[0]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[0]_INST_0_i_4 
       (.I0(inp8[0]),
        .I1(inp7[0]),
        .I2(sel0[1]),
        .I3(inp6[0]),
        .I4(sel0[0]),
        .I5(inp5[0]),
        .O(\outp2[0]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[0]_INST_0_i_5 
       (.I0(inp12[0]),
        .I1(inp11[0]),
        .I2(sel0[1]),
        .I3(inp10[0]),
        .I4(sel0[0]),
        .I5(inp9[0]),
        .O(\outp2[0]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[0]_INST_0_i_6 
       (.I0(inp16[0]),
        .I1(inp15[0]),
        .I2(sel0[1]),
        .I3(inp14[0]),
        .I4(sel0[0]),
        .I5(inp13[0]),
        .O(\outp2[0]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[10]_INST_0 
       (.I0(\outp2[10]_INST_0_i_1_n_0 ),
        .I1(\outp2[10]_INST_0_i_2_n_0 ),
        .O(outp2[10]),
        .S(sel0[3]));
  MUXF7 \outp2[10]_INST_0_i_1 
       (.I0(\outp2[10]_INST_0_i_3_n_0 ),
        .I1(\outp2[10]_INST_0_i_4_n_0 ),
        .O(\outp2[10]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[10]_INST_0_i_2 
       (.I0(\outp2[10]_INST_0_i_5_n_0 ),
        .I1(\outp2[10]_INST_0_i_6_n_0 ),
        .O(\outp2[10]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[10]_INST_0_i_3 
       (.I0(inp4[10]),
        .I1(inp3[10]),
        .I2(sel0[1]),
        .I3(inp2[10]),
        .I4(sel0[0]),
        .I5(inp1[10]),
        .O(\outp2[10]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[10]_INST_0_i_4 
       (.I0(inp8[10]),
        .I1(inp7[10]),
        .I2(sel0[1]),
        .I3(inp6[10]),
        .I4(sel0[0]),
        .I5(inp5[10]),
        .O(\outp2[10]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[10]_INST_0_i_5 
       (.I0(inp12[10]),
        .I1(inp11[10]),
        .I2(sel0[1]),
        .I3(inp10[10]),
        .I4(sel0[0]),
        .I5(inp9[10]),
        .O(\outp2[10]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[10]_INST_0_i_6 
       (.I0(inp16[10]),
        .I1(inp15[10]),
        .I2(sel0[1]),
        .I3(inp14[10]),
        .I4(sel0[0]),
        .I5(inp13[10]),
        .O(\outp2[10]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[11]_INST_0 
       (.I0(\outp2[11]_INST_0_i_1_n_0 ),
        .I1(\outp2[11]_INST_0_i_2_n_0 ),
        .O(outp2[11]),
        .S(sel0[3]));
  MUXF7 \outp2[11]_INST_0_i_1 
       (.I0(\outp2[11]_INST_0_i_3_n_0 ),
        .I1(\outp2[11]_INST_0_i_4_n_0 ),
        .O(\outp2[11]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[11]_INST_0_i_2 
       (.I0(\outp2[11]_INST_0_i_5_n_0 ),
        .I1(\outp2[11]_INST_0_i_6_n_0 ),
        .O(\outp2[11]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[11]_INST_0_i_3 
       (.I0(inp4[11]),
        .I1(inp3[11]),
        .I2(sel0[1]),
        .I3(inp2[11]),
        .I4(sel0[0]),
        .I5(inp1[11]),
        .O(\outp2[11]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[11]_INST_0_i_4 
       (.I0(inp8[11]),
        .I1(inp7[11]),
        .I2(sel0[1]),
        .I3(inp6[11]),
        .I4(sel0[0]),
        .I5(inp5[11]),
        .O(\outp2[11]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[11]_INST_0_i_5 
       (.I0(inp12[11]),
        .I1(inp11[11]),
        .I2(sel0[1]),
        .I3(inp10[11]),
        .I4(sel0[0]),
        .I5(inp9[11]),
        .O(\outp2[11]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[11]_INST_0_i_6 
       (.I0(inp16[11]),
        .I1(inp15[11]),
        .I2(sel0[1]),
        .I3(inp14[11]),
        .I4(sel0[0]),
        .I5(inp13[11]),
        .O(\outp2[11]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[12]_INST_0 
       (.I0(\outp2[12]_INST_0_i_1_n_0 ),
        .I1(\outp2[12]_INST_0_i_2_n_0 ),
        .O(outp2[12]),
        .S(sel0[3]));
  MUXF7 \outp2[12]_INST_0_i_1 
       (.I0(\outp2[12]_INST_0_i_3_n_0 ),
        .I1(\outp2[12]_INST_0_i_4_n_0 ),
        .O(\outp2[12]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[12]_INST_0_i_2 
       (.I0(\outp2[12]_INST_0_i_5_n_0 ),
        .I1(\outp2[12]_INST_0_i_6_n_0 ),
        .O(\outp2[12]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[12]_INST_0_i_3 
       (.I0(inp4[12]),
        .I1(inp3[12]),
        .I2(sel0[1]),
        .I3(inp2[12]),
        .I4(sel0[0]),
        .I5(inp1[12]),
        .O(\outp2[12]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[12]_INST_0_i_4 
       (.I0(inp8[12]),
        .I1(inp7[12]),
        .I2(sel0[1]),
        .I3(inp6[12]),
        .I4(sel0[0]),
        .I5(inp5[12]),
        .O(\outp2[12]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[12]_INST_0_i_5 
       (.I0(inp12[12]),
        .I1(inp11[12]),
        .I2(sel0[1]),
        .I3(inp10[12]),
        .I4(sel0[0]),
        .I5(inp9[12]),
        .O(\outp2[12]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[12]_INST_0_i_6 
       (.I0(inp16[12]),
        .I1(inp15[12]),
        .I2(sel0[1]),
        .I3(inp14[12]),
        .I4(sel0[0]),
        .I5(inp13[12]),
        .O(\outp2[12]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[13]_INST_0 
       (.I0(\outp2[13]_INST_0_i_1_n_0 ),
        .I1(\outp2[13]_INST_0_i_2_n_0 ),
        .O(outp2[13]),
        .S(sel0[3]));
  MUXF7 \outp2[13]_INST_0_i_1 
       (.I0(\outp2[13]_INST_0_i_3_n_0 ),
        .I1(\outp2[13]_INST_0_i_4_n_0 ),
        .O(\outp2[13]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[13]_INST_0_i_2 
       (.I0(\outp2[13]_INST_0_i_5_n_0 ),
        .I1(\outp2[13]_INST_0_i_6_n_0 ),
        .O(\outp2[13]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[13]_INST_0_i_3 
       (.I0(inp4[13]),
        .I1(inp3[13]),
        .I2(sel0[1]),
        .I3(inp2[13]),
        .I4(sel0[0]),
        .I5(inp1[13]),
        .O(\outp2[13]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[13]_INST_0_i_4 
       (.I0(inp8[13]),
        .I1(inp7[13]),
        .I2(sel0[1]),
        .I3(inp6[13]),
        .I4(sel0[0]),
        .I5(inp5[13]),
        .O(\outp2[13]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[13]_INST_0_i_5 
       (.I0(inp12[13]),
        .I1(inp11[13]),
        .I2(sel0[1]),
        .I3(inp10[13]),
        .I4(sel0[0]),
        .I5(inp9[13]),
        .O(\outp2[13]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[13]_INST_0_i_6 
       (.I0(inp16[13]),
        .I1(inp15[13]),
        .I2(sel0[1]),
        .I3(inp14[13]),
        .I4(sel0[0]),
        .I5(inp13[13]),
        .O(\outp2[13]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[14]_INST_0 
       (.I0(\outp2[14]_INST_0_i_1_n_0 ),
        .I1(\outp2[14]_INST_0_i_2_n_0 ),
        .O(outp2[14]),
        .S(sel0[3]));
  MUXF7 \outp2[14]_INST_0_i_1 
       (.I0(\outp2[14]_INST_0_i_3_n_0 ),
        .I1(\outp2[14]_INST_0_i_4_n_0 ),
        .O(\outp2[14]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[14]_INST_0_i_2 
       (.I0(\outp2[14]_INST_0_i_5_n_0 ),
        .I1(\outp2[14]_INST_0_i_6_n_0 ),
        .O(\outp2[14]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[14]_INST_0_i_3 
       (.I0(inp4[14]),
        .I1(inp3[14]),
        .I2(sel0[1]),
        .I3(inp2[14]),
        .I4(sel0[0]),
        .I5(inp1[14]),
        .O(\outp2[14]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[14]_INST_0_i_4 
       (.I0(inp8[14]),
        .I1(inp7[14]),
        .I2(sel0[1]),
        .I3(inp6[14]),
        .I4(sel0[0]),
        .I5(inp5[14]),
        .O(\outp2[14]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[14]_INST_0_i_5 
       (.I0(inp12[14]),
        .I1(inp11[14]),
        .I2(sel0[1]),
        .I3(inp10[14]),
        .I4(sel0[0]),
        .I5(inp9[14]),
        .O(\outp2[14]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[14]_INST_0_i_6 
       (.I0(inp16[14]),
        .I1(inp15[14]),
        .I2(sel0[1]),
        .I3(inp14[14]),
        .I4(sel0[0]),
        .I5(inp13[14]),
        .O(\outp2[14]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[15]_INST_0 
       (.I0(\outp2[15]_INST_0_i_1_n_0 ),
        .I1(\outp2[15]_INST_0_i_2_n_0 ),
        .O(outp2[15]),
        .S(sel0[3]));
  MUXF7 \outp2[15]_INST_0_i_1 
       (.I0(\outp2[15]_INST_0_i_3_n_0 ),
        .I1(\outp2[15]_INST_0_i_4_n_0 ),
        .O(\outp2[15]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[15]_INST_0_i_2 
       (.I0(\outp2[15]_INST_0_i_5_n_0 ),
        .I1(\outp2[15]_INST_0_i_6_n_0 ),
        .O(\outp2[15]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[15]_INST_0_i_3 
       (.I0(inp4[15]),
        .I1(inp3[15]),
        .I2(sel0[1]),
        .I3(inp2[15]),
        .I4(sel0[0]),
        .I5(inp1[15]),
        .O(\outp2[15]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[15]_INST_0_i_4 
       (.I0(inp8[15]),
        .I1(inp7[15]),
        .I2(sel0[1]),
        .I3(inp6[15]),
        .I4(sel0[0]),
        .I5(inp5[15]),
        .O(\outp2[15]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[15]_INST_0_i_5 
       (.I0(inp12[15]),
        .I1(inp11[15]),
        .I2(sel0[1]),
        .I3(inp10[15]),
        .I4(sel0[0]),
        .I5(inp9[15]),
        .O(\outp2[15]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[15]_INST_0_i_6 
       (.I0(inp16[15]),
        .I1(inp15[15]),
        .I2(sel0[1]),
        .I3(inp14[15]),
        .I4(sel0[0]),
        .I5(inp13[15]),
        .O(\outp2[15]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[1]_INST_0 
       (.I0(\outp2[1]_INST_0_i_1_n_0 ),
        .I1(\outp2[1]_INST_0_i_2_n_0 ),
        .O(outp2[1]),
        .S(sel0[3]));
  MUXF7 \outp2[1]_INST_0_i_1 
       (.I0(\outp2[1]_INST_0_i_3_n_0 ),
        .I1(\outp2[1]_INST_0_i_4_n_0 ),
        .O(\outp2[1]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[1]_INST_0_i_2 
       (.I0(\outp2[1]_INST_0_i_5_n_0 ),
        .I1(\outp2[1]_INST_0_i_6_n_0 ),
        .O(\outp2[1]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[1]_INST_0_i_3 
       (.I0(inp4[1]),
        .I1(inp3[1]),
        .I2(sel0[1]),
        .I3(inp2[1]),
        .I4(sel0[0]),
        .I5(inp1[1]),
        .O(\outp2[1]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[1]_INST_0_i_4 
       (.I0(inp8[1]),
        .I1(inp7[1]),
        .I2(sel0[1]),
        .I3(inp6[1]),
        .I4(sel0[0]),
        .I5(inp5[1]),
        .O(\outp2[1]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[1]_INST_0_i_5 
       (.I0(inp12[1]),
        .I1(inp11[1]),
        .I2(sel0[1]),
        .I3(inp10[1]),
        .I4(sel0[0]),
        .I5(inp9[1]),
        .O(\outp2[1]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[1]_INST_0_i_6 
       (.I0(inp16[1]),
        .I1(inp15[1]),
        .I2(sel0[1]),
        .I3(inp14[1]),
        .I4(sel0[0]),
        .I5(inp13[1]),
        .O(\outp2[1]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[2]_INST_0 
       (.I0(\outp2[2]_INST_0_i_1_n_0 ),
        .I1(\outp2[2]_INST_0_i_2_n_0 ),
        .O(outp2[2]),
        .S(sel0[3]));
  MUXF7 \outp2[2]_INST_0_i_1 
       (.I0(\outp2[2]_INST_0_i_3_n_0 ),
        .I1(\outp2[2]_INST_0_i_4_n_0 ),
        .O(\outp2[2]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[2]_INST_0_i_2 
       (.I0(\outp2[2]_INST_0_i_5_n_0 ),
        .I1(\outp2[2]_INST_0_i_6_n_0 ),
        .O(\outp2[2]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[2]_INST_0_i_3 
       (.I0(inp4[2]),
        .I1(inp3[2]),
        .I2(sel0[1]),
        .I3(inp2[2]),
        .I4(sel0[0]),
        .I5(inp1[2]),
        .O(\outp2[2]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[2]_INST_0_i_4 
       (.I0(inp8[2]),
        .I1(inp7[2]),
        .I2(sel0[1]),
        .I3(inp6[2]),
        .I4(sel0[0]),
        .I5(inp5[2]),
        .O(\outp2[2]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[2]_INST_0_i_5 
       (.I0(inp12[2]),
        .I1(inp11[2]),
        .I2(sel0[1]),
        .I3(inp10[2]),
        .I4(sel0[0]),
        .I5(inp9[2]),
        .O(\outp2[2]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[2]_INST_0_i_6 
       (.I0(inp16[2]),
        .I1(inp15[2]),
        .I2(sel0[1]),
        .I3(inp14[2]),
        .I4(sel0[0]),
        .I5(inp13[2]),
        .O(\outp2[2]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[3]_INST_0 
       (.I0(\outp2[3]_INST_0_i_1_n_0 ),
        .I1(\outp2[3]_INST_0_i_2_n_0 ),
        .O(outp2[3]),
        .S(sel0[3]));
  MUXF7 \outp2[3]_INST_0_i_1 
       (.I0(\outp2[3]_INST_0_i_3_n_0 ),
        .I1(\outp2[3]_INST_0_i_4_n_0 ),
        .O(\outp2[3]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[3]_INST_0_i_2 
       (.I0(\outp2[3]_INST_0_i_5_n_0 ),
        .I1(\outp2[3]_INST_0_i_6_n_0 ),
        .O(\outp2[3]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[3]_INST_0_i_3 
       (.I0(inp4[3]),
        .I1(inp3[3]),
        .I2(sel0[1]),
        .I3(inp2[3]),
        .I4(sel0[0]),
        .I5(inp1[3]),
        .O(\outp2[3]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[3]_INST_0_i_4 
       (.I0(inp8[3]),
        .I1(inp7[3]),
        .I2(sel0[1]),
        .I3(inp6[3]),
        .I4(sel0[0]),
        .I5(inp5[3]),
        .O(\outp2[3]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[3]_INST_0_i_5 
       (.I0(inp12[3]),
        .I1(inp11[3]),
        .I2(sel0[1]),
        .I3(inp10[3]),
        .I4(sel0[0]),
        .I5(inp9[3]),
        .O(\outp2[3]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[3]_INST_0_i_6 
       (.I0(inp16[3]),
        .I1(inp15[3]),
        .I2(sel0[1]),
        .I3(inp14[3]),
        .I4(sel0[0]),
        .I5(inp13[3]),
        .O(\outp2[3]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[4]_INST_0 
       (.I0(\outp2[4]_INST_0_i_1_n_0 ),
        .I1(\outp2[4]_INST_0_i_2_n_0 ),
        .O(outp2[4]),
        .S(sel0[3]));
  MUXF7 \outp2[4]_INST_0_i_1 
       (.I0(\outp2[4]_INST_0_i_3_n_0 ),
        .I1(\outp2[4]_INST_0_i_4_n_0 ),
        .O(\outp2[4]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[4]_INST_0_i_2 
       (.I0(\outp2[4]_INST_0_i_5_n_0 ),
        .I1(\outp2[4]_INST_0_i_6_n_0 ),
        .O(\outp2[4]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[4]_INST_0_i_3 
       (.I0(inp4[4]),
        .I1(inp3[4]),
        .I2(sel0[1]),
        .I3(inp2[4]),
        .I4(sel0[0]),
        .I5(inp1[4]),
        .O(\outp2[4]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[4]_INST_0_i_4 
       (.I0(inp8[4]),
        .I1(inp7[4]),
        .I2(sel0[1]),
        .I3(inp6[4]),
        .I4(sel0[0]),
        .I5(inp5[4]),
        .O(\outp2[4]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[4]_INST_0_i_5 
       (.I0(inp12[4]),
        .I1(inp11[4]),
        .I2(sel0[1]),
        .I3(inp10[4]),
        .I4(sel0[0]),
        .I5(inp9[4]),
        .O(\outp2[4]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[4]_INST_0_i_6 
       (.I0(inp16[4]),
        .I1(inp15[4]),
        .I2(sel0[1]),
        .I3(inp14[4]),
        .I4(sel0[0]),
        .I5(inp13[4]),
        .O(\outp2[4]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[5]_INST_0 
       (.I0(\outp2[5]_INST_0_i_1_n_0 ),
        .I1(\outp2[5]_INST_0_i_2_n_0 ),
        .O(outp2[5]),
        .S(sel0[3]));
  MUXF7 \outp2[5]_INST_0_i_1 
       (.I0(\outp2[5]_INST_0_i_3_n_0 ),
        .I1(\outp2[5]_INST_0_i_4_n_0 ),
        .O(\outp2[5]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[5]_INST_0_i_2 
       (.I0(\outp2[5]_INST_0_i_5_n_0 ),
        .I1(\outp2[5]_INST_0_i_6_n_0 ),
        .O(\outp2[5]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[5]_INST_0_i_3 
       (.I0(inp4[5]),
        .I1(inp3[5]),
        .I2(sel0[1]),
        .I3(inp2[5]),
        .I4(sel0[0]),
        .I5(inp1[5]),
        .O(\outp2[5]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[5]_INST_0_i_4 
       (.I0(inp8[5]),
        .I1(inp7[5]),
        .I2(sel0[1]),
        .I3(inp6[5]),
        .I4(sel0[0]),
        .I5(inp5[5]),
        .O(\outp2[5]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[5]_INST_0_i_5 
       (.I0(inp12[5]),
        .I1(inp11[5]),
        .I2(sel0[1]),
        .I3(inp10[5]),
        .I4(sel0[0]),
        .I5(inp9[5]),
        .O(\outp2[5]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[5]_INST_0_i_6 
       (.I0(inp16[5]),
        .I1(inp15[5]),
        .I2(sel0[1]),
        .I3(inp14[5]),
        .I4(sel0[0]),
        .I5(inp13[5]),
        .O(\outp2[5]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[6]_INST_0 
       (.I0(\outp2[6]_INST_0_i_1_n_0 ),
        .I1(\outp2[6]_INST_0_i_2_n_0 ),
        .O(outp2[6]),
        .S(sel0[3]));
  MUXF7 \outp2[6]_INST_0_i_1 
       (.I0(\outp2[6]_INST_0_i_3_n_0 ),
        .I1(\outp2[6]_INST_0_i_4_n_0 ),
        .O(\outp2[6]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[6]_INST_0_i_2 
       (.I0(\outp2[6]_INST_0_i_5_n_0 ),
        .I1(\outp2[6]_INST_0_i_6_n_0 ),
        .O(\outp2[6]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[6]_INST_0_i_3 
       (.I0(inp4[6]),
        .I1(inp3[6]),
        .I2(sel0[1]),
        .I3(inp2[6]),
        .I4(sel0[0]),
        .I5(inp1[6]),
        .O(\outp2[6]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[6]_INST_0_i_4 
       (.I0(inp8[6]),
        .I1(inp7[6]),
        .I2(sel0[1]),
        .I3(inp6[6]),
        .I4(sel0[0]),
        .I5(inp5[6]),
        .O(\outp2[6]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[6]_INST_0_i_5 
       (.I0(inp12[6]),
        .I1(inp11[6]),
        .I2(sel0[1]),
        .I3(inp10[6]),
        .I4(sel0[0]),
        .I5(inp9[6]),
        .O(\outp2[6]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[6]_INST_0_i_6 
       (.I0(inp16[6]),
        .I1(inp15[6]),
        .I2(sel0[1]),
        .I3(inp14[6]),
        .I4(sel0[0]),
        .I5(inp13[6]),
        .O(\outp2[6]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[7]_INST_0 
       (.I0(\outp2[7]_INST_0_i_1_n_0 ),
        .I1(\outp2[7]_INST_0_i_2_n_0 ),
        .O(outp2[7]),
        .S(sel0[3]));
  MUXF7 \outp2[7]_INST_0_i_1 
       (.I0(\outp2[7]_INST_0_i_3_n_0 ),
        .I1(\outp2[7]_INST_0_i_4_n_0 ),
        .O(\outp2[7]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[7]_INST_0_i_2 
       (.I0(\outp2[7]_INST_0_i_5_n_0 ),
        .I1(\outp2[7]_INST_0_i_6_n_0 ),
        .O(\outp2[7]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[7]_INST_0_i_3 
       (.I0(inp4[7]),
        .I1(inp3[7]),
        .I2(sel0[1]),
        .I3(inp2[7]),
        .I4(sel0[0]),
        .I5(inp1[7]),
        .O(\outp2[7]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[7]_INST_0_i_4 
       (.I0(inp8[7]),
        .I1(inp7[7]),
        .I2(sel0[1]),
        .I3(inp6[7]),
        .I4(sel0[0]),
        .I5(inp5[7]),
        .O(\outp2[7]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[7]_INST_0_i_5 
       (.I0(inp12[7]),
        .I1(inp11[7]),
        .I2(sel0[1]),
        .I3(inp10[7]),
        .I4(sel0[0]),
        .I5(inp9[7]),
        .O(\outp2[7]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[7]_INST_0_i_6 
       (.I0(inp16[7]),
        .I1(inp15[7]),
        .I2(sel0[1]),
        .I3(inp14[7]),
        .I4(sel0[0]),
        .I5(inp13[7]),
        .O(\outp2[7]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[8]_INST_0 
       (.I0(\outp2[8]_INST_0_i_1_n_0 ),
        .I1(\outp2[8]_INST_0_i_2_n_0 ),
        .O(outp2[8]),
        .S(sel0[3]));
  MUXF7 \outp2[8]_INST_0_i_1 
       (.I0(\outp2[8]_INST_0_i_3_n_0 ),
        .I1(\outp2[8]_INST_0_i_4_n_0 ),
        .O(\outp2[8]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[8]_INST_0_i_2 
       (.I0(\outp2[8]_INST_0_i_5_n_0 ),
        .I1(\outp2[8]_INST_0_i_6_n_0 ),
        .O(\outp2[8]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[8]_INST_0_i_3 
       (.I0(inp4[8]),
        .I1(inp3[8]),
        .I2(sel0[1]),
        .I3(inp2[8]),
        .I4(sel0[0]),
        .I5(inp1[8]),
        .O(\outp2[8]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[8]_INST_0_i_4 
       (.I0(inp8[8]),
        .I1(inp7[8]),
        .I2(sel0[1]),
        .I3(inp6[8]),
        .I4(sel0[0]),
        .I5(inp5[8]),
        .O(\outp2[8]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[8]_INST_0_i_5 
       (.I0(inp12[8]),
        .I1(inp11[8]),
        .I2(sel0[1]),
        .I3(inp10[8]),
        .I4(sel0[0]),
        .I5(inp9[8]),
        .O(\outp2[8]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[8]_INST_0_i_6 
       (.I0(inp16[8]),
        .I1(inp15[8]),
        .I2(sel0[1]),
        .I3(inp14[8]),
        .I4(sel0[0]),
        .I5(inp13[8]),
        .O(\outp2[8]_INST_0_i_6_n_0 ));
  MUXF8 \outp2[9]_INST_0 
       (.I0(\outp2[9]_INST_0_i_1_n_0 ),
        .I1(\outp2[9]_INST_0_i_2_n_0 ),
        .O(outp2[9]),
        .S(sel0[3]));
  MUXF7 \outp2[9]_INST_0_i_1 
       (.I0(\outp2[9]_INST_0_i_3_n_0 ),
        .I1(\outp2[9]_INST_0_i_4_n_0 ),
        .O(\outp2[9]_INST_0_i_1_n_0 ),
        .S(sel0[2]));
  MUXF7 \outp2[9]_INST_0_i_2 
       (.I0(\outp2[9]_INST_0_i_5_n_0 ),
        .I1(\outp2[9]_INST_0_i_6_n_0 ),
        .O(\outp2[9]_INST_0_i_2_n_0 ),
        .S(sel0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[9]_INST_0_i_3 
       (.I0(inp4[9]),
        .I1(inp3[9]),
        .I2(sel0[1]),
        .I3(inp2[9]),
        .I4(sel0[0]),
        .I5(inp1[9]),
        .O(\outp2[9]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[9]_INST_0_i_4 
       (.I0(inp8[9]),
        .I1(inp7[9]),
        .I2(sel0[1]),
        .I3(inp6[9]),
        .I4(sel0[0]),
        .I5(inp5[9]),
        .O(\outp2[9]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[9]_INST_0_i_5 
       (.I0(inp12[9]),
        .I1(inp11[9]),
        .I2(sel0[1]),
        .I3(inp10[9]),
        .I4(sel0[0]),
        .I5(inp9[9]),
        .O(\outp2[9]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \outp2[9]_INST_0_i_6 
       (.I0(inp16[9]),
        .I1(inp15[9]),
        .I2(sel0[1]),
        .I3(inp14[9]),
        .I4(sel0[0]),
        .I5(inp13[9]),
        .O(\outp2[9]_INST_0_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg0[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[0]),
        .O(p_1_in[3]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[3]),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg0_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg0_reg_n_0_[10] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg0_reg_n_0_[11] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg0_reg_n_0_[12] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg0_reg_n_0_[13] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg0_reg_n_0_[14] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg0_reg_n_0_[15] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg0_reg_n_0_[16] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg0_reg_n_0_[17] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg0_reg_n_0_[18] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg0_reg_n_0_[19] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[3]),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg0_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg0_reg_n_0_[20] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg0_reg_n_0_[21] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg0_reg_n_0_[22] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg0_reg_n_0_[23] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg0_reg_n_0_[24] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg0_reg_n_0_[25] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg0_reg_n_0_[26] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg0_reg_n_0_[27] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg0_reg_n_0_[28] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg0_reg_n_0_[29] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[3]),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg0_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg0_reg_n_0_[30] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg0_reg_n_0_[31] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[3]),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg0_reg_n_0_[3] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[3]),
        .D(s00_axi_wdata[4]),
        .Q(sel0[0]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[3]),
        .D(s00_axi_wdata[5]),
        .Q(sel0[1]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[3]),
        .D(s00_axi_wdata[6]),
        .Q(sel0[2]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[3]),
        .D(s00_axi_wdata[7]),
        .Q(sel0[3]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg0_reg_n_0_[8] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg0_reg_n_0_[9] ),
        .R(p_0_in));
  MUXF8 \trig[0]_INST_0 
       (.I0(\trig[0]_INST_0_i_1_n_0 ),
        .I1(\trig[0]_INST_0_i_2_n_0 ),
        .O(trig[0]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[0]_INST_0_i_1 
       (.I0(\trig[0]_INST_0_i_3_n_0 ),
        .I1(\trig[0]_INST_0_i_4_n_0 ),
        .O(\trig[0]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[0]_INST_0_i_2 
       (.I0(\trig[0]_INST_0_i_5_n_0 ),
        .I1(\trig[0]_INST_0_i_6_n_0 ),
        .O(\trig[0]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[0]_INST_0_i_3 
       (.I0(inp4[0]),
        .I1(inp3[0]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[0]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[0]),
        .O(\trig[0]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[0]_INST_0_i_4 
       (.I0(inp8[0]),
        .I1(inp7[0]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[0]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[0]),
        .O(\trig[0]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[0]_INST_0_i_5 
       (.I0(inp12[0]),
        .I1(inp11[0]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[0]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[0]),
        .O(\trig[0]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[0]_INST_0_i_6 
       (.I0(inp16[0]),
        .I1(inp15[0]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[0]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[0]),
        .O(\trig[0]_INST_0_i_6_n_0 ));
  MUXF8 \trig[10]_INST_0 
       (.I0(\trig[10]_INST_0_i_1_n_0 ),
        .I1(\trig[10]_INST_0_i_2_n_0 ),
        .O(trig[10]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[10]_INST_0_i_1 
       (.I0(\trig[10]_INST_0_i_3_n_0 ),
        .I1(\trig[10]_INST_0_i_4_n_0 ),
        .O(\trig[10]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[10]_INST_0_i_2 
       (.I0(\trig[10]_INST_0_i_5_n_0 ),
        .I1(\trig[10]_INST_0_i_6_n_0 ),
        .O(\trig[10]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[10]_INST_0_i_3 
       (.I0(inp4[10]),
        .I1(inp3[10]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[10]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[10]),
        .O(\trig[10]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[10]_INST_0_i_4 
       (.I0(inp8[10]),
        .I1(inp7[10]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[10]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[10]),
        .O(\trig[10]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[10]_INST_0_i_5 
       (.I0(inp12[10]),
        .I1(inp11[10]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[10]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[10]),
        .O(\trig[10]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[10]_INST_0_i_6 
       (.I0(inp16[10]),
        .I1(inp15[10]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[10]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[10]),
        .O(\trig[10]_INST_0_i_6_n_0 ));
  MUXF8 \trig[11]_INST_0 
       (.I0(\trig[11]_INST_0_i_1_n_0 ),
        .I1(\trig[11]_INST_0_i_2_n_0 ),
        .O(trig[11]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[11]_INST_0_i_1 
       (.I0(\trig[11]_INST_0_i_3_n_0 ),
        .I1(\trig[11]_INST_0_i_4_n_0 ),
        .O(\trig[11]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[11]_INST_0_i_2 
       (.I0(\trig[11]_INST_0_i_5_n_0 ),
        .I1(\trig[11]_INST_0_i_6_n_0 ),
        .O(\trig[11]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[11]_INST_0_i_3 
       (.I0(inp4[11]),
        .I1(inp3[11]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[11]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[11]),
        .O(\trig[11]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[11]_INST_0_i_4 
       (.I0(inp8[11]),
        .I1(inp7[11]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[11]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[11]),
        .O(\trig[11]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[11]_INST_0_i_5 
       (.I0(inp12[11]),
        .I1(inp11[11]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[11]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[11]),
        .O(\trig[11]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[11]_INST_0_i_6 
       (.I0(inp16[11]),
        .I1(inp15[11]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[11]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[11]),
        .O(\trig[11]_INST_0_i_6_n_0 ));
  MUXF8 \trig[12]_INST_0 
       (.I0(\trig[12]_INST_0_i_1_n_0 ),
        .I1(\trig[12]_INST_0_i_2_n_0 ),
        .O(trig[12]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[12]_INST_0_i_1 
       (.I0(\trig[12]_INST_0_i_3_n_0 ),
        .I1(\trig[12]_INST_0_i_4_n_0 ),
        .O(\trig[12]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[12]_INST_0_i_2 
       (.I0(\trig[12]_INST_0_i_5_n_0 ),
        .I1(\trig[12]_INST_0_i_6_n_0 ),
        .O(\trig[12]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[12]_INST_0_i_3 
       (.I0(inp4[12]),
        .I1(inp3[12]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[12]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[12]),
        .O(\trig[12]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[12]_INST_0_i_4 
       (.I0(inp8[12]),
        .I1(inp7[12]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[12]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[12]),
        .O(\trig[12]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[12]_INST_0_i_5 
       (.I0(inp12[12]),
        .I1(inp11[12]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[12]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[12]),
        .O(\trig[12]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[12]_INST_0_i_6 
       (.I0(inp16[12]),
        .I1(inp15[12]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[12]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[12]),
        .O(\trig[12]_INST_0_i_6_n_0 ));
  MUXF8 \trig[13]_INST_0 
       (.I0(\trig[13]_INST_0_i_1_n_0 ),
        .I1(\trig[13]_INST_0_i_2_n_0 ),
        .O(trig[13]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[13]_INST_0_i_1 
       (.I0(\trig[13]_INST_0_i_3_n_0 ),
        .I1(\trig[13]_INST_0_i_4_n_0 ),
        .O(\trig[13]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[13]_INST_0_i_2 
       (.I0(\trig[13]_INST_0_i_5_n_0 ),
        .I1(\trig[13]_INST_0_i_6_n_0 ),
        .O(\trig[13]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[13]_INST_0_i_3 
       (.I0(inp4[13]),
        .I1(inp3[13]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[13]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[13]),
        .O(\trig[13]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[13]_INST_0_i_4 
       (.I0(inp8[13]),
        .I1(inp7[13]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[13]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[13]),
        .O(\trig[13]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[13]_INST_0_i_5 
       (.I0(inp12[13]),
        .I1(inp11[13]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[13]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[13]),
        .O(\trig[13]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[13]_INST_0_i_6 
       (.I0(inp16[13]),
        .I1(inp15[13]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[13]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[13]),
        .O(\trig[13]_INST_0_i_6_n_0 ));
  MUXF8 \trig[14]_INST_0 
       (.I0(\trig[14]_INST_0_i_1_n_0 ),
        .I1(\trig[14]_INST_0_i_2_n_0 ),
        .O(trig[14]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[14]_INST_0_i_1 
       (.I0(\trig[14]_INST_0_i_3_n_0 ),
        .I1(\trig[14]_INST_0_i_4_n_0 ),
        .O(\trig[14]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[14]_INST_0_i_2 
       (.I0(\trig[14]_INST_0_i_5_n_0 ),
        .I1(\trig[14]_INST_0_i_6_n_0 ),
        .O(\trig[14]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[14]_INST_0_i_3 
       (.I0(inp4[14]),
        .I1(inp3[14]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[14]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[14]),
        .O(\trig[14]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[14]_INST_0_i_4 
       (.I0(inp8[14]),
        .I1(inp7[14]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[14]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[14]),
        .O(\trig[14]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[14]_INST_0_i_5 
       (.I0(inp12[14]),
        .I1(inp11[14]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[14]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[14]),
        .O(\trig[14]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[14]_INST_0_i_6 
       (.I0(inp16[14]),
        .I1(inp15[14]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[14]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[14]),
        .O(\trig[14]_INST_0_i_6_n_0 ));
  MUXF8 \trig[15]_INST_0 
       (.I0(\trig[15]_INST_0_i_1_n_0 ),
        .I1(\trig[15]_INST_0_i_2_n_0 ),
        .O(trig[15]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[15]_INST_0_i_1 
       (.I0(\trig[15]_INST_0_i_3_n_0 ),
        .I1(\trig[15]_INST_0_i_4_n_0 ),
        .O(\trig[15]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[15]_INST_0_i_2 
       (.I0(\trig[15]_INST_0_i_5_n_0 ),
        .I1(\trig[15]_INST_0_i_6_n_0 ),
        .O(\trig[15]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[15]_INST_0_i_3 
       (.I0(inp4[15]),
        .I1(inp3[15]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[15]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[15]),
        .O(\trig[15]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[15]_INST_0_i_4 
       (.I0(inp8[15]),
        .I1(inp7[15]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[15]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[15]),
        .O(\trig[15]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[15]_INST_0_i_5 
       (.I0(inp12[15]),
        .I1(inp11[15]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[15]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[15]),
        .O(\trig[15]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[15]_INST_0_i_6 
       (.I0(inp16[15]),
        .I1(inp15[15]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[15]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[15]),
        .O(\trig[15]_INST_0_i_6_n_0 ));
  MUXF8 \trig[1]_INST_0 
       (.I0(\trig[1]_INST_0_i_1_n_0 ),
        .I1(\trig[1]_INST_0_i_2_n_0 ),
        .O(trig[1]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[1]_INST_0_i_1 
       (.I0(\trig[1]_INST_0_i_3_n_0 ),
        .I1(\trig[1]_INST_0_i_4_n_0 ),
        .O(\trig[1]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[1]_INST_0_i_2 
       (.I0(\trig[1]_INST_0_i_5_n_0 ),
        .I1(\trig[1]_INST_0_i_6_n_0 ),
        .O(\trig[1]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[1]_INST_0_i_3 
       (.I0(inp4[1]),
        .I1(inp3[1]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[1]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[1]),
        .O(\trig[1]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[1]_INST_0_i_4 
       (.I0(inp8[1]),
        .I1(inp7[1]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[1]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[1]),
        .O(\trig[1]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[1]_INST_0_i_5 
       (.I0(inp12[1]),
        .I1(inp11[1]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[1]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[1]),
        .O(\trig[1]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[1]_INST_0_i_6 
       (.I0(inp16[1]),
        .I1(inp15[1]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[1]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[1]),
        .O(\trig[1]_INST_0_i_6_n_0 ));
  MUXF8 \trig[2]_INST_0 
       (.I0(\trig[2]_INST_0_i_1_n_0 ),
        .I1(\trig[2]_INST_0_i_2_n_0 ),
        .O(trig[2]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[2]_INST_0_i_1 
       (.I0(\trig[2]_INST_0_i_3_n_0 ),
        .I1(\trig[2]_INST_0_i_4_n_0 ),
        .O(\trig[2]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[2]_INST_0_i_2 
       (.I0(\trig[2]_INST_0_i_5_n_0 ),
        .I1(\trig[2]_INST_0_i_6_n_0 ),
        .O(\trig[2]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[2]_INST_0_i_3 
       (.I0(inp4[2]),
        .I1(inp3[2]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[2]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[2]),
        .O(\trig[2]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[2]_INST_0_i_4 
       (.I0(inp8[2]),
        .I1(inp7[2]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[2]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[2]),
        .O(\trig[2]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[2]_INST_0_i_5 
       (.I0(inp12[2]),
        .I1(inp11[2]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[2]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[2]),
        .O(\trig[2]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[2]_INST_0_i_6 
       (.I0(inp16[2]),
        .I1(inp15[2]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[2]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[2]),
        .O(\trig[2]_INST_0_i_6_n_0 ));
  MUXF8 \trig[3]_INST_0 
       (.I0(\trig[3]_INST_0_i_1_n_0 ),
        .I1(\trig[3]_INST_0_i_2_n_0 ),
        .O(trig[3]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[3]_INST_0_i_1 
       (.I0(\trig[3]_INST_0_i_3_n_0 ),
        .I1(\trig[3]_INST_0_i_4_n_0 ),
        .O(\trig[3]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[3]_INST_0_i_2 
       (.I0(\trig[3]_INST_0_i_5_n_0 ),
        .I1(\trig[3]_INST_0_i_6_n_0 ),
        .O(\trig[3]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[3]_INST_0_i_3 
       (.I0(inp4[3]),
        .I1(inp3[3]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[3]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[3]),
        .O(\trig[3]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[3]_INST_0_i_4 
       (.I0(inp8[3]),
        .I1(inp7[3]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[3]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[3]),
        .O(\trig[3]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[3]_INST_0_i_5 
       (.I0(inp12[3]),
        .I1(inp11[3]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[3]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[3]),
        .O(\trig[3]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[3]_INST_0_i_6 
       (.I0(inp16[3]),
        .I1(inp15[3]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[3]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[3]),
        .O(\trig[3]_INST_0_i_6_n_0 ));
  MUXF8 \trig[4]_INST_0 
       (.I0(\trig[4]_INST_0_i_1_n_0 ),
        .I1(\trig[4]_INST_0_i_2_n_0 ),
        .O(trig[4]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[4]_INST_0_i_1 
       (.I0(\trig[4]_INST_0_i_3_n_0 ),
        .I1(\trig[4]_INST_0_i_4_n_0 ),
        .O(\trig[4]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[4]_INST_0_i_2 
       (.I0(\trig[4]_INST_0_i_5_n_0 ),
        .I1(\trig[4]_INST_0_i_6_n_0 ),
        .O(\trig[4]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[4]_INST_0_i_3 
       (.I0(inp4[4]),
        .I1(inp3[4]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[4]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[4]),
        .O(\trig[4]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[4]_INST_0_i_4 
       (.I0(inp8[4]),
        .I1(inp7[4]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[4]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[4]),
        .O(\trig[4]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[4]_INST_0_i_5 
       (.I0(inp12[4]),
        .I1(inp11[4]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[4]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[4]),
        .O(\trig[4]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[4]_INST_0_i_6 
       (.I0(inp16[4]),
        .I1(inp15[4]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[4]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[4]),
        .O(\trig[4]_INST_0_i_6_n_0 ));
  MUXF8 \trig[5]_INST_0 
       (.I0(\trig[5]_INST_0_i_1_n_0 ),
        .I1(\trig[5]_INST_0_i_2_n_0 ),
        .O(trig[5]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[5]_INST_0_i_1 
       (.I0(\trig[5]_INST_0_i_3_n_0 ),
        .I1(\trig[5]_INST_0_i_4_n_0 ),
        .O(\trig[5]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[5]_INST_0_i_2 
       (.I0(\trig[5]_INST_0_i_5_n_0 ),
        .I1(\trig[5]_INST_0_i_6_n_0 ),
        .O(\trig[5]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[5]_INST_0_i_3 
       (.I0(inp4[5]),
        .I1(inp3[5]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[5]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[5]),
        .O(\trig[5]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[5]_INST_0_i_4 
       (.I0(inp8[5]),
        .I1(inp7[5]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[5]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[5]),
        .O(\trig[5]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[5]_INST_0_i_5 
       (.I0(inp12[5]),
        .I1(inp11[5]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[5]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[5]),
        .O(\trig[5]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[5]_INST_0_i_6 
       (.I0(inp16[5]),
        .I1(inp15[5]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[5]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[5]),
        .O(\trig[5]_INST_0_i_6_n_0 ));
  MUXF8 \trig[6]_INST_0 
       (.I0(\trig[6]_INST_0_i_1_n_0 ),
        .I1(\trig[6]_INST_0_i_2_n_0 ),
        .O(trig[6]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[6]_INST_0_i_1 
       (.I0(\trig[6]_INST_0_i_3_n_0 ),
        .I1(\trig[6]_INST_0_i_4_n_0 ),
        .O(\trig[6]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[6]_INST_0_i_2 
       (.I0(\trig[6]_INST_0_i_5_n_0 ),
        .I1(\trig[6]_INST_0_i_6_n_0 ),
        .O(\trig[6]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[6]_INST_0_i_3 
       (.I0(inp4[6]),
        .I1(inp3[6]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[6]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[6]),
        .O(\trig[6]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[6]_INST_0_i_4 
       (.I0(inp8[6]),
        .I1(inp7[6]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[6]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[6]),
        .O(\trig[6]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[6]_INST_0_i_5 
       (.I0(inp12[6]),
        .I1(inp11[6]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[6]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[6]),
        .O(\trig[6]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[6]_INST_0_i_6 
       (.I0(inp16[6]),
        .I1(inp15[6]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[6]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[6]),
        .O(\trig[6]_INST_0_i_6_n_0 ));
  MUXF8 \trig[7]_INST_0 
       (.I0(\trig[7]_INST_0_i_1_n_0 ),
        .I1(\trig[7]_INST_0_i_2_n_0 ),
        .O(trig[7]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[7]_INST_0_i_1 
       (.I0(\trig[7]_INST_0_i_3_n_0 ),
        .I1(\trig[7]_INST_0_i_4_n_0 ),
        .O(\trig[7]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[7]_INST_0_i_2 
       (.I0(\trig[7]_INST_0_i_5_n_0 ),
        .I1(\trig[7]_INST_0_i_6_n_0 ),
        .O(\trig[7]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[7]_INST_0_i_3 
       (.I0(inp4[7]),
        .I1(inp3[7]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[7]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[7]),
        .O(\trig[7]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[7]_INST_0_i_4 
       (.I0(inp8[7]),
        .I1(inp7[7]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[7]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[7]),
        .O(\trig[7]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[7]_INST_0_i_5 
       (.I0(inp12[7]),
        .I1(inp11[7]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[7]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[7]),
        .O(\trig[7]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[7]_INST_0_i_6 
       (.I0(inp16[7]),
        .I1(inp15[7]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[7]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[7]),
        .O(\trig[7]_INST_0_i_6_n_0 ));
  MUXF8 \trig[8]_INST_0 
       (.I0(\trig[8]_INST_0_i_1_n_0 ),
        .I1(\trig[8]_INST_0_i_2_n_0 ),
        .O(trig[8]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[8]_INST_0_i_1 
       (.I0(\trig[8]_INST_0_i_3_n_0 ),
        .I1(\trig[8]_INST_0_i_4_n_0 ),
        .O(\trig[8]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[8]_INST_0_i_2 
       (.I0(\trig[8]_INST_0_i_5_n_0 ),
        .I1(\trig[8]_INST_0_i_6_n_0 ),
        .O(\trig[8]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[8]_INST_0_i_3 
       (.I0(inp4[8]),
        .I1(inp3[8]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[8]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[8]),
        .O(\trig[8]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[8]_INST_0_i_4 
       (.I0(inp8[8]),
        .I1(inp7[8]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[8]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[8]),
        .O(\trig[8]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[8]_INST_0_i_5 
       (.I0(inp12[8]),
        .I1(inp11[8]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[8]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[8]),
        .O(\trig[8]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[8]_INST_0_i_6 
       (.I0(inp16[8]),
        .I1(inp15[8]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[8]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[8]),
        .O(\trig[8]_INST_0_i_6_n_0 ));
  MUXF8 \trig[9]_INST_0 
       (.I0(\trig[9]_INST_0_i_1_n_0 ),
        .I1(\trig[9]_INST_0_i_2_n_0 ),
        .O(trig[9]),
        .S(\slv_reg0_reg_n_0_[3] ));
  MUXF7 \trig[9]_INST_0_i_1 
       (.I0(\trig[9]_INST_0_i_3_n_0 ),
        .I1(\trig[9]_INST_0_i_4_n_0 ),
        .O(\trig[9]_INST_0_i_1_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  MUXF7 \trig[9]_INST_0_i_2 
       (.I0(\trig[9]_INST_0_i_5_n_0 ),
        .I1(\trig[9]_INST_0_i_6_n_0 ),
        .O(\trig[9]_INST_0_i_2_n_0 ),
        .S(\slv_reg0_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[9]_INST_0_i_3 
       (.I0(inp4[9]),
        .I1(inp3[9]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp2[9]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp1[9]),
        .O(\trig[9]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[9]_INST_0_i_4 
       (.I0(inp8[9]),
        .I1(inp7[9]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp6[9]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp5[9]),
        .O(\trig[9]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[9]_INST_0_i_5 
       (.I0(inp12[9]),
        .I1(inp11[9]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp10[9]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp9[9]),
        .O(\trig[9]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \trig[9]_INST_0_i_6 
       (.I0(inp16[9]),
        .I1(inp15[9]),
        .I2(\slv_reg0_reg_n_0_[1] ),
        .I3(inp14[9]),
        .I4(\slv_reg0_reg_n_0_[0] ),
        .I5(inp13[9]),
        .O(\trig[9]_INST_0_i_6_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
