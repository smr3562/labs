// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Mon Feb 15 14:40:48 2021
// Host        : ZBOOK running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_shaper_0_0_stub.v
// Design      : design_1_ip_shaper_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ip_shaper,Vivado 2018.3" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(data_in, axi_clk_domain_clk, 
  filter_clk_domain_clk, axi_clk_domain_aresetn, axi_clk_domain_s_axi_awaddr, 
  axi_clk_domain_s_axi_awvalid, axi_clk_domain_s_axi_wdata, axi_clk_domain_s_axi_wstrb, 
  axi_clk_domain_s_axi_wvalid, axi_clk_domain_s_axi_bready, 
  axi_clk_domain_s_axi_araddr, axi_clk_domain_s_axi_arvalid, 
  axi_clk_domain_s_axi_rready, impulse_out, rect_out, shaper_out, 
  axi_clk_domain_s_axi_awready, axi_clk_domain_s_axi_wready, 
  axi_clk_domain_s_axi_bresp, axi_clk_domain_s_axi_bvalid, 
  axi_clk_domain_s_axi_arready, axi_clk_domain_s_axi_rdata, axi_clk_domain_s_axi_rresp, 
  axi_clk_domain_s_axi_rvalid)
/* synthesis syn_black_box black_box_pad_pin="data_in[15:0],axi_clk_domain_clk,filter_clk_domain_clk,axi_clk_domain_aresetn,axi_clk_domain_s_axi_awaddr[4:0],axi_clk_domain_s_axi_awvalid,axi_clk_domain_s_axi_wdata[31:0],axi_clk_domain_s_axi_wstrb[3:0],axi_clk_domain_s_axi_wvalid,axi_clk_domain_s_axi_bready,axi_clk_domain_s_axi_araddr[4:0],axi_clk_domain_s_axi_arvalid,axi_clk_domain_s_axi_rready,impulse_out[15:0],rect_out[15:0],shaper_out[15:0],axi_clk_domain_s_axi_awready,axi_clk_domain_s_axi_wready,axi_clk_domain_s_axi_bresp[1:0],axi_clk_domain_s_axi_bvalid,axi_clk_domain_s_axi_arready,axi_clk_domain_s_axi_rdata[31:0],axi_clk_domain_s_axi_rresp[1:0],axi_clk_domain_s_axi_rvalid" */;
  input [15:0]data_in;
  input axi_clk_domain_clk;
  input filter_clk_domain_clk;
  input axi_clk_domain_aresetn;
  input [4:0]axi_clk_domain_s_axi_awaddr;
  input axi_clk_domain_s_axi_awvalid;
  input [31:0]axi_clk_domain_s_axi_wdata;
  input [3:0]axi_clk_domain_s_axi_wstrb;
  input axi_clk_domain_s_axi_wvalid;
  input axi_clk_domain_s_axi_bready;
  input [4:0]axi_clk_domain_s_axi_araddr;
  input axi_clk_domain_s_axi_arvalid;
  input axi_clk_domain_s_axi_rready;
  output [15:0]impulse_out;
  output [15:0]rect_out;
  output [15:0]shaper_out;
  output axi_clk_domain_s_axi_awready;
  output axi_clk_domain_s_axi_wready;
  output [1:0]axi_clk_domain_s_axi_bresp;
  output axi_clk_domain_s_axi_bvalid;
  output axi_clk_domain_s_axi_arready;
  output [31:0]axi_clk_domain_s_axi_rdata;
  output [1:0]axi_clk_domain_s_axi_rresp;
  output axi_clk_domain_s_axi_rvalid;
endmodule
