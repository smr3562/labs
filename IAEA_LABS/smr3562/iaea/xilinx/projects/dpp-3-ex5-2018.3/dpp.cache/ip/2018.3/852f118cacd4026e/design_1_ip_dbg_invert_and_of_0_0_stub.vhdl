-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Thu Feb  4 23:54:45 2021
-- Host        : ZBOOK running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_dbg_invert_and_of_0_0_stub.vhdl
-- Design      : design_1_ip_dbg_invert_and_of_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    adc_data : in STD_LOGIC_VECTOR ( 13 downto 0 );
    outp : in STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    outp_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "adc_data[13:0],outp[15:0],adc_data_out[15:0],outp_out[15:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "top,Vivado 2018.3";
begin
end;
