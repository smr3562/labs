-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Thu Feb  4 23:56:33 2021
-- Host        : ZBOOK running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_dbg_term_pulse_co_0_3_sim_netlist.vhdl
-- Design      : design_1_ip_dbg_term_pulse_co_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    impulse_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rect_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    shaper_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    blr_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    dc_stab_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    dc_stab_acc_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ip_dbg_term_pulse_co_0_3,top,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "top,Vivado 2018.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of blr_out : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast blr";
  attribute x_interface_info of dc_stab_acc_out : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast dc_stab_acc";
  attribute x_interface_info of dc_stab_out : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast dc_stab";
  attribute x_interface_info of impulse_out : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast impulse";
  attribute x_interface_info of rect_out : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast rect";
  attribute x_interface_info of shaper_out : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast shaper";
begin
  blr_out(15) <= \<const0>\;
  blr_out(14) <= \<const0>\;
  blr_out(13) <= \<const0>\;
  blr_out(12) <= \<const0>\;
  blr_out(11) <= \<const0>\;
  blr_out(10) <= \<const0>\;
  blr_out(9) <= \<const0>\;
  blr_out(8) <= \<const0>\;
  blr_out(7) <= \<const0>\;
  blr_out(6) <= \<const0>\;
  blr_out(5) <= \<const0>\;
  blr_out(4) <= \<const0>\;
  blr_out(3) <= \<const0>\;
  blr_out(2) <= \<const0>\;
  blr_out(1) <= \<const0>\;
  blr_out(0) <= \<const0>\;
  dc_stab_acc_out(15) <= \<const0>\;
  dc_stab_acc_out(14) <= \<const0>\;
  dc_stab_acc_out(13) <= \<const0>\;
  dc_stab_acc_out(12) <= \<const0>\;
  dc_stab_acc_out(11) <= \<const0>\;
  dc_stab_acc_out(10) <= \<const0>\;
  dc_stab_acc_out(9) <= \<const0>\;
  dc_stab_acc_out(8) <= \<const0>\;
  dc_stab_acc_out(7) <= \<const0>\;
  dc_stab_acc_out(6) <= \<const0>\;
  dc_stab_acc_out(5) <= \<const0>\;
  dc_stab_acc_out(4) <= \<const0>\;
  dc_stab_acc_out(3) <= \<const0>\;
  dc_stab_acc_out(2) <= \<const0>\;
  dc_stab_acc_out(1) <= \<const0>\;
  dc_stab_acc_out(0) <= \<const0>\;
  dc_stab_out(15) <= \<const0>\;
  dc_stab_out(14) <= \<const0>\;
  dc_stab_out(13) <= \<const0>\;
  dc_stab_out(12) <= \<const0>\;
  dc_stab_out(11) <= \<const0>\;
  dc_stab_out(10) <= \<const0>\;
  dc_stab_out(9) <= \<const0>\;
  dc_stab_out(8) <= \<const0>\;
  dc_stab_out(7) <= \<const0>\;
  dc_stab_out(6) <= \<const0>\;
  dc_stab_out(5) <= \<const0>\;
  dc_stab_out(4) <= \<const0>\;
  dc_stab_out(3) <= \<const0>\;
  dc_stab_out(2) <= \<const0>\;
  dc_stab_out(1) <= \<const0>\;
  dc_stab_out(0) <= \<const0>\;
  impulse_out(15) <= \<const0>\;
  impulse_out(14) <= \<const0>\;
  impulse_out(13) <= \<const0>\;
  impulse_out(12) <= \<const0>\;
  impulse_out(11) <= \<const0>\;
  impulse_out(10) <= \<const0>\;
  impulse_out(9) <= \<const0>\;
  impulse_out(8) <= \<const0>\;
  impulse_out(7) <= \<const0>\;
  impulse_out(6) <= \<const0>\;
  impulse_out(5) <= \<const0>\;
  impulse_out(4) <= \<const0>\;
  impulse_out(3) <= \<const0>\;
  impulse_out(2) <= \<const0>\;
  impulse_out(1) <= \<const0>\;
  impulse_out(0) <= \<const0>\;
  rect_out(15) <= \<const0>\;
  rect_out(14) <= \<const0>\;
  rect_out(13) <= \<const0>\;
  rect_out(12) <= \<const0>\;
  rect_out(11) <= \<const0>\;
  rect_out(10) <= \<const0>\;
  rect_out(9) <= \<const0>\;
  rect_out(8) <= \<const0>\;
  rect_out(7) <= \<const0>\;
  rect_out(6) <= \<const0>\;
  rect_out(5) <= \<const0>\;
  rect_out(4) <= \<const0>\;
  rect_out(3) <= \<const0>\;
  rect_out(2) <= \<const0>\;
  rect_out(1) <= \<const0>\;
  rect_out(0) <= \<const0>\;
  shaper_out(15) <= \<const0>\;
  shaper_out(14) <= \<const0>\;
  shaper_out(13) <= \<const0>\;
  shaper_out(12) <= \<const0>\;
  shaper_out(11) <= \<const0>\;
  shaper_out(10) <= \<const0>\;
  shaper_out(9) <= \<const0>\;
  shaper_out(8) <= \<const0>\;
  shaper_out(7) <= \<const0>\;
  shaper_out(6) <= \<const0>\;
  shaper_out(5) <= \<const0>\;
  shaper_out(4) <= \<const0>\;
  shaper_out(3) <= \<const0>\;
  shaper_out(2) <= \<const0>\;
  shaper_out(1) <= \<const0>\;
  shaper_out(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
end STRUCTURE;
