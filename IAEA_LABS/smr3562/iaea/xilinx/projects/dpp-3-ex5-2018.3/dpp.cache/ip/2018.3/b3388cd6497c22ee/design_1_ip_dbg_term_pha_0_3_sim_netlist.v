// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Mon Feb 15 11:27:35 2021
// Host        : ZBOOK running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_dbg_term_pha_0_3_sim_netlist.v
// Design      : design_1_ip_dbg_term_pha_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ip_dbg_term_pha_0_3,top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "top,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (peak_amp_rdy_fast_out,
    rejectn_out,
    peak_amp_rdy_slow_out,
    peak_det_signal_out);
  (* x_interface_info = "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_amp_rdy_fast_out" *) output [15:0]peak_amp_rdy_fast_out;
  (* x_interface_info = "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 rejectn_out" *) output [15:0]rejectn_out;
  (* x_interface_info = "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_amp_rdy_slow_out" *) output [15:0]peak_amp_rdy_slow_out;
  (* x_interface_info = "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_det_signal_out" *) output [15:0]peak_det_signal_out;

  wire \<const0> ;

  assign peak_amp_rdy_fast_out[15] = \<const0> ;
  assign peak_amp_rdy_fast_out[14] = \<const0> ;
  assign peak_amp_rdy_fast_out[13] = \<const0> ;
  assign peak_amp_rdy_fast_out[12] = \<const0> ;
  assign peak_amp_rdy_fast_out[11] = \<const0> ;
  assign peak_amp_rdy_fast_out[10] = \<const0> ;
  assign peak_amp_rdy_fast_out[9] = \<const0> ;
  assign peak_amp_rdy_fast_out[8] = \<const0> ;
  assign peak_amp_rdy_fast_out[7] = \<const0> ;
  assign peak_amp_rdy_fast_out[6] = \<const0> ;
  assign peak_amp_rdy_fast_out[5] = \<const0> ;
  assign peak_amp_rdy_fast_out[4] = \<const0> ;
  assign peak_amp_rdy_fast_out[3] = \<const0> ;
  assign peak_amp_rdy_fast_out[2] = \<const0> ;
  assign peak_amp_rdy_fast_out[1] = \<const0> ;
  assign peak_amp_rdy_fast_out[0] = \<const0> ;
  assign peak_amp_rdy_slow_out[15] = \<const0> ;
  assign peak_amp_rdy_slow_out[14] = \<const0> ;
  assign peak_amp_rdy_slow_out[13] = \<const0> ;
  assign peak_amp_rdy_slow_out[12] = \<const0> ;
  assign peak_amp_rdy_slow_out[11] = \<const0> ;
  assign peak_amp_rdy_slow_out[10] = \<const0> ;
  assign peak_amp_rdy_slow_out[9] = \<const0> ;
  assign peak_amp_rdy_slow_out[8] = \<const0> ;
  assign peak_amp_rdy_slow_out[7] = \<const0> ;
  assign peak_amp_rdy_slow_out[6] = \<const0> ;
  assign peak_amp_rdy_slow_out[5] = \<const0> ;
  assign peak_amp_rdy_slow_out[4] = \<const0> ;
  assign peak_amp_rdy_slow_out[3] = \<const0> ;
  assign peak_amp_rdy_slow_out[2] = \<const0> ;
  assign peak_amp_rdy_slow_out[1] = \<const0> ;
  assign peak_amp_rdy_slow_out[0] = \<const0> ;
  assign peak_det_signal_out[15] = \<const0> ;
  assign peak_det_signal_out[14] = \<const0> ;
  assign peak_det_signal_out[13] = \<const0> ;
  assign peak_det_signal_out[12] = \<const0> ;
  assign peak_det_signal_out[11] = \<const0> ;
  assign peak_det_signal_out[10] = \<const0> ;
  assign peak_det_signal_out[9] = \<const0> ;
  assign peak_det_signal_out[8] = \<const0> ;
  assign peak_det_signal_out[7] = \<const0> ;
  assign peak_det_signal_out[6] = \<const0> ;
  assign peak_det_signal_out[5] = \<const0> ;
  assign peak_det_signal_out[4] = \<const0> ;
  assign peak_det_signal_out[3] = \<const0> ;
  assign peak_det_signal_out[2] = \<const0> ;
  assign peak_det_signal_out[1] = \<const0> ;
  assign peak_det_signal_out[0] = \<const0> ;
  assign rejectn_out[15] = \<const0> ;
  assign rejectn_out[14] = \<const0> ;
  assign rejectn_out[13] = \<const0> ;
  assign rejectn_out[12] = \<const0> ;
  assign rejectn_out[11] = \<const0> ;
  assign rejectn_out[10] = \<const0> ;
  assign rejectn_out[9] = \<const0> ;
  assign rejectn_out[8] = \<const0> ;
  assign rejectn_out[7] = \<const0> ;
  assign rejectn_out[6] = \<const0> ;
  assign rejectn_out[5] = \<const0> ;
  assign rejectn_out[4] = \<const0> ;
  assign rejectn_out[3] = \<const0> ;
  assign rejectn_out[2] = \<const0> ;
  assign rejectn_out[1] = \<const0> ;
  assign rejectn_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
