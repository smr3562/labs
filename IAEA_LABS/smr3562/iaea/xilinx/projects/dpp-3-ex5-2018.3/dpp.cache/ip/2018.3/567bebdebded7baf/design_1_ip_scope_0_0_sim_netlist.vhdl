-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Thu Feb  4 23:58:05 2021
-- Host        : ZBOOK running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_scope_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_scope_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog is
  port (
    axi_awready_reg_0 : out STD_LOGIC;
    axi_wready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    i : out STD_LOGIC_VECTOR ( 10 downto 0 );
    \slv_reg_array_reg[0][0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[3][15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r3_dina : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg_array_reg[5][0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[6][10]_0\ : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    r9_douta : in STD_LOGIC_VECTOR ( 31 downto 0 );
    q : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog is
  signal axi_araddr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^d\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dec_r__15\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^i\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal r1_addra : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal r2_wea : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \^r3_dina\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r4_threshold : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal r5_enable : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal r6_delay : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal r7_clear : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg_array[0][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][12]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][13]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][15]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][16]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][17]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][18]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][20]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][21]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][22]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][23]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][24]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][25]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][26]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][27]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][28]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][29]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][30]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][31]_i_6_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][7]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][8]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[0][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][10]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][12]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][13]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][14]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][16]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][17]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][18]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][20]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][21]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][22]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][24]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][25]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][26]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][27]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][28]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][29]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][30]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][31]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][8]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[1][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][6]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[2][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][10]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][2]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][4]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][5]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][6]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[3][9]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[4][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[5][7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][31]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_array[6][7]_i_2_n_0\ : STD_LOGIC;
  signal \^slv_reg_array_reg[0][0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg_array_reg[3][15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^slv_reg_array_reg[5][0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg_array_reg[6][10]_0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal slv_wire_array : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \slv_reg_array[0][10]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \slv_reg_array[0][11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \slv_reg_array[0][12]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \slv_reg_array[0][13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \slv_reg_array[0][14]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \slv_reg_array[0][15]_i_3\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \slv_reg_array[0][16]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg_array[0][17]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[0][18]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \slv_reg_array[0][19]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \slv_reg_array[0][1]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \slv_reg_array[0][20]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \slv_reg_array[0][21]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \slv_reg_array[0][22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \slv_reg_array[0][23]_i_3\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \slv_reg_array[0][24]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \slv_reg_array[0][25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \slv_reg_array[0][26]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \slv_reg_array[0][27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \slv_reg_array[0][28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \slv_reg_array[0][29]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \slv_reg_array[0][2]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \slv_reg_array[0][30]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \slv_reg_array[0][31]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[0][3]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \slv_reg_array[0][4]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \slv_reg_array[0][5]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \slv_reg_array[0][6]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \slv_reg_array[0][7]_i_3\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \slv_reg_array[0][8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \slv_reg_array[0][9]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \slv_reg_array[1][0]_i_2\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \slv_reg_array[1][11]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \slv_reg_array[1][12]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \slv_reg_array[1][13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \slv_reg_array[1][14]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \slv_reg_array[1][15]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \slv_reg_array[1][16]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg_array[1][17]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg_array[1][18]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \slv_reg_array[1][19]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \slv_reg_array[1][1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg_array[1][20]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \slv_reg_array[1][21]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \slv_reg_array[1][22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \slv_reg_array[1][23]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \slv_reg_array[1][24]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \slv_reg_array[1][25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \slv_reg_array[1][26]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \slv_reg_array[1][27]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \slv_reg_array[1][28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \slv_reg_array[1][29]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \slv_reg_array[1][30]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \slv_reg_array[1][31]_i_2\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \slv_reg_array[1][3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \slv_reg_array[1][4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \slv_reg_array[1][7]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \slv_reg_array[1][8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \slv_reg_array[2][6]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[3][1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg_array[3][3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \slv_reg_array[3][4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \slv_reg_array[3][6]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg_array[3][7]_i_2\ : label is "soft_lutpair6";
begin
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  d(0) <= \^d\(0);
  i(10 downto 0) <= \^i\(10 downto 0);
  r3_dina(31 downto 0) <= \^r3_dina\(31 downto 0);
  \slv_reg_array_reg[0][0]_0\(0) <= \^slv_reg_array_reg[0][0]_0\(0);
  \slv_reg_array_reg[3][15]_0\(15 downto 0) <= \^slv_reg_array_reg[3][15]_0\(15 downto 0);
  \slv_reg_array_reg[5][0]_0\(0) <= \^slv_reg_array_reg[5][0]_0\(0);
  \slv_reg_array_reg[6][10]_0\(10 downto 0) <= \^slv_reg_array_reg[6][10]_0\(10 downto 0);
\axi_araddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(0),
      Q => axi_araddr(0),
      R => p_0_in
    );
\axi_araddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(1),
      Q => axi_araddr(1),
      R => p_0_in
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(2),
      Q => axi_araddr(2),
      R => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(3),
      Q => axi_araddr(3),
      R => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(4),
      Q => axi_araddr(4),
      R => p_0_in
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_arready0,
      D => axibusdomain_s_axi_araddr(5),
      Q => axi_araddr(5),
      R => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => p_0_in
    );
\axi_awaddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(0),
      Q => axi_awaddr(0),
      R => p_0_in
    );
\axi_awaddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(1),
      Q => axi_awaddr(1),
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(2),
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(3),
      Q => axi_awaddr(3),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(4),
      Q => axi_awaddr(4),
      R => p_0_in
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => axi_awready0,
      D => axibusdomain_s_axi_awaddr(5),
      Q => axi_awaddr(5),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axibusdomain_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => p_0_in
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => axibusdomain_s_axi_bvalid,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(0),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[0]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[0]_i_3_n_0\,
      O => slv_wire_array(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => q(0),
      I1 => \^slv_reg_array_reg[6][10]_0\(0),
      I2 => \dec_r__15\(1),
      I3 => \^slv_reg_array_reg[5][0]_0\(0),
      I4 => \dec_r__15\(0),
      I5 => \^r3_dina\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(0),
      I1 => \^d\(0),
      I2 => \dec_r__15\(1),
      I3 => \^i\(0),
      I4 => \dec_r__15\(0),
      I5 => \^slv_reg_array_reg[0][0]_0\(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(10),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[10]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[10]_i_3_n_0\,
      O => slv_wire_array(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(10),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(10),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(10),
      I1 => r5_enable(10),
      I2 => \dec_r__15\(1),
      I3 => \^i\(10),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(11),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[11]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[11]_i_3_n_0\,
      O => slv_wire_array(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(11),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(11),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(11),
      I1 => r5_enable(11),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(11),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(12),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[12]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[12]_i_3_n_0\,
      O => slv_wire_array(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(12),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(12),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(12),
      I1 => r5_enable(12),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(12),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(13),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[13]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[13]_i_3_n_0\,
      O => slv_wire_array(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(13),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(13),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(13),
      I1 => r5_enable(13),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(13),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(14),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[14]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[14]_i_3_n_0\,
      O => slv_wire_array(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(14),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(14),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(14),
      I1 => r5_enable(14),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(14),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(15),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[15]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[15]_i_3_n_0\,
      O => slv_wire_array(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(15),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(15),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(15),
      I1 => r5_enable(15),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(15),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(16),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[16]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[16]_i_3_n_0\,
      O => slv_wire_array(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(16),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(16),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(16),
      I1 => r5_enable(16),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(16),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(17),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[17]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[17]_i_3_n_0\,
      O => slv_wire_array(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(17),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(17),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(17),
      I1 => r5_enable(17),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(17),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(18),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[18]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[18]_i_3_n_0\,
      O => slv_wire_array(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(18),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(18),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(18),
      I1 => r5_enable(18),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(18),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(19),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[19]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[19]_i_3_n_0\,
      O => slv_wire_array(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(19),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(19),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(19),
      I1 => r5_enable(19),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(19),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(1),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[1]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[1]_i_3_n_0\,
      O => slv_wire_array(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(1),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(1),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(1),
      I1 => r5_enable(1),
      I2 => \dec_r__15\(1),
      I3 => \^i\(1),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(20),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[20]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[20]_i_3_n_0\,
      O => slv_wire_array(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(20),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(20),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(20),
      I1 => r5_enable(20),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(20),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(21),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[21]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[21]_i_3_n_0\,
      O => slv_wire_array(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(21),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(21),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(21),
      I1 => r5_enable(21),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(21),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(22),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[22]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[22]_i_3_n_0\,
      O => slv_wire_array(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(22),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(22),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(22),
      I1 => r5_enable(22),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(22),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(23),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[23]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[23]_i_3_n_0\,
      O => slv_wire_array(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(23),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(23),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(23),
      I1 => r5_enable(23),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(23),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(24),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[24]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[24]_i_3_n_0\,
      O => slv_wire_array(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(24),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(24),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(24),
      I1 => r5_enable(24),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(24),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(25),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[25]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[25]_i_3_n_0\,
      O => slv_wire_array(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(25),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(25),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(25),
      I1 => r5_enable(25),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(25),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(26),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[26]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[26]_i_3_n_0\,
      O => slv_wire_array(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(26),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(26),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(26),
      I1 => r5_enable(26),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(26),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(27),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[27]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[27]_i_3_n_0\,
      O => slv_wire_array(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(27),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(27),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(27),
      I1 => r5_enable(27),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(27),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(28),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[28]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[28]_i_3_n_0\,
      O => slv_wire_array(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(28),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(28),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(28),
      I1 => r5_enable(28),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(28),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(29),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[29]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[29]_i_3_n_0\,
      O => slv_wire_array(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(29),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(29),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(29),
      I1 => r5_enable(29),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(29),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(2),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[2]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[2]_i_3_n_0\,
      O => slv_wire_array(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(2),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(2),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(2),
      I1 => r5_enable(2),
      I2 => \dec_r__15\(1),
      I3 => \^i\(2),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(30),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[30]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[30]_i_3_n_0\,
      O => slv_wire_array(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(30),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(30),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(30),
      I1 => r5_enable(30),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(30),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(31),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[31]_i_3_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[31]_i_5_n_0\,
      O => slv_wire_array(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => axi_araddr(0),
      I1 => axi_araddr(4),
      I2 => axi_araddr(1),
      I3 => axi_araddr(5),
      I4 => axi_araddr(3),
      I5 => axi_araddr(2),
      O => \dec_r__15\(3)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => r1_addra(31),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(31),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(4),
      I3 => axi_araddr(1),
      O => \dec_r__15\(2)
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r4_threshold(31),
      I1 => r5_enable(31),
      I2 => \dec_r__15\(1),
      I3 => r6_delay(31),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(31),
      O => \axi_rdata[31]_i_5_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(3),
      I3 => axi_araddr(1),
      O => \dec_r__15\(1)
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => axi_araddr(5),
      I1 => axi_araddr(0),
      I2 => axi_araddr(2),
      I3 => axi_araddr(1),
      O => \dec_r__15\(0)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(3),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[3]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[3]_i_3_n_0\,
      O => slv_wire_array(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(3),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(3),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(3),
      I1 => r5_enable(3),
      I2 => \dec_r__15\(1),
      I3 => \^i\(3),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(4),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[4]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[4]_i_3_n_0\,
      O => slv_wire_array(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(4),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(4),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(4),
      I1 => r5_enable(4),
      I2 => \dec_r__15\(1),
      I3 => \^i\(4),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(5),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[5]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[5]_i_3_n_0\,
      O => slv_wire_array(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(5),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(5),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(5),
      I1 => r5_enable(5),
      I2 => \dec_r__15\(1),
      I3 => \^i\(5),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(6),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[6]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[6]_i_3_n_0\,
      O => slv_wire_array(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(6),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(6),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(6),
      I1 => r5_enable(6),
      I2 => \dec_r__15\(1),
      I3 => \^i\(6),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(7),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[7]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[7]_i_3_n_0\,
      O => slv_wire_array(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(7),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(7),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(7),
      I1 => r5_enable(7),
      I2 => \dec_r__15\(1),
      I3 => \^i\(7),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(8),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[8]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[8]_i_3_n_0\,
      O => slv_wire_array(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(8),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(8),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(8),
      I1 => r5_enable(8),
      I2 => \dec_r__15\(1),
      I3 => \^i\(8),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => r9_douta(9),
      I1 => \dec_r__15\(3),
      I2 => \axi_rdata[9]_i_2_n_0\,
      I3 => \dec_r__15\(2),
      I4 => \axi_rdata[9]_i_3_n_0\,
      O => slv_wire_array(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^slv_reg_array_reg[6][10]_0\(9),
      I1 => \dec_r__15\(1),
      I2 => r2_wea(9),
      I3 => \dec_r__15\(0),
      I4 => \^r3_dina\(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg_array_reg[3][15]_0\(9),
      I1 => r5_enable(9),
      I2 => \dec_r__15\(1),
      I3 => \^i\(9),
      I4 => \dec_r__15\(0),
      I5 => r7_clear(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(0),
      Q => axibusdomain_s_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(10),
      Q => axibusdomain_s_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(11),
      Q => axibusdomain_s_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(12),
      Q => axibusdomain_s_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(13),
      Q => axibusdomain_s_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(14),
      Q => axibusdomain_s_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(15),
      Q => axibusdomain_s_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(16),
      Q => axibusdomain_s_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(17),
      Q => axibusdomain_s_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(18),
      Q => axibusdomain_s_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(19),
      Q => axibusdomain_s_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(1),
      Q => axibusdomain_s_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(20),
      Q => axibusdomain_s_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(21),
      Q => axibusdomain_s_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(22),
      Q => axibusdomain_s_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(23),
      Q => axibusdomain_s_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(24),
      Q => axibusdomain_s_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(25),
      Q => axibusdomain_s_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(26),
      Q => axibusdomain_s_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(27),
      Q => axibusdomain_s_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(28),
      Q => axibusdomain_s_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(29),
      Q => axibusdomain_s_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(2),
      Q => axibusdomain_s_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(30),
      Q => axibusdomain_s_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(31),
      Q => axibusdomain_s_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(3),
      Q => axibusdomain_s_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(4),
      Q => axibusdomain_s_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(5),
      Q => axibusdomain_s_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(6),
      Q => axibusdomain_s_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(7),
      Q => axibusdomain_s_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(8),
      Q => axibusdomain_s_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => \slv_reg_rden__0\,
      D => slv_wire_array(9),
      Q => axibusdomain_s_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^axibusdomain_s_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => p_0_in
    );
\slv_reg_array[0][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8FFF000080000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      I3 => \slv_reg_array[0][31]_i_4_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[0][0]_0\(0),
      O => \slv_reg_array[0][0]_i_1_n_0\
    );
\slv_reg_array[0][10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(10),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][10]_i_1_n_0\
    );
\slv_reg_array[0][11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(11),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][11]_i_1_n_0\
    );
\slv_reg_array[0][12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(12),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][12]_i_1_n_0\
    );
\slv_reg_array[0][13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(13),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][13]_i_1_n_0\
    );
\slv_reg_array[0][14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(14),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][14]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array[0][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      O => \slv_reg_array[0][15]_i_2_n_0\
    );
\slv_reg_array[0][15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(15),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][15]_i_3_n_0\
    );
\slv_reg_array[0][15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wvalid,
      I1 => axibusdomain_s_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => axibusdomain_aresetn,
      I5 => axibusdomain_s_axi_wstrb(1),
      O => \slv_reg_array[0][15]_i_4_n_0\
    );
\slv_reg_array[0][15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(1),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][15]_i_5_n_0\
    );
\slv_reg_array[0][16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(16),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][16]_i_1_n_0\
    );
\slv_reg_array[0][17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(17),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][17]_i_1_n_0\
    );
\slv_reg_array[0][18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(18),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][18]_i_1_n_0\
    );
\slv_reg_array[0][19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(19),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][19]_i_1_n_0\
    );
\slv_reg_array[0][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(1),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][1]_i_1_n_0\
    );
\slv_reg_array[0][20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(20),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][20]_i_1_n_0\
    );
\slv_reg_array[0][21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(21),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][21]_i_1_n_0\
    );
\slv_reg_array[0][22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(22),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][22]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][23]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array[0][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][23]_i_4_n_0\,
      O => \slv_reg_array[0][23]_i_2_n_0\
    );
\slv_reg_array[0][23]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(23),
      I1 => axibusdomain_s_axi_wstrb(2),
      I2 => \slv_reg_array[0][23]_i_5_n_0\,
      O => \slv_reg_array[0][23]_i_3_n_0\
    );
\slv_reg_array[0][23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_4_n_0\
    );
\slv_reg_array[0][23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(2),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][23]_i_5_n_0\
    );
\slv_reg_array[0][24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(24),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][24]_i_1_n_0\
    );
\slv_reg_array[0][25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(25),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][25]_i_1_n_0\
    );
\slv_reg_array[0][26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(26),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][26]_i_1_n_0\
    );
\slv_reg_array[0][27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(27),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][27]_i_1_n_0\
    );
\slv_reg_array[0][28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(28),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][28]_i_1_n_0\
    );
\slv_reg_array[0][29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(29),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][29]_i_1_n_0\
    );
\slv_reg_array[0][2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(2),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][2]_i_1_n_0\
    );
\slv_reg_array[0][30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(30),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][30]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array[0][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][31]_i_5_n_0\,
      O => \slv_reg_array[0][31]_i_2_n_0\
    );
\slv_reg_array[0][31]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(31),
      I1 => axibusdomain_s_axi_wstrb(3),
      I2 => \slv_reg_array[0][31]_i_6_n_0\,
      O => \slv_reg_array[0][31]_i_3_n_0\
    );
\slv_reg_array[0][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFCFFFCFFFCFEFD"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[0][31]_i_4_n_0\
    );
\slv_reg_array[0][31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_5_n_0\
    );
\slv_reg_array[0][31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(3),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][31]_i_6_n_0\
    );
\slv_reg_array[0][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(3),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][3]_i_1_n_0\
    );
\slv_reg_array[0][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(4),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][4]_i_1_n_0\
    );
\slv_reg_array[0][5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(5),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][5]_i_1_n_0\
    );
\slv_reg_array[0][6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(6),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][6]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array[0][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_4_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      O => \slv_reg_array[0][7]_i_2_n_0\
    );
\slv_reg_array[0][7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(7),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[0][7]_i_5_n_0\,
      O => \slv_reg_array[0][7]_i_3_n_0\
    );
\slv_reg_array[0][7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_4_n_0\
    );
\slv_reg_array[0][7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080008000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => axibusdomain_s_axi_wvalid,
      I4 => axibusdomain_s_axi_wstrb(0),
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[0][7]_i_5_n_0\
    );
\slv_reg_array[0][8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(8),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][8]_i_1_n_0\
    );
\slv_reg_array[0][9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(9),
      I1 => axibusdomain_s_axi_wstrb(1),
      I2 => \slv_reg_array[0][15]_i_5_n_0\,
      O => \slv_reg_array[0][9]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4FFF404040004040"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => \slv_reg_array[1][0]_i_2_n_0\,
      I2 => \slv_reg_array[1][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^i\(0),
      O => \slv_reg_array[1][0]_i_1_n_0\
    );
\slv_reg_array[1][0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => axibusdomain_s_axi_wdata(0),
      O => \slv_reg_array[1][0]_i_2_n_0\
    );
\slv_reg_array[1][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDD0200"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(10),
      I4 => \^i\(10),
      O => \slv_reg_array[1][10]_i_1_n_0\
    );
\slv_reg_array[1][10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][10]_i_2_n_0\
    );
\slv_reg_array[1][11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(11),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][11]_i_1_n_0\
    );
\slv_reg_array[1][12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(12),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][12]_i_1_n_0\
    );
\slv_reg_array[1][13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(13),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][13]_i_1_n_0\
    );
\slv_reg_array[1][14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(14),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][14]_i_1_n_0\
    );
\slv_reg_array[1][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      O => \slv_reg_array[1][15]_i_1_n_0\
    );
\slv_reg_array[1][15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(15),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][15]_i_2_n_0\
    );
\slv_reg_array[1][16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(16),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][16]_i_1_n_0\
    );
\slv_reg_array[1][17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(17),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][17]_i_1_n_0\
    );
\slv_reg_array[1][18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(18),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][18]_i_1_n_0\
    );
\slv_reg_array[1][19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(19),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][19]_i_1_n_0\
    );
\slv_reg_array[1][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(1),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][1]_i_1_n_0\
    );
\slv_reg_array[1][20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(20),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][20]_i_1_n_0\
    );
\slv_reg_array[1][21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(21),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][21]_i_1_n_0\
    );
\slv_reg_array[1][22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(22),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][22]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      O => \slv_reg_array[1][23]_i_1_n_0\
    );
\slv_reg_array[1][23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(2),
      I1 => axibusdomain_s_axi_wdata(23),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][23]_i_2_n_0\
    );
\slv_reg_array[1][24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(24),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][24]_i_1_n_0\
    );
\slv_reg_array[1][25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(25),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][25]_i_1_n_0\
    );
\slv_reg_array[1][26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(26),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][26]_i_1_n_0\
    );
\slv_reg_array[1][27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(27),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][27]_i_1_n_0\
    );
\slv_reg_array[1][28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(28),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][28]_i_1_n_0\
    );
\slv_reg_array[1][29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(29),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][29]_i_1_n_0\
    );
\slv_reg_array[1][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(2),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(2),
      O => \slv_reg_array[1][2]_i_1_n_0\
    );
\slv_reg_array[1][30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(30),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][30]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      O => \slv_reg_array[1][31]_i_1_n_0\
    );
\slv_reg_array[1][31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(3),
      I1 => axibusdomain_s_axi_wdata(31),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][31]_i_2_n_0\
    );
\slv_reg_array[1][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[1][31]_i_3_n_0\
    );
\slv_reg_array[1][31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \slv_reg_array[1][10]_i_2_n_0\,
      I1 => axibusdomain_s_axi_wvalid,
      I2 => axibusdomain_s_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      I4 => \^axi_awready_reg_0\,
      I5 => axibusdomain_aresetn,
      O => \slv_reg_array[1][31]_i_4_n_0\
    );
\slv_reg_array[1][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(3),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][3]_i_1_n_0\
    );
\slv_reg_array[1][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(4),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][4]_i_1_n_0\
    );
\slv_reg_array[1][5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(5),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(5),
      O => \slv_reg_array[1][5]_i_1_n_0\
    );
\slv_reg_array[1][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(6),
      I1 => \slv_reg_array[1][31]_i_3_n_0\,
      I2 => \slv_reg_array[0][7]_i_4_n_0\,
      I3 => \^i\(6),
      O => \slv_reg_array[1][6]_i_1_n_0\
    );
\slv_reg_array[1][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][7]_i_4_n_0\,
      O => \slv_reg_array[1][7]_i_1_n_0\
    );
\slv_reg_array[1][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(7),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][7]_i_2_n_0\
    );
\slv_reg_array[1][8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(1),
      I1 => axibusdomain_s_axi_wdata(8),
      I2 => \slv_reg_array[1][31]_i_4_n_0\,
      O => \slv_reg_array[1][8]_i_1_n_0\
    );
\slv_reg_array[1][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDD0200"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_3_n_0\,
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(9),
      I4 => \^i\(9),
      O => \slv_reg_array[1][9]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFEF00"
    )
        port map (
      I0 => \slv_reg_array[2][6]_i_3_n_0\,
      I1 => axibusdomain_s_axi_wdata(0),
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => \slv_reg_array[2][0]_i_2_n_0\,
      I4 => \^d\(0),
      O => \slv_reg_array[2][0]_i_1_n_0\
    );
\slv_reg_array[2][0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \slv_reg_array[2][31]_i_2_n_0\,
      I1 => \slv_reg_array[0][7]_i_5_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[2][0]_i_2_n_0\
    );
\slv_reg_array[2][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE0100"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(10),
      I4 => r5_enable(10),
      O => \slv_reg_array[2][10]_i_1_n_0\
    );
\slv_reg_array[2][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][15]_i_1_n_0\
    );
\slv_reg_array[2][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][23]_i_1_n_0\
    );
\slv_reg_array[2][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(2),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(2),
      O => \slv_reg_array[2][2]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][31]_i_1_n_0\
    );
\slv_reg_array[2][31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => axi_awaddr(3),
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(5),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(1),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[2][31]_i_2_n_0\
    );
\slv_reg_array[2][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(5),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(5),
      O => \slv_reg_array[2][5]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFDDD00002000"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[2][6]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(6),
      I4 => \slv_reg_array[2][6]_i_3_n_0\,
      I5 => r5_enable(6),
      O => \slv_reg_array[2][6]_i_1_n_0\
    );
\slv_reg_array[2][6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      O => \slv_reg_array[2][6]_i_2_n_0\
    );
\slv_reg_array[2][6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000F0001000A"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[2][6]_i_3_n_0\
    );
\slv_reg_array[2][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      O => \slv_reg_array[2][7]_i_1_n_0\
    );
\slv_reg_array[2][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE0100"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[2][31]_i_2_n_0\,
      I2 => \slv_reg_array[1][10]_i_2_n_0\,
      I3 => axibusdomain_s_axi_wdata(9),
      I4 => r5_enable(9),
      O => \slv_reg_array[2][9]_i_1_n_0\
    );
\slv_reg_array[3][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4FF040404000404"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => \slv_reg_array[1][0]_i_2_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[3][15]_0\(0),
      O => \slv_reg_array[3][0]_i_1_n_0\
    );
\slv_reg_array[3][10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(10),
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(10),
      O => \slv_reg_array[3][10]_i_1_n_0\
    );
\slv_reg_array[3][15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][15]_i_1_n_0\
    );
\slv_reg_array[3][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => axibusdomain_s_axi_wstrb(0),
      I1 => \slv_reg_array[1][31]_i_4_n_0\,
      I2 => axibusdomain_s_axi_wdata(1),
      O => \slv_reg_array[3][1]_i_1_n_0\
    );
\slv_reg_array[3][23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][23]_i_1_n_0\
    );
\slv_reg_array[3][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(2),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][2]_i_1_n_0\
    );
\slv_reg_array[3][31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][31]_i_1_n_0\
    );
\slv_reg_array[3][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(3),
      O => \slv_reg_array[3][3]_i_1_n_0\
    );
\slv_reg_array[3][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(4),
      O => \slv_reg_array[3][4]_i_1_n_0\
    );
\slv_reg_array[3][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(5),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][5]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(6),
      I1 => axi_awaddr(3),
      I2 => \slv_reg_array[3][6]_i_2_n_0\,
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(2),
      O => \slv_reg_array[3][6]_i_1_n_0\
    );
\slv_reg_array[3][6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(5),
      O => \slv_reg_array[3][6]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[3][7]_i_3_n_0\,
      O => \slv_reg_array[3][7]_i_1_n_0\
    );
\slv_reg_array[3][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \slv_reg_array[1][31]_i_4_n_0\,
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => axibusdomain_s_axi_wdata(7),
      O => \slv_reg_array[3][7]_i_2_n_0\
    );
\slv_reg_array[3][7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFDFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(5),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(1),
      I4 => axi_awaddr(4),
      I5 => axi_awaddr(3),
      O => \slv_reg_array[3][7]_i_3_n_0\
    );
\slv_reg_array[3][9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(9),
      I1 => \slv_reg_array[0][15]_i_4_n_0\,
      I2 => \slv_reg_array[3][7]_i_3_n_0\,
      I3 => \^slv_reg_array_reg[3][15]_0\(9),
      O => \slv_reg_array[3][9]_i_1_n_0\
    );
\slv_reg_array[4][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[4][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^r3_dina\(0),
      O => \slv_reg_array[4][0]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array[4][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][15]_i_2_n_0\
    );
\slv_reg_array[4][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array[4][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][23]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array[4][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][31]_i_2_n_0\
    );
\slv_reg_array[4][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(1),
      I2 => axi_awaddr(0),
      I3 => axi_awaddr(5),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[4][31]_i_3_n_0\
    );
\slv_reg_array[4][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array[4][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[4][31]_i_3_n_0\,
      O => \slv_reg_array[4][7]_i_2_n_0\
    );
\slv_reg_array[5][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[5][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[5][0]_0\(0),
      O => \slv_reg_array[5][0]_i_1_n_0\
    );
\slv_reg_array[5][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array[5][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][15]_i_2_n_0\
    );
\slv_reg_array[5][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array[5][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][23]_i_2_n_0\
    );
\slv_reg_array[5][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array[5][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][31]_i_2_n_0\
    );
\slv_reg_array[5][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFFFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(1),
      I1 => axi_awaddr(0),
      I2 => axi_awaddr(5),
      I3 => axi_awaddr(4),
      I4 => axi_awaddr(3),
      I5 => axi_awaddr(2),
      O => \slv_reg_array[5][31]_i_3_n_0\
    );
\slv_reg_array[5][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array[5][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[5][31]_i_3_n_0\,
      O => \slv_reg_array[5][7]_i_2_n_0\
    );
\slv_reg_array[6][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FF000008000000"
    )
        port map (
      I0 => axibusdomain_s_axi_wdata(0),
      I1 => axibusdomain_s_axi_wstrb(0),
      I2 => \slv_reg_array[6][31]_i_3_n_0\,
      I3 => \slv_reg_array[0][7]_i_5_n_0\,
      I4 => axibusdomain_aresetn,
      I5 => \^slv_reg_array_reg[6][10]_0\(0),
      O => \slv_reg_array[6][0]_i_1_n_0\
    );
\slv_reg_array[6][15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array[6][15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \slv_reg_array[0][15]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][15]_i_2_n_0\
    );
\slv_reg_array[6][23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array[6][23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][23]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][23]_i_2_n_0\
    );
\slv_reg_array[6][31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array[6][31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][31]_i_5_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][31]_i_2_n_0\
    );
\slv_reg_array[6][31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBFFFFFFFF"
    )
        port map (
      I0 => axi_awaddr(2),
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(1),
      I3 => axi_awaddr(0),
      I4 => axi_awaddr(5),
      I5 => axi_awaddr(4),
      O => \slv_reg_array[6][31]_i_3_n_0\
    );
\slv_reg_array[6][7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      I2 => axibusdomain_aresetn,
      O => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array[6][7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slv_reg_array[0][7]_i_4_n_0\,
      I1 => \slv_reg_array[6][31]_i_3_n_0\,
      O => \slv_reg_array[6][7]_i_2_n_0\
    );
\slv_reg_array_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[0][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[0][0]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[0][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => r7_clear(10),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r7_clear(11),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r7_clear(12),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r7_clear(13),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r7_clear(14),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r7_clear(15),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r7_clear(16),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r7_clear(17),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r7_clear(18),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r7_clear(19),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => r7_clear(1),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r7_clear(20),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r7_clear(21),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r7_clear(22),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r7_clear(23),
      R => \slv_reg_array[0][23]_i_1_n_0\
    );
\slv_reg_array_reg[0][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r7_clear(24),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r7_clear(25),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r7_clear(26),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r7_clear(27),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r7_clear(28),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r7_clear(29),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => r7_clear(2),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r7_clear(30),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r7_clear(31),
      R => \slv_reg_array[0][31]_i_1_n_0\
    );
\slv_reg_array_reg[0][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => r7_clear(3),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => r7_clear(4),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => r7_clear(5),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => r7_clear(6),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => r7_clear(7),
      R => \slv_reg_array[0][7]_i_1_n_0\
    );
\slv_reg_array_reg[0][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => r7_clear(8),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[0][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[0][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => r7_clear(9),
      R => \slv_reg_array[0][15]_i_1_n_0\
    );
\slv_reg_array_reg[1][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][0]_i_1_n_0\,
      Q => \^i\(0),
      R => '0'
    );
\slv_reg_array_reg[1][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][10]_i_1_n_0\,
      Q => \^i\(10),
      R => '0'
    );
\slv_reg_array_reg[1][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => r6_delay(11),
      R => '0'
    );
\slv_reg_array_reg[1][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => r6_delay(12),
      R => '0'
    );
\slv_reg_array_reg[1][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => r6_delay(13),
      R => '0'
    );
\slv_reg_array_reg[1][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => r6_delay(14),
      R => '0'
    );
\slv_reg_array_reg[1][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => r6_delay(15),
      R => '0'
    );
\slv_reg_array_reg[1][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r6_delay(16),
      R => '0'
    );
\slv_reg_array_reg[1][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r6_delay(17),
      R => '0'
    );
\slv_reg_array_reg[1][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r6_delay(18),
      R => '0'
    );
\slv_reg_array_reg[1][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r6_delay(19),
      R => '0'
    );
\slv_reg_array_reg[1][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => \^i\(1),
      R => '0'
    );
\slv_reg_array_reg[1][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r6_delay(20),
      R => '0'
    );
\slv_reg_array_reg[1][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r6_delay(21),
      R => '0'
    );
\slv_reg_array_reg[1][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r6_delay(22),
      R => '0'
    );
\slv_reg_array_reg[1][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r6_delay(23),
      R => '0'
    );
\slv_reg_array_reg[1][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r6_delay(24),
      R => '0'
    );
\slv_reg_array_reg[1][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r6_delay(25),
      R => '0'
    );
\slv_reg_array_reg[1][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r6_delay(26),
      R => '0'
    );
\slv_reg_array_reg[1][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r6_delay(27),
      R => '0'
    );
\slv_reg_array_reg[1][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r6_delay(28),
      R => '0'
    );
\slv_reg_array_reg[1][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r6_delay(29),
      R => '0'
    );
\slv_reg_array_reg[1][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][2]_i_1_n_0\,
      Q => \^i\(2),
      R => '0'
    );
\slv_reg_array_reg[1][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r6_delay(30),
      R => '0'
    );
\slv_reg_array_reg[1][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r6_delay(31),
      R => '0'
    );
\slv_reg_array_reg[1][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][3]_i_1_n_0\,
      Q => \^i\(3),
      R => '0'
    );
\slv_reg_array_reg[1][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][4]_i_1_n_0\,
      Q => \^i\(4),
      R => '0'
    );
\slv_reg_array_reg[1][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][5]_i_1_n_0\,
      Q => \^i\(5),
      R => '0'
    );
\slv_reg_array_reg[1][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][6]_i_1_n_0\,
      Q => \^i\(6),
      R => '0'
    );
\slv_reg_array_reg[1][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][7]_i_1_n_0\,
      D => \slv_reg_array[1][7]_i_2_n_0\,
      Q => \^i\(7),
      R => '0'
    );
\slv_reg_array_reg[1][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[1][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => \^i\(8),
      R => '0'
    );
\slv_reg_array_reg[1][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[1][9]_i_1_n_0\,
      Q => \^i\(9),
      R => '0'
    );
\slv_reg_array_reg[2][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][0]_i_1_n_0\,
      Q => \^d\(0),
      R => '0'
    );
\slv_reg_array_reg[2][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][10]_i_1_n_0\,
      Q => r5_enable(10),
      R => '0'
    );
\slv_reg_array_reg[2][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => r5_enable(11),
      R => '0'
    );
\slv_reg_array_reg[2][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => r5_enable(12),
      R => '0'
    );
\slv_reg_array_reg[2][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => r5_enable(13),
      R => '0'
    );
\slv_reg_array_reg[2][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => r5_enable(14),
      R => '0'
    );
\slv_reg_array_reg[2][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => r5_enable(15),
      R => '0'
    );
\slv_reg_array_reg[2][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r5_enable(16),
      R => '0'
    );
\slv_reg_array_reg[2][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r5_enable(17),
      R => '0'
    );
\slv_reg_array_reg[2][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r5_enable(18),
      R => '0'
    );
\slv_reg_array_reg[2][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r5_enable(19),
      R => '0'
    );
\slv_reg_array_reg[2][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][1]_i_1_n_0\,
      Q => r5_enable(1),
      R => '0'
    );
\slv_reg_array_reg[2][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r5_enable(20),
      R => '0'
    );
\slv_reg_array_reg[2][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r5_enable(21),
      R => '0'
    );
\slv_reg_array_reg[2][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r5_enable(22),
      R => '0'
    );
\slv_reg_array_reg[2][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r5_enable(23),
      R => '0'
    );
\slv_reg_array_reg[2][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r5_enable(24),
      R => '0'
    );
\slv_reg_array_reg[2][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r5_enable(25),
      R => '0'
    );
\slv_reg_array_reg[2][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r5_enable(26),
      R => '0'
    );
\slv_reg_array_reg[2][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r5_enable(27),
      R => '0'
    );
\slv_reg_array_reg[2][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r5_enable(28),
      R => '0'
    );
\slv_reg_array_reg[2][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r5_enable(29),
      R => '0'
    );
\slv_reg_array_reg[2][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][2]_i_1_n_0\,
      Q => r5_enable(2),
      R => '0'
    );
\slv_reg_array_reg[2][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r5_enable(30),
      R => '0'
    );
\slv_reg_array_reg[2][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r5_enable(31),
      R => '0'
    );
\slv_reg_array_reg[2][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][3]_i_1_n_0\,
      Q => r5_enable(3),
      R => '0'
    );
\slv_reg_array_reg[2][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][4]_i_1_n_0\,
      Q => r5_enable(4),
      R => '0'
    );
\slv_reg_array_reg[2][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][5]_i_1_n_0\,
      Q => r5_enable(5),
      R => '0'
    );
\slv_reg_array_reg[2][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][6]_i_1_n_0\,
      Q => r5_enable(6),
      R => '0'
    );
\slv_reg_array_reg[2][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][7]_i_1_n_0\,
      D => \slv_reg_array[1][7]_i_2_n_0\,
      Q => r5_enable(7),
      R => '0'
    );
\slv_reg_array_reg[2][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[2][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => r5_enable(8),
      R => '0'
    );
\slv_reg_array_reg[2][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[2][9]_i_1_n_0\,
      Q => r5_enable(9),
      R => '0'
    );
\slv_reg_array_reg[3][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[3][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][10]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(10),
      R => '0'
    );
\slv_reg_array_reg[3][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][11]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(11),
      R => '0'
    );
\slv_reg_array_reg[3][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][12]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(12),
      R => '0'
    );
\slv_reg_array_reg[3][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][13]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(13),
      R => '0'
    );
\slv_reg_array_reg[3][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][14]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(14),
      R => '0'
    );
\slv_reg_array_reg[3][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][15]_i_2_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(15),
      R => '0'
    );
\slv_reg_array_reg[3][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][16]_i_1_n_0\,
      Q => r4_threshold(16),
      R => '0'
    );
\slv_reg_array_reg[3][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][17]_i_1_n_0\,
      Q => r4_threshold(17),
      R => '0'
    );
\slv_reg_array_reg[3][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][18]_i_1_n_0\,
      Q => r4_threshold(18),
      R => '0'
    );
\slv_reg_array_reg[3][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][19]_i_1_n_0\,
      Q => r4_threshold(19),
      R => '0'
    );
\slv_reg_array_reg[3][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(1),
      R => '0'
    );
\slv_reg_array_reg[3][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][20]_i_1_n_0\,
      Q => r4_threshold(20),
      R => '0'
    );
\slv_reg_array_reg[3][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][21]_i_1_n_0\,
      Q => r4_threshold(21),
      R => '0'
    );
\slv_reg_array_reg[3][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][22]_i_1_n_0\,
      Q => r4_threshold(22),
      R => '0'
    );
\slv_reg_array_reg[3][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][23]_i_1_n_0\,
      D => \slv_reg_array[1][23]_i_2_n_0\,
      Q => r4_threshold(23),
      R => '0'
    );
\slv_reg_array_reg[3][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][24]_i_1_n_0\,
      Q => r4_threshold(24),
      R => '0'
    );
\slv_reg_array_reg[3][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][25]_i_1_n_0\,
      Q => r4_threshold(25),
      R => '0'
    );
\slv_reg_array_reg[3][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][26]_i_1_n_0\,
      Q => r4_threshold(26),
      R => '0'
    );
\slv_reg_array_reg[3][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][27]_i_1_n_0\,
      Q => r4_threshold(27),
      R => '0'
    );
\slv_reg_array_reg[3][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][28]_i_1_n_0\,
      Q => r4_threshold(28),
      R => '0'
    );
\slv_reg_array_reg[3][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][29]_i_1_n_0\,
      Q => r4_threshold(29),
      R => '0'
    );
\slv_reg_array_reg[3][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(2),
      R => '0'
    );
\slv_reg_array_reg[3][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][30]_i_1_n_0\,
      Q => r4_threshold(30),
      R => '0'
    );
\slv_reg_array_reg[3][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][31]_i_1_n_0\,
      D => \slv_reg_array[1][31]_i_2_n_0\,
      Q => r4_threshold(31),
      R => '0'
    );
\slv_reg_array_reg[3][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(3),
      R => '0'
    );
\slv_reg_array_reg[3][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(4),
      R => '0'
    );
\slv_reg_array_reg[3][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(5),
      R => '0'
    );
\slv_reg_array_reg[3][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(6),
      R => '0'
    );
\slv_reg_array_reg[3][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][7]_i_1_n_0\,
      D => \slv_reg_array[3][7]_i_2_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(7),
      R => '0'
    );
\slv_reg_array_reg[3][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[3][15]_i_1_n_0\,
      D => \slv_reg_array[1][8]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(8),
      R => '0'
    );
\slv_reg_array_reg[3][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[3][9]_i_1_n_0\,
      Q => \^slv_reg_array_reg[3][15]_0\(9),
      R => '0'
    );
\slv_reg_array_reg[4][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[4][0]_i_1_n_0\,
      Q => \^r3_dina\(0),
      R => '0'
    );
\slv_reg_array_reg[4][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => \^r3_dina\(10),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => \^r3_dina\(11),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => \^r3_dina\(12),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => \^r3_dina\(13),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => \^r3_dina\(14),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => \^r3_dina\(15),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => \^r3_dina\(16),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => \^r3_dina\(17),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => \^r3_dina\(18),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => \^r3_dina\(19),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => \^r3_dina\(1),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => \^r3_dina\(20),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => \^r3_dina\(21),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => \^r3_dina\(22),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => \^r3_dina\(23),
      R => \slv_reg_array[4][23]_i_1_n_0\
    );
\slv_reg_array_reg[4][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => \^r3_dina\(24),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => \^r3_dina\(25),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => \^r3_dina\(26),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => \^r3_dina\(27),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => \^r3_dina\(28),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => \^r3_dina\(29),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => \^r3_dina\(2),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => \^r3_dina\(30),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => \^r3_dina\(31),
      R => \slv_reg_array[4][31]_i_1_n_0\
    );
\slv_reg_array_reg[4][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => \^r3_dina\(3),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => \^r3_dina\(4),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => \^r3_dina\(5),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => \^r3_dina\(6),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => \^r3_dina\(7),
      R => \slv_reg_array[4][7]_i_1_n_0\
    );
\slv_reg_array_reg[4][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => \^r3_dina\(8),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[4][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[4][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => \^r3_dina\(9),
      R => \slv_reg_array[4][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[5][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[5][0]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[5][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => r2_wea(10),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r2_wea(11),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r2_wea(12),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r2_wea(13),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r2_wea(14),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r2_wea(15),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r2_wea(16),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r2_wea(17),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r2_wea(18),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r2_wea(19),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => r2_wea(1),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r2_wea(20),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r2_wea(21),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r2_wea(22),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r2_wea(23),
      R => \slv_reg_array[5][23]_i_1_n_0\
    );
\slv_reg_array_reg[5][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r2_wea(24),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r2_wea(25),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r2_wea(26),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r2_wea(27),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r2_wea(28),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r2_wea(29),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => r2_wea(2),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r2_wea(30),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r2_wea(31),
      R => \slv_reg_array[5][31]_i_1_n_0\
    );
\slv_reg_array_reg[5][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => r2_wea(3),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => r2_wea(4),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => r2_wea(5),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => r2_wea(6),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => r2_wea(7),
      R => \slv_reg_array[5][7]_i_1_n_0\
    );
\slv_reg_array_reg[5][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => r2_wea(8),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[5][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[5][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => r2_wea(9),
      R => \slv_reg_array[5][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => '1',
      D => \slv_reg_array[6][0]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(0),
      R => '0'
    );
\slv_reg_array_reg[6][10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][10]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(10),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][11]_i_1_n_0\,
      Q => r1_addra(11),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][12]_i_1_n_0\,
      Q => r1_addra(12),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][13]_i_1_n_0\,
      Q => r1_addra(13),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][14]_i_1_n_0\,
      Q => r1_addra(14),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][15]_i_3_n_0\,
      Q => r1_addra(15),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][16]_i_1_n_0\,
      Q => r1_addra(16),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][17]_i_1_n_0\,
      Q => r1_addra(17),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][18]_i_1_n_0\,
      Q => r1_addra(18),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][19]_i_1_n_0\,
      Q => r1_addra(19),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][1]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(1),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][20]_i_1_n_0\,
      Q => r1_addra(20),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][21]_i_1_n_0\,
      Q => r1_addra(21),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][22]_i_1_n_0\,
      Q => r1_addra(22),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][23]_i_2_n_0\,
      D => \slv_reg_array[0][23]_i_3_n_0\,
      Q => r1_addra(23),
      R => \slv_reg_array[6][23]_i_1_n_0\
    );
\slv_reg_array_reg[6][24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][24]_i_1_n_0\,
      Q => r1_addra(24),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][25]_i_1_n_0\,
      Q => r1_addra(25),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][26]_i_1_n_0\,
      Q => r1_addra(26),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][27]_i_1_n_0\,
      Q => r1_addra(27),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][28]_i_1_n_0\,
      Q => r1_addra(28),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][29]_i_1_n_0\,
      Q => r1_addra(29),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][2]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(2),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][30]_i_1_n_0\,
      Q => r1_addra(30),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][31]_i_2_n_0\,
      D => \slv_reg_array[0][31]_i_3_n_0\,
      Q => r1_addra(31),
      R => \slv_reg_array[6][31]_i_1_n_0\
    );
\slv_reg_array_reg[6][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][3]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(3),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][4]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(4),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][5]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(5),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][6]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(6),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][7]_i_2_n_0\,
      D => \slv_reg_array[0][7]_i_3_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(7),
      R => \slv_reg_array[6][7]_i_1_n_0\
    );
\slv_reg_array_reg[6][8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][8]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(8),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
\slv_reg_array_reg[6][9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axibusdomain_clk,
      CE => \slv_reg_array[6][15]_i_2_n_0\,
      D => \slv_reg_array[0][9]_i_1_n_0\,
      Q => \^slv_reg_array_reg[6][10]_0\(9),
      R => \slv_reg_array[6][15]_i_1_n_0\
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => axibusdomain_s_axi_arvalid,
      I1 => \^axibusdomain_s_axi_rvalid\,
      I2 => \^axi_arready_reg_0\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a is
  port (
    \ff_2_17_reg[0]_0\ : out STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a is
begin
\ff_2_17_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => register4_q_net(0),
      Q => \ff_2_17_reg[0]_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d is
  port (
    \full_i_5_24_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    register_q_net : in STD_LOGIC;
    \state_4_23_reg[1]_2\ : in STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d is
  signal \^q\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \addr_i_6_24[7]_i_2_n_0\ : STD_LOGIC;
  signal \full_i_5_24[0]_i_1_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal rel_39_16 : STD_LOGIC;
  signal \rel_39_16_carry__0_n_1\ : STD_LOGIC;
  signal \rel_39_16_carry__0_n_2\ : STD_LOGIC;
  signal \rel_39_16_carry__0_n_3\ : STD_LOGIC;
  signal rel_39_16_carry_n_0 : STD_LOGIC;
  signal rel_39_16_carry_n_1 : STD_LOGIC;
  signal rel_39_16_carry_n_2 : STD_LOGIC;
  signal rel_39_16_carry_n_3 : STD_LOGIC;
  signal state_4_23 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \state_4_23[0]_i_1_n_0\ : STD_LOGIC;
  signal \state_4_23[0]_i_2_n_0\ : STD_LOGIC;
  signal \state_4_23[0]_i_3_n_0\ : STD_LOGIC;
  signal \state_4_23[1]_i_1_n_0\ : STD_LOGIC;
  signal wm_8_20_inv_i_1_n_0 : STD_LOGIC;
  signal wm_8_20_reg_inv_n_0 : STD_LOGIC;
  signal NLW_rel_39_16_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_rel_39_16_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \addr_i_6_24[10]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \addr_i_6_24[1]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addr_i_6_24[2]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addr_i_6_24[3]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \addr_i_6_24[4]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \addr_i_6_24[7]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \addr_i_6_24[8]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \addr_i_6_24[9]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \full_i_5_24[0]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \state_4_23[0]_i_2\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \state_4_23[0]_i_3\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of wm_8_20_inv_i_1 : label is "soft_lutpair37";
begin
  Q(10 downto 0) <= \^q\(10 downto 0);
\addr_i_6_24[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => p_0_in(0)
    );
\addr_i_6_24[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \state_4_23[0]_i_3_n_0\,
      I1 => \^q\(8),
      I2 => \^q\(9),
      I3 => \^q\(10),
      O => p_0_in(10)
    );
\addr_i_6_24[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => p_0_in(1)
    );
\addr_i_6_24[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => p_0_in(2)
    );
\addr_i_6_24[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(3),
      O => p_0_in(3)
    );
\addr_i_6_24[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^q\(4),
      O => p_0_in(4)
    );
\addr_i_6_24[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(3),
      I5 => \^q\(5),
      O => p_0_in(5)
    );
\addr_i_6_24[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \addr_i_6_24[7]_i_2_n_0\,
      I1 => \^q\(6),
      O => p_0_in(6)
    );
\addr_i_6_24[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(6),
      I1 => \addr_i_6_24[7]_i_2_n_0\,
      I2 => \^q\(7),
      O => p_0_in(7)
    );
\addr_i_6_24[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(3),
      I5 => \^q\(5),
      O => \addr_i_6_24[7]_i_2_n_0\
    );
\addr_i_6_24[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \state_4_23[0]_i_3_n_0\,
      I1 => \^q\(8),
      O => p_0_in(8)
    );
\addr_i_6_24[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(8),
      I1 => \state_4_23[0]_i_3_n_0\,
      I2 => \^q\(9),
      O => p_0_in(9)
    );
\addr_i_6_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(0),
      Q => \^q\(0),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(10),
      Q => \^q\(10),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(1),
      Q => \^q\(1),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(2),
      Q => \^q\(2),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(3),
      Q => \^q\(3),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(4),
      Q => \^q\(4),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(5),
      Q => \^q\(5),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(6),
      Q => \^q\(6),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(7),
      Q => \^q\(7),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(8),
      Q => \^q\(8),
      R => wm_8_20_reg_inv_n_0
    );
\addr_i_6_24_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => p_0_in(9),
      Q => \^q\(9),
      R => wm_8_20_reg_inv_n_0
    );
\full_i_5_24[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => register_q_net,
      I1 => state_4_23(1),
      I2 => state_4_23(0),
      O => \full_i_5_24[0]_i_1_n_0\
    );
\full_i_5_24_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \full_i_5_24[0]_i_1_n_0\,
      Q => \full_i_5_24_reg[0]_0\(0),
      R => '0'
    );
rel_39_16_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rel_39_16_carry_n_0,
      CO(2) => rel_39_16_carry_n_1,
      CO(1) => rel_39_16_carry_n_2,
      CO(0) => rel_39_16_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => NLW_rel_39_16_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\rel_39_16_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => rel_39_16_carry_n_0,
      CO(3) => rel_39_16,
      CO(2) => \rel_39_16_carry__0_n_1\,
      CO(1) => \rel_39_16_carry__0_n_2\,
      CO(0) => \rel_39_16_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \state_4_23_reg[1]_0\(3 downto 0),
      O(3 downto 0) => \NLW_rel_39_16_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \state_4_23_reg[1]_1\(3 downto 0)
    );
\state_4_23[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF555F777FFFF"
    )
        port map (
      I0 => register_q_net,
      I1 => rel_39_16,
      I2 => \state_4_23[0]_i_2_n_0\,
      I3 => \state_4_23[0]_i_3_n_0\,
      I4 => state_4_23(0),
      I5 => state_4_23(1),
      O => \state_4_23[0]_i_1_n_0\
    );
\state_4_23[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^q\(9),
      I2 => \^q\(10),
      I3 => state_4_23(1),
      O => \state_4_23[0]_i_2_n_0\
    );
\state_4_23[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^q\(6),
      I1 => \addr_i_6_24[7]_i_2_n_0\,
      I2 => \^q\(7),
      O => \state_4_23[0]_i_3_n_0\
    );
\state_4_23[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFFFCC0000000000"
    )
        port map (
      I0 => \state_4_23_reg[1]_2\,
      I1 => rel_39_16,
      I2 => register4_q_net(0),
      I3 => state_4_23(0),
      I4 => state_4_23(1),
      I5 => register_q_net,
      O => \state_4_23[1]_i_1_n_0\
    );
\state_4_23_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \state_4_23[0]_i_1_n_0\,
      Q => state_4_23(0),
      R => '0'
    );
\state_4_23_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \state_4_23[1]_i_1_n_0\,
      Q => state_4_23(1),
      R => '0'
    );
we_i_7_22_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => '1',
      Q => web(0),
      R => wm_8_20_reg_inv_n_0
    );
wm_8_20_inv_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F7"
    )
        port map (
      I0 => register_q_net,
      I1 => state_4_23(1),
      I2 => state_4_23(0),
      O => wm_8_20_inv_i_1_n_0
    );
wm_8_20_reg_inv: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => wm_8_20_inv_i_1_n_0,
      Q => wm_8_20_reg_inv_n_0,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88 is
  port (
    LOAD : out STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    o : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88 is
  signal \op_mem_37_22[0]_i_2_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_3_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_4_n_0\ : STD_LOGIC;
  signal \op_mem_37_22[0]_i_5_n_0\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \op_mem_37_22_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal result_12_3_rel : STD_LOGIC;
  signal \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\op_mem_37_22[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => o(9),
      I1 => Q(9),
      I2 => o(10),
      I3 => Q(10),
      O => \op_mem_37_22[0]_i_2_n_0\
    );
\op_mem_37_22[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(6),
      I1 => Q(6),
      I2 => Q(8),
      I3 => o(8),
      I4 => Q(7),
      I5 => o(7),
      O => \op_mem_37_22[0]_i_3_n_0\
    );
\op_mem_37_22[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(3),
      I1 => Q(3),
      I2 => Q(5),
      I3 => o(5),
      I4 => Q(4),
      I5 => o(4),
      O => \op_mem_37_22[0]_i_4_n_0\
    );
\op_mem_37_22[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => o(0),
      I1 => Q(0),
      I2 => Q(2),
      I3 => o(2),
      I4 => Q(1),
      I5 => o(1),
      O => \op_mem_37_22[0]_i_5_n_0\
    );
\op_mem_37_22_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => result_12_3_rel,
      Q => LOAD,
      R => '0'
    );
\op_mem_37_22_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result_12_3_rel,
      CO(2) => \op_mem_37_22_reg[0]_i_1_n_1\,
      CO(1) => \op_mem_37_22_reg[0]_i_1_n_2\,
      CO(0) => \op_mem_37_22_reg[0]_i_1_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED\(3 downto 0),
      S(3) => \op_mem_37_22[0]_i_2_n_0\,
      S(2) => \op_mem_37_22[0]_i_3_n_0\,
      S(1) => \op_mem_37_22[0]_i_4_n_0\,
      S(0) => \op_mem_37_22[0]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => enable(0),
      Q => register_q_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net,
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  port (
    d3_net : out STD_LOGIC;
    d2_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net,
      Q => d3_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\ is
  port (
    d2_net : out STD_LOGIC;
    d1_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net,
      Q => d2_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\ is
  port (
    d1_net : out STD_LOGIC;
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r5_enable(0),
      Q => d1_net,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal register5_q_net : STD_LOGIC_VECTOR ( 15 downto 14 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(13 downto 0) <= \^o\(13 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => register5_q_net(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => register5_q_net(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_39_16_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => register5_q_net(15),
      I1 => \rel_39_16_carry__0\(15),
      I2 => \rel_39_16_carry__0\(14),
      I3 => register5_q_net(14),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_1\(0)
    );
\rel_39_16_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => register5_q_net(15),
      I1 => \rel_39_16_carry__0\(15),
      I2 => register5_q_net(14),
      I3 => \rel_39_16_carry__0\(14),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(3)
    );
\rel_39_16_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(13),
      I1 => \rel_39_16_carry__0\(13),
      I2 => \^o\(12),
      I3 => \rel_39_16_carry__0\(12),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(2)
    );
\rel_39_16_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(11),
      I1 => \rel_39_16_carry__0\(11),
      I2 => \^o\(10),
      I3 => \rel_39_16_carry__0\(10),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(1)
    );
\rel_39_16_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(9),
      I1 => \rel_39_16_carry__0\(9),
      I2 => \^o\(8),
      I3 => \rel_39_16_carry__0\(8),
      O => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0)
    );
rel_39_16_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(7),
      I1 => \rel_39_16_carry__0\(7),
      I2 => \^o\(6),
      I3 => \rel_39_16_carry__0\(6),
      O => S(3)
    );
rel_39_16_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(5),
      I1 => \rel_39_16_carry__0\(5),
      I2 => \^o\(4),
      I3 => \rel_39_16_carry__0\(4),
      O => S(2)
    );
rel_39_16_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(3),
      I1 => \rel_39_16_carry__0\(3),
      I2 => \^o\(2),
      I3 => \rel_39_16_carry__0\(2),
      O => S(1)
    );
rel_39_16_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^o\(1),
      I1 => \rel_39_16_carry__0\(1),
      I2 => \^o\(0),
      I3 => \rel_39_16_carry__0\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r4_threshold(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch2(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(11),
      Q => o(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(12),
      Q => o(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(13),
      Q => o(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(14),
      Q => o(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(15),
      Q => o(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch1(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\ is
  signal \^o\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[11].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[12].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[13].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[14].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[15].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
  o(15 downto 0) <= \^o\(15 downto 0);
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(0),
      Q => \^o\(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(10),
      Q => \^o\(10),
      R => '0'
    );
\fd_prim_array[11].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(11),
      Q => \^o\(11),
      R => '0'
    );
\fd_prim_array[12].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(12),
      Q => \^o\(12),
      R => '0'
    );
\fd_prim_array[13].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(13),
      Q => \^o\(13),
      R => '0'
    );
\fd_prim_array[14].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(14),
      Q => \^o\(14),
      R => '0'
    );
\fd_prim_array[15].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(15),
      Q => \^o\(15),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(1),
      Q => \^o\(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(2),
      Q => \^o\(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(3),
      Q => \^o\(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(4),
      Q => \^o\(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(5),
      Q => \^o\(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(6),
      Q => \^o\(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(7),
      Q => \^o\(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(8),
      Q => \^o\(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => ch_trigger(9),
      Q => \^o\(9),
      R => '0'
    );
\rel_39_16_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(13),
      I1 => \rel_39_16_carry__0\(13),
      I2 => \^o\(12),
      I3 => \rel_39_16_carry__0\(12),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(2)
    );
\rel_39_16_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(11),
      I1 => \rel_39_16_carry__0\(11),
      I2 => \^o\(10),
      I3 => \rel_39_16_carry__0\(10),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(1)
    );
\rel_39_16_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(9),
      I1 => \rel_39_16_carry__0\(9),
      I2 => \^o\(8),
      I3 => \rel_39_16_carry__0\(8),
      O => \fd_prim_array[13].bit_is_0.fdre_comp_0\(0)
    );
rel_39_16_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(7),
      I1 => \rel_39_16_carry__0\(7),
      I2 => \^o\(6),
      I3 => \rel_39_16_carry__0\(6),
      O => DI(3)
    );
rel_39_16_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(5),
      I1 => \rel_39_16_carry__0\(5),
      I2 => \^o\(4),
      I3 => \rel_39_16_carry__0\(4),
      O => DI(2)
    );
rel_39_16_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(3),
      I1 => \rel_39_16_carry__0\(3),
      I2 => \^o\(2),
      I3 => \rel_39_16_carry__0\(2),
      O => DI(1)
    );
rel_39_16_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^o\(1),
      I1 => \rel_39_16_carry__0\(1),
      I2 => \^o\(0),
      I3 => \rel_39_16_carry__0\(0),
      O => DI(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => clear(0),
      Q => register4_q_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => r8_full(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => full(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d3_net(0),
      Q => q(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d2_net(0),
      Q => d3_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => d1_net(0),
      Q => d2_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r7_clear(0),
      Q => d1_net(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => signaldomain_clk,
      CE => '1',
      D => i(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\ : entity is "xil_defaultlib_single_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\ is
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \fd_prim_array[0].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[10].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[1].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[2].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[3].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[4].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[5].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[6].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[7].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[8].bit_is_0.fdre_comp\ : label is "TRUE";
  attribute fpga_dont_touch of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "true";
  attribute syn_black_box of \fd_prim_array[9].bit_is_0.fdre_comp\ : label is "TRUE";
begin
\fd_prim_array[0].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(0),
      Q => o(0),
      R => '0'
    );
\fd_prim_array[10].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(10),
      Q => o(10),
      R => '0'
    );
\fd_prim_array[1].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(1),
      Q => o(1),
      R => '0'
    );
\fd_prim_array[2].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(2),
      Q => o(2),
      R => '0'
    );
\fd_prim_array[3].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(3),
      Q => o(3),
      R => '0'
    );
\fd_prim_array[4].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(4),
      Q => o(4),
      R => '0'
    );
\fd_prim_array[5].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(5),
      Q => o(5),
      R => '0'
    );
\fd_prim_array[6].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(6),
      Q => o(6),
      R => '0'
    );
\fd_prim_array[7].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(7),
      Q => o(7),
      R => '0'
    );
\fd_prim_array[8].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(8),
      Q => o(8),
      R => '0'
    );
\fd_prim_array[9].bit_is_0.fdre_comp\: unisim.vcomponents.FDRE
     port map (
      C => axibusdomain_clk,
      CE => '1',
      D => r6_delay(9),
      Q => o(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e is
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \reg_array[0].fde_used.u2\ : label is "FDE";
  attribute fpga_dont_touch : string;
  attribute fpga_dont_touch of \reg_array[0].fde_used.u2\ : label is "true";
  attribute syn_black_box : string;
  attribute syn_black_box of \reg_array[0].fde_used.u2\ : label is "TRUE";
begin
\reg_array[0].fde_used.u2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => signaldomain_clk,
      CE => '1',
      D => \reg_array[0].fde_used.u2_0\(0),
      Q => full(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 8) => dina(16 downto 9),
      DIADI(7 downto 0) => dina(7 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1) => dina(17),
      DIPADIP(0) => dina(8),
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15 downto 8) => douta(16 downto 9),
      DOADO(7 downto 0) => douta(7 downto 0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => douta(17),
      DOPADOP(0) => douta(8),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 15) => B"00000000000000000",
      DIADI(14 downto 8) => dina(13 downto 7),
      DIADI(7) => '0',
      DIADI(6 downto 0) => dina(6 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20\,
      DOADO(14 downto 8) => douta(13 downto 7),
      DOADO(7) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28\,
      DOADO(6 downto 0) => douta(6 downto 0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71\,
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 4) => addrb(10 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 8) => dina(16 downto 9),
      DIADI(7 downto 0) => dina(7 downto 0),
      DIBDI(31 downto 16) => B"0000000000000000",
      DIBDI(15 downto 8) => dinb(16 downto 9),
      DIBDI(7 downto 0) => dinb(7 downto 0),
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1) => dina(17),
      DIPADIP(0) => dina(8),
      DIPBDIP(3 downto 2) => B"00",
      DIPBDIP(1) => dinb(17),
      DIPBDIP(0) => dinb(8),
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15 downto 8) => douta(16 downto 9),
      DOADO(7 downto 0) => douta(7 downto 0),
      DOBDO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 16),
      DOBDO(15 downto 8) => doutb(16 downto 9),
      DOBDO(7 downto 0) => doutb(7 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => douta(17),
      DOPADOP(0) => douta(8),
      DOPBDOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 2),
      DOPBDOP(1) => doutb(17),
      DOPBDOP(0) => doutb(8),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => enb,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => web(0),
      WEBWE(2) => web(0),
      WEBWE(1) => web(0),
      WEBWE(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addra(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 4) => addrb(10 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 15) => B"00000000000000000",
      DIADI(14 downto 8) => dina(13 downto 7),
      DIADI(7) => '0',
      DIADI(6 downto 0) => dina(6 downto 0),
      DIBDI(31 downto 15) => B"00000000000000000",
      DIBDI(14 downto 8) => dinb(13 downto 7),
      DIBDI(7) => '0',
      DIBDI(6 downto 0) => dinb(6 downto 0),
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 16),
      DOADO(15) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20\,
      DOADO(14 downto 8) => douta(13 downto 7),
      DOADO(7) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28\,
      DOADO(6 downto 0) => douta(6 downto 0),
      DOBDO(31 downto 16) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 16),
      DOBDO(15) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52\,
      DOBDO(14 downto 8) => doutb(13 downto 7),
      DOBDO(7) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60\,
      DOBDO(6 downto 0) => doutb(6 downto 0),
      DOPADOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 2),
      DOPADOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70\,
      DOPADOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71\,
      DOPBDOP(3 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 2),
      DOPBDOP(1) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74\,
      DOPBDOP(0) => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75\,
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => ena,
      ENBWREN => enb,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 4) => B"0000",
      WEBWE(3) => web(0),
      WEBWE(2) => web(0),
      WEBWE(1) => web(0),
      WEBWE(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
PkyhyBb59EPgq8kANKUgNUvJSxwVgcYTKLlfXroHeM6zPnPHm+ATuJPY2OmCojZnDY2A6SHiMUmx
ylnsx6jVAA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
XgKClVpS+h3z22aTgNZepCZW5Yffl4m6nNLRjY88G0b6Og6dF7wA3of30X3Vr2BKX5GVSe+jeu6a
q3D7Qa0T3sEnO1qnWdbom/P31G6nS7/pQCPaLh+suxznQX2imRfhfTkmY1B9wExxZtZBbss2GPfs
EFGX8a+efiUiZLAKaSE=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
LYMHL9qwz9VPPAbHAyLFK1YM6t0YBJUbhdak6y3IQta7KscLfLakFo9QXv7rXKj3R5WEjx6Vg+9K
QUgoa/uCYy+n2t004DDpVeDamNuGIrJU3WXV9mo6tEi21Rm+kIG+CFgVuqLY9JSjwI3dhmEqYYtS
wC2GIO6hKaV0keq1ldvsRFBu71kLY+jczboTe6EddpUktWp3UM/RqnrSfHPMlZWhHp1k3YC0SDq9
gvcPn9DB3vIjXgn+xRbyzZOt/j+s8RfjF446i2RalkF5p/den9o/OMG5jmv4rZKHj9S1V3Z2UuL1
c2fxe26sNIvZ7tpz8RHVWRMloPfcPVakam2zhg==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
BACIRg239ZSAZHpsLobWk7IZyWSAM1rsaZq5LesIgnba07iijhvT5s8WIOIIgHZs1XEDKelSnU1J
+5cyEbU9WgPZsja6FQEw6J0GuN3L/1QyrvmNIJKsNXINx7R+xaY/n0uby2eFsFE9luplvdOyrCEw
eK82BghXwPdasTT1ZUgKiycyGYtNsp5ZaPIWXI9ezN9oHowcWp7Mn6v2jrdDl4lzJuoHgqRtkZvG
7GqevJFheGfXkRPuQGkNK2Pk6XN9woSB1a9C+FUsQBM5MlIE7zrBQAjONIQj/nd82Hlp1H4PRxBW
1mmFP7PskMeNR2hH5xwkvg4Q3IfYBlw8gdzneg==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
vUWbACu3JL9XeVH21XChN1bLnACIM0U/dLRQNf2LGaDFNW9CL0o3SY9pOtV226o71+9Eal6i7P4l
ht62RU2AHTweJsgWkXtQBI0/jHIw4/gxbBebNbqZM6m3qjEE5blPsuzJ1njoX2JWCJElO3p9FfRu
uHpC+4hYoccdFayGku3vk1gwz9lLJ4FcYG9mi1vLIY+tzs0o83THQ8dLrg50Rr/r2n0Xf4hxWe4U
tJ6iUOYBQUYjeOwNQOOxfjv5PKfLIgGA2WC8sJb2GFe9MkTDoMAo40nBLK0Y8+klDIJTyx079Bx0
wdRg2JxUF3+TGlXW98+2/iWy94H1CPEVRm18FQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
VX8rVAT0l4oniSvb1X0sblwaqcWh2XE0oCAZbC0SVv8fCy8dLmmtqBzFq3w2V/7nyMmJzWKNP/yV
0GW7ICEfrGaBejU3VpwaHA69xE56Y/8NSHGlZOhr390/5/UqELcFOknZEPJXMLpeKjUn2ijACn/u
O0myDIvGFiUyRGWWYKM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
dlKAt52rb1rebbUvCxUw/pmWR03F+be3vApC1VuekYTvk7BFt7xopdHrqsvoU8rgaCBc2wuCudx5
nUcu7bKEyHKFc6bcbp6J84c2uG0ZckyqBn/OHRMbmq4Vbar8C3ERI2YmcbL0Q0fBLzMosVarF9eM
+c6VfE9hA5lx9qpwFJhgk5v/yx6kjgu+kEnG+xsdWrpKrj8LIxxh6gkrPOn+jQtKQSX3o7q35Rcv
W3vWLRYdH+pHsfJqCdT0wL4oBTLa7ozdsufX9l6UDgT4ECxLf7R1TtNj7XA1jaaefThL0F1AUCjF
5WuhMqBOotpDZUmvB91yVtbXLMm0r85tK9b/iA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
InIrwvwO7znYwz1McidkIPtIe2t5scQWOh4uXlyUWB+630Ki9vmZyzyB0e0lI00sMT8BDjDVC+hP
ZM9L/ou45n05ABPICm88J8sOKRgQ2J7N0xB/7Hi70CiRLg+0Kw+6P6veU8vwuFaBjB8kRzpZQ/Zk
YRo+Azk5Jvzq2n8qc5nOzhS/GSoZVGHuxAuLFRw8XzNIr6R5WngSx/0w3GLQUEb8XhO2yC3b/nX+
tMZbYom5/0dupI3SB6m9zXbqmiX9cbgfE97ufZuAzAGDg86mMJYWHp9ABhz5x9+1EWpKKwH43oWR
XvnL9r4s2F1Sg5QNLkiu26EhUK8BYEPCUM3LYA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cp8HZ9vNgKytxJ+BJN88J/mf2eFYsmEp01XLPYE1+50HFn+7JKh+3yUgov3RtQxTYW8j1HDFgfCz
Yk4KTwVNJO3HmZviwxi8RNYYofczw56mNUpepOk26eukGXlGLFu8DdbPeOcA+X7/MC6BvNNk9U/I
ZZJnmSGKNa4x/RNAFR64iUZJLywuk255z5aKQSWmr1UOJTohTlFE+obfEghJHql+LOeFk67FIvEk
rRUFvMq28Dm/wqitYsEiVw5rUsHwpRPpaiLLn8uaHOre25yvs+jtj3eKYkcZpOp6L/r2dSrWClDp
dlKnOeW7KGp3eoAzXmZTNeo7MK39uC1bvrqztA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2864)
`protect data_block
iVqYXBc1/jPj2nn5a7zAMcAgJbp1YVMr32TsrePs6e2bo60l4jkaCxtvtrTr+tfRSvqouKvhlgh9
OzdtajRiDy+jn0cjCOeX4RktwK8c/LLWTEPYU1RQEvGsNLmjdeH6HL0KUwMkV3SOHPlg6MMYdkUq
mgBA9iwL4StOnzjYkTg9VS0GSgptvUjCQWyJL7CkXXOYb334CuIJ9rNfHnr2wqDVbHTV6VInj+/1
axWS/0PecSBNgGoj2gM8iMZ+usW1NasalBuX6jd11Da53KhLwK6FsZSfYS6RlqkC2ICoYQeoYG2k
ignW1+RBxosKW08AxIU6ClwKlLC78/NGExbehemmVcuiCRF29eMbVh1G6OKX0sBwUF+A4+9iIxKv
gX7IHI8bRfoqgg5R4HdkaSRkqYTxVS8BIyghp8hdtpQrpEaDf8Y/IHWygtNRKkO2S15EmdDclRW2
te5JsKo015xa1NQLV6cqNHq+qlTdA3e7/ma54KIVvBLErZzFyZhWJV+7RcPvdA38PfEHzmjfMQN/
D0HJJHIGGowM/plq2wHkgSyi0G6jDRmqLskQmWT9TGXAhjkXryP8Z4bEPRuwyfFHWXeESHG6xPYi
kFWjAYWe1fLBjmQmCclrgUH1wPC2/bQIA0HZfnmcokdlwIz8YB0XS8iV1iR13LhaHaiuAq5A8Z83
4ivUga27qoD8Anx+g12HuOeKAlGhaeaX5UQ94AX59lPrpTi5qNyMpVp1pIc7/Q+WpgcWgFWiCMSZ
f1vD9zUa1KSHR+6InWd9O8WFJ9cRO4vuTTSm80Du9oXeeKOV/ZDQ8gvOO/RHmuiUWvavQQk99IFY
6//B2yx7NSVANRnlA0d/8M5rvclnmRyYNEV0fLnMf74xOYmJp6s1BBxkCoTF+lkI0+G7wx9mrVMz
usniUhChTJfbe4GlCpgGBLhyVrHtx/Xkg/fVxS/6+ktJApOw8ClPD1W3N7A9RXCjLXnx8slT698D
QTnKz70OLzbdeaO8un8T0VU09GEROzH9/XdwVy6HvjbIOKyYWpP8zn2bdOW6mHWJsmS1XVHMlZKa
dCl7NoIQbqbdknIM/CB/ub5kLqko0MB1pCEqpsHBRLTvjYiHQf0/zcoclWEKv2CFWu8pfpOcttFl
am+SO55TD9ps6HfUkTU0b8Usxf78im4gyp8PVRvqZL3O4hvMzvfTHGqLkvYBERqsqjUj9ZKi/b+d
E7X0RjtJZmICE3C7EQAKMuMBXqjJqvUTr+U9AyF3J3DgXwD+ajB4Rnkr9KTLFYlUKlOgNd6KQN+g
tDNmOAOYzb4Y7GHfNnQbdWOjKxH9/BT9pXFCEjPmRbVTAS1cURkp0YfTW7kRhGFFW6iHbKhA/cKY
P0MmM8w27XZkzoBZZouFoIyrg+mlbZ/pxJCfLVufnUlnZtWwJTe1dQTLeEbGK6Vro3XjA3KYPwbM
pusE8ObFOhGYG+nRiNJ3gfz84SxeOCXDXIHKZf2AEKxQu175u6Q7s20FXPiGE/DqxI+2Xftfj8WE
BH7jKwI4UFsEfh8SpINS7M7msq2SvfgeOwtmcwhowHdXYHa/fRA413/HhzcVwfaiSiRqYiYaOW3B
tP048IioMYQOADy1KiWGzhEk1s0YmtitRBK8tNG4lvwpEquiMlnvspxzX7N4UqyOlCzmgoR587OK
hqGdAWCsLWgRgfdGBcflt6aTgSiWHaK7Tm0PWHDYKfrdvDBNhYKJ5UvHPumo/HhN+YPRmF3KYFPs
B+C6NM6/qh/RdFhRudKExH8s/upt2HOADET8sUlzsTzg5/w+tbNhvBay5NgRCwD43hWU5GaMMuOB
NCmSj46doPv31qT5tGYnX22IU9UlmyIo2iZfZVPnaqY8d53x4GMCyfqlW7et9z0S8L6lLO3NQfev
OdhDTS6lLU1VF8zwmUmylcitZRWbgQFalPSjQi1SCamGtl0DM5jO3i6FzFL4+HOF/hFtBnj+ItRk
iLqzluZBabfvWERV/eifqvnRkVxzWYsyOCvxeozRrr1BCUY/BgZefHYAXiib7jmBqifNPgxnkGM+
iDDnTOLMBb8kOW0shqJGCmDnQSJmfiZHRShsU5HKRtyYcVSIYvbKQNfbkyA3weAg/k6SdQR6vbn8
pKER+42mQQCx2LA02361EYp+NfQ7O8rx6rPa10rJLtpRhceGw9nKzNDJsZITLkrlxRzozZ9ib5mU
gf/wYvMblwKjzF5ZJ1qhOw6KbSy2wG7dLOlJM2/zG7wyWGPjz23HzjDU2f8kDtwWw1nsZAtyQ00C
d9QBCI9h6Diq0MfdmNRdVG2lxTTFoAPmYg0ftflvlwe6OF8xYXC8DtGOs3/H4qrYOg3PV/cUNXtR
o/6Pb8LDy4yYEvzu6aMnX6IrENVHWJNY5ST+Z4K0nyy5+3boPdcZieJgn7HK6f3JNzAUfATCAgKe
6fMjmbPaFM2WpjV+zrAx0jAlAW3ISc3yQbwwReAZbBe5NPzdj5hTNpbw1YfoNZwGlNznRJnBmuu6
LX9lwE5PD3YInfU7BRFYBRxhN2kQ2jB0/aGoXkxgbAo7eOUrQWSk35AOg9ufPK4q5xYWXeHjCOvT
+2+/VEOUCr2a4/PBvYnvwRwRDutpgazM62+DTXwhOHgN+02cLwcmWciBMHjeLTZzj9D4ncroFzMc
xhP8EvxrouYzaAwtsXMil7X6WPK5citqtTIXxZ/GNrj7nskviWNvBXZVZnBx961BGPAZhQJeSReX
KdbZNZBpZ6cuNcWo3sUfEPgOUqyF0FuoFRvk0aeFyd+FoLRayz/ja19E5YPQtp/Vc2gqoRI6bZVn
3ajtt4Z88jA3+FfQXQe8+diLmAW2m3UG56/kPMt0d4G9HNmvfWRnOLhJ0X741z/tN25VRDhwdTjZ
laFSGBC8xtTiqKyfVtAXiAwbHIECHNPMadKdd2/CvPI5T4kb1c1W+gInU+6BMMRfZ4bwu7t3r1/H
UkOCd8zIAj/t5Bn6TJID9qQqyQ+U7isI9z8wGPnRmAXFE5Vu+9n+SaXRgkym+rNqkQUEE/plmtFw
gC4aVrp0HP+yzRhWHO9k7vJ9QDhZ1vDw9vrLYpcdFiZZYaufu45IAGgCkaBUBJ5TF2EGPFKKrWDK
1LnOgJJIJGQStbH2yRtONYnbihocJMxWFz1G6sbqAg5F6alggFUlQ9urLuhPuktkSPRm3cPnQUXf
HIPDaFfA2GYzOPndfvVX7eMKk6POrij7KealJD3BZhpIm2V4vFYMb1AVVuTUWFb3dm2iVuAn7M0K
C2hraLy5ilN1aXxvIp4qvuqskGvTSLN5+q8C4/SpPGFk3t930KES9irwTZ0gn609BZOdGPfKNbHy
PgOzTQLdLmYUO2Jfk1xwBUT7BgoPE7hazaHE/6ye0L7U8i4/MPlEfwDjsDKTiU/YmR9wOz4pXXgn
U3aBsdnq6TkXXfuM6J7yfCB8Vk21mGnw45YB1VW27ZUOw2hIcF9MGTIePtrbitMWcO3bQXw8PXpa
eV0QF7ccJQAEgr7t3WpNZ9wkzAj1wMJ6wYZBTGVwdUp5DJHQGYq7+J10Fyv9qCSsiXt14CQGHBV2
FiG2LCH/e3KQHaPw1ULI9EKGEAb+o3PlG2rCKoFeTIDrhnPqklAVmSw21I0yMmGD/XbcPJWmMN8J
w2O2mPiBWnQPYR/OyY6BMG0DszSuND8lceNCdozftZJw7CJs1/+XLk4e2jXEYJHVVOBf3s39Pqpn
WYamOjCUWlBBf4KIvMp+9Hyg5qguBYzTQtpsSU8hrqqnaklYTGb9qPdc4QbMZOKdm9EsTRWWcQF2
qekEe2lDmciK047DTOQ=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface is
  port (
    d : out STD_LOGIC_VECTOR ( 0 to 0 );
    i : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    \slv_reg_array_reg[0][0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[3][15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r3_dina : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg_array_reg[5][0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg_array_reg[6][10]\ : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC;
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    r9_douta : in STD_LOGIC_VECTOR ( 31 downto 0 );
    q : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface is
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^axibusdomain_s_axi_arready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_awready\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_bvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_rvalid\ : STD_LOGIC;
  signal \^axibusdomain_s_axi_wready\ : STD_LOGIC;
begin
  axibusdomain_s_axi_arready <= \^axibusdomain_s_axi_arready\;
  axibusdomain_s_axi_awready <= \^axibusdomain_s_axi_awready\;
  axibusdomain_s_axi_bvalid <= \^axibusdomain_s_axi_bvalid\;
  axibusdomain_s_axi_rvalid <= \^axibusdomain_s_axi_rvalid\;
  axibusdomain_s_axi_wready <= \^axibusdomain_s_axi_wready\;
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7444444444444444"
    )
        port map (
      I0 => axibusdomain_s_axi_bready,
      I1 => \^axibusdomain_s_axi_bvalid\,
      I2 => axibusdomain_s_axi_wvalid,
      I3 => axibusdomain_s_axi_awvalid,
      I4 => \^axibusdomain_s_axi_wready\,
      I5 => \^axibusdomain_s_axi_awready\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0F88"
    )
        port map (
      I0 => \^axibusdomain_s_axi_arready\,
      I1 => axibusdomain_s_axi_arvalid,
      I2 => axibusdomain_s_axi_rready,
      I3 => \^axibusdomain_s_axi_rvalid\,
      O => axi_rvalid_i_1_n_0
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog
     port map (
      axi_arready_reg_0 => \^axibusdomain_s_axi_arready\,
      axi_awready_reg_0 => \^axibusdomain_s_axi_awready\,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      axi_wready_reg_0 => \^axibusdomain_s_axi_wready\,
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bvalid => \^axibusdomain_s_axi_bvalid\,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rvalid => \^axibusdomain_s_axi_rvalid\,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => d(0),
      i(10 downto 0) => i(10 downto 0),
      q(0) => q(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      \slv_reg_array_reg[0][0]_0\(0) => \slv_reg_array_reg[0][0]\(0),
      \slv_reg_array_reg[3][15]_0\(15 downto 0) => \slv_reg_array_reg[3][15]\(15 downto 0),
      \slv_reg_array_reg[5][0]_0\(0) => \slv_reg_array_reg[5][0]\(0),
      \slv_reg_array_reg[6][10]_0\(10 downto 0) => \slv_reg_array_reg[6][10]\(10 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller is
  port (
    \full_i_5_24_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \state_4_23_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    register_q_net : in STD_LOGIC;
    \state_4_23_reg[1]_1\ : in STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller is
begin
mcode: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      Q(10 downto 0) => Q(10 downto 0),
      S(3 downto 0) => S(3 downto 0),
      \full_i_5_24_reg[0]_0\(0) => \full_i_5_24_reg[0]\(0),
      register4_q_net(0) => register4_q_net(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk,
      \state_4_23_reg[1]_0\(3 downto 0) => \state_4_23_reg[1]\(3 downto 0),
      \state_4_23_reg[1]_1\(3 downto 0) => \state_4_23_reg[1]_0\(3 downto 0),
      \state_4_23_reg[1]_2\ => \state_4_23_reg[1]_1\,
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger is
  port (
    \ff_2_17_reg[0]\ : out STD_LOGIC;
    register4_q_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger is
begin
mcode: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a
     port map (
      \ff_2_17_reg[0]_0\ => \ff_2_17_reg[0]\,
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg is
begin
\partial_one.last_srlc33e\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2_0\(0) => \reg_array[0].fde_used.u2\(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0\
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
  port (
    d1_net : out STD_LOGIC;
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net => d1_net,
      r5_enable(0) => r5_enable(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
  port (
    d2_net : out STD_LOGIC;
    d1_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46\
     port map (
      d1_net => d1_net,
      d2_net => d2_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\ is
  port (
    d3_net : out STD_LOGIC;
    d2_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45\
     port map (
      d2_net => d2_net,
      d3_net => d3_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44\
     port map (
      d3_net => d3_net,
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1\
     port map (
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(3 downto 0) => \fd_prim_array[15].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_1\(0) => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0),
      i(15 downto 0) => i(15 downto 0),
      o(13 downto 0) => o(13 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => o(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14\
     port map (
      i(15 downto 0) => i(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp_0\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(13 downto 0) => \rel_39_16_carry__0\(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25\
     port map (
      d1_net(0) => d1_net(0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d3_net(0) => d3_net(0),
      r8_full(0) => r8_full(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\ is
  port (
    d1_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r7_clear(0) => r7_clear(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\ is
  port (
    d2_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d1_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\ is
  port (
    d3_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    d2_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    d3_net : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => o(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\ : entity is "xil_defaultlib_synth_reg_w_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\ is
begin
\latency_gt_0.fd_array[1].reg_comp\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3\
     port map (
      i(10 downto 0) => i(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
begin
\prim_init.ram\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(17 downto 0) => dina(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(13 downto 0) => dina(13 downto 0),
      douta(13 downto 0) => douta(13 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 17 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 17 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 17 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 17 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(17 downto 0) => dina(17 downto 0),
      dinb(17 downto 0) => dinb(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      doutb(17 downto 0) => doutb(17 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 13 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 13 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(13 downto 0) => dina(13 downto 0),
      dinb(13 downto 0) => dinb(13 downto 0),
      douta(13 downto 0) => douta(13 downto 0),
      doutb(13 downto 0) => doutb(13 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
INaBf8vh5mCmDzf2yp77pxZAxQdyEQiT/vG2dEgvrFjseUnGc6ldwH4JvdnpZSpdf/ihioPyMNjl
u6ooyzv5TA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S5XIZZtuFR/MZffuhwdnvE3H9oRWM4uXoaGZTa/Dyk62O+Wa0v41pjmZELCiR7uodZPFQfykZ6K9
2ZDMu8dB3afQRMs5lnd/53M1b9ke+MNEeZ/wzjUcsJghubnEAwzdWeW/0tlqST1WD9B/KCxYqwH5
Gj6IZTTFHAXcaVhnCT8=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
CM6IcdzP0PbD6yMSqylmi4JE2qpmxiNeI+prjGwJiD8e3Xsynu3PbGKJAOpOxtR1hT/3mpBcx1Rz
Fkz0QBh4wtE8fiziv1i+xi8T6cqC8ClamjrpZ//sn6dh7NvwSYik14MlwVuei4DZoZJZF63aoPUn
RXkQ13wtK+MkYKBcPVSZMFZmaCU6jMMBYclXzvRG1JqqZoa7mWFTeFZePUTXG7Wo12QaZ8GUi0AV
UIshoN25yn5e2Xr3FyuEtm5AvsZb+iLsgLeHBtKBnsVaHQphicgqwgwv6MQQF6ZNBgU/aACfibDS
3+n/mMMm8k1cj2bW6VCi7a+c8LmCf81NlJuLww==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ehl0CusS7+JNGq6HfhyaBMy68nccIdIGqixoEztEZfkCpXuUYsdqw6G9MIJdWdu0Ck2acV7K6IVg
rzb8/bNaDDVWp48kupToegTkOdwDkCejEqppido4BkJ+iEkjPniz+aJHlOlOwmauETy2hCMuuC57
oWDprzGWlsqbCjqzKrXmPYm6fNdcOa2DiOYstQaMFNbPU2ccrbLJAiYMHNDqtPNqWxKBsD67kiGf
2eOneDOmdmy7YkNsL+cx8MJc3BVUsYBrpAEsGyFMkmX8a8nYz8R/wlFQFGQAd/t5XrfxFNI58mj1
AHXbcAMhGKVq9YdKeU/vSXY/NwMqp12xJ1nUaw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
h/qRAwiPuqY/Zg/QWqbaYm8xWTi9SshYuPzyL0UME9ZDDF+C2CyGAugh9HzMdD0kZmT94TKmBgLR
dKP28nlE8VCCU5rvbjKxfn/wNtNKHCvZ1hns8CF7+pGuelhxGvXNmYKFw5co8+4grYFaDXeoZZR6
S5sjvhqtSVD3+qq4vYWRjT2Y/yes7L9dRsLq2D3iZ4xjgVHuIbOQLT/EUKW+9iYudT9Uy6YTwB+5
mSb0QK3YfZdGwZyXB4S3mdF9vNQHdW/rnACq3yngF+lprNkh3ooQKdGqtxtz8KSQxNZOAFE+koOw
h00o7AKpvDAp3uNguLvnNJH3rugOhh95b8Jatw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TsA04vIYHDZne2CBj5bWCBFH4MtNoFDCn/3DNEi0BwutuUf+X+lD9kAO3kl352WHjQbF79Ssm+PT
fCYpODgWdxSVbzaHFpITxCQ4HcIJhUeW5PC5tw09Tand68D6eg84qRguH+llbb5jdGJkJeTCf+Mx
pupkkLiDvNyTYWe+nqw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rx9hgQkvaJJTJJcTjGFW1DrrWiT+xanrcMvFn0Z3KRXlZvf+SE7IQgGCiP7ZDA6T5z1Zv5nzS4h5
cVi+CvwC9UMZRWmLDAjzASJ2nx1g9BjbYe2vHAUmyurIiR6LSigTeM/9TlMv+fFwJbqwuH6FJ3/z
Vl4tIMk4NrqkMn/riOG87SjhesepM6kcQOBgDGzLTG14z3qeZG8OPzxgApfyubmX4qdD1oTgGm2u
Q4mQfFxEye6Jqkn4Rzjhifs/ieNYomHlK7R2/72QJj5j0WyYBIhvO+09izz299Z54ZP2ZXaRMfDT
lU4lQNqQU14PX9Yk9p7sy2PnK4vTwwF0CFIgSQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RHJhOCOMV6J0g7rOG30mH12E0qjR4OqI6nnfwP2CmGqZEmTbXLfCJA96IDYZ2BpBr25QODvKMWCb
zh1OjGKw3oMN8sH0Zu3JKcW4UVKL6kfDFOyS668uGbBRJuL2VNpI4sCntqupeTJ+uOeQpaJGFQjd
3jk/ym+yXwEnoXMZkeigKgKuXYEwj6Ew3kp5qMx5E4FhdHteZP8MHFHop0MdaVDUCU+EhbEKM7aI
QA5rH58I0ZvVPrrrFumYtSOHLhJGLheWy6VmJ4eYMBKYa3kDhuYiy4Q2n1leim3Pqh2LF33JURT4
YenPbktzkhZPTcHH1V1giIEATq6sv0smZMH4eg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
fgfzseA8fSzPq4zXLpoEKcDV5WEiMvbFovpnfKlQJhkhBn7bBPhn2h2nLs8qNZT2USQ7HDmBlHso
b7Q2djCWd6acYEaAD6HUTXAB6sLnGwLyFM8+CtzXTO16BxIIb7zLKONfOIrC22a0EgX/tqpPemw7
UhtxSIAdaNbKmPoB5dl8qP2YeJb92n1EtiDrGRvYhlvzQfq2JGczGDaVTHty28+JzNRnGrpQvJI+
99e6XBDwuAtWiVpslSAY3gePuxocSddFXHdL/TFVAMD3e+i6ADBaAtsuk/RhdfXcrUdiZhre3fvY
eJea0q9/YiLQ5n+Fq5MtWWPURPklHvvNJzwiJQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10672)
`protect data_block
ul6ENJPmVHPHLVihorGm/Sxl1OzOueqfHKpbzzz6fs7SXeWa1plChw60+bf3/FjzbeL649aaMwdz
pwZURKy/2jFr9zYWIpOatyBozOjKdXpAX61FI3tcnVBT7Q/XojIH4H8T+t3XxJ6LlzvpECHdRN1o
b502VA+CiJr8qsqoUaVW/M06tTFpb/d5Pxl/iWjpLYSszp7+5LstTwL9g5SB6HuqIF/KdFBkKPF8
GmCnW9+9dcl5GhfyFe2iFTNL9gvZvKcZu2E+L3TtgAhtlIINcMhoJGmQvYxMHPN2qlnmEYmA0jQm
kRv4/aVXrayn3Ge58Z1seGy2UhYpwoqTXjXzW2KGDkM0RLPa5JRQOZ+75tpamAyJ5+tg4fQrGtxg
oIdn2ybnickswGzyqY1IjqfwOrq5FhIYNWZhp1cWB6zXJ7hj9nHQBmUvnmdWbx+7kEz1N0aSx7QY
y9vmwCd4jfuILlNFYCTiruVLwbWEKTcDPYFcD1B8lBTeqHyoaYh6DV6Tn/Ex95TgBRZif6FhuNtz
9PhP52+DrRcG9bpV3rDca9JsnWCgcvzmracVZYf0zf3TILVk/5rkko+k1Ln7xLhIwzIOQVdd5Irm
Xe4eZ6Ynk7vmjnN3OeYPWFeKEafRQIMDh0rpRiMDt9hs2JwjYmw+ADQ9stnT1UIrGzgwCjTCrwFQ
WZKDt9jhmn7EHV+MbyisDdU99+CespaKbNXKb/VBRXkeoI4o5g7t/0DPV9x8A6Du7c1uQvZwD9p3
dwv4sKuuQPaTIJwnTbpLyc06Nfs16ZI8NHdvHxkG1rYZPvy6mExxWxGp2sbhGejDDZ/NGeZ9d8hO
DH3IbT9yOxdMXXbMWbHJE9EJfn7R6d27DtGqGbKuyVGNMRpRgFaJx3JRmXc4KsGflz0IYMznK88p
UcPss2KY2Wgwpx+wtkL3Frk751iqx6Wf9Xdnv53ReK7xHvzLQCvMy84L+M6MN0wtEPPwoBiwt9/3
MbWbVrBvS9a12sk7CNcdT1p5LjClwbhQ86rIgaUv9ZaVCNM/tb7VcKKkmzRDeT9EBMGG/DC8vWOD
vXmJKfOk/A4rP7B+a+vorYNqmI5hI7zJasQyIlxSDMzgser8XckJcJ8h2Y+C6YvNoDREuyHHq0jb
tK7a2xRaYbiWqvQmRkHc+CR/Hm/s0S+W5cF+bOPJx89IJ1/Jm4EfeLxRJsMQA9BppIgW2rW6O8zX
CKdAliqXD+WNHaBDt/zqPYOB2rMu97+mHHYXgTUrSxyV5EDY2gk3ERwSc2GRITBZp2Az/bWwUqJX
MVnxdPUMMeIs1/PsS/55KKSAa5wFjAC/4xXtR04P4muhE5FYLlTE3TZphtTpSevIT+atMbgRaKtL
2TqPhW8WbcMhLnuz4LNaSUb7XuY+0seU26bJZkiWGqohC0rV/K4kzb4jRVfrpFhWunvQdfcB6CBt
KFwDTPs3vK1pHf9duGmRq9ayqPZLj8uRbsj2wtteGupeVxETO030CSB/wtftck/cU2Qbmmj8PyoI
lE17osRoQt/l6wxg1Af7HqxCZY2/s7peXqhpv3DJrB437RRYBluCm2IFt5URk1xQK0mSNK4E+kWk
rp+Tzgjf37nFtNbnUigFSuhgvosJigKVwQ9rOYhN/81EK9Qj1brhiDYaSqB1pRHwayD94daxSv/O
hbtL4jxUIN4Kvaz5chTQOaQk1J5zDS9tImR2pM61CvN6BFc7VOrRSUHU4s1qPzPFkFIcuB/ZFoQ3
KlxOTG6CY4P8meTn0tRO/OWOZUjW6pHdjcR8NuUlHBSHvFL/nyjc7HJ7G3n2JbM1FSJ2SGbrrRyD
V3RrwplvhG8MiayqFhzhVhvzMmkSYWsfJB2KE//l4ZQYgf700NlWFWKRo1eYr8yN2rhY83qtNh7D
PFc2xxFomrS2RlNChg4lQduLiDC3RBwRNo+M4XeVjcdgo6Q3M9v3CAhnxNyVAAnGlYAVShcamMb+
ZceVLQCsxxUptW15Ix7WntKaUImWNZjX4hDLAMikGFvIPJPpO7tyDGl0xEIbnbwoE6zUUa5HMdQK
G6fO2ZOyWNl8cEbATM6jOFi4nYQgomaCjlfrHzqBAXQ6oKyhSc9ad5UyVEu0DCg0NXc0CklG04AC
h1NnwU7TfFzw/C4FWntIqYw2ubmDg/zm/bXURo5wljNan4RH/qOQeI3+g75X9jVq4O6qWLZ8U+X1
Uzmn/JoMepPh753TNwF1MhvgC4HVuHI90T7kCwkVidF0nTigfX0maNWP2f5xVTfQATh5932xq109
gskyzwS1X5vCPaqtxuujK/A8/mGE9UheyR7b/N43E0Q5Ir7CGbRVQ9c1kK0ItEd2OYE8V4oFWVoU
MFtDTIB40EFhQP915K/6is3rEps3wWV2HPoc8y8PimR0FNDa8ixN6iFlOF6j5Vg7z1cbY/BMfhBb
tuC1YPYgL2XnMa7/iAYifc7HPlnxwDfxzergKqshoiiUF43BlfyEjNXOnxpnlnfz7haOACUqw/e2
bfV/wWolyV/mCy8rT73Udz/0bdEf4ZiFW4bNlOuW8J1IQHRD3LgLGXxw6QtkIIYFjWYsZ73D3Uv6
Ury0TqKlMqvGiuhOMkxFoTEryHlPlXEA5KClmxwccG+IuoeAmRBIQzswQofoFkr2aPxqUbkm5wwt
w4j/jW12USLGP1VJrifYuNIZVd/Dq2NUZ2wffCcRafrpdvlanTn6XOpDE9HT2WPU9DdAOVgtZ40O
ujgBPZ/TxqRfP/J3Gf39Gg/ssBGT17xBCFo/SlzvDWzMLK5SJEK8xyn8ZudG5FuftAoL0A7ivSVq
pv0h7/uJvED+BvZDecMCu9F8MO+29loQ8++f7wrwnB4d7LQS2NHzQzUQUxUtJOgcO/f+gh6GVp/T
q5tF36szwCOxQ2pgrfdR9B4tQSCdoFHVu1bM6L1bt/Zo3LTmnvJgngCGgXzOf0ptVcT0iA3dxQQ1
yzhKFsYMXTd0k+cKLHlWuds8qbfaOXE8OgHw6RCsb6Skqvpnnw2uLv1w+tUUU0vfqQBYL1Gs+OnY
zATzKcenpI1Go9t0Io1VnsaEkwNUPY7LFy1xoI6RnE6yNh6qG1V5q0TYxYGSmBg/oT6xXNjMMdGl
WA1MVnWq+DvZNADg2Rd+JExaDHAQEv8kNU7qGFvsjqAmozzXZN0lVl86bD1XJkqODwakolfG2Dwk
K3DgbGXQntDpa2B9ItRhQduQDdXri4VzEVKh9CtzNQvFHxsy5A7PQdCeQMh3g/BOBoijly4AISKk
JT1YT+4mMblPTs1IVwKuf3eAbpwIVWrHnxtTx8RAGU1pLpAO0tGoLq2JLUJJ8uTxgY9LRXffCTD5
wTyjlsVr1i2GSXyjmMvOuGjWPu5oYFQMkIILDYkdEjPNXGM1oqnHd3vpm/gJyN5EwprTVpge4qfv
rjmeCcJVdDenp5GYIBHm5WTmd1KvYjEiSaATvRcR0a9BPZKOwhtnuSrrcB8zah9z04UxOEnRTHRj
7mONx7qgz1Zp1DIPnmpQN/X6CjKBhXVkM/cCM6Am4pFx7cTe9WwTyEK6mwhEI1VmsqzWs5W0xoZU
FQ5xEU/uhyo9sbCYCK3xTSpK/727mY+vDeyrVg7CdOhSRdGOsnI1ZXZiZDSZTkVJ55hL8XT/mCiq
WUA0ZQQ0toMAp6kQlQ+cSjjHKDC1NGgcKEMb3qtw3YhBegIKfFgONgsQT5ADdKoxU2XmKGmlSZte
uMAQMM/hgV4v2w9OwBNhh6TWRHPkUwqkjywrNQUrpy2fOq0eANa+frQBvftzMfcwqL7vtroWE5XB
NK9fjsGryJlIhbciDDNIlwFs9Oqi/GSDSO4nVaCHKcokTXm0S1lxFZcjejzMEus6mRD1j+RyJw0+
Vk2ZmaAXRBoaOSROmm5amy3S0eeJhtXStPBeC4LMRafrYemAHju8QC+Szy5SSqalKsWbZYtwjEZk
Q5PShHxWXnTIKWnh++akYV+ZlNcf9seqP/kIpqFHlliauk6os/lTYOEQLCK00jagPqmkqbusN16x
AE+qQTy1dKlrQwmQUZuiWx835z4KDElnmbxzv7Ub5Y2wyomhc/y39XqSfc5sDPsrgy/6KRJw+eB3
IowyMuOrxsfRVxKR7QGK1TGIBYJ5F/fYdMfoAPAtZZomfR/Z4gt1bUZVG3e7NWiMqkuWZlZz9J3n
Vk7/jIDCyW37MIDhDLa2j49fZ9FsT4Nyus4bGBeCKZ80V8ZxK/MofFnNo6In8VqVonnEimt61ndz
PWeEPYiwDPWWu8M+pCz9c0Z1GtGVrNHnYbqPzInGQwhALMSqEmIQJnVtloNIGAtjXik4vmej+SIR
WGUGVQtKRdHMpgxMyP3Ao0Tgdjb8kWNxc/Q2z130/6O1Eu3do1YIK95JXJ06IE9YEvuYY3AzyNgp
a5tp6+QY85TPJnnMHu/r1S5K9731axbEFHFsLSc+sz4yGvPCkXKL8OSdflHy3pgaZ9zGn77BpSBa
7JbwqQf3SDltBGE0YPDJzMrIN0nWm/kBgT6DgYko71Vm9BUPf6kd1RAYKAt8yBA34ADmhpQMP//N
+hGvBKza9aDF3LrybC4P2GhAB0iySNqV4KnD++9faXEOzo/vZanPfuRLv+FEh2z+hHmnmz5BL3gJ
btModQHJ/vnvbl3WgziOEzyVcsoBOnZB9yfKLhCG/lyJMZdJVXXmI1R8is6JK/Zeat8dlTUkyg11
q2kb0J/X21WPhRMm+XtL4inRYG46KxjeetomnObve5TJlIL967KxU5waJ+wG17BOvK+Q4t8McdIx
FoYRLM8aNripauqDzUonwbacNhiW1BG2BOF638A9ETxzD70aZo2++MyQBmh+9RIb3gDB827cad1X
jojd4rA2A4gb9BThb9f8ZSK4m9t80OswbdVaO/JjQMog3NFmSXwZNQdSuk5PJ+Uf5pP9vKctl2er
QKvyA4lQltsIWBw4DuqaOPeOif+CSCtoqACDHtma8/kgXCcFO2Z3pNldfetK9/9Qd3VYLqeq+kJH
XWjcKf0arrWoEIsI1Tm5belPLBngttcghY3VDDMxLpWeaD+tcXTGl8wTc5ua2S2Z3o2yzHtfI7jw
6crMe80A8fbCyTWJjJUYpuY5JUUG6c2ez9vjZFstGN33P0Z9hu7gHTp3PBO9LWhz1ODS6hPi889O
dE7rAG3gQvk3tCGJOr6F2yvHzR9zaZp4SwoA1lc+UGC4aS89S5ui3GAY4/fbnb0cXchMkMU9+c3o
be4x+i29VAjqT/lJ641Ha42U7Dp16XFNjSFlp8n+AmRJaZ3Ghb7Pam8nRckm+7Wg6qVk4Uow8B+i
FjGFDEV8Gg0DlMKQa29q3ubZf15xZIxQjctH2FIwiRzG/vWHY+Q1CJBIvB87lh8fHgi6A4hToN+G
YB0RioCKLO2VYCUtKpjWvInadF1SEWE+IvCS7a4UK1UeZRC42/YfuKdCgTfJlSCaAl6Fjsrl+DWH
ut+ChOgvFqFyk/3BlC0yrDeynaBfbU5mYhGXOg/IjPjtURUpYGRCsJzd3ol8bvNGogUqXHyZAcsd
OOLQjikXTI9d49hCQQpi2dCz5bflqnOvp3VNeHc9xcdEjF+bpg3sBUrrzcnDIpyAi9b0lhxB5cqY
zgnTcHXGmdG3yjUS4PmGN62awRXXDbnYTBC+q0OldyJf1PyPMShYHchiZcLwMYBjNjNnVQ1cTFvA
2OLgbOSFkR5M3W/N6AH+5AmhQigw1A4kuSw/S/sLf9wPAEBMVmhGXF4Uj5H+Tmm3VmVZ6jMTREl4
uUZxDPKeaWOqj2jhCcE8Jk+PzObhqtX5adDm+pLlYcIYlbbUHjByI7y3B/EKXp/klRJ1opbQ51c3
7V51d87ZjTyWFai1YHdYFXs26Wotf8xGJn24onvLy1nmNqOG0L85TFQt1Aq7dDtRIf6hoEnYH5d2
ctZo1zbKf4leRQ8w00SFjeJqj8XwIDrKw9Q6+tZt4g9Jw3/WPscCv/3mdkMGZ2F3vHFL3MXNsAjI
BKwHgG8umIEcAFPV8wwpeO7B/seNfVpTjoGzZgYHQN7OABH3XAhXQgdvkX5yKQpUcWwLyO4JtXQ5
Kmcg4+rA6IAyyIWnMRdL3/r1G17rAlMcVj4EeM8lNCo6cJPbnp8DgrG2L0jySX5P05w4jiN4tvaC
URR+VDxb2GTqJURALaoJ/X0YZGsuA8R+10YGE7asaYRNFnTcurf+sV82uzoJtqSv9WkEH7EF2L+k
z4DjlzyNW/QLKfoB0bphmDsKk7Pa9C9L69viSDZ/ZWxpN0iyvX89Yls6UvP1kflrGlf1ndUxagl6
8OzgyH2yGBu8gDRTPJKm7cMJLa+labS1QhEXY8J51XsKCN9aznWwkSdWU9G+/ZogSPZj3vxYa5rc
KG9XmhzW8F0ksDlEuEH3Gy68O3xHyFFR1PUTV4sbhFB4RzsKEZkXemb47ua4P+tfDH87HvqmtGid
7bA+Jg+bnGADhdKNOE7Pg9Xiz4ZF5QToIv0eL7QDikiCerAQedJi55/E58KuJKgcrgtDvYwIovfI
pamUkQjsqw8uA/BqE3TXngP/UHaUSnRI7PlXcT2xqJ1bn3g8jb5ZWHTSwXQpZArZSXR+DThH4vKH
Tae2U1S/8uDbiLebjvHj9Cc8CoAELjF22ORSFV1LwCvy4UOjxM4OavaL840rl3/WlXjEJfzWkKSz
IWBkzT+lGa6rBumpFcJn5OIFdWS8EaLdMXNNMWgm100VKgutf4UoFoTvzWY8ZJITi5YQBJ+AgV03
YIvGMEmkbvqaMUw2wJuvYDj18hpVFbbC3GZ121a/h7h5uO+lTHcXYMgt+9wpm1xjAagrMIoCJK+O
vd/V/gtgPQ5cBAAOqSl6DSmNlUgx78257BDCwo8YsL1bkR9l58JIlDpL3/Yt0FnHk5Ca4cuocLw+
A4XJ/1cG/UiWqP/eggWRrJWV5JFvn7gafp9dXG4M30klnPdxuaFEYjhd5hvkeH4T62qkD8fqQ45q
g3rPAshlS7zCEzAGl6VBmhsx+CDRJ4X9bfhVZM9dR8e6F54SXRa46y9H5zLXWLjzw6jxuYmKQBu4
C6bzhXl3v3FEkf3E8aeVX0CCr6ZSuq20Oc33V3IjyretCUuO1lrQDKMhGIGXFhLUFnvAY64Dgaih
xacl1WXvHAaLIv4zzWUQRUEJ3D+f4ZDD6eo/AW6jLDN+6NmMg+r+kAhjqa0LFZZdArl8XSs6+JYR
BDqOaYTjnRG7GyAEwcaBpYE9vRcwqaAVMMFgGdHXQR3sOghPSEnjbkrPFGDjtfjecUeZytmo5wPt
8b4uG+q3z6X/jfOjoXOXroA+WEAbLpsE3P7HNFtnxQxLG61VCzmIvIq/35syJVw5dOq09KhJKcqX
arDPUwbcunFWIxeTwHneJqitGemTvMYvyveVe2OWeDYAQ70Rc0IViPY2JDvd251VbPVGBnosHSPB
MqiSirVF41p6oRuoIjSdml13mbBng1s0oqteXr8sStjJ3Mn13L9S0FqfF6pOCuU1M47N8wDyULmP
65BwzKLSonO4RGjIfEAb8+wdSkDSEK4JAoxdILisA/SU+li4d0wX4Z3crtyP7JvJZN2bJbiXpanu
QRouRqyrSLZXGXUXluZlI8mQKDfhj6b/spXQDpXtgkYPLNLKpqJd91hs70Z1p9vW2d7+NBWF2XOr
oUPLRZ5x5DL/E9NXHiJivJ0TW5aq+n30GmIq6GMwob0OJquSEp99HoRTeuxdZpRHxj28PV/tgKav
mESv8COdwaCyyUCImHfEVNYJT99WorWIVGa+65XKTT2XsDqTVim+PirsIA7qyhoqLcSknYAKGg+g
Z4wgnXFLsZ3vBZPfr16X+gDFANDCU8GICkKYgM6WkwYLoDwXrNWtevPrpa8RI0v78gulSvnVSMwV
9YT7mDfGiSsV0eKJsZAcK+LX+IhkrpqFb8uRPBGsPqEonim9yBgeADrBGiFB3foJdorNOjt9RFC5
J5qvpoyQfdmkCE35gEyczFSiKtz7IazrhKTg5vZCy6B9bV9irdO7PUkD6sdFwRNR4+k50WM/J8ex
oVRMvBkM1sk6lsjQ2hIjevu2AYs4vQsr6Gke3Y95UommSftOz4WiSgRaBoGb6lmypoBo9XPTmaVt
W/kO8muXrRVigtw+kXt5r6d3YXOovEFQUjZjnig6YLaXhxjqeLVe1b0wcInJDiGUUVY0tReA5UYV
v4dF7Xnrj1W37uLf4GXmpahWPJ2wyDMIS8TcWA6ze09HhsXttms+O48vOdMoXMHvwwH6GuwzawvA
tahMlQ5ZKflqGPALhg3n7AmdBf7QFstEBBAeUXDNavEEEuIKePKv67/7qNh1ujBB5HOo20Fwkqsd
brEqtO6Esj/yfE+kFTj7EM99mCJjTLDWIlB9EjqK/RxXTv2mOEdgtyyCUxsvw0sh+mPd3+Zpcdoh
7ZM+Ch8ha8OOv4VyA2QWgz59K4NhUEo6hO0u20ZRk2gN4cPPgGGt83C1yisqIbIZKlWxkgXFqwxU
+VVRH7+wwWcwDeE3jEmcrV0pNi9+co7Kz69uU/Ykl8/xGWZadXDFtjmEIzwR5evCDQ4/FSk1JrNW
m2DPwQyyy8aV+M32tvvtnJtH/Ld1jFS+jRlwv6vehcbbudoGI4pSBzavII+QPPniIvFl7oeoQEBr
58I8ujhtGXFuASIH2iKMF/lhKxuvBqtksK8nLh0zp6L2PINeRcHjWA6pDXSfuSxCP3cxc2PxCrn5
m5ufk/xhRlXM3a//XKLrSxsmi1rcFs2jbmL72yG/0bPXDYICR0vSyuRMGwjTOgs9w7cg5shIg5ux
v4PvU1DuFkeGL1fdhOyh+jT1t635nkkhPd2uPArFacqe8zxXGZ0Yw2ujDkhcS/9LIbCtbhAjF8Ua
c4y4/97EaO5UGGEXarZ5CMYxkHtUTl+JImkZdFQrVOAnsfOAIFBl+6oDyRsBrFL+12M2vF/+Kwrs
tneVmx8iALqYgmkGi/PDPXfDgaQgsR35CUUArsKsw0DNX7dOqmiGDJwjZ+S6gycBiGvrmM74YrWo
Y7lOgFf1fwN4FBor0x3H/gqyBwAqM2xNue0xp2PjGMZmhSWRvVJ2qkk24oyz1LThWksE5myKAIoE
2u7iPoHY5Z7SeCo9GvfzMHSfuWJfj8hbnAPquoa7Xxi/sYPgRdKRUnTPizT9KSWDXAsi6CMz/zg6
zUNuIi/Wo/VPFCu4oAc+BB7l5ElSNrhoR7QIcumLKOzurOpPDkGajSksdJeHW5cqzJIcYcIjiAvi
BsodgKIlwRjSxjxSBvEdb69UrBvIZ91OFCMMG/1M3PbKgNQ2ivfxttyd6VEP2viLyK9C5VR4l1Aw
2WC3eFu0CZ7R8UPvnb4egdVON5wH1Q/QmqkGO/H//cxfQBTsOuxPrq7PBX951/kcQrT8c015FVTq
w/9YBQ+EqjVfQqU0hgS1II+acrsFtPSXK3EqPF4HlzwV1mjQurXtvujjrjLmMIvgJTaRqpaMrPAr
EKdI6LXmFd36KHBzQhAgMderHWgCuq+NzLedAW/k/pop2uMMNBrTWy3cTXbdPctuTX46IirU+m55
hXVYeLhmFGPE8g/Xo5xYlMe3y1X/PUOYu9KQkLngUOa/LQZfEOcuDqOB2t5jkxasuSCkx7pP+7qc
ytdFqE+AY0G951lTYHY+aVZKBZWKdZohQ2qAQ83BW/82NPjqSaa+ix6bJbplPXVU/kiGv9WYSzf2
yhySCBJaWO1dMFYRCk1oVSA96cY9HHx4zKbLLUx289pG0I2/+wv1C3Xmha0aHVqn3jO/EcM6a0IW
2dfg/tEFyXLCN6iAUv3XMIxKmWrln55NqdtNQtl1v9JF/c3YReN/VqODCUGUON+AnLDwJmTmfp2N
R4Gf09qebp4tR7DYp1F20B2LZ9z+QkC6wB0l26wGdk5Ie27dszREYBcqiV3VHCKUJ8D84ecJTIHs
6tKKFeSFZSUr23x+MZjmdoqbaht8oS37xTLSwXgeHkyTIi2iEoZijGPLKMsvDUJZnkDjrK/wOdag
fIp3yIXtwqyzCDtYokMfgxMdk3t2hLn10auUjwdtI/udUvug9G49eLLL+li3W6HgJ94bbc+fmDRq
1VIOLpBo0RJtvipzWXjupGCpMMXLxdi3ZTpPXndwcNLI97hdwBoCKkxs2UsRih9n/62W3gm2BTmm
W9YClFVEyVhCz/H/wUCMAODOnDHeqJ5XTz+DHSkuQP4Ph7LsW8tuqy66qSrlRB2HYzHLNHjlaRoc
ZiFEIjVJ8UOQ61WxKl3ypLymgg22FeYxXrZ1olL/SkK9v7qjzEH70hDArN6OjJmUlFT0nT1xKxwI
/ETQ3OJDMAZscD1h4cawZgDZuRoleZneEYftwAB5WHW2YAgnojWZrcjrzqBXx+aWqDKog9qQSUb7
3LwNdUY/rivcCwlYiLFaMtdgyNIMm3yML26dxaE+fdf7xnCKKES2k3rBzkXj2Wqa2/BmWUqKoTMn
pENpXfRK0geMW66bEoCvb4O2YixopEMxM39lv/gArSv2dLYFXY1AuJAMYtjkO5DKr0MT5lRZlAPs
Iph3GRmct0OZDFD6dW186OgoT4gqVcUsBsSOAY3MP1AgJMPa+lEaTA7x3n4xU185lVBAmhKWjFJb
ZZ9Nux4KkZYDCj8pUZLYi0bG4y/2NKPVspST5KhWX6gnvvXpCMC2u2ZtJ0z3YhBcrx59D4dNirBG
ZzHQCetbPmclgCRFWoUh0jKVb2tpvOHbFltW24o8MtI/a2iFpmKKEIzZIeNm8u06+KKfjRYWZu5A
tZz5pyvDl6XtpZhMeJTjvpFl98ezln46BftmDPLBYczJhl2eREHeSav4t/S/WZ8asdQSIJquTLKz
SvAcIZLvGQbnWG6JDKV/ZynVjQNW4DKuQjthB/NCGZW4/lDr1QBWcwyJuJYJ+PtmAnYZAwb/GoVU
wRqUuVwRLA11/+FOUn2PU5jKSOZ/KTOREAHiQlmTvPLsnyPig3EGT28D0gKVvA3UoOpy9/aCV3km
9f5PPh4y3a/h4ymH/RCkEP+D0ygByQkjr2RIviNNOKbhUEVcf+wY1n3coSqGe/6/NkbgenjoDqLz
SWKyXVF+vyoO4d5nknRF3vfBGm3/sBevBv/CC9AKxBCRY6zz8jxLhKrrwn5jK7hkgP106qMkBwTS
RS0kAkuddIU5TVa90AY2xrnloh9+ISdWlUjQ4g28oyIvEAt289Qco/zTQZ6TxvEeG0TEVN8J/KqV
VcYHSp8JNUW8+FQ3EX0A6V11744WN0UOD7WZjiOhGzBBij7hTFYqcNHCCQbgNjjQzPeM/zp8DcUb
hGCjLTOx7mZasDCNIiTN07moBG8zsX+dLSCUi8N2EySJgh6DQROO0w/xWnrUcJst6lA1Wo3zpYMG
eHXWaxQvECSDNaUCcHwBKGCHd403Fl3c+VsIkk9lODhMCZhZPqGHXSf7e52WFmlZKYGcMwxiM8ir
uASd5x5cYK4JruN9H0lMdkjSFNbNFUTg11wOdu/J4lq0kH8yo7TRpULjTuUHoVuuff/dG1zg+4el
UwLCWp94XOvIG+XayBE8uG5n00pN486qVDM3JNlk93Wq3Cs6xeQfV6Y1/Yd0ILz7aoVXJMMUaP7c
HGDzCZuXwhDZmsLAdbfiPaHjzfAkZI4Zb2LI2jX0i7cZu3/ROcfoh9ozXG1roaptrIdJEpoliZ0d
StgJkT0wyIvzvXLkH6rPd5ln8Gd6e9/2prEMVO5Ay9Djrpbj33bmxWhV2RdM60gQyunq4kg17H0V
OnGWAX6bbJiF4oaJqVMyRXOPTf5+W0mOv9gHS9MlurvyWRNgEZ8IgmLEWGCTMbuIkvs/BK2Wd7oM
ad9pOuHWjo/ishxgFriRSlAicklmUAdI8IDmDQEGsBVVcsg0KWiFP+jHts7tfu0+SN2WgX009Srg
whaQpIdGTwd7cDC8VXXp21SlXSJx//TrlOUVgWO1m+9wVGBwiE7/X5i3RSK0LnZKgWfvYu3dbR+o
zwP5DEgtQ/hNYpczo4v37md6r6h9bn8LpTeIJeB1OWinxdfffFoWbz5iJ6I/W0i+VOQqyeS0WUyF
nL+KX067evLfBw9vCTFM4DnwfGYb76rPXiyCTKhbNeQ2wX+ZWftPThhYdL3xUSu2Mp4JZg1j5K1v
5+9etuvjkS71w6p1irUbCU8j/tzK5dLU1yOWdIXq/BMP+PM+iDAdKvTFAfAAuousf2xBHkr4rJOC
rWrJdTQOR5vd2t0Cm3QErtYGfnHMxfcQVLGSjuQgW1YSOoJOqGdcmQP5seOljMEFboGhcClZjX0P
CNpJvmv+s9+V5EU7JvLzjaKzRNh9p0pWiwd0a0MnOXydgZ4bd1HLNqDo5A4U4xEI4KA7NXLve17p
e1ebqOkCFLOZDwp1hgBE9Dg4OsXhLj9rUIRg/ZMm2evnWdogNaLrGDHCzh/dDhtwNgWksXH93+St
ZNpzRETYrELOH/jWPq6G1wNdRPGUqUPY2QQ6rS6jGJwPM/k4PCPsEAYRPcDM9oTYNY1GRhePJ3We
jcLpsbWtOHYI1tq73wWpMIkOai3iyO3TLcWQU8uK+06VBfWisPVI3JOn8gYR/48UjjszTkgO3DzJ
J9GObyixrID2L+FzgyDaO/sc6T2kl3TCoz9N+euf0z4gxN4aChvze9OShH1x6ZYCWeMFlFRrrUls
L2P3d68HxyhMjAKDeM7+5fgqSeGpd1mlBvjwX0ITVtA2Y84Hvsudmoqz3Paz06zeiKKYuc/B7JWI
U7+Xbz4OdCr8Wk5xhXU1tLGxrQMVp72mm3aBmB/cu6ZT4HmY3dLk/QbwlGrksiioAZEk+bJfQ8cp
Q2CdmAgMaSrVYI3VHBnBkjWmLua0jDOEySpTF2ZbK2lTEefnSixUcuvgS97NHcZwL7CJkBwT2Qwp
MZNZQyuteJL3t25WBPzRW7Kq0Ea2OmCwhQLyA8c5GQ5uwHuMPcy504YxSmHNBoB+DaoYNPb9A093
9AIrwtYIvbTmW+Mr/rlZGdN+yrH4+C6ve775b6M9/FTj+yjw2MSC4vA873oXsdtqPeUAt9VFa02W
URT3DZZXlsKwi8lAj8KYD98YJXpQtAGVjcERlm9nmFW1SMmQ6NVM/MRLZSfDAK124XIIXHhnO3b8
8Ya+wTD2O6ryHCHDNY9/zRxAaRQSEFI8Je2Uv3bJYrisGP4JUQLr/ULKbjZxoCKFitMlwaaCNJlq
UhPq0T1ua9g0l5/9UepfWHZcUfcf5tgwKDC9DqR1VQ/Ulf0CSqbwkBnJToFoonlcTf0CEYg89zFk
EpMHzGvgZMDBMTIcI4ybNKm+72YRsIF5Z7r6nnabtKffS+/Yh0ubXAfPQlzV/5z6539YGaTC66Kp
ulxbOWiQ3KCdzesNB7PllWW3kDQVBaXPnBmMFuvfVwIiWnsNoMsUYGQyYXWwi/TzLS9Qec9tqv2y
fJbdaXfrCtfb8hF+fJbF1c3b5sineRLy5khwy0Y9MglWXG2tF4e5Bul707wAFQba3rK3Os6QnCdc
yOSiKHaulHhbtmFyUlR8U341OeVgwmuU4H1xdCOkGdYS1Sm2CWgFga9u8SqhpPqeD0VTB2OZRrV9
w4R6smOw0MpKywz40A4c86UD3U4qdmvLSG1W2i51EZ3ekwSkRwZwsLLvzoK0ch+UrbcIHPVSn1TL
w87DXVTTURGNgMZZ/N8qzSQC/r6XGrOh97cUv0G97WgOjbngEAnG/rDLT+x40PTQt9WMEO/FOVb3
6XFD8vBLeAS1NcPFFbtTawVBb8+AcJwNvNhHNmlu7TyHV56ADQ0RQK/KcCunTBZeLaYo3CGFvVAE
Uy/7ODTGzS++VXETX9SnkEYSX2oSiePSZ5Ypq8+2QQARfhR2MoIYoZVoA39q0+oZCPlUvw9UgivP
1CjKAN2Ac0CQIVFyUkmmxD/I9zJ8x2b35simR/bLALFesZGzyVN3FKuyVvM4i8vYL541+93HxzUf
5LMZ6buYdFWZn2IilwP5jcfc2Dq8x1tWPznNuJ4TuCWeMYFF4CilSytr6Lmf7q3qKmMSaxh3sTPl
y1K+2xdInr+5kPl6PfuN/MD+Tc5YBFpo49b3I2NQGfyGZu0/bEJgOAcXsdD4RHrIQvPxN75FQl2t
3WyB//kGYxRevqLB2huFXocftE1WhBXwKgecxuCXiYETjREetbkAFXvyPN4p+wOrfPBiQWp11tZx
DUTML3ohAn04o7tluA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister is
  signal d1_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => d1_net(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11\
     port map (
      i(15 downto 0) => d1_net(15 downto 0),
      o(15 downto 0) => d2_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12\
     port map (
      i(15 downto 0) => d2_net(15 downto 0),
      o(15 downto 0) => d3_net(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13\
     port map (
      i(15 downto 0) => d3_net(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\ : entity is "ip_scope_xlAsynRegister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\ is
  signal d1_net : STD_LOGIC;
  signal d2_net : STD_LOGIC;
  signal d3_net : STD_LOGIC;
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net => d1_net,
      r5_enable(0) => r5_enable(0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41\
     port map (
      d1_net => d1_net,
      d2_net => d2_net,
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42\
     port map (
      d2_net => d2_net,
      d3_net => d3_net,
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43\
     port map (
      d3_net => d3_net,
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\ is
  port (
    o : out STD_LOGIC_VECTOR ( 10 downto 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\ : entity is "ip_scope_xlAsynRegister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\ is
  signal d1_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 10 downto 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => d1_net(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34\
     port map (
      i(10 downto 0) => d1_net(10 downto 0),
      o(10 downto 0) => d2_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35\
     port map (
      i(10 downto 0) => d2_net(10 downto 0),
      o(10 downto 0) => d3_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36\
     port map (
      i(10 downto 0) => d3_net(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\ is
  port (
    q : out STD_LOGIC_VECTOR ( 0 to 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\ : entity is "ip_scope_xlAsynRegister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\ is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      r7_clear(0) => r7_clear(0)
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27\
     port map (
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28\
     port map (
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29\
     port map (
      d3_net(0) => d3_net(0),
      q(0) => q(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\ is
  port (
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    full : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\ : entity is "ip_scope_xlAsynRegister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\ is
  signal d1_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d2_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal d3_net : STD_LOGIC_VECTOR ( 0 to 0 );
begin
synth_reg_inst_0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18\
     port map (
      d1_net(0) => d1_net(0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
synth_reg_inst_1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d1_net(0) => d1_net(0),
      d2_net(0) => d2_net(0)
    );
synth_reg_inst_2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d2_net(0) => d2_net(0),
      d3_net(0) => d3_net(0)
    );
synth_reg_inst_3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      d3_net(0) => d3_net(0),
      r8_full(0) => r8_full(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\ is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_array[0].fde_used.u2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\ : entity is "ip_scope_xlconvert";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\ is
begin
\latency_test.reg\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2\(0) => \reg_array[0].fde_used.u2\(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister is
  port (
    register_q_net : out STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0\
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\ is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \fd_prim_array[13].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8\
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0) => \fd_prim_array[13].bit_is_0.fdre_comp\(2 downto 0),
      o(15 downto 0) => o(15 downto 0),
      \rel_39_16_carry__0\(13 downto 0) => \rel_39_16_carry__0\(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\ is
  port (
    o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => o(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\ is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \fd_prim_array[15].bit_is_0.fdre_comp_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \rel_39_16_carry__0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1\
     port map (
      S(3 downto 0) => S(3 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp\(3 downto 0) => \fd_prim_array[15].bit_is_0.fdre_comp\(3 downto 0),
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(0) => \fd_prim_array[15].bit_is_0.fdre_comp_0\(0),
      i(15 downto 0) => i(15 downto 0),
      o(13 downto 0) => o(13 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => \rel_39_16_carry__0\(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\ is
  port (
    register4_q_net : out STD_LOGIC_VECTOR ( 0 to 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\ : entity is "ip_scope_xlregister";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\ is
begin
synth_reg_inst: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
begin
\ramloop[0].ram.r\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(17 downto 0) => dina(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
\ramloop[1].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(13 downto 0) => dina(31 downto 18),
      douta(13 downto 0) => douta(31 downto 18),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\ : entity is "blk_mem_gen_generic_cstr";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\ is
begin
\ramloop[0].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(17 downto 0) => dina(17 downto 0),
      dinb(17 downto 0) => dinb(17 downto 0),
      douta(17 downto 0) => douta(17 downto 0),
      doutb(17 downto 0) => doutb(17 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
\ramloop[1].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(13 downto 0) => dina(31 downto 18),
      dinb(13 downto 0) => dinb(31 downto 18),
      douta(13 downto 0) => douta(31 downto 18),
      doutb(13 downto 0) => doutb(31 downto 18),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
INaBf8vh5mCmDzf2yp77pxZAxQdyEQiT/vG2dEgvrFjseUnGc6ldwH4JvdnpZSpdf/ihioPyMNjl
u6ooyzv5TA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S5XIZZtuFR/MZffuhwdnvE3H9oRWM4uXoaGZTa/Dyk62O+Wa0v41pjmZELCiR7uodZPFQfykZ6K9
2ZDMu8dB3afQRMs5lnd/53M1b9ke+MNEeZ/wzjUcsJghubnEAwzdWeW/0tlqST1WD9B/KCxYqwH5
Gj6IZTTFHAXcaVhnCT8=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
CM6IcdzP0PbD6yMSqylmi4JE2qpmxiNeI+prjGwJiD8e3Xsynu3PbGKJAOpOxtR1hT/3mpBcx1Rz
Fkz0QBh4wtE8fiziv1i+xi8T6cqC8ClamjrpZ//sn6dh7NvwSYik14MlwVuei4DZoZJZF63aoPUn
RXkQ13wtK+MkYKBcPVSZMFZmaCU6jMMBYclXzvRG1JqqZoa7mWFTeFZePUTXG7Wo12QaZ8GUi0AV
UIshoN25yn5e2Xr3FyuEtm5AvsZb+iLsgLeHBtKBnsVaHQphicgqwgwv6MQQF6ZNBgU/aACfibDS
3+n/mMMm8k1cj2bW6VCi7a+c8LmCf81NlJuLww==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ehl0CusS7+JNGq6HfhyaBMy68nccIdIGqixoEztEZfkCpXuUYsdqw6G9MIJdWdu0Ck2acV7K6IVg
rzb8/bNaDDVWp48kupToegTkOdwDkCejEqppido4BkJ+iEkjPniz+aJHlOlOwmauETy2hCMuuC57
oWDprzGWlsqbCjqzKrXmPYm6fNdcOa2DiOYstQaMFNbPU2ccrbLJAiYMHNDqtPNqWxKBsD67kiGf
2eOneDOmdmy7YkNsL+cx8MJc3BVUsYBrpAEsGyFMkmX8a8nYz8R/wlFQFGQAd/t5XrfxFNI58mj1
AHXbcAMhGKVq9YdKeU/vSXY/NwMqp12xJ1nUaw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
h/qRAwiPuqY/Zg/QWqbaYm8xWTi9SshYuPzyL0UME9ZDDF+C2CyGAugh9HzMdD0kZmT94TKmBgLR
dKP28nlE8VCCU5rvbjKxfn/wNtNKHCvZ1hns8CF7+pGuelhxGvXNmYKFw5co8+4grYFaDXeoZZR6
S5sjvhqtSVD3+qq4vYWRjT2Y/yes7L9dRsLq2D3iZ4xjgVHuIbOQLT/EUKW+9iYudT9Uy6YTwB+5
mSb0QK3YfZdGwZyXB4S3mdF9vNQHdW/rnACq3yngF+lprNkh3ooQKdGqtxtz8KSQxNZOAFE+koOw
h00o7AKpvDAp3uNguLvnNJH3rugOhh95b8Jatw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TsA04vIYHDZne2CBj5bWCBFH4MtNoFDCn/3DNEi0BwutuUf+X+lD9kAO3kl352WHjQbF79Ssm+PT
fCYpODgWdxSVbzaHFpITxCQ4HcIJhUeW5PC5tw09Tand68D6eg84qRguH+llbb5jdGJkJeTCf+Mx
pupkkLiDvNyTYWe+nqw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rx9hgQkvaJJTJJcTjGFW1DrrWiT+xanrcMvFn0Z3KRXlZvf+SE7IQgGCiP7ZDA6T5z1Zv5nzS4h5
cVi+CvwC9UMZRWmLDAjzASJ2nx1g9BjbYe2vHAUmyurIiR6LSigTeM/9TlMv+fFwJbqwuH6FJ3/z
Vl4tIMk4NrqkMn/riOG87SjhesepM6kcQOBgDGzLTG14z3qeZG8OPzxgApfyubmX4qdD1oTgGm2u
Q4mQfFxEye6Jqkn4Rzjhifs/ieNYomHlK7R2/72QJj5j0WyYBIhvO+09izz299Z54ZP2ZXaRMfDT
lU4lQNqQU14PX9Yk9p7sy2PnK4vTwwF0CFIgSQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MBN/SJWsOqa01ZDnJ7FAyiKmDwFm2zemvV2ywmNBgPBIqQpcoeRCQghBzUdDUlSrx/0+80ePoHUH
b5mfvx5hHqDUvfnSgclsuOTzGB8b4oxxHiYCkBiszKD+fIwG2WD5Va0u7J5kLJzWHL+ocBp9mh+T
OrOOXTcVyDAp9w8lIh9v+8hK7JggRScSXjK9XLrnoUqiSACiUkkzQSRBX5Ws+JC075dnZFRod0vX
GlorwmUyo6H2nagsJ3K5PwyI1QjMHF1BrOf9hFvw7/Fa+vQGNv/EzPQgIqjqEbQqdA+xXY65GFrp
ok8gblFicXdAFcuFjxrkHxQKbK9/DutMvruWPA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TUA5dEuY9l30bHBeeuDUaDlC/Y6WKakJMWOPxgVRpc0nPpG9i8sM+5eHqZ6dZRzjQ0mbfC+trUE0
3yrALuIMHXMylKJYMZ1j79zT0sVFvjSYI08oriZMvihBnnbbZ8fb9gc0APtKJ4JXs9h8EUv4LbwG
iS1WtK8C/BnQOcYAVoDACYKdxnlOR9/lV0y8FcJQGFLUmW4DgNw4gmgG+I8qF0lvAkyx8Yn/uh0A
IziMoQ0bm/I91zI/TlB1tgXLl0qnJ68NGjLLvpGXsE5m0AOe+tJmmbY5OyPE8o88lDZGWPrbKiA/
yFwo+6UO3vG923uoQGVp8rsMaely/ZBk+QYzSQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 880)
`protect data_block
ul6ENJPmVHPHLVihorGm/Sxl1OzOueqfHKpbzzz6fs7SXeWa1plChw60+bf3/FjzbeL649aaMwdz
pwZURKy/2jFr9zYWIpOatyBozOjKdXpAX61FI3tcnVBT7Q/XojIH4H8T+t3XxJ6LlzvpECHdRN1o
b502VA+CiJr8qsqoUaVW/M06tTFpb/d5Pxl/iWjpLYSszp7+5LstTwL9g5SB6F3MSIveoAQQR+je
0lAXSQePA6+mW8T5DcsI2q4NXYdiiM1pXLrynIXcrwtRIBrps/vB0vIXRP9TzqHW0uGYNHyZI3IY
Vv8GFAjnTDBvXj3zhF7Ijd+oxP9uqA6A7p6l/miz6JYvOtjsRPFhGFT6XvhGePB2S0DzkwCAd4St
vGmLBCS8Ts6b1VTF7/00+FK4DIADUWliKobRd5oxTWQZ7KfDwCeN+M/rfCXFEC+3MvICtsd4Hd0G
qJv4Hg4/5ewQ0NBl5AjoAJxu3yRdFb8FM/vTYDEIatDgUlLZwrzeZo2DwwvQUCc+0YNzPUNC6h2L
uZerHrYJ6/WRHvBJcfzqjXXshEsqKXWC3k+lRC9QVdN+8ETxu0rbUmKE2YbY9pqJ039atk6CxaF9
6qcNdze9pl19VmrhWI+0p/0tSN8cNpxTJp4JBX2cgGLfx4OYQycb2ZD1ZUss/H47jVDLYm4srdwS
ONBISyv22wOteVAimxqkyPVPKeRqCqGFn5P9oNvrBpw50vKrDeBMubW4iutKlMNu1McZfAVwbV+f
TimL9XR0jCcHoXzG1DQuwLhvdoL0tzNPu7isUBcpaj+AfySCGFmBIcrEVhQqRvSpB2fPpA2yFfeS
9bO9o4P9eoimysYtbG6mI2fW5nvU4NiI9XlL5/+A/mMIOOzLjkOyNkxty23gYahQarkM1Z/eJ9sO
a3zXsESGRqa7dEmdiVuOKYDIgEHz7pbmCLO+L8RgAsU22Z0fWXAIqVx5pjxrBxk1B3SVA8qNLMdZ
jjG1XcniT3499Rm+Y4sTP76z+aPeWU0vBkp0muYYRtSg2VUT9B1rdpjUnUay9GlAkXoZ/yz404Qd
E8qZDQQ3+VFF16QPn5WHo+xwYjgEf01dBV/PXllaC8wtmok4WnuomEIEezV0CF7Q6b8lDZG3Sci8
fbqGmM9x4Ep8cyYnZOxnq3G/ByD2/s/QTQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
begin
\valid.cstr\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\ : entity is "blk_mem_gen_top";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\ is
begin
\valid.cstr\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
INaBf8vh5mCmDzf2yp77pxZAxQdyEQiT/vG2dEgvrFjseUnGc6ldwH4JvdnpZSpdf/ihioPyMNjl
u6ooyzv5TA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S5XIZZtuFR/MZffuhwdnvE3H9oRWM4uXoaGZTa/Dyk62O+Wa0v41pjmZELCiR7uodZPFQfykZ6K9
2ZDMu8dB3afQRMs5lnd/53M1b9ke+MNEeZ/wzjUcsJghubnEAwzdWeW/0tlqST1WD9B/KCxYqwH5
Gj6IZTTFHAXcaVhnCT8=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
CM6IcdzP0PbD6yMSqylmi4JE2qpmxiNeI+prjGwJiD8e3Xsynu3PbGKJAOpOxtR1hT/3mpBcx1Rz
Fkz0QBh4wtE8fiziv1i+xi8T6cqC8ClamjrpZ//sn6dh7NvwSYik14MlwVuei4DZoZJZF63aoPUn
RXkQ13wtK+MkYKBcPVSZMFZmaCU6jMMBYclXzvRG1JqqZoa7mWFTeFZePUTXG7Wo12QaZ8GUi0AV
UIshoN25yn5e2Xr3FyuEtm5AvsZb+iLsgLeHBtKBnsVaHQphicgqwgwv6MQQF6ZNBgU/aACfibDS
3+n/mMMm8k1cj2bW6VCi7a+c8LmCf81NlJuLww==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ehl0CusS7+JNGq6HfhyaBMy68nccIdIGqixoEztEZfkCpXuUYsdqw6G9MIJdWdu0Ck2acV7K6IVg
rzb8/bNaDDVWp48kupToegTkOdwDkCejEqppido4BkJ+iEkjPniz+aJHlOlOwmauETy2hCMuuC57
oWDprzGWlsqbCjqzKrXmPYm6fNdcOa2DiOYstQaMFNbPU2ccrbLJAiYMHNDqtPNqWxKBsD67kiGf
2eOneDOmdmy7YkNsL+cx8MJc3BVUsYBrpAEsGyFMkmX8a8nYz8R/wlFQFGQAd/t5XrfxFNI58mj1
AHXbcAMhGKVq9YdKeU/vSXY/NwMqp12xJ1nUaw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
h/qRAwiPuqY/Zg/QWqbaYm8xWTi9SshYuPzyL0UME9ZDDF+C2CyGAugh9HzMdD0kZmT94TKmBgLR
dKP28nlE8VCCU5rvbjKxfn/wNtNKHCvZ1hns8CF7+pGuelhxGvXNmYKFw5co8+4grYFaDXeoZZR6
S5sjvhqtSVD3+qq4vYWRjT2Y/yes7L9dRsLq2D3iZ4xjgVHuIbOQLT/EUKW+9iYudT9Uy6YTwB+5
mSb0QK3YfZdGwZyXB4S3mdF9vNQHdW/rnACq3yngF+lprNkh3ooQKdGqtxtz8KSQxNZOAFE+koOw
h00o7AKpvDAp3uNguLvnNJH3rugOhh95b8Jatw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TsA04vIYHDZne2CBj5bWCBFH4MtNoFDCn/3DNEi0BwutuUf+X+lD9kAO3kl352WHjQbF79Ssm+PT
fCYpODgWdxSVbzaHFpITxCQ4HcIJhUeW5PC5tw09Tand68D6eg84qRguH+llbb5jdGJkJeTCf+Mx
pupkkLiDvNyTYWe+nqw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rx9hgQkvaJJTJJcTjGFW1DrrWiT+xanrcMvFn0Z3KRXlZvf+SE7IQgGCiP7ZDA6T5z1Zv5nzS4h5
cVi+CvwC9UMZRWmLDAjzASJ2nx1g9BjbYe2vHAUmyurIiR6LSigTeM/9TlMv+fFwJbqwuH6FJ3/z
Vl4tIMk4NrqkMn/riOG87SjhesepM6kcQOBgDGzLTG14z3qeZG8OPzxgApfyubmX4qdD1oTgGm2u
Q4mQfFxEye6Jqkn4Rzjhifs/ieNYomHlK7R2/72QJj5j0WyYBIhvO+09izz299Z54ZP2ZXaRMfDT
lU4lQNqQU14PX9Yk9p7sy2PnK4vTwwF0CFIgSQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QQNQ0dLZBlwUzdONTRbGmJipoqZpGTB/xCeHeBcGNrav1QLqoo12NWT6oFrXy8rRGTJ89aTy9B7i
9Y3UUCVy4SRiNoR7LNQjJ2Jn9u5k5z8aCjiF23Bko2L80EJfQ2ooSDJU4l2rQaxIac5WxmWgPH7O
3Ahum41gQAbTP42ayu2HrX+nnc/4Ino69Nd4KwWxTOYFn0a6wyTM9Rm8kZubklHolUrr+8ppjRo+
djMIoOG43lVhjqAZsmWBfNt9Zzy5rP7n55uEEf/8L86MKMTypLRILzN9o9dYIz5N+64mkHuvw7m+
yosSPTTwLPD4wrIFq5DY/xKUBOMm+r2oa3YGGA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
vycpRolaG7c2j+2by2tzhzk1lAgHDjeRNg8jytncyMr6ZtYKZXkeL6es6bIj61gsPN1LCmmsmPho
VmwptuUb+oHOwHEVSDKFu/KHlaPV9HxSFkOmGSK3F8maRwBvsS1ZSDTwviLfAwbe1eMuU08c0VTa
ZDRBEfUY+Uc35N9DLc2mJHUR2EqFD2cWaLG36JN7IlHlYIAqYdUJns/Jz3zcvOiXQS0STuymuZrL
tNUH4rlwLBC1kYVefRtoY88Bu/1Ttlw7WHWDYVa3tv7EyqzYdF5PIg5YJ2F3ZPvJhF8ky5yJo22l
JU1guTk6h99U7T0KnCgM3kkUdJe9vM+2achuBw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 880)
`protect data_block
ul6ENJPmVHPHLVihorGm/Sxl1OzOueqfHKpbzzz6fs7SXeWa1plChw60+bf3/FjzbeL649aaMwdz
pwZURKy/2jFr9zYWIpOatyBozOjKdXpAX61FI3tcnVBT7Q/XojIH4H8T+t3XxJ6LlzvpECHdRN1o
b502VA+CiJr8qsqoUaVW/M06tTFpb/d5Pxl/iWjpLYSszp7+5LstTwL9g5SB6DhhVFIDvGmu0gTe
Or1fpCfXprezA5eX0dbw/fhtUb5GKPGr7E96uqhSkyIwLMNEjJ2S59PwqQiQgCW52Zicbm2pSf6B
nOL685YBKfciFlPIhYIGgQWU/BVfxv8uTUmE8Canz1xmIOPEA447C41S0doqEo6WlmboD8g7REhA
dhKG2KTM8KT7YlavlzNi6EHSauly2lV9iUok9iU6W6cZHGY+RPd096EWgt/7FY8yauXbN3We26Oq
kG+OSZza33YMzObbrSnOceCIDPeWkp/kdUKxh89g3CH0JrjhgfJ5KuNTc3wkY/g/pNjqmG6i/Oym
DpOlEdRoEjXNfx4NiQdOOqdwgIki4+m1HMR5nGW+VCumZZ27gwlbeOeY8d9Mdk4oILbE7bEcAW1V
r2UILfuhAECMyn0XgnccCnALA5XydIOq3fzQib6u/qyo7oJ9A4llkXg4cIF8qsblXEhH2hEk7Ivd
IcaFzrAc/zdOyrAkK2e7KAseSc8hINZYcscTKdg0gqy+7sv/wW14/D0X5InWrNSchXbHUUiTUH+f
eIu1AUJtl+z3tPSkbLvaNWm1v/z/yOOTCRNWK53bVulvWOfLEXXHdWBY5rBNqf4av9MfqJNzYgON
Zm0zOa2kx9iPJoeYZIhY+NToT5+vR6ZW6ajxNh2ThzE4d+APv0X178/UkcX5mKP0wnwpqz2pACVb
zoLWAXhjDv5qMLGCQyUDTHc/ZxJdgyc4WgLgFKH5oD30V4TaHn9j8rdBtm/K0NBY/wquaaGEXPQK
ZlwbNqxypBh92x2sXUmWmN1f4GCxDqytgGJtnxg1l6wFxl66x3D9OyBDFZS/zPcLBrYyjwTni83b
oDeldiKUsiyBgFRn/FR4RHffk+3g9mqxKbnjAOxzS3R1a9LBbesuu9JH9Scyl8AggmV/NEMjmqIB
mFiuJ80EvYwcRLFm0D5Ie6RHGVtrrIGPUw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena : in STD_LOGIC;
    enb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    web : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth__parameterized0\ : entity is "blk_mem_gen_v8_4_2_synth";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth__parameterized0\ is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
ZBY06y5BSEA3vwLtCYy6nxOZv3rYFFgZv5ABjBaqtaItkwdtQfFvZBIMhBOgu0+1i4DhnUz7pdYr
Y88DaxXmyw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Q91nMYZhjxb8KT0ODrW+miquus8bIV0xJDXXyQLu4mbE2ZGK0HYqPk6xE96lKrNSpNViHea0rEyX
J3Qsb1QJLBM/4rnfg8PNzn8acqAN22JgnqyTntYQVpk0fARej5ldkyKbsCPgkFDFJQnDbUHBIcF2
clV1QCjE7A3SvN91cV0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
fpeDNxCbq4trL0iAEhu+gbl4Rix2OTBKp+3DlpwRVRrJB8M79X6xv2dY4g29GTJWY/qcPCM3xauG
RxLbIsN70w9DSrpdJ31jxXSOp/N0b21smrkPYOGR9al1eBkfjYMFWbiVzWEKHK/6z705awwEunRN
qhtuKyDzs9JphrMi08O8ld4FYuGNYbtDOUXkizCIgaOdAfQTq0yCDea9z6uJ5sQUPwqrjRIroSnJ
mW8XvC4+hFTtIH4kcsR/hWe9eHVCVq7yIdgTrHznDz5I4c7+A0ZUoahnR5dHirQC2z7KKzrCldej
93tdxPQksB7VjPElshg8WP1MGrwn+7hvSijdSw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
No6agU7QCIBdcP4teTJDlwXV+g3qBzu8V5gqFUsql+qUP2ZRyYvAPscmGZyPnHh9xvIYYFmXqCE7
RRM/BcEtyrJ9GJvahRcE/doL0n1EHIOASw/MZnFHkf6gtqWvN+SIv29/H/UyUfhuDXqJBGjBGBRs
+/RValRovCLF1SU7AdbCQbWKJbpj9JDmu7gpnhPbkiKkLcd0L7j/KcvlPBvHLG2JvHXct9Oyye9y
FJ190Nne/diMvLsfTBKIzRzQiV/kj3aSYxw4yzuKLbdVZ9eZYqFHwhjBXrVIvIAq9zy3Z0JajEGH
8Eg7Z1uVL2BNbnB2qP4/6a3wYkq6RDa/mFw99g==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Au9tuW8YCiySVmtwoSZ2LqBsVMwu9uzGBs0i03rtA+ohnDzpS7/saWzSdnxtvJsmHKLPTnuG8etw
O+1iKknogGQAhYN8j4DK0/PmelqEJy8N5vwkQ/o6l1cfVFLfqvAMRbZ7lkPzco2SCT7/KjEJHW7i
5gy7tqPxnW7QwYv2vH65EVqe0p2tQ2kCHVUvvPaAZbeDzA1LHleCahBpWEI3g5wztTT869s7a4yn
1IeWyD5NV38NHHcwqubPZ09C1Vm5NLAHW7sEnM3is9mRkFnCh/x4Fb6Ecuu4bJYFhgmNzCCKgYK9
PEdkW2OgY7EzDM7ocQQuoE0+aHQvw9lRdJm00Q==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
n8+Js6UruWrTa5ioc59l4AeAloQ6ZDwzPNPXUOknQWFRecrzd2eOQ2KSf6tv5Oxix315yAoI88kJ
L1R7xZeU1dj4QCJCinzjHZXGEfUurXJVEcq84ofioKIpCyBd7YnxOq469vjhUCYiTJvMARwPVvDY
U+jspt29lk+k5/XFur0=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HvScITgcbiG4YgkXwlLAPuMki7p9oPIAapsMuPCpK/tVnY9llE0MvUk/POKYiMFRuKgzht1jfNyM
pX8Qwv3/+iDiBgwTwibzi053ET+OglbpoF/MDrRErGx8VRvmBKwxnlefbxg6dCEzjNwYuFpDkHVT
YZySWRuz7hA0uzRJwLLkvg9LoVoAsjHpp+GqlpSqfuVaV3IJzpIboKGmFv2qLj7Z3k2aE4HhZfXc
HclRJsWxw/CA2DK86EGTnPC71xJNT7pgY1DSHCglqFwF35L0FfZes57Wpz5Ka6YR9dKPNCocMfXO
DZKOoy0+Zz/G4HOrhtHGxgzfEtHjRq0ZthhxDQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Y6P8mey8Ef9hS5ixjfpRKmT+5Zx0F5lS2a08GaYBAhv1Y2wGLI+vsYfQGolse03gMVZyvsC/W6fO
ZG7Djz9x17OaZA+x8meoWEDIkFm7Fc7Ml03Lu3od5d6U4LkoHUqw+EGW7xqzsLPZCqdBQXyMTTdO
lfVa2CfmB5KxSA3qGsIE4DTwHjM60pKfeeGSdp21WDiTXWOzyFHyoFnYUWy1xUo6smcPYRbZqAaA
zqvYJLDDhr+5WR/aU9HcqnSUWBRQ293p70ucO7LTFDDwI8eUnVSbU/QXhFqt6vEk2osR3ynYmUEB
iY+0ZxpqyKi6z+MmN7azz8zDLXRlQitRHGg/bw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HJg21oEAny4mltiwmhQWcfu+12wHtX7WSaF5Vp3XpwXwLYhOR/h0t521DSHHjxe+4r453sQNXfEl
t2OOpwnsYsEAuauVAiJL8/JjNqYwtt4J651YlfXElDfu23ctMTpPh1h7YQNGR+Z9auBbvKah83Tf
uHmFCBilL377tQP+0qwHiEHZ/gsi98g2Xm4bspViz5yhaqJA17/NWGKGbvVu9MTvyWqBIU3rFxqa
4RygyPM6XfXDIW8b2XnHVayND+ZOji8ACZsFrC/8ahmucbHACUgbLIR9i849IvUpuTvBSIHsrK6C
Tkid+un+wjpMlqxYVrETtVF2hBB8vZO+KczX9Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 880)
`protect data_block
NhQyjLtUcio6tibmweyBeS+HyWiZI2EH/qv6+zMa/b2K+tG3wRtyFxsGHXnDlbFFyZxwjymJhjvB
/WNtpt5b19825eWhHFZeBNVmmfbKN9S6w1n7I1QbK+pH/ZUawHzDh/0HXFNCtcM2oH7PQLtK+LhZ
BOXsAVOeVLKI+yNqi+V95GzU/HYxKJzBHbSu3vResnQk2zk/8jEpz+bstwQcgpjeWAuP6qlusfBz
z6fqTAEwxClIq03EEUfXS3z8eAz6EpPJd03U+jNdNZv/hazjDcRC4Gaw1LmdpmJ6GGuJh5euOsyk
bWp4rmEokVcD/ChstJfBOlfG/eL4D/uM5tUHkaRC5I8e6gYx3od6s24YG5vdoWRfXrEnMVEDQHD9
np0rMBuR6pwuvzmYBldAB5/Gr6lSa5lkWYPebHraG3ZpsluYKG71EKlWXVWdbD0m7aoCAqfWvCht
9vcfZmTE5wozAuHo+wQhnDggBYACehUBNxojCkqIiaDWfo8AcoPZl2Ls5oc89Uf0hLOLpR4zm2B7
1ng82JcO4HGBXB1WToDm2JgsfX9dyHTYLvp5g3FOhp7JR8UruehiJcf6hFB5zQ0Fq1w3xpVm+/j/
taCRQ1KFuvh1q4i2/MMpzo0aT7MAz9ztdq4rUn87cRF/QvCXjPpgSQE+rhMmMZIU7NaI9XYiYmiF
lL4rXNPxwZ6K/KVi0Dfv+I/JZSEt2ETzM6XlJi2bV8fFX6pguYCEK/o1M1TNSSqSQ0IvVWgYHvBz
HhUlB9Dd7UH1Fr4Vi6qNVp/7M2VCHM7xpcpHjRX9kQ3VvP6mpI/KXuH4wZ920WHehihwJtmJ/sOB
H6U8sGN1j43QUYiAwnziFabL352pUFB05TGgcvHHXUG2e/qT6njxDBTN5x4kINn86ooiF/7NxlFH
f8tYkiGpWbt8YxiTiyekWnFeYvbazPiEIOv2xQfaHPc57BUiVeZS1KvBhUJcsTG2ITl3J5v2XaOr
3w4ZscirncqZSOT1cJQThCESDPQU/IMbqswTrJE88HZujEOd47ImGnvp7/yAcO7rwufFA/Chrvgd
2iLm17FwakY/9eB867Enpw5tUaax9AwhVg7h99JN/TyTYtx2PithVmr52DudhYzhtZyO9l+YvNfl
bbjaMLRgQlaKmZioiIvjvGuUpQx0mKv8KQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "Estimated Power for IP     :     5.6824 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "ip_scope_blk_mem_gen_i1.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "ip_scope_blk_mem_gen_i1.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "yes";
  attribute c_family : string;
  attribute c_family of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 : entity is "artix7";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2 is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  doutb(31) <= \<const0>\;
  doutb(30) <= \<const0>\;
  doutb(29) <= \<const0>\;
  doutb(28) <= \<const0>\;
  doutb(27) <= \<const0>\;
  doutb(26) <= \<const0>\;
  doutb(25) <= \<const0>\;
  doutb(24) <= \<const0>\;
  doutb(23) <= \<const0>\;
  doutb(22) <= \<const0>\;
  doutb(21) <= \<const0>\;
  doutb(20) <= \<const0>\;
  doutb(19) <= \<const0>\;
  doutb(18) <= \<const0>\;
  doutb(17) <= \<const0>\;
  doutb(16) <= \<const0>\;
  doutb(15) <= \<const0>\;
  doutb(14) <= \<const0>\;
  doutb(13) <= \<const0>\;
  doutb(12) <= \<const0>\;
  doutb(11) <= \<const0>\;
  doutb(10) <= \<const0>\;
  doutb(9) <= \<const0>\;
  doutb(8) <= \<const0>\;
  doutb(7) <= \<const0>\;
  doutb(6) <= \<const0>\;
  doutb(5) <= \<const0>\;
  doutb(4) <= \<const0>\;
  doutb(3) <= \<const0>\;
  doutb(2) <= \<const0>\;
  doutb(1) <= \<const0>\;
  doutb(0) <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      clka => clka,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      ena => ena,
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "Estimated Power for IP     :     10.698 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "ip_scope_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "ip_scope_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "yes";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "blk_mem_gen_v8_4_2";
  attribute c_family : string;
  attribute c_family of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ : entity is "artix7";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\ is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2_synth__parameterized0\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      ena => ena,
      enb => enb,
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
ZBY06y5BSEA3vwLtCYy6nxOZv3rYFFgZv5ABjBaqtaItkwdtQfFvZBIMhBOgu0+1i4DhnUz7pdYr
Y88DaxXmyw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Q91nMYZhjxb8KT0ODrW+miquus8bIV0xJDXXyQLu4mbE2ZGK0HYqPk6xE96lKrNSpNViHea0rEyX
J3Qsb1QJLBM/4rnfg8PNzn8acqAN22JgnqyTntYQVpk0fARej5ldkyKbsCPgkFDFJQnDbUHBIcF2
clV1QCjE7A3SvN91cV0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
fpeDNxCbq4trL0iAEhu+gbl4Rix2OTBKp+3DlpwRVRrJB8M79X6xv2dY4g29GTJWY/qcPCM3xauG
RxLbIsN70w9DSrpdJ31jxXSOp/N0b21smrkPYOGR9al1eBkfjYMFWbiVzWEKHK/6z705awwEunRN
qhtuKyDzs9JphrMi08O8ld4FYuGNYbtDOUXkizCIgaOdAfQTq0yCDea9z6uJ5sQUPwqrjRIroSnJ
mW8XvC4+hFTtIH4kcsR/hWe9eHVCVq7yIdgTrHznDz5I4c7+A0ZUoahnR5dHirQC2z7KKzrCldej
93tdxPQksB7VjPElshg8WP1MGrwn+7hvSijdSw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
No6agU7QCIBdcP4teTJDlwXV+g3qBzu8V5gqFUsql+qUP2ZRyYvAPscmGZyPnHh9xvIYYFmXqCE7
RRM/BcEtyrJ9GJvahRcE/doL0n1EHIOASw/MZnFHkf6gtqWvN+SIv29/H/UyUfhuDXqJBGjBGBRs
+/RValRovCLF1SU7AdbCQbWKJbpj9JDmu7gpnhPbkiKkLcd0L7j/KcvlPBvHLG2JvHXct9Oyye9y
FJ190Nne/diMvLsfTBKIzRzQiV/kj3aSYxw4yzuKLbdVZ9eZYqFHwhjBXrVIvIAq9zy3Z0JajEGH
8Eg7Z1uVL2BNbnB2qP4/6a3wYkq6RDa/mFw99g==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Au9tuW8YCiySVmtwoSZ2LqBsVMwu9uzGBs0i03rtA+ohnDzpS7/saWzSdnxtvJsmHKLPTnuG8etw
O+1iKknogGQAhYN8j4DK0/PmelqEJy8N5vwkQ/o6l1cfVFLfqvAMRbZ7lkPzco2SCT7/KjEJHW7i
5gy7tqPxnW7QwYv2vH65EVqe0p2tQ2kCHVUvvPaAZbeDzA1LHleCahBpWEI3g5wztTT869s7a4yn
1IeWyD5NV38NHHcwqubPZ09C1Vm5NLAHW7sEnM3is9mRkFnCh/x4Fb6Ecuu4bJYFhgmNzCCKgYK9
PEdkW2OgY7EzDM7ocQQuoE0+aHQvw9lRdJm00Q==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
n8+Js6UruWrTa5ioc59l4AeAloQ6ZDwzPNPXUOknQWFRecrzd2eOQ2KSf6tv5Oxix315yAoI88kJ
L1R7xZeU1dj4QCJCinzjHZXGEfUurXJVEcq84ofioKIpCyBd7YnxOq469vjhUCYiTJvMARwPVvDY
U+jspt29lk+k5/XFur0=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HvScITgcbiG4YgkXwlLAPuMki7p9oPIAapsMuPCpK/tVnY9llE0MvUk/POKYiMFRuKgzht1jfNyM
pX8Qwv3/+iDiBgwTwibzi053ET+OglbpoF/MDrRErGx8VRvmBKwxnlefbxg6dCEzjNwYuFpDkHVT
YZySWRuz7hA0uzRJwLLkvg9LoVoAsjHpp+GqlpSqfuVaV3IJzpIboKGmFv2qLj7Z3k2aE4HhZfXc
HclRJsWxw/CA2DK86EGTnPC71xJNT7pgY1DSHCglqFwF35L0FfZes57Wpz5Ka6YR9dKPNCocMfXO
DZKOoy0+Zz/G4HOrhtHGxgzfEtHjRq0ZthhxDQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cKVYha4Te4udY8FRbf54r+kiMCDJLdj8cZ3W6fXS11h0I7QwRbP6OG7FbmMvT2GCOzmn4XFy6B2Z
gxL3ApKJzyLzYVfSkFeEzcBI6J8Dt2AlK9c+PGKZyJW60oYWpAPqWY20JfkTPAUKZHSYSX2m5BcF
fTTgU4ee8muBIDDBpvmB5A6T+fbsDgyjERP1jFwczrTt7PtnaAfYR/GSJLSrRiihIX10CqwONOev
FIL7n+7u5OQv8H7LHM+NBnheGweJoEjAsgL83uGsgyustlm5Rdn0QJ9Z87JbX0xA3LpRpH3lqS/r
Zhqx6EUxsohAa6X8OoelhAVicHZ820Qqxt2trg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
xPc5IDi8vguqpn1mtZ5BpdsrWbggKaDjbFXf7YzkA1/be2U1tdplp+WASa4kAO9MAxiW9rhjS3ZV
9CTL6ErdG1KpsZoS4t9kF7/WuDkQYPLebiSU6vwbk4VvuoH0J5vfDGgysdLhzEQSEwd1E1ARWcnS
KrceAExRiqEiyVVOI/adv1awB1LFTustw61OgABxXrim8bZCy5Rn7wUe+664m0yzdfLQ+lmo7bci
8s1Lte4ud8CW6SScpEliasBH8HQtIUpaErKLWkxNURU2GtlfevPG5En6uR/filkC8Pr/oxbOD6o/
6sK4BMaXs7LtCGq29R/Arb36Y5YbngMzbtWD1Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4976)
`protect data_block
NhQyjLtUcio6tibmweyBeS+HyWiZI2EH/qv6+zMa/b2K+tG3wRtyFxsGHXnDlbFFyZxwjymJhjvB
/WNtpt5b19825eWhHFZeBNVmmfbKN9S6w1n7I1QbK+pH/ZUawHzDh/0HXFNCtcM2oH7PQLtK+LhZ
BOXsAVOeVLKI+yNqi+V95GzU/HYxKJzBHbSu3vResnQk2zk/8jEpz+bstwQcgpjeWAuP6qlusfBz
z6fqTAG4cya0Ny4OFGTMsP8sz1J859/oWqYr6R7oGq4O6QaSLGd2gi9SnYDmGQAbmaj8wC9x+dSO
DC6xH1JR9piuWBn7TVjbX/ZktDyTStypZ4h/OBHsdPDGr0gpruBA00RJ/XV3qV/+VIaGANV2euwf
jQ8YLhY0zR+OvG3wqrhUbNSx6Rc5EDvbaSsKJAVWiLK/oG38fUxBPz20PyRsvsurSa6x9Kd072l3
sX/rO8UvXr78zE3KxlpwoadnpVRrtASquhszvrcsr0sOmZS340Tty7wBkVQkvo0C6zoTZdLX86KT
umg4Y7SCwj0GFEH4PZ/4iW3ntIkTFRasjScbykWGw2bOoLnPNox4IuKYPJjBjx9nDli2A4qcoSJp
PNiOIAaTYuMEJpbLqevGXZLv2NvsTjv0+eLJJbyn0jfM2/0xqkVBe8Nc9QRhWZHupu+JCqNnJFpM
c1ZbzQIJF8NN5YieevAz1oKNFSyAAkSH6mTExxO5qUBoayZq5O3FLoDYU+S+MRiLbgbE5zkqxjT5
7OrrWFUWzb3N4HIORb19/N74PEY8umO16usR2J9UGWcN2EKno9tZML0mVsYHGY7j1Gz0nOAYM9Yo
fek7O+Zuq/TXWmJpcgpjPQXZcgaVA4d1YU6jKPVk7uYizVBj8rKf2TN1Oi7pbHMf/hm95P5RoeX7
O75OQFDteGORfqHsug3IH9umzTXKEhq7D67j7s6Dx5uL5cU06uBdb2z2gSiiRgRoAKhA2fVCNXxK
2hGbdkUPpPhqq5MW3ou3C4GxfCp33jSFYh+TwYDRDr6OKCAXHzMyWgepGWGAaMJI563BWPGAf+0u
NdPExYeCyKA9zXPxBppjpO4u7uC2gMt2gpTnUMk6g/LYATh+1/oVV6LVuzLCfvrJ91U819KGHgMC
AZE//Y3d+ogt+/u2FctkslN8mTB2V6k3eXzOFfY3if/pYaaX8qtERa7sWYD7atyv7Pyr3Fz9+L06
j1fg5vJH2yuHLmbLYwoQEQQ9gDsCcrEYO/rxQt63h9C5Wqm9aFyqfpVbNolQLYIZm81L8ljNiz8S
MTd6LRuXObZzI5sXwhoW+JGHXVAIIec5TYxyEQYUc6B6SoYkwKjVi8KCsxm4gwHnwI6WmTV9XykG
69w0xw5nuhPVKV7MIQNwqwi5umOAC2MhiHsT8pDsGAZyJ5PFfCP2Z9gjvCsLIeJgduSjUNe6i7bW
z3L8163gBdyliNN5+1WED05RGpUreveRSkFUmNdItnRm1ed+kxt8dLCU4SMyd+ZYN7KULNGAbQDq
zJhAtOAywsnnNSDwffMtkxCQYWZVqBKYh9ncxwn5diEShnujexIxEhBfe4TNHO8WBLgzbWfO4U2T
jcsXDltJ7JSb4FyW0thQA6rxOmMGDChBtc1mxhfWCsq7PZCM4oeLY6iirUY3tZHX0at/fpgmWsNf
hMPbL4ot3e3j830a/XnFUc817yd0DXcuSlXgMWNDzmJWERVno0ZrDKS5VlFJE5trTKRIF5xqCAed
GDu/fKO8nt/RMsUuWdUjyCoR9BUS3Q0MDCZflIRW2N5tIbXqYqVFED0U8NA3C3GDNC/JXLRu9ilH
oH27x8LTI+lzs4R9+bJpALlLDC/dQA2sZYLC0OKrr/XrepMLaYUNHMbSb7pDyySHSNhFWW4ispVV
mnI5rU4UJJbnd7s///JKDIjzK64ZyudZ8qs668J5YeRyGXtSefFCEE2s3DHavBXw0pZXikKi/wML
u8uzja2xA6C9LTSv0eIyIxFdqMqfDbvs8u91cBc28qCZ2PQvxnSm17zuHP5lYIGNgUMlqqBUchHv
wrJIhhVQExo2QBfHVt2xXvXeE/zeH1kCNSFLRY7pTyfhGrnfSi0B9B1V6nlwhH8KEKGIO242xSX2
h9lc3+mjToYssUMvZ0JhnnWv8FeQQ5glpxw1Dfwg0F7q3uJz1NDHLZlrh+A546nPAIh5HWoQa/9x
EJkkH8sGktlOl87hfIWIJnWa+T7ltxvKzcHoHLdZChyz/Pi1CWphsGG64cIRHwYler2ILeVEk6Lg
8NqFFeMI+WaQQnaTBpYeao1VtQN4ifWMErrpvZQHlcJDx2cMAw25LyQ2sOfV7uvhTvYUVQ27wqa0
grF2fETEnRjhyExqINsAi0T1gnfrVQnlQUISLhMwzGgVGDGCu2Ses3ufemMqha7X9Iuj1iD1Ebdh
dcM5H3qLV51O5jd8AT5iEkPBy/Mb2TKOmiuHezX5g93H2SkCYPH/OClzQRnIqvU1xFAUQacziEeU
XGpOJ73pQTZy5Km24LWMDvUScT2Ax1RttPBaVI0S+6QijtIUq1eFafVFv0fGSrAyPrB2fjF4EG09
NKVVjgyYs6g8sm5lf7OoAecyB800NxQ8njiCLcj3QSzQzeN8wy1uRn+c65re0KJoWDvAvEkseqPe
N0VuezZOtOEUDh6S1wfijD6z2oMevJNWgDZnsax5leXRV+oaZ/3VMbUsRBJPsvq8SkvV4wsUMtge
/mE3cPES6kCzXFCiJDVkI3Hy4m2GQJJcUhMdR3A5CiMBgUHePfJh4nz3/MUZIu00zM5KPT+saJK3
UH2ayG2xxnRMhhQM/i5JK9MxPCbdMJicjuh/Aia5WhdI0/m21fFcK2YafhxNA36Fb+Oe4XLdX+qQ
+uiPukRoo42ewdBz33xAIymAvZPYsBK5jpIja1ET4tdcwA86M3SXs9Iz6br50tamhglBSBMxiu49
XxjtYF3KMTdaDLBcEHkZUjdxAk32GjmtCQQr6wadSc3aMGy2uRQA1yFR4ORpgQwgqti1s1TgO1yR
YXsYspHSop3dUT9k+GM+qfvuNrSKKlfk3QS84EMJn0xk6dsLC8imbe3AgDt+dHYlTsqbAT/XaFaF
zwNi3ltoQfOrVK1khuuQejxsakpCOqXu+bg7BeqwJDwF4c46f9t64LQ8GqoI/aG1sXMHJiQa+3qj
NjWd679MQ4TFdESLSaOPsD0eR/D1KsEa7DVMMskC0wfv4ZTRtXOfIxzeGfj35IovauQGja9CLYdf
Uj6lpSDQaYAQs6iK8Jsluaq6m010Z/u8XmGT9h92x9C9cD/g3asUfPNtdYPM5gFY4dwBQ1afIqhe
qdkFJD3ioTugNUl2sFPXpfUdnLHoQDX4QTgjM8OjjalwkqmfzM31Sph0zfW711HLVo4p3/5N1VAc
psCSh2vyM5DlLw1OyLAOgqMF8RteifGHPEoMtIK5WGKU/ZH/IqaB0DzfeyK87UwvPC7YG7Uz5oia
fqaoOr3XtGxmLh0coyVcwhbb4WPoUjzY7UZj3NfasXx8nozkTEciNAEPVbX2Yx3JlnppWdqKPo/W
I42bszc6qaQWoeUAwtHhMYCoG1hL9Ngr9NgxL6h/dw+cqLb1lzIRZXAWX2cpvCcYN71/09wqzxq4
jvCAkJOsNaGr5f/MylHqfJEFdrERSBkrolM4xzQZNlrjpmF6YlnQYYuRASShiPDeNiFw+eM+8BBz
Jlba65LVgIqhjsPN346sMwmb+B8fx2Jx31bvWClRqu0dhVKOfuNna+eL+QiVDSS8B2GCxCqYW28B
0kd2gu6JUqjDgGsytV86ttrih5MSUrSeI68rQ6tfzNbv6NUv0q9t/vncitkI4MeCzKLW3gtLSYuU
+P6rUJsE2D8JVQmqJ4SL3qgfJHEQrlbwzKuao2vfdu5AGlQuu8gl3Pf8EfzhqftaPQYa68nWjFfk
a5gS9PxX5ySq5ctHRlVZyNEo4rrAiFimREPHLtv27/0gEqcro8KQvU4dGcN2MNuYuve6szw2bJiK
1TBpbE1yZWIkW6I5HEMMbHDpaA/QsJd5G5GGnX6pYS7oGsjhmRb8NdbjvIhpn++tV7VQ8n0uDij7
aWxFOJj02K36lGC2Cb17Uml3Xrvc6qUtA0V2jUp7Tp4IQyXTDUdwB8WIaHzH3LWljZ4H0lHU5/i0
zAgQMrEoX0hM++d+cvHdg4zqBmQNM5vXaxUaEOU0W8M1rCN/O50976SzI4G2yq8pD65N/VffV0BF
UOE+ZyFeCpcoMAJKoPppK55hekRx0np0d1FmLH/hLtrJsECVBgrVFOEZjjYAC3d8grQ63RK33X27
e+y2t7mD3y6OonGYXGNv9jUIHvYCwuh6/BufoDIfwUUpRIO+w077qPLsjVoR5//NXQaVnkd4WSkV
ywxloEzJhTlJp+Z1sPF2SEKYnmdyck37uuiaHlek3T7oeT7+BYtILbkvJqC3GM1j0ifcPcdsJcOm
2W1HVhEyXqujI4oA3mwd4RKt3SZQbD+R7Xq+q60illDkZ0ywGT8Wz33ANGpGCSrB5iT680XmGF+w
6VyLEPEK0ytm04eQ7EoDe3H6zn5n4UjaDrpexKpUfizVq5ahfE4Ziw3Olpg6/1xg6At3NWswTDXu
zvpzIPIiHhZCASV/QiIUqEvWEH9pDC2iynMYNsemf/dmPLk5kVmHSgHcqhToqPPUogxpxTXWreoo
z6yqb6YL43xf8vE5LhcjF+WCuNwiVftG3uamdKI56XkmEb+MTrO+g06PZLhNxdOdCNiF9mAoszQy
LEUkkCliJX0n7BX08sRjdrJan13YJbosXx6zq77KaADpyEAX1ovcofRDFmNrZsrtJnR5jUbuxm7F
6bkHyPLdrxtjSLK1hdfrfV9ey9jRPu2/cUHYBy7C4TtaLTVN+kZdi7RAtVXm7d9Tahu4oB7oO2mm
nuy3zF85ghqJ231QA67O77DfYy5QLRwJXEefXiEjt08VCkDSag3D7XC+cLbcji/6HPYAHOVNDwuz
XFWbgQby1heNHjprjZ1Cb6P1OGck/aCJCWL+7Pu2H29B+KWcDQOmKr72ViN413LOwaJTkJIOVOpC
2NbFOU2eEw8QgoBKmkpSR1QoAckW0wtP2nuDEXchgaatacVHzj3GDyifm2aE6/ujSeI9CJyp8EE/
+77V5OC5dFVuOY+jx2VUy3b6kJ/CJI0FWaIAHFpSxylEHVRr8SRzJTIPbYhptrARYXoSXjltxXHR
1/Wnf7wtydgMfcAHfOsFqP0ZjYqFKvyUbFGeMxYhxORabuC8ggHCyaNC1/2hiBDPvf44rERdf2K3
DTR82goiSPfwbgAdVW7/jiCzwAyAKC6IM70QtFx4P2UubbPbwszAag0Wf5TDPJbP1Zl6xAhADSro
Lusk1FIj2+ZeUWwTVwnZ4wDmdsUQqDMq79SNWdS58a0FiCgYBXLbDRNvHAyM1ka+oAR58LfAdeJi
b9a7uCgVlmEmGITAD+veP00FxIqVechYVnw0ZFM2GgHUZn84JqF3Bdu+iyZ+yhjfCnxW9Js+ZHFu
LueTghjeqD7CwCl/W63k4tyOH30uWEUUDHBN8qzUVhOuPfDhfN5A51nm97hYd16F6crgEfm3q3YE
fJeDLnLJ7DHPnLemaXDETY/S7+Suhvqig4AQGKUX1Z5ypAjXAS+5SAZzb8qra1cdKrZTp4U7cot6
IeldydArS7mdEbFPzCzoHrfTPdqFyj+xvROcNVT+T8gQJGrKVPtM8wmoyabdeUqRjuHQ7LbCFqmM
Y3TofhTEbuVONjxZbPGJPGSbfF/ul29+YZk2h/XcTKWP830IPtXbRCRm2Hu+RAqS5rsTPi1Bv+8q
8SOf7+v66P0tDCeAUwHpbHNRbj1fFULA4cyEPPqfIHqv4orErvq3JBwj9c7ofnLgn7vFt7AfNoZQ
auDSSegDag0S6qzozpLLh3lTVt6SCrXdFU8ED31a+vRHuIjj4qNMoZZFMmHxh5rPQ58A/QVCipEI
O2lfUdTl9j9CJEe1PcrjxGxa6ZJpSnIVrCVCUWqpXJAw3hUnZ1o5g4pWaOoKdABVZHHvc3xuwNA3
LwSdEKdnXiqwKxMv3IdWYlGBVO2wEWLjI0QhiLNc1eh3lZV4K3WE5xZnUfFzR6/0RFwP6VZZO8Yb
74HAzP8R+fENn6yMA/rHxVL9tQvXFE+UTO6laG/iZBUpEjlJB5AsO9cC+VmO5+Fg31heW1Q17OIx
uDbtWKF6XAAg+7azeZPERC6OttJHdtcY7IRMZSAr9Bimp8sFjcz9gniw94yMFVA8y/JzGZ7d2tt9
WSV+cy1xaC95lREpWimyoEN/NJu1NP4/GTkzxc0h7LszapeM6bPS5bqM4J3DJp4LdNXNFJVfG2YC
aUCheFcDhiNwgEDRpNyrapKm6nVRA2Bt93c1XaVHjQxAl11jaoBmJgAEHoTCHTH05uY30FruE+sm
e3MirWeXU9S5sJ3b6PP2dQGCHZ6e1bmoE0Jc+LjVjIStixOI2JMwG5OdrRooxejrSyF3B6GBm3EE
MocT0qYm4Lw4ST4t42YgxfGWRnGojH841GwgkeKyjXiw+ZnWhA0XRG2fj3HEJCIcR2fDvJLXRlLv
g+/ickdf3XRDHpPvhF2+yZQ=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 : entity is "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_2,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 : entity is "blk_mem_gen_v8_4_2,Vivado 2018.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     10.698 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 1;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_scope_blk_mem_gen_i0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_scope_blk_mem_gen_i0.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 2;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of clkb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK";
  attribute X_INTERFACE_PARAMETER of clkb : signal is "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of enb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of addrb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of dinb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of doutb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
  attribute X_INTERFACE_INFO of web : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB WE";
begin
U0: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2__parameterized1\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => doutb(31 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => enb,
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(10 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(10 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(10 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(10 downto 0),
      s_axi_rdata(31 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 is
  port (
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 : entity is "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_2,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 : entity is "blk_mem_gen_v8_4_2,Vivado 2018.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_doutb_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 11;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 11;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "2";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     5.6824 mW";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 1;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "ip_scope_blk_mem_gen_i1.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "ip_scope_blk_mem_gen_i1.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 2048;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 2048;
  attribute C_READ_LATENCY_A : integer;
  attribute C_READ_LATENCY_A of U0 : label is 1;
  attribute C_READ_LATENCY_B : integer;
  attribute C_READ_LATENCY_B of U0 : label is 1;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 32;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 32;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 2048;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 2048;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "READ_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 32;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 32;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_family : string;
  attribute c_family of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of ena : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  attribute X_INTERFACE_INFO of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute X_INTERFACE_INFO of dina : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  attribute X_INTERFACE_INFO of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute X_INTERFACE_INFO of wea : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_2
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => B"00000000000",
      clka => clka,
      clkb => '0',
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => B"00000000000000000000000000000000",
      douta(31 downto 0) => douta(31 downto 0),
      doutb(31 downto 0) => NLW_U0_doutb_UNCONNECTED(31 downto 0),
      eccpipece => '0',
      ena => ena,
      enb => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(10 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(10 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(10 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(10 downto 0),
      s_axi_rdata(31 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 10 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is "1";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is "1";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is 11;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is "artix7";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_i_synth_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 1;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 11;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "1";
  attribute c_has_load of i_synth : label is 1;
  attribute c_has_thresh0 of i_synth : label is 0;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 0;
  attribute c_thresh0_value of i_synth : label is "1";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
  THRESH0 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12_viv
     port map (
      CE => CE,
      CLK => CLK,
      L(10 downto 0) => L(10 downto 0),
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_i_synth_THRESH0_UNCONNECTED,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 : entity is "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_12,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 : entity is "c_counter_binary_v12_0_12,Vivado 2018.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 is
  signal NLW_U0_THRESH0_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 1;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 11;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "1";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 1;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 0;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "1";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute X_INTERFACE_PARAMETER of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of LOAD : signal is "xilinx.com:signal:data:1.0 load_intf DATA";
  attribute X_INTERFACE_PARAMETER of LOAD : signal is "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of SINIT : signal is "xilinx.com:signal:data:1.0 sinit_intf DATA";
  attribute X_INTERFACE_PARAMETER of SINIT : signal is "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of L : signal is "xilinx.com:signal:data:1.0 l_intf DATA";
  attribute X_INTERFACE_PARAMETER of L : signal is "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute X_INTERFACE_PARAMETER of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_12
     port map (
      CE => CE,
      CLK => CLK,
      L(10 downto 0) => L(10 downto 0),
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SCLR => '0',
      SINIT => SINIT,
      SSET => '0',
      THRESH0 => NLW_U0_THRESH0_UNCONNECTED,
      UP => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram is
  port (
    r9_douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r2_wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    r1_addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r3_dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram is
  signal \comp0.core_instance0_n_32\ : STD_LOGIC;
  signal \comp0.core_instance0_n_33\ : STD_LOGIC;
  signal \comp0.core_instance0_n_34\ : STD_LOGIC;
  signal \comp0.core_instance0_n_35\ : STD_LOGIC;
  signal \comp0.core_instance0_n_36\ : STD_LOGIC;
  signal \comp0.core_instance0_n_37\ : STD_LOGIC;
  signal \comp0.core_instance0_n_38\ : STD_LOGIC;
  signal \comp0.core_instance0_n_39\ : STD_LOGIC;
  signal \comp0.core_instance0_n_40\ : STD_LOGIC;
  signal \comp0.core_instance0_n_41\ : STD_LOGIC;
  signal \comp0.core_instance0_n_42\ : STD_LOGIC;
  signal \comp0.core_instance0_n_43\ : STD_LOGIC;
  signal \comp0.core_instance0_n_44\ : STD_LOGIC;
  signal \comp0.core_instance0_n_45\ : STD_LOGIC;
  signal \comp0.core_instance0_n_46\ : STD_LOGIC;
  signal \comp0.core_instance0_n_47\ : STD_LOGIC;
  signal \comp0.core_instance0_n_48\ : STD_LOGIC;
  signal \comp0.core_instance0_n_49\ : STD_LOGIC;
  signal \comp0.core_instance0_n_50\ : STD_LOGIC;
  signal \comp0.core_instance0_n_51\ : STD_LOGIC;
  signal \comp0.core_instance0_n_52\ : STD_LOGIC;
  signal \comp0.core_instance0_n_53\ : STD_LOGIC;
  signal \comp0.core_instance0_n_54\ : STD_LOGIC;
  signal \comp0.core_instance0_n_55\ : STD_LOGIC;
  signal \comp0.core_instance0_n_56\ : STD_LOGIC;
  signal \comp0.core_instance0_n_57\ : STD_LOGIC;
  signal \comp0.core_instance0_n_58\ : STD_LOGIC;
  signal \comp0.core_instance0_n_59\ : STD_LOGIC;
  signal \comp0.core_instance0_n_60\ : STD_LOGIC;
  signal \comp0.core_instance0_n_61\ : STD_LOGIC;
  signal \comp0.core_instance0_n_62\ : STD_LOGIC;
  signal \comp0.core_instance0_n_63\ : STD_LOGIC;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_2,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_2,Vivado 2018.3";
begin
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0
     port map (
      addra(10 downto 0) => r1_addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => axibusdomain_clk,
      clkb => signaldomain_clk,
      dina(31 downto 0) => r3_dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      douta(31 downto 0) => r9_douta(31 downto 0),
      doutb(31) => \comp0.core_instance0_n_32\,
      doutb(30) => \comp0.core_instance0_n_33\,
      doutb(29) => \comp0.core_instance0_n_34\,
      doutb(28) => \comp0.core_instance0_n_35\,
      doutb(27) => \comp0.core_instance0_n_36\,
      doutb(26) => \comp0.core_instance0_n_37\,
      doutb(25) => \comp0.core_instance0_n_38\,
      doutb(24) => \comp0.core_instance0_n_39\,
      doutb(23) => \comp0.core_instance0_n_40\,
      doutb(22) => \comp0.core_instance0_n_41\,
      doutb(21) => \comp0.core_instance0_n_42\,
      doutb(20) => \comp0.core_instance0_n_43\,
      doutb(19) => \comp0.core_instance0_n_44\,
      doutb(18) => \comp0.core_instance0_n_45\,
      doutb(17) => \comp0.core_instance0_n_46\,
      doutb(16) => \comp0.core_instance0_n_47\,
      doutb(15) => \comp0.core_instance0_n_48\,
      doutb(14) => \comp0.core_instance0_n_49\,
      doutb(13) => \comp0.core_instance0_n_50\,
      doutb(12) => \comp0.core_instance0_n_51\,
      doutb(11) => \comp0.core_instance0_n_52\,
      doutb(10) => \comp0.core_instance0_n_53\,
      doutb(9) => \comp0.core_instance0_n_54\,
      doutb(8) => \comp0.core_instance0_n_55\,
      doutb(7) => \comp0.core_instance0_n_56\,
      doutb(6) => \comp0.core_instance0_n_57\,
      doutb(5) => \comp0.core_instance0_n_58\,
      doutb(4) => \comp0.core_instance0_n_59\,
      doutb(3) => \comp0.core_instance0_n_60\,
      doutb(2) => \comp0.core_instance0_n_61\,
      doutb(1) => \comp0.core_instance0_n_62\,
      doutb(0) => \comp0.core_instance0_n_63\,
      ena => '1',
      enb => '1',
      wea(0) => r2_wea(0),
      web(0) => web(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram is
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_2,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "blk_mem_gen_v8_4_2,Vivado 2018.3";
begin
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1
     port map (
      addra(10 downto 0) => Q(10 downto 0),
      clka => signaldomain_clk,
      dina(31 downto 0) => dina(31 downto 0),
      douta(31 downto 0) => dinb(31 downto 0),
      ena => '1',
      wea(0) => '1'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free is
  port (
    Q : out STD_LOGIC_VECTOR ( 10 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    LOAD : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free is
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of \comp0.core_instance0\ : label is "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_12,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of \comp0.core_instance0\ : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of \comp0.core_instance0\ : label is "c_counter_binary_v12_0_12,Vivado 2018.3";
begin
\comp0.core_instance0\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0
     port map (
      CE => '1',
      CLK => signaldomain_clk,
      L(10 downto 0) => B"00000000000",
      LOAD => LOAD,
      Q(10 downto 0) => Q(10 downto 0),
      SINIT => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    signaldomain_clk : in STD_LOGIC;
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    o : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline is
  signal counter_op_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal relational_op_net : STD_LOGIC;
begin
counter: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free
     port map (
      LOAD => relational_op_net,
      Q(10 downto 0) => counter_op_net(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
relational: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88
     port map (
      LOAD => relational_op_net,
      Q(10 downto 0) => counter_op_net(10 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
single_port_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram
     port map (
      Q(10 downto 0) => counter_op_net(10 downto 0),
      dina(31 downto 0) => dina(31 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain is
  port (
    dinb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    \full_i_5_24_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    web : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clear : in STD_LOGIC_VECTOR ( 0 to 0 );
    i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    o : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain is
  signal \^full_i_5_24_reg[0]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register1_n_0 : STD_LOGIC;
  signal register1_n_1 : STD_LOGIC;
  signal register1_n_2 : STD_LOGIC;
  signal register1_n_20 : STD_LOGIC;
  signal register1_n_21 : STD_LOGIC;
  signal register1_n_22 : STD_LOGIC;
  signal register1_n_3 : STD_LOGIC;
  signal register1_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register2_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register3_q_net_x0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal register4_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register5_n_0 : STD_LOGIC;
  signal register5_n_1 : STD_LOGIC;
  signal register5_n_18 : STD_LOGIC;
  signal register5_n_19 : STD_LOGIC;
  signal register5_n_2 : STD_LOGIC;
  signal register5_n_20 : STD_LOGIC;
  signal register5_n_21 : STD_LOGIC;
  signal register5_n_22 : STD_LOGIC;
  signal register5_n_3 : STD_LOGIC;
  signal register5_q_net : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal register_q_net : STD_LOGIC;
  signal risingedgetrigger_n_0 : STD_LOGIC;
begin
  \full_i_5_24_reg[0]\(0) <= \^full_i_5_24_reg[0]\(0);
convert: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0\
     port map (
      full(0) => full(0),
      \reg_array[0].fde_used.u2\(0) => \^full_i_5_24_reg[0]\(0),
      signaldomain_clk => signaldomain_clk
    );
delayline: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline
     port map (
      dina(31 downto 16) => register2_q_net_x0(15 downto 0),
      dina(15 downto 0) => register3_q_net_x0(15 downto 0),
      dinb(31 downto 0) => dinb(31 downto 0),
      o(10 downto 0) => o(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
leveltriggerfifocontroller: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller
     port map (
      DI(3) => register1_n_0,
      DI(2) => register1_n_1,
      DI(1) => register1_n_2,
      DI(0) => register1_n_3,
      Q(10 downto 0) => addrb(10 downto 0),
      S(3) => register5_n_0,
      S(2) => register5_n_1,
      S(1) => register5_n_2,
      S(0) => register5_n_3,
      \full_i_5_24_reg[0]\(0) => \^full_i_5_24_reg[0]\(0),
      register4_q_net(0) => register4_q_net(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk,
      \state_4_23_reg[1]\(3) => register5_n_22,
      \state_4_23_reg[1]\(2) => register1_n_20,
      \state_4_23_reg[1]\(1) => register1_n_21,
      \state_4_23_reg[1]\(0) => register1_n_22,
      \state_4_23_reg[1]_0\(3) => register5_n_18,
      \state_4_23_reg[1]_0\(2) => register5_n_19,
      \state_4_23_reg[1]_0\(1) => register5_n_20,
      \state_4_23_reg[1]_0\(0) => register5_n_21,
      \state_4_23_reg[1]_1\ => risingedgetrigger_n_0,
      web(0) => web(0)
    );
register1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0\
     port map (
      DI(3) => register1_n_0,
      DI(2) => register1_n_1,
      DI(1) => register1_n_2,
      DI(0) => register1_n_3,
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      \fd_prim_array[13].bit_is_0.fdre_comp\(2) => register1_n_20,
      \fd_prim_array[13].bit_is_0.fdre_comp\(1) => register1_n_21,
      \fd_prim_array[13].bit_is_0.fdre_comp\(0) => register1_n_22,
      o(15 downto 0) => register1_q_net_x0(15 downto 0),
      \rel_39_16_carry__0\(13 downto 0) => register5_q_net(13 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1\
     port map (
      ch1(15 downto 0) => ch1(15 downto 0),
      o(15 downto 0) => register2_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2\
     port map (
      ch2(15 downto 0) => ch2(15 downto 0),
      o(15 downto 0) => register3_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register4: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1\
     port map (
      clear(0) => clear(0),
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
register5: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3\
     port map (
      S(3) => register5_n_0,
      S(2) => register5_n_1,
      S(1) => register5_n_2,
      S(0) => register5_n_3,
      \fd_prim_array[15].bit_is_0.fdre_comp\(3) => register5_n_18,
      \fd_prim_array[15].bit_is_0.fdre_comp\(2) => register5_n_19,
      \fd_prim_array[15].bit_is_0.fdre_comp\(1) => register5_n_20,
      \fd_prim_array[15].bit_is_0.fdre_comp\(0) => register5_n_21,
      \fd_prim_array[15].bit_is_0.fdre_comp_0\(0) => register5_n_22,
      i(15 downto 0) => i(15 downto 0),
      o(13 downto 0) => register5_q_net(13 downto 0),
      \rel_39_16_carry__0\(15 downto 0) => register1_q_net_x0(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister
     port map (
      enable(0) => enable(0),
      register_q_net => register_q_net,
      signaldomain_clk => signaldomain_clk
    );
risingedgetrigger: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger
     port map (
      \ff_2_17_reg[0]\ => risingedgetrigger_n_0,
      register4_q_net(0) => register4_q_net(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct is
  port (
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    r9_douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    r8_full : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    r2_wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    r1_addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r3_dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    r4_threshold : in STD_LOGIC_VECTOR ( 15 downto 0 );
    r5_enable : in STD_LOGIC_VECTOR ( 0 to 0 );
    r6_delay : in STD_LOGIC_VECTOR ( 10 downto 0 );
    r7_clear : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct is
  signal convert_dout_net_x1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal mcode_we_net : STD_LOGIC;
  signal register1_q_net : STD_LOGIC;
  signal register2_q_net : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal register3_q_net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal register_q_net : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal single_port_ram_data_out_net : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slice1_y_net : STD_LOGIC_VECTOR ( 10 downto 0 );
begin
dual_port_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram
     port map (
      addrb(10 downto 0) => slice1_y_net(10 downto 0),
      axibusdomain_clk => axibusdomain_clk,
      dinb(31 downto 0) => single_port_ram_data_out_net(31 downto 0),
      r1_addra(10 downto 0) => r1_addra(10 downto 0),
      r2_wea(0) => r2_wea(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      signaldomain_clk => signaldomain_clk,
      web(0) => mcode_we_net
    );
register1: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register1_q_net,
      r5_enable(0) => r5_enable(0),
      signaldomain_clk => signaldomain_clk
    );
register2: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(10 downto 0) => register2_q_net(10 downto 0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0),
      signaldomain_clk => signaldomain_clk
    );
register3: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      q(0) => register3_q_net(0),
      r7_clear(0) => r7_clear(0),
      signaldomain_clk => signaldomain_clk
    );
register4: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0\
     port map (
      axibusdomain_clk => axibusdomain_clk,
      full(0) => convert_dout_net_x1(0),
      r8_full(0) => r8_full(0),
      signaldomain_clk => signaldomain_clk
    );
register_x0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister
     port map (
      axibusdomain_clk => axibusdomain_clk,
      o(15 downto 0) => register_q_net(15 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0),
      signaldomain_clk => signaldomain_clk
    );
signaldomain: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain
     port map (
      addrb(10 downto 0) => slice1_y_net(10 downto 0),
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      clear(0) => register3_q_net(0),
      dinb(31 downto 0) => single_port_ram_data_out_net(31 downto 0),
      enable(0) => register1_q_net,
      full(0) => convert_dout_net_x1(0),
      \full_i_5_24_reg[0]\(0) => full(0),
      i(15 downto 0) => register_q_net(15 downto 0),
      o(10 downto 0) => register2_q_net(10 downto 0),
      signaldomain_clk => signaldomain_clk,
      web(0) => mcode_we_net
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope is
  port (
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope is
  signal \<const0>\ : STD_LOGIC;
  signal r1_addra : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal r2_wea : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r3_dina : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r4_threshold : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal r5_enable : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r6_delay : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal r7_clear : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r8_full : STD_LOGIC_VECTOR ( 0 to 0 );
  signal r9_douta : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  axibusdomain_s_axi_bresp(1) <= \<const0>\;
  axibusdomain_s_axi_bresp(0) <= \<const0>\;
  axibusdomain_s_axi_rresp(1) <= \<const0>\;
  axibusdomain_s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
axibusdomain_axi_lite_interface: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      d(0) => r5_enable(0),
      i(10 downto 0) => r6_delay(10 downto 0),
      q(0) => r8_full(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      \slv_reg_array_reg[0][0]\(0) => r7_clear(0),
      \slv_reg_array_reg[3][15]\(15 downto 0) => r4_threshold(15 downto 0),
      \slv_reg_array_reg[5][0]\(0) => r2_wea(0),
      \slv_reg_array_reg[6][10]\(10 downto 0) => r1_addra(10 downto 0)
    );
ip_scope_struct: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct
     port map (
      axibusdomain_clk => axibusdomain_clk,
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      full(0) => full(0),
      r1_addra(10 downto 0) => r1_addra(10 downto 0),
      r2_wea(0) => r2_wea(0),
      r3_dina(31 downto 0) => r3_dina(31 downto 0),
      r4_threshold(15 downto 0) => r4_threshold(15 downto 0),
      r5_enable(0) => r5_enable(0),
      r6_delay(10 downto 0) => r6_delay(10 downto 0),
      r7_clear(0) => r7_clear(0),
      r8_full(0) => r8_full(0),
      r9_douta(31 downto 0) => r9_douta(31 downto 0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    ch1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ch_trigger : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axibusdomain_clk : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    axibusdomain_aresetn : in STD_LOGIC;
    axibusdomain_s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_awvalid : in STD_LOGIC;
    axibusdomain_s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axibusdomain_s_axi_wvalid : in STD_LOGIC;
    axibusdomain_s_axi_bready : in STD_LOGIC;
    axibusdomain_s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axibusdomain_s_axi_arvalid : in STD_LOGIC;
    axibusdomain_s_axi_rready : in STD_LOGIC;
    full : out STD_LOGIC_VECTOR ( 0 to 0 );
    axibusdomain_s_axi_awready : out STD_LOGIC;
    axibusdomain_s_axi_wready : out STD_LOGIC;
    axibusdomain_s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_bvalid : out STD_LOGIC;
    axibusdomain_s_axi_arready : out STD_LOGIC;
    axibusdomain_s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axibusdomain_s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axibusdomain_s_axi_rvalid : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ip_scope_0_0,ip_scope,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sysgen";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ip_scope,Vivado 2018.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of axibusdomain_aresetn : signal is "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of axibusdomain_aresetn : signal is "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_clk : signal is "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of axibusdomain_clk : signal is "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID";
  attribute X_INTERFACE_PARAMETER of axibusdomain_s_axi_rvalid : signal is "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID";
  attribute X_INTERFACE_INFO of signaldomain_clk : signal is "xilinx.com:signal:clock:1.0 signaldomain_clk CLK";
  attribute X_INTERFACE_PARAMETER of signaldomain_clk : signal is "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA";
  attribute X_INTERFACE_INFO of axibusdomain_s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB";
  attribute X_INTERFACE_INFO of ch1 : signal is "xilinx.com:signal:data:1.0 ch1 DATA";
  attribute X_INTERFACE_PARAMETER of ch1 : signal is "XIL_INTERFACENAME ch1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of ch2 : signal is "xilinx.com:signal:data:1.0 ch2 DATA";
  attribute X_INTERFACE_PARAMETER of ch2 : signal is "XIL_INTERFACENAME ch2, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of ch_trigger : signal is "xilinx.com:signal:data:1.0 ch_trigger DATA";
  attribute X_INTERFACE_PARAMETER of ch_trigger : signal is "XIL_INTERFACENAME ch_trigger, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}";
  attribute X_INTERFACE_INFO of full : signal is "xilinx.com:signal:interrupt:1.0 full INTERRUPT";
  attribute X_INTERFACE_PARAMETER of full : signal is "XIL_INTERFACENAME full, SENSITIVITY LEVEL_HIGH, PortWidth 1";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope
     port map (
      axibusdomain_aresetn => axibusdomain_aresetn,
      axibusdomain_clk => axibusdomain_clk,
      axibusdomain_s_axi_araddr(5 downto 0) => axibusdomain_s_axi_araddr(5 downto 0),
      axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
      axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
      axibusdomain_s_axi_awaddr(5 downto 0) => axibusdomain_s_axi_awaddr(5 downto 0),
      axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
      axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
      axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
      axibusdomain_s_axi_bresp(1 downto 0) => axibusdomain_s_axi_bresp(1 downto 0),
      axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
      axibusdomain_s_axi_rdata(31 downto 0) => axibusdomain_s_axi_rdata(31 downto 0),
      axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
      axibusdomain_s_axi_rresp(1 downto 0) => axibusdomain_s_axi_rresp(1 downto 0),
      axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
      axibusdomain_s_axi_wdata(31 downto 0) => axibusdomain_s_axi_wdata(31 downto 0),
      axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
      axibusdomain_s_axi_wstrb(3 downto 0) => axibusdomain_s_axi_wstrb(3 downto 0),
      axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
      ch1(15 downto 0) => ch1(15 downto 0),
      ch2(15 downto 0) => ch2(15 downto 0),
      ch_trigger(15 downto 0) => ch_trigger(15 downto 0),
      full(0) => full(0),
      signaldomain_clk => signaldomain_clk
    );
end STRUCTURE;
