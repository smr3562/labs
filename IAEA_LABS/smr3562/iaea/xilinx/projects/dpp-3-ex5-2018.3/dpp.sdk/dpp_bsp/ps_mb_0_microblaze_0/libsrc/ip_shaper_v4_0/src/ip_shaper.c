#include "ip_shaper.h"
int ip_shaper_CfgInitialize(ip_shaper *InstancePtr, ip_shaper_Config *ConfigPtr)
{
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

	/* Clear instance memory and make copy of configuration*/
	memset(InstancePtr, 0, sizeof(ip_shaper));
	memcpy(&InstancePtr->Config, ConfigPtr, sizeof(ip_shaper_Config));

    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}

static u32 ip_shaper_offset[IP_SHAPER_PRM_CNT] =
{
	0							, //Tclk - not used in FPGA logic;
	0							, //Taur - not used in FPGA logic;
	0							, //Taud - not used in FPGA logic;
	0 							, //Taupk - not used in FPGA logic;
	0							, //Taupk_top - not used in FPGA logic;
	0							, //gain - not used in FPGA logic;
	
	(IP_SHAPER_R1_B10)			, //b10
	(IP_SHAPER_R4_NA_INV)		, //na_inv
	(IP_SHAPER_R2_NA)			, //na
	(IP_SHAPER_R3_NB)			, //nb
	(IP_SHAPER_R5_B00)			, //b00
	(IP_SHAPER_R6_B20)	  		  //b20	
};

// register read/write access
// bit 0: 1=Readable 0=not readable
// bit 1: 1=writable 0=not writable
static int ip_shaper_rw[IP_SHAPER_PRM_CNT] =
{
	0,		//Tclk;
	0,		//Taur;
	0,		//Taud;
	0,		//Taupk;
	0,		//Taupk_top;
	0, 		//gain
	
	3,		//b10;
	3,		//na_inv;
	3,		//na;
	3,		//nb;
	3,		//b00;
	3		//b20;
};

//
// read from registers
//
void ip_shaper_ReadLogic(ip_shaper *shaper)
{
	u32 baseaddr;
	u32 *prm;
	int i, r;

	baseaddr = shaper->Config.BaseAddress;
	prm = shaper->Config.prm;
	
	for(i=0;i<IP_SHAPER_PRM_CNT;i++)
	{
		r = (ip_shaper_rw[i] & 1);
		if ( r != 1) continue;
		//read parameter from Logic if its register is readable
		//u32 *addr = baseaddr + ip_shaper_offset[i];
		//prm[i] = *( (volatile u32*)(addr) );
		prm[i] = ip_shaper_ReadReg(baseaddr, ip_shaper_offset[i]);
	}
}

//
// write to registers
//
void ip_shaper_WriteLogic(ip_shaper *shaper)
{
	u32 baseaddr;
	u32 *prm;
	int i, r;

	baseaddr = shaper->Config.BaseAddress;
	prm = shaper->Config.prm;
	
	for(i=0;i<IP_SHAPER_PRM_CNT;i++)
	{
		//write parameter to the Logic if its register is writable
		r = (ip_shaper_rw[i] & 2);
		if ( r != 2) continue;
		//u32 *addr = baseaddr + ip_shaper_offset[i];
		//*( (volatile u32*)(addr) ) = prm[i];
		ip_shaper_WriteReg(baseaddr, ip_shaper_offset[i], prm[i]);
	}
}
