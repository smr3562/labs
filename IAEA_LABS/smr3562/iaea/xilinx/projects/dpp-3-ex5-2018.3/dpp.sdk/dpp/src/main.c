/******************************************************************************
*
* Copyright (C) 2020 - 2021 IAEA.  All rights reserved.
*
******************************************************************************/


//xilinx declarations
#include "xil_types.h"
#include "xparameters.h"		//Contains the address and configurations of need for the system
#include "xstatus.h"			//Contains the status definitions used
#include "platform.h"			//Declarations for the MicroBlaze
#include <string.h>

//custom declarations
#include "define.h"

#include "uart.h"
#include "ip_scope.h"
#include "invert_and_offset.h"
#include "ip_mux16_2_if.h"
#include "ip_shaper.h"
#include "intc.h"
#include "registers.h"
#include "util.h"

#include "main.h"

int main()
   {
	InitSoPC();

	while(1)
	{
		DoHostTasks();
	}
	cleanup_platform();
	return 0;
   }

void DoHostTasks()
   {
   int cmdID;
   unsigned int uiReceivedCount;

   if(UartLiteReceiveCommand(XPAR_UARTLITE_0_DEVICE_ID, u8Command, &uiReceivedCount, MEMORY_RX_BUFFER_SIZE) == XST_SUCCESS)
      {
      cmdID = GetCommandId(u8Command, uiReceivedCount) + 1;
      switch (cmdID)
         {
         //"$SP": Set Filter parameters
         case 3:
            DoCommand003(u8Command, uiReceivedCount);
			break;
         //"$LS" Load Scope ( PS Scope -> Host; PS Scope is flushed automatically in interrupt mode)
         case 4:
            DoCommand004(u8Command, uiReceivedCount);
			break;
		 //"$RM": Read spectrum ( PS DRAM - host)
		 case 6:
			DoCommand006(u8Command, uiReceivedCount);
			break;
		 //"$AQ": AcQuisition start/stop
		 case 7:
			DoCommand007(u8Command, uiReceivedCount);
			break;
		 //"$WT": Write Timer register
		 case 8:
			DoCommand008(u8Command, uiReceivedCount);
			break;
		 //"$RT": Read Timer register
		 case 9:
			DoCommand009(u8Command, uiReceivedCount);
			break;
		 //"$CS": Clear Spectrum
		 case 10:
			DoCommand010(u8Command, uiReceivedCount);
			break;
		 //"$GP": Get parameters
		 case 11:
			DoCommand011(u8Command, uiReceivedCount);
			break;
		 //"$GR": Get ROI
		 case 12:
			DoCommand012(u8Command, uiReceivedCount);
			break;
		 //"$SR": Set ROI
		 case 13:
			DoCommand013(u8Command, uiReceivedCount);
			break;
         //"$VR": Read ROI values
         case 14:
            DoCommand014(u8Command, uiReceivedCount);
            break;
         case 15://$RL: get RealTimer/LiveTimer
        	DoCommand015(u8Command, uiReceivedCount);
        	break;
         default:
			SendError(ERROR_INVALID_COMMAND);
			break;
		 }
      }
   }

void InitSoPC()
   {
   ip_shaper_Config *ip_shaper_ConfigPtrSlow;

   init_platform();

   //**************************************************
   //	initialize UART controller
   //**************************************************
   UartLiteInit(XPAR_UARTLITE_0_DEVICE_ID);

   //**************************************************
   //	initialize interrupt controller
   //**************************************************
   IntcPSInit((u16)XPAR_INTC_0_DEVICE_ID);
   IntcEnableInterrupt(XPAR_INTC_0_DEVICE_ID);

   //**************************************************
   //	initialize IP Scope
   //**************************************************
   ip_scope_Initialize(&IpScope, XPAR_IP_SCOPE_0_DEVICE_ID);
   scope_SetDefaultUserParameters(IpScope.Config.prm);
   ip_scope_WriteLogic(&IpScope);

   //**************************************************
   //	initialize IP Shaper
   //**************************************************
   ip_shaper_ConfigPtrSlow = ip_shaper_LookupConfig(XPAR_DPP_0_PULSE_CONDITIONING_SLOW_IP_SHAPER_0_DEVICE_ID);
   ip_shaper_CfgInitialize(&IpShaperSlow, ip_shaper_ConfigPtrSlow);
   shaper_SetDefaultUserParametersSlow(IpShaperSlow.Config.prm);
   ip_shaper_WriteLogic(&IpShaperSlow);
   }

int GetCommandId(u8 *u8Command, unsigned int uiCommandSize)
   {
   int i;
   char szCmdId[4];

   for(i=0;i<3;i++) szCmdId[i] = u8Command[i];
   szCmdId[3]='\0';

   for(i=0;i<CMD_CNT;i++)
	  {
	  if(strcmp(szCmdId, m_szCmdId[i]) == 0)return (i);
	  }
   return -1;
   }


//
// CMD003: $SP = Set parameters
// command parameters: param_group_id, p1, p2, ... pn
//
void DoCommand003(u8 *u8Command, unsigned int uiCommandSize)
   {
   int iParam;
   char szParameter[32];
   u32 u32Data;
   if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0) goto failed;
   iParam = atoi32(szParameter);
   switch(iParam)
      {

	  case 1://IP_Shaper parameter group
	  {
	  u32 *prm = IpShaperSlow.Config.prm;
	  int j=2;
	  for(int i=0;i<IP_SHAPER_PRM_CNT;i++,j++)
			{
			if( GetCommandParameter(u8Command,uiCommandSize,j,szParameter) == 0) goto failed;
			prm[i] = atou32(szParameter);
			}
		 ip_shaper_WriteLogic(&IpShaperSlow);
		 //dependent parameters: IP PeakDetector: x_delay
		 break;
	  }
	  case 3://IP Scope parameter group
		 {
		 u32 *prm = IpScope.Config.prm;
		 int j=2;
		 for(int i=0;i<IP_SCOPE_PRM_CNT;i++,j++)
			{
			if( GetCommandParameter(u8Command,uiCommandSize,j,szParameter) == 0) goto failed;
			prm[i] = atou32(szParameter);
			}
		 ip_scope_WriteLogic(&IpScope);
		 break;
         }
	  case 6://scopemux
		 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(SCOPEMUX_REG0, u32Data);
		 break;
	  case 8://invert & offset
		 if( GetCommandParameter(u8Command,uiCommandSize,2,szParameter) == 0) goto failed;
		 u32Data = atou32(szParameter);
		 Xil_Out32(INVERT_OFFSET_REG0, u32Data);
		 break;
      default:
		  goto failed;
		  break;
      }
   u8Replay[0] = '!';
   u8Replay[1] = 'S';
   u8Replay[2] = 'P';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';
   UartLiteSendReply(XPAR_MB_PERIPH_0_AXI_UARTLITE_0_DEVICE_ID, u8Replay,5);

   return;
failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   }

//
// CMD004: $LS = Load data from scope (same as Read but using interrupt)
// command parameter: channel ID (integer value 1 or 2)
//
void DoCommand004(u8 *u8Command, unsigned int uiCommandSize)
   {
   if(IntcIsWaveformReady()==0)
	 {
	 //SendError(ERROR_SCOPE_DATA_NOT_READY);
	 //return;
	 }

   //header
   u8Replay[0] = '!';
   u8Replay[1] = 'L';
   u8Replay[2] = (u8) '\n';
   u8Replay[3] = (u8) '\r';

   //data
   u32 *data = (u32 *) (u8Replay + 4);
   ip_scope_ReadWaveform(&IpScope,data);

   UartLiteSendReply(XPAR_MB_PERIPH_0_AXI_UARTLITE_0_DEVICE_ID, u8Replay, MEMORY_TX_BUFFER_SIZE);

   //clear scope and enable interrupt
   ip_scope_WaveformAccepted(&IpScope);
   IntcEnableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);
   }

//
// CMD006: $RM = Get data from spectrum
//
void DoCommand006(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'R';
   u8Replay[2] = 'M';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD007: $AQ = Acquisition start/stop
//
void DoCommand007(u8 *u8Command, unsigned int uiCommandSize)
   {
	   char szParameter[32];
	   int iParam;

	   if( GetCommandParameter(u8Command,uiCommandSize,1,szParameter) == 0)
	      {
		  SendError(ERROR_INVALID_COMMAND_PARAMETER);
		  return;
		  }

	   iParam = atoi32(szParameter);
	   switch(iParam)
	      {
		  //stop acquisition
		  case 2:
			 //stop scope
			 ip_scope_Acq(&IpScope,0);
			 IntcDisableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);
			 break;

		  //start acquisition
		  case 1:
		     //start scope
			 ip_scope_Acq(&IpScope,1);
			 ip_scope_WaveformAccepted(&IpScope);
			 IntcEnableInterrupt( XPAR_PS_MB_0_AXI_INTC_0_DPP_0_SCOPE_IP_SCOPE_0_FULL_INTR);
			 break;

		  default:
			 goto failed;
			 break;
	      }

   u8Replay[0] = '!';
   u8Replay[1] = 'A';
   u8Replay[2] = 'Q';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   return;

failed:
   SendError(ERROR_INVALID_COMMAND_PARAMETER);
   }


//
// CMD008: Write to Timers register
// command parameter1: register ID (zero based index)
// command parameter2: register value
//
void DoCommand008(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'W';
   u8Replay[2] = 'T';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD009: $RT = read from a Timers' register
// command parameter1: register ID (zero based index or -1)
// command parameter2: register value
//
void DoCommand009(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'R';
   u8Replay[2] = 'T';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD010: $CS = Clear Spectrum, alternative $AQ 3
// command parameter1: register ID (zero based index)
// command parameter2: register value
//
void DoCommand010(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'C';
   u8Replay[2] = 'S';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD011: $GP = Get parameters
// command parameters: param_group_id, p1, p2, ... pn
//
void DoCommand011(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'G';
   u8Replay[2] = 'P';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }


//
// CMD012: $GR = Get ROIs
// no parameters, send 5 x 32-bit numbers
//
void DoCommand012(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'G';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD013: $SR = Set ROIs
//  no parameters, get 5 x 32-bit numbers
//
void DoCommand013(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'S';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//
// CMD014: $VR = Get Value from ROI FIFO
//  no parameters, get 5 x 32-bit numbers
//
void DoCommand014(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'V';
   u8Replay[2] = 'R';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }

//$RL. get real/live timer values
void DoCommand015(u8 *u8Command, unsigned int uiCommandSize)
   {
   u8Replay[0] = '!';
   u8Replay[1] = 'R';
   u8Replay[2] = 'L';
   u8Replay[3] = '\n';
   u8Replay[4] = '\r';

   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay,5);
   }



//
// Parses for command parameter
// if parameter is first then iParametrId = 1
// if parameter is second then iParameter = 2 etc
// on return the szParameter contains the command parameter as string (null terminated)
//
int GetCommandParameter(u8 *u8Command, unsigned int uiCommandSize, int iParameterId, char *szParameter)
   {
   char *c1,*c2,*cc;
   int i=0;
   char szCommand[MEMORY_RX_BUFFER_SIZE];

   memcpy(szCommand,u8Command,uiCommandSize);
   szCommand[uiCommandSize]='\0';

   cc = szCommand;

   //look for i-th occurrences of space character '' i=iParametrId
   while(1)
      {
	  i++;
	  c1 = strchr(cc,' ');
	  if(c1 == NULL) return 0;
	  cc = c1 + 1;
      if(i == iParameterId) break;
	  }

   //look for the next ' '
   c2 = strchr(cc,' ');
   if(c2)
	  {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }

   //look for the '\n'
   c2 = strchr(cc,'\n');
   if(c2)
	  {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }
   //look for the '\r'
   c2 = strchr(cc,'\r');
   if(c2)
      {
	  *c2 = '\0';
	  strcpy(szParameter,cc);
	  return 1;
	  }
   return 0;
   }

//
// assuming iError in the interval [0, 99]
//
void SendError(int iErrorId)
   {
   char c1,c2;

   c1 = (char) (0x30 + iErrorId/10);
   c2 = (char) (0x30 + iErrorId%10);

   u8Replay[0] = '!';
   u8Replay[1] = 'E';
   u8Replay[2] = 'R';
   u8Replay[3] = 'R';
   u8Replay[4] = 'O';
   u8Replay[5] = 'R';
   u8Replay[6] = ':';
   u8Replay[7] = c1;
   u8Replay[8] = c2;
   u8Replay[9] = '\n';
   u8Replay[10] = '\r';
   UartLiteSendReply(XPAR_UARTLITE_0_DEVICE_ID, u8Replay, 11);
   }


//
// hard-coded default values for the scope IP
//
void scope_SetDefaultUserParameters(u32 *prm)
   {
   prm[ 0]	= 20;		//Tclk: 			clock period time in nsec
   prm[ 1]	= 2048;		//bram_size:		scope size
   prm[ 2]	= 1638;		//(0.1,16,14);		threshold
   prm[ 3]	= 100;		//delay				delay in clocks
   prm[ 4]	= 1;		//enable			enable
   prm[ 5]  = 1;			//clear
   prm[ 6]  = 1;			//full
   }
//
// hard-coded default values
//
void shaper_SetDefaultUserParametersSlow (u32 *prm)
   {
   /*prm[ 0]	= 20;	//Tclk: 			clock period time in nsec
   prm[ 1]	= 5000;		//Taud:				decay time of exponential input signal in nsec
   prm[ 2]	= 3000;		//Taupk:			trapezoid filter: peaking time in nsec
   prm[ 3]	= 0;		//Taupk_top:		trapezoid flat top in nsec

   prm[ 4]	= 32768;	//(1.0,16,15);		gain at output of the filter: must be [0,2] and dc_block_sel = 1
   prm[ 5]	= 0;		//ch1_sel: 			select internal test-point signals for channel 1 [0=trapez,1=input,2=delta pulse, 3=rect]
   prm[ 6]	= 4;		//ch2_sel:			select for channel 2
   prm[ 7]	= 0;		//format_sel:		output format 1=binary offset, 0=2's complemenmt 16.14
   prm[ 8]	= 0;		//dc_block_sel:		remove DC signal [0=n0, 1=yes]
   prm[ 9]	= 1;		//inv_sel:			invert input [0=no, 1=yes]

   prm[10]	= 8355120; 	//b10: 				(exp(-Tclk/Taud,23,23);
   prm[11]	= 1747;		//na_inv			(Tclk/Taupk,18,18)
   prm[12] = 147;		//na				Taupk/Tclk
   prm[13] = 147;		//nb				(Taupk+taupk_top)/Tclk

   prm[14] = 167772;	//(0.020,23,23);	dc_clip in mV
   prm[15] = 9585;		//(0.893,14,13);	dc_offset*/

   prm[ 0]	= 20;		//Tclk: 			clock period time in nsec
   prm[ 1]	= 100;		//Taur:				decay time of exponential input signal in nsec
   prm[ 2]	= 420;		//Taud:				decay time of exponential input signal in nsec
   prm[ 3]	= 1000;		//Taupk:			trapezoid filter: peaking time in nsec
   prm[ 4]	= 100;		//Taupk_top:		trapezoid flat top in nsec
   prm[ 5]	= 33554432;	//gain;				gain at output of the filter
   prm[ 6]	= 16119371; //b10: 				(exp(-Tclk/Taud,24,24);
   prm[ 7]  = 671088;	//na_inv			(Tclk/Taupk,26,25)
   prm[ 8]  = 47;		//na				Taupk/Tclk (10.0)
   prm[ 9]  = 52;		//nb				(Taupk+taupk_top)/Tclk (10.0)
   prm[10]  = 60572317;	//U(1,32,24);		b00
   prm[11]  = 13736022;	//U(0,24,24);		b20
   }

