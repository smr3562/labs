#ifndef IP_SHAPER__H
#define IP_SHAPER__H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#include "ip_shaper_hw.h"

/**************************** Type Definitions ******************************/
	
#define IP_SHAPER_PRM_CNT 					12
#define IP_SHAPER_PRM_INDEX_TCLK			0
#define IP_SHAPER_PRM_INDEX_TAUR			1
#define IP_SHAPER_PRM_INDEX_TAUD			2
#define IP_SHAPER_PRM_INDEX_TAUPK			3
#define IP_SHAPER_PRM_INDEX_TAUPK_TOP		4
#define IP_SHAPER_PRM_INDEX_GAIN			5

#define IP_SHAPER_PRM_INDEX_B10				6
#define IP_SHAPER_PRM_INDEX_NA_INV			7
#define IP_SHAPER_PRM_INDEX_NA				8
#define IP_SHAPER_PRM_INDEX_NB				9
#define IP_SHAPER_PRM_INDEX_B00				10
#define IP_SHAPER_PRM_INDEX_B20				11


typedef struct 
{
    u16 DeviceId;
    u32 BaseAddress;
	
//***************************************************
// user defined parameters
//***************************************************
//	prm[0]		Tclk;
//	prm[1]		Taur;
//	prm[2]		Taud;
//	prm[3]		Taupk;
//	prm[4]		Taupk_top;
//	prm[5]		gain;
	
//	prm[6]		b10;
//	prm[7]		na_inv;
//	prm[8]		na;
//	prm[9]		nb;
//	prm[10]		b00;
//	prm[11]		b20;
	u32 prm[IP_SHAPER_PRM_CNT];

} ip_shaper_Config;
/**
* The ip_shaper driver instance data. The user is required to
* allocate a variable of this type for every ip_shaper device in the system.
* A pointer to a variable of this type is then passed to the driver
* API functions.
*/
typedef struct {
	ip_shaper_Config Config;	
    u32 IsReady;
} ip_shaper;

/***************** Macros (Inline Functions) Definitions *********************/

#define ip_shaper_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define ip_shaper_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes *****************************/

int ip_shaper_Initialize(ip_shaper *InstancePtr, u16 DeviceId);
ip_shaper_Config* ip_shaper_LookupConfig(u16 DeviceId);
int ip_shaper_CfgInitialize(ip_shaper *InstancePtr, ip_shaper_Config *ConfigPtr);


void ip_shaper_ReadLogic(ip_shaper *shaper);
void ip_shaper_WriteLogic(ip_shaper *shaper);

#ifdef __cplusplus
}
#endif

#endif
