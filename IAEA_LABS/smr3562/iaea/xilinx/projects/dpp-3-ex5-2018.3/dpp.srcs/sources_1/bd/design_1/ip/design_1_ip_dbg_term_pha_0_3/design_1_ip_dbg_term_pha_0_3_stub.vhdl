-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Mon Feb 15 11:27:35 2021
-- Host        : ZBOOK running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               D:/dpp/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_term_pha_0_3/design_1_ip_dbg_term_pha_0_3_stub.vhdl
-- Design      : design_1_ip_dbg_term_pha_0_3
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_ip_dbg_term_pha_0_3 is
  Port ( 
    peak_amp_rdy_fast_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rejectn_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy_slow_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_det_signal_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end design_1_ip_dbg_term_pha_0_3;

architecture stub of design_1_ip_dbg_term_pha_0_3 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "peak_amp_rdy_fast_out[15:0],rejectn_out[15:0],peak_amp_rdy_slow_out[15:0],peak_det_signal_out[15:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "top,Vivado 2018.3";
begin
end;
