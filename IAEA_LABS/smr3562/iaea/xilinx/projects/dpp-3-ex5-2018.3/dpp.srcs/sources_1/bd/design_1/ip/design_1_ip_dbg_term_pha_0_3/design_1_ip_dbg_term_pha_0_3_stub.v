// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Mon Feb 15 11:27:35 2021
// Host        : ZBOOK running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/dpp/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_term_pha_0_3/design_1_ip_dbg_term_pha_0_3_stub.v
// Design      : design_1_ip_dbg_term_pha_0_3
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "top,Vivado 2018.3" *)
module design_1_ip_dbg_term_pha_0_3(peak_amp_rdy_fast_out, rejectn_out, 
  peak_amp_rdy_slow_out, peak_det_signal_out)
/* synthesis syn_black_box black_box_pad_pin="peak_amp_rdy_fast_out[15:0],rejectn_out[15:0],peak_amp_rdy_slow_out[15:0],peak_det_signal_out[15:0]" */;
  output [15:0]peak_amp_rdy_fast_out;
  output [15:0]rejectn_out;
  output [15:0]peak_amp_rdy_slow_out;
  output [15:0]peak_det_signal_out;
endmodule
