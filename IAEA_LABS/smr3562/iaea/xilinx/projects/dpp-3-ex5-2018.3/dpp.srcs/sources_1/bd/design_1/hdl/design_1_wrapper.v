//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
//Date        : Mon Feb 15 14:36:43 2021
//Host        : ZBOOK running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (adc_clk,
    adc_data,
    reset,
    sys_clock,
    uart_rtl_rxd,
    uart_rtl_txd);
  output adc_clk;
  input [13:0]adc_data;
  input reset;
  input sys_clock;
  input uart_rtl_rxd;
  output uart_rtl_txd;

  wire adc_clk;
  wire [13:0]adc_data;
  wire reset;
  wire sys_clock;
  wire uart_rtl_rxd;
  wire uart_rtl_txd;

  design_1 design_1_i
       (.adc_clk(adc_clk),
        .adc_data(adc_data),
        .reset(reset),
        .sys_clock(sys_clock),
        .uart_rtl_rxd(uart_rtl_rxd),
        .uart_rtl_txd(uart_rtl_txd));
endmodule
