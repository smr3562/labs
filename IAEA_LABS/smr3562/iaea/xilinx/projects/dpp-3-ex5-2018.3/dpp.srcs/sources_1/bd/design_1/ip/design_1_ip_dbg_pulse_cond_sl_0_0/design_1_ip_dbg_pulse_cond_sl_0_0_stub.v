// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Mon Feb 15 14:38:11 2021
// Host        : ZBOOK running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               d:/dpp/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_pulse_cond_sl_0_0/design_1_ip_dbg_pulse_cond_sl_0_0_stub.v
// Design      : design_1_ip_dbg_pulse_cond_sl_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "top,Vivado 2018.3" *)
module design_1_ip_dbg_pulse_cond_sl_0_0(impulse, rect, shaper, blr, dc_stab, dc_stab_acc, 
  impulse_out, rect_out, shaper_out, blr_out, dc_stab_out, dc_stab_acc_out)
/* synthesis syn_black_box black_box_pad_pin="impulse[15:0],rect[15:0],shaper[15:0],blr[15:0],dc_stab[15:0],dc_stab_acc[15:0],impulse_out[15:0],rect_out[15:0],shaper_out[15:0],blr_out[15:0],dc_stab_out[15:0],dc_stab_acc_out[15:0]" */;
  input [15:0]impulse;
  input [15:0]rect;
  input [15:0]shaper;
  input [15:0]blr;
  input [15:0]dc_stab;
  input [15:0]dc_stab_acc;
  output [15:0]impulse_out;
  output [15:0]rect_out;
  output [15:0]shaper_out;
  output [15:0]blr_out;
  output [15:0]dc_stab_out;
  output [15:0]dc_stab_acc_out;
endmodule
