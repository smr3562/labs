// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Mon Feb 15 14:38:11 2021
// Host        : ZBOOK running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/dpp/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_pulse_cond_sl_0_0/design_1_ip_dbg_pulse_cond_sl_0_0_sim_netlist.v
// Design      : design_1_ip_dbg_pulse_cond_sl_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ip_dbg_pulse_cond_sl_0_0,top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "top,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module design_1_ip_dbg_pulse_cond_sl_0_0
   (impulse,
    rect,
    shaper,
    blr,
    dc_stab,
    dc_stab_acc,
    impulse_out,
    rect_out,
    shaper_out,
    blr_out,
    dc_stab_out,
    dc_stab_acc_out);
  input [15:0]impulse;
  input [15:0]rect;
  input [15:0]shaper;
  input [15:0]blr;
  input [15:0]dc_stab;
  input [15:0]dc_stab_acc;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 impulse" *) output [15:0]impulse_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 rect" *) output [15:0]rect_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 shaper" *) output [15:0]shaper_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 blr" *) output [15:0]blr_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 dc_stab" *) output [15:0]dc_stab_out;
  (* x_interface_info = "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_m0 dc_stab_acc" *) output [15:0]dc_stab_acc_out;

  wire [15:0]blr;
  wire [15:0]dc_stab;
  wire [15:0]dc_stab_acc;
  wire [15:0]impulse;
  wire [15:0]rect;
  wire [15:0]shaper;

  assign blr_out[15:0] = blr;
  assign dc_stab_acc_out[15:0] = dc_stab_acc;
  assign dc_stab_out[15:0] = dc_stab;
  assign impulse_out[15:0] = impulse;
  assign rect_out[15:0] = rect;
  assign shaper_out[15:0] = shaper;
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
