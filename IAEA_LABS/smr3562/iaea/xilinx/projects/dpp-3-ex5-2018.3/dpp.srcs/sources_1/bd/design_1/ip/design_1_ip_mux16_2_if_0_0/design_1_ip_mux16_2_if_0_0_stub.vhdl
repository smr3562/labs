-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Thu Feb  4 23:54:48 2021
-- Host        : ZBOOK running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               e:/smr3562/iaea/xilinx/projects/dpp/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_mux16_2_if_0_0/design_1_ip_mux16_2_if_0_0_stub.vhdl
-- Design      : design_1_ip_mux16_2_if_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_ip_mux16_2_if_0_0 is
  Port ( 
    inp1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp4 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp5 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp6 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp7 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp8 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp9 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp10 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp11 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp12 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp13 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp14 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp15 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp16 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    outp1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    outp2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    trig : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );

end design_1_ip_mux16_2_if_0_0;

architecture stub of design_1_ip_mux16_2_if_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "inp1[15:0],inp2[15:0],inp3[15:0],inp4[15:0],inp5[15:0],inp6[15:0],inp7[15:0],inp8[15:0],inp9[15:0],inp10[15:0],inp11[15:0],inp12[15:0],inp13[15:0],inp14[15:0],inp15[15:0],inp16[15:0],outp1[15:0],outp2[15:0],trig[15:0],s00_axi_awaddr[3:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[3:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,s00_axi_aclk,s00_axi_aresetn";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "ip_mux16_2_v1_0,Vivado 2018.3";
begin
end;
