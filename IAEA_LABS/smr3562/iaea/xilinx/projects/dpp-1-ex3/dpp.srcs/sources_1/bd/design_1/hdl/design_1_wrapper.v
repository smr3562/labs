//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Sat Feb 13 15:30:58 2021
//Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (adc_clk,
    reset,
    sys_clock,
    uart_rtl_rxd,
    uart_rtl_txd);
  output adc_clk;
  input reset;
  input sys_clock;
  input uart_rtl_rxd;
  output uart_rtl_txd;

  wire adc_clk;
  wire reset;
  wire sys_clock;
  wire uart_rtl_rxd;
  wire uart_rtl_txd;

  design_1 design_1_i
       (.adc_clk(adc_clk),
        .reset(reset),
        .sys_clock(sys_clock),
        .uart_rtl_rxd(uart_rtl_rxd),
        .uart_rtl_txd(uart_rtl_txd));
endmodule
