-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Feb 13 17:51:14 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/smr3562/iaea/xilinx/projects/dpp-2/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_mux16_2_if_0_0/design_1_ip_mux16_2_if_0_0_sim_netlist.vhdl
-- Design      : design_1_ip_mux16_2_if_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0_S00_AXI is
  port (
    S_AXI_ARREADY : out STD_LOGIC;
    axi_rvalid_reg_0 : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    trig : out STD_LOGIC_VECTOR ( 15 downto 0 );
    outp2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    inp4 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp8 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp7 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp6 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp5 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp12 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp11 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp10 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp9 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp16 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp15 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp14 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp13 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0_S00_AXI : entity is "ip_mux16_2_v1_0_S00_AXI";
end design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0_S00_AXI;

architecture STRUCTURE of design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0_S00_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[31]_i_1_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^axi_rvalid_reg_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \outp2[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[0]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[0]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[0]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[10]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[10]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[10]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[10]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[10]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[10]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[11]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[11]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[11]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[11]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[11]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[11]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[12]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[12]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[12]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[12]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[12]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[12]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[13]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[13]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[13]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[13]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[13]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[13]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[14]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[14]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[14]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[14]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[14]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[14]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[15]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[15]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[15]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[15]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[15]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[15]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[1]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[1]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[1]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[1]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[1]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[2]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[2]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[2]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[2]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[2]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[3]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[3]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[3]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[3]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[4]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[4]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[4]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[4]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[4]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[4]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[5]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[5]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[5]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[5]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[5]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[5]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[6]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[6]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[6]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[6]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[6]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[6]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[7]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[7]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[7]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[7]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[7]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[7]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[8]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[8]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[8]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[8]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[8]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[8]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \outp2[9]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \outp2[9]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \outp2[9]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \outp2[9]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \outp2[9]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \outp2[9]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_0_in_0 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 3 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \slv_reg0_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[9]\ : STD_LOGIC;
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  signal \trig[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[0]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[0]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[0]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[10]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[10]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[10]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[10]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[10]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[10]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[11]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[11]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[11]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[11]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[11]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[11]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[12]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[12]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[12]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[12]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[12]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[12]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[13]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[13]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[13]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[13]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[13]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[13]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[14]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[14]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[14]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[14]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[14]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[14]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[15]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[15]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[15]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[15]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[15]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[15]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[1]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[1]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[1]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[1]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[1]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[2]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[2]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[2]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[2]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[2]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[3]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[3]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[3]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[3]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[4]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[4]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[4]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[4]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[4]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[4]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[5]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[5]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[5]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[5]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[5]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[5]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[6]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[6]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[6]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[6]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[6]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[6]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[7]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[7]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[7]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[7]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[7]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[7]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[8]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[8]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[8]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[8]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[8]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[8]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \trig[9]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \trig[9]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \trig[9]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \trig[9]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \trig[9]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \trig[9]_INST_0_i_6_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair0";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  axi_rvalid_reg_0 <= \^axi_rvalid_reg_0\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBF00BF00BF00"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => s00_axi_awvalid,
      I2 => s00_axi_wvalid,
      I3 => aw_en_reg_n_0,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => p_0_in
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => p_0_in_0(0),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => p_0_in_0(1),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => p_0_in_0(0),
      S => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => p_0_in_0(1),
      S => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => p_0_in
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => axi_awaddr(2),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => axi_awaddr(3),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => axi_awaddr(3),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => p_0_in
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => p_0_in
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000E000FFFFFFFF"
    )
        port map (
      I0 => p_0_in_0(0),
      I1 => p_0_in_0(1),
      I2 => \^s_axi_arready\,
      I3 => s00_axi_arvalid,
      I4 => \^axi_rvalid_reg_0\,
      I5 => s00_axi_aresetn,
      O => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^axi_rvalid_reg_0\,
      O => slv_reg_rden
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[0]\,
      Q => s00_axi_rdata(0),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[10]\,
      Q => s00_axi_rdata(10),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[11]\,
      Q => s00_axi_rdata(11),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[12]\,
      Q => s00_axi_rdata(12),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[13]\,
      Q => s00_axi_rdata(13),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[14]\,
      Q => s00_axi_rdata(14),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[15]\,
      Q => s00_axi_rdata(15),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[16]\,
      Q => s00_axi_rdata(16),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[17]\,
      Q => s00_axi_rdata(17),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[18]\,
      Q => s00_axi_rdata(18),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[19]\,
      Q => s00_axi_rdata(19),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[1]\,
      Q => s00_axi_rdata(1),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[20]\,
      Q => s00_axi_rdata(20),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[21]\,
      Q => s00_axi_rdata(21),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[22]\,
      Q => s00_axi_rdata(22),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[23]\,
      Q => s00_axi_rdata(23),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[24]\,
      Q => s00_axi_rdata(24),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[25]\,
      Q => s00_axi_rdata(25),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[26]\,
      Q => s00_axi_rdata(26),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[27]\,
      Q => s00_axi_rdata(27),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[28]\,
      Q => s00_axi_rdata(28),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[29]\,
      Q => s00_axi_rdata(29),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[2]\,
      Q => s00_axi_rdata(2),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[30]\,
      Q => s00_axi_rdata(30),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[31]\,
      Q => s00_axi_rdata(31),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[3]\,
      Q => s00_axi_rdata(3),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => sel0(0),
      Q => s00_axi_rdata(4),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => sel0(1),
      Q => s00_axi_rdata(5),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => sel0(2),
      Q => s00_axi_rdata(6),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => sel0(3),
      Q => s00_axi_rdata(7),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[8]\,
      Q => s00_axi_rdata(8),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \slv_reg0_reg_n_0_[9]\,
      Q => s00_axi_rdata(9),
      R => \axi_rdata[31]_i_1_n_0\
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^axi_rvalid_reg_0\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^axi_rvalid_reg_0\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => p_0_in
    );
\outp2[0]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[0]_INST_0_i_1_n_0\,
      I1 => \outp2[0]_INST_0_i_2_n_0\,
      O => outp2(0),
      S => sel0(3)
    );
\outp2[0]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[0]_INST_0_i_3_n_0\,
      I1 => \outp2[0]_INST_0_i_4_n_0\,
      O => \outp2[0]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[0]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[0]_INST_0_i_5_n_0\,
      I1 => \outp2[0]_INST_0_i_6_n_0\,
      O => \outp2[0]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[0]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(0),
      I1 => inp3(0),
      I2 => sel0(1),
      I3 => inp2(0),
      I4 => sel0(0),
      I5 => inp1(0),
      O => \outp2[0]_INST_0_i_3_n_0\
    );
\outp2[0]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(0),
      I1 => inp7(0),
      I2 => sel0(1),
      I3 => inp6(0),
      I4 => sel0(0),
      I5 => inp5(0),
      O => \outp2[0]_INST_0_i_4_n_0\
    );
\outp2[0]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(0),
      I1 => inp11(0),
      I2 => sel0(1),
      I3 => inp10(0),
      I4 => sel0(0),
      I5 => inp9(0),
      O => \outp2[0]_INST_0_i_5_n_0\
    );
\outp2[0]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(0),
      I1 => inp15(0),
      I2 => sel0(1),
      I3 => inp14(0),
      I4 => sel0(0),
      I5 => inp13(0),
      O => \outp2[0]_INST_0_i_6_n_0\
    );
\outp2[10]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[10]_INST_0_i_1_n_0\,
      I1 => \outp2[10]_INST_0_i_2_n_0\,
      O => outp2(10),
      S => sel0(3)
    );
\outp2[10]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[10]_INST_0_i_3_n_0\,
      I1 => \outp2[10]_INST_0_i_4_n_0\,
      O => \outp2[10]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[10]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[10]_INST_0_i_5_n_0\,
      I1 => \outp2[10]_INST_0_i_6_n_0\,
      O => \outp2[10]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[10]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(10),
      I1 => inp3(10),
      I2 => sel0(1),
      I3 => inp2(10),
      I4 => sel0(0),
      I5 => inp1(10),
      O => \outp2[10]_INST_0_i_3_n_0\
    );
\outp2[10]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(10),
      I1 => inp7(10),
      I2 => sel0(1),
      I3 => inp6(10),
      I4 => sel0(0),
      I5 => inp5(10),
      O => \outp2[10]_INST_0_i_4_n_0\
    );
\outp2[10]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(10),
      I1 => inp11(10),
      I2 => sel0(1),
      I3 => inp10(10),
      I4 => sel0(0),
      I5 => inp9(10),
      O => \outp2[10]_INST_0_i_5_n_0\
    );
\outp2[10]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(10),
      I1 => inp15(10),
      I2 => sel0(1),
      I3 => inp14(10),
      I4 => sel0(0),
      I5 => inp13(10),
      O => \outp2[10]_INST_0_i_6_n_0\
    );
\outp2[11]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[11]_INST_0_i_1_n_0\,
      I1 => \outp2[11]_INST_0_i_2_n_0\,
      O => outp2(11),
      S => sel0(3)
    );
\outp2[11]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[11]_INST_0_i_3_n_0\,
      I1 => \outp2[11]_INST_0_i_4_n_0\,
      O => \outp2[11]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[11]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[11]_INST_0_i_5_n_0\,
      I1 => \outp2[11]_INST_0_i_6_n_0\,
      O => \outp2[11]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[11]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(11),
      I1 => inp3(11),
      I2 => sel0(1),
      I3 => inp2(11),
      I4 => sel0(0),
      I5 => inp1(11),
      O => \outp2[11]_INST_0_i_3_n_0\
    );
\outp2[11]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(11),
      I1 => inp7(11),
      I2 => sel0(1),
      I3 => inp6(11),
      I4 => sel0(0),
      I5 => inp5(11),
      O => \outp2[11]_INST_0_i_4_n_0\
    );
\outp2[11]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(11),
      I1 => inp11(11),
      I2 => sel0(1),
      I3 => inp10(11),
      I4 => sel0(0),
      I5 => inp9(11),
      O => \outp2[11]_INST_0_i_5_n_0\
    );
\outp2[11]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(11),
      I1 => inp15(11),
      I2 => sel0(1),
      I3 => inp14(11),
      I4 => sel0(0),
      I5 => inp13(11),
      O => \outp2[11]_INST_0_i_6_n_0\
    );
\outp2[12]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[12]_INST_0_i_1_n_0\,
      I1 => \outp2[12]_INST_0_i_2_n_0\,
      O => outp2(12),
      S => sel0(3)
    );
\outp2[12]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[12]_INST_0_i_3_n_0\,
      I1 => \outp2[12]_INST_0_i_4_n_0\,
      O => \outp2[12]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[12]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[12]_INST_0_i_5_n_0\,
      I1 => \outp2[12]_INST_0_i_6_n_0\,
      O => \outp2[12]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[12]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(12),
      I1 => inp3(12),
      I2 => sel0(1),
      I3 => inp2(12),
      I4 => sel0(0),
      I5 => inp1(12),
      O => \outp2[12]_INST_0_i_3_n_0\
    );
\outp2[12]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(12),
      I1 => inp7(12),
      I2 => sel0(1),
      I3 => inp6(12),
      I4 => sel0(0),
      I5 => inp5(12),
      O => \outp2[12]_INST_0_i_4_n_0\
    );
\outp2[12]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(12),
      I1 => inp11(12),
      I2 => sel0(1),
      I3 => inp10(12),
      I4 => sel0(0),
      I5 => inp9(12),
      O => \outp2[12]_INST_0_i_5_n_0\
    );
\outp2[12]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(12),
      I1 => inp15(12),
      I2 => sel0(1),
      I3 => inp14(12),
      I4 => sel0(0),
      I5 => inp13(12),
      O => \outp2[12]_INST_0_i_6_n_0\
    );
\outp2[13]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[13]_INST_0_i_1_n_0\,
      I1 => \outp2[13]_INST_0_i_2_n_0\,
      O => outp2(13),
      S => sel0(3)
    );
\outp2[13]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[13]_INST_0_i_3_n_0\,
      I1 => \outp2[13]_INST_0_i_4_n_0\,
      O => \outp2[13]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[13]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[13]_INST_0_i_5_n_0\,
      I1 => \outp2[13]_INST_0_i_6_n_0\,
      O => \outp2[13]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[13]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(13),
      I1 => inp3(13),
      I2 => sel0(1),
      I3 => inp2(13),
      I4 => sel0(0),
      I5 => inp1(13),
      O => \outp2[13]_INST_0_i_3_n_0\
    );
\outp2[13]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(13),
      I1 => inp7(13),
      I2 => sel0(1),
      I3 => inp6(13),
      I4 => sel0(0),
      I5 => inp5(13),
      O => \outp2[13]_INST_0_i_4_n_0\
    );
\outp2[13]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(13),
      I1 => inp11(13),
      I2 => sel0(1),
      I3 => inp10(13),
      I4 => sel0(0),
      I5 => inp9(13),
      O => \outp2[13]_INST_0_i_5_n_0\
    );
\outp2[13]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(13),
      I1 => inp15(13),
      I2 => sel0(1),
      I3 => inp14(13),
      I4 => sel0(0),
      I5 => inp13(13),
      O => \outp2[13]_INST_0_i_6_n_0\
    );
\outp2[14]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[14]_INST_0_i_1_n_0\,
      I1 => \outp2[14]_INST_0_i_2_n_0\,
      O => outp2(14),
      S => sel0(3)
    );
\outp2[14]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[14]_INST_0_i_3_n_0\,
      I1 => \outp2[14]_INST_0_i_4_n_0\,
      O => \outp2[14]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[14]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[14]_INST_0_i_5_n_0\,
      I1 => \outp2[14]_INST_0_i_6_n_0\,
      O => \outp2[14]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[14]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(14),
      I1 => inp3(14),
      I2 => sel0(1),
      I3 => inp2(14),
      I4 => sel0(0),
      I5 => inp1(14),
      O => \outp2[14]_INST_0_i_3_n_0\
    );
\outp2[14]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(14),
      I1 => inp7(14),
      I2 => sel0(1),
      I3 => inp6(14),
      I4 => sel0(0),
      I5 => inp5(14),
      O => \outp2[14]_INST_0_i_4_n_0\
    );
\outp2[14]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(14),
      I1 => inp11(14),
      I2 => sel0(1),
      I3 => inp10(14),
      I4 => sel0(0),
      I5 => inp9(14),
      O => \outp2[14]_INST_0_i_5_n_0\
    );
\outp2[14]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(14),
      I1 => inp15(14),
      I2 => sel0(1),
      I3 => inp14(14),
      I4 => sel0(0),
      I5 => inp13(14),
      O => \outp2[14]_INST_0_i_6_n_0\
    );
\outp2[15]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[15]_INST_0_i_1_n_0\,
      I1 => \outp2[15]_INST_0_i_2_n_0\,
      O => outp2(15),
      S => sel0(3)
    );
\outp2[15]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[15]_INST_0_i_3_n_0\,
      I1 => \outp2[15]_INST_0_i_4_n_0\,
      O => \outp2[15]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[15]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[15]_INST_0_i_5_n_0\,
      I1 => \outp2[15]_INST_0_i_6_n_0\,
      O => \outp2[15]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[15]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(15),
      I1 => inp3(15),
      I2 => sel0(1),
      I3 => inp2(15),
      I4 => sel0(0),
      I5 => inp1(15),
      O => \outp2[15]_INST_0_i_3_n_0\
    );
\outp2[15]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(15),
      I1 => inp7(15),
      I2 => sel0(1),
      I3 => inp6(15),
      I4 => sel0(0),
      I5 => inp5(15),
      O => \outp2[15]_INST_0_i_4_n_0\
    );
\outp2[15]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(15),
      I1 => inp11(15),
      I2 => sel0(1),
      I3 => inp10(15),
      I4 => sel0(0),
      I5 => inp9(15),
      O => \outp2[15]_INST_0_i_5_n_0\
    );
\outp2[15]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(15),
      I1 => inp15(15),
      I2 => sel0(1),
      I3 => inp14(15),
      I4 => sel0(0),
      I5 => inp13(15),
      O => \outp2[15]_INST_0_i_6_n_0\
    );
\outp2[1]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[1]_INST_0_i_1_n_0\,
      I1 => \outp2[1]_INST_0_i_2_n_0\,
      O => outp2(1),
      S => sel0(3)
    );
\outp2[1]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[1]_INST_0_i_3_n_0\,
      I1 => \outp2[1]_INST_0_i_4_n_0\,
      O => \outp2[1]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[1]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[1]_INST_0_i_5_n_0\,
      I1 => \outp2[1]_INST_0_i_6_n_0\,
      O => \outp2[1]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[1]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(1),
      I1 => inp3(1),
      I2 => sel0(1),
      I3 => inp2(1),
      I4 => sel0(0),
      I5 => inp1(1),
      O => \outp2[1]_INST_0_i_3_n_0\
    );
\outp2[1]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(1),
      I1 => inp7(1),
      I2 => sel0(1),
      I3 => inp6(1),
      I4 => sel0(0),
      I5 => inp5(1),
      O => \outp2[1]_INST_0_i_4_n_0\
    );
\outp2[1]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(1),
      I1 => inp11(1),
      I2 => sel0(1),
      I3 => inp10(1),
      I4 => sel0(0),
      I5 => inp9(1),
      O => \outp2[1]_INST_0_i_5_n_0\
    );
\outp2[1]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(1),
      I1 => inp15(1),
      I2 => sel0(1),
      I3 => inp14(1),
      I4 => sel0(0),
      I5 => inp13(1),
      O => \outp2[1]_INST_0_i_6_n_0\
    );
\outp2[2]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[2]_INST_0_i_1_n_0\,
      I1 => \outp2[2]_INST_0_i_2_n_0\,
      O => outp2(2),
      S => sel0(3)
    );
\outp2[2]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[2]_INST_0_i_3_n_0\,
      I1 => \outp2[2]_INST_0_i_4_n_0\,
      O => \outp2[2]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[2]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[2]_INST_0_i_5_n_0\,
      I1 => \outp2[2]_INST_0_i_6_n_0\,
      O => \outp2[2]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[2]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(2),
      I1 => inp3(2),
      I2 => sel0(1),
      I3 => inp2(2),
      I4 => sel0(0),
      I5 => inp1(2),
      O => \outp2[2]_INST_0_i_3_n_0\
    );
\outp2[2]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(2),
      I1 => inp7(2),
      I2 => sel0(1),
      I3 => inp6(2),
      I4 => sel0(0),
      I5 => inp5(2),
      O => \outp2[2]_INST_0_i_4_n_0\
    );
\outp2[2]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(2),
      I1 => inp11(2),
      I2 => sel0(1),
      I3 => inp10(2),
      I4 => sel0(0),
      I5 => inp9(2),
      O => \outp2[2]_INST_0_i_5_n_0\
    );
\outp2[2]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(2),
      I1 => inp15(2),
      I2 => sel0(1),
      I3 => inp14(2),
      I4 => sel0(0),
      I5 => inp13(2),
      O => \outp2[2]_INST_0_i_6_n_0\
    );
\outp2[3]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[3]_INST_0_i_1_n_0\,
      I1 => \outp2[3]_INST_0_i_2_n_0\,
      O => outp2(3),
      S => sel0(3)
    );
\outp2[3]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[3]_INST_0_i_3_n_0\,
      I1 => \outp2[3]_INST_0_i_4_n_0\,
      O => \outp2[3]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[3]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[3]_INST_0_i_5_n_0\,
      I1 => \outp2[3]_INST_0_i_6_n_0\,
      O => \outp2[3]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(3),
      I1 => inp3(3),
      I2 => sel0(1),
      I3 => inp2(3),
      I4 => sel0(0),
      I5 => inp1(3),
      O => \outp2[3]_INST_0_i_3_n_0\
    );
\outp2[3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(3),
      I1 => inp7(3),
      I2 => sel0(1),
      I3 => inp6(3),
      I4 => sel0(0),
      I5 => inp5(3),
      O => \outp2[3]_INST_0_i_4_n_0\
    );
\outp2[3]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(3),
      I1 => inp11(3),
      I2 => sel0(1),
      I3 => inp10(3),
      I4 => sel0(0),
      I5 => inp9(3),
      O => \outp2[3]_INST_0_i_5_n_0\
    );
\outp2[3]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(3),
      I1 => inp15(3),
      I2 => sel0(1),
      I3 => inp14(3),
      I4 => sel0(0),
      I5 => inp13(3),
      O => \outp2[3]_INST_0_i_6_n_0\
    );
\outp2[4]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[4]_INST_0_i_1_n_0\,
      I1 => \outp2[4]_INST_0_i_2_n_0\,
      O => outp2(4),
      S => sel0(3)
    );
\outp2[4]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[4]_INST_0_i_3_n_0\,
      I1 => \outp2[4]_INST_0_i_4_n_0\,
      O => \outp2[4]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[4]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[4]_INST_0_i_5_n_0\,
      I1 => \outp2[4]_INST_0_i_6_n_0\,
      O => \outp2[4]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[4]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(4),
      I1 => inp3(4),
      I2 => sel0(1),
      I3 => inp2(4),
      I4 => sel0(0),
      I5 => inp1(4),
      O => \outp2[4]_INST_0_i_3_n_0\
    );
\outp2[4]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(4),
      I1 => inp7(4),
      I2 => sel0(1),
      I3 => inp6(4),
      I4 => sel0(0),
      I5 => inp5(4),
      O => \outp2[4]_INST_0_i_4_n_0\
    );
\outp2[4]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(4),
      I1 => inp11(4),
      I2 => sel0(1),
      I3 => inp10(4),
      I4 => sel0(0),
      I5 => inp9(4),
      O => \outp2[4]_INST_0_i_5_n_0\
    );
\outp2[4]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(4),
      I1 => inp15(4),
      I2 => sel0(1),
      I3 => inp14(4),
      I4 => sel0(0),
      I5 => inp13(4),
      O => \outp2[4]_INST_0_i_6_n_0\
    );
\outp2[5]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[5]_INST_0_i_1_n_0\,
      I1 => \outp2[5]_INST_0_i_2_n_0\,
      O => outp2(5),
      S => sel0(3)
    );
\outp2[5]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[5]_INST_0_i_3_n_0\,
      I1 => \outp2[5]_INST_0_i_4_n_0\,
      O => \outp2[5]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[5]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[5]_INST_0_i_5_n_0\,
      I1 => \outp2[5]_INST_0_i_6_n_0\,
      O => \outp2[5]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[5]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(5),
      I1 => inp3(5),
      I2 => sel0(1),
      I3 => inp2(5),
      I4 => sel0(0),
      I5 => inp1(5),
      O => \outp2[5]_INST_0_i_3_n_0\
    );
\outp2[5]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(5),
      I1 => inp7(5),
      I2 => sel0(1),
      I3 => inp6(5),
      I4 => sel0(0),
      I5 => inp5(5),
      O => \outp2[5]_INST_0_i_4_n_0\
    );
\outp2[5]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(5),
      I1 => inp11(5),
      I2 => sel0(1),
      I3 => inp10(5),
      I4 => sel0(0),
      I5 => inp9(5),
      O => \outp2[5]_INST_0_i_5_n_0\
    );
\outp2[5]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(5),
      I1 => inp15(5),
      I2 => sel0(1),
      I3 => inp14(5),
      I4 => sel0(0),
      I5 => inp13(5),
      O => \outp2[5]_INST_0_i_6_n_0\
    );
\outp2[6]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[6]_INST_0_i_1_n_0\,
      I1 => \outp2[6]_INST_0_i_2_n_0\,
      O => outp2(6),
      S => sel0(3)
    );
\outp2[6]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[6]_INST_0_i_3_n_0\,
      I1 => \outp2[6]_INST_0_i_4_n_0\,
      O => \outp2[6]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[6]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[6]_INST_0_i_5_n_0\,
      I1 => \outp2[6]_INST_0_i_6_n_0\,
      O => \outp2[6]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[6]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(6),
      I1 => inp3(6),
      I2 => sel0(1),
      I3 => inp2(6),
      I4 => sel0(0),
      I5 => inp1(6),
      O => \outp2[6]_INST_0_i_3_n_0\
    );
\outp2[6]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(6),
      I1 => inp7(6),
      I2 => sel0(1),
      I3 => inp6(6),
      I4 => sel0(0),
      I5 => inp5(6),
      O => \outp2[6]_INST_0_i_4_n_0\
    );
\outp2[6]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(6),
      I1 => inp11(6),
      I2 => sel0(1),
      I3 => inp10(6),
      I4 => sel0(0),
      I5 => inp9(6),
      O => \outp2[6]_INST_0_i_5_n_0\
    );
\outp2[6]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(6),
      I1 => inp15(6),
      I2 => sel0(1),
      I3 => inp14(6),
      I4 => sel0(0),
      I5 => inp13(6),
      O => \outp2[6]_INST_0_i_6_n_0\
    );
\outp2[7]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[7]_INST_0_i_1_n_0\,
      I1 => \outp2[7]_INST_0_i_2_n_0\,
      O => outp2(7),
      S => sel0(3)
    );
\outp2[7]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[7]_INST_0_i_3_n_0\,
      I1 => \outp2[7]_INST_0_i_4_n_0\,
      O => \outp2[7]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[7]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[7]_INST_0_i_5_n_0\,
      I1 => \outp2[7]_INST_0_i_6_n_0\,
      O => \outp2[7]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[7]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(7),
      I1 => inp3(7),
      I2 => sel0(1),
      I3 => inp2(7),
      I4 => sel0(0),
      I5 => inp1(7),
      O => \outp2[7]_INST_0_i_3_n_0\
    );
\outp2[7]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(7),
      I1 => inp7(7),
      I2 => sel0(1),
      I3 => inp6(7),
      I4 => sel0(0),
      I5 => inp5(7),
      O => \outp2[7]_INST_0_i_4_n_0\
    );
\outp2[7]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(7),
      I1 => inp11(7),
      I2 => sel0(1),
      I3 => inp10(7),
      I4 => sel0(0),
      I5 => inp9(7),
      O => \outp2[7]_INST_0_i_5_n_0\
    );
\outp2[7]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(7),
      I1 => inp15(7),
      I2 => sel0(1),
      I3 => inp14(7),
      I4 => sel0(0),
      I5 => inp13(7),
      O => \outp2[7]_INST_0_i_6_n_0\
    );
\outp2[8]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[8]_INST_0_i_1_n_0\,
      I1 => \outp2[8]_INST_0_i_2_n_0\,
      O => outp2(8),
      S => sel0(3)
    );
\outp2[8]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[8]_INST_0_i_3_n_0\,
      I1 => \outp2[8]_INST_0_i_4_n_0\,
      O => \outp2[8]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[8]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[8]_INST_0_i_5_n_0\,
      I1 => \outp2[8]_INST_0_i_6_n_0\,
      O => \outp2[8]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[8]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(8),
      I1 => inp3(8),
      I2 => sel0(1),
      I3 => inp2(8),
      I4 => sel0(0),
      I5 => inp1(8),
      O => \outp2[8]_INST_0_i_3_n_0\
    );
\outp2[8]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(8),
      I1 => inp7(8),
      I2 => sel0(1),
      I3 => inp6(8),
      I4 => sel0(0),
      I5 => inp5(8),
      O => \outp2[8]_INST_0_i_4_n_0\
    );
\outp2[8]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(8),
      I1 => inp11(8),
      I2 => sel0(1),
      I3 => inp10(8),
      I4 => sel0(0),
      I5 => inp9(8),
      O => \outp2[8]_INST_0_i_5_n_0\
    );
\outp2[8]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(8),
      I1 => inp15(8),
      I2 => sel0(1),
      I3 => inp14(8),
      I4 => sel0(0),
      I5 => inp13(8),
      O => \outp2[8]_INST_0_i_6_n_0\
    );
\outp2[9]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \outp2[9]_INST_0_i_1_n_0\,
      I1 => \outp2[9]_INST_0_i_2_n_0\,
      O => outp2(9),
      S => sel0(3)
    );
\outp2[9]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[9]_INST_0_i_3_n_0\,
      I1 => \outp2[9]_INST_0_i_4_n_0\,
      O => \outp2[9]_INST_0_i_1_n_0\,
      S => sel0(2)
    );
\outp2[9]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \outp2[9]_INST_0_i_5_n_0\,
      I1 => \outp2[9]_INST_0_i_6_n_0\,
      O => \outp2[9]_INST_0_i_2_n_0\,
      S => sel0(2)
    );
\outp2[9]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(9),
      I1 => inp3(9),
      I2 => sel0(1),
      I3 => inp2(9),
      I4 => sel0(0),
      I5 => inp1(9),
      O => \outp2[9]_INST_0_i_3_n_0\
    );
\outp2[9]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(9),
      I1 => inp7(9),
      I2 => sel0(1),
      I3 => inp6(9),
      I4 => sel0(0),
      I5 => inp5(9),
      O => \outp2[9]_INST_0_i_4_n_0\
    );
\outp2[9]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(9),
      I1 => inp11(9),
      I2 => sel0(1),
      I3 => inp10(9),
      I4 => sel0(0),
      I5 => inp9(9),
      O => \outp2[9]_INST_0_i_5_n_0\
    );
\outp2[9]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(9),
      I1 => inp15(9),
      I2 => sel0(1),
      I3 => inp14(9),
      I4 => sel0(0),
      I5 => inp13(9),
      O => \outp2[9]_INST_0_i_6_n_0\
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(1),
      O => p_1_in(15)
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(2),
      O => p_1_in(23)
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(3),
      O => p_1_in(31)
    );
\slv_reg0[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(0),
      O => p_1_in(3)
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(3),
      D => s00_axi_wdata(0),
      Q => \slv_reg0_reg_n_0_[0]\,
      R => p_0_in
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => \slv_reg0_reg_n_0_[10]\,
      R => p_0_in
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => \slv_reg0_reg_n_0_[11]\,
      R => p_0_in
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => \slv_reg0_reg_n_0_[12]\,
      R => p_0_in
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => \slv_reg0_reg_n_0_[13]\,
      R => p_0_in
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => \slv_reg0_reg_n_0_[14]\,
      R => p_0_in
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => \slv_reg0_reg_n_0_[15]\,
      R => p_0_in
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => \slv_reg0_reg_n_0_[16]\,
      R => p_0_in
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => \slv_reg0_reg_n_0_[17]\,
      R => p_0_in
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => \slv_reg0_reg_n_0_[18]\,
      R => p_0_in
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => \slv_reg0_reg_n_0_[19]\,
      R => p_0_in
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(3),
      D => s00_axi_wdata(1),
      Q => \slv_reg0_reg_n_0_[1]\,
      R => p_0_in
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => \slv_reg0_reg_n_0_[20]\,
      R => p_0_in
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => \slv_reg0_reg_n_0_[21]\,
      R => p_0_in
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => \slv_reg0_reg_n_0_[22]\,
      R => p_0_in
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => \slv_reg0_reg_n_0_[23]\,
      R => p_0_in
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => \slv_reg0_reg_n_0_[24]\,
      R => p_0_in
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => \slv_reg0_reg_n_0_[25]\,
      R => p_0_in
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => \slv_reg0_reg_n_0_[26]\,
      R => p_0_in
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => \slv_reg0_reg_n_0_[27]\,
      R => p_0_in
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => \slv_reg0_reg_n_0_[28]\,
      R => p_0_in
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => \slv_reg0_reg_n_0_[29]\,
      R => p_0_in
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(3),
      D => s00_axi_wdata(2),
      Q => \slv_reg0_reg_n_0_[2]\,
      R => p_0_in
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => \slv_reg0_reg_n_0_[30]\,
      R => p_0_in
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => \slv_reg0_reg_n_0_[31]\,
      R => p_0_in
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(3),
      D => s00_axi_wdata(3),
      Q => \slv_reg0_reg_n_0_[3]\,
      R => p_0_in
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(3),
      D => s00_axi_wdata(4),
      Q => sel0(0),
      R => p_0_in
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(3),
      D => s00_axi_wdata(5),
      Q => sel0(1),
      R => p_0_in
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(3),
      D => s00_axi_wdata(6),
      Q => sel0(2),
      R => p_0_in
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(3),
      D => s00_axi_wdata(7),
      Q => sel0(3),
      R => p_0_in
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => \slv_reg0_reg_n_0_[8]\,
      R => p_0_in
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => \slv_reg0_reg_n_0_[9]\,
      R => p_0_in
    );
\trig[0]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[0]_INST_0_i_1_n_0\,
      I1 => \trig[0]_INST_0_i_2_n_0\,
      O => trig(0),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[0]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[0]_INST_0_i_3_n_0\,
      I1 => \trig[0]_INST_0_i_4_n_0\,
      O => \trig[0]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[0]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[0]_INST_0_i_5_n_0\,
      I1 => \trig[0]_INST_0_i_6_n_0\,
      O => \trig[0]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[0]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(0),
      I1 => inp3(0),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(0),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(0),
      O => \trig[0]_INST_0_i_3_n_0\
    );
\trig[0]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(0),
      I1 => inp7(0),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(0),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(0),
      O => \trig[0]_INST_0_i_4_n_0\
    );
\trig[0]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(0),
      I1 => inp11(0),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(0),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(0),
      O => \trig[0]_INST_0_i_5_n_0\
    );
\trig[0]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(0),
      I1 => inp15(0),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(0),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(0),
      O => \trig[0]_INST_0_i_6_n_0\
    );
\trig[10]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[10]_INST_0_i_1_n_0\,
      I1 => \trig[10]_INST_0_i_2_n_0\,
      O => trig(10),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[10]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[10]_INST_0_i_3_n_0\,
      I1 => \trig[10]_INST_0_i_4_n_0\,
      O => \trig[10]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[10]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[10]_INST_0_i_5_n_0\,
      I1 => \trig[10]_INST_0_i_6_n_0\,
      O => \trig[10]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[10]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(10),
      I1 => inp3(10),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(10),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(10),
      O => \trig[10]_INST_0_i_3_n_0\
    );
\trig[10]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(10),
      I1 => inp7(10),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(10),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(10),
      O => \trig[10]_INST_0_i_4_n_0\
    );
\trig[10]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(10),
      I1 => inp11(10),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(10),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(10),
      O => \trig[10]_INST_0_i_5_n_0\
    );
\trig[10]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(10),
      I1 => inp15(10),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(10),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(10),
      O => \trig[10]_INST_0_i_6_n_0\
    );
\trig[11]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[11]_INST_0_i_1_n_0\,
      I1 => \trig[11]_INST_0_i_2_n_0\,
      O => trig(11),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[11]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[11]_INST_0_i_3_n_0\,
      I1 => \trig[11]_INST_0_i_4_n_0\,
      O => \trig[11]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[11]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[11]_INST_0_i_5_n_0\,
      I1 => \trig[11]_INST_0_i_6_n_0\,
      O => \trig[11]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[11]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(11),
      I1 => inp3(11),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(11),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(11),
      O => \trig[11]_INST_0_i_3_n_0\
    );
\trig[11]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(11),
      I1 => inp7(11),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(11),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(11),
      O => \trig[11]_INST_0_i_4_n_0\
    );
\trig[11]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(11),
      I1 => inp11(11),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(11),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(11),
      O => \trig[11]_INST_0_i_5_n_0\
    );
\trig[11]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(11),
      I1 => inp15(11),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(11),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(11),
      O => \trig[11]_INST_0_i_6_n_0\
    );
\trig[12]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[12]_INST_0_i_1_n_0\,
      I1 => \trig[12]_INST_0_i_2_n_0\,
      O => trig(12),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[12]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[12]_INST_0_i_3_n_0\,
      I1 => \trig[12]_INST_0_i_4_n_0\,
      O => \trig[12]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[12]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[12]_INST_0_i_5_n_0\,
      I1 => \trig[12]_INST_0_i_6_n_0\,
      O => \trig[12]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[12]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(12),
      I1 => inp3(12),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(12),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(12),
      O => \trig[12]_INST_0_i_3_n_0\
    );
\trig[12]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(12),
      I1 => inp7(12),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(12),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(12),
      O => \trig[12]_INST_0_i_4_n_0\
    );
\trig[12]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(12),
      I1 => inp11(12),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(12),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(12),
      O => \trig[12]_INST_0_i_5_n_0\
    );
\trig[12]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(12),
      I1 => inp15(12),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(12),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(12),
      O => \trig[12]_INST_0_i_6_n_0\
    );
\trig[13]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[13]_INST_0_i_1_n_0\,
      I1 => \trig[13]_INST_0_i_2_n_0\,
      O => trig(13),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[13]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[13]_INST_0_i_3_n_0\,
      I1 => \trig[13]_INST_0_i_4_n_0\,
      O => \trig[13]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[13]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[13]_INST_0_i_5_n_0\,
      I1 => \trig[13]_INST_0_i_6_n_0\,
      O => \trig[13]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[13]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(13),
      I1 => inp3(13),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(13),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(13),
      O => \trig[13]_INST_0_i_3_n_0\
    );
\trig[13]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(13),
      I1 => inp7(13),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(13),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(13),
      O => \trig[13]_INST_0_i_4_n_0\
    );
\trig[13]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(13),
      I1 => inp11(13),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(13),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(13),
      O => \trig[13]_INST_0_i_5_n_0\
    );
\trig[13]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(13),
      I1 => inp15(13),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(13),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(13),
      O => \trig[13]_INST_0_i_6_n_0\
    );
\trig[14]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[14]_INST_0_i_1_n_0\,
      I1 => \trig[14]_INST_0_i_2_n_0\,
      O => trig(14),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[14]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[14]_INST_0_i_3_n_0\,
      I1 => \trig[14]_INST_0_i_4_n_0\,
      O => \trig[14]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[14]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[14]_INST_0_i_5_n_0\,
      I1 => \trig[14]_INST_0_i_6_n_0\,
      O => \trig[14]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[14]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(14),
      I1 => inp3(14),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(14),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(14),
      O => \trig[14]_INST_0_i_3_n_0\
    );
\trig[14]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(14),
      I1 => inp7(14),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(14),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(14),
      O => \trig[14]_INST_0_i_4_n_0\
    );
\trig[14]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(14),
      I1 => inp11(14),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(14),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(14),
      O => \trig[14]_INST_0_i_5_n_0\
    );
\trig[14]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(14),
      I1 => inp15(14),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(14),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(14),
      O => \trig[14]_INST_0_i_6_n_0\
    );
\trig[15]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[15]_INST_0_i_1_n_0\,
      I1 => \trig[15]_INST_0_i_2_n_0\,
      O => trig(15),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[15]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[15]_INST_0_i_3_n_0\,
      I1 => \trig[15]_INST_0_i_4_n_0\,
      O => \trig[15]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[15]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[15]_INST_0_i_5_n_0\,
      I1 => \trig[15]_INST_0_i_6_n_0\,
      O => \trig[15]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[15]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(15),
      I1 => inp3(15),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(15),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(15),
      O => \trig[15]_INST_0_i_3_n_0\
    );
\trig[15]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(15),
      I1 => inp7(15),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(15),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(15),
      O => \trig[15]_INST_0_i_4_n_0\
    );
\trig[15]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(15),
      I1 => inp11(15),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(15),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(15),
      O => \trig[15]_INST_0_i_5_n_0\
    );
\trig[15]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(15),
      I1 => inp15(15),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(15),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(15),
      O => \trig[15]_INST_0_i_6_n_0\
    );
\trig[1]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[1]_INST_0_i_1_n_0\,
      I1 => \trig[1]_INST_0_i_2_n_0\,
      O => trig(1),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[1]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[1]_INST_0_i_3_n_0\,
      I1 => \trig[1]_INST_0_i_4_n_0\,
      O => \trig[1]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[1]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[1]_INST_0_i_5_n_0\,
      I1 => \trig[1]_INST_0_i_6_n_0\,
      O => \trig[1]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[1]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(1),
      I1 => inp3(1),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(1),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(1),
      O => \trig[1]_INST_0_i_3_n_0\
    );
\trig[1]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(1),
      I1 => inp7(1),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(1),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(1),
      O => \trig[1]_INST_0_i_4_n_0\
    );
\trig[1]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(1),
      I1 => inp11(1),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(1),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(1),
      O => \trig[1]_INST_0_i_5_n_0\
    );
\trig[1]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(1),
      I1 => inp15(1),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(1),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(1),
      O => \trig[1]_INST_0_i_6_n_0\
    );
\trig[2]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[2]_INST_0_i_1_n_0\,
      I1 => \trig[2]_INST_0_i_2_n_0\,
      O => trig(2),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[2]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[2]_INST_0_i_3_n_0\,
      I1 => \trig[2]_INST_0_i_4_n_0\,
      O => \trig[2]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[2]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[2]_INST_0_i_5_n_0\,
      I1 => \trig[2]_INST_0_i_6_n_0\,
      O => \trig[2]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[2]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(2),
      I1 => inp3(2),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(2),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(2),
      O => \trig[2]_INST_0_i_3_n_0\
    );
\trig[2]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(2),
      I1 => inp7(2),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(2),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(2),
      O => \trig[2]_INST_0_i_4_n_0\
    );
\trig[2]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(2),
      I1 => inp11(2),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(2),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(2),
      O => \trig[2]_INST_0_i_5_n_0\
    );
\trig[2]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(2),
      I1 => inp15(2),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(2),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(2),
      O => \trig[2]_INST_0_i_6_n_0\
    );
\trig[3]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[3]_INST_0_i_1_n_0\,
      I1 => \trig[3]_INST_0_i_2_n_0\,
      O => trig(3),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[3]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[3]_INST_0_i_3_n_0\,
      I1 => \trig[3]_INST_0_i_4_n_0\,
      O => \trig[3]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[3]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[3]_INST_0_i_5_n_0\,
      I1 => \trig[3]_INST_0_i_6_n_0\,
      O => \trig[3]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(3),
      I1 => inp3(3),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(3),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(3),
      O => \trig[3]_INST_0_i_3_n_0\
    );
\trig[3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(3),
      I1 => inp7(3),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(3),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(3),
      O => \trig[3]_INST_0_i_4_n_0\
    );
\trig[3]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(3),
      I1 => inp11(3),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(3),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(3),
      O => \trig[3]_INST_0_i_5_n_0\
    );
\trig[3]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(3),
      I1 => inp15(3),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(3),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(3),
      O => \trig[3]_INST_0_i_6_n_0\
    );
\trig[4]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[4]_INST_0_i_1_n_0\,
      I1 => \trig[4]_INST_0_i_2_n_0\,
      O => trig(4),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[4]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[4]_INST_0_i_3_n_0\,
      I1 => \trig[4]_INST_0_i_4_n_0\,
      O => \trig[4]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[4]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[4]_INST_0_i_5_n_0\,
      I1 => \trig[4]_INST_0_i_6_n_0\,
      O => \trig[4]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[4]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(4),
      I1 => inp3(4),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(4),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(4),
      O => \trig[4]_INST_0_i_3_n_0\
    );
\trig[4]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(4),
      I1 => inp7(4),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(4),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(4),
      O => \trig[4]_INST_0_i_4_n_0\
    );
\trig[4]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(4),
      I1 => inp11(4),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(4),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(4),
      O => \trig[4]_INST_0_i_5_n_0\
    );
\trig[4]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(4),
      I1 => inp15(4),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(4),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(4),
      O => \trig[4]_INST_0_i_6_n_0\
    );
\trig[5]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[5]_INST_0_i_1_n_0\,
      I1 => \trig[5]_INST_0_i_2_n_0\,
      O => trig(5),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[5]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[5]_INST_0_i_3_n_0\,
      I1 => \trig[5]_INST_0_i_4_n_0\,
      O => \trig[5]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[5]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[5]_INST_0_i_5_n_0\,
      I1 => \trig[5]_INST_0_i_6_n_0\,
      O => \trig[5]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[5]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(5),
      I1 => inp3(5),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(5),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(5),
      O => \trig[5]_INST_0_i_3_n_0\
    );
\trig[5]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(5),
      I1 => inp7(5),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(5),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(5),
      O => \trig[5]_INST_0_i_4_n_0\
    );
\trig[5]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(5),
      I1 => inp11(5),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(5),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(5),
      O => \trig[5]_INST_0_i_5_n_0\
    );
\trig[5]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(5),
      I1 => inp15(5),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(5),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(5),
      O => \trig[5]_INST_0_i_6_n_0\
    );
\trig[6]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[6]_INST_0_i_1_n_0\,
      I1 => \trig[6]_INST_0_i_2_n_0\,
      O => trig(6),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[6]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[6]_INST_0_i_3_n_0\,
      I1 => \trig[6]_INST_0_i_4_n_0\,
      O => \trig[6]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[6]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[6]_INST_0_i_5_n_0\,
      I1 => \trig[6]_INST_0_i_6_n_0\,
      O => \trig[6]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[6]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(6),
      I1 => inp3(6),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(6),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(6),
      O => \trig[6]_INST_0_i_3_n_0\
    );
\trig[6]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(6),
      I1 => inp7(6),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(6),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(6),
      O => \trig[6]_INST_0_i_4_n_0\
    );
\trig[6]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(6),
      I1 => inp11(6),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(6),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(6),
      O => \trig[6]_INST_0_i_5_n_0\
    );
\trig[6]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(6),
      I1 => inp15(6),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(6),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(6),
      O => \trig[6]_INST_0_i_6_n_0\
    );
\trig[7]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[7]_INST_0_i_1_n_0\,
      I1 => \trig[7]_INST_0_i_2_n_0\,
      O => trig(7),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[7]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[7]_INST_0_i_3_n_0\,
      I1 => \trig[7]_INST_0_i_4_n_0\,
      O => \trig[7]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[7]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[7]_INST_0_i_5_n_0\,
      I1 => \trig[7]_INST_0_i_6_n_0\,
      O => \trig[7]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[7]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(7),
      I1 => inp3(7),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(7),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(7),
      O => \trig[7]_INST_0_i_3_n_0\
    );
\trig[7]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(7),
      I1 => inp7(7),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(7),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(7),
      O => \trig[7]_INST_0_i_4_n_0\
    );
\trig[7]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(7),
      I1 => inp11(7),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(7),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(7),
      O => \trig[7]_INST_0_i_5_n_0\
    );
\trig[7]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(7),
      I1 => inp15(7),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(7),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(7),
      O => \trig[7]_INST_0_i_6_n_0\
    );
\trig[8]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[8]_INST_0_i_1_n_0\,
      I1 => \trig[8]_INST_0_i_2_n_0\,
      O => trig(8),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[8]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[8]_INST_0_i_3_n_0\,
      I1 => \trig[8]_INST_0_i_4_n_0\,
      O => \trig[8]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[8]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[8]_INST_0_i_5_n_0\,
      I1 => \trig[8]_INST_0_i_6_n_0\,
      O => \trig[8]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[8]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(8),
      I1 => inp3(8),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(8),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(8),
      O => \trig[8]_INST_0_i_3_n_0\
    );
\trig[8]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(8),
      I1 => inp7(8),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(8),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(8),
      O => \trig[8]_INST_0_i_4_n_0\
    );
\trig[8]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(8),
      I1 => inp11(8),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(8),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(8),
      O => \trig[8]_INST_0_i_5_n_0\
    );
\trig[8]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(8),
      I1 => inp15(8),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(8),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(8),
      O => \trig[8]_INST_0_i_6_n_0\
    );
\trig[9]_INST_0\: unisim.vcomponents.MUXF8
     port map (
      I0 => \trig[9]_INST_0_i_1_n_0\,
      I1 => \trig[9]_INST_0_i_2_n_0\,
      O => trig(9),
      S => \slv_reg0_reg_n_0_[3]\
    );
\trig[9]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[9]_INST_0_i_3_n_0\,
      I1 => \trig[9]_INST_0_i_4_n_0\,
      O => \trig[9]_INST_0_i_1_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[9]_INST_0_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \trig[9]_INST_0_i_5_n_0\,
      I1 => \trig[9]_INST_0_i_6_n_0\,
      O => \trig[9]_INST_0_i_2_n_0\,
      S => \slv_reg0_reg_n_0_[2]\
    );
\trig[9]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp4(9),
      I1 => inp3(9),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp2(9),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp1(9),
      O => \trig[9]_INST_0_i_3_n_0\
    );
\trig[9]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp8(9),
      I1 => inp7(9),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp6(9),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp5(9),
      O => \trig[9]_INST_0_i_4_n_0\
    );
\trig[9]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp12(9),
      I1 => inp11(9),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp10(9),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp9(9),
      O => \trig[9]_INST_0_i_5_n_0\
    );
\trig[9]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => inp16(9),
      I1 => inp15(9),
      I2 => \slv_reg0_reg_n_0_[1]\,
      I3 => inp14(9),
      I4 => \slv_reg0_reg_n_0_[0]\,
      I5 => inp13(9),
      O => \trig[9]_INST_0_i_6_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0 is
  port (
    S_AXI_ARREADY : out STD_LOGIC;
    axi_rvalid_reg : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    trig : out STD_LOGIC_VECTOR ( 15 downto 0 );
    outp2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    inp4 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp8 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp7 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp6 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp5 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp12 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp11 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp10 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp9 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp16 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp15 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp14 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp13 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0 : entity is "ip_mux16_2_v1_0";
end design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0;

architecture STRUCTURE of design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0 is
begin
ip_mux16_2_v1_0_S00_AXI_inst: entity work.design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0_S00_AXI
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      axi_rvalid_reg_0 => axi_rvalid_reg,
      inp1(15 downto 0) => inp1(15 downto 0),
      inp10(15 downto 0) => inp10(15 downto 0),
      inp11(15 downto 0) => inp11(15 downto 0),
      inp12(15 downto 0) => inp12(15 downto 0),
      inp13(15 downto 0) => inp13(15 downto 0),
      inp14(15 downto 0) => inp14(15 downto 0),
      inp15(15 downto 0) => inp15(15 downto 0),
      inp16(15 downto 0) => inp16(15 downto 0),
      inp2(15 downto 0) => inp2(15 downto 0),
      inp3(15 downto 0) => inp3(15 downto 0),
      inp4(15 downto 0) => inp4(15 downto 0),
      inp5(15 downto 0) => inp5(15 downto 0),
      inp6(15 downto 0) => inp6(15 downto 0),
      inp7(15 downto 0) => inp7(15 downto 0),
      inp8(15 downto 0) => inp8(15 downto 0),
      inp9(15 downto 0) => inp9(15 downto 0),
      outp2(15 downto 0) => outp2(15 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(1 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(1 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      trig(15 downto 0) => trig(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ip_mux16_2_if_0_0 is
  port (
    inp1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp4 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp5 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp6 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp7 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp8 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp9 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp10 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp11 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp12 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp13 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp14 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp15 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    inp16 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    outp1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    outp2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    trig : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ip_mux16_2_if_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ip_mux16_2_if_0_0 : entity is "design_1_ip_mux16_2_if_0_0,ip_mux16_2_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_ip_mux16_2_if_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_1_ip_mux16_2_if_0_0 : entity is "ip_mux16_2_v1_0,Vivado 2019.1";
end design_1_ip_mux16_2_if_0_0;

architecture STRUCTURE of design_1_ip_mux16_2_if_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^trig\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute x_interface_info : string;
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of inp1 : signal is "iaea.org:interface:dbg_invert_and_offset:1.0 dbg_invert_and_offset_s0 adc_data";
  attribute x_interface_info of inp10 : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_amp_rdy_slow_out";
  attribute x_interface_info of inp11 : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_amp_rdy_fast_out";
  attribute x_interface_info of inp12 : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 impulse";
  attribute x_interface_info of inp13 : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 rect";
  attribute x_interface_info of inp14 : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 impulse";
  attribute x_interface_info of inp15 : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 rect";
  attribute x_interface_info of inp16 : signal is "iaea.org:interface:dbg_invert_and_offset:1.0 dbg_invert_and_offset_s0 outp";
  attribute x_interface_info of inp2 : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 dc_stab";
  attribute x_interface_info of inp3 : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 shaper";
  attribute x_interface_info of inp4 : signal is "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 blr";
  attribute x_interface_info of inp5 : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 dc_stab";
  attribute x_interface_info of inp6 : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 shaper";
  attribute x_interface_info of inp7 : signal is "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 blr";
  attribute x_interface_info of inp8 : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_det_signal_out";
  attribute x_interface_info of inp9 : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 rejectn_out";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  outp1(15 downto 0) <= \^trig\(15 downto 0);
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
  trig(15 downto 0) <= \^trig\(15 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.design_1_ip_mux16_2_if_0_0_ip_mux16_2_v1_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      axi_rvalid_reg => s00_axi_rvalid,
      inp1(15 downto 0) => inp1(15 downto 0),
      inp10(15 downto 0) => inp10(15 downto 0),
      inp11(15 downto 0) => inp11(15 downto 0),
      inp12(15 downto 0) => inp12(15 downto 0),
      inp13(15 downto 0) => inp13(15 downto 0),
      inp14(15 downto 0) => inp14(15 downto 0),
      inp15(15 downto 0) => inp15(15 downto 0),
      inp16(15 downto 0) => inp16(15 downto 0),
      inp2(15 downto 0) => inp2(15 downto 0),
      inp3(15 downto 0) => inp3(15 downto 0),
      inp4(15 downto 0) => inp4(15 downto 0),
      inp5(15 downto 0) => inp5(15 downto 0),
      inp6(15 downto 0) => inp6(15 downto 0),
      inp7(15 downto 0) => inp7(15 downto 0),
      inp8(15 downto 0) => inp8(15 downto 0),
      inp9(15 downto 0) => inp9(15 downto 0),
      outp2(15 downto 0) => outp2(15 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(3 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(3 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      trig(15 downto 0) => \^trig\(15 downto 0)
    );
end STRUCTURE;
