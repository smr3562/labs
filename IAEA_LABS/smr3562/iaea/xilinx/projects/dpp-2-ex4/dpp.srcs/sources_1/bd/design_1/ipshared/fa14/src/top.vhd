----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/26/2021 10:10:31 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( 
           impulse_out : out STD_LOGIC_VECTOR (15 downto 0);
           rect_out : out STD_LOGIC_VECTOR (15 downto 0);
           shaper_out : out STD_LOGIC_VECTOR (15 downto 0);
           blr_out : out STD_LOGIC_VECTOR (15 downto 0);
           dc_stab_out : out STD_LOGIC_VECTOR (15 downto 0);
           dc_stab_acc_out : out STD_LOGIC_VECTOR (15 downto 0));
end top;

architecture Behavioral of top is
 
begin

   impulse_out <= "0000000000000000";
   rect_out <= "0000000000000000";
   shaper_out <= "0000000000000000";
   blr_out <= "0000000000000000";
   dc_stab_out <= "0000000000000000";
   dc_stab_acc_out <= "0000000000000000";

end Behavioral;
