-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Feb 13 17:53:40 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               E:/smr3562/iaea/xilinx/projects/dpp-2/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_term_pulse_co_0_3/design_1_ip_dbg_term_pulse_co_0_3_stub.vhdl
-- Design      : design_1_ip_dbg_term_pulse_co_0_3
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_ip_dbg_term_pulse_co_0_3 is
  Port ( 
    impulse_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rect_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    shaper_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    blr_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    dc_stab_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    dc_stab_acc_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end design_1_ip_dbg_term_pulse_co_0_3;

architecture stub of design_1_ip_dbg_term_pulse_co_0_3 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "impulse_out[15:0],rect_out[15:0],shaper_out[15:0],blr_out[15:0],dc_stab_out[15:0],dc_stab_acc_out[15:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "top,Vivado 2019.1";
begin
end;
