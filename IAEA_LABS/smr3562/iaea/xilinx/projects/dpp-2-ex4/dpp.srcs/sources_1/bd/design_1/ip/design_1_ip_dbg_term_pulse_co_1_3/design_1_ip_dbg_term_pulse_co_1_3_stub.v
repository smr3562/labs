// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Feb 13 17:56:04 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               E:/smr3562/iaea/xilinx/projects/dpp-2/dpp.srcs/sources_1/bd/design_1/ip/design_1_ip_dbg_term_pulse_co_1_3/design_1_ip_dbg_term_pulse_co_1_3_stub.v
// Design      : design_1_ip_dbg_term_pulse_co_1_3
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "top,Vivado 2019.1" *)
module design_1_ip_dbg_term_pulse_co_1_3(impulse_out, rect_out, shaper_out, blr_out, 
  dc_stab_out, dc_stab_acc_out)
/* synthesis syn_black_box black_box_pad_pin="impulse_out[15:0],rect_out[15:0],shaper_out[15:0],blr_out[15:0],dc_stab_out[15:0],dc_stab_acc_out[15:0]" */;
  output [15:0]impulse_out;
  output [15:0]rect_out;
  output [15:0]shaper_out;
  output [15:0]blr_out;
  output [15:0]dc_stab_out;
  output [15:0]dc_stab_acc_out;
endmodule
