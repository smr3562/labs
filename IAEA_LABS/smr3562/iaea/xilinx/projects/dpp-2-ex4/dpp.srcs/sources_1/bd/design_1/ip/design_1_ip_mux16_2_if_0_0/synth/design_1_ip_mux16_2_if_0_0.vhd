-- (c) Copyright 1995-2021 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:user:ip_mux16_2_if:1.0
-- IP Revision: 7

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_ip_mux16_2_if_0_0 IS
  PORT (
    inp1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp3 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp4 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp5 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp6 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp7 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp8 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp9 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp10 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp11 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp12 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp13 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp14 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp15 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    inp16 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    outp1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    outp2 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    trig : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    s00_axi_awaddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s00_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s00_axi_awvalid : IN STD_LOGIC;
    s00_axi_awready : OUT STD_LOGIC;
    s00_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s00_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s00_axi_wvalid : IN STD_LOGIC;
    s00_axi_wready : OUT STD_LOGIC;
    s00_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s00_axi_bvalid : OUT STD_LOGIC;
    s00_axi_bready : IN STD_LOGIC;
    s00_axi_araddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s00_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s00_axi_arvalid : IN STD_LOGIC;
    s00_axi_arready : OUT STD_LOGIC;
    s00_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s00_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s00_axi_rvalid : OUT STD_LOGIC;
    s00_axi_rready : IN STD_LOGIC;
    s00_axi_aclk : IN STD_LOGIC;
    s00_axi_aresetn : IN STD_LOGIC
  );
END design_1_ip_mux16_2_if_0_0;

ARCHITECTURE design_1_ip_mux16_2_if_0_0_arch OF design_1_ip_mux16_2_if_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_ip_mux16_2_if_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT ip_mux16_2_v1_0 IS
    GENERIC (
      C_S00_AXI_DATA_WIDTH : INTEGER; -- Width of S_AXI data bus
      C_S00_AXI_ADDR_WIDTH : INTEGER -- Width of S_AXI address bus
    );
    PORT (
      inp1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp3 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp4 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp5 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp6 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp7 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp8 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp9 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp10 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp11 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp12 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp13 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp14 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp15 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      inp16 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      outp1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      outp2 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      trig : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      s00_axi_awaddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s00_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s00_axi_awvalid : IN STD_LOGIC;
      s00_axi_awready : OUT STD_LOGIC;
      s00_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s00_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s00_axi_wvalid : IN STD_LOGIC;
      s00_axi_wready : OUT STD_LOGIC;
      s00_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s00_axi_bvalid : OUT STD_LOGIC;
      s00_axi_bready : IN STD_LOGIC;
      s00_axi_araddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s00_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s00_axi_arvalid : IN STD_LOGIC;
      s00_axi_arready : OUT STD_LOGIC;
      s00_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s00_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s00_axi_rvalid : OUT STD_LOGIC;
      s00_axi_rready : IN STD_LOGIC;
      s00_axi_aclk : IN STD_LOGIC;
      s00_axi_aresetn : IN STD_LOGIC
    );
  END COMPONENT ip_mux16_2_v1_0;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_ip_mux16_2_if_0_0_arch: ARCHITECTURE IS "ip_mux16_2_v1_0,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_ip_mux16_2_if_0_0_arch : ARCHITECTURE IS "design_1_ip_mux16_2_if_0_0,ip_mux16_2_v1_0,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF design_1_ip_mux16_2_if_0_0_arch: ARCHITECTURE IS "design_1_ip_mux16_2_if_0_0,ip_mux16_2_v1_0,{x_ipProduct=Vivado 2019.1,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=ip_mux16_2_if,x_ipVersion=1.0,x_ipCoreRevision=7,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED,C_S00_AXI_DATA_WIDTH=32,C_S00_AXI_ADDR_WIDTH=4}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF s00_axi_aresetn: SIGNAL IS "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s00_axi_aclk: SIGNAL IS "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s00_axi_awaddr: SIGNAL IS "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_" & 
"READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s00_axi_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  ATTRIBUTE X_INTERFACE_INFO OF inp16: SIGNAL IS "iaea.org:interface:dbg_invert_and_offset:1.0 dbg_invert_and_offset_s0 outp";
  ATTRIBUTE X_INTERFACE_INFO OF inp15: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 rect";
  ATTRIBUTE X_INTERFACE_INFO OF inp14: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 impulse";
  ATTRIBUTE X_INTERFACE_INFO OF inp13: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 rect";
  ATTRIBUTE X_INTERFACE_INFO OF inp12: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 impulse";
  ATTRIBUTE X_INTERFACE_INFO OF inp11: SIGNAL IS "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_amp_rdy_fast_out";
  ATTRIBUTE X_INTERFACE_INFO OF inp10: SIGNAL IS "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_amp_rdy_slow_out";
  ATTRIBUTE X_INTERFACE_INFO OF inp9: SIGNAL IS "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 rejectn_out";
  ATTRIBUTE X_INTERFACE_INFO OF inp8: SIGNAL IS "iaea.org:interface:dbg_pha:1.0 dbg_pha_s0 peak_det_signal_out";
  ATTRIBUTE X_INTERFACE_INFO OF inp7: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 blr";
  ATTRIBUTE X_INTERFACE_INFO OF inp6: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 shaper";
  ATTRIBUTE X_INTERFACE_INFO OF inp5: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_fast:1.0 dbg_pulse_cond_fast_s0 dc_stab";
  ATTRIBUTE X_INTERFACE_INFO OF inp4: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 blr";
  ATTRIBUTE X_INTERFACE_INFO OF inp3: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 shaper";
  ATTRIBUTE X_INTERFACE_INFO OF inp2: SIGNAL IS "iaea.org:interface:dbg_pulse_cond_slow:1.0 dbg_pulse_cond_slow_s0 dc_stab";
  ATTRIBUTE X_INTERFACE_INFO OF inp1: SIGNAL IS "iaea.org:interface:dbg_invert_and_offset:1.0 dbg_invert_and_offset_s0 adc_data";
BEGIN
  U0 : ip_mux16_2_v1_0
    GENERIC MAP (
      C_S00_AXI_DATA_WIDTH => 32,
      C_S00_AXI_ADDR_WIDTH => 4
    )
    PORT MAP (
      inp1 => inp1,
      inp2 => inp2,
      inp3 => inp3,
      inp4 => inp4,
      inp5 => inp5,
      inp6 => inp6,
      inp7 => inp7,
      inp8 => inp8,
      inp9 => inp9,
      inp10 => inp10,
      inp11 => inp11,
      inp12 => inp12,
      inp13 => inp13,
      inp14 => inp14,
      inp15 => inp15,
      inp16 => inp16,
      outp1 => outp1,
      outp2 => outp2,
      trig => trig,
      s00_axi_awaddr => s00_axi_awaddr,
      s00_axi_awprot => s00_axi_awprot,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_awready => s00_axi_awready,
      s00_axi_wdata => s00_axi_wdata,
      s00_axi_wstrb => s00_axi_wstrb,
      s00_axi_wvalid => s00_axi_wvalid,
      s00_axi_wready => s00_axi_wready,
      s00_axi_bresp => s00_axi_bresp,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_araddr => s00_axi_araddr,
      s00_axi_arprot => s00_axi_arprot,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_arready => s00_axi_arready,
      s00_axi_rdata => s00_axi_rdata,
      s00_axi_rresp => s00_axi_rresp,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_rready => s00_axi_rready,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn
    );
END design_1_ip_mux16_2_if_0_0_arch;
