-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Feb 13 17:53:53 2021
-- Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_dbg_term_pha_0_3_sim_netlist.vhdl
-- Design      : design_1_ip_dbg_term_pha_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    peak_amp_rdy_fast_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rejectn_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_rdy_slow_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_det_signal_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ip_dbg_term_pha_0_3,top,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "top,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of peak_amp_rdy_fast_out : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_amp_rdy_fast_out";
  attribute x_interface_info of peak_amp_rdy_slow_out : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_amp_rdy_slow_out";
  attribute x_interface_info of peak_det_signal_out : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 peak_det_signal_out";
  attribute x_interface_info of rejectn_out : signal is "iaea.org:interface:dbg_pha:1.0 dbg_pha_m0 rejectn_out";
begin
  peak_amp_rdy_fast_out(15) <= \<const0>\;
  peak_amp_rdy_fast_out(14) <= \<const0>\;
  peak_amp_rdy_fast_out(13) <= \<const0>\;
  peak_amp_rdy_fast_out(12) <= \<const0>\;
  peak_amp_rdy_fast_out(11) <= \<const0>\;
  peak_amp_rdy_fast_out(10) <= \<const0>\;
  peak_amp_rdy_fast_out(9) <= \<const0>\;
  peak_amp_rdy_fast_out(8) <= \<const0>\;
  peak_amp_rdy_fast_out(7) <= \<const0>\;
  peak_amp_rdy_fast_out(6) <= \<const0>\;
  peak_amp_rdy_fast_out(5) <= \<const0>\;
  peak_amp_rdy_fast_out(4) <= \<const0>\;
  peak_amp_rdy_fast_out(3) <= \<const0>\;
  peak_amp_rdy_fast_out(2) <= \<const0>\;
  peak_amp_rdy_fast_out(1) <= \<const0>\;
  peak_amp_rdy_fast_out(0) <= \<const0>\;
  peak_amp_rdy_slow_out(15) <= \<const0>\;
  peak_amp_rdy_slow_out(14) <= \<const0>\;
  peak_amp_rdy_slow_out(13) <= \<const0>\;
  peak_amp_rdy_slow_out(12) <= \<const0>\;
  peak_amp_rdy_slow_out(11) <= \<const0>\;
  peak_amp_rdy_slow_out(10) <= \<const0>\;
  peak_amp_rdy_slow_out(9) <= \<const0>\;
  peak_amp_rdy_slow_out(8) <= \<const0>\;
  peak_amp_rdy_slow_out(7) <= \<const0>\;
  peak_amp_rdy_slow_out(6) <= \<const0>\;
  peak_amp_rdy_slow_out(5) <= \<const0>\;
  peak_amp_rdy_slow_out(4) <= \<const0>\;
  peak_amp_rdy_slow_out(3) <= \<const0>\;
  peak_amp_rdy_slow_out(2) <= \<const0>\;
  peak_amp_rdy_slow_out(1) <= \<const0>\;
  peak_amp_rdy_slow_out(0) <= \<const0>\;
  peak_det_signal_out(15) <= \<const0>\;
  peak_det_signal_out(14) <= \<const0>\;
  peak_det_signal_out(13) <= \<const0>\;
  peak_det_signal_out(12) <= \<const0>\;
  peak_det_signal_out(11) <= \<const0>\;
  peak_det_signal_out(10) <= \<const0>\;
  peak_det_signal_out(9) <= \<const0>\;
  peak_det_signal_out(8) <= \<const0>\;
  peak_det_signal_out(7) <= \<const0>\;
  peak_det_signal_out(6) <= \<const0>\;
  peak_det_signal_out(5) <= \<const0>\;
  peak_det_signal_out(4) <= \<const0>\;
  peak_det_signal_out(3) <= \<const0>\;
  peak_det_signal_out(2) <= \<const0>\;
  peak_det_signal_out(1) <= \<const0>\;
  peak_det_signal_out(0) <= \<const0>\;
  rejectn_out(15) <= \<const0>\;
  rejectn_out(14) <= \<const0>\;
  rejectn_out(13) <= \<const0>\;
  rejectn_out(12) <= \<const0>\;
  rejectn_out(11) <= \<const0>\;
  rejectn_out(10) <= \<const0>\;
  rejectn_out(9) <= \<const0>\;
  rejectn_out(8) <= \<const0>\;
  rejectn_out(7) <= \<const0>\;
  rejectn_out(6) <= \<const0>\;
  rejectn_out(5) <= \<const0>\;
  rejectn_out(4) <= \<const0>\;
  rejectn_out(3) <= \<const0>\;
  rejectn_out(2) <= \<const0>\;
  rejectn_out(1) <= \<const0>\;
  rejectn_out(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
end STRUCTURE;
