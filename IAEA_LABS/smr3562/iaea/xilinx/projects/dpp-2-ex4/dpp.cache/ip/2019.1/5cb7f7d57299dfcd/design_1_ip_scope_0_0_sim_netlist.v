// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Feb 13 18:17:25 2021
// Host        : DESKTOP-GMQ0EOB running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ip_scope_0_0_sim_netlist.v
// Design      : design_1_ip_scope_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface
   (d,
    i,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    \slv_reg_array_reg[0][0] ,
    \slv_reg_array_reg[3][15] ,
    r3_dina,
    \slv_reg_array_reg[5][0] ,
    \slv_reg_array_reg[6][10] ,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rvalid,
    axibusdomain_s_axi_bvalid,
    axibusdomain_aresetn,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_clk,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_arvalid,
    r9_douta,
    q,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_rready);
  output [0:0]d;
  output [10:0]i;
  output axibusdomain_s_axi_awready;
  output axibusdomain_s_axi_wready;
  output [0:0]\slv_reg_array_reg[0][0] ;
  output [15:0]\slv_reg_array_reg[3][15] ;
  output [31:0]r3_dina;
  output [0:0]\slv_reg_array_reg[5][0] ;
  output [10:0]\slv_reg_array_reg[6][10] ;
  output axibusdomain_s_axi_arready;
  output [31:0]axibusdomain_s_axi_rdata;
  output axibusdomain_s_axi_rvalid;
  output axibusdomain_s_axi_bvalid;
  input axibusdomain_aresetn;
  input [31:0]axibusdomain_s_axi_wdata;
  input [3:0]axibusdomain_s_axi_wstrb;
  input axibusdomain_clk;
  input [5:0]axibusdomain_s_axi_awaddr;
  input [5:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_awvalid;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_arvalid;
  input [31:0]r9_douta;
  input [0:0]q;
  input axibusdomain_s_axi_bready;
  input axibusdomain_s_axi_rready;

  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [5:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [5:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [0:0]d;
  wire [10:0]i;
  wire [0:0]q;
  wire [31:0]r3_dina;
  wire [31:0]r9_douta;
  wire [0:0]\slv_reg_array_reg[0][0] ;
  wire [15:0]\slv_reg_array_reg[3][15] ;
  wire [0:0]\slv_reg_array_reg[5][0] ;
  wire [10:0]\slv_reg_array_reg[6][10] ;

  LUT6 #(
    .INIT(64'h7444444444444444)) 
    axi_bvalid_i_1
       (.I0(axibusdomain_s_axi_bready),
        .I1(axibusdomain_s_axi_bvalid),
        .I2(axibusdomain_s_axi_wvalid),
        .I3(axibusdomain_s_axi_awvalid),
        .I4(axibusdomain_s_axi_wready),
        .I5(axibusdomain_s_axi_awready),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h0F88)) 
    axi_rvalid_i_1
       (.I0(axibusdomain_s_axi_arready),
        .I1(axibusdomain_s_axi_arvalid),
        .I2(axibusdomain_s_axi_rready),
        .I3(axibusdomain_s_axi_rvalid),
        .O(axi_rvalid_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog inst
       (.axi_arready_reg_0(axibusdomain_s_axi_arready),
        .axi_awready_reg_0(axibusdomain_s_axi_awready),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .axi_rvalid_reg_0(axi_rvalid_i_1_n_0),
        .axi_wready_reg_0(axibusdomain_s_axi_wready),
        .axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .d(d),
        .i(i),
        .q(q),
        .r3_dina(r3_dina),
        .r9_douta(r9_douta),
        .\slv_reg_array_reg[0][0]_0 (\slv_reg_array_reg[0][0] ),
        .\slv_reg_array_reg[3][15]_0 (\slv_reg_array_reg[3][15] ),
        .\slv_reg_array_reg[5][0]_0 (\slv_reg_array_reg[5][0] ),
        .\slv_reg_array_reg[6][10]_0 (\slv_reg_array_reg[6][10] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface_verilog
   (axi_awready_reg_0,
    axi_wready_reg_0,
    axi_arready_reg_0,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_rvalid,
    i,
    \slv_reg_array_reg[0][0]_0 ,
    d,
    \slv_reg_array_reg[3][15]_0 ,
    r3_dina,
    \slv_reg_array_reg[5][0]_0 ,
    \slv_reg_array_reg[6][10]_0 ,
    axibusdomain_s_axi_rdata,
    axibusdomain_clk,
    axi_bvalid_reg_0,
    axi_rvalid_reg_0,
    axibusdomain_aresetn,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wvalid,
    r9_douta,
    q);
  output axi_awready_reg_0;
  output axi_wready_reg_0;
  output axi_arready_reg_0;
  output axibusdomain_s_axi_bvalid;
  output axibusdomain_s_axi_rvalid;
  output [10:0]i;
  output [0:0]\slv_reg_array_reg[0][0]_0 ;
  output [0:0]d;
  output [15:0]\slv_reg_array_reg[3][15]_0 ;
  output [31:0]r3_dina;
  output [0:0]\slv_reg_array_reg[5][0]_0 ;
  output [10:0]\slv_reg_array_reg[6][10]_0 ;
  output [31:0]axibusdomain_s_axi_rdata;
  input axibusdomain_clk;
  input axi_bvalid_reg_0;
  input axi_rvalid_reg_0;
  input axibusdomain_aresetn;
  input [31:0]axibusdomain_s_axi_wdata;
  input [3:0]axibusdomain_s_axi_wstrb;
  input [5:0]axibusdomain_s_axi_awaddr;
  input [5:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_awvalid;
  input axibusdomain_s_axi_wvalid;
  input [31:0]r9_douta;
  input [0:0]q;

  wire [5:0]axi_araddr;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire [5:0]axi_awaddr;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [5:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arvalid;
  wire [5:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [0:0]d;
  wire [3:0]dec_r__15;
  wire [10:0]i;
  wire p_0_in;
  wire [0:0]q;
  wire [31:11]r1_addra;
  wire [31:1]r2_wea;
  wire [31:0]r3_dina;
  wire [31:16]r4_threshold;
  wire [31:1]r5_enable;
  wire [31:11]r6_delay;
  wire [31:1]r7_clear;
  wire [31:0]r9_douta;
  wire \slv_reg_array[0][0]_i_1_n_0 ;
  wire \slv_reg_array[0][10]_i_1_n_0 ;
  wire \slv_reg_array[0][11]_i_1_n_0 ;
  wire \slv_reg_array[0][12]_i_1_n_0 ;
  wire \slv_reg_array[0][13]_i_1_n_0 ;
  wire \slv_reg_array[0][14]_i_1_n_0 ;
  wire \slv_reg_array[0][15]_i_1_n_0 ;
  wire \slv_reg_array[0][15]_i_2_n_0 ;
  wire \slv_reg_array[0][15]_i_3_n_0 ;
  wire \slv_reg_array[0][15]_i_4_n_0 ;
  wire \slv_reg_array[0][15]_i_5_n_0 ;
  wire \slv_reg_array[0][16]_i_1_n_0 ;
  wire \slv_reg_array[0][17]_i_1_n_0 ;
  wire \slv_reg_array[0][18]_i_1_n_0 ;
  wire \slv_reg_array[0][19]_i_1_n_0 ;
  wire \slv_reg_array[0][1]_i_1_n_0 ;
  wire \slv_reg_array[0][20]_i_1_n_0 ;
  wire \slv_reg_array[0][21]_i_1_n_0 ;
  wire \slv_reg_array[0][22]_i_1_n_0 ;
  wire \slv_reg_array[0][23]_i_1_n_0 ;
  wire \slv_reg_array[0][23]_i_2_n_0 ;
  wire \slv_reg_array[0][23]_i_3_n_0 ;
  wire \slv_reg_array[0][23]_i_4_n_0 ;
  wire \slv_reg_array[0][23]_i_5_n_0 ;
  wire \slv_reg_array[0][24]_i_1_n_0 ;
  wire \slv_reg_array[0][25]_i_1_n_0 ;
  wire \slv_reg_array[0][26]_i_1_n_0 ;
  wire \slv_reg_array[0][27]_i_1_n_0 ;
  wire \slv_reg_array[0][28]_i_1_n_0 ;
  wire \slv_reg_array[0][29]_i_1_n_0 ;
  wire \slv_reg_array[0][2]_i_1_n_0 ;
  wire \slv_reg_array[0][30]_i_1_n_0 ;
  wire \slv_reg_array[0][31]_i_1_n_0 ;
  wire \slv_reg_array[0][31]_i_2_n_0 ;
  wire \slv_reg_array[0][31]_i_3_n_0 ;
  wire \slv_reg_array[0][31]_i_4_n_0 ;
  wire \slv_reg_array[0][31]_i_5_n_0 ;
  wire \slv_reg_array[0][31]_i_6_n_0 ;
  wire \slv_reg_array[0][3]_i_1_n_0 ;
  wire \slv_reg_array[0][4]_i_1_n_0 ;
  wire \slv_reg_array[0][5]_i_1_n_0 ;
  wire \slv_reg_array[0][6]_i_1_n_0 ;
  wire \slv_reg_array[0][7]_i_1_n_0 ;
  wire \slv_reg_array[0][7]_i_2_n_0 ;
  wire \slv_reg_array[0][7]_i_3_n_0 ;
  wire \slv_reg_array[0][7]_i_4_n_0 ;
  wire \slv_reg_array[0][7]_i_5_n_0 ;
  wire \slv_reg_array[0][8]_i_1_n_0 ;
  wire \slv_reg_array[0][9]_i_1_n_0 ;
  wire \slv_reg_array[1][0]_i_1_n_0 ;
  wire \slv_reg_array[1][0]_i_2_n_0 ;
  wire \slv_reg_array[1][10]_i_1_n_0 ;
  wire \slv_reg_array[1][10]_i_2_n_0 ;
  wire \slv_reg_array[1][11]_i_1_n_0 ;
  wire \slv_reg_array[1][12]_i_1_n_0 ;
  wire \slv_reg_array[1][13]_i_1_n_0 ;
  wire \slv_reg_array[1][14]_i_1_n_0 ;
  wire \slv_reg_array[1][15]_i_1_n_0 ;
  wire \slv_reg_array[1][15]_i_2_n_0 ;
  wire \slv_reg_array[1][16]_i_1_n_0 ;
  wire \slv_reg_array[1][17]_i_1_n_0 ;
  wire \slv_reg_array[1][18]_i_1_n_0 ;
  wire \slv_reg_array[1][19]_i_1_n_0 ;
  wire \slv_reg_array[1][1]_i_1_n_0 ;
  wire \slv_reg_array[1][20]_i_1_n_0 ;
  wire \slv_reg_array[1][21]_i_1_n_0 ;
  wire \slv_reg_array[1][22]_i_1_n_0 ;
  wire \slv_reg_array[1][23]_i_1_n_0 ;
  wire \slv_reg_array[1][23]_i_2_n_0 ;
  wire \slv_reg_array[1][24]_i_1_n_0 ;
  wire \slv_reg_array[1][25]_i_1_n_0 ;
  wire \slv_reg_array[1][26]_i_1_n_0 ;
  wire \slv_reg_array[1][27]_i_1_n_0 ;
  wire \slv_reg_array[1][28]_i_1_n_0 ;
  wire \slv_reg_array[1][29]_i_1_n_0 ;
  wire \slv_reg_array[1][2]_i_1_n_0 ;
  wire \slv_reg_array[1][30]_i_1_n_0 ;
  wire \slv_reg_array[1][31]_i_1_n_0 ;
  wire \slv_reg_array[1][31]_i_2_n_0 ;
  wire \slv_reg_array[1][31]_i_3_n_0 ;
  wire \slv_reg_array[1][31]_i_4_n_0 ;
  wire \slv_reg_array[1][3]_i_1_n_0 ;
  wire \slv_reg_array[1][4]_i_1_n_0 ;
  wire \slv_reg_array[1][5]_i_1_n_0 ;
  wire \slv_reg_array[1][6]_i_1_n_0 ;
  wire \slv_reg_array[1][7]_i_1_n_0 ;
  wire \slv_reg_array[1][7]_i_2_n_0 ;
  wire \slv_reg_array[1][8]_i_1_n_0 ;
  wire \slv_reg_array[1][9]_i_1_n_0 ;
  wire \slv_reg_array[2][0]_i_1_n_0 ;
  wire \slv_reg_array[2][0]_i_2_n_0 ;
  wire \slv_reg_array[2][10]_i_1_n_0 ;
  wire \slv_reg_array[2][15]_i_1_n_0 ;
  wire \slv_reg_array[2][23]_i_1_n_0 ;
  wire \slv_reg_array[2][2]_i_1_n_0 ;
  wire \slv_reg_array[2][31]_i_1_n_0 ;
  wire \slv_reg_array[2][31]_i_2_n_0 ;
  wire \slv_reg_array[2][5]_i_1_n_0 ;
  wire \slv_reg_array[2][6]_i_1_n_0 ;
  wire \slv_reg_array[2][6]_i_2_n_0 ;
  wire \slv_reg_array[2][6]_i_3_n_0 ;
  wire \slv_reg_array[2][7]_i_1_n_0 ;
  wire \slv_reg_array[2][9]_i_1_n_0 ;
  wire \slv_reg_array[3][0]_i_1_n_0 ;
  wire \slv_reg_array[3][10]_i_1_n_0 ;
  wire \slv_reg_array[3][15]_i_1_n_0 ;
  wire \slv_reg_array[3][1]_i_1_n_0 ;
  wire \slv_reg_array[3][23]_i_1_n_0 ;
  wire \slv_reg_array[3][2]_i_1_n_0 ;
  wire \slv_reg_array[3][31]_i_1_n_0 ;
  wire \slv_reg_array[3][3]_i_1_n_0 ;
  wire \slv_reg_array[3][4]_i_1_n_0 ;
  wire \slv_reg_array[3][5]_i_1_n_0 ;
  wire \slv_reg_array[3][6]_i_1_n_0 ;
  wire \slv_reg_array[3][6]_i_2_n_0 ;
  wire \slv_reg_array[3][7]_i_1_n_0 ;
  wire \slv_reg_array[3][7]_i_2_n_0 ;
  wire \slv_reg_array[3][7]_i_3_n_0 ;
  wire \slv_reg_array[3][9]_i_1_n_0 ;
  wire \slv_reg_array[4][0]_i_1_n_0 ;
  wire \slv_reg_array[4][15]_i_1_n_0 ;
  wire \slv_reg_array[4][15]_i_2_n_0 ;
  wire \slv_reg_array[4][23]_i_1_n_0 ;
  wire \slv_reg_array[4][23]_i_2_n_0 ;
  wire \slv_reg_array[4][31]_i_1_n_0 ;
  wire \slv_reg_array[4][31]_i_2_n_0 ;
  wire \slv_reg_array[4][31]_i_3_n_0 ;
  wire \slv_reg_array[4][7]_i_1_n_0 ;
  wire \slv_reg_array[4][7]_i_2_n_0 ;
  wire \slv_reg_array[5][0]_i_1_n_0 ;
  wire \slv_reg_array[5][15]_i_1_n_0 ;
  wire \slv_reg_array[5][15]_i_2_n_0 ;
  wire \slv_reg_array[5][23]_i_1_n_0 ;
  wire \slv_reg_array[5][23]_i_2_n_0 ;
  wire \slv_reg_array[5][31]_i_1_n_0 ;
  wire \slv_reg_array[5][31]_i_2_n_0 ;
  wire \slv_reg_array[5][31]_i_3_n_0 ;
  wire \slv_reg_array[5][7]_i_1_n_0 ;
  wire \slv_reg_array[5][7]_i_2_n_0 ;
  wire \slv_reg_array[6][0]_i_1_n_0 ;
  wire \slv_reg_array[6][15]_i_1_n_0 ;
  wire \slv_reg_array[6][15]_i_2_n_0 ;
  wire \slv_reg_array[6][23]_i_1_n_0 ;
  wire \slv_reg_array[6][23]_i_2_n_0 ;
  wire \slv_reg_array[6][31]_i_1_n_0 ;
  wire \slv_reg_array[6][31]_i_2_n_0 ;
  wire \slv_reg_array[6][31]_i_3_n_0 ;
  wire \slv_reg_array[6][7]_i_1_n_0 ;
  wire \slv_reg_array[6][7]_i_2_n_0 ;
  wire [0:0]\slv_reg_array_reg[0][0]_0 ;
  wire [15:0]\slv_reg_array_reg[3][15]_0 ;
  wire [0:0]\slv_reg_array_reg[5][0]_0 ;
  wire [10:0]\slv_reg_array_reg[6][10]_0 ;
  wire slv_reg_rden__0;
  wire [31:0]slv_wire_array;

  FDRE \axi_araddr_reg[0] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[0]),
        .Q(axi_araddr[0]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[1] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[1]),
        .Q(axi_araddr[1]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[2] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[2]),
        .Q(axi_araddr[2]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[3] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[3]),
        .Q(axi_araddr[3]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[4] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[4]),
        .Q(axi_araddr[4]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[5] 
       (.C(axibusdomain_clk),
        .CE(axi_arready0),
        .D(axibusdomain_s_axi_araddr[5]),
        .Q(axi_araddr[5]),
        .R(p_0_in));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(axibusdomain_s_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[0] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[0]),
        .Q(axi_awaddr[0]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[1] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[1]),
        .Q(axi_awaddr[1]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[2] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[2]),
        .Q(axi_awaddr[2]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[3]),
        .Q(axi_awaddr[3]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[4]),
        .Q(axi_awaddr[4]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[5] 
       (.C(axibusdomain_clk),
        .CE(axi_awready0),
        .D(axibusdomain_s_axi_awaddr[5]),
        .Q(axi_awaddr[5]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(axibusdomain_aresetn),
        .O(p_0_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .I2(axi_awready_reg_0),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(p_0_in));
  FDRE axi_bvalid_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(axibusdomain_s_axi_bvalid),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[0]_i_1 
       (.I0(r9_douta[0]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[0]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[0]_i_3_n_0 ),
        .O(slv_wire_array[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(q),
        .I1(\slv_reg_array_reg[6][10]_0 [0]),
        .I2(dec_r__15[1]),
        .I3(\slv_reg_array_reg[5][0]_0 ),
        .I4(dec_r__15[0]),
        .I5(r3_dina[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [0]),
        .I1(d),
        .I2(dec_r__15[1]),
        .I3(i[0]),
        .I4(dec_r__15[0]),
        .I5(\slv_reg_array_reg[0][0]_0 ),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[10]_i_1 
       (.I0(r9_douta[10]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[10]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[10]_i_3_n_0 ),
        .O(slv_wire_array[10]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [10]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[10]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [10]),
        .I1(r5_enable[10]),
        .I2(dec_r__15[1]),
        .I3(i[10]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[11]_i_1 
       (.I0(r9_douta[11]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[11]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[11]_i_3_n_0 ),
        .O(slv_wire_array[11]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_2 
       (.I0(r1_addra[11]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[11]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [11]),
        .I1(r5_enable[11]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[11]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[12]_i_1 
       (.I0(r9_douta[12]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[12]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[12]_i_3_n_0 ),
        .O(slv_wire_array[12]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[12]_i_2 
       (.I0(r1_addra[12]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[12]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [12]),
        .I1(r5_enable[12]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[12]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[13]_i_1 
       (.I0(r9_douta[13]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[13]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[13]_i_3_n_0 ),
        .O(slv_wire_array[13]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[13]_i_2 
       (.I0(r1_addra[13]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[13]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [13]),
        .I1(r5_enable[13]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[13]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[13]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[14]_i_1 
       (.I0(r9_douta[14]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[14]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[14]_i_3_n_0 ),
        .O(slv_wire_array[14]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[14]_i_2 
       (.I0(r1_addra[14]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[14]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [14]),
        .I1(r5_enable[14]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[14]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[14]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[15]_i_1 
       (.I0(r9_douta[15]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[15]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[15]_i_3_n_0 ),
        .O(slv_wire_array[15]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[15]_i_2 
       (.I0(r1_addra[15]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[15]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [15]),
        .I1(r5_enable[15]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[15]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[16]_i_1 
       (.I0(r9_douta[16]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[16]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[16]_i_3_n_0 ),
        .O(slv_wire_array[16]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[16]_i_2 
       (.I0(r1_addra[16]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[16]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_3 
       (.I0(r4_threshold[16]),
        .I1(r5_enable[16]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[16]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[17]_i_1 
       (.I0(r9_douta[17]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[17]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[17]_i_3_n_0 ),
        .O(slv_wire_array[17]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[17]_i_2 
       (.I0(r1_addra[17]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[17]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_3 
       (.I0(r4_threshold[17]),
        .I1(r5_enable[17]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[17]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[18]_i_1 
       (.I0(r9_douta[18]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[18]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[18]_i_3_n_0 ),
        .O(slv_wire_array[18]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[18]_i_2 
       (.I0(r1_addra[18]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[18]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_3 
       (.I0(r4_threshold[18]),
        .I1(r5_enable[18]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[18]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[18]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[19]_i_1 
       (.I0(r9_douta[19]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[19]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[19]_i_3_n_0 ),
        .O(slv_wire_array[19]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[19]_i_2 
       (.I0(r1_addra[19]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[19]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_3 
       (.I0(r4_threshold[19]),
        .I1(r5_enable[19]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[19]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[1]_i_1 
       (.I0(r9_douta[1]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[1]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[1]_i_3_n_0 ),
        .O(slv_wire_array[1]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [1]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[1]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [1]),
        .I1(r5_enable[1]),
        .I2(dec_r__15[1]),
        .I3(i[1]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[20]_i_1 
       (.I0(r9_douta[20]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[20]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[20]_i_3_n_0 ),
        .O(slv_wire_array[20]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[20]_i_2 
       (.I0(r1_addra[20]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[20]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_3 
       (.I0(r4_threshold[20]),
        .I1(r5_enable[20]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[20]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[20]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[21]_i_1 
       (.I0(r9_douta[21]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[21]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[21]_i_3_n_0 ),
        .O(slv_wire_array[21]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[21]_i_2 
       (.I0(r1_addra[21]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[21]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_3 
       (.I0(r4_threshold[21]),
        .I1(r5_enable[21]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[21]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[22]_i_1 
       (.I0(r9_douta[22]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[22]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[22]_i_3_n_0 ),
        .O(slv_wire_array[22]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[22]_i_2 
       (.I0(r1_addra[22]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[22]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_3 
       (.I0(r4_threshold[22]),
        .I1(r5_enable[22]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[22]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[22]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[23]_i_1 
       (.I0(r9_douta[23]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[23]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[23]_i_3_n_0 ),
        .O(slv_wire_array[23]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[23]_i_2 
       (.I0(r1_addra[23]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[23]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_3 
       (.I0(r4_threshold[23]),
        .I1(r5_enable[23]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[23]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[24]_i_1 
       (.I0(r9_douta[24]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[24]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[24]_i_3_n_0 ),
        .O(slv_wire_array[24]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[24]_i_2 
       (.I0(r1_addra[24]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[24]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_3 
       (.I0(r4_threshold[24]),
        .I1(r5_enable[24]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[24]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[25]_i_1 
       (.I0(r9_douta[25]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[25]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[25]_i_3_n_0 ),
        .O(slv_wire_array[25]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[25]_i_2 
       (.I0(r1_addra[25]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[25]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_3 
       (.I0(r4_threshold[25]),
        .I1(r5_enable[25]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[25]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[26]_i_1 
       (.I0(r9_douta[26]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[26]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[26]_i_3_n_0 ),
        .O(slv_wire_array[26]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[26]_i_2 
       (.I0(r1_addra[26]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[26]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_3 
       (.I0(r4_threshold[26]),
        .I1(r5_enable[26]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[26]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[26]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[27]_i_1 
       (.I0(r9_douta[27]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[27]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[27]_i_3_n_0 ),
        .O(slv_wire_array[27]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[27]_i_2 
       (.I0(r1_addra[27]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[27]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_3 
       (.I0(r4_threshold[27]),
        .I1(r5_enable[27]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[27]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[28]_i_1 
       (.I0(r9_douta[28]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[28]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[28]_i_3_n_0 ),
        .O(slv_wire_array[28]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[28]_i_2 
       (.I0(r1_addra[28]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[28]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_3 
       (.I0(r4_threshold[28]),
        .I1(r5_enable[28]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[28]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[29]_i_1 
       (.I0(r9_douta[29]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[29]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[29]_i_3_n_0 ),
        .O(slv_wire_array[29]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[29]_i_2 
       (.I0(r1_addra[29]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[29]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_3 
       (.I0(r4_threshold[29]),
        .I1(r5_enable[29]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[29]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[29]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[2]_i_1 
       (.I0(r9_douta[2]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[2]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[2]_i_3_n_0 ),
        .O(slv_wire_array[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [2]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[2]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [2]),
        .I1(r5_enable[2]),
        .I2(dec_r__15[1]),
        .I3(i[2]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[30]_i_1 
       (.I0(r9_douta[30]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[30]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[30]_i_3_n_0 ),
        .O(slv_wire_array[30]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[30]_i_2 
       (.I0(r1_addra[30]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[30]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_3 
       (.I0(r4_threshold[30]),
        .I1(r5_enable[30]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[30]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[31]_i_1 
       (.I0(r9_douta[31]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[31]_i_3_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[31]_i_5_n_0 ),
        .O(slv_wire_array[31]));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \axi_rdata[31]_i_2 
       (.I0(axi_araddr[0]),
        .I1(axi_araddr[4]),
        .I2(axi_araddr[1]),
        .I3(axi_araddr[5]),
        .I4(axi_araddr[3]),
        .I5(axi_araddr[2]),
        .O(dec_r__15[3]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[31]_i_3 
       (.I0(r1_addra[31]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[31]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_4 
       (.I0(axi_araddr[5]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[4]),
        .I3(axi_araddr[1]),
        .O(dec_r__15[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_5 
       (.I0(r4_threshold[31]),
        .I1(r5_enable[31]),
        .I2(dec_r__15[1]),
        .I3(r6_delay[31]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[31]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_6 
       (.I0(axi_araddr[5]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[1]),
        .O(dec_r__15[1]));
  LUT4 #(
    .INIT(16'h0010)) 
    \axi_rdata[31]_i_7 
       (.I0(axi_araddr[5]),
        .I1(axi_araddr[0]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[1]),
        .O(dec_r__15[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[3]_i_1 
       (.I0(r9_douta[3]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[3]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[3]_i_3_n_0 ),
        .O(slv_wire_array[3]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [3]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[3]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [3]),
        .I1(r5_enable[3]),
        .I2(dec_r__15[1]),
        .I3(i[3]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[4]_i_1 
       (.I0(r9_douta[4]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[4]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[4]_i_3_n_0 ),
        .O(slv_wire_array[4]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [4]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[4]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [4]),
        .I1(r5_enable[4]),
        .I2(dec_r__15[1]),
        .I3(i[4]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[5]_i_1 
       (.I0(r9_douta[5]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[5]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[5]_i_3_n_0 ),
        .O(slv_wire_array[5]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [5]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[5]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [5]),
        .I1(r5_enable[5]),
        .I2(dec_r__15[1]),
        .I3(i[5]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[6]_i_1 
       (.I0(r9_douta[6]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[6]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[6]_i_3_n_0 ),
        .O(slv_wire_array[6]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [6]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[6]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [6]),
        .I1(r5_enable[6]),
        .I2(dec_r__15[1]),
        .I3(i[6]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[7]_i_1 
       (.I0(r9_douta[7]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[7]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[7]_i_3_n_0 ),
        .O(slv_wire_array[7]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [7]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[7]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [7]),
        .I1(r5_enable[7]),
        .I2(dec_r__15[1]),
        .I3(i[7]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[8]_i_1 
       (.I0(r9_douta[8]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[8]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[8]_i_3_n_0 ),
        .O(slv_wire_array[8]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [8]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[8]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [8]),
        .I1(r5_enable[8]),
        .I2(dec_r__15[1]),
        .I3(i[8]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[9]_i_1 
       (.I0(r9_douta[9]),
        .I1(dec_r__15[3]),
        .I2(\axi_rdata[9]_i_2_n_0 ),
        .I3(dec_r__15[2]),
        .I4(\axi_rdata[9]_i_3_n_0 ),
        .O(slv_wire_array[9]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_2 
       (.I0(\slv_reg_array_reg[6][10]_0 [9]),
        .I1(dec_r__15[1]),
        .I2(r2_wea[9]),
        .I3(dec_r__15[0]),
        .I4(r3_dina[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_3 
       (.I0(\slv_reg_array_reg[3][15]_0 [9]),
        .I1(r5_enable[9]),
        .I2(dec_r__15[1]),
        .I3(i[9]),
        .I4(dec_r__15[0]),
        .I5(r7_clear[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[0]),
        .Q(axibusdomain_s_axi_rdata[0]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[10] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[10]),
        .Q(axibusdomain_s_axi_rdata[10]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[11] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[11]),
        .Q(axibusdomain_s_axi_rdata[11]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[12] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[12]),
        .Q(axibusdomain_s_axi_rdata[12]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[13] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[13]),
        .Q(axibusdomain_s_axi_rdata[13]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[14] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[14]),
        .Q(axibusdomain_s_axi_rdata[14]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[15] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[15]),
        .Q(axibusdomain_s_axi_rdata[15]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[16] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[16]),
        .Q(axibusdomain_s_axi_rdata[16]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[17] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[17]),
        .Q(axibusdomain_s_axi_rdata[17]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[18] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[18]),
        .Q(axibusdomain_s_axi_rdata[18]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[19] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[19]),
        .Q(axibusdomain_s_axi_rdata[19]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[1] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[1]),
        .Q(axibusdomain_s_axi_rdata[1]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[20] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[20]),
        .Q(axibusdomain_s_axi_rdata[20]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[21] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[21]),
        .Q(axibusdomain_s_axi_rdata[21]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[22] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[22]),
        .Q(axibusdomain_s_axi_rdata[22]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[23] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[23]),
        .Q(axibusdomain_s_axi_rdata[23]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[24] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[24]),
        .Q(axibusdomain_s_axi_rdata[24]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[25] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[25]),
        .Q(axibusdomain_s_axi_rdata[25]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[26] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[26]),
        .Q(axibusdomain_s_axi_rdata[26]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[27] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[27]),
        .Q(axibusdomain_s_axi_rdata[27]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[28] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[28]),
        .Q(axibusdomain_s_axi_rdata[28]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[29] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[29]),
        .Q(axibusdomain_s_axi_rdata[29]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[2] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[2]),
        .Q(axibusdomain_s_axi_rdata[2]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[30] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[30]),
        .Q(axibusdomain_s_axi_rdata[30]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[31] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[31]),
        .Q(axibusdomain_s_axi_rdata[31]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[3] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[3]),
        .Q(axibusdomain_s_axi_rdata[3]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[4] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[4]),
        .Q(axibusdomain_s_axi_rdata[4]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[5] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[5]),
        .Q(axibusdomain_s_axi_rdata[5]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[6] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[6]),
        .Q(axibusdomain_s_axi_rdata[6]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[7] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[7]),
        .Q(axibusdomain_s_axi_rdata[7]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[8] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[8]),
        .Q(axibusdomain_s_axi_rdata[8]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[9] 
       (.C(axibusdomain_clk),
        .CE(slv_reg_rden__0),
        .D(slv_wire_array[9]),
        .Q(axibusdomain_s_axi_rdata[9]),
        .R(p_0_in));
  FDRE axi_rvalid_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_rvalid_reg_0),
        .Q(axibusdomain_s_axi_rvalid),
        .R(p_0_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .I2(axi_wready_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h8FFF000080000000)) 
    \slv_reg_array[0][0]_i_1 
       (.I0(axibusdomain_s_axi_wdata[0]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .I3(\slv_reg_array[0][31]_i_4_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[0][0]_0 ),
        .O(\slv_reg_array[0][0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][10]_i_1 
       (.I0(axibusdomain_s_axi_wdata[10]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][11]_i_1 
       (.I0(axibusdomain_s_axi_wdata[11]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][12]_i_1 
       (.I0(axibusdomain_s_axi_wdata[12]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][13]_i_1 
       (.I0(axibusdomain_s_axi_wdata[13]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][14]_i_1 
       (.I0(axibusdomain_s_axi_wdata[14]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][14]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[0][15]_i_1 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[0][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[0][15]_i_2 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .O(\slv_reg_array[0][15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][15]_i_3 
       (.I0(axibusdomain_s_axi_wdata[15]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \slv_reg_array[0][15]_i_4 
       (.I0(axibusdomain_s_axi_wvalid),
        .I1(axibusdomain_s_axi_awvalid),
        .I2(axi_wready_reg_0),
        .I3(axi_awready_reg_0),
        .I4(axibusdomain_aresetn),
        .I5(axibusdomain_s_axi_wstrb[1]),
        .O(\slv_reg_array[0][15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000080008000)) 
    \slv_reg_array[0][15]_i_5 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_wvalid),
        .I4(axibusdomain_s_axi_wstrb[1]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][15]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][16]_i_1 
       (.I0(axibusdomain_s_axi_wdata[16]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][17]_i_1 
       (.I0(axibusdomain_s_axi_wdata[17]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][18]_i_1 
       (.I0(axibusdomain_s_axi_wdata[18]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][19]_i_1 
       (.I0(axibusdomain_s_axi_wdata[19]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][1]_i_1 
       (.I0(axibusdomain_s_axi_wdata[1]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][20]_i_1 
       (.I0(axibusdomain_s_axi_wdata[20]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][21]_i_1 
       (.I0(axibusdomain_s_axi_wdata[21]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][22]_i_1 
       (.I0(axibusdomain_s_axi_wdata[22]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][22]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[0][23]_i_1 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][23]_i_4_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[0][23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[0][23]_i_2 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][23]_i_4_n_0 ),
        .O(\slv_reg_array[0][23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][23]_i_3 
       (.I0(axibusdomain_s_axi_wdata[23]),
        .I1(axibusdomain_s_axi_wstrb[2]),
        .I2(\slv_reg_array[0][23]_i_5_n_0 ),
        .O(\slv_reg_array[0][23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[0][23]_i_4 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000080008000)) 
    \slv_reg_array[0][23]_i_5 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_wvalid),
        .I4(axibusdomain_s_axi_wstrb[2]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][23]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][24]_i_1 
       (.I0(axibusdomain_s_axi_wdata[24]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][25]_i_1 
       (.I0(axibusdomain_s_axi_wdata[25]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][26]_i_1 
       (.I0(axibusdomain_s_axi_wdata[26]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][27]_i_1 
       (.I0(axibusdomain_s_axi_wdata[27]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][28]_i_1 
       (.I0(axibusdomain_s_axi_wdata[28]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][29]_i_1 
       (.I0(axibusdomain_s_axi_wdata[29]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][2]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wdata[2]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][30]_i_1 
       (.I0(axibusdomain_s_axi_wdata[30]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][30]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[0][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][31]_i_5_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[0][31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[0][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][31]_i_5_n_0 ),
        .O(\slv_reg_array[0][31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][31]_i_3 
       (.I0(axibusdomain_s_axi_wdata[31]),
        .I1(axibusdomain_s_axi_wstrb[3]),
        .I2(\slv_reg_array[0][31]_i_6_n_0 ),
        .O(\slv_reg_array[0][31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFCFFFCFFFCFEFD)) 
    \slv_reg_array[0][31]_i_4 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[5]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[0][31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[0][31]_i_5 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8000000080008000)) 
    \slv_reg_array[0][31]_i_6 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_wvalid),
        .I4(axibusdomain_s_axi_wstrb[3]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][31]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][3]_i_1 
       (.I0(axibusdomain_s_axi_wdata[3]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][4]_i_1 
       (.I0(axibusdomain_s_axi_wdata[4]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][5]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wdata[5]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][6]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wdata[6]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[0][7]_i_1 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][7]_i_4_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[0][7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[0][7]_i_2 
       (.I0(\slv_reg_array[0][31]_i_4_n_0 ),
        .I1(\slv_reg_array[0][7]_i_4_n_0 ),
        .O(\slv_reg_array[0][7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][7]_i_3 
       (.I0(axibusdomain_s_axi_wdata[7]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[0][7]_i_5_n_0 ),
        .O(\slv_reg_array[0][7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg_array[0][7]_i_4 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000080008000)) 
    \slv_reg_array[0][7]_i_5 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axibusdomain_s_axi_wvalid),
        .I4(axibusdomain_s_axi_wstrb[0]),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[0][7]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][8]_i_1 
       (.I0(axibusdomain_s_axi_wdata[8]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \slv_reg_array[0][9]_i_1 
       (.I0(axibusdomain_s_axi_wdata[9]),
        .I1(axibusdomain_s_axi_wstrb[1]),
        .I2(\slv_reg_array[0][15]_i_5_n_0 ),
        .O(\slv_reg_array[0][9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4FFF404040004040)) 
    \slv_reg_array[1][0]_i_1 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(\slv_reg_array[1][0]_i_2_n_0 ),
        .I2(\slv_reg_array[1][31]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(i[0]),
        .O(\slv_reg_array[1][0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[1][0]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(axibusdomain_s_axi_wdata[0]),
        .O(\slv_reg_array[1][0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFDD0200)) 
    \slv_reg_array[1][10]_i_1 
       (.I0(\slv_reg_array[1][31]_i_3_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(\slv_reg_array[1][10]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[10]),
        .I4(i[10]),
        .O(\slv_reg_array[1][10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \slv_reg_array[1][10]_i_2 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[5]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[1][10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][11]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[11]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][12]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[12]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][13]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[13]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][14]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[14]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][14]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[1][15]_i_1 
       (.I0(\slv_reg_array[1][31]_i_3_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .O(\slv_reg_array[1][15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][15]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[15]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][16]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[16]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][17]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[17]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][18]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[18]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][19]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[19]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][1]_i_1 
       (.I0(axibusdomain_s_axi_wdata[1]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][20]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[20]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][21]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[21]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][22]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[22]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][22]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[1][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .O(\slv_reg_array[1][23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][23]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[2]),
        .I1(axibusdomain_s_axi_wdata[23]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][24]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[24]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][25]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[25]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][26]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[26]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][27]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[27]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][28]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[28]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][29]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[29]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][29]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \slv_reg_array[1][2]_i_1 
       (.I0(axibusdomain_s_axi_wdata[2]),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .I2(\slv_reg_array[0][7]_i_4_n_0 ),
        .I3(i[2]),
        .O(\slv_reg_array[1][2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][30]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[30]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][30]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[1][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .O(\slv_reg_array[1][31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][31]_i_2 
       (.I0(axibusdomain_s_axi_wstrb[3]),
        .I1(axibusdomain_s_axi_wdata[31]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \slv_reg_array[1][31]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[5]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[1][31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFFFFFFFFFF)) 
    \slv_reg_array[1][31]_i_4 
       (.I0(\slv_reg_array[1][10]_i_2_n_0 ),
        .I1(axibusdomain_s_axi_wvalid),
        .I2(axibusdomain_s_axi_awvalid),
        .I3(axi_wready_reg_0),
        .I4(axi_awready_reg_0),
        .I5(axibusdomain_aresetn),
        .O(\slv_reg_array[1][31]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][3]_i_1 
       (.I0(axibusdomain_s_axi_wdata[3]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][4]_i_1 
       (.I0(axibusdomain_s_axi_wdata[4]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \slv_reg_array[1][5]_i_1 
       (.I0(axibusdomain_s_axi_wdata[5]),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .I2(\slv_reg_array[0][7]_i_4_n_0 ),
        .I3(i[5]),
        .O(\slv_reg_array[1][5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \slv_reg_array[1][6]_i_1 
       (.I0(axibusdomain_s_axi_wdata[6]),
        .I1(\slv_reg_array[1][31]_i_3_n_0 ),
        .I2(\slv_reg_array[0][7]_i_4_n_0 ),
        .I3(i[6]),
        .O(\slv_reg_array[1][6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \slv_reg_array[1][7]_i_1 
       (.I0(\slv_reg_array[1][31]_i_3_n_0 ),
        .I1(\slv_reg_array[0][7]_i_4_n_0 ),
        .O(\slv_reg_array[1][7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][7]_i_2 
       (.I0(axibusdomain_s_axi_wdata[7]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg_array[1][8]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[1]),
        .I1(axibusdomain_s_axi_wdata[8]),
        .I2(\slv_reg_array[1][31]_i_4_n_0 ),
        .O(\slv_reg_array[1][8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hDFDD0200)) 
    \slv_reg_array[1][9]_i_1 
       (.I0(\slv_reg_array[1][31]_i_3_n_0 ),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(\slv_reg_array[1][10]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[9]),
        .I4(i[9]),
        .O(\slv_reg_array[1][9]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEFFFEF00)) 
    \slv_reg_array[2][0]_i_1 
       (.I0(\slv_reg_array[2][6]_i_3_n_0 ),
        .I1(axibusdomain_s_axi_wdata[0]),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(\slv_reg_array[2][0]_i_2_n_0 ),
        .I4(d),
        .O(\slv_reg_array[2][0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h4F)) 
    \slv_reg_array[2][0]_i_2 
       (.I0(\slv_reg_array[2][31]_i_2_n_0 ),
        .I1(\slv_reg_array[0][7]_i_5_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[2][0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFEE0100)) 
    \slv_reg_array[2][10]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[1][10]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[10]),
        .I4(r5_enable[10]),
        .O(\slv_reg_array[2][10]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[2][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .O(\slv_reg_array[2][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[2][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .O(\slv_reg_array[2][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDDDDFDDD00002000)) 
    \slv_reg_array[2][2]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[2]),
        .I4(\slv_reg_array[2][6]_i_3_n_0 ),
        .I5(r5_enable[2]),
        .O(\slv_reg_array[2][2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[2][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .O(\slv_reg_array[2][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    \slv_reg_array[2][31]_i_2 
       (.I0(axi_awaddr[3]),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[5]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[1]),
        .I5(axi_awaddr[2]),
        .O(\slv_reg_array[2][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hDDDDFDDD00002000)) 
    \slv_reg_array[2][5]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[5]),
        .I4(\slv_reg_array[2][6]_i_3_n_0 ),
        .I5(r5_enable[5]),
        .O(\slv_reg_array[2][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDDDDFDDD00002000)) 
    \slv_reg_array[2][6]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[2][6]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[6]),
        .I4(\slv_reg_array[2][6]_i_3_n_0 ),
        .I5(r5_enable[6]),
        .O(\slv_reg_array[2][6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \slv_reg_array[2][6]_i_2 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[1]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[5]),
        .O(\slv_reg_array[2][6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000F0001000A)) 
    \slv_reg_array[2][6]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[1]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[5]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[2][6]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[2][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .O(\slv_reg_array[2][7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEFEE0100)) 
    \slv_reg_array[2][9]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[2][31]_i_2_n_0 ),
        .I2(\slv_reg_array[1][10]_i_2_n_0 ),
        .I3(axibusdomain_s_axi_wdata[9]),
        .I4(r5_enable[9]),
        .O(\slv_reg_array[2][9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF4FF040404000404)) 
    \slv_reg_array[3][0]_i_1 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(\slv_reg_array[1][0]_i_2_n_0 ),
        .I2(\slv_reg_array[3][7]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[3][15]_0 [0]),
        .O(\slv_reg_array[3][0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFE02)) 
    \slv_reg_array[3][10]_i_1 
       (.I0(axibusdomain_s_axi_wdata[10]),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(\slv_reg_array[3][7]_i_3_n_0 ),
        .I3(\slv_reg_array_reg[3][15]_0 [10]),
        .O(\slv_reg_array[3][10]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[3][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[3][7]_i_3_n_0 ),
        .O(\slv_reg_array[3][15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \slv_reg_array[3][1]_i_1 
       (.I0(axibusdomain_s_axi_wstrb[0]),
        .I1(\slv_reg_array[1][31]_i_4_n_0 ),
        .I2(axibusdomain_s_axi_wdata[1]),
        .O(\slv_reg_array[3][1]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[3][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[3][7]_i_3_n_0 ),
        .O(\slv_reg_array[3][23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    \slv_reg_array[3][2]_i_1 
       (.I0(axibusdomain_s_axi_wdata[2]),
        .I1(axi_awaddr[3]),
        .I2(\slv_reg_array[3][6]_i_2_n_0 ),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg_array[3][2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[3][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[3][7]_i_3_n_0 ),
        .O(\slv_reg_array[3][31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][3]_i_1 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(axibusdomain_s_axi_wdata[3]),
        .O(\slv_reg_array[3][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][4]_i_1 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(axibusdomain_s_axi_wdata[4]),
        .O(\slv_reg_array[3][4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    \slv_reg_array[3][5]_i_1 
       (.I0(axibusdomain_s_axi_wdata[5]),
        .I1(axi_awaddr[3]),
        .I2(\slv_reg_array[3][6]_i_2_n_0 ),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg_array[3][5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    \slv_reg_array[3][6]_i_1 
       (.I0(axibusdomain_s_axi_wdata[6]),
        .I1(axi_awaddr[3]),
        .I2(\slv_reg_array[3][6]_i_2_n_0 ),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg_array[3][6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \slv_reg_array[3][6]_i_2 
       (.I0(axi_awaddr[1]),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[5]),
        .O(\slv_reg_array[3][6]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[3][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[3][7]_i_3_n_0 ),
        .O(\slv_reg_array[3][7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \slv_reg_array[3][7]_i_2 
       (.I0(\slv_reg_array[1][31]_i_4_n_0 ),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(axibusdomain_s_axi_wdata[7]),
        .O(\slv_reg_array[3][7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFDFFFFFFFF)) 
    \slv_reg_array[3][7]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[5]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[1]),
        .I4(axi_awaddr[4]),
        .I5(axi_awaddr[3]),
        .O(\slv_reg_array[3][7]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFE02)) 
    \slv_reg_array[3][9]_i_1 
       (.I0(axibusdomain_s_axi_wdata[9]),
        .I1(\slv_reg_array[0][15]_i_4_n_0 ),
        .I2(\slv_reg_array[3][7]_i_3_n_0 ),
        .I3(\slv_reg_array_reg[3][15]_0 [9]),
        .O(\slv_reg_array[3][9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF8FF000008000000)) 
    \slv_reg_array[4][0]_i_1 
       (.I0(axibusdomain_s_axi_wdata[0]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[4][31]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(r3_dina[0]),
        .O(\slv_reg_array[4][0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \slv_reg_array[4][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[4][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[4][15]_i_2 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][15]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[4][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[4][23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[4][23]_i_2 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][23]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[4][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[4][31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[4][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \slv_reg_array[4][31]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[1]),
        .I2(axi_awaddr[0]),
        .I3(axi_awaddr[5]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[4][31]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[4][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[4][7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[4][7]_i_2 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[4][31]_i_3_n_0 ),
        .O(\slv_reg_array[4][7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF8FF000008000000)) 
    \slv_reg_array[5][0]_i_1 
       (.I0(axibusdomain_s_axi_wdata[0]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[5][31]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[5][0]_0 ),
        .O(\slv_reg_array[5][0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \slv_reg_array[5][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[5][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[5][15]_i_2 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .O(\slv_reg_array[5][15]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[5][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[5][23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[5][23]_i_2 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .O(\slv_reg_array[5][23]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[5][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[5][31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[5][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .O(\slv_reg_array[5][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFFFFFFFFFF)) 
    \slv_reg_array[5][31]_i_3 
       (.I0(axi_awaddr[1]),
        .I1(axi_awaddr[0]),
        .I2(axi_awaddr[5]),
        .I3(axi_awaddr[4]),
        .I4(axi_awaddr[3]),
        .I5(axi_awaddr[2]),
        .O(\slv_reg_array[5][31]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[5][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[5][7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[5][7]_i_2 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[5][31]_i_3_n_0 ),
        .O(\slv_reg_array[5][7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF8FF000008000000)) 
    \slv_reg_array[6][0]_i_1 
       (.I0(axibusdomain_s_axi_wdata[0]),
        .I1(axibusdomain_s_axi_wstrb[0]),
        .I2(\slv_reg_array[6][31]_i_3_n_0 ),
        .I3(\slv_reg_array[0][7]_i_5_n_0 ),
        .I4(axibusdomain_aresetn),
        .I5(\slv_reg_array_reg[6][10]_0 [0]),
        .O(\slv_reg_array[6][0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \slv_reg_array[6][15]_i_1 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[6][15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \slv_reg_array[6][15]_i_2 
       (.I0(\slv_reg_array[0][15]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .O(\slv_reg_array[6][15]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[6][23]_i_1 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[6][23]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[6][23]_i_2 
       (.I0(\slv_reg_array[0][23]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .O(\slv_reg_array[6][23]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[6][31]_i_1 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[6][31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[6][31]_i_2 
       (.I0(\slv_reg_array[0][31]_i_5_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .O(\slv_reg_array[6][31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFBFFFFFFFF)) 
    \slv_reg_array[6][31]_i_3 
       (.I0(axi_awaddr[2]),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[1]),
        .I3(axi_awaddr[0]),
        .I4(axi_awaddr[5]),
        .I5(axi_awaddr[4]),
        .O(\slv_reg_array[6][31]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \slv_reg_array[6][7]_i_1 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .I2(axibusdomain_aresetn),
        .O(\slv_reg_array[6][7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \slv_reg_array[6][7]_i_2 
       (.I0(\slv_reg_array[0][7]_i_4_n_0 ),
        .I1(\slv_reg_array[6][31]_i_3_n_0 ),
        .O(\slv_reg_array[6][7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[0][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[0][0]_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][10]_i_1_n_0 ),
        .Q(r7_clear[10]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][11]_i_1_n_0 ),
        .Q(r7_clear[11]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][12]_i_1_n_0 ),
        .Q(r7_clear[12]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][13]_i_1_n_0 ),
        .Q(r7_clear[13]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][14]_i_1_n_0 ),
        .Q(r7_clear[14]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][15]_i_3_n_0 ),
        .Q(r7_clear[15]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][16]_i_1_n_0 ),
        .Q(r7_clear[16]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][17]_i_1_n_0 ),
        .Q(r7_clear[17]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][18]_i_1_n_0 ),
        .Q(r7_clear[18]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][19]_i_1_n_0 ),
        .Q(r7_clear[19]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][1]_i_1_n_0 ),
        .Q(r7_clear[1]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][20]_i_1_n_0 ),
        .Q(r7_clear[20]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][21]_i_1_n_0 ),
        .Q(r7_clear[21]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][22]_i_1_n_0 ),
        .Q(r7_clear[22]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][23]_i_3_n_0 ),
        .Q(r7_clear[23]),
        .R(\slv_reg_array[0][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][24]_i_1_n_0 ),
        .Q(r7_clear[24]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][25]_i_1_n_0 ),
        .Q(r7_clear[25]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][26]_i_1_n_0 ),
        .Q(r7_clear[26]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][27]_i_1_n_0 ),
        .Q(r7_clear[27]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][28]_i_1_n_0 ),
        .Q(r7_clear[28]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][29]_i_1_n_0 ),
        .Q(r7_clear[29]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][2]_i_1_n_0 ),
        .Q(r7_clear[2]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][30]_i_1_n_0 ),
        .Q(r7_clear[30]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][31]_i_3_n_0 ),
        .Q(r7_clear[31]),
        .R(\slv_reg_array[0][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][3]_i_1_n_0 ),
        .Q(r7_clear[3]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][4]_i_1_n_0 ),
        .Q(r7_clear[4]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][5]_i_1_n_0 ),
        .Q(r7_clear[5]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][6]_i_1_n_0 ),
        .Q(r7_clear[6]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][7]_i_3_n_0 ),
        .Q(r7_clear[7]),
        .R(\slv_reg_array[0][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][8]_i_1_n_0 ),
        .Q(r7_clear[8]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[0][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[0][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][9]_i_1_n_0 ),
        .Q(r7_clear[9]),
        .R(\slv_reg_array[0][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][0]_i_1_n_0 ),
        .Q(i[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][10] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][10]_i_1_n_0 ),
        .Q(i[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][11]_i_1_n_0 ),
        .Q(r6_delay[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][12]_i_1_n_0 ),
        .Q(r6_delay[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][13]_i_1_n_0 ),
        .Q(r6_delay[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][14]_i_1_n_0 ),
        .Q(r6_delay[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][15]_i_2_n_0 ),
        .Q(r6_delay[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][16]_i_1_n_0 ),
        .Q(r6_delay[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][17]_i_1_n_0 ),
        .Q(r6_delay[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][18]_i_1_n_0 ),
        .Q(r6_delay[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][19]_i_1_n_0 ),
        .Q(r6_delay[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][1]_i_1_n_0 ),
        .Q(i[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][20]_i_1_n_0 ),
        .Q(r6_delay[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][21]_i_1_n_0 ),
        .Q(r6_delay[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][22]_i_1_n_0 ),
        .Q(r6_delay[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][23]_i_2_n_0 ),
        .Q(r6_delay[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][24]_i_1_n_0 ),
        .Q(r6_delay[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][25]_i_1_n_0 ),
        .Q(r6_delay[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][26]_i_1_n_0 ),
        .Q(r6_delay[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][27]_i_1_n_0 ),
        .Q(r6_delay[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][28]_i_1_n_0 ),
        .Q(r6_delay[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][29]_i_1_n_0 ),
        .Q(r6_delay[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][2] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][2]_i_1_n_0 ),
        .Q(i[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][30]_i_1_n_0 ),
        .Q(r6_delay[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][31]_i_2_n_0 ),
        .Q(r6_delay[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][3]_i_1_n_0 ),
        .Q(i[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][4]_i_1_n_0 ),
        .Q(i[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][5] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][5]_i_1_n_0 ),
        .Q(i[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[1][6] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][6]_i_1_n_0 ),
        .Q(i[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][7]_i_2_n_0 ),
        .Q(i[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[1][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][8]_i_1_n_0 ),
        .Q(i[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[1][9] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[1][9]_i_1_n_0 ),
        .Q(i[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[2][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][0]_i_1_n_0 ),
        .Q(d),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][10] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][10]_i_1_n_0 ),
        .Q(r5_enable[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][11]_i_1_n_0 ),
        .Q(r5_enable[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][12]_i_1_n_0 ),
        .Q(r5_enable[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][13]_i_1_n_0 ),
        .Q(r5_enable[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][14]_i_1_n_0 ),
        .Q(r5_enable[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][15]_i_2_n_0 ),
        .Q(r5_enable[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][16]_i_1_n_0 ),
        .Q(r5_enable[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][17]_i_1_n_0 ),
        .Q(r5_enable[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][18]_i_1_n_0 ),
        .Q(r5_enable[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][19]_i_1_n_0 ),
        .Q(r5_enable[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][1]_i_1_n_0 ),
        .Q(r5_enable[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][20]_i_1_n_0 ),
        .Q(r5_enable[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][21]_i_1_n_0 ),
        .Q(r5_enable[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][22]_i_1_n_0 ),
        .Q(r5_enable[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][23]_i_2_n_0 ),
        .Q(r5_enable[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][24]_i_1_n_0 ),
        .Q(r5_enable[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][25]_i_1_n_0 ),
        .Q(r5_enable[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][26]_i_1_n_0 ),
        .Q(r5_enable[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][27]_i_1_n_0 ),
        .Q(r5_enable[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][28]_i_1_n_0 ),
        .Q(r5_enable[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][29]_i_1_n_0 ),
        .Q(r5_enable[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][2] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][2]_i_1_n_0 ),
        .Q(r5_enable[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][30]_i_1_n_0 ),
        .Q(r5_enable[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][31]_i_2_n_0 ),
        .Q(r5_enable[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][3]_i_1_n_0 ),
        .Q(r5_enable[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][4]_i_1_n_0 ),
        .Q(r5_enable[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][5] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][5]_i_1_n_0 ),
        .Q(r5_enable[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][6] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][6]_i_1_n_0 ),
        .Q(r5_enable[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][7]_i_1_n_0 ),
        .D(\slv_reg_array[1][7]_i_2_n_0 ),
        .Q(r5_enable[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[2][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][8]_i_1_n_0 ),
        .Q(r5_enable[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[2][9] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[2][9]_i_1_n_0 ),
        .Q(r5_enable[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][10] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][10]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][11]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][12]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][13]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][14]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][15]_i_2_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][16]_i_1_n_0 ),
        .Q(r4_threshold[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][17]_i_1_n_0 ),
        .Q(r4_threshold[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][18]_i_1_n_0 ),
        .Q(r4_threshold[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][19]_i_1_n_0 ),
        .Q(r4_threshold[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][1]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][20]_i_1_n_0 ),
        .Q(r4_threshold[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][21]_i_1_n_0 ),
        .Q(r4_threshold[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][22]_i_1_n_0 ),
        .Q(r4_threshold[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][23]_i_1_n_0 ),
        .D(\slv_reg_array[1][23]_i_2_n_0 ),
        .Q(r4_threshold[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][24]_i_1_n_0 ),
        .Q(r4_threshold[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][25]_i_1_n_0 ),
        .Q(r4_threshold[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][26]_i_1_n_0 ),
        .Q(r4_threshold[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][27]_i_1_n_0 ),
        .Q(r4_threshold[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][28]_i_1_n_0 ),
        .Q(r4_threshold[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][29]_i_1_n_0 ),
        .Q(r4_threshold[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][2]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][30]_i_1_n_0 ),
        .Q(r4_threshold[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][31]_i_1_n_0 ),
        .D(\slv_reg_array[1][31]_i_2_n_0 ),
        .Q(r4_threshold[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][3]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][4]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][5]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][6]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][7]_i_1_n_0 ),
        .D(\slv_reg_array[3][7]_i_2_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[3][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[3][15]_i_1_n_0 ),
        .D(\slv_reg_array[1][8]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \slv_reg_array_reg[3][9] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[3][9]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[3][15]_0 [9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[4][0]_i_1_n_0 ),
        .Q(r3_dina[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][10]_i_1_n_0 ),
        .Q(r3_dina[10]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][11]_i_1_n_0 ),
        .Q(r3_dina[11]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][12]_i_1_n_0 ),
        .Q(r3_dina[12]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][13]_i_1_n_0 ),
        .Q(r3_dina[13]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][14]_i_1_n_0 ),
        .Q(r3_dina[14]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][15]_i_3_n_0 ),
        .Q(r3_dina[15]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][16]_i_1_n_0 ),
        .Q(r3_dina[16]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][17]_i_1_n_0 ),
        .Q(r3_dina[17]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][18]_i_1_n_0 ),
        .Q(r3_dina[18]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][19]_i_1_n_0 ),
        .Q(r3_dina[19]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][1]_i_1_n_0 ),
        .Q(r3_dina[1]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][20]_i_1_n_0 ),
        .Q(r3_dina[20]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][21]_i_1_n_0 ),
        .Q(r3_dina[21]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][22]_i_1_n_0 ),
        .Q(r3_dina[22]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][23]_i_3_n_0 ),
        .Q(r3_dina[23]),
        .R(\slv_reg_array[4][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][24]_i_1_n_0 ),
        .Q(r3_dina[24]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][25]_i_1_n_0 ),
        .Q(r3_dina[25]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][26]_i_1_n_0 ),
        .Q(r3_dina[26]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][27]_i_1_n_0 ),
        .Q(r3_dina[27]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][28]_i_1_n_0 ),
        .Q(r3_dina[28]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][29]_i_1_n_0 ),
        .Q(r3_dina[29]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][2]_i_1_n_0 ),
        .Q(r3_dina[2]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][30]_i_1_n_0 ),
        .Q(r3_dina[30]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][31]_i_3_n_0 ),
        .Q(r3_dina[31]),
        .R(\slv_reg_array[4][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][3]_i_1_n_0 ),
        .Q(r3_dina[3]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][4]_i_1_n_0 ),
        .Q(r3_dina[4]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][5]_i_1_n_0 ),
        .Q(r3_dina[5]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][6]_i_1_n_0 ),
        .Q(r3_dina[6]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][7]_i_3_n_0 ),
        .Q(r3_dina[7]),
        .R(\slv_reg_array[4][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][8]_i_1_n_0 ),
        .Q(r3_dina[8]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[4][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[4][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][9]_i_1_n_0 ),
        .Q(r3_dina[9]),
        .R(\slv_reg_array[4][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[5][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[5][0]_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][10]_i_1_n_0 ),
        .Q(r2_wea[10]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][11]_i_1_n_0 ),
        .Q(r2_wea[11]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][12]_i_1_n_0 ),
        .Q(r2_wea[12]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][13]_i_1_n_0 ),
        .Q(r2_wea[13]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][14]_i_1_n_0 ),
        .Q(r2_wea[14]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][15]_i_3_n_0 ),
        .Q(r2_wea[15]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][16]_i_1_n_0 ),
        .Q(r2_wea[16]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][17]_i_1_n_0 ),
        .Q(r2_wea[17]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][18]_i_1_n_0 ),
        .Q(r2_wea[18]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][19]_i_1_n_0 ),
        .Q(r2_wea[19]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][1]_i_1_n_0 ),
        .Q(r2_wea[1]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][20]_i_1_n_0 ),
        .Q(r2_wea[20]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][21]_i_1_n_0 ),
        .Q(r2_wea[21]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][22]_i_1_n_0 ),
        .Q(r2_wea[22]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][23]_i_3_n_0 ),
        .Q(r2_wea[23]),
        .R(\slv_reg_array[5][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][24]_i_1_n_0 ),
        .Q(r2_wea[24]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][25]_i_1_n_0 ),
        .Q(r2_wea[25]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][26]_i_1_n_0 ),
        .Q(r2_wea[26]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][27]_i_1_n_0 ),
        .Q(r2_wea[27]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][28]_i_1_n_0 ),
        .Q(r2_wea[28]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][29]_i_1_n_0 ),
        .Q(r2_wea[29]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][2]_i_1_n_0 ),
        .Q(r2_wea[2]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][30]_i_1_n_0 ),
        .Q(r2_wea[30]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][31]_i_3_n_0 ),
        .Q(r2_wea[31]),
        .R(\slv_reg_array[5][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][3]_i_1_n_0 ),
        .Q(r2_wea[3]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][4]_i_1_n_0 ),
        .Q(r2_wea[4]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][5]_i_1_n_0 ),
        .Q(r2_wea[5]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][6]_i_1_n_0 ),
        .Q(r2_wea[6]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][7]_i_3_n_0 ),
        .Q(r2_wea[7]),
        .R(\slv_reg_array[5][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][8]_i_1_n_0 ),
        .Q(r2_wea[8]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[5][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[5][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][9]_i_1_n_0 ),
        .Q(r2_wea[9]),
        .R(\slv_reg_array[5][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][0] 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(\slv_reg_array[6][0]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][10] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][10]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [10]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][11] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][11]_i_1_n_0 ),
        .Q(r1_addra[11]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][12] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][12]_i_1_n_0 ),
        .Q(r1_addra[12]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][13] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][13]_i_1_n_0 ),
        .Q(r1_addra[13]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][14] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][14]_i_1_n_0 ),
        .Q(r1_addra[14]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][15] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][15]_i_3_n_0 ),
        .Q(r1_addra[15]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][16] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][16]_i_1_n_0 ),
        .Q(r1_addra[16]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][17] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][17]_i_1_n_0 ),
        .Q(r1_addra[17]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][18] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][18]_i_1_n_0 ),
        .Q(r1_addra[18]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][19] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][19]_i_1_n_0 ),
        .Q(r1_addra[19]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][1] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][1]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [1]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][20] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][20]_i_1_n_0 ),
        .Q(r1_addra[20]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][21] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][21]_i_1_n_0 ),
        .Q(r1_addra[21]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][22] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][22]_i_1_n_0 ),
        .Q(r1_addra[22]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][23] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][23]_i_2_n_0 ),
        .D(\slv_reg_array[0][23]_i_3_n_0 ),
        .Q(r1_addra[23]),
        .R(\slv_reg_array[6][23]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][24] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][24]_i_1_n_0 ),
        .Q(r1_addra[24]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][25] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][25]_i_1_n_0 ),
        .Q(r1_addra[25]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][26] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][26]_i_1_n_0 ),
        .Q(r1_addra[26]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][27] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][27]_i_1_n_0 ),
        .Q(r1_addra[27]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][28] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][28]_i_1_n_0 ),
        .Q(r1_addra[28]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][29] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][29]_i_1_n_0 ),
        .Q(r1_addra[29]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][2] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][2]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [2]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][30] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][30]_i_1_n_0 ),
        .Q(r1_addra[30]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][31] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][31]_i_2_n_0 ),
        .D(\slv_reg_array[0][31]_i_3_n_0 ),
        .Q(r1_addra[31]),
        .R(\slv_reg_array[6][31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][3] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][3]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [3]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][4] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][4]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [4]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][5] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][5]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [5]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][6] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][6]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [6]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][7] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][7]_i_2_n_0 ),
        .D(\slv_reg_array[0][7]_i_3_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [7]),
        .R(\slv_reg_array[6][7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][8] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][8]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [8]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slv_reg_array_reg[6][9] 
       (.C(axibusdomain_clk),
        .CE(\slv_reg_array[6][15]_i_2_n_0 ),
        .D(\slv_reg_array[0][9]_i_1_n_0 ),
        .Q(\slv_reg_array_reg[6][10]_0 [9]),
        .R(\slv_reg_array[6][15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(axibusdomain_s_axi_arvalid),
        .I1(axibusdomain_s_axi_rvalid),
        .I2(axi_arready_reg_0),
        .O(slv_reg_rden__0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ip_scope_0_0,ip_scope,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "sysgen" *) 
(* X_CORE_INFO = "ip_scope,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ch1,
    ch2,
    ch_trigger,
    axibusdomain_clk,
    signaldomain_clk,
    axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready,
    full,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 ch1 DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ch1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}" *) input [15:0]ch1;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 ch2 DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ch2, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}" *) input [15:0]ch2;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 ch_trigger DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ch_trigger, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 14} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}" *) input [15:0]ch_trigger;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 axibusdomain_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_clk, ASSOCIATED_RESET axibusdomain_aresetn, ASSOCIATED_BUSIF axibusdomain_s_axi, FREQ_HZ 120000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input axibusdomain_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signaldomain_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME signaldomain_clk, FREQ_HZ 50000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input signaldomain_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 axibusdomain_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input axibusdomain_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWADDR" *) input [5:0]axibusdomain_s_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWVALID" *) input axibusdomain_s_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WDATA" *) input [31:0]axibusdomain_s_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WSTRB" *) input [3:0]axibusdomain_s_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WVALID" *) input axibusdomain_s_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BREADY" *) input axibusdomain_s_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARADDR" *) input [5:0]axibusdomain_s_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARVALID" *) input axibusdomain_s_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RREADY" *) input axibusdomain_s_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 full INTERRUPT" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME full, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output [0:0]full;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi AWREADY" *) output axibusdomain_s_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi WREADY" *) output axibusdomain_s_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BRESP" *) output [1:0]axibusdomain_s_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi BVALID" *) output axibusdomain_s_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi ARREADY" *) output axibusdomain_s_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RDATA" *) output [31:0]axibusdomain_s_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RRESP" *) output [1:0]axibusdomain_s_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axibusdomain_s_axi RVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axibusdomain_s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 120000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output axibusdomain_s_axi_rvalid;

  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [5:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [5:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire [1:0]axibusdomain_s_axi_bresp;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire [1:0]axibusdomain_s_axi_rresp;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [15:0]ch1;
  wire [15:0]ch2;
  wire [15:0]ch_trigger;
  wire [0:0]full;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope inst
       (.axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_arready),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_awready),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_bready),
        .axibusdomain_s_axi_bresp(axibusdomain_s_axi_bresp),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_rready),
        .axibusdomain_s_axi_rresp(axibusdomain_s_axi_rresp),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_wready),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .ch1(ch1),
        .ch2(ch2),
        .ch_trigger(ch_trigger),
        .full(full),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope
   (ch1,
    ch2,
    ch_trigger,
    axibusdomain_clk,
    signaldomain_clk,
    axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready,
    full,
    axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid);
  input [15:0]ch1;
  input [15:0]ch2;
  input [15:0]ch_trigger;
  input axibusdomain_clk;
  input signaldomain_clk;
  input axibusdomain_aresetn;
  input [5:0]axibusdomain_s_axi_awaddr;
  input axibusdomain_s_axi_awvalid;
  input [31:0]axibusdomain_s_axi_wdata;
  input [3:0]axibusdomain_s_axi_wstrb;
  input axibusdomain_s_axi_wvalid;
  input axibusdomain_s_axi_bready;
  input [5:0]axibusdomain_s_axi_araddr;
  input axibusdomain_s_axi_arvalid;
  input axibusdomain_s_axi_rready;
  output [0:0]full;
  output axibusdomain_s_axi_awready;
  output axibusdomain_s_axi_wready;
  output [1:0]axibusdomain_s_axi_bresp;
  output axibusdomain_s_axi_bvalid;
  output axibusdomain_s_axi_arready;
  output [31:0]axibusdomain_s_axi_rdata;
  output [1:0]axibusdomain_s_axi_rresp;
  output axibusdomain_s_axi_rvalid;

  wire \<const0> ;
  wire axibusdomain_aresetn;
  wire axibusdomain_clk;
  wire [5:0]axibusdomain_s_axi_araddr;
  wire axibusdomain_s_axi_arready;
  wire axibusdomain_s_axi_arvalid;
  wire [5:0]axibusdomain_s_axi_awaddr;
  wire axibusdomain_s_axi_awready;
  wire axibusdomain_s_axi_awvalid;
  wire axibusdomain_s_axi_bready;
  wire axibusdomain_s_axi_bvalid;
  wire [31:0]axibusdomain_s_axi_rdata;
  wire axibusdomain_s_axi_rready;
  wire axibusdomain_s_axi_rvalid;
  wire [31:0]axibusdomain_s_axi_wdata;
  wire axibusdomain_s_axi_wready;
  wire [3:0]axibusdomain_s_axi_wstrb;
  wire axibusdomain_s_axi_wvalid;
  wire [15:0]ch1;
  wire [15:0]ch2;
  wire [15:0]ch_trigger;
  wire [0:0]full;
  wire [10:0]r1_addra;
  wire [0:0]r2_wea;
  wire [31:0]r3_dina;
  wire [15:0]r4_threshold;
  wire [0:0]r5_enable;
  wire [10:0]r6_delay;
  wire [0:0]r7_clear;
  wire [0:0]r8_full;
  wire [31:0]r9_douta;
  wire signaldomain_clk;

  assign axibusdomain_s_axi_bresp[1] = \<const0> ;
  assign axibusdomain_s_axi_bresp[0] = \<const0> ;
  assign axibusdomain_s_axi_rresp[1] = \<const0> ;
  assign axibusdomain_s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axibusdomain_axi_lite_interface axibusdomain_axi_lite_interface
       (.axibusdomain_aresetn(axibusdomain_aresetn),
        .axibusdomain_clk(axibusdomain_clk),
        .axibusdomain_s_axi_araddr(axibusdomain_s_axi_araddr),
        .axibusdomain_s_axi_arready(axibusdomain_s_axi_arready),
        .axibusdomain_s_axi_arvalid(axibusdomain_s_axi_arvalid),
        .axibusdomain_s_axi_awaddr(axibusdomain_s_axi_awaddr),
        .axibusdomain_s_axi_awready(axibusdomain_s_axi_awready),
        .axibusdomain_s_axi_awvalid(axibusdomain_s_axi_awvalid),
        .axibusdomain_s_axi_bready(axibusdomain_s_axi_bready),
        .axibusdomain_s_axi_bvalid(axibusdomain_s_axi_bvalid),
        .axibusdomain_s_axi_rdata(axibusdomain_s_axi_rdata),
        .axibusdomain_s_axi_rready(axibusdomain_s_axi_rready),
        .axibusdomain_s_axi_rvalid(axibusdomain_s_axi_rvalid),
        .axibusdomain_s_axi_wdata(axibusdomain_s_axi_wdata),
        .axibusdomain_s_axi_wready(axibusdomain_s_axi_wready),
        .axibusdomain_s_axi_wstrb(axibusdomain_s_axi_wstrb),
        .axibusdomain_s_axi_wvalid(axibusdomain_s_axi_wvalid),
        .d(r5_enable),
        .i(r6_delay),
        .q(r8_full),
        .r3_dina(r3_dina),
        .r9_douta(r9_douta),
        .\slv_reg_array_reg[0][0] (r7_clear),
        .\slv_reg_array_reg[3][15] (r4_threshold),
        .\slv_reg_array_reg[5][0] (r2_wea),
        .\slv_reg_array_reg[6][10] (r1_addra));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct ip_scope_struct
       (.axibusdomain_clk(axibusdomain_clk),
        .ch1(ch1),
        .ch2(ch2),
        .ch_trigger(ch_trigger),
        .full(full),
        .r1_addra(r1_addra),
        .r2_wea(r2_wea),
        .r3_dina(r3_dina),
        .r4_threshold(r4_threshold),
        .r5_enable(r5_enable),
        .r6_delay(r6_delay),
        .r7_clear(r7_clear),
        .r8_full(r8_full),
        .r9_douta(r9_douta),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* CHECK_LICENSE_TYPE = "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0
   (clka,
    ena,
    wea,
    addra,
    dina,
    douta,
    clkb,
    enb,
    web,
    addrb,
    dinb,
    doutb);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB EN" *) input enb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) input [0:0]web;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [10:0]addrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) input [31:0]dinb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [31:0]doutb;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "2" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     10.698 mW" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "1" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ip_scope_blk_mem_gen_i0.mem" *) 
  (* C_INIT_FILE_NAME = "ip_scope_blk_mem_gen_i0.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "2" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "2048" *) 
  (* C_READ_DEPTH_B = "2048" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "2048" *) 
  (* C_WRITE_DEPTH_B = "2048" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_family = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(enb),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(web));
endmodule

(* CHECK_LICENSE_TYPE = "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_3,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1
   (clka,
    ena,
    wea,
    addra,
    dina,
    douta);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [10:0]addra;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [10:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [10:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "11" *) 
  (* C_ADDRB_WIDTH = "11" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "2" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     5.6824 mW" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "ip_scope_blk_mem_gen_i1.mem" *) 
  (* C_INIT_FILE_NAME = "ip_scope_blk_mem_gen_i1.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "2048" *) 
  (* C_READ_DEPTH_B = "2048" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "2048" *) 
  (* C_WRITE_DEPTH_B = "2048" *) 
  (* C_WRITE_MODE_A = "READ_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_family = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[10:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[10:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "c_counter_binary_v12_0_13,Vivado 2019.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0
   (CLK,
    CE,
    SINIT,
    LOAD,
    L,
    Q);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 sinit_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef" *) input SINIT;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [10:0]L;
  (* X_INTERFACE_INFO = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [10:0]Q;

  wire CE;
  wire CLK;
  wire [10:0]L;
  wire LOAD;
  wire [10:0]Q;
  wire SINIT;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "11" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline
   (dinb,
    signaldomain_clk,
    dina,
    o);
  output [31:0]dinb;
  input signaldomain_clk;
  input [31:0]dina;
  input [10:0]o;

  wire [10:0]counter_op_net;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [10:0]o;
  wire relational_op_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free counter
       (.LOAD(relational_op_net),
        .Q(counter_op_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88 relational
       (.LOAD(relational_op_net),
        .Q(counter_op_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram single_port_ram
       (.Q(counter_op_net),
        .dina(dina),
        .dinb(dinb),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller
   (\full_i_5_24_reg[0] ,
    web,
    addrb,
    signaldomain_clk,
    DI,
    S,
    \state_4_23_reg[1] ,
    \state_4_23_reg[1]_0 ,
    register_q_net,
    \state_4_23_reg[1]_1 ,
    register4_q_net);
  output [0:0]\full_i_5_24_reg[0] ;
  output [0:0]web;
  output [10:0]addrb;
  input signaldomain_clk;
  input [3:0]DI;
  input [3:0]S;
  input [3:0]\state_4_23_reg[1] ;
  input [3:0]\state_4_23_reg[1]_0 ;
  input register_q_net;
  input \state_4_23_reg[1]_1 ;
  input [0:0]register4_q_net;

  wire [3:0]DI;
  wire [3:0]S;
  wire [10:0]addrb;
  wire [0:0]\full_i_5_24_reg[0] ;
  wire [0:0]register4_q_net;
  wire register_q_net;
  wire signaldomain_clk;
  wire [3:0]\state_4_23_reg[1] ;
  wire [3:0]\state_4_23_reg[1]_0 ;
  wire \state_4_23_reg[1]_1 ;
  wire [0:0]web;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d mcode
       (.DI(DI),
        .S(S),
        .addrb(addrb),
        .\full_i_5_24_reg[0]_0 (\full_i_5_24_reg[0] ),
        .register4_q_net(register4_q_net),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk),
        .\state_4_23_reg[1]_0 (\state_4_23_reg[1] ),
        .\state_4_23_reg[1]_1 (\state_4_23_reg[1]_0 ),
        .\state_4_23_reg[1]_2 (\state_4_23_reg[1]_1 ),
        .web(web));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger
   (\ff_2_17_reg[0] ,
    register4_q_net,
    signaldomain_clk);
  output \ff_2_17_reg[0] ;
  input [0:0]register4_q_net;
  input signaldomain_clk;

  wire \ff_2_17_reg[0] ;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a mcode
       (.\ff_2_17_reg[0]_0 (\ff_2_17_reg[0] ),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain
   (addrb,
    dinb,
    full,
    \full_i_5_24_reg[0] ,
    web,
    signaldomain_clk,
    enable,
    ch_trigger,
    ch1,
    ch2,
    clear,
    i,
    o);
  output [10:0]addrb;
  output [31:0]dinb;
  output [0:0]full;
  output [0:0]\full_i_5_24_reg[0] ;
  output [0:0]web;
  input signaldomain_clk;
  input [0:0]enable;
  input [15:0]ch_trigger;
  input [15:0]ch1;
  input [15:0]ch2;
  input [0:0]clear;
  input [15:0]i;
  input [10:0]o;

  wire [10:0]addrb;
  wire [15:0]ch1;
  wire [15:0]ch2;
  wire [15:0]ch_trigger;
  wire [0:0]clear;
  wire [31:0]dinb;
  wire [0:0]enable;
  wire [0:0]full;
  wire [0:0]\full_i_5_24_reg[0] ;
  wire [15:0]i;
  wire [10:0]o;
  wire register1_n_0;
  wire register1_n_1;
  wire register1_n_2;
  wire register1_n_20;
  wire register1_n_21;
  wire register1_n_22;
  wire register1_n_23;
  wire register1_n_3;
  wire [15:0]register1_q_net_x0;
  wire [15:0]register2_q_net_x0;
  wire [15:0]register3_q_net_x0;
  wire [0:0]register4_q_net;
  wire register5_n_0;
  wire register5_n_1;
  wire register5_n_2;
  wire register5_n_20;
  wire register5_n_21;
  wire register5_n_22;
  wire register5_n_23;
  wire register5_n_3;
  wire [15:0]register5_q_net;
  wire register_q_net;
  wire risingedgetrigger_n_0;
  wire signaldomain_clk;
  wire [0:0]web;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0 convert
       (.full(full),
        .\reg_array[0].fde_used.u2 (\full_i_5_24_reg[0] ),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_delayline delayline
       (.dina({register2_q_net_x0,register3_q_net_x0}),
        .dinb(dinb),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_leveltriggerfifocontroller leveltriggerfifocontroller
       (.DI({register1_n_0,register1_n_1,register1_n_2,register1_n_3}),
        .S({register5_n_0,register5_n_1,register5_n_2,register5_n_3}),
        .addrb(addrb),
        .\full_i_5_24_reg[0] (\full_i_5_24_reg[0] ),
        .register4_q_net(register4_q_net),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk),
        .\state_4_23_reg[1] ({register5_n_23,register1_n_20,register1_n_21,register1_n_22}),
        .\state_4_23_reg[1]_0 ({register1_n_23,register5_n_20,register5_n_21,register5_n_22}),
        .\state_4_23_reg[1]_1 (risingedgetrigger_n_0),
        .web(web));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0 register1
       (.DI({register1_n_0,register1_n_1,register1_n_2,register1_n_3}),
        .ch_trigger(ch_trigger),
        .\fd_prim_array[13].bit_is_0.fdre_comp ({register1_n_20,register1_n_21,register1_n_22}),
        .\fd_prim_array[15].bit_is_0.fdre_comp (register1_n_23),
        .o(register1_q_net_x0),
        .rel_39_16_carry__0(register5_q_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1 register2
       (.ch1(ch1),
        .o(register2_q_net_x0),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2 register3
       (.ch2(ch2),
        .o(register3_q_net_x0),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1 register4
       (.clear(clear),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3 register5
       (.S({register5_n_0,register5_n_1,register5_n_2,register5_n_3}),
        .\fd_prim_array[13].bit_is_0.fdre_comp ({register5_n_20,register5_n_21,register5_n_22}),
        .\fd_prim_array[15].bit_is_0.fdre_comp (register5_n_23),
        .i(i),
        .o(register5_q_net),
        .rel_39_16_carry__0(register1_q_net_x0),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister register_x0
       (.enable(enable),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_risingedgetrigger risingedgetrigger
       (.\ff_2_17_reg[0] (risingedgetrigger_n_0),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_struct
   (full,
    r9_douta,
    r8_full,
    signaldomain_clk,
    ch_trigger,
    ch1,
    ch2,
    axibusdomain_clk,
    r2_wea,
    r1_addra,
    r3_dina,
    r4_threshold,
    r5_enable,
    r6_delay,
    r7_clear);
  output [0:0]full;
  output [31:0]r9_douta;
  output [0:0]r8_full;
  input signaldomain_clk;
  input [15:0]ch_trigger;
  input [15:0]ch1;
  input [15:0]ch2;
  input axibusdomain_clk;
  input [0:0]r2_wea;
  input [10:0]r1_addra;
  input [31:0]r3_dina;
  input [15:0]r4_threshold;
  input [0:0]r5_enable;
  input [10:0]r6_delay;
  input [0:0]r7_clear;

  wire axibusdomain_clk;
  wire [15:0]ch1;
  wire [15:0]ch2;
  wire [15:0]ch_trigger;
  wire [0:0]convert_dout_net_x1;
  wire [0:0]full;
  wire mcode_we_net;
  wire [10:0]r1_addra;
  wire [0:0]r2_wea;
  wire [31:0]r3_dina;
  wire [15:0]r4_threshold;
  wire [0:0]r5_enable;
  wire [10:0]r6_delay;
  wire [0:0]r7_clear;
  wire [0:0]r8_full;
  wire [31:0]r9_douta;
  wire register1_q_net;
  wire [10:0]register2_q_net;
  wire [0:0]register3_q_net;
  wire [15:0]register_q_net;
  wire signaldomain_clk;
  wire [31:0]single_port_ram_data_out_net;
  wire [10:0]slice1_y_net;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram dual_port_ram
       (.addrb(slice1_y_net),
        .axibusdomain_clk(axibusdomain_clk),
        .dinb(single_port_ram_data_out_net),
        .r1_addra(r1_addra),
        .r2_wea(r2_wea),
        .r3_dina(r3_dina),
        .r9_douta(r9_douta),
        .signaldomain_clk(signaldomain_clk),
        .web(mcode_we_net));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0 register1
       (.axibusdomain_clk(axibusdomain_clk),
        .q(register1_q_net),
        .r5_enable(r5_enable),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1 register2
       (.axibusdomain_clk(axibusdomain_clk),
        .o(register2_q_net),
        .r6_delay(r6_delay),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2 register3
       (.axibusdomain_clk(axibusdomain_clk),
        .q(register3_q_net),
        .r7_clear(r7_clear),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0 register4
       (.axibusdomain_clk(axibusdomain_clk),
        .full(convert_dout_net_x1),
        .r8_full(r8_full),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister register_x0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(register_q_net),
        .r4_threshold(r4_threshold),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_signaldomain signaldomain
       (.addrb(slice1_y_net),
        .ch1(ch1),
        .ch2(ch2),
        .ch_trigger(ch_trigger),
        .clear(register3_q_net),
        .dinb(single_port_ram_data_out_net),
        .enable(register1_q_net),
        .full(convert_dout_net_x1),
        .\full_i_5_24_reg[0] (full),
        .i(register_q_net),
        .o(register2_q_net),
        .signaldomain_clk(signaldomain_clk),
        .web(mcode_we_net));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister
   (o,
    r4_threshold,
    axibusdomain_clk,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]r4_threshold;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [15:0]d1_net;
  wire [15:0]d2_net;
  wire [15:0]d3_net;
  wire [15:0]o;
  wire [15:0]r4_threshold;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r4_threshold(r4_threshold));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13 synth_reg_inst_3
       (.i(d3_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized0
   (q,
    r5_enable,
    axibusdomain_clk,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]r5_enable;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire d1_net;
  wire d2_net;
  wire d3_net;
  wire [0:0]q;
  wire [0:0]r5_enable;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r5_enable(r5_enable));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41 synth_reg_inst_1
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42 synth_reg_inst_2
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43 synth_reg_inst_3
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized1
   (o,
    r6_delay,
    axibusdomain_clk,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]r6_delay;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [10:0]d1_net;
  wire [10:0]d2_net;
  wire [10:0]d3_net;
  wire [10:0]o;
  wire [10:0]r6_delay;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .o(d1_net),
        .r6_delay(r6_delay));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34 synth_reg_inst_1
       (.i(d1_net),
        .o(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35 synth_reg_inst_2
       (.i(d2_net),
        .o(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36 synth_reg_inst_3
       (.i(d3_net),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2
   (q,
    r7_clear,
    axibusdomain_clk,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]r7_clear;
  input axibusdomain_clk;
  input signaldomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire [0:0]q;
  wire [0:0]r7_clear;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26 synth_reg_inst_0
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r7_clear(r7_clear));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27 synth_reg_inst_1
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28 synth_reg_inst_2
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29 synth_reg_inst_3
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlAsynRegister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlAsynRegister__parameterized2_0
   (r8_full,
    full,
    signaldomain_clk,
    axibusdomain_clk);
  output [0:0]r8_full;
  input [0:0]full;
  input signaldomain_clk;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire [0:0]full;
  wire [0:0]r8_full;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18 synth_reg_inst_0
       (.d1_net(d1_net),
        .full(full),
        .signaldomain_clk(signaldomain_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19 synth_reg_inst_1
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .d2_net(d2_net));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20 synth_reg_inst_2
       (.axibusdomain_clk(axibusdomain_clk),
        .d2_net(d2_net),
        .d3_net(d3_net));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21 synth_reg_inst_3
       (.axibusdomain_clk(axibusdomain_clk),
        .d3_net(d3_net),
        .r8_full(r8_full));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlconvert" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlconvert__parameterized0
   (full,
    \reg_array[0].fde_used.u2 ,
    signaldomain_clk);
  output [0:0]full;
  input [0:0]\reg_array[0].fde_used.u2 ;
  input signaldomain_clk;

  wire [0:0]full;
  wire [0:0]\reg_array[0].fde_used.u2 ;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg \latency_test.reg 
       (.full(full),
        .\reg_array[0].fde_used.u2 (\reg_array[0].fde_used.u2 ),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlcounter_free
   (Q,
    signaldomain_clk,
    LOAD);
  output [10:0]Q;
  input signaldomain_clk;
  input LOAD;

  wire LOAD;
  wire [10:0]Q;
  wire signaldomain_clk;

  (* CHECK_LICENSE_TYPE = "ip_scope_c_counter_binary_v12_0_i0,c_counter_binary_v12_0_13,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "c_counter_binary_v12_0_13,Vivado 2019.1" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_c_counter_binary_v12_0_i0 \comp0.core_instance0 
       (.CE(1'b1),
        .CLK(signaldomain_clk),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(LOAD),
        .Q(Q),
        .SINIT(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xldpram
   (r9_douta,
    axibusdomain_clk,
    r2_wea,
    r1_addra,
    r3_dina,
    signaldomain_clk,
    web,
    addrb,
    dinb);
  output [31:0]r9_douta;
  input axibusdomain_clk;
  input [0:0]r2_wea;
  input [10:0]r1_addra;
  input [31:0]r3_dina;
  input signaldomain_clk;
  input [0:0]web;
  input [10:0]addrb;
  input [31:0]dinb;

  wire [10:0]addrb;
  wire axibusdomain_clk;
  wire \comp0.core_instance0_n_32 ;
  wire \comp0.core_instance0_n_33 ;
  wire \comp0.core_instance0_n_34 ;
  wire \comp0.core_instance0_n_35 ;
  wire \comp0.core_instance0_n_36 ;
  wire \comp0.core_instance0_n_37 ;
  wire \comp0.core_instance0_n_38 ;
  wire \comp0.core_instance0_n_39 ;
  wire \comp0.core_instance0_n_40 ;
  wire \comp0.core_instance0_n_41 ;
  wire \comp0.core_instance0_n_42 ;
  wire \comp0.core_instance0_n_43 ;
  wire \comp0.core_instance0_n_44 ;
  wire \comp0.core_instance0_n_45 ;
  wire \comp0.core_instance0_n_46 ;
  wire \comp0.core_instance0_n_47 ;
  wire \comp0.core_instance0_n_48 ;
  wire \comp0.core_instance0_n_49 ;
  wire \comp0.core_instance0_n_50 ;
  wire \comp0.core_instance0_n_51 ;
  wire \comp0.core_instance0_n_52 ;
  wire \comp0.core_instance0_n_53 ;
  wire \comp0.core_instance0_n_54 ;
  wire \comp0.core_instance0_n_55 ;
  wire \comp0.core_instance0_n_56 ;
  wire \comp0.core_instance0_n_57 ;
  wire \comp0.core_instance0_n_58 ;
  wire \comp0.core_instance0_n_59 ;
  wire \comp0.core_instance0_n_60 ;
  wire \comp0.core_instance0_n_61 ;
  wire \comp0.core_instance0_n_62 ;
  wire \comp0.core_instance0_n_63 ;
  wire [31:0]dinb;
  wire [10:0]r1_addra;
  wire [0:0]r2_wea;
  wire [31:0]r3_dina;
  wire [31:0]r9_douta;
  wire signaldomain_clk;
  wire [0:0]web;

  (* CHECK_LICENSE_TYPE = "ip_scope_blk_mem_gen_i0,blk_mem_gen_v8_4_3,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i0 \comp0.core_instance0 
       (.addra(r1_addra),
        .addrb(addrb),
        .clka(axibusdomain_clk),
        .clkb(signaldomain_clk),
        .dina(r3_dina),
        .dinb(dinb),
        .douta(r9_douta),
        .doutb({\comp0.core_instance0_n_32 ,\comp0.core_instance0_n_33 ,\comp0.core_instance0_n_34 ,\comp0.core_instance0_n_35 ,\comp0.core_instance0_n_36 ,\comp0.core_instance0_n_37 ,\comp0.core_instance0_n_38 ,\comp0.core_instance0_n_39 ,\comp0.core_instance0_n_40 ,\comp0.core_instance0_n_41 ,\comp0.core_instance0_n_42 ,\comp0.core_instance0_n_43 ,\comp0.core_instance0_n_44 ,\comp0.core_instance0_n_45 ,\comp0.core_instance0_n_46 ,\comp0.core_instance0_n_47 ,\comp0.core_instance0_n_48 ,\comp0.core_instance0_n_49 ,\comp0.core_instance0_n_50 ,\comp0.core_instance0_n_51 ,\comp0.core_instance0_n_52 ,\comp0.core_instance0_n_53 ,\comp0.core_instance0_n_54 ,\comp0.core_instance0_n_55 ,\comp0.core_instance0_n_56 ,\comp0.core_instance0_n_57 ,\comp0.core_instance0_n_58 ,\comp0.core_instance0_n_59 ,\comp0.core_instance0_n_60 ,\comp0.core_instance0_n_61 ,\comp0.core_instance0_n_62 ,\comp0.core_instance0_n_63 }),
        .ena(1'b1),
        .enb(1'b1),
        .wea(r2_wea),
        .web(web));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister
   (register_q_net,
    enable,
    signaldomain_clk);
  output register_q_net;
  input [0:0]enable;
  input signaldomain_clk;

  wire [0:0]enable;
  wire register_q_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0 synth_reg_inst
       (.enable(enable),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0
   (DI,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp ,
    \fd_prim_array[15].bit_is_0.fdre_comp ,
    rel_39_16_carry__0,
    ch_trigger,
    signaldomain_clk);
  output [3:0]DI;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]ch_trigger;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [15:0]ch_trigger;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  wire [15:0]o;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8 synth_reg_inst
       (.DI(DI),
        .ch_trigger(ch_trigger),
        .\fd_prim_array[13].bit_is_0.fdre_comp (\fd_prim_array[13].bit_is_0.fdre_comp ),
        .\fd_prim_array[15].bit_is_0.fdre_comp (\fd_prim_array[15].bit_is_0.fdre_comp ),
        .o(o),
        .rel_39_16_carry__0(rel_39_16_carry__0),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_1
   (o,
    ch1,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch1;
  input signaldomain_clk;

  wire [15:0]ch1;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6 synth_reg_inst
       (.ch1(ch1),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_2
   (o,
    ch2,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch2;
  input signaldomain_clk;

  wire [15:0]ch2;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4 synth_reg_inst
       (.ch2(ch2),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized0_3
   (S,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp ,
    \fd_prim_array[15].bit_is_0.fdre_comp ,
    rel_39_16_carry__0,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]S;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  wire [15:0]i;
  wire [15:0]o;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1 synth_reg_inst
       (.S(S),
        .\fd_prim_array[13].bit_is_0.fdre_comp (\fd_prim_array[13].bit_is_0.fdre_comp ),
        .\fd_prim_array[15].bit_is_0.fdre_comp (\fd_prim_array[15].bit_is_0.fdre_comp ),
        .i(i),
        .o(o),
        .rel_39_16_carry__0(rel_39_16_carry__0),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "ip_scope_xlregister" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlregister__parameterized1
   (register4_q_net,
    clear,
    signaldomain_clk);
  output [0:0]register4_q_net;
  input [0:0]clear;
  input signaldomain_clk;

  wire [0:0]clear;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2 synth_reg_inst
       (.clear(clear),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_xlspram
   (dinb,
    signaldomain_clk,
    Q,
    dina);
  output [31:0]dinb;
  input signaldomain_clk;
  input [10:0]Q;
  input [31:0]dina;

  wire [10:0]Q;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire signaldomain_clk;

  (* CHECK_LICENSE_TYPE = "ip_scope_blk_mem_gen_i1,blk_mem_gen_v8_4_3,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ip_scope_blk_mem_gen_i1 \comp0.core_instance0 
       (.addra(Q),
        .clka(signaldomain_clk),
        .dina(dina),
        .douta(dinb),
        .ena(1'b1),
        .wea(1'b1));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_2be3aaba4a
   (\ff_2_17_reg[0]_0 ,
    register4_q_net,
    signaldomain_clk);
  output \ff_2_17_reg[0]_0 ;
  input [0:0]register4_q_net;
  input signaldomain_clk;

  wire \ff_2_17_reg[0]_0 ;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  FDRE #(
    .INIT(1'b0)) 
    \ff_2_17_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(register4_q_net),
        .Q(\ff_2_17_reg[0]_0 ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_mcode_block_46de72b52d
   (\full_i_5_24_reg[0]_0 ,
    web,
    addrb,
    signaldomain_clk,
    DI,
    S,
    \state_4_23_reg[1]_0 ,
    \state_4_23_reg[1]_1 ,
    register_q_net,
    \state_4_23_reg[1]_2 ,
    register4_q_net);
  output [0:0]\full_i_5_24_reg[0]_0 ;
  output [0:0]web;
  output [10:0]addrb;
  input signaldomain_clk;
  input [3:0]DI;
  input [3:0]S;
  input [3:0]\state_4_23_reg[1]_0 ;
  input [3:0]\state_4_23_reg[1]_1 ;
  input register_q_net;
  input \state_4_23_reg[1]_2 ;
  input [0:0]register4_q_net;

  wire [3:0]DI;
  wire [3:0]S;
  wire \addr_i_6_24[10]_i_1_n_0 ;
  wire \addr_i_6_24[10]_i_2_n_0 ;
  wire \addr_i_6_24[7]_i_2_n_0 ;
  wire [10:0]addrb;
  wire \full_i_5_24[0]_i_1_n_0 ;
  wire [0:0]\full_i_5_24_reg[0]_0 ;
  wire [9:0]p_0_in;
  wire [0:0]register4_q_net;
  wire register_q_net;
  wire rel_39_16;
  wire rel_39_16_carry__0_n_1;
  wire rel_39_16_carry__0_n_2;
  wire rel_39_16_carry__0_n_3;
  wire rel_39_16_carry_n_0;
  wire rel_39_16_carry_n_1;
  wire rel_39_16_carry_n_2;
  wire rel_39_16_carry_n_3;
  wire signaldomain_clk;
  wire [1:0]state_4_23;
  wire \state_4_23[0]_i_1_n_0 ;
  wire \state_4_23[0]_i_2_n_0 ;
  wire \state_4_23[1]_i_1_n_0 ;
  wire [3:0]\state_4_23_reg[1]_0 ;
  wire [3:0]\state_4_23_reg[1]_1 ;
  wire \state_4_23_reg[1]_2 ;
  wire [0:0]web;
  wire wm_8_20_inv_i_1_n_0;
  wire wm_8_20_reg_inv_n_0;
  wire [3:0]NLW_rel_39_16_carry_O_UNCONNECTED;
  wire [3:0]NLW_rel_39_16_carry__0_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \addr_i_6_24[0]_i_1 
       (.I0(addrb[0]),
        .O(p_0_in[0]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \addr_i_6_24[10]_i_1 
       (.I0(addrb[10]),
        .I1(\addr_i_6_24[10]_i_2_n_0 ),
        .I2(addrb[7]),
        .I3(addrb[6]),
        .I4(addrb[8]),
        .I5(addrb[9]),
        .O(\addr_i_6_24[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \addr_i_6_24[10]_i_2 
       (.I0(addrb[2]),
        .I1(addrb[0]),
        .I2(addrb[1]),
        .I3(addrb[3]),
        .I4(addrb[4]),
        .I5(addrb[5]),
        .O(\addr_i_6_24[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \addr_i_6_24[1]_i_1 
       (.I0(addrb[0]),
        .I1(addrb[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \addr_i_6_24[2]_i_1 
       (.I0(addrb[2]),
        .I1(addrb[0]),
        .I2(addrb[1]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \addr_i_6_24[3]_i_1 
       (.I0(addrb[1]),
        .I1(addrb[0]),
        .I2(addrb[2]),
        .I3(addrb[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \addr_i_6_24[4]_i_1 
       (.I0(addrb[4]),
        .I1(addrb[1]),
        .I2(addrb[0]),
        .I3(addrb[2]),
        .I4(addrb[3]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \addr_i_6_24[5]_i_1 
       (.I0(addrb[5]),
        .I1(addrb[3]),
        .I2(addrb[2]),
        .I3(addrb[0]),
        .I4(addrb[1]),
        .I5(addrb[4]),
        .O(p_0_in[5]));
  LUT5 #(
    .INIT(32'hAAAA6AAA)) 
    \addr_i_6_24[6]_i_1 
       (.I0(addrb[6]),
        .I1(addrb[5]),
        .I2(addrb[4]),
        .I3(addrb[3]),
        .I4(\addr_i_6_24[7]_i_2_n_0 ),
        .O(p_0_in[6]));
  LUT6 #(
    .INIT(64'h9AAAAAAAAAAAAAAA)) 
    \addr_i_6_24[7]_i_1 
       (.I0(addrb[7]),
        .I1(\addr_i_6_24[7]_i_2_n_0 ),
        .I2(addrb[3]),
        .I3(addrb[4]),
        .I4(addrb[5]),
        .I5(addrb[6]),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \addr_i_6_24[7]_i_2 
       (.I0(addrb[1]),
        .I1(addrb[0]),
        .I2(addrb[2]),
        .O(\addr_i_6_24[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \addr_i_6_24[8]_i_1 
       (.I0(addrb[8]),
        .I1(addrb[6]),
        .I2(addrb[7]),
        .I3(\addr_i_6_24[10]_i_2_n_0 ),
        .O(p_0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \addr_i_6_24[9]_i_1 
       (.I0(addrb[9]),
        .I1(\addr_i_6_24[10]_i_2_n_0 ),
        .I2(addrb[7]),
        .I3(addrb[6]),
        .I4(addrb[8]),
        .O(p_0_in[9]));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(addrb[0]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[10] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\addr_i_6_24[10]_i_1_n_0 ),
        .Q(addrb[10]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[1] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(addrb[1]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[2] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(addrb[2]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[3] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(addrb[3]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[4] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(addrb[4]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[5] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(addrb[5]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[6] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(addrb[6]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[7] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(addrb[7]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[8] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(addrb[8]),
        .R(wm_8_20_reg_inv_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_6_24_reg[9] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(addrb[9]),
        .R(wm_8_20_reg_inv_n_0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \full_i_5_24[0]_i_1 
       (.I0(state_4_23[1]),
        .I1(register_q_net),
        .I2(state_4_23[0]),
        .O(\full_i_5_24[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \full_i_5_24_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\full_i_5_24[0]_i_1_n_0 ),
        .Q(\full_i_5_24_reg[0]_0 ),
        .R(1'b0));
  CARRY4 rel_39_16_carry
       (.CI(1'b0),
        .CO({rel_39_16_carry_n_0,rel_39_16_carry_n_1,rel_39_16_carry_n_2,rel_39_16_carry_n_3}),
        .CYINIT(1'b0),
        .DI(DI),
        .O(NLW_rel_39_16_carry_O_UNCONNECTED[3:0]),
        .S(S));
  CARRY4 rel_39_16_carry__0
       (.CI(rel_39_16_carry_n_0),
        .CO({rel_39_16,rel_39_16_carry__0_n_1,rel_39_16_carry__0_n_2,rel_39_16_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\state_4_23_reg[1]_0 ),
        .O(NLW_rel_39_16_carry__0_O_UNCONNECTED[3:0]),
        .S(\state_4_23_reg[1]_1 ));
  LUT6 #(
    .INIT(64'hB7F7B7F7BFFFB7F7)) 
    \state_4_23[0]_i_1 
       (.I0(state_4_23[1]),
        .I1(register_q_net),
        .I2(state_4_23[0]),
        .I3(rel_39_16),
        .I4(addrb[10]),
        .I5(\state_4_23[0]_i_2_n_0 ),
        .O(\state_4_23[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \state_4_23[0]_i_2 
       (.I0(addrb[9]),
        .I1(addrb[8]),
        .I2(addrb[6]),
        .I3(addrb[7]),
        .I4(\addr_i_6_24[10]_i_2_n_0 ),
        .O(\state_4_23[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBFF0000F0000000)) 
    \state_4_23[1]_i_1 
       (.I0(\state_4_23_reg[1]_2 ),
        .I1(register4_q_net),
        .I2(rel_39_16),
        .I3(state_4_23[0]),
        .I4(register_q_net),
        .I5(state_4_23[1]),
        .O(\state_4_23[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_4_23_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\state_4_23[0]_i_1_n_0 ),
        .Q(state_4_23[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \state_4_23_reg[1] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\state_4_23[1]_i_1_n_0 ),
        .Q(state_4_23[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    we_i_7_22_reg
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(1'b1),
        .Q(web),
        .R(wm_8_20_reg_inv_n_0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    wm_8_20_inv_i_1
       (.I0(state_4_23[0]),
        .I1(state_4_23[1]),
        .I2(register_q_net),
        .O(wm_8_20_inv_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    wm_8_20_reg_inv
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(wm_8_20_inv_i_1_n_0),
        .Q(wm_8_20_reg_inv_n_0),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sysgen_relational_f845914c88
   (LOAD,
    signaldomain_clk,
    o,
    Q);
  output LOAD;
  input signaldomain_clk;
  input [10:0]o;
  input [10:0]Q;

  wire LOAD;
  wire [10:0]Q;
  wire [10:0]o;
  wire \op_mem_37_22[0]_i_2_n_0 ;
  wire \op_mem_37_22[0]_i_3_n_0 ;
  wire \op_mem_37_22[0]_i_4_n_0 ;
  wire \op_mem_37_22[0]_i_5_n_0 ;
  wire \op_mem_37_22_reg[0]_i_1_n_1 ;
  wire \op_mem_37_22_reg[0]_i_1_n_2 ;
  wire \op_mem_37_22_reg[0]_i_1_n_3 ;
  wire result_12_3_rel;
  wire signaldomain_clk;
  wire [3:0]\NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h9009)) 
    \op_mem_37_22[0]_i_2 
       (.I0(o[9]),
        .I1(Q[9]),
        .I2(o[10]),
        .I3(Q[10]),
        .O(\op_mem_37_22[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_3 
       (.I0(o[6]),
        .I1(Q[6]),
        .I2(Q[8]),
        .I3(o[8]),
        .I4(Q[7]),
        .I5(o[7]),
        .O(\op_mem_37_22[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_4 
       (.I0(o[3]),
        .I1(Q[3]),
        .I2(Q[5]),
        .I3(o[5]),
        .I4(Q[4]),
        .I5(o[4]),
        .O(\op_mem_37_22[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \op_mem_37_22[0]_i_5 
       (.I0(o[0]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(o[2]),
        .I4(Q[1]),
        .I5(o[1]),
        .O(\op_mem_37_22[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \op_mem_37_22_reg[0] 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(result_12_3_rel),
        .Q(LOAD),
        .R(1'b0));
  CARRY4 \op_mem_37_22_reg[0]_i_1 
       (.CI(1'b0),
        .CO({result_12_3_rel,\op_mem_37_22_reg[0]_i_1_n_1 ,\op_mem_37_22_reg[0]_i_1_n_2 ,\op_mem_37_22_reg[0]_i_1_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_op_mem_37_22_reg[0]_i_1_O_UNCONNECTED [3:0]),
        .S({\op_mem_37_22[0]_i_2_n_0 ,\op_mem_37_22[0]_i_3_n_0 ,\op_mem_37_22[0]_i_4_n_0 ,\op_mem_37_22[0]_i_5_n_0 }));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0
   (register_q_net,
    enable,
    signaldomain_clk);
  output register_q_net;
  input [0:0]enable;
  input signaldomain_clk;

  wire [0:0]enable;
  wire register_q_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(enable),
        .Q(register_q_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input d3_net;
  input signaldomain_clk;

  wire d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d3_net),
        .Q(q),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45
   (d3_net,
    d2_net,
    signaldomain_clk);
  output d3_net;
  input d2_net;
  input signaldomain_clk;

  wire d2_net;
  wire d3_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d2_net),
        .Q(d3_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46
   (d2_net,
    d1_net,
    signaldomain_clk);
  output d2_net;
  input d1_net;
  input signaldomain_clk;

  wire d1_net;
  wire d2_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d1_net),
        .Q(d2_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47
   (d1_net,
    r5_enable,
    axibusdomain_clk);
  output d1_net;
  input [0:0]r5_enable;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire d1_net;
  wire [0:0]r5_enable;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r5_enable),
        .Q(d1_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1
   (S,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[15].bit_is_0.fdre_comp_0 ,
    rel_39_16_carry__0,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp_0 ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]S;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp_0 ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  wire [15:0]i;
  wire [15:0]o;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry__0_i_1
       (.I0(o[15]),
        .I1(rel_39_16_carry__0[15]),
        .I2(rel_39_16_carry__0[14]),
        .I3(o[14]),
        .O(\fd_prim_array[15].bit_is_0.fdre_comp_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry__0_i_6
       (.I0(o[13]),
        .I1(rel_39_16_carry__0[13]),
        .I2(o[12]),
        .I3(rel_39_16_carry__0[12]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry__0_i_7
       (.I0(o[11]),
        .I1(rel_39_16_carry__0[11]),
        .I2(o[10]),
        .I3(rel_39_16_carry__0[10]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry__0_i_8
       (.I0(o[9]),
        .I1(rel_39_16_carry__0[9]),
        .I2(o[8]),
        .I3(rel_39_16_carry__0[8]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry_i_5
       (.I0(o[7]),
        .I1(rel_39_16_carry__0[7]),
        .I2(o[6]),
        .I3(rel_39_16_carry__0[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry_i_6
       (.I0(o[5]),
        .I1(rel_39_16_carry__0[5]),
        .I2(o[4]),
        .I3(rel_39_16_carry__0[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry_i_7
       (.I0(o[3]),
        .I1(rel_39_16_carry__0[3]),
        .I2(o[2]),
        .I3(rel_39_16_carry__0[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry_i_8
       (.I0(o[1]),
        .I1(rel_39_16_carry__0[1]),
        .I2(o[0]),
        .I3(rel_39_16_carry__0[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17
   (o,
    r4_threshold,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r4_threshold;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r4_threshold;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r4_threshold[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5
   (o,
    ch2,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch2;
  input signaldomain_clk;

  wire [15:0]ch2;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch2[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7
   (o,
    ch1,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch1;
  input signaldomain_clk;

  wire [15:0]ch1;
  wire [15:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch1[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9
   (DI,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp_0 ,
    \fd_prim_array[15].bit_is_0.fdre_comp_0 ,
    rel_39_16_carry__0,
    ch_trigger,
    signaldomain_clk);
  output [3:0]DI;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp_0 ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]ch_trigger;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [15:0]ch_trigger;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp_0 ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp_0 ;
  wire [15:0]o;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[11].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[11]),
        .Q(o[11]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[12].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[12]),
        .Q(o[12]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[13].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[13]),
        .Q(o[13]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[14].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[14]),
        .Q(o[14]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[15].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[15]),
        .Q(o[15]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(ch_trigger[9]),
        .Q(o[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry__0_i_2
       (.I0(o[13]),
        .I1(rel_39_16_carry__0[13]),
        .I2(o[12]),
        .I3(rel_39_16_carry__0[12]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry__0_i_3
       (.I0(o[11]),
        .I1(rel_39_16_carry__0[11]),
        .I2(o[10]),
        .I3(rel_39_16_carry__0[10]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry__0_i_4
       (.I0(o[9]),
        .I1(rel_39_16_carry__0[9]),
        .I2(o[8]),
        .I3(rel_39_16_carry__0[8]),
        .O(\fd_prim_array[13].bit_is_0.fdre_comp_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    rel_39_16_carry__0_i_5
       (.I0(o[15]),
        .I1(rel_39_16_carry__0[15]),
        .I2(rel_39_16_carry__0[14]),
        .I3(o[14]),
        .O(\fd_prim_array[15].bit_is_0.fdre_comp_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry_i_1
       (.I0(o[7]),
        .I1(rel_39_16_carry__0[7]),
        .I2(o[6]),
        .I3(rel_39_16_carry__0[6]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry_i_2
       (.I0(o[5]),
        .I1(rel_39_16_carry__0[5]),
        .I2(o[4]),
        .I3(rel_39_16_carry__0[4]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry_i_3
       (.I0(o[3]),
        .I1(rel_39_16_carry__0[3]),
        .I2(o[2]),
        .I3(rel_39_16_carry__0[2]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    rel_39_16_carry_i_4
       (.I0(o[1]),
        .I1(rel_39_16_carry__0[1]),
        .I2(o[0]),
        .I3(rel_39_16_carry__0[0]),
        .O(DI[0]));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2
   (register4_q_net,
    clear,
    signaldomain_clk);
  output [0:0]register4_q_net;
  input [0:0]clear;
  input signaldomain_clk;

  wire [0:0]clear;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(clear),
        .Q(register4_q_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22
   (r8_full,
    d3_net,
    axibusdomain_clk);
  output [0:0]r8_full;
  input [0:0]d3_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d3_net;
  wire [0:0]r8_full;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(d3_net),
        .Q(r8_full),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23
   (d3_net,
    d2_net,
    axibusdomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d2_net;
  wire [0:0]d3_net;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(d2_net),
        .Q(d3_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24
   (d2_net,
    d1_net,
    axibusdomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(d1_net),
        .Q(d2_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25
   (d1_net,
    full,
    signaldomain_clk);
  output [0:0]d1_net;
  input [0:0]full;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]full;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(full),
        .Q(d1_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]d3_net;
  input signaldomain_clk;

  wire [0:0]d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d3_net),
        .Q(q),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31
   (d3_net,
    d2_net,
    signaldomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input signaldomain_clk;

  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d2_net),
        .Q(d3_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32
   (d2_net,
    d1_net,
    signaldomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(d1_net),
        .Q(d2_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33
   (d1_net,
    r7_clear,
    axibusdomain_clk);
  output [0:0]d1_net;
  input [0:0]r7_clear;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]r7_clear;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r7_clear),
        .Q(d1_net),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(i[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_single_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39
   (o,
    r6_delay,
    axibusdomain_clk);
  output [10:0]o;
  input [10:0]r6_delay;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [10:0]o;
  wire [10:0]r6_delay;

  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[0].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[0]),
        .Q(o[0]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[10].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[10]),
        .Q(o[10]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[1].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[1]),
        .Q(o[1]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[2].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[2]),
        .Q(o[2]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[3].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[3]),
        .Q(o[3]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[4].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[4]),
        .Q(o[4]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[5].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[5]),
        .Q(o[5]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[6].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[6]),
        .Q(o[6]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[7].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[7]),
        .Q(o[7]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[8].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[8]),
        .Q(o[8]),
        .R(1'b0));
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE \fd_prim_array[9].bit_is_0.fdre_comp 
       (.C(axibusdomain_clk),
        .CE(1'b1),
        .D(r6_delay[9]),
        .Q(o[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e
   (full,
    \reg_array[0].fde_used.u2_0 ,
    signaldomain_clk);
  output [0:0]full;
  input [0:0]\reg_array[0].fde_used.u2_0 ;
  input signaldomain_clk;

  wire [0:0]full;
  wire [0:0]\reg_array[0].fde_used.u2_0 ;
  wire signaldomain_clk;

  (* XILINX_LEGACY_PRIM = "FDE" *) 
  (* fpga_dont_touch = "true" *) 
  (* syn_black_box = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \reg_array[0].fde_used.u2 
       (.C(signaldomain_clk),
        .CE(1'b1),
        .D(\reg_array[0].fde_used.u2_0 ),
        .Q(full),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg
   (full,
    \reg_array[0].fde_used.u2 ,
    signaldomain_clk);
  output [0:0]full;
  input [0:0]\reg_array[0].fde_used.u2 ;
  input signaldomain_clk;

  wire [0:0]full;
  wire [0:0]\reg_array[0].fde_used.u2 ;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_srlc33e \partial_one.last_srlc33e 
       (.full(full),
        .\reg_array[0].fde_used.u2_0 (\reg_array[0].fde_used.u2 ),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0
   (register_q_net,
    enable,
    signaldomain_clk);
  output register_q_net;
  input [0:0]enable;
  input signaldomain_clk;

  wire [0:0]enable;
  wire register_q_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0 \latency_gt_0.fd_array[1].reg_comp 
       (.enable(enable),
        .register_q_net(register_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_40
   (d1_net,
    r5_enable,
    axibusdomain_clk);
  output d1_net;
  input [0:0]r5_enable;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire d1_net;
  wire [0:0]r5_enable;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_47 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r5_enable(r5_enable));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_41
   (d2_net,
    d1_net,
    signaldomain_clk);
  output d2_net;
  input d1_net;
  input signaldomain_clk;

  wire d1_net;
  wire d2_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_46 \latency_gt_0.fd_array[1].reg_comp 
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_42
   (d3_net,
    d2_net,
    signaldomain_clk);
  output d3_net;
  input d2_net;
  input signaldomain_clk;

  wire d2_net;
  wire d3_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_45 \latency_gt_0.fd_array[1].reg_comp 
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized0_43
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input d3_net;
  input signaldomain_clk;

  wire d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized0_44 \latency_gt_0.fd_array[1].reg_comp 
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1
   (S,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp ,
    \fd_prim_array[15].bit_is_0.fdre_comp ,
    rel_39_16_carry__0,
    i,
    signaldomain_clk);
  output [3:0]S;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]i;
  input signaldomain_clk;

  wire [3:0]S;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  wire [15:0]i;
  wire [15:0]o;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1 \latency_gt_0.fd_array[1].reg_comp 
       (.S(S),
        .\fd_prim_array[13].bit_is_0.fdre_comp_0 (\fd_prim_array[13].bit_is_0.fdre_comp ),
        .\fd_prim_array[15].bit_is_0.fdre_comp_0 (\fd_prim_array[15].bit_is_0.fdre_comp ),
        .i(i),
        .o(o),
        .rel_39_16_carry__0(rel_39_16_carry__0),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_10
   (o,
    r4_threshold,
    axibusdomain_clk);
  output [15:0]o;
  input [15:0]r4_threshold;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [15:0]o;
  wire [15:0]r4_threshold;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_17 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r4_threshold(r4_threshold));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_11
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_16 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_12
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_15 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_13
   (o,
    i,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]i;
  input signaldomain_clk;

  wire [15:0]i;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_14 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_4
   (o,
    ch2,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch2;
  input signaldomain_clk;

  wire [15:0]ch2;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_5 \latency_gt_0.fd_array[1].reg_comp 
       (.ch2(ch2),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_6
   (o,
    ch1,
    signaldomain_clk);
  output [15:0]o;
  input [15:0]ch1;
  input signaldomain_clk;

  wire [15:0]ch1;
  wire [15:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_7 \latency_gt_0.fd_array[1].reg_comp 
       (.ch1(ch1),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized1_8
   (DI,
    o,
    \fd_prim_array[13].bit_is_0.fdre_comp ,
    \fd_prim_array[15].bit_is_0.fdre_comp ,
    rel_39_16_carry__0,
    ch_trigger,
    signaldomain_clk);
  output [3:0]DI;
  output [15:0]o;
  output [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  output [0:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  input [15:0]rel_39_16_carry__0;
  input [15:0]ch_trigger;
  input signaldomain_clk;

  wire [3:0]DI;
  wire [15:0]ch_trigger;
  wire [2:0]\fd_prim_array[13].bit_is_0.fdre_comp ;
  wire [0:0]\fd_prim_array[15].bit_is_0.fdre_comp ;
  wire [15:0]o;
  wire [15:0]rel_39_16_carry__0;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized1_9 \latency_gt_0.fd_array[1].reg_comp 
       (.DI(DI),
        .ch_trigger(ch_trigger),
        .\fd_prim_array[13].bit_is_0.fdre_comp_0 (\fd_prim_array[13].bit_is_0.fdre_comp ),
        .\fd_prim_array[15].bit_is_0.fdre_comp_0 (\fd_prim_array[15].bit_is_0.fdre_comp ),
        .o(o),
        .rel_39_16_carry__0(rel_39_16_carry__0),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2
   (register4_q_net,
    clear,
    signaldomain_clk);
  output [0:0]register4_q_net;
  input [0:0]clear;
  input signaldomain_clk;

  wire [0:0]clear;
  wire [0:0]register4_q_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2 \latency_gt_0.fd_array[1].reg_comp 
       (.clear(clear),
        .register4_q_net(register4_q_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_18
   (d1_net,
    full,
    signaldomain_clk);
  output [0:0]d1_net;
  input [0:0]full;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]full;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_25 \latency_gt_0.fd_array[1].reg_comp 
       (.d1_net(d1_net),
        .full(full),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_19
   (d2_net,
    d1_net,
    axibusdomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]d2_net;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_24 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .d2_net(d2_net));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_20
   (d3_net,
    d2_net,
    axibusdomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d2_net;
  wire [0:0]d3_net;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_23 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d2_net(d2_net),
        .d3_net(d3_net));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_21
   (r8_full,
    d3_net,
    axibusdomain_clk);
  output [0:0]r8_full;
  input [0:0]d3_net;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d3_net;
  wire [0:0]r8_full;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_22 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d3_net(d3_net),
        .r8_full(r8_full));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_26
   (d1_net,
    r7_clear,
    axibusdomain_clk);
  output [0:0]d1_net;
  input [0:0]r7_clear;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [0:0]d1_net;
  wire [0:0]r7_clear;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_33 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .d1_net(d1_net),
        .r7_clear(r7_clear));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_27
   (d2_net,
    d1_net,
    signaldomain_clk);
  output [0:0]d2_net;
  input [0:0]d1_net;
  input signaldomain_clk;

  wire [0:0]d1_net;
  wire [0:0]d2_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_32 \latency_gt_0.fd_array[1].reg_comp 
       (.d1_net(d1_net),
        .d2_net(d2_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_28
   (d3_net,
    d2_net,
    signaldomain_clk);
  output [0:0]d3_net;
  input [0:0]d2_net;
  input signaldomain_clk;

  wire [0:0]d2_net;
  wire [0:0]d3_net;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_31 \latency_gt_0.fd_array[1].reg_comp 
       (.d2_net(d2_net),
        .d3_net(d3_net),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized2_29
   (q,
    d3_net,
    signaldomain_clk);
  output [0:0]q;
  input [0:0]d3_net;
  input signaldomain_clk;

  wire [0:0]d3_net;
  wire [0:0]q;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized2_30 \latency_gt_0.fd_array[1].reg_comp 
       (.d3_net(d3_net),
        .q(q),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3
   (o,
    r6_delay,
    axibusdomain_clk);
  output [10:0]o;
  input [10:0]r6_delay;
  input axibusdomain_clk;

  wire axibusdomain_clk;
  wire [10:0]o;
  wire [10:0]r6_delay;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_39 \latency_gt_0.fd_array[1].reg_comp 
       (.axibusdomain_clk(axibusdomain_clk),
        .o(o),
        .r6_delay(r6_delay));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_34
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_38 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_35
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3_37 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

(* ORIG_REF_NAME = "xil_defaultlib_synth_reg_w_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_synth_reg_w_init__parameterized3_36
   (o,
    i,
    signaldomain_clk);
  output [10:0]o;
  input [10:0]i;
  input signaldomain_clk;

  wire [10:0]i;
  wire [10:0]o;
  wire signaldomain_clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xil_defaultlib_single_reg_w_init__parameterized3 \latency_gt_0.fd_array[1].reg_comp 
       (.i(i),
        .o(o),
        .signaldomain_clk(signaldomain_clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [31:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [31:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width \ramloop[0].ram.r 
       (.addra(addra),
        .clka(clka),
        .dina(dina[17:0]),
        .douta(douta[17:0]),
        .ena(ena),
        .wea(wea));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0 \ramloop[1].ram.r 
       (.addra(addra),
        .clka(clka),
        .dina(dina[31:18]),
        .douta(douta[31:18]),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_generic_cstr" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [31:0]douta;
  output [31:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [31:0]dina;
  input [31:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1 \ramloop[0].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina[17:0]),
        .dinb(dinb[17:0]),
        .douta(douta[17:0]),
        .doutb(doutb[17:0]),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2 \ramloop[1].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina[31:18]),
        .dinb(dinb[31:18]),
        .douta(douta[31:18]),
        .doutb(doutb[31:18]),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [17:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [17:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [17:0]dina;
  wire [17:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init \prim_init.ram 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [13:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [13:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [13:0]dina;
  wire [13:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0 \prim_init.ram 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [17:0]douta;
  output [17:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [17:0]dina;
  input [17:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [17:0]dina;
  wire [17:0]dinb;
  wire [17:0]douta;
  wire [17:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [13:0]douta;
  output [13:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [13:0]dina;
  input [13:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [13:0]dina;
  wire [13:0]dinb;
  wire [13:0]douta;
  wire [13:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [17:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [17:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [17:0]dina;
  wire [17:0]douta;
  wire ena;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[16:9],dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,dina[17],dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:16],douta[16:9],douta[7:0]}),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:2],douta[17],douta[8]}),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [13:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [13:0]dina;
  input [0:0]wea;

  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71 ;
  wire [10:0]addra;
  wire clka;
  wire [13:0]dina;
  wire [13:0]douta;
  wire ena;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[13:7],1'b0,dina[6:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:16],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_20 ,douta[13:7],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_28 ,douta[6:0]}),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:2],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_70 ,\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_n_71 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [17:0]douta;
  output [17:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [17:0]dina;
  input [17:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [17:0]dina;
  wire [17:0]dinb;
  wire [17:0]douta;
  wire [17:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,addrb,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[16:9],dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dinb[16:9],dinb[7:0]}),
        .DIPADIP({1'b0,1'b0,dina[17],dina[8]}),
        .DIPBDIP({1'b0,1'b0,dinb[17],dinb[8]}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:16],douta[16:9],douta[7:0]}),
        .DOBDO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:16],doutb[16:9],doutb[7:0]}),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:2],douta[17],douta[8]}),
        .DOPBDOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:2],doutb[17],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena),
        .ENBWREN(enb),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,web,web,web,web}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [13:0]douta;
  output [13:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [13:0]dina;
  input [13:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75 ;
  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [13:0]dina;
  wire [13:0]dinb;
  wire [13:0]douta;
  wire [13:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:16]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,addrb,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[13:7],1'b0,dina[6:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dinb[13:7],1'b0,dinb[6:0]}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:16],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_20 ,douta[13:7],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_28 ,douta[6:0]}),
        .DOBDO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:16],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_52 ,doutb[13:7],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_60 ,doutb[6:0]}),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:2],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_70 ,\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_71 }),
        .DOPBDOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:2],\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_74 ,\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_75 }),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena),
        .ENBWREN(enb),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,web,web,web,web}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [31:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [31:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr \valid.cstr 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_top" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [31:0]douta;
  output [31:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [31:0]dina;
  input [31:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr__parameterized0 \valid.cstr 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* C_ADDRA_WIDTH = "11" *) (* C_ADDRB_WIDTH = "11" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "0" *) (* C_COUNT_18K_BRAM = "0" *) 
(* C_COUNT_36K_BRAM = "2" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     5.6824 mW" *) 
(* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "1" *) (* C_HAS_ENB = "0" *) 
(* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
(* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) (* C_HAS_REGCEA = "0" *) 
(* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) (* C_HAS_RSTB = "0" *) 
(* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) (* C_INITA_VAL = "0" *) 
(* C_INITB_VAL = "0" *) (* C_INIT_FILE = "ip_scope_blk_mem_gen_i1.mem" *) (* C_INIT_FILE_NAME = "ip_scope_blk_mem_gen_i1.mif" *) 
(* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "1" *) (* C_MEM_TYPE = "0" *) 
(* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) (* C_READ_DEPTH_A = "2048" *) 
(* C_READ_DEPTH_B = "2048" *) (* C_READ_LATENCY_A = "1" *) (* C_READ_LATENCY_B = "1" *) 
(* C_READ_WIDTH_A = "32" *) (* C_READ_WIDTH_B = "32" *) (* C_RSTRAM_A = "0" *) 
(* C_RSTRAM_B = "0" *) (* C_RST_PRIORITY_A = "CE" *) (* C_RST_PRIORITY_B = "CE" *) 
(* C_SIM_COLLISION_CHECK = "ALL" *) (* C_USE_BRAM_BLOCK = "0" *) (* C_USE_BYTE_WEA = "0" *) 
(* C_USE_BYTE_WEB = "0" *) (* C_USE_DEFAULT_DATA = "0" *) (* C_USE_ECC = "0" *) 
(* C_USE_SOFTECC = "0" *) (* C_USE_URAM = "0" *) (* C_WEA_WIDTH = "1" *) 
(* C_WEB_WIDTH = "1" *) (* C_WRITE_DEPTH_A = "2048" *) (* C_WRITE_DEPTH_B = "2048" *) 
(* C_WRITE_MODE_A = "READ_FIRST" *) (* C_WRITE_MODE_B = "WRITE_FIRST" *) (* C_WRITE_WIDTH_A = "32" *) 
(* C_WRITE_WIDTH_B = "32" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* c_family = "artix7" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [10:0]addra;
  input [31:0]dina;
  output [31:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [10:0]addrb;
  input [31:0]dinb;
  output [31:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [10:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [10:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;

  assign dbiterr = \<const0> ;
  assign doutb[31] = \<const0> ;
  assign doutb[30] = \<const0> ;
  assign doutb[29] = \<const0> ;
  assign doutb[28] = \<const0> ;
  assign doutb[27] = \<const0> ;
  assign doutb[26] = \<const0> ;
  assign doutb[25] = \<const0> ;
  assign doutb[24] = \<const0> ;
  assign doutb[23] = \<const0> ;
  assign doutb[22] = \<const0> ;
  assign doutb[21] = \<const0> ;
  assign doutb[20] = \<const0> ;
  assign doutb[19] = \<const0> ;
  assign doutb[18] = \<const0> ;
  assign doutb[17] = \<const0> ;
  assign doutb[16] = \<const0> ;
  assign doutb[15] = \<const0> ;
  assign doutb[14] = \<const0> ;
  assign doutb[13] = \<const0> ;
  assign doutb[12] = \<const0> ;
  assign doutb[11] = \<const0> ;
  assign doutb[10] = \<const0> ;
  assign doutb[9] = \<const0> ;
  assign doutb[8] = \<const0> ;
  assign doutb[7] = \<const0> ;
  assign doutb[6] = \<const0> ;
  assign doutb[5] = \<const0> ;
  assign doutb[4] = \<const0> ;
  assign doutb[3] = \<const0> ;
  assign doutb[2] = \<const0> ;
  assign doutb[1] = \<const0> ;
  assign doutb[0] = \<const0> ;
  assign rdaddrecc[10] = \<const0> ;
  assign rdaddrecc[9] = \<const0> ;
  assign rdaddrecc[8] = \<const0> ;
  assign rdaddrecc[7] = \<const0> ;
  assign rdaddrecc[6] = \<const0> ;
  assign rdaddrecc[5] = \<const0> ;
  assign rdaddrecc[4] = \<const0> ;
  assign rdaddrecc[3] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[10] = \<const0> ;
  assign s_axi_rdaddrecc[9] = \<const0> ;
  assign s_axi_rdaddrecc[8] = \<const0> ;
  assign s_axi_rdaddrecc[7] = \<const0> ;
  assign s_axi_rdaddrecc[6] = \<const0> ;
  assign s_axi_rdaddrecc[5] = \<const0> ;
  assign s_axi_rdaddrecc[4] = \<const0> ;
  assign s_axi_rdaddrecc[3] = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[31] = \<const0> ;
  assign s_axi_rdata[30] = \<const0> ;
  assign s_axi_rdata[29] = \<const0> ;
  assign s_axi_rdata[28] = \<const0> ;
  assign s_axi_rdata[27] = \<const0> ;
  assign s_axi_rdata[26] = \<const0> ;
  assign s_axi_rdata[25] = \<const0> ;
  assign s_axi_rdata[24] = \<const0> ;
  assign s_axi_rdata[23] = \<const0> ;
  assign s_axi_rdata[22] = \<const0> ;
  assign s_axi_rdata[21] = \<const0> ;
  assign s_axi_rdata[20] = \<const0> ;
  assign s_axi_rdata[19] = \<const0> ;
  assign s_axi_rdata[18] = \<const0> ;
  assign s_axi_rdata[17] = \<const0> ;
  assign s_axi_rdata[16] = \<const0> ;
  assign s_axi_rdata[15] = \<const0> ;
  assign s_axi_rdata[14] = \<const0> ;
  assign s_axi_rdata[13] = \<const0> ;
  assign s_axi_rdata[12] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth inst_blk_mem_gen
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* C_ADDRA_WIDTH = "11" *) (* C_ADDRB_WIDTH = "11" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "0" *) (* C_COUNT_18K_BRAM = "0" *) 
(* C_COUNT_36K_BRAM = "2" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     10.698 mW" *) 
(* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "1" *) (* C_HAS_ENB = "1" *) 
(* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
(* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) (* C_HAS_REGCEA = "0" *) 
(* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) (* C_HAS_RSTB = "0" *) 
(* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) (* C_INITA_VAL = "0" *) 
(* C_INITB_VAL = "0" *) (* C_INIT_FILE = "ip_scope_blk_mem_gen_i0.mem" *) (* C_INIT_FILE_NAME = "ip_scope_blk_mem_gen_i0.mif" *) 
(* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "1" *) (* C_MEM_TYPE = "2" *) 
(* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) (* C_READ_DEPTH_A = "2048" *) 
(* C_READ_DEPTH_B = "2048" *) (* C_READ_LATENCY_A = "1" *) (* C_READ_LATENCY_B = "1" *) 
(* C_READ_WIDTH_A = "32" *) (* C_READ_WIDTH_B = "32" *) (* C_RSTRAM_A = "0" *) 
(* C_RSTRAM_B = "0" *) (* C_RST_PRIORITY_A = "CE" *) (* C_RST_PRIORITY_B = "CE" *) 
(* C_SIM_COLLISION_CHECK = "ALL" *) (* C_USE_BRAM_BLOCK = "0" *) (* C_USE_BYTE_WEA = "0" *) 
(* C_USE_BYTE_WEB = "0" *) (* C_USE_DEFAULT_DATA = "0" *) (* C_USE_ECC = "0" *) 
(* C_USE_SOFTECC = "0" *) (* C_USE_URAM = "0" *) (* C_WEA_WIDTH = "1" *) 
(* C_WEB_WIDTH = "1" *) (* C_WRITE_DEPTH_A = "2048" *) (* C_WRITE_DEPTH_B = "2048" *) 
(* C_WRITE_MODE_A = "WRITE_FIRST" *) (* C_WRITE_MODE_B = "WRITE_FIRST" *) (* C_WRITE_WIDTH_A = "32" *) 
(* C_WRITE_WIDTH_B = "32" *) (* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
(* ORIG_REF_NAME = "blk_mem_gen_v8_4_3" *) (* c_family = "artix7" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3__parameterized1
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [10:0]addra;
  input [31:0]dina;
  output [31:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [10:0]addrb;
  input [31:0]dinb;
  output [31:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [10:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [10:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  assign dbiterr = \<const0> ;
  assign rdaddrecc[10] = \<const0> ;
  assign rdaddrecc[9] = \<const0> ;
  assign rdaddrecc[8] = \<const0> ;
  assign rdaddrecc[7] = \<const0> ;
  assign rdaddrecc[6] = \<const0> ;
  assign rdaddrecc[5] = \<const0> ;
  assign rdaddrecc[4] = \<const0> ;
  assign rdaddrecc[3] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[10] = \<const0> ;
  assign s_axi_rdaddrecc[9] = \<const0> ;
  assign s_axi_rdaddrecc[8] = \<const0> ;
  assign s_axi_rdaddrecc[7] = \<const0> ;
  assign s_axi_rdaddrecc[6] = \<const0> ;
  assign s_axi_rdaddrecc[5] = \<const0> ;
  assign s_axi_rdaddrecc[4] = \<const0> ;
  assign s_axi_rdaddrecc[3] = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[31] = \<const0> ;
  assign s_axi_rdata[30] = \<const0> ;
  assign s_axi_rdata[29] = \<const0> ;
  assign s_axi_rdata[28] = \<const0> ;
  assign s_axi_rdata[27] = \<const0> ;
  assign s_axi_rdata[26] = \<const0> ;
  assign s_axi_rdata[25] = \<const0> ;
  assign s_axi_rdata[24] = \<const0> ;
  assign s_axi_rdata[23] = \<const0> ;
  assign s_axi_rdata[22] = \<const0> ;
  assign s_axi_rdata[21] = \<const0> ;
  assign s_axi_rdata[20] = \<const0> ;
  assign s_axi_rdata[19] = \<const0> ;
  assign s_axi_rdata[18] = \<const0> ;
  assign s_axi_rdata[17] = \<const0> ;
  assign s_axi_rdata[16] = \<const0> ;
  assign s_axi_rdata[15] = \<const0> ;
  assign s_axi_rdata[14] = \<const0> ;
  assign s_axi_rdata[13] = \<const0> ;
  assign s_axi_rdata[12] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth__parameterized0 inst_blk_mem_gen
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth
   (douta,
    clka,
    ena,
    addra,
    dina,
    wea);
  output [31:0]douta;
  input clka;
  input ena;
  input [10:0]addra;
  input [31:0]dina;
  input [0:0]wea;

  wire [10:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire ena;
  wire [0:0]wea;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .ena(ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_4_3_synth" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_3_synth__parameterized0
   (douta,
    doutb,
    clka,
    clkb,
    ena,
    enb,
    addra,
    addrb,
    dina,
    dinb,
    wea,
    web);
  output [31:0]douta;
  output [31:0]doutb;
  input clka;
  input clkb;
  input ena;
  input enb;
  input [10:0]addra;
  input [10:0]addrb;
  input [31:0]dina;
  input [31:0]dinb;
  input [0:0]wea;
  input [0:0]web;

  wire [10:0]addra;
  wire [10:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire ena;
  wire enb;
  wire [0:0]wea;
  wire [0:0]web;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top__parameterized0 \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .ena(ena),
        .enb(enb),
        .wea(wea),
        .web(web));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_LOAD = "1" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "1" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "0" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "0" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "11" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* DowngradeIPIdentifiedWarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [10:0]L;
  output THRESH0;
  output [10:0]Q;

  wire \<const0> ;
  wire CE;
  wire CLK;
  wire [10:0]L;
  wire LOAD;
  wire [10:0]Q;
  wire SINIT;
  wire NLW_i_synth_THRESH0_UNCONNECTED;

  assign THRESH0 = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "11" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_13_viv i_synth
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_i_synth_THRESH0_UNCONNECTED),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nj0mUIQXW9VMlW8UIy0+rhBPRzPE/2hNCYd9Pq/2lupvmSLanFgWdHL8UL8/+/RIIcCDnmQGy1BQ
ap0VWLUG3/sO5IjsZSHycp96KW1SZOFUG6H0ush/SZMcoiGqfoj4ibt01ffKES2JTmtlplW8eP3u
UTnOVqSFqHUqGa5S7LOKks9Tsf+GEE/D/PYCjYVZINuDQqjAcu2hLOgOM4dw+0+MfBdnaB1+yuFV
VnQCX38WZdAWMAN8QA02gUD9Pccjx9rGpdaE5+TzWeKwNG/ASfDjJQHS0rDhPEg0IBUV/EUKgf8s
dkXpQWBx9Wwe6xiLynaB/1by9GPtVomLVHRTBw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
vztyisJkHBQBDZh95U6YwB14lemhKoLP8PyOoPIcv1ZdfEaFlTGoNH2h5AW9LbUlfo47CEC3IEgn
IZyWTmaQBzPPa3RSXLTsLVApBIZeteNlGRZ1XRcecHkTP4qsOWeZhD4smnm/LE5DYG1pcuPK2deC
F752WcmY6hteISUHZzg2s34j4RwUIRFwwjZSXSX1Q8cpr4tLq5Fb18gM4I1dZvLdRUodKaH3SyLz
SELfqHdexpvBnIOelwEpCumOcM62/m2IDZ5HDObDtJeKbchdqQeGHmYb8tjzHzKkpvGTYh5Qq92T
U/9YqbAQt1fCAKg7vEduiocjvv7uYLrujxj2zA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12528)
`pragma protect data_block
JzHZAaRjGEpBl/jJJ7Q/at4Spv++wbP3oHMlgwhOe2oLF/0Dk3mAMc/RUPeDxJqjaeBuvV6ZVxg9
Vbx7WE9MrtZVvZQf6EwSH0Y3tf2sum5bk3ZATS1hPyAbYuXmD9MzvXzT6c/QC0pcCCQ/ts8x0JJC
Hkm2W2esU4bUdDTAXoKOuOaKJ3Bd5DmT7/knN6CNnsOpx4msnKXQOQl23SsGSq63NxoBL1Av/BN6
oxf8Eg/4TVCwIw0ge8/mSF0ACEUoljLa1McqD9zryCUeHRTTH+kQMHfNRUQ78Q5o5ZaQTuO5Tsgy
PTnka1T4wDdpaK71u5iNMOvUXy51lf3Jv6dReuhOCMkfmA7qxi90gwbcJlhn6TDsu6CfPiJoB43c
9EWFO6rnZ73tl0l9h/HDBqdqxmV5lRtOACcBaCUD8G+81inRxcIn5GiSt7c3uq8DCfrUv3VnjKAD
vvUhl/6tXHerjR4Ou0NkuYVuUl4uU5jKVHDnUhIMSS5G5X3nO/dP0HUQejOAJAmDUn5L9jGTe/zP
+epV54EAGPNNL4zzBeTDhCZ1gP/eoJe0g7fwYB+eE2nS5h2nXJxeUcdojZeodipFI1kXJ0WHpjQ2
hpSXoc6lTCE9vyPT2mhzshCP4lzbt7SRC6jXOd1xyu7FvzvvvCJgkGj87Ihi0pzztIGu8adtUnZU
DlAJd7966em3XVbDmCkLaiAqAseV5lxqQ/syQa5+Ng0bFNsUxJXuSUxBQj7R2VQdxK8ly0oCZA4J
eO0+Z53iLOcfLffAZ0Qes2HgbLO9XCzKmweNwDuNzGtgj+qAlQANGfFrBDFKzX4dKgreljdfq0Sh
orq9Dg/Z5FALJqxRICazclUb43ttNG1zFhCvlGhZMJdfVXZgT5eU0IJbuH+M0lVFr/Yy2wi+8DT/
HaVkwuWSD1W8vAJsnS4cTwC4hkAhnpVeZKe+rrKymcyEpYDazZ5yAKewyfhuALHydnfWzmtSOLmc
xSFgn+lTBWO02FV6AfXRrmHPEFUDugizVX2L9JO9HBnLmJKum3YY12AoT972uo1y0FT3BBvJOhjy
fbBd2dWn27e9RBjCY/CqURCjTU54H1fRR2n1JYXLDX7mRZwpW1axsQ41ZCMUApT5NBIoyQ3BMnR6
TpEj7Xp/egJeACac1HNuICQ0GlmUu2UHrioG7U7uHI5jd7hn0xqVxQkZrz6n2SU9s6KaBhVyTmFR
Uj7sxB4ntUuuSq+ls6jNEEQcIyWQ8AVu8g4QbWpwd+UL4xY0HrPZn87OuaPPZ7on0nwHmpf19sYB
iPX/61Ws4/huIujIL5+95poOxcLfIMQofN6booVBtvXbfvtKf11XFCYDJBJ+PidtwRSkpnC5olVX
Sx7SoKir2i2MqOgPm21OrAtJkyj2Vqtx47AmYVC9wCFAuwioP/vNFQWej4tqnD5z58JW2G/u0vhL
fLyaBw63/TXmpZFixmeX+/clInmvVCtjyeanQMJSwGWnEvQiKy2VuEQpG675NdWdJIUYGnAhX7uX
kQ48BhH1S1jxwegm5EzBH/QVBneFbi7lVm6ZI5W9Dlk1I4+Gp47fNU1opO2cfjVqdSMiFOxz+dvf
hIM9ND2twM6/g6QJoyqYZ6cum65vH2jcjN1wOI+PoJtyEPR+2BqNlowq6jBT5S1HEQIncCwjGt85
LHdjNbxgs/42anFrPNma5LHvS4d0YEG5+GFphXf9MM8YXRR1BwRo6uoHFLNtKRq+1zrIg2kK9yme
NvRDEFLjkSRhoLaOmdZjv9pPcUHkJoeddVAORAexSMkgfwpHhfpkXMWIEHGYHgqVX/hoqXxQBNwK
EglITUzj5IodBuHC6f750a1iiCQSi9h/rLDwXX/elMk1rWjYf1l2jkAsVfKP7ZIxqUWcm0PNAUsA
wuvoxCkVFs7L112wOqMOAHOfgGbPmTIZH73laK0qRNSmoqg+0LGlhSCrLXGdPYMbN1ILeRbEzOAW
cL3xp44ItNzO/WgP/frZIgTCyTnY1IJLLSeYtQhAnm1ld8QebCx2ITeKwO7H7v41hU9BXqgAFCEq
j8b9S7tqcC0Tkja3S+Q79yBnmjQ34ZZzf2RR7WkHf408gtA3UHG+blLV42z3jfiT+zqdBvINnxF8
rrX2iMZRmgIEIZ9chxpOx8Pj1bP+TCeNukrMZN8NV/RMYYxKBh++rfpKJF4wKTXaAQoTu4t1wg1N
rz6dzZeh3PLQQfzi5T3aLl0j3MLvPWYVYJmt7ajJ/iYmfGZI7NNGqk2unCdWHV4koAFjczs34qTa
6uDm8vKsv8ngHi7fj3iCM0JGT3aI0lKTqYCHLLBHGFRg7bvEfua74MFZZIQjalFTdbtlzeV4OKdp
aColDUf3om6mUoU6nPGE2v9OrEG8h8CeLT07VZ0zhY0H31z/MBKov27ojaflEHTJubpYBE5OhJ89
Wwbwg3paPX1Po+5FqzzU6HqrKs5vg1KlGOQtIdqikooTTmx8k1OeWk5Z+XS+5u4FaCv1uN2Ecct4
w2JLc/Ztt3i3juKLhJaQrsgyPU26eFVPDuVeL/B4d5zVgATRDq+eYml4QjTixKZMeu+DTQ56ITLu
AWp3prkaZNXW64Ybv+4wMMZbGIGCNXqDoQnSROsJNzECVoYpJJUBBhL6C7RoVYV89to73Os4kWI5
C7c84jQT9AWwtqkV3qV13Sqq7+b4qrBO/TY/UTEr8+7tsotwuHkdEgFDiTeCbYS/rJ3ecvAyuN0d
olLyPDXCuay1sIECjJE3/0bF4ZxnVCyxitrvsYGerR2FjZL9trYVGJWSiKYPEfSfbV7LulYyqeyW
IDY0n1MHAq6kZDtukmyGTdWEtwZWTmYfdA6djRTRb9MbQBC6QwEk+3TwbB8E64IIkDYcfGtrDYhN
y0hqIZAqZIQnLbOTLomkJPFHfuLgjQ+uCA85JzS7vueQ8mBJwrmMg4Djxlb+t0iGrdpmvIotBBTK
fCuxtnpIqsF+w/9HsrCCWZPgruhTCDWBAhFqfJ3O7U6f8NZxmYpRDhTiSej/B2ONgn0ee0b74TO7
3ksixu2dDAN9lsTUk8GrAsF8cPK5ciayFK4ogXDB8UUfpkXMu0Xo+VxTUIzYpZTjOAyMv4dnFGed
0pehZXosNqlFutU49Y5dvQCg6WdWfP9//AFO/H4GR4W9573ZoHpo0c2Xp0mqz/OP4Lr+RQkRDtN0
ZtyECla0IAmj0DISNmjqjVeeouGB3/j6Q+QF9j9hnQOzyCKwAgbVe5BbXhcrsk1T3Og4rOUitvvP
te1YHhGixi42j7InFXLNq9BSk2cZHwXcj67r8JbvNAqio9FVmDT6b9zASlXrnqcqtHO1JrW5WgX+
cq1KaMJwZauOeyFZcENJ/NU28VQ92Zv3fbVhPeX001KFMo5xN2Fmc5SB1A1iaHryKx8oaDReJyf5
kjQQFFPRhtbpUoxqdjRR8v7Jni2WQp+R18717ZEuNDScKhZdW5CDQ7vMY/7EkvEJD4HY1+G5hMBX
DU6xX70uthKvb9X2mmuoBWj9pTQv2S/TWTFs25AQB7kQYNomX21YoxH0ahd7jzZQtTRJjYbkp+o0
GJPcbLQMzl1wpZx4OsMNn257KNVo1Z2P8FFhurtD2C8CzKCqpBVqEIQ05WQrmFJIt5akRZZIm9fk
qAMMXmwyONBk3lUGy/tsB3JHjlMCdTApHlkWSKykiSvQqvPnBn4/AOnuOw69JXJfFT2OvIKZexnu
2KNOfhHujUqQ18c6HZZ3g3fKuxH3XmtU4wO4A92HsQefEBqRWx+cMGirz6Cuu56ux+NY/Kp3X82/
ck5x6mUmJ0F/KGM/PrsG/GLbwF3GYLfqQQQbD7s3In5i9mTq/wyWlc3/5+NMo6lLe7C2mm1Yyaw1
TMrHkj+/Vji5fD8xXer9ulmDpONTLls2tMFN/Bo1lvSPVUfAqSZFKy3WhR56OXo4FYvMFGEVN5Ex
Gv/uaX1UPrc406mJBJN7UysnucWlgq/XDqJFr7KKCjaVup48DhmYdU+x3tJLVKn6TYZdp7YpJ2qH
QR3qPmWkxbblrImnvIPA4ofnmGT15mKmsUi+ZaIBgEktU2p3fbSGxNj40eY+76o91RHE6IguvgI8
prjrUdzJbrX2sOdZQHIee96vxF68BIaLL5NrEvdRN2eM1wecDQbqRfyadE78h1TpuqTmA7kjhHdt
T++ReNgYqtLhnb73tYCAD6KeM2MAMxounTJ6aI9c5GBRudrbxt25QioTXmDjGa+XYf2Wroyt0fIE
AJmN4fzuj2DbXUE4+CPfDFH0tCxmMdn1EbK5m8qfZ4oUQS7k5uz6alDt6gapiGPf+HUlcp/amJ3/
nDmmM/C+dmVhmdnPYdtExroF5OsLa+fFy/nuF6wVPYkk1Bmtlg7uh7PLmrHcsmDR/fYJ9DuTWYzM
I9r7lYuWc150r1x2iSQzABD5WXmA6p23OSjLJWdBMk4P6/+NhK26piTm0PE/HNQlPjn2Pf+RpZm6
DL5TXggwxG9yZFNPmeZhbx+lEiCYUvQWhJz2bs2IOZswO/IxH4ElUIwZ4mTb5m1+KtS59WBdyt++
HmZLiXU0SyL9AjcJGJh3sLz1abJ/8waYXz7SpWgoIDzvAb7u1lXAHmpmlVVIjg6yNbMyuslXkV7n
U/YLtgKttuN1RbO9AwjL9mI3Od1v+sYhmbzkuy176Y7jt8PLqM222rXt5vWiyPoZsyHsyPR2z8d1
wMf7pSO+zU4plJhgEEULDFLIaU72jCbWaI57GUO8rxaQBIIrJYn0akPG45OZ+M9XFsjlxAkOIkIN
f651nXEGMx/SS4WkNqpeWuE52Gcc9zG6rfTSM5d4dBnHF0aRuvRL23LqRg6JHvGdiC8Yb36Eofpc
SkJ+in4PF2etsltuKbE73POo2kT3Se4Alq6Vh+awygDqNJOpxUsU9tAx9+M7aQDiO77Q7TOwARgp
jDmv3gKqIznYjJISCR9diAgNYibOTC8F7ajpqlQInlPwv8WP7+HcAshb/3lBRGoojZU1UUPxTvfE
01/5k9KrE2dKyrzhM30lZrNofNehChL95md2ta6GomL1PyrmCzUvCB0KFXN3n9+xduhvsPI4vciW
Y/BflsSTtSzIpV/HcCzfMevEAOt2+dr+QlqqANS99g0L0Xd4GP7mZXD7CT4eUD/gG/ZtrMroylbQ
MLflmN/HAvGqarE6yBzLvbhvpRCde9HCb4sN+VMlju9Bn9n+V18zajB8zK8Nt/HRwCA51vCOXDBt
wspMfh0Q43GPddweUgjrdCitApKqdNkw2h38h0GGvasYpBF2GAXvHHuvFSZKzJ8IgKVwQBxTxn+V
BxCJHLbDzeKNr8/y1sc3yJIzK6lANuoRmYvAbyH973MtDSQ82T5HAs+FbcuL9C63rBc3mVOGtjAF
8NOq97zyLtlO6fnhCVuLL7YwJxZl5uohjGAssPv/qUn6910r+KF5Qgz6EiowSREP2FKJtHXRkTAc
o3/CGi11S2R/8+uB+x65jlR4sK4mbFAMHxa+LN8T5KfgvW4K8f11ejMGwnozRe00QcakKhkZpfw3
AzC2vqjn8GA4n4+1LE4LbeAnEJPvKyPcC26zXj5/GyUqB+hkX4MFze0wqpcD1jrKDntzDDNn9+O+
PnrjoLqmkYabdzAxLjVYJJUUSCvJgzi6uBnQwoRS6RgBcVKDMWexYbnA1pyC5CZDB7xRHn+OGRlz
74ibpm2bbXLNTpU13O3bwGxC3ckB37Q1CpAIxqy6jnGqeHd03S3KJN8f8P+4SO5B4VoSgPkOdGPk
nu/it/e9W6BB8EjEWPIH5KT3rC2G9PNhYCnor9KsD1pHVV/lI6tHk1GnMI/OIFXEdX1Cxc1yAs2j
NmPLZzzuS32gKe7goO0vX5gpoHeNfBXSqKv+W37+x/svjwG5cs9XQ7R4a0Bx91IiF27gCtCmwule
VJk4+f43rtH50Ll+SLhVjYCmQ5RQm5B5n7ssvs7OFH95UXsYjEczbOo7FGX5eCoEFyVr0YrUhtq0
VLPOpumOzmZ8tagNcTXYB0z+aXku/B9+1SaSQnnjbV5ZU3yBA8r4iaJdHRxX2DBm0efF5+IwdJ22
Lp3m+0j0jPNBUYBnBlgiwGk2XuGUm629b4yBi0+p9QbgGeVqWCwBazRv4aBhR0ca24c1Etz6l7NG
gu0UrDdPJrhFnPvs4nVudh7b9RnJsK9Rr1gZxjfyNvUZmjqbkg+jMPb9dCglzYjQvXM23+fCT9Gm
dQm09IhX6kFNlTJ7wdSaw/qRu2knSjTl/KfsRIQFbJv6NybaTsPO2fqzzxFSfzOqzuW1qxqtM1gQ
bUW9ZRbAh9QvLqvR6+Ukk/C2atT34fSgleMsgwtonFJqNYMNpGKwaoADfSzxClezksG0kABBketB
wPx+MjvY1A9YHuzr3ddzhMaJ4WdBFD6Aq0GqMGCfhMPgZp2q5KM8NvSRwxI/SZFSrcQxIUz9OVPQ
Pv0O0uY/s+aHEgjFwQk/BlOoV1ioQGtagVTeobSJbYYUOnuXG4u2hhDqPMF3LvylUb+AdVNyRS9J
xNvATH30ASgfdRnmZ5yYKEeQGPMsRveR9nB+BRoPr2NA4KAW0l1IIoK9tX6V6GRcDN6P2xazGcxj
IpoS8vVe9/WXdZtO87SesY8el/Gknp8NElc9p/555uPsUf9D4nGctkF46aID0Jh5whUhzYltJFp3
aCYArtcDUUU4ap0yMJ32btIEGyFiJ1z/K+anw/hDFXpsbakmXxBN4UwE81Xhcu8hQj/y41KPY2Yj
KIAdSwK4y4Lk+jTerVn/nM2I4Kay7Yl56G/Pq+Gx/RxAewenWWbkwde6TJiExW/c6wtHkrfYnQ3K
XlFcq/YwLPmOswxBYEEOroS1CyJPlL6t11/pitQEmGpfhmaYgD4LQ1B8LUzAWbGNm038vMuvxHgv
8QlDnKiIKWvEv30pzj2MjbFUz6I7A0TDnUluTqNZOw5QsEqYW9V40XFHiIQtIouD2dYpL0cDDnw2
fGd7Yl/O9lfdnI+Puhv36lubS2j4IAE9rM+K8Di8Ytbd/0jaDjYzeMRpCqpK4yNBHLrEJrJM5FAg
6mfwoerZnTMOuH8Ncb7RM78XFbPhSSnOg8vSYOEUBIWbt7dbvmdXZl1lyJ3LgSQnjZoWDmSoNtP6
xKYENEMcPdE3Vxt554qPSIhr/ZEjnZVHXWnMi1vRafmf1GkHqTiZyqF9k5glXUkl56cai0KsagCG
2pe0fXFtyE94Hsy3Z6lIyaFdW2lq6tMYHspEZ33Xgx50asn+19ueWL/RYsnGRk8OGGM6yg4w7mI3
tuCgJVyNAxj1VnIHSx0dAdet/bGeVW5l8T06ev8pljI7+H+TpDGkwyX3k56/hW4EUVSYjICVvj+M
VhUud8BcB2p8U1FcnzQc91DoAFx7/I36zGitFPjdCkxtwb0snmFxbyI8vJ/NkSaZGQaIoecvchOx
KF41S0FqytJ3flfqZZgtyecms8ZuYEnt4jw9tbZUyO+x1Hvks/RXqUz8O3BzPacvHNVL9BtTTnbQ
jZiiWK54HWLpc6jFkVlVdDz5TOOkU15Nb+JjEFWSqBcSQLgShxsAgGUNV4WdEaPZGva5VWXRbFYG
XNzHPmcfwAj/lzb2yADIjVL2sjA07rfgMomt45aARZ7o0zyGWWoOWcUUxJSSKd9oRewsFgh9qd8u
xSICmBLGMraDbY9m3Gl26afdfgznJNk1ZVn7O7MEQm0Drm9Zepu8pwl6a/Gd2fVISJBv/Gij+qvM
S2sooifhUooPozG3baTzjc8+R0NkwjEkuzxuKK2L88/ZmRZyuBtbyxsnGQ6oRHTFXmUsPh6mxeIo
X/1ZhW97jAIdaMw9PDz/wpShyKauSqhlQrQSEU1tdryherXWOcWN/42Gcqg8TkUqeF5W4mxWTm4S
8wX51sd2AqivmpKKuORy+lQ718xTJYmfeNcEPVy6Tgovn+BZxFoaLdGrMOuMOQjQmbFlrQKVSH2Z
Sb4N9c4tZHmZqtX68PKsFD2fwkNZi6Brj/YgPe8NtjdFBWvy6TAhVnC0oMB1q91FxI/x7LTwwi+v
HxSU2yoe3R6y/ixSZFpBwFUtz17ukcFu2JZ6jHwg7HCZEeeDGAs3bDvyTxrGeQdXCkInd/4cSaAZ
DGHqSpVQSQbzMN6KN99lSqE43e13NiCFXFM/N79YhuZLA6hLC3ni19fE6CCgu2naad0PGj9TojH4
wwfyzuWalL0BpjYOCyqJpXju5BtUcYGgYkcvcgouHENZQR2mOXw9oO0QEl+C8QhNir1ZjXcBXifD
c0qDzx0Dr7CbguhVq/ODh/qLu5Q6c11O1v9MPQqt7qtz6mAS1MX+xh+eJxgNrvu/S0b9ZCr4Xyjy
wUBfpMd9+qcXTdF8OeWidn51boZ0O2RsAP2jkS0mi5TnUiJ40S7bzFNqYekiPAbUjVuyAutfHuhx
bIOYqwPrXhnbXbppxe7pd6mDo8+RQNQ9XYexkFEo+jjMMmTHHGA0ELBSAs4nMo503UPa+fJ8ZlsC
vszEZhHvCgG4QLi0L0v0H9RudlPw7+4yABvwO8nDGuFZB0Y2mVLOvpEL/IzWKVJbeR5s+C9yjXdM
uF8Eu1V8cL4HpaQgPL8Ey66SGcYwu7ujPhAA+HPGq05k20Fc5ks2il7h3Fk3FgBKLY2p2z1GmjYc
dCnLO35o9joacJLI/qTndoRO0aI+b7n5yLVxQnKbjYnid+e2h1op6A3K2oD2FRloKqKSNyKYHP5b
JgwLkeFiVeb6Hzjl7+hagt7c1bEbygUlMcyLvxsm9jdvIA7U8C2tPBR6VeLNbDhrVAKkHoRoHQ5V
m4aq4UZ5UAxvyw0iSGYp1TtEzRgODfe1j/8K/X1qHTK1m60dgy2MgvTH9SWYr9u314s3SHZ0KHdX
CXHp865WAXoRYubxrWuJSI7hwSdMVotH7+1mk+NOfZJvAokcQyM9SpYrXTBU6RUFKTdkTC+V/ld/
2gUwkM8zG6WsBStYUBFWf9VgLJw/mW1vdkY8K9GHvaH4+daV99Bpz2E3NPQxoySK8v6N3DsTyak6
3SjSTVOKc54e2F0qtyXLHYi1I07Zq8um26Z79GqnmquQzBF8HZqLD7CtZiUlzIVvNIafklEl0Hqd
gFTminD3HE9PWQd5qysXtt7m01jqprsTRJP0NsTmhKIL7JeDiBR4VyzPjg0yiex6GY4y2Gnc97hl
dUAl2XC7ost/GJkQzUTqp6RPLTU2EkLPCG70kS0I+OPPkjl6sTuz26wO44vsgUr1dNNolbn/63ia
/e0Jw7CnrBDgUIv2N1EpxmfPgzAb6+YYN/4sGs9c3rmq+h7pnEZRPPQooy2PPeM4o11vlpaKPBeV
0XWIPnCszYOwM9zwaNR5D6bEYh5b0d9yICGTzx2UH2xJKp25G+ycuMoM0yeSHXpuolc0YFiabRwl
o4yve4Kc4lNXaaIeeow1jVk3TJ+5C8i9WzXLtrJEzS7Vfu/8jhQVTNtVzl7s6NWfrjJbnfEyPT3d
C6qeyeawGIo+KlLA6e7vOkapfvU42yiAP3v7lU5jyCCEkg1plbFGfkfACfN4gs2JXJ6Xcy8gtwMF
eXxQot7mm30IH+Dra2bm1ktRhq8QlaV2OvWTuvMimNBTPk3nBUM+jpwb8YFs3VclA6YR1HLwnmok
5x16kOv3XVyUgIviNjgTt8K+W49Qa4mt80fJGwVTqaDq6mPMI2Bk8YEqhtqh6gbTC93VNgLwbev8
GwdicXa4IYzCbxFXIO6HTeVbc3qlg0/IeSg9+2cgq14EZ3DBmJtKr0WJI3FXrdGuq/WGHp3TaNpd
t/xCGOx+Gp4GDcFARkLfVCX7ozHDDbZ0NX56zyp0+SkfeUfF/OUt03km+xOKXxHKriNSvkxUmQ/O
3k3BmUIL0LNMfoAqsyxQZO39rr9+it4yTngWDBFSYxe5A878oh2cP5hKRT/kpb52pSK0k4PL438w
QaFXC0QXD3Sz2WwNcFCkmbV34j4vQ3O8s1Kfd7kGQKm1IK+5Jckn2XnPk0tb9r3U59Qzfmgqw2lo
92v8QdC3X10A6t0gAjFGyCfzWr6bA8s5LRRtFFKDAHo/2EBNiRImTG5wRSWlCK5Qbudd7T6P6ku8
eiFm/xPgZECmaEMVJu0OGOdC85YmTPxgui7UGY9D/YfK+3B4G9YHK/Nay5WZZHNd9Ls2Q6JiO3Zd
Xa5Oe6Btm56qQ4dD9u0cu17FHWjntDYo9Uy4tbgRfLGdo3In1sLz1oBHVDvMJ84Ert7+EMR3CGqs
H9fAp9y7+wsTSgHffzzf012KLKnFwwJ3vRyXig3PizlU2GCq4YKLQvJydapWbdTJ4KuG5y6iHR9J
c3iieWRiZPjrdbIQ4G+qtLvciSQjNLBZvxSARHEfYq7OKhR5ery8ZKqOl3yET4O+mQli8BNWLhzD
mrrscqPOFwEQljmduozh/Pl68rTUXBNNSmSX3t0d5ksxBb181B6sln9wiTR2pWtCaUf4nTG9adMc
DrLzfcmyreQ6wsYK8kq5i5nbQbROaiXJbra2ojI+3gIzyK/XOGHULf2rRfucK5KiWFuLSDC8ok7P
uLZu87kTdlMKvpsXAnu/LZD3mCs59k+aHNfY0LG7xUDLPCfam5QKlg1v7hmEh82TUhtkjeW4b3xf
KFZgssLbvyXzbpmqLELCBKOXuN8i5ezJhTzVDmyMvXRTP+8GXmGdBvzjtfxES1Xi2sKeeyc4BVbc
4mmHrOBTsEHFogo14HrUNhsTbH/YKnbrDrvICxs4WBboiL1ttGvqnuBn+7MYqax5a5bYy122jjaE
bRdPWozEUNozsdHGuFfVecBvm/U8HmFbspsdxA99KhKLjNFBtjt6RLL5lLrZtcShn69NJM+7NQ8V
SykoyglP9bTUJ0H9DoByY8anfYnfRFlnTf1cyW0IebTAGhuo6oXW0WWVD98vfcsBXTkBsxJ7OJ1V
8SMvmrxvJwY8CRSkeT4r8pIkwKO2meDPnovedwZFhwCb6TimD2ttm62MVKbgQ0lHAyWFVgzbiQnn
9+xohH1syvq+Fcq/TSld0b5k6x/J6l1BVQnuh9xj3JjSrpPrTppbE1DgZfvLx9cShmuo+ZWry8Ct
GdDjcsorAfrNv7Y5AU47OACaDFtXOIwI0+ajkrGW4gOp5163ti9EUWY4vL/+6oYhXZgKn0RT9PDe
52d/l2svB3Hf5B4pbqkSmpSgZHqYT3JePEuRhjpGH3nD3dvyEnr5MysENws80VgM2yz6OaUQalrb
onp1AeYlH2ErvFmnfHlPxfuM0EWG9G9CDI9O8VMXrJ5ORl+nP3/LG1Z9RbUxZdlSliBDtmGRCFkk
oSHRebOnuf17LjtCjDVEpRZGS8bO8Qxn2qswMX7kOLa53xZww1Xwpjv+b43YeuRVYFwmPbMRiidg
jlCZuYXV7b+txBh3xZWTo05qL+Uxul+MedKy1cEAEzKF3T99g2L8VyECEqqk5WhbTS6U5Hng8Db1
IGAA9LPrgCS7NezhR5TjoSOGlpEAcY4EraSZXIPOgvcYcESkhhbnJc84Ovw7bmQ7C+zhj8TU0UAW
2XEIn0u5DS1P41u4uPHAtrtZsScbF2vVfUlyCa1GKn7EJrDyuV8tDhUitKDtlH3OpdeY2MfKIx4s
6ssmx0R3ecX8V5CWzMn6Pfx7x8JlsisSy7+lQpTWQrPEKUc2jE0RIYKZ3RF0vWTqvnSqx4C+H/V9
BuOlSCACKBUuR/EyeWmQFaYvNH3wbX7KuPwdIIsgiZ4lkdP+aooq2sxPvnBxd8DSi+x9GCDKGHhD
L2PPa6JpyD8k27KNq7d83+qvmx3xpEj0Aqi21DENOWnwI8jE3n4jjJZlXTJqId2F1VpOq8t2KAUd
87DEJmg0MHQ89LRYHkkcX4i1W0lstEiPGktbd50SmGbokG9kM6EWtqxyEosx7xYiFrLJzgTZRJLf
Apm07dDoojw1JbpP/ynvkgKGugF+Ab909Mub1habLVHEkZOCbdsBGbOesL4ctqo3HvVwt/A9cHoE
z4SHs4vg8T/gIHvE4TgDOBEtUZTn4Ekn6mWxJTrGwHgStVGjoHI04QDt6w4MHVgU/8eOI6qRb4lq
urIFBFRp2BNZS6olYCEwvIMJ+sFcbsSErhtUpyEK8Oae08Oc0B5M6TKJSTOQPd61MHhND88G9ADZ
51Qpsw3zUOjOFUhpVZHHQY3zZGp1g2Fm/i4JUO4dZEfAUs79M9moybMlqXgpdOZ+6wPeCjDTTHx4
nfCFF8k3ZgPVUlMg4yX7uHGboIVoKYqNfEKgzpjoMMcW2D+5AiDYUWUpU4sCLMOYfu7RDfCIlDX6
32bxh8JVYzCwDfVRpESowO5zv/wtLUfBm2oIEXf2fsUXReEkHICxT1fBJKSMAMR1G3yoJPe5Q0Ep
dZznPm7UolKFyehdWhXT2j95SJd7pdkNvY6DLWtA1Bq7XQg/UPlbBzsY7aSOTPJN7cshXh9tK1sb
T2KV6CFtSmbK9NwTuyMVWyIsd/7OIpyfw6tWemfGggHgyhmJk+kLBElgzxoS7VIQPL948b0skJvC
G4CH0mLbctVV1/1MXJsQ2Wcjhrq9vm+1ua/XMlxlrK5Q97YlNncdVablk3ibpEys5hR8TXqDdVzM
UpuVkXsNudS6eK7bO5TwnkfNoT2r3IJVn/MNquLPShKm9W+TwfKmI2hXvmF3+/4qSXw7PwboFQzG
d8fpyGGEo9hWW0VDHWy0yKVK7U/1wAvO2AXholunUoTRu5m8q79OjU2bX8OWT0skes5289dCueUB
JZU9xFTt720zjlHASiEbtv07QppcYDPikI+Jsxxe5p9KY/NduLAjiUEH6DRS/K4I2ZUKgRnAf8f+
r0A9t7YJduN1sOf7Tu9T5ODSS6b1kribrcPbDnSYJtCFQ5dHKRi1TYJeLjGRolV1b3115y0UDsOd
4qqPCSxtJ4dIgw+lD96UYUwE+XjU8TU/sAArn6UKVc7VXvaBC2mXR7iFSzr0OacayYctNOWY50YW
TNcNDiaFG7hOFx7DBF81kRkuNaYOLrV8hEnlJGSlpAS1/70SNeAh9nrqodHi2mHI1gFgmxfTNhHn
cqTfar2BRKuMwqVVISmZj8kieEX4bhEhCS2/yvVSRfSNd5Z79Lkw6uHuY6GueI3VN3UnWXyW7fa2
XDytGTxW0+LEWoYkGN4f4ZKRe2hEG4upqQzi+RXTe1L5XqDj7Gro6U8FeIAOxUU3I4nQkADrKHFb
kiKiM9WGDM/9lNFFzXw/mrRvct0Ieqn5SkG19MSfPkj/72GtsvqdHJ3uYSALDfFk5hVgw9HTSOJc
WAfEAz/qRimwW/CLbFrZ7FsREO6qJQeQpCyZHSrn1dZprpP1ea2A9/xM+axx7Ugky1mWSxKWR7aK
y97WpHaL8Ml3xuDIIjEJG+XdOOXwmv6wBzW0iQ5u3gUD+y68/TjK+6kQit9us0D3IbyRkyknl8ux
dVsELamI40L132vGPcndy2qBQEpBw7nFFutzNX1zDUqIruaDNEkRzF93+pOW92amL9e4lVGfpXTI
CCosAcBYbMuscnthgMR8a2yIF4zfxxIw7EIPVYd3Ev1QQZtjubrE81oO4gH4Cb670yzAHO3bB8ox
ss3AhJhHyhUFQYWIh9mwKruUg9ZvKwLjosc21ezxWKXkRSyCBTkz7ptN4ygXCrSudhIBKfWvU14K
3c+OslexZz9W1L7nonpflx6TMCqJ4oLQGc+fevRAI6b2OagVUyueVcR7iq4v/pb6gG2oCbJdqB+n
nwXpuCz6EZf24BeiAf4U7i+o8hUYa8FXISOpNF3z7tktGqfOYk7GLHHErVTxP00bI0CxFCQzFBDZ
XBKiNA8BoTJy+tz8wHNKbbYfEcMqvtppOF9G9Xg5CVWur2CtWdmC8BHLoB0TuAZB9SFPguqTcOCy
JzLLkzvVjqTSLxQ5UsrQQ4SklZg5jSgtK89tMGeUKd5MDVGVfxeTZt0Tb0wxOCPE5SnEhFh1NBOS
lBlusxFGrZhDdrwmT4rjFJhJrLi2ruGcTzDqsEyZuTBtZytZQb2n2Q7dK9hGSN+NZz36tYpNmmf7
gmmUpvK2KaKTM/BoUzUbCxKuxDg2a+Dq23BOiUdIGnAxSFf8Mcz2sxBXg4Cxz+0sfhhoqX9CyHbg
CvVIe+LsODA0s2SFABVUH5xiz57fiM9wpBFEq1YhcxXVzLRN597CP0g/mtKSXLqVaLVm0cqPkqik
J1Gdz18fBASN421zVcH6mLmUVz2eBQt+FZpQQltQOKdEBwxBhg/pCf3vmerUJxD1PgDOKwmKU/WO
oqL2I7ts8nF/Yqtpc2fr+Begpr2pTOoynNFQT5vasgFjU5TUq/X5aNttF8Av4hTUvjnPRxHlWGb+
55c5ChnmrzHrN+DqE0DvR+5ZZQJR2VRDOX1len9rAtQL+EfkcTPssKuha6rJ0BcgvvdWH4lWKTCY
9ckUyNxT2WVt/XXAJuNnDKRqHYIYhd39N3ZZjGLrdJGLEvbQKSVzpK1DOlYSuW60r/5puYGeZRIM
3XoMVbZTCcr/MJ7G5P/MOkkkka8o8TCpDNVw8pA3xMwVKx8MBYpGC/Q2W4A1/ho1n2XjzQQyGBYK
cjwhnVyG1Ci9UsbPb5uOkYr39GyFzrB5VMz7f5jGL+f+2DkTctjufZU8z5SX3vFXnBPU0sJCBVno
UFQ0to70PEszMX2+kDytz2zt7/UXqTEBQMohRYFbdRumvOF+qt+Icpl4J2T8XD/4qC+9lt7U2nWd
yoteWnRbefjQwzlHqAKCo62KQq8zETKEV9zZiI58RJpAhasGn/Sea7h74u4n5g7Cgp38CamCq5Oz
bj9nwfrTPMfIBOcL0P8rmaYwJWP8Ala2Sc4+rFByAS1l7jVxer/bXuPvgXddM+iyZr0idX89I7Vy
sNIwiSQiTDOtJpHt7zmIZHH3LWevPhI27SDU2MS0zuY1Q8Bkro0S3sVVIlXspThUcZ20zdmuUu4j
nDWEE0OEf2Hzx9h3cVoNCBBWAS3rFeKdNKV3J6XaUDUlsq9o9LUMuDAMXRd+bXsQioz4dtZeOyzh
xT999brCP1MRgIxs+l6eHJZa25jbnHvZnXmyuj7GWW4Zcxdtzg1J/9y0QuXV5NKU4ujk4iBV6FAn
xWkz7fywctzwoSdv7LIebmRCnmeUUEHEgMo+YOA2pTFBrvi5fEXEwZApWWOitCOcU0ybUQn6WMVY
Iptv/mlRZblx19leP9FI0t3VS4egRL0FMOdQgKOo4QSBvnPsHBFl7azR6AYZZ196OaTd7TmU4XSb
UE8t+fUcB/13R3FajvnlYO/Ot92RPaYBU+ff2qvShRU3WsXyQO3zOjbQ8tgJ2NYMO02qGUoh8UAN
Yw78IbgDVxN1VRPk+TTA3gd9JpcDDC7XtSLvaL8BZip2QsFmlSUnckB4Cjd4bCH6hDiYlDAgnaom
E5+5VNFVNYQ1mEXCVVMmlsht6Rh6CgKL0pRL6Y+TbvLTX96w3uujKbYTFl3LuNl1n/1t2lvBd99r
4y9gRBwr1OYo53LKwFNTupJ3W63C5f18FVweW5ii9pfupdvPJLwvNEVUh2p0wV8U2VLAArO+XXLC
ZbNmWEUlvS8TYyirUcZ7Xl6oDBSOY/6LDDT4Ekc53UPZSifurJWp8e3YYzFUue9CBguTUQFy6fkl
Wrf2KxjHYpuLRp2xcAOqUeGVO4FLnAK5y1gzwS+5RshBErEIp5IvmQaKGuMKpv3D6WkaNfmMAltT
W6fbFHP/mNtFpyxmJESxTneYmz2+aQYNwRU1qZShQrHBSvHQN4kvu9lmrYELraX2QkT1xj8uXn92
dFYwVYz+efFpfjqBRQHFSGTn34vaWBfIBKh0U9cLyo6o1FHVdLvzr/e/6o73LgBFsNXq5O3N9DDx
xE27eOJu12boyGwlj05Czx4nf53eC5upjNBnUtdbrIqILTHWJcuALkjJuddDlcT9HF8pzwyDlFwg
/1saTGDe63AR+Q12X+D4JGFiIIzWG/oIne+WP2ZexXLPVSn7VYo6gYdUzmD9QPuVRn0k+J4GRCZy
v06nUAMLyHHjKHalt9GJ8Jh+aiGilJ7uEx3YZa0svV4tUHwxsN19Gil/QqZSw03dqf544VDzA0Tp
NZl53nIXR3+J2xejcZqpAaff8pfyhG0tUn0WqRzPDt/mrnWmq20sv3vsd4mEApzP+t6omGuj8ZYm
oV7Vei61Fs988+4ajjmyDgMiZMfT/XDlQHx3vd6kvMCyBV6sA0bj1GQ6mb0OW4B9yvMyL0tfPxUV
eYhK3zDerhqc3oD2FdpxYMwxSiTkY/jpSCPKrsPiuqqRWlh20k73qpBStoXA0rtoffR/g+Qyt32y
fFjdFwgrm1MdZxxm/seIdKlJFHfvGGv4w3XFRDAvCOF89Jqevn+oGnxqcH2hk/bMdsFaqsowHgkw
l/mL4iZCMdhU+yPzge/GjzYVMkTsQbHt0xwY+qDzsqqXvY6rw+EWgp08cYT6YHKwUv7ktdPHehvQ
JJtLbP+f+yEoZvOtmjNtp8bfsc7v4Fe7TjYMkIsVy78/euMErC8U4nukNKcnXmJPgujaPP30zRw3
ud4bHG5li5Txc7p5ZHafZgzMfNIJVWTN5Os5DuzA15hQEOw0eQKRKyUZskIRI9rly8+L47jz5RtT
BdJRd9TRrObqz7GJXtN2PnRxl7p7/X4kUZTQODzM3nX6CdKwHiRPY4be577v
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
