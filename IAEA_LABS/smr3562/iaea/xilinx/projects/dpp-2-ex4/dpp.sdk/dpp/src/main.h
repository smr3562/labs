/*
 * main.h
 *
 *  Created on: Oct 21, 2019
 *      Author: boss
 */

#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_


void InitSoPC();

int GetCommandId(u8 *u8Command, unsigned int uiCommandSize);
int GetCommandParameter(u8 *u8Command, unsigned int uiCommandSize, int iParameterId, char *szParameter);
void SendError(int iError);
void DoHostTasks();
void DoCommand001(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand002(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand003(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand004(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand005(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand006(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand007(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand008(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand009(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand010(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand011(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand012(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand013(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand014(u8 *u8Command, unsigned int uiCommandSize);
void DoCommand015(u8 *u8Command, unsigned int uiCommandSize);

//
// variables defined in this module
//
u8 u8Command[MEMORY_RX_BUFFER_SIZE];
u8 u8Replay[UART_TX_BUFFER_SIZE];

char *m_szCmdId[CMD_CNT]={"$FS", "$RS", "$SP", "$LS", "$FM", "$RM", "$AQ", "$WT", "$RT", "$CS","$GP","$GR","$SR","$VR","$RL" };

ip_scope IpScope;
void scope_SetDefaultUserParameters(u32 *prm);

#endif /* SRC_MAIN_H_ */



