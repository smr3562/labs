# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator. 
#


namespace eval ::xilinx::dsp::planaheaddriver {
	set Compilation {IP Catalog}
	set CompilationFlow {IP}
	set CreateInterfaceDocument {off}
	set DSPDevice {xc7z020}
	set DSPFamily {zynq}
	set DSPPackage {clg484}
	set DSPSpeed {-1}
	set FPGAClockPeriod -10
	set GenerateTestBench 0
	set HDLLanguage {vhdl}
	set IPOOCCacheRootPath {C:/Users/boss/AppData/Local/Xilinx/Sysgen/SysgenVivado/win64.o/ip}
	set IP_Auto_Infer {1}
	set IP_Categories_Text {System Generator for DSP}
	set IP_Common_Repos {0}
	set IP_Description {}
	set IP_Dir {}
	set IP_Library_Text {SysGen}
	set IP_LifeCycle_Menu {2}
	set IP_Logo {sysgen_icon_100.png}
	set IP_Name {ip_scope}
	set IP_Revision {203795285}
	set IP_Socket_IP {0}
	set IP_Socket_IP_Proj_Path {}
	set IP_Vendor_Text {User Company}
	set IP_Version_Text {1.2}
	set ImplStrategyName {Vivado Implementation Defaults}
	set PostProjectCreationProc {dsp_package_for_vivado_ip_integrator}
	set Project {ip_scope}
	set ProjectFiles {
		{{conv_pkg.vhd} -lib {xil_defaultlib}}
		{{synth_reg.vhd} -lib {xil_defaultlib}}
		{{synth_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{srl17e.vhd} -lib {xil_defaultlib}}
		{{srl33e.vhd} -lib {xil_defaultlib}}
		{{synth_reg_reg.vhd} -lib {xil_defaultlib}}
		{{single_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{xlclockdriver_rd.vhd} -lib {xil_defaultlib}}
		{{vivado_ip.tcl}}
		{{ip_scope_blk_mem_gen_i0_vivado.coe}}
		{{ip_scope_blk_mem_gen_i1_vivado.coe}}
		{{axibusdomain_axi_lite_interface_verilog.v}}
		{{conv_pkg.v}}
		{{synth_reg.v}}
		{{synth_reg_w_init.v}}
		{{convert_type.v}}
		{{ip_scope.mdd}}
		{{ip_scope_hw.h}}
		{{ip_scope.h}}
		{{ip_scope_sinit.c}}
		{{ip_scope.c}}
		{{ip_scope_linux.c}}
		{{ip_scope.mtcl}}
		{{Makefile.mak}}
		{{index.html}}
		{{ip_scope_entity_declarations.vhd} -lib {xil_defaultlib}}
		{{ip_scope.vhd} -lib {xil_defaultlib}}
		{{ip_scope_clock.xdc}}
		{{ip_scope.xdc}}
		{{C:/Users/boss/Dropbox/FPGA/SimulinkModelsForIPCores/IP_Scope/netlist/sysgen/ip_scope_false_paths.xdc}}
	}
	set SimPeriod -1
	set SimTime 0.0008
	set SimulationTime {800220.00000000 ns}
	set SynthStrategyName {Vivado Synthesis Defaults}
	set SynthesisTool {Vivado}
	set TargetDir {C:/Users/boss/Dropbox/FPGA/SimulinkModelsForIPCores/IP_Scope/netlist}
	set TopLevelModule {ip_scope}
	set TopLevelSimulinkHandle 2.00061
	set VHDLLib {xil_defaultlib}
	set TopLevelPortInterface {}
	dict set TopLevelPortInterface ch_trigger Name {ch_trigger}
	dict set TopLevelPortInterface ch_trigger Type Fix_16_14
	dict set TopLevelPortInterface ch_trigger ArithmeticType xlSigned
	dict set TopLevelPortInterface ch_trigger BinaryPoint 14
	dict set TopLevelPortInterface ch_trigger Width 16
	dict set TopLevelPortInterface ch_trigger DatFile {ip_scope_signaldomain_ch_trigger.dat}
	dict set TopLevelPortInterface ch_trigger IconText {ch_trigger}
	dict set TopLevelPortInterface ch_trigger Direction in
	dict set TopLevelPortInterface ch_trigger Period 1
	dict set TopLevelPortInterface ch_trigger Interface 0
	dict set TopLevelPortInterface ch_trigger InterfaceName {}
	dict set TopLevelPortInterface ch_trigger InterfaceString {DATA}
	dict set TopLevelPortInterface ch_trigger ClockDomain {signaldomain}
	dict set TopLevelPortInterface ch_trigger Locs {}
	dict set TopLevelPortInterface ch_trigger IOStandard {}
	dict set TopLevelPortInterface ch2 Name {ch2}
	dict set TopLevelPortInterface ch2 Type Fix_16_14
	dict set TopLevelPortInterface ch2 ArithmeticType xlSigned
	dict set TopLevelPortInterface ch2 BinaryPoint 14
	dict set TopLevelPortInterface ch2 Width 16
	dict set TopLevelPortInterface ch2 DatFile {ip_scope_signaldomain_ch2.dat}
	dict set TopLevelPortInterface ch2 IconText {ch2}
	dict set TopLevelPortInterface ch2 Direction in
	dict set TopLevelPortInterface ch2 Period 1
	dict set TopLevelPortInterface ch2 Interface 0
	dict set TopLevelPortInterface ch2 InterfaceName {}
	dict set TopLevelPortInterface ch2 InterfaceString {DATA}
	dict set TopLevelPortInterface ch2 ClockDomain {signaldomain}
	dict set TopLevelPortInterface ch2 Locs {}
	dict set TopLevelPortInterface ch2 IOStandard {}
	dict set TopLevelPortInterface ch1 Name {ch1}
	dict set TopLevelPortInterface ch1 Type Fix_16_14
	dict set TopLevelPortInterface ch1 ArithmeticType xlSigned
	dict set TopLevelPortInterface ch1 BinaryPoint 14
	dict set TopLevelPortInterface ch1 Width 16
	dict set TopLevelPortInterface ch1 DatFile {ip_scope_signaldomain_ch1.dat}
	dict set TopLevelPortInterface ch1 IconText {ch1}
	dict set TopLevelPortInterface ch1 Direction in
	dict set TopLevelPortInterface ch1 Period 1
	dict set TopLevelPortInterface ch1 Interface 0
	dict set TopLevelPortInterface ch1 InterfaceName {}
	dict set TopLevelPortInterface ch1 InterfaceString {DATA}
	dict set TopLevelPortInterface ch1 ClockDomain {signaldomain}
	dict set TopLevelPortInterface ch1 Locs {}
	dict set TopLevelPortInterface ch1 IOStandard {}
	dict set TopLevelPortInterface full Name {full}
	dict set TopLevelPortInterface full Type UFix_1_0
	dict set TopLevelPortInterface full ArithmeticType xlUnsigned
	dict set TopLevelPortInterface full BinaryPoint 0
	dict set TopLevelPortInterface full Width 1
	dict set TopLevelPortInterface full DatFile {ip_scope_signaldomain_full.dat}
	dict set TopLevelPortInterface full IconText { full}
	dict set TopLevelPortInterface full Direction out
	dict set TopLevelPortInterface full Period 1
	dict set TopLevelPortInterface full Interface 4
	dict set TopLevelPortInterface full InterfaceName {}
	dict set TopLevelPortInterface full InterfaceString {INTERRUPT}
	dict set TopLevelPortInterface full ClockDomain {signaldomain}
	dict set TopLevelPortInterface full Locs {}
	dict set TopLevelPortInterface full IOStandard {}
	dict set TopLevelPortInterface axibusdomain_aresetn Name {axibusdomain_aresetn}
	dict set TopLevelPortInterface axibusdomain_aresetn Type -
	dict set TopLevelPortInterface axibusdomain_aresetn ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_aresetn BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_aresetn Width 1
	dict set TopLevelPortInterface axibusdomain_aresetn DatFile {}
	dict set TopLevelPortInterface axibusdomain_aresetn IconText {axibusdomain_aresetn}
	dict set TopLevelPortInterface axibusdomain_aresetn Direction in
	dict set TopLevelPortInterface axibusdomain_aresetn Period 1
	dict set TopLevelPortInterface axibusdomain_aresetn Interface 8
	dict set TopLevelPortInterface axibusdomain_aresetn InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_aresetn InterfaceString {ARESETN}
	dict set TopLevelPortInterface axibusdomain_aresetn ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_aresetn Locs {}
	dict set TopLevelPortInterface axibusdomain_aresetn IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Name {axibusdomain_s_axi_awaddr}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Width 6
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr IconText {axibusdomain_s_axi_awaddr}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Name {axibusdomain_s_axi_awvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid IconText {axibusdomain_s_axi_awvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Name {axibusdomain_s_axi_awready}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_awready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_awready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready IconText {axibusdomain_s_axi_awready}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_awready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Name {axibusdomain_s_axi_wdata}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Width 32
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata IconText {axibusdomain_s_axi_wdata}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Name {axibusdomain_s_axi_wstrb}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Width 4
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb IconText {axibusdomain_s_axi_wstrb}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Name {axibusdomain_s_axi_wvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid IconText {axibusdomain_s_axi_wvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Name {axibusdomain_s_axi_wready}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_wready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_wready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready IconText {axibusdomain_s_axi_wready}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_wready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Name {axibusdomain_s_axi_bresp}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Width 2
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp IconText {axibusdomain_s_axi_bresp}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Name {axibusdomain_s_axi_bvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid IconText {axibusdomain_s_axi_bvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Name {axibusdomain_s_axi_bready}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_bready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_bready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready IconText {axibusdomain_s_axi_bready}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_bready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Name {axibusdomain_s_axi_araddr}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Width 6
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr IconText {axibusdomain_s_axi_araddr}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Name {axibusdomain_s_axi_arvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid IconText {axibusdomain_s_axi_arvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Name {axibusdomain_s_axi_arready}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_arready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_arready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_arready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready IconText {axibusdomain_s_axi_arready}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_arready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Name {axibusdomain_s_axi_rdata}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Width 32
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata IconText {axibusdomain_s_axi_rdata}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Name {axibusdomain_s_axi_rresp}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Width 2
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp IconText {axibusdomain_s_axi_rresp}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Name {axibusdomain_s_axi_rvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid IconText {axibusdomain_s_axi_rvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Name {axibusdomain_s_axi_rready}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_rready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_rready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready IconText {axibusdomain_s_axi_rready}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_rready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready IOStandard {}
	dict set TopLevelPortInterface signaldomain_clk Name {signaldomain_clk}
	dict set TopLevelPortInterface signaldomain_clk Type -
	dict set TopLevelPortInterface signaldomain_clk ArithmeticType xlUnsigned
	dict set TopLevelPortInterface signaldomain_clk BinaryPoint 0
	dict set TopLevelPortInterface signaldomain_clk Width 1
	dict set TopLevelPortInterface signaldomain_clk DatFile {}
	dict set TopLevelPortInterface signaldomain_clk Direction in
	dict set TopLevelPortInterface signaldomain_clk Period 1
	dict set TopLevelPortInterface signaldomain_clk Interface 6
	dict set TopLevelPortInterface signaldomain_clk InterfaceName {}
	dict set TopLevelPortInterface signaldomain_clk InterfaceString {CLOCK}
	dict set TopLevelPortInterface signaldomain_clk ClockDomain {signaldomain}
	dict set TopLevelPortInterface signaldomain_clk Locs {}
	dict set TopLevelPortInterface signaldomain_clk IOStandard {}
	dict set TopLevelPortInterface signaldomain_clk AssociatedInterfaces {}
	dict set TopLevelPortInterface signaldomain_clk AssociatedResets {}
	dict set TopLevelPortInterface axibusdomain_clk Name {axibusdomain_clk}
	dict set TopLevelPortInterface axibusdomain_clk Type -
	dict set TopLevelPortInterface axibusdomain_clk ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_clk BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_clk Width 1
	dict set TopLevelPortInterface axibusdomain_clk DatFile {}
	dict set TopLevelPortInterface axibusdomain_clk Direction in
	dict set TopLevelPortInterface axibusdomain_clk Period 1
	dict set TopLevelPortInterface axibusdomain_clk Interface 6
	dict set TopLevelPortInterface axibusdomain_clk InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_clk InterfaceString {CLOCK}
	dict set TopLevelPortInterface axibusdomain_clk ClockDomain {axibusdomain}
	dict set TopLevelPortInterface axibusdomain_clk Locs {}
	dict set TopLevelPortInterface axibusdomain_clk IOStandard {}
	dict set TopLevelPortInterface axibusdomain_clk AssociatedInterfaces {axibusdomain_s_axi }
	dict set TopLevelPortInterface axibusdomain_clk AssociatedResets {axibusdomain_aresetn }
	set MemoryMappedPort {}
	dict set MemoryMappedPort r7_clear Name {r7_clear}
	dict set MemoryMappedPort r7_clear Type UFix_32_0
	dict set MemoryMappedPort r7_clear ArithmeticType xlUnsigned
	dict set MemoryMappedPort r7_clear BinaryPoint 0
	dict set MemoryMappedPort r7_clear Width 32
	dict set MemoryMappedPort r7_clear DatFile {ip_scope_axibusdomain_r7_clear.dat}
	dict set MemoryMappedPort r7_clear AddressOffset 0
	dict set MemoryMappedPort r7_clear IconText {r7_clear}
	dict set MemoryMappedPort r7_clear Direction in
	dict set MemoryMappedPort r7_clear Period 1
	dict set MemoryMappedPort r7_clear Interface 2
	dict set MemoryMappedPort r7_clear InterfaceName {}
	dict set MemoryMappedPort r7_clear InterfaceString {CONTROL}
	dict set MemoryMappedPort r7_clear ClockDomain {axibusdomain}
	dict set MemoryMappedPort r7_clear Locs {}
	dict set MemoryMappedPort r7_clear IOStandard {}
	dict set MemoryMappedPort r6_delay Name {r6_delay}
	dict set MemoryMappedPort r6_delay Type UFix_32_0
	dict set MemoryMappedPort r6_delay ArithmeticType xlUnsigned
	dict set MemoryMappedPort r6_delay BinaryPoint 0
	dict set MemoryMappedPort r6_delay Width 32
	dict set MemoryMappedPort r6_delay DatFile {ip_scope_axibusdomain_r6_delay.dat}
	dict set MemoryMappedPort r6_delay AddressOffset 4
	dict set MemoryMappedPort r6_delay IconText {r6_delay}
	dict set MemoryMappedPort r6_delay Direction in
	dict set MemoryMappedPort r6_delay Period 1
	dict set MemoryMappedPort r6_delay Interface 2
	dict set MemoryMappedPort r6_delay InterfaceName {}
	dict set MemoryMappedPort r6_delay InterfaceString {CONTROL}
	dict set MemoryMappedPort r6_delay ClockDomain {axibusdomain}
	dict set MemoryMappedPort r6_delay Locs {}
	dict set MemoryMappedPort r6_delay IOStandard {}
	dict set MemoryMappedPort r5_enable Name {r5_enable}
	dict set MemoryMappedPort r5_enable Type UFix_32_0
	dict set MemoryMappedPort r5_enable ArithmeticType xlUnsigned
	dict set MemoryMappedPort r5_enable BinaryPoint 0
	dict set MemoryMappedPort r5_enable Width 32
	dict set MemoryMappedPort r5_enable DatFile {ip_scope_axibusdomain_r5_enable.dat}
	dict set MemoryMappedPort r5_enable AddressOffset 8
	dict set MemoryMappedPort r5_enable IconText {r5_enable}
	dict set MemoryMappedPort r5_enable Direction in
	dict set MemoryMappedPort r5_enable Period 1
	dict set MemoryMappedPort r5_enable Interface 2
	dict set MemoryMappedPort r5_enable InterfaceName {}
	dict set MemoryMappedPort r5_enable InterfaceString {CONTROL}
	dict set MemoryMappedPort r5_enable ClockDomain {axibusdomain}
	dict set MemoryMappedPort r5_enable Locs {}
	dict set MemoryMappedPort r5_enable IOStandard {}
	dict set MemoryMappedPort r4_threshold Name {r4_threshold}
	dict set MemoryMappedPort r4_threshold Type UFix_32_0
	dict set MemoryMappedPort r4_threshold ArithmeticType xlUnsigned
	dict set MemoryMappedPort r4_threshold BinaryPoint 0
	dict set MemoryMappedPort r4_threshold Width 32
	dict set MemoryMappedPort r4_threshold DatFile {ip_scope_axibusdomain_r4_threshold.dat}
	dict set MemoryMappedPort r4_threshold AddressOffset 12
	dict set MemoryMappedPort r4_threshold IconText {r4_threshold}
	dict set MemoryMappedPort r4_threshold Direction in
	dict set MemoryMappedPort r4_threshold Period 1
	dict set MemoryMappedPort r4_threshold Interface 2
	dict set MemoryMappedPort r4_threshold InterfaceName {}
	dict set MemoryMappedPort r4_threshold InterfaceString {CONTROL}
	dict set MemoryMappedPort r4_threshold ClockDomain {axibusdomain}
	dict set MemoryMappedPort r4_threshold Locs {}
	dict set MemoryMappedPort r4_threshold IOStandard {}
	dict set MemoryMappedPort r3_dina Name {r3_dina}
	dict set MemoryMappedPort r3_dina Type UFix_32_0
	dict set MemoryMappedPort r3_dina ArithmeticType xlUnsigned
	dict set MemoryMappedPort r3_dina BinaryPoint 0
	dict set MemoryMappedPort r3_dina Width 32
	dict set MemoryMappedPort r3_dina DatFile {ip_scope_axibusdomain_r3_dina.dat}
	dict set MemoryMappedPort r3_dina AddressOffset 16
	dict set MemoryMappedPort r3_dina IconText {r3_dina}
	dict set MemoryMappedPort r3_dina Direction in
	dict set MemoryMappedPort r3_dina Period 1
	dict set MemoryMappedPort r3_dina Interface 2
	dict set MemoryMappedPort r3_dina InterfaceName {}
	dict set MemoryMappedPort r3_dina InterfaceString {CONTROL}
	dict set MemoryMappedPort r3_dina ClockDomain {axibusdomain}
	dict set MemoryMappedPort r3_dina Locs {}
	dict set MemoryMappedPort r3_dina IOStandard {}
	dict set MemoryMappedPort r2_wea Name {r2_wea}
	dict set MemoryMappedPort r2_wea Type UFix_32_0
	dict set MemoryMappedPort r2_wea ArithmeticType xlUnsigned
	dict set MemoryMappedPort r2_wea BinaryPoint 0
	dict set MemoryMappedPort r2_wea Width 32
	dict set MemoryMappedPort r2_wea DatFile {ip_scope_axibusdomain_r2_wea.dat}
	dict set MemoryMappedPort r2_wea AddressOffset 20
	dict set MemoryMappedPort r2_wea IconText {r2_wea}
	dict set MemoryMappedPort r2_wea Direction in
	dict set MemoryMappedPort r2_wea Period 1
	dict set MemoryMappedPort r2_wea Interface 2
	dict set MemoryMappedPort r2_wea InterfaceName {}
	dict set MemoryMappedPort r2_wea InterfaceString {CONTROL}
	dict set MemoryMappedPort r2_wea ClockDomain {axibusdomain}
	dict set MemoryMappedPort r2_wea Locs {}
	dict set MemoryMappedPort r2_wea IOStandard {}
	dict set MemoryMappedPort r1_addra Name {r1_addra}
	dict set MemoryMappedPort r1_addra Type UFix_32_0
	dict set MemoryMappedPort r1_addra ArithmeticType xlUnsigned
	dict set MemoryMappedPort r1_addra BinaryPoint 0
	dict set MemoryMappedPort r1_addra Width 32
	dict set MemoryMappedPort r1_addra DatFile {ip_scope_axibusdomain_r1_addra.dat}
	dict set MemoryMappedPort r1_addra AddressOffset 24
	dict set MemoryMappedPort r1_addra IconText {r1_addra}
	dict set MemoryMappedPort r1_addra Direction in
	dict set MemoryMappedPort r1_addra Period 1
	dict set MemoryMappedPort r1_addra Interface 2
	dict set MemoryMappedPort r1_addra InterfaceName {}
	dict set MemoryMappedPort r1_addra InterfaceString {CONTROL}
	dict set MemoryMappedPort r1_addra ClockDomain {axibusdomain}
	dict set MemoryMappedPort r1_addra Locs {}
	dict set MemoryMappedPort r1_addra IOStandard {}
	dict set MemoryMappedPort r8_full Name {r8_full}
	dict set MemoryMappedPort r8_full Type UFix_32_0
	dict set MemoryMappedPort r8_full ArithmeticType xlUnsigned
	dict set MemoryMappedPort r8_full BinaryPoint 0
	dict set MemoryMappedPort r8_full Width 32
	dict set MemoryMappedPort r8_full DatFile {ip_scope_axibusdomain_r8_full.dat}
	dict set MemoryMappedPort r8_full AddressOffset 28
	dict set MemoryMappedPort r8_full IconText {r8_full}
	dict set MemoryMappedPort r8_full Direction out
	dict set MemoryMappedPort r8_full Period 1
	dict set MemoryMappedPort r8_full Interface 2
	dict set MemoryMappedPort r8_full InterfaceName {}
	dict set MemoryMappedPort r8_full InterfaceString {CONTROL}
	dict set MemoryMappedPort r8_full ClockDomain {axibusdomain}
	dict set MemoryMappedPort r8_full Locs {}
	dict set MemoryMappedPort r8_full IOStandard {}
	dict set MemoryMappedPort r9_douta Name {r9_douta}
	dict set MemoryMappedPort r9_douta Type UFix_32_0
	dict set MemoryMappedPort r9_douta ArithmeticType xlUnsigned
	dict set MemoryMappedPort r9_douta BinaryPoint 0
	dict set MemoryMappedPort r9_douta Width 32
	dict set MemoryMappedPort r9_douta DatFile {ip_scope_axibusdomain_r9_douta.dat}
	dict set MemoryMappedPort r9_douta AddressOffset 32
	dict set MemoryMappedPort r9_douta IconText {r9_douta}
	dict set MemoryMappedPort r9_douta Direction out
	dict set MemoryMappedPort r9_douta Period 1
	dict set MemoryMappedPort r9_douta Interface 2
	dict set MemoryMappedPort r9_douta InterfaceName {}
	dict set MemoryMappedPort r9_douta InterfaceString {CONTROL}
	dict set MemoryMappedPort r9_douta ClockDomain {axibusdomain}
	dict set MemoryMappedPort r9_douta Locs {}
	dict set MemoryMappedPort r9_douta IOStandard {}
}

source SgPaProject.tcl
::xilinx::dsp::planaheadworker::dsp_create_project