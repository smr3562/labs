#ifndef IP_SCOPE__H
#define IP_SCOPE__H
#ifdef __cplusplus
extern "C" {
#endif
/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "ip_scope_hw.h"
/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 ip_scope_BaseAddress;
} ip_scope_Config;
#endif
/**
* The ip_scope driver instance data. The user is required to
* allocate a variable of this type for every ip_scope device in the system.
* A pointer to a variable of this type is then passed to the driver
* API functions.
*/
typedef struct {
    u32 ip_scope_BaseAddress;
    u32 IsReady;
} ip_scope;
/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define ip_scope_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define ip_scope_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define ip_scope_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define ip_scope_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif
/************************** Function Prototypes *****************************/
#ifndef __linux__
int ip_scope_Initialize(ip_scope *InstancePtr, u16 DeviceId);
ip_scope_Config* ip_scope_LookupConfig(u16 DeviceId);
int ip_scope_CfgInitialize(ip_scope *InstancePtr, ip_scope_Config *ConfigPtr);
#else
int ip_scope_Initialize(ip_scope *InstancePtr, const char* InstanceName);
int ip_scope_Release(ip_scope *InstancePtr);
#endif
/**
* Write to r7_clear gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r7_clear instance to operate on.
* @param	Data is value to be written to gateway r7_clear.
*
* @return	None.
*
* @note    .
*
*/
void ip_scope_r7_clear_write(ip_scope *InstancePtr, u32 Data);
/**
* Read from r7_clear gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r7_clear instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r7_clear_read(ip_scope *InstancePtr);
/**
* Write to r6_delay gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r6_delay instance to operate on.
* @param	Data is value to be written to gateway r6_delay.
*
* @return	None.
*
* @note    .
*
*/
void ip_scope_r6_delay_write(ip_scope *InstancePtr, u32 Data);
/**
* Read from r6_delay gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r6_delay instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r6_delay_read(ip_scope *InstancePtr);
/**
* Write to r5_enable gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r5_enable instance to operate on.
* @param	Data is value to be written to gateway r5_enable.
*
* @return	None.
*
* @note    .
*
*/
void ip_scope_r5_enable_write(ip_scope *InstancePtr, u32 Data);
/**
* Read from r5_enable gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r5_enable instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r5_enable_read(ip_scope *InstancePtr);
/**
* Write to r4_threshold gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r4_threshold instance to operate on.
* @param	Data is value to be written to gateway r4_threshold.
*
* @return	None.
*
* @note    .
*
*/
void ip_scope_r4_threshold_write(ip_scope *InstancePtr, u32 Data);
/**
* Read from r4_threshold gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r4_threshold instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r4_threshold_read(ip_scope *InstancePtr);
/**
* Write to r3_dina gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r3_dina instance to operate on.
* @param	Data is value to be written to gateway r3_dina.
*
* @return	None.
*
* @note    .
*
*/
void ip_scope_r3_dina_write(ip_scope *InstancePtr, u32 Data);
/**
* Read from r3_dina gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r3_dina instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r3_dina_read(ip_scope *InstancePtr);
/**
* Write to r2_wea gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r2_wea instance to operate on.
* @param	Data is value to be written to gateway r2_wea.
*
* @return	None.
*
* @note    .
*
*/
void ip_scope_r2_wea_write(ip_scope *InstancePtr, u32 Data);
/**
* Read from r2_wea gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r2_wea instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r2_wea_read(ip_scope *InstancePtr);
/**
* Write to r1_addra gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r1_addra instance to operate on.
* @param	Data is value to be written to gateway r1_addra.
*
* @return	None.
*
* @note    .
*
*/
void ip_scope_r1_addra_write(ip_scope *InstancePtr, u32 Data);
/**
* Read from r1_addra gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r1_addra instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r1_addra_read(ip_scope *InstancePtr);
/**
* Read from r8_full gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r8_full instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r8_full_read(ip_scope *InstancePtr);
/**
* Read from r9_douta gateway of ip_scope. Assignments are LSB-justified.
*
* @param	InstancePtr is the r9_douta instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_scope_r9_douta_read(ip_scope *InstancePtr);
#ifdef __cplusplus
}
#endif
#endif
