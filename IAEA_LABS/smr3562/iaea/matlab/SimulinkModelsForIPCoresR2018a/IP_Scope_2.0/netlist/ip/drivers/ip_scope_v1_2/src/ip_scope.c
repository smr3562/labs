#include "ip_scope.h"
#ifndef __linux__
int ip_scope_CfgInitialize(ip_scope *InstancePtr, ip_scope_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->ip_scope_BaseAddress = ConfigPtr->ip_scope_BaseAddress;

    InstancePtr->IsReady = 1;
    return XST_SUCCESS;
}
#endif
void ip_scope_r7_clear_write(ip_scope *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_scope_WriteReg(InstancePtr->ip_scope_BaseAddress, 0, Data);
}
u32 ip_scope_r7_clear_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 0);
    return Data;
}
void ip_scope_r6_delay_write(ip_scope *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_scope_WriteReg(InstancePtr->ip_scope_BaseAddress, 4, Data);
}
u32 ip_scope_r6_delay_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 4);
    return Data;
}
void ip_scope_r5_enable_write(ip_scope *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_scope_WriteReg(InstancePtr->ip_scope_BaseAddress, 8, Data);
}
u32 ip_scope_r5_enable_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 8);
    return Data;
}
void ip_scope_r4_threshold_write(ip_scope *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_scope_WriteReg(InstancePtr->ip_scope_BaseAddress, 12, Data);
}
u32 ip_scope_r4_threshold_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 12);
    return Data;
}
void ip_scope_r3_dina_write(ip_scope *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_scope_WriteReg(InstancePtr->ip_scope_BaseAddress, 16, Data);
}
u32 ip_scope_r3_dina_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 16);
    return Data;
}
void ip_scope_r2_wea_write(ip_scope *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_scope_WriteReg(InstancePtr->ip_scope_BaseAddress, 20, Data);
}
u32 ip_scope_r2_wea_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 20);
    return Data;
}
void ip_scope_r1_addra_write(ip_scope *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_scope_WriteReg(InstancePtr->ip_scope_BaseAddress, 24, Data);
}
u32 ip_scope_r1_addra_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 24);
    return Data;
}
u32 ip_scope_r8_full_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 28);
    return Data;
}
u32 ip_scope_r9_douta_read(ip_scope *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_scope_ReadReg(InstancePtr->ip_scope_BaseAddress, 32);
    return Data;
}
