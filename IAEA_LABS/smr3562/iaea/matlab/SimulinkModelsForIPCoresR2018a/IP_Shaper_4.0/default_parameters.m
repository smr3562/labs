clear all
Tclk=20.0e-9;
taud=0.5e-6;
taur=0.1e-6;
amplitude=1.85;
dc_offset_amplifier=-0.9;
dc_offset_fpga=-0.9;
noise = 0;

gain_s=(taur/taud)^(taur/(taud-taur))-(taur/taud)^(taud/(taud-taur));
gain_z=exp(-Tclk/taud)-exp(-Tclk/taur); %theory
gain_sp=(gain_s/gain_z);
gain_a=amplitude/gain_s; 

%gain_a=amplitude; 
%gain_sp=amplitude;

b10=exp(-Tclk/taud);
b20=exp(-Tclk/taur);
b00 = gain_sp;
Taupk=1e-6;
Taupk_top=0.5e-6;
na = (Taupk          )/Tclk;
nb = (Taupk+Taupk_top)/Tclk;
Bna = 10;
Bnb = 10;
na_inv = 1/na;

