/**
* @file ip_shaper_sinit.c
*
* The implementation of the ip_shaper driver's static initialzation
* functionality.
*
* @note
*
* None
*
*/
#ifndef __linux__
#include "xstatus.h"
#include "xparameters.h"
#include "ip_shaper.h"
extern ip_shaper_Config ip_shaper_ConfigTable[];
/**
* Lookup the device configuration based on the unique device ID.  The table
* ConfigTable contains the configuration info for each device in the system.
*
* @param DeviceId is the device identifier to lookup.
*
* @return
*     - A pointer of data type ip_shaper_Config which
*    points to the device configuration if DeviceID is found.
*    - NULL if DeviceID is not found.
*
* @note    None.
*
*/
ip_shaper_Config *ip_shaper_LookupConfig(u16 DeviceId) {
    ip_shaper_Config *ConfigPtr = NULL;
    int Index;
    for (Index = 0; Index < XPAR_IP_SHAPER_NUM_INSTANCES; Index++) {
        if (ip_shaper_ConfigTable[Index].DeviceId == DeviceId) {
            ConfigPtr = &ip_shaper_ConfigTable[Index];
            break;
        }
    }
    return ConfigPtr;
}
int ip_shaper_Initialize(ip_shaper *InstancePtr, u16 DeviceId) {
    ip_shaper_Config *ConfigPtr;
    Xil_AssertNonvoid(InstancePtr != NULL);
    ConfigPtr = ip_shaper_LookupConfig(DeviceId);
    if (ConfigPtr == NULL) {
        InstancePtr->IsReady = 0;
        return (XST_DEVICE_NOT_FOUND);
    }
    return ip_shaper_CfgInitialize(InstancePtr, ConfigPtr);
}
#endif
