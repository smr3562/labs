-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
entity ip_shaper_stub is
  port (
    data_in : in std_logic_vector( 16-1 downto 0 );
    axi_clk_domain_clk : in std_logic;
    filter_clk_domain_clk : in std_logic;
    axi_clk_domain_aresetn : in std_logic;
    axi_clk_domain_s_axi_awaddr : in std_logic_vector( 5-1 downto 0 );
    axi_clk_domain_s_axi_awvalid : in std_logic;
    axi_clk_domain_s_axi_wdata : in std_logic_vector( 32-1 downto 0 );
    axi_clk_domain_s_axi_wstrb : in std_logic_vector( 4-1 downto 0 );
    axi_clk_domain_s_axi_wvalid : in std_logic;
    axi_clk_domain_s_axi_bready : in std_logic;
    axi_clk_domain_s_axi_araddr : in std_logic_vector( 5-1 downto 0 );
    axi_clk_domain_s_axi_arvalid : in std_logic;
    axi_clk_domain_s_axi_rready : in std_logic;
    impulse_out : out std_logic_vector( 16-1 downto 0 );
    rect_out : out std_logic_vector( 16-1 downto 0 );
    shaper_out : out std_logic_vector( 16-1 downto 0 );
    axi_clk_domain_s_axi_awready : out std_logic;
    axi_clk_domain_s_axi_wready : out std_logic;
    axi_clk_domain_s_axi_bresp : out std_logic_vector( 2-1 downto 0 );
    axi_clk_domain_s_axi_bvalid : out std_logic;
    axi_clk_domain_s_axi_arready : out std_logic;
    axi_clk_domain_s_axi_rdata : out std_logic_vector( 32-1 downto 0 );
    axi_clk_domain_s_axi_rresp : out std_logic_vector( 2-1 downto 0 );
    axi_clk_domain_s_axi_rvalid : out std_logic
  );
end ip_shaper_stub;
architecture structural of ip_shaper_stub is 
begin
  sysgen_dut : entity xil_defaultlib.ip_shaper 
  port map (
    data_in => data_in,
    axi_clk_domain_clk => axi_clk_domain_clk,
    filter_clk_domain_clk => filter_clk_domain_clk,
    axi_clk_domain_aresetn => axi_clk_domain_aresetn,
    axi_clk_domain_s_axi_awaddr => axi_clk_domain_s_axi_awaddr,
    axi_clk_domain_s_axi_awvalid => axi_clk_domain_s_axi_awvalid,
    axi_clk_domain_s_axi_wdata => axi_clk_domain_s_axi_wdata,
    axi_clk_domain_s_axi_wstrb => axi_clk_domain_s_axi_wstrb,
    axi_clk_domain_s_axi_wvalid => axi_clk_domain_s_axi_wvalid,
    axi_clk_domain_s_axi_bready => axi_clk_domain_s_axi_bready,
    axi_clk_domain_s_axi_araddr => axi_clk_domain_s_axi_araddr,
    axi_clk_domain_s_axi_arvalid => axi_clk_domain_s_axi_arvalid,
    axi_clk_domain_s_axi_rready => axi_clk_domain_s_axi_rready,
    impulse_out => impulse_out,
    rect_out => rect_out,
    shaper_out => shaper_out,
    axi_clk_domain_s_axi_awready => axi_clk_domain_s_axi_awready,
    axi_clk_domain_s_axi_wready => axi_clk_domain_s_axi_wready,
    axi_clk_domain_s_axi_bresp => axi_clk_domain_s_axi_bresp,
    axi_clk_domain_s_axi_bvalid => axi_clk_domain_s_axi_bvalid,
    axi_clk_domain_s_axi_arready => axi_clk_domain_s_axi_arready,
    axi_clk_domain_s_axi_rdata => axi_clk_domain_s_axi_rdata,
    axi_clk_domain_s_axi_rresp => axi_clk_domain_s_axi_rresp,
    axi_clk_domain_s_axi_rvalid => axi_clk_domain_s_axi_rvalid
  );
end structural;
