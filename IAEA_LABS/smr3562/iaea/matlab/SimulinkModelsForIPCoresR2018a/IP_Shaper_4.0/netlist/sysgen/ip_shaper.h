#ifndef IP_SHAPER__H
#define IP_SHAPER__H
#ifdef __cplusplus
extern "C" {
#endif
/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "ip_shaper_hw.h"
/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 ip_shaper_BaseAddress;
} ip_shaper_Config;
#endif
/**
* The ip_shaper driver instance data. The user is required to
* allocate a variable of this type for every ip_shaper device in the system.
* A pointer to a variable of this type is then passed to the driver
* API functions.
*/
typedef struct {
    u32 ip_shaper_BaseAddress;
    u32 IsReady;
} ip_shaper;
/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define ip_shaper_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define ip_shaper_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define ip_shaper_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define ip_shaper_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif
/************************** Function Prototypes *****************************/
#ifndef __linux__
int ip_shaper_Initialize(ip_shaper *InstancePtr, u16 DeviceId);
ip_shaper_Config* ip_shaper_LookupConfig(u16 DeviceId);
int ip_shaper_CfgInitialize(ip_shaper *InstancePtr, ip_shaper_Config *ConfigPtr);
#else
int ip_shaper_Initialize(ip_shaper *InstancePtr, const char* InstanceName);
int ip_shaper_Release(ip_shaper *InstancePtr);
#endif
/**
* Write to r6_b20 gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r6_b20 instance to operate on.
* @param	Data is value to be written to gateway r6_b20.
*
* @return	None.
*
* @note    .
*
*/
void ip_shaper_r6_b20_write(ip_shaper *InstancePtr, u32 Data);
/**
* Read from r6_b20 gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r6_b20 instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_shaper_r6_b20_read(ip_shaper *InstancePtr);
/**
* Write to r5_b00 gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r5_b00 instance to operate on.
* @param	Data is value to be written to gateway r5_b00.
*
* @return	None.
*
* @note    .
*
*/
void ip_shaper_r5_b00_write(ip_shaper *InstancePtr, u32 Data);
/**
* Read from r5_b00 gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r5_b00 instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_shaper_r5_b00_read(ip_shaper *InstancePtr);
/**
* Write to r4_na_inv gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r4_na_inv instance to operate on.
* @param	Data is value to be written to gateway r4_na_inv.
*
* @return	None.
*
* @note    .
*
*/
void ip_shaper_r4_na_inv_write(ip_shaper *InstancePtr, u32 Data);
/**
* Read from r4_na_inv gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r4_na_inv instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_shaper_r4_na_inv_read(ip_shaper *InstancePtr);
/**
* Write to r3_nb gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r3_nb instance to operate on.
* @param	Data is value to be written to gateway r3_nb.
*
* @return	None.
*
* @note    .
*
*/
void ip_shaper_r3_nb_write(ip_shaper *InstancePtr, u32 Data);
/**
* Read from r3_nb gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r3_nb instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_shaper_r3_nb_read(ip_shaper *InstancePtr);
/**
* Write to r2_na gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r2_na instance to operate on.
* @param	Data is value to be written to gateway r2_na.
*
* @return	None.
*
* @note    .
*
*/
void ip_shaper_r2_na_write(ip_shaper *InstancePtr, u32 Data);
/**
* Read from r2_na gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r2_na instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_shaper_r2_na_read(ip_shaper *InstancePtr);
/**
* Write to r1_b10 gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r1_b10 instance to operate on.
* @param	Data is value to be written to gateway r1_b10.
*
* @return	None.
*
* @note    .
*
*/
void ip_shaper_r1_b10_write(ip_shaper *InstancePtr, u32 Data);
/**
* Read from r1_b10 gateway of ip_shaper. Assignments are LSB-justified.
*
* @param	InstancePtr is the r1_b10 instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_shaper_r1_b10_read(ip_shaper *InstancePtr);
#ifdef __cplusplus
}
#endif
#endif
