--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
--Date        : Sat May 16 17:17:31 2020
--Host        : LAPTOP-QL3HLA93 running 64-bit major release  (build 9200)
--Command     : generate_target ip_shaper_bd_wrapper.bd
--Design      : ip_shaper_bd_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ip_shaper_bd_wrapper is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC;
    CLK_IN1_D_clk_p : in STD_LOGIC;
    data_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    filter_clk_domain_clk : in STD_LOGIC;
    impulse_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rect_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset_rtl : in STD_LOGIC;
    shaper_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end ip_shaper_bd_wrapper;

architecture STRUCTURE of ip_shaper_bd_wrapper is
  component ip_shaper_bd is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC;
    CLK_IN1_D_clk_p : in STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    data_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    filter_clk_domain_clk : in STD_LOGIC;
    impulse_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rect_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    shaper_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component ip_shaper_bd;
begin
ip_shaper_bd_i: component ip_shaper_bd
     port map (
      CLK_IN1_D_clk_n => CLK_IN1_D_clk_n,
      CLK_IN1_D_clk_p => CLK_IN1_D_clk_p,
      data_in(15 downto 0) => data_in(15 downto 0),
      filter_clk_domain_clk => filter_clk_domain_clk,
      impulse_out(15 downto 0) => impulse_out(15 downto 0),
      rect_out(15 downto 0) => rect_out(15 downto 0),
      reset_rtl => reset_rtl,
      shaper_out(15 downto 0) => shaper_out(15 downto 0)
    );
end STRUCTURE;
