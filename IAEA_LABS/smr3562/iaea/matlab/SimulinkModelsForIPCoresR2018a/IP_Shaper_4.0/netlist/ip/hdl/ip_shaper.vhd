-- Generated from Simulink block IP_Shaper/axi_clk_domain
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_axi_clk_domain is
  port (
    r1_b10 : in std_logic_vector( 32-1 downto 0 );
    r2_na : in std_logic_vector( 32-1 downto 0 );
    r3_nb : in std_logic_vector( 32-1 downto 0 );
    r4_na_inv : in std_logic_vector( 32-1 downto 0 );
    r5_b00 : in std_logic_vector( 32-1 downto 0 );
    r6_b20 : in std_logic_vector( 32-1 downto 0 )
  );
end ip_shaper_axi_clk_domain;
architecture structural of ip_shaper_axi_clk_domain is 
  signal r1_b10_net : std_logic_vector( 32-1 downto 0 );
  signal r4_na_inv_net : std_logic_vector( 32-1 downto 0 );
  signal r5_b00_net : std_logic_vector( 32-1 downto 0 );
  signal r6_b20_net : std_logic_vector( 32-1 downto 0 );
  signal r3_nb_net : std_logic_vector( 32-1 downto 0 );
  signal r2_na_net : std_logic_vector( 32-1 downto 0 );
begin
  r1_b10_net <= r1_b10;
  r2_na_net <= r2_na;
  r3_nb_net <= r3_nb;
  r4_na_inv_net <= r4_na_inv;
  r5_b00_net <= r5_b00;
  r6_b20_net <= r6_b20;
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/Normalization
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_normalization is
  port (
    in1 : in std_logic_vector( 26-1 downto 0 );
    nf : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 16-1 downto 0 )
  );
end ip_shaper_normalization;
architecture structural of ip_shaper_normalization is 
  signal convert_dout_net : std_logic_vector( 26-1 downto 0 );
  signal clk_net : std_logic;
  signal register3_q_net : std_logic_vector( 32-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 26-1 downto 0 );
  signal mult_p_net : std_logic_vector( 16-1 downto 0 );
  signal ce_net : std_logic;
  signal reinterpret2_output_port_net : std_logic_vector( 26-1 downto 0 );
begin
  out1 <= mult_p_net;
  convert_dout_net <= in1;
  register3_q_net <= nf;
  clk_net <= clk_1;
  ce_net <= ce_1;
  mult : entity xil_defaultlib.ip_shaper_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 14,
    a_width => 26,
    b_arith => xlUnsigned,
    b_bin_pt => 25,
    b_width => 26,
    c_a_type => 0,
    c_a_width => 26,
    c_b_type => 1,
    c_b_width => 26,
    c_baat => 26,
    c_output_width => 52,
    c_type => 0,
    core_name0 => "ip_shaper_mult_gen_v12_0_i0",
    extra_registers => 1,
    multsign => 2,
    overflow => 2,
    p_arith => xlSigned,
    p_bin_pt => 14,
    p_width => 16,
    quantization => 2
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => convert_dout_net,
    b => reinterpret2_output_port_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult_p_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_487e71aa5f 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  slice2 : entity xil_defaultlib.ip_shaper_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 25,
    x_width => 32,
    y_width => 26
  )
  port map (
    x => register3_q_net,
    y => slice2_y_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/SecondPoleCorrection/toUFix
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_toufix is
  port (
    in1 : in std_logic_vector( 32-1 downto 0 );
    out1 : out std_logic_vector( 32-1 downto 0 )
  );
end ip_shaper_toufix;
architecture structural of ip_shaper_toufix is 
  signal reinterpret3_output_port_net : std_logic_vector( 32-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 32-1 downto 0 );
  signal register4_q_net : std_logic_vector( 32-1 downto 0 );
begin
  out1 <= reinterpret3_output_port_net;
  register4_q_net <= in1;
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_1f5ee3cfa1 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  slice3 : entity xil_defaultlib.ip_shaper_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 31,
    x_width => 32,
    y_width => 32
  )
  port map (
    x => register4_q_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/SecondPoleCorrection/toUFix1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_toufix1 is
  port (
    in1 : in std_logic_vector( 32-1 downto 0 );
    out1 : out std_logic_vector( 24-1 downto 0 )
  );
end ip_shaper_toufix1;
architecture structural of ip_shaper_toufix1 is 
  signal reinterpret3_output_port_net : std_logic_vector( 24-1 downto 0 );
  signal register5_q_net : std_logic_vector( 32-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 24-1 downto 0 );
begin
  out1 <= reinterpret3_output_port_net;
  register5_q_net <= in1;
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a70faf2c7 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  slice3 : entity xil_defaultlib.ip_shaper_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 23,
    x_width => 32,
    y_width => 24
  )
  port map (
    x => register5_q_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/SecondPoleCorrection
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_secondpolecorrection is
  port (
    in1 : in std_logic_vector( 16-1 downto 0 );
    b00 : in std_logic_vector( 32-1 downto 0 );
    b20 : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 26-1 downto 0 )
  );
end ip_shaper_secondpolecorrection;
architecture structural of ip_shaper_secondpolecorrection is 
  signal ce_net : std_logic;
  signal multiplier1_p_net : std_logic_vector( 32-1 downto 0 );
  signal data_in_net : std_logic_vector( 16-1 downto 0 );
  signal register5_q_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 32-1 downto 0 );
  signal register4_q_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 24-1 downto 0 );
  signal multiplier2_p_net : std_logic_vector( 32-1 downto 0 );
  signal subtractor_s_net : std_logic_vector( 26-1 downto 0 );
  signal clk_net : std_logic;
  signal delay_q_net : std_logic_vector( 32-1 downto 0 );
begin
  out1 <= subtractor_s_net;
  data_in_net <= in1;
  register4_q_net <= b00;
  register5_q_net <= b20;
  clk_net <= clk_1;
  ce_net <= ce_1;
  toufix : entity xil_defaultlib.ip_shaper_toufix 
  port map (
    in1 => register4_q_net,
    out1 => reinterpret3_output_port_net_x0
  );
  toufix1 : entity xil_defaultlib.ip_shaper_toufix1 
  port map (
    in1 => register5_q_net,
    out1 => reinterpret3_output_port_net
  );
  delay : entity xil_defaultlib.ip_shaper_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 32
  )
  port map (
    en => '1',
    rst => '1',
    d => multiplier1_p_net,
    clk => clk_net,
    ce => ce_net,
    q => delay_q_net
  );
  multiplier1 : entity xil_defaultlib.ip_shaper_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 14,
    a_width => 16,
    b_arith => xlUnsigned,
    b_bin_pt => 24,
    b_width => 32,
    c_a_type => 0,
    c_a_width => 16,
    c_b_type => 1,
    c_b_width => 32,
    c_baat => 16,
    c_output_width => 48,
    c_type => 0,
    core_name0 => "ip_shaper_mult_gen_v12_0_i1",
    extra_registers => 1,
    multsign => 2,
    overflow => 2,
    p_arith => xlSigned,
    p_bin_pt => 24,
    p_width => 32,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => data_in_net,
    b => reinterpret3_output_port_net_x0,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => multiplier1_p_net
  );
  multiplier2 : entity xil_defaultlib.ip_shaper_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 24,
    a_width => 32,
    b_arith => xlUnsigned,
    b_bin_pt => 24,
    b_width => 24,
    c_a_type => 0,
    c_a_width => 32,
    c_b_type => 1,
    c_b_width => 24,
    c_baat => 32,
    c_output_width => 56,
    c_type => 0,
    core_name0 => "ip_shaper_mult_gen_v12_0_i2",
    extra_registers => 0,
    multsign => 2,
    overflow => 2,
    p_arith => xlSigned,
    p_bin_pt => 24,
    p_width => 32,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => delay_q_net,
    b => reinterpret3_output_port_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => multiplier2_p_net
  );
  subtractor : entity xil_defaultlib.ip_shaper_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 24,
    a_width => 32,
    b_arith => xlSigned,
    b_bin_pt => 24,
    b_width => 32,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 33,
    core_name0 => "ip_shaper_c_addsub_v12_0_i0",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 33,
    latency => 1,
    overflow => 2,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 24,
    s_width => 26
  )
  port map (
    clr => '0',
    en => "1",
    a => multiplier1_p_net,
    b => multiplier2_p_net,
    clk => clk_net,
    ce => ce_net,
    s => subtractor_s_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/movingAverage/delayLine
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_delayline_x0 is
  port (
    din : in std_logic_vector( 30-1 downto 0 );
    addr : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dout : out std_logic_vector( 30-1 downto 0 )
  );
end ip_shaper_delayline_x0;
architecture structural of ip_shaper_delayline_x0 is 
  signal single_port_ram_data_out_net : std_logic_vector( 30-1 downto 0 );
  signal accumulator_q_net : std_logic_vector( 30-1 downto 0 );
  signal counter_op_net : std_logic_vector( 10-1 downto 0 );
  signal relational_op_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 10-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 10-1 downto 0 );
  signal ce_net : std_logic;
  signal clk_net : std_logic;
  signal constant1_op_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal constant_op_net : std_logic_vector( 10-1 downto 0 );
begin
  dout <= single_port_ram_data_out_net;
  accumulator_q_net <= din;
  register1_q_net <= addr;
  clk_net <= clk_1;
  ce_net <= ce_1;
  constant_x0 : entity xil_defaultlib.sysgen_constant_acc20755fc 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_74030a8c3d 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  counter : entity xil_defaultlib.ip_shaper_xlcounter_free 
  generic map (
    core_name0 => "ip_shaper_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 10
  )
  port map (
    en => "1",
    rst => "0",
    clr => '0',
    load => relational_op_net,
    din => constant_op_net,
    clk => clk_net,
    ce => ce_net,
    op => counter_op_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_f56935bafc 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  relational : entity xil_defaultlib.sysgen_relational_ea8293cc57 
  port map (
    clr => '0',
    a => reinterpret3_output_port_net,
    b => counter_op_net,
    clk => clk_net,
    ce => ce_net,
    op => relational_op_net
  );
  single_port_ram : entity xil_defaultlib.ip_shaper_xlspram 
  generic map (
    c_address_width => 10,
    c_width => 30,
    core_name0 => "ip_shaper_blk_mem_gen_i0",
    latency => 1
  )
  port map (
    en => "1",
    rst => "0",
    addr => counter_op_net,
    data_in => accumulator_q_net,
    we => constant1_op_net,
    clk => clk_net,
    ce => ce_net,
    data_out => single_port_ram_data_out_net
  );
  slice3 : entity xil_defaultlib.ip_shaper_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 9,
    x_width => 32,
    y_width => 10
  )
  port map (
    x => register1_q_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/movingAverage
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_movingaverage is
  port (
    in1 : in std_logic_vector( 26-1 downto 0 );
    n : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 16-1 downto 0 )
  );
end ip_shaper_movingaverage;
architecture structural of ip_shaper_movingaverage is 
  signal single_port_ram_data_out_net : std_logic_vector( 30-1 downto 0 );
  signal sub_s_net : std_logic_vector( 26-1 downto 0 );
  signal accumulator_q_net : std_logic_vector( 30-1 downto 0 );
  signal ce_net : std_logic;
  signal convert_dout_net : std_logic_vector( 16-1 downto 0 );
  signal clk_net : std_logic;
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal sub_s_net_x0 : std_logic_vector( 30-1 downto 0 );
begin
  out1 <= convert_dout_net;
  sub_s_net <= in1;
  register1_q_net <= n;
  clk_net <= clk_1;
  ce_net <= ce_1;
  delayline : entity xil_defaultlib.ip_shaper_delayline_x0 
  port map (
    din => accumulator_q_net,
    addr => register1_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dout => single_port_ram_data_out_net
  );
  accumulator : entity xil_defaultlib.sysgen_accum_5b1ea6b148 
  port map (
    clr => '0',
    b => sub_s_net,
    clk => clk_net,
    ce => ce_net,
    q => accumulator_q_net
  );
  convert : entity xil_defaultlib.ip_shaper_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 24,
    din_width => 30,
    dout_arith => 2,
    dout_bin_pt => 14,
    dout_width => 16,
    latency => 1,
    overflow => xlSaturate,
    quantization => xlRound
  )
  port map (
    clr => '0',
    en => "1",
    din => sub_s_net_x0,
    clk => clk_net,
    ce => ce_net,
    dout => convert_dout_net
  );
  sub : entity xil_defaultlib.ip_shaper_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 24,
    a_width => 30,
    b_arith => xlSigned,
    b_bin_pt => 24,
    b_width => 30,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 31,
    core_name0 => "ip_shaper_c_addsub_v12_0_i1",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 31,
    latency => 1,
    overflow => 1,
    quantization => 2,
    s_arith => xlSigned,
    s_bin_pt => 24,
    s_width => 30
  )
  port map (
    clr => '0',
    en => "1",
    a => accumulator_q_net,
    b => single_port_ram_data_out_net,
    clk => clk_net,
    ce => ce_net,
    s => sub_s_net_x0
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/movingAverage1/delayLine
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_delayline is
  port (
    din : in std_logic_vector( 26-1 downto 0 );
    addr : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dout : out std_logic_vector( 26-1 downto 0 )
  );
end ip_shaper_delayline;
architecture structural of ip_shaper_delayline is 
  signal single_port_ram_data_out_net : std_logic_vector( 26-1 downto 0 );
  signal accumulator_q_net : std_logic_vector( 26-1 downto 0 );
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal ce_net : std_logic;
  signal clk_net : std_logic;
  signal relational_op_net : std_logic_vector( 1-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 10-1 downto 0 );
  signal counter_op_net : std_logic_vector( 10-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 10-1 downto 0 );
  signal constant_op_net : std_logic_vector( 10-1 downto 0 );
begin
  dout <= single_port_ram_data_out_net;
  accumulator_q_net <= din;
  register2_q_net <= addr;
  clk_net <= clk_1;
  ce_net <= ce_1;
  constant_x0 : entity xil_defaultlib.sysgen_constant_acc20755fc 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_74030a8c3d 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  counter : entity xil_defaultlib.ip_shaper_xlcounter_free 
  generic map (
    core_name0 => "ip_shaper_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 10
  )
  port map (
    en => "1",
    rst => "0",
    clr => '0',
    load => relational_op_net,
    din => constant_op_net,
    clk => clk_net,
    ce => ce_net,
    op => counter_op_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_f56935bafc 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  relational : entity xil_defaultlib.sysgen_relational_ea8293cc57 
  port map (
    clr => '0',
    a => reinterpret3_output_port_net,
    b => counter_op_net,
    clk => clk_net,
    ce => ce_net,
    op => relational_op_net
  );
  single_port_ram : entity xil_defaultlib.ip_shaper_xlspram 
  generic map (
    c_address_width => 10,
    c_width => 26,
    core_name0 => "ip_shaper_blk_mem_gen_i1",
    latency => 1
  )
  port map (
    en => "1",
    rst => "0",
    addr => counter_op_net,
    data_in => accumulator_q_net,
    we => constant1_op_net,
    clk => clk_net,
    ce => ce_net,
    data_out => single_port_ram_data_out_net
  );
  slice3 : entity xil_defaultlib.ip_shaper_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 9,
    x_width => 32,
    y_width => 10
  )
  port map (
    x => register2_q_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/movingAverage1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_movingaverage1 is
  port (
    in1 : in std_logic_vector( 16-1 downto 0 );
    n : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 26-1 downto 0 )
  );
end ip_shaper_movingaverage1;
architecture structural of ip_shaper_movingaverage1 is 
  signal convert_dout_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal sub_s_net : std_logic_vector( 26-1 downto 0 );
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal convert_dout_net : std_logic_vector( 26-1 downto 0 );
  signal ce_net : std_logic;
  signal clk_net : std_logic;
  signal single_port_ram_data_out_net : std_logic_vector( 26-1 downto 0 );
  signal accumulator_q_net : std_logic_vector( 26-1 downto 0 );
begin
  out1 <= convert_dout_net;
  convert_dout_net_x0 <= in1;
  register2_q_net <= n;
  clk_net <= clk_1;
  ce_net <= ce_1;
  delayline : entity xil_defaultlib.ip_shaper_delayline 
  port map (
    din => accumulator_q_net,
    addr => register2_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dout => single_port_ram_data_out_net
  );
  accumulator : entity xil_defaultlib.sysgen_accum_445d330a8c 
  port map (
    clr => '0',
    b => convert_dout_net_x0,
    clk => clk_net,
    ce => ce_net,
    q => accumulator_q_net
  );
  convert : entity xil_defaultlib.ip_shaper_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 14,
    din_width => 26,
    dout_arith => 2,
    dout_bin_pt => 14,
    dout_width => 26,
    latency => 1,
    overflow => xlSaturate,
    quantization => xlRound
  )
  port map (
    clr => '0',
    en => "1",
    din => sub_s_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert_dout_net
  );
  sub : entity xil_defaultlib.ip_shaper_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 14,
    a_width => 26,
    b_arith => xlSigned,
    b_bin_pt => 14,
    b_width => 26,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 27,
    core_name0 => "ip_shaper_c_addsub_v12_0_i2",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 27,
    latency => 1,
    overflow => 1,
    quantization => 2,
    s_arith => xlSigned,
    s_bin_pt => 14,
    s_width => 26
  )
  port map (
    clr => '0',
    en => "1",
    a => accumulator_q_net,
    b => single_port_ram_data_out_net,
    clk => clk_net,
    ce => ce_net,
    s => sub_s_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter/pulseUnfolder
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_pulseunfolder is
  port (
    in1 : in std_logic_vector( 26-1 downto 0 );
    b10 : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 26-1 downto 0 )
  );
end ip_shaper_pulseunfolder;
architecture structural of ip_shaper_pulseunfolder is 
  signal delay_q_net : std_logic_vector( 26-1 downto 0 );
  signal ce_net : std_logic;
  signal clk_net : std_logic;
  signal register_q_net : std_logic_vector( 32-1 downto 0 );
  signal subtractor_s_net : std_logic_vector( 26-1 downto 0 );
  signal sub_s_net : std_logic_vector( 26-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 24-1 downto 0 );
  signal mult_p_net : std_logic_vector( 26-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 24-1 downto 0 );
begin
  out1 <= sub_s_net;
  subtractor_s_net <= in1;
  register_q_net <= b10;
  clk_net <= clk_1;
  ce_net <= ce_1;
  delay : entity xil_defaultlib.ip_shaper_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 26
  )
  port map (
    en => '1',
    rst => '1',
    d => subtractor_s_net,
    clk => clk_net,
    ce => ce_net,
    q => delay_q_net
  );
  mult : entity xil_defaultlib.ip_shaper_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 24,
    a_width => 26,
    b_arith => xlUnsigned,
    b_bin_pt => 24,
    b_width => 24,
    c_a_type => 0,
    c_a_width => 26,
    c_b_type => 1,
    c_b_width => 24,
    c_baat => 26,
    c_output_width => 50,
    c_type => 0,
    core_name0 => "ip_shaper_mult_gen_v12_0_i3",
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 24,
    p_width => 26,
    quantization => 2
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => delay_q_net,
    b => reinterpret5_output_port_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult_p_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a70faf2c7 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  slice5 : entity xil_defaultlib.ip_shaper_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 23,
    x_width => 32,
    y_width => 24
  )
  port map (
    x => register_q_net,
    y => slice5_y_net
  );
  sub : entity xil_defaultlib.ip_shaper_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 24,
    a_width => 26,
    b_arith => xlSigned,
    b_bin_pt => 24,
    b_width => 26,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 27,
    core_name0 => "ip_shaper_c_addsub_v12_0_i2",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 27,
    latency => 1,
    overflow => 1,
    quantization => 2,
    s_arith => xlSigned,
    s_bin_pt => 24,
    s_width => 26
  )
  port map (
    clr => '0',
    en => "1",
    a => subtractor_s_net,
    b => mult_p_net,
    clk => clk_net,
    ce => ce_net,
    s => sub_s_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain/Filter
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_filter is
  port (
    in_x0 : in std_logic_vector( 16-1 downto 0 );
    b10 : in std_logic_vector( 32-1 downto 0 );
    na : in std_logic_vector( 32-1 downto 0 );
    nb : in std_logic_vector( 32-1 downto 0 );
    na_inv : in std_logic_vector( 32-1 downto 0 );
    b00 : in std_logic_vector( 32-1 downto 0 );
    b20 : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 16-1 downto 0 );
    impulse : out std_logic_vector( 26-1 downto 0 );
    rect : out std_logic_vector( 16-1 downto 0 )
  );
end ip_shaper_filter;
architecture structural of ip_shaper_filter is 
  signal register5_q_net : std_logic_vector( 32-1 downto 0 );
  signal data_in_net : std_logic_vector( 16-1 downto 0 );
  signal convert_dout_net : std_logic_vector( 26-1 downto 0 );
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal register4_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_q_net : std_logic_vector( 32-1 downto 0 );
  signal sub_s_net : std_logic_vector( 26-1 downto 0 );
  signal convert_dout_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal subtractor_s_net : std_logic_vector( 26-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal mult_p_net : std_logic_vector( 16-1 downto 0 );
  signal register3_q_net : std_logic_vector( 32-1 downto 0 );
begin
  out_x0 <= mult_p_net;
  impulse <= sub_s_net;
  rect <= convert_dout_net_x0;
  data_in_net <= in_x0;
  register_q_net <= b10;
  register1_q_net <= na;
  register2_q_net <= nb;
  register3_q_net <= na_inv;
  register4_q_net <= b00;
  register5_q_net <= b20;
  clk_net <= clk_1;
  ce_net <= ce_1;
  normalization : entity xil_defaultlib.ip_shaper_normalization 
  port map (
    in1 => convert_dout_net,
    nf => register3_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => mult_p_net
  );
  secondpolecorrection : entity xil_defaultlib.ip_shaper_secondpolecorrection 
  port map (
    in1 => data_in_net,
    b00 => register4_q_net,
    b20 => register5_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => subtractor_s_net
  );
  movingaverage : entity xil_defaultlib.ip_shaper_movingaverage 
  port map (
    in1 => sub_s_net,
    n => register1_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => convert_dout_net_x0
  );
  movingaverage1 : entity xil_defaultlib.ip_shaper_movingaverage1 
  port map (
    in1 => convert_dout_net_x0,
    n => register2_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => convert_dout_net
  );
  pulseunfolder : entity xil_defaultlib.ip_shaper_pulseunfolder 
  port map (
    in1 => subtractor_s_net,
    b10 => register_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => sub_s_net
  );
end structural;
-- Generated from Simulink block IP_Shaper/filter_clk_domain
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_filter_clk_domain is
  port (
    b10 : in std_logic_vector( 32-1 downto 0 );
    na : in std_logic_vector( 32-1 downto 0 );
    nb : in std_logic_vector( 32-1 downto 0 );
    na_inv : in std_logic_vector( 32-1 downto 0 );
    b00 : in std_logic_vector( 32-1 downto 0 );
    b20 : in std_logic_vector( 32-1 downto 0 );
    data_in : in std_logic_vector( 16-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    impulse_out : out std_logic_vector( 16-1 downto 0 );
    rect_out : out std_logic_vector( 16-1 downto 0 );
    shaper_out : out std_logic_vector( 16-1 downto 0 )
  );
end ip_shaper_filter_clk_domain;
architecture structural of ip_shaper_filter_clk_domain is 
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal register3_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_q_net : std_logic_vector( 32-1 downto 0 );
  signal register5_q_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal clk_net : std_logic;
  signal reinterpret1_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal convert_dout_net : std_logic_vector( 16-1 downto 0 );
  signal mult_p_net : std_logic_vector( 16-1 downto 0 );
  signal ce_net : std_logic;
  signal reinterpret3_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal data_in_net : std_logic_vector( 16-1 downto 0 );
  signal convert_dout_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal register4_q_net : std_logic_vector( 32-1 downto 0 );
  signal convert1_dout_net : std_logic_vector( 16-1 downto 0 );
  signal sub_s_net : std_logic_vector( 26-1 downto 0 );
begin
  register_q_net <= b10;
  register1_q_net <= na;
  register2_q_net <= nb;
  register3_q_net <= na_inv;
  register4_q_net <= b00;
  register5_q_net <= b20;
  data_in_net <= data_in;
  impulse_out <= reinterpret2_output_port_net;
  rect_out <= reinterpret3_output_port_net;
  shaper_out <= reinterpret1_output_port_net;
  clk_net <= clk_1;
  ce_net <= ce_1;
  filter : entity xil_defaultlib.ip_shaper_filter 
  port map (
    in_x0 => data_in_net,
    b10 => register_q_net,
    na => register1_q_net,
    nb => register2_q_net,
    na_inv => register3_q_net,
    b00 => register4_q_net,
    b20 => register5_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out_x0 => mult_p_net,
    impulse => sub_s_net,
    rect => convert_dout_net
  );
  convert : entity xil_defaultlib.ip_shaper_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 14,
    din_width => 16,
    dout_arith => 2,
    dout_bin_pt => 14,
    dout_width => 16,
    latency => 1,
    overflow => xlSaturate,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => convert_dout_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert_dout_net_x0
  );
  convert1 : entity xil_defaultlib.ip_shaper_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 24,
    din_width => 26,
    dout_arith => 2,
    dout_bin_pt => 14,
    dout_width => 16,
    latency => 1,
    overflow => xlSaturate,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => sub_s_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert1_dout_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_0065810125 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => mult_p_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_0065810125 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => convert1_dout_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_0065810125 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => convert_dout_net_x0,
    output_port => reinterpret3_output_port_net
  );
end structural;
-- Generated from Simulink block IP_Shaper_struct
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_struct is
  port (
    r1_b10 : in std_logic_vector( 32-1 downto 0 );
    r2_na : in std_logic_vector( 32-1 downto 0 );
    r3_nb : in std_logic_vector( 32-1 downto 0 );
    r4_na_inv : in std_logic_vector( 32-1 downto 0 );
    r5_b00 : in std_logic_vector( 32-1 downto 0 );
    r6_b20 : in std_logic_vector( 32-1 downto 0 );
    data_in : in std_logic_vector( 16-1 downto 0 );
    clk_1_x0 : in std_logic;
    ce_1_x0 : in std_logic;
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    impulse_out : out std_logic_vector( 16-1 downto 0 );
    rect_out : out std_logic_vector( 16-1 downto 0 );
    shaper_out : out std_logic_vector( 16-1 downto 0 )
  );
end ip_shaper_struct;
architecture structural of ip_shaper_struct is 
  signal r1_b10_net : std_logic_vector( 32-1 downto 0 );
  signal r2_na_net : std_logic_vector( 32-1 downto 0 );
  signal r4_na_inv_net : std_logic_vector( 32-1 downto 0 );
  signal r5_b00_net : std_logic_vector( 32-1 downto 0 );
  signal r3_nb_net : std_logic_vector( 32-1 downto 0 );
  signal r6_b20_net : std_logic_vector( 32-1 downto 0 );
  signal d_clk_net : std_logic;
  signal register4_q_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal q_ce_net : std_logic;
  signal reinterpret1_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal register_q_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal register3_q_net : std_logic_vector( 32-1 downto 0 );
  signal data_in_net : std_logic_vector( 16-1 downto 0 );
  signal register5_q_net : std_logic_vector( 32-1 downto 0 );
begin
  r1_b10_net <= r1_b10;
  r2_na_net <= r2_na;
  r3_nb_net <= r3_nb;
  r4_na_inv_net <= r4_na_inv;
  r5_b00_net <= r5_b00;
  r6_b20_net <= r6_b20;
  data_in_net <= data_in;
  impulse_out <= reinterpret2_output_port_net;
  rect_out <= reinterpret3_output_port_net;
  shaper_out <= reinterpret1_output_port_net;
  d_clk_net <= clk_1_x0;
  q_ce_net <= ce_1_x0;
  clk_net <= clk_1;
  ce_net <= ce_1;
  axi_clk_domain : entity xil_defaultlib.ip_shaper_axi_clk_domain 
  port map (
    r1_b10 => r1_b10_net,
    r2_na => r2_na_net,
    r3_nb => r3_nb_net,
    r4_na_inv => r4_na_inv_net,
    r5_b00 => r5_b00_net,
    r6_b20 => r6_b20_net
  );
  filter_clk_domain : entity xil_defaultlib.ip_shaper_filter_clk_domain 
  port map (
    b10 => register_q_net,
    na => register1_q_net,
    nb => register2_q_net,
    na_inv => register3_q_net,
    b00 => register4_q_net,
    b20 => register5_q_net,
    data_in => data_in_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    impulse_out => reinterpret2_output_port_net,
    rect_out => reinterpret3_output_port_net,
    shaper_out => reinterpret1_output_port_net
  );
  register_x0 : entity xil_defaultlib.ip_shaper_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r1_b10_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register_q_net
  );
  register1 : entity xil_defaultlib.ip_shaper_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r2_na_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.ip_shaper_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r3_nb_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register2_q_net
  );
  register3 : entity xil_defaultlib.ip_shaper_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r4_na_inv_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register3_q_net
  );
  register4 : entity xil_defaultlib.ip_shaper_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r5_b00_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register4_q_net
  );
  register5 : entity xil_defaultlib.ip_shaper_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r6_b20_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register5_q_net
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper_default_clock_driver is
  port (
    axi_clk_domain_sysclk : in std_logic;
    axi_clk_domain_sysce : in std_logic;
    axi_clk_domain_sysclr : in std_logic;
    filter_clk_domain_sysclk : in std_logic;
    filter_clk_domain_sysce : in std_logic;
    filter_clk_domain_sysclr : in std_logic;
    axi_clk_domain_clk1 : out std_logic;
    axi_clk_domain_ce1 : out std_logic;
    filter_clk_domain_clk1 : out std_logic;
    filter_clk_domain_ce1 : out std_logic
  );
end ip_shaper_default_clock_driver;
architecture structural of ip_shaper_default_clock_driver is 
begin
  clockdriver_x0 : entity xil_defaultlib.xlclockdriver 
  generic map (
    period => 1,
    log_2_period => 1
  )
  port map (
    sysclk => axi_clk_domain_sysclk,
    sysce => axi_clk_domain_sysce,
    sysclr => axi_clk_domain_sysclr,
    clk => axi_clk_domain_clk1,
    ce => axi_clk_domain_ce1
  );
  clockdriver : entity xil_defaultlib.xlclockdriver 
  generic map (
    period => 1,
    log_2_period => 1
  )
  port map (
    sysclk => filter_clk_domain_sysclk,
    sysce => filter_clk_domain_sysce,
    sysclr => filter_clk_domain_sysclr,
    clk => filter_clk_domain_clk1,
    ce => filter_clk_domain_ce1
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_shaper is
  port (
    data_in : in std_logic_vector( 16-1 downto 0 );
    axi_clk_domain_clk : in std_logic;
    filter_clk_domain_clk : in std_logic;
    axi_clk_domain_aresetn : in std_logic;
    axi_clk_domain_s_axi_awaddr : in std_logic_vector( 5-1 downto 0 );
    axi_clk_domain_s_axi_awvalid : in std_logic;
    axi_clk_domain_s_axi_wdata : in std_logic_vector( 32-1 downto 0 );
    axi_clk_domain_s_axi_wstrb : in std_logic_vector( 4-1 downto 0 );
    axi_clk_domain_s_axi_wvalid : in std_logic;
    axi_clk_domain_s_axi_bready : in std_logic;
    axi_clk_domain_s_axi_araddr : in std_logic_vector( 5-1 downto 0 );
    axi_clk_domain_s_axi_arvalid : in std_logic;
    axi_clk_domain_s_axi_rready : in std_logic;
    impulse_out : out std_logic_vector( 16-1 downto 0 );
    rect_out : out std_logic_vector( 16-1 downto 0 );
    shaper_out : out std_logic_vector( 16-1 downto 0 );
    axi_clk_domain_s_axi_awready : out std_logic;
    axi_clk_domain_s_axi_wready : out std_logic;
    axi_clk_domain_s_axi_bresp : out std_logic_vector( 2-1 downto 0 );
    axi_clk_domain_s_axi_bvalid : out std_logic;
    axi_clk_domain_s_axi_arready : out std_logic;
    axi_clk_domain_s_axi_rdata : out std_logic_vector( 32-1 downto 0 );
    axi_clk_domain_s_axi_rresp : out std_logic_vector( 2-1 downto 0 );
    axi_clk_domain_s_axi_rvalid : out std_logic
  );
end ip_shaper;
architecture structural of ip_shaper is 
  attribute core_generation_info : string;
  attribute core_generation_info of structural : architecture is "ip_shaper,sysgen_core_2018_3,{,compilation=IP Catalog,block_icon_display=Default,family=artix7,part=xc7a35t,speed=-1,package=cpg236,synthesis_language=vhdl,hdl_library=xil_defaultlib,synthesis_strategy=Vivado Synthesis Defaults,implementation_strategy=Vivado Implementation Defaults,testbench=0,interface_doc=1,ce_clr=0,clock_period=-10,system_simulink_period=-1,waveform_viewer=0,axilite_interface=1,ip_catalog_plugin=0,hwcosim_burst_mode=0,simulation_time=0.0004,accum=2,addsub=4,constant=4,convert=4,counter=2,delay=2,mult=4,register=6,reinterpret=9,relational=2,slice=6,spram=2,}";
  signal axi_clk_domain_clk_net : std_logic;
  signal r1_b10 : std_logic_vector( 32-1 downto 0 );
  signal r2_na : std_logic_vector( 32-1 downto 0 );
  signal r4_na_inv : std_logic_vector( 32-1 downto 0 );
  signal ce_1_net_x0 : std_logic;
  signal r5_b00 : std_logic_vector( 32-1 downto 0 );
  signal clk_1_net : std_logic;
  signal ce_1_net : std_logic;
  signal r6_b20 : std_logic_vector( 32-1 downto 0 );
  signal r3_nb : std_logic_vector( 32-1 downto 0 );
  signal clk_1_net_x0 : std_logic;
begin
  axi_clk_domain_axi_lite_interface : entity xil_defaultlib.axi_clk_domain_axi_lite_interface 
  port map (
    axi_clk_domain_s_axi_awaddr => axi_clk_domain_s_axi_awaddr,
    axi_clk_domain_s_axi_awvalid => axi_clk_domain_s_axi_awvalid,
    axi_clk_domain_s_axi_wdata => axi_clk_domain_s_axi_wdata,
    axi_clk_domain_s_axi_wstrb => axi_clk_domain_s_axi_wstrb,
    axi_clk_domain_s_axi_wvalid => axi_clk_domain_s_axi_wvalid,
    axi_clk_domain_s_axi_bready => axi_clk_domain_s_axi_bready,
    axi_clk_domain_s_axi_araddr => axi_clk_domain_s_axi_araddr,
    axi_clk_domain_s_axi_arvalid => axi_clk_domain_s_axi_arvalid,
    axi_clk_domain_s_axi_rready => axi_clk_domain_s_axi_rready,
    axi_clk_domain_aresetn => axi_clk_domain_aresetn,
    axi_clk_domain_aclk => axi_clk_domain_clk,
    r6_b20 => r6_b20,
    r5_b00 => r5_b00,
    r4_na_inv => r4_na_inv,
    r3_nb => r3_nb,
    r2_na => r2_na,
    r1_b10 => r1_b10,
    axi_clk_domain_s_axi_awready => axi_clk_domain_s_axi_awready,
    axi_clk_domain_s_axi_wready => axi_clk_domain_s_axi_wready,
    axi_clk_domain_s_axi_bresp => axi_clk_domain_s_axi_bresp,
    axi_clk_domain_s_axi_bvalid => axi_clk_domain_s_axi_bvalid,
    axi_clk_domain_s_axi_arready => axi_clk_domain_s_axi_arready,
    axi_clk_domain_s_axi_rdata => axi_clk_domain_s_axi_rdata,
    axi_clk_domain_s_axi_rresp => axi_clk_domain_s_axi_rresp,
    axi_clk_domain_s_axi_rvalid => axi_clk_domain_s_axi_rvalid,
    axi_clk_domain_clk => axi_clk_domain_clk_net
  );
  ip_shaper_default_clock_driver : entity xil_defaultlib.ip_shaper_default_clock_driver 
  port map (
    axi_clk_domain_sysclk => axi_clk_domain_clk_net,
    axi_clk_domain_sysce => '1',
    axi_clk_domain_sysclr => '0',
    filter_clk_domain_sysclk => filter_clk_domain_clk,
    filter_clk_domain_sysce => '1',
    filter_clk_domain_sysclr => '0',
    axi_clk_domain_clk1 => clk_1_net,
    axi_clk_domain_ce1 => ce_1_net,
    filter_clk_domain_clk1 => clk_1_net_x0,
    filter_clk_domain_ce1 => ce_1_net_x0
  );
  ip_shaper_struct : entity xil_defaultlib.ip_shaper_struct 
  port map (
    r1_b10 => r1_b10,
    r2_na => r2_na,
    r3_nb => r3_nb,
    r4_na_inv => r4_na_inv,
    r5_b00 => r5_b00,
    r6_b20 => r6_b20,
    data_in => data_in,
    clk_1_x0 => clk_1_net,
    ce_1_x0 => ce_1_net,
    clk_1 => clk_1_net_x0,
    ce_1 => ce_1_net_x0,
    impulse_out => impulse_out,
    rect_out => rect_out,
    shaper_out => shaper_out
  );
end structural;
