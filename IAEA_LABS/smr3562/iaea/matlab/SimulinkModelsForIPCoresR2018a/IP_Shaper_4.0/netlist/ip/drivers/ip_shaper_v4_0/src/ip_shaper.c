#include "ip_shaper.h"
#ifndef __linux__
int ip_shaper_CfgInitialize(ip_shaper *InstancePtr, ip_shaper_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->ip_shaper_BaseAddress = ConfigPtr->ip_shaper_BaseAddress;

    InstancePtr->IsReady = 1;
    return XST_SUCCESS;
}
#endif
void ip_shaper_r6_b20_write(ip_shaper *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_shaper_WriteReg(InstancePtr->ip_shaper_BaseAddress, 0, Data);
}
u32 ip_shaper_r6_b20_read(ip_shaper *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_shaper_ReadReg(InstancePtr->ip_shaper_BaseAddress, 0);
    return Data;
}
void ip_shaper_r5_b00_write(ip_shaper *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_shaper_WriteReg(InstancePtr->ip_shaper_BaseAddress, 4, Data);
}
u32 ip_shaper_r5_b00_read(ip_shaper *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_shaper_ReadReg(InstancePtr->ip_shaper_BaseAddress, 4);
    return Data;
}
void ip_shaper_r4_na_inv_write(ip_shaper *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_shaper_WriteReg(InstancePtr->ip_shaper_BaseAddress, 8, Data);
}
u32 ip_shaper_r4_na_inv_read(ip_shaper *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_shaper_ReadReg(InstancePtr->ip_shaper_BaseAddress, 8);
    return Data;
}
void ip_shaper_r3_nb_write(ip_shaper *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_shaper_WriteReg(InstancePtr->ip_shaper_BaseAddress, 12, Data);
}
u32 ip_shaper_r3_nb_read(ip_shaper *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_shaper_ReadReg(InstancePtr->ip_shaper_BaseAddress, 12);
    return Data;
}
void ip_shaper_r2_na_write(ip_shaper *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_shaper_WriteReg(InstancePtr->ip_shaper_BaseAddress, 16, Data);
}
u32 ip_shaper_r2_na_read(ip_shaper *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_shaper_ReadReg(InstancePtr->ip_shaper_BaseAddress, 16);
    return Data;
}
void ip_shaper_r1_b10_write(ip_shaper *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_shaper_WriteReg(InstancePtr->ip_shaper_BaseAddress, 20, Data);
}
u32 ip_shaper_r1_b10_read(ip_shaper *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_shaper_ReadReg(InstancePtr->ip_shaper_BaseAddress, 20);
    return Data;
}
