# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator. 
#


namespace eval ::xilinx::dsp::planaheaddriver {
	set Compilation {IP Catalog}
	set CompilationFlow {IP}
	set CreateInterfaceDocument {on}
	set DSPDevice {xc7a35t}
	set DSPFamily {artix7}
	set DSPPackage {cpg236}
	set DSPSpeed {-1}
	set FPGAClockPeriod -10
	set GenerateTestBench 0
	set HDLLanguage {vhdl}
	set IPOOCCacheRootPath {C:/Users/Mladen/AppData/Local/Xilinx/Sysgen/SysgenVivado/win64.o/ip}
	set IP_Auto_Infer {1}
	set IP_Categories_Text {System Generator for DSP}
	set IP_Common_Repos {0}
	set IP_Description {}
	set IP_Dir {C:/Users/boss/Desktop/FPGA/iprepo/zynq}
	set IP_Library_Text {IP_Shaper}
	set IP_LifeCycle_Menu {1}
	set IP_Logo {sysgen_icon_100.png}
	set IP_Name {IP_Shaper}
	set IP_Revision {220554996}
	set IP_Socket_IP {0}
	set IP_Socket_IP_Proj_Path {}
	set IP_Vendor_Text {IAEA}
	set IP_Version_Text {4.0}
	set ImplStrategyName {Vivado Implementation Defaults}
	set PostProjectCreationProc {dsp_package_for_vivado_ip_integrator}
	set Project {ip_shaper}
	set ProjectFiles {
		{{conv_pkg.vhd} -lib {xil_defaultlib}}
		{{synth_reg.vhd} -lib {xil_defaultlib}}
		{{synth_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{srl17e.vhd} -lib {xil_defaultlib}}
		{{srl33e.vhd} -lib {xil_defaultlib}}
		{{synth_reg_reg.vhd} -lib {xil_defaultlib}}
		{{single_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{xlclockdriver_rd.vhd} -lib {xil_defaultlib}}
		{{vivado_ip.tcl}}
		{{ip_shaper_blk_mem_gen_i0_vivado.coe}}
		{{ip_shaper_blk_mem_gen_i1_vivado.coe}}
		{{axi_clk_domain_axi_lite_interface_verilog.v}}
		{{conv_pkg.v}}
		{{synth_reg.v}}
		{{synth_reg_w_init.v}}
		{{convert_type.v}}
		{{ip_shaper.mdd}}
		{{ip_shaper_hw.h}}
		{{ip_shaper.h}}
		{{ip_shaper_sinit.c}}
		{{ip_shaper.c}}
		{{ip_shaper_linux.c}}
		{{ip_shaper.mtcl}}
		{{Makefile.mak}}
		{{index.html}}
		{{ip_shaper_entity_declarations.vhd} -lib {xil_defaultlib}}
		{{ip_shaper.vhd} -lib {xil_defaultlib}}
		{{ip_shaper_clock.xdc}}
		{{ip_shaper.xdc}}
		{{C:/Users/Mladen/Dropbox/FPGA/SimulinkModelsForIPCores/IP_Shaper_4.0/netlist/sysgen/ip_shaper_false_paths.xdc}}
		{{ip_shaper.htm}}
	}
	set SimPeriod -1
	set SimTime 0.0004
	set SimulationTime {400220.00000000 ns}
	set SynthStrategyName {Vivado Synthesis Defaults}
	set SynthesisTool {Vivado}
	set TargetDir {C:/Users/Mladen/Dropbox/FPGA/SimulinkModelsForIPCores/IP_Shaper_4.0/netlist}
	set TopLevelModule {ip_shaper}
	set TopLevelSimulinkHandle 2.00037
	set VHDLLib {xil_defaultlib}
	set TopLevelPortInterface {}
	dict set TopLevelPortInterface data_in Name {data_in}
	dict set TopLevelPortInterface data_in Type Fix_16_14
	dict set TopLevelPortInterface data_in ArithmeticType xlSigned
	dict set TopLevelPortInterface data_in BinaryPoint 14
	dict set TopLevelPortInterface data_in Width 16
	dict set TopLevelPortInterface data_in DatFile {ip_shaper_filter_clk_domain_data_in.dat}
	dict set TopLevelPortInterface data_in IconText {data_in}
	dict set TopLevelPortInterface data_in Direction in
	dict set TopLevelPortInterface data_in Period 1
	dict set TopLevelPortInterface data_in Interface 0
	dict set TopLevelPortInterface data_in InterfaceName {}
	dict set TopLevelPortInterface data_in InterfaceString {DATA}
	dict set TopLevelPortInterface data_in ClockDomain {filter_clk_domain}
	dict set TopLevelPortInterface data_in Locs {}
	dict set TopLevelPortInterface data_in IOStandard {}
	dict set TopLevelPortInterface impulse_out Name {impulse_out}
	dict set TopLevelPortInterface impulse_out Type UFix_16_0
	dict set TopLevelPortInterface impulse_out ArithmeticType xlUnsigned
	dict set TopLevelPortInterface impulse_out BinaryPoint 0
	dict set TopLevelPortInterface impulse_out Width 16
	dict set TopLevelPortInterface impulse_out DatFile {ip_shaper_filter_clk_domain_impulse_out.dat}
	dict set TopLevelPortInterface impulse_out IconText {impulse_out}
	dict set TopLevelPortInterface impulse_out Direction out
	dict set TopLevelPortInterface impulse_out Period 1
	dict set TopLevelPortInterface impulse_out Interface 0
	dict set TopLevelPortInterface impulse_out InterfaceName {}
	dict set TopLevelPortInterface impulse_out InterfaceString {DATA}
	dict set TopLevelPortInterface impulse_out ClockDomain {filter_clk_domain}
	dict set TopLevelPortInterface impulse_out Locs {}
	dict set TopLevelPortInterface impulse_out IOStandard {}
	dict set TopLevelPortInterface rect_out Name {rect_out}
	dict set TopLevelPortInterface rect_out Type UFix_16_0
	dict set TopLevelPortInterface rect_out ArithmeticType xlUnsigned
	dict set TopLevelPortInterface rect_out BinaryPoint 0
	dict set TopLevelPortInterface rect_out Width 16
	dict set TopLevelPortInterface rect_out DatFile {ip_shaper_filter_clk_domain_rect_out.dat}
	dict set TopLevelPortInterface rect_out IconText {rect_out}
	dict set TopLevelPortInterface rect_out Direction out
	dict set TopLevelPortInterface rect_out Period 1
	dict set TopLevelPortInterface rect_out Interface 0
	dict set TopLevelPortInterface rect_out InterfaceName {}
	dict set TopLevelPortInterface rect_out InterfaceString {DATA}
	dict set TopLevelPortInterface rect_out ClockDomain {filter_clk_domain}
	dict set TopLevelPortInterface rect_out Locs {}
	dict set TopLevelPortInterface rect_out IOStandard {}
	dict set TopLevelPortInterface shaper_out Name {shaper_out}
	dict set TopLevelPortInterface shaper_out Type UFix_16_0
	dict set TopLevelPortInterface shaper_out ArithmeticType xlUnsigned
	dict set TopLevelPortInterface shaper_out BinaryPoint 0
	dict set TopLevelPortInterface shaper_out Width 16
	dict set TopLevelPortInterface shaper_out DatFile {ip_shaper_filter_clk_domain_shaper_out.dat}
	dict set TopLevelPortInterface shaper_out IconText {shaper_out}
	dict set TopLevelPortInterface shaper_out Direction out
	dict set TopLevelPortInterface shaper_out Period 1
	dict set TopLevelPortInterface shaper_out Interface 0
	dict set TopLevelPortInterface shaper_out InterfaceName {}
	dict set TopLevelPortInterface shaper_out InterfaceString {DATA}
	dict set TopLevelPortInterface shaper_out ClockDomain {filter_clk_domain}
	dict set TopLevelPortInterface shaper_out Locs {}
	dict set TopLevelPortInterface shaper_out IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_aresetn Name {axi_clk_domain_aresetn}
	dict set TopLevelPortInterface axi_clk_domain_aresetn Type -
	dict set TopLevelPortInterface axi_clk_domain_aresetn ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_aresetn BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_aresetn Width 1
	dict set TopLevelPortInterface axi_clk_domain_aresetn DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_aresetn IconText {axi_clk_domain_aresetn}
	dict set TopLevelPortInterface axi_clk_domain_aresetn Direction in
	dict set TopLevelPortInterface axi_clk_domain_aresetn Period 1
	dict set TopLevelPortInterface axi_clk_domain_aresetn Interface 8
	dict set TopLevelPortInterface axi_clk_domain_aresetn InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_aresetn InterfaceString {ARESETN}
	dict set TopLevelPortInterface axi_clk_domain_aresetn ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_aresetn Locs {}
	dict set TopLevelPortInterface axi_clk_domain_aresetn IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr Name {axi_clk_domain_s_axi_awaddr}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr Width 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr IconText {axi_clk_domain_s_axi_awaddr}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awaddr IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid Name {axi_clk_domain_s_axi_awvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid IconText {axi_clk_domain_s_axi_awvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awvalid IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready Name {axi_clk_domain_s_axi_awready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready IconText {axi_clk_domain_s_axi_awready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready Direction out
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_awready IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata Name {axi_clk_domain_s_axi_wdata}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata Width 32
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata IconText {axi_clk_domain_s_axi_wdata}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wdata IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb Name {axi_clk_domain_s_axi_wstrb}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb Width 4
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb IconText {axi_clk_domain_s_axi_wstrb}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wstrb IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid Name {axi_clk_domain_s_axi_wvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid IconText {axi_clk_domain_s_axi_wvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wvalid IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready Name {axi_clk_domain_s_axi_wready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready IconText {axi_clk_domain_s_axi_wready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready Direction out
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_wready IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp Name {axi_clk_domain_s_axi_bresp}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp Width 2
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp IconText {axi_clk_domain_s_axi_bresp}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp Direction out
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bresp IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid Name {axi_clk_domain_s_axi_bvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid IconText {axi_clk_domain_s_axi_bvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid Direction out
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bvalid IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready Name {axi_clk_domain_s_axi_bready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready IconText {axi_clk_domain_s_axi_bready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_bready IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr Name {axi_clk_domain_s_axi_araddr}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr Width 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr IconText {axi_clk_domain_s_axi_araddr}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_araddr IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid Name {axi_clk_domain_s_axi_arvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid IconText {axi_clk_domain_s_axi_arvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arvalid IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready Name {axi_clk_domain_s_axi_arready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready IconText {axi_clk_domain_s_axi_arready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready Direction out
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_arready IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata Name {axi_clk_domain_s_axi_rdata}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata Width 32
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata IconText {axi_clk_domain_s_axi_rdata}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata Direction out
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rdata IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp Name {axi_clk_domain_s_axi_rresp}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp Width 2
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp IconText {axi_clk_domain_s_axi_rresp}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp Direction out
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rresp IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid Name {axi_clk_domain_s_axi_rvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid IconText {axi_clk_domain_s_axi_rvalid}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid Direction out
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rvalid IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready Name {axi_clk_domain_s_axi_rready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready Type -
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready Width 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready IconText {axi_clk_domain_s_axi_rready}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready Direction in
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready Period 1
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready Interface 5
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready ClockDomain {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready Locs {}
	dict set TopLevelPortInterface axi_clk_domain_s_axi_rready IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_clk Name {axi_clk_domain_clk}
	dict set TopLevelPortInterface axi_clk_domain_clk Type -
	dict set TopLevelPortInterface axi_clk_domain_clk ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axi_clk_domain_clk BinaryPoint 0
	dict set TopLevelPortInterface axi_clk_domain_clk Width 1
	dict set TopLevelPortInterface axi_clk_domain_clk DatFile {}
	dict set TopLevelPortInterface axi_clk_domain_clk Direction in
	dict set TopLevelPortInterface axi_clk_domain_clk Period 1
	dict set TopLevelPortInterface axi_clk_domain_clk Interface 6
	dict set TopLevelPortInterface axi_clk_domain_clk InterfaceName {}
	dict set TopLevelPortInterface axi_clk_domain_clk InterfaceString {CLOCK}
	dict set TopLevelPortInterface axi_clk_domain_clk ClockDomain {axi_clk_domain}
	dict set TopLevelPortInterface axi_clk_domain_clk Locs {}
	dict set TopLevelPortInterface axi_clk_domain_clk IOStandard {}
	dict set TopLevelPortInterface axi_clk_domain_clk AssociatedInterfaces {axi_clk_domain_s_axi }
	dict set TopLevelPortInterface axi_clk_domain_clk AssociatedResets {axi_clk_domain_aresetn }
	dict set TopLevelPortInterface filter_clk_domain_clk Name {filter_clk_domain_clk}
	dict set TopLevelPortInterface filter_clk_domain_clk Type -
	dict set TopLevelPortInterface filter_clk_domain_clk ArithmeticType xlUnsigned
	dict set TopLevelPortInterface filter_clk_domain_clk BinaryPoint 0
	dict set TopLevelPortInterface filter_clk_domain_clk Width 1
	dict set TopLevelPortInterface filter_clk_domain_clk DatFile {}
	dict set TopLevelPortInterface filter_clk_domain_clk Direction in
	dict set TopLevelPortInterface filter_clk_domain_clk Period 1
	dict set TopLevelPortInterface filter_clk_domain_clk Interface 6
	dict set TopLevelPortInterface filter_clk_domain_clk InterfaceName {}
	dict set TopLevelPortInterface filter_clk_domain_clk InterfaceString {CLOCK}
	dict set TopLevelPortInterface filter_clk_domain_clk ClockDomain {filter_clk_domain}
	dict set TopLevelPortInterface filter_clk_domain_clk Locs {}
	dict set TopLevelPortInterface filter_clk_domain_clk IOStandard {}
	dict set TopLevelPortInterface filter_clk_domain_clk AssociatedInterfaces {}
	dict set TopLevelPortInterface filter_clk_domain_clk AssociatedResets {}
	set MemoryMappedPort {}
	dict set MemoryMappedPort r6_b20 Name {r6_b20}
	dict set MemoryMappedPort r6_b20 Type UFix_32_0
	dict set MemoryMappedPort r6_b20 ArithmeticType xlUnsigned
	dict set MemoryMappedPort r6_b20 BinaryPoint 0
	dict set MemoryMappedPort r6_b20 Width 32
	dict set MemoryMappedPort r6_b20 DatFile {ip_shaper_axi_clk_domain_r6_b20.dat}
	dict set MemoryMappedPort r6_b20 AddressOffset 0
	dict set MemoryMappedPort r6_b20 IconText {r6_b20}
	dict set MemoryMappedPort r6_b20 Direction in
	dict set MemoryMappedPort r6_b20 Period 1
	dict set MemoryMappedPort r6_b20 Interface 2
	dict set MemoryMappedPort r6_b20 InterfaceName {}
	dict set MemoryMappedPort r6_b20 InterfaceString {CONTROL}
	dict set MemoryMappedPort r6_b20 ClockDomain {axi_clk_domain}
	dict set MemoryMappedPort r6_b20 Locs {}
	dict set MemoryMappedPort r6_b20 IOStandard {}
	dict set MemoryMappedPort r5_b00 Name {r5_b00}
	dict set MemoryMappedPort r5_b00 Type UFix_32_0
	dict set MemoryMappedPort r5_b00 ArithmeticType xlUnsigned
	dict set MemoryMappedPort r5_b00 BinaryPoint 0
	dict set MemoryMappedPort r5_b00 Width 32
	dict set MemoryMappedPort r5_b00 DatFile {ip_shaper_axi_clk_domain_r5_b00.dat}
	dict set MemoryMappedPort r5_b00 AddressOffset 4
	dict set MemoryMappedPort r5_b00 IconText {r5_b00}
	dict set MemoryMappedPort r5_b00 Direction in
	dict set MemoryMappedPort r5_b00 Period 1
	dict set MemoryMappedPort r5_b00 Interface 2
	dict set MemoryMappedPort r5_b00 InterfaceName {}
	dict set MemoryMappedPort r5_b00 InterfaceString {CONTROL}
	dict set MemoryMappedPort r5_b00 ClockDomain {axi_clk_domain}
	dict set MemoryMappedPort r5_b00 Locs {}
	dict set MemoryMappedPort r5_b00 IOStandard {}
	dict set MemoryMappedPort r4_na_inv Name {r4_na_inv}
	dict set MemoryMappedPort r4_na_inv Type UFix_32_0
	dict set MemoryMappedPort r4_na_inv ArithmeticType xlUnsigned
	dict set MemoryMappedPort r4_na_inv BinaryPoint 0
	dict set MemoryMappedPort r4_na_inv Width 32
	dict set MemoryMappedPort r4_na_inv DatFile {ip_shaper_axi_clk_domain_r4_na_inv.dat}
	dict set MemoryMappedPort r4_na_inv AddressOffset 8
	dict set MemoryMappedPort r4_na_inv IconText {r4_na_inv}
	dict set MemoryMappedPort r4_na_inv Direction in
	dict set MemoryMappedPort r4_na_inv Period 1
	dict set MemoryMappedPort r4_na_inv Interface 2
	dict set MemoryMappedPort r4_na_inv InterfaceName {}
	dict set MemoryMappedPort r4_na_inv InterfaceString {CONTROL}
	dict set MemoryMappedPort r4_na_inv ClockDomain {axi_clk_domain}
	dict set MemoryMappedPort r4_na_inv Locs {}
	dict set MemoryMappedPort r4_na_inv IOStandard {}
	dict set MemoryMappedPort r3_nb Name {r3_nb}
	dict set MemoryMappedPort r3_nb Type UFix_32_0
	dict set MemoryMappedPort r3_nb ArithmeticType xlUnsigned
	dict set MemoryMappedPort r3_nb BinaryPoint 0
	dict set MemoryMappedPort r3_nb Width 32
	dict set MemoryMappedPort r3_nb DatFile {ip_shaper_axi_clk_domain_r3_nb.dat}
	dict set MemoryMappedPort r3_nb AddressOffset 12
	dict set MemoryMappedPort r3_nb IconText {r3_nb}
	dict set MemoryMappedPort r3_nb Direction in
	dict set MemoryMappedPort r3_nb Period 1
	dict set MemoryMappedPort r3_nb Interface 2
	dict set MemoryMappedPort r3_nb InterfaceName {}
	dict set MemoryMappedPort r3_nb InterfaceString {CONTROL}
	dict set MemoryMappedPort r3_nb ClockDomain {axi_clk_domain}
	dict set MemoryMappedPort r3_nb Locs {}
	dict set MemoryMappedPort r3_nb IOStandard {}
	dict set MemoryMappedPort r2_na Name {r2_na}
	dict set MemoryMappedPort r2_na Type UFix_32_0
	dict set MemoryMappedPort r2_na ArithmeticType xlUnsigned
	dict set MemoryMappedPort r2_na BinaryPoint 0
	dict set MemoryMappedPort r2_na Width 32
	dict set MemoryMappedPort r2_na DatFile {ip_shaper_axi_clk_domain_r2_na.dat}
	dict set MemoryMappedPort r2_na AddressOffset 16
	dict set MemoryMappedPort r2_na IconText {r2_na}
	dict set MemoryMappedPort r2_na Direction in
	dict set MemoryMappedPort r2_na Period 1
	dict set MemoryMappedPort r2_na Interface 2
	dict set MemoryMappedPort r2_na InterfaceName {}
	dict set MemoryMappedPort r2_na InterfaceString {CONTROL}
	dict set MemoryMappedPort r2_na ClockDomain {axi_clk_domain}
	dict set MemoryMappedPort r2_na Locs {}
	dict set MemoryMappedPort r2_na IOStandard {}
	dict set MemoryMappedPort r1_b10 Name {r1_b10}
	dict set MemoryMappedPort r1_b10 Type UFix_32_0
	dict set MemoryMappedPort r1_b10 ArithmeticType xlUnsigned
	dict set MemoryMappedPort r1_b10 BinaryPoint 0
	dict set MemoryMappedPort r1_b10 Width 32
	dict set MemoryMappedPort r1_b10 DatFile {ip_shaper_axi_clk_domain_r1_b10.dat}
	dict set MemoryMappedPort r1_b10 AddressOffset 20
	dict set MemoryMappedPort r1_b10 IconText {r1_b10}
	dict set MemoryMappedPort r1_b10 Direction in
	dict set MemoryMappedPort r1_b10 Period 1
	dict set MemoryMappedPort r1_b10 Interface 2
	dict set MemoryMappedPort r1_b10 InterfaceName {}
	dict set MemoryMappedPort r1_b10 InterfaceString {CONTROL}
	dict set MemoryMappedPort r1_b10 ClockDomain {axi_clk_domain}
	dict set MemoryMappedPort r1_b10 Locs {}
	dict set MemoryMappedPort r1_b10 IOStandard {}
}

source SgPaProject.tcl
::xilinx::dsp::planaheadworker::dsp_create_project