-- Generated from Simulink block IP_PeakDetector/axiBusDomain
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_axibusdomain is
  port (
    r4_x_delay : in std_logic_vector( 32-1 downto 0 );
    r5_x_noise : in std_logic_vector( 32-1 downto 0 );
    r6_x_min : in std_logic_vector( 32-1 downto 0 );
    r7_x_max : in std_logic_vector( 32-1 downto 0 );
    r8_en_pkd : in std_logic_vector( 32-1 downto 0 )
  );
end ip_peakdetector_axibusdomain;
architecture structural of ip_peakdetector_axibusdomain is 
  signal r8_en_pkd_net : std_logic_vector( 32-1 downto 0 );
  signal r4_x_delay_net : std_logic_vector( 32-1 downto 0 );
  signal r7_x_max_net : std_logic_vector( 32-1 downto 0 );
  signal r6_x_min_net : std_logic_vector( 32-1 downto 0 );
  signal r5_x_noise_net : std_logic_vector( 32-1 downto 0 );
begin
  r4_x_delay_net <= r4_x_delay;
  r5_x_noise_net <= r5_x_noise;
  r6_x_min_net <= r6_x_min;
  r7_x_max_net <= r7_x_max;
  r8_en_pkd_net <= r8_en_pkd;
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/format_dac
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_format_dac is
  port (
    in1 : in std_logic_vector( 16-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 14-1 downto 0 )
  );
end ip_peakdetector_format_dac;
architecture structural of ip_peakdetector_format_dac is 
  signal constant7_op_net : std_logic_vector( 14-1 downto 0 );
  signal logical_y_net : std_logic_vector( 14-1 downto 0 );
  signal mcode1_xmax_net : std_logic_vector( 16-1 downto 0 );
  signal convert4_dout_net : std_logic_vector( 14-1 downto 0 );
  signal ce_net : std_logic;
  signal clk_net : std_logic;
  signal reinterpret_output_port_net : std_logic_vector( 14-1 downto 0 );
begin
  out1 <= logical_y_net;
  mcode1_xmax_net <= in1;
  clk_net <= clk_1;
  ce_net <= ce_1;
  constant7 : entity xil_defaultlib.sysgen_constant_2ef5d3fc33 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
  convert4 : entity xil_defaultlib.ip_peakdetector_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 14,
    din_width => 16,
    dout_arith => 2,
    dout_bin_pt => 12,
    dout_width => 14,
    latency => 1,
    overflow => xlSaturate,
    quantization => xlRound
  )
  port map (
    clr => '0',
    en => "1",
    din => mcode1_xmax_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert4_dout_net
  );
  logical : entity xil_defaultlib.sysgen_logical_80d79f42b8 
  port map (
    clr => '0',
    d0 => reinterpret_output_port_net,
    d1 => constant7_op_net,
    clk => clk_net,
    ce => ce_net,
    y => logical_y_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_2a25fbe518 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => convert4_dout_net,
    output_port => reinterpret_output_port_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/delayLine
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_delayline is
  port (
    din : in std_logic_vector( 16-1 downto 0 );
    addr : in std_logic_vector( 10-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dout : out std_logic_vector( 16-1 downto 0 )
  );
end ip_peakdetector_delayline;
architecture structural of ip_peakdetector_delayline is 
  signal single_port_ram_data_out_net : std_logic_vector( 16-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 10-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal relational_op_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal constant_op_net : std_logic_vector( 10-1 downto 0 );
  signal reinterpret3_output_port_net_x1 : std_logic_vector( 10-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 10-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 1-1 downto 0 );
  signal counter_op_net : std_logic_vector( 10-1 downto 0 );
begin
  dout <= single_port_ram_data_out_net;
  reinterpret3_output_port_net_x0 <= din;
  reinterpret3_output_port_net <= addr;
  clk_net <= clk_1;
  ce_net <= ce_1;
  constant_x0 : entity xil_defaultlib.sysgen_constant_3018b0b739 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_389a487358 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  counter : entity xil_defaultlib.ip_peakdetector_xlcounter_free 
  generic map (
    core_name0 => "ip_peakdetector_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 10
  )
  port map (
    en => "1",
    rst => "0",
    clr => '0',
    load => relational_op_net,
    din => constant_op_net,
    clk => clk_net,
    ce => ce_net,
    op => counter_op_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_8089087059 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net_x1
  );
  relational : entity xil_defaultlib.sysgen_relational_3828661dd3 
  port map (
    clr => '0',
    a => reinterpret3_output_port_net_x1,
    b => counter_op_net,
    clk => clk_net,
    ce => ce_net,
    op => relational_op_net
  );
  single_port_ram : entity xil_defaultlib.ip_peakdetector_xlspram 
  generic map (
    c_address_width => 10,
    c_width => 16,
    core_name0 => "ip_peakdetector_blk_mem_gen_i0",
    latency => 1
  )
  port map (
    en => "1",
    rst => "0",
    addr => counter_op_net,
    data_in => reinterpret3_output_port_net_x0,
    we => constant1_op_net,
    clk => clk_net,
    ce => ce_net,
    data_out => single_port_ram_data_out_net
  );
  slice3 : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 9,
    x_width => 10,
    y_width => 10
  )
  port map (
    x => reinterpret3_output_port_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/math
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_math is
  port (
    x : in std_logic_vector( 16-1 downto 0 );
    xnoise : in std_logic_vector( 16-1 downto 0 );
    xdelay : in std_logic_vector( 16-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    x_m_xnoise : out std_logic_vector( 16-1 downto 0 );
    xdelay_m_xnoise : out std_logic_vector( 16-1 downto 0 )
  );
end ip_peakdetector_math;
architecture structural of ip_peakdetector_math is 
  signal single_port_ram_data_out_net : std_logic_vector( 16-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 16-1 downto 0 );
  signal ce_net : std_logic;
  signal reinterpret3_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal clk_net : std_logic;
  signal addsub_s_net : std_logic_vector( 16-1 downto 0 );
begin
  x_m_xnoise <= addsub_s_net;
  xdelay_m_xnoise <= addsub1_s_net;
  reinterpret3_output_port_net <= x;
  reinterpret3_output_port_net_x0 <= xnoise;
  single_port_ram_data_out_net <= xdelay;
  clk_net <= clk_1;
  ce_net <= ce_1;
  addsub : entity xil_defaultlib.ip_peakdetector_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 14,
    a_width => 16,
    b_arith => xlSigned,
    b_bin_pt => 14,
    b_width => 16,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 17,
    core_name0 => "ip_peakdetector_c_addsub_v12_0_i0",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 17,
    latency => 1,
    overflow => 2,
    quantization => 2,
    s_arith => xlSigned,
    s_bin_pt => 14,
    s_width => 16
  )
  port map (
    clr => '0',
    en => "1",
    a => reinterpret3_output_port_net,
    b => reinterpret3_output_port_net_x0,
    clk => clk_net,
    ce => ce_net,
    s => addsub_s_net
  );
  addsub1 : entity xil_defaultlib.ip_peakdetector_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 14,
    a_width => 16,
    b_arith => xlSigned,
    b_bin_pt => 14,
    b_width => 16,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 17,
    core_name0 => "ip_peakdetector_c_addsub_v12_0_i0",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 17,
    latency => 1,
    overflow => 2,
    quantization => 2,
    s_arith => xlSigned,
    s_bin_pt => 14,
    s_width => 16
  )
  port map (
    clr => '0',
    en => "1",
    a => single_port_ram_data_out_net,
    b => reinterpret3_output_port_net_x0,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/risingEdgeTrigger
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_risingedgetrigger is
  port (
    x : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    re_trigg : out std_logic_vector( 1-1 downto 0 )
  );
end ip_peakdetector_risingedgetrigger;
architecture structural of ip_peakdetector_risingedgetrigger is 
  signal mcode_re_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal slice_y_net : std_logic_vector( 1-1 downto 0 );
  signal mcode_pkd_net : std_logic_vector( 1-1 downto 0 );
  signal ce_net : std_logic;
  signal mcode_y_net : std_logic_vector( 1-1 downto 0 );
begin
  re_trigg <= mcode_re_net;
  mcode_pkd_net <= x;
  clk_net <= clk_1;
  ce_net <= ce_1;
  mcode : entity xil_defaultlib.sysgen_mcode_block_0c4ff59dc3 
  port map (
    clr => '0',
    x => reinterpret_output_port_net,
    clk => clk_net,
    ce => ce_net,
    y => mcode_y_net,
    re => mcode_re_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_df705fa772 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice_y_net,
    output_port => reinterpret_output_port_net
  );
  slice : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 0,
    x_width => 1,
    y_width => 1
  )
  port map (
    x => mcode_pkd_net,
    y => slice_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/toBool
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_tobool_x0 is
  port (
    in1 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 1-1 downto 0 )
  );
end ip_peakdetector_tobool_x0;
architecture structural of ip_peakdetector_tobool_x0 is 
  signal convert_dout_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal convert_dout_net : std_logic_vector( 1-1 downto 0 );
  signal ce_net : std_logic;
  signal slice_y_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
begin
  out1 <= convert_dout_net_x0;
  convert_dout_net <= in1;
  clk_net <= clk_1;
  ce_net <= ce_1;
  convert : entity xil_defaultlib.ip_peakdetector_xlconvert 
  generic map (
    bool_conversion => 1,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 1,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 1,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => slice_y_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert_dout_net_x0
  );
  slice : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 0,
    x_width => 1,
    y_width => 1
  )
  port map (
    x => convert_dout_net,
    y => slice_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/toSFIx
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_tosfix is
  port (
    in1 : in std_logic_vector( 32-1 downto 0 );
    out1 : out std_logic_vector( 16-1 downto 0 )
  );
end ip_peakdetector_tosfix;
architecture structural of ip_peakdetector_tosfix is 
  signal slice3_y_net : std_logic_vector( 16-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 16-1 downto 0 );
begin
  out1 <= reinterpret3_output_port_net;
  register1_q_net <= in1;
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_770a5d44ff 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  slice3 : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 15,
    x_width => 32,
    y_width => 16
  )
  port map (
    x => register1_q_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/toSFIx1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_tosfix1 is
  port (
    in1 : in std_logic_vector( 16-1 downto 0 );
    out1 : out std_logic_vector( 16-1 downto 0 )
  );
end ip_peakdetector_tosfix1;
architecture structural of ip_peakdetector_tosfix1 is 
  signal slice3_y_net : std_logic_vector( 16-1 downto 0 );
  signal x_net : std_logic_vector( 16-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 16-1 downto 0 );
begin
  out1 <= reinterpret3_output_port_net;
  x_net <= in1;
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_770a5d44ff 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  slice3 : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 15,
    x_width => 16,
    y_width => 16
  )
  port map (
    x => x_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/toSFIx2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_tosfix2 is
  port (
    in1 : in std_logic_vector( 32-1 downto 0 );
    out1 : out std_logic_vector( 16-1 downto 0 )
  );
end ip_peakdetector_tosfix2;
architecture structural of ip_peakdetector_tosfix2 is 
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 16-1 downto 0 );
begin
  out1 <= reinterpret3_output_port_net;
  register2_q_net <= in1;
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_770a5d44ff 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  slice3 : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 15,
    x_width => 32,
    y_width => 16
  )
  port map (
    x => register2_q_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/toSFIx3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_tosfix3 is
  port (
    in1 : in std_logic_vector( 32-1 downto 0 );
    out1 : out std_logic_vector( 16-1 downto 0 )
  );
end ip_peakdetector_tosfix3;
architecture structural of ip_peakdetector_tosfix3 is 
  signal reinterpret3_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 16-1 downto 0 );
  signal register3_q_net : std_logic_vector( 32-1 downto 0 );
begin
  out1 <= reinterpret3_output_port_net;
  register3_q_net <= in1;
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_770a5d44ff 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  slice3 : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 15,
    x_width => 32,
    y_width => 16
  )
  port map (
    x => register3_q_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector/toUFix
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_toufix is
  port (
    in1 : in std_logic_vector( 32-1 downto 0 );
    out1 : out std_logic_vector( 10-1 downto 0 )
  );
end ip_peakdetector_toufix;
architecture structural of ip_peakdetector_toufix is 
  signal reinterpret3_output_port_net : std_logic_vector( 10-1 downto 0 );
  signal register_q_net : std_logic_vector( 32-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 10-1 downto 0 );
begin
  out1 <= reinterpret3_output_port_net;
  register_q_net <= in1;
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_8089087059 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  slice3 : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 9,
    x_width => 32,
    y_width => 10
  )
  port map (
    x => register_q_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/peakDetector
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_peakdetector is
  port (
    x : in std_logic_vector( 16-1 downto 0 );
    x_delay : in std_logic_vector( 32-1 downto 0 );
    x_noise : in std_logic_vector( 32-1 downto 0 );
    x_min : in std_logic_vector( 32-1 downto 0 );
    x_max : in std_logic_vector( 32-1 downto 0 );
    en : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    peak : out std_logic_vector( 1-1 downto 0 );
    peak_amp : out std_logic_vector( 16-1 downto 0 )
  );
end ip_peakdetector_peakdetector;
architecture structural of ip_peakdetector_peakdetector is 
  signal mcode1_xmax_net : std_logic_vector( 16-1 downto 0 );
  signal reinterpret3_output_port_net_x3 : std_logic_vector( 10-1 downto 0 );
  signal addsub_s_net : std_logic_vector( 16-1 downto 0 );
  signal mcode_pkd_net : std_logic_vector( 1-1 downto 0 );
  signal x_net : std_logic_vector( 16-1 downto 0 );
  signal reinterpret3_output_port_net_x2 : std_logic_vector( 16-1 downto 0 );
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal register3_q_net : std_logic_vector( 32-1 downto 0 );
  signal convert_dout_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal mcode_cmp_en_net : std_logic_vector( 1-1 downto 0 );
  signal convert_dout_net : std_logic_vector( 1-1 downto 0 );
  signal mcode_cmp_rst_net : std_logic_vector( 1-1 downto 0 );
  signal mcode_re_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal register_q_net : std_logic_vector( 32-1 downto 0 );
  signal clk_net : std_logic;
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal ce_net : std_logic;
  signal reinterpret3_output_port_net_x1 : std_logic_vector( 16-1 downto 0 );
  signal single_port_ram_data_out_net : std_logic_vector( 16-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 16-1 downto 0 );
begin
  peak <= mcode_re_net;
  peak_amp <= mcode1_xmax_net;
  x_net <= x;
  register_q_net <= x_delay;
  register1_q_net <= x_noise;
  register2_q_net <= x_min;
  register3_q_net <= x_max;
  convert_dout_net_x0 <= en;
  clk_net <= clk_1;
  ce_net <= ce_1;
  delayline : entity xil_defaultlib.ip_peakdetector_delayline 
  port map (
    din => reinterpret3_output_port_net_x0,
    addr => reinterpret3_output_port_net_x3,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dout => single_port_ram_data_out_net
  );
  math : entity xil_defaultlib.ip_peakdetector_math 
  port map (
    x => reinterpret3_output_port_net_x0,
    xnoise => reinterpret3_output_port_net,
    xdelay => single_port_ram_data_out_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    x_m_xnoise => addsub_s_net,
    xdelay_m_xnoise => addsub1_s_net
  );
  risingedgetrigger : entity xil_defaultlib.ip_peakdetector_risingedgetrigger 
  port map (
    x => mcode_pkd_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    re_trigg => mcode_re_net
  );
  tobool : entity xil_defaultlib.ip_peakdetector_tobool_x0 
  port map (
    in1 => convert_dout_net_x0,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => convert_dout_net
  );
  tosfix : entity xil_defaultlib.ip_peakdetector_tosfix 
  port map (
    in1 => register1_q_net,
    out1 => reinterpret3_output_port_net
  );
  tosfix1 : entity xil_defaultlib.ip_peakdetector_tosfix1 
  port map (
    in1 => x_net,
    out1 => reinterpret3_output_port_net_x0
  );
  tosfix2 : entity xil_defaultlib.ip_peakdetector_tosfix2 
  port map (
    in1 => register2_q_net,
    out1 => reinterpret3_output_port_net_x1
  );
  tosfix3 : entity xil_defaultlib.ip_peakdetector_tosfix3 
  port map (
    in1 => register3_q_net,
    out1 => reinterpret3_output_port_net_x2
  );
  toufix : entity xil_defaultlib.ip_peakdetector_toufix 
  port map (
    in1 => register_q_net,
    out1 => reinterpret3_output_port_net_x3
  );
  mcode : entity xil_defaultlib.sysgen_mcode_block_a80b0cfdd5 
  port map (
    clr => '0',
    x => reinterpret3_output_port_net_x0,
    xdelay => single_port_ram_data_out_net,
    xnoise => reinterpret3_output_port_net,
    en => convert_dout_net,
    x_m_xnoise => addsub_s_net,
    xdelay_m_xnoise => addsub1_s_net,
    xmin => reinterpret3_output_port_net_x1,
    xmax => reinterpret3_output_port_net_x2,
    clk => clk_net,
    ce => ce_net,
    pkd => mcode_pkd_net,
    cmp_en => mcode_cmp_en_net,
    cmp_rst => mcode_cmp_rst_net
  );
  mcode1 : entity xil_defaultlib.sysgen_mcode_block_0c3d4c1851 
  port map (
    clr => '0',
    x => reinterpret3_output_port_net_x0,
    rst => mcode_cmp_rst_net,
    en => mcode_cmp_en_net,
    clk => clk_net,
    ce => ce_net,
    xmax => mcode1_xmax_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain/toBool
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_tobool is
  port (
    in1 : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 1-1 downto 0 )
  );
end ip_peakdetector_tobool;
architecture structural of ip_peakdetector_tobool is 
  signal ce_net : std_logic;
  signal clk_net : std_logic;
  signal slice_y_net : std_logic_vector( 1-1 downto 0 );
  signal convert_dout_net : std_logic_vector( 1-1 downto 0 );
  signal register4_q_net : std_logic_vector( 32-1 downto 0 );
begin
  out1 <= convert_dout_net;
  register4_q_net <= in1;
  clk_net <= clk_1;
  ce_net <= ce_1;
  convert : entity xil_defaultlib.ip_peakdetector_xlconvert 
  generic map (
    bool_conversion => 1,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 1,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 1,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => slice_y_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert_dout_net
  );
  slice : entity xil_defaultlib.ip_peakdetector_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 0,
    x_width => 32,
    y_width => 1
  )
  port map (
    x => register4_q_net,
    y => slice_y_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector/signalDomain
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_signaldomain is
  port (
    x_delay : in std_logic_vector( 32-1 downto 0 );
    x_noise : in std_logic_vector( 32-1 downto 0 );
    x_min : in std_logic_vector( 32-1 downto 0 );
    x_max : in std_logic_vector( 32-1 downto 0 );
    en_pkd : in std_logic_vector( 32-1 downto 0 );
    x : in std_logic_vector( 16-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    peak_amp : out std_logic_vector( 16-1 downto 0 );
    peak_amp_dac : out std_logic_vector( 14-1 downto 0 );
    peak_amp_rdy : out std_logic_vector( 1-1 downto 0 )
  );
end ip_peakdetector_signaldomain;
architecture structural of ip_peakdetector_signaldomain is 
  signal x_net : std_logic_vector( 16-1 downto 0 );
  signal convert_dout_net : std_logic_vector( 1-1 downto 0 );
  signal register4_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_q_net : std_logic_vector( 32-1 downto 0 );
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal register3_q_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal logical_y_net : std_logic_vector( 14-1 downto 0 );
  signal mcode_re_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal mcode1_xmax_net : std_logic_vector( 16-1 downto 0 );
begin
  register_q_net <= x_delay;
  register1_q_net <= x_noise;
  register2_q_net <= x_min;
  register3_q_net <= x_max;
  register4_q_net <= en_pkd;
  peak_amp <= reinterpret_output_port_net;
  peak_amp_dac <= logical_y_net;
  peak_amp_rdy <= mcode_re_net;
  x_net <= x;
  clk_net <= clk_1;
  ce_net <= ce_1;
  format_dac : entity xil_defaultlib.ip_peakdetector_format_dac 
  port map (
    in1 => mcode1_xmax_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => logical_y_net
  );
  peakdetector : entity xil_defaultlib.ip_peakdetector_peakdetector 
  port map (
    x => x_net,
    x_delay => register_q_net,
    x_noise => register1_q_net,
    x_min => register2_q_net,
    x_max => register3_q_net,
    en => convert_dout_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    peak => mcode_re_net,
    peak_amp => mcode1_xmax_net
  );
  tobool : entity xil_defaultlib.ip_peakdetector_tobool 
  port map (
    in1 => register4_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => convert_dout_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_254866499c 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => mcode1_xmax_net,
    output_port => reinterpret_output_port_net
  );
end structural;
-- Generated from Simulink block IP_PeakDetector_struct
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_struct is
  port (
    r4_x_delay : in std_logic_vector( 32-1 downto 0 );
    r5_x_noise : in std_logic_vector( 32-1 downto 0 );
    r6_x_min : in std_logic_vector( 32-1 downto 0 );
    r7_x_max : in std_logic_vector( 32-1 downto 0 );
    r8_en_pkd : in std_logic_vector( 32-1 downto 0 );
    x : in std_logic_vector( 16-1 downto 0 );
    clk_1_x0 : in std_logic;
    ce_1_x0 : in std_logic;
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    peak_amp : out std_logic_vector( 16-1 downto 0 );
    peak_amp_dac : out std_logic_vector( 14-1 downto 0 );
    peak_amp_rdy : out std_logic_vector( 1-1 downto 0 )
  );
end ip_peakdetector_struct;
architecture structural of ip_peakdetector_struct is 
  signal r5_x_noise_net : std_logic_vector( 32-1 downto 0 );
  signal r4_x_delay_net : std_logic_vector( 32-1 downto 0 );
  signal r6_x_min_net : std_logic_vector( 32-1 downto 0 );
  signal r7_x_max_net : std_logic_vector( 32-1 downto 0 );
  signal r8_en_pkd_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal logical_y_net : std_logic_vector( 14-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal mcode_re_net : std_logic_vector( 1-1 downto 0 );
  signal q_ce_net : std_logic;
  signal register_q_net : std_logic_vector( 32-1 downto 0 );
  signal register3_q_net : std_logic_vector( 32-1 downto 0 );
  signal d_clk_net : std_logic;
  signal register2_q_net : std_logic_vector( 32-1 downto 0 );
  signal register4_q_net : std_logic_vector( 32-1 downto 0 );
  signal x_net : std_logic_vector( 16-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
begin
  r4_x_delay_net <= r4_x_delay;
  r5_x_noise_net <= r5_x_noise;
  r6_x_min_net <= r6_x_min;
  r7_x_max_net <= r7_x_max;
  r8_en_pkd_net <= r8_en_pkd;
  peak_amp <= reinterpret_output_port_net;
  peak_amp_dac <= logical_y_net;
  peak_amp_rdy <= mcode_re_net;
  x_net <= x;
  clk_net <= clk_1_x0;
  ce_net <= ce_1_x0;
  d_clk_net <= clk_1;
  q_ce_net <= ce_1;
  axibusdomain : entity xil_defaultlib.ip_peakdetector_axibusdomain 
  port map (
    r4_x_delay => r4_x_delay_net,
    r5_x_noise => r5_x_noise_net,
    r6_x_min => r6_x_min_net,
    r7_x_max => r7_x_max_net,
    r8_en_pkd => r8_en_pkd_net
  );
  signaldomain : entity xil_defaultlib.ip_peakdetector_signaldomain 
  port map (
    x_delay => register_q_net,
    x_noise => register1_q_net,
    x_min => register2_q_net,
    x_max => register3_q_net,
    en_pkd => register4_q_net,
    x => x_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    peak_amp => reinterpret_output_port_net,
    peak_amp_dac => logical_y_net,
    peak_amp_rdy => mcode_re_net
  );
  register_x0 : entity xil_defaultlib.ip_peakdetector_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r4_x_delay_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register_q_net
  );
  register1 : entity xil_defaultlib.ip_peakdetector_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r5_x_noise_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.ip_peakdetector_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r6_x_min_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register2_q_net
  );
  register3 : entity xil_defaultlib.ip_peakdetector_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r7_x_max_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register3_q_net
  );
  register4 : entity xil_defaultlib.ip_peakdetector_xlAsynRegister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r8_en_pkd_net,
    d_clk => d_clk_net,
    q_ce => q_ce_net,
    q_clk => clk_net,
    d_ce => ce_net,
    q => register4_q_net
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector_default_clock_driver is
  port (
    signaldomain_sysclk : in std_logic;
    signaldomain_sysce : in std_logic;
    signaldomain_sysclr : in std_logic;
    axibusdomain_sysclk : in std_logic;
    axibusdomain_sysce : in std_logic;
    axibusdomain_sysclr : in std_logic;
    signaldomain_clk1 : out std_logic;
    signaldomain_ce1 : out std_logic;
    axibusdomain_clk1 : out std_logic;
    axibusdomain_ce1 : out std_logic
  );
end ip_peakdetector_default_clock_driver;
architecture structural of ip_peakdetector_default_clock_driver is 
begin
  clockdriver_x0 : entity xil_defaultlib.xlclockdriver 
  generic map (
    period => 1,
    log_2_period => 1
  )
  port map (
    sysclk => signaldomain_sysclk,
    sysce => signaldomain_sysce,
    sysclr => signaldomain_sysclr,
    clk => signaldomain_clk1,
    ce => signaldomain_ce1
  );
  clockdriver : entity xil_defaultlib.xlclockdriver 
  generic map (
    period => 1,
    log_2_period => 1
  )
  port map (
    sysclk => axibusdomain_sysclk,
    sysce => axibusdomain_sysce,
    sysclr => axibusdomain_sysclr,
    clk => axibusdomain_clk1,
    ce => axibusdomain_ce1
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity ip_peakdetector is
  port (
    x : in std_logic_vector( 16-1 downto 0 );
    signaldomain_clk : in std_logic;
    axibusdomain_clk : in std_logic;
    axibusdomain_aresetn : in std_logic;
    axibusdomain_s_axi_awaddr : in std_logic_vector( 5-1 downto 0 );
    axibusdomain_s_axi_awvalid : in std_logic;
    axibusdomain_s_axi_wdata : in std_logic_vector( 32-1 downto 0 );
    axibusdomain_s_axi_wstrb : in std_logic_vector( 4-1 downto 0 );
    axibusdomain_s_axi_wvalid : in std_logic;
    axibusdomain_s_axi_bready : in std_logic;
    axibusdomain_s_axi_araddr : in std_logic_vector( 5-1 downto 0 );
    axibusdomain_s_axi_arvalid : in std_logic;
    axibusdomain_s_axi_rready : in std_logic;
    peak_amp : out std_logic_vector( 16-1 downto 0 );
    peak_amp_dac : out std_logic_vector( 14-1 downto 0 );
    peak_amp_rdy : out std_logic_vector( 1-1 downto 0 );
    axibusdomain_s_axi_awready : out std_logic;
    axibusdomain_s_axi_wready : out std_logic;
    axibusdomain_s_axi_bresp : out std_logic_vector( 2-1 downto 0 );
    axibusdomain_s_axi_bvalid : out std_logic;
    axibusdomain_s_axi_arready : out std_logic;
    axibusdomain_s_axi_rdata : out std_logic_vector( 32-1 downto 0 );
    axibusdomain_s_axi_rresp : out std_logic_vector( 2-1 downto 0 );
    axibusdomain_s_axi_rvalid : out std_logic
  );
end ip_peakdetector;
architecture structural of ip_peakdetector is 
  attribute core_generation_info : string;
  attribute core_generation_info of structural : architecture is "ip_peakdetector,sysgen_core_2018_3,{,compilation=IP Catalog,block_icon_display=Default,family=artix7,part=xc7a35t,speed=-1,package=cpg236,synthesis_language=vhdl,hdl_library=xil_defaultlib,synthesis_strategy=Vivado Synthesis Defaults,implementation_strategy=Vivado Implementation Defaults,testbench=0,interface_doc=1,ce_clr=0,clock_period=-10,system_simulink_period=-1,waveform_viewer=0,axilite_interface=1,ip_catalog_plugin=0,hwcosim_burst_mode=0,simulation_time=0.004,addsub=2,constant=3,convert=3,counter=1,logical=1,mcode=3,register=5,reinterpret=9,relational=1,slice=9,spram=1,}";
  signal axibusdomain_clk_net : std_logic;
  signal ce_1_net_x0 : std_logic;
  signal clk_1_net_x0 : std_logic;
  signal ce_1_net : std_logic;
  signal r4_x_delay : std_logic_vector( 32-1 downto 0 );
  signal r6_x_min : std_logic_vector( 32-1 downto 0 );
  signal r5_x_noise : std_logic_vector( 32-1 downto 0 );
  signal r7_x_max : std_logic_vector( 32-1 downto 0 );
  signal r8_en_pkd : std_logic_vector( 32-1 downto 0 );
  signal clk_1_net : std_logic;
begin
  axibusdomain_axi_lite_interface : entity xil_defaultlib.axibusdomain_axi_lite_interface 
  port map (
    axibusdomain_s_axi_awaddr => axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata => axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb => axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr => axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
    axibusdomain_aresetn => axibusdomain_aresetn,
    axibusdomain_aclk => axibusdomain_clk,
    r8_en_pkd => r8_en_pkd,
    r7_x_max => r7_x_max,
    r6_x_min => r6_x_min,
    r5_x_noise => r5_x_noise,
    r4_x_delay => r4_x_delay,
    axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp => axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata => axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp => axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid,
    axibusdomain_clk => axibusdomain_clk_net
  );
  ip_peakdetector_default_clock_driver : entity xil_defaultlib.ip_peakdetector_default_clock_driver 
  port map (
    signaldomain_sysclk => signaldomain_clk,
    signaldomain_sysce => '1',
    signaldomain_sysclr => '0',
    axibusdomain_sysclk => axibusdomain_clk_net,
    axibusdomain_sysce => '1',
    axibusdomain_sysclr => '0',
    signaldomain_clk1 => clk_1_net,
    signaldomain_ce1 => ce_1_net_x0,
    axibusdomain_clk1 => clk_1_net_x0,
    axibusdomain_ce1 => ce_1_net
  );
  ip_peakdetector_struct : entity xil_defaultlib.ip_peakdetector_struct 
  port map (
    r4_x_delay => r4_x_delay,
    r5_x_noise => r5_x_noise,
    r6_x_min => r6_x_min,
    r7_x_max => r7_x_max,
    r8_en_pkd => r8_en_pkd,
    x => x,
    clk_1_x0 => clk_1_net,
    ce_1_x0 => ce_1_net_x0,
    clk_1 => clk_1_net_x0,
    ce_1 => ce_1_net,
    peak_amp => peak_amp,
    peak_amp_dac => peak_amp_dac,
    peak_amp_rdy => peak_amp_rdy
  );
end structural;
