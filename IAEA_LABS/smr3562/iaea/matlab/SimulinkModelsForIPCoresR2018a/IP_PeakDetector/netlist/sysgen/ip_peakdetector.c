#include "ip_peakdetector.h"
#ifndef __linux__
int ip_peakdetector_CfgInitialize(ip_peakdetector *InstancePtr, ip_peakdetector_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->ip_peakdetector_BaseAddress = ConfigPtr->ip_peakdetector_BaseAddress;

    InstancePtr->IsReady = 1;
    return XST_SUCCESS;
}
#endif
void ip_peakdetector_r8_en_pkd_write(ip_peakdetector *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_peakdetector_WriteReg(InstancePtr->ip_peakdetector_BaseAddress, 0, Data);
}
u32 ip_peakdetector_r8_en_pkd_read(ip_peakdetector *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_peakdetector_ReadReg(InstancePtr->ip_peakdetector_BaseAddress, 0);
    return Data;
}
void ip_peakdetector_r7_x_max_write(ip_peakdetector *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_peakdetector_WriteReg(InstancePtr->ip_peakdetector_BaseAddress, 4, Data);
}
u32 ip_peakdetector_r7_x_max_read(ip_peakdetector *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_peakdetector_ReadReg(InstancePtr->ip_peakdetector_BaseAddress, 4);
    return Data;
}
void ip_peakdetector_r6_x_min_write(ip_peakdetector *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_peakdetector_WriteReg(InstancePtr->ip_peakdetector_BaseAddress, 8, Data);
}
u32 ip_peakdetector_r6_x_min_read(ip_peakdetector *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_peakdetector_ReadReg(InstancePtr->ip_peakdetector_BaseAddress, 8);
    return Data;
}
void ip_peakdetector_r5_x_noise_write(ip_peakdetector *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_peakdetector_WriteReg(InstancePtr->ip_peakdetector_BaseAddress, 12, Data);
}
u32 ip_peakdetector_r5_x_noise_read(ip_peakdetector *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_peakdetector_ReadReg(InstancePtr->ip_peakdetector_BaseAddress, 12);
    return Data;
}
void ip_peakdetector_r4_x_delay_write(ip_peakdetector *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    ip_peakdetector_WriteReg(InstancePtr->ip_peakdetector_BaseAddress, 16, Data);
}
u32 ip_peakdetector_r4_x_delay_read(ip_peakdetector *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = ip_peakdetector_ReadReg(InstancePtr->ip_peakdetector_BaseAddress, 16);
    return Data;
}
