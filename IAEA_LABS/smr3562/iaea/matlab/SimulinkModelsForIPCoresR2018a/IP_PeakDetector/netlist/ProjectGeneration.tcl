# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator. 
#


namespace eval ::xilinx::dsp::planaheaddriver {
	set Compilation {IP Catalog}
	set CompilationFlow {IP}
	set CreateInterfaceDocument {on}
	set DSPDevice {xc7a35t}
	set DSPFamily {artix7}
	set DSPPackage {cpg236}
	set DSPSpeed {-1}
	set FPGAClockPeriod -10
	set GenerateTestBench 0
	set HDLLanguage {vhdl}
	set IPOOCCacheRootPath {C:/Users/boss/AppData/Local/Xilinx/Sysgen/SysgenVivado/win64.o/ip}
	set IP_Auto_Infer {1}
	set IP_Categories_Text {System Generator for DSP}
	set IP_Common_Repos {0}
	set IP_Description {}
	set IP_Dir {C:/Zynqtr_zedboard/ip_repo}
	set IP_Library_Text {user}
	set IP_LifeCycle_Menu {1}
	set IP_Logo {sysgen_icon_100.png}
	set IP_Name {IP_PeakDetector}
	set IP_Revision {204460901}
	set IP_Socket_IP {0}
	set IP_Socket_IP_Proj_Path {}
	set IP_Vendor_Text {IAEA}
	set IP_Version_Text {1.0}
	set ImplStrategyName {Vivado Implementation Defaults}
	set PostProjectCreationProc {dsp_package_for_vivado_ip_integrator}
	set Project {ip_peakdetector}
	set ProjectFiles {
		{{conv_pkg.vhd} -lib {xil_defaultlib}}
		{{synth_reg.vhd} -lib {xil_defaultlib}}
		{{synth_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{srl17e.vhd} -lib {xil_defaultlib}}
		{{srl33e.vhd} -lib {xil_defaultlib}}
		{{synth_reg_reg.vhd} -lib {xil_defaultlib}}
		{{single_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{xlclockdriver_rd.vhd} -lib {xil_defaultlib}}
		{{vivado_ip.tcl}}
		{{ip_peakdetector_blk_mem_gen_i0_vivado.coe}}
		{{axibusdomain_axi_lite_interface_verilog.v}}
		{{conv_pkg.v}}
		{{synth_reg.v}}
		{{synth_reg_w_init.v}}
		{{convert_type.v}}
		{{ip_peakdetector.mdd}}
		{{ip_peakdetector_hw.h}}
		{{ip_peakdetector.h}}
		{{ip_peakdetector_sinit.c}}
		{{ip_peakdetector.c}}
		{{ip_peakdetector_linux.c}}
		{{ip_peakdetector.mtcl}}
		{{Makefile.mak}}
		{{index.html}}
		{{ip_peakdetector_entity_declarations.vhd} -lib {xil_defaultlib}}
		{{ip_peakdetector.vhd} -lib {xil_defaultlib}}
		{{ip_peakdetector_clock.xdc}}
		{{ip_peakdetector.xdc}}
		{{C:/Users/boss/Dropbox/FPGA/SimulinkModelsForIPCores/IP_PeakDetector/netlist/sysgen/ip_peakdetector_false_paths.xdc}}
		{{ip_peakdetector.htm}}
	}
	set SimPeriod -1
	set SimTime 0.004
	set SimulationTime {4000220.00000000 ns}
	set SynthStrategyName {Vivado Synthesis Defaults}
	set SynthesisTool {Vivado}
	set TargetDir {C:/Users/boss/Dropbox/FPGA/SimulinkModelsForIPCores/IP_PeakDetector/netlist}
	set TopLevelModule {ip_peakdetector}
	set TopLevelSimulinkHandle 2.00024
	set VHDLLib {xil_defaultlib}
	set TopLevelPortInterface {}
	dict set TopLevelPortInterface x Name {x}
	dict set TopLevelPortInterface x Type Fix_16_14
	dict set TopLevelPortInterface x ArithmeticType xlSigned
	dict set TopLevelPortInterface x BinaryPoint 14
	dict set TopLevelPortInterface x Width 16
	dict set TopLevelPortInterface x DatFile {ip_peakdetector_signaldomain_x.dat}
	dict set TopLevelPortInterface x IconText {x}
	dict set TopLevelPortInterface x Direction in
	dict set TopLevelPortInterface x Period 1
	dict set TopLevelPortInterface x Interface 0
	dict set TopLevelPortInterface x InterfaceName {}
	dict set TopLevelPortInterface x InterfaceString {DATA}
	dict set TopLevelPortInterface x ClockDomain {signaldomain}
	dict set TopLevelPortInterface x Locs {}
	dict set TopLevelPortInterface x IOStandard {}
	dict set TopLevelPortInterface peak_amp Name {peak_amp}
	dict set TopLevelPortInterface peak_amp Type UFix_16_0
	dict set TopLevelPortInterface peak_amp ArithmeticType xlUnsigned
	dict set TopLevelPortInterface peak_amp BinaryPoint 0
	dict set TopLevelPortInterface peak_amp Width 16
	dict set TopLevelPortInterface peak_amp DatFile {ip_peakdetector_signaldomain_peak_amp.dat}
	dict set TopLevelPortInterface peak_amp IconText {peak_amp}
	dict set TopLevelPortInterface peak_amp Direction out
	dict set TopLevelPortInterface peak_amp Period 1
	dict set TopLevelPortInterface peak_amp Interface 0
	dict set TopLevelPortInterface peak_amp InterfaceName {}
	dict set TopLevelPortInterface peak_amp InterfaceString {DATA}
	dict set TopLevelPortInterface peak_amp ClockDomain {signaldomain}
	dict set TopLevelPortInterface peak_amp Locs {}
	dict set TopLevelPortInterface peak_amp IOStandard {}
	dict set TopLevelPortInterface peak_amp_dac Name {peak_amp_dac}
	dict set TopLevelPortInterface peak_amp_dac Type UFix_14_0
	dict set TopLevelPortInterface peak_amp_dac ArithmeticType xlUnsigned
	dict set TopLevelPortInterface peak_amp_dac BinaryPoint 0
	dict set TopLevelPortInterface peak_amp_dac Width 14
	dict set TopLevelPortInterface peak_amp_dac DatFile {ip_peakdetector_signaldomain_peak_amp_dac.dat}
	dict set TopLevelPortInterface peak_amp_dac IconText {peak_amp_dac}
	dict set TopLevelPortInterface peak_amp_dac Direction out
	dict set TopLevelPortInterface peak_amp_dac Period 1
	dict set TopLevelPortInterface peak_amp_dac Interface 0
	dict set TopLevelPortInterface peak_amp_dac InterfaceName {}
	dict set TopLevelPortInterface peak_amp_dac InterfaceString {DATA}
	dict set TopLevelPortInterface peak_amp_dac ClockDomain {signaldomain}
	dict set TopLevelPortInterface peak_amp_dac Locs {}
	dict set TopLevelPortInterface peak_amp_dac IOStandard {}
	dict set TopLevelPortInterface peak_amp_rdy Name {peak_amp_rdy}
	dict set TopLevelPortInterface peak_amp_rdy Type Bool
	dict set TopLevelPortInterface peak_amp_rdy ArithmeticType xlUnsigned
	dict set TopLevelPortInterface peak_amp_rdy BinaryPoint 0
	dict set TopLevelPortInterface peak_amp_rdy Width 1
	dict set TopLevelPortInterface peak_amp_rdy DatFile {ip_peakdetector_signaldomain_peak_amp_rdy.dat}
	dict set TopLevelPortInterface peak_amp_rdy IconText {peak_amp_rdy}
	dict set TopLevelPortInterface peak_amp_rdy Direction out
	dict set TopLevelPortInterface peak_amp_rdy Period 1
	dict set TopLevelPortInterface peak_amp_rdy Interface 0
	dict set TopLevelPortInterface peak_amp_rdy InterfaceName {}
	dict set TopLevelPortInterface peak_amp_rdy InterfaceString {DATA}
	dict set TopLevelPortInterface peak_amp_rdy ClockDomain {signaldomain}
	dict set TopLevelPortInterface peak_amp_rdy Locs {}
	dict set TopLevelPortInterface peak_amp_rdy IOStandard {}
	dict set TopLevelPortInterface axibusdomain_aresetn Name {axibusdomain_aresetn}
	dict set TopLevelPortInterface axibusdomain_aresetn Type -
	dict set TopLevelPortInterface axibusdomain_aresetn ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_aresetn BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_aresetn Width 1
	dict set TopLevelPortInterface axibusdomain_aresetn DatFile {}
	dict set TopLevelPortInterface axibusdomain_aresetn IconText {axibusdomain_aresetn}
	dict set TopLevelPortInterface axibusdomain_aresetn Direction in
	dict set TopLevelPortInterface axibusdomain_aresetn Period 1
	dict set TopLevelPortInterface axibusdomain_aresetn Interface 8
	dict set TopLevelPortInterface axibusdomain_aresetn InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_aresetn InterfaceString {ARESETN}
	dict set TopLevelPortInterface axibusdomain_aresetn ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_aresetn Locs {}
	dict set TopLevelPortInterface axibusdomain_aresetn IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Name {axibusdomain_s_axi_awaddr}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Width 5
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr IconText {axibusdomain_s_axi_awaddr}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awaddr IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Name {axibusdomain_s_axi_awvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid IconText {axibusdomain_s_axi_awvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Name {axibusdomain_s_axi_awready}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_awready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_awready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready IconText {axibusdomain_s_axi_awready}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_awready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_awready IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Name {axibusdomain_s_axi_wdata}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Width 32
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata IconText {axibusdomain_s_axi_wdata}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wdata IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Name {axibusdomain_s_axi_wstrb}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Width 4
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb IconText {axibusdomain_s_axi_wstrb}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wstrb IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Name {axibusdomain_s_axi_wvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid IconText {axibusdomain_s_axi_wvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Name {axibusdomain_s_axi_wready}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_wready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_wready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready IconText {axibusdomain_s_axi_wready}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_wready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_wready IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Name {axibusdomain_s_axi_bresp}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Width 2
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp IconText {axibusdomain_s_axi_bresp}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bresp IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Name {axibusdomain_s_axi_bvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid IconText {axibusdomain_s_axi_bvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Name {axibusdomain_s_axi_bready}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_bready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_bready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready IconText {axibusdomain_s_axi_bready}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_bready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_bready IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Name {axibusdomain_s_axi_araddr}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Width 5
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr IconText {axibusdomain_s_axi_araddr}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_araddr IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Name {axibusdomain_s_axi_arvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid IconText {axibusdomain_s_axi_arvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Name {axibusdomain_s_axi_arready}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_arready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_arready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_arready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready IconText {axibusdomain_s_axi_arready}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_arready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_arready IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Name {axibusdomain_s_axi_rdata}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Width 32
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata IconText {axibusdomain_s_axi_rdata}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rdata IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Name {axibusdomain_s_axi_rresp}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Width 2
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp IconText {axibusdomain_s_axi_rresp}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rresp IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Name {axibusdomain_s_axi_rvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid IconText {axibusdomain_s_axi_rvalid}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Direction out
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rvalid IOStandard {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Name {axibusdomain_s_axi_rready}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Type -
	dict set TopLevelPortInterface axibusdomain_s_axi_rready ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_s_axi_rready BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Width 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rready DatFile {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready IconText {axibusdomain_s_axi_rready}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Direction in
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Period 1
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Interface 5
	dict set TopLevelPortInterface axibusdomain_s_axi_rready InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready InterfaceString {WIRE_AXI_LITE}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready ClockDomain {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready Locs {}
	dict set TopLevelPortInterface axibusdomain_s_axi_rready IOStandard {}
	dict set TopLevelPortInterface signaldomain_clk Name {signaldomain_clk}
	dict set TopLevelPortInterface signaldomain_clk Type -
	dict set TopLevelPortInterface signaldomain_clk ArithmeticType xlUnsigned
	dict set TopLevelPortInterface signaldomain_clk BinaryPoint 0
	dict set TopLevelPortInterface signaldomain_clk Width 1
	dict set TopLevelPortInterface signaldomain_clk DatFile {}
	dict set TopLevelPortInterface signaldomain_clk Direction in
	dict set TopLevelPortInterface signaldomain_clk Period 1
	dict set TopLevelPortInterface signaldomain_clk Interface 6
	dict set TopLevelPortInterface signaldomain_clk InterfaceName {}
	dict set TopLevelPortInterface signaldomain_clk InterfaceString {CLOCK}
	dict set TopLevelPortInterface signaldomain_clk ClockDomain {signaldomain}
	dict set TopLevelPortInterface signaldomain_clk Locs {}
	dict set TopLevelPortInterface signaldomain_clk IOStandard {}
	dict set TopLevelPortInterface signaldomain_clk AssociatedInterfaces {}
	dict set TopLevelPortInterface signaldomain_clk AssociatedResets {}
	dict set TopLevelPortInterface axibusdomain_clk Name {axibusdomain_clk}
	dict set TopLevelPortInterface axibusdomain_clk Type -
	dict set TopLevelPortInterface axibusdomain_clk ArithmeticType xlUnsigned
	dict set TopLevelPortInterface axibusdomain_clk BinaryPoint 0
	dict set TopLevelPortInterface axibusdomain_clk Width 1
	dict set TopLevelPortInterface axibusdomain_clk DatFile {}
	dict set TopLevelPortInterface axibusdomain_clk Direction in
	dict set TopLevelPortInterface axibusdomain_clk Period 1
	dict set TopLevelPortInterface axibusdomain_clk Interface 6
	dict set TopLevelPortInterface axibusdomain_clk InterfaceName {}
	dict set TopLevelPortInterface axibusdomain_clk InterfaceString {CLOCK}
	dict set TopLevelPortInterface axibusdomain_clk ClockDomain {axibusdomain}
	dict set TopLevelPortInterface axibusdomain_clk Locs {}
	dict set TopLevelPortInterface axibusdomain_clk IOStandard {}
	dict set TopLevelPortInterface axibusdomain_clk AssociatedInterfaces {axibusdomain_s_axi }
	dict set TopLevelPortInterface axibusdomain_clk AssociatedResets {axibusdomain_aresetn }
	set MemoryMappedPort {}
	dict set MemoryMappedPort r8_en_pkd Name {r8_en_pkd}
	dict set MemoryMappedPort r8_en_pkd Type UFix_32_0
	dict set MemoryMappedPort r8_en_pkd ArithmeticType xlUnsigned
	dict set MemoryMappedPort r8_en_pkd BinaryPoint 0
	dict set MemoryMappedPort r8_en_pkd Width 32
	dict set MemoryMappedPort r8_en_pkd DatFile {ip_peakdetector_axibusdomain_r8_en_pkd.dat}
	dict set MemoryMappedPort r8_en_pkd AddressOffset 0
	dict set MemoryMappedPort r8_en_pkd IconText {r8_en_pkd}
	dict set MemoryMappedPort r8_en_pkd Direction in
	dict set MemoryMappedPort r8_en_pkd Period 1
	dict set MemoryMappedPort r8_en_pkd Interface 2
	dict set MemoryMappedPort r8_en_pkd InterfaceName {}
	dict set MemoryMappedPort r8_en_pkd InterfaceString {CONTROL}
	dict set MemoryMappedPort r8_en_pkd ClockDomain {axibusdomain}
	dict set MemoryMappedPort r8_en_pkd Locs {}
	dict set MemoryMappedPort r8_en_pkd IOStandard {}
	dict set MemoryMappedPort r7_x_max Name {r7_x_max}
	dict set MemoryMappedPort r7_x_max Type UFix_32_0
	dict set MemoryMappedPort r7_x_max ArithmeticType xlUnsigned
	dict set MemoryMappedPort r7_x_max BinaryPoint 0
	dict set MemoryMappedPort r7_x_max Width 32
	dict set MemoryMappedPort r7_x_max DatFile {ip_peakdetector_axibusdomain_r7_x_max.dat}
	dict set MemoryMappedPort r7_x_max AddressOffset 4
	dict set MemoryMappedPort r7_x_max IconText {r7_x_max}
	dict set MemoryMappedPort r7_x_max Direction in
	dict set MemoryMappedPort r7_x_max Period 1
	dict set MemoryMappedPort r7_x_max Interface 2
	dict set MemoryMappedPort r7_x_max InterfaceName {}
	dict set MemoryMappedPort r7_x_max InterfaceString {CONTROL}
	dict set MemoryMappedPort r7_x_max ClockDomain {axibusdomain}
	dict set MemoryMappedPort r7_x_max Locs {}
	dict set MemoryMappedPort r7_x_max IOStandard {}
	dict set MemoryMappedPort r6_x_min Name {r6_x_min}
	dict set MemoryMappedPort r6_x_min Type UFix_32_0
	dict set MemoryMappedPort r6_x_min ArithmeticType xlUnsigned
	dict set MemoryMappedPort r6_x_min BinaryPoint 0
	dict set MemoryMappedPort r6_x_min Width 32
	dict set MemoryMappedPort r6_x_min DatFile {ip_peakdetector_axibusdomain_r6_x_min.dat}
	dict set MemoryMappedPort r6_x_min AddressOffset 8
	dict set MemoryMappedPort r6_x_min IconText {r6_x_min}
	dict set MemoryMappedPort r6_x_min Direction in
	dict set MemoryMappedPort r6_x_min Period 1
	dict set MemoryMappedPort r6_x_min Interface 2
	dict set MemoryMappedPort r6_x_min InterfaceName {}
	dict set MemoryMappedPort r6_x_min InterfaceString {CONTROL}
	dict set MemoryMappedPort r6_x_min ClockDomain {axibusdomain}
	dict set MemoryMappedPort r6_x_min Locs {}
	dict set MemoryMappedPort r6_x_min IOStandard {}
	dict set MemoryMappedPort r5_x_noise Name {r5_x_noise}
	dict set MemoryMappedPort r5_x_noise Type UFix_32_0
	dict set MemoryMappedPort r5_x_noise ArithmeticType xlUnsigned
	dict set MemoryMappedPort r5_x_noise BinaryPoint 0
	dict set MemoryMappedPort r5_x_noise Width 32
	dict set MemoryMappedPort r5_x_noise DatFile {ip_peakdetector_axibusdomain_r5_x_noise.dat}
	dict set MemoryMappedPort r5_x_noise AddressOffset 12
	dict set MemoryMappedPort r5_x_noise IconText {r5_x_noise}
	dict set MemoryMappedPort r5_x_noise Direction in
	dict set MemoryMappedPort r5_x_noise Period 1
	dict set MemoryMappedPort r5_x_noise Interface 2
	dict set MemoryMappedPort r5_x_noise InterfaceName {}
	dict set MemoryMappedPort r5_x_noise InterfaceString {CONTROL}
	dict set MemoryMappedPort r5_x_noise ClockDomain {axibusdomain}
	dict set MemoryMappedPort r5_x_noise Locs {}
	dict set MemoryMappedPort r5_x_noise IOStandard {}
	dict set MemoryMappedPort r4_x_delay Name {r4_x_delay}
	dict set MemoryMappedPort r4_x_delay Type UFix_32_0
	dict set MemoryMappedPort r4_x_delay ArithmeticType xlUnsigned
	dict set MemoryMappedPort r4_x_delay BinaryPoint 0
	dict set MemoryMappedPort r4_x_delay Width 32
	dict set MemoryMappedPort r4_x_delay DatFile {ip_peakdetector_axibusdomain_r4_x_delay.dat}
	dict set MemoryMappedPort r4_x_delay AddressOffset 16
	dict set MemoryMappedPort r4_x_delay IconText {r4_x_delay}
	dict set MemoryMappedPort r4_x_delay Direction in
	dict set MemoryMappedPort r4_x_delay Period 1
	dict set MemoryMappedPort r4_x_delay Interface 2
	dict set MemoryMappedPort r4_x_delay InterfaceName {}
	dict set MemoryMappedPort r4_x_delay InterfaceString {CONTROL}
	dict set MemoryMappedPort r4_x_delay ClockDomain {axibusdomain}
	dict set MemoryMappedPort r4_x_delay Locs {}
	dict set MemoryMappedPort r4_x_delay IOStandard {}
}

source SgPaProject.tcl
::xilinx::dsp::planaheadworker::dsp_create_project