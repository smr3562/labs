--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
--Date        : Tue Nov 12 10:42:24 2019
--Host        : LAPTOP-E0VBN3RI running 64-bit major release  (build 9200)
--Command     : generate_target ip_peakdetector_bd_wrapper.bd
--Design      : ip_peakdetector_bd_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ip_peakdetector_bd_wrapper is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC;
    CLK_IN1_D_clk_p : in STD_LOGIC;
    peak_amp : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_rtl : in STD_LOGIC;
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end ip_peakdetector_bd_wrapper;

architecture STRUCTURE of ip_peakdetector_bd_wrapper is
  component ip_peakdetector_bd is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC;
    CLK_IN1_D_clk_p : in STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    peak_amp : out STD_LOGIC_VECTOR ( 15 downto 0 );
    peak_amp_dac : out STD_LOGIC_VECTOR ( 13 downto 0 );
    peak_amp_rdy : out STD_LOGIC_VECTOR ( 0 to 0 );
    signaldomain_clk : in STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component ip_peakdetector_bd;
begin
ip_peakdetector_bd_i: component ip_peakdetector_bd
     port map (
      CLK_IN1_D_clk_n => CLK_IN1_D_clk_n,
      CLK_IN1_D_clk_p => CLK_IN1_D_clk_p,
      peak_amp(15 downto 0) => peak_amp(15 downto 0),
      peak_amp_dac(13 downto 0) => peak_amp_dac(13 downto 0),
      peak_amp_rdy(0) => peak_amp_rdy(0),
      reset_rtl => reset_rtl,
      signaldomain_clk => signaldomain_clk,
      x(15 downto 0) => x(15 downto 0)
    );
end STRUCTURE;
