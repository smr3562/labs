/**
*
* @file ip_peakdetector_hw.h
*
* This header file contains identifiers and driver functions (or
* macros) that can be used to access the device.  The user should refer to the
* hardware device specification for more details of the device operation.
*/ 
#define IP_PEAKDETECTOR_R8_EN_PKD 0x0/**< r8_en_pkd */
#define IP_PEAKDETECTOR_R7_X_MAX 0x4/**< r7_x_max */
#define IP_PEAKDETECTOR_R6_X_MIN 0x8/**< r6_x_min */
#define IP_PEAKDETECTOR_R5_X_NOISE 0xc/**< r5_x_noise */
#define IP_PEAKDETECTOR_R4_X_DELAY 0x10/**< r4_x_delay */
