-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
entity ip_peakdetector_stub is
  port (
    x : in std_logic_vector( 16-1 downto 0 );
    axibusdomain_clk : in std_logic;
    signaldomain_clk : in std_logic;
    axibusdomain_aresetn : in std_logic;
    axibusdomain_s_axi_awaddr : in std_logic_vector( 5-1 downto 0 );
    axibusdomain_s_axi_awvalid : in std_logic;
    axibusdomain_s_axi_wdata : in std_logic_vector( 32-1 downto 0 );
    axibusdomain_s_axi_wstrb : in std_logic_vector( 4-1 downto 0 );
    axibusdomain_s_axi_wvalid : in std_logic;
    axibusdomain_s_axi_bready : in std_logic;
    axibusdomain_s_axi_araddr : in std_logic_vector( 5-1 downto 0 );
    axibusdomain_s_axi_arvalid : in std_logic;
    axibusdomain_s_axi_rready : in std_logic;
    peak_amp : out std_logic_vector( 16-1 downto 0 );
    peak_amp_dac : out std_logic_vector( 14-1 downto 0 );
    peak_amp_rdy : out std_logic_vector( 1-1 downto 0 );
    axibusdomain_s_axi_awready : out std_logic;
    axibusdomain_s_axi_wready : out std_logic;
    axibusdomain_s_axi_bresp : out std_logic_vector( 2-1 downto 0 );
    axibusdomain_s_axi_bvalid : out std_logic;
    axibusdomain_s_axi_arready : out std_logic;
    axibusdomain_s_axi_rdata : out std_logic_vector( 32-1 downto 0 );
    axibusdomain_s_axi_rresp : out std_logic_vector( 2-1 downto 0 );
    axibusdomain_s_axi_rvalid : out std_logic
  );
end ip_peakdetector_stub;
architecture structural of ip_peakdetector_stub is 
begin
  sysgen_dut : entity xil_defaultlib.ip_peakdetector_0 
  port map (
    x => x,
    axibusdomain_clk => axibusdomain_clk,
    signaldomain_clk => signaldomain_clk,
    axibusdomain_aresetn => axibusdomain_aresetn,
    axibusdomain_s_axi_awaddr => axibusdomain_s_axi_awaddr,
    axibusdomain_s_axi_awvalid => axibusdomain_s_axi_awvalid,
    axibusdomain_s_axi_wdata => axibusdomain_s_axi_wdata,
    axibusdomain_s_axi_wstrb => axibusdomain_s_axi_wstrb,
    axibusdomain_s_axi_wvalid => axibusdomain_s_axi_wvalid,
    axibusdomain_s_axi_bready => axibusdomain_s_axi_bready,
    axibusdomain_s_axi_araddr => axibusdomain_s_axi_araddr,
    axibusdomain_s_axi_arvalid => axibusdomain_s_axi_arvalid,
    axibusdomain_s_axi_rready => axibusdomain_s_axi_rready,
    peak_amp => peak_amp,
    peak_amp_dac => peak_amp_dac,
    peak_amp_rdy => peak_amp_rdy,
    axibusdomain_s_axi_awready => axibusdomain_s_axi_awready,
    axibusdomain_s_axi_wready => axibusdomain_s_axi_wready,
    axibusdomain_s_axi_bresp => axibusdomain_s_axi_bresp,
    axibusdomain_s_axi_bvalid => axibusdomain_s_axi_bvalid,
    axibusdomain_s_axi_arready => axibusdomain_s_axi_arready,
    axibusdomain_s_axi_rdata => axibusdomain_s_axi_rdata,
    axibusdomain_s_axi_rresp => axibusdomain_s_axi_rresp,
    axibusdomain_s_axi_rvalid => axibusdomain_s_axi_rvalid
  );
end structural;
