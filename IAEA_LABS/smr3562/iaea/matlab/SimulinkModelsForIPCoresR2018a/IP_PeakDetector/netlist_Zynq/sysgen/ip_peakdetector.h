#ifndef IP_PEAKDETECTOR__H
#define IP_PEAKDETECTOR__H
#ifdef __cplusplus
extern "C" {
#endif
/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "ip_peakdetector_hw.h"
/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 ip_peakdetector_BaseAddress;
} ip_peakdetector_Config;
#endif
/**
* The ip_peakdetector driver instance data. The user is required to
* allocate a variable of this type for every ip_peakdetector device in the system.
* A pointer to a variable of this type is then passed to the driver
* API functions.
*/
typedef struct {
    u32 ip_peakdetector_BaseAddress;
    u32 IsReady;
} ip_peakdetector;
/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define ip_peakdetector_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define ip_peakdetector_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define ip_peakdetector_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define ip_peakdetector_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif
/************************** Function Prototypes *****************************/
#ifndef __linux__
int ip_peakdetector_Initialize(ip_peakdetector *InstancePtr, u16 DeviceId);
ip_peakdetector_Config* ip_peakdetector_LookupConfig(u16 DeviceId);
int ip_peakdetector_CfgInitialize(ip_peakdetector *InstancePtr, ip_peakdetector_Config *ConfigPtr);
#else
int ip_peakdetector_Initialize(ip_peakdetector *InstancePtr, const char* InstanceName);
int ip_peakdetector_Release(ip_peakdetector *InstancePtr);
#endif
/**
* Write to r8_en_pkd gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r8_en_pkd instance to operate on.
* @param	Data is value to be written to gateway r8_en_pkd.
*
* @return	None.
*
* @note    .
*
*/
void ip_peakdetector_r8_en_pkd_write(ip_peakdetector *InstancePtr, u32 Data);
/**
* Read from r8_en_pkd gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r8_en_pkd instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_peakdetector_r8_en_pkd_read(ip_peakdetector *InstancePtr);
/**
* Write to r7_x_max gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r7_x_max instance to operate on.
* @param	Data is value to be written to gateway r7_x_max.
*
* @return	None.
*
* @note    .
*
*/
void ip_peakdetector_r7_x_max_write(ip_peakdetector *InstancePtr, u32 Data);
/**
* Read from r7_x_max gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r7_x_max instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_peakdetector_r7_x_max_read(ip_peakdetector *InstancePtr);
/**
* Write to r6_x_min gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r6_x_min instance to operate on.
* @param	Data is value to be written to gateway r6_x_min.
*
* @return	None.
*
* @note    .
*
*/
void ip_peakdetector_r6_x_min_write(ip_peakdetector *InstancePtr, u32 Data);
/**
* Read from r6_x_min gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r6_x_min instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_peakdetector_r6_x_min_read(ip_peakdetector *InstancePtr);
/**
* Write to r5_x_noise gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r5_x_noise instance to operate on.
* @param	Data is value to be written to gateway r5_x_noise.
*
* @return	None.
*
* @note    .
*
*/
void ip_peakdetector_r5_x_noise_write(ip_peakdetector *InstancePtr, u32 Data);
/**
* Read from r5_x_noise gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r5_x_noise instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_peakdetector_r5_x_noise_read(ip_peakdetector *InstancePtr);
/**
* Write to r4_x_delay gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r4_x_delay instance to operate on.
* @param	Data is value to be written to gateway r4_x_delay.
*
* @return	None.
*
* @note    .
*
*/
void ip_peakdetector_r4_x_delay_write(ip_peakdetector *InstancePtr, u32 Data);
/**
* Read from r4_x_delay gateway of ip_peakdetector. Assignments are LSB-justified.
*
* @param	InstancePtr is the r4_x_delay instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 ip_peakdetector_r4_x_delay_read(ip_peakdetector *InstancePtr);
#ifdef __cplusplus
}
#endif
#endif
