% accept = bit 0: 1 == PUR is disabled; 0 == PUR is enabled
% accept = bit 1: 1 == peak did not rejected ; 0 == peak rejected
% note : if pur is disabled bit 1 is always equal 1

% accept = bit 0: 1 == PUR is disabled; 0 == PUR is enabled
% accept = bit 1: 1 == peak did not rejected ; 0 == peak rejected
% note : if pur is disabled bit 1 is always equal 1
 
function [xmax, data_rdy] = pha_pkd(xn, dxn, xt1, en)
 
    s1 = 0, s2 = 1, s3 = 2, s4 = 3, s5 = 4;
    persistent state, state= xl_state(s1,{xlUnsigned,3,0});
    persistent xmax1, xmax1 = xl_state(0.0,{xlSigned, 16, 14, xlRound, xlSaturate});
    persistent xmax_rdy, xmax_rdy = xl_state(false,{xlBoolean});
    
    rst = (~en);
    
    data_rdy = xmax_rdy;
    xmax = xmax1;%xl_force(xmax1,xlUnsigned,0); %xmax1;
           
    if(rst)
        state = s1;
        xmax1 = 0;
        xmax_rdy = false;
    else
        if(state == s1)         %check threshold
            xmax_rdy = false;
            if (xn > xt1 && xn > dxn && en == true)  %above threshold
                state = s2;
            else
                state = s1;         %bellow thresold: wait 
            end
 
        elseif(state == s2)     %check if amplitude falls to 90%
            if (xmax1 < xn)     % not yet maximum: track for maximum
                xmax1 = xn;
            end
            if (xn < dxn)       % 90%point: we have passed peak; xmax1 = peak value
                state = s3;
                xmax_rdy = true;
            end
            
        %wait only one clock for pha arbitrator to letch data 
        elseif(state == s3)     
                state = s4;
            
        elseif(state == s4)     
                xmax_rdy = false;
                state = s5;
          
        elseif(state == s5)     %check if amplitude: falls to xt1 or starts rising to xmin*1.1            
            if (xn < xt1 || xn > dxn)        
                xmax1 = 0;
                state = s1;
            end
                      
        else
            state = s1;
        end
    end
