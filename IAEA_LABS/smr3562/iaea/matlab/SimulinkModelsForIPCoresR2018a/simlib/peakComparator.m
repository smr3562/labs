%    inputs 
%-----------------------
% x      :input signal
% xmin   :a value just above noise
% xdelay :delayed signal - delay is a time in which signal drops 10%
% en     :enable peak detector
%     outputs
%-----------------------
% pkd    :peak detected on rising edge
% xmax   :peak value
function [xmax] = peakComparator(x, rst, en)
    persistent xmax_i, xmax_i = xl_state(0.0,{xlSigned, 16, 14, xlRound, xlSaturate});
    zero = xfix({xlSigned, 16, 14, xlRound, xlWrap}, 0.0);

    xmax = xmax_i;

    if(rst == true)
        xmax_i = zero;
    elseif (en == true && (xmax_i < x))
        xmax_i = x;
    else
        xmax_i = xmax_i;
    end;
    
    