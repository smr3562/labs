%    inputs
%-----------------------
% x      :input signal
% xnoise :a value just above noise
% xdelay :delayed signal - delay is a time in which signal drops 10%
% en     :enable peak detector
%     outputs
%-----------------------
% pkd    :peak detected on rising edge
% cmp_en :enable external comparator
% cmp_rst:reset external comparator
function [pkd, cmp_en, cmp_rst] = peakFSM(x, xdelay, xnoise, en, x_m_xnoise, xdelay_m_xnoise, xmin, xmax)
    st1_IDLE = 0; st2_WAITING_PULSE = 1; st3_INIT_TRACKING = 2; 
    st4_TRACKING_RISE = 3; st5_PEAK_DETECTED = 4; st6_TRACKING_FALL = 5; 
    persistent state, state= xl_state(st1_IDLE,{xlUnsigned,3,0});
    persistent pkd_i, pkd_i = xl_state(false,{xlBoolean});
    persistent cmp_rst_i, cmp_rst_i = xl_state(false,{xlBoolean});
    persistent cmp_en_i, cmp_en_i = xl_state(false,{xlBoolean});
 
    cmp_rst = cmp_rst_i;
    cmp_en = cmp_en_i;
    pkd = pkd_i;
    
    if(en == false)
        state = st1_IDLE;
    end;
    
    switch double(state)
        case st1_IDLE
            state = st2_WAITING_PULSE;
            pkd_i = false;
            cmp_rst_i = true;
            cmp_en_i = false;
        case st2_WAITING_PULSE
            if(x > xnoise)
                state = st3_INIT_TRACKING;
            end;
            pkd_i = false; 
            cmp_rst_i = true;
            cmp_en_i = false;
        case st3_INIT_TRACKING
            state = st4_TRACKING_RISE;
            pkd_i = false;
            cmp_rst_i = true;
            cmp_en_i = false;
        case st4_TRACKING_RISE
            if(x  < xdelay_m_xnoise) 
                state = st5_PEAK_DETECTED;
            end;
            pkd_i = false;
            cmp_rst_i = false;
            cmp_en_i = true;
        case st5_PEAK_DETECTED
            state = st6_TRACKING_FALL;
            if(x  >= xmin && x <= xmax) 
                pkd_i = true;
            end
            cmp_rst_i = false;
            cmp_en_i = false;            
        case st6_TRACKING_FALL
            if(x < xnoise && xdelay < xnoise)
                state = st2_WAITING_PULSE;
            elseif (x_m_xnoise > xdelay)
                state = st3_INIT_TRACKING;
            end;
            if(x  >= xmin && x <= xmax) 
                pkd_i = true;
            end
            cmp_rst_i = false;
            cmp_en_i = false;            
        otherwise
            state = st1_IDLE;            
    end
    