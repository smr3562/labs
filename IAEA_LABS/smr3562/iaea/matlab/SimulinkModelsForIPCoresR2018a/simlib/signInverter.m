% Input: x : Signed number
% en: bool: 
% Output: y 
% 
% calculates:
% y = -x if en = true 
% y =  x if en = false
%
% MCode block literature
% Title: System Generator for DSP Reference Guide, UG638, 
% File name: sysgen_ref.pdf, Chapter 1
% Edition: v11.4 December 2, 2009, page 220 - 241
 
function y = sign_inverter(x,en)
 
%extract data type properties of the input number x
arith = xl_arith(x);
if(arith ~= xlSigned)
    error('my message input must be signed number');
end
nbits = xl_nbits(x);
binpt = xl_binpt(x);

% note:  an example: smallest 16 bit 0 bin pts number is -32768
% its negative value is +32768, which can not be represented with 16 bits
% therefore we must work with 1 bit more, that is nbits+1 
% after obtaining result we must round to nbits
nbits2 = nbits+1;
xsigned = xfix({xlSigned,nbits2,binpt},x);

%in 2's complement sign inversion is done by the following procedure:
 
%1. number is reinterpret as unsigned integer 
xunsigned = xl_force(xsigned,xlUnsigned,0);

%2. each bit is inverted (xl_not) and 1 is added
xinv = xl_not(xunsigned);
xsgninvbin = xfix({xlUnsigned,nbits2,0},xinv+1);

%3 result must be sign number with same number of binary points as input
xsgninvxfix2 = xl_force(xsgninvbin,arith,binpt);

%4 round number with nbits2 bits down to closest number with nbits
xsgninvxfix = xfix({xlSigned,nbits,binpt,xlRound, xlSaturate},xsgninvxfix2);

disp(['nbits=',num2str(nbits),' binpt=',num2str(binpt),' x=',num2str(x),' x=',num2str(xunsigned)]);
 
%output result depending on 'en' port
if(en)
    %reinterpret xsgninvbin with same format as x
    y = xsgninvxfix;
  else
    y = x;
end
