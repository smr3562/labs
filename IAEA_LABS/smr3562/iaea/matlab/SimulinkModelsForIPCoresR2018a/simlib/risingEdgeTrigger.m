function [y, re] = risingedgetrigger(x)
 persistent ff, ff = xl_state(0, {xlUnsigned, 1, 0});
 y = ff;
 ff = x;
 if(y == 0 && x == 1)
     re = true;
 else 
     re = false;
 end