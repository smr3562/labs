% this module does the following:

function [we_ram, we_adr, pha_dacc] = peakStore(en, pha_drdy)
    s1 = 0, s2 = 1, s3 = 2, s4 = 3; 
    
    persistent state, state= xl_state(s1,{xlUnsigned,2,0});
    persistent we_ram_reg, we_ram_reg = xl_state(false,{xlBoolean});
    persistent we_adr_reg, we_adr_reg = xl_state(false,{xlBoolean});
    persistent pha_dacc_reg, pha_dacc_reg = xl_state(false,{xlBoolean});
    
    we_adr = we_adr_reg;
    we_ram = we_ram_reg;
    pha_dacc = pha_dacc_reg; 
     
    if(en == false)
        we_ram_reg = false;
        we_adr_reg = false;
        pha_dacc_reg = false;
        state = s1;
    else        
    
        %wait for data; when data comes latch it
        if(state == s1)      
            if (pha_drdy == true)
                we_adr_reg = true;
                state = s2;
            end

        %increment RAM output data 
        elseif(state == s2)
            we_adr_reg = false;
            pha_dacc_reg = true;
            state = s3;
     
        %write incremented value to the RAM    
        elseif(state == s3)
            we_ram_reg = true;
            state = s4;

        %write incremented value to the RAM    
        elseif(state == s4)
            we_ram_reg = false;
            pha_dacc_reg = false;
            state = s1;
            
        else
            we_ram_reg = false;
            we_adr_reg = false;
            pha_dacc_reg = false;
            state = s1;
        end;
    end;
