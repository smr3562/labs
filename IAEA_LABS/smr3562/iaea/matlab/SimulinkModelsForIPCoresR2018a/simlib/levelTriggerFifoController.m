function [addr, we, full] = levelTriggerFifoController(x, threshold, ena, clear, addr_max, addr_bits)
    st0_IDLE = 0; st1_WAITING_PULSE = 1; st2_WRITING_MEMORY = 2; 
    st3_MEMORY_FULL = 3; 
    persistent state, state= xl_state(st0_IDLE,{xlUnsigned,3,0});
    persistent full_i, full_i = xl_state(0,{xlUnsigned,1,0});
    persistent addr_i, addr_i = xl_state(0,{xlUnsigned, addr_bits, 0, xlTruncate});
    persistent we_i, we_i = xl_state(false,{xlBoolean});
    persistent wm, wm = xl_state(false,{xlBoolean});

    we = we_i;
    addr = addr_i;
    full = full_i;
    
    %the reset signal of the register addr_i is assigned to wm.
    if(wm == false)
        addr_i = 0;
    else
        addr_i = addr_i+1;
    end;
    
    %the reset signal of the register we_i is assigned to wm.
    if(wm == false)
        we_i = false;
    else
        we_i = true;
    end;
        
    %the reset signal of the register state is assigned to 'ena'.
    if(ena == false)
        state = st0_IDLE;
    end;
    
    switch double(state)
        case st0_IDLE
            state = st1_WAITING_PULSE;
            full_i = 0;
            wm = false;
        case st1_WAITING_PULSE
            if(x > threshold)
                state = st2_WRITING_MEMORY;
            end
            full_i = 0;
            wm = false;
        case st2_WRITING_MEMORY
            if(addr == addr_max)
                state = st3_MEMORY_FULL;
            end
            full_i = 0;
            wm = true;
        case st3_MEMORY_FULL
            %stay here until clear comes
            if(clear == true)
                state = st1_WAITING_PULSE;
            end
            full_i = 1;
            wm = false;
        otherwise
            state = st0_IDLE; 
            full_i = 0;
            wm = false;
    end
    