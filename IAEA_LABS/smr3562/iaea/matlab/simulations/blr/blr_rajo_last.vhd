----------------------------------------------------------------------------------
-- Company: 
-- Engineer: M. Bogovac
-- 
-- Create Date: 04/07/2020 07:37:31 AM
-- Design Name: 
-- Module Name: test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: calculates the following recursive relation
--     y(n) = x(n) - u(n)
--     u(n) = u(n-1) + (1/2^m)*y(n)
--   it is the same as 
--     y(n) = x(n) - (1/2^m)*u(n)    subtract scaled
--     u(n) = u(n-1) + y(n)          accumulate

-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blr is
   Generic
      (      
	  SHIFT_WIDTH	: integer	:= 4
	  );
   Port 
      (  
      x              : in std_logic_vector(15 downto 0);   --top modul does not support VHDL 2008   
      y              : out std_logic_vector(15 downto 0);           
      threshold_high : in std_logic_vector(15 downto 0);   
	  threshold_low  : in std_logic_vector(15 downto 0);   
      resetn         : in std_logic;
      clk            : in std_logic;
      blr_enable     : in std_logic;
	  m              : in std_logic_vector (SHIFT_WIDTH-1 downto 0);
	  dbg_acc        : out std_logic_vector(15 downto 0)

      );
end blr;

architecture Behavioral of blr is
   
   signal en          : std_logic;
   signal y16_reg     : signed(15 downto 0):= (others => '0');        
   signal y16         : signed(15 downto 0);        
   signal y32         : signed(31 downto 0);        
   signal x16         : signed(15 downto 0);        
   signal x32         : signed(31 downto 0);        
   signal u16              : signed(15 downto 0);        
   signal u32              : signed(31 downto 0);        
   signal acc32            : signed(31 downto 0) := (others => '0');        
   signal threshold_high16 : signed(15 downto 0);        
   --signal threshold_high32 : signed(31 downto 0);        
   signal threshold_low16 : signed(15 downto 0);        
   --signal threshold_low32 : signed(31 downto 0);        
begin

   -- inputs conversion to sign
   x16              <= signed(x);
   x32              <= resize(x16,32);
   threshold_high16 <= signed(threshold_high);
   threshold_low16  <= signed(threshold_low);
   --outputs
   
   dbg_acc <= std_logic_vector(resize(acc32,16));
   
   
   y <= std_logic_vector(y16_reg);-- when blr_enable = '1' else x;
   
   dbg_acc <= std_logic_vector(resize(acc32,16));
   
   --register output signal
   y16_register :process (clk)
   begin
      if clk'event and clk ='1' then
         if resetn = '0' then
            y16_reg <= (others => '0');
         else
            y16_reg <= y16;
         end if;
      end if;
   end process;
 

 
   --
   -- implementation of the recursive relation
   --
    
   -- enable accumulator
   -- compare threshold with registered output
   --en <= '1' when y16_reg < threshold16 else '0';  
   
   
   en <= '1' when y16_reg <  threshold_high16 and y16_reg > threshold_low16  else '0';
    
   -- calculate y(n) = x(n) - u(n)
   y32 <= x32 - u32;
    
   -- calculate (1/2^m)*u(n)
   u32 <= shift_right(acc32,to_integer(unsigned(m)));

   u16 <= resize(u32,16);
   y16 <= resize(y32,16); 
   -- calculate u(n) = u(n-1) + y(n)
   --y32 <= resize(y16,32);
   accumulator : process (clk)
   begin
      if clk='1' and clk'event then
	     if resetn = '0' or blr_enable = '0' then
            acc32 <= (others => '0');
		 elsif en = '1' then
		    acc32 <= acc32 + y32;
		 end if;
      end if;
   end process;

    --
    -- end of implementation of the recursive relation
    --

end Behavioral;
