----------------------------------------------------------------------------------
-- Company: 
-- Engineer: M. Bogovac
-- 
-- Create Date: 04/07/2020 07:37:31 AM
-- Design Name: 
-- Module Name: test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: calculates the following
--     y(n) = x(n) - v(n)
--	   v(n)= (1/2^m)*u(n)		 
--     u(n) = acc(n) - acc(n-2^m)
--	   acc(n) =  acc(n-1) + x(n)	

-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blr is 
	generic
		(
		BLR_SHIFT_WIDTH	: integer	:= 10;
		BLR_ACCUMULATOR_WIDTH :	integer := 32;
		BLR_DATA_WIDTH	: integer	:= 16
		);
	port 
		(  
		x              : in std_logic_vector(15 downto 0);   --top modul does not support VHDL 2008   
		y              : out std_logic_vector(15 downto 0);           
		threshold_high : in std_logic_vector(15 downto 0);   
		threshold_low  : in std_logic_vector(15 downto 0);   
		resetn         : in std_logic;
		clk            : in std_logic;
		ce             : in std_logic;
		blr_enable     : in std_logic;
		delay          : in std_logic_vector (9 downto 0);	-- must be (2^m)-1
		m              : in std_logic_vector (9 downto 0);
		dbg_acc        : out std_logic_vector(15 downto 0)
	  );
end blr;

architecture Behavioral of blr is
   
	signal en   : std_logic := '0';        
	signal en_delay_line : std_logic; 
	signal y16  : signed(BLR_DATA_WIDTH-1 downto 0);        
	signal y32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);        
	signal x16  : signed(BLR_DATA_WIDTH-1 downto 0);        
	signal x32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);        
	signal u32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);        
	signal v32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0);        
	signal acc32  : signed(BLR_ACCUMULATOR_WIDTH-1 downto 0) := (others => '0');        
	signal acc32_m 	: std_logic_vector(BLR_ACCUMULATOR_WIDTH-1 downto 0) := (others => '0');
	signal threshold_high16 : signed(BLR_DATA_WIDTH-1 downto 0);        
	signal threshold_low16 : signed(BLR_DATA_WIDTH-1 downto 0);        

	component delay_line is
    generic
		(
			DL_DATA_WIDTH : integer :=32;
			DL_ADDRESS_WIDTH : integer :=10
		);

	port(   clk, en : in std_logic;
			delay : in std_logic_vector(DL_ADDRESS_WIDTH-1 downto 0);
			dia : in std_logic_vector(DL_DATA_WIDTH-1 downto 0);
			dob : out std_logic_vector(DL_DATA_WIDTH-1 downto 0));
	end component;
   
begin

	-- inputs conversion to sign
	x16              <= signed(x);
	x32              <= resize(x16,BLR_ACCUMULATOR_WIDTH);
	threshold_high16 <= signed(threshold_high);
	threshold_low16  <= signed(threshold_low);

	-- outputs 		
	y <= std_logic_vector(y16);
	y16 <= resize(y32,BLR_DATA_WIDTH);
	dbg_acc <= std_logic_vector(resize(acc32,BLR_DATA_WIDTH));  
			 
	--enable signals
	en <= '1' when x16 <  threshold_high16 and x16 > threshold_low16  else '0';
	en_delay_line <= en and blr_enable; 

	--calculate final result: y32(n) = x32(n)-u32(n)
	subtractor1 : process (clk)
	begin
	  if clk='1' and clk'event then
			y32 <= x32 - v32;
	  end if;
	end process;

	-- calculate (1/2^m)*u32(n)
	v32 <= shift_right(u32,2);--to_integer(unsigned(m)));

	--calculate u32(n) = acc32(n)-acc32(n-m))
	subtractor2 : process (clk)
	begin
	  if clk='1' and clk'event then
		 if resetn = '0' or blr_enable = '0' then
			u32 <= (others => '0');
		 elsif en = '1' then
			u32 <= acc32 - signed(acc32_m);
		 end if;
	  end if;
	end process;
	 
	--calculate acc32(n)
	accumulator : process (clk)
	begin
	  if clk='1' and clk'event then
		 if resetn = '0' or blr_enable = '0' then
			acc32 <= (others => '0');
		 elsif en = '1' then
			acc32 <= acc32 + x32;
		 end if;
	  end if;
	end process;

	--calculate acc32(n-m))
	delay_line_inst : delay_line
	generic map(DL_DATA_WIDTH => BLR_ACCUMULATOR_WIDTH, DL_ADDRESS_WIDTH => BLR_SHIFT_WIDTH)
	port map( clk=>clk, en=>en_delay_line, delay => delay, dia => std_logic_vector(acc32),dob => acc32_m);

end Behavioral;
