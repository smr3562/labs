----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/23/2020 10:28:33 PM
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter is
    generic (COUNTER_WIDTH : integer := 10);
    port(   clk, resetn, clr : in std_logic;
            q : out std_logic_vector(COUNTER_WIDTH-1 downto 0));
end counter;

architecture Behavioral of counter is	 
	 
    signal tmp: std_logic_vector(COUNTER_WIDTH-1 downto 0);
begin
    process (clk, clr)
    begin
        if (clk'event and clk='1') then
			if (clr='1' or resetn='0') then
				tmp <= (others => '0');
			else
				tmp <= tmp + 1;
			end if;
        end if;
    end process;
    q <= tmp;

end Behavioral;




