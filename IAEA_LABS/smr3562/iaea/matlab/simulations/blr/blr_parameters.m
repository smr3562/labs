noise = 1e-12;
B = 13;

Tclk = 20e-9;
Taud = 0.5e-6;
Taud_generator = 0.5e-6;

%slow filter
Taupk = 1e-6;
Taupk_top = 0.2e-6;
dc_offset_manual = 0;
dc_offset_amplifier = 0;%-0.050;
b10 = exp(-Tclk/Taud);
na = (Taupk          )/Tclk;
nb = (Taupk+Taupk_top)/Tclk;
nad = na - 3;
nbd = nb - 3;
Bna = 10;
Bnb = 10;
na_inv = 1./na;

blr_thr_hi = 0.007;
blr_thr_lo =  -0.15;
blr_m = 5;
blr_delay = 2^blr_m;