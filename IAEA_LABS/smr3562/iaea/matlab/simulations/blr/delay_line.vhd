----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/23/2020 11:10:26 PM
-- Design Name: 
-- Module Name: delay_line - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_line is
    generic(
			DL_DATA_WIDTH : integer :=32;
			DL_ADDRESS_WIDTH : integer :=10
		);

    port(   clk : in std_logic;
			resetn : in std_logic;
            delay : in std_logic_vector(DL_ADDRESS_WIDTH-1 downto 0);
            dia : in std_logic_vector(DL_DATA_WIDTH-1 downto 0);
            dob : out std_logic_vector(DL_DATA_WIDTH-1 downto 0));
end delay_line;

architecture Behavioral of delay_line is

--instantiate comparator and generate terminal count output 
    component comparator is
        generic(COMPARATOR_WIDTH : integer :=10);
        port(  a, b : in std_logic_vector(COMPARATOR_WIDTH-1 downto 0);
               cmp : out std_logic);
    end component;
    
    component counter is
        generic (COUNTER_WIDTH : integer := 10);
        port(   clk, resetn, clr : in std_logic;
                q : out std_logic_vector(COUNTER_WIDTH-1 downto 0));
    end component;

    component sdp_ram is
	generic(
			RAM_DATA_WIDTH : integer :=32;
			RAM_ADDRESS_WIDTH : integer :=10
		);	
    port(
        clk : in std_logic;
        ena : in std_logic;
        enb : in std_logic;
        wea : in std_logic;
        addra : in std_logic_vector(RAM_ADDRESS_WIDTH-1 downto 0);
        addrb : in std_logic_vector(RAM_ADDRESS_WIDTH-1 downto 0);
        dia : in std_logic_vector(RAM_DATA_WIDTH-1 downto 0);
        dob : out std_logic_vector(RAM_DATA_WIDTH-1 downto 0)
    );
    end component;

    signal counter_clr : std_logic;
    signal counter_out : std_logic_vector(DL_ADDRESS_WIDTH-1 downto 0);




    
begin
sdp_ram_inst: sdp_ram
	generic map(RAM_DATA_WIDTH => DL_DATA_WIDTH, RAM_ADDRESS_WIDTH => DL_ADDRESS_WIDTH)
    port map(
        clk => clk,
        ena => '1',
        enb => '1',
        wea => '1',
        addra => counter_out,
        addrb => counter_out,
        dia => dia,
        dob => dob
    );

--instantiate comparator and generate terminal count output 
comparator_inst: comparator
    generic map(COMPARATOR_WIDTH => DL_ADDRESS_WIDTH)
	port map(a=>delay, b=>counter_out, cmp => counter_clr);	

--instantiate counter
counter_inst: counter
    generic map(COUNTER_WIDTH => DL_ADDRESS_WIDTH)
	port map(clk=>clk, resetn=>resetn, clr=>counter_clr, q=>counter_out);

end Behavioral;
