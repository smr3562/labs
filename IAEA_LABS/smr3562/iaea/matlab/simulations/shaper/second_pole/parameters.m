Tclk=20.0e-9;
taud=5.0e-6;
taur=0.50e-6;
gain0=0.9;
gain1=(taud/taur)^(taur/(taur-taud))-(taud/taur)^(taud/(taur-taud));
gain2=exp(-Tclk/taud)-exp(-Tclk/taur); %theory
gain=(gain1/gain2);%*exp(-Tclk/taud); 
b10=exp(-Tclk/taud);
p10=exp(-Tclk/taur);