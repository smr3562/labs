function blkStruct = slblocks
		% This function specifies that the library should appear
		% in the Library Browser
		% and be cached in the browser repository
        
         	     
        Browser(3).Library = 'Delay_Line';
        Browser(3).Name = 'Delay Line';
        Browser(3).IsTopLevel  = 0;% 
        
        blkStruct.Browser = Browser;