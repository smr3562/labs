function blkStruct = slblocks
		% This function specifies that the library should appear
		% in the Library Browser
		% and be cached in the browser repository
                 
		Browser(1).Library = 'Pulse_Generator';
        Browser(1).Name = 'Pulse Generator';
        Browser(1).IsTopLevel  = 0; 
                   
        blkStruct.Browser = Browser;
     
    