function blkStruct = slblocks
		% This function specifies that the library should appear
		% in the Library Browser
		% and be cached in the browser repository
        
         		       
        Browser(1).Library    = 'IAEA PZC Excercise';
        Browser(1).Name       = 'IAEA PZC Excercise';
        Browser(1).Type       = 'Palette';
        Browser(1).Children   = ...
        { 
          'Pulse Generator', 'Delay Line', ...
          'Sign Inverter'
        };                            
        
        blkStruct.Browser = Browser;

