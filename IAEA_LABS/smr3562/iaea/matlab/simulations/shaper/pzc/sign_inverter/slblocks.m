function blkStruct = slblocks
		% This function specifies that the library should appear
		% in the Library Browser
		% and be cached in the browser repository
        
         	     
        Browser(4).Library = 'Sign_Inverter';
        Browser(4).Name = 'Sign Inverter';
        Browser(4).IsTopLevel  = 0; 
        
        blkStruct.Browser = Browser;