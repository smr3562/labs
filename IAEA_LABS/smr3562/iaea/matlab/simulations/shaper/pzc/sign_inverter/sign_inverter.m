% Input: x : Signed number
% en: bool: 
% Output: y 
% 
% calculates:
% y = -x if en = true 
% y =  x if en = false
%
% MCode block literature
% Title: System Generator for DSP Reference Guide, UG638, 
% File name: sysgen_ref.pdf, Chapter 1
% Edition: v11.4 December 2, 2009, page 220 - 241
 
function y = sign_inverter(x,en)
 
%extract data type properties of the input number x
arith = xl_arith(x);
if(arith ~= xlSigned)
    error('my message input must be signed number');
end
nbits = xl_nbits(x);
binpt = xl_binpt(x);
 
%in 2's complement sign inversion is done by the following procedure:
%1.  number is reinterpret as unsigned integer 
xunsigned = xl_force(x,xlUnsigned,0);
%2. each bit is inverted (xl_not) and 1 is added
xsgninvbin = xfix({xlUnsigned,nbits,0},xl_not(xunsigned)+1);
%3. result is reinterpret in original representation
xsgninvxfix = xl_force(xsgninvbin,arith,binpt);
 
disp(['nbits=',num2str(nbits),' binpt=',num2str(binpt),' x Unsigned=',num2str(xsgninvbin)]);
 
%output result depending on 'en' port
if(en)
    %reinterpret xsgninvbin with same format as x
    y = xsgninvxfix;
  else
    y = x;
end
