Tclk = 20e-9;
Tpprd = 800e-6;
Taud = 5e-6;
dc = 0;%-0.98;
Taupk = 6e-6;
Taupk_top = 2e-6;
Taupa = 80e-6;
Taua = 120e-6;
Taupaguess = 80e-6;

b10 = exp(-Tclk/Taud);
B = 13;
na = (Taupk          )/Tclk;
nb = (Taupk+Taupk_top)/Tclk;
nad = na - 3;
nbd = nb - 3;
Bna = 10;
Bnb = 10;
na_inv = 1./na;

%PZC filter parameters (PZC as correction to unfolder)
b1=exp(-Tclk/Taupaguess);
a0 = (1-Taud /Taua)/(1-Taud /Taupaguess)*exp(-Tclk/Taupaguess)+  ...
      (1-Taupaguess/Taua)/(1-Taupaguess/Taud )*exp(-Tclk/Taud );

%PZC filter parameters (PZC as correction to unfolder)
%gain=delta*a01*(1-b10)/(1-a01);
