noise = 1e-12;
B = 13;

Tclk = 20e-9;
Taud = 0.5e-6;
Taud_generator = 0.5e-6;

%slow filter
Taupk = 1e-6;
Taupk_top = 0.2e-6;
dc_offset_manual = -0.8;
dc_offset_amplifier = -0.85;
dc_stabilizer_thr_hi = 0.007;
dc_stabilizer_thr_lo = -0.9;
blr_thr_hi = 0.007;
blr_thr_lo = -0.9;
b10 = exp(-Tclk/Taud);
na = (Taupk          )/Tclk;
nb = (Taupk+Taupk_top)/Tclk;
nad = na - 3;
nbd = nb - 3;
Bna = 10;
Bnb = 10;
na_inv = 1./na;

%fast filter
ff_Taupk = 1e-6;
ff_Taupk_top = 0.2e-6;
ff_dc_offset_manual = -0.8;
ff_dc_offset_amplifier = -0.85;
ff_dc_stabilizer_thr_hi = 0.007;
ff_dc_stabilizer_thr_lo = -0.9;
ff_blr_thr_hi = 0.007;
ff_blr_thr_lo = -0.9;
ff_b10 = exp(-Tclk/Taud);
ff_na = (Taupk          )/Tclk;
ff_nb = (Taupk+Taupk_top)/Tclk;
ff_nad = na - 3;
ff_nbd = nb - 3;
ff_Bna = 10;
ff_Bnb = 10;
ff_na_inv = 1./na;
